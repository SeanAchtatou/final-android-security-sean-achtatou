package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.graphics.g2d.c;
import com.badlogic.gdx.utils.f0;
import com.badlogic.gdx.utils.g0;
import com.badlogic.gdx.utils.m;
import com.esotericsoftware.spine.Animation;
import e.d.b.t.b;

/* compiled from: GlyphLayout */
public class e implements f0.a {

    /* renamed from: a  reason: collision with root package name */
    public final com.badlogic.gdx.utils.a<a> f4842a = new com.badlogic.gdx.utils.a<>();

    /* renamed from: b  reason: collision with root package name */
    public float f4843b;

    /* renamed from: c  reason: collision with root package name */
    public float f4844c;

    /* renamed from: d  reason: collision with root package name */
    private final com.badlogic.gdx.utils.a<b> f4845d = new com.badlogic.gdx.utils.a<>(4);

    /* compiled from: GlyphLayout */
    public static class a implements f0.a {

        /* renamed from: a  reason: collision with root package name */
        public com.badlogic.gdx.utils.a<c.b> f4846a = new com.badlogic.gdx.utils.a<>();

        /* renamed from: b  reason: collision with root package name */
        public m f4847b = new m();

        /* renamed from: c  reason: collision with root package name */
        public float f4848c;

        /* renamed from: d  reason: collision with root package name */
        public float f4849d;

        /* renamed from: e  reason: collision with root package name */
        public float f4850e;

        /* renamed from: f  reason: collision with root package name */
        public final b f4851f = new b();

        public void reset() {
            this.f4846a.clear();
            this.f4847b.a();
            this.f4850e = Animation.CurveTimeline.LINEAR;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder(this.f4846a.f5374b);
            com.badlogic.gdx.utils.a<c.b> aVar = this.f4846a;
            int i2 = aVar.f5374b;
            for (int i3 = 0; i3 < i2; i3++) {
                sb.append((char) aVar.get(i3).f4820a);
            }
            sb.append(", #");
            sb.append(this.f4851f);
            sb.append(", ");
            sb.append(this.f4848c);
            sb.append(", ");
            sb.append(this.f4849d);
            sb.append(", ");
            sb.append(this.f4850e);
            return sb.toString();
        }
    }

    public void a(c cVar, CharSequence charSequence) {
        a(cVar, charSequence, 0, charSequence.length(), cVar.getColor(), Animation.CurveTimeline.LINEAR, 8, false, null);
    }

    public void reset() {
        g0.a(a.class).freeAll(this.f4842a);
        this.f4842a.clear();
        this.f4843b = Animation.CurveTimeline.LINEAR;
        this.f4844c = Animation.CurveTimeline.LINEAR;
    }

    public String toString() {
        if (this.f4842a.f5374b == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder(128);
        sb.append(this.f4843b);
        sb.append('x');
        sb.append(this.f4844c);
        sb.append(10);
        int i2 = this.f4842a.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            sb.append(this.f4842a.get(i3).toString());
            sb.append(10);
        }
        sb.setLength(sb.length() - 1);
        return sb.toString();
    }

    public void a(c cVar, CharSequence charSequence, b bVar, float f2, int i2, boolean z) {
        a(cVar, charSequence, 0, charSequence.length(), bVar, f2, i2, z, null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:128:0x03a6  */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x03c6  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00af  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.badlogic.gdx.graphics.g2d.c r29, java.lang.CharSequence r30, int r31, int r32, e.d.b.t.b r33, float r34, int r35, boolean r36, java.lang.String r37) {
        /*
            r28 = this;
            r7 = r28
            r6 = r30
            r8 = r32
            r0 = r29
            com.badlogic.gdx.graphics.g2d.c$a r9 = r0.f4803a
            if (r37 == 0) goto L_0x000e
            r12 = 1
            goto L_0x001c
        L_0x000e:
            float r0 = r9.u
            r1 = 1077936128(0x40400000, float:3.0)
            float r0 = r0 * r1
            int r0 = (r34 > r0 ? 1 : (r34 == r0 ? 0 : -1))
            if (r0 > 0) goto L_0x001a
            r12 = 0
            goto L_0x001c
        L_0x001a:
            r12 = r36
        L_0x001c:
            boolean r13 = r9.q
            java.lang.Class<com.badlogic.gdx.graphics.g2d.e$a> r0 = com.badlogic.gdx.graphics.g2d.e.a.class
            com.badlogic.gdx.utils.f0 r14 = com.badlogic.gdx.utils.g0.a(r0)
            com.badlogic.gdx.utils.a<com.badlogic.gdx.graphics.g2d.e$a> r15 = r7.f4842a
            r14.freeAll(r15)
            r15.clear()
            com.badlogic.gdx.utils.a<e.d.b.t.b> r5 = r7.f4845d
            r0 = r33
            r5.add(r0)
            java.lang.Class<e.d.b.t.b> r1 = e.d.b.t.b.class
            com.badlogic.gdx.utils.f0 r4 = com.badlogic.gdx.utils.g0.a(r1)
            r16 = 0
            r2 = r31
            r3 = r0
            r22 = r3
            r1 = r16
            r11 = 0
            r17 = 0
            r19 = 0
            r20 = 0
            r21 = 0
            r0 = r2
        L_0x004c:
            if (r0 != r8) goto L_0x0065
            if (r2 != r8) goto L_0x005b
            r26 = r4
            r13 = r5
            r0 = r17
            r8 = r19
        L_0x0057:
            r1 = r20
            goto L_0x01a8
        L_0x005b:
            r33 = r1
            r10 = r8
            r25 = r22
            r24 = 0
            r22 = r0
            goto L_0x00ac
        L_0x0065:
            int r10 = r0 + 1
            char r0 = r6.charAt(r0)
            r33 = r1
            r1 = 10
            if (r0 == r1) goto L_0x00a3
            r1 = 91
            if (r0 == r1) goto L_0x0076
            goto L_0x009a
        L_0x0076:
            if (r13 == 0) goto L_0x009a
            int r0 = r7.a(r6, r10, r8, r4)
            if (r0 < 0) goto L_0x0092
            int r1 = r10 + -1
            int r0 = r0 + 1
            int r10 = r10 + r0
            java.lang.Object r0 = r5.peek()
            e.d.b.t.b r0 = (e.d.b.t.b) r0
            r25 = r0
            r22 = r10
            r0 = -1
            r24 = 0
            r10 = r1
            goto L_0x00ad
        L_0x0092:
            r1 = -2
            if (r0 != r1) goto L_0x009a
            int r0 = r10 + 1
            r1 = r33
            goto L_0x004c
        L_0x009a:
            r25 = r22
            r0 = -1
            r24 = 0
            r22 = r10
            r10 = -1
            goto L_0x00ad
        L_0x00a3:
            int r0 = r10 + -1
            r25 = r22
            r24 = 1
            r22 = r10
            r10 = r0
        L_0x00ac:
            r0 = -1
        L_0x00ad:
            if (r10 == r0) goto L_0x03c6
            if (r10 == r2) goto L_0x0392
            java.lang.Object r0 = r14.obtain()
            r1 = r0
            com.badlogic.gdx.graphics.g2d.e$a r1 = (com.badlogic.gdx.graphics.g2d.e.a) r1
            e.d.b.t.b r0 = r1.f4851f
            r0.b(r3)
            r0 = r9
            r3 = r1
            r31 = r2
            r2 = r30
            r6 = r3
            r29 = r13
            r13 = 0
            r3 = r31
            r13 = r4
            r4 = r10
            r26 = r13
            r13 = r5
            r5 = r33
            r0.a(r1, r2, r3, r4, r5)
            com.badlogic.gdx.utils.a<com.badlogic.gdx.graphics.g2d.c$b> r0 = r6.f4846a
            int r0 = r0.f5374b
            if (r0 != 0) goto L_0x00e4
            r14.free(r6)
            r0 = r33
            r2 = 0
            r5 = 0
            r23 = 1
            goto L_0x039f
        L_0x00e4:
            r0 = r33
            if (r0 == 0) goto L_0x0103
            boolean r1 = r0.n
            if (r1 == 0) goto L_0x00f4
            int r0 = r0.l
            float r0 = (float) r0
            float r1 = r9.o
            float r0 = r0 * r1
            goto L_0x0101
        L_0x00f4:
            int r1 = r0.f4823d
            int r0 = r0.f4829j
            int r1 = r1 + r0
            float r0 = (float) r1
            float r1 = r9.o
            float r0 = r0 * r1
            float r1 = r9.f4814f
            float r0 = r0 - r1
        L_0x0101:
            float r17 = r17 - r0
        L_0x0103:
            r0 = r17
            com.badlogic.gdx.utils.a<com.badlogic.gdx.graphics.g2d.c$b> r1 = r6.f4846a
            java.lang.Object r1 = r1.peek()
            com.badlogic.gdx.graphics.g2d.c$b r1 = (com.badlogic.gdx.graphics.g2d.c.b) r1
            r6.f4848c = r0
            r6.f4849d = r11
            if (r24 != 0) goto L_0x0115
            if (r10 != r8) goto L_0x0118
        L_0x0115:
            r7.a(r9, r6)
        L_0x0118:
            r15.add(r6)
            com.badlogic.gdx.utils.m r2 = r6.f4847b
            float[] r3 = r2.f5527a
            int r2 = r2.f5528b
            if (r12 != 0) goto L_0x0132
            r4 = 0
            r5 = 0
        L_0x0125:
            if (r4 >= r2) goto L_0x012e
            r17 = r3[r4]
            float r5 = r5 + r17
            int r4 = r4 + 1
            goto L_0x0125
        L_0x012e:
            float r0 = r0 + r5
            r6.f4850e = r5
            goto L_0x013d
        L_0x0132:
            r4 = 0
            r5 = r3[r4]
            float r0 = r0 + r5
            r5 = r3[r4]
            r6.f4850e = r5
            r4 = 1
            if (r2 >= r4) goto L_0x0146
        L_0x013d:
            r3 = r0
            r6 = r19
            r2 = 0
        L_0x0141:
            r5 = 0
            r23 = 1
            goto L_0x03a4
        L_0x0146:
            r5 = r3[r4]
            float r0 = r0 + r5
            float r5 = r6.f4850e
            r17 = r3[r4]
            float r5 = r5 + r17
            r6.f4850e = r5
            r4 = 2
            r17 = r11
            r5 = 2
            r11 = r6
            r6 = r19
        L_0x0158:
            if (r5 >= r2) goto L_0x0388
            com.badlogic.gdx.utils.a<com.badlogic.gdx.graphics.g2d.c$b> r4 = r11.f4846a
            r33 = r1
            int r1 = r5 + -1
            java.lang.Object r4 = r4.get(r1)
            com.badlogic.gdx.graphics.g2d.c$b r4 = (com.badlogic.gdx.graphics.g2d.c.b) r4
            r19 = r1
            int r1 = r4.f4823d
            int r4 = r4.f4829j
            int r1 = r1 + r4
            float r1 = (float) r1
            float r4 = r9.o
            float r1 = r1 * r4
            float r4 = r9.f4814f
            float r1 = r1 - r4
            float r1 = r1 + r0
            int r1 = (r1 > r34 ? 1 : (r1 == r34 ? 0 : -1))
            if (r1 > 0) goto L_0x018f
            r1 = r3[r5]
            float r0 = r0 + r1
            float r1 = r11.f4850e
            r4 = r3[r5]
            float r1 = r1 + r4
            r11.f4850e = r1
            r4 = r33
            r1 = r2
            r18 = r5
            r2 = 0
            r5 = 0
            r23 = 1
            goto L_0x0380
        L_0x018f:
            if (r37 == 0) goto L_0x025c
            r0 = r28
            r1 = r9
            r2 = r11
            r3 = r34
            r4 = r37
            r8 = r6
            r6 = r14
            r0.a(r1, r2, r3, r4, r5, r6)
            float r0 = r11.f4848c
            float r1 = r11.f4850e
            float r17 = r0 + r1
            r0 = r17
            goto L_0x0057
        L_0x01a8:
            float r0 = java.lang.Math.max(r8, r0)
            int r2 = r13.f5374b
            r3 = 1
        L_0x01af:
            if (r3 >= r2) goto L_0x01bd
            java.lang.Object r4 = r13.get(r3)
            r6 = r26
            r6.free(r4)
            int r3 = r3 + 1
            goto L_0x01af
        L_0x01bd:
            r13.clear()
            r2 = r35 & 8
            if (r2 != 0) goto L_0x022a
            r2 = 1
            r3 = r35 & 1
            if (r3 == 0) goto L_0x01cc
            r18 = 1
            goto L_0x01ce
        L_0x01cc:
            r18 = 0
        L_0x01ce:
            r2 = -822083584(0xffffffffcf000000, float:-2.14748365E9)
            int r3 = r15.f5374b
            r2 = 0
            r4 = 0
            r5 = -822083584(0xffffffffcf000000, float:-2.14748365E9)
            r23 = 0
        L_0x01d8:
            r6 = 1073741824(0x40000000, float:2.0)
            if (r2 >= r3) goto L_0x0211
            java.lang.Object r8 = r15.get(r2)
            com.badlogic.gdx.graphics.g2d.e$a r8 = (com.badlogic.gdx.graphics.g2d.e.a) r8
            float r10 = r8.f4849d
            int r11 = (r10 > r5 ? 1 : (r10 == r5 ? 0 : -1))
            if (r11 == 0) goto L_0x0205
            float r4 = r34 - r4
            if (r18 == 0) goto L_0x01ed
            float r4 = r4 / r6
        L_0x01ed:
            r5 = r4
            r4 = r23
        L_0x01f0:
            if (r4 >= r2) goto L_0x0201
            int r6 = r4 + 1
            java.lang.Object r4 = r15.get(r4)
            com.badlogic.gdx.graphics.g2d.e$a r4 = (com.badlogic.gdx.graphics.g2d.e.a) r4
            float r11 = r4.f4848c
            float r11 = r11 + r5
            r4.f4848c = r11
            r4 = r6
            goto L_0x01f0
        L_0x0201:
            r23 = r4
            r5 = r10
            r4 = 0
        L_0x0205:
            float r6 = r8.f4848c
            float r8 = r8.f4850e
            float r6 = r6 + r8
            float r4 = java.lang.Math.max(r4, r6)
            int r2 = r2 + 1
            goto L_0x01d8
        L_0x0211:
            float r2 = r34 - r4
            if (r18 == 0) goto L_0x0216
            float r2 = r2 / r6
        L_0x0216:
            r4 = r2
            r2 = r23
        L_0x0219:
            if (r2 >= r3) goto L_0x022a
            int r5 = r2 + 1
            java.lang.Object r2 = r15.get(r2)
            com.badlogic.gdx.graphics.g2d.e$a r2 = (com.badlogic.gdx.graphics.g2d.e.a) r2
            float r6 = r2.f4848c
            float r6 = r6 + r4
            r2.f4848c = r6
            r2 = r5
            goto L_0x0219
        L_0x022a:
            r7.f4843b = r0
            boolean r0 = r9.f4812d
            if (r0 == 0) goto L_0x0245
            float r0 = r9.f4818j
            float r1 = (float) r1
            float r2 = r9.m
            float r1 = r1 * r2
            float r0 = r0 + r1
            r4 = r21
            float r1 = (float) r4
            float r1 = r1 * r2
            float r2 = r9.n
            float r1 = r1 * r2
            float r0 = r0 + r1
            r7.f4844c = r0
            goto L_0x025b
        L_0x0245:
            r4 = r21
            float r0 = r9.f4818j
            float r1 = (float) r1
            float r2 = r9.m
            float r3 = -r2
            float r1 = r1 * r3
            float r0 = r0 + r1
            float r1 = (float) r4
            float r2 = -r2
            float r1 = r1 * r2
            float r2 = r9.n
            float r1 = r1 * r2
            float r0 = r0 + r1
            r7.f4844c = r0
        L_0x025b:
            return
        L_0x025c:
            r3 = r6
            r4 = r21
            r6 = r26
            com.badlogic.gdx.utils.a<com.badlogic.gdx.graphics.g2d.c$b> r0 = r11.f4846a
            int r0 = r9.a(r0, r5)
            float r1 = r11.f4848c
            r2 = 0
            int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r1 != 0) goto L_0x0270
            if (r0 == 0) goto L_0x0279
        L_0x0270:
            com.badlogic.gdx.utils.a<com.badlogic.gdx.graphics.g2d.c$b> r1 = r11.f4846a
            int r1 = r1.f5374b
            if (r0 < r1) goto L_0x0277
            goto L_0x0279
        L_0x0277:
            r19 = r0
        L_0x0279:
            if (r19 != 0) goto L_0x0324
            r0 = 0
            r11.f4850e = r0
            com.badlogic.gdx.utils.a<com.badlogic.gdx.graphics.g2d.c$b> r0 = r11.f4846a
            int r0 = r0.f5374b
            r1 = r19
        L_0x0284:
            if (r1 >= r0) goto L_0x029b
            com.badlogic.gdx.utils.a<com.badlogic.gdx.graphics.g2d.c$b> r2 = r11.f4846a
            java.lang.Object r2 = r2.get(r1)
            com.badlogic.gdx.graphics.g2d.c$b r2 = (com.badlogic.gdx.graphics.g2d.c.b) r2
            int r2 = r2.f4820a
            char r2 = (char) r2
            boolean r2 = r9.d(r2)
            if (r2 != 0) goto L_0x0298
            goto L_0x029b
        L_0x0298:
            int r1 = r1 + 1
            goto L_0x0284
        L_0x029b:
            if (r1 <= 0) goto L_0x02ab
            com.badlogic.gdx.utils.a<com.badlogic.gdx.graphics.g2d.c$b> r0 = r11.f4846a
            int r2 = r1 + -1
            r5 = 0
            r0.a(r5, r2)
            com.badlogic.gdx.utils.m r0 = r11.f4847b
            r2 = 1
            r0.a(r2, r1)
        L_0x02ab:
            com.badlogic.gdx.utils.m r0 = r11.f4847b
            com.badlogic.gdx.utils.a<com.badlogic.gdx.graphics.g2d.c$b> r1 = r11.f4846a
            java.lang.Object r1 = r1.a()
            com.badlogic.gdx.graphics.g2d.c$b r1 = (com.badlogic.gdx.graphics.g2d.c.b) r1
            int r1 = r1.f4829j
            int r1 = -r1
            float r1 = (float) r1
            float r2 = r9.o
            float r1 = r1 * r2
            float r2 = r9.f4816h
            float r1 = r1 - r2
            r2 = 0
            r0.a(r2, r1)
            int r0 = r15.f5374b
            r1 = 1
            if (r0 <= r1) goto L_0x031e
            int r0 = r0 + -2
            java.lang.Object r0 = r15.get(r0)
            com.badlogic.gdx.graphics.g2d.e$a r0 = (com.badlogic.gdx.graphics.g2d.e.a) r0
            com.badlogic.gdx.utils.a<com.badlogic.gdx.graphics.g2d.c$b> r2 = r0.f4846a
            int r2 = r2.f5374b
            int r2 = r2 - r1
        L_0x02d6:
            if (r2 <= 0) goto L_0x02fe
            com.badlogic.gdx.utils.a<com.badlogic.gdx.graphics.g2d.c$b> r1 = r0.f4846a
            java.lang.Object r1 = r1.get(r2)
            com.badlogic.gdx.graphics.g2d.c$b r1 = (com.badlogic.gdx.graphics.g2d.c.b) r1
            int r1 = r1.f4820a
            char r1 = (char) r1
            boolean r1 = r9.d(r1)
            if (r1 != 0) goto L_0x02ea
            goto L_0x02fe
        L_0x02ea:
            float r1 = r0.f4850e
            com.badlogic.gdx.utils.m r5 = r0.f4847b
            r21 = r4
            int r4 = r2 + 1
            float r4 = r5.b(r4)
            float r1 = r1 - r4
            r0.f4850e = r1
            int r2 = r2 + -1
            r4 = r21
            goto L_0x02d6
        L_0x02fe:
            r21 = r4
            com.badlogic.gdx.utils.a<com.badlogic.gdx.graphics.g2d.c$b> r1 = r0.f4846a
            int r4 = r2 + 1
            r1.g(r4)
            com.badlogic.gdx.utils.m r1 = r0.f4847b
            int r2 = r2 + 2
            r1.e(r2)
            r7.a(r9, r0)
            float r1 = r0.f4848c
            float r0 = r0.f4850e
            float r1 = r1 + r0
            float r0 = java.lang.Math.max(r3, r1)
            r26 = r6
            r6 = r0
            goto L_0x0354
        L_0x031e:
            r21 = r4
            r26 = r6
            r6 = r3
            goto L_0x0354
        L_0x0324:
            r21 = r4
            r0 = r28
            r1 = r9
            r2 = r11
            r4 = r3
            r3 = r14
            r26 = r6
            r6 = r4
            r4 = r19
            com.badlogic.gdx.graphics.g2d.e$a r0 = r0.a(r1, r2, r3, r4, r5)
            float r1 = r11.f4848c
            float r2 = r11.f4850e
            float r1 = r1 + r2
            float r19 = java.lang.Math.max(r6, r1)
            if (r0 != 0) goto L_0x034e
            float r0 = r9.m
            float r11 = r17 + r0
            int r20 = r20 + 1
            r1 = r16
            r6 = r19
            r2 = 0
            r3 = 0
            goto L_0x0141
        L_0x034e:
            r15.add(r0)
            r11 = r0
            r6 = r19
        L_0x0354:
            com.badlogic.gdx.utils.m r0 = r11.f4847b
            int r1 = r0.f5528b
            float[] r0 = r0.f5527a
            r2 = 0
            r3 = r0[r2]
            r4 = 1
            if (r1 <= r4) goto L_0x0363
            r5 = r0[r4]
            float r3 = r3 + r5
        L_0x0363:
            float r4 = r11.f4850e
            float r4 = r4 + r3
            r11.f4850e = r4
            float r4 = r9.m
            float r4 = r17 + r4
            int r20 = r20 + 1
            r5 = 0
            r11.f4848c = r5
            r11.f4849d = r4
            r17 = r4
            r4 = r16
            r18 = 1
            r23 = 1
            r27 = r3
            r3 = r0
            r0 = r27
        L_0x0380:
            int r18 = r18 + 1
            r2 = r1
            r1 = r4
            r5 = r18
            goto L_0x0158
        L_0x0388:
            r33 = r1
            r2 = 0
            r5 = 0
            r23 = 1
            r3 = r0
            r11 = r17
            goto L_0x03a4
        L_0x0392:
            r0 = r33
            r31 = r2
            r26 = r4
            r29 = r13
            r2 = 0
            r23 = 1
            r13 = r5
            r5 = 0
        L_0x039f:
            r1 = r0
            r3 = r17
            r6 = r19
        L_0x03a4:
            if (r24 == 0) goto L_0x03bd
            float r6 = java.lang.Math.max(r6, r3)
            float r0 = r9.m
            r1 = r31
            if (r10 != r1) goto L_0x03b7
            float r1 = r9.n
            float r0 = r0 * r1
            int r21 = r21 + 1
            goto L_0x03b9
        L_0x03b7:
            int r20 = r20 + 1
        L_0x03b9:
            float r11 = r11 + r0
            r1 = r16
            r3 = 0
        L_0x03bd:
            r17 = r3
            r19 = r6
            r0 = r22
            r3 = r25
            goto L_0x03d7
        L_0x03c6:
            r0 = r33
            r1 = r2
            r26 = r4
            r29 = r13
            r2 = 0
            r23 = 1
            r13 = r5
            r5 = 0
            r27 = r1
            r1 = r0
            r0 = r27
        L_0x03d7:
            r6 = r30
            r2 = r0
            r5 = r13
            r0 = r22
            r22 = r25
            r4 = r26
            r13 = r29
            goto L_0x004c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.graphics.g2d.e.a(com.badlogic.gdx.graphics.g2d.c, java.lang.CharSequence, int, int, e.d.b.t.b, float, int, boolean, java.lang.String):void");
    }

    private void a(c.a aVar, a aVar2, float f2, String str, int i2, f0<a> f0Var) {
        a obtain = f0Var.obtain();
        aVar.a(obtain, str, 0, str.length(), null);
        int i3 = obtain.f4847b.f5528b;
        float f3 = Animation.CurveTimeline.LINEAR;
        if (i3 > 0) {
            a(aVar, obtain);
            int i4 = obtain.f4847b.f5528b;
            float f4 = Animation.CurveTimeline.LINEAR;
            for (int i5 = 1; i5 < i4; i5++) {
                f4 += obtain.f4847b.b(i5);
            }
            f3 = f4;
        }
        float f5 = f2 - f3;
        float f6 = aVar2.f4848c;
        int i6 = 0;
        while (true) {
            m mVar = aVar2.f4847b;
            if (i6 >= mVar.f5528b) {
                break;
            }
            float b2 = mVar.b(i6);
            f6 += b2;
            if (f6 > f5) {
                aVar2.f4850e = (f6 - aVar2.f4848c) - b2;
                break;
            }
            i6++;
        }
        if (i6 > 1) {
            aVar2.f4846a.g(i6 - 1);
            aVar2.f4847b.e(i6);
            a(aVar, aVar2);
            m mVar2 = obtain.f4847b;
            int i7 = mVar2.f5528b;
            if (i7 > 0) {
                aVar2.f4847b.a(mVar2, 1, i7 - 1);
            }
        } else {
            aVar2.f4846a.clear();
            aVar2.f4847b.a();
            aVar2.f4847b.a(obtain.f4847b);
            m mVar3 = obtain.f4847b;
            if (mVar3.f5528b > 0) {
                aVar2.f4850e += mVar3.b(0);
            }
        }
        aVar2.f4846a.a(obtain.f4846a);
        aVar2.f4850e += f3;
        f0Var.free(obtain);
    }

    private a a(c.a aVar, a aVar2, f0<a> f0Var, int i2, int i3) {
        com.badlogic.gdx.utils.a<c.b> aVar3 = aVar2.f4846a;
        int i4 = aVar3.f5374b;
        m mVar = aVar2.f4847b;
        int i5 = i2;
        while (i5 > 0 && aVar.d((char) aVar3.get(i5 - 1).f4820a)) {
            i5--;
        }
        while (i2 < i4 && aVar.d((char) aVar3.get(i2).f4820a)) {
            i2++;
        }
        while (i3 < i5) {
            aVar2.f4850e += mVar.b(i3);
            i3++;
        }
        int i6 = i5 + 1;
        while (i3 > i6) {
            i3--;
            aVar2.f4850e -= mVar.b(i3);
        }
        a aVar4 = null;
        if (i2 < i4) {
            aVar4 = f0Var.obtain();
            aVar4.f4851f.b(aVar2.f4851f);
            com.badlogic.gdx.utils.a<c.b> aVar5 = aVar4.f4846a;
            aVar5.a(aVar3, 0, i5);
            aVar3.a(0, i2 - 1);
            aVar2.f4846a = aVar5;
            aVar4.f4846a = aVar3;
            m mVar2 = aVar4.f4847b;
            mVar2.a(mVar, 0, i6);
            mVar.a(1, i2);
            mVar.a(0, (((float) (-aVar3.a().f4829j)) * aVar.o) - aVar.f4816h);
            aVar2.f4847b = mVar2;
            aVar4.f4847b = mVar;
        } else {
            aVar3.g(i5);
            mVar.e(i6);
        }
        if (i5 == 0) {
            f0Var.free(aVar2);
            this.f4842a.pop();
        } else {
            a(aVar, aVar2);
        }
        return aVar4;
    }

    private void a(c.a aVar, a aVar2) {
        c.b peek = aVar2.f4846a.peek();
        if (!peek.n) {
            float f2 = (((float) (peek.f4823d + peek.f4829j)) * aVar.o) - aVar.f4814f;
            aVar2.f4850e += f2 - aVar2.f4847b.c();
            m mVar = aVar2.f4847b;
            mVar.a(mVar.f5528b - 1, f2);
        }
    }

    private int a(CharSequence charSequence, int i2, int i3, f0<b> f0Var) {
        int i4;
        int i5;
        if (i2 == i3) {
            return -1;
        }
        char charAt = charSequence.charAt(i2);
        if (charAt == '#') {
            int i6 = i2 + 1;
            int i7 = 0;
            while (true) {
                if (i6 >= i3) {
                    break;
                }
                char charAt2 = charSequence.charAt(i6);
                if (charAt2 != ']') {
                    if (charAt2 >= '0' && charAt2 <= '9') {
                        i4 = i7 * 16;
                        i5 = charAt2 - '0';
                    } else if (charAt2 < 'a' || charAt2 > 'f') {
                        if (charAt2 < 'A' || charAt2 > 'F') {
                            break;
                        }
                        i4 = i7 * 16;
                        i5 = charAt2 - '7';
                    } else {
                        i4 = i7 * 16;
                        i5 = charAt2 - 'W';
                    }
                    i7 = i4 + i5;
                    i6++;
                } else if (i6 >= i2 + 2 && i6 <= i2 + 9) {
                    int i8 = i6 - i2;
                    if (i8 <= 7) {
                        for (int i9 = 0; i9 < 9 - i8; i9++) {
                            i7 <<= 4;
                        }
                        i7 |= 255;
                    }
                    b obtain = f0Var.obtain();
                    this.f4845d.add(obtain);
                    b.b(obtain, i7);
                    return i8;
                }
            }
            return -1;
        } else if (charAt == '[') {
            return -2;
        } else {
            if (charAt != ']') {
                int i10 = i2 + 1;
                while (i10 < i3) {
                    if (charSequence.charAt(i10) != ']') {
                        i10++;
                    } else {
                        b a2 = e.d.b.t.c.a(charSequence.subSequence(i2, i10).toString());
                        if (a2 == null) {
                            return -1;
                        }
                        b obtain2 = f0Var.obtain();
                        this.f4845d.add(obtain2);
                        obtain2.b(a2);
                        return i10 - i2;
                    }
                }
                return -1;
            }
            com.badlogic.gdx.utils.a<b> aVar = this.f4845d;
            if (aVar.f5374b > 1) {
                f0Var.free(aVar.pop());
            }
            return 0;
        }
    }
}
