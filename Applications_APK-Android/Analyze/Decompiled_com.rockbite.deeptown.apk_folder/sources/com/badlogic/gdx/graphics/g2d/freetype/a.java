package com.badlogic.gdx.graphics.g2d.freetype;

import com.badlogic.gdx.graphics.g2d.c;
import com.badlogic.gdx.graphics.g2d.e;
import com.badlogic.gdx.graphics.g2d.freetype.FreeType;
import com.badlogic.gdx.graphics.g2d.i;
import com.badlogic.gdx.graphics.g2d.r;
import com.badlogic.gdx.math.h;
import com.badlogic.gdx.utils.l;
import com.badlogic.gdx.utils.o;
import com.esotericsoftware.spine.Animation;
import e.d.b.g;
import e.d.b.t.l;
import e.d.b.t.n;
import java.nio.ByteBuffer;

/* compiled from: FreeTypeFontGenerator */
public class a implements l {

    /* renamed from: e  reason: collision with root package name */
    private static int f4878e = 1024;

    /* renamed from: a  reason: collision with root package name */
    final FreeType.Library f4879a;

    /* renamed from: b  reason: collision with root package name */
    final FreeType.Face f4880b;

    /* renamed from: c  reason: collision with root package name */
    final String f4881c;

    /* renamed from: d  reason: collision with root package name */
    boolean f4882d;

    /* renamed from: com.badlogic.gdx.graphics.g2d.freetype.a$a  reason: collision with other inner class name */
    /* compiled from: FreeTypeFontGenerator */
    static /* synthetic */ class C0116a {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f4883a = new int[d.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(14:0|1|2|3|4|5|6|7|8|9|10|11|12|(3:13|14|16)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(16:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|16) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.badlogic.gdx.graphics.g2d.freetype.a$d[] r0 = com.badlogic.gdx.graphics.g2d.freetype.a.d.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.badlogic.gdx.graphics.g2d.freetype.a.C0116a.f4883a = r0
                int[] r0 = com.badlogic.gdx.graphics.g2d.freetype.a.C0116a.f4883a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.badlogic.gdx.graphics.g2d.freetype.a$d r1 = com.badlogic.gdx.graphics.g2d.freetype.a.d.None     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.badlogic.gdx.graphics.g2d.freetype.a.C0116a.f4883a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.badlogic.gdx.graphics.g2d.freetype.a$d r1 = com.badlogic.gdx.graphics.g2d.freetype.a.d.Slight     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.badlogic.gdx.graphics.g2d.freetype.a.C0116a.f4883a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.badlogic.gdx.graphics.g2d.freetype.a$d r1 = com.badlogic.gdx.graphics.g2d.freetype.a.d.Medium     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.badlogic.gdx.graphics.g2d.freetype.a.C0116a.f4883a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.badlogic.gdx.graphics.g2d.freetype.a$d r1 = com.badlogic.gdx.graphics.g2d.freetype.a.d.Full     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = com.badlogic.gdx.graphics.g2d.freetype.a.C0116a.f4883a     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.badlogic.gdx.graphics.g2d.freetype.a$d r1 = com.badlogic.gdx.graphics.g2d.freetype.a.d.AutoSlight     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r0 = com.badlogic.gdx.graphics.g2d.freetype.a.C0116a.f4883a     // Catch:{ NoSuchFieldError -> 0x004b }
                com.badlogic.gdx.graphics.g2d.freetype.a$d r1 = com.badlogic.gdx.graphics.g2d.freetype.a.d.AutoMedium     // Catch:{ NoSuchFieldError -> 0x004b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                int[] r0 = com.badlogic.gdx.graphics.g2d.freetype.a.C0116a.f4883a     // Catch:{ NoSuchFieldError -> 0x0056 }
                com.badlogic.gdx.graphics.g2d.freetype.a$d r1 = com.badlogic.gdx.graphics.g2d.freetype.a.d.AutoFull     // Catch:{ NoSuchFieldError -> 0x0056 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0056 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0056 }
            L_0x0056:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.graphics.g2d.freetype.a.C0116a.<clinit>():void");
        }
    }

    /* compiled from: FreeTypeFontGenerator */
    public static class c {
        public boolean A;

        /* renamed from: a  reason: collision with root package name */
        public int f4884a = 16;

        /* renamed from: b  reason: collision with root package name */
        public boolean f4885b;

        /* renamed from: c  reason: collision with root package name */
        public d f4886c = d.AutoMedium;

        /* renamed from: d  reason: collision with root package name */
        public e.d.b.t.b f4887d = e.d.b.t.b.f6927e;

        /* renamed from: e  reason: collision with root package name */
        public float f4888e = 1.8f;

        /* renamed from: f  reason: collision with root package name */
        public int f4889f = 2;

        /* renamed from: g  reason: collision with root package name */
        public float f4890g = Animation.CurveTimeline.LINEAR;

        /* renamed from: h  reason: collision with root package name */
        public e.d.b.t.b f4891h = e.d.b.t.b.f6931i;

        /* renamed from: i  reason: collision with root package name */
        public boolean f4892i = false;

        /* renamed from: j  reason: collision with root package name */
        public float f4893j = 1.8f;

        /* renamed from: k  reason: collision with root package name */
        public int f4894k = 0;
        public int l = 0;
        public e.d.b.t.b m = new e.d.b.t.b(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 0.75f);
        public int n;
        public int o;
        public int p;
        public int q;
        public int r;
        public int s;
        public String t = "\u0000ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890\"!`?'.,;:()[]{}<>|/@\\^$€-%+=#_&~* ¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ";
        public boolean u = true;
        public i v = null;
        public boolean w = false;
        public boolean x = false;
        public n.b y;
        public n.b z;

        public c() {
            n.b bVar = n.b.Nearest;
            this.y = bVar;
            this.z = bVar;
        }
    }

    /* compiled from: FreeTypeFontGenerator */
    public enum d {
        None,
        Slight,
        Medium,
        Full,
        AutoSlight,
        AutoMedium,
        AutoFull
    }

    public a(e.d.b.s.a aVar) {
        this(aVar, 0);
    }

    private boolean a(int i2) {
        return b(i2, FreeType.f4867e | FreeType.f4869g);
    }

    private int b(c cVar) {
        int i2;
        int i3;
        int i4;
        int i5 = FreeType.f4867e;
        switch (C0116a.f4883a[cVar.f4886c.ordinal()]) {
            case 1:
                i2 = FreeType.f4868f;
                break;
            case 2:
                i2 = FreeType.f4871i;
                break;
            case 3:
                i2 = FreeType.f4870h;
                break;
            case 4:
                i2 = FreeType.f4872j;
                break;
            case 5:
                i3 = FreeType.f4869g;
                i4 = FreeType.f4871i;
                i2 = i3 | i4;
                break;
            case 6:
                i3 = FreeType.f4869g;
                i4 = FreeType.f4870h;
                i2 = i3 | i4;
                break;
            case 7:
                i3 = FreeType.f4869g;
                i4 = FreeType.f4872j;
                i2 = i3 | i4;
                break;
            default:
                return i5;
        }
        return i5 | i2;
    }

    private boolean j() {
        int j2 = this.f4880b.j();
        int i2 = FreeType.f4865c;
        if ((j2 & i2) == i2) {
            int i3 = FreeType.f4866d;
            if ((j2 & i3) == i3 && a(32) && this.f4880b.k().j() == 1651078259) {
                this.f4882d = true;
            }
        }
        return this.f4882d;
    }

    public void dispose() {
        this.f4880b.dispose();
        this.f4879a.dispose();
    }

    public String toString() {
        return this.f4881c;
    }

    public a(e.d.b.s.a aVar, int i2) {
        this.f4882d = false;
        this.f4881c = aVar.h();
        this.f4879a = FreeType.a();
        this.f4880b = this.f4879a.a(aVar, i2);
        if (!j()) {
            a(0, 15);
        }
    }

    public com.badlogic.gdx.graphics.g2d.c a(c cVar) {
        return b(cVar, new b());
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3) {
        if (!this.f4882d && !this.f4880b.b(i2, i3)) {
            throw new o("Couldn't set size for font");
        }
    }

    public b a(c cVar, b bVar) {
        boolean z;
        i iVar;
        i iVar2;
        int[] iArr;
        FreeType.Stroker stroker;
        int[] iArr2;
        i iVar3;
        int i2;
        int b2;
        i.b eVar;
        c cVar2 = cVar;
        b bVar2 = bVar;
        bVar2.f4809a = this.f4881c + "-" + cVar2.f4884a;
        if (cVar2 == null) {
            cVar2 = new c();
        }
        c cVar3 = cVar2;
        char[] charArray = cVar3.t.toCharArray();
        int length = charArray.length;
        boolean z2 = cVar3.A;
        int b3 = b(cVar3);
        char c2 = 0;
        a(0, cVar3.f4884a);
        FreeType.SizeMetrics j2 = this.f4880b.n().j();
        bVar2.f4812d = cVar3.w;
        bVar2.f4819k = (float) FreeType.a(j2.j());
        bVar2.l = (float) FreeType.a(j2.k());
        bVar2.f4817i = (float) FreeType.a(j2.l());
        float f2 = bVar2.f4819k;
        if (this.f4882d && bVar2.f4817i == Animation.CurveTimeline.LINEAR) {
            for (int i3 = 32; i3 < this.f4880b.m() + 32; i3++) {
                if (b(i3, b3)) {
                    float a2 = (float) FreeType.a(this.f4880b.k().l().j());
                    float f3 = bVar2.f4817i;
                    if (a2 <= f3) {
                        a2 = f3;
                    }
                    bVar2.f4817i = a2;
                }
            }
        }
        bVar2.f4817i += (float) cVar3.o;
        if (b(32, b3) || b(108, b3)) {
            bVar2.u = (float) FreeType.a(this.f4880b.k().l().k());
        } else {
            bVar2.u = (float) this.f4880b.l();
        }
        char[] cArr = bVar2.x;
        int length2 = cArr.length;
        int i4 = 0;
        while (true) {
            if (i4 < length2) {
                if (b(cArr[i4], b3)) {
                    bVar2.v = (float) FreeType.a(this.f4880b.k().l().j());
                    break;
                }
                i4++;
            } else {
                break;
            }
        }
        if (bVar2.v != Animation.CurveTimeline.LINEAR) {
            char[] cArr2 = bVar2.y;
            int length3 = cArr2.length;
            int i5 = 0;
            while (true) {
                if (i5 < length3) {
                    if (b(cArr2[i5], b3)) {
                        bVar2.f4818j = (float) (FreeType.a(this.f4880b.k().l().j()) + Math.abs(cVar3.l));
                        break;
                    }
                    i5++;
                } else {
                    break;
                }
            }
            if (this.f4882d || bVar2.f4818j != 1.0f) {
                bVar2.f4819k -= bVar2.f4818j;
                bVar2.m = -bVar2.f4817i;
                if (cVar3.w) {
                    bVar2.f4819k = -bVar2.f4819k;
                    bVar2.m = -bVar2.m;
                }
                i iVar4 = cVar3.v;
                if (iVar4 == null) {
                    if (z2) {
                        b2 = f4878e;
                        eVar = new i.a();
                    } else {
                        int ceil = (int) Math.ceil((double) bVar2.f4817i);
                        b2 = h.b((int) Math.sqrt((double) (ceil * ceil * length)));
                        int i6 = f4878e;
                        if (i6 > 0) {
                            b2 = Math.min(b2, i6);
                        }
                        eVar = new i.e();
                    }
                    int i7 = b2;
                    i iVar5 = new i(i7, i7, l.c.RGBA8888, 1, false, eVar);
                    iVar5.a(cVar3.f4887d);
                    iVar5.k().f6937d = Animation.CurveTimeline.LINEAR;
                    if (cVar3.f4890g > Animation.CurveTimeline.LINEAR) {
                        iVar5.a(cVar3.f4891h);
                        iVar5.k().f6937d = Animation.CurveTimeline.LINEAR;
                    }
                    iVar = iVar5;
                    z = true;
                } else {
                    iVar = iVar4;
                    z = false;
                }
                if (z2) {
                    bVar2.E = new com.badlogic.gdx.utils.a<>(length + 32);
                }
                FreeType.Stroker stroker2 = null;
                if (cVar3.f4890g > Animation.CurveTimeline.LINEAR) {
                    stroker2 = this.f4879a.j();
                    stroker2.a((int) (cVar3.f4890g * 64.0f), cVar3.f4892i ? FreeType.m : FreeType.n, cVar3.f4892i ? FreeType.q : FreeType.o, 0);
                }
                FreeType.Stroker stroker3 = stroker2;
                int[] iArr3 = new int[length];
                int i8 = 0;
                while (i8 < length) {
                    char c3 = charArray[i8];
                    iArr3[i8] = b(c3, b3) ? FreeType.a(this.f4880b.k().l().j()) : 0;
                    if (c3 == 0) {
                        i2 = i8;
                        iArr2 = iArr3;
                        stroker = stroker3;
                        iVar3 = iVar;
                        c.b a3 = a(0, bVar, cVar3, stroker3, f2, iVar3);
                        if (!(a3 == null || a3.f4823d == 0 || a3.f4824e == 0)) {
                            bVar2.a(0, a3);
                            bVar2.t = a3;
                            if (z2) {
                                bVar2.E.add(a3);
                            }
                        }
                    } else {
                        i2 = i8;
                        iArr2 = iArr3;
                        stroker = stroker3;
                        iVar3 = iVar;
                    }
                    i8 = i2 + 1;
                    stroker3 = stroker;
                    iVar = iVar3;
                    iArr3 = iArr2;
                }
                int[] iArr4 = iArr3;
                FreeType.Stroker stroker4 = stroker3;
                i iVar6 = iVar;
                int length4 = iArr4.length;
                while (length4 > 0) {
                    int i9 = iArr4[c2];
                    int i10 = 0;
                    for (int i11 = 1; i11 < length4; i11++) {
                        int i12 = iArr4[i11];
                        if (i12 > i9) {
                            i10 = i11;
                            i9 = i12;
                        }
                    }
                    char c4 = charArray[i10];
                    if (bVar2.a(c4) == null) {
                        char c5 = c4;
                        iArr = iArr4;
                        c.b a4 = a(c4, bVar, cVar3, stroker4, f2, iVar6);
                        if (a4 != null) {
                            bVar2.a(c5, a4);
                            if (z2) {
                                bVar2.E.add(a4);
                            }
                        }
                    } else {
                        iArr = iArr4;
                    }
                    length4--;
                    iArr[i10] = iArr[length4];
                    char c6 = charArray[i10];
                    charArray[i10] = charArray[length4];
                    charArray[length4] = c6;
                    iArr4 = iArr;
                    c2 = 0;
                }
                FreeType.Stroker stroker5 = stroker4;
                if (stroker5 != null && !z2) {
                    stroker5.dispose();
                }
                if (z2) {
                    bVar2.A = this;
                    bVar2.B = cVar3;
                    bVar2.C = stroker5;
                    iVar2 = iVar6;
                    bVar2.D = iVar2;
                } else {
                    iVar2 = iVar6;
                }
                cVar3.u &= this.f4880b.o();
                if (cVar3.u) {
                    for (int i13 = 0; i13 < length; i13++) {
                        char c7 = charArray[i13];
                        c.b a5 = bVar2.a(c7);
                        if (a5 != null) {
                            int a6 = this.f4880b.a(c7);
                            for (int i14 = i13; i14 < length; i14++) {
                                char c8 = charArray[i14];
                                c.b a7 = bVar2.a(c8);
                                if (a7 != null) {
                                    int a8 = this.f4880b.a(c8);
                                    int a9 = this.f4880b.a(a6, a8, 0);
                                    if (a9 != 0) {
                                        a5.a(c8, FreeType.a(a9));
                                    }
                                    int a10 = this.f4880b.a(a8, a6, 0);
                                    if (a10 != 0) {
                                        a7.a(c7, FreeType.a(a10));
                                    }
                                }
                            }
                        }
                    }
                }
                if (z) {
                    bVar2.z = new com.badlogic.gdx.utils.a<>();
                    iVar2.a(bVar2.z, cVar3.y, cVar3.z, cVar3.x);
                }
                c.b a11 = bVar2.a(' ');
                if (a11 == null) {
                    a11 = new c.b();
                    a11.l = ((int) bVar2.u) + cVar3.n;
                    a11.f4820a = 32;
                    bVar2.a(32, a11);
                }
                if (a11.f4823d == 0) {
                    a11.f4823d = (int) (((float) a11.l) + bVar2.f4814f);
                }
                return bVar2;
            }
            throw new o("No cap character found in font");
        }
        throw new o("No x-height character found in font");
    }

    private boolean b(int i2, int i3) {
        return this.f4880b.a(i2, i3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.c.<init>(com.badlogic.gdx.graphics.g2d.c$a, com.badlogic.gdx.utils.a<com.badlogic.gdx.graphics.g2d.r>, boolean):void
     arg types: [com.badlogic.gdx.graphics.g2d.freetype.a$b, com.badlogic.gdx.utils.a<com.badlogic.gdx.graphics.g2d.r>, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.c.<init>(com.badlogic.gdx.graphics.g2d.c$a, com.badlogic.gdx.graphics.g2d.r, boolean):void
      com.badlogic.gdx.graphics.g2d.c.<init>(e.d.b.s.a, com.badlogic.gdx.graphics.g2d.r, boolean):void
      com.badlogic.gdx.graphics.g2d.c.<init>(e.d.b.s.a, e.d.b.s.a, boolean):void
      com.badlogic.gdx.graphics.g2d.c.<init>(com.badlogic.gdx.graphics.g2d.c$a, com.badlogic.gdx.utils.a<com.badlogic.gdx.graphics.g2d.r>, boolean):void */
    public com.badlogic.gdx.graphics.g2d.c b(c cVar, b bVar) {
        boolean z = false;
        boolean z2 = bVar.z == null && cVar.v != null;
        if (z2) {
            bVar.z = new com.badlogic.gdx.utils.a<>();
        }
        a(cVar, bVar);
        if (z2) {
            cVar.v.a(bVar.z, cVar.y, cVar.z, cVar.x);
        }
        if (!bVar.z.isEmpty()) {
            com.badlogic.gdx.graphics.g2d.c cVar2 = new com.badlogic.gdx.graphics.g2d.c((c.a) bVar, bVar.z, true);
            if (cVar.v == null) {
                z = true;
            }
            cVar2.b(z);
            return cVar2;
        }
        throw new o("Unable to create a font with no texture regions.");
    }

    /* compiled from: FreeTypeFontGenerator */
    public static class b extends c.a implements com.badlogic.gdx.utils.l {
        a A;
        c B;
        FreeType.Stroker C;
        i D;
        com.badlogic.gdx.utils.a<c.b> E;
        private boolean F;
        public com.badlogic.gdx.utils.a<r> z;

        public c.b a(char c2) {
            a aVar;
            c.b a2 = super.a(c2);
            if (a2 == null && (aVar = this.A) != null) {
                aVar.a(0, this.B.f4884a);
                a2 = this.A.a(c2, this, this.B, this.C, ((this.f4812d ? -this.f4819k : this.f4819k) + this.f4818j) / this.p, this.D);
                if (a2 == null) {
                    return this.t;
                }
                a(a2, this.z.get(a2.o));
                a(c2, a2);
                this.E.add(a2);
                this.F = true;
                FreeType.Face face = this.A.f4880b;
                if (this.B.u) {
                    int a3 = face.a(c2);
                    int i2 = this.E.f5374b;
                    for (int i3 = 0; i3 < i2; i3++) {
                        c.b bVar = this.E.get(i3);
                        int a4 = face.a(bVar.f4820a);
                        int a5 = face.a(a3, a4, 0);
                        if (a5 != 0) {
                            a2.a(bVar.f4820a, FreeType.a(a5));
                        }
                        int a6 = face.a(a4, a3, 0);
                        if (a6 != 0) {
                            bVar.a(c2, FreeType.a(a6));
                        }
                    }
                }
            }
            return a2;
        }

        public void dispose() {
            FreeType.Stroker stroker = this.C;
            if (stroker != null) {
                stroker.dispose();
            }
            i iVar = this.D;
            if (iVar != null) {
                iVar.dispose();
            }
        }

        public void a(e.a aVar, CharSequence charSequence, int i2, int i3, c.b bVar) {
            i iVar = this.D;
            if (iVar != null) {
                iVar.b(true);
            }
            super.a(aVar, charSequence, i2, i3, bVar);
            if (this.F) {
                this.F = false;
                i iVar2 = this.D;
                com.badlogic.gdx.utils.a<r> aVar2 = this.z;
                c cVar = this.B;
                iVar2.a(aVar2, cVar.y, cVar.z, cVar.x);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public c.b a(char c2, b bVar, c cVar, FreeType.Stroker stroker, float f2, i iVar) {
        FreeType.Bitmap bitmap;
        com.badlogic.gdx.utils.a<r> aVar;
        FreeType.Glyph glyph;
        ByteBuffer byteBuffer;
        byte b2;
        char c3 = c2;
        c cVar2 = cVar;
        if ((this.f4880b.a(c3) == 0 && c3 != 0) || !b(c3, b(cVar2))) {
            return null;
        }
        FreeType.GlyphSlot k2 = this.f4880b.k();
        FreeType.Glyph k3 = k2.k();
        try {
            k3.a(cVar2.f4885b ? FreeType.l : FreeType.f4873k);
            FreeType.Bitmap j2 = k3.j();
            e.d.b.t.l a2 = j2.a(l.c.RGBA8888, cVar2.f4887d, cVar2.f4888e);
            if (j2.m() == 0 || j2.l() == 0) {
                bitmap = j2;
            } else {
                if (cVar2.f4890g > Animation.CurveTimeline.LINEAR) {
                    int l = k3.l();
                    int k4 = k3.k();
                    FreeType.Glyph k5 = k2.k();
                    k5.a(stroker, false);
                    k5.a(cVar2.f4885b ? FreeType.l : FreeType.f4873k);
                    int k6 = k4 - k5.k();
                    int i2 = -(l - k5.l());
                    e.d.b.t.l a3 = k5.j().a(l.c.RGBA8888, cVar2.f4891h, cVar2.f4893j);
                    int i3 = cVar2.f4889f;
                    for (int i4 = 0; i4 < i3; i4++) {
                        a3.a(a2, k6, i2);
                    }
                    a2.dispose();
                    k3.dispose();
                    a2 = a3;
                    k3 = k5;
                }
                if (cVar2.f4894k == 0 && cVar2.l == 0) {
                    if (cVar2.f4890g == Animation.CurveTimeline.LINEAR) {
                        int i5 = cVar2.f4889f - 1;
                        for (int i6 = 0; i6 < i5; i6++) {
                            a2.a(a2, 0, 0);
                        }
                    }
                    bitmap = j2;
                    glyph = k3;
                } else {
                    int q = a2.q();
                    int o = a2.o();
                    int max = Math.max(cVar2.f4894k, 0);
                    int max2 = Math.max(cVar2.l, 0);
                    int abs = Math.abs(cVar2.f4894k) + q;
                    glyph = k3;
                    e.d.b.t.l lVar = new e.d.b.t.l(abs, Math.abs(cVar2.l) + o, a2.k());
                    e.d.b.t.b bVar2 = cVar2.m;
                    float f3 = bVar2.f6937d;
                    if (f3 != Animation.CurveTimeline.LINEAR) {
                        byte b3 = (byte) ((int) (bVar2.f6934a * 255.0f));
                        bitmap = j2;
                        byte b4 = (byte) ((int) (bVar2.f6935b * 255.0f));
                        byte b5 = (byte) ((int) (bVar2.f6936c * 255.0f));
                        ByteBuffer p = a2.p();
                        ByteBuffer p2 = lVar.p();
                        int i7 = 0;
                        while (i7 < o) {
                            int i8 = ((i7 + max2) * abs) + max;
                            int i9 = o;
                            int i10 = 0;
                            while (i10 < q) {
                                int i11 = q;
                                byte b6 = p.get((((q * i7) + i10) * 4) + 3);
                                if (b6 == 0) {
                                    byteBuffer = p;
                                    b2 = b3;
                                } else {
                                    byteBuffer = p;
                                    int i12 = (i8 + i10) * 4;
                                    p2.put(i12, b3);
                                    b2 = b3;
                                    p2.put(i12 + 1, b4);
                                    p2.put(i12 + 2, b5);
                                    p2.put(i12 + 3, (byte) ((int) (((float) (b6 & 255)) * f3)));
                                }
                                i10++;
                                b3 = b2;
                                q = i11;
                                p = byteBuffer;
                            }
                            i7++;
                            o = i9;
                        }
                    } else {
                        bitmap = j2;
                    }
                    int i13 = cVar2.f4889f;
                    for (int i14 = 0; i14 < i13; i14++) {
                        lVar.a(a2, Math.max(-cVar2.f4894k, 0), Math.max(-cVar2.l, 0));
                    }
                    a2.dispose();
                    a2 = lVar;
                }
                if (cVar2.p > 0 || cVar2.q > 0 || cVar2.r > 0 || cVar2.s > 0) {
                    e.d.b.t.l lVar2 = new e.d.b.t.l(a2.q() + cVar2.q + cVar2.s, a2.o() + cVar2.p + cVar2.r, a2.k());
                    lVar2.a(l.a.None);
                    lVar2.a(a2, cVar2.q, cVar2.p);
                    a2.dispose();
                    k3 = glyph;
                    a2 = lVar2;
                } else {
                    k3 = glyph;
                }
            }
            FreeType.GlyphMetrics l2 = k2.l();
            c.b bVar3 = new c.b();
            bVar3.f4820a = c2;
            bVar3.f4823d = a2.q();
            bVar3.f4824e = a2.o();
            bVar3.f4829j = k3.k();
            if (cVar2.w) {
                bVar3.f4830k = (-k3.l()) + ((int) f2);
            } else {
                bVar3.f4830k = (-(bVar3.f4824e - k3.l())) - ((int) f2);
            }
            bVar3.l = FreeType.a(l2.k()) + ((int) cVar2.f4890g) + cVar2.n;
            if (this.f4882d) {
                a2.setColor(e.d.b.t.b.f6933k);
                a2.j();
                ByteBuffer a4 = bitmap.a();
                int c4 = e.d.b.t.b.f6927e.c();
                int c5 = e.d.b.t.b.f6933k.c();
                for (int i15 = 0; i15 < bVar3.f4824e; i15++) {
                    int j3 = bitmap.j() * i15;
                    for (int i16 = 0; i16 < bVar3.f4823d + bVar3.f4829j; i16++) {
                        a2.a(i16, i15, ((a4.get((i16 / 8) + j3) >>> (7 - (i16 % 8))) & 1) == 1 ? c4 : c5);
                    }
                }
            }
            i iVar2 = iVar;
            com.badlogic.gdx.math.n a5 = iVar2.a(a2);
            bVar3.o = iVar.j().f5374b - 1;
            bVar3.f4821b = (int) a5.f5290a;
            bVar3.f4822c = (int) a5.f5291b;
            if (cVar2.A && (aVar = bVar.z) != null && aVar.f5374b <= bVar3.o) {
                iVar2.a(aVar, cVar2.y, cVar2.z, cVar2.x);
            }
            a2.dispose();
            k3.dispose();
            return bVar3;
        } catch (o unused) {
            char c6 = c3;
            k3.dispose();
            e.d.b.a aVar2 = g.f6800a;
            aVar2.b("FreeTypeFontGenerator", "Couldn't render char: " + c6);
            return null;
        }
    }
}
