package com.badlogic.gdx.graphics.g2d.freetype;

import com.badlogic.gdx.graphics.g2d.freetype.a;
import e.d.b.q.g.b;
import e.d.b.q.g.e;

/* compiled from: FreetypeFontLoader */
public class c extends b<com.badlogic.gdx.graphics.g2d.c, a> {

    /* compiled from: FreetypeFontLoader */
    public static class a extends e.d.b.q.c<com.badlogic.gdx.graphics.g2d.c> {

        /* renamed from: a  reason: collision with root package name */
        public String f4903a;

        /* renamed from: b  reason: collision with root package name */
        public a.c f4904b = new a.c();
    }

    public c(e eVar) {
        super(eVar);
    }

    /* renamed from: a */
    public void loadAsync(e.d.b.q.e eVar, String str, e.d.b.s.a aVar, a aVar2) {
        if (aVar2 == null) {
            throw new RuntimeException("FreetypeFontParameter must be set in AssetManager#load to point at a TTF file!");
        }
    }

    /* renamed from: b */
    public com.badlogic.gdx.graphics.g2d.c loadSync(e.d.b.q.e eVar, String str, e.d.b.s.a aVar, a aVar2) {
        if (aVar2 != null) {
            return ((a) eVar.a(aVar2.f4903a + ".gen", a.class)).a(aVar2.f4904b);
        }
        throw new RuntimeException("FreetypeFontParameter must be set in AssetManager#load to point at a TTF file!");
    }

    /* renamed from: a */
    public com.badlogic.gdx.utils.a<e.d.b.q.a> getDependencies(String str, e.d.b.s.a aVar, a aVar2) {
        com.badlogic.gdx.utils.a<e.d.b.q.a> aVar3 = new com.badlogic.gdx.utils.a<>();
        aVar3.add(new e.d.b.q.a(aVar2.f4903a + ".gen", a.class));
        return aVar3;
    }
}
