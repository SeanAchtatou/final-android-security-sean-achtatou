package com.badlogic.gdx.graphics.glutils;

import e.d.b.a;
import e.d.b.g;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: GLVersion */
public class f {

    /* renamed from: a  reason: collision with root package name */
    private int f5120a;

    /* renamed from: b  reason: collision with root package name */
    private final a f5121b;

    /* compiled from: GLVersion */
    public enum a {
        OpenGL,
        GLES,
        WebGL,
        NONE
    }

    public f(a.C0167a aVar, String str, String str2, String str3) {
        if (aVar == a.C0167a.Android) {
            this.f5121b = a.GLES;
        } else if (aVar == a.C0167a.iOS) {
            this.f5121b = a.GLES;
        } else if (aVar == a.C0167a.Desktop) {
            this.f5121b = a.OpenGL;
        } else if (aVar == a.C0167a.Applet) {
            this.f5121b = a.OpenGL;
        } else if (aVar == a.C0167a.WebGL) {
            this.f5121b = a.WebGL;
        } else {
            this.f5121b = a.NONE;
        }
        a aVar2 = this.f5121b;
        if (aVar2 == a.GLES) {
            a("OpenGL ES (\\d(\\.\\d){0,2})", str);
        } else if (aVar2 == a.WebGL) {
            a("WebGL (\\d(\\.\\d){0,2})", str);
        } else if (aVar2 == a.OpenGL) {
            a("(\\d(\\.\\d){0,2})", str);
        } else {
            this.f5120a = -1;
        }
    }

    private void a(String str, String str2) {
        Matcher matcher = Pattern.compile(str).matcher(str2);
        if (matcher.find()) {
            String[] split = matcher.group(1).split("\\.");
            this.f5120a = a(split[0], 2);
            if (split.length >= 2) {
                a(split[1], 0);
            }
            if (split.length >= 3) {
                a(split[2], 0);
                return;
            }
            return;
        }
        e.d.b.a aVar = g.f6800a;
        aVar.b("GLVersion", "Invalid version string: " + str2);
        this.f5120a = 2;
    }

    private int a(String str, int i2) {
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException unused) {
            e.d.b.a aVar = g.f6800a;
            aVar.c("LibGDX GL", "Error parsing number: " + str + ", assuming: " + i2);
            return i2;
        }
    }

    public int a() {
        return this.f5120a;
    }
}
