package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.math.h;
import com.badlogic.gdx.math.n;
import e.d.b.t.b;

/* compiled from: PolygonSprite */
public class m {

    /* renamed from: a  reason: collision with root package name */
    k f4994a;

    /* renamed from: b  reason: collision with root package name */
    private float f4995b;

    /* renamed from: c  reason: collision with root package name */
    private float f4996c;

    /* renamed from: d  reason: collision with root package name */
    private float f4997d;

    /* renamed from: e  reason: collision with root package name */
    private float f4998e;

    /* renamed from: f  reason: collision with root package name */
    private float f4999f = 1.0f;

    /* renamed from: g  reason: collision with root package name */
    private float f5000g = 1.0f;

    /* renamed from: h  reason: collision with root package name */
    private float f5001h;

    /* renamed from: i  reason: collision with root package name */
    private float f5002i;

    /* renamed from: j  reason: collision with root package name */
    private float f5003j;

    /* renamed from: k  reason: collision with root package name */
    private float[] f5004k;
    private boolean l;
    private final b m;

    public m(k kVar) {
        new n();
        this.m = new b(1.0f, 1.0f, 1.0f, 1.0f);
        a(kVar);
        r rVar = kVar.f4988d;
        c((float) rVar.f5065f, (float) rVar.f5066g);
        a(this.f4997d / 2.0f, this.f4998e / 2.0f);
    }

    public void a(b bVar) {
        this.m.b(bVar);
        float b2 = bVar.b();
        float[] fArr = this.f5004k;
        for (int i2 = 2; i2 < fArr.length; i2 += 5) {
            fArr[i2] = b2;
        }
    }

    public void b(float f2, float f3) {
        d(f2 - this.f4995b, f3 - this.f4996c);
    }

    public void c(float f2, float f3) {
        this.f4997d = f2;
        this.f4998e = f3;
        this.l = true;
    }

    public void d(float f2, float f3) {
        this.f4995b += f2;
        this.f4996c += f3;
        if (!this.l) {
            float[] fArr = this.f5004k;
            for (int i2 = 0; i2 < fArr.length; i2 += 5) {
                fArr[i2] = fArr[i2] + f2;
                int i3 = i2 + 1;
                fArr[i3] = fArr[i3] + f3;
            }
        }
    }

    public void b(float f2) {
        this.f4999f = f2;
        this.f5000g = f2;
        this.l = true;
    }

    public void a(float f2, float f3) {
        this.f5002i = f2;
        this.f5003j = f3;
        this.l = true;
    }

    public void a(float f2) {
        this.f5001h = f2;
        this.l = true;
    }

    public float[] a() {
        if (!this.l) {
            return this.f5004k;
        }
        int i2 = 0;
        this.l = false;
        float f2 = this.f5002i;
        float f3 = this.f5003j;
        float f4 = this.f4999f;
        float f5 = this.f5000g;
        k kVar = this.f4994a;
        float[] fArr = this.f5004k;
        float[] fArr2 = kVar.f4986b;
        float f6 = this.f4995b + f2;
        float f7 = this.f4996c + f3;
        float b2 = this.f4997d / ((float) kVar.f4988d.b());
        float a2 = this.f4998e / ((float) kVar.f4988d.a());
        float b3 = h.b(this.f5001h);
        float h2 = h.h(this.f5001h);
        int length = fArr2.length;
        int i3 = 0;
        while (i2 < length) {
            float f8 = ((fArr2[i2] * b2) - f2) * f4;
            float f9 = ((fArr2[i2 + 1] * a2) - f3) * f5;
            fArr[i3] = ((b3 * f8) - (h2 * f9)) + f6;
            fArr[i3 + 1] = (f8 * h2) + (f9 * b3) + f7;
            i2 += 2;
            i3 += 5;
        }
        return fArr;
    }

    public void a(n nVar) {
        k kVar = this.f4994a;
        e.d.b.t.n nVar2 = kVar.f4988d.f5060a;
        float[] a2 = a();
        int length = this.f5004k.length;
        short[] sArr = kVar.f4987c;
        nVar.a(nVar2, a2, 0, length, sArr, 0, sArr.length);
    }

    public void a(k kVar) {
        this.f4994a = kVar;
        float[] fArr = kVar.f4986b;
        float[] fArr2 = kVar.f4985a;
        int length = (fArr.length / 2) * 5;
        float[] fArr3 = this.f5004k;
        if (fArr3 == null || fArr3.length != length) {
            this.f5004k = new float[length];
        }
        float b2 = this.m.b();
        float[] fArr4 = this.f5004k;
        int i2 = 0;
        for (int i3 = 2; i3 < length; i3 += 5) {
            fArr4[i3] = b2;
            fArr4[i3 + 1] = fArr2[i2];
            fArr4[i3 + 2] = fArr2[i2 + 1];
            i2 += 2;
        }
        this.l = true;
    }
}
