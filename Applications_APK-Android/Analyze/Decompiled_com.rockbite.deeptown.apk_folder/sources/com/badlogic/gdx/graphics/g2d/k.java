package com.badlogic.gdx.graphics.g2d;

/* compiled from: PolygonRegion */
public class k {

    /* renamed from: a  reason: collision with root package name */
    final float[] f4985a;

    /* renamed from: b  reason: collision with root package name */
    final float[] f4986b;

    /* renamed from: c  reason: collision with root package name */
    final short[] f4987c;

    /* renamed from: d  reason: collision with root package name */
    final r f4988d;

    public k(r rVar, float[] fArr, short[] sArr) {
        this.f4988d = rVar;
        this.f4986b = fArr;
        this.f4987c = sArr;
        float[] fArr2 = new float[fArr.length];
        this.f4985a = fArr2;
        float f2 = rVar.f5061b;
        float f3 = rVar.f5062c;
        float f4 = rVar.f5063d - f2;
        float f5 = rVar.f5064e - f3;
        int i2 = rVar.f5065f;
        int i3 = rVar.f5066g;
        int length = fArr.length;
        for (int i4 = 0; i4 < length; i4 += 2) {
            fArr2[i4] = ((fArr[i4] / ((float) i2)) * f4) + f2;
            int i5 = i4 + 1;
            fArr2[i5] = ((1.0f - (fArr[i5] / ((float) i3))) * f5) + f3;
        }
    }

    public r a() {
        return this.f4988d;
    }

    public float[] b() {
        return this.f4985a;
    }

    public short[] c() {
        return this.f4987c;
    }

    public float[] d() {
        return this.f4986b;
    }
}
