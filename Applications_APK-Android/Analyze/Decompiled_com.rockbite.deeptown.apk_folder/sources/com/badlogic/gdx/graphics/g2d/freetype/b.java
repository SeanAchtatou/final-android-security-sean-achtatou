package com.badlogic.gdx.graphics.g2d.freetype;

import e.d.b.q.c;
import e.d.b.q.g.e;
import e.d.b.q.g.n;

/* compiled from: FreeTypeFontGeneratorLoader */
public class b extends n<a, a> {

    /* compiled from: FreeTypeFontGeneratorLoader */
    public static class a extends c<a> {
    }

    public b(e eVar) {
        super(eVar);
    }

    /* renamed from: a */
    public com.badlogic.gdx.utils.a<e.d.b.q.a> getDependencies(String str, e.d.b.s.a aVar, a aVar2) {
        return null;
    }

    public a a(e.d.b.q.e eVar, String str, e.d.b.s.a aVar, a aVar2) {
        if (aVar.b().equals("gen")) {
            return new a(aVar.d(aVar.h()));
        }
        return new a(aVar);
    }
}
