package com.badlogic.gdx.graphics.g2d.freetype;

import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.l;
import com.badlogic.gdx.utils.m0;
import com.badlogic.gdx.utils.o;
import com.badlogic.gdx.utils.q0;
import com.badlogic.gdx.utils.y;
import com.google.android.gms.drive.MetadataChangeSet;
import e.d.b.t.b;
import e.d.b.t.l;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

public class FreeType {

    /* renamed from: a  reason: collision with root package name */
    public static int f4863a = 1;

    /* renamed from: b  reason: collision with root package name */
    public static int f4864b = 2;

    /* renamed from: c  reason: collision with root package name */
    public static int f4865c = 2;

    /* renamed from: d  reason: collision with root package name */
    public static int f4866d = 16;

    /* renamed from: e  reason: collision with root package name */
    public static int f4867e = 0;

    /* renamed from: f  reason: collision with root package name */
    public static int f4868f = 2;

    /* renamed from: g  reason: collision with root package name */
    public static int f4869g = 32;

    /* renamed from: h  reason: collision with root package name */
    public static int f4870h = 0;

    /* renamed from: i  reason: collision with root package name */
    public static int f4871i = 65536;

    /* renamed from: j  reason: collision with root package name */
    public static int f4872j = MetadataChangeSet.INDEXABLE_TEXT_SIZE_LIMIT_BYTES;

    /* renamed from: k  reason: collision with root package name */
    public static int f4873k = 0;
    public static int l = 2;
    public static int m = 0;
    public static int n = 1;
    public static int o = 0;
    public static int p = 2;
    public static int q = 3;

    public static class Face extends a implements l {

        /* renamed from: b  reason: collision with root package name */
        Library f4874b;

        public Face(long j2, Library library) {
            super(j2);
            this.f4874b = library;
        }

        private static native void doneFace(long j2);

        private static native int getCharIndex(long j2, int i2);

        private static native int getFaceFlags(long j2);

        private static native long getGlyph(long j2);

        private static native int getKerning(long j2, int i2, int i3, int i4);

        private static native int getMaxAdvanceWidth(long j2);

        private static native int getNumGlyphs(long j2);

        private static native long getSize(long j2);

        private static native boolean hasKerning(long j2);

        private static native boolean loadChar(long j2, int i2, int i3);

        private static native boolean setPixelSizes(long j2, int i2, int i3);

        public boolean a(int i2, int i3) {
            return loadChar(this.f4877a, i2, i3);
        }

        public boolean b(int i2, int i3) {
            return setPixelSizes(this.f4877a, i2, i3);
        }

        public void dispose() {
            doneFace(this.f4877a);
            ByteBuffer a2 = this.f4874b.f4876b.a(this.f4877a);
            if (a2 != null) {
                this.f4874b.f4876b.b(this.f4877a);
                if (BufferUtils.b(a2)) {
                    BufferUtils.a(a2);
                }
            }
        }

        public int j() {
            return getFaceFlags(this.f4877a);
        }

        public GlyphSlot k() {
            return new GlyphSlot(getGlyph(this.f4877a));
        }

        public int l() {
            return getMaxAdvanceWidth(this.f4877a);
        }

        public int m() {
            return getNumGlyphs(this.f4877a);
        }

        public Size n() {
            return new Size(getSize(this.f4877a));
        }

        public boolean o() {
            return hasKerning(this.f4877a);
        }

        public int a(int i2, int i3, int i4) {
            return getKerning(this.f4877a, i2, i3, i4);
        }

        public int a(int i2) {
            return getCharIndex(this.f4877a, i2);
        }
    }

    public static class Glyph extends a implements l {

        /* renamed from: b  reason: collision with root package name */
        private boolean f4875b;

        Glyph(long j2) {
            super(j2);
        }

        private static native void done(long j2);

        private static native long getBitmap(long j2);

        private static native int getLeft(long j2);

        private static native int getTop(long j2);

        private static native long strokeBorder(long j2, long j3, boolean z);

        private static native long toBitmap(long j2, int i2);

        public void a(Stroker stroker, boolean z) {
            this.f4877a = strokeBorder(this.f4877a, stroker.f4877a, z);
        }

        public void dispose() {
            done(this.f4877a);
        }

        public Bitmap j() {
            if (this.f4875b) {
                return new Bitmap(getBitmap(this.f4877a));
            }
            throw new o("Glyph is not yet rendered");
        }

        public int k() {
            if (this.f4875b) {
                return getLeft(this.f4877a);
            }
            throw new o("Glyph is not yet rendered");
        }

        public int l() {
            if (this.f4875b) {
                return getTop(this.f4877a);
            }
            throw new o("Glyph is not yet rendered");
        }

        public void a(int i2) {
            long bitmap = toBitmap(this.f4877a, i2);
            if (bitmap != 0) {
                this.f4877a = bitmap;
                this.f4875b = true;
                return;
            }
            throw new o("Couldn't render glyph, FreeType error code: " + FreeType.getLastErrorCode());
        }
    }

    public static class GlyphMetrics extends a {
        GlyphMetrics(long j2) {
            super(j2);
        }

        private static native int getHeight(long j2);

        private static native int getHoriAdvance(long j2);

        public int j() {
            return getHeight(this.f4877a);
        }

        public int k() {
            return getHoriAdvance(this.f4877a);
        }
    }

    public static class GlyphSlot extends a {
        GlyphSlot(long j2) {
            super(j2);
        }

        private static native int getFormat(long j2);

        private static native long getGlyph(long j2);

        private static native long getMetrics(long j2);

        public int j() {
            return getFormat(this.f4877a);
        }

        public Glyph k() {
            long glyph = getGlyph(this.f4877a);
            if (glyph != 0) {
                return new Glyph(glyph);
            }
            throw new o("Couldn't get glyph, FreeType error code: " + FreeType.getLastErrorCode());
        }

        public GlyphMetrics l() {
            return new GlyphMetrics(getMetrics(this.f4877a));
        }
    }

    public static class Size extends a {
        Size(long j2) {
            super(j2);
        }

        private static native long getMetrics(long j2);

        public SizeMetrics j() {
            return new SizeMetrics(getMetrics(this.f4877a));
        }
    }

    public static class SizeMetrics extends a {
        SizeMetrics(long j2) {
            super(j2);
        }

        private static native int getAscender(long j2);

        private static native int getDescender(long j2);

        private static native int getHeight(long j2);

        public int j() {
            return getAscender(this.f4877a);
        }

        public int k() {
            return getDescender(this.f4877a);
        }

        public int l() {
            return getHeight(this.f4877a);
        }
    }

    public static class Stroker extends a implements l {
        Stroker(long j2) {
            super(j2);
        }

        private static native void done(long j2);

        private static native void set(long j2, int i2, int i3, int i4, int i5);

        public void a(int i2, int i3, int i4, int i5) {
            set(this.f4877a, i2, i3, i4, i5);
        }

        public void dispose() {
            done(this.f4877a);
        }
    }

    private static class a {

        /* renamed from: a  reason: collision with root package name */
        long f4877a;

        a(long j2) {
            this.f4877a = j2;
        }
    }

    static {
        a('s', 'y', 'm', 'b');
        a('u', 'n', 'i', 'c');
        a('s', 'j', 'i', 's');
        a('g', 'b', ' ', ' ');
        a('b', 'i', 'g', '5');
        a('w', 'a', 'n', 's');
        a('j', 'o', 'h', 'a');
        a('A', 'D', 'O', 'B');
        a('A', 'D', 'B', 'E');
        a('A', 'D', 'B', 'C');
        a('l', 'a', 't', '1');
        a('l', 'a', 't', '2');
        a('a', 'r', 'm', 'n');
    }

    private static int a(char c2, char c3, char c4, char c5) {
        return (c2 << 24) | (c3 << 16) | (c4 << 8) | c5;
    }

    public static int a(int i2) {
        return ((i2 + 63) & -64) >> 6;
    }

    public static Library a() {
        new m0().a("gdx-freetype");
        long initFreeTypeJni = initFreeTypeJni();
        if (initFreeTypeJni != 0) {
            return new Library(initFreeTypeJni);
        }
        throw new o("Couldn't initialize FreeType library, FreeType error code: " + getLastErrorCode());
    }

    static native int getLastErrorCode();

    private static native long initFreeTypeJni();

    public static class Bitmap extends a {
        Bitmap(long j2) {
            super(j2);
        }

        private static native ByteBuffer getBuffer(long j2);

        private static native int getPitch(long j2);

        private static native int getPixelMode(long j2);

        private static native int getRows(long j2);

        private static native int getWidth(long j2);

        public ByteBuffer a() {
            if (l() == 0) {
                return BufferUtils.a(1);
            }
            return getBuffer(this.f4877a);
        }

        public int j() {
            return getPitch(this.f4877a);
        }

        public int k() {
            return getPixelMode(this.f4877a);
        }

        public int l() {
            return getRows(this.f4877a);
        }

        public int m() {
            return getWidth(this.f4877a);
        }

        public e.d.b.t.l a(l.c cVar, b bVar, float f2) {
            e.d.b.t.l lVar;
            int i2;
            int i3;
            int i4;
            l.c cVar2 = cVar;
            float f3 = f2;
            int m = m();
            int l = l();
            ByteBuffer a2 = a();
            int k2 = k();
            int abs = Math.abs(j());
            if (bVar == b.f6927e && k2 == FreeType.f4864b && abs == m && f3 == 1.0f) {
                lVar = new e.d.b.t.l(m, l, l.c.Alpha);
                BufferUtils.a(a2, lVar.p(), lVar.p().capacity());
            } else {
                e.d.b.t.l lVar2 = new e.d.b.t.l(m, l, l.c.RGBA8888);
                int c2 = b.c(bVar);
                byte[] bArr = new byte[abs];
                int[] iArr = new int[m];
                IntBuffer asIntBuffer = lVar2.p().asIntBuffer();
                if (k2 == FreeType.f4863a) {
                    for (int i5 = 0; i5 < l; i5++) {
                        a2.get(bArr);
                        int i6 = 0;
                        for (int i7 = 0; i7 < m; i7 += 8) {
                            byte b2 = bArr[i6];
                            int min = Math.min(8, m - i7);
                            for (int i8 = 0; i8 < min; i8++) {
                                if ((b2 & (1 << (7 - i8))) != 0) {
                                    iArr[i7 + i8] = c2;
                                } else {
                                    iArr[i7 + i8] = 0;
                                }
                            }
                            i6++;
                        }
                        asIntBuffer.put(iArr);
                    }
                } else {
                    int i9 = c2 & -256;
                    byte b3 = 255;
                    int i10 = c2 & 255;
                    int i11 = 0;
                    while (i11 < l) {
                        a2.get(bArr);
                        int i12 = 0;
                        while (i12 < m) {
                            byte b4 = bArr[i12] & b3;
                            if (b4 == 0) {
                                iArr[i12] = i9;
                            } else if (b4 == b3) {
                                iArr[i12] = i9 | i10;
                            } else {
                                i3 = i10;
                                double d2 = (double) (((float) b4) / 255.0f);
                                i4 = m;
                                i2 = l;
                                iArr[i12] = ((int) (((float) i10) * ((float) Math.pow(d2, (double) f3)))) | i9;
                                i12++;
                                m = i4;
                                i10 = i3;
                                l = i2;
                                b3 = 255;
                            }
                            i4 = m;
                            i2 = l;
                            i3 = i10;
                            i12++;
                            m = i4;
                            i10 = i3;
                            l = i2;
                            b3 = 255;
                        }
                        asIntBuffer.put(iArr);
                        i11++;
                        b3 = 255;
                    }
                }
                lVar = lVar2;
            }
            if (cVar2 == lVar.k()) {
                return lVar;
            }
            e.d.b.t.l lVar3 = new e.d.b.t.l(lVar.q(), lVar.o(), cVar2);
            lVar3.a(l.a.None);
            lVar3.a(lVar, 0, 0);
            lVar3.a(l.a.SourceOver);
            lVar.dispose();
            return lVar3;
        }
    }

    public static class Library extends a implements com.badlogic.gdx.utils.l {

        /* renamed from: b  reason: collision with root package name */
        y<ByteBuffer> f4876b = new y<>();

        Library(long j2) {
            super(j2);
        }

        private static native void doneFreeType(long j2);

        private static native long newMemoryFace(long j2, ByteBuffer byteBuffer, int i2, int i3);

        private static native long strokerNew(long j2);

        public Face a(e.d.b.s.a aVar, int i2) {
            ByteBuffer byteBuffer;
            ByteBuffer byteBuffer2;
            try {
                byteBuffer = aVar.e();
            } catch (o unused) {
                byteBuffer = null;
            }
            if (byteBuffer == null) {
                InputStream l = aVar.l();
                try {
                    int d2 = (int) aVar.d();
                    if (d2 == 0) {
                        byte[] a2 = q0.a(l, 16384);
                        ByteBuffer d3 = BufferUtils.d(a2.length);
                        BufferUtils.a(a2, 0, d3, a2.length);
                        byteBuffer2 = d3;
                    } else {
                        byteBuffer2 = BufferUtils.d(d2);
                        q0.a(l, byteBuffer2);
                    }
                    q0.a(l);
                } catch (IOException e2) {
                    throw new o(e2);
                } catch (Throwable th) {
                    q0.a(l);
                    throw th;
                }
            } else {
                byteBuffer2 = byteBuffer;
            }
            return a(byteBuffer2, i2);
        }

        public void dispose() {
            doneFreeType(this.f4877a);
            y.d<ByteBuffer> b2 = this.f4876b.b();
            b2.iterator();
            while (b2.hasNext()) {
                ByteBuffer byteBuffer = (ByteBuffer) b2.next();
                if (BufferUtils.b(byteBuffer)) {
                    BufferUtils.a(byteBuffer);
                }
            }
        }

        public Stroker j() {
            long strokerNew = strokerNew(this.f4877a);
            if (strokerNew != 0) {
                return new Stroker(strokerNew);
            }
            throw new o("Couldn't create FreeType stroker, FreeType error code: " + FreeType.getLastErrorCode());
        }

        public Face a(ByteBuffer byteBuffer, int i2) {
            long newMemoryFace = newMemoryFace(this.f4877a, byteBuffer, byteBuffer.remaining(), i2);
            if (newMemoryFace == 0) {
                if (BufferUtils.b(byteBuffer)) {
                    BufferUtils.a(byteBuffer);
                }
                throw new o("Couldn't load font, FreeType error code: " + FreeType.getLastErrorCode());
            }
            this.f4876b.b(newMemoryFace, byteBuffer);
            return new Face(newMemoryFace, this);
        }
    }
}
