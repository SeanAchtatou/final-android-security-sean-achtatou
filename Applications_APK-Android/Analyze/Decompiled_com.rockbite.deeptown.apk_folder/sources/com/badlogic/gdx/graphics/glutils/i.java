package com.badlogic.gdx.graphics.glutils;

import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.utils.a;
import e.d.b.t.b;
import e.d.b.t.j;
import e.d.b.t.r;

/* compiled from: ImmediateModeRenderer20 */
public class i implements j {

    /* renamed from: a  reason: collision with root package name */
    private int f5131a;

    /* renamed from: b  reason: collision with root package name */
    private int f5132b;

    /* renamed from: c  reason: collision with root package name */
    private final int f5133c;

    /* renamed from: d  reason: collision with root package name */
    private int f5134d;

    /* renamed from: e  reason: collision with root package name */
    private final j f5135e;

    /* renamed from: f  reason: collision with root package name */
    private s f5136f;

    /* renamed from: g  reason: collision with root package name */
    private boolean f5137g;

    /* renamed from: h  reason: collision with root package name */
    private final int f5138h;

    /* renamed from: i  reason: collision with root package name */
    private final int f5139i;

    /* renamed from: j  reason: collision with root package name */
    private final int f5140j;

    /* renamed from: k  reason: collision with root package name */
    private final Matrix4 f5141k;
    private final float[] l;
    private final String[] m;

    public i(int i2, boolean z, boolean z2, int i3) {
        this(i2, z, z2, i3, b(z, z2, i3));
        this.f5137g = true;
    }

    private r[] a(boolean z, boolean z2, int i2) {
        a aVar = new a();
        aVar.add(new r(1, 3, "a_position"));
        if (z) {
            aVar.add(new r(8, 3, "a_normal"));
        }
        if (z2) {
            aVar.add(new r(4, 4, "a_color"));
        }
        for (int i3 = 0; i3 < i2; i3++) {
            aVar.add(new r(16, 2, "a_texCoord" + i3));
        }
        r[] rVarArr = new r[aVar.f5374b];
        for (int i4 = 0; i4 < aVar.f5374b; i4++) {
            rVarArr[i4] = (r) aVar.get(i4);
        }
        return rVarArr;
    }

    public static s b(boolean z, boolean z2, int i2) {
        return new s(d(z, z2, i2), c(z, z2, i2));
    }

    public int c() {
        return this.f5134d;
    }

    public int d() {
        return this.f5133c;
    }

    public void dispose() {
        s sVar;
        if (this.f5137g && (sVar = this.f5136f) != null) {
            sVar.dispose();
        }
        this.f5135e.dispose();
    }

    public void end() {
        a();
    }

    private static String c(boolean z, boolean z2, int i2) {
        String str;
        String str2 = "#ifdef GL_ES\nprecision mediump float;\n#endif\n";
        if (z2) {
            str2 = str2 + "varying vec4 v_col;\n";
        }
        String str3 = str2;
        for (int i3 = 0; i3 < i2; i3++) {
            str3 = (str3 + "varying vec2 v_tex" + i3 + ";\n") + "uniform sampler2D u_sampler" + i3 + ";\n";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str3);
        sb.append("void main() {\n   gl_FragColor = ");
        sb.append(z2 ? "v_col" : "vec4(1, 1, 1, 1)");
        String sb2 = sb.toString();
        if (i2 > 0) {
            sb2 = sb2 + " * ";
        }
        for (int i4 = 0; i4 < i2; i4++) {
            if (i4 == i2 - 1) {
                str = str + " texture2D(u_sampler" + i4 + ",  v_tex" + i4 + ")";
            } else {
                str = str + " texture2D(u_sampler" + i4 + ",  v_tex" + i4 + ") *";
            }
        }
        return str + ";\n}";
    }

    private static String d(boolean z, boolean z2, int i2) {
        StringBuilder sb = new StringBuilder();
        sb.append("attribute vec4 a_position;\n");
        String str = "";
        sb.append(z ? "attribute vec3 a_normal;\n" : str);
        sb.append(z2 ? "attribute vec4 a_color;\n" : str);
        String sb2 = sb.toString();
        String str2 = sb2;
        for (int i3 = 0; i3 < i2; i3++) {
            str2 = str2 + "attribute vec2 a_texCoord" + i3 + ";\n";
        }
        String str3 = str2 + "uniform mat4 u_projModelView;\n";
        StringBuilder sb3 = new StringBuilder();
        sb3.append(str3);
        sb3.append(z2 ? "varying vec4 v_col;\n" : str);
        String sb4 = sb3.toString();
        for (int i4 = 0; i4 < i2; i4++) {
            sb4 = sb4 + "varying vec2 v_tex" + i4 + ";\n";
        }
        StringBuilder sb5 = new StringBuilder();
        sb5.append(sb4);
        sb5.append("void main() {\n   gl_Position = u_projModelView * a_position;\n");
        if (z2) {
            str = "   v_col = a_color;\n";
        }
        sb5.append(str);
        String sb6 = sb5.toString();
        for (int i5 = 0; i5 < i2; i5++) {
            sb6 = sb6 + "   v_tex" + i5 + " = " + "a_texCoord" + i5 + ";\n";
        }
        return (sb6 + "   gl_PointSize = 1.0;\n") + "}\n";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.b.t.j.<init>(boolean, int, int, e.d.b.t.r[]):void
     arg types: [int, int, int, e.d.b.t.r[]]
     candidates:
      e.d.b.t.j.<init>(boolean, int, int, e.d.b.t.s):void
      e.d.b.t.j.<init>(boolean, int, int, e.d.b.t.r[]):void */
    public i(int i2, boolean z, boolean z2, int i3, s sVar) {
        this.f5141k = new Matrix4();
        this.f5133c = i2;
        this.f5138h = i3;
        this.f5136f = sVar;
        this.f5135e = new j(false, i2, 0, a(z, z2, i3));
        this.l = new float[(i2 * (this.f5135e.k().f7013b / 4))];
        this.f5139i = this.f5135e.k().f7013b / 4;
        if (this.f5135e.a(8) != null) {
            int i4 = this.f5135e.a(8).f7008e / 4;
        }
        this.f5140j = this.f5135e.a(4) != null ? this.f5135e.a(4).f7008e / 4 : 0;
        if (this.f5135e.a(16) != null) {
            int i5 = this.f5135e.a(16).f7008e / 4;
        }
        this.m = new String[i3];
        for (int i6 = 0; i6 < i3; i6++) {
            String[] strArr = this.m;
            strArr[i6] = "u_sampler" + i6;
        }
    }

    public void a(Matrix4 matrix4, int i2) {
        this.f5141k.b(matrix4);
        this.f5131a = i2;
    }

    public void a(float f2, float f3, float f4, float f5) {
        this.l[this.f5132b + this.f5140j] = b.d(f2, f3, f4, f5);
    }

    public void a(float f2) {
        this.l[this.f5132b + this.f5140j] = f2;
    }

    public void a(float f2, float f3, float f4) {
        int i2 = this.f5132b;
        float[] fArr = this.l;
        fArr[i2] = f2;
        fArr[i2 + 1] = f3;
        fArr[i2 + 2] = f4;
        this.f5132b = i2 + this.f5139i;
        this.f5134d++;
    }

    public void a() {
        if (this.f5134d != 0) {
            this.f5136f.begin();
            this.f5136f.a("u_projModelView", this.f5141k);
            for (int i2 = 0; i2 < this.f5138h; i2++) {
                this.f5136f.a(this.m[i2], i2);
            }
            this.f5135e.a(this.l, 0, this.f5132b);
            this.f5135e.a(this.f5136f, this.f5131a);
            this.f5136f.end();
            this.f5132b = 0;
            this.f5134d = 0;
        }
    }
}
