package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.utils.l;
import com.badlogic.gdx.utils.o;
import java.io.IOException;
import java.nio.ByteBuffer;

public class Gdx2DPixmap implements l {

    /* renamed from: a  reason: collision with root package name */
    long f4784a;

    /* renamed from: b  reason: collision with root package name */
    int f4785b;

    /* renamed from: c  reason: collision with root package name */
    int f4786c;

    /* renamed from: d  reason: collision with root package name */
    int f4787d;

    /* renamed from: e  reason: collision with root package name */
    ByteBuffer f4788e;

    /* renamed from: f  reason: collision with root package name */
    long[] f4789f = new long[4];

    public Gdx2DPixmap(byte[] bArr, int i2, int i3, int i4) throws IOException {
        this.f4788e = load(this.f4789f, bArr, i2, i3);
        if (this.f4788e != null) {
            long[] jArr = this.f4789f;
            this.f4784a = jArr[0];
            this.f4785b = (int) jArr[1];
            this.f4786c = (int) jArr[2];
            this.f4787d = (int) jArr[3];
            if (i4 != 0 && i4 != this.f4787d) {
                c(i4);
                return;
            }
            return;
        }
        throw new IOException("Error loading pixmap: " + getFailureReason());
    }

    private void c(int i2) {
        Gdx2DPixmap gdx2DPixmap = new Gdx2DPixmap(this.f4785b, this.f4786c, i2);
        gdx2DPixmap.b(0);
        gdx2DPixmap.a(this, 0, 0, 0, 0, this.f4785b, this.f4786c);
        dispose();
        this.f4784a = gdx2DPixmap.f4784a;
        this.f4787d = gdx2DPixmap.f4787d;
        this.f4786c = gdx2DPixmap.f4786c;
        this.f4789f = gdx2DPixmap.f4789f;
        this.f4788e = gdx2DPixmap.f4788e;
        this.f4785b = gdx2DPixmap.f4785b;
    }

    private static native void clear(long j2, int i2);

    public static int d(int i2) {
        switch (i2) {
            case 1:
                return 6406;
            case 2:
                return 6410;
            case 3:
            case 5:
                return 6407;
            case 4:
            case 6:
                return 6408;
            default:
                throw new o("unknown format: " + i2);
        }
    }

    private static native void drawPixmap(long j2, long j3, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9);

    public static int e(int i2) {
        switch (i2) {
            case 1:
            case 2:
            case 3:
            case 4:
                return 5121;
            case 5:
                return 33635;
            case 6:
                return 32819;
            default:
                throw new o("unknown format: " + i2);
        }
    }

    private static native void fillRect(long j2, int i2, int i3, int i4, int i5, int i6);

    private static native void free(long j2);

    public static native String getFailureReason();

    private static native int getPixel(long j2, int i2, int i3);

    private static native ByteBuffer load(long[] jArr, byte[] bArr, int i2, int i3);

    private static native ByteBuffer newPixmap(long[] jArr, int i2, int i3, int i4);

    private static native void setBlend(long j2, int i2);

    private static native void setPixel(long j2, int i2, int i3, int i4);

    public void a(int i2) {
        clear(this.f4784a, i2);
    }

    public void b(int i2) {
        setBlend(this.f4784a, i2);
    }

    public void dispose() {
        free(this.f4784a);
    }

    public int j() {
        return this.f4787d;
    }

    public int k() {
        return l();
    }

    public int l() {
        return d(this.f4787d);
    }

    public int m() {
        return e(this.f4787d);
    }

    public int n() {
        return this.f4786c;
    }

    public ByteBuffer o() {
        return this.f4788e;
    }

    public int p() {
        return this.f4785b;
    }

    public void a(int i2, int i3, int i4) {
        setPixel(this.f4784a, i2, i3, i4);
    }

    public int a(int i2, int i3) {
        return getPixel(this.f4784a, i2, i3);
    }

    public void a(int i2, int i3, int i4, int i5, int i6) {
        fillRect(this.f4784a, i2, i3, i4, i5, i6);
    }

    public void a(Gdx2DPixmap gdx2DPixmap, int i2, int i3, int i4, int i5, int i6, int i7) {
        drawPixmap(gdx2DPixmap.f4784a, this.f4784a, i2, i3, i6, i7, i4, i5, i6, i7);
    }

    public void a(Gdx2DPixmap gdx2DPixmap, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
        drawPixmap(gdx2DPixmap.f4784a, this.f4784a, i2, i3, i4, i5, i6, i7, i8, i9);
    }

    public Gdx2DPixmap(int i2, int i3, int i4) throws o {
        this.f4788e = newPixmap(this.f4789f, i2, i3, i4);
        if (this.f4788e != null) {
            long[] jArr = this.f4789f;
            this.f4784a = jArr[0];
            this.f4785b = (int) jArr[1];
            this.f4786c = (int) jArr[2];
            this.f4787d = (int) jArr[3];
            return;
        }
        throw new o("Error loading pixmap.");
    }
}
