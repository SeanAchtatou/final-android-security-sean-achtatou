package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.math.h;

/* compiled from: Animation */
public class a<T> {

    /* renamed from: a  reason: collision with root package name */
    T[] f4790a;

    /* renamed from: b  reason: collision with root package name */
    private float f4791b;

    /* renamed from: c  reason: collision with root package name */
    private int f4792c;

    /* renamed from: d  reason: collision with root package name */
    private float f4793d;

    /* renamed from: e  reason: collision with root package name */
    private b f4794e;

    /* renamed from: com.badlogic.gdx.graphics.g2d.a$a  reason: collision with other inner class name */
    /* compiled from: Animation */
    static /* synthetic */ class C0115a {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f4795a = new int[b.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|(3:11|12|14)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.badlogic.gdx.graphics.g2d.a$b[] r0 = com.badlogic.gdx.graphics.g2d.a.b.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.badlogic.gdx.graphics.g2d.a.C0115a.f4795a = r0
                int[] r0 = com.badlogic.gdx.graphics.g2d.a.C0115a.f4795a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.badlogic.gdx.graphics.g2d.a$b r1 = com.badlogic.gdx.graphics.g2d.a.b.NORMAL     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.badlogic.gdx.graphics.g2d.a.C0115a.f4795a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.badlogic.gdx.graphics.g2d.a$b r1 = com.badlogic.gdx.graphics.g2d.a.b.LOOP     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.badlogic.gdx.graphics.g2d.a.C0115a.f4795a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.badlogic.gdx.graphics.g2d.a$b r1 = com.badlogic.gdx.graphics.g2d.a.b.LOOP_PINGPONG     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.badlogic.gdx.graphics.g2d.a.C0115a.f4795a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.badlogic.gdx.graphics.g2d.a$b r1 = com.badlogic.gdx.graphics.g2d.a.b.LOOP_RANDOM     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = com.badlogic.gdx.graphics.g2d.a.C0115a.f4795a     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.badlogic.gdx.graphics.g2d.a$b r1 = com.badlogic.gdx.graphics.g2d.a.b.REVERSED     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r0 = com.badlogic.gdx.graphics.g2d.a.C0115a.f4795a     // Catch:{ NoSuchFieldError -> 0x004b }
                com.badlogic.gdx.graphics.g2d.a$b r1 = com.badlogic.gdx.graphics.g2d.a.b.LOOP_REVERSED     // Catch:{ NoSuchFieldError -> 0x004b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.graphics.g2d.a.C0115a.<clinit>():void");
        }
    }

    /* compiled from: Animation */
    public enum b {
        NORMAL,
        REVERSED,
        LOOP,
        LOOP_REVERSED,
        LOOP_PINGPONG,
        LOOP_RANDOM
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.utils.z0.a.a(java.lang.Class, int):java.lang.Object
     arg types: [java.lang.Class<?>, int]
     candidates:
      com.badlogic.gdx.utils.z0.a.a(java.lang.Object, int):java.lang.Object
      com.badlogic.gdx.utils.z0.a.a(java.lang.Class, int):java.lang.Object */
    public a(float f2, com.badlogic.gdx.utils.a<? extends T> aVar) {
        this.f4794e = b.NORMAL;
        this.f4791b = f2;
        Object[] objArr = (Object[]) com.badlogic.gdx.utils.z0.a.a((Class) aVar.f5373a.getClass().getComponentType(), aVar.f5374b);
        int i2 = aVar.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            objArr[i3] = aVar.get(i3);
        }
        a(objArr);
    }

    public T a(float f2) {
        return this.f4790a[b(f2)];
    }

    public int b(float f2) {
        if (this.f4790a.length == 1) {
            return 0;
        }
        int i2 = (int) (f2 / this.f4791b);
        switch (C0115a.f4795a[this.f4794e.ordinal()]) {
            case 1:
                i2 = Math.min(this.f4790a.length - 1, i2);
                break;
            case 2:
                i2 %= this.f4790a.length;
                break;
            case 3:
                T[] tArr = this.f4790a;
                i2 %= (tArr.length * 2) - 2;
                if (i2 >= tArr.length) {
                    i2 = (tArr.length - 2) - (i2 - tArr.length);
                    break;
                }
                break;
            case 4:
                if (((int) (this.f4793d / this.f4791b)) == i2) {
                    i2 = this.f4792c;
                    break;
                } else {
                    i2 = h.c(this.f4790a.length - 1);
                    break;
                }
            case 5:
                i2 = Math.max((this.f4790a.length - i2) - 1, 0);
                break;
            case 6:
                T[] tArr2 = this.f4790a;
                i2 = (tArr2.length - (i2 % tArr2.length)) - 1;
                break;
        }
        this.f4792c = i2;
        this.f4793d = f2;
        return i2;
    }

    public void c(float f2) {
        this.f4791b = f2;
        int length = this.f4790a.length;
    }

    /* access modifiers changed from: protected */
    public void a(T... tArr) {
        this.f4790a = tArr;
        int length = tArr.length;
    }

    public void a(b bVar) {
        this.f4794e = bVar;
    }

    public a(float f2, com.badlogic.gdx.utils.a<? extends T> aVar, b bVar) {
        this(f2, aVar);
        a(bVar);
    }
}
