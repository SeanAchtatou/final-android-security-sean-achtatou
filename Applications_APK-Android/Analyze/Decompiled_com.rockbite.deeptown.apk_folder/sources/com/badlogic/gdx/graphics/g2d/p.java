package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.graphics.glutils.s;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.h;
import com.esotericsoftware.spine.Animation;
import e.d.b.g;
import e.d.b.t.b;
import e.d.b.t.j;
import e.d.b.t.n;
import e.d.b.t.r;

/* compiled from: SpriteBatch */
public class p implements b {
    @Deprecated
    public static j.b x = j.b.VertexArray;

    /* renamed from: a  reason: collision with root package name */
    private j f5020a;

    /* renamed from: b  reason: collision with root package name */
    final float[] f5021b;

    /* renamed from: c  reason: collision with root package name */
    int f5022c;

    /* renamed from: d  reason: collision with root package name */
    n f5023d;

    /* renamed from: e  reason: collision with root package name */
    float f5024e;

    /* renamed from: f  reason: collision with root package name */
    float f5025f;

    /* renamed from: g  reason: collision with root package name */
    boolean f5026g;

    /* renamed from: h  reason: collision with root package name */
    private final Matrix4 f5027h;

    /* renamed from: i  reason: collision with root package name */
    private final Matrix4 f5028i;

    /* renamed from: j  reason: collision with root package name */
    private final Matrix4 f5029j;

    /* renamed from: k  reason: collision with root package name */
    private boolean f5030k;
    private int l;
    private int m;
    private int n;
    private int o;
    private final s p;
    private s q;
    private boolean r;
    private final b s;
    float t;
    public int u;
    public int v;
    public int w;

    public p() {
        this(1000, null);
    }

    public static s b() {
        s sVar = new s("attribute vec4 a_position;\nattribute vec4 a_color;\nattribute vec2 a_texCoord0;\nuniform mat4 u_projTrans;\nvarying vec4 v_color;\nvarying vec2 v_texCoords;\n\nvoid main()\n{\n   v_color = a_color;\n   v_color.a = v_color.a * (255.0/254.0);\n   v_texCoords = a_texCoord0;\n   gl_Position =  u_projTrans * a_position;\n}\n", "#ifdef GL_ES\n#define LOWP lowp\nprecision mediump float;\n#else\n#define LOWP \n#endif\nvarying LOWP vec4 v_color;\nvarying vec2 v_texCoords;\nuniform sampler2D u_texture;\nvoid main()\n{\n  gl_FragColor = v_color * texture2D(u_texture, v_texCoords);\n}");
        if (sVar.l()) {
            return sVar;
        }
        throw new IllegalArgumentException("Error compiling shader: " + sVar.k());
    }

    private void c() {
        Matrix4 matrix4 = this.f5029j;
        matrix4.b(this.f5028i);
        matrix4.a(this.f5027h);
        s sVar = this.q;
        if (sVar != null) {
            sVar.a("u_projTrans", this.f5029j);
            this.q.a("u_texture", 0);
            return;
        }
        this.p.a("u_projTrans", this.f5029j);
        this.p.a("u_texture", 0);
    }

    /* access modifiers changed from: protected */
    public void a(n nVar) {
        flush();
        this.f5023d = nVar;
        this.f5024e = 1.0f / ((float) nVar.r());
        this.f5025f = 1.0f / ((float) nVar.p());
    }

    public void begin() {
        if (!this.f5026g) {
            this.u = 0;
            g.f6806g.a(false);
            s sVar = this.q;
            if (sVar != null) {
                sVar.begin();
            } else {
                this.p.begin();
            }
            c();
            this.f5026g = true;
            return;
        }
        throw new IllegalStateException("SpriteBatch.end must be called before begin.");
    }

    public void dispose() {
        s sVar;
        this.f5020a.dispose();
        if (this.r && (sVar = this.p) != null) {
            sVar.dispose();
        }
    }

    public void draw(n nVar, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, int i2, int i3, int i4, int i5, boolean z, boolean z2) {
        float f11;
        float f12;
        float f13;
        float f14;
        float f15 = f4;
        float f16 = f5;
        int i6 = i2;
        int i7 = i3;
        if (this.f5026g) {
            float[] fArr = this.f5021b;
            if (nVar != this.f5023d) {
                a(nVar);
            } else if (this.f5022c == fArr.length) {
                flush();
            }
            float f17 = f2 + f15;
            float f18 = f3 + f16;
            float f19 = -f15;
            float f20 = -f16;
            float f21 = f6 - f15;
            float f22 = f7 - f16;
            if (!(f8 == 1.0f && f9 == 1.0f)) {
                f19 *= f8;
                f20 *= f9;
                f21 *= f8;
                f22 *= f9;
            }
            if (f10 != Animation.CurveTimeline.LINEAR) {
                float b2 = h.b(f10);
                float h2 = h.h(f10);
                float f23 = b2 * f19;
                float f24 = f23 - (h2 * f20);
                float f25 = f19 * h2;
                f20 = (f20 * b2) + f25;
                float f26 = h2 * f22;
                f12 = f23 - f26;
                float f27 = f22 * b2;
                float f28 = f25 + f27;
                float f29 = (b2 * f21) - f26;
                float f30 = f27 + (h2 * f21);
                float f31 = f30 - (f28 - f20);
                f13 = (f29 - f12) + f24;
                f21 = f29;
                f14 = f30;
                f22 = f28;
                f19 = f24;
                f11 = f31;
            } else {
                f13 = f21;
                f14 = f22;
                f12 = f19;
                f11 = f20;
            }
            float f32 = f19 + f17;
            float f33 = f20 + f18;
            float f34 = f12 + f17;
            float f35 = f22 + f18;
            float f36 = f21 + f17;
            float f37 = f14 + f18;
            float f38 = f13 + f17;
            float f39 = f11 + f18;
            float f40 = this.f5024e;
            float f41 = ((float) i6) * f40;
            float f42 = this.f5025f;
            float f43 = ((float) (i7 + i5)) * f42;
            float f44 = ((float) (i6 + i4)) * f40;
            float f45 = ((float) i7) * f42;
            if (!z) {
                float f46 = f41;
                f41 = f44;
                f44 = f46;
            }
            if (!z2) {
                float f47 = f43;
                f43 = f45;
                f45 = f47;
            }
            float f48 = this.t;
            int i8 = this.f5022c;
            fArr[i8] = f32;
            fArr[i8 + 1] = f33;
            fArr[i8 + 2] = f48;
            fArr[i8 + 3] = f44;
            fArr[i8 + 4] = f45;
            fArr[i8 + 5] = f34;
            fArr[i8 + 6] = f35;
            fArr[i8 + 7] = f48;
            fArr[i8 + 8] = f44;
            fArr[i8 + 9] = f43;
            fArr[i8 + 10] = f36;
            fArr[i8 + 11] = f37;
            fArr[i8 + 12] = f48;
            fArr[i8 + 13] = f41;
            fArr[i8 + 14] = f43;
            fArr[i8 + 15] = f38;
            fArr[i8 + 16] = f39;
            fArr[i8 + 17] = f48;
            fArr[i8 + 18] = f41;
            fArr[i8 + 19] = f45;
            this.f5022c = i8 + 20;
            return;
        }
        throw new IllegalStateException("SpriteBatch.begin must be called before draw.");
    }

    public void end() {
        if (this.f5026g) {
            if (this.f5022c > 0) {
                flush();
            }
            this.f5023d = null;
            this.f5026g = false;
            e.d.b.t.g gVar = g.f6806g;
            gVar.a(true);
            if (a()) {
                gVar.k(3042);
            }
            s sVar = this.q;
            if (sVar != null) {
                sVar.end();
            } else {
                this.p.end();
            }
        } else {
            throw new IllegalStateException("SpriteBatch.begin must be called before end.");
        }
    }

    public void flush() {
        int i2 = this.f5022c;
        if (i2 != 0) {
            this.u++;
            this.v++;
            int i3 = i2 / 20;
            if (i3 > this.w) {
                this.w = i3;
            }
            int i4 = i3 * 6;
            this.f5023d.f();
            j jVar = this.f5020a;
            jVar.a(this.f5021b, 0, this.f5022c);
            jVar.j().position(0);
            jVar.j().limit(i4);
            if (this.f5030k) {
                g.f6806g.k(3042);
            } else {
                g.f6806g.a(3042);
                int i5 = this.l;
                if (i5 != -1) {
                    g.f6806g.e(i5, this.m, this.n, this.o);
                }
            }
            s sVar = this.q;
            if (sVar == null) {
                sVar = this.p;
            }
            jVar.a(sVar, 4, 0, i4);
            this.f5022c = 0;
        }
    }

    public int getBlendDstFunc() {
        return this.m;
    }

    public int getBlendDstFuncAlpha() {
        return this.o;
    }

    public int getBlendSrcFunc() {
        return this.l;
    }

    public int getBlendSrcFuncAlpha() {
        return this.n;
    }

    public b getColor() {
        return this.s;
    }

    public s getShader() {
        s sVar = this.q;
        return sVar == null ? this.p : sVar;
    }

    public Matrix4 getTransformMatrix() {
        return this.f5027h;
    }

    public void setBlendFunction(int i2, int i3) {
        setBlendFunctionSeparate(i2, i3, i2, i3);
    }

    public void setBlendFunctionSeparate(int i2, int i3, int i4, int i5) {
        if (this.l != i2 || this.m != i3 || this.n != i4 || this.o != i5) {
            flush();
            this.l = i2;
            this.m = i3;
            this.n = i4;
            this.o = i5;
        }
    }

    public void setColor(b bVar) {
        this.s.b(bVar);
        this.t = bVar.b();
    }

    public void setProjectionMatrix(Matrix4 matrix4) {
        if (this.f5026g) {
            flush();
        }
        this.f5028i.b(matrix4);
        if (this.f5026g) {
            c();
        }
    }

    public void setShader(s sVar) {
        if (this.f5026g) {
            flush();
            s sVar2 = this.q;
            if (sVar2 != null) {
                sVar2.end();
            } else {
                this.p.end();
            }
        }
        this.q = sVar;
        if (this.f5026g) {
            s sVar3 = this.q;
            if (sVar3 != null) {
                sVar3.begin();
            } else {
                this.p.begin();
            }
            c();
        }
    }

    public void setTransformMatrix(Matrix4 matrix4) {
        if (this.f5026g) {
            flush();
        }
        this.f5027h.b(matrix4);
        if (this.f5026g) {
            c();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.b.t.j.<init>(e.d.b.t.j$b, boolean, int, int, e.d.b.t.r[]):void
     arg types: [e.d.b.t.j$b, int, int, int, e.d.b.t.r[]]
     candidates:
      e.d.b.t.j.<init>(e.d.b.t.j$b, boolean, int, int, e.d.b.t.s):void
      e.d.b.t.j.<init>(e.d.b.t.j$b, boolean, int, int, e.d.b.t.r[]):void */
    public p(int i2, s sVar) {
        int i3 = i2;
        s sVar2 = sVar;
        this.f5022c = 0;
        this.f5023d = null;
        this.f5024e = Animation.CurveTimeline.LINEAR;
        this.f5025f = Animation.CurveTimeline.LINEAR;
        this.f5026g = false;
        this.f5027h = new Matrix4();
        this.f5028i = new Matrix4();
        this.f5029j = new Matrix4();
        this.f5030k = false;
        this.l = 770;
        this.m = 771;
        this.n = 770;
        this.o = 771;
        this.q = null;
        this.s = new b(1.0f, 1.0f, 1.0f, 1.0f);
        this.t = b.f6932j;
        this.u = 0;
        this.v = 0;
        this.w = 0;
        if (i3 <= 8191) {
            int i4 = i3 * 6;
            this.f5020a = new j(g.f6808i != null ? j.b.VertexBufferObjectWithVAO : x, false, i3 * 4, i4, new r(1, 2, "a_position"), new r(4, 4, "a_color"), new r(16, 2, "a_texCoord0"));
            this.f5028i.a(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, (float) g.f6801b.getWidth(), (float) g.f6801b.getHeight());
            this.f5021b = new float[(i3 * 20)];
            short[] sArr = new short[i4];
            int i5 = 0;
            short s2 = 0;
            while (i5 < i4) {
                sArr[i5] = s2;
                sArr[i5 + 1] = (short) (s2 + 1);
                short s3 = (short) (s2 + 2);
                sArr[i5 + 2] = s3;
                sArr[i5 + 3] = s3;
                sArr[i5 + 4] = (short) (s2 + 3);
                sArr[i5 + 5] = s2;
                i5 += 6;
                s2 = (short) (s2 + 4);
            }
            this.f5020a.a(sArr);
            if (sVar2 == null) {
                this.p = b();
                this.r = true;
                return;
            }
            this.p = sVar2;
            return;
        }
        throw new IllegalArgumentException("Can't have more than 8191 sprites per batch: " + i3);
    }

    public void setColor(float f2, float f3, float f4, float f5) {
        this.s.b(f2, f3, f4, f5);
        this.t = this.s.b();
    }

    public boolean a() {
        return !this.f5030k;
    }

    public void draw(n nVar, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9) {
        if (this.f5026g) {
            float[] fArr = this.f5021b;
            if (nVar != this.f5023d) {
                a(nVar);
            } else if (this.f5022c == fArr.length) {
                flush();
            }
            float f10 = f4 + f2;
            float f11 = f5 + f3;
            float f12 = this.t;
            int i2 = this.f5022c;
            fArr[i2] = f2;
            fArr[i2 + 1] = f3;
            fArr[i2 + 2] = f12;
            fArr[i2 + 3] = f6;
            fArr[i2 + 4] = f7;
            fArr[i2 + 5] = f2;
            fArr[i2 + 6] = f11;
            fArr[i2 + 7] = f12;
            fArr[i2 + 8] = f6;
            fArr[i2 + 9] = f9;
            fArr[i2 + 10] = f10;
            fArr[i2 + 11] = f11;
            fArr[i2 + 12] = f12;
            fArr[i2 + 13] = f8;
            fArr[i2 + 14] = f9;
            fArr[i2 + 15] = f10;
            fArr[i2 + 16] = f3;
            fArr[i2 + 17] = f12;
            fArr[i2 + 18] = f8;
            fArr[i2 + 19] = f7;
            this.f5022c = i2 + 20;
            return;
        }
        throw new IllegalStateException("SpriteBatch.begin must be called before draw.");
    }

    public void draw(n nVar, float f2, float f3, float f4, float f5) {
        if (this.f5026g) {
            float[] fArr = this.f5021b;
            if (nVar != this.f5023d) {
                a(nVar);
            } else if (this.f5022c == fArr.length) {
                flush();
            }
            float f6 = f4 + f2;
            float f7 = f5 + f3;
            float f8 = this.t;
            int i2 = this.f5022c;
            fArr[i2] = f2;
            fArr[i2 + 1] = f3;
            fArr[i2 + 2] = f8;
            fArr[i2 + 3] = 0.0f;
            fArr[i2 + 4] = 1.0f;
            fArr[i2 + 5] = f2;
            fArr[i2 + 6] = f7;
            fArr[i2 + 7] = f8;
            fArr[i2 + 8] = 0.0f;
            fArr[i2 + 9] = 0.0f;
            fArr[i2 + 10] = f6;
            fArr[i2 + 11] = f7;
            fArr[i2 + 12] = f8;
            fArr[i2 + 13] = 1.0f;
            fArr[i2 + 14] = 0.0f;
            fArr[i2 + 15] = f6;
            fArr[i2 + 16] = f3;
            fArr[i2 + 17] = f8;
            fArr[i2 + 18] = 1.0f;
            fArr[i2 + 19] = 1.0f;
            this.f5022c = i2 + 20;
            return;
        }
        throw new IllegalStateException("SpriteBatch.begin must be called before draw.");
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x002c A[LOOP:0: B:10:0x0029->B:12:0x002c, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0040 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void draw(e.d.b.t.n r4, float[] r5, int r6, int r7) {
        /*
            r3 = this;
            boolean r0 = r3.f5026g
            if (r0 == 0) goto L_0x0041
            float[] r0 = r3.f5021b
            int r0 = r0.length
            e.d.b.t.n r1 = r3.f5023d
            if (r4 == r1) goto L_0x000f
            r3.a(r4)
            goto L_0x0018
        L_0x000f:
            int r4 = r3.f5022c
            int r4 = r0 - r4
            if (r4 != 0) goto L_0x0019
            r3.flush()
        L_0x0018:
            r4 = r0
        L_0x0019:
            int r4 = java.lang.Math.min(r4, r7)
            float[] r1 = r3.f5021b
            int r2 = r3.f5022c
            java.lang.System.arraycopy(r5, r6, r1, r2, r4)
            int r1 = r3.f5022c
            int r1 = r1 + r4
            r3.f5022c = r1
        L_0x0029:
            int r7 = r7 - r4
            if (r7 <= 0) goto L_0x0040
            int r6 = r6 + r4
            r3.flush()
            int r4 = java.lang.Math.min(r0, r7)
            float[] r1 = r3.f5021b
            r2 = 0
            java.lang.System.arraycopy(r5, r6, r1, r2, r4)
            int r1 = r3.f5022c
            int r1 = r1 + r4
            r3.f5022c = r1
            goto L_0x0029
        L_0x0040:
            return
        L_0x0041:
            java.lang.IllegalStateException r4 = new java.lang.IllegalStateException
            java.lang.String r5 = "SpriteBatch.begin must be called before draw."
            r4.<init>(r5)
            goto L_0x004a
        L_0x0049:
            throw r4
        L_0x004a:
            goto L_0x0049
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.graphics.g2d.p.draw(e.d.b.t.n, float[], int, int):void");
    }

    public void draw(r rVar, float f2, float f3, float f4, float f5) {
        if (this.f5026g) {
            float[] fArr = this.f5021b;
            n nVar = rVar.f5060a;
            if (nVar != this.f5023d) {
                a(nVar);
            } else if (this.f5022c == fArr.length) {
                flush();
            }
            float f6 = f4 + f2;
            float f7 = f5 + f3;
            float f8 = rVar.f5061b;
            float f9 = rVar.f5064e;
            float f10 = rVar.f5063d;
            float f11 = rVar.f5062c;
            float f12 = this.t;
            int i2 = this.f5022c;
            fArr[i2] = f2;
            fArr[i2 + 1] = f3;
            fArr[i2 + 2] = f12;
            fArr[i2 + 3] = f8;
            fArr[i2 + 4] = f9;
            fArr[i2 + 5] = f2;
            fArr[i2 + 6] = f7;
            fArr[i2 + 7] = f12;
            fArr[i2 + 8] = f8;
            fArr[i2 + 9] = f11;
            fArr[i2 + 10] = f6;
            fArr[i2 + 11] = f7;
            fArr[i2 + 12] = f12;
            fArr[i2 + 13] = f10;
            fArr[i2 + 14] = f11;
            fArr[i2 + 15] = f6;
            fArr[i2 + 16] = f3;
            fArr[i2 + 17] = f12;
            fArr[i2 + 18] = f10;
            fArr[i2 + 19] = f9;
            this.f5022c = i2 + 20;
            return;
        }
        throw new IllegalStateException("SpriteBatch.begin must be called before draw.");
    }

    public void draw(r rVar, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10) {
        float f11;
        float f12;
        float f13;
        float f14;
        float f15;
        if (this.f5026g) {
            float[] fArr = this.f5021b;
            n nVar = rVar.f5060a;
            if (nVar != this.f5023d) {
                a(nVar);
            } else if (this.f5022c == fArr.length) {
                flush();
            }
            float f16 = f2 + f4;
            float f17 = f3 + f5;
            float f18 = -f4;
            float f19 = -f5;
            float f20 = f6 - f4;
            float f21 = f7 - f5;
            if (!(f8 == 1.0f && f9 == 1.0f)) {
                f18 *= f8;
                f19 *= f9;
                f20 *= f8;
                f21 *= f9;
            }
            if (f10 != Animation.CurveTimeline.LINEAR) {
                float b2 = h.b(f10);
                float h2 = h.h(f10);
                float f22 = b2 * f18;
                f12 = f22 - (h2 * f19);
                float f23 = f18 * h2;
                f19 = (f19 * b2) + f23;
                float f24 = h2 * f21;
                f13 = f22 - f24;
                float f25 = f21 * b2;
                float f26 = f23 + f25;
                f15 = (b2 * f20) - f24;
                float f27 = f25 + (h2 * f20);
                f20 = f12 + (f15 - f13);
                f11 = f27 - (f26 - f19);
                f14 = f27;
                f21 = f26;
            } else {
                f15 = f20;
                f14 = f21;
                f13 = f18;
                f12 = f13;
                f11 = f19;
            }
            float f28 = f12 + f16;
            float f29 = f19 + f17;
            float f30 = f13 + f16;
            float f31 = f21 + f17;
            float f32 = f15 + f16;
            float f33 = f14 + f17;
            float f34 = f20 + f16;
            float f35 = f11 + f17;
            float f36 = rVar.f5061b;
            float f37 = rVar.f5064e;
            float f38 = rVar.f5063d;
            float f39 = rVar.f5062c;
            float f40 = this.t;
            int i2 = this.f5022c;
            fArr[i2] = f28;
            fArr[i2 + 1] = f29;
            fArr[i2 + 2] = f40;
            fArr[i2 + 3] = f36;
            fArr[i2 + 4] = f37;
            fArr[i2 + 5] = f30;
            fArr[i2 + 6] = f31;
            fArr[i2 + 7] = f40;
            fArr[i2 + 8] = f36;
            fArr[i2 + 9] = f39;
            fArr[i2 + 10] = f32;
            fArr[i2 + 11] = f33;
            fArr[i2 + 12] = f40;
            fArr[i2 + 13] = f38;
            fArr[i2 + 14] = f39;
            fArr[i2 + 15] = f34;
            fArr[i2 + 16] = f35;
            fArr[i2 + 17] = f40;
            fArr[i2 + 18] = f38;
            fArr[i2 + 19] = f37;
            this.f5022c = i2 + 20;
            return;
        }
        throw new IllegalStateException("SpriteBatch.begin must be called before draw.");
    }
}
