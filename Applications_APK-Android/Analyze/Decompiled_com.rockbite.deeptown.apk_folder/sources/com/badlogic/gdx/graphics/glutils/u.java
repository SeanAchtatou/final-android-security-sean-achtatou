package com.badlogic.gdx.graphics.glutils;

import com.badlogic.gdx.utils.BufferUtils;
import e.d.b.t.r;
import e.d.b.t.s;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

/* compiled from: VertexArray */
public class u implements y {

    /* renamed from: a  reason: collision with root package name */
    final s f5202a;

    /* renamed from: b  reason: collision with root package name */
    final FloatBuffer f5203b = this.f5204c.asFloatBuffer();

    /* renamed from: c  reason: collision with root package name */
    final ByteBuffer f5204c;

    public u(int i2, s sVar) {
        this.f5202a = sVar;
        this.f5204c = BufferUtils.d(this.f5202a.f7013b * i2);
        this.f5203b.flip();
        this.f5204c.flip();
    }

    public FloatBuffer a() {
        return this.f5203b;
    }

    public void b() {
    }

    public void b(s sVar, int[] iArr) {
        int size = this.f5202a.size();
        int i2 = 0;
        if (iArr == null) {
            while (i2 < size) {
                sVar.a(this.f5202a.get(i2).f7009f);
                i2++;
            }
            return;
        }
        while (i2 < size) {
            int i3 = iArr[i2];
            if (i3 >= 0) {
                sVar.a(i3);
            }
            i2++;
        }
    }

    public int c() {
        return (this.f5203b.limit() * 4) / this.f5202a.f7013b;
    }

    public s d() {
        return this.f5202a;
    }

    public void dispose() {
        BufferUtils.a(this.f5204c);
    }

    public void a(float[] fArr, int i2, int i3) {
        BufferUtils.a(fArr, this.f5204c, i3, i2);
        this.f5203b.position(0);
        this.f5203b.limit(i3);
    }

    public void a(s sVar, int[] iArr) {
        int size = this.f5202a.size();
        this.f5204c.limit(this.f5203b.limit() * 4);
        int i2 = 0;
        if (iArr == null) {
            while (i2 < size) {
                r rVar = this.f5202a.get(i2);
                int b2 = sVar.b(rVar.f7009f);
                if (b2 >= 0) {
                    sVar.b(b2);
                    if (rVar.f7007d == 5126) {
                        this.f5203b.position(rVar.f7008e / 4);
                        sVar.a(b2, rVar.f7005b, rVar.f7007d, rVar.f7006c, this.f5202a.f7013b, this.f5203b);
                    } else {
                        this.f5204c.position(rVar.f7008e);
                        sVar.a(b2, rVar.f7005b, rVar.f7007d, rVar.f7006c, this.f5202a.f7013b, this.f5204c);
                    }
                }
                i2++;
            }
            return;
        }
        while (i2 < size) {
            r rVar2 = this.f5202a.get(i2);
            int i3 = iArr[i2];
            if (i3 >= 0) {
                sVar.b(i3);
                if (rVar2.f7007d == 5126) {
                    this.f5203b.position(rVar2.f7008e / 4);
                    sVar.a(i3, rVar2.f7005b, rVar2.f7007d, rVar2.f7006c, this.f5202a.f7013b, this.f5203b);
                } else {
                    this.f5204c.position(rVar2.f7008e);
                    sVar.a(i3, rVar2.f7005b, rVar2.f7007d, rVar2.f7006c, this.f5202a.f7013b, this.f5204c);
                }
            }
            i2++;
        }
    }
}
