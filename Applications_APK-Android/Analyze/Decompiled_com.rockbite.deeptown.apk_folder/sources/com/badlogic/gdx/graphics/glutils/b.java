package com.badlogic.gdx.graphics.glutils;

import com.badlogic.gdx.utils.o;
import e.d.b.s.a;
import e.d.b.t.l;
import e.d.b.t.m;
import e.d.b.t.q;

/* compiled from: FileTextureData */
public class b implements q {

    /* renamed from: a  reason: collision with root package name */
    final a f5079a;

    /* renamed from: b  reason: collision with root package name */
    int f5080b = 0;

    /* renamed from: c  reason: collision with root package name */
    int f5081c = 0;

    /* renamed from: d  reason: collision with root package name */
    l.c f5082d;

    /* renamed from: e  reason: collision with root package name */
    l f5083e;

    /* renamed from: f  reason: collision with root package name */
    boolean f5084f;

    /* renamed from: g  reason: collision with root package name */
    boolean f5085g = false;

    public b(a aVar, l lVar, l.c cVar, boolean z) {
        this.f5079a = aVar;
        this.f5083e = lVar;
        this.f5082d = cVar;
        this.f5084f = z;
        l lVar2 = this.f5083e;
        if (lVar2 != null) {
            this.f5080b = lVar2.q();
            this.f5081c = this.f5083e.o();
            if (cVar == null) {
                this.f5082d = this.f5083e.k();
            }
        }
    }

    public void a(int i2) {
        throw new o("This TextureData implementation does not upload data itself");
    }

    public boolean a() {
        return true;
    }

    public void b() {
        if (!this.f5085g) {
            if (this.f5083e == null) {
                if (this.f5079a.b().equals("cim")) {
                    this.f5083e = m.a(this.f5079a);
                } else {
                    this.f5083e = new l(this.f5079a);
                }
                this.f5080b = this.f5083e.q();
                this.f5081c = this.f5083e.o();
                if (this.f5082d == null) {
                    this.f5082d = this.f5083e.k();
                }
            }
            this.f5085g = true;
            return;
        }
        throw new o("Already prepared");
    }

    public boolean c() {
        return this.f5085g;
    }

    public l d() {
        if (this.f5085g) {
            this.f5085g = false;
            l lVar = this.f5083e;
            this.f5083e = null;
            return lVar;
        }
        throw new o("Call prepare() before calling getPixmap()");
    }

    public boolean e() {
        return this.f5084f;
    }

    public boolean f() {
        return true;
    }

    public l.c getFormat() {
        return this.f5082d;
    }

    public int getHeight() {
        return this.f5081c;
    }

    public q.b getType() {
        return q.b.Pixmap;
    }

    public int getWidth() {
        return this.f5080b;
    }

    public String toString() {
        return this.f5079a.toString();
    }
}
