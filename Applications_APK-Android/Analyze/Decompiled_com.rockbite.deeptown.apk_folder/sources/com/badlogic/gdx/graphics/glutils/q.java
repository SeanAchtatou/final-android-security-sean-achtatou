package com.badlogic.gdx.graphics.glutils;

import com.badlogic.gdx.utils.o;
import e.d.b.a;
import e.d.b.g;
import e.d.b.t.l;

/* compiled from: MipMapGenerator */
public class q {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f5170a = true;

    public static void a(int i2, l lVar, int i3, int i4) {
        if (!f5170a) {
            b(i2, lVar, i3, i4);
        } else if (g.f6800a.getType() == a.C0167a.Android || g.f6800a.getType() == a.C0167a.WebGL || g.f6800a.getType() == a.C0167a.iOS) {
            a(i2, lVar);
        } else {
            c(i2, lVar, i3, i4);
        }
    }

    private static void b(int i2, l lVar, int i3, int i4) {
        g.f6806g.b(i2, 0, lVar.m(), lVar.q(), lVar.o(), 0, lVar.l(), lVar.n(), lVar.p());
        if (g.f6807h != null || i3 == i4) {
            int q = lVar.q() / 2;
            int o = lVar.o() / 2;
            int i5 = 1;
            l lVar2 = lVar;
            while (q > 0 && o > 0) {
                l lVar3 = new l(q, o, lVar2.k());
                lVar3.a(l.a.None);
                lVar3.a(lVar2, 0, 0, lVar2.q(), lVar2.o(), 0, 0, q, o);
                if (i5 > 1) {
                    lVar2.dispose();
                }
                lVar2 = lVar3;
                g.f6806g.b(i2, i5, lVar3.m(), lVar3.q(), lVar3.o(), 0, lVar3.l(), lVar3.n(), lVar3.p());
                q = lVar2.q() / 2;
                o = lVar2.o() / 2;
                i5++;
            }
            return;
        }
        throw new o("texture width and height must be square when using mipmapping.");
    }

    private static void c(int i2, l lVar, int i3, int i4) {
        if (g.f6801b.a("GL_ARB_framebuffer_object") || g.f6801b.a("GL_EXT_framebuffer_object") || g.f6808i != null) {
            g.f6806g.b(i2, 0, lVar.m(), lVar.q(), lVar.o(), 0, lVar.l(), lVar.n(), lVar.p());
            g.f6807h.n(i2);
            return;
        }
        b(i2, lVar, i3, i4);
    }

    private static void a(int i2, l lVar) {
        g.f6806g.b(i2, 0, lVar.m(), lVar.q(), lVar.o(), 0, lVar.l(), lVar.n(), lVar.p());
        g.f6807h.n(i2);
    }
}
