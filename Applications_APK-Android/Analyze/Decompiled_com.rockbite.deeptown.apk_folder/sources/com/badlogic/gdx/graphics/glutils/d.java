package com.badlogic.gdx.graphics.glutils;

import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.l;
import com.badlogic.gdx.utils.o;
import com.underwater.demolisher.data.vo.RemoteConfigConst;
import e.d.b.a;
import e.d.b.g;
import e.d.b.t.i;
import e.d.b.t.l;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: GLFrameBuffer */
public abstract class d<T extends i> implements l {

    /* renamed from: i  reason: collision with root package name */
    protected static final Map<e.d.b.a, com.badlogic.gdx.utils.a<d>> f5086i = new HashMap();

    /* renamed from: j  reason: collision with root package name */
    protected static int f5087j;

    /* renamed from: k  reason: collision with root package name */
    protected static boolean f5088k = false;

    /* renamed from: a  reason: collision with root package name */
    protected com.badlogic.gdx.utils.a<T> f5089a = new com.badlogic.gdx.utils.a<>();

    /* renamed from: b  reason: collision with root package name */
    protected int f5090b;

    /* renamed from: c  reason: collision with root package name */
    protected int f5091c;

    /* renamed from: d  reason: collision with root package name */
    protected int f5092d;

    /* renamed from: e  reason: collision with root package name */
    protected int f5093e;

    /* renamed from: f  reason: collision with root package name */
    protected boolean f5094f;

    /* renamed from: g  reason: collision with root package name */
    protected boolean f5095g;

    /* renamed from: h  reason: collision with root package name */
    protected C0120d<? extends d<T>> f5096h;

    /* compiled from: GLFrameBuffer */
    public static class a extends C0120d<c> {
        public a(int i2, int i3) {
            super(i2, i3);
        }
    }

    /* compiled from: GLFrameBuffer */
    protected static class b {

        /* renamed from: a  reason: collision with root package name */
        int f5097a;

        public b(int i2) {
            this.f5097a = i2;
        }
    }

    /* compiled from: GLFrameBuffer */
    protected static class c {

        /* renamed from: a  reason: collision with root package name */
        int f5098a;

        /* renamed from: b  reason: collision with root package name */
        int f5099b;

        /* renamed from: c  reason: collision with root package name */
        int f5100c;

        /* renamed from: d  reason: collision with root package name */
        boolean f5101d;

        /* renamed from: e  reason: collision with root package name */
        boolean f5102e;

        /* renamed from: f  reason: collision with root package name */
        boolean f5103f;

        public c(int i2, int i3, int i4) {
            this.f5098a = i2;
            this.f5099b = i3;
            this.f5100c = i4;
        }

        public boolean a() {
            return !this.f5102e && !this.f5103f;
        }
    }

    /* renamed from: com.badlogic.gdx.graphics.glutils.d$d  reason: collision with other inner class name */
    /* compiled from: GLFrameBuffer */
    protected static abstract class C0120d<U extends d<? extends i>> {

        /* renamed from: a  reason: collision with root package name */
        protected int f5104a;

        /* renamed from: b  reason: collision with root package name */
        protected int f5105b;

        /* renamed from: c  reason: collision with root package name */
        protected com.badlogic.gdx.utils.a<c> f5106c = new com.badlogic.gdx.utils.a<>();

        /* renamed from: d  reason: collision with root package name */
        protected b f5107d;

        /* renamed from: e  reason: collision with root package name */
        protected b f5108e;

        /* renamed from: f  reason: collision with root package name */
        protected b f5109f;

        /* renamed from: g  reason: collision with root package name */
        protected boolean f5110g;

        /* renamed from: h  reason: collision with root package name */
        protected boolean f5111h;

        /* renamed from: i  reason: collision with root package name */
        protected boolean f5112i;

        public C0120d(int i2, int i3) {
            this.f5104a = i2;
            this.f5105b = i3;
        }

        public C0120d<U> a(int i2, int i3, int i4) {
            this.f5106c.add(new c(i2, i3, i4));
            return this;
        }

        public C0120d<U> b(int i2) {
            this.f5107d = new b(i2);
            this.f5110g = true;
            return this;
        }

        public C0120d<U> a(l.c cVar) {
            int b2 = l.c.b(cVar);
            a(b2, b2, l.c.c(cVar));
            return this;
        }

        public C0120d<U> b() {
            b(36168);
            return this;
        }

        public C0120d<U> a(int i2) {
            this.f5108e = new b(i2);
            this.f5111h = true;
            return this;
        }

        public C0120d<U> a() {
            a(33189);
            return this;
        }
    }

    d() {
    }

    public static void b(e.d.b.a aVar) {
        com.badlogic.gdx.utils.a aVar2;
        if (g.f6807h != null && (aVar2 = f5086i.get(aVar)) != null) {
            for (int i2 = 0; i2 < aVar2.f5374b; i2++) {
                ((d) aVar2.get(i2)).j();
            }
        }
    }

    public static void e() {
        g.f6807h.c(36160, f5087j);
    }

    private void o() {
        if (!g.f6801b.a()) {
            C0120d<? extends d<T>> dVar = this.f5096h;
            if (!dVar.f5112i) {
                com.badlogic.gdx.utils.a<c> aVar = dVar.f5106c;
                if (aVar.f5374b <= 1) {
                    Iterator<c> it = aVar.iterator();
                    while (it.hasNext()) {
                        c next = it.next();
                        if (next.f5102e) {
                            throw new o("Depth texture FrameBuffer Attachment not available on GLES 2.0");
                        } else if (next.f5103f) {
                            throw new o("Stencil texture FrameBuffer Attachment not available on GLES 2.0");
                        } else if (next.f5101d && !g.f6801b.a("OES_texture_float")) {
                            throw new o("Float texture FrameBuffer Attachment not available on GLES 2.0");
                        }
                    }
                    return;
                }
                throw new o("Multiple render targets not available on GLES 2.0");
            }
            throw new o("Packed Stencil/Render render buffers are not available on GLES 2.0");
        }
    }

    public static String p() {
        StringBuilder sb = new StringBuilder();
        a(sb);
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public abstract T a(c cVar);

    public void a(int i2, int i3, int i4, int i5) {
        e();
        g.f6807h.f(i2, i3, i4, i5);
    }

    /* access modifiers changed from: protected */
    public abstract void a(i iVar);

    /* access modifiers changed from: protected */
    public abstract void b(i iVar);

    public void begin() {
        f();
        n();
    }

    public void dispose() {
        e.d.b.t.g gVar = g.f6807h;
        Iterator<T> it = this.f5089a.iterator();
        while (it.hasNext()) {
            b((i) it.next());
        }
        if (this.f5094f) {
            gVar.g(this.f5093e);
        } else {
            if (this.f5096h.f5111h) {
                gVar.g(this.f5091c);
            }
            if (this.f5096h.f5110g) {
                gVar.g(this.f5092d);
            }
        }
        gVar.m(this.f5090b);
        if (f5086i.get(g.f6800a) != null) {
            f5086i.get(g.f6800a).d(this, true);
        }
    }

    public void end() {
        a(0, 0, g.f6801b.b(), g.f6801b.e());
    }

    public void f() {
        g.f6807h.c(36160, this.f5090b);
    }

    /* access modifiers changed from: protected */
    public void j() {
        int i2;
        e.d.b.t.g gVar = g.f6807h;
        o();
        if (!f5088k) {
            f5088k = true;
            if (g.f6800a.getType() == a.C0167a.iOS) {
                IntBuffer asIntBuffer = ByteBuffer.allocateDirect(64).order(ByteOrder.nativeOrder()).asIntBuffer();
                gVar.c(36006, asIntBuffer);
                f5087j = asIntBuffer.get(0);
            } else {
                f5087j = 0;
            }
        }
        this.f5090b = gVar.f();
        gVar.c(36160, this.f5090b);
        C0120d<? extends d<T>> dVar = this.f5096h;
        int i3 = dVar.f5104a;
        int i4 = dVar.f5105b;
        if (dVar.f5111h) {
            this.f5091c = gVar.d();
            gVar.a(36161, this.f5091c);
            gVar.c(36161, this.f5096h.f5108e.f5097a, i3, i4);
        }
        if (this.f5096h.f5110g) {
            this.f5092d = gVar.d();
            gVar.a(36161, this.f5092d);
            gVar.c(36161, this.f5096h.f5107d.f5097a, i3, i4);
        }
        if (this.f5096h.f5112i) {
            this.f5093e = gVar.d();
            gVar.a(36161, this.f5093e);
            gVar.c(36161, this.f5096h.f5109f.f5097a, i3, i4);
        }
        this.f5095g = this.f5096h.f5106c.f5374b > 1;
        if (this.f5095g) {
            Iterator<c> it = this.f5096h.f5106c.iterator();
            int i5 = 0;
            while (it.hasNext()) {
                c next = it.next();
                i a2 = a(next);
                this.f5089a.add(a2);
                if (next.a()) {
                    gVar.a(36160, i5 + 36064, 3553, a2.m(), 0);
                    i5++;
                } else if (next.f5102e) {
                    gVar.a(36160, 36096, 3553, a2.m(), 0);
                } else if (next.f5103f) {
                    gVar.a(36160, 36128, 3553, a2.m(), 0);
                }
            }
            i2 = i5;
        } else {
            i a3 = a(this.f5096h.f5106c.a());
            this.f5089a.add(a3);
            gVar.h(a3.f6943a, a3.m());
            i2 = 0;
        }
        if (this.f5095g) {
            IntBuffer c2 = BufferUtils.c(i2);
            for (int i6 = 0; i6 < i2; i6++) {
                c2.put(i6 + 36064);
            }
            c2.position(0);
            g.f6808i.b(i2, c2);
        } else {
            a((i) this.f5089a.a());
        }
        if (this.f5096h.f5111h) {
            gVar.a(36160, 36096, 36161, this.f5091c);
        }
        if (this.f5096h.f5110g) {
            gVar.a(36160, 36128, 36161, this.f5092d);
        }
        if (this.f5096h.f5112i) {
            gVar.a(36160, 33306, 36161, this.f5093e);
        }
        gVar.a(36161, 0);
        Iterator<T> it2 = this.f5089a.iterator();
        while (it2.hasNext()) {
            gVar.h(((i) it2.next()).f6943a, 0);
        }
        int r = gVar.r(36160);
        if (r == 36061) {
            C0120d<? extends d<T>> dVar2 = this.f5096h;
            if (dVar2.f5111h && dVar2.f5110g && (g.f6801b.a("GL_OES_packed_depth_stencil") || g.f6801b.a("GL_EXT_packed_depth_stencil"))) {
                if (this.f5096h.f5111h) {
                    gVar.g(this.f5091c);
                    this.f5091c = 0;
                }
                if (this.f5096h.f5110g) {
                    gVar.g(this.f5092d);
                    this.f5092d = 0;
                }
                if (this.f5096h.f5112i) {
                    gVar.g(this.f5093e);
                    this.f5093e = 0;
                }
                this.f5093e = gVar.d();
                this.f5094f = true;
                gVar.a(36161, this.f5093e);
                gVar.c(36161, 35056, i3, i4);
                gVar.a(36161, 0);
                gVar.a(36160, 36096, 36161, this.f5093e);
                gVar.a(36160, 36128, 36161, this.f5093e);
                r = gVar.r(36160);
            }
        }
        gVar.c(36160, f5087j);
        if (r != 36053) {
            Iterator<T> it3 = this.f5089a.iterator();
            while (it3.hasNext()) {
                b((i) it3.next());
            }
            if (this.f5094f) {
                gVar.e(this.f5093e);
            } else {
                if (this.f5096h.f5111h) {
                    gVar.g(this.f5091c);
                }
                if (this.f5096h.f5110g) {
                    gVar.g(this.f5092d);
                }
            }
            gVar.m(this.f5090b);
            if (r == 36054) {
                throw new IllegalStateException("Frame buffer couldn't be constructed: incomplete attachment");
            } else if (r == 36057) {
                throw new IllegalStateException("Frame buffer couldn't be constructed: incomplete dimensions");
            } else if (r == 36055) {
                throw new IllegalStateException("Frame buffer couldn't be constructed: missing attachment");
            } else if (r == 36061) {
                throw new IllegalStateException("Frame buffer couldn't be constructed: unsupported combination of formats");
            } else {
                throw new IllegalStateException("Frame buffer couldn't be constructed: unknown error " + r);
            }
        } else {
            a(g.f6800a, this);
        }
    }

    public T k() {
        return (i) this.f5089a.a();
    }

    public int l() {
        return this.f5096h.f5105b;
    }

    public int m() {
        return this.f5096h.f5104a;
    }

    /* access modifiers changed from: protected */
    public void n() {
        e.d.b.t.g gVar = g.f6807h;
        C0120d<? extends d<T>> dVar = this.f5096h;
        gVar.f(0, 0, dVar.f5104a, dVar.f5105b);
    }

    private static void a(e.d.b.a aVar, d dVar) {
        com.badlogic.gdx.utils.a aVar2 = f5086i.get(aVar);
        if (aVar2 == null) {
            aVar2 = new com.badlogic.gdx.utils.a();
        }
        aVar2.add(dVar);
        f5086i.put(aVar, aVar2);
    }

    public static void a(e.d.b.a aVar) {
        f5086i.remove(aVar);
    }

    public static StringBuilder a(StringBuilder sb) {
        sb.append("Managed buffers/app: { ");
        for (e.d.b.a aVar : f5086i.keySet()) {
            sb.append(f5086i.get(aVar).f5374b);
            sb.append(RemoteConfigConst.SHOP_SPECIAL_EVENT_OFFER);
        }
        sb.append("}");
        return sb;
    }
}
