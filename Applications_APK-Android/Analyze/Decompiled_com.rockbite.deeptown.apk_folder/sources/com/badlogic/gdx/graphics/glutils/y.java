package com.badlogic.gdx.graphics.glutils;

import com.badlogic.gdx.utils.l;
import e.d.b.t.s;
import java.nio.FloatBuffer;

/* compiled from: VertexData */
public interface y extends l {
    FloatBuffer a();

    void a(s sVar, int[] iArr);

    void a(float[] fArr, int i2, int i3);

    void b();

    void b(s sVar, int[] iArr);

    int c();

    s d();

    void dispose();
}
