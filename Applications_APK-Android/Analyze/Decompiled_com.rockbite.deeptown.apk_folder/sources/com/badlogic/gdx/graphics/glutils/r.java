package com.badlogic.gdx.graphics.glutils;

import com.badlogic.gdx.utils.o;
import e.d.b.t.l;
import e.d.b.t.q;

/* compiled from: PixmapTextureData */
public class r implements q {

    /* renamed from: a  reason: collision with root package name */
    final l f5171a;

    /* renamed from: b  reason: collision with root package name */
    final l.c f5172b;

    /* renamed from: c  reason: collision with root package name */
    final boolean f5173c;

    /* renamed from: d  reason: collision with root package name */
    final boolean f5174d;

    /* renamed from: e  reason: collision with root package name */
    final boolean f5175e;

    public r(l lVar, l.c cVar, boolean z, boolean z2) {
        this(lVar, cVar, z, z2, false);
    }

    public boolean a() {
        return this.f5175e;
    }

    public void b() {
        throw new o("prepare() must not be called on a PixmapTextureData instance as it is already prepared.");
    }

    public boolean c() {
        return true;
    }

    public l d() {
        return this.f5171a;
    }

    public boolean e() {
        return this.f5173c;
    }

    public boolean f() {
        return this.f5174d;
    }

    public l.c getFormat() {
        return this.f5172b;
    }

    public int getHeight() {
        return this.f5171a.o();
    }

    public q.b getType() {
        return q.b.Pixmap;
    }

    public int getWidth() {
        return this.f5171a.q();
    }

    public r(l lVar, l.c cVar, boolean z, boolean z2, boolean z3) {
        this.f5171a = lVar;
        this.f5172b = cVar == null ? lVar.k() : cVar;
        this.f5173c = z;
        this.f5174d = z2;
        this.f5175e = z3;
    }

    public void a(int i2) {
        throw new o("This TextureData implementation does not upload data itself");
    }
}
