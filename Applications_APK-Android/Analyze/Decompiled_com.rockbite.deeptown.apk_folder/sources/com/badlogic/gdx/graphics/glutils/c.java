package com.badlogic.gdx.graphics.glutils;

import com.badlogic.gdx.graphics.glutils.d;
import e.d.b.g;
import e.d.b.t.l;
import e.d.b.t.n;

/* compiled from: FrameBuffer */
public class c extends d<n> {
    public c(l.c cVar, int i2, int i3, boolean z) {
        this(cVar, i2, i3, z, false);
    }

    public c(l.c cVar, int i2, int i3, boolean z, boolean z2) {
        d.a aVar = new d.a(i2, i3);
        aVar.a(cVar);
        if (z) {
            aVar.a();
        }
        if (z2) {
            aVar.b();
        }
        this.f5096h = aVar;
        j();
    }

    /* access modifiers changed from: protected */
    public void b(n nVar) {
        nVar.dispose();
    }

    /* access modifiers changed from: protected */
    public n a(d.c cVar) {
        d.C0120d<? extends d<T>> dVar = this.f5096h;
        n nVar = new n(new e(dVar.f5104a, dVar.f5105b, 0, cVar.f5098a, cVar.f5099b, cVar.f5100c));
        n.b bVar = n.b.Linear;
        nVar.a(bVar, bVar);
        n.c cVar2 = n.c.ClampToEdge;
        nVar.a(cVar2, cVar2);
        return nVar;
    }

    /* access modifiers changed from: protected */
    public void a(n nVar) {
        g.f6807h.a(36160, 36064, 3553, nVar.m(), 0);
    }
}
