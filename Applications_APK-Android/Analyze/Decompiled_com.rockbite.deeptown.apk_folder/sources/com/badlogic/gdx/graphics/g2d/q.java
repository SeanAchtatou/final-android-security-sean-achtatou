package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.utils.c0;
import com.badlogic.gdx.utils.d0;
import com.badlogic.gdx.utils.l;
import com.badlogic.gdx.utils.o;
import com.badlogic.gdx.utils.q0;
import com.esotericsoftware.spine.Animation;
import com.google.android.gms.common.api.Api;
import e.d.b.t.l;
import e.d.b.t.n;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Comparator;
import java.util.Iterator;

/* compiled from: TextureAtlas */
public class q implements l {

    /* renamed from: c  reason: collision with root package name */
    static final String[] f5031c = new String[4];

    /* renamed from: d  reason: collision with root package name */
    static final Comparator<d.b> f5032d = new a();

    /* renamed from: a  reason: collision with root package name */
    private final d0<n> f5033a;

    /* renamed from: b  reason: collision with root package name */
    private final com.badlogic.gdx.utils.a<b> f5034b;

    /* compiled from: TextureAtlas */
    static class a implements Comparator<d.b> {
        a() {
        }

        /* renamed from: a */
        public int compare(d.b bVar, d.b bVar2) {
            int i2 = bVar.f5050b;
            if (i2 == -1) {
                i2 = Api.BaseClientBuilder.API_PRIORITY_OTHER;
            }
            int i3 = bVar2.f5050b;
            if (i3 == -1) {
                i3 = Api.BaseClientBuilder.API_PRIORITY_OTHER;
            }
            return i2 - i3;
        }
    }

    /* compiled from: TextureAtlas */
    public static class c extends o {
        final b u;
        float v;
        float w;

        public c(b bVar) {
            this.u = new b(bVar);
            this.v = bVar.f5037j;
            this.w = bVar.f5038k;
            a(bVar);
            a(((float) bVar.n) / 2.0f, ((float) bVar.o) / 2.0f);
            int b2 = bVar.b();
            int a2 = bVar.a();
            if (bVar.p) {
                super.a(true);
                super.b(bVar.f5037j, bVar.f5038k, (float) a2, (float) b2);
            } else {
                super.b(bVar.f5037j, bVar.f5038k, (float) b2, (float) a2);
            }
            c(1.0f, 1.0f, 1.0f, 1.0f);
        }

        public void a(float f2, float f3) {
            b bVar = this.u;
            super.a(f2 - bVar.f5037j, f3 - bVar.f5038k);
        }

        public void b(float f2, float f3) {
            b bVar = this.u;
            super.b(f2 + bVar.f5037j, f3 + bVar.f5038k);
        }

        public void d(float f2, float f3) {
            b(s(), t(), f2, f3);
        }

        public void h(float f2) {
            super.h(f2 + this.u.f5037j);
        }

        public void i(float f2) {
            super.i(f2 + this.u.f5038k);
        }

        public float n() {
            return (super.n() / this.u.l()) * ((float) this.u.o);
        }

        public float o() {
            return super.o() + this.u.f5037j;
        }

        public float p() {
            return super.p() + this.u.f5038k;
        }

        public float r() {
            return (super.r() / this.u.m()) * ((float) this.u.n);
        }

        public float s() {
            return super.s() - this.u.f5037j;
        }

        public float t() {
            return super.t() - this.u.f5038k;
        }

        public String toString() {
            return this.u.toString();
        }

        public void u() {
            b bVar = this.u;
            super.a((this.l / 2.0f) - bVar.f5037j, (this.m / 2.0f) - bVar.f5038k);
        }

        public float v() {
            return super.n() / this.u.l();
        }

        public float w() {
            return super.r() / this.u.m();
        }

        public void a(boolean z, boolean z2) {
            if (this.u.p) {
                super.a(z2, z);
            } else {
                super.a(z, z2);
            }
            float o = o();
            float p = p();
            b bVar = this.u;
            float f2 = bVar.f5037j;
            float f3 = bVar.f5038k;
            float w2 = w();
            float v2 = v();
            b bVar2 = this.u;
            bVar2.f5037j = this.v;
            bVar2.f5038k = this.w;
            bVar2.a(z, z2);
            b bVar3 = this.u;
            float f4 = bVar3.f5037j;
            this.v = f4;
            float f5 = bVar3.f5038k;
            this.w = f5;
            bVar3.f5037j = f4 * w2;
            bVar3.f5038k = f5 * v2;
            e(bVar3.f5037j - f2, bVar3.f5038k - f3);
            a(o, p);
        }

        public void b(float f2, float f3, float f4, float f5) {
            b bVar = this.u;
            float f6 = f4 / ((float) bVar.n);
            float f7 = f5 / ((float) bVar.o);
            bVar.f5037j = this.v * f6;
            bVar.f5038k = this.w * f7;
            int i2 = bVar.p ? bVar.m : bVar.l;
            b bVar2 = this.u;
            int i3 = bVar2.p ? bVar2.l : bVar2.m;
            b bVar3 = this.u;
            super.b(f2 + bVar3.f5037j, f3 + bVar3.f5038k, ((float) i2) * f6, ((float) i3) * f7);
        }

        public c(c cVar) {
            this.u = cVar.u;
            this.v = cVar.v;
            this.w = cVar.w;
            a((o) cVar);
        }

        public void a(boolean z) {
            super.a(z);
            float o = o();
            float p = p();
            b bVar = this.u;
            float f2 = bVar.f5037j;
            float f3 = bVar.f5038k;
            float w2 = w();
            float v2 = v();
            if (z) {
                b bVar2 = this.u;
                bVar2.f5037j = f3;
                bVar2.f5038k = ((((float) bVar2.o) * v2) - f2) - (((float) bVar2.l) * w2);
            } else {
                b bVar3 = this.u;
                bVar3.f5037j = ((((float) bVar3.n) * w2) - f3) - (((float) bVar3.m) * v2);
                bVar3.f5038k = f2;
            }
            b bVar4 = this.u;
            e(bVar4.f5037j - f2, bVar4.f5038k - f3);
            a(o, p);
        }
    }

    /* compiled from: TextureAtlas */
    public static class d {

        /* renamed from: a  reason: collision with root package name */
        final com.badlogic.gdx.utils.a<a> f5039a = new com.badlogic.gdx.utils.a<>();

        /* renamed from: b  reason: collision with root package name */
        final com.badlogic.gdx.utils.a<b> f5040b = new com.badlogic.gdx.utils.a<>();

        /* compiled from: TextureAtlas */
        public static class a {

            /* renamed from: a  reason: collision with root package name */
            public final e.d.b.s.a f5041a;

            /* renamed from: b  reason: collision with root package name */
            public n f5042b;

            /* renamed from: c  reason: collision with root package name */
            public final boolean f5043c;

            /* renamed from: d  reason: collision with root package name */
            public final l.c f5044d;

            /* renamed from: e  reason: collision with root package name */
            public final n.b f5045e;

            /* renamed from: f  reason: collision with root package name */
            public final n.b f5046f;

            /* renamed from: g  reason: collision with root package name */
            public final n.c f5047g;

            /* renamed from: h  reason: collision with root package name */
            public final n.c f5048h;

            public a(e.d.b.s.a aVar, float f2, float f3, boolean z, l.c cVar, n.b bVar, n.b bVar2, n.c cVar2, n.c cVar3) {
                this.f5041a = aVar;
                this.f5043c = z;
                this.f5044d = cVar;
                this.f5045e = bVar;
                this.f5046f = bVar2;
                this.f5047g = cVar2;
                this.f5048h = cVar3;
            }
        }

        /* compiled from: TextureAtlas */
        public static class b {

            /* renamed from: a  reason: collision with root package name */
            public a f5049a;

            /* renamed from: b  reason: collision with root package name */
            public int f5050b;

            /* renamed from: c  reason: collision with root package name */
            public String f5051c;

            /* renamed from: d  reason: collision with root package name */
            public float f5052d;

            /* renamed from: e  reason: collision with root package name */
            public float f5053e;

            /* renamed from: f  reason: collision with root package name */
            public int f5054f;

            /* renamed from: g  reason: collision with root package name */
            public int f5055g;

            /* renamed from: h  reason: collision with root package name */
            public boolean f5056h;

            /* renamed from: i  reason: collision with root package name */
            public int f5057i;

            /* renamed from: j  reason: collision with root package name */
            public int f5058j;

            /* renamed from: k  reason: collision with root package name */
            public int f5059k;
            public int l;
            public int m;
            public boolean n;
            public int[] o;
            public int[] p;
        }

        public d(e.d.b.s.a aVar, e.d.b.s.a aVar2, boolean z) {
            int i2;
            float f2;
            float f3;
            n.c cVar;
            n.c cVar2;
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(aVar.l()), 64);
            while (true) {
                a aVar3 = null;
                while (true) {
                    try {
                        String readLine = bufferedReader.readLine();
                        if (readLine == null) {
                            q0.a(bufferedReader);
                            this.f5040b.sort(q.f5032d);
                            return;
                        } else if (readLine.trim().length() == 0) {
                            break;
                        } else if (aVar3 == null) {
                            e.d.b.s.a a2 = aVar2.a(readLine);
                            if (q.a(bufferedReader) == 2) {
                                q.a(bufferedReader);
                                f3 = (float) Integer.parseInt(q.f5031c[0]);
                                f2 = (float) Integer.parseInt(q.f5031c[1]);
                            } else {
                                f3 = Animation.CurveTimeline.LINEAR;
                                f2 = Animation.CurveTimeline.LINEAR;
                            }
                            l.c valueOf = l.c.valueOf(q.f5031c[0]);
                            q.a(bufferedReader);
                            n.b valueOf2 = n.b.valueOf(q.f5031c[0]);
                            n.b valueOf3 = n.b.valueOf(q.f5031c[1]);
                            String b2 = q.b(bufferedReader);
                            n.c cVar3 = n.c.ClampToEdge;
                            n.c cVar4 = n.c.ClampToEdge;
                            if (b2.equals("x")) {
                                cVar2 = n.c.Repeat;
                            } else {
                                if (b2.equals("y")) {
                                    cVar = n.c.Repeat;
                                    cVar2 = cVar3;
                                } else if (b2.equals("xy")) {
                                    cVar2 = n.c.Repeat;
                                    cVar = n.c.Repeat;
                                } else {
                                    cVar2 = cVar3;
                                }
                                aVar3 = new a(a2, f3, f2, valueOf2.b(), valueOf, valueOf2, valueOf3, cVar2, cVar);
                                this.f5039a.add(aVar3);
                            }
                            cVar = cVar4;
                            aVar3 = new a(a2, f3, f2, valueOf2.b(), valueOf, valueOf2, valueOf3, cVar2, cVar);
                            this.f5039a.add(aVar3);
                        } else {
                            e.d.b.s.a aVar4 = aVar2;
                            String b3 = q.b(bufferedReader);
                            if (b3.equalsIgnoreCase("true")) {
                                i2 = 90;
                            } else if (b3.equalsIgnoreCase("false")) {
                                i2 = 0;
                            } else {
                                i2 = Integer.valueOf(b3).intValue();
                            }
                            q.a(bufferedReader);
                            int parseInt = Integer.parseInt(q.f5031c[0]);
                            int parseInt2 = Integer.parseInt(q.f5031c[1]);
                            q.a(bufferedReader);
                            int parseInt3 = Integer.parseInt(q.f5031c[0]);
                            int parseInt4 = Integer.parseInt(q.f5031c[1]);
                            b bVar = new b();
                            bVar.f5049a = aVar3;
                            bVar.f5058j = parseInt;
                            bVar.f5059k = parseInt2;
                            bVar.l = parseInt3;
                            bVar.m = parseInt4;
                            bVar.f5051c = readLine;
                            bVar.f5056h = i2 == 90;
                            bVar.f5057i = i2;
                            if (q.a(bufferedReader) == 4) {
                                bVar.o = new int[]{Integer.parseInt(q.f5031c[0]), Integer.parseInt(q.f5031c[1]), Integer.parseInt(q.f5031c[2]), Integer.parseInt(q.f5031c[3])};
                                if (q.a(bufferedReader) == 4) {
                                    bVar.p = new int[]{Integer.parseInt(q.f5031c[0]), Integer.parseInt(q.f5031c[1]), Integer.parseInt(q.f5031c[2]), Integer.parseInt(q.f5031c[3])};
                                    q.a(bufferedReader);
                                }
                            }
                            bVar.f5054f = Integer.parseInt(q.f5031c[0]);
                            bVar.f5055g = Integer.parseInt(q.f5031c[1]);
                            q.a(bufferedReader);
                            bVar.f5052d = (float) Integer.parseInt(q.f5031c[0]);
                            bVar.f5053e = (float) Integer.parseInt(q.f5031c[1]);
                            bVar.f5050b = Integer.parseInt(q.b(bufferedReader));
                            if (z) {
                                bVar.n = true;
                            }
                            this.f5040b.add(bVar);
                        }
                    } catch (Exception e2) {
                        throw new o("Error reading pack file: " + aVar, e2);
                    } catch (Throwable th) {
                        q0.a(bufferedReader);
                        throw th;
                    }
                }
                e.d.b.s.a aVar5 = aVar2;
            }
        }

        public com.badlogic.gdx.utils.a<a> a() {
            return this.f5039a;
        }
    }

    public q() {
        this.f5033a = new d0<>(4);
        this.f5034b = new com.badlogic.gdx.utils.a<>();
    }

    private void a(d dVar) {
        c0 c0Var = new c0();
        Iterator<d.a> it = dVar.f5039a.iterator();
        while (it.hasNext()) {
            d.a next = it.next();
            n nVar = next.f5042b;
            if (nVar == null) {
                nVar = new n(next.f5041a, next.f5044d, next.f5043c);
                nVar.a(next.f5045e, next.f5046f);
                nVar.a(next.f5047g, next.f5048h);
            } else {
                nVar.a(next.f5045e, next.f5046f);
                nVar.a(next.f5047g, next.f5048h);
            }
            this.f5033a.add(nVar);
            c0Var.b(next, nVar);
        }
        Iterator<d.b> it2 = dVar.f5040b.iterator();
        while (it2.hasNext()) {
            d.b next2 = it2.next();
            int i2 = next2.l;
            int i3 = next2.m;
            b bVar = new b((n) c0Var.b(next2.f5049a), next2.f5058j, next2.f5059k, next2.f5056h ? i3 : i2, next2.f5056h ? i2 : i3);
            bVar.f5035h = next2.f5050b;
            bVar.f5036i = next2.f5051c;
            bVar.f5037j = next2.f5052d;
            bVar.f5038k = next2.f5053e;
            bVar.o = next2.f5055g;
            bVar.n = next2.f5054f;
            bVar.p = next2.f5056h;
            bVar.q = next2.f5057i;
            bVar.r = next2.o;
            bVar.s = next2.p;
            if (next2.n) {
                bVar.a(false, true);
            }
            this.f5034b.add(bVar);
        }
    }

    public b b(String str) {
        int i2 = this.f5034b.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            if (this.f5034b.get(i3).f5036i.equals(str)) {
                return this.f5034b.get(i3);
            }
        }
        return null;
    }

    public com.badlogic.gdx.utils.a<b> c(String str) {
        com.badlogic.gdx.utils.a<b> aVar = new com.badlogic.gdx.utils.a<>(b.class);
        int i2 = this.f5034b.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            b bVar = this.f5034b.get(i3);
            if (bVar.f5036i.equals(str)) {
                aVar.add(new b(bVar));
            }
        }
        return aVar;
    }

    public void dispose() {
        d0.a<n> it = this.f5033a.iterator();
        while (it.hasNext()) {
            ((n) it.next()).dispose();
        }
        this.f5033a.c(0);
    }

    public com.badlogic.gdx.utils.a<b> j() {
        return this.f5034b;
    }

    public d0<n> k() {
        return this.f5033a;
    }

    static String b(BufferedReader bufferedReader) throws IOException {
        String readLine = bufferedReader.readLine();
        int indexOf = readLine.indexOf(58);
        if (indexOf != -1) {
            return readLine.substring(indexOf + 1).trim();
        }
        throw new o("Invalid line: " + readLine);
    }

    public q(e.d.b.s.a aVar) {
        this(aVar, aVar.i());
    }

    /* compiled from: TextureAtlas */
    public static class b extends r {

        /* renamed from: h  reason: collision with root package name */
        public int f5035h;

        /* renamed from: i  reason: collision with root package name */
        public String f5036i;

        /* renamed from: j  reason: collision with root package name */
        public float f5037j;

        /* renamed from: k  reason: collision with root package name */
        public float f5038k;
        public int l;
        public int m;
        public int n;
        public int o;
        public boolean p;
        public int q;
        public int[] r;
        public int[] s;

        public b(n nVar, int i2, int i3, int i4, int i5) {
            super(nVar, i2, i3, i4, i5);
            this.n = i4;
            this.o = i5;
            this.l = i4;
            this.m = i5;
        }

        public void a(boolean z, boolean z2) {
            super.a(z, z2);
            if (z) {
                this.f5037j = (((float) this.n) - this.f5037j) - m();
            }
            if (z2) {
                this.f5038k = (((float) this.o) - this.f5038k) - l();
            }
        }

        public float l() {
            return (float) (this.p ? this.l : this.m);
        }

        public float m() {
            return (float) (this.p ? this.m : this.l);
        }

        public String toString() {
            return this.f5036i;
        }

        public b(b bVar) {
            a(bVar);
            this.f5035h = bVar.f5035h;
            this.f5036i = bVar.f5036i;
            this.f5037j = bVar.f5037j;
            this.f5038k = bVar.f5038k;
            this.l = bVar.l;
            this.m = bVar.m;
            this.n = bVar.n;
            this.o = bVar.o;
            this.p = bVar.p;
            this.q = bVar.q;
            this.r = bVar.r;
        }
    }

    public q(e.d.b.s.a aVar, e.d.b.s.a aVar2) {
        this(aVar, aVar2, false);
    }

    public q(e.d.b.s.a aVar, e.d.b.s.a aVar2, boolean z) {
        this(new d(aVar, aVar2, z));
    }

    public q(d dVar) {
        this.f5033a = new d0<>(4);
        this.f5034b = new com.badlogic.gdx.utils.a<>();
        if (dVar != null) {
            a(dVar);
        }
    }

    public o a(String str) {
        int i2 = this.f5034b.f5374b;
        for (int i3 = 0; i3 < i2; i3++) {
            if (this.f5034b.get(i3).f5036i.equals(str)) {
                return a(this.f5034b.get(i3));
            }
        }
        return null;
    }

    private o a(b bVar) {
        if (bVar.l != bVar.n || bVar.m != bVar.o) {
            return new c(bVar);
        }
        if (!bVar.p) {
            return new o(bVar);
        }
        o oVar = new o(bVar);
        oVar.b(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, (float) bVar.a(), (float) bVar.b());
        oVar.a(true);
        return oVar;
    }

    static int a(BufferedReader bufferedReader) throws IOException {
        int indexOf;
        String readLine = bufferedReader.readLine();
        int indexOf2 = readLine.indexOf(58);
        if (indexOf2 != -1) {
            int i2 = indexOf2 + 1;
            int i3 = 0;
            while (i3 < 3 && (indexOf = readLine.indexOf(44, i2)) != -1) {
                f5031c[i3] = readLine.substring(i2, indexOf).trim();
                i2 = indexOf + 1;
                i3++;
            }
            f5031c[i3] = readLine.substring(i2).trim();
            return i3 + 1;
        }
        throw new o("Invalid line: " + readLine);
    }
}
