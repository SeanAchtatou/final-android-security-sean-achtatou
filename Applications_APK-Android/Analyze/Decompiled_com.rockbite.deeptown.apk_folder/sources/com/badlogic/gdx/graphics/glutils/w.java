package com.badlogic.gdx.graphics.glutils;

import com.badlogic.gdx.utils.BufferUtils;
import e.d.b.g;
import e.d.b.t.r;
import e.d.b.t.s;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

/* compiled from: VertexBufferObjectSubData */
public class w implements y {

    /* renamed from: a  reason: collision with root package name */
    final s f5213a;

    /* renamed from: b  reason: collision with root package name */
    final FloatBuffer f5214b;

    /* renamed from: c  reason: collision with root package name */
    final ByteBuffer f5215c;

    /* renamed from: d  reason: collision with root package name */
    int f5216d;

    /* renamed from: e  reason: collision with root package name */
    final boolean f5217e;

    /* renamed from: f  reason: collision with root package name */
    final int f5218f;

    /* renamed from: g  reason: collision with root package name */
    boolean f5219g = false;

    /* renamed from: h  reason: collision with root package name */
    boolean f5220h = false;

    public w(boolean z, int i2, s sVar) {
        this.f5213a = sVar;
        this.f5215c = BufferUtils.a(this.f5213a.f7013b * i2);
        this.f5217e = true;
        this.f5218f = z ? 35044 : 35048;
        this.f5214b = this.f5215c.asFloatBuffer();
        this.f5216d = f();
        this.f5214b.flip();
        this.f5215c.flip();
    }

    private void e() {
        if (this.f5220h) {
            g.f6807h.a(34962, 0, this.f5215c.limit(), this.f5215c);
            this.f5219g = false;
        }
    }

    private int f() {
        int a2 = g.f6807h.a();
        g.f6807h.e(34962, a2);
        g.f6807h.a(34962, this.f5215c.capacity(), (Buffer) null, this.f5218f);
        g.f6807h.e(34962, 0);
        return a2;
    }

    public FloatBuffer a() {
        this.f5219g = true;
        return this.f5214b;
    }

    public void b(s sVar, int[] iArr) {
        e.d.b.t.g gVar = g.f6807h;
        int size = this.f5213a.size();
        if (iArr == null) {
            for (int i2 = 0; i2 < size; i2++) {
                sVar.a(this.f5213a.get(i2).f7009f);
            }
        } else {
            for (int i3 = 0; i3 < size; i3++) {
                int i4 = iArr[i3];
                if (i4 >= 0) {
                    sVar.a(i4);
                }
            }
        }
        gVar.e(34962, 0);
        this.f5220h = false;
    }

    public int c() {
        return (this.f5214b.limit() * 4) / this.f5213a.f7013b;
    }

    public s d() {
        return this.f5213a;
    }

    public void dispose() {
        e.d.b.t.g gVar = g.f6807h;
        gVar.e(34962, 0);
        gVar.e(this.f5216d);
        this.f5216d = 0;
    }

    public void a(float[] fArr, int i2, int i3) {
        this.f5219g = true;
        if (this.f5217e) {
            BufferUtils.a(fArr, this.f5215c, i3, i2);
            this.f5214b.position(0);
            this.f5214b.limit(i3);
        } else {
            this.f5214b.clear();
            this.f5214b.put(fArr, i2, i3);
            this.f5214b.flip();
            this.f5215c.position(0);
            this.f5215c.limit(this.f5214b.limit() << 2);
        }
        e();
    }

    public void b() {
        this.f5216d = f();
        this.f5219g = true;
    }

    public void a(s sVar, int[] iArr) {
        e.d.b.t.g gVar = g.f6807h;
        gVar.e(34962, this.f5216d);
        int i2 = 0;
        if (this.f5219g) {
            this.f5215c.limit(this.f5214b.limit() * 4);
            gVar.a(34962, this.f5215c.limit(), this.f5215c, this.f5218f);
            this.f5219g = false;
        }
        int size = this.f5213a.size();
        if (iArr == null) {
            while (i2 < size) {
                r rVar = this.f5213a.get(i2);
                int b2 = sVar.b(rVar.f7009f);
                if (b2 >= 0) {
                    sVar.b(b2);
                    sVar.a(b2, rVar.f7005b, rVar.f7007d, rVar.f7006c, this.f5213a.f7013b, rVar.f7008e);
                }
                i2++;
            }
        } else {
            while (i2 < size) {
                r rVar2 = this.f5213a.get(i2);
                int i3 = iArr[i2];
                if (i3 >= 0) {
                    sVar.b(i3);
                    sVar.a(i3, rVar2.f7005b, rVar2.f7007d, rVar2.f7006c, this.f5213a.f7013b, rVar2.f7008e);
                }
                i2++;
            }
        }
        this.f5220h = true;
    }
}
