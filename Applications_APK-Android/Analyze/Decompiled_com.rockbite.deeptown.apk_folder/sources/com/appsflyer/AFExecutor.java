package com.appsflyer;

import android.content.Context;
import android.net.TrafficStats;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import b.e.e.a;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class AFExecutor {

    /* renamed from: ॱ  reason: contains not printable characters */
    private static AFExecutor f0;

    /* renamed from: ˊ  reason: contains not printable characters */
    public final ThreadFactory f1 = new AnonymousClass2();

    /* renamed from: ˋ  reason: contains not printable characters */
    public Executor f2;

    /* renamed from: ˎ  reason: contains not printable characters */
    public ScheduledExecutorService f3;

    /* renamed from: ˏ  reason: contains not printable characters */
    public ScheduledExecutorService f4;

    /* renamed from: com.appsflyer.AFExecutor$2  reason: invalid class name */
    class AnonymousClass2 implements ThreadFactory {

        /* renamed from: com.appsflyer.AFExecutor$2$1  reason: invalid class name */
        public class AnonymousClass1 implements Runnable {

            /* renamed from: ˏ  reason: contains not printable characters */
            private /* synthetic */ Runnable f7;

            AnonymousClass1(Runnable runnable) {
                this.f7 = runnable;
            }

            /* renamed from: ˋ  reason: contains not printable characters */
            public static boolean m3(Context context, String str) {
                int a2 = a.a(context, str);
                StringBuilder sb = new StringBuilder("is Permission Available: ");
                sb.append(str);
                sb.append("; res: ");
                sb.append(a2);
                AFLogger.afRDLog(sb.toString());
                return a2 == 0;
            }

            public final void run() {
                if (Build.VERSION.SDK_INT >= 14) {
                    TrafficStats.setThreadStatsTag("AppsFlyer".hashCode());
                }
                this.f7.run();
            }

            AnonymousClass1() {
            }
        }

        AnonymousClass2() {
        }

        /* renamed from: ˋ  reason: contains not printable characters */
        static Map<String, String> m2(Context context) {
            HashMap hashMap = new HashMap();
            try {
                DisplayMetrics displayMetrics = new DisplayMetrics();
                ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
                hashMap.put("x_px", String.valueOf(displayMetrics.widthPixels));
                hashMap.put("y_px", String.valueOf(displayMetrics.heightPixels));
                hashMap.put("d_dpi", String.valueOf(displayMetrics.densityDpi));
                hashMap.put("size", String.valueOf(context.getResources().getConfiguration().screenLayout & 15));
                hashMap.put("xdp", String.valueOf(displayMetrics.xdpi));
                hashMap.put("ydp", String.valueOf(displayMetrics.ydpi));
            } catch (Throwable th) {
                AFLogger.afErrorLog("Couldn't aggregate screen stats: ", th);
            }
            return hashMap;
        }

        public final Thread newThread(Runnable runnable) {
            return new Thread(new AnonymousClass1(runnable));
        }

        AnonymousClass2() {
        }
    }

    private AFExecutor() {
    }

    public static AFExecutor getInstance() {
        if (f0 == null) {
            f0 = new AFExecutor();
        }
        return f0;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public static void m0(ExecutorService executorService) {
        try {
            AFLogger.afRDLog("shut downing executor ...");
            executorService.shutdown();
            executorService.awaitTermination(10, TimeUnit.SECONDS);
            if (!executorService.isTerminated()) {
                AFLogger.afRDLog("killing non-finished tasks");
            }
            executorService.shutdownNow();
        } catch (InterruptedException unused) {
            AFLogger.afRDLog("InterruptedException!!!");
            if (!executorService.isTerminated()) {
                AFLogger.afRDLog("killing non-finished tasks");
            }
            executorService.shutdownNow();
        } catch (Throwable th) {
            if (!executorService.isTerminated()) {
                AFLogger.afRDLog("killing non-finished tasks");
            }
            executorService.shutdownNow();
            throw th;
        }
    }

    public Executor getThreadPoolExecutor() {
        Executor executor = this.f2;
        if (executor == null || ((executor instanceof ThreadPoolExecutor) && (((ThreadPoolExecutor) executor).isShutdown() || ((ThreadPoolExecutor) this.f2).isTerminated() || ((ThreadPoolExecutor) this.f2).isTerminating()))) {
            if (Build.VERSION.SDK_INT < 11) {
                return Executors.newSingleThreadExecutor();
            }
            this.f2 = Executors.newFixedThreadPool(2, this.f1);
        }
        return this.f2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final ScheduledThreadPoolExecutor m1() {
        ScheduledExecutorService scheduledExecutorService = this.f4;
        if (scheduledExecutorService == null || scheduledExecutorService.isShutdown() || this.f4.isTerminated()) {
            this.f4 = Executors.newScheduledThreadPool(2, this.f1);
        }
        return (ScheduledThreadPoolExecutor) this.f4;
    }
}
