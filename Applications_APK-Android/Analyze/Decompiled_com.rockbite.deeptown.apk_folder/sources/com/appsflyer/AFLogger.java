package com.appsflyer;

import android.util.Log;
import com.appsflyer.internal.aa;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class AFLogger {

    /* renamed from: ˋ  reason: contains not printable characters */
    private static long f21 = System.currentTimeMillis();

    public enum LogLevel {
        NONE(0),
        ERROR(1),
        WARNING(2),
        INFO(3),
        DEBUG(4),
        VERBOSE(5);
        

        /* renamed from: ˊ  reason: contains not printable characters */
        private int f23;

        private LogLevel(int i2) {
            this.f23 = i2;
        }

        public final int getLevel() {
            return this.f23;
        }
    }

    public static void afDebugLog(String str) {
        if (LogLevel.DEBUG.getLevel() <= AppsFlyerProperties.getInstance().getInt("logLevel", LogLevel.NONE.getLevel())) {
            Log.d(AppsFlyerLibCore.LOG_TAG, m14(str, false));
        }
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108(null, "D", m14(str, true));
    }

    public static void afErrorLog(String str, Throwable th) {
        m13(str, th, false);
    }

    public static void afInfoLog(String str, boolean z) {
        if (LogLevel.INFO.getLevel() <= AppsFlyerProperties.getInstance().getInt("logLevel", LogLevel.NONE.getLevel())) {
            Log.i(AppsFlyerLibCore.LOG_TAG, m14(str, false));
        }
        if (z) {
            if (aa.f104 == null) {
                aa.f104 = new aa();
            }
            aa.f104.m108(null, "I", m14(str, true));
        }
    }

    public static void afRDLog(String str) {
        if (LogLevel.VERBOSE.getLevel() <= AppsFlyerProperties.getInstance().getInt("logLevel", LogLevel.NONE.getLevel())) {
            Log.v(AppsFlyerLibCore.LOG_TAG, m14(str, false));
        }
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108(null, "V", m14(str, true));
    }

    public static void afWarnLog(String str) {
        m12(str);
    }

    public static void resetDeltaTime() {
        f21 = System.currentTimeMillis();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static void m13(String str, Throwable th, boolean z) {
        String[] strArr;
        if ((LogLevel.ERROR.getLevel() <= AppsFlyerProperties.getInstance().getInt("logLevel", LogLevel.NONE.getLevel())) && z) {
            Log.e(AppsFlyerLibCore.LOG_TAG, m14(str, false), th);
        }
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa aaVar = aa.f104;
        Throwable cause = th.getCause();
        String simpleName = th.getClass().getSimpleName();
        String message = cause == null ? th.getMessage() : cause.getMessage();
        StackTraceElement[] stackTrace = cause == null ? th.getStackTrace() : cause.getStackTrace();
        if (stackTrace == null) {
            strArr = new String[]{message};
        } else {
            String[] strArr2 = new String[(stackTrace.length + 1)];
            strArr2[0] = message;
            for (int i2 = 1; i2 < stackTrace.length; i2++) {
                strArr2[i2] = stackTrace[i2].toString();
            }
            strArr = strArr2;
        }
        aaVar.m108("exception", simpleName, strArr);
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private static String m14(String str, boolean z) {
        if (!z && LogLevel.VERBOSE.getLevel() > AppsFlyerProperties.getInstance().getInt("logLevel", LogLevel.NONE.getLevel())) {
            return str;
        }
        StringBuilder sb = new StringBuilder("(");
        sb.append(m11(System.currentTimeMillis() - f21));
        sb.append(") [");
        sb.append(Thread.currentThread().getName());
        sb.append("] ");
        sb.append(str);
        return sb.toString();
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    static void m15(String str) {
        if (!AppsFlyerProperties.getInstance().isLogsDisabledCompletely()) {
            Log.d(AppsFlyerLibCore.LOG_TAG, m14(str, false));
        }
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108(null, "F", str);
    }

    public static void afErrorLog(String str, Throwable th, boolean z) {
        m13(str, th, z);
    }

    public static void afInfoLog(String str) {
        afInfoLog(str, true);
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    static void m12(String str) {
        if (LogLevel.WARNING.getLevel() <= AppsFlyerProperties.getInstance().getInt("logLevel", LogLevel.NONE.getLevel())) {
            Log.w(AppsFlyerLibCore.LOG_TAG, m14(str, false));
        }
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108(null, "W", m14(str, true));
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static String m11(long j2) {
        long hours = TimeUnit.MILLISECONDS.toHours(j2);
        long millis = j2 - TimeUnit.HOURS.toMillis(hours);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
        long millis2 = millis - TimeUnit.MINUTES.toMillis(minutes);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis2);
        long millis3 = TimeUnit.MILLISECONDS.toMillis(millis2 - TimeUnit.SECONDS.toMillis(seconds));
        return String.format(Locale.getDefault(), "%02d:%02d:%02d:%03d", Long.valueOf(hours), Long.valueOf(minutes), Long.valueOf(seconds), Long.valueOf(millis3));
    }
}
