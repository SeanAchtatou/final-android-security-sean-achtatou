package com.appsflyer;

import android.app.Activity;
import android.app.Application;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.os.Process;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import com.appsflyer.AFExecutor;
import com.appsflyer.AFFacebookDeferredDeeplink;
import com.appsflyer.AFLogger;
import com.appsflyer.AppsFlyerProperties;
import com.appsflyer.OneLinkHttpTask;
import com.appsflyer.internal.aa;
import com.appsflyer.internal.ab;
import com.appsflyer.internal.ac;
import com.appsflyer.internal.ad;
import com.appsflyer.internal.b;
import com.appsflyer.internal.f;
import com.appsflyer.internal.g;
import com.appsflyer.internal.h;
import com.appsflyer.internal.k;
import com.appsflyer.internal.l;
import com.appsflyer.internal.m;
import com.appsflyer.internal.p;
import com.appsflyer.internal.q;
import com.appsflyer.internal.r;
import com.appsflyer.internal.s;
import com.appsflyer.internal.u;
import com.appsflyer.internal.v;
import com.appsflyer.internal.w;
import com.appsflyer.internal.x;
import com.appsflyer.internal.y;
import com.appsflyer.share.Constants;
import com.facebook.internal.NativeProtocol;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.remoteconfig.RemoteConfigConstants;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TapjoyConstants;
import com.underwater.demolisher.data.vo.RemoteConfigConst;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.NetworkInterface;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AppsFlyerLibCore extends AppsFlyerLib implements k {
    public static final String AF_PRE_INSTALL_PATH = "AF_PRE_INSTALL_PATH";
    public static final String IS_STOP_TRACKING_USED = "is_stop_tracking_used";
    public static final String LOG_TAG = "AppsFlyer_4.10.3";
    public static final String PRE_INSTALL_SYSTEM_DEFAULT = "/data/local/tmp/pre_install.appsflyer";
    public static final String PRE_INSTALL_SYSTEM_DEFAULT_ETC = "/etc/pre_install.appsflyer";
    public static final String PRE_INSTALL_SYSTEM_RO_PROP = "ro.appsflyer.preinstall.path";

    /* renamed from: ʻ  reason: contains not printable characters */
    private static String f24 = null;

    /* renamed from: ʼ  reason: contains not printable characters */
    private static final String f25;

    /* renamed from: ˊ  reason: contains not printable characters */
    public static AppsFlyerInAppPurchaseValidatorListener f26 = null;

    /* renamed from: ˊॱ  reason: contains not printable characters */
    private static final List<String> f27 = Arrays.asList("is_cache");

    /* renamed from: ˋॱ  reason: contains not printable characters */
    private static AppsFlyerLibCore f28 = new AppsFlyerLibCore();

    /* renamed from: ˎ  reason: contains not printable characters */
    public static final String f29 = "4";

    /* renamed from: ˏ  reason: contains not printable characters */
    public static final String f30;
    /* access modifiers changed from: private */

    /* renamed from: ˏॱ  reason: contains not printable characters */
    public static AppsFlyerConversionListener f31 = null;

    /* renamed from: ͺ  reason: contains not printable characters */
    private static String f32;
    /* access modifiers changed from: private */

    /* renamed from: ॱˊ  reason: contains not printable characters */
    public static final List<String> f33 = Arrays.asList("googleplay", "playstore", "googleplaystore");

    /* renamed from: ᐝ  reason: contains not printable characters */
    private static String f34;
    public Uri latestDeepLink = null;

    /* renamed from: ʻॱ  reason: contains not printable characters */
    private long f35 = TimeUnit.SECONDS.toMillis(5);
    /* access modifiers changed from: private */

    /* renamed from: ʼॱ  reason: contains not printable characters */
    public boolean f36 = false;

    /* renamed from: ʽ  reason: contains not printable characters */
    long f37;
    /* access modifiers changed from: private */

    /* renamed from: ʽॱ  reason: contains not printable characters */
    public Map<String, String> f38;

    /* renamed from: ʾ  reason: contains not printable characters */
    private u.a f39;
    /* access modifiers changed from: private */

    /* renamed from: ʿ  reason: contains not printable characters */
    public long f40;
    /* access modifiers changed from: private */

    /* renamed from: ˈ  reason: contains not printable characters */
    public ScheduledExecutorService f41 = null;

    /* renamed from: ˉ  reason: contains not printable characters */
    private long f42;

    /* renamed from: ˊˊ  reason: contains not printable characters */
    private String f43;

    /* renamed from: ˊˋ  reason: contains not printable characters */
    private boolean f44 = false;

    /* renamed from: ˊᐝ  reason: contains not printable characters */
    private boolean f45 = false;

    /* renamed from: ˋ  reason: contains not printable characters */
    public String f46;

    /* renamed from: ˋˊ  reason: contains not printable characters */
    private Map<Long, String> f47;

    /* renamed from: ˋˋ  reason: contains not printable characters */
    private boolean f48;

    /* renamed from: ˋᐝ  reason: contains not printable characters */
    private ac f49 = new ac();

    /* renamed from: ˌ  reason: contains not printable characters */
    private boolean f50 = false;

    /* renamed from: ˍ  reason: contains not printable characters */
    private boolean f51;

    /* renamed from: ˎˎ  reason: contains not printable characters */
    private boolean f52 = false;

    /* renamed from: ˎˏ  reason: contains not printable characters */
    private boolean f53 = false;
    /* access modifiers changed from: private */

    /* renamed from: ˏˎ  reason: contains not printable characters */
    public JSONObject f54;

    /* renamed from: ˏˏ  reason: contains not printable characters */
    private String f55;

    /* renamed from: ˑ  reason: contains not printable characters */
    private Application f56;

    /* renamed from: ॱ  reason: contains not printable characters */
    public String f57;

    /* renamed from: ॱˋ  reason: contains not printable characters */
    private long f58 = -1;

    /* renamed from: ॱˎ  reason: contains not printable characters */
    private String f59;

    /* renamed from: ॱॱ  reason: contains not printable characters */
    public long f60;

    /* renamed from: ॱᐝ  reason: contains not printable characters */
    private long f61 = -1;

    /* renamed from: ᐝॱ  reason: contains not printable characters */
    private f f62 = null;

    /* renamed from: com.appsflyer.AppsFlyerLibCore$3  reason: invalid class name */
    static /* synthetic */ class AnonymousClass3 {

        /* renamed from: ॱ  reason: contains not printable characters */
        static final /* synthetic */ int[] f67 = new int[AppsFlyerProperties.EmailsCryptType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            /*
                com.appsflyer.AppsFlyerProperties$EmailsCryptType[] r0 = com.appsflyer.AppsFlyerProperties.EmailsCryptType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.appsflyer.AppsFlyerLibCore.AnonymousClass3.f67 = r0
                int[] r0 = com.appsflyer.AppsFlyerLibCore.AnonymousClass3.f67     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.appsflyer.AppsFlyerProperties$EmailsCryptType r1 = com.appsflyer.AppsFlyerProperties.EmailsCryptType.SHA1     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.appsflyer.AppsFlyerLibCore.AnonymousClass3.f67     // Catch:{ NoSuchFieldError -> 0x001f }
                com.appsflyer.AppsFlyerProperties$EmailsCryptType r1 = com.appsflyer.AppsFlyerProperties.EmailsCryptType.MD5     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.appsflyer.AppsFlyerLibCore.AnonymousClass3.f67     // Catch:{ NoSuchFieldError -> 0x002a }
                com.appsflyer.AppsFlyerProperties$EmailsCryptType r1 = com.appsflyer.AppsFlyerProperties.EmailsCryptType.SHA256     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.appsflyer.AppsFlyerLibCore.AnonymousClass3.f67     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.appsflyer.AppsFlyerProperties$EmailsCryptType r1 = com.appsflyer.AppsFlyerProperties.EmailsCryptType.NONE     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.appsflyer.AppsFlyerLibCore.AnonymousClass3.<clinit>():void");
        }
    }

    abstract class a implements Runnable {

        /* renamed from: ˊ  reason: contains not printable characters */
        private AtomicInteger f73 = new AtomicInteger(0);

        /* renamed from: ˋ  reason: contains not printable characters */
        private ScheduledExecutorService f74;

        /* renamed from: ˎ  reason: contains not printable characters */
        WeakReference<Context> f75 = null;

        /* renamed from: ॱ  reason: contains not printable characters */
        private String f77;

        a(Context context, String str, ScheduledExecutorService scheduledExecutorService) {
            this.f75 = new WeakReference<>(context);
            this.f77 = str;
            if (scheduledExecutorService == null) {
                this.f74 = AFExecutor.getInstance().m1();
            } else {
                this.f74 = scheduledExecutorService;
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:77:0x024a, code lost:
            if (r8 == null) goto L_0x026f;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:89:0x026f, code lost:
            r1.f74.shutdown();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:90:0x0274, code lost:
            return;
         */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00a4 A[Catch:{ all -> 0x024f }] */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x0104 A[Catch:{ q -> 0x020e, all -> 0x024d }] */
        /* JADX WARNING: Removed duplicated region for block: B:34:0x0125 A[Catch:{ q -> 0x020e, all -> 0x024d }] */
        /* JADX WARNING: Removed duplicated region for block: B:72:0x0219 A[Catch:{ q -> 0x020e, all -> 0x024d }] */
        /* JADX WARNING: Removed duplicated region for block: B:84:0x0257 A[Catch:{ all -> 0x0275 }] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r17 = this;
                r1 = r17
                java.lang.String r0 = "is_first_launch"
                java.lang.String r2 = "af_siteid"
                java.lang.String r3 = r1.f77
                if (r3 == 0) goto L_0x0281
                int r3 = r3.length()
                if (r3 != 0) goto L_0x0012
                goto L_0x0281
            L_0x0012:
                com.appsflyer.AppsFlyerLibCore r3 = com.appsflyer.AppsFlyerLibCore.this
                boolean r3 = r3.isTrackingStopped()
                if (r3 == 0) goto L_0x001b
                return
            L_0x001b:
                java.util.concurrent.atomic.AtomicInteger r3 = r1.f73
                r3.incrementAndGet()
                r3 = 0
                r4 = 0
                java.lang.ref.WeakReference<android.content.Context> r5 = r1.f75     // Catch:{ all -> 0x024f }
                java.lang.Object r5 = r5.get()     // Catch:{ all -> 0x024f }
                android.content.Context r5 = (android.content.Context) r5     // Catch:{ all -> 0x024f }
                if (r5 != 0) goto L_0x0032
                java.util.concurrent.atomic.AtomicInteger r0 = r1.f73
                r0.decrementAndGet()
                return
            L_0x0032:
                long r6 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x024f }
                java.lang.ref.WeakReference r8 = new java.lang.ref.WeakReference     // Catch:{ all -> 0x024f }
                r8.<init>(r5)     // Catch:{ all -> 0x024f }
                java.lang.String r8 = com.appsflyer.AppsFlyerLibCore.m64(r8)     // Catch:{ all -> 0x024f }
                java.lang.String r8 = com.appsflyer.AppsFlyerLibCore.m19(r5, r8)     // Catch:{ all -> 0x024f }
                java.lang.String r9 = ""
                r10 = 1
                if (r8 == 0) goto L_0x006e
                java.util.List r11 = com.appsflyer.AppsFlyerLibCore.f33     // Catch:{ all -> 0x024f }
                java.lang.String r12 = r8.toLowerCase()     // Catch:{ all -> 0x024f }
                boolean r11 = r11.contains(r12)     // Catch:{ all -> 0x024f }
                if (r11 != 0) goto L_0x0061
                java.lang.String r11 = "-"
                java.lang.String r8 = java.lang.String.valueOf(r8)     // Catch:{ all -> 0x024f }
                java.lang.String r8 = r11.concat(r8)     // Catch:{ all -> 0x024f }
                goto L_0x006f
            L_0x0061:
                java.lang.String r11 = "AF detected using redundant Google-Play channel for attribution - %s. Using without channel postfix."
                java.lang.Object[] r12 = new java.lang.Object[r10]     // Catch:{ all -> 0x024f }
                r12[r4] = r8     // Catch:{ all -> 0x024f }
                java.lang.String r8 = java.lang.String.format(r11, r12)     // Catch:{ all -> 0x024f }
                com.appsflyer.AFLogger.afWarnLog(r8)     // Catch:{ all -> 0x024f }
            L_0x006e:
                r8 = r9
            L_0x006f:
                java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x024f }
                r11.<init>()     // Catch:{ all -> 0x024f }
                java.lang.String r12 = r17.m85()     // Catch:{ all -> 0x024f }
                r11.append(r12)     // Catch:{ all -> 0x024f }
                java.lang.String r12 = r5.getPackageName()     // Catch:{ all -> 0x024f }
                r11.append(r12)     // Catch:{ all -> 0x024f }
                r11.append(r8)     // Catch:{ all -> 0x024f }
                java.lang.String r8 = "?devkey="
                r11.append(r8)     // Catch:{ all -> 0x024f }
                java.lang.String r8 = r1.f77     // Catch:{ all -> 0x024f }
                r11.append(r8)     // Catch:{ all -> 0x024f }
                java.lang.String r8 = "&device_id="
                r11.append(r8)     // Catch:{ all -> 0x024f }
                java.lang.ref.WeakReference r8 = new java.lang.ref.WeakReference     // Catch:{ all -> 0x024f }
                r8.<init>(r5)     // Catch:{ all -> 0x024f }
                java.lang.String r8 = com.appsflyer.internal.ab.m111(r8)     // Catch:{ all -> 0x024f }
                r11.append(r8)     // Catch:{ all -> 0x024f }
                com.appsflyer.internal.aa r8 = com.appsflyer.internal.aa.f104     // Catch:{ all -> 0x024f }
                if (r8 != 0) goto L_0x00ab
                com.appsflyer.internal.aa r8 = new com.appsflyer.internal.aa     // Catch:{ all -> 0x024f }
                r8.<init>()     // Catch:{ all -> 0x024f }
                com.appsflyer.internal.aa.f104 = r8     // Catch:{ all -> 0x024f }
            L_0x00ab:
                com.appsflyer.internal.aa r8 = com.appsflyer.internal.aa.f104     // Catch:{ all -> 0x024f }
                java.lang.String r12 = r11.toString()     // Catch:{ all -> 0x024f }
                java.lang.String r13 = "server_request"
                java.lang.String[] r14 = new java.lang.String[r10]     // Catch:{ all -> 0x024f }
                r14[r4] = r9     // Catch:{ all -> 0x024f }
                r8.m108(r13, r12, r14)     // Catch:{ all -> 0x024f }
                java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x024f }
                java.lang.String r9 = "Calling server for attribution url: "
                r8.<init>(r9)     // Catch:{ all -> 0x024f }
                java.lang.String r9 = r11.toString()     // Catch:{ all -> 0x024f }
                r8.append(r9)     // Catch:{ all -> 0x024f }
                java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x024f }
                com.appsflyer.AFFacebookDeferredDeeplink.AnonymousClass2.m5(r8)     // Catch:{ all -> 0x024f }
                java.net.URL r8 = new java.net.URL     // Catch:{ all -> 0x024f }
                java.lang.String r9 = r11.toString()     // Catch:{ all -> 0x024f }
                r8.<init>(r9)     // Catch:{ all -> 0x024f }
                java.net.URLConnection r8 = r8.openConnection()     // Catch:{ all -> 0x024f }
                java.lang.Object r8 = com.google.firebase.perf.network.FirebasePerfUrlConnection.instrument(r8)     // Catch:{ all -> 0x024f }
                java.net.URLConnection r8 = (java.net.URLConnection) r8     // Catch:{ all -> 0x024f }
                java.net.HttpURLConnection r8 = (java.net.HttpURLConnection) r8     // Catch:{ all -> 0x024f }
                java.lang.String r3 = "GET"
                r8.setRequestMethod(r3)     // Catch:{ all -> 0x024d }
                r3 = 10000(0x2710, float:1.4013E-41)
                r8.setConnectTimeout(r3)     // Catch:{ all -> 0x024d }
                java.lang.String r3 = "Connection"
                java.lang.String r9 = "close"
                r8.setRequestProperty(r3, r9)     // Catch:{ all -> 0x024d }
                r8.connect()     // Catch:{ all -> 0x024d }
                int r3 = r8.getResponseCode()     // Catch:{ all -> 0x024d }
                java.lang.String r9 = com.appsflyer.AppsFlyerLibCore.m65(r8)     // Catch:{ all -> 0x024d }
                com.appsflyer.internal.aa r12 = com.appsflyer.internal.aa.f104     // Catch:{ all -> 0x024d }
                if (r12 != 0) goto L_0x010b
                com.appsflyer.internal.aa r12 = new com.appsflyer.internal.aa     // Catch:{ all -> 0x024d }
                r12.<init>()     // Catch:{ all -> 0x024d }
                com.appsflyer.internal.aa.f104 = r12     // Catch:{ all -> 0x024d }
            L_0x010b:
                com.appsflyer.internal.aa r12 = com.appsflyer.internal.aa.f104     // Catch:{ all -> 0x024d }
                java.lang.String r13 = r11.toString()     // Catch:{ all -> 0x024d }
                java.lang.String r14 = "server_response"
                r15 = 2
                java.lang.String[] r15 = new java.lang.String[r15]     // Catch:{ all -> 0x024d }
                java.lang.String r16 = java.lang.String.valueOf(r3)     // Catch:{ all -> 0x024d }
                r15[r4] = r16     // Catch:{ all -> 0x024d }
                r15[r10] = r9     // Catch:{ all -> 0x024d }
                r12.m108(r14, r13, r15)     // Catch:{ all -> 0x024d }
                r12 = 200(0xc8, float:2.8E-43)
                if (r3 != r12) goto L_0x0219
                long r11 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x024d }
                java.lang.String r3 = "appsflyerGetConversionDataTiming"
                long r11 = r11 - r6
                com.appsflyer.AppsFlyerLibCore.m68(r5, r3, r11)     // Catch:{ all -> 0x024d }
                java.lang.String r3 = "Attribution data: "
                java.lang.String r6 = java.lang.String.valueOf(r9)     // Catch:{ all -> 0x024d }
                java.lang.String r3 = r3.concat(r6)     // Catch:{ all -> 0x024d }
                com.appsflyer.AFFacebookDeferredDeeplink.AnonymousClass2.m5(r3)     // Catch:{ all -> 0x024d }
                int r3 = r9.length()     // Catch:{ all -> 0x024d }
                if (r3 <= 0) goto L_0x0245
                if (r5 == 0) goto L_0x0245
                java.util.Map r3 = com.appsflyer.AppsFlyerLibCore.m41(r9)     // Catch:{ all -> 0x024d }
                java.lang.String r6 = "iscache"
                java.lang.Object r6 = r3.get(r6)     // Catch:{ all -> 0x024d }
                java.lang.String r6 = (java.lang.String) r6     // Catch:{ all -> 0x024d }
                if (r6 == 0) goto L_0x0165
                java.lang.String r7 = java.lang.Boolean.toString(r4)     // Catch:{ all -> 0x024d }
                boolean r7 = r7.equals(r6)     // Catch:{ all -> 0x024d }
                if (r7 == 0) goto L_0x0165
                java.lang.String r7 = "appsflyerConversionDataCacheExpiration"
                long r11 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x024d }
                com.appsflyer.AppsFlyerLibCore.m68(r5, r7, r11)     // Catch:{ all -> 0x024d }
            L_0x0165:
                boolean r7 = r3.containsKey(r2)     // Catch:{ all -> 0x024d }
                java.lang.String r11 = "[Invite] Detected App-Invite via channel: "
                java.lang.String r12 = "af_channel"
                if (r7 == 0) goto L_0x019c
                boolean r7 = r3.containsKey(r12)     // Catch:{ all -> 0x024d }
                if (r7 == 0) goto L_0x018b
                java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x024d }
                r7.<init>(r11)     // Catch:{ all -> 0x024d }
                java.lang.Object r13 = r3.get(r12)     // Catch:{ all -> 0x024d }
                java.lang.String r13 = (java.lang.String) r13     // Catch:{ all -> 0x024d }
                r7.append(r13)     // Catch:{ all -> 0x024d }
                java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x024d }
                com.appsflyer.AFLogger.afDebugLog(r7)     // Catch:{ all -> 0x024d }
                goto L_0x019c
            L_0x018b:
                java.lang.String r7 = "[CrossPromotion] App was installed via %s's Cross Promotion"
                java.lang.Object[] r13 = new java.lang.Object[r10]     // Catch:{ all -> 0x024d }
                java.lang.Object r14 = r3.get(r2)     // Catch:{ all -> 0x024d }
                r13[r4] = r14     // Catch:{ all -> 0x024d }
                java.lang.String r7 = java.lang.String.format(r7, r13)     // Catch:{ all -> 0x024d }
                com.appsflyer.AFLogger.afDebugLog(r7)     // Catch:{ all -> 0x024d }
            L_0x019c:
                boolean r2 = r3.containsKey(r2)     // Catch:{ all -> 0x024d }
                if (r2 == 0) goto L_0x01b7
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x024d }
                r2.<init>(r11)     // Catch:{ all -> 0x024d }
                java.lang.Object r7 = r3.get(r12)     // Catch:{ all -> 0x024d }
                java.lang.String r7 = (java.lang.String) r7     // Catch:{ all -> 0x024d }
                r2.append(r7)     // Catch:{ all -> 0x024d }
                java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x024d }
                com.appsflyer.AFLogger.afDebugLog(r2)     // Catch:{ all -> 0x024d }
            L_0x01b7:
                java.lang.String r2 = java.lang.Boolean.toString(r4)     // Catch:{ all -> 0x024d }
                r3.put(r0, r2)     // Catch:{ all -> 0x024d }
                org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ all -> 0x024d }
                r2.<init>(r3)     // Catch:{ all -> 0x024d }
                java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x024d }
                java.lang.String r7 = "attributionId"
                if (r2 == 0) goto L_0x01cf
                com.appsflyer.AppsFlyerLibCore.m28(r5, r7, r2)     // Catch:{ all -> 0x024d }
                goto L_0x01d2
            L_0x01cf:
                com.appsflyer.AppsFlyerLibCore.m28(r5, r7, r9)     // Catch:{ all -> 0x024d }
            L_0x01d2:
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x024d }
                java.lang.String r7 = "iscache="
                r2.<init>(r7)     // Catch:{ all -> 0x024d }
                r2.append(r6)     // Catch:{ all -> 0x024d }
                java.lang.String r6 = " caching conversion data"
                r2.append(r6)     // Catch:{ all -> 0x024d }
                java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x024d }
                com.appsflyer.AFLogger.afDebugLog(r2)     // Catch:{ all -> 0x024d }
                com.appsflyer.AppsFlyerConversionListener r2 = com.appsflyer.AppsFlyerLibCore.f31     // Catch:{ all -> 0x024d }
                if (r2 == 0) goto L_0x0245
                java.util.concurrent.atomic.AtomicInteger r2 = r1.f73     // Catch:{ all -> 0x024d }
                int r2 = r2.intValue()     // Catch:{ all -> 0x024d }
                if (r2 > r10) goto L_0x0245
                java.util.Map r2 = com.appsflyer.AppsFlyerLibCore.m40(r5)     // Catch:{ q -> 0x020e }
                android.content.SharedPreferences r5 = com.appsflyer.AppsFlyerLibCore.m49(r5)     // Catch:{ q -> 0x020e }
                java.lang.String r6 = "sixtyDayConversionData"
                boolean r5 = r5.getBoolean(r6, r4)     // Catch:{ q -> 0x020e }
                if (r5 != 0) goto L_0x0215
                java.lang.String r5 = java.lang.Boolean.toString(r10)     // Catch:{ q -> 0x020e }
                r2.put(r0, r5)     // Catch:{ q -> 0x020e }
                goto L_0x0215
            L_0x020e:
                r0 = move-exception
                java.lang.String r2 = "Exception while trying to fetch attribution data. "
                com.appsflyer.AFLogger.afErrorLog(r2, r0)     // Catch:{ all -> 0x024d }
                r2 = r3
            L_0x0215:
                r1.m83(r2)     // Catch:{ all -> 0x024d }
                goto L_0x0245
            L_0x0219:
                com.appsflyer.AppsFlyerConversionListener r0 = com.appsflyer.AppsFlyerLibCore.f31     // Catch:{ all -> 0x024d }
                if (r0 == 0) goto L_0x022c
                java.lang.String r0 = "Error connection to server: "
                java.lang.String r2 = java.lang.String.valueOf(r3)     // Catch:{ all -> 0x024d }
                java.lang.String r0 = r0.concat(r2)     // Catch:{ all -> 0x024d }
                r1.m84(r0, r3)     // Catch:{ all -> 0x024d }
            L_0x022c:
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x024d }
                java.lang.String r2 = "AttributionIdFetcher response code: "
                r0.<init>(r2)     // Catch:{ all -> 0x024d }
                r0.append(r3)     // Catch:{ all -> 0x024d }
                java.lang.String r2 = "  url: "
                r0.append(r2)     // Catch:{ all -> 0x024d }
                r0.append(r11)     // Catch:{ all -> 0x024d }
                java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x024d }
                com.appsflyer.AFFacebookDeferredDeeplink.AnonymousClass2.m5(r0)     // Catch:{ all -> 0x024d }
            L_0x0245:
                java.util.concurrent.atomic.AtomicInteger r0 = r1.f73
                r0.decrementAndGet()
                if (r8 == 0) goto L_0x026f
                goto L_0x026c
            L_0x024d:
                r0 = move-exception
                goto L_0x0251
            L_0x024f:
                r0 = move-exception
                r8 = r3
            L_0x0251:
                com.appsflyer.AppsFlyerConversionListener r2 = com.appsflyer.AppsFlyerLibCore.f31     // Catch:{ all -> 0x0275 }
                if (r2 == 0) goto L_0x025e
                java.lang.String r2 = r0.getMessage()     // Catch:{ all -> 0x0275 }
                r1.m84(r2, r4)     // Catch:{ all -> 0x0275 }
            L_0x025e:
                java.lang.String r2 = r0.getMessage()     // Catch:{ all -> 0x0275 }
                com.appsflyer.AFLogger.afErrorLog(r2, r0)     // Catch:{ all -> 0x0275 }
                java.util.concurrent.atomic.AtomicInteger r0 = r1.f73
                r0.decrementAndGet()
                if (r8 == 0) goto L_0x026f
            L_0x026c:
                r8.disconnect()
            L_0x026f:
                java.util.concurrent.ScheduledExecutorService r0 = r1.f74
                r0.shutdown()
                return
            L_0x0275:
                r0 = move-exception
                java.util.concurrent.atomic.AtomicInteger r2 = r1.f73
                r2.decrementAndGet()
                if (r8 == 0) goto L_0x0280
                r8.disconnect()
            L_0x0280:
                throw r0
            L_0x0281:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.appsflyer.AppsFlyerLibCore.a.run():void");
        }

        /* access modifiers changed from: protected */
        /* renamed from: ˊ  reason: contains not printable characters */
        public abstract void m83(Map<String, String> map);

        /* access modifiers changed from: protected */
        /* renamed from: ˋ  reason: contains not printable characters */
        public abstract void m84(String str, int i2);

        /* renamed from: ˎ  reason: contains not printable characters */
        public abstract String m85();
    }

    class b implements Runnable {

        /* renamed from: ˊ  reason: contains not printable characters */
        private WeakReference<Context> f78 = null;

        public b(Context context) {
            this.f78 = new WeakReference<>(context);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.appsflyer.AppsFlyerLibCore.ˏ(com.appsflyer.AppsFlyerLibCore, boolean):boolean
         arg types: [com.appsflyer.AppsFlyerLibCore, int]
         candidates:
          com.appsflyer.AppsFlyerLibCore.ˏ(android.content.Context, java.lang.String):java.util.Map<java.lang.String, java.lang.String>
          com.appsflyer.AppsFlyerLibCore.ˏ(android.content.Context, java.util.Map<java.lang.String, ? super java.lang.String>):void
          com.appsflyer.AppsFlyerLibCore.ˏ(com.appsflyer.AppsFlyerLibCore, com.appsflyer.internal.h):void
          com.appsflyer.AppsFlyerLibCore.ˏ(com.appsflyer.AppsFlyerLibCore, boolean):boolean */
        public final void run() {
            if (!AppsFlyerLibCore.this.f36) {
                long unused = AppsFlyerLibCore.this.f40 = System.currentTimeMillis();
                if (this.f78 != null) {
                    boolean unused2 = AppsFlyerLibCore.this.f36 = true;
                    try {
                        String r1 = AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.AF_KEY);
                        synchronized (this.f78) {
                            w.m193();
                            for (AFFacebookDeferredDeeplink next : w.m194(this.f78.get())) {
                                StringBuilder sb = new StringBuilder("resending request: ");
                                sb.append(next.f8);
                                AFLogger.afInfoLog(sb.toString());
                                try {
                                    long currentTimeMillis = System.currentTimeMillis();
                                    long parseLong = Long.parseLong(next.f9, 10);
                                    AppsFlyerLibCore appsFlyerLibCore = AppsFlyerLibCore.this;
                                    h hVar = new h();
                                    StringBuilder sb2 = new StringBuilder();
                                    sb2.append(next.f8);
                                    sb2.append("&isCachedRequest=true&timeincache=");
                                    sb2.append((currentTimeMillis - parseLong) / 1000);
                                    hVar.f224 = sb2.toString();
                                    hVar.f220 = next.f10;
                                    hVar.f227 = r1;
                                    hVar.f222 = this.f78;
                                    hVar.f215 = next.f9;
                                    hVar.f214 = false;
                                    AppsFlyerLibCore.m69(appsFlyerLibCore, hVar);
                                } catch (Exception e2) {
                                    AFLogger.afErrorLog("Failed to resend cached request", e2);
                                }
                            }
                        }
                    } catch (Exception e3) {
                        try {
                            AFLogger.afErrorLog("failed to check cache. ", e3);
                        } catch (Throwable th) {
                            boolean unused3 = AppsFlyerLibCore.this.f36 = false;
                            throw th;
                        }
                    }
                    boolean unused4 = AppsFlyerLibCore.this.f36 = false;
                    AppsFlyerLibCore.this.f41.shutdown();
                    ScheduledExecutorService unused5 = AppsFlyerLibCore.this.f41 = null;
                }
            }
        }
    }

    class c implements Runnable {

        /* renamed from: ˏ  reason: contains not printable characters */
        private h f81;

        /* synthetic */ c(AppsFlyerLibCore appsFlyerLibCore, h hVar, byte b2) {
            this(hVar);
        }

        public final void run() {
            AppsFlyerLibCore appsFlyerLibCore = AppsFlyerLibCore.this;
            h hVar = this.f81;
            hVar.f221 = hVar.f222.get();
            AppsFlyerLibCore.m56(appsFlyerLibCore, hVar);
        }

        private c(h hVar) {
            this.f81 = hVar;
        }
    }

    class d implements Runnable {

        /* renamed from: ˊ  reason: contains not printable characters */
        private final h f82;

        /* synthetic */ d(AppsFlyerLibCore appsFlyerLibCore, h hVar, byte b2) {
            this(hVar);
        }

        /* JADX INFO: additional move instructions added (3) to help type inference */
        /* JADX WARN: Failed to insert an additional move for type inference into block B:22:0x0062 */
        /* JADX INFO: additional move instructions added (1) to help type inference */
        /* JADX WARN: Failed to insert an additional move for type inference into block B:28:0x0076 */
        /* JADX WARN: Type inference failed for: r2v1 */
        /* JADX WARN: Type inference failed for: r2v15 */
        /* JADX WARN: Type inference failed for: r2v19, types: [java.lang.String] */
        /* JADX WARN: Type inference failed for: r2v24, types: [java.lang.Object, java.lang.String] */
        /* JADX WARN: Type inference failed for: r2v25 */
        /* JADX WARN: Type inference failed for: r2v26 */
        /* JADX WARN: Type inference failed for: r2v29 */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
          ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException} */
        /* JADX WARNING: Code restructure failed: missing block: B:40:0x008b, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:41:0x008c, code lost:
            com.appsflyer.AFLogger.afErrorLog(r0.getMessage(), r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:0x0093, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:43:0x0094, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:44:0x0095, code lost:
            r2 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:50:0x00a7, code lost:
            com.appsflyer.internal.w.m193();
            r1 = new com.appsflyer.AFFacebookDeferredDeeplink(r3, r2, com.appsflyer.BuildConfig.VERSION_NAME);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
            r2 = com.appsflyer.internal.w.m191(r5);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:53:0x00b9, code lost:
            if (r2.exists() == false) goto L_0x00bb;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:54:0x00bb, code lost:
            r2.mkdir();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:55:0x00c0, code lost:
            r2 = r2.listFiles();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:56:0x00c4, code lost:
            if (r2 == null) goto L_0x00d3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:59:0x00cb, code lost:
            android.util.Log.i(com.appsflyer.AppsFlyerLibCore.LOG_TAG, "reached cache limit, not caching request");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:60:0x00d3, code lost:
            android.util.Log.i(com.appsflyer.AppsFlyerLibCore.LOG_TAG, "caching request...");
            r2 = new java.io.File(com.appsflyer.internal.w.m191(r5), java.lang.Long.toString(java.lang.System.currentTimeMillis()));
            r2.createNewFile();
            r3 = new java.io.OutputStreamWriter(new java.io.FileOutputStream(r2.getPath(), true));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:63:?, code lost:
            r3.write("version=");
            r3.write(r1.f11);
            r3.write(10);
            r3.write("url=");
            r3.write(r1.f8);
            r3.write(10);
            r3.write("data=");
            r3.write(r1.f10);
            r3.write(10);
            r3.flush();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:65:?, code lost:
            r3.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:66:0x012d, code lost:
            r0 = th;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:67:0x012e, code lost:
            r6 = r3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:68:0x0130, code lost:
            r6 = r3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:70:0x0132, code lost:
            r0 = th;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:74:?, code lost:
            android.util.Log.i(com.appsflyer.AppsFlyerLibCore.LOG_TAG, "Could not cache request");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:75:0x013b, code lost:
            if (r6 != null) goto L_0x013d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:77:?, code lost:
            r6.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:78:0x0140, code lost:
            com.appsflyer.AFLogger.afErrorLog(r0.getMessage(), r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:80:0x0148, code lost:
            if (r6 != null) goto L_0x014a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:82:?, code lost:
            r6.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:83:0x014d, code lost:
            throw r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:87:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:88:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x008b A[ExcHandler: all (r0v10 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:19:0x005b] */
        /* JADX WARNING: Removed duplicated region for block: B:76:0x013d A[SYNTHETIC, Splitter:B:76:0x013d] */
        /* JADX WARNING: Removed duplicated region for block: B:81:0x014a A[SYNTHETIC, Splitter:B:81:0x014a] */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void run() {
            /*
                r7 = this;
                com.appsflyer.internal.h r0 = r7.f82
                java.util.Map<java.lang.String, java.lang.Object> r1 = r0.f219
                boolean r2 = r0.f214
                java.lang.String r3 = r0.f224
                int r4 = r0.f218
                android.content.Context r5 = r0.f221
                r6 = 0
                if (r5 == 0) goto L_0x0010
                goto L_0x001d
            L_0x0010:
                java.lang.ref.WeakReference<android.content.Context> r0 = r0.f222
                if (r0 == 0) goto L_0x001c
                java.lang.Object r0 = r0.get()
                r5 = r0
                android.content.Context r5 = (android.content.Context) r5
                goto L_0x001d
            L_0x001c:
                r5 = r6
            L_0x001d:
                com.appsflyer.AppsFlyerLibCore r0 = com.appsflyer.AppsFlyerLibCore.this
                boolean r0 = r0.isTrackingStopped()
                if (r0 == 0) goto L_0x0026
                return
            L_0x0026:
                if (r2 == 0) goto L_0x0051
                r0 = 2
                if (r4 > r0) goto L_0x0051
                com.appsflyer.AppsFlyerLibCore r0 = com.appsflyer.AppsFlyerLibCore.this
                boolean r0 = com.appsflyer.AppsFlyerLibCore.m38(r0)
                if (r0 == 0) goto L_0x003e
                com.appsflyer.AppsFlyerLibCore r0 = com.appsflyer.AppsFlyerLibCore.this
                java.util.Map r0 = r0.f38
                java.lang.String r2 = "rfr"
                r1.put(r2, r0)
            L_0x003e:
                com.appsflyer.AppsFlyerLibCore r0 = com.appsflyer.AppsFlyerLibCore.this
                org.json.JSONObject r0 = r0.f54
                if (r0 == 0) goto L_0x0051
                com.appsflyer.AppsFlyerLibCore r0 = com.appsflyer.AppsFlyerLibCore.this
                org.json.JSONObject r0 = r0.f54
                java.lang.String r2 = "fb_ddl"
                r1.put(r2, r0)
            L_0x0051:
                com.appsflyer.internal.b$b r0 = new com.appsflyer.internal.b$b
                r0.<init>(r1, r5)
                r1.putAll(r0)
                java.lang.String r0 = "appsflyerKey"
                java.lang.Object r0 = r1.get(r0)     // Catch:{ IOException -> 0x0094, all -> 0x008b }
                java.lang.String r0 = (java.lang.String) r0     // Catch:{ IOException -> 0x0094, all -> 0x008b }
                monitor-enter(r1)     // Catch:{ IOException -> 0x0094, all -> 0x008b }
                int r2 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x0085 }
                r4 = 19
                if (r2 < r4) goto L_0x006e
                org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ all -> 0x0085 }
                r2.<init>(r1)     // Catch:{ all -> 0x0085 }
                goto L_0x0072
            L_0x006e:
                org.json.JSONObject r2 = com.appsflyer.internal.i.m159(r1)     // Catch:{ all -> 0x0085 }
            L_0x0072:
                java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0085 }
                monitor-exit(r1)     // Catch:{ all -> 0x0083 }
                com.appsflyer.AppsFlyerLibCore r1 = com.appsflyer.AppsFlyerLibCore.this     // Catch:{ IOException -> 0x0089, all -> 0x008b }
                com.appsflyer.internal.h r4 = r7.f82     // Catch:{ IOException -> 0x0089, all -> 0x008b }
                r4.f220 = r2     // Catch:{ IOException -> 0x0089, all -> 0x008b }
                r4.f227 = r0     // Catch:{ IOException -> 0x0089, all -> 0x008b }
                com.appsflyer.AppsFlyerLibCore.m69(r1, r4)     // Catch:{ IOException -> 0x0089, all -> 0x008b }
                return
            L_0x0083:
                r0 = move-exception
                goto L_0x0087
            L_0x0085:
                r0 = move-exception
                r2 = r6
            L_0x0087:
                monitor-exit(r1)     // Catch:{ IOException -> 0x0089, all -> 0x008b }
                throw r0     // Catch:{ IOException -> 0x0089, all -> 0x008b }
            L_0x0089:
                r0 = move-exception
                goto L_0x0096
            L_0x008b:
                r0 = move-exception
                java.lang.String r1 = r0.getMessage()
                com.appsflyer.AFLogger.afErrorLog(r1, r0)
                return
            L_0x0094:
                r0 = move-exception
                r2 = r6
            L_0x0096:
                java.lang.String r1 = "Exception while sending request to server. "
                com.appsflyer.AFLogger.afErrorLog(r1, r0)
                if (r2 == 0) goto L_0x014e
                if (r5 == 0) goto L_0x014e
                java.lang.String r1 = "&isCachedRequest=true&timeincache="
                boolean r1 = r3.contains(r1)
                if (r1 != 0) goto L_0x014e
                com.appsflyer.internal.w.m193()
                com.appsflyer.AFFacebookDeferredDeeplink r1 = new com.appsflyer.AFFacebookDeferredDeeplink
                java.lang.String r4 = "4.10.3"
                r1.<init>(r3, r2, r4)
                java.io.File r2 = com.appsflyer.internal.w.m191(r5)     // Catch:{ Exception -> 0x0134 }
                boolean r3 = r2.exists()     // Catch:{ Exception -> 0x0134 }
                if (r3 != 0) goto L_0x00c0
                r2.mkdir()     // Catch:{ Exception -> 0x0134 }
                goto L_0x0140
            L_0x00c0:
                java.io.File[] r2 = r2.listFiles()     // Catch:{ Exception -> 0x0134 }
                if (r2 == 0) goto L_0x00d3
                int r2 = r2.length     // Catch:{ Exception -> 0x0134 }
                r3 = 40
                if (r2 <= r3) goto L_0x00d3
                java.lang.String r1 = "AppsFlyer_4.10.3"
                java.lang.String r2 = "reached cache limit, not caching request"
                android.util.Log.i(r1, r2)     // Catch:{ Exception -> 0x0134 }
                goto L_0x0140
            L_0x00d3:
                java.lang.String r2 = "AppsFlyer_4.10.3"
                java.lang.String r3 = "caching request..."
                android.util.Log.i(r2, r3)     // Catch:{ Exception -> 0x0134 }
                java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x0134 }
                java.io.File r3 = com.appsflyer.internal.w.m191(r5)     // Catch:{ Exception -> 0x0134 }
                long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0134 }
                java.lang.String r4 = java.lang.Long.toString(r4)     // Catch:{ Exception -> 0x0134 }
                r2.<init>(r3, r4)     // Catch:{ Exception -> 0x0134 }
                r2.createNewFile()     // Catch:{ Exception -> 0x0134 }
                java.io.OutputStreamWriter r3 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x0134 }
                java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0134 }
                java.lang.String r2 = r2.getPath()     // Catch:{ Exception -> 0x0134 }
                r5 = 1
                r4.<init>(r2, r5)     // Catch:{ Exception -> 0x0134 }
                r3.<init>(r4)     // Catch:{ Exception -> 0x0134 }
                java.lang.String r2 = "version="
                r3.write(r2)     // Catch:{ Exception -> 0x0130, all -> 0x012d }
                java.lang.String r2 = r1.f11     // Catch:{ Exception -> 0x0130, all -> 0x012d }
                r3.write(r2)     // Catch:{ Exception -> 0x0130, all -> 0x012d }
                r2 = 10
                r3.write(r2)     // Catch:{ Exception -> 0x0130, all -> 0x012d }
                java.lang.String r4 = "url="
                r3.write(r4)     // Catch:{ Exception -> 0x0130, all -> 0x012d }
                java.lang.String r4 = r1.f8     // Catch:{ Exception -> 0x0130, all -> 0x012d }
                r3.write(r4)     // Catch:{ Exception -> 0x0130, all -> 0x012d }
                r3.write(r2)     // Catch:{ Exception -> 0x0130, all -> 0x012d }
                java.lang.String r4 = "data="
                r3.write(r4)     // Catch:{ Exception -> 0x0130, all -> 0x012d }
                java.lang.String r1 = r1.f10     // Catch:{ Exception -> 0x0130, all -> 0x012d }
                r3.write(r1)     // Catch:{ Exception -> 0x0130, all -> 0x012d }
                r3.write(r2)     // Catch:{ Exception -> 0x0130, all -> 0x012d }
                r3.flush()     // Catch:{ Exception -> 0x0130, all -> 0x012d }
                r3.close()     // Catch:{ IOException -> 0x0140 }
                goto L_0x0140
            L_0x012d:
                r0 = move-exception
                r6 = r3
                goto L_0x0148
            L_0x0130:
                r6 = r3
                goto L_0x0134
            L_0x0132:
                r0 = move-exception
                goto L_0x0148
            L_0x0134:
                java.lang.String r1 = "AppsFlyer_4.10.3"
                java.lang.String r2 = "Could not cache request"
                android.util.Log.i(r1, r2)     // Catch:{ all -> 0x0132 }
                if (r6 == 0) goto L_0x0140
                r6.close()     // Catch:{ IOException -> 0x0140 }
            L_0x0140:
                java.lang.String r1 = r0.getMessage()
                com.appsflyer.AFLogger.afErrorLog(r1, r0)
                goto L_0x014e
            L_0x0148:
                if (r6 == 0) goto L_0x014d
                r6.close()     // Catch:{ IOException -> 0x014d }
            L_0x014d:
                throw r0
            L_0x014e:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.appsflyer.AppsFlyerLibCore.d.run():void");
        }

        private d(h hVar) {
            hVar.f222 = new WeakReference<>(hVar.f221);
            hVar.f221 = null;
            this.f82 = hVar;
        }
    }

    class e extends a {
        public e(Context context, String str, ScheduledExecutorService scheduledExecutorService) {
            super(context, str, scheduledExecutorService);
        }

        /* access modifiers changed from: protected */
        /* renamed from: ˊ  reason: contains not printable characters */
        public final void m86(Map<String, String> map) {
            AppsFlyerLibCore.f31.onInstallConversionDataLoaded(map);
            AppsFlyerLibCore.m37(this.f75.get(), "appsflyerConversionDataRequestRetries", 0);
        }

        /* access modifiers changed from: protected */
        /* renamed from: ˋ  reason: contains not printable characters */
        public final void m87(String str, int i2) {
            AppsFlyerLibCore.f31.onInstallConversionFailure(str);
            if (i2 >= 400 && i2 < 500) {
                AppsFlyerLibCore.m37(this.f75.get(), "appsflyerConversionDataRequestRetries", AppsFlyerLibCore.m49(this.f75.get()).getInt("appsflyerConversionDataRequestRetries", 0) + 1);
            }
        }

        /* renamed from: ˎ  reason: contains not printable characters */
        public final String m88() {
            return ServerConfigHandler.getUrl("https://%sapi.%s/install_data/v3/");
        }
    }

    static {
        StringBuilder sb = new StringBuilder();
        sb.append(f29);
        sb.append("/androidevent?buildnumber=4.10.3&app_id=");
        f25 = sb.toString();
        StringBuilder sb2 = new StringBuilder("https://%sattr.%s/api/v");
        sb2.append(f25);
        f24 = sb2.toString();
        StringBuilder sb3 = new StringBuilder("https://%st.%s/api/v");
        sb3.append(f25);
        f34 = sb3.toString();
        StringBuilder sb4 = new StringBuilder("https://%sevents.%s/api/v");
        sb4.append(f25);
        f32 = sb4.toString();
        StringBuilder sb5 = new StringBuilder("https://%sregister.%s/api/v");
        sb5.append(f25);
        f30 = sb5.toString();
    }

    private AppsFlyerLibCore() {
        AFVersionDeclaration.init();
    }

    public static AppsFlyerLibCore getInstance() {
        return f28;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static boolean m20(Context context) {
        if (context != null) {
            int i2 = Build.VERSION.SDK_INT;
            if (i2 >= 23) {
                try {
                    ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
                    for (Network networkCapabilities : connectivityManager.getAllNetworks()) {
                        NetworkCapabilities networkCapabilities2 = connectivityManager.getNetworkCapabilities(networkCapabilities);
                        if (networkCapabilities2.hasTransport(4) && !networkCapabilities2.hasCapability(15)) {
                            return true;
                        }
                    }
                    return false;
                } catch (Exception e2) {
                    AFLogger.afErrorLog("Failed collecting ivc data", e2);
                }
            } else if (i2 >= 16) {
                ArrayList arrayList = new ArrayList();
                try {
                    Iterator it = Collections.list(NetworkInterface.getNetworkInterfaces()).iterator();
                    while (it.hasNext()) {
                        NetworkInterface networkInterface = (NetworkInterface) it.next();
                        if (networkInterface.isUp()) {
                            arrayList.add(networkInterface.getName());
                        }
                    }
                    return arrayList.contains("tun0");
                } catch (Exception e3) {
                    AFLogger.afErrorLog("Failed collecting ivc data", e3);
                }
            }
        }
        return false;
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private static float m21(Context context) {
        try {
            Intent registerReceiver = context.getApplicationContext().registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            int intExtra = registerReceiver.getIntExtra("level", -1);
            int intExtra2 = registerReceiver.getIntExtra("scale", -1);
            if (intExtra == -1 || intExtra2 == -1) {
                return 50.0f;
            }
            return (((float) intExtra) / ((float) intExtra2)) * 100.0f;
        } catch (Throwable th) {
            AFLogger.afErrorLog(th.getMessage(), th);
            return 1.0f;
        }
    }

    /* renamed from: ॱॱ  reason: contains not printable characters */
    private static boolean m71(Context context) {
        try {
            if (GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context) == 0) {
                return true;
            }
        } catch (Throwable th) {
            AFLogger.afErrorLog("WARNING:  Google play services is unavailable. ", th);
        }
        try {
            context.getPackageManager().getPackageInfo("com.google.android.gms", 0);
            return true;
        } catch (PackageManager.NameNotFoundException e2) {
            AFLogger.afErrorLog("WARNING:  Google Play Services is unavailable. ", e2);
            return false;
        }
    }

    /* renamed from: ᐝ  reason: contains not printable characters */
    private static String m72(Context context) {
        String string = AppsFlyerProperties.getInstance().getString("api_store_value");
        if (string != null) {
            return string;
        }
        String r2 = m24(new WeakReference(context), "AF_STORE");
        if (r2 != null) {
            return r2;
        }
        return null;
    }

    public void enableFacebookDeferredApplinks(boolean z) {
        this.f53 = z;
    }

    public AppsFlyerLib enableLocationCollection(boolean z) {
        this.f45 = z;
        return this;
    }

    @Deprecated
    public void enableUninstallTracking(String str) {
        AFLogger.afWarnLog("enableUninstallTracking() is deprecated. GCM no longer works. This API does nothing. Please follow the documentation.");
    }

    public String getAppsFlyerUID(Context context) {
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108("public_api_call", "getAppsFlyerUID", new String[0]);
        return ab.m111(new WeakReference(context));
    }

    public String getAttributionId(Context context) {
        try {
            v vVar = new v(context);
            if (vVar.f288 == null) {
                return vVar.m190();
            }
            ProviderInfo resolveContentProvider = vVar.f288.getPackageManager().resolveContentProvider("com.facebook.katana.provider.AttributionIdProvider", 128);
            if (resolveContentProvider != null) {
                Signature[] signatureArr = vVar.f288.getPackageManager().getPackageInfo(resolveContentProvider.packageName, 64).signatures;
                MessageDigest instance = MessageDigest.getInstance("SHA1");
                instance.update(((X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(signatureArr[0].toByteArray()))).getEncoded());
                if (Base64.encodeToString(instance.digest(), 2).equals("ijxLJi1yGs1JpL+X1SExmchvork=")) {
                    return vVar.m190();
                }
            }
            return null;
        } catch (Throwable th) {
            AFLogger.afErrorLog("Could not collect facebook attribution id. ", th);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void getConversionData(Context context, final ConversionDataListener conversionDataListener) {
        f31 = new AppsFlyerConversionListener() {
            public final void onAppOpenAttribution(Map<String, String> map) {
            }

            public final void onAttributionFailure(String str) {
            }

            public final void onInstallConversionDataLoaded(Map<String, String> map) {
                conversionDataListener.onConversionDataLoaded(map);
            }

            public final void onInstallConversionFailure(String str) {
                conversionDataListener.onConversionFailure(str);
            }
        };
    }

    public String getHostName() {
        String string = AppsFlyerProperties.getInstance().getString("custom_host");
        return string != null ? string : ServerParameters.DEFAULT_HOST;
    }

    public String getHostPrefix() {
        String string = AppsFlyerProperties.getInstance().getString("custom_host_prefix");
        return string != null ? string : "";
    }

    public String getOutOfStore(Context context) {
        String string = AppsFlyerProperties.getInstance().getString("api_store_value");
        if (string != null) {
            return string;
        }
        String r3 = m24(new WeakReference(context), "AF_STORE");
        if (r3 != null) {
            return r3;
        }
        AFLogger.afInfoLog("No out-of-store value set");
        return null;
    }

    public String getSdkVersion() {
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108("public_api_call", "getSdkVersion", new String[0]);
        return "version: 4.10.3 (build 233)";
    }

    public void handleDeepLinkCallback(Context context, Map<String, Object> map, Uri uri) {
        final Map map2;
        String str;
        String obj = uri.toString();
        if (obj == null) {
            obj = null;
        } else if (obj.matches("fb\\d*?://authorize.*") && obj.contains("access_token")) {
            int indexOf = obj.indexOf(63);
            if (indexOf == -1) {
                str = "";
            } else {
                str = obj.substring(indexOf);
            }
            if (str.length() != 0) {
                ArrayList arrayList = new ArrayList();
                if (str.contains("&")) {
                    arrayList = new ArrayList(Arrays.asList(str.split("&")));
                } else {
                    arrayList.add(str);
                }
                StringBuilder sb = new StringBuilder();
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    String str2 = (String) it.next();
                    if (str2.contains("access_token")) {
                        it.remove();
                    } else {
                        if (sb.length() != 0) {
                            sb.append("&");
                        } else if (!str2.startsWith("?")) {
                            sb.append("?");
                        }
                        sb.append(str2);
                    }
                }
                obj = obj.replace(str, sb.toString());
            }
        }
        if (!map.containsKey("af_deeplink")) {
            map.put("af_deeplink", obj);
        }
        boolean z = true;
        if (uri.getQueryParameter("af_deeplink") != null) {
            this.f44 = "AppsFlyer_Test".equals(uri.getQueryParameter("media_source")) && Boolean.parseBoolean(uri.getQueryParameter("is_retargeting"));
            map2 = m52(context, uri.getQuery());
            String path = uri.getPath();
            if (path != null) {
                map2.put("path", path);
            }
            String scheme = uri.getScheme();
            if (scheme != null) {
                map2.put("scheme", scheme);
            }
            String host = uri.getHost();
            if (host != null) {
                map2.put("host", host);
            }
        } else {
            map2 = new HashMap();
            map2.put("link", uri.toString());
        }
        final WeakReference weakReference = new WeakReference(context);
        y yVar = new y(uri, this);
        yVar.setConnProvider(new OneLinkHttpTask.HttpsUrlConnectionProvider());
        if (TextUtils.isEmpty(yVar.f100) || TextUtils.isEmpty(yVar.f302)) {
            z = false;
        }
        if (z) {
            yVar.f301 = new y.a() {
                /* renamed from: ˋ  reason: contains not printable characters */
                private void m80(Map<String, String> map) {
                    if (weakReference.get() != null) {
                        AppsFlyerLibCore.m28((Context) weakReference.get(), "deeplinkAttribution", new JSONObject(map).toString());
                    }
                }

                /* renamed from: ˊ  reason: contains not printable characters */
                public final void m81(String str) {
                    if (AppsFlyerLibCore.f31 != null) {
                        m80(map2);
                        AppsFlyerLibCore.f31.onAttributionFailure(str);
                    }
                }

                /* renamed from: ˏ  reason: contains not printable characters */
                public final void m82(Map<String, String> map) {
                    for (String next : map.keySet()) {
                        map2.put(next, map.get(next));
                    }
                    m80(map2);
                    AppsFlyerLibCore.m30(map2);
                }
            };
            AFExecutor.getInstance().getThreadPoolExecutor().execute(yVar);
            return;
        }
        AppsFlyerConversionListener appsFlyerConversionListener = f31;
        if (appsFlyerConversionListener != null) {
            try {
                appsFlyerConversionListener.onAppOpenAttribution(map2);
            } catch (Throwable th) {
                AFLogger.afErrorLog(th.getLocalizedMessage(), th);
            }
        }
    }

    public AppsFlyerLib init(String str, AppsFlyerConversionListener appsFlyerConversionListener, Context context) {
        if (context != null) {
            this.f56 = (Application) context.getApplicationContext();
            if (m32(context)) {
                if (this.f62 == null) {
                    this.f62 = new f();
                    this.f62.m151(context, this);
                } else {
                    AFLogger.afWarnLog("AFInstallReferrer instance already created");
                }
            }
        } else {
            AFLogger.afWarnLog("init :: context is null, Google Install Referrer will be not initialized!");
        }
        return init(str, appsFlyerConversionListener);
    }

    public boolean isPreInstalledApp(Context context) {
        try {
            if ((context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).flags & 1) != 0) {
                return true;
            }
            return false;
        } catch (PackageManager.NameNotFoundException e2) {
            AFLogger.afErrorLog("Could not check if app is pre installed", e2);
        }
    }

    public boolean isTrackingStopped() {
        return this.f50;
    }

    public void onHandleReferrer(Map<String, String> map) {
        this.f38 = map;
    }

    public void onPause(Context context) {
        g.AnonymousClass2.m156(context);
        m r2 = m.m165(context);
        r2.f243.post(r2.f238);
    }

    public void registerConversionListener(Context context, AppsFlyerConversionListener appsFlyerConversionListener) {
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108("public_api_call", "registerConversionListener", new String[0]);
        if (appsFlyerConversionListener != null) {
            f31 = appsFlyerConversionListener;
        }
    }

    public void registerValidatorListener(Context context, AppsFlyerInAppPurchaseValidatorListener appsFlyerInAppPurchaseValidatorListener) {
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108("public_api_call", "registerValidatorListener", new String[0]);
        AFLogger.afDebugLog("registerValidatorListener called");
        if (appsFlyerInAppPurchaseValidatorListener == null) {
            AFLogger.afDebugLog("registerValidatorListener null listener");
        } else {
            f26 = appsFlyerInAppPurchaseValidatorListener;
        }
    }

    public void reportTrackSession(Context context) {
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108("public_api_call", "reportTrackSession", new String[0]);
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.f115 = false;
        h hVar = new h();
        hVar.f221 = context;
        hVar.f216 = null;
        hVar.f228 = null;
        m73(hVar);
    }

    public void sendDeepLinkData(Activity activity) {
        if (activity != null && activity.getIntent() != null) {
            if (aa.f104 == null) {
                aa.f104 = new aa();
            }
            aa aaVar = aa.f104;
            StringBuilder sb = new StringBuilder("activity_intent_");
            sb.append(activity.getIntent().toString());
            aaVar.m108("public_api_call", "sendDeepLinkData", activity.getLocalClassName(), sb.toString());
        } else if (activity != null) {
            if (aa.f104 == null) {
                aa.f104 = new aa();
            }
            aa.f104.m108("public_api_call", "sendDeepLinkData", activity.getLocalClassName(), "activity_intent_null");
        } else {
            if (aa.f104 == null) {
                aa.f104 = new aa();
            }
            aa.f104.m108("public_api_call", "sendDeepLinkData", "activity_null");
        }
        try {
            Application application = activity.getApplication();
            h hVar = new h();
            hVar.f221 = application;
            m45(hVar);
            StringBuilder sb2 = new StringBuilder("getDeepLinkData with activity ");
            sb2.append(activity.getIntent().getDataString());
            AFLogger.afInfoLog(sb2.toString());
        } catch (Exception e2) {
            AFLogger.afInfoLog("getDeepLinkData Exception: ".concat(String.valueOf(e2)));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:57:0x0191  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void sendPushNotificationData(android.app.Activity r19) {
        /*
            r18 = this;
            r1 = r18
            r2 = r19
            java.lang.String r0 = "c"
            java.lang.String r3 = "pid"
            r4 = 2
            java.lang.String r5 = "public_api_call"
            r6 = 0
            r7 = 1
            java.lang.String r8 = "sendPushNotificationData"
            if (r2 == 0) goto L_0x0048
            android.content.Intent r9 = r19.getIntent()
            if (r9 == 0) goto L_0x0048
            com.appsflyer.internal.aa r9 = com.appsflyer.internal.aa.f104
            if (r9 != 0) goto L_0x0022
            com.appsflyer.internal.aa r9 = new com.appsflyer.internal.aa
            r9.<init>()
            com.appsflyer.internal.aa.f104 = r9
        L_0x0022:
            com.appsflyer.internal.aa r9 = com.appsflyer.internal.aa.f104
            java.lang.String[] r10 = new java.lang.String[r4]
            java.lang.String r11 = r19.getLocalClassName()
            r10[r6] = r11
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r11 = "activity_intent_"
            r6.<init>(r11)
            android.content.Intent r11 = r19.getIntent()
            java.lang.String r11 = r11.toString()
            r6.append(r11)
            java.lang.String r6 = r6.toString()
            r10[r7] = r6
            r9.m108(r5, r8, r10)
            goto L_0x007d
        L_0x0048:
            if (r2 == 0) goto L_0x0067
            com.appsflyer.internal.aa r9 = com.appsflyer.internal.aa.f104
            if (r9 != 0) goto L_0x0055
            com.appsflyer.internal.aa r9 = new com.appsflyer.internal.aa
            r9.<init>()
            com.appsflyer.internal.aa.f104 = r9
        L_0x0055:
            com.appsflyer.internal.aa r9 = com.appsflyer.internal.aa.f104
            java.lang.String[] r10 = new java.lang.String[r4]
            java.lang.String r11 = r19.getLocalClassName()
            r10[r6] = r11
            java.lang.String r6 = "activity_intent_null"
            r10[r7] = r6
            r9.m108(r5, r8, r10)
            goto L_0x007d
        L_0x0067:
            com.appsflyer.internal.aa r9 = com.appsflyer.internal.aa.f104
            if (r9 != 0) goto L_0x0072
            com.appsflyer.internal.aa r9 = new com.appsflyer.internal.aa
            r9.<init>()
            com.appsflyer.internal.aa.f104 = r9
        L_0x0072:
            com.appsflyer.internal.aa r9 = com.appsflyer.internal.aa.f104
            java.lang.String[] r7 = new java.lang.String[r7]
            java.lang.String r10 = "activity_null"
            r7[r6] = r10
            r9.m108(r5, r8, r7)
        L_0x007d:
            boolean r5 = r2 instanceof android.app.Activity
            if (r5 == 0) goto L_0x00ad
            android.content.Intent r5 = r19.getIntent()
            if (r5 == 0) goto L_0x00ad
            android.os.Bundle r7 = r5.getExtras()
            if (r7 == 0) goto L_0x00ad
            java.lang.String r8 = "af"
            java.lang.String r9 = r7.getString(r8)
            if (r9 == 0) goto L_0x00ae
            java.lang.String r10 = java.lang.String.valueOf(r9)
            java.lang.String r11 = "Push Notification received af payload = "
            java.lang.String r10 = r11.concat(r10)
            com.appsflyer.AFLogger.afInfoLog(r10)
            r7.remove(r8)
            android.content.Intent r5 = r5.putExtras(r7)
            r2.setIntent(r5)
            goto L_0x00ae
        L_0x00ad:
            r9 = 0
        L_0x00ae:
            r1.f43 = r9
            java.lang.String r5 = r1.f43
            if (r5 == 0) goto L_0x01c7
            long r7 = java.lang.System.currentTimeMillis()
            java.util.Map<java.lang.Long, java.lang.String> r5 = r1.f47
            java.lang.String r9 = ")"
            if (r5 != 0) goto L_0x00cd
            java.lang.String r0 = "pushes: initializing pushes history.."
            com.appsflyer.AFLogger.afInfoLog(r0)
            java.util.concurrent.ConcurrentHashMap r0 = new java.util.concurrent.ConcurrentHashMap
            r0.<init>()
            r1.f47 = r0
            r12 = r7
            goto L_0x017e
        L_0x00cd:
            com.appsflyer.AppsFlyerProperties r5 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x0163 }
            java.lang.String r10 = "pushPayloadMaxAging"
            r11 = 1800000(0x1b7740, double:8.89318E-318)
            long r10 = r5.getLong(r10, r11)     // Catch:{ all -> 0x0163 }
            java.util.Map<java.lang.Long, java.lang.String> r5 = r1.f47     // Catch:{ all -> 0x0163 }
            java.util.Set r5 = r5.keySet()     // Catch:{ all -> 0x0163 }
            java.util.Iterator r5 = r5.iterator()     // Catch:{ all -> 0x0163 }
            r12 = r7
        L_0x00e5:
            boolean r14 = r5.hasNext()     // Catch:{ all -> 0x0161 }
            if (r14 == 0) goto L_0x017e
            java.lang.Object r14 = r5.next()     // Catch:{ all -> 0x0161 }
            java.lang.Long r14 = (java.lang.Long) r14     // Catch:{ all -> 0x0161 }
            org.json.JSONObject r15 = new org.json.JSONObject     // Catch:{ all -> 0x0161 }
            java.lang.String r4 = r1.f43     // Catch:{ all -> 0x0161 }
            r15.<init>(r4)     // Catch:{ all -> 0x0161 }
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ all -> 0x0161 }
            java.util.Map<java.lang.Long, java.lang.String> r6 = r1.f47     // Catch:{ all -> 0x0161 }
            java.lang.Object r6 = r6.get(r14)     // Catch:{ all -> 0x0161 }
            java.lang.String r6 = (java.lang.String) r6     // Catch:{ all -> 0x0161 }
            r4.<init>(r6)     // Catch:{ all -> 0x0161 }
            java.lang.Object r6 = r15.get(r3)     // Catch:{ all -> 0x0161 }
            java.lang.Object r2 = r4.get(r3)     // Catch:{ all -> 0x0161 }
            boolean r2 = r6.equals(r2)     // Catch:{ all -> 0x0161 }
            if (r2 == 0) goto L_0x0141
            java.lang.Object r2 = r15.get(r0)     // Catch:{ all -> 0x0161 }
            java.lang.Object r6 = r4.get(r0)     // Catch:{ all -> 0x0161 }
            boolean r2 = r2.equals(r6)     // Catch:{ all -> 0x0161 }
            if (r2 == 0) goto L_0x0141
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0161 }
            java.lang.String r2 = "PushNotificationMeasurement: A previous payload with same PID and campaign was already acknowledged! (old: "
            r0.<init>(r2)     // Catch:{ all -> 0x0161 }
            r0.append(r4)     // Catch:{ all -> 0x0161 }
            java.lang.String r2 = ", new: "
            r0.append(r2)     // Catch:{ all -> 0x0161 }
            r0.append(r15)     // Catch:{ all -> 0x0161 }
            r0.append(r9)     // Catch:{ all -> 0x0161 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0161 }
            com.appsflyer.AFLogger.afInfoLog(r0)     // Catch:{ all -> 0x0161 }
            r2 = 0
            r1.f43 = r2     // Catch:{ all -> 0x0161 }
            return
        L_0x0141:
            r2 = 0
            long r16 = r14.longValue()     // Catch:{ all -> 0x0161 }
            long r16 = r7 - r16
            int r4 = (r16 > r10 ? 1 : (r16 == r10 ? 0 : -1))
            if (r4 <= 0) goto L_0x0151
            java.util.Map<java.lang.Long, java.lang.String> r4 = r1.f47     // Catch:{ all -> 0x0161 }
            r4.remove(r14)     // Catch:{ all -> 0x0161 }
        L_0x0151:
            long r16 = r14.longValue()     // Catch:{ all -> 0x0161 }
            int r4 = (r16 > r12 ? 1 : (r16 == r12 ? 0 : -1))
            if (r4 > 0) goto L_0x015d
            long r12 = r14.longValue()     // Catch:{ all -> 0x0161 }
        L_0x015d:
            r2 = r19
            r4 = 2
            goto L_0x00e5
        L_0x0161:
            r0 = move-exception
            goto L_0x0165
        L_0x0163:
            r0 = move-exception
            r12 = r7
        L_0x0165:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Error while handling push notification measurement: "
            r2.<init>(r3)
            java.lang.Class r3 = r0.getClass()
            java.lang.String r3 = r3.getSimpleName()
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.appsflyer.AFLogger.afErrorLog(r2, r0)
        L_0x017e:
            com.appsflyer.AppsFlyerProperties r0 = com.appsflyer.AppsFlyerProperties.getInstance()
            java.lang.String r2 = "pushPayloadHistorySize"
            r3 = 2
            int r0 = r0.getInt(r2, r3)
            java.util.Map<java.lang.Long, java.lang.String> r2 = r1.f47
            int r2 = r2.size()
            if (r2 != r0) goto L_0x01ae
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r2 = "pushes: removing oldest overflowing push (oldest push:"
            r0.<init>(r2)
            r0.append(r12)
            r0.append(r9)
            java.lang.String r0 = r0.toString()
            com.appsflyer.AFLogger.afInfoLog(r0)
            java.util.Map<java.lang.Long, java.lang.String> r0 = r1.f47
            java.lang.Long r2 = java.lang.Long.valueOf(r12)
            r0.remove(r2)
        L_0x01ae:
            java.util.Map<java.lang.Long, java.lang.String> r0 = r1.f47
            java.lang.Long r2 = java.lang.Long.valueOf(r7)
            java.lang.String r3 = r1.f43
            r0.put(r2, r3)
            android.app.Application r0 = r19.getApplication()
            com.appsflyer.internal.h r2 = new com.appsflyer.internal.h
            r2.<init>()
            r2.f221 = r0
            r1.m45(r2)
        L_0x01c7:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsflyer.AppsFlyerLibCore.sendPushNotificationData(android.app.Activity):void");
    }

    public void setAdditionalData(HashMap<String, Object> hashMap) {
        if (hashMap != null) {
            if (aa.f104 == null) {
                aa.f104 = new aa();
            }
            aa.f104.m108("public_api_call", "setAdditionalData", hashMap.toString());
            AppsFlyerProperties.getInstance().setCustomData(new JSONObject(hashMap).toString());
        }
    }

    public void setAndroidIdData(String str) {
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108("public_api_call", "setAndroidIdData", str);
        this.f46 = str;
    }

    public void setAppId(String str) {
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108("public_api_call", "setAppId", str);
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.APP_ID, str);
    }

    public void setAppInviteOneLink(String str) {
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108("public_api_call", "setAppInviteOneLink", str);
        AFLogger.afInfoLog("setAppInviteOneLink = ".concat(String.valueOf(str)));
        if (str == null || !str.equals(AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.ONELINK_ID))) {
            AppsFlyerProperties.getInstance().remove(AppsFlyerProperties.ONELINK_DOMAIN);
            AppsFlyerProperties.getInstance().remove("onelinkVersion");
            AppsFlyerProperties.getInstance().remove(AppsFlyerProperties.ONELINK_SCHEME);
        }
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.ONELINK_ID, str);
    }

    @Deprecated
    public void setAppUserId(String str) {
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108("public_api_call", "setAppUserId", str);
        setCustomerUserId(str);
    }

    public void setCollectAndroidID(boolean z) {
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108("public_api_call", "setCollectAndroidID", String.valueOf(z));
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.COLLECT_ANDROID_ID, Boolean.toString(z));
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.COLLECT_ANDROID_ID_FORCE_BY_USER, Boolean.toString(z));
    }

    @Deprecated
    public void setCollectFingerPrint(boolean z) {
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108("public_api_call", "setCollectFingerPrint", String.valueOf(z));
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.COLLECT_FINGER_PRINT, Boolean.toString(z));
    }

    public void setCollectIMEI(boolean z) {
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108("public_api_call", "setCollectIMEI", String.valueOf(z));
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.COLLECT_IMEI, Boolean.toString(z));
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.COLLECT_IMEI_FORCE_BY_USER, Boolean.toString(z));
    }

    public void setCollectOaid(boolean z) {
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108("public_api_call", "setCollectOaid", String.valueOf(z));
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.COLLECT_OAID, Boolean.toString(z));
    }

    public void setConsumeAFDeepLinks(boolean z) {
        AppsFlyerProperties.getInstance().set("consumeAfDeepLink", z);
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108("public_api_call", "setConsumeAFDeepLinks: ".concat(String.valueOf(z)), new String[0]);
    }

    public void setCurrencyCode(String str) {
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108("public_api_call", "setCurrencyCode", str);
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.CURRENCY_CODE, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.appsflyer.AppsFlyerProperties.set(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.appsflyer.AppsFlyerProperties.set(java.lang.String, int):void
      com.appsflyer.AppsFlyerProperties.set(java.lang.String, long):void
      com.appsflyer.AppsFlyerProperties.set(java.lang.String, java.lang.String):void
      com.appsflyer.AppsFlyerProperties.set(java.lang.String, java.lang.String[]):void
      com.appsflyer.AppsFlyerProperties.set(java.lang.String, boolean):void */
    public void setCustomerIdAndTrack(String str, Context context) {
        if (context != null) {
            if (AppsFlyerProperties.getInstance().getBoolean(AppsFlyerProperties.AF_WAITFOR_CUSTOMERID, false) && AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.APP_USER_ID) == null) {
                setCustomerUserId(str);
                AppsFlyerProperties.getInstance().set(AppsFlyerProperties.AF_WAITFOR_CUSTOMERID, false);
                StringBuilder sb = new StringBuilder("CustomerUserId set: ");
                sb.append(str);
                sb.append(" - Initializing AppsFlyer Tacking");
                AFLogger.afInfoLog(sb.toString(), true);
                String referrer = AppsFlyerProperties.getInstance().getReferrer(context);
                String string = AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.AF_KEY);
                if (referrer == null) {
                    referrer = "";
                }
                m54(context, string, referrer, context instanceof Activity ? ((Activity) context).getIntent() : null);
                if (AppsFlyerProperties.getInstance().getString("afUninstallToken") != null) {
                    m75(context, AppsFlyerProperties.getInstance().getString("afUninstallToken"));
                    return;
                }
                return;
            }
            setCustomerUserId(str);
            AFLogger.afInfoLog("waitForCustomerUserId is false; setting CustomerUserID: ".concat(String.valueOf(str)), true);
        }
    }

    public void setCustomerUserId(String str) {
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108("public_api_call", "setCustomerUserId", str);
        AFLogger.afInfoLog("setCustomerUserId = ".concat(String.valueOf(str)));
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.APP_USER_ID, str);
    }

    public void setDebugLog(boolean z) {
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108("public_api_call", "setDebugLog", String.valueOf(z));
        AppsFlyerProperties.getInstance().set("shouldLog", z);
        AppsFlyerProperties.getInstance().set("logLevel", (z ? AFLogger.LogLevel.DEBUG : AFLogger.LogLevel.NONE).getLevel());
    }

    /* access modifiers changed from: protected */
    public void setDeepLinkData(Intent intent) {
        if (intent != null) {
            try {
                if ("android.intent.action.VIEW".equals(intent.getAction())) {
                    this.latestDeepLink = intent.getData();
                    StringBuilder sb = new StringBuilder("Unity setDeepLinkData = ");
                    sb.append(this.latestDeepLink);
                    AFLogger.afDebugLog(sb.toString());
                }
            } catch (Throwable th) {
                AFLogger.afErrorLog("Exception while setting deeplink data (unity). ", th);
            }
        }
    }

    public void setDeviceTrackingDisabled(boolean z) {
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108("public_api_call", "setDeviceTrackingDisabled", String.valueOf(z));
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.DEVICE_TRACKING_DISABLED, z);
    }

    public void setExtension(String str) {
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108("public_api_call", "setExtension", str);
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.EXTENSION, str);
    }

    @Deprecated
    public void setGCMProjectID(String str) {
        AFLogger.afWarnLog("setGCMProjectID() is deprecated. GCM no longer works. This API does nothing. Please follow the documentation.");
    }

    @Deprecated
    public void setGCMProjectNumber(String str) {
        AFLogger.afWarnLog("setGCMProjectNumber() is deprecated. GCM no longer works. This API does nothing. Please follow the documentation.");
    }

    public void setHost(String str, String str2) {
        if (str != null) {
            AppsFlyerProperties.getInstance().set("custom_host_prefix", str);
        }
        if (str2 == null || str2.isEmpty()) {
            AFLogger.afWarnLog("hostName cannot be null or empty");
        } else {
            AppsFlyerProperties.getInstance().set("custom_host", str2);
        }
    }

    @Deprecated
    public void setHostName(String str) {
        AppsFlyerProperties.getInstance().set("custom_host", str);
    }

    public void setImeiData(String str) {
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108("public_api_call", "setImeiData", str);
        this.f57 = str;
    }

    public void setIsUpdate(boolean z) {
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108("public_api_call", "setIsUpdate", String.valueOf(z));
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.IS_UPDATE, z);
    }

    public void setLogLevel(AFLogger.LogLevel logLevel) {
        AppsFlyerProperties.getInstance().set("logLevel", logLevel.getLevel());
    }

    public void setMinTimeBetweenSessions(int i2) {
        this.f35 = TimeUnit.SECONDS.toMillis((long) i2);
    }

    public void setOaidData(String str) {
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108("public_api_call", "setOaidData", str);
        this.f59 = str;
    }

    public void setOneLinkCustomDomain(String... strArr) {
        AFLogger.afDebugLog(String.format("setOneLinkCustomDomain %s", Arrays.toString(strArr)));
        g.f202 = strArr;
    }

    public void setOutOfStore(String str) {
        if (str != null) {
            String lowerCase = str.toLowerCase();
            AppsFlyerProperties.getInstance().set("api_store_value", lowerCase);
            AFLogger.afInfoLog("Store API set with value: ".concat(String.valueOf(lowerCase)), true);
            return;
        }
        AFLogger.m12("Cannot set setOutOfStore with null");
    }

    public void setPhoneNumber(String str) {
        this.f55 = ad.m120(str);
    }

    public void setPluginDeepLinkData(Intent intent) {
        setDeepLinkData(intent);
    }

    public void setPreinstallAttribution(String str, String str2, String str3) {
        AFLogger.afDebugLog("setPreinstallAttribution API called");
        JSONObject jSONObject = new JSONObject();
        if (str != null) {
            try {
                jSONObject.put(Constants.URL_MEDIA_SOURCE, str);
            } catch (JSONException e2) {
                AFLogger.afErrorLog(e2.getMessage(), e2);
            }
        }
        if (str2 != null) {
            jSONObject.put(Constants.URL_CAMPAIGN, str2);
        }
        if (str3 != null) {
            jSONObject.put(Constants.URL_SITE_ID, str3);
        }
        if (jSONObject.has(Constants.URL_MEDIA_SOURCE)) {
            AppsFlyerProperties.getInstance().set("preInstallName", jSONObject.toString());
            return;
        }
        AFLogger.afWarnLog("Cannot set preinstall attribution data without a media source");
    }

    public void setResolveDeepLinkURLs(String... strArr) {
        AFLogger.afDebugLog(String.format("setResolveDeepLinkURLs %s", Arrays.toString(strArr)));
        g.f205 = strArr;
    }

    @Deprecated
    public void setUserEmail(String str) {
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108("public_api_call", "setUserEmail", str);
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.USER_EMAIL, str);
    }

    public void setUserEmails(AppsFlyerProperties.EmailsCryptType emailsCryptType, String... strArr) {
        ArrayList arrayList = new ArrayList(strArr.length + 1);
        arrayList.add(emailsCryptType.toString());
        arrayList.addAll(Arrays.asList(strArr));
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108("public_api_call", "setUserEmails", (String[]) arrayList.toArray(new String[(strArr.length + 1)]));
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.EMAIL_CRYPT_TYPE, emailsCryptType.getValue());
        HashMap hashMap = new HashMap();
        String str = null;
        ArrayList arrayList2 = new ArrayList();
        for (String str2 : strArr) {
            int i2 = AnonymousClass3.f67[emailsCryptType.ordinal()];
            if (i2 == 2) {
                arrayList2.add(ad.m119(str2));
                str = "md5_el_arr";
            } else if (i2 == 3) {
                arrayList2.add(ad.m120(str2));
                str = "sha256_el_arr";
            } else if (i2 != 4) {
                arrayList2.add(ad.m121(str2));
                str = "sha1_el_arr";
            } else {
                arrayList2.add(str2);
                str = "plain_el_arr";
            }
        }
        hashMap.put(str, arrayList2);
        AppsFlyerProperties.getInstance().setUserEmails(new JSONObject(hashMap).toString());
    }

    public void startTracking(Application application) {
        if (!this.f51) {
            AFLogger.afWarnLog("ERROR: AppsFlyer SDK is not initialized! The API call 'startTracking(Application)' must be called after the 'init(String, AppsFlyerConversionListener)' API method, which should be called on the Application's onCreate.");
        } else {
            startTracking(application, null);
        }
    }

    public void stopTracking(boolean z, Context context) {
        this.f50 = z;
        w.m193();
        try {
            File r1 = w.m191(context);
            if (!r1.exists()) {
                r1.mkdir();
            } else {
                for (File file : r1.listFiles()) {
                    StringBuilder sb = new StringBuilder("Found cached request");
                    sb.append(file.getName());
                    Log.i(LOG_TAG, sb.toString());
                    w.m195(w.m192(file).f9, context);
                }
            }
        } catch (Exception unused) {
            Log.i(LOG_TAG, "Could not cache request");
        }
        if (this.f50) {
            SharedPreferences.Editor edit = context.getApplicationContext().getSharedPreferences("appsflyer-data", 0).edit();
            edit.putBoolean(IS_STOP_TRACKING_USED, true);
            edit.apply();
        }
    }

    public void trackAppLaunch(Context context, String str) {
        if (m32(context)) {
            if (this.f62 == null) {
                this.f62 = new f();
                this.f62.m151(context, this);
            } else {
                AFLogger.afWarnLog("AFInstallReferrer instance already created");
            }
        }
        m54(context, str, "", null);
    }

    public void trackEvent(Context context, String str, Map<String, Object> map, AppsFlyerTrackingRequestListener appsFlyerTrackingRequestListener) {
        JSONObject jSONObject = new JSONObject(map == null ? new HashMap<>() : map);
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108("public_api_call", "trackEvent", str, jSONObject.toString());
        h hVar = new h();
        hVar.f221 = context;
        hVar.f216 = str;
        hVar.f228 = map;
        hVar.f225 = appsFlyerTrackingRequestListener;
        m73(hVar);
    }

    public void trackLocation(Context context, double d2, double d3) {
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108("public_api_call", "trackLocation", String.valueOf(d2), String.valueOf(d3));
        HashMap hashMap = new HashMap();
        hashMap.put(AFInAppEventParameterName.LONGTITUDE, Double.toString(d3));
        hashMap.put(AFInAppEventParameterName.LATITUDE, Double.toString(d2));
        h hVar = new h();
        hVar.f221 = context;
        hVar.f216 = AFInAppEventType.LOCATION_COORDINATES;
        hVar.f228 = hashMap;
        m73(hVar);
    }

    public void unregisterConversionListener() {
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108("public_api_call", "unregisterConversionListener", new String[0]);
        f31 = null;
    }

    public void updateServerUninstallToken(Context context, String str) {
        if (str != null) {
            l.a.m163(context, new b.C0111b.a(str).f182);
        }
    }

    public void validateAndTrackInAppPurchase(Context context, String str, String str2, String str3, String str4, String str5, Map<String, String> map) {
        String str6;
        Context context2 = context;
        String str7 = str3;
        String str8 = str4;
        String str9 = str5;
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa aaVar = aa.f104;
        String[] strArr = new String[6];
        strArr[0] = str;
        strArr[1] = str2;
        strArr[2] = str7;
        strArr[3] = str8;
        strArr[4] = str9;
        if (map == null) {
            str6 = "";
        } else {
            str6 = map.toString();
        }
        strArr[5] = str6;
        aaVar.m108("public_api_call", "validateAndTrackInAppPurchase", strArr);
        if (!isTrackingStopped()) {
            StringBuilder sb = new StringBuilder("Validate in app called with parameters: ");
            sb.append(str7);
            sb.append(RemoteConfigConst.SHOP_SPECIAL_EVENT_OFFER);
            sb.append(str8);
            sb.append(RemoteConfigConst.SHOP_SPECIAL_EVENT_OFFER);
            sb.append(str9);
            AFLogger.afInfoLog(sb.toString());
        }
        if (str == null || str8 == null || str2 == null || str9 == null || str7 == null) {
            AppsFlyerInAppPurchaseValidatorListener appsFlyerInAppPurchaseValidatorListener = f26;
            if (appsFlyerInAppPurchaseValidatorListener != null) {
                appsFlyerInAppPurchaseValidatorListener.onValidateInAppFailure("Please provide purchase parameters");
                return;
            }
            return;
        }
        m46(AFExecutor.getInstance().m1(), new r(context.getApplicationContext(), AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.AF_KEY), str, str2, str3, str4, str5, map, context2 instanceof Activity ? ((Activity) context2).getIntent() : null), 10, TimeUnit.MILLISECONDS);
    }

    public void waitForCustomerUserId(boolean z) {
        AFLogger.afInfoLog("initAfterCustomerUserID: ".concat(String.valueOf(z)), true);
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.AF_WAITFOR_CUSTOMERID, z);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static boolean m18(Context context) {
        if ((AppsFlyerProperties.getInstance().getBoolean(AppsFlyerProperties.COLLECT_ANDROID_ID_FORCE_BY_USER, false) || AppsFlyerProperties.getInstance().getBoolean(AppsFlyerProperties.COLLECT_IMEI_FORCE_BY_USER, false)) || !m71(context)) {
            return true;
        }
        return false;
    }

    @Deprecated
    public void setGCMProjectNumber(Context context, String str) {
        AFLogger.afWarnLog("setGCMProjectNumber() is deprecated. GCM no longer works. This API does nothing. Please follow the documentation.");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public final void m76(Context context, Intent intent) {
        if (intent.getStringExtra("appsflyer_preinstall") != null) {
            getInstance();
            String stringExtra = intent.getStringExtra("appsflyer_preinstall");
            try {
                if (new JSONObject(stringExtra).has(Constants.URL_MEDIA_SOURCE)) {
                    AppsFlyerProperties.getInstance().set("preInstallName", stringExtra);
                } else {
                    AFLogger.afWarnLog("Cannot set preinstall attribution data without a media source");
                }
            } catch (JSONException e2) {
                AFLogger.afErrorLog("Error parsing JSON for preinstall", e2);
            }
        }
        String stringExtra2 = intent.getStringExtra(AppsFlyerProperties.IS_MONITOR);
        if (stringExtra2 != null) {
            AFLogger.afInfoLog("Turning on monitoring.");
            AppsFlyerProperties.getInstance().set(AppsFlyerProperties.IS_MONITOR, stringExtra2.equals("true"));
            m29(context, null, "START_TRACKING", context.getPackageName());
            return;
        }
        AFLogger.afInfoLog("****** onReceive called *******");
        AppsFlyerProperties.getInstance().setOnReceiveCalled();
        String stringExtra3 = intent.getStringExtra(TapjoyConstants.TJC_REFERRER);
        AFLogger.afInfoLog("Play store referrer: ".concat(String.valueOf(stringExtra3)));
        if (stringExtra3 != null) {
            if ("AppsFlyer_Test".equals(intent.getStringExtra("TestIntegrationMode"))) {
                SharedPreferences.Editor edit = context.getApplicationContext().getSharedPreferences("appsflyer-data", 0).edit();
                edit.clear();
                edit.apply();
                AppsFlyerProperties.getInstance().setFirstLaunchCalled(false);
                AFLogger.afInfoLog("Test mode started..");
                this.f42 = System.currentTimeMillis();
            }
            SharedPreferences.Editor edit2 = context.getApplicationContext().getSharedPreferences("appsflyer-data", 0).edit();
            edit2.putString(TapjoyConstants.TJC_REFERRER, stringExtra3);
            edit2.apply();
            AppsFlyerProperties.getInstance().setReferrer(stringExtra3);
            if (AppsFlyerProperties.getInstance().isFirstLaunchCalled()) {
                AFLogger.afInfoLog("onReceive: isLaunchCalled");
                if (stringExtra3 != null && stringExtra3.length() > 5) {
                    ScheduledThreadPoolExecutor r0 = AFExecutor.getInstance().m1();
                    h hVar = new h();
                    hVar.f221 = context;
                    h r9 = hVar.m157();
                    r9.f222 = new WeakReference<>(r9.f221);
                    r9.f221 = null;
                    r9.f226 = stringExtra3;
                    r9.f223 = true;
                    r9.f217 = intent;
                    m46(r0, new c(this, r9, (byte) 0), 5, TimeUnit.MILLISECONDS);
                }
            }
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static void m26(Context context, String str) {
        Intent intent = new Intent("com.appsflyer.testIntgrationBroadcast");
        intent.putExtra(NativeProtocol.WEB_DIALOG_PARAMS, str);
        if (Build.VERSION.SDK_INT < 26) {
            context.sendBroadcast(intent);
        } else if (context.getPackageManager().queryBroadcastReceivers(intent, 0).toString().contains("com.appsflyer.referrerSender")) {
            Intent intent2 = new Intent(intent);
            intent2.setComponent(new ComponentName("com.appsflyer.referrerSender", "com.appsflyer.referrerSender.Receiver"));
            context.sendBroadcast(intent2);
        }
    }

    /* JADX WARNING: Missing exception handler attribute for start block: B:22:0x003f */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0031 A[SYNTHETIC, Splitter:B:16:0x0031] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:13:0x0028=Splitter:B:13:0x0028, B:22:0x003f=Splitter:B:22:0x003f} */
    /* renamed from: ˋ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String m33(java.io.File r4, java.lang.String r5) {
        /*
            r0 = 0
            java.util.Properties r1 = new java.util.Properties     // Catch:{ FileNotFoundException -> 0x003e, all -> 0x0026 }
            r1.<init>()     // Catch:{ FileNotFoundException -> 0x003e, all -> 0x0026 }
            java.io.FileReader r2 = new java.io.FileReader     // Catch:{ FileNotFoundException -> 0x003e, all -> 0x0026 }
            r2.<init>(r4)     // Catch:{ FileNotFoundException -> 0x003e, all -> 0x0026 }
            r1.load(r2)     // Catch:{ FileNotFoundException -> 0x003f, all -> 0x0024 }
            java.lang.String r3 = "Found PreInstall property!"
            com.appsflyer.AFLogger.afInfoLog(r3)     // Catch:{ FileNotFoundException -> 0x003f, all -> 0x0024 }
            java.lang.String r4 = r1.getProperty(r5)     // Catch:{ FileNotFoundException -> 0x003f, all -> 0x0024 }
            r2.close()     // Catch:{ all -> 0x001b }
            goto L_0x0023
        L_0x001b:
            r5 = move-exception
            java.lang.String r0 = r5.getMessage()
            com.appsflyer.AFLogger.afErrorLog(r0, r5)
        L_0x0023:
            return r4
        L_0x0024:
            r4 = move-exception
            goto L_0x0028
        L_0x0026:
            r4 = move-exception
            r2 = r0
        L_0x0028:
            java.lang.String r5 = r4.getMessage()     // Catch:{ all -> 0x005a }
            com.appsflyer.AFLogger.afErrorLog(r5, r4)     // Catch:{ all -> 0x005a }
            if (r2 == 0) goto L_0x0059
            r2.close()     // Catch:{ all -> 0x0035 }
            goto L_0x0059
        L_0x0035:
            r4 = move-exception
            java.lang.String r5 = r4.getMessage()
            com.appsflyer.AFLogger.afErrorLog(r5, r4)
            goto L_0x0059
        L_0x003e:
            r2 = r0
        L_0x003f:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x005a }
            java.lang.String r1 = "PreInstall file wasn't found: "
            r5.<init>(r1)     // Catch:{ all -> 0x005a }
            java.lang.String r4 = r4.getAbsolutePath()     // Catch:{ all -> 0x005a }
            r5.append(r4)     // Catch:{ all -> 0x005a }
            java.lang.String r4 = r5.toString()     // Catch:{ all -> 0x005a }
            com.appsflyer.AFLogger.afDebugLog(r4)     // Catch:{ all -> 0x005a }
            if (r2 == 0) goto L_0x0059
            r2.close()     // Catch:{ all -> 0x0035 }
        L_0x0059:
            return r0
        L_0x005a:
            r4 = move-exception
            if (r2 == 0) goto L_0x0069
            r2.close()     // Catch:{ all -> 0x0061 }
            goto L_0x0069
        L_0x0061:
            r5 = move-exception
            java.lang.String r0 = r5.getMessage()
            com.appsflyer.AFLogger.afErrorLog(r0, r5)
        L_0x0069:
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsflyer.AppsFlyerLibCore.m33(java.io.File, java.lang.String):java.lang.String");
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    private void m70(h hVar) {
        hVar.m157();
        boolean z = hVar.f216 == null;
        if (AppsFlyerProperties.getInstance().getBoolean(AppsFlyerProperties.AF_WAITFOR_CUSTOMERID, false) && AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.APP_USER_ID) == null) {
            AFLogger.afInfoLog("CustomerUserId not set, Tracking is disabled", true);
            return;
        }
        if (z) {
            if (!AppsFlyerProperties.getInstance().getBoolean(AppsFlyerProperties.LAUNCH_PROTECT_ENABLED, true)) {
                AFLogger.afInfoLog("Allowing multiple launches within a 5 second time window.");
            } else if (m31()) {
                return;
            }
            this.f58 = System.currentTimeMillis();
        }
        ScheduledThreadPoolExecutor r0 = AFExecutor.getInstance().m1();
        hVar.f222 = new WeakReference<>(hVar.f221);
        hVar.f221 = null;
        hVar.f223 = false;
        m46(r0, new c(this, hVar, (byte) 0), 150, TimeUnit.MILLISECONDS);
    }

    public void startTracking(Application application, String str) {
        startTracking(application, str, null);
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private static void m58(JSONObject jSONObject) {
        String str;
        ArrayList arrayList = new ArrayList();
        Iterator<String> keys = jSONObject.keys();
        while (true) {
            if (!keys.hasNext()) {
                break;
            }
            try {
                JSONArray jSONArray = new JSONArray((String) jSONObject.get(keys.next()));
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    arrayList.add(Long.valueOf(jSONArray.getLong(i2)));
                }
            } catch (JSONException unused) {
            }
        }
        Collections.sort(arrayList);
        Iterator<String> keys2 = jSONObject.keys();
        loop2:
        while (true) {
            str = null;
            while (keys2.hasNext() && str == null) {
                String next = keys2.next();
                try {
                    JSONArray jSONArray2 = new JSONArray((String) jSONObject.get(next));
                    String str2 = str;
                    int i3 = 0;
                    while (i3 < jSONArray2.length()) {
                        try {
                            if (jSONArray2.getLong(i3) != ((Long) arrayList.get(0)).longValue() && jSONArray2.getLong(i3) != ((Long) arrayList.get(1)).longValue() && jSONArray2.getLong(i3) != ((Long) arrayList.get(arrayList.size() - 1)).longValue()) {
                                i3++;
                                str2 = next;
                            }
                        } catch (JSONException unused2) {
                        }
                    }
                    str = str2;
                } catch (JSONException unused3) {
                }
            }
        }
        if (str != null) {
            jSONObject.remove(str);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.appsflyer.AppsFlyerLibCore.ˏ(android.content.SharedPreferences, java.lang.String, boolean):int
     arg types: [android.content.SharedPreferences, java.lang.String, int]
     candidates:
      com.appsflyer.AppsFlyerLibCore.ˏ(android.content.Context, java.lang.String, long):void
      com.appsflyer.AppsFlyerLibCore.ˏ(android.content.SharedPreferences, java.lang.String, boolean):int */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00ef  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void startTracking(android.app.Application r17, java.lang.String r18, com.appsflyer.AppsFlyerTrackingRequestListener r19) {
        /*
            r16 = this;
            r1 = r16
            r2 = r17
            r3 = r18
            r1.f56 = r2
            com.appsflyer.internal.aa r0 = com.appsflyer.internal.aa.f104
            if (r0 != 0) goto L_0x0013
            com.appsflyer.internal.aa r0 = new com.appsflyer.internal.aa
            r0.<init>()
            com.appsflyer.internal.aa.f104 = r0
        L_0x0013:
            com.appsflyer.internal.aa r0 = com.appsflyer.internal.aa.f104
            r4 = 1
            java.lang.String[] r5 = new java.lang.String[r4]
            r6 = 0
            r5[r6] = r3
            java.lang.String r7 = "startTracking"
            java.lang.String r8 = "public_api_call"
            r0.m108(r8, r7, r5)
            r5 = 2
            java.lang.Object[] r0 = new java.lang.Object[r5]
            java.lang.String r7 = "4.10.3"
            r0[r6] = r7
            java.lang.String r7 = "233"
            r0[r4] = r7
            java.lang.String r7 = "Starting AppsFlyer Tracking: (v%s.%s)"
            java.lang.String r0 = java.lang.String.format(r7, r0)
            com.appsflyer.AFLogger.afInfoLog(r0)
            java.lang.String r0 = "Build Number: 233"
            com.appsflyer.AFLogger.afInfoLog(r0)
            com.appsflyer.AppsFlyerProperties r0 = com.appsflyer.AppsFlyerProperties.getInstance()
            android.content.Context r7 = r17.getApplicationContext()
            r0.loadProperties(r7)
            boolean r0 = android.text.TextUtils.isEmpty(r18)
            java.lang.String r7 = "AppsFlyerKey"
            if (r0 != 0) goto L_0x0059
            com.appsflyer.AppsFlyerProperties r0 = com.appsflyer.AppsFlyerProperties.getInstance()
            r0.set(r7, r3)
            com.appsflyer.AFFacebookDeferredDeeplink.AnonymousClass2.m4(r18)
            goto L_0x006d
        L_0x0059:
            com.appsflyer.AppsFlyerProperties r0 = com.appsflyer.AppsFlyerProperties.getInstance()
            java.lang.String r0 = r0.getString(r7)
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 == 0) goto L_0x006d
            java.lang.String r0 = "ERROR: AppsFlyer SDK is not initialized! You must provide AppsFlyer Dev-Key either in the 'init' API method (should be called on Application's onCreate),or in the startTracking API method (should be called on Activity's onCreate)."
            com.appsflyer.AFLogger.afWarnLog(r0)
            return
        L_0x006d:
            android.content.Context r0 = r17.getBaseContext()
            android.content.pm.PackageManager r7 = r0.getPackageManager()     // Catch:{ Exception -> 0x00a5 }
            java.lang.String r8 = r0.getPackageName()     // Catch:{ Exception -> 0x00a5 }
            android.content.pm.PackageInfo r7 = r7.getPackageInfo(r8, r6)     // Catch:{ Exception -> 0x00a5 }
            android.content.pm.ApplicationInfo r7 = r7.applicationInfo     // Catch:{ Exception -> 0x00a5 }
            int r7 = r7.flags     // Catch:{ Exception -> 0x00a5 }
            r8 = 32768(0x8000, float:4.5918E-41)
            r7 = r7 & r8
            if (r7 == 0) goto L_0x00bb
            android.content.res.Resources r7 = r0.getResources()     // Catch:{ Exception -> 0x00a5 }
            java.lang.String r8 = "appsflyer_backup_rules"
            java.lang.String r9 = "xml"
            java.lang.String r0 = r0.getPackageName()     // Catch:{ Exception -> 0x00a5 }
            int r0 = r7.getIdentifier(r8, r9, r0)     // Catch:{ Exception -> 0x00a5 }
            if (r0 == 0) goto L_0x009f
            java.lang.String r0 = "appsflyer_backup_rules.xml detected, using AppsFlyer defined backup rules for AppsFlyer SDK data"
            com.appsflyer.AFLogger.afInfoLog(r0, r4)     // Catch:{ Exception -> 0x00a5 }
            goto L_0x00bb
        L_0x009f:
            java.lang.String r0 = "'allowBackup' is set to true; appsflyer_backup_rules.xml not detected.\nAppsFlyer shared preferences should be excluded from auto backup by adding: <exclude domain=\"sharedpref\" path=\"appsflyer-data\"/> to the Application's <full-backup-content> rules"
            com.appsflyer.AFLogger.m12(r0)     // Catch:{ Exception -> 0x00a5 }
            goto L_0x00bb
        L_0x00a5:
            r0 = move-exception
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = "checkBackupRules Exception: "
            r7.<init>(r8)
            java.lang.String r0 = r0.toString()
            r7.append(r0)
            java.lang.String r0 = r7.toString()
            com.appsflyer.AFLogger.afRDLog(r0)
        L_0x00bb:
            android.content.Context r0 = r17.getBaseContext()
            boolean r7 = r1.f53
            if (r7 == 0) goto L_0x00ec
            org.json.JSONObject r7 = r1.f54
            if (r7 == 0) goto L_0x00cf
            int r7 = r7.length()
            if (r7 <= 0) goto L_0x00cf
            r7 = 1
            goto L_0x00d0
        L_0x00cf:
            r7 = 0
        L_0x00d0:
            if (r7 == 0) goto L_0x00d8
            boolean r7 = r16.m47()
            if (r7 == 0) goto L_0x00ec
        L_0x00d8:
            android.content.Context r0 = r0.getApplicationContext()
            java.lang.String r7 = "appsflyer-data"
            android.content.SharedPreferences r0 = r0.getSharedPreferences(r7, r6)
            java.lang.String r7 = "appsFlyerCount"
            int r0 = m48(r0, r7, r6)
            if (r0 > r5) goto L_0x00ec
            r0 = 1
            goto L_0x00ed
        L_0x00ec:
            r0 = 0
        L_0x00ed:
            if (r0 == 0) goto L_0x019c
            android.content.Context r0 = r17.getApplicationContext()
            org.json.JSONObject r7 = new org.json.JSONObject
            r7.<init>()
            r1.f54 = r7
            long r7 = java.lang.System.currentTimeMillis()
            com.appsflyer.AppsFlyerLibCore$1 r9 = new com.appsflyer.AppsFlyerLibCore$1
            r9.<init>(r7)
            java.lang.String r7 = "com.facebook.FacebookSdk"
            java.lang.Class r7 = java.lang.Class.forName(r7)     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            java.lang.String r8 = "sdkInitialize"
            java.lang.Class[] r10 = new java.lang.Class[r4]     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            java.lang.Class<android.content.Context> r11 = android.content.Context.class
            r10[r6] = r11     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            java.lang.reflect.Method r7 = r7.getMethod(r8, r10)     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            java.lang.Object[] r8 = new java.lang.Object[r4]     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            r8[r6] = r0     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            r10 = 0
            r7.invoke(r10, r8)     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            java.lang.String r7 = "com.facebook.applinks.AppLinkData"
            java.lang.Class r7 = java.lang.Class.forName(r7)     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            java.lang.String r8 = "com.facebook.applinks.AppLinkData$CompletionHandler"
            java.lang.Class r8 = java.lang.Class.forName(r8)     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            java.lang.String r11 = "fetchDeferredAppLinkData"
            r12 = 3
            java.lang.Class[] r13 = new java.lang.Class[r12]     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            java.lang.Class<android.content.Context> r14 = android.content.Context.class
            r13[r6] = r14     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            java.lang.Class<java.lang.String> r14 = java.lang.String.class
            r13[r4] = r14     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            r13[r5] = r8     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            java.lang.reflect.Method r11 = r7.getMethod(r11, r13)     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            com.appsflyer.AFFacebookDeferredDeeplink$2 r13 = new com.appsflyer.AFFacebookDeferredDeeplink$2     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            r13.<init>(r7, r9)     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            java.lang.ClassLoader r7 = r8.getClassLoader()     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            java.lang.Class[] r14 = new java.lang.Class[r4]     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            r14[r6] = r8     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            java.lang.Object r7 = java.lang.reflect.Proxy.newProxyInstance(r7, r14, r13)     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            android.content.res.Resources r8 = r0.getResources()     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            java.lang.String r13 = "facebook_app_id"
            java.lang.String r14 = "string"
            java.lang.String r15 = r0.getPackageName()     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            int r8 = r8.getIdentifier(r13, r14, r15)     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            java.lang.String r8 = r0.getString(r8)     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            boolean r13 = android.text.TextUtils.isEmpty(r8)     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            if (r13 == 0) goto L_0x016d
            java.lang.String r0 = "Facebook app id not defined in resources"
            r9.onAppLinkFetchFailed(r0)     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            goto L_0x019c
        L_0x016d:
            java.lang.Object[] r12 = new java.lang.Object[r12]     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            r12[r6] = r0     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            r12[r4] = r8     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            r12[r5] = r7     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            r11.invoke(r10, r12)     // Catch:{ NoSuchMethodException -> 0x0194, InvocationTargetException -> 0x018b, ClassNotFoundException -> 0x0182, IllegalAccessException -> 0x0179 }
            goto L_0x019c
        L_0x0179:
            r0 = move-exception
            java.lang.String r0 = r0.toString()
            r9.onAppLinkFetchFailed(r0)
            goto L_0x019c
        L_0x0182:
            r0 = move-exception
            java.lang.String r0 = r0.toString()
            r9.onAppLinkFetchFailed(r0)
            goto L_0x019c
        L_0x018b:
            r0 = move-exception
            java.lang.String r0 = r0.toString()
            r9.onAppLinkFetchFailed(r0)
            goto L_0x019c
        L_0x0194:
            r0 = move-exception
            java.lang.String r0 = r0.toString()
            r9.onAppLinkFetchFailed(r0)
        L_0x019c:
            com.appsflyer.internal.h r0 = new com.appsflyer.internal.h
            r0.<init>()
            r0.f221 = r2
            r0.f227 = r3
            r2 = r19
            r0.f225 = r2
            r1.m45(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsflyer.AppsFlyerLibCore.startTracking(android.app.Application, java.lang.String, com.appsflyer.AppsFlyerTrackingRequestListener):void");
    }

    public AppsFlyerLib init(String str, AppsFlyerConversionListener appsFlyerConversionListener) {
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa aaVar = aa.f104;
        String[] strArr = new String[2];
        strArr[0] = str;
        strArr[1] = appsFlyerConversionListener == null ? "null" : "conversionDataListener";
        aaVar.m108("public_api_call", "init", strArr);
        AFLogger.m15(String.format("Initializing AppsFlyer SDK: (v%s.%s)", BuildConfig.VERSION_NAME, "233"));
        this.f51 = true;
        AppsFlyerProperties.getInstance().set(AppsFlyerProperties.AF_KEY, str);
        AFFacebookDeferredDeeplink.AnonymousClass2.m4(str);
        f31 = appsFlyerConversionListener;
        return this;
    }

    /* access modifiers changed from: private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public static String m19(Context context, String str) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences("appsflyer-data", 0);
        if (sharedPreferences.contains("CACHED_CHANNEL")) {
            return sharedPreferences.getString("CACHED_CHANNEL", null);
        }
        m28(context, "CACHED_CHANNEL", str);
        return str;
    }

    public void trackEvent(Context context, String str, Map<String, Object> map) {
        trackEvent(context, str, map, null);
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static void m29(Context context, String str, String str2, String str3) {
        if (AppsFlyerProperties.getInstance().getBoolean(AppsFlyerProperties.IS_MONITOR, false)) {
            Intent intent = new Intent("com.appsflyer.MonitorBroadcast");
            intent.setPackage("com.appsflyer.nightvision");
            intent.putExtra(TJAdUnitConstants.String.MESSAGE, str2);
            intent.putExtra("value", str3);
            intent.putExtra(RemoteConfigConstants.RequestFieldKey.PACKAGE_NAME, "true");
            intent.putExtra(Constants.URL_MEDIA_SOURCE, new Integer(Process.myPid()));
            intent.putExtra("eventIdentifier", str);
            intent.putExtra("sdk", BuildConfig.VERSION_NAME);
            context.sendBroadcast(intent);
        }
    }

    @Deprecated
    public String getAttributionId(ContentResolver contentResolver) {
        return getAttributionId(this.f56);
    }

    /* access modifiers changed from: private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public static void m37(Context context, String str, int i2) {
        SharedPreferences.Editor edit = context.getApplicationContext().getSharedPreferences("appsflyer-data", 0).edit();
        edit.putInt(str, i2);
        edit.apply();
    }

    /* access modifiers changed from: private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public static String m64(WeakReference<Context> weakReference) {
        String string = AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.CHANNEL);
        if (string == null) {
            string = m24(weakReference, "CHANNEL");
        }
        if (string == null || !string.equals("")) {
            return string;
        }
        return null;
    }

    public void setUserEmails(String... strArr) {
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa.f104.m108("public_api_call", "setUserEmails", strArr);
        setUserEmails(AppsFlyerProperties.EmailsCryptType.NONE, strArr);
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private static Map<String, String> m52(Context context, String str) {
        String str2;
        int i2;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        String[] split = str.split("&");
        int length = split.length;
        int i3 = 0;
        boolean z = false;
        while (true) {
            str2 = "media_source";
            if (i3 < length) {
                String str3 = split[i3];
                int indexOf = str3.indexOf("=");
                String substring = indexOf > 0 ? str3.substring(0, indexOf) : str3;
                if (!linkedHashMap.containsKey(substring)) {
                    if (substring.equals(Constants.URL_CAMPAIGN)) {
                        str2 = FirebaseAnalytics.Param.CAMPAIGN;
                    } else if (!substring.equals(Constants.URL_MEDIA_SOURCE)) {
                        if (substring.equals("af_prt")) {
                            z = true;
                            str2 = "agency";
                        }
                        linkedHashMap.put(substring, "");
                    }
                    substring = str2;
                    linkedHashMap.put(substring, "");
                }
                linkedHashMap.put(substring, (indexOf <= 0 || str3.length() <= (i2 = indexOf + 1)) ? null : str3.substring(i2));
                i3++;
            } else {
                try {
                    break;
                } catch (Exception e2) {
                    AFLogger.afErrorLog("Could not fetch install time. ", e2);
                }
            }
        }
        if (!linkedHashMap.containsKey("install_time")) {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            long j2 = packageInfo.firstInstallTime;
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            linkedHashMap.put("install_time", simpleDateFormat.format(new Date(j2)));
        }
        if (!linkedHashMap.containsKey("af_status")) {
            linkedHashMap.put("af_status", "Non-organic");
        }
        if (z) {
            linkedHashMap.remove(str2);
        }
        return linkedHashMap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.appsflyer.AppsFlyerLibCore.ˏ(android.content.SharedPreferences, java.lang.String, boolean):int
     arg types: [android.content.SharedPreferences, java.lang.String, int]
     candidates:
      com.appsflyer.AppsFlyerLibCore.ˏ(android.content.Context, java.lang.String, long):void
      com.appsflyer.AppsFlyerLibCore.ˏ(android.content.SharedPreferences, java.lang.String, boolean):int */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m75(Context context, String str) {
        if (AppsFlyerProperties.getInstance().getBoolean(AppsFlyerProperties.AF_WAITFOR_CUSTOMERID, false) && AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.APP_USER_ID) == null) {
            AFLogger.afInfoLog("CustomerUserId not set, Tracking is disabled", true);
            return;
        }
        HashMap hashMap = new HashMap();
        String string = AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.AF_KEY);
        if (string == null) {
            AFLogger.afWarnLog("[registerUninstall] AppsFlyer's SDK cannot send any event without providing DevKey.");
            return;
        }
        PackageManager packageManager = context.getPackageManager();
        String packageName = context.getPackageName();
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(packageName, 0);
            hashMap.put("app_version_code", Integer.toString(packageInfo.versionCode));
            hashMap.put("app_version_name", packageInfo.versionName);
            hashMap.put(NativeProtocol.BRIDGE_ARG_APP_NAME_STRING, packageManager.getApplicationLabel(packageInfo.applicationInfo).toString());
            long j2 = packageInfo.firstInstallTime;
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd_HHmmssZ", Locale.US);
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            hashMap.put("installDate", simpleDateFormat.format(new Date(j2)));
        } catch (Throwable th) {
            AFLogger.afErrorLog("Exception while collecting application version info.", th);
        }
        m55(context, hashMap);
        String string2 = AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.APP_USER_ID);
        if (string2 != null) {
            hashMap.put("appUserId", string2);
        }
        try {
            hashMap.put("model", Build.MODEL);
            hashMap.put("brand", Build.BRAND);
        } catch (Throwable th2) {
            AFLogger.afErrorLog("Exception while collecting device brand and model.", th2);
        }
        if (AppsFlyerProperties.getInstance().getBoolean(AppsFlyerProperties.DEVICE_TRACKING_DISABLED, false)) {
            hashMap.put(AppsFlyerProperties.DEVICE_TRACKING_DISABLED, "true");
        }
        s r2 = p.m178(context.getContentResolver());
        if (r2 != null) {
            hashMap.put("amazon_aid", r2.f271);
            hashMap.put("amazon_aid_limit", String.valueOf(r2.f272));
        }
        String string3 = AppsFlyerProperties.getInstance().getString(ServerParameters.ADVERTISING_ID_PARAM);
        if (string3 != null) {
            hashMap.put(ServerParameters.ADVERTISING_ID_PARAM, string3);
        }
        hashMap.put("devkey", string);
        hashMap.put("uid", ab.m111(new WeakReference(context)));
        hashMap.put("af_gcm_token", str);
        hashMap.put("launch_counter", Integer.toString(m48(context.getApplicationContext().getSharedPreferences("appsflyer-data", 0), "appsFlyerCount", false)));
        hashMap.put("sdk", Integer.toString(Build.VERSION.SDK_INT));
        String r13 = m64(new WeakReference(context));
        if (r13 != null) {
            hashMap.put(AppsFlyerProperties.CHANNEL, r13);
        }
        try {
            x xVar = new x(context, isTrackingStopped());
            xVar.f296 = hashMap;
            StringBuilder sb = new StringBuilder();
            sb.append(ServerConfigHandler.getUrl(f30));
            sb.append(packageName);
            xVar.execute(sb.toString());
        } catch (Throwable th3) {
            AFLogger.afErrorLog(th3.getMessage(), th3);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0057, code lost:
        if (r3 != null) goto L_0x002c;
     */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0054 A[SYNTHETIC, Splitter:B:23:0x0054] */
    /* renamed from: ॱ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String m65(java.net.HttpURLConnection r7) {
        /*
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r1 = 0
            java.io.InputStream r2 = r7.getErrorStream()     // Catch:{ all -> 0x0037 }
            if (r2 != 0) goto L_0x0010
            java.io.InputStream r2 = r7.getInputStream()     // Catch:{ all -> 0x0037 }
        L_0x0010:
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ all -> 0x0037 }
            r3.<init>(r2)     // Catch:{ all -> 0x0037 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ all -> 0x0035 }
            r2.<init>(r3)     // Catch:{ all -> 0x0035 }
        L_0x001a:
            java.lang.String r1 = r2.readLine()     // Catch:{ all -> 0x0030 }
            if (r1 == 0) goto L_0x0029
            r0.append(r1)     // Catch:{ all -> 0x0030 }
            r1 = 10
            r0.append(r1)     // Catch:{ all -> 0x0030 }
            goto L_0x001a
        L_0x0029:
            r2.close()     // Catch:{ all -> 0x005a }
        L_0x002c:
            r3.close()     // Catch:{ all -> 0x005a }
            goto L_0x005a
        L_0x0030:
            r1 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
            goto L_0x0039
        L_0x0035:
            r2 = move-exception
            goto L_0x0039
        L_0x0037:
            r2 = move-exception
            r3 = r1
        L_0x0039:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x007d }
            java.lang.String r5 = "Could not read connection response from: "
            r4.<init>(r5)     // Catch:{ all -> 0x007d }
            java.net.URL r7 = r7.getURL()     // Catch:{ all -> 0x007d }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x007d }
            r4.append(r7)     // Catch:{ all -> 0x007d }
            java.lang.String r7 = r4.toString()     // Catch:{ all -> 0x007d }
            com.appsflyer.AFLogger.afErrorLog(r7, r2)     // Catch:{ all -> 0x007d }
            if (r1 == 0) goto L_0x0057
            r1.close()     // Catch:{ all -> 0x005a }
        L_0x0057:
            if (r3 == 0) goto L_0x005a
            goto L_0x002c
        L_0x005a:
            java.lang.String r7 = r0.toString()
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0064 }
            r0.<init>(r7)     // Catch:{ JSONException -> 0x0064 }
            return r7
        L_0x0064:
            org.json.JSONObject r0 = new org.json.JSONObject
            r0.<init>()
            java.lang.String r1 = "string_response"
            r0.put(r1, r7)     // Catch:{ JSONException -> 0x0073 }
            java.lang.String r7 = r0.toString()     // Catch:{ JSONException -> 0x0073 }
            return r7
        L_0x0073:
            org.json.JSONObject r7 = new org.json.JSONObject
            r7.<init>()
            java.lang.String r7 = r7.toString()
            return r7
        L_0x007d:
            r7 = move-exception
            if (r1 == 0) goto L_0x0083
            r1.close()     // Catch:{ all -> 0x0088 }
        L_0x0083:
            if (r3 == 0) goto L_0x0088
            r3.close()     // Catch:{ all -> 0x0088 }
        L_0x0088:
            goto L_0x008a
        L_0x0089:
            throw r7
        L_0x008a:
            goto L_0x0089
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsflyer.AppsFlyerLibCore.m65(java.net.HttpURLConnection):java.lang.String");
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean m31() {
        if (this.f58 > 0) {
            long currentTimeMillis = System.currentTimeMillis() - this.f58;
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS Z", Locale.US);
            long j2 = this.f58;
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            String format = simpleDateFormat.format(new Date(j2));
            long j3 = this.f61;
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            String format2 = simpleDateFormat.format(new Date(j3));
            if (currentTimeMillis < this.f35 && !isTrackingStopped()) {
                AFLogger.afInfoLog(String.format(Locale.US, "Last Launch attempt: %s;\nLast successful Launch event: %s;\nThis launch is blocked: %s ms < %s ms", format, format2, Long.valueOf(currentTimeMillis), Long.valueOf(this.f35)));
                return true;
            } else if (!isTrackingStopped()) {
                AFLogger.afInfoLog(String.format(Locale.US, "Last Launch attempt: %s;\nLast successful Launch event: %s;\nSending launch (+%s ms)", format, format2, Long.valueOf(currentTimeMillis)));
            }
        } else if (!isTrackingStopped()) {
            AFLogger.afInfoLog("Sending first launch for this session!");
        }
        return false;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static String m24(WeakReference<Context> weakReference, String str) {
        if (weakReference.get() == null) {
            return null;
        }
        return m39(str, weakReference.get().getPackageManager(), weakReference.get().getPackageName());
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static File m23(String str) {
        if (str == null) {
            return null;
        }
        try {
            if (str.trim().length() > 0) {
                return new File(str.trim());
            }
            return null;
        } catch (Throwable th) {
            AFLogger.afErrorLog(th.getMessage(), th);
            return null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public static void m68(Context context, String str, long j2) {
        SharedPreferences.Editor edit = context.getApplicationContext().getSharedPreferences("appsflyer-data", 0).edit();
        edit.putLong(str, j2);
        edit.apply();
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private void m54(Context context, String str, String str2, Intent intent) {
        h hVar = new h();
        hVar.f221 = context;
        hVar.f216 = null;
        hVar.f227 = str;
        hVar.f228 = null;
        hVar.f226 = str2;
        hVar.f217 = intent;
        hVar.f213 = null;
        m70(hVar);
    }

    /* access modifiers changed from: private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public static void m28(Context context, String str, String str2) {
        SharedPreferences.Editor edit = context.getApplicationContext().getSharedPreferences("appsflyer-data", 0).edit();
        edit.putString(str, str2);
        edit.apply();
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    static void m43(Context context, String str) {
        JSONObject jSONObject;
        JSONArray jSONArray;
        AFLogger.afDebugLog("received a new (extra) referrer: ".concat(String.valueOf(str)));
        try {
            long currentTimeMillis = System.currentTimeMillis();
            String string = context.getApplicationContext().getSharedPreferences("appsflyer-data", 0).getString("extraReferrers", null);
            if (string == null) {
                JSONObject jSONObject2 = new JSONObject();
                jSONObject = jSONObject2;
                jSONArray = new JSONArray();
            } else {
                jSONObject = new JSONObject(string);
                if (jSONObject.has(str)) {
                    jSONArray = new JSONArray((String) jSONObject.get(str));
                } else {
                    jSONArray = new JSONArray();
                }
            }
            if (((long) jSONArray.length()) < 5) {
                jSONArray.put(currentTimeMillis);
            }
            if (((long) jSONObject.length()) >= 4) {
                m58(jSONObject);
            }
            jSONObject.put(str, jSONArray.toString());
            String jSONObject3 = jSONObject.toString();
            SharedPreferences.Editor edit = context.getApplicationContext().getSharedPreferences("appsflyer-data", 0).edit();
            edit.putString("extraReferrers", jSONObject3);
            edit.apply();
        } catch (JSONException unused) {
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder("Couldn't save referrer - ");
            sb.append(str);
            sb.append(": ");
            AFLogger.afErrorLog(sb.toString(), th);
        }
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    private static void m67(Context context) {
        int i2;
        if ("OPPO".equals(Build.BRAND)) {
            i2 = 23;
            AFLogger.afRDLog("OPPO device found");
        } else {
            i2 = 18;
        }
        if (Build.VERSION.SDK_INT < i2 || AppsFlyerProperties.getInstance().getBoolean(AppsFlyerProperties.DISABLE_KEYSTORE, false)) {
            StringBuilder sb = new StringBuilder("OS SDK is=");
            sb.append(Build.VERSION.SDK_INT);
            sb.append("; no KeyStore usage");
            AFLogger.afRDLog(sb.toString());
            return;
        }
        StringBuilder sb2 = new StringBuilder("OS SDK is=");
        sb2.append(Build.VERSION.SDK_INT);
        sb2.append("; use KeyStore");
        AFLogger.afRDLog(sb2.toString());
        AFKeystoreWrapper aFKeystoreWrapper = new AFKeystoreWrapper(context);
        if (!aFKeystoreWrapper.m10()) {
            aFKeystoreWrapper.f19 = ab.m111(new WeakReference(context));
            aFKeystoreWrapper.f16 = 0;
            aFKeystoreWrapper.m9(aFKeystoreWrapper.m6());
        } else {
            String r4 = aFKeystoreWrapper.m6();
            synchronized (aFKeystoreWrapper.f20) {
                aFKeystoreWrapper.f16++;
                AFLogger.afInfoLog("Deleting key with alias: ".concat(String.valueOf(r4)));
                try {
                    synchronized (aFKeystoreWrapper.f20) {
                        aFKeystoreWrapper.f18.deleteEntry(r4);
                    }
                } catch (KeyStoreException e2) {
                    StringBuilder sb3 = new StringBuilder("Exception ");
                    sb3.append(e2.getMessage());
                    sb3.append(" occurred");
                    AFLogger.afErrorLog(sb3.toString(), e2);
                }
            }
            aFKeystoreWrapper.m9(aFKeystoreWrapper.m6());
        }
        AppsFlyerProperties.getInstance().set("KSAppsFlyerId", aFKeystoreWrapper.m7());
        AppsFlyerProperties.getInstance().set("KSAppsFlyerRICounter", String.valueOf(aFKeystoreWrapper.m8()));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.appsflyer.AppsFlyerLibCore.ˏ(android.content.SharedPreferences, java.lang.String, boolean):int
     arg types: [android.content.SharedPreferences, java.lang.String, int]
     candidates:
      com.appsflyer.AppsFlyerLibCore.ˏ(android.content.Context, java.lang.String, long):void
      com.appsflyer.AppsFlyerLibCore.ˏ(android.content.SharedPreferences, java.lang.String, boolean):int */
    /* renamed from: ˊ  reason: contains not printable characters */
    private static boolean m32(Context context) {
        if (m48(context.getApplicationContext().getSharedPreferences("appsflyer-data", 0), "appsFlyerCount", false) > 2) {
            AFLogger.afRDLog("Install referrer will not load, the counter > 2, ");
            return false;
        }
        try {
            Class.forName("com.android.installreferrer.api.InstallReferrerClient");
            if (AFExecutor.AnonymousClass2.AnonymousClass1.m3(context, "com.google.android.finsky.permission.BIND_GET_INSTALL_REFERRER_SERVICE")) {
                AFLogger.afDebugLog("Install referrer is allowed");
                return true;
            }
            AFLogger.afDebugLog("Install referrer is not allowed");
            return false;
        } catch (ClassNotFoundException unused) {
            AFLogger.afRDLog("Class com.android.installreferrer.api.InstallReferrerClient not found");
            return false;
        } catch (Throwable th) {
            AFLogger.afErrorLog("An error occurred while trying to verify manifest : com.android.installreferrer.api.InstallReferrerClient", th);
            return false;
        }
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private static void m55(Context context, Map<String, ? super String> map) {
        l.m160();
        l.a r2 = l.m161(context);
        map.put("network", r2.f230);
        String str = r2.f231;
        if (str != null) {
            map.put("operator", str);
        }
        String str2 = r2.f229;
        if (str2 != null) {
            map.put("carrier", str2);
        }
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private static String m50(String str) {
        try {
            return (String) Class.forName("android.os.SystemProperties").getMethod("get", String.class).invoke(null, str);
        } catch (Throwable th) {
            AFLogger.afErrorLog(th.getMessage(), th);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0051  */
    /* renamed from: ˊ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m73(com.appsflyer.internal.h r8) {
        /*
            r7 = this;
            android.content.Context r0 = r8.f221
            r1 = 0
            if (r0 == 0) goto L_0x0006
            goto L_0x0012
        L_0x0006:
            java.lang.ref.WeakReference<android.content.Context> r0 = r8.f222
            if (r0 == 0) goto L_0x0011
            java.lang.Object r0 = r0.get()
            android.content.Context r0 = (android.content.Context) r0
            goto L_0x0012
        L_0x0011:
            r0 = r1
        L_0x0012:
            boolean r2 = r0 instanceof android.app.Activity
            java.lang.String r3 = ""
            if (r2 == 0) goto L_0x003d
            r2 = r0
            android.app.Activity r2 = (android.app.Activity) r2
            android.content.Intent r4 = r2.getIntent()
            com.appsflyer.internal.g r5 = com.appsflyer.internal.g.m154()
            int r6 = java.lang.System.identityHashCode(r2)
            r5.f208 = r6
            android.net.Uri r1 = androidx.core.app.a.c(r2)     // Catch:{ all -> 0x002e }
            goto L_0x0036
        L_0x002e:
            r2 = move-exception
            java.lang.String r5 = r2.getLocalizedMessage()
            com.appsflyer.AFLogger.afErrorLog(r5, r2)
        L_0x0036:
            if (r1 == 0) goto L_0x003e
            java.lang.String r1 = r1.toString()
            goto L_0x003f
        L_0x003d:
            r4 = r1
        L_0x003e:
            r1 = r3
        L_0x003f:
            com.appsflyer.AppsFlyerProperties r2 = com.appsflyer.AppsFlyerProperties.getInstance()
            java.lang.String r5 = "AppsFlyerKey"
            java.lang.String r2 = r2.getString(r5)
            if (r2 != 0) goto L_0x0051
            java.lang.String r8 = "[TrackEvent/Launch] AppsFlyer's SDK cannot send any event without providing DevKey."
            com.appsflyer.AFLogger.afWarnLog(r8)
            return
        L_0x0051:
            com.appsflyer.AppsFlyerProperties r2 = com.appsflyer.AppsFlyerProperties.getInstance()
            java.lang.String r0 = r2.getReferrer(r0)
            if (r0 != 0) goto L_0x005c
            r0 = r3
        L_0x005c:
            r8.f226 = r0
            r8.f217 = r4
            r8.f213 = r1
            r7.m70(r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsflyer.AppsFlyerLibCore.m73(com.appsflyer.internal.h):void");
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public static SharedPreferences m49(Context context) {
        return context.getApplicationContext().getSharedPreferences("appsflyer-data", 0);
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static int m48(SharedPreferences sharedPreferences, String str, boolean z) {
        int i2 = sharedPreferences.getInt(str, 0);
        if (z) {
            i2++;
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putInt(str, i2);
            edit.apply();
        }
        if (aa.f104 == null) {
            aa.f104 = new aa();
        }
        aa aaVar = aa.f104;
        if (aaVar.f126) {
            if (aaVar == null) {
                aa.f104 = new aa();
            }
            aa.f104.m110(String.valueOf(i2));
        }
        return i2;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean m47() {
        try {
            return TextUtils.isEmpty((String) this.f54.get("link"));
        } catch (JSONException e2) {
            AFLogger.afErrorLog("JSONException while checking \"link\" value for fb_ddl", e2);
            return true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.appsflyer.AppsFlyerLibCore.ˏ(android.content.SharedPreferences, java.lang.String, boolean):int
     arg types: [android.content.SharedPreferences, java.lang.String, int]
     candidates:
      com.appsflyer.AppsFlyerLibCore.ˏ(android.content.Context, java.lang.String, long):void
      com.appsflyer.AppsFlyerLibCore.ˏ(android.content.SharedPreferences, java.lang.String, boolean):int */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x0307 A[Catch:{ Exception -> 0x0320, all -> 0x02b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x031a A[Catch:{ Exception -> 0x0320, all -> 0x02b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:149:0x034f A[Catch:{ Exception -> 0x0320, all -> 0x02b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x0376 A[Catch:{ Exception -> 0x0320, all -> 0x02b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x037e A[Catch:{ Exception -> 0x0320, all -> 0x02b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x039c A[Catch:{ Exception -> 0x0320, all -> 0x02b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:168:0x03ba A[Catch:{ Exception -> 0x0320, all -> 0x02b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:181:0x03f1 A[Catch:{ Exception -> 0x0320, all -> 0x02b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:183:0x0405 A[Catch:{ Exception -> 0x0320, all -> 0x02b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:189:0x0411 A[Catch:{ Exception -> 0x0320, all -> 0x02b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:191:0x0419 A[Catch:{ Exception -> 0x0320, all -> 0x02b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:197:0x0425 A[Catch:{ Exception -> 0x0320, all -> 0x02b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:199:0x042d A[Catch:{ Exception -> 0x0320, all -> 0x02b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:205:0x0439 A[Catch:{ Exception -> 0x0320, all -> 0x02b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:210:0x0448 A[Catch:{ Exception -> 0x0320, all -> 0x02b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:211:0x0449 A[Catch:{ Exception -> 0x0320, all -> 0x02b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:217:0x0464 A[Catch:{ Exception -> 0x0320, all -> 0x02b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:220:0x0473 A[Catch:{ Exception -> 0x0320, all -> 0x02b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:223:0x0480 A[SYNTHETIC, Splitter:B:223:0x0480] */
    /* JADX WARNING: Removed duplicated region for block: B:229:0x0496 A[Catch:{ Exception -> 0x0320, all -> 0x02b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:234:0x04ab A[Catch:{ Exception -> 0x0320, all -> 0x02b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:237:0x04bc A[Catch:{ Exception -> 0x0320, all -> 0x02b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:238:0x04c2 A[Catch:{ Exception -> 0x0320, all -> 0x02b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:242:0x04d9 A[Catch:{ Exception -> 0x0320, all -> 0x02b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:247:0x04f1 A[Catch:{ Exception -> 0x0320, all -> 0x02b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:250:0x050c A[Catch:{ Exception -> 0x0320, all -> 0x02b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:256:0x053a A[Catch:{ Exception -> 0x0320, all -> 0x02b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:261:0x055b A[SYNTHETIC, Splitter:B:261:0x055b] */
    /* JADX WARNING: Removed duplicated region for block: B:271:0x057b A[Catch:{ all -> 0x0c0f, all -> 0x0c05, NameNotFoundException -> 0x0573, all -> 0x056a, all -> 0x0c67 }] */
    /* JADX WARNING: Removed duplicated region for block: B:275:0x058f  */
    /* JADX WARNING: Removed duplicated region for block: B:278:0x0596 A[Catch:{ all -> 0x0c0f, all -> 0x0c05, NameNotFoundException -> 0x0573, all -> 0x056a, all -> 0x0c67 }] */
    /* JADX WARNING: Removed duplicated region for block: B:310:0x062d A[Catch:{ all -> 0x0c0f, all -> 0x0c05, NameNotFoundException -> 0x0573, all -> 0x056a, all -> 0x0c67 }] */
    /* JADX WARNING: Removed duplicated region for block: B:311:0x0638 A[Catch:{ all -> 0x0c0f, all -> 0x0c05, NameNotFoundException -> 0x0573, all -> 0x056a, all -> 0x0c67 }] */
    /* JADX WARNING: Removed duplicated region for block: B:314:0x0651 A[Catch:{ all -> 0x0c0f, all -> 0x0c05, NameNotFoundException -> 0x0573, all -> 0x056a, all -> 0x0c67 }] */
    /* JADX WARNING: Removed duplicated region for block: B:336:0x069e A[Catch:{ all -> 0x0c0f, all -> 0x0c05, NameNotFoundException -> 0x0573, all -> 0x056a, all -> 0x0c67 }] */
    /* JADX WARNING: Removed duplicated region for block: B:339:0x06a4 A[Catch:{ all -> 0x0c0f, all -> 0x0c05, NameNotFoundException -> 0x0573, all -> 0x056a, all -> 0x0c67 }] */
    /* JADX WARNING: Removed duplicated region for block: B:340:0x06af A[Catch:{ all -> 0x0c0f, all -> 0x0c05, NameNotFoundException -> 0x0573, all -> 0x056a, all -> 0x0c67 }] */
    /* JADX WARNING: Removed duplicated region for block: B:343:0x06ca A[Catch:{ all -> 0x0c0f, all -> 0x0c05, NameNotFoundException -> 0x0573, all -> 0x056a, all -> 0x0c67 }] */
    /* JADX WARNING: Removed duplicated region for block: B:344:0x06cd A[ADDED_TO_REGION, Catch:{ all -> 0x0c0f, all -> 0x0c05, NameNotFoundException -> 0x0573, all -> 0x056a, all -> 0x0c67 }] */
    /* JADX WARNING: Removed duplicated region for block: B:352:0x06f0 A[Catch:{ all -> 0x0c0f, all -> 0x0c05, NameNotFoundException -> 0x0573, all -> 0x056a, all -> 0x0c67 }] */
    /* JADX WARNING: Removed duplicated region for block: B:356:0x0705 A[Catch:{ Exception -> 0x070b }] */
    /* JADX WARNING: Removed duplicated region for block: B:395:0x07bf A[Catch:{ all -> 0x0870 }] */
    /* JADX WARNING: Removed duplicated region for block: B:407:0x0828 A[Catch:{ all -> 0x0862 }] */
    /* JADX WARNING: Removed duplicated region for block: B:428:0x088d A[Catch:{ all -> 0x0c0f, all -> 0x0c05, NameNotFoundException -> 0x0573, all -> 0x056a, all -> 0x0c67 }] */
    /* JADX WARNING: Removed duplicated region for block: B:431:0x089d A[Catch:{ all -> 0x0c0f, all -> 0x0c05, NameNotFoundException -> 0x0573, all -> 0x056a, all -> 0x0c67 }] */
    /* JADX WARNING: Removed duplicated region for block: B:434:0x08bf A[Catch:{ all -> 0x0c0f, all -> 0x0c05, NameNotFoundException -> 0x0573, all -> 0x056a, all -> 0x0c67 }] */
    /* JADX WARNING: Removed duplicated region for block: B:436:0x08c8 A[Catch:{ all -> 0x0c0f, all -> 0x0c05, NameNotFoundException -> 0x0573, all -> 0x056a, all -> 0x0c67 }] */
    /* JADX WARNING: Removed duplicated region for block: B:441:0x08fa A[Catch:{ all -> 0x0c0f, all -> 0x0c05, NameNotFoundException -> 0x0573, all -> 0x056a, all -> 0x0c67 }] */
    /* JADX WARNING: Removed duplicated region for block: B:444:0x0900 A[Catch:{ all -> 0x0c0f, all -> 0x0c05, NameNotFoundException -> 0x0573, all -> 0x056a, all -> 0x0c67 }] */
    /* JADX WARNING: Removed duplicated region for block: B:447:0x092b A[ADDED_TO_REGION, Catch:{ all -> 0x0c0f, all -> 0x0c05, NameNotFoundException -> 0x0573, all -> 0x056a, all -> 0x0c67 }] */
    /* JADX WARNING: Removed duplicated region for block: B:453:0x093a A[Catch:{ all -> 0x0c0f, all -> 0x0c05, NameNotFoundException -> 0x0573, all -> 0x056a, all -> 0x0c67 }] */
    /* JADX WARNING: Removed duplicated region for block: B:456:0x0965 A[Catch:{ all -> 0x0c0f, all -> 0x0c05, NameNotFoundException -> 0x0573, all -> 0x056a, all -> 0x0c67 }] */
    /* JADX WARNING: Removed duplicated region for block: B:462:0x098b A[Catch:{ all -> 0x0c0f, all -> 0x0c05, NameNotFoundException -> 0x0573, all -> 0x056a, all -> 0x0c67 }] */
    /* JADX WARNING: Removed duplicated region for block: B:472:0x09e9 A[Catch:{ all -> 0x0c0f, all -> 0x0c05, NameNotFoundException -> 0x0573, all -> 0x056a, all -> 0x0c67 }] */
    /* JADX WARNING: Removed duplicated region for block: B:473:0x09eb A[Catch:{ all -> 0x0c0f, all -> 0x0c05, NameNotFoundException -> 0x0573, all -> 0x056a, all -> 0x0c67 }] */
    /* JADX WARNING: Removed duplicated region for block: B:480:0x0a12 A[Catch:{ all -> 0x0c0f, all -> 0x0c05, NameNotFoundException -> 0x0573, all -> 0x056a, all -> 0x0c67 }] */
    /* JADX WARNING: Removed duplicated region for block: B:483:0x0a20 A[Catch:{ all -> 0x0c0f, all -> 0x0c05, NameNotFoundException -> 0x0573, all -> 0x056a, all -> 0x0c67 }] */
    /* JADX WARNING: Removed duplicated region for block: B:484:0x0a22 A[Catch:{ all -> 0x0c0f, all -> 0x0c05, NameNotFoundException -> 0x0573, all -> 0x056a, all -> 0x0c67 }] */
    /* JADX WARNING: Removed duplicated region for block: B:487:0x0a5d A[Catch:{ all -> 0x0c0f, all -> 0x0c05, NameNotFoundException -> 0x0573, all -> 0x056a, all -> 0x0c67 }] */
    /* JADX WARNING: Removed duplicated region for block: B:528:0x0c21 A[Catch:{ all -> 0x0c0f, all -> 0x0c05, NameNotFoundException -> 0x0573, all -> 0x056a, all -> 0x0c67 }] */
    /* JADX WARNING: Removed duplicated region for block: B:531:0x0c3d A[Catch:{ all -> 0x0c0f, all -> 0x0c05, NameNotFoundException -> 0x0573, all -> 0x056a, all -> 0x0c67 }] */
    /* renamed from: ˋ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.Map<java.lang.String, java.lang.Object> m74(com.appsflyer.internal.h r30) {
        /*
            r29 = this;
            r1 = r29
            r2 = r30
            java.lang.String r3 = "prev_event_name"
            java.lang.String r4 = "AppsFlyer_4.10.3"
            java.lang.String r5 = "preInstallName"
            android.content.Context r6 = r2.f221
            if (r6 == 0) goto L_0x000f
            goto L_0x001b
        L_0x000f:
            java.lang.ref.WeakReference<android.content.Context> r6 = r2.f222
            if (r6 == 0) goto L_0x001a
            java.lang.Object r6 = r6.get()
            android.content.Context r6 = (android.content.Context) r6
            goto L_0x001b
        L_0x001a:
            r6 = 0
        L_0x001b:
            java.lang.String r8 = r2.f227
            java.lang.String r9 = r2.f216
            org.json.JSONObject r10 = new org.json.JSONObject
            java.util.Map<java.lang.String, java.lang.Object> r11 = r2.f228
            if (r11 != 0) goto L_0x002a
            java.util.HashMap r11 = new java.util.HashMap
            r11.<init>()
        L_0x002a:
            r10.<init>(r11)
            java.lang.String r10 = r10.toString()
            java.lang.String r11 = r2.f226
            android.content.Context r12 = r6.getApplicationContext()
            java.lang.String r13 = "appsflyer-data"
            r14 = 0
            android.content.SharedPreferences r12 = r12.getSharedPreferences(r13, r14)
            boolean r15 = r2.f214
            android.content.Intent r14 = r2.f217
            java.lang.String r2 = r2.f213
            java.util.HashMap r7 = new java.util.HashMap
            r7.<init>()
            com.appsflyer.internal.p.m177(r6, r7)
            java.util.Date r16 = new java.util.Date
            r16.<init>()
            r17 = r10
            r18 = r11
            long r10 = r16.getTime()
            r16 = r2
            java.lang.String r2 = java.lang.Long.toString(r10)
            r19 = r14
            java.lang.String r14 = "af_timestamp"
            r7.put(r14, r2)
            java.lang.String r2 = com.appsflyer.internal.b.m133(r6, r10)
            if (r2 == 0) goto L_0x0071
            java.lang.String r10 = "cksm_v1"
            r7.put(r10, r2)
        L_0x0071:
            boolean r2 = r29.isTrackingStopped()     // Catch:{ all -> 0x0c69 }
            if (r2 != 0) goto L_0x008f
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0c69 }
            java.lang.String r10 = "******* sendTrackingWithEvent: "
            r2.<init>(r10)     // Catch:{ all -> 0x0c69 }
            if (r15 == 0) goto L_0x0083
            java.lang.String r10 = "Launch"
            goto L_0x0084
        L_0x0083:
            r10 = r9
        L_0x0084:
            r2.append(r10)     // Catch:{ all -> 0x0c69 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0c69 }
            com.appsflyer.AFLogger.afInfoLog(r2)     // Catch:{ all -> 0x0c69 }
            goto L_0x0094
        L_0x008f:
            java.lang.String r2 = "SDK tracking has been stopped"
            com.appsflyer.AFLogger.afInfoLog(r2)     // Catch:{ all -> 0x0c69 }
        L_0x0094:
            java.lang.String r2 = "EVENT_CREATED_WITH_NAME"
            if (r15 == 0) goto L_0x009b
            java.lang.String r10 = "Launch"
            goto L_0x009c
        L_0x009b:
            r10 = r9
        L_0x009c:
            m29(r6, r4, r2, r10)     // Catch:{ all -> 0x0c69 }
            com.appsflyer.internal.w.m193()     // Catch:{ all -> 0x0c69 }
            java.io.File r2 = com.appsflyer.internal.w.m191(r6)     // Catch:{ Exception -> 0x00b4 }
            boolean r2 = r2.exists()     // Catch:{ Exception -> 0x00b4 }
            if (r2 != 0) goto L_0x00b9
            java.io.File r2 = com.appsflyer.internal.w.m191(r6)     // Catch:{ Exception -> 0x00b4 }
            r2.mkdir()     // Catch:{ Exception -> 0x00b4 }
            goto L_0x00b9
        L_0x00b4:
            java.lang.String r2 = "Could not create cache directory"
            android.util.Log.i(r4, r2)     // Catch:{ all -> 0x0c69 }
        L_0x00b9:
            android.content.pm.PackageManager r2 = r6.getPackageManager()     // Catch:{ Exception -> 0x00fb }
            java.lang.String r10 = r6.getPackageName()     // Catch:{ Exception -> 0x00fb }
            r11 = 4096(0x1000, float:5.74E-42)
            android.content.pm.PackageInfo r2 = r2.getPackageInfo(r10, r11)     // Catch:{ Exception -> 0x00fb }
            java.lang.String[] r2 = r2.requestedPermissions     // Catch:{ Exception -> 0x00fb }
            java.util.List r2 = java.util.Arrays.asList(r2)     // Catch:{ Exception -> 0x00fb }
            java.lang.String r10 = "android.permission.INTERNET"
            boolean r10 = r2.contains(r10)     // Catch:{ Exception -> 0x00fb }
            if (r10 != 0) goto L_0x00e0
            java.lang.String r10 = "Permission android.permission.INTERNET is missing in the AndroidManifest.xml"
            com.appsflyer.AFLogger.afWarnLog(r10)     // Catch:{ Exception -> 0x00fb }
            java.lang.String r10 = "PERMISSION_INTERNET_MISSING"
            r11 = 0
            m29(r6, r11, r10, r11)     // Catch:{ Exception -> 0x00fb }
        L_0x00e0:
            java.lang.String r10 = "android.permission.ACCESS_NETWORK_STATE"
            boolean r10 = r2.contains(r10)     // Catch:{ Exception -> 0x00fb }
            if (r10 != 0) goto L_0x00ed
            java.lang.String r10 = "Permission android.permission.ACCESS_NETWORK_STATE is missing in the AndroidManifest.xml"
            com.appsflyer.AFLogger.afWarnLog(r10)     // Catch:{ Exception -> 0x00fb }
        L_0x00ed:
            java.lang.String r10 = "android.permission.ACCESS_WIFI_STATE"
            boolean r2 = r2.contains(r10)     // Catch:{ Exception -> 0x00fb }
            if (r2 != 0) goto L_0x0102
            java.lang.String r2 = "Permission android.permission.ACCESS_WIFI_STATE is missing in the AndroidManifest.xml"
            com.appsflyer.AFLogger.afWarnLog(r2)     // Catch:{ Exception -> 0x00fb }
            goto L_0x0102
        L_0x00fb:
            r0 = move-exception
            r2 = r0
            java.lang.String r10 = "Exception while validation permissions. "
            com.appsflyer.AFLogger.afErrorLog(r10, r2)     // Catch:{ all -> 0x0c69 }
        L_0x0102:
            java.lang.String r2 = "af_events_api"
            java.lang.String r10 = "1"
            r7.put(r2, r10)     // Catch:{ all -> 0x0c69 }
            java.lang.String r2 = "brand"
            java.lang.String r10 = android.os.Build.BRAND     // Catch:{ all -> 0x0c69 }
            r7.put(r2, r10)     // Catch:{ all -> 0x0c69 }
            java.lang.String r2 = "device"
            java.lang.String r10 = android.os.Build.DEVICE     // Catch:{ all -> 0x0c69 }
            r7.put(r2, r10)     // Catch:{ all -> 0x0c69 }
            java.lang.String r2 = "product"
            java.lang.String r10 = android.os.Build.PRODUCT     // Catch:{ all -> 0x0c69 }
            r7.put(r2, r10)     // Catch:{ all -> 0x0c69 }
            java.lang.String r2 = "sdk"
            int r10 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x0c69 }
            java.lang.String r10 = java.lang.Integer.toString(r10)     // Catch:{ all -> 0x0c69 }
            r7.put(r2, r10)     // Catch:{ all -> 0x0c69 }
            java.lang.String r2 = "model"
            java.lang.String r10 = android.os.Build.MODEL     // Catch:{ all -> 0x0c69 }
            r7.put(r2, r10)     // Catch:{ all -> 0x0c69 }
            java.lang.String r2 = "deviceType"
            java.lang.String r10 = android.os.Build.TYPE     // Catch:{ all -> 0x0c69 }
            r7.put(r2, r10)     // Catch:{ all -> 0x0c69 }
            java.lang.String r2 = "window"
            java.lang.Object r2 = r6.getSystemService(r2)     // Catch:{ all -> 0x0c69 }
            android.view.WindowManager r2 = (android.view.WindowManager) r2     // Catch:{ all -> 0x0c69 }
            java.lang.String r11 = ""
            r10 = 1
            if (r2 == 0) goto L_0x0168
            android.view.Display r2 = r2.getDefaultDisplay()     // Catch:{ all -> 0x0c69 }
            int r2 = r2.getRotation()     // Catch:{ all -> 0x0c69 }
            if (r2 == 0) goto L_0x0161
            if (r2 == r10) goto L_0x015e
            r10 = 2
            if (r2 == r10) goto L_0x015b
            r10 = 3
            if (r2 == r10) goto L_0x0158
            r2 = r11
            goto L_0x0163
        L_0x0158:
            java.lang.String r2 = "lr"
            goto L_0x0163
        L_0x015b:
            java.lang.String r2 = "pr"
            goto L_0x0163
        L_0x015e:
            java.lang.String r2 = "l"
            goto L_0x0163
        L_0x0161:
            java.lang.String r2 = "p"
        L_0x0163:
            java.lang.String r10 = "sc_o"
            r7.put(r10, r2)     // Catch:{ all -> 0x0c69 }
        L_0x0168:
            r2 = r11
            java.lang.String r10 = "appsFlyerCount"
            if (r15 == 0) goto L_0x022a
            android.content.Context r3 = r6.getApplicationContext()     // Catch:{ all -> 0x0c69 }
            r11 = 0
            android.content.SharedPreferences r3 = r3.getSharedPreferences(r13, r11)     // Catch:{ all -> 0x0c69 }
            boolean r3 = r3.contains(r10)     // Catch:{ all -> 0x0c69 }
            r11 = 1
            r3 = r3 ^ r11
            if (r3 == 0) goto L_0x0198
            com.appsflyer.AppsFlyerProperties r3 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x0c69 }
            boolean r3 = r3.isOtherSdkStringDisabled()     // Catch:{ all -> 0x0c69 }
            if (r3 != 0) goto L_0x0195
            float r3 = m21(r6)     // Catch:{ all -> 0x0c69 }
            java.lang.String r11 = "batteryLevel"
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ all -> 0x0c69 }
            r7.put(r11, r3)     // Catch:{ all -> 0x0c69 }
        L_0x0195:
            m67(r6)     // Catch:{ all -> 0x0c69 }
        L_0x0198:
            java.lang.String r3 = "timepassedsincelastlaunch"
            android.content.Context r11 = r6.getApplicationContext()     // Catch:{ all -> 0x0c69 }
            r22 = r4
            r4 = 0
            android.content.SharedPreferences r11 = r11.getSharedPreferences(r13, r4)     // Catch:{ all -> 0x0c69 }
            java.lang.String r4 = "AppsFlyerTimePassedSincePrevLaunch"
            r24 = r14
            r23 = r15
            r14 = 0
            long r25 = r11.getLong(r4, r14)     // Catch:{ all -> 0x0c69 }
            long r14 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0c69 }
            java.lang.String r4 = "AppsFlyerTimePassedSincePrevLaunch"
            m68(r6, r4, r14)     // Catch:{ all -> 0x0c69 }
            r20 = 0
            int r4 = (r25 > r20 ? 1 : (r25 == r20 ? 0 : -1))
            if (r4 <= 0) goto L_0x01c7
            long r14 = r14 - r25
            r25 = 1000(0x3e8, double:4.94E-321)
            long r14 = r14 / r25
            goto L_0x01c9
        L_0x01c7:
            r14 = -1
        L_0x01c9:
            java.lang.String r4 = java.lang.Long.toString(r14)     // Catch:{ all -> 0x0c69 }
            r7.put(r3, r4)     // Catch:{ all -> 0x0c69 }
            com.appsflyer.AppsFlyerProperties r3 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x0c69 }
            java.lang.String r4 = "oneLinkSlug"
            java.lang.String r3 = r3.getString(r4)     // Catch:{ all -> 0x0c69 }
            if (r3 == 0) goto L_0x01f0
            java.lang.String r4 = "onelink_id"
            r7.put(r4, r3)     // Catch:{ all -> 0x0c69 }
            java.lang.String r3 = "ol_ver"
            com.appsflyer.AppsFlyerProperties r4 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x0c69 }
            java.lang.String r11 = "onelinkVersion"
            java.lang.String r4 = r4.getString(r11)     // Catch:{ all -> 0x0c69 }
            r7.put(r3, r4)     // Catch:{ all -> 0x0c69 }
        L_0x01f0:
            java.lang.String r3 = "appsflyerGetConversionDataTiming"
            r14 = 0
            long r3 = r12.getLong(r3, r14)     // Catch:{ all -> 0x0c69 }
            int r11 = (r3 > r14 ? 1 : (r3 == r14 ? 0 : -1))
            if (r11 <= 0) goto L_0x0213
            java.lang.String r11 = "gcd_timing"
            java.lang.String r3 = java.lang.Long.toString(r3)     // Catch:{ all -> 0x0c69 }
            r7.put(r11, r3)     // Catch:{ all -> 0x0c69 }
            java.lang.String r3 = "appsflyerGetConversionDataTiming"
            android.content.SharedPreferences$Editor r4 = r12.edit()     // Catch:{ all -> 0x0c69 }
            r14 = 0
            r4.putLong(r3, r14)     // Catch:{ all -> 0x0c69 }
            r4.apply()     // Catch:{ all -> 0x0c69 }
        L_0x0213:
            java.lang.String r3 = r1.f55     // Catch:{ all -> 0x0c69 }
            if (r3 == 0) goto L_0x021e
            java.lang.String r3 = "phone"
            java.lang.String r4 = r1.f55     // Catch:{ all -> 0x0c69 }
            r7.put(r3, r4)     // Catch:{ all -> 0x0c69 }
        L_0x021e:
            r25 = r2
            r27 = r5
            r28 = r6
            r26 = r12
            r2 = r17
            goto L_0x02cf
        L_0x022a:
            r22 = r4
            r24 = r14
            r23 = r15
            android.content.Context r4 = r6.getApplicationContext()     // Catch:{ all -> 0x0c69 }
            r11 = 0
            android.content.SharedPreferences r4 = r4.getSharedPreferences(r13, r11)     // Catch:{ all -> 0x0c69 }
            android.content.SharedPreferences$Editor r11 = r4.edit()     // Catch:{ all -> 0x0c69 }
            r14 = 0
            java.lang.String r15 = r4.getString(r3, r14)     // Catch:{ Exception -> 0x02be }
            if (r15 == 0) goto L_0x0296
            org.json.JSONObject r14 = new org.json.JSONObject     // Catch:{ Exception -> 0x0289 }
            r14.<init>()     // Catch:{ Exception -> 0x0289 }
            r25 = r2
            java.lang.String r2 = "prev_event_timestamp"
            r26 = r12
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0281 }
            r12.<init>()     // Catch:{ Exception -> 0x0281 }
            java.lang.String r1 = "prev_event_timestamp"
            r27 = r5
            r28 = r6
            r5 = -1
            long r5 = r4.getLong(r1, r5)     // Catch:{ Exception -> 0x027f }
            r12.append(r5)     // Catch:{ Exception -> 0x027f }
            java.lang.String r1 = r12.toString()     // Catch:{ Exception -> 0x027f }
            r14.put(r2, r1)     // Catch:{ Exception -> 0x027f }
            java.lang.String r1 = "prev_event_value"
            java.lang.String r2 = "prev_event_value"
            r5 = 0
            java.lang.String r2 = r4.getString(r2, r5)     // Catch:{ Exception -> 0x027f }
            r14.put(r1, r2)     // Catch:{ Exception -> 0x027f }
            r14.put(r3, r15)     // Catch:{ Exception -> 0x027f }
            java.lang.String r1 = "prev_event"
            r7.put(r1, r14)     // Catch:{ Exception -> 0x027f }
            goto L_0x029e
        L_0x027f:
            r0 = move-exception
            goto L_0x0292
        L_0x0281:
            r0 = move-exception
            r27 = r5
            r28 = r6
            goto L_0x0292
        L_0x0287:
            r0 = move-exception
            goto L_0x028c
        L_0x0289:
            r0 = move-exception
            r25 = r2
        L_0x028c:
            r27 = r5
            r28 = r6
            r26 = r12
        L_0x0292:
            r1 = r0
            r2 = r17
            goto L_0x02ca
        L_0x0296:
            r25 = r2
            r27 = r5
            r28 = r6
            r26 = r12
        L_0x029e:
            r11.putString(r3, r9)     // Catch:{ Exception -> 0x02b7 }
            java.lang.String r1 = "prev_event_value"
            r2 = r17
            r11.putString(r1, r2)     // Catch:{ Exception -> 0x02b5 }
            java.lang.String r1 = "prev_event_timestamp"
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x02b5 }
            r11.putLong(r1, r3)     // Catch:{ Exception -> 0x02b5 }
            r11.apply()     // Catch:{ Exception -> 0x02b5 }
            goto L_0x02cf
        L_0x02b5:
            r0 = move-exception
            goto L_0x02c9
        L_0x02b7:
            r0 = move-exception
            goto L_0x02c7
        L_0x02b9:
            r0 = move-exception
            r2 = r29
            goto L_0x0c6b
        L_0x02be:
            r0 = move-exception
            r25 = r2
            r27 = r5
            r28 = r6
            r26 = r12
        L_0x02c7:
            r2 = r17
        L_0x02c9:
            r1 = r0
        L_0x02ca:
            java.lang.String r3 = "Error while processing previous event."
            com.appsflyer.AFLogger.afErrorLog(r3, r1)     // Catch:{ all -> 0x02b9 }
        L_0x02cf:
            java.lang.String r1 = "KSAppsFlyerId"
            com.appsflyer.AppsFlyerProperties r3 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x02b9 }
            java.lang.String r1 = r3.getString(r1)     // Catch:{ all -> 0x02b9 }
            java.lang.String r3 = "KSAppsFlyerRICounter"
            com.appsflyer.AppsFlyerProperties r4 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x02b9 }
            java.lang.String r3 = r4.getString(r3)     // Catch:{ all -> 0x02b9 }
            if (r1 == 0) goto L_0x02fb
            if (r3 == 0) goto L_0x02fb
            java.lang.Integer r4 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x02b9 }
            int r4 = r4.intValue()     // Catch:{ all -> 0x02b9 }
            if (r4 <= 0) goto L_0x02fb
            java.lang.String r4 = "reinstallCounter"
            r7.put(r4, r3)     // Catch:{ all -> 0x02b9 }
            java.lang.String r3 = "originalAppsflyerId"
            r7.put(r3, r1)     // Catch:{ all -> 0x02b9 }
        L_0x02fb:
            java.lang.String r1 = "additionalCustomData"
            com.appsflyer.AppsFlyerProperties r3 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x02b9 }
            java.lang.String r1 = r3.getString(r1)     // Catch:{ all -> 0x02b9 }
            if (r1 == 0) goto L_0x030c
            java.lang.String r3 = "customData"
            r7.put(r3, r1)     // Catch:{ all -> 0x02b9 }
        L_0x030c:
            android.content.pm.PackageManager r1 = r28.getPackageManager()     // Catch:{ Exception -> 0x0320 }
            java.lang.String r3 = r28.getPackageName()     // Catch:{ Exception -> 0x0320 }
            java.lang.String r1 = r1.getInstallerPackageName(r3)     // Catch:{ Exception -> 0x0320 }
            if (r1 == 0) goto L_0x0327
            java.lang.String r3 = "installer_package"
            r7.put(r3, r1)     // Catch:{ Exception -> 0x0320 }
            goto L_0x0327
        L_0x0320:
            r0 = move-exception
            r1 = r0
            java.lang.String r3 = "Exception while getting the app's installer package. "
            com.appsflyer.AFLogger.afErrorLog(r3, r1)     // Catch:{ all -> 0x02b9 }
        L_0x0327:
            com.appsflyer.AppsFlyerProperties r1 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x02b9 }
            java.lang.String r3 = "sdkExtension"
            java.lang.String r1 = r1.getString(r3)     // Catch:{ all -> 0x02b9 }
            if (r1 == 0) goto L_0x033e
            int r3 = r1.length()     // Catch:{ all -> 0x02b9 }
            if (r3 <= 0) goto L_0x033e
            java.lang.String r3 = "sdkExtension"
            r7.put(r3, r1)     // Catch:{ all -> 0x02b9 }
        L_0x033e:
            java.lang.ref.WeakReference r1 = new java.lang.ref.WeakReference     // Catch:{ all -> 0x02b9 }
            r6 = r28
            r1.<init>(r6)     // Catch:{ all -> 0x02b9 }
            java.lang.String r1 = m64(r1)     // Catch:{ all -> 0x02b9 }
            java.lang.String r3 = m19(r6, r1)     // Catch:{ all -> 0x02b9 }
            if (r3 == 0) goto L_0x0354
            java.lang.String r4 = "channel"
            r7.put(r4, r3)     // Catch:{ all -> 0x02b9 }
        L_0x0354:
            if (r3 == 0) goto L_0x035c
            boolean r4 = r3.equals(r1)     // Catch:{ all -> 0x02b9 }
            if (r4 == 0) goto L_0x0360
        L_0x035c:
            if (r3 != 0) goto L_0x0365
            if (r1 == 0) goto L_0x0365
        L_0x0360:
            java.lang.String r3 = "af_latestchannel"
            r7.put(r3, r1)     // Catch:{ all -> 0x02b9 }
        L_0x0365:
            android.content.Context r1 = r6.getApplicationContext()     // Catch:{ all -> 0x02b9 }
            r3 = 0
            android.content.SharedPreferences r1 = r1.getSharedPreferences(r13, r3)     // Catch:{ all -> 0x02b9 }
            java.lang.String r3 = "INSTALL_STORE"
            boolean r3 = r1.contains(r3)     // Catch:{ all -> 0x02b9 }
            if (r3 == 0) goto L_0x037e
            java.lang.String r3 = "INSTALL_STORE"
            r4 = 0
            java.lang.String r1 = r1.getString(r3, r4)     // Catch:{ all -> 0x02b9 }
            goto L_0x039a
        L_0x037e:
            android.content.Context r1 = r6.getApplicationContext()     // Catch:{ all -> 0x02b9 }
            r3 = 0
            android.content.SharedPreferences r1 = r1.getSharedPreferences(r13, r3)     // Catch:{ all -> 0x02b9 }
            boolean r1 = r1.contains(r10)     // Catch:{ all -> 0x02b9 }
            r3 = 1
            r1 = r1 ^ r3
            if (r1 == 0) goto L_0x0394
            java.lang.String r1 = m72(r6)     // Catch:{ all -> 0x02b9 }
            goto L_0x0395
        L_0x0394:
            r1 = 0
        L_0x0395:
            java.lang.String r3 = "INSTALL_STORE"
            m28(r6, r3, r1)     // Catch:{ all -> 0x02b9 }
        L_0x039a:
            if (r1 == 0) goto L_0x03a5
            java.lang.String r3 = "af_installstore"
            java.lang.String r1 = r1.toLowerCase()     // Catch:{ all -> 0x02b9 }
            r7.put(r3, r1)     // Catch:{ all -> 0x02b9 }
        L_0x03a5:
            android.content.Context r1 = r6.getApplicationContext()     // Catch:{ all -> 0x02b9 }
            r3 = 0
            android.content.SharedPreferences r1 = r1.getSharedPreferences(r13, r3)     // Catch:{ all -> 0x02b9 }
            com.appsflyer.AppsFlyerProperties r3 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x02b9 }
            r4 = r27
            java.lang.String r3 = r3.getString(r4)     // Catch:{ all -> 0x02b9 }
            if (r3 != 0) goto L_0x0462
            boolean r5 = r1.contains(r4)     // Catch:{ all -> 0x02b9 }
            if (r5 == 0) goto L_0x03c8
            r3 = 0
            java.lang.String r1 = r1.getString(r4, r3)     // Catch:{ all -> 0x02b9 }
            r3 = r1
            goto L_0x0459
        L_0x03c8:
            android.content.Context r1 = r6.getApplicationContext()     // Catch:{ all -> 0x02b9 }
            r5 = 0
            android.content.SharedPreferences r1 = r1.getSharedPreferences(r13, r5)     // Catch:{ all -> 0x02b9 }
            boolean r1 = r1.contains(r10)     // Catch:{ all -> 0x02b9 }
            r5 = 1
            r1 = r1 ^ r5
            if (r1 == 0) goto L_0x0454
            java.lang.String r1 = "ro.appsflyer.preinstall.path"
            java.lang.String r1 = m50(r1)     // Catch:{ all -> 0x02b9 }
            java.io.File r1 = m23(r1)     // Catch:{ all -> 0x02b9 }
            if (r1 == 0) goto L_0x03ee
            boolean r3 = r1.exists()     // Catch:{ all -> 0x02b9 }
            if (r3 != 0) goto L_0x03ec
            goto L_0x03ee
        L_0x03ec:
            r3 = 0
            goto L_0x03ef
        L_0x03ee:
            r3 = 1
        L_0x03ef:
            if (r3 == 0) goto L_0x0403
            java.lang.String r1 = "AF_PRE_INSTALL_PATH"
            android.content.pm.PackageManager r3 = r6.getPackageManager()     // Catch:{ all -> 0x02b9 }
            java.lang.String r5 = r6.getPackageName()     // Catch:{ all -> 0x02b9 }
            java.lang.String r1 = m39(r1, r3, r5)     // Catch:{ all -> 0x02b9 }
            java.io.File r1 = m23(r1)     // Catch:{ all -> 0x02b9 }
        L_0x0403:
            if (r1 == 0) goto L_0x040e
            boolean r3 = r1.exists()     // Catch:{ all -> 0x02b9 }
            if (r3 != 0) goto L_0x040c
            goto L_0x040e
        L_0x040c:
            r3 = 0
            goto L_0x040f
        L_0x040e:
            r3 = 1
        L_0x040f:
            if (r3 == 0) goto L_0x0417
            java.lang.String r1 = "/data/local/tmp/pre_install.appsflyer"
            java.io.File r1 = m23(r1)     // Catch:{ all -> 0x02b9 }
        L_0x0417:
            if (r1 == 0) goto L_0x0422
            boolean r3 = r1.exists()     // Catch:{ all -> 0x02b9 }
            if (r3 != 0) goto L_0x0420
            goto L_0x0422
        L_0x0420:
            r3 = 0
            goto L_0x0423
        L_0x0422:
            r3 = 1
        L_0x0423:
            if (r3 == 0) goto L_0x042b
            java.lang.String r1 = "/etc/pre_install.appsflyer"
            java.io.File r1 = m23(r1)     // Catch:{ all -> 0x02b9 }
        L_0x042b:
            if (r1 == 0) goto L_0x0436
            boolean r3 = r1.exists()     // Catch:{ all -> 0x02b9 }
            if (r3 != 0) goto L_0x0434
            goto L_0x0436
        L_0x0434:
            r3 = 0
            goto L_0x0437
        L_0x0436:
            r3 = 1
        L_0x0437:
            if (r3 != 0) goto L_0x0445
            java.lang.String r3 = r6.getPackageName()     // Catch:{ all -> 0x02b9 }
            java.lang.String r1 = m33(r1, r3)     // Catch:{ all -> 0x02b9 }
            if (r1 == 0) goto L_0x0445
            r3 = r1
            goto L_0x0446
        L_0x0445:
            r3 = 0
        L_0x0446:
            if (r3 == 0) goto L_0x0449
            goto L_0x0454
        L_0x0449:
            java.lang.ref.WeakReference r1 = new java.lang.ref.WeakReference     // Catch:{ all -> 0x02b9 }
            r1.<init>(r6)     // Catch:{ all -> 0x02b9 }
            java.lang.String r3 = "AF_PRE_INSTALL_NAME"
            java.lang.String r3 = m24(r1, r3)     // Catch:{ all -> 0x02b9 }
        L_0x0454:
            if (r3 == 0) goto L_0x0459
            m28(r6, r4, r3)     // Catch:{ all -> 0x02b9 }
        L_0x0459:
            if (r3 == 0) goto L_0x0462
            com.appsflyer.AppsFlyerProperties r1 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x02b9 }
            r1.set(r4, r3)     // Catch:{ all -> 0x02b9 }
        L_0x0462:
            if (r3 == 0) goto L_0x046d
            java.lang.String r1 = "af_preinstall_name"
            java.lang.String r3 = r3.toLowerCase()     // Catch:{ all -> 0x02b9 }
            r7.put(r1, r3)     // Catch:{ all -> 0x02b9 }
        L_0x046d:
            java.lang.String r1 = m72(r6)     // Catch:{ all -> 0x02b9 }
            if (r1 == 0) goto L_0x047c
            java.lang.String r3 = "af_currentstore"
            java.lang.String r1 = r1.toLowerCase()     // Catch:{ all -> 0x02b9 }
            r7.put(r3, r1)     // Catch:{ all -> 0x02b9 }
        L_0x047c:
            java.lang.String r1 = "appsflyerKey"
            if (r8 == 0) goto L_0x048a
            int r3 = r8.length()     // Catch:{ all -> 0x02b9 }
            if (r3 <= 0) goto L_0x048a
            r7.put(r1, r8)     // Catch:{ all -> 0x02b9 }
            goto L_0x049f
        L_0x048a:
            java.lang.String r3 = "AppsFlyerKey"
            com.appsflyer.AppsFlyerProperties r4 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x02b9 }
            java.lang.String r3 = r4.getString(r3)     // Catch:{ all -> 0x02b9 }
            if (r3 == 0) goto L_0x0c52
            int r4 = r3.length()     // Catch:{ all -> 0x02b9 }
            if (r4 <= 0) goto L_0x0c52
            r7.put(r1, r3)     // Catch:{ all -> 0x02b9 }
        L_0x049f:
            java.lang.String r3 = "AppUserId"
            com.appsflyer.AppsFlyerProperties r4 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x02b9 }
            java.lang.String r3 = r4.getString(r3)     // Catch:{ all -> 0x02b9 }
            if (r3 == 0) goto L_0x04b0
            java.lang.String r4 = "appUserId"
            r7.put(r4, r3)     // Catch:{ all -> 0x02b9 }
        L_0x04b0:
            com.appsflyer.AppsFlyerProperties r3 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x02b9 }
            java.lang.String r4 = "userEmails"
            java.lang.String r3 = r3.getString(r4)     // Catch:{ all -> 0x02b9 }
            if (r3 == 0) goto L_0x04c2
            java.lang.String r4 = "user_emails"
            r7.put(r4, r3)     // Catch:{ all -> 0x02b9 }
            goto L_0x04d7
        L_0x04c2:
            java.lang.String r3 = "userEmail"
            com.appsflyer.AppsFlyerProperties r4 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x02b9 }
            java.lang.String r3 = r4.getString(r3)     // Catch:{ all -> 0x02b9 }
            if (r3 == 0) goto L_0x04d7
            java.lang.String r4 = "sha1_el"
            java.lang.String r3 = com.appsflyer.internal.ad.m121(r3)     // Catch:{ all -> 0x02b9 }
            r7.put(r4, r3)     // Catch:{ all -> 0x02b9 }
        L_0x04d7:
            if (r9 == 0) goto L_0x04e5
            java.lang.String r3 = "eventName"
            r7.put(r3, r9)     // Catch:{ all -> 0x02b9 }
            if (r2 == 0) goto L_0x04e5
            java.lang.String r3 = "eventValue"
            r7.put(r3, r2)     // Catch:{ all -> 0x02b9 }
        L_0x04e5:
            java.lang.String r2 = "appid"
            com.appsflyer.AppsFlyerProperties r3 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x02b9 }
            java.lang.String r2 = r3.getString(r2)     // Catch:{ all -> 0x02b9 }
            if (r2 == 0) goto L_0x0500
            java.lang.String r2 = "appid"
            java.lang.String r3 = "appid"
            com.appsflyer.AppsFlyerProperties r4 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x02b9 }
            java.lang.String r3 = r4.getString(r3)     // Catch:{ all -> 0x02b9 }
            r7.put(r2, r3)     // Catch:{ all -> 0x02b9 }
        L_0x0500:
            java.lang.String r2 = "currencyCode"
            com.appsflyer.AppsFlyerProperties r3 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x02b9 }
            java.lang.String r2 = r3.getString(r2)     // Catch:{ all -> 0x02b9 }
            if (r2 == 0) goto L_0x052e
            int r3 = r2.length()     // Catch:{ all -> 0x02b9 }
            r4 = 3
            if (r3 == r4) goto L_0x0529
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x02b9 }
            java.lang.String r4 = "WARNING: currency code should be 3 characters!!! '"
            r3.<init>(r4)     // Catch:{ all -> 0x02b9 }
            r3.append(r2)     // Catch:{ all -> 0x02b9 }
            java.lang.String r4 = "' is not a legal value."
            r3.append(r4)     // Catch:{ all -> 0x02b9 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x02b9 }
            com.appsflyer.AFLogger.afWarnLog(r3)     // Catch:{ all -> 0x02b9 }
        L_0x0529:
            java.lang.String r3 = "currency"
            r7.put(r3, r2)     // Catch:{ all -> 0x02b9 }
        L_0x052e:
            java.lang.String r2 = "IS_UPDATE"
            com.appsflyer.AppsFlyerProperties r3 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x02b9 }
            java.lang.String r2 = r3.getString(r2)     // Catch:{ all -> 0x02b9 }
            if (r2 == 0) goto L_0x053f
            java.lang.String r3 = "isUpdate"
            r7.put(r3, r2)     // Catch:{ all -> 0x02b9 }
        L_0x053f:
            r2 = r29
            boolean r3 = r2.isPreInstalledApp(r6)     // Catch:{ all -> 0x0c67 }
            java.lang.String r4 = "af_preinstalled"
            java.lang.String r3 = java.lang.Boolean.toString(r3)     // Catch:{ all -> 0x0c67 }
            r7.put(r4, r3)     // Catch:{ all -> 0x0c67 }
            com.appsflyer.AppsFlyerProperties r3 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x0c67 }
            java.lang.String r4 = "collectFacebookAttrId"
            r5 = 1
            boolean r3 = r3.getBoolean(r4, r5)     // Catch:{ all -> 0x0c67 }
            if (r3 == 0) goto L_0x0580
            android.content.pm.PackageManager r3 = r6.getPackageManager()     // Catch:{ NameNotFoundException -> 0x0573, all -> 0x056a }
            java.lang.String r4 = "com.facebook.katana"
            r5 = 0
            r3.getApplicationInfo(r4, r5)     // Catch:{ NameNotFoundException -> 0x0573, all -> 0x056a }
            java.lang.String r3 = r2.getAttributionId(r6)     // Catch:{ NameNotFoundException -> 0x0573, all -> 0x056a }
            goto L_0x0579
        L_0x056a:
            r0 = move-exception
            r3 = r0
            java.lang.String r4 = "Exception while collecting facebook's attribution ID. "
            com.appsflyer.AFLogger.afErrorLog(r4, r3)     // Catch:{ all -> 0x0c67 }
        L_0x0571:
            r3 = 0
            goto L_0x0579
        L_0x0573:
            java.lang.String r3 = "Exception while collecting facebook's attribution ID. "
            com.appsflyer.AFLogger.afWarnLog(r3)     // Catch:{ all -> 0x0c67 }
            goto L_0x0571
        L_0x0579:
            if (r3 == 0) goto L_0x0580
            java.lang.String r4 = "fb"
            r7.put(r4, r3)     // Catch:{ all -> 0x0c67 }
        L_0x0580:
            com.appsflyer.AppsFlyerProperties r3 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x0c67 }
            java.lang.String r4 = "deviceTrackingDisabled"
            r5 = 0
            boolean r3 = r3.getBoolean(r4, r5)     // Catch:{ all -> 0x0c67 }
            java.lang.String r4 = "true"
            if (r3 == 0) goto L_0x0596
            java.lang.String r3 = "deviceTrackingDisabled"
            r7.put(r3, r4)     // Catch:{ all -> 0x0c67 }
            goto L_0x06fa
        L_0x0596:
            android.content.Context r3 = r6.getApplicationContext()     // Catch:{ all -> 0x0c67 }
            r5 = 0
            android.content.SharedPreferences r3 = r3.getSharedPreferences(r13, r5)     // Catch:{ all -> 0x0c67 }
            com.appsflyer.AppsFlyerProperties r5 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x0c67 }
            java.lang.String r8 = "collectIMEI"
            r11 = 1
            boolean r5 = r5.getBoolean(r8, r11)     // Catch:{ all -> 0x0c67 }
            java.lang.String r8 = "imeiCached"
            r11 = 0
            java.lang.String r8 = r3.getString(r8, r11)     // Catch:{ all -> 0x0c67 }
            if (r5 == 0) goto L_0x0622
            java.lang.String r5 = r2.f57     // Catch:{ all -> 0x0c67 }
            boolean r5 = android.text.TextUtils.isEmpty(r5)     // Catch:{ all -> 0x0c67 }
            if (r5 == 0) goto L_0x0622
            boolean r5 = m18(r6)     // Catch:{ all -> 0x0c67 }
            if (r5 == 0) goto L_0x062a
            java.lang.String r5 = "phone"
            java.lang.Object r5 = r6.getSystemService(r5)     // Catch:{ InvocationTargetException -> 0x060a, Exception -> 0x05f1 }
            android.telephony.TelephonyManager r5 = (android.telephony.TelephonyManager) r5     // Catch:{ InvocationTargetException -> 0x060a, Exception -> 0x05f1 }
            java.lang.Class r11 = r5.getClass()     // Catch:{ InvocationTargetException -> 0x060a, Exception -> 0x05f1 }
            java.lang.String r12 = "getDeviceId"
            r14 = 0
            java.lang.Class[] r15 = new java.lang.Class[r14]     // Catch:{ InvocationTargetException -> 0x060a, Exception -> 0x05f1 }
            java.lang.reflect.Method r11 = r11.getMethod(r12, r15)     // Catch:{ InvocationTargetException -> 0x060a, Exception -> 0x05f1 }
            java.lang.Object[] r12 = new java.lang.Object[r14]     // Catch:{ InvocationTargetException -> 0x060a, Exception -> 0x05f1 }
            java.lang.Object r5 = r11.invoke(r5, r12)     // Catch:{ InvocationTargetException -> 0x060a, Exception -> 0x05f1 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ InvocationTargetException -> 0x060a, Exception -> 0x05f1 }
            if (r5 == 0) goto L_0x05e1
            goto L_0x0628
        L_0x05e1:
            if (r8 == 0) goto L_0x062a
            java.lang.String r5 = "use cached IMEI: "
            java.lang.String r11 = java.lang.String.valueOf(r8)     // Catch:{ InvocationTargetException -> 0x060a, Exception -> 0x05f1 }
            java.lang.String r5 = r5.concat(r11)     // Catch:{ InvocationTargetException -> 0x060a, Exception -> 0x05f1 }
            com.appsflyer.AFLogger.afDebugLog(r5)     // Catch:{ InvocationTargetException -> 0x060a, Exception -> 0x05f1 }
            goto L_0x062b
        L_0x05f1:
            r0 = move-exception
            r5 = r0
            if (r8 == 0) goto L_0x0603
            java.lang.String r11 = "use cached IMEI: "
            java.lang.String r12 = java.lang.String.valueOf(r8)     // Catch:{ all -> 0x0c67 }
            java.lang.String r11 = r11.concat(r12)     // Catch:{ all -> 0x0c67 }
            com.appsflyer.AFLogger.afDebugLog(r11)     // Catch:{ all -> 0x0c67 }
            goto L_0x0604
        L_0x0603:
            r8 = 0
        L_0x0604:
            java.lang.String r11 = "WARNING: other reason: "
            com.appsflyer.AFLogger.afErrorLog(r11, r5)     // Catch:{ all -> 0x0c67 }
            goto L_0x062b
        L_0x060a:
            if (r8 == 0) goto L_0x061b
            java.lang.String r5 = "use cached IMEI: "
            java.lang.String r11 = java.lang.String.valueOf(r8)     // Catch:{ all -> 0x0c67 }
            java.lang.String r5 = r5.concat(r11)     // Catch:{ all -> 0x0c67 }
            com.appsflyer.AFLogger.afDebugLog(r5)     // Catch:{ all -> 0x0c67 }
            goto L_0x061c
        L_0x061b:
            r8 = 0
        L_0x061c:
            java.lang.String r5 = "WARNING: READ_PHONE_STATE is missing."
            com.appsflyer.AFLogger.afWarnLog(r5)     // Catch:{ all -> 0x0c67 }
            goto L_0x062b
        L_0x0622:
            java.lang.String r5 = r2.f57     // Catch:{ all -> 0x0c67 }
            if (r5 == 0) goto L_0x062a
            java.lang.String r5 = r2.f57     // Catch:{ all -> 0x0c67 }
        L_0x0628:
            r8 = r5
            goto L_0x062b
        L_0x062a:
            r8 = 0
        L_0x062b:
            if (r8 == 0) goto L_0x0638
            java.lang.String r5 = "imeiCached"
            m28(r6, r5, r8)     // Catch:{ all -> 0x0c67 }
            java.lang.String r5 = "imei"
            r7.put(r5, r8)     // Catch:{ all -> 0x0c67 }
            goto L_0x063d
        L_0x0638:
            java.lang.String r5 = "IMEI was not collected."
            com.appsflyer.AFLogger.afInfoLog(r5)     // Catch:{ all -> 0x0c67 }
        L_0x063d:
            com.appsflyer.AppsFlyerProperties r5 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x0c67 }
            java.lang.String r8 = "collectAndroidId"
            r11 = 1
            boolean r5 = r5.getBoolean(r8, r11)     // Catch:{ all -> 0x0c67 }
            java.lang.String r8 = "androidIdCached"
            r11 = 0
            java.lang.String r8 = r3.getString(r8, r11)     // Catch:{ all -> 0x0c67 }
            if (r5 == 0) goto L_0x069a
            java.lang.String r5 = r2.f46     // Catch:{ all -> 0x0c67 }
            boolean r5 = android.text.TextUtils.isEmpty(r5)     // Catch:{ all -> 0x0c67 }
            if (r5 == 0) goto L_0x069a
            boolean r5 = m18(r6)     // Catch:{ all -> 0x0c67 }
            if (r5 == 0) goto L_0x06a1
            android.content.ContentResolver r5 = r6.getContentResolver()     // Catch:{ Exception -> 0x067e }
            java.lang.String r11 = "android_id"
            java.lang.String r5 = android.provider.Settings.Secure.getString(r5, r11)     // Catch:{ Exception -> 0x067e }
            if (r5 == 0) goto L_0x066c
            goto L_0x06a2
        L_0x066c:
            if (r8 == 0) goto L_0x067c
            java.lang.String r5 = "use cached AndroidId: "
            java.lang.String r11 = java.lang.String.valueOf(r8)     // Catch:{ Exception -> 0x067e }
            java.lang.String r5 = r5.concat(r11)     // Catch:{ Exception -> 0x067e }
            com.appsflyer.AFLogger.afDebugLog(r5)     // Catch:{ Exception -> 0x067e }
            goto L_0x0698
        L_0x067c:
            r8 = 0
            goto L_0x0698
        L_0x067e:
            r0 = move-exception
            r5 = r0
            if (r8 == 0) goto L_0x0690
            java.lang.String r11 = "use cached AndroidId: "
            java.lang.String r12 = java.lang.String.valueOf(r8)     // Catch:{ all -> 0x0c67 }
            java.lang.String r11 = r11.concat(r12)     // Catch:{ all -> 0x0c67 }
            com.appsflyer.AFLogger.afDebugLog(r11)     // Catch:{ all -> 0x0c67 }
            goto L_0x0691
        L_0x0690:
            r8 = 0
        L_0x0691:
            java.lang.String r11 = r5.getMessage()     // Catch:{ all -> 0x0c67 }
            com.appsflyer.AFLogger.afErrorLog(r11, r5)     // Catch:{ all -> 0x0c67 }
        L_0x0698:
            r5 = r8
            goto L_0x06a2
        L_0x069a:
            java.lang.String r5 = r2.f46     // Catch:{ all -> 0x0c67 }
            if (r5 == 0) goto L_0x06a1
            java.lang.String r5 = r2.f46     // Catch:{ all -> 0x0c67 }
            goto L_0x06a2
        L_0x06a1:
            r5 = 0
        L_0x06a2:
            if (r5 == 0) goto L_0x06af
            java.lang.String r8 = "androidIdCached"
            m28(r6, r8, r5)     // Catch:{ all -> 0x0c67 }
            java.lang.String r8 = "android_id"
            r7.put(r8, r5)     // Catch:{ all -> 0x0c67 }
            goto L_0x06b4
        L_0x06af:
            java.lang.String r5 = "Android ID was not collected."
            com.appsflyer.AFLogger.afInfoLog(r5)     // Catch:{ all -> 0x0c67 }
        L_0x06b4:
            com.appsflyer.AppsFlyerProperties r5 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x0c67 }
            java.lang.String r8 = "collectOAID"
            r11 = 0
            boolean r5 = r5.getBoolean(r8, r11)     // Catch:{ all -> 0x0c67 }
            java.lang.String r8 = "oaidCached"
            r11 = 0
            java.lang.String r3 = r3.getString(r8, r11)     // Catch:{ all -> 0x0c67 }
            java.lang.String r8 = r2.f59     // Catch:{ all -> 0x0c67 }
            if (r8 == 0) goto L_0x06cd
            java.lang.String r3 = r2.f59     // Catch:{ all -> 0x0c67 }
            goto L_0x06ee
        L_0x06cd:
            if (r5 == 0) goto L_0x06ee
            if (r3 != 0) goto L_0x06ee
            boolean r5 = com.appsflyer.internal.z.m204()     // Catch:{ all -> 0x0c67 }
            if (r5 == 0) goto L_0x06ee
            boolean r5 = com.appsflyer.internal.z.m206(r6)     // Catch:{ all -> 0x0c67 }
            if (r5 != 0) goto L_0x06ee
            java.lang.String r3 = com.appsflyer.internal.z.m205(r6)     // Catch:{ all -> 0x0c67 }
            java.lang.String r5 = "OAID was collected: "
            java.lang.String r8 = java.lang.String.valueOf(r3)     // Catch:{ all -> 0x0c67 }
            java.lang.String r5 = r5.concat(r8)     // Catch:{ all -> 0x0c67 }
            com.appsflyer.AFLogger.afDebugLog(r5)     // Catch:{ all -> 0x0c67 }
        L_0x06ee:
            if (r3 == 0) goto L_0x06fa
            java.lang.String r5 = "oaidCached"
            m28(r6, r5, r3)     // Catch:{ all -> 0x0c67 }
            java.lang.String r5 = "oaid"
            r7.put(r5, r3)     // Catch:{ all -> 0x0c67 }
        L_0x06fa:
            java.lang.ref.WeakReference r3 = new java.lang.ref.WeakReference     // Catch:{ Exception -> 0x070b }
            r3.<init>(r6)     // Catch:{ Exception -> 0x070b }
            java.lang.String r3 = com.appsflyer.internal.ab.m111(r3)     // Catch:{ Exception -> 0x070b }
            if (r3 == 0) goto L_0x0722
            java.lang.String r5 = "uid"
            r7.put(r5, r3)     // Catch:{ Exception -> 0x070b }
            goto L_0x0722
        L_0x070b:
            r0 = move-exception
            r3 = r0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0c67 }
            java.lang.String r8 = "ERROR: could not get uid "
            r5.<init>(r8)     // Catch:{ all -> 0x0c67 }
            java.lang.String r8 = r3.getMessage()     // Catch:{ all -> 0x0c67 }
            r5.append(r8)     // Catch:{ all -> 0x0c67 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0c67 }
            com.appsflyer.AFLogger.afErrorLog(r5, r3)     // Catch:{ all -> 0x0c67 }
        L_0x0722:
            java.lang.String r3 = "lang"
            java.util.Locale r5 = java.util.Locale.getDefault()     // Catch:{ Exception -> 0x0730 }
            java.lang.String r5 = r5.getDisplayLanguage()     // Catch:{ Exception -> 0x0730 }
            r7.put(r3, r5)     // Catch:{ Exception -> 0x0730 }
            goto L_0x0737
        L_0x0730:
            r0 = move-exception
            r3 = r0
            java.lang.String r5 = "Exception while collecting display language name. "
            com.appsflyer.AFLogger.afErrorLog(r5, r3)     // Catch:{ all -> 0x0c67 }
        L_0x0737:
            java.lang.String r3 = "lang_code"
            java.util.Locale r5 = java.util.Locale.getDefault()     // Catch:{ Exception -> 0x0745 }
            java.lang.String r5 = r5.getLanguage()     // Catch:{ Exception -> 0x0745 }
            r7.put(r3, r5)     // Catch:{ Exception -> 0x0745 }
            goto L_0x074c
        L_0x0745:
            r0 = move-exception
            r3 = r0
            java.lang.String r5 = "Exception while collecting display language code. "
            com.appsflyer.AFLogger.afErrorLog(r5, r3)     // Catch:{ all -> 0x0c67 }
        L_0x074c:
            java.lang.String r3 = "country"
            java.util.Locale r5 = java.util.Locale.getDefault()     // Catch:{ Exception -> 0x075a }
            java.lang.String r5 = r5.getCountry()     // Catch:{ Exception -> 0x075a }
            r7.put(r3, r5)     // Catch:{ Exception -> 0x075a }
            goto L_0x0761
        L_0x075a:
            r0 = move-exception
            r3 = r0
            java.lang.String r5 = "Exception while collecting country name. "
            com.appsflyer.AFLogger.afErrorLog(r5, r3)     // Catch:{ all -> 0x0c67 }
        L_0x0761:
            java.lang.String r3 = "platformextension"
            com.appsflyer.internal.ac r5 = r2.f49     // Catch:{ all -> 0x0c67 }
            java.lang.String r5 = r5.m115()     // Catch:{ all -> 0x0c67 }
            r7.put(r3, r5)     // Catch:{ all -> 0x0c67 }
            m55(r6, r7)     // Catch:{ all -> 0x0c67 }
            java.lang.String r3 = "yyyy-MM-dd_HHmmssZ"
            java.text.SimpleDateFormat r5 = new java.text.SimpleDateFormat     // Catch:{ all -> 0x0c67 }
            java.util.Locale r8 = java.util.Locale.US     // Catch:{ all -> 0x0c67 }
            r5.<init>(r3, r8)     // Catch:{ all -> 0x0c67 }
            android.content.pm.PackageManager r3 = r6.getPackageManager()     // Catch:{ Exception -> 0x079f }
            java.lang.String r8 = r6.getPackageName()     // Catch:{ Exception -> 0x079f }
            r11 = 0
            android.content.pm.PackageInfo r3 = r3.getPackageInfo(r8, r11)     // Catch:{ Exception -> 0x079f }
            long r11 = r3.firstInstallTime     // Catch:{ Exception -> 0x079f }
            java.lang.String r3 = "installDate"
            java.lang.String r8 = "UTC"
            java.util.TimeZone r8 = java.util.TimeZone.getTimeZone(r8)     // Catch:{ Exception -> 0x079f }
            r5.setTimeZone(r8)     // Catch:{ Exception -> 0x079f }
            java.util.Date r8 = new java.util.Date     // Catch:{ Exception -> 0x079f }
            r8.<init>(r11)     // Catch:{ Exception -> 0x079f }
            java.lang.String r8 = r5.format(r8)     // Catch:{ Exception -> 0x079f }
            r7.put(r3, r8)     // Catch:{ Exception -> 0x079f }
            goto L_0x07a6
        L_0x079f:
            r0 = move-exception
            r3 = r0
            java.lang.String r8 = "Exception while collecting install date. "
            com.appsflyer.AFLogger.afErrorLog(r8, r3)     // Catch:{ all -> 0x0c67 }
        L_0x07a6:
            android.content.pm.PackageManager r3 = r6.getPackageManager()     // Catch:{ all -> 0x0878 }
            java.lang.String r8 = r6.getPackageName()     // Catch:{ all -> 0x0878 }
            r11 = 0
            android.content.pm.PackageInfo r3 = r3.getPackageInfo(r8, r11)     // Catch:{ all -> 0x0878 }
            java.lang.String r8 = "versionCode"
            r12 = r26
            int r8 = r12.getInt(r8, r11)     // Catch:{ all -> 0x0870 }
            int r14 = r3.versionCode     // Catch:{ all -> 0x0870 }
            if (r14 <= r8) goto L_0x07cb
            java.lang.String r8 = "appsflyerConversionDataRequestRetries"
            m37(r6, r8, r11)     // Catch:{ all -> 0x0870 }
            java.lang.String r8 = "versionCode"
            int r11 = r3.versionCode     // Catch:{ all -> 0x0870 }
            m37(r6, r8, r11)     // Catch:{ all -> 0x0870 }
        L_0x07cb:
            java.lang.String r8 = "app_version_code"
            int r11 = r3.versionCode     // Catch:{ all -> 0x0870 }
            java.lang.String r11 = java.lang.Integer.toString(r11)     // Catch:{ all -> 0x0870 }
            r7.put(r8, r11)     // Catch:{ all -> 0x0870 }
            java.lang.String r8 = "app_version_name"
            java.lang.String r11 = r3.versionName     // Catch:{ all -> 0x0870 }
            r7.put(r8, r11)     // Catch:{ all -> 0x0870 }
            long r14 = r3.firstInstallTime     // Catch:{ all -> 0x0870 }
            r11 = r9
            long r8 = r3.lastUpdateTime     // Catch:{ all -> 0x0868 }
            java.lang.String r3 = "date1"
            r17 = r1
            java.lang.String r1 = "yyyy-MM-dd_HHmmssZ"
            r22 = r11
            java.text.SimpleDateFormat r11 = new java.text.SimpleDateFormat     // Catch:{ all -> 0x0864 }
            r26 = r4
            java.util.Locale r4 = java.util.Locale.US     // Catch:{ all -> 0x0862 }
            r11.<init>(r1, r4)     // Catch:{ all -> 0x0862 }
            java.util.Date r1 = new java.util.Date     // Catch:{ all -> 0x0862 }
            r1.<init>(r14)     // Catch:{ all -> 0x0862 }
            java.lang.String r1 = r11.format(r1)     // Catch:{ all -> 0x0862 }
            r7.put(r3, r1)     // Catch:{ all -> 0x0862 }
            java.lang.String r1 = "date2"
            java.lang.String r3 = "yyyy-MM-dd_HHmmssZ"
            java.text.SimpleDateFormat r4 = new java.text.SimpleDateFormat     // Catch:{ all -> 0x0862 }
            java.util.Locale r11 = java.util.Locale.US     // Catch:{ all -> 0x0862 }
            r4.<init>(r3, r11)     // Catch:{ all -> 0x0862 }
            java.util.Date r3 = new java.util.Date     // Catch:{ all -> 0x0862 }
            r3.<init>(r8)     // Catch:{ all -> 0x0862 }
            java.lang.String r3 = r4.format(r3)     // Catch:{ all -> 0x0862 }
            r7.put(r1, r3)     // Catch:{ all -> 0x0862 }
            android.content.Context r1 = r6.getApplicationContext()     // Catch:{ all -> 0x0862 }
            r3 = 0
            android.content.SharedPreferences r1 = r1.getSharedPreferences(r13, r3)     // Catch:{ all -> 0x0862 }
            java.lang.String r4 = "appsFlyerFirstInstall"
            r8 = 0
            java.lang.String r1 = r1.getString(r4, r8)     // Catch:{ all -> 0x0862 }
            if (r1 != 0) goto L_0x084f
            android.content.Context r1 = r6.getApplicationContext()     // Catch:{ all -> 0x0862 }
            android.content.SharedPreferences r1 = r1.getSharedPreferences(r13, r3)     // Catch:{ all -> 0x0862 }
            boolean r1 = r1.contains(r10)     // Catch:{ all -> 0x0862 }
            r3 = 1
            r1 = r1 ^ r3
            if (r1 == 0) goto L_0x0848
            java.lang.String r1 = "AppsFlyer: first launch detected"
            com.appsflyer.AFLogger.afDebugLog(r1)     // Catch:{ all -> 0x0862 }
            java.util.Date r1 = new java.util.Date     // Catch:{ all -> 0x0862 }
            r1.<init>()     // Catch:{ all -> 0x0862 }
            java.lang.String r11 = r5.format(r1)     // Catch:{ all -> 0x0862 }
            r1 = r11
            goto L_0x084a
        L_0x0848:
            r1 = r25
        L_0x084a:
            java.lang.String r3 = "appsFlyerFirstInstall"
            m28(r6, r3, r1)     // Catch:{ all -> 0x0862 }
        L_0x084f:
            java.lang.String r3 = "AppsFlyer: first launch date: "
            java.lang.String r4 = java.lang.String.valueOf(r1)     // Catch:{ all -> 0x0862 }
            java.lang.String r3 = r3.concat(r4)     // Catch:{ all -> 0x0862 }
            com.appsflyer.AFLogger.afInfoLog(r3)     // Catch:{ all -> 0x0862 }
            java.lang.String r3 = "firstLaunchDate"
            r7.put(r3, r1)     // Catch:{ all -> 0x0862 }
            goto L_0x0887
        L_0x0862:
            r0 = move-exception
            goto L_0x0881
        L_0x0864:
            r0 = move-exception
            goto L_0x087f
        L_0x0866:
            r0 = move-exception
            goto L_0x086b
        L_0x0868:
            r0 = move-exception
            r17 = r1
        L_0x086b:
            r26 = r4
            r22 = r11
            goto L_0x0881
        L_0x0870:
            r0 = move-exception
            r17 = r1
            r26 = r4
            r22 = r9
            goto L_0x0881
        L_0x0878:
            r0 = move-exception
            r17 = r1
            r22 = r9
            r12 = r26
        L_0x087f:
            r26 = r4
        L_0x0881:
            r1 = r0
            java.lang.String r3 = "Exception while collecting app version data "
            com.appsflyer.AFLogger.afErrorLog(r3, r1)     // Catch:{ all -> 0x0c67 }
        L_0x0887:
            int r1 = r18.length()     // Catch:{ all -> 0x0c67 }
            if (r1 <= 0) goto L_0x0894
            java.lang.String r1 = "referrer"
            r3 = r18
            r7.put(r1, r3)     // Catch:{ all -> 0x0c67 }
        L_0x0894:
            java.lang.String r1 = "extraReferrers"
            r3 = 0
            java.lang.String r1 = r12.getString(r1, r3)     // Catch:{ all -> 0x0c67 }
            if (r1 == 0) goto L_0x08a2
            java.lang.String r3 = "extraReferrers"
            r7.put(r3, r1)     // Catch:{ all -> 0x0c67 }
        L_0x08a2:
            boolean r1 = com.appsflyer.internal.l.a.m162(r6)     // Catch:{ all -> 0x0c67 }
            r2.f48 = r1     // Catch:{ all -> 0x0c67 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0c67 }
            java.lang.String r3 = "didConfigureTokenRefreshService="
            r1.<init>(r3)     // Catch:{ all -> 0x0c67 }
            boolean r3 = r2.f48     // Catch:{ all -> 0x0c67 }
            r1.append(r3)     // Catch:{ all -> 0x0c67 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0c67 }
            com.appsflyer.AFLogger.afDebugLog(r1)     // Catch:{ all -> 0x0c67 }
            boolean r1 = r2.f48     // Catch:{ all -> 0x0c67 }
            if (r1 != 0) goto L_0x08c6
            java.lang.String r1 = "tokenRefreshConfigured"
            java.lang.Boolean r3 = java.lang.Boolean.FALSE     // Catch:{ all -> 0x0c67 }
            r7.put(r1, r3)     // Catch:{ all -> 0x0c67 }
        L_0x08c6:
            if (r23 == 0) goto L_0x08fa
            com.appsflyer.internal.g r1 = com.appsflyer.internal.g.m154()     // Catch:{ all -> 0x0c67 }
            r3 = r19
            r1.m155(r3, r6, r7)     // Catch:{ all -> 0x0c67 }
            java.lang.String r1 = r2.f43     // Catch:{ all -> 0x0c67 }
            if (r1 == 0) goto L_0x08ed
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ all -> 0x0c67 }
            java.lang.String r3 = r2.f43     // Catch:{ all -> 0x0c67 }
            r1.<init>(r3)     // Catch:{ all -> 0x0c67 }
            java.lang.String r3 = "isPush"
            r4 = r26
            r1.put(r3, r4)     // Catch:{ all -> 0x0c67 }
            java.lang.String r3 = "af_deeplink"
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0c67 }
            r7.put(r3, r1)     // Catch:{ all -> 0x0c67 }
            goto L_0x08ef
        L_0x08ed:
            r4 = r26
        L_0x08ef:
            r1 = 0
            r2.f43 = r1     // Catch:{ all -> 0x0c67 }
            java.lang.String r1 = "open_referrer"
            r3 = r16
            r7.put(r1, r3)     // Catch:{ all -> 0x0c67 }
            goto L_0x08fc
        L_0x08fa:
            r4 = r26
        L_0x08fc:
            boolean r1 = r2.f44     // Catch:{ all -> 0x0c67 }
            if (r1 == 0) goto L_0x0916
            java.lang.String r1 = "testAppMode_retargeting"
            r7.put(r1, r4)     // Catch:{ all -> 0x0c67 }
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ all -> 0x0c67 }
            r1.<init>(r7)     // Catch:{ all -> 0x0c67 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0c67 }
            m26(r6, r1)     // Catch:{ all -> 0x0c67 }
            java.lang.String r1 = "Sent retargeting params to test app"
            com.appsflyer.AFLogger.afInfoLog(r1)     // Catch:{ all -> 0x0c67 }
        L_0x0916:
            long r8 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0c67 }
            long r13 = r2.f42     // Catch:{ all -> 0x0c67 }
            long r8 = r8 - r13
            com.appsflyer.AppsFlyerProperties r1 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x0c67 }
            java.lang.String r1 = r1.getReferrer(r6)     // Catch:{ all -> 0x0c67 }
            r13 = 30000(0x7530, double:1.4822E-319)
            int r3 = (r8 > r13 ? 1 : (r8 == r13 ? 0 : -1))
            if (r3 > 0) goto L_0x0937
            if (r1 == 0) goto L_0x0937
            java.lang.String r3 = "AppsFlyer_Test"
            boolean r1 = r1.contains(r3)     // Catch:{ all -> 0x0c67 }
            if (r1 == 0) goto L_0x0937
            r1 = 1
            goto L_0x0938
        L_0x0937:
            r1 = 0
        L_0x0938:
            if (r1 == 0) goto L_0x0959
            java.lang.String r1 = "testAppMode"
            r7.put(r1, r4)     // Catch:{ all -> 0x0c67 }
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ all -> 0x0c67 }
            r1.<init>(r7)     // Catch:{ all -> 0x0c67 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0c67 }
            m26(r6, r1)     // Catch:{ all -> 0x0c67 }
            java.lang.String r1 = "Sent params to test app"
            com.appsflyer.AFLogger.afInfoLog(r1)     // Catch:{ all -> 0x0c67 }
            java.lang.String r1 = "Test mode ended!"
            com.appsflyer.AFLogger.afInfoLog(r1)     // Catch:{ all -> 0x0c67 }
            r8 = 0
            r2.f42 = r8     // Catch:{ all -> 0x0c67 }
        L_0x0959:
            java.lang.String r1 = "advertiserId"
            com.appsflyer.AppsFlyerProperties r3 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x0c67 }
            java.lang.String r1 = r3.getString(r1)     // Catch:{ all -> 0x0c67 }
            if (r1 != 0) goto L_0x0981
            com.appsflyer.internal.p.m177(r6, r7)     // Catch:{ all -> 0x0c67 }
            java.lang.String r1 = "advertiserId"
            com.appsflyer.AppsFlyerProperties r3 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x0c67 }
            java.lang.String r1 = r3.getString(r1)     // Catch:{ all -> 0x0c67 }
            if (r1 == 0) goto L_0x097a
            java.lang.String r1 = "GAID_retry"
            r7.put(r1, r4)     // Catch:{ all -> 0x0c67 }
            goto L_0x0981
        L_0x097a:
            java.lang.String r1 = "GAID_retry"
            java.lang.String r3 = "false"
            r7.put(r1, r3)     // Catch:{ all -> 0x0c67 }
        L_0x0981:
            android.content.ContentResolver r1 = r6.getContentResolver()     // Catch:{ all -> 0x0c67 }
            com.appsflyer.internal.s r1 = com.appsflyer.internal.p.m178(r1)     // Catch:{ all -> 0x0c67 }
            if (r1 == 0) goto L_0x099d
            java.lang.String r3 = "amazon_aid"
            java.lang.String r5 = r1.f271     // Catch:{ all -> 0x0c67 }
            r7.put(r3, r5)     // Catch:{ all -> 0x0c67 }
            java.lang.String r3 = "amazon_aid_limit"
            boolean r1 = r1.f272     // Catch:{ all -> 0x0c67 }
            java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ all -> 0x0c67 }
            r7.put(r3, r1)     // Catch:{ all -> 0x0c67 }
        L_0x099d:
            com.appsflyer.AppsFlyerProperties r1 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x0c67 }
            java.lang.String r1 = r1.getReferrer(r6)     // Catch:{ all -> 0x0c67 }
            if (r1 == 0) goto L_0x09ba
            int r3 = r1.length()     // Catch:{ all -> 0x0c67 }
            if (r3 <= 0) goto L_0x09ba
            java.lang.String r3 = "referrer"
            java.lang.Object r3 = r7.get(r3)     // Catch:{ all -> 0x0c67 }
            if (r3 != 0) goto L_0x09ba
            java.lang.String r3 = "referrer"
            r7.put(r3, r1)     // Catch:{ all -> 0x0c67 }
        L_0x09ba:
            java.lang.String r1 = "sentSuccessfully"
            r3 = r25
            java.lang.String r1 = r12.getString(r1, r3)     // Catch:{ all -> 0x0c67 }
            boolean r1 = r4.equals(r1)     // Catch:{ all -> 0x0c67 }
            java.lang.String r3 = "sentRegisterRequestToAF"
            r4 = 0
            boolean r3 = r12.getBoolean(r3, r4)     // Catch:{ all -> 0x0c67 }
            java.lang.String r4 = "registeredUninstall"
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r3)     // Catch:{ all -> 0x0c67 }
            r7.put(r4, r3)     // Catch:{ all -> 0x0c67 }
            r3 = r23
            int r4 = m48(r12, r10, r3)     // Catch:{ all -> 0x0c67 }
            java.lang.String r5 = "counter"
            java.lang.String r8 = java.lang.Integer.toString(r4)     // Catch:{ all -> 0x0c67 }
            r7.put(r5, r8)     // Catch:{ all -> 0x0c67 }
            java.lang.String r5 = "iaecounter"
            if (r22 == 0) goto L_0x09eb
            r8 = 1
            goto L_0x09ec
        L_0x09eb:
            r8 = 0
        L_0x09ec:
            java.lang.String r9 = "appsFlyerInAppEventCount"
            int r8 = m48(r12, r9, r8)     // Catch:{ all -> 0x0c67 }
            java.lang.String r8 = java.lang.Integer.toString(r8)     // Catch:{ all -> 0x0c67 }
            r7.put(r5, r8)     // Catch:{ all -> 0x0c67 }
            if (r3 == 0) goto L_0x0a1c
            r5 = 1
            if (r4 != r5) goto L_0x0a1c
            com.appsflyer.AppsFlyerProperties r5 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x0c67 }
            r5.setFirstLaunchCalled()     // Catch:{ all -> 0x0c67 }
            java.lang.String r5 = "waitForCustomerId"
            com.appsflyer.AppsFlyerProperties r8 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x0c67 }
            r9 = 0
            boolean r5 = r8.getBoolean(r5, r9)     // Catch:{ all -> 0x0c67 }
            if (r5 == 0) goto L_0x0a1c
            java.lang.String r5 = "wait_cid"
            r8 = 1
            java.lang.String r9 = java.lang.Boolean.toString(r8)     // Catch:{ all -> 0x0c67 }
            r7.put(r5, r9)     // Catch:{ all -> 0x0c67 }
        L_0x0a1c:
            java.lang.String r5 = "isFirstCall"
            if (r1 != 0) goto L_0x0a22
            r1 = 1
            goto L_0x0a23
        L_0x0a22:
            r1 = 0
        L_0x0a23:
            java.lang.String r1 = java.lang.Boolean.toString(r1)     // Catch:{ all -> 0x0c67 }
            r7.put(r5, r1)     // Catch:{ all -> 0x0c67 }
            java.util.HashMap r1 = new java.util.HashMap     // Catch:{ all -> 0x0c67 }
            r1.<init>()     // Catch:{ all -> 0x0c67 }
            java.lang.String r5 = "cpu_abi"
            java.lang.String r8 = "ro.product.cpu.abi"
            java.lang.String r8 = m50(r8)     // Catch:{ all -> 0x0c67 }
            r1.put(r5, r8)     // Catch:{ all -> 0x0c67 }
            java.lang.String r5 = "cpu_abi2"
            java.lang.String r8 = "ro.product.cpu.abi2"
            java.lang.String r8 = m50(r8)     // Catch:{ all -> 0x0c67 }
            r1.put(r5, r8)     // Catch:{ all -> 0x0c67 }
            java.lang.String r5 = "arch"
            java.lang.String r8 = "os.arch"
            java.lang.String r8 = m50(r8)     // Catch:{ all -> 0x0c67 }
            r1.put(r5, r8)     // Catch:{ all -> 0x0c67 }
            java.lang.String r5 = "build_display_id"
            java.lang.String r8 = "ro.build.display.id"
            java.lang.String r8 = m50(r8)     // Catch:{ all -> 0x0c67 }
            r1.put(r5, r8)     // Catch:{ all -> 0x0c67 }
            if (r3 == 0) goto L_0x0ad6
            boolean r5 = r2.f45     // Catch:{ all -> 0x0c67 }
            if (r5 == 0) goto L_0x0aa2
            com.appsflyer.internal.o.m174()     // Catch:{ all -> 0x0c67 }
            android.location.Location r5 = com.appsflyer.internal.o.m176(r6)     // Catch:{ all -> 0x0c67 }
            java.util.HashMap r8 = new java.util.HashMap     // Catch:{ all -> 0x0c67 }
            r9 = 3
            r8.<init>(r9)     // Catch:{ all -> 0x0c67 }
            if (r5 == 0) goto L_0x0a97
            java.lang.String r9 = "lat"
            double r13 = r5.getLatitude()     // Catch:{ all -> 0x0c67 }
            java.lang.String r11 = java.lang.String.valueOf(r13)     // Catch:{ all -> 0x0c67 }
            r8.put(r9, r11)     // Catch:{ all -> 0x0c67 }
            java.lang.String r9 = "lon"
            double r13 = r5.getLongitude()     // Catch:{ all -> 0x0c67 }
            java.lang.String r11 = java.lang.String.valueOf(r13)     // Catch:{ all -> 0x0c67 }
            r8.put(r9, r11)     // Catch:{ all -> 0x0c67 }
            java.lang.String r9 = "ts"
            long r13 = r5.getTime()     // Catch:{ all -> 0x0c67 }
            java.lang.String r5 = java.lang.String.valueOf(r13)     // Catch:{ all -> 0x0c67 }
            r8.put(r9, r5)     // Catch:{ all -> 0x0c67 }
        L_0x0a97:
            boolean r5 = r8.isEmpty()     // Catch:{ all -> 0x0c67 }
            if (r5 != 0) goto L_0x0aa2
            java.lang.String r5 = "loc"
            r1.put(r5, r8)     // Catch:{ all -> 0x0c67 }
        L_0x0aa2:
            com.appsflyer.internal.e r5 = com.appsflyer.internal.e.m149()     // Catch:{ all -> 0x0c67 }
            com.appsflyer.internal.e$c r5 = r5.m150(r6)     // Catch:{ all -> 0x0c67 }
            java.lang.String r8 = "btl"
            float r9 = r5.f198     // Catch:{ all -> 0x0c67 }
            java.lang.String r9 = java.lang.Float.toString(r9)     // Catch:{ all -> 0x0c67 }
            r1.put(r8, r9)     // Catch:{ all -> 0x0c67 }
            java.lang.String r8 = r5.f197     // Catch:{ all -> 0x0c67 }
            if (r8 == 0) goto L_0x0ac0
            java.lang.String r8 = "btch"
            java.lang.String r5 = r5.f197     // Catch:{ all -> 0x0c67 }
            r1.put(r8, r5)     // Catch:{ all -> 0x0c67 }
        L_0x0ac0:
            r5 = 2
            if (r5 < r4) goto L_0x0ad6
            com.appsflyer.internal.m r4 = com.appsflyer.internal.m.m165(r6)     // Catch:{ all -> 0x0c67 }
            java.util.List r4 = r4.m166()     // Catch:{ all -> 0x0c67 }
            boolean r5 = r4.isEmpty()     // Catch:{ all -> 0x0c67 }
            if (r5 != 0) goto L_0x0ad6
            java.lang.String r5 = "sensors"
            r1.put(r5, r4)     // Catch:{ all -> 0x0c67 }
        L_0x0ad6:
            java.util.Map r4 = com.appsflyer.AFExecutor.AnonymousClass2.m2(r6)     // Catch:{ all -> 0x0c67 }
            java.lang.String r5 = "dim"
            r1.put(r5, r4)     // Catch:{ all -> 0x0c67 }
            java.lang.String r4 = "deviceData"
            r7.put(r4, r1)     // Catch:{ all -> 0x0c67 }
            com.appsflyer.internal.ad r1 = new com.appsflyer.internal.ad     // Catch:{ all -> 0x0c67 }
            r1.<init>()     // Catch:{ all -> 0x0c67 }
            r1 = r17
            java.lang.Object r4 = r7.get(r1)     // Catch:{ all -> 0x0c67 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ all -> 0x0c67 }
            r5 = r24
            java.lang.Object r8 = r7.get(r5)     // Catch:{ all -> 0x0c67 }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ all -> 0x0c67 }
            java.lang.String r9 = "uid"
            java.lang.Object r9 = r7.get(r9)     // Catch:{ all -> 0x0c67 }
            java.lang.String r9 = (java.lang.String) r9     // Catch:{ all -> 0x0c67 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x0c67 }
            r11.<init>()     // Catch:{ all -> 0x0c67 }
            r13 = 7
            r14 = 0
            java.lang.String r4 = r4.substring(r14, r13)     // Catch:{ all -> 0x0c67 }
            r11.append(r4)     // Catch:{ all -> 0x0c67 }
            r4 = 7
            java.lang.String r4 = r9.substring(r14, r4)     // Catch:{ all -> 0x0c67 }
            r11.append(r4)     // Catch:{ all -> 0x0c67 }
            int r4 = r8.length()     // Catch:{ all -> 0x0c67 }
            int r4 = r4 + -7
            java.lang.String r4 = r8.substring(r4)     // Catch:{ all -> 0x0c67 }
            r11.append(r4)     // Catch:{ all -> 0x0c67 }
            java.lang.String r4 = r11.toString()     // Catch:{ all -> 0x0c67 }
            java.lang.String r4 = com.appsflyer.internal.ad.m121(r4)     // Catch:{ all -> 0x0c67 }
            java.lang.String r8 = "af_v"
            r7.put(r8, r4)     // Catch:{ all -> 0x0c67 }
            com.appsflyer.internal.ad r4 = new com.appsflyer.internal.ad     // Catch:{ all -> 0x0c67 }
            r4.<init>()     // Catch:{ all -> 0x0c67 }
            java.lang.Object r1 = r7.get(r1)     // Catch:{ all -> 0x0c67 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x0c67 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0c67 }
            r4.<init>()     // Catch:{ all -> 0x0c67 }
            r4.append(r1)     // Catch:{ all -> 0x0c67 }
            java.lang.Object r1 = r7.get(r5)     // Catch:{ all -> 0x0c67 }
            r4.append(r1)     // Catch:{ all -> 0x0c67 }
            java.lang.String r1 = r4.toString()     // Catch:{ all -> 0x0c67 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0c67 }
            r4.<init>()     // Catch:{ all -> 0x0c67 }
            r4.append(r1)     // Catch:{ all -> 0x0c67 }
            java.lang.String r1 = "uid"
            java.lang.Object r1 = r7.get(r1)     // Catch:{ all -> 0x0c67 }
            r4.append(r1)     // Catch:{ all -> 0x0c67 }
            java.lang.String r1 = r4.toString()     // Catch:{ all -> 0x0c67 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0c67 }
            r4.<init>()     // Catch:{ all -> 0x0c67 }
            r4.append(r1)     // Catch:{ all -> 0x0c67 }
            java.lang.String r1 = "installDate"
            java.lang.Object r1 = r7.get(r1)     // Catch:{ all -> 0x0c67 }
            r4.append(r1)     // Catch:{ all -> 0x0c67 }
            java.lang.String r1 = r4.toString()     // Catch:{ all -> 0x0c67 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0c67 }
            r4.<init>()     // Catch:{ all -> 0x0c67 }
            r4.append(r1)     // Catch:{ all -> 0x0c67 }
            java.lang.String r1 = "counter"
            java.lang.Object r1 = r7.get(r1)     // Catch:{ all -> 0x0c67 }
            r4.append(r1)     // Catch:{ all -> 0x0c67 }
            java.lang.String r1 = r4.toString()     // Catch:{ all -> 0x0c67 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0c67 }
            r4.<init>()     // Catch:{ all -> 0x0c67 }
            r4.append(r1)     // Catch:{ all -> 0x0c67 }
            java.lang.String r1 = "iaecounter"
            java.lang.Object r1 = r7.get(r1)     // Catch:{ all -> 0x0c67 }
            r4.append(r1)     // Catch:{ all -> 0x0c67 }
            java.lang.String r1 = r4.toString()     // Catch:{ all -> 0x0c67 }
            java.lang.String r1 = com.appsflyer.internal.ad.m119(r1)     // Catch:{ all -> 0x0c67 }
            java.lang.String r1 = com.appsflyer.internal.ad.m121(r1)     // Catch:{ all -> 0x0c67 }
            java.lang.String r4 = "af_v2"
            r7.put(r4, r1)     // Catch:{ all -> 0x0c67 }
            boolean r1 = m20(r6)     // Catch:{ all -> 0x0c67 }
            java.lang.String r4 = "ivc"
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)     // Catch:{ all -> 0x0c67 }
            r7.put(r4, r1)     // Catch:{ all -> 0x0c67 }
            if (r3 == 0) goto L_0x0c19
            r1 = 0
            int r3 = m48(r12, r10, r1)     // Catch:{ all -> 0x0c67 }
            r4 = 2
            if (r3 > r4) goto L_0x0c19
            r3 = 28260(0x6e64, float:3.96E-41)
            r4 = 24
            java.lang.Object r3 = com.appsflyer.internal.c.m147(r1, r3, r4)     // Catch:{ all -> 0x0c0f }
            java.lang.Class r3 = (java.lang.Class) r3     // Catch:{ all -> 0x0c0f }
            java.lang.String r1 = "ˏ"
            r4 = 0
            java.lang.reflect.Method r1 = r3.getMethod(r1, r4)     // Catch:{ all -> 0x0c0f }
            java.lang.Object r1 = r1.invoke(r4, r4)     // Catch:{ all -> 0x0c0f }
            r3 = 2
            java.lang.Object[] r4 = new java.lang.Object[r3]     // Catch:{ all -> 0x0c05 }
            r3 = 1
            r4[r3] = r7     // Catch:{ all -> 0x0c05 }
            r3 = 0
            r4[r3] = r6     // Catch:{ all -> 0x0c05 }
            r5 = 28260(0x6e64, float:3.96E-41)
            r6 = 24
            java.lang.Object r5 = com.appsflyer.internal.c.m147(r3, r5, r6)     // Catch:{ all -> 0x0c05 }
            java.lang.Class r5 = (java.lang.Class) r5     // Catch:{ all -> 0x0c05 }
            java.lang.String r6 = "ˏ"
            r8 = 2
            java.lang.Class[] r8 = new java.lang.Class[r8]     // Catch:{ all -> 0x0c05 }
            java.lang.Class<android.content.Context> r9 = android.content.Context.class
            r8[r3] = r9     // Catch:{ all -> 0x0c05 }
            java.lang.Class<java.util.Map> r3 = java.util.Map.class
            r9 = 1
            r8[r9] = r3     // Catch:{ all -> 0x0c05 }
            java.lang.reflect.Method r3 = r5.getMethod(r6, r8)     // Catch:{ all -> 0x0c05 }
            r3.invoke(r1, r4)     // Catch:{ all -> 0x0c05 }
            goto L_0x0c19
        L_0x0c05:
            r0 = move-exception
            r1 = r0
            java.lang.Throwable r3 = r1.getCause()     // Catch:{ all -> 0x0c67 }
            if (r3 == 0) goto L_0x0c0e
            throw r3     // Catch:{ all -> 0x0c67 }
        L_0x0c0e:
            throw r1     // Catch:{ all -> 0x0c67 }
        L_0x0c0f:
            r0 = move-exception
            r1 = r0
            java.lang.Throwable r3 = r1.getCause()     // Catch:{ all -> 0x0c67 }
            if (r3 == 0) goto L_0x0c18
            throw r3     // Catch:{ all -> 0x0c67 }
        L_0x0c18:
            throw r1     // Catch:{ all -> 0x0c67 }
        L_0x0c19:
            java.lang.String r1 = "is_stop_tracking_used"
            boolean r1 = r12.contains(r1)     // Catch:{ all -> 0x0c67 }
            if (r1 == 0) goto L_0x0c31
            java.lang.String r1 = "istu"
            java.lang.String r3 = "is_stop_tracking_used"
            r4 = 0
            boolean r3 = r12.getBoolean(r3, r4)     // Catch:{ all -> 0x0c67 }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ all -> 0x0c67 }
            r7.put(r1, r3)     // Catch:{ all -> 0x0c67 }
        L_0x0c31:
            com.appsflyer.AppsFlyerProperties r1 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x0c67 }
            java.lang.String r3 = "consumeAfDeepLink"
            java.lang.Object r1 = r1.getObject(r3)     // Catch:{ all -> 0x0c67 }
            if (r1 == 0) goto L_0x0c73
            com.appsflyer.AppsFlyerProperties r1 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x0c67 }
            java.lang.String r3 = "consumeAfDeepLink"
            r4 = 0
            boolean r1 = r1.getBoolean(r3, r4)     // Catch:{ all -> 0x0c67 }
            java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ all -> 0x0c67 }
            java.lang.String r3 = "is_dp_api"
            r7.put(r3, r1)     // Catch:{ all -> 0x0c67 }
            goto L_0x0c73
        L_0x0c52:
            r2 = r29
            java.lang.String r1 = "AppsFlyer dev key is missing!!! Please use  AppsFlyerLib.getInstance().setAppsFlyerKey(...) to set it. "
            com.appsflyer.AFLogger.afInfoLog(r1)     // Catch:{ all -> 0x0c67 }
            java.lang.String r1 = "DEV_KEY_MISSING"
            r3 = r22
            r4 = 0
            m29(r6, r3, r1, r4)     // Catch:{ all -> 0x0c67 }
            java.lang.String r1 = "AppsFlyer will not track this event."
            com.appsflyer.AFLogger.afInfoLog(r1)     // Catch:{ all -> 0x0c67 }
            return r4
        L_0x0c67:
            r0 = move-exception
            goto L_0x0c6b
        L_0x0c69:
            r0 = move-exception
            r2 = r1
        L_0x0c6b:
            r1 = r0
            java.lang.String r3 = r1.getLocalizedMessage()
            com.appsflyer.AFLogger.afErrorLog(r3, r1)
        L_0x0c73:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsflyer.AppsFlyerLibCore.m74(com.appsflyer.internal.h):java.util.Map");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.appsflyer.AppsFlyerLibCore.ˏ(android.content.SharedPreferences, java.lang.String, boolean):int
     arg types: [android.content.SharedPreferences, java.lang.String, int]
     candidates:
      com.appsflyer.AppsFlyerLibCore.ˏ(android.content.Context, java.lang.String, long):void
      com.appsflyer.AppsFlyerLibCore.ˏ(android.content.SharedPreferences, java.lang.String, boolean):int */
    /* renamed from: ˎ  reason: contains not printable characters */
    public final void m77(WeakReference<Context> weakReference) {
        if (weakReference.get() != null) {
            AFLogger.afInfoLog("app went to background");
            SharedPreferences sharedPreferences = weakReference.get().getApplicationContext().getSharedPreferences("appsflyer-data", 0);
            AppsFlyerProperties.getInstance().saveProperties(sharedPreferences);
            long j2 = this.f60 - this.f37;
            HashMap hashMap = new HashMap();
            String string = AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.AF_KEY);
            if (string == null) {
                AFLogger.afWarnLog("[callStats] AppsFlyer's SDK cannot send any event without providing DevKey.");
                return;
            }
            String string2 = AppsFlyerProperties.getInstance().getString("KSAppsFlyerId");
            if (AppsFlyerProperties.getInstance().getBoolean(AppsFlyerProperties.DEVICE_TRACKING_DISABLED, false)) {
                hashMap.put(AppsFlyerProperties.DEVICE_TRACKING_DISABLED, "true");
            }
            s r7 = p.m178(weakReference.get().getContentResolver());
            if (r7 != null) {
                hashMap.put("amazon_aid", r7.f271);
                hashMap.put("amazon_aid_limit", String.valueOf(r7.f272));
            }
            String string3 = AppsFlyerProperties.getInstance().getString(ServerParameters.ADVERTISING_ID_PARAM);
            if (string3 != null) {
                hashMap.put(ServerParameters.ADVERTISING_ID_PARAM, string3);
            }
            hashMap.put("app_id", weakReference.get().getPackageName());
            hashMap.put("devkey", string);
            hashMap.put("uid", ab.m111(weakReference));
            hashMap.put("time_in_app", String.valueOf(j2 / 1000));
            hashMap.put("statType", "user_closed_app");
            hashMap.put(TapjoyConstants.TJC_PLATFORM, "Android");
            hashMap.put("launch_counter", Integer.toString(m48(sharedPreferences, "appsFlyerCount", false)));
            hashMap.put(AppsFlyerProperties.CHANNEL, m64(weakReference));
            if (string2 == null) {
                string2 = "";
            }
            hashMap.put("originalAppsflyerId", string2);
            if (this.f52) {
                try {
                    x xVar = new x(null, isTrackingStopped());
                    xVar.f296 = hashMap;
                    if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
                        AFLogger.afDebugLog("Main thread detected. Running callStats task in a new thread.");
                        xVar.execute(ServerConfigHandler.getUrl("https://%sstats.%s/stats"));
                        return;
                    }
                    StringBuilder sb = new StringBuilder("Running callStats task (on current thread: ");
                    sb.append(Thread.currentThread().toString());
                    sb.append(" )");
                    AFLogger.afDebugLog(sb.toString());
                    xVar.onPreExecute();
                    xVar.onPostExecute(xVar.doInBackground(ServerConfigHandler.getUrl("https://%sstats.%s/stats")));
                } catch (Throwable th) {
                    AFLogger.afErrorLog("Could not send callStats request", th);
                }
            } else {
                AFLogger.afDebugLog("Stats call is disabled, ignore ...");
            }
        }
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    static /* synthetic */ void m69(AppsFlyerLibCore appsFlyerLibCore, h hVar) throws IOException {
        String str = hVar.f220;
        StringBuilder sb = new StringBuilder("url: ");
        sb.append(hVar.f224);
        AFLogger.afInfoLog(sb.toString());
        AFFacebookDeferredDeeplink.AnonymousClass2.m5("data: ".concat(String.valueOf(str)));
        Context context = hVar.f221;
        Context context2 = null;
        if (context == null) {
            WeakReference<Context> weakReference = hVar.f222;
            context = weakReference != null ? weakReference.get() : null;
        }
        m29(context, LOG_TAG, "EVENT_DATA", str);
        try {
            appsFlyerLibCore.m57(hVar);
        } catch (IOException e2) {
            AFLogger.afErrorLog("Exception in sendRequestToServer. ", e2);
            if (AppsFlyerProperties.getInstance().getBoolean(AppsFlyerProperties.USE_HTTP_FALLBACK, false)) {
                hVar.f224 = hVar.f224.replace("https:", "http:");
                appsFlyerLibCore.m57(hVar);
                return;
            }
            StringBuilder sb2 = new StringBuilder("failed to send requeset to server. ");
            sb2.append(e2.getLocalizedMessage());
            AFLogger.afInfoLog(sb2.toString());
            Context context3 = hVar.f221;
            if (context3 == null) {
                WeakReference<Context> weakReference2 = hVar.f222;
                if (weakReference2 != null) {
                    context2 = weakReference2.get();
                }
            } else {
                context2 = context3;
            }
            m29(context2, LOG_TAG, "ERROR", e2.getLocalizedMessage());
            throw e2;
        }
    }

    /* JADX WARN: Type inference failed for: r0v6, types: [java.net.URLConnection] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.appsflyer.AppsFlyerLibCore.ˏ(android.content.SharedPreferences, java.lang.String, boolean):int
     arg types: [android.content.SharedPreferences, java.lang.String, int]
     candidates:
      com.appsflyer.AppsFlyerLibCore.ˏ(android.content.Context, java.lang.String, long):void
      com.appsflyer.AppsFlyerLibCore.ˏ(android.content.SharedPreferences, java.lang.String, boolean):int */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x0258  */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x0260 A[Catch:{ q -> 0x0243, all -> 0x0264 }] */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x026b  */
    /* renamed from: ˏ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m57(com.appsflyer.internal.h r23) throws java.io.IOException {
        /*
            r22 = this;
            r1 = r22
            r0 = r23
            java.lang.String r2 = "is_first_launch"
            java.lang.String r3 = "appsflyerConversionDataCacheExpiration"
            java.lang.String r4 = "appsflyer-data"
            java.net.URL r5 = new java.net.URL
            java.lang.String r6 = r0.f224
            r5.<init>(r6)
            java.lang.String r6 = r0.f220
            java.lang.String r7 = r0.f227
            java.lang.String r8 = r0.f215
            boolean r9 = r0.f214
            android.content.Context r10 = r0.f221
            if (r10 == 0) goto L_0x001e
            goto L_0x002a
        L_0x001e:
            java.lang.ref.WeakReference<android.content.Context> r10 = r0.f222
            if (r10 == 0) goto L_0x0029
            java.lang.Object r10 = r10.get()
            android.content.Context r10 = (android.content.Context) r10
            goto L_0x002a
        L_0x0029:
            r10 = 0
        L_0x002a:
            com.appsflyer.AppsFlyerTrackingRequestListener r12 = r0.f225
            r13 = 1
            if (r9 == 0) goto L_0x0035
            com.appsflyer.AppsFlyerConversionListener r15 = com.appsflyer.AppsFlyerLibCore.f31
            if (r15 == 0) goto L_0x0035
            r15 = 1
            goto L_0x0036
        L_0x0035:
            r15 = 0
        L_0x0036:
            com.appsflyer.internal.aa r16 = com.appsflyer.internal.aa.f104     // Catch:{ all -> 0x0266 }
            if (r16 != 0) goto L_0x0046
            com.appsflyer.internal.aa r16 = new com.appsflyer.internal.aa     // Catch:{ all -> 0x0042 }
            r16.<init>()     // Catch:{ all -> 0x0042 }
            com.appsflyer.internal.aa.f104 = r16     // Catch:{ all -> 0x0042 }
            goto L_0x0046
        L_0x0042:
            r0 = move-exception
            r11 = 0
            goto L_0x0269
        L_0x0046:
            com.appsflyer.internal.aa r11 = com.appsflyer.internal.aa.f104     // Catch:{ all -> 0x0266 }
            java.lang.String r0 = r0.f224     // Catch:{ all -> 0x0266 }
            java.lang.String r14 = "server_request"
            r18 = r2
            java.lang.String[] r2 = new java.lang.String[r13]     // Catch:{ all -> 0x0266 }
            r17 = 0
            r2[r17] = r6     // Catch:{ all -> 0x0266 }
            r11.m108(r14, r0, r2)     // Catch:{ all -> 0x0266 }
            java.net.URLConnection r0 = r5.openConnection()     // Catch:{ all -> 0x0266 }
            java.lang.Object r0 = com.google.firebase.perf.network.FirebasePerfUrlConnection.instrument(r0)     // Catch:{ all -> 0x0266 }
            java.net.URLConnection r0 = (java.net.URLConnection) r0     // Catch:{ all -> 0x0266 }
            r11 = r0
            java.net.HttpURLConnection r11 = (java.net.HttpURLConnection) r11     // Catch:{ all -> 0x0266 }
            java.lang.String r0 = "POST"
            r11.setRequestMethod(r0)     // Catch:{ all -> 0x0264 }
            byte[] r0 = r6.getBytes()     // Catch:{ all -> 0x0264 }
            int r0 = r0.length     // Catch:{ all -> 0x0264 }
            java.lang.String r2 = "Content-Length"
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x0264 }
            r11.setRequestProperty(r2, r0)     // Catch:{ all -> 0x0264 }
            java.lang.String r0 = "Content-Type"
            java.lang.String r2 = "application/json"
            r11.setRequestProperty(r0, r2)     // Catch:{ all -> 0x0264 }
            r0 = 10000(0x2710, float:1.4013E-41)
            r11.setConnectTimeout(r0)     // Catch:{ all -> 0x0264 }
            r11.setDoOutput(r13)     // Catch:{ all -> 0x0264 }
            java.io.OutputStreamWriter r2 = new java.io.OutputStreamWriter     // Catch:{ all -> 0x0254 }
            java.io.OutputStream r0 = r11.getOutputStream()     // Catch:{ all -> 0x0254 }
            java.lang.String r14 = "UTF-8"
            r2.<init>(r0, r14)     // Catch:{ all -> 0x0254 }
            r2.write(r6)     // Catch:{ all -> 0x0251 }
            r2.close()     // Catch:{ all -> 0x0264 }
            int r0 = r11.getResponseCode()     // Catch:{ all -> 0x0264 }
            java.lang.String r2 = m65(r11)     // Catch:{ all -> 0x0264 }
            com.appsflyer.internal.aa r6 = com.appsflyer.internal.aa.f104     // Catch:{ all -> 0x0264 }
            if (r6 != 0) goto L_0x00aa
            com.appsflyer.internal.aa r6 = new com.appsflyer.internal.aa     // Catch:{ all -> 0x0264 }
            r6.<init>()     // Catch:{ all -> 0x0264 }
            com.appsflyer.internal.aa.f104 = r6     // Catch:{ all -> 0x0264 }
        L_0x00aa:
            com.appsflyer.internal.aa r6 = com.appsflyer.internal.aa.f104     // Catch:{ all -> 0x0264 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0264 }
            java.lang.String r14 = "server_response"
            r13 = 2
            java.lang.String[] r13 = new java.lang.String[r13]     // Catch:{ all -> 0x0264 }
            java.lang.String r20 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x0264 }
            r17 = 0
            r13[r17] = r20     // Catch:{ all -> 0x0264 }
            r19 = 1
            r13[r19] = r2     // Catch:{ all -> 0x0264 }
            r6.m108(r14, r5, r13)     // Catch:{ all -> 0x0264 }
            java.lang.String r5 = "response code: "
            java.lang.String r6 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x0264 }
            java.lang.String r5 = r5.concat(r6)     // Catch:{ all -> 0x0264 }
            com.appsflyer.AFLogger.afInfoLog(r5)     // Catch:{ all -> 0x0264 }
            java.lang.String r5 = "AppsFlyer_4.10.3"
            java.lang.String r6 = "SERVER_RESPONSE_CODE"
            java.lang.String r13 = java.lang.Integer.toString(r0)     // Catch:{ all -> 0x0264 }
            m29(r10, r5, r6, r13)     // Catch:{ all -> 0x0264 }
            android.content.Context r5 = r10.getApplicationContext()     // Catch:{ all -> 0x0264 }
            r6 = 0
            android.content.SharedPreferences r5 = r5.getSharedPreferences(r4, r6)     // Catch:{ all -> 0x0264 }
            r6 = 200(0xc8, float:2.8E-43)
            if (r0 != r6) goto L_0x0187
            if (r10 == 0) goto L_0x00f3
            if (r9 == 0) goto L_0x00f3
            long r13 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0264 }
            r1.f61 = r13     // Catch:{ all -> 0x0264 }
        L_0x00f3:
            if (r12 == 0) goto L_0x00f8
            r12.onTrackingRequestSuccess()     // Catch:{ all -> 0x0264 }
        L_0x00f8:
            java.lang.String r0 = "afUninstallToken"
            com.appsflyer.AppsFlyerProperties r6 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x0264 }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ all -> 0x0264 }
            if (r0 == 0) goto L_0x0131
            java.lang.String r6 = "Uninstall Token exists: "
            java.lang.String r9 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x0264 }
            java.lang.String r6 = r6.concat(r9)     // Catch:{ all -> 0x0264 }
            com.appsflyer.AFLogger.afDebugLog(r6)     // Catch:{ all -> 0x0264 }
            java.lang.String r6 = "sentRegisterRequestToAF"
            r9 = 0
            boolean r6 = r5.getBoolean(r6, r9)     // Catch:{ all -> 0x0264 }
            if (r6 != 0) goto L_0x0131
            java.lang.String r6 = "Resending Uninstall token to AF servers: "
            java.lang.String r9 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x0264 }
            java.lang.String r6 = r6.concat(r9)     // Catch:{ all -> 0x0264 }
            com.appsflyer.AFLogger.afDebugLog(r6)     // Catch:{ all -> 0x0264 }
            com.appsflyer.internal.b$b$a r6 = new com.appsflyer.internal.b$b$a     // Catch:{ all -> 0x0264 }
            r6.<init>(r0)     // Catch:{ all -> 0x0264 }
            java.lang.String r0 = r6.f182     // Catch:{ all -> 0x0264 }
            com.appsflyer.internal.l.a.m163(r10, r0)     // Catch:{ all -> 0x0264 }
        L_0x0131:
            android.net.Uri r0 = r1.latestDeepLink     // Catch:{ all -> 0x0264 }
            if (r0 == 0) goto L_0x0138
            r6 = 0
            r1.latestDeepLink = r6     // Catch:{ all -> 0x0264 }
        L_0x0138:
            if (r8 == 0) goto L_0x0140
            com.appsflyer.internal.w.m193()     // Catch:{ all -> 0x0264 }
            com.appsflyer.internal.w.m195(r8, r10)     // Catch:{ all -> 0x0264 }
        L_0x0140:
            if (r10 == 0) goto L_0x0179
            if (r8 != 0) goto L_0x0179
            java.lang.String r0 = "sentSuccessfully"
            java.lang.String r6 = "true"
            m28(r10, r0, r6)     // Catch:{ all -> 0x0264 }
            boolean r0 = r1.f36     // Catch:{ all -> 0x0264 }
            if (r0 != 0) goto L_0x0179
            long r8 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0264 }
            long r12 = r1.f40     // Catch:{ all -> 0x0264 }
            long r8 = r8 - r12
            r12 = 15000(0x3a98, double:7.411E-320)
            int r0 = (r8 > r12 ? 1 : (r8 == r12 ? 0 : -1))
            if (r0 >= 0) goto L_0x015d
            goto L_0x0179
        L_0x015d:
            java.util.concurrent.ScheduledExecutorService r0 = r1.f41     // Catch:{ all -> 0x0264 }
            if (r0 != 0) goto L_0x0179
            com.appsflyer.AFExecutor r0 = com.appsflyer.AFExecutor.getInstance()     // Catch:{ all -> 0x0264 }
            java.util.concurrent.ScheduledThreadPoolExecutor r0 = r0.m1()     // Catch:{ all -> 0x0264 }
            r1.f41 = r0     // Catch:{ all -> 0x0264 }
            com.appsflyer.AppsFlyerLibCore$b r0 = new com.appsflyer.AppsFlyerLibCore$b     // Catch:{ all -> 0x0264 }
            r0.<init>(r10)     // Catch:{ all -> 0x0264 }
            java.util.concurrent.ScheduledExecutorService r6 = r1.f41     // Catch:{ all -> 0x0264 }
            r8 = 1
            java.util.concurrent.TimeUnit r12 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ all -> 0x0264 }
            m46(r6, r0, r8, r12)     // Catch:{ all -> 0x0264 }
        L_0x0179:
            org.json.JSONObject r0 = com.appsflyer.ServerConfigHandler.m98(r2)     // Catch:{ all -> 0x0264 }
            java.lang.String r2 = "send_background"
            r6 = 0
            boolean r0 = r0.optBoolean(r2, r6)     // Catch:{ all -> 0x0264 }
            r1.f52 = r0     // Catch:{ all -> 0x0264 }
            goto L_0x0196
        L_0x0187:
            if (r12 == 0) goto L_0x0196
            java.lang.String r2 = "Failure: "
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x0264 }
            java.lang.String r0 = r2.concat(r0)     // Catch:{ all -> 0x0264 }
            r12.onTrackingRequestFailure(r0)     // Catch:{ all -> 0x0264 }
        L_0x0196:
            java.lang.String r0 = "appsflyerConversionDataRequestRetries"
            r2 = 0
            int r0 = r5.getInt(r0, r2)     // Catch:{ all -> 0x0264 }
            r8 = 0
            long r12 = r5.getLong(r3, r8)     // Catch:{ all -> 0x0264 }
            java.lang.String r2 = "attributionId"
            int r6 = (r12 > r8 ? 1 : (r12 == r8 ? 0 : -1))
            if (r6 == 0) goto L_0x01d5
            long r20 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0264 }
            long r20 = r20 - r12
            r12 = 5184000000(0x134fd9000, double:2.561236308E-314)
            int r6 = (r20 > r12 ? 1 : (r20 == r12 ? 0 : -1))
            if (r6 <= 0) goto L_0x01d5
            java.lang.String r6 = "sixtyDayConversionData"
            android.content.Context r12 = r10.getApplicationContext()     // Catch:{ all -> 0x0264 }
            r13 = 0
            android.content.SharedPreferences r4 = r12.getSharedPreferences(r4, r13)     // Catch:{ all -> 0x0264 }
            android.content.SharedPreferences$Editor r4 = r4.edit()     // Catch:{ all -> 0x0264 }
            r12 = 1
            r4.putBoolean(r6, r12)     // Catch:{ all -> 0x0264 }
            r4.apply()     // Catch:{ all -> 0x0264 }
            r4 = 0
            m28(r10, r2, r4)     // Catch:{ all -> 0x0264 }
            m68(r10, r3, r8)     // Catch:{ all -> 0x0264 }
        L_0x01d5:
            r3 = 0
            java.lang.String r4 = r5.getString(r2, r3)     // Catch:{ all -> 0x0264 }
            if (r4 != 0) goto L_0x0200
            if (r7 == 0) goto L_0x0200
            if (r15 == 0) goto L_0x0200
            com.appsflyer.AppsFlyerConversionListener r3 = com.appsflyer.AppsFlyerLibCore.f31     // Catch:{ all -> 0x0264 }
            if (r3 == 0) goto L_0x0200
            r3 = 5
            if (r0 > r3) goto L_0x0200
            com.appsflyer.AFExecutor r0 = com.appsflyer.AFExecutor.getInstance()     // Catch:{ all -> 0x0264 }
            java.util.concurrent.ScheduledThreadPoolExecutor r0 = r0.m1()     // Catch:{ all -> 0x0264 }
            com.appsflyer.AppsFlyerLibCore$e r2 = new com.appsflyer.AppsFlyerLibCore$e     // Catch:{ all -> 0x0264 }
            android.content.Context r3 = r10.getApplicationContext()     // Catch:{ all -> 0x0264 }
            r2.<init>(r3, r7, r0)     // Catch:{ all -> 0x0264 }
            r3 = 10
            java.util.concurrent.TimeUnit r5 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ all -> 0x0264 }
            m46(r0, r2, r3, r5)     // Catch:{ all -> 0x0264 }
            goto L_0x024b
        L_0x0200:
            if (r7 != 0) goto L_0x0208
            java.lang.String r0 = "AppsFlyer dev key is missing."
            com.appsflyer.AFLogger.afWarnLog(r0)     // Catch:{ all -> 0x0264 }
            goto L_0x024b
        L_0x0208:
            if (r15 == 0) goto L_0x024b
            com.appsflyer.AppsFlyerConversionListener r0 = com.appsflyer.AppsFlyerLibCore.f31     // Catch:{ all -> 0x0264 }
            if (r0 == 0) goto L_0x024b
            r3 = 0
            java.lang.String r0 = r5.getString(r2, r3)     // Catch:{ all -> 0x0264 }
            if (r0 == 0) goto L_0x024b
            java.lang.String r0 = "appsFlyerCount"
            r2 = 0
            int r0 = m48(r5, r0, r2)     // Catch:{ all -> 0x0264 }
            r3 = 1
            if (r0 <= r3) goto L_0x024b
            java.util.Map r0 = m40(r10)     // Catch:{ q -> 0x0243 }
            if (r0 == 0) goto L_0x024b
            r3 = r18
            boolean r4 = r0.containsKey(r3)     // Catch:{ all -> 0x023a }
            if (r4 != 0) goto L_0x0234
            java.lang.String r2 = java.lang.Boolean.toString(r2)     // Catch:{ all -> 0x023a }
            r0.put(r3, r2)     // Catch:{ all -> 0x023a }
        L_0x0234:
            com.appsflyer.AppsFlyerConversionListener r2 = com.appsflyer.AppsFlyerLibCore.f31     // Catch:{ all -> 0x023a }
            r2.onInstallConversionDataLoaded(r0)     // Catch:{ all -> 0x023a }
            goto L_0x024b
        L_0x023a:
            r0 = move-exception
            java.lang.String r2 = r0.getLocalizedMessage()     // Catch:{ q -> 0x0243 }
            com.appsflyer.AFLogger.afErrorLog(r2, r0)     // Catch:{ q -> 0x0243 }
            goto L_0x024b
        L_0x0243:
            r0 = move-exception
            java.lang.String r2 = r0.getMessage()     // Catch:{ all -> 0x0264 }
            com.appsflyer.AFLogger.afErrorLog(r2, r0)     // Catch:{ all -> 0x0264 }
        L_0x024b:
            if (r11 == 0) goto L_0x0250
            r11.disconnect()
        L_0x0250:
            return
        L_0x0251:
            r0 = move-exception
            r3 = r2
            goto L_0x0256
        L_0x0254:
            r0 = move-exception
            r3 = 0
        L_0x0256:
            if (r3 != 0) goto L_0x0260
            if (r12 == 0) goto L_0x0263
            java.lang.String r2 = "No Connectivity"
            r12.onTrackingRequestFailure(r2)     // Catch:{ all -> 0x0264 }
            goto L_0x0263
        L_0x0260:
            r3.close()     // Catch:{ all -> 0x0264 }
        L_0x0263:
            throw r0     // Catch:{ all -> 0x0264 }
        L_0x0264:
            r0 = move-exception
            goto L_0x0269
        L_0x0266:
            r0 = move-exception
            r3 = 0
            r11 = r3
        L_0x0269:
            if (r11 == 0) goto L_0x026e
            r11.disconnect()
        L_0x026e:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsflyer.AppsFlyerLibCore.m57(com.appsflyer.internal.h):void");
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    static /* synthetic */ void m30(Map map) {
        AppsFlyerConversionListener appsFlyerConversionListener = f31;
        if (appsFlyerConversionListener != null) {
            try {
                appsFlyerConversionListener.onAppOpenAttribution(map);
            } catch (Throwable th) {
                AFLogger.afErrorLog(th.getLocalizedMessage(), th);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public static Map<String, String> m41(String str) {
        HashMap hashMap = new HashMap();
        try {
            JSONObject jSONObject = new JSONObject(str);
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                if (!f27.contains(next)) {
                    String string = jSONObject.getString(next);
                    if (!TextUtils.isEmpty(string) && !"null".equals(string)) {
                        hashMap.put(next, string);
                    }
                }
            }
            return hashMap;
        } catch (JSONException e2) {
            AFLogger.afErrorLog(e2.getMessage(), e2);
            return null;
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private static String m39(String str, PackageManager packageManager, String str2) {
        Object obj;
        try {
            Bundle bundle = packageManager.getApplicationInfo(str2, 128).metaData;
            if (bundle == null || (obj = bundle.get(str)) == null) {
                return null;
            }
            return obj.toString();
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder("Could not find ");
            sb.append(str);
            sb.append(" value in the manifest");
            AFLogger.afErrorLog(sb.toString(), th);
            return null;
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private static void m46(ScheduledExecutorService scheduledExecutorService, Runnable runnable, long j2, TimeUnit timeUnit) {
        if (scheduledExecutorService != null) {
            try {
                if (!scheduledExecutorService.isShutdown() && !scheduledExecutorService.isTerminated()) {
                    scheduledExecutorService.schedule(runnable, j2, timeUnit);
                    return;
                }
            } catch (RejectedExecutionException e2) {
                AFLogger.afErrorLog("scheduleJob failed with RejectedExecutionException Exception", e2);
                return;
            } catch (Throwable th) {
                AFLogger.afErrorLog("scheduleJob failed with Exception", th);
                return;
            }
        }
        AFLogger.afWarnLog("scheduler is null, shut downed or terminated");
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private void m45(final h hVar) {
        Context context = hVar.f221;
        if (context == null) {
            WeakReference<Context> weakReference = hVar.f222;
            context = weakReference != null ? weakReference.get() : null;
        }
        Application application = (Application) context;
        AppsFlyerProperties.getInstance().loadProperties(application.getApplicationContext());
        if (Build.VERSION.SDK_INT < 14) {
            AFLogger.afInfoLog("SDK<14 call trackEvent manually");
            AFLogger.afInfoLog("onBecameForeground");
            getInstance().f37 = System.currentTimeMillis();
            getInstance().m73(hVar);
            AFLogger.resetDeltaTime();
        } else if (this.f39 == null) {
            if (u.f282 == null) {
                u.f282 = new u();
            }
            this.f39 = new u.a() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.appsflyer.AppsFlyerLibCore.ˏ(android.content.SharedPreferences, java.lang.String, boolean):int
                 arg types: [android.content.SharedPreferences, java.lang.String, int]
                 candidates:
                  com.appsflyer.AppsFlyerLibCore.ˏ(android.content.Context, java.lang.String, long):void
                  com.appsflyer.AppsFlyerLibCore.ˏ(android.content.SharedPreferences, java.lang.String, boolean):int */
                /* renamed from: ˊ  reason: contains not printable characters */
                public final void m78(Activity activity) {
                    if (2 > AppsFlyerLibCore.m48(AppsFlyerLibCore.m49(activity), "appsFlyerCount", false)) {
                        m r0 = m.m165(activity);
                        r0.f243.post(r0.f238);
                        r0.f243.post(r0.f244);
                    }
                    h hVar = hVar;
                    hVar.f221 = activity;
                    AFLogger.afInfoLog("onBecameForeground");
                    AppsFlyerLibCore.getInstance().f37 = System.currentTimeMillis();
                    AppsFlyerLibCore.getInstance().m73(hVar);
                    AFLogger.resetDeltaTime();
                }

                /* renamed from: ॱ  reason: contains not printable characters */
                public final void m79(WeakReference<Context> weakReference) {
                    g.AnonymousClass2.m156(weakReference.get());
                    m r2 = m.m165(weakReference.get());
                    r2.f243.post(r2.f238);
                }
            };
            u uVar = u.f282;
            if (uVar != null) {
                uVar.f284 = this.f39;
                if (Build.VERSION.SDK_INT >= 14) {
                    application.registerActivityLifecycleCallbacks(u.f282);
                    return;
                }
                return;
            }
            throw new IllegalStateException("Foreground is not initialised - invoke at least once with parameter init/get");
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public static Map<String, String> m40(Context context) throws q {
        String string = context.getApplicationContext().getSharedPreferences("appsflyer-data", 0).getString("attributionId", null);
        if (string != null && string.length() > 0) {
            return m41(string);
        }
        throw new q();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.appsflyer.AppsFlyerLibCore.ˏ(android.content.SharedPreferences, java.lang.String, boolean):int
     arg types: [android.content.SharedPreferences, java.lang.String, int]
     candidates:
      com.appsflyer.AppsFlyerLibCore.ˏ(android.content.Context, java.lang.String, long):void
      com.appsflyer.AppsFlyerLibCore.ˏ(android.content.SharedPreferences, java.lang.String, boolean):int */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0143  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x014e  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0166  */
    /* renamed from: ˏ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static /* synthetic */ void m56(com.appsflyer.AppsFlyerLibCore r9, com.appsflyer.internal.h r10) {
        /*
            android.content.Context r0 = r10.f221
            if (r0 == 0) goto L_0x0005
            goto L_0x0011
        L_0x0005:
            java.lang.ref.WeakReference<android.content.Context> r0 = r10.f222
            if (r0 == 0) goto L_0x0010
            java.lang.Object r0 = r0.get()
            android.content.Context r0 = (android.content.Context) r0
            goto L_0x0011
        L_0x0010:
            r0 = 0
        L_0x0011:
            java.lang.String r1 = r10.f216
            boolean r2 = r10.f223
            if (r0 != 0) goto L_0x001d
            java.lang.String r9 = "sendTrackingWithEvent - got null context. skipping event/launch."
            com.appsflyer.AFLogger.afDebugLog(r9)
            return
        L_0x001d:
            android.content.Context r3 = r0.getApplicationContext()
            r4 = 0
            java.lang.String r5 = "appsflyer-data"
            android.content.SharedPreferences r3 = r3.getSharedPreferences(r5, r4)
            com.appsflyer.AppsFlyerProperties r5 = com.appsflyer.AppsFlyerProperties.getInstance()
            r5.saveProperties(r3)
            boolean r5 = r9.isTrackingStopped()
            if (r5 != 0) goto L_0x004e
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "sendTrackingWithEvent from activity: "
            r5.<init>(r6)
            java.lang.Class r6 = r0.getClass()
            java.lang.String r6 = r6.getName()
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            com.appsflyer.AFLogger.afInfoLog(r5)
        L_0x004e:
            r5 = 1
            if (r1 != 0) goto L_0x0053
            r1 = 1
            goto L_0x0054
        L_0x0053:
            r1 = 0
        L_0x0054:
            r10.f214 = r1
            java.util.Map r6 = r9.m74(r10)
            java.lang.String r7 = "appsflyerKey"
            java.lang.Object r7 = r6.get(r7)
            java.lang.String r7 = (java.lang.String) r7
            if (r7 == 0) goto L_0x0175
            int r7 = r7.length()
            if (r7 != 0) goto L_0x006c
            goto L_0x0175
        L_0x006c:
            boolean r7 = r9.isTrackingStopped()
            if (r7 != 0) goto L_0x0077
            java.lang.String r7 = "AppsFlyerLib.sendTrackingWithEvent"
            com.appsflyer.AFLogger.afInfoLog(r7)
        L_0x0077:
            if (r1 == 0) goto L_0x0089
            if (r2 == 0) goto L_0x0082
            java.lang.String r2 = com.appsflyer.AppsFlyerLibCore.f24
            java.lang.String r2 = com.appsflyer.ServerConfigHandler.getUrl(r2)
            goto L_0x008f
        L_0x0082:
            java.lang.String r2 = com.appsflyer.AppsFlyerLibCore.f34
            java.lang.String r2 = com.appsflyer.ServerConfigHandler.getUrl(r2)
            goto L_0x008f
        L_0x0089:
            java.lang.String r2 = com.appsflyer.AppsFlyerLibCore.f32
            java.lang.String r2 = com.appsflyer.ServerConfigHandler.getUrl(r2)
        L_0x008f:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            r7.append(r2)
            java.lang.String r2 = r0.getPackageName()
            r7.append(r2)
            java.lang.String r2 = r7.toString()
            java.lang.String r7 = "appsFlyerCount"
            int r3 = m48(r3, r7, r4)
            com.appsflyer.AppsFlyerProperties r7 = com.appsflyer.AppsFlyerProperties.getInstance()
            java.lang.String r8 = "collectAndroidIdForceByUser"
            boolean r7 = r7.getBoolean(r8, r4)
            if (r7 != 0) goto L_0x00c3
            com.appsflyer.AppsFlyerProperties r7 = com.appsflyer.AppsFlyerProperties.getInstance()
            java.lang.String r8 = "collectIMEIForceByUser"
            boolean r7 = r7.getBoolean(r8, r4)
            if (r7 == 0) goto L_0x00c1
            goto L_0x00c3
        L_0x00c1:
            r7 = 0
            goto L_0x00c4
        L_0x00c3:
            r7 = 1
        L_0x00c4:
            if (r7 != 0) goto L_0x00ff
            java.lang.String r7 = "advertiserId"
            java.lang.Object r7 = r6.get(r7)
            if (r7 == 0) goto L_0x00ff
            java.lang.String r7 = r9.f46     // Catch:{ Exception -> 0x00f9 }
            boolean r7 = android.text.TextUtils.isEmpty(r7)     // Catch:{ Exception -> 0x00f9 }
            if (r7 == 0) goto L_0x00e3
            java.lang.String r7 = "android_id"
            java.lang.Object r7 = r6.remove(r7)     // Catch:{ Exception -> 0x00f9 }
            if (r7 == 0) goto L_0x00e3
            java.lang.String r7 = "validateGaidAndIMEI :: removing: android_id"
            com.appsflyer.AFLogger.afInfoLog(r7)     // Catch:{ Exception -> 0x00f9 }
        L_0x00e3:
            java.lang.String r7 = r9.f57     // Catch:{ Exception -> 0x00f9 }
            boolean r7 = android.text.TextUtils.isEmpty(r7)     // Catch:{ Exception -> 0x00f9 }
            if (r7 == 0) goto L_0x00ff
            java.lang.String r7 = "imei"
            java.lang.Object r7 = r6.remove(r7)     // Catch:{ Exception -> 0x00f9 }
            if (r7 == 0) goto L_0x00ff
            java.lang.String r7 = "validateGaidAndIMEI :: removing: imei"
            com.appsflyer.AFLogger.afInfoLog(r7)     // Catch:{ Exception -> 0x00f9 }
            goto L_0x00ff
        L_0x00f9:
            r7 = move-exception
            java.lang.String r8 = "failed to remove IMEI or AndroidID key from params; "
            com.appsflyer.AFLogger.afErrorLog(r8, r7)
        L_0x00ff:
            com.appsflyer.AppsFlyerLibCore$d r7 = new com.appsflyer.AppsFlyerLibCore$d
            r10.f224 = r2
            r10.f219 = r6
            com.appsflyer.internal.h r10 = r10.m157()
            r10.f218 = r3
            r7.<init>(r9, r10, r4)
            r10 = 500(0x1f4, float:7.0E-43)
            if (r1 == 0) goto L_0x012f
            boolean r0 = m32(r0)
            if (r0 == 0) goto L_0x012f
            java.util.Map<java.lang.String, java.lang.String> r0 = r9.f38
            if (r0 == 0) goto L_0x0124
            int r0 = r0.size()
            if (r0 <= 0) goto L_0x0124
            r0 = 1
            goto L_0x0125
        L_0x0124:
            r0 = 0
        L_0x0125:
            if (r0 != 0) goto L_0x012f
            java.lang.String r0 = "Failed to get new referrer, wait ..."
            com.appsflyer.AFLogger.afDebugLog(r0)
            r0 = 500(0x1f4, float:7.0E-43)
            goto L_0x0130
        L_0x012f:
            r0 = 0
        L_0x0130:
            if (r1 == 0) goto L_0x0149
            boolean r1 = r9.f53
            if (r1 == 0) goto L_0x0149
            org.json.JSONObject r9 = r9.f54
            if (r9 == 0) goto L_0x0141
            int r9 = r9.length()
            if (r9 <= 0) goto L_0x0141
            r4 = 1
        L_0x0141:
            if (r4 != 0) goto L_0x0149
            java.lang.String r9 = "fetching Facebook deferred AppLink data, wait ..."
            com.appsflyer.AFLogger.afDebugLog(r9)
            goto L_0x014a
        L_0x0149:
            r10 = r0
        L_0x014a:
            boolean r9 = com.appsflyer.internal.g.f206
            if (r9 == 0) goto L_0x0166
            java.lang.String r9 = "ESP deeplink: execute launch on SerialExecutor"
            com.appsflyer.AFLogger.afRDLog(r9)
            com.appsflyer.AFExecutor r9 = com.appsflyer.AFExecutor.getInstance()
            java.util.concurrent.ScheduledExecutorService r0 = r9.f3
            if (r0 != 0) goto L_0x0163
            java.util.concurrent.ThreadFactory r0 = r9.f1
            java.util.concurrent.ScheduledExecutorService r0 = java.util.concurrent.Executors.newSingleThreadScheduledExecutor(r0)
            r9.f3 = r0
        L_0x0163:
            java.util.concurrent.ScheduledExecutorService r9 = r9.f3
            goto L_0x016e
        L_0x0166:
            com.appsflyer.AFExecutor r9 = com.appsflyer.AFExecutor.getInstance()
            java.util.concurrent.ScheduledThreadPoolExecutor r9 = r9.m1()
        L_0x016e:
            long r0 = (long) r10
            java.util.concurrent.TimeUnit r10 = java.util.concurrent.TimeUnit.MILLISECONDS
            m46(r9, r7, r0, r10)
            return
        L_0x0175:
            java.lang.String r9 = "Not sending data yet, waiting for dev key"
            com.appsflyer.AFLogger.afDebugLog(r9)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsflyer.AppsFlyerLibCore.m56(com.appsflyer.AppsFlyerLibCore, com.appsflyer.internal.h):void");
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    static /* synthetic */ boolean m38(AppsFlyerLibCore appsFlyerLibCore) {
        Map<String, String> map = appsFlyerLibCore.f38;
        return map != null && map.size() > 0;
    }
}
