package com.appsflyer;

import com.appsflyer.internal.b;
import com.appsflyer.internal.l;
import com.google.firebase.messaging.FirebaseMessagingService;

public class FirebaseMessagingServiceListener extends FirebaseMessagingService {
    public void onNewToken(String str) {
        super.onNewToken(str);
        long currentTimeMillis = System.currentTimeMillis();
        if (str != null) {
            AFLogger.afInfoLog("Firebase Refreshed Token = ".concat(String.valueOf(str)));
            b.C0111b.a r2 = b.C0111b.a.m142(AppsFlyerProperties.getInstance().getString("afUninstallToken"));
            b.C0111b.a aVar = new b.C0111b.a(currentTimeMillis, str);
            if (r2.m145(aVar.f183, aVar.f182)) {
                l.a.m163(getApplicationContext(), aVar.f182);
            }
        }
    }
}
