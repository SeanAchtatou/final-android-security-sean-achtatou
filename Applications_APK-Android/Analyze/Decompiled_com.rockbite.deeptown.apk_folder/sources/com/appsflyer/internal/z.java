package com.appsflyer.internal;

import android.content.Context;
import com.appsflyer.AFLogger;
import com.huawei.hms.pps.AdvertisingIdClient;

public final class z {
    z() {
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public static boolean m204() {
        try {
            Class.forName("com.huawei.hms.pps.AdvertisingIdClient");
            return true;
        } catch (Throwable unused) {
            AFLogger.afDebugLog("OAID Jar not found.");
            return false;
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public static String m205(Context context) {
        try {
            return AdvertisingIdClient.getAdvertisingIdInfo(context).getId();
        } catch (Exception unused) {
            AFLogger.afDebugLog("Error collecting OAID.");
            return null;
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public static boolean m206(Context context) {
        try {
            return AdvertisingIdClient.getAdvertisingIdInfo(context).isLimitAdTrackingEnabled();
        } catch (Exception unused) {
            return false;
        }
    }
}
