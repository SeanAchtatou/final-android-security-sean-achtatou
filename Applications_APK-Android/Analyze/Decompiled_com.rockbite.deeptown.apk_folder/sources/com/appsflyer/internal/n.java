package com.appsflyer.internal;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class n implements SensorEventListener {

    /* renamed from: ʻ  reason: contains not printable characters */
    private final int f249;

    /* renamed from: ˊ  reason: contains not printable characters */
    private final String f250;

    /* renamed from: ˋ  reason: contains not printable characters */
    private final long[] f251 = new long[2];

    /* renamed from: ˎ  reason: contains not printable characters */
    private final String f252;

    /* renamed from: ˏ  reason: contains not printable characters */
    private final float[][] f253 = new float[2][];

    /* renamed from: ॱ  reason: contains not printable characters */
    private final int f254;

    /* renamed from: ॱॱ  reason: contains not printable characters */
    private double f255;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private long f256;

    private n(int i2, String str, String str2) {
        this.f254 = i2;
        this.f252 = str == null ? "" : str;
        this.f250 = str2 == null ? "" : str2;
        this.f249 = ((((i2 + 31) * 31) + this.f252.hashCode()) * 31) + this.f250.hashCode();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static double m168(float[] fArr, float[] fArr2) {
        int min = Math.min(fArr.length, fArr2.length);
        double d2 = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
        for (int i2 = 0; i2 < min; i2++) {
            d2 += StrictMath.pow((double) (fArr[i2] - fArr2[i2]), 2.0d);
        }
        return Math.sqrt(d2);
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    static n m170(Sensor sensor) {
        return new n(sensor.getType(), sensor.getName(), sensor.getVendor());
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private boolean m171(int i2, String str, String str2) {
        return this.f254 == i2 && this.f252.equals(str) && this.f250.equals(str2);
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    private static List<Float> m172(float[] fArr) {
        ArrayList arrayList = new ArrayList(fArr.length);
        for (float valueOf : fArr) {
            arrayList.add(Float.valueOf(valueOf));
        }
        return arrayList;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof n)) {
            return false;
        }
        n nVar = (n) obj;
        return m171(nVar.f254, nVar.f252, nVar.f250);
    }

    public final int hashCode() {
        return this.f249;
    }

    public final void onAccuracyChanged(Sensor sensor, int i2) {
    }

    public final void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent != null && sensorEvent.values != null) {
            Sensor sensor = sensorEvent.sensor;
            if ((sensor == null || sensor.getName() == null || sensor.getVendor() == null) ? false : true) {
                int type = sensorEvent.sensor.getType();
                String name = sensorEvent.sensor.getName();
                String vendor = sensorEvent.sensor.getVendor();
                long j2 = sensorEvent.timestamp;
                float[] fArr = sensorEvent.values;
                if (m171(type, name, vendor)) {
                    long currentTimeMillis = System.currentTimeMillis();
                    float[][] fArr2 = this.f253;
                    float[] fArr3 = fArr2[0];
                    if (fArr3 == null) {
                        fArr2[0] = Arrays.copyOf(fArr, fArr.length);
                        this.f251[0] = currentTimeMillis;
                        return;
                    }
                    float[] fArr4 = fArr2[1];
                    if (fArr4 == null) {
                        float[] copyOf = Arrays.copyOf(fArr, fArr.length);
                        this.f253[1] = copyOf;
                        this.f251[1] = currentTimeMillis;
                        this.f255 = m168(fArr3, copyOf);
                    } else if (50000000 <= j2 - this.f256) {
                        this.f256 = j2;
                        if (Arrays.equals(fArr4, fArr)) {
                            this.f251[1] = currentTimeMillis;
                            return;
                        }
                        double r0 = m168(fArr3, fArr);
                        if (r0 > this.f255) {
                            this.f253[1] = Arrays.copyOf(fArr, fArr.length);
                            this.f251[1] = currentTimeMillis;
                            this.f255 = r0;
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m173(Map<n, Map<String, Object>> map, boolean z) {
        if (this.f253[0] != null) {
            map.put(this, m169());
            if (z) {
                for (int i2 = 0; i2 < 2; i2++) {
                    this.f253[i2] = null;
                }
                for (int i3 = 0; i3 < 2; i3++) {
                    this.f251[i3] = 0;
                }
                this.f255 = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
                this.f256 = 0;
            }
        } else if (!map.containsKey(this)) {
            map.put(this, m169());
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private Map<String, Object> m169() {
        HashMap hashMap = new HashMap(7);
        hashMap.put("sT", Integer.valueOf(this.f254));
        hashMap.put("sN", this.f252);
        hashMap.put("sV", this.f250);
        float[] fArr = this.f253[0];
        if (fArr != null) {
            hashMap.put("sVS", m172(fArr));
        }
        float[] fArr2 = this.f253[1];
        if (fArr2 != null) {
            hashMap.put("sVE", m172(fArr2));
        }
        return hashMap;
    }
}
