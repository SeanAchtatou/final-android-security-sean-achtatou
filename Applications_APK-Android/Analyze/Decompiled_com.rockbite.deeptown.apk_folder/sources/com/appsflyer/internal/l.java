package com.appsflyer.internal;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import com.appsflyer.AFLogger;
import com.appsflyer.AppsFlyerLib;
import com.appsflyer.AppsFlyerLibCore;
import com.appsflyer.AppsFlyerProperties;
import com.appsflyer.FirebaseInstanceIdListener;
import com.appsflyer.internal.b;
import com.google.firebase.iid.FirebaseInstanceIdService;

public final class l {

    static final class b {

        /* renamed from: ॱ  reason: contains not printable characters */
        static final l f232 = new l();
    }

    l() {
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public static l m160() {
        return b.f232;
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    public static a m161(Context context) {
        String str;
        String str2 = "unknown";
        String str3 = null;
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager != null) {
                boolean z = false;
                if (21 <= Build.VERSION.SDK_INT) {
                    Network[] allNetworks = connectivityManager.getAllNetworks();
                    int length = allNetworks.length;
                    int i2 = 0;
                    while (true) {
                        if (i2 >= length) {
                            break;
                        }
                        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(allNetworks[i2]);
                        if (!(networkInfo != null && networkInfo.isConnectedOrConnecting())) {
                            i2++;
                        } else if (1 != networkInfo.getType()) {
                            if (networkInfo.getType() == 0) {
                            }
                        }
                    }
                } else {
                    NetworkInfo networkInfo2 = connectivityManager.getNetworkInfo(1);
                    if (!(networkInfo2 != null && networkInfo2.isConnectedOrConnecting())) {
                        NetworkInfo networkInfo3 = connectivityManager.getNetworkInfo(0);
                        if (!(networkInfo3 != null && networkInfo3.isConnectedOrConnecting())) {
                            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                            if (activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting()) {
                                z = true;
                            }
                            if (z) {
                                if (1 != activeNetworkInfo.getType()) {
                                    if (activeNetworkInfo.getType() == 0) {
                                    }
                                }
                            }
                        }
                        str2 = "MOBILE";
                    }
                }
                str2 = "WIFI";
            }
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            str = telephonyManager.getSimOperatorName();
            try {
                str3 = telephonyManager.getNetworkOperatorName();
                if ((str3 == null || str3.isEmpty()) && 2 == telephonyManager.getPhoneType()) {
                    str3 = "CDMA";
                }
            } catch (Throwable th) {
                th = th;
                AFLogger.afErrorLog("Exception while collecting network info. ", th);
                return new a(str2, str3, str);
            }
        } catch (Throwable th2) {
            th = th2;
            str = null;
            AFLogger.afErrorLog("Exception while collecting network info. ", th);
            return new a(str2, str3, str);
        }
        return new a(str2, str3, str);
    }

    public static final class a {

        /* renamed from: ˊ  reason: contains not printable characters */
        public final String f229;

        /* renamed from: ˋ  reason: contains not printable characters */
        public final String f230;

        /* renamed from: ॱ  reason: contains not printable characters */
        public final String f231;

        a(String str, String str2, String str3) {
            this.f230 = str;
            this.f229 = str2;
            this.f231 = str3;
        }

        @Deprecated
        /* renamed from: ˊ  reason: contains not printable characters */
        public static boolean m162(Context context) {
            if (AppsFlyerLib.getInstance().isTrackingStopped()) {
                return false;
            }
            try {
                Class.forName("com.google.firebase.iid.FirebaseInstanceIdService");
                Intent intent = new Intent("com.google.firebase.INSTANCE_ID_EVENT", null, context, FirebaseInstanceIdListener.class);
                Intent intent2 = new Intent("com.google.firebase.INSTANCE_ID_EVENT", null, context, FirebaseInstanceIdService.class);
                if (!(context.getPackageManager().queryIntentServices(intent, 0).size() > 0)) {
                    if (!(context.getPackageManager().queryIntentServices(intent2, 0).size() > 0)) {
                        AFLogger.afWarnLog("Cannot verify existence of our InstanceID Listener Service in the manifest. Please refer to documentation.");
                        return false;
                    }
                }
                return true;
            } catch (ClassNotFoundException unused) {
            } catch (Throwable th) {
                AFLogger.afErrorLog("An error occurred while trying to verify manifest declarations: ", th);
            }
        }

        /* renamed from: ॱ  reason: contains not printable characters */
        public static void m163(Context context, String str) {
            String str2;
            AFLogger.afInfoLog("updateServerUninstallToken called with: ".concat(String.valueOf(str)));
            b.C0111b.a r0 = b.C0111b.a.m142(AppsFlyerProperties.getInstance().getString("afUninstallToken"));
            if (!AppsFlyerLibCore.m49(context).getBoolean("sentRegisterRequestToAF", false) || (str2 = r0.f182) == null || !str2.equals(str)) {
                AppsFlyerProperties.getInstance().set("afUninstallToken", str);
                AppsFlyerLibCore.getInstance().m75(context, str);
            }
        }

        a() {
        }
    }
}
