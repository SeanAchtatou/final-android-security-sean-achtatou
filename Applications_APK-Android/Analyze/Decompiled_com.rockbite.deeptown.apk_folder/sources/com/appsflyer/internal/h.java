package com.appsflyer.internal;

import android.content.Context;
import android.content.Intent;
import com.appsflyer.AppsFlyerTrackingRequestListener;
import java.lang.ref.WeakReference;
import java.util.Map;

public final class h {

    /* renamed from: ʻ  reason: contains not printable characters */
    public String f213;

    /* renamed from: ʻॱ  reason: contains not printable characters */
    public boolean f214;

    /* renamed from: ʼ  reason: contains not printable characters */
    public String f215;

    /* renamed from: ʽ  reason: contains not printable characters */
    public String f216;

    /* renamed from: ˊ  reason: contains not printable characters */
    public Intent f217;

    /* renamed from: ˊॱ  reason: contains not printable characters */
    public int f218;

    /* renamed from: ˋ  reason: contains not printable characters */
    public Map<String, Object> f219;

    /* renamed from: ˋॱ  reason: contains not printable characters */
    public String f220;

    /* renamed from: ˎ  reason: contains not printable characters */
    public Context f221;

    /* renamed from: ˏ  reason: contains not printable characters */
    public WeakReference<Context> f222;

    /* renamed from: ˏॱ  reason: contains not printable characters */
    public boolean f223;

    /* renamed from: ͺ  reason: contains not printable characters */
    public String f224;

    /* renamed from: ॱ  reason: contains not printable characters */
    public AppsFlyerTrackingRequestListener f225;

    /* renamed from: ॱˊ  reason: contains not printable characters */
    public String f226;

    /* renamed from: ॱॱ  reason: contains not printable characters */
    public String f227;

    /* renamed from: ᐝ  reason: contains not printable characters */
    public Map<String, Object> f228;

    /* renamed from: ˏ  reason: contains not printable characters */
    public final h m157() {
        Context context = this.f221;
        Context context2 = null;
        if (context == null) {
            WeakReference<Context> weakReference = this.f222;
            context = weakReference != null ? weakReference.get() : null;
        }
        if (context != null) {
            Context context3 = this.f221;
            if (context3 != null) {
                context2 = context3;
            } else {
                WeakReference<Context> weakReference2 = this.f222;
                if (weakReference2 != null) {
                    context2 = weakReference2.get();
                }
            }
            this.f221 = context2.getApplicationContext();
        }
        return this;
    }
}
