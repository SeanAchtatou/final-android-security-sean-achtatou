package com.appsflyer.internal;

import com.appsflyer.AppsFlyerLibCore;
import com.facebook.appevents.AppEventsConstants;
import com.tapjoy.TapjoyAuctionFlags;
import java.util.HashMap;

public final class a$3 extends HashMap<String, String> {

    /* renamed from: ॱ$5b41b7af  reason: contains not printable characters */
    private /* synthetic */ Object f102$5b41b7af;

    public a$3(Object obj) {
        this.f102$5b41b7af = obj;
        put(AppEventsConstants.EVENT_PARAM_VALUE_NO, "ro.arch");
        put("1", "ro.chipname");
        put(TapjoyAuctionFlags.AUCTION_TYPE_SECOND_PRICE, "ro.dalvik.vm.native.bridge");
        put("3", "persist.sys.nativebridge");
        put(AppsFlyerLibCore.f29, "ro.enable.native.bridge.exec");
        put("5", "dalvik.vm.isa.x86.features");
        put("6", "dalvik.vm.isa.x86.variant");
        put("7", "ro.zygote");
        put("8", "ro.allow.mock.location");
        put("9", "ro.dalvik.vm.isa.arm");
        put("10", "dalvik.vm.isa.arm.features");
        put("11", "dalvik.vm.isa.arm.variant");
        put("12", "dalvik.vm.isa.arm64.features");
        put("13", "dalvik.vm.isa.arm64.variant");
        put("14", "vzw.os.rooted");
        put("15", "ro.build.user");
        put("16", "ro.kernel.qemu");
        put("17", "ro.hardware");
        put("18", "ro.product.cpu.abi");
        put("19", "ro.product.cpu.abilist");
        put("20", "ro.product.cpu.abilist32");
        put("21", "ro.product.cpu.abilist64");
    }
}
