package com.appsflyer.internal;

import android.content.Context;
import com.android.installreferrer.api.InstallReferrerClient;
import com.android.installreferrer.api.InstallReferrerStateListener;
import com.android.installreferrer.api.ReferrerDetails;
import com.appsflyer.AFLogger;
import java.util.HashMap;

public final class f implements InstallReferrerStateListener {

    /* renamed from: ˊ  reason: contains not printable characters */
    private InstallReferrerClient f199;

    /* renamed from: ॱ  reason: contains not printable characters */
    private k f200;

    public final void onInstallReferrerServiceDisconnected() {
        AFLogger.afDebugLog("Install Referrer service disconnected");
    }

    public final void onInstallReferrerSetupFinished(int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put("code", String.valueOf(i2));
        ReferrerDetails referrerDetails = null;
        if (i2 == 0) {
            try {
                AFLogger.afDebugLog("InstallReferrer connected");
                if (this.f199.isReady()) {
                    referrerDetails = this.f199.getInstallReferrer();
                    this.f199.endConnection();
                } else {
                    AFLogger.afWarnLog("ReferrerClient: InstallReferrer is not ready");
                    hashMap.put("err", "ReferrerClient: InstallReferrer is not ready");
                }
            } catch (Throwable th) {
                StringBuilder sb = new StringBuilder("Failed to get install referrer: ");
                sb.append(th.getMessage());
                AFLogger.afWarnLog(sb.toString());
                hashMap.put("err", th.getMessage());
            }
        } else if (i2 == 1) {
            AFLogger.afWarnLog("InstallReferrer not supported");
        } else if (i2 != 2) {
            AFLogger.afWarnLog("responseCode not found.");
        } else {
            AFLogger.afWarnLog("InstallReferrer not supported");
        }
        if (referrerDetails != null) {
            try {
                if (referrerDetails.getInstallReferrer() != null) {
                    hashMap.put("val", referrerDetails.getInstallReferrer());
                }
                hashMap.put("clk", Long.toString(referrerDetails.getReferrerClickTimestampSeconds()));
                hashMap.put("install", Long.toString(referrerDetails.getInstallBeginTimestampSeconds()));
            } catch (Exception e2) {
                e2.printStackTrace();
                hashMap.put("val", "-1");
                hashMap.put("clk", "-1");
                hashMap.put("install", "-1");
            }
        }
        k kVar = this.f200;
        if (kVar != null) {
            kVar.onHandleReferrer(hashMap);
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m151(Context context, k kVar) {
        this.f200 = kVar;
        try {
            this.f199 = InstallReferrerClient.newBuilder(context).build();
            this.f199.startConnection(this);
        } catch (Throwable th) {
            AFLogger.afErrorLog("referrerClient -> startConnection", th);
        }
    }
}
