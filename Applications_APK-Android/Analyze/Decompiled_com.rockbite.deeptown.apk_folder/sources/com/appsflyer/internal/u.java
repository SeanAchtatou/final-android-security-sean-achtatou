package com.appsflyer.internal;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import com.appsflyer.AFExecutor;
import com.appsflyer.AFLogger;
import java.lang.ref.WeakReference;
import java.util.concurrent.RejectedExecutionException;

public final class u implements Application.ActivityLifecycleCallbacks {

    /* renamed from: ॱ  reason: contains not printable characters */
    public static u f282;

    /* renamed from: ˊ  reason: contains not printable characters */
    boolean f283 = true;

    /* renamed from: ˋ  reason: contains not printable characters */
    public a f284 = null;

    /* renamed from: ˏ  reason: contains not printable characters */
    boolean f285 = false;

    public interface a {
        /* renamed from: ˊ  reason: contains not printable characters */
        void m187(Activity activity);

        /* renamed from: ॱ  reason: contains not printable characters */
        void m188(WeakReference<Context> weakReference);
    }

    class e extends AsyncTask<Void, Void, Void> {

        /* renamed from: ˎ  reason: contains not printable characters */
        private WeakReference<Context> f286;

        public e(WeakReference<Context> weakReference) {
            this.f286 = weakReference;
        }

        /* renamed from: ॱ  reason: contains not printable characters */
        private Void m189() {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e2) {
                AFLogger.afErrorLog("Sleeping attempt failed (essential for background state verification)\n", e2);
            }
            u uVar = u.this;
            if (uVar.f285 && uVar.f283) {
                uVar.f285 = false;
                try {
                    uVar.f284.m188(this.f286);
                } catch (Exception e3) {
                    AFLogger.afErrorLog("Listener threw exception! ", e3);
                    cancel(true);
                }
            }
            this.f286.clear();
            return null;
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ Object doInBackground(Object[] objArr) {
            return m189();
        }
    }

    public final void onActivityCreated(Activity activity, Bundle bundle) {
        g.m154();
        Intent intent = activity.getIntent();
        if (g.m153(intent) != null && intent.getData() != g.f204) {
            g.f204 = intent.getData();
        }
    }

    public final void onActivityDestroyed(Activity activity) {
    }

    public final void onActivityPaused(Activity activity) {
        this.f283 = true;
        try {
            new e(new WeakReference(activity.getApplicationContext())).executeOnExecutor(AFExecutor.getInstance().getThreadPoolExecutor(), new Void[0]);
        } catch (RejectedExecutionException e2) {
            AFLogger.afErrorLog("backgroundTask.executeOnExecutor failed with RejectedExecutionException Exception", e2);
        } catch (Throwable th) {
            AFLogger.afErrorLog("backgroundTask.executeOnExecutor failed with Exception", th);
        }
    }

    public final void onActivityResumed(Activity activity) {
        this.f283 = false;
        boolean z = !this.f285;
        this.f285 = true;
        if (z) {
            try {
                this.f284.m187(activity);
            } catch (Exception e2) {
                AFLogger.afErrorLog("Listener threw exception! ", e2);
            }
        }
    }

    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public final void onActivityStarted(Activity activity) {
    }

    public final void onActivityStopped(Activity activity) {
    }
}
