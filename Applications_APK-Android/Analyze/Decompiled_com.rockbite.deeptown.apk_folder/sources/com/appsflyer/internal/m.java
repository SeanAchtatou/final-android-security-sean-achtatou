package com.appsflyer.internal;

import android.content.Context;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Looper;
import com.tapjoy.TapjoyConstants;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class m {

    /* renamed from: ˊॱ  reason: contains not printable characters */
    private static final Handler f233 = new Handler(Looper.getMainLooper());

    /* renamed from: ˏ  reason: contains not printable characters */
    static final BitSet f234 = new BitSet(6);

    /* renamed from: ͺ  reason: contains not printable characters */
    private static volatile m f235;

    /* renamed from: ʻ  reason: contains not printable characters */
    boolean f236;

    /* renamed from: ʼ  reason: contains not printable characters */
    final Runnable f237 = new Runnable() {
        public final void run() {
            synchronized (m.this.f240) {
                m.this.m167();
                m.this.f243.postDelayed(m.this.f244, TapjoyConstants.SESSION_ID_INACTIVITY_TIME);
            }
        }
    };

    /* renamed from: ʽ  reason: contains not printable characters */
    public final Runnable f238 = new Runnable() {
        public final void run() {
            synchronized (m.this.f240) {
                if (m.this.f245) {
                    m.this.f243.removeCallbacks(m.this.f244);
                    m.this.f243.removeCallbacks(m.this.f237);
                    m.this.m167();
                    m.this.f245 = false;
                }
            }
        }
    };

    /* renamed from: ˊ  reason: contains not printable characters */
    final SensorManager f239;

    /* renamed from: ˋ  reason: contains not printable characters */
    final Object f240 = new Object();

    /* renamed from: ˎ  reason: contains not printable characters */
    final Map<n, n> f241 = new HashMap(f234.size());

    /* renamed from: ˏॱ  reason: contains not printable characters */
    private final Map<n, Map<String, Object>> f242 = new HashMap(f234.size());

    /* renamed from: ॱ  reason: contains not printable characters */
    public final Handler f243;

    /* renamed from: ॱॱ  reason: contains not printable characters */
    public final Runnable f244 = new Runnable() {
        /* JADX WARNING: Can't wrap try/catch for region: R(9:2|3|4|5|6|(5:9|(1:14)(1:13)|(4:16|(1:18)|19|29)(1:28)|27|7)|20|21|22) */
        /* JADX WARNING: Missing exception handler attribute for start block: B:20:0x0052 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void run() {
            /*
                r8 = this;
                com.appsflyer.internal.m r0 = com.appsflyer.internal.m.this
                java.lang.Object r0 = r0.f240
                monitor-enter(r0)
                com.appsflyer.internal.m r1 = com.appsflyer.internal.m.this     // Catch:{ all -> 0x0067 }
                r2 = 1
                android.hardware.SensorManager r3 = r1.f239     // Catch:{ all -> 0x0052 }
                r4 = -1
                java.util.List r3 = r3.getSensorList(r4)     // Catch:{ all -> 0x0052 }
                java.util.Iterator r3 = r3.iterator()     // Catch:{ all -> 0x0052 }
            L_0x0013:
                boolean r4 = r3.hasNext()     // Catch:{ all -> 0x0052 }
                if (r4 == 0) goto L_0x0052
                java.lang.Object r4 = r3.next()     // Catch:{ all -> 0x0052 }
                android.hardware.Sensor r4 = (android.hardware.Sensor) r4     // Catch:{ all -> 0x0052 }
                int r5 = r4.getType()     // Catch:{ all -> 0x0052 }
                r6 = 0
                if (r5 < 0) goto L_0x0030
                java.util.BitSet r7 = com.appsflyer.internal.m.f234     // Catch:{ all -> 0x0052 }
                boolean r5 = r7.get(r5)     // Catch:{ all -> 0x0052 }
                if (r5 == 0) goto L_0x0030
                r5 = 1
                goto L_0x0031
            L_0x0030:
                r5 = 0
            L_0x0031:
                if (r5 == 0) goto L_0x0013
                com.appsflyer.internal.n r5 = com.appsflyer.internal.n.m170(r4)     // Catch:{ all -> 0x0052 }
                java.util.Map<com.appsflyer.internal.n, com.appsflyer.internal.n> r7 = r1.f241     // Catch:{ all -> 0x0052 }
                boolean r7 = r7.containsKey(r5)     // Catch:{ all -> 0x0052 }
                if (r7 != 0) goto L_0x0044
                java.util.Map<com.appsflyer.internal.n, com.appsflyer.internal.n> r7 = r1.f241     // Catch:{ all -> 0x0052 }
                r7.put(r5, r5)     // Catch:{ all -> 0x0052 }
            L_0x0044:
                java.util.Map<com.appsflyer.internal.n, com.appsflyer.internal.n> r7 = r1.f241     // Catch:{ all -> 0x0052 }
                java.lang.Object r5 = r7.get(r5)     // Catch:{ all -> 0x0052 }
                android.hardware.SensorEventListener r5 = (android.hardware.SensorEventListener) r5     // Catch:{ all -> 0x0052 }
                android.hardware.SensorManager r7 = r1.f239     // Catch:{ all -> 0x0052 }
                r7.registerListener(r5, r4, r6)     // Catch:{ all -> 0x0052 }
                goto L_0x0013
            L_0x0052:
                r1.f236 = r2     // Catch:{ all -> 0x0067 }
                com.appsflyer.internal.m r1 = com.appsflyer.internal.m.this     // Catch:{ all -> 0x0067 }
                android.os.Handler r1 = r1.f243     // Catch:{ all -> 0x0067 }
                com.appsflyer.internal.m r3 = com.appsflyer.internal.m.this     // Catch:{ all -> 0x0067 }
                java.lang.Runnable r3 = r3.f237     // Catch:{ all -> 0x0067 }
                r4 = 500(0x1f4, double:2.47E-321)
                r1.postDelayed(r3, r4)     // Catch:{ all -> 0x0067 }
                com.appsflyer.internal.m r1 = com.appsflyer.internal.m.this     // Catch:{ all -> 0x0067 }
                r1.f245 = r2     // Catch:{ all -> 0x0067 }
                monitor-exit(r0)     // Catch:{ all -> 0x0067 }
                return
            L_0x0067:
                r1 = move-exception
                monitor-exit(r0)
                goto L_0x006b
            L_0x006a:
                throw r1
            L_0x006b:
                goto L_0x006a
            */
            throw new UnsupportedOperationException("Method not decompiled: com.appsflyer.internal.m.AnonymousClass2.run():void");
        }
    };

    /* renamed from: ᐝ  reason: contains not printable characters */
    boolean f245;

    static {
        f234.set(1);
        f234.set(2);
        f234.set(4);
    }

    private m(SensorManager sensorManager, Handler handler) {
        this.f239 = sensorManager;
        this.f243 = handler;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static m m164(SensorManager sensorManager, Handler handler) {
        if (f235 == null) {
            synchronized (m.class) {
                if (f235 == null) {
                    f235 = new m(sensorManager, handler);
                }
            }
        }
        return f235;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public static m m165(Context context) {
        return m164((SensorManager) context.getApplicationContext().getSystemService("sensor"), f233);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m167() {
        try {
            if (!this.f241.isEmpty()) {
                for (n next : this.f241.values()) {
                    this.f239.unregisterListener(next);
                    next.m173(this.f242, true);
                }
            }
        } catch (Throwable unused) {
        }
        this.f236 = false;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final List<Map<String, Object>> m166() {
        synchronized (this.f240) {
            if (!this.f241.isEmpty() && this.f236) {
                for (n r2 : this.f241.values()) {
                    r2.m173(this.f242, false);
                }
            }
            if (this.f242.isEmpty()) {
                List<Map<String, Object>> emptyList = Collections.emptyList();
                return emptyList;
            }
            ArrayList arrayList = new ArrayList(this.f242.values());
            return arrayList;
        }
    }
}
