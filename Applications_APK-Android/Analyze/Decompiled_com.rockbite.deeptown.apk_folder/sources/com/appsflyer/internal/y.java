package com.appsflyer.internal;

import android.net.Uri;
import android.text.TextUtils;
import com.appsflyer.AFLogger;
import com.appsflyer.AppsFlyerLibCore;
import com.appsflyer.OneLinkHttpTask;
import com.appsflyer.ServerConfigHandler;
import com.appsflyer.share.Constants;
import com.google.firebase.perf.FirebasePerformance;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import org.json.JSONException;
import org.json.JSONObject;

public final class y extends OneLinkHttpTask {

    /* renamed from: ˊ  reason: contains not printable characters */
    private static List<String> f300 = Arrays.asList("onelink.me", "onelnk.com", "app.aflink.com");

    /* renamed from: ˎ  reason: contains not printable characters */
    public a f301;

    /* renamed from: ॱ  reason: contains not printable characters */
    public String f302;

    public interface a {
        /* renamed from: ˊ  reason: contains not printable characters */
        void m202(String str);

        /* renamed from: ˏ  reason: contains not printable characters */
        void m203(Map<String, String> map);
    }

    public y(Uri uri, AppsFlyerLibCore appsFlyerLibCore) {
        super(appsFlyerLibCore);
        if (!TextUtils.isEmpty(uri.getHost()) && !TextUtils.isEmpty(uri.getPath())) {
            boolean z = false;
            for (String contains : f300) {
                if (uri.getHost().contains(contains)) {
                    z = true;
                }
            }
            String[] strArr = g.f202;
            if (strArr != null) {
                for (String str : strArr) {
                    if (uri.getHost().contains(str) && str != "") {
                        z = true;
                    }
                }
            }
            String[] split = uri.getPath().split(Constants.URL_PATH_DELIMITER);
            if (z && split.length == 3) {
                this.f100 = split[1];
                this.f302 = split[2];
            }
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m199(HttpsURLConnection httpsURLConnection) throws JSONException, IOException {
        httpsURLConnection.setRequestMethod(FirebasePerformance.HttpMethod.GET);
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final void m200(String str) {
        try {
            HashMap hashMap = new HashMap();
            JSONObject jSONObject = new JSONObject(str);
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                hashMap.put(next, jSONObject.optString(next));
            }
            this.f301.m203(hashMap);
        } catch (JSONException e2) {
            this.f301.m202("Can't parse one link data");
            AFLogger.afErrorLog("Error while parsing to json ".concat(String.valueOf(str)), e2);
        }
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    public final String m201() {
        StringBuilder sb = new StringBuilder();
        sb.append(ServerConfigHandler.getUrl("https://%sonelink.%s/shortlink-sdk/v1"));
        sb.append(Constants.URL_PATH_DELIMITER);
        sb.append(this.f100);
        sb.append("?id=");
        sb.append(this.f302);
        return sb.toString();
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m198() {
        this.f301.m202("Can't get one link data");
    }
}
