package com.appsflyer.internal;

public final class s {

    /* renamed from: ˊ  reason: contains not printable characters */
    private e f270;

    /* renamed from: ˋ  reason: contains not printable characters */
    public String f271;

    /* renamed from: ॱ  reason: contains not printable characters */
    public boolean f272;

    enum e {
        GOOGLE(0),
        AMAZON(1);
        

        /* renamed from: ˏ  reason: contains not printable characters */
        private int f276;

        private e(int i2) {
            this.f276 = i2;
        }

        public final String toString() {
            return String.valueOf(this.f276);
        }
    }

    s(e eVar, String str, boolean z) {
        this.f270 = eVar;
        this.f271 = str;
        this.f272 = z;
    }

    public final String toString() {
        return String.format("%s,%s", this.f271, Boolean.valueOf(this.f272));
    }
}
