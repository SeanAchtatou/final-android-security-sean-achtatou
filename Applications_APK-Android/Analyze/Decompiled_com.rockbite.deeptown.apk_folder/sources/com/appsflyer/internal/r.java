package com.appsflyer.internal;

import android.content.Context;
import android.content.Intent;
import android.os.Looper;
import com.applovin.sdk.AppLovinEventParameters;
import com.appsflyer.AFInAppEventType;
import com.appsflyer.AFLogger;
import com.appsflyer.AppsFlyerInAppPurchaseValidatorListener;
import com.appsflyer.AppsFlyerLib;
import com.appsflyer.AppsFlyerLibCore;
import com.appsflyer.AppsFlyerProperties;
import com.appsflyer.ServerConfigHandler;
import com.appsflyer.ServerParameters;
import com.tapjoy.TapjoyConstants;
import com.underwater.demolisher.data.vo.RemoteConfigConst;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;

public final class r implements Runnable {

    /* renamed from: ॱ  reason: contains not printable characters */
    private static String f258;

    /* renamed from: ʻ  reason: contains not printable characters */
    private String f259;

    /* renamed from: ʼ  reason: contains not printable characters */
    private String f260;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public Map<String, String> f261;

    /* renamed from: ˊ  reason: contains not printable characters */
    private final Intent f262;

    /* renamed from: ˋ  reason: contains not printable characters */
    private String f263;

    /* renamed from: ˎ  reason: contains not printable characters */
    private String f264;
    /* access modifiers changed from: private */

    /* renamed from: ˏ  reason: contains not printable characters */
    public WeakReference<Context> f265;

    /* renamed from: ॱॱ  reason: contains not printable characters */
    private String f266;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private String f267;

    static {
        StringBuilder sb = new StringBuilder("https://%svalidate.%s/api/v");
        sb.append(AppsFlyerLibCore.f29);
        sb.append("/androidevent?buildnumber=4.10.3&app_id=");
        f258 = sb.toString();
    }

    public r(Context context, String str, String str2, String str3, String str4, String str5, String str6, Map<String, String> map, Intent intent) {
        this.f265 = new WeakReference<>(context);
        this.f263 = str;
        this.f264 = str2;
        this.f266 = str4;
        this.f267 = str5;
        this.f260 = str6;
        this.f261 = map;
        this.f259 = str3;
        this.f262 = intent;
    }

    public final void run() {
        String str = this.f263;
        if (str != null && str.length() != 0 && !AppsFlyerLib.getInstance().isTrackingStopped()) {
            HttpURLConnection httpURLConnection = null;
            try {
                Context context = this.f265.get();
                if (context != null) {
                    HashMap hashMap = new HashMap();
                    hashMap.put("public-key", this.f264);
                    hashMap.put("sig-data", this.f266);
                    hashMap.put("signature", this.f259);
                    final HashMap hashMap2 = new HashMap();
                    hashMap2.putAll(hashMap);
                    Executors.newSingleThreadScheduledExecutor().schedule(new Runnable() {
                        public final void run() {
                            r rVar = r.this;
                            r.m182(rVar, hashMap2, rVar.f261, r.this.f265);
                        }
                    }, 5, TimeUnit.MILLISECONDS);
                    hashMap.put("dev_key", this.f263);
                    hashMap.put("app_id", context.getPackageName());
                    hashMap.put("uid", AppsFlyerLib.getInstance().getAppsFlyerUID(context));
                    hashMap.put(ServerParameters.ADVERTISING_ID_PARAM, AppsFlyerProperties.getInstance().getString(ServerParameters.ADVERTISING_ID_PARAM));
                    String jSONObject = new JSONObject(hashMap).toString();
                    String url = ServerConfigHandler.getUrl("https://%ssdk-services.%s/validate-android-signature");
                    if (aa.f104 == null) {
                        aa.f104 = new aa();
                    }
                    aa.f104.m108("server_request", url, jSONObject);
                    HttpURLConnection r2 = m181(jSONObject, url);
                    int i2 = -1;
                    if (r2 != null) {
                        i2 = r2.getResponseCode();
                    }
                    AppsFlyerLibCore.getInstance();
                    String r5 = AppsFlyerLibCore.m65(r2);
                    if (aa.f104 == null) {
                        aa.f104 = new aa();
                    }
                    aa.f104.m108("server_response", url, String.valueOf(i2), r5);
                    JSONObject jSONObject2 = new JSONObject(r5);
                    jSONObject2.put("code", i2);
                    if (i2 == 200) {
                        StringBuilder sb = new StringBuilder("Validate response 200 ok: ");
                        sb.append(jSONObject2.toString());
                        AFLogger.afInfoLog(sb.toString());
                        m183(jSONObject2.optBoolean("result") ? jSONObject2.getBoolean("result") : false, this.f266, this.f267, this.f260, jSONObject2.toString());
                    } else {
                        AFLogger.afInfoLog("Failed Validate request");
                        m183(false, this.f266, this.f267, this.f260, jSONObject2.toString());
                    }
                    if (r2 != null) {
                        r2.disconnect();
                    }
                }
            } catch (Throwable th) {
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
                throw th;
            }
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private static HttpURLConnection m181(String str, String str2) throws IOException {
        try {
            x xVar = new x(null, AppsFlyerLib.getInstance().isTrackingStopped());
            xVar.f297 = str;
            xVar.f295 = false;
            if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
                StringBuilder sb = new StringBuilder("Main thread detected. Calling ");
                sb.append(str2);
                sb.append(" in a new thread.");
                AFLogger.afDebugLog(sb.toString());
                xVar.execute(str2);
            } else {
                StringBuilder sb2 = new StringBuilder("Calling ");
                sb2.append(str2);
                sb2.append(" (on current thread: ");
                sb2.append(Thread.currentThread().toString());
                sb2.append(" )");
                AFLogger.afDebugLog(sb2.toString());
                xVar.onPreExecute();
                xVar.onPostExecute(xVar.doInBackground(str2));
            }
            return xVar.f293;
        } catch (Throwable th) {
            AFLogger.afErrorLog("Could not send callStats request", th);
            return null;
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private static void m183(boolean z, String str, String str2, String str3, String str4) {
        if (AppsFlyerLibCore.f26 != null) {
            StringBuilder sb = new StringBuilder("Validate callback parameters: ");
            sb.append(str);
            sb.append(RemoteConfigConst.SHOP_SPECIAL_EVENT_OFFER);
            sb.append(str2);
            sb.append(RemoteConfigConst.SHOP_SPECIAL_EVENT_OFFER);
            sb.append(str3);
            AFLogger.afDebugLog(sb.toString());
            if (z) {
                AFLogger.afDebugLog("Validate in app purchase success: ".concat(String.valueOf(str4)));
                AppsFlyerLibCore.f26.onValidateInApp();
                return;
            }
            AFLogger.afDebugLog("Validate in app purchase failed: ".concat(String.valueOf(str4)));
            AppsFlyerInAppPurchaseValidatorListener appsFlyerInAppPurchaseValidatorListener = AppsFlyerLibCore.f26;
            if (str4 == null) {
                str4 = "Failed validating";
            }
            appsFlyerInAppPurchaseValidatorListener.onValidateInAppFailure(str4);
        }
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [java.net.HttpURLConnection, java.lang.String] */
    /* renamed from: ˎ  reason: contains not printable characters */
    static /* synthetic */ void m182(r rVar, Map map, Map map2, WeakReference weakReference) {
        if (weakReference.get() != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(ServerConfigHandler.getUrl(f258));
            sb.append(((Context) weakReference.get()).getPackageName());
            String obj = sb.toString();
            ? r2 = 0;
            String string = AppsFlyerLibCore.m49((Context) weakReference.get()).getString(TapjoyConstants.TJC_REFERRER, r2);
            if (string == null) {
                string = "";
            }
            AppsFlyerLibCore instance = AppsFlyerLibCore.getInstance();
            h hVar = new h();
            hVar.f221 = (Context) weakReference.get();
            hVar.f227 = rVar.f263;
            hVar.f216 = AFInAppEventType.PURCHASE;
            hVar.f226 = string;
            hVar.f217 = rVar.f262;
            Map<String, Object> r9 = instance.m74(hVar);
            r9.put("price", rVar.f267);
            r9.put("currency", rVar.f260);
            JSONObject jSONObject = new JSONObject(r9);
            JSONObject jSONObject2 = new JSONObject();
            try {
                for (Map.Entry entry : map.entrySet()) {
                    jSONObject2.put((String) entry.getKey(), entry.getValue());
                }
                jSONObject.put(AppLovinEventParameters.IN_APP_PURCHASE_DATA, jSONObject2);
            } catch (JSONException e2) {
                AFLogger.afErrorLog("Failed to build 'receipt_data'", e2);
            }
            if (map2 != null) {
                JSONObject jSONObject3 = new JSONObject();
                try {
                    for (Map.Entry entry2 : map2.entrySet()) {
                        jSONObject3.put((String) entry2.getKey(), entry2.getValue());
                    }
                    jSONObject.put("extra_prms", jSONObject3);
                } catch (JSONException e3) {
                    AFLogger.afErrorLog("Failed to build 'extra_prms'", e3);
                }
            }
            String jSONObject4 = jSONObject.toString();
            if (aa.f104 == null) {
                aa.f104 = new aa();
            }
            aa.f104.m108("server_request", obj, jSONObject4);
            try {
                HttpURLConnection r22 = m181(jSONObject4, obj);
                int i2 = -1;
                if (r22 != null) {
                    i2 = r22.getResponseCode();
                }
                AppsFlyerLibCore.getInstance();
                String r7 = AppsFlyerLibCore.m65(r22);
                if (aa.f104 == null) {
                    aa.f104 = new aa();
                }
                aa.f104.m108("server_response", obj, String.valueOf(i2), r7);
                JSONObject jSONObject5 = new JSONObject(r7);
                if (i2 == 200) {
                    StringBuilder sb2 = new StringBuilder("Validate-WH response - 200: ");
                    sb2.append(jSONObject5.toString());
                    AFLogger.afInfoLog(sb2.toString());
                } else {
                    StringBuilder sb3 = new StringBuilder("Validate-WH response failed - ");
                    sb3.append(i2);
                    sb3.append(": ");
                    sb3.append(jSONObject5.toString());
                    AFLogger.afWarnLog(sb3.toString());
                }
                if (r22 != null) {
                    r22.disconnect();
                }
            } catch (Throwable th) {
                if (r2 != 0) {
                    r2.disconnect();
                }
                throw th;
            }
        }
    }
}
