package com.appsflyer.internal;

import com.appsflyer.AFLogger;

public final class ac implements a {

    /* renamed from: ˊ  reason: contains not printable characters */
    private a f138 = this;

    interface a {
        /* renamed from: ˊ  reason: contains not printable characters */
        Class<?> m116(String str) throws ClassNotFoundException;
    }

    enum b {
        UNITY("android_unity", "com.unity3d.player.UnityPlayer"),
        REACT_NATIVE("android_reactNative", "com.facebook.react.ReactApplication"),
        CORDOVA("android_cordova", "org.apache.cordova.CordovaActivity"),
        SEGMENT("android_segment", "com.segment.analytics.integrations.Integration"),
        COCOS2DX("android_cocos2dx", "org.cocos2dx.lib.Cocos2dxActivity"),
        DEFAULT("android_native", "android_native"),
        ADOBE_EX("android_adobe_ex", "com.appsflyer.adobeextension"),
        FLUTTER("android_flutter", "io.flutter.plugin.common.MethodChannel.MethodCallHandler");
        

        /* renamed from: ʽ  reason: contains not printable characters */
        private String f148;

        /* renamed from: ᐝ  reason: contains not printable characters */
        private String f149;

        private b(String str, String str2) {
            this.f148 = str;
            this.f149 = str2;
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean m113(String str) {
        try {
            this.f138.m116(str);
            StringBuilder sb = new StringBuilder("Class: ");
            sb.append(str);
            sb.append(" is found.");
            AFLogger.afRDLog(sb.toString());
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        } catch (Throwable th) {
            AFLogger.afErrorLog(th.getMessage(), th);
            return false;
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final Class<?> m114(String str) throws ClassNotFoundException {
        return Class.forName(str);
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public final String m115() {
        for (b bVar : b.values()) {
            if (m113(b.m118(bVar))) {
                return b.m117(bVar);
            }
        }
        return b.m117(b.DEFAULT);
    }
}
