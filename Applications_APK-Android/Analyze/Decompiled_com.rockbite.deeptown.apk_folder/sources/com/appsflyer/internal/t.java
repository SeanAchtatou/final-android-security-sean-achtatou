package com.appsflyer.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Parcel;
import android.os.RemoteException;
import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;

public final class t {

    static final class b implements IInterface {

        /* renamed from: ˏ  reason: contains not printable characters */
        private IBinder f277;

        b(IBinder iBinder) {
            this.f277 = iBinder;
        }

        public final IBinder asBinder() {
            return this.f277;
        }

        /* renamed from: ˋ  reason: contains not printable characters */
        public final String m185() throws RemoteException {
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            try {
                obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                this.f277.transact(1, obtain, obtain2, 0);
                obtain2.readException();
                return obtain2.readString();
            } finally {
                obtain2.recycle();
                obtain.recycle();
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ॱ  reason: contains not printable characters */
        public final boolean m186() throws RemoteException {
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            try {
                obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                boolean z = true;
                obtain.writeInt(1);
                this.f277.transact(2, obtain, obtain2, 0);
                obtain2.readException();
                if (obtain2.readInt() == 0) {
                    z = false;
                }
                return z;
            } finally {
                obtain2.recycle();
                obtain.recycle();
            }
        }
    }

    static final class c {

        /* renamed from: ˊ  reason: contains not printable characters */
        final String f278;

        /* renamed from: ˋ  reason: contains not printable characters */
        final boolean f279;

        c(String str, boolean z) {
            this.f278 = str;
            this.f279 = z;
        }
    }

    t() {
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    static c m184(Context context) throws Exception {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            try {
                context.getPackageManager().getPackageInfo("com.android.vending", 0);
                d dVar = new d((byte) 0);
                Intent intent = new Intent("com.google.android.gms.ads.identifier.service.START");
                intent.setPackage("com.google.android.gms");
                try {
                    if (!context.bindService(intent, dVar, 1)) {
                        if (context != null) {
                            context.unbindService(dVar);
                        }
                        throw new IOException("Google Play connection failed");
                    } else if (!dVar.f280) {
                        dVar.f280 = true;
                        b bVar = new b(dVar.f281.take());
                        c cVar = new c(bVar.m185(), bVar.m186());
                        if (context != null) {
                            context.unbindService(dVar);
                        }
                        return cVar;
                    } else {
                        throw new IllegalStateException();
                    }
                } catch (Exception e2) {
                    throw e2;
                } catch (Throwable th) {
                    if (context != null) {
                        context.unbindService(dVar);
                    }
                    throw th;
                }
            } catch (Exception e3) {
                throw e3;
            }
        } else {
            throw new IllegalStateException("Cannot be called from the main thread");
        }
    }

    static final class d implements ServiceConnection {

        /* renamed from: ˊ  reason: contains not printable characters */
        boolean f280;

        /* renamed from: ˏ  reason: contains not printable characters */
        final LinkedBlockingQueue<IBinder> f281;

        private d() {
            this.f281 = new LinkedBlockingQueue<>(1);
            this.f280 = false;
        }

        public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            try {
                this.f281.put(iBinder);
            } catch (InterruptedException unused) {
            }
        }

        public final void onServiceDisconnected(ComponentName componentName) {
        }

        /* synthetic */ d(byte b2) {
            this();
        }
    }
}
