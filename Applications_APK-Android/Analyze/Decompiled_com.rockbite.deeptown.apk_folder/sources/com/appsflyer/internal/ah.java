package com.appsflyer.internal;

import com.google.android.gms.common.api.Api;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class ah extends FilterInputStream {

    /* renamed from: ˊ  reason: contains not printable characters */
    private static final int[] f151 = ai.f171;

    /* renamed from: ˋ  reason: contains not printable characters */
    private static final int[] f152 = ai.f169;

    /* renamed from: ˎ  reason: contains not printable characters */
    private static final byte[] f153 = ai.f170;

    /* renamed from: ˏ  reason: contains not printable characters */
    private static final int[] f154 = ai.f168;

    /* renamed from: ॱ  reason: contains not printable characters */
    private static final int[] f155 = ai.f167;

    /* renamed from: ʻ  reason: contains not printable characters */
    private final int f156;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final byte[][] f157;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final byte[] f158 = new byte[16];

    /* renamed from: ˊॱ  reason: contains not printable characters */
    private int f159 = 16;

    /* renamed from: ˋॱ  reason: contains not printable characters */
    private final byte[] f160 = new byte[16];

    /* renamed from: ˏॱ  reason: contains not printable characters */
    private int f161 = 16;

    /* renamed from: ͺ  reason: contains not printable characters */
    private int f162 = Api.BaseClientBuilder.API_PRIORITY_OTHER;

    /* renamed from: ॱॱ  reason: contains not printable characters */
    private final int[] f163 = new int[4];

    /* renamed from: ᐝ  reason: contains not printable characters */
    private final int[] f164;

    public ah(InputStream inputStream, int i2, byte[] bArr, byte[][] bArr2) {
        super(inputStream);
        this.f156 = i2;
        this.f164 = ai.m128(bArr, i2);
        this.f157 = m125(bArr2);
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private static byte[][] m125(byte[][] bArr) {
        byte[][] bArr2 = new byte[bArr.length][];
        for (int i2 = 0; i2 < bArr.length; i2++) {
            bArr2[i2] = new byte[bArr[i2].length];
            for (int i3 = 0; i3 < bArr[i2].length; i3++) {
                bArr2[i2][bArr[i2][i3]] = (byte) i3;
            }
        }
        return bArr2;
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    private void m126(byte[] bArr, byte[] bArr2) {
        int[] iArr = this.f163;
        char c2 = 1;
        byte b2 = (bArr[0] << 24) | ((bArr[1] & 255) << 16) | ((bArr[2] & 255) << 8) | (bArr[3] & 255);
        int[] iArr2 = this.f164;
        iArr[0] = b2 ^ iArr2[0];
        iArr[1] = ((((bArr[4] << 24) | ((bArr[5] & 255) << 16)) | ((bArr[6] & 255) << 8)) | (bArr[7] & 255)) ^ iArr2[1];
        iArr[2] = ((((bArr[8] << 24) | ((bArr[9] & 255) << 16)) | ((bArr[10] & 255) << 8)) | (bArr[11] & 255)) ^ iArr2[2];
        iArr[3] = iArr2[3] ^ ((((bArr[12] << 24) | ((bArr[13] & 255) << 16)) | ((bArr[14] & 255) << 8)) | (bArr[15] & 255));
        int i2 = 1;
        int i3 = 4;
        while (i2 < this.f156) {
            int[] iArr3 = f154;
            int[] iArr4 = this.f163;
            byte[][] bArr3 = this.f157;
            int i4 = iArr3[iArr4[bArr3[0][0]] >>> 24];
            int[] iArr5 = f152;
            int i5 = i4 ^ iArr5[(iArr4[bArr3[c2][0]] >>> 16) & 255];
            int[] iArr6 = f155;
            int i6 = iArr6[(iArr4[bArr3[2][0]] >>> 8) & 255] ^ i5;
            int[] iArr7 = f151;
            int i7 = iArr7[iArr4[bArr3[3][0]] & 255] ^ i6;
            int[] iArr8 = this.f164;
            int i8 = i7 ^ iArr8[i3];
            int i9 = ((iArr6[(iArr4[bArr3[2][c2]] >>> 8) & 255] ^ (iArr3[iArr4[bArr3[0][c2]] >>> 24] ^ iArr5[(iArr4[bArr3[c2][c2]] >>> 16) & 255])) ^ iArr7[iArr4[bArr3[3][c2]] & 255]) ^ iArr8[i3 + 1];
            int i10 = (((iArr5[(iArr4[bArr3[c2][2]] >>> 16) & 255] ^ iArr3[iArr4[bArr3[0][2]] >>> 24]) ^ iArr6[(iArr4[bArr3[2][2]] >>> 8) & 255]) ^ iArr7[iArr4[bArr3[3][2]] & 255]) ^ iArr8[i3 + 2];
            iArr4[0] = i8;
            iArr4[1] = i9;
            iArr4[2] = i10;
            iArr4[3] = (((iArr3[iArr4[bArr3[0][3]] >>> 24] ^ iArr5[(iArr4[bArr3[1][3]] >>> 16) & 255]) ^ iArr6[(iArr4[bArr3[2][3]] >>> 8) & 255]) ^ iArr7[iArr4[bArr3[3][3]] & 255]) ^ iArr8[i3 + 3];
            i2++;
            i3 += 4;
            c2 = 1;
        }
        int[] iArr9 = this.f164;
        int i11 = iArr9[i3];
        byte[] bArr4 = f153;
        int[] iArr10 = this.f163;
        byte[][] bArr5 = this.f157;
        bArr2[0] = (byte) (bArr4[iArr10[bArr5[0][0]] >>> 24] ^ (i11 >>> 24));
        bArr2[1] = (byte) (bArr4[(iArr10[bArr5[1][0]] >>> 16) & 255] ^ (i11 >>> 16));
        bArr2[2] = (byte) (bArr4[(iArr10[bArr5[2][0]] >>> 8) & 255] ^ (i11 >>> 8));
        bArr2[3] = (byte) (i11 ^ bArr4[iArr10[bArr5[3][0]] & 255]);
        int i12 = iArr9[i3 + 1];
        bArr2[4] = (byte) (bArr4[iArr10[bArr5[0][1]] >>> 24] ^ (i12 >>> 24));
        bArr2[5] = (byte) (bArr4[(iArr10[bArr5[1][1]] >>> 16) & 255] ^ (i12 >>> 16));
        bArr2[6] = (byte) (bArr4[(iArr10[bArr5[2][1]] >>> 8) & 255] ^ (i12 >>> 8));
        bArr2[7] = (byte) (i12 ^ bArr4[iArr10[bArr5[3][1]] & 255]);
        int i13 = iArr9[i3 + 2];
        bArr2[8] = (byte) (bArr4[iArr10[bArr5[0][2]] >>> 24] ^ (i13 >>> 24));
        bArr2[9] = (byte) (bArr4[(iArr10[bArr5[1][2]] >>> 16) & 255] ^ (i13 >>> 16));
        bArr2[10] = (byte) (bArr4[(iArr10[bArr5[2][2]] >>> 8) & 255] ^ (i13 >>> 8));
        bArr2[11] = (byte) (i13 ^ bArr4[iArr10[bArr5[3][2]] & 255]);
        int i14 = iArr9[i3 + 3];
        bArr2[12] = (byte) (bArr4[iArr10[bArr5[0][3]] >>> 24] ^ (i14 >>> 24));
        bArr2[13] = (byte) (bArr4[(iArr10[bArr5[1][3]] >>> 16) & 255] ^ (i14 >>> 16));
        bArr2[14] = (byte) (bArr4[(iArr10[bArr5[2][3]] >>> 8) & 255] ^ (i14 >>> 8));
        bArr2[15] = (byte) (i14 ^ bArr4[iArr10[bArr5[3][3]] & 255]);
    }

    public final int available() throws IOException {
        return this.f159 - this.f161;
    }

    public final void close() throws IOException {
        super.close();
    }

    public final synchronized void mark(int i2) {
    }

    public final boolean markSupported() {
        return false;
    }

    public final int read() throws IOException {
        m124();
        int i2 = this.f161;
        if (i2 >= this.f159) {
            return -1;
        }
        byte[] bArr = this.f160;
        this.f161 = i2 + 1;
        return bArr[i2] & 255;
    }

    public final synchronized void reset() throws IOException {
    }

    public final long skip(long j2) throws IOException {
        long j3 = 0;
        while (j3 < j2 && read() != -1) {
            j3++;
        }
        return j3;
    }

    public final int read(byte[] bArr) throws IOException {
        return read(bArr, 0, bArr.length);
    }

    public final int read(byte[] bArr, int i2, int i3) throws IOException {
        int i4 = i2 + i3;
        int i5 = i2;
        while (i5 < i4) {
            m124();
            int i6 = this.f161;
            if (i6 < this.f159) {
                byte[] bArr2 = this.f160;
                this.f161 = i6 + 1;
                bArr[i5] = bArr2[i6];
                i5++;
            } else if (i5 == i2) {
                return -1;
            } else {
                return i3 - (i4 - i5);
            }
        }
        return i3;
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private int m124() throws IOException {
        if (this.f162 == Integer.MAX_VALUE) {
            this.f162 = this.in.read();
        }
        int i2 = 16;
        if (this.f161 == 16) {
            this.f158[0] = (byte) this.f162;
            int i3 = 1;
            do {
                i3 += this.in.read(this.f158, i3, 16 - i3);
            } while (i3 < 16);
            m126(this.f158, this.f160);
            this.f162 = this.in.read();
            this.f161 = 0;
            if (this.f162 < 0) {
                i2 = 16 - (this.f160[15] & 255);
            }
            this.f159 = i2;
        }
        return this.f159;
    }
}
