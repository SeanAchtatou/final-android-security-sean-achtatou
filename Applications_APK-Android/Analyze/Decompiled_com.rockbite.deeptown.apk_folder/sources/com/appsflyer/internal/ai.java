package com.appsflyer.internal;

public final class ai {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static byte[] f165 = new byte[256];

    /* renamed from: ʽ  reason: contains not printable characters */
    private static int[] f166 = new int[10];

    /* renamed from: ˊ  reason: contains not printable characters */
    static final int[] f167 = new int[256];

    /* renamed from: ˋ  reason: contains not printable characters */
    static final int[] f168 = new int[256];

    /* renamed from: ˎ  reason: contains not printable characters */
    static final int[] f169 = new int[256];

    /* renamed from: ˏ  reason: contains not printable characters */
    static final byte[] f170 = new byte[256];

    /* renamed from: ॱ  reason: contains not printable characters */
    static final int[] f171 = new int[256];

    static {
        byte[] bArr;
        byte b2;
        byte b3 = 1;
        byte b4 = 1;
        do {
            b3 = (byte) (((b3 & 128) != 0 ? (byte) 27 : 0) ^ ((b3 << 1) ^ b3));
            byte b5 = (byte) (b4 ^ (b4 << 1));
            byte b6 = (byte) (b5 ^ (b5 << 2));
            byte b7 = (byte) (b6 ^ (b6 << 4));
            b4 = (byte) (b7 ^ ((b7 & 128) != 0 ? (byte) 9 : 0));
            bArr = f165;
            b2 = b3 & 255;
            byte b8 = b4 & 255;
            bArr[b2] = (byte) (((((b4 ^ 99) ^ ((b8 << 1) | (b8 >> 7))) ^ ((b8 << 2) | (b8 >> 6))) ^ ((b8 << 3) | (b8 >> 5))) ^ ((b8 >> 4) | (b8 << 4)));
        } while (b2 != 1);
        bArr[0] = 99;
        for (int i2 = 0; i2 < 256; i2++) {
            byte b9 = f165[i2] & 255;
            f170[b9] = (byte) i2;
            int i3 = i2 << 1;
            if (i3 >= 256) {
                i3 ^= 283;
            }
            int i4 = i3 << 1;
            if (i4 >= 256) {
                i4 ^= 283;
            }
            int i5 = i4 << 1;
            if (i5 >= 256) {
                i5 ^= 283;
            }
            int i6 = i5 ^ i2;
            int i7 = ((i3 ^ (i4 ^ i5)) << 24) | (i6 << 16) | ((i6 ^ i4) << 8) | (i6 ^ i3);
            f168[b9] = i7;
            f169[b9] = (i7 >>> 8) | (i7 << 24);
            f167[b9] = (i7 >>> 16) | (i7 << 16);
            f171[b9] = (i7 << 8) | (i7 >>> 24);
        }
        f166[0] = 16777216;
        int i8 = 1;
        for (int i9 = 1; i9 < 10; i9++) {
            i8 <<= 1;
            if (i8 >= 256) {
                i8 ^= 283;
            }
            f166[i9] = i8 << 24;
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public static byte[][] m127(int i2) {
        byte[][] bArr = new byte[4][];
        for (int i3 = 0; i3 < 4; i3++) {
            int i4 = i2 >>> (i3 << 3);
            bArr[i3] = new byte[]{(byte) (i4 & 3), (byte) ((i4 >> 2) & 3), (byte) ((i4 >> 4) & 3), (byte) ((i4 >> 6) & 3)};
        }
        return bArr;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    static int[] m128(byte[] bArr, int i2) throws IllegalArgumentException {
        if (bArr.length == 16) {
            int i3 = (i2 + 1) * 4;
            int[] iArr = new int[i3];
            int i4 = 0;
            int i5 = 0;
            while (i4 < 4) {
                int i6 = i5 + 1;
                int i7 = i6 + 1;
                byte b2 = (bArr[i5] << 24) | ((bArr[i6] & 255) << 16);
                int i8 = i7 + 1;
                iArr[i4] = b2 | ((bArr[i7] & 255) << 8) | (bArr[i8] & 255);
                i4++;
                i5 = i8 + 1;
            }
            int i9 = 4;
            int i10 = 0;
            int i11 = 0;
            while (i9 < i3) {
                int i12 = iArr[i9 - 1];
                if (i10 == 0) {
                    byte[] bArr2 = f165;
                    i12 = ((bArr2[i12 >>> 24] & 255) | (((bArr2[(i12 >>> 16) & 255] << 24) | ((bArr2[(i12 >>> 8) & 255] & 255) << 16)) | ((bArr2[i12 & 255] & 255) << 8))) ^ f166[i11];
                    i11++;
                    i10 = 4;
                }
                iArr[i9] = i12 ^ iArr[i9 - 4];
                i9++;
                i10--;
            }
            return m129(bArr, iArr, i2);
        }
        throw new IllegalArgumentException();
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private static int[] m129(byte[] bArr, int[] iArr, int i2) throws IllegalArgumentException {
        if (bArr.length == 16) {
            int i3 = 4;
            int[] iArr2 = new int[((i2 + 1) * 4)];
            int i4 = i2 * 4;
            int i5 = i4 + 1;
            iArr2[0] = iArr[i4];
            int i6 = i5 + 1;
            int i7 = iArr[i5];
            int i8 = 1;
            iArr2[1] = i7;
            int i9 = i6 + 1;
            iArr2[2] = iArr[i6];
            iArr2[3] = iArr[i9];
            int i10 = i9 - 7;
            while (i8 < i2) {
                int i11 = i10 + 1;
                int i12 = iArr[i10];
                int i13 = i3 + 1;
                int[] iArr3 = f168;
                byte[] bArr2 = f165;
                int i14 = iArr3[bArr2[i12 >>> 24] & 255];
                int[] iArr4 = f169;
                int i15 = i14 ^ iArr4[bArr2[(i12 >>> 16) & 255] & 255];
                int[] iArr5 = f167;
                int i16 = i15 ^ iArr5[bArr2[(i12 >>> 8) & 255] & 255];
                int[] iArr6 = f171;
                iArr2[i3] = iArr6[bArr2[i12 & 255] & 255] ^ i16;
                int i17 = i11 + 1;
                int i18 = iArr[i11];
                int i19 = i13 + 1;
                iArr2[i13] = iArr6[bArr2[i18 & 255] & 255] ^ ((iArr3[bArr2[i18 >>> 24] & 255] ^ iArr4[bArr2[(i18 >>> 16) & 255] & 255]) ^ iArr5[bArr2[(i18 >>> 8) & 255] & 255]);
                int i20 = i17 + 1;
                int i21 = iArr[i17];
                int i22 = i19 + 1;
                iArr2[i19] = iArr6[bArr2[i21 & 255] & 255] ^ ((iArr3[bArr2[i21 >>> 24] & 255] ^ iArr4[bArr2[(i21 >>> 16) & 255] & 255]) ^ iArr5[bArr2[(i21 >>> 8) & 255] & 255]);
                int i23 = iArr[i20];
                int i24 = i22 + 1;
                iArr2[i22] = iArr6[bArr2[i23 & 255] & 255] ^ ((iArr3[bArr2[i23 >>> 24] & 255] ^ iArr4[bArr2[(i23 >>> 16) & 255] & 255]) ^ iArr5[bArr2[(i23 >>> 8) & 255] & 255]);
                i10 = i20 - 7;
                i8++;
                i3 = i24;
            }
            int i25 = i3 + 1;
            int i26 = i10 + 1;
            iArr2[i3] = iArr[i10];
            int i27 = i25 + 1;
            int i28 = i26 + 1;
            iArr2[i25] = iArr[i26];
            iArr2[i27] = iArr[i28];
            iArr2[i27 + 1] = iArr[i28 + 1];
            return iArr2;
        }
        throw new IllegalArgumentException();
    }
}
