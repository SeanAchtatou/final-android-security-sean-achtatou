package com.appsflyer.internal;

import com.appsflyer.AppsFlyerProperties;
import com.appsflyer.ServerParameters;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TapjoyConstants;
import java.text.SimpleDateFormat;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONObject;

public final class aa {

    /* renamed from: ˋ  reason: contains not printable characters */
    public static aa f104;

    /* renamed from: ʻ  reason: contains not printable characters */
    private final String f105;

    /* renamed from: ʻॱ  reason: contains not printable characters */
    private final String f106;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final String f107;

    /* renamed from: ʼॱ  reason: contains not printable characters */
    private final String f108;

    /* renamed from: ʽ  reason: contains not printable characters */
    private final String f109;

    /* renamed from: ʽॱ  reason: contains not printable characters */
    private final String f110;

    /* renamed from: ʾ  reason: contains not printable characters */
    private final String f111;

    /* renamed from: ʿ  reason: contains not printable characters */
    private final String f112;

    /* renamed from: ˈ  reason: contains not printable characters */
    private final String f113;

    /* renamed from: ˉ  reason: contains not printable characters */
    private final String f114;

    /* renamed from: ˊ  reason: contains not printable characters */
    public boolean f115;

    /* renamed from: ˊˊ  reason: contains not printable characters */
    private boolean f116;

    /* renamed from: ˊˋ  reason: contains not printable characters */
    private JSONObject f117;

    /* renamed from: ˊॱ  reason: contains not printable characters */
    private final String f118;

    /* renamed from: ˊᐝ  reason: contains not printable characters */
    private JSONArray f119;

    /* renamed from: ˋˊ  reason: contains not printable characters */
    private final String f120;

    /* renamed from: ˋˋ  reason: contains not printable characters */
    private String f121;

    /* renamed from: ˋॱ  reason: contains not printable characters */
    private final String f122;

    /* renamed from: ˋᐝ  reason: contains not printable characters */
    private int f123;

    /* renamed from: ˎ  reason: contains not printable characters */
    private final String f124;

    /* renamed from: ˎˎ  reason: contains not printable characters */
    private boolean f125;

    /* renamed from: ˏ  reason: contains not printable characters */
    public boolean f126;

    /* renamed from: ˏॱ  reason: contains not printable characters */
    private final String f127;

    /* renamed from: ͺ  reason: contains not printable characters */
    private final String f128;

    /* renamed from: ॱ  reason: contains not printable characters */
    private final String f129;

    /* renamed from: ॱˊ  reason: contains not printable characters */
    private final String f130;

    /* renamed from: ॱˋ  reason: contains not printable characters */
    private final String f131;

    /* renamed from: ॱˎ  reason: contains not printable characters */
    private final String f132;

    /* renamed from: ॱॱ  reason: contains not printable characters */
    private final String f133;

    /* renamed from: ॱᐝ  reason: contains not printable characters */
    private final String f134;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private final String f135;

    /* renamed from: ᐝॱ  reason: contains not printable characters */
    private final String f136;

    public aa() {
        this.f129 = "brand";
        this.f124 = "model";
        this.f135 = TapjoyConstants.TJC_PLATFORM;
        this.f109 = "platform_version";
        this.f107 = ServerParameters.ADVERTISING_ID_PARAM;
        this.f133 = "imei";
        this.f105 = TapjoyConstants.TJC_ANDROID_ID;
        this.f118 = "sdk_version";
        this.f122 = "devkey";
        this.f128 = "originalAppsFlyerId";
        this.f130 = "uid";
        this.f127 = "app_id";
        this.f106 = TapjoyConstants.TJC_APP_VERSION_NAME;
        this.f136 = AppsFlyerProperties.CHANNEL;
        this.f134 = "preInstall";
        this.f132 = TJAdUnitConstants.String.DATA;
        this.f131 = "r_debugging_off";
        this.f111 = "r_debugging_on";
        this.f108 = "public_api_call";
        this.f110 = "exception";
        this.f112 = "server_request";
        this.f113 = "server_response";
        this.f114 = "yyyy-MM-dd HH:mm:ssZ";
        this.f120 = "MM-dd HH:mm:ss.SSS";
        this.f116 = true;
        this.f123 = 0;
        this.f121 = "-1";
        this.f125 = AppsFlyerProperties.getInstance().getBoolean(AppsFlyerProperties.DPM, false);
        this.f115 = true ^ this.f125;
        this.f119 = new JSONArray();
        this.f123 = 0;
        this.f126 = false;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private synchronized void m99(String str, String str2, String str3, String str4) {
        if (str != null) {
            try {
                if (str.length() > 0) {
                    this.f117.put("app_id", str);
                }
            } catch (Throwable unused) {
                return;
            }
        }
        if (str2 != null && str2.length() > 0) {
            this.f117.put(TapjoyConstants.TJC_APP_VERSION_NAME, str2);
        }
        if (str3 != null && str3.length() > 0) {
            this.f117.put(AppsFlyerProperties.CHANNEL, str3);
        }
        if (str4 != null && str4.length() > 0) {
            this.f117.put("preInstall", str4);
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private synchronized void m100(String str, String str2, String str3, String str4) {
        try {
            this.f117.put("sdk_version", str);
            if (str2 != null && str2.length() > 0) {
                this.f117.put("devkey", str2);
            }
            if (str3 != null && str3.length() > 0) {
                this.f117.put("originalAppsFlyerId", str3);
            }
            if (str4 != null && str4.length() > 0) {
                this.f117.put("uid", str4);
            }
        } catch (Throwable unused) {
        }
    }

    /* renamed from: ॱॱ  reason: contains not printable characters */
    private synchronized void m102() {
        this.f119 = null;
        this.f119 = new JSONArray();
        this.f123 = 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final synchronized void m103() {
        this.f117 = null;
        this.f119 = null;
        f104 = null;
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public final synchronized void m107() {
        m108("r_debugging_off", new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ", Locale.ENGLISH).format(Long.valueOf(System.currentTimeMillis())), new String[0]);
        this.f126 = false;
        this.f116 = false;
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    public final synchronized void m110(String str) {
        this.f121 = str;
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    public final synchronized void m109() {
        this.f126 = true;
        m108("r_debugging_on", new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ", Locale.ENGLISH).format(Long.valueOf(System.currentTimeMillis())), new String[0]);
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private synchronized void m101(String str, String str2, String str3, String str4, String str5, String str6) {
        try {
            this.f117.put("brand", str);
            this.f117.put("model", str2);
            this.f117.put(TapjoyConstants.TJC_PLATFORM, "Android");
            this.f117.put("platform_version", str3);
            if (str4 != null && str4.length() > 0) {
                this.f117.put(ServerParameters.ADVERTISING_ID_PARAM, str4);
            }
            if (str5 != null && str5.length() > 0) {
                this.f117.put("imei", str5);
            }
            if (str6 != null && str6.length() > 0) {
                this.f117.put(TapjoyConstants.TJC_ANDROID_ID, str6);
            }
        } catch (Throwable unused) {
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0070 */
    /* renamed from: ˎ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void m106(java.lang.String r11, android.content.pm.PackageManager r12) {
        /*
            r10 = this;
            monitor-enter(r10)
            com.appsflyer.AppsFlyerProperties r0 = com.appsflyer.AppsFlyerProperties.getInstance()     // Catch:{ all -> 0x0081 }
            com.appsflyer.AppsFlyerLibCore r1 = com.appsflyer.AppsFlyerLibCore.getInstance()     // Catch:{ all -> 0x0081 }
            java.lang.String r2 = "remote_debug_static_data"
            java.lang.String r2 = r0.getString(r2)     // Catch:{ all -> 0x0081 }
            if (r2 == 0) goto L_0x0019
            org.json.JSONObject r11 = new org.json.JSONObject     // Catch:{ all -> 0x0070 }
            r11.<init>(r2)     // Catch:{ all -> 0x0070 }
            r10.f117 = r11     // Catch:{ all -> 0x0070 }
            goto L_0x0070
        L_0x0019:
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ all -> 0x0081 }
            r2.<init>()     // Catch:{ all -> 0x0081 }
            r10.f117 = r2     // Catch:{ all -> 0x0081 }
            java.lang.String r4 = android.os.Build.BRAND     // Catch:{ all -> 0x0081 }
            java.lang.String r5 = android.os.Build.MODEL     // Catch:{ all -> 0x0081 }
            java.lang.String r6 = android.os.Build.VERSION.RELEASE     // Catch:{ all -> 0x0081 }
            java.lang.String r2 = "advertiserId"
            java.lang.String r7 = r0.getString(r2)     // Catch:{ all -> 0x0081 }
            java.lang.String r8 = r1.f57     // Catch:{ all -> 0x0081 }
            java.lang.String r9 = r1.f46     // Catch:{ all -> 0x0081 }
            r3 = r10
            r3.m101(r4, r5, r6, r7, r8, r9)     // Catch:{ all -> 0x0081 }
            java.lang.String r1 = "4.10.3.233"
            java.lang.String r2 = "AppsFlyerKey"
            java.lang.String r2 = r0.getString(r2)     // Catch:{ all -> 0x0081 }
            java.lang.String r3 = "KSAppsFlyerId"
            java.lang.String r3 = r0.getString(r3)     // Catch:{ all -> 0x0081 }
            java.lang.String r4 = "uid"
            java.lang.String r4 = r0.getString(r4)     // Catch:{ all -> 0x0081 }
            r10.m100(r1, r2, r3, r4)     // Catch:{ all -> 0x0081 }
            r1 = 0
            android.content.pm.PackageInfo r12 = r12.getPackageInfo(r11, r1)     // Catch:{ all -> 0x0065 }
            int r12 = r12.versionCode     // Catch:{ all -> 0x0065 }
            java.lang.String r1 = "channel"
            java.lang.String r1 = r0.getString(r1)     // Catch:{ all -> 0x0065 }
            java.lang.String r2 = "preInstallName"
            java.lang.String r2 = r0.getString(r2)     // Catch:{ all -> 0x0065 }
            java.lang.String r12 = java.lang.String.valueOf(r12)     // Catch:{ all -> 0x0065 }
            r10.m99(r11, r12, r1, r2)     // Catch:{ all -> 0x0065 }
        L_0x0065:
            java.lang.String r11 = "remote_debug_static_data"
            org.json.JSONObject r12 = r10.f117     // Catch:{ all -> 0x0081 }
            java.lang.String r12 = r12.toString()     // Catch:{ all -> 0x0081 }
            r0.set(r11, r12)     // Catch:{ all -> 0x0081 }
        L_0x0070:
            org.json.JSONObject r11 = r10.f117     // Catch:{ JSONException -> 0x007b }
            java.lang.String r12 = "launch_counter"
            java.lang.String r0 = r10.f121     // Catch:{ JSONException -> 0x007b }
            r11.put(r12, r0)     // Catch:{ JSONException -> 0x007b }
            monitor-exit(r10)
            return
        L_0x007b:
            r11 = move-exception
            r11.printStackTrace()     // Catch:{ all -> 0x0081 }
            monitor-exit(r10)
            return
        L_0x0081:
            r11 = move-exception
            monitor-exit(r10)
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsflyer.internal.aa.m106(java.lang.String, android.content.pm.PackageManager):void");
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* renamed from: ˋ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.lang.String m104() {
        /*
            r4 = this;
            monitor-enter(r4)
            r0 = 0
            org.json.JSONObject r1 = r4.f117     // Catch:{ JSONException -> 0x0018, all -> 0x0015 }
            java.lang.String r2 = "data"
            org.json.JSONArray r3 = r4.f119     // Catch:{ JSONException -> 0x0018, all -> 0x0015 }
            r1.put(r2, r3)     // Catch:{ JSONException -> 0x0018, all -> 0x0015 }
            org.json.JSONObject r1 = r4.f117     // Catch:{ JSONException -> 0x0018, all -> 0x0015 }
            java.lang.String r0 = r1.toString()     // Catch:{ JSONException -> 0x0018, all -> 0x0015 }
            r4.m102()     // Catch:{ JSONException -> 0x0018, all -> 0x0015 }
            goto L_0x0018
        L_0x0015:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x0018:
            monitor-exit(r4)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsflyer.internal.aa.m104():java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00b4, code lost:
        return;
     */
    /* renamed from: ˏ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void m108(java.lang.String r12, java.lang.String r13, java.lang.String... r14) {
        /*
            r11 = this;
            monitor-enter(r11)
            boolean r0 = r11.f115     // Catch:{ all -> 0x00b5 }
            r1 = 1
            r2 = 0
            if (r0 == 0) goto L_0x0011
            boolean r0 = r11.f116     // Catch:{ all -> 0x00b5 }
            if (r0 != 0) goto L_0x000f
            boolean r0 = r11.f126     // Catch:{ all -> 0x00b5 }
            if (r0 == 0) goto L_0x0011
        L_0x000f:
            r0 = 1
            goto L_0x0012
        L_0x0011:
            r0 = 0
        L_0x0012:
            if (r0 == 0) goto L_0x00b3
            int r0 = r11.f123     // Catch:{ all -> 0x00b5 }
            r3 = 98304(0x18000, float:1.37753E-40)
            if (r0 < r3) goto L_0x001d
            goto L_0x00b3
        L_0x001d:
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00b1 }
            java.lang.String r0 = ""
            int r5 = r14.length     // Catch:{ all -> 0x00b1 }
            if (r5 <= 0) goto L_0x0045
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00b1 }
            r0.<init>()     // Catch:{ all -> 0x00b1 }
            int r5 = r14.length     // Catch:{ all -> 0x00b1 }
            int r5 = r5 - r1
        L_0x002d:
            if (r5 <= 0) goto L_0x003c
            r6 = r14[r5]     // Catch:{ all -> 0x00b1 }
            r0.append(r6)     // Catch:{ all -> 0x00b1 }
            java.lang.String r6 = ", "
            r0.append(r6)     // Catch:{ all -> 0x00b1 }
            int r5 = r5 + -1
            goto L_0x002d
        L_0x003c:
            r14 = r14[r2]     // Catch:{ all -> 0x00b1 }
            r0.append(r14)     // Catch:{ all -> 0x00b1 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00b1 }
        L_0x0045:
            java.text.SimpleDateFormat r14 = new java.text.SimpleDateFormat     // Catch:{ all -> 0x00b1 }
            java.lang.String r5 = "MM-dd HH:mm:ss.SSS"
            java.util.Locale r6 = java.util.Locale.ENGLISH     // Catch:{ all -> 0x00b1 }
            r14.<init>(r5, r6)     // Catch:{ all -> 0x00b1 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x00b1 }
            java.lang.String r14 = r14.format(r3)     // Catch:{ all -> 0x00b1 }
            r3 = 4
            r4 = 3
            r5 = 2
            r6 = 5
            if (r12 == 0) goto L_0x0080
            java.lang.String r7 = "%18s %5s _/%s [%s] %s %s"
            r8 = 6
            java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ all -> 0x00b1 }
            r8[r2] = r14     // Catch:{ all -> 0x00b1 }
            java.lang.Thread r14 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x00b1 }
            long r9 = r14.getId()     // Catch:{ all -> 0x00b1 }
            java.lang.Long r14 = java.lang.Long.valueOf(r9)     // Catch:{ all -> 0x00b1 }
            r8[r1] = r14     // Catch:{ all -> 0x00b1 }
            java.lang.String r14 = "AppsFlyer_4.10.3"
            r8[r5] = r14     // Catch:{ all -> 0x00b1 }
            r8[r4] = r12     // Catch:{ all -> 0x00b1 }
            r8[r3] = r13     // Catch:{ all -> 0x00b1 }
            r8[r6] = r0     // Catch:{ all -> 0x00b1 }
            java.lang.String r12 = java.lang.String.format(r7, r8)     // Catch:{ all -> 0x00b1 }
            goto L_0x00a0
        L_0x0080:
            java.lang.String r12 = "%18s %5s %s/%s %s"
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ all -> 0x00b1 }
            r6[r2] = r14     // Catch:{ all -> 0x00b1 }
            java.lang.Thread r14 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x00b1 }
            long r7 = r14.getId()     // Catch:{ all -> 0x00b1 }
            java.lang.Long r14 = java.lang.Long.valueOf(r7)     // Catch:{ all -> 0x00b1 }
            r6[r1] = r14     // Catch:{ all -> 0x00b1 }
            r6[r5] = r13     // Catch:{ all -> 0x00b1 }
            java.lang.String r13 = "AppsFlyer_4.10.3"
            r6[r4] = r13     // Catch:{ all -> 0x00b1 }
            r6[r3] = r0     // Catch:{ all -> 0x00b1 }
            java.lang.String r12 = java.lang.String.format(r12, r6)     // Catch:{ all -> 0x00b1 }
        L_0x00a0:
            org.json.JSONArray r13 = r11.f119     // Catch:{ all -> 0x00b1 }
            r13.put(r12)     // Catch:{ all -> 0x00b1 }
            int r13 = r11.f123     // Catch:{ all -> 0x00b1 }
            byte[] r12 = r12.getBytes()     // Catch:{ all -> 0x00b1 }
            int r12 = r12.length     // Catch:{ all -> 0x00b1 }
            int r13 = r13 + r12
            r11.f123 = r13     // Catch:{ all -> 0x00b1 }
            monitor-exit(r11)
            return
        L_0x00b1:
            monitor-exit(r11)
            return
        L_0x00b3:
            monitor-exit(r11)
            return
        L_0x00b5:
            r12 = move-exception
            monitor-exit(r11)
            goto L_0x00b9
        L_0x00b8:
            throw r12
        L_0x00b9:
            goto L_0x00b8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsflyer.internal.aa.m108(java.lang.String, java.lang.String, java.lang.String[]):void");
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final synchronized void m105() {
        this.f116 = false;
        m102();
    }
}
