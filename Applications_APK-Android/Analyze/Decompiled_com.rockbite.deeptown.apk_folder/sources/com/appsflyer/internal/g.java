package com.appsflyer.internal;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import com.appsflyer.AFExecutor;
import com.appsflyer.AFLogger;
import com.appsflyer.AppsFlyerLib;
import com.appsflyer.AppsFlyerLibCore;
import com.appsflyer.AppsFlyerProperties;
import com.appsflyer.ServerConfigHandler;
import com.appsflyer.share.Constants;
import com.google.firebase.perf.network.FirebasePerfUrlConnection;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

public final class g {

    /* renamed from: ʼ  reason: contains not printable characters */
    private static Uri f201 = null;

    /* renamed from: ˊ  reason: contains not printable characters */
    public static String[] f202;

    /* renamed from: ˋ  reason: contains not printable characters */
    static final int f203 = ((int) TimeUnit.SECONDS.toMillis(2));

    /* renamed from: ˎ  reason: contains not printable characters */
    static Uri f204 = null;

    /* renamed from: ˏ  reason: contains not printable characters */
    public static String[] f205;

    /* renamed from: ॱ  reason: contains not printable characters */
    public static volatile boolean f206;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private static g f207 = null;

    /* renamed from: ʻ  reason: contains not printable characters */
    public int f208 = -1;

    /* renamed from: com.appsflyer.internal.g$2  reason: invalid class name */
    public class AnonymousClass2 implements Runnable {

        /* renamed from: ˊ  reason: contains not printable characters */
        private /* synthetic */ Uri f209;

        /* renamed from: ˏ  reason: contains not printable characters */
        private /* synthetic */ Context f211;

        /* renamed from: ॱ  reason: contains not printable characters */
        private /* synthetic */ Map f212;

        AnonymousClass2(Uri uri, Map map, Context context) {
            this.f209 = uri;
            this.f212 = map;
            this.f211 = context;
        }

        /* renamed from: ˏ  reason: contains not printable characters */
        public static void m156(Context context) {
            Context applicationContext = context.getApplicationContext();
            AFLogger.afInfoLog("onBecameBackground");
            AppsFlyerLibCore.getInstance().f60 = System.currentTimeMillis();
            AFLogger.afInfoLog("callStatsBackground background call");
            AppsFlyerLibCore.getInstance().m77(new WeakReference(applicationContext));
            if (aa.f104 == null) {
                aa.f104 = new aa();
            }
            aa aaVar = aa.f104;
            if (aaVar.f126) {
                aaVar.m107();
                if (applicationContext != null) {
                    String packageName = applicationContext.getPackageName();
                    PackageManager packageManager = applicationContext.getPackageManager();
                    try {
                        if (aa.f104 == null) {
                            aa.f104 = new aa();
                        }
                        aa.f104.m106(packageName, packageManager);
                        if (aa.f104 == null) {
                            aa.f104 = new aa();
                        }
                        String r6 = aa.f104.m104();
                        x xVar = new x(null, AppsFlyerLib.getInstance().isTrackingStopped());
                        xVar.f297 = r6;
                        xVar.f294 = false;
                        StringBuilder sb = new StringBuilder();
                        sb.append(ServerConfigHandler.getUrl("https://%smonitorsdk.%s/remote-debug?app_id="));
                        sb.append(packageName);
                        xVar.execute(sb.toString());
                    } catch (Throwable unused) {
                    }
                }
                aaVar.m103();
            } else {
                AFLogger.afDebugLog("RD status is OFF");
            }
            AFExecutor instance = AFExecutor.getInstance();
            try {
                AFExecutor.m0(instance.f4);
                if (instance.f2 instanceof ThreadPoolExecutor) {
                    AFExecutor.m0((ThreadPoolExecutor) instance.f2);
                }
            } catch (Throwable th) {
                AFLogger.afErrorLog("failed to stop Executors", th);
            }
        }

        public final void run() {
            long j2;
            HashMap hashMap = new HashMap();
            long currentTimeMillis = System.currentTimeMillis();
            Uri uri = null;
            try {
                StringBuilder sb = new StringBuilder("ESP deeplink resoling is started: ");
                sb.append(this.f209.toString());
                AFLogger.afRDLog(sb.toString());
                HttpURLConnection httpURLConnection = (HttpURLConnection) ((URLConnection) FirebasePerfUrlConnection.instrument(new URL(this.f209.toString()).openConnection()));
                httpURLConnection.setInstanceFollowRedirects(false);
                httpURLConnection.setReadTimeout(g.f203);
                httpURLConnection.setConnectTimeout(g.f203);
                httpURLConnection.setRequestProperty("User-agent", "Dalvik/2.1.0 (Linux; U; Android 6.0.1; Nexus 5 Build/M4B30Z)");
                httpURLConnection.connect();
                AFLogger.afRDLog("ESP deeplink resoling is finished");
                hashMap.put("status", String.valueOf(httpURLConnection.getResponseCode()));
                if (httpURLConnection.getResponseCode() >= 300 && httpURLConnection.getResponseCode() <= 305) {
                    uri = Uri.parse(httpURLConnection.getHeaderField(Constants.HTTP_REDIRECT_URL_HEADER_FIELD));
                }
                j2 = System.currentTimeMillis() - currentTimeMillis;
                httpURLConnection.disconnect();
            } catch (Throwable th) {
                hashMap.put("error", th.getLocalizedMessage());
                hashMap.put("status", "-1");
                j2 = System.currentTimeMillis() - currentTimeMillis;
                AFLogger.afErrorLog(th.getMessage(), th);
            }
            hashMap.put("latency", Long.toString(j2));
            if (uri != null) {
                hashMap.put("res", uri.toString());
            } else {
                hashMap.put("res", "");
            }
            StringBuilder sb2 = new StringBuilder("ESP deeplink results: ");
            sb2.append(new JSONObject(hashMap).toString());
            AFLogger.afRDLog(sb2.toString());
            synchronized (this.f212) {
                this.f212.put("af_deeplink_r", hashMap);
                this.f212.put("af_deeplink", this.f209.toString());
            }
            g.f206 = false;
            if (uri == null) {
                uri = this.f209;
            }
            AppsFlyerLibCore.getInstance().handleDeepLinkCallback(this.f211, this.f212, uri);
        }

        AnonymousClass2() {
        }
    }

    private g() {
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private void m152(Context context, Map<String, Object> map, Uri uri) {
        String obj = uri.toString();
        boolean z = false;
        if (f205 != null && !obj.contains("af_tranid=")) {
            StringBuilder sb = new StringBuilder("Validate ESP deeplinks : ");
            sb.append(Arrays.asList(f205));
            AFLogger.afRDLog(sb.toString());
            String[] strArr = f205;
            int length = strArr.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    break;
                }
                String str = strArr[i2];
                if (obj.contains("://".concat(String.valueOf(str)))) {
                    z = true;
                    break;
                } else {
                    AFLogger.afRDLog("Validate ESP: reject ".concat(String.valueOf(str)));
                    i2++;
                }
            }
        }
        if (z) {
            StringBuilder sb2 = new StringBuilder("Validation ESP succeeded for: ");
            sb2.append(uri.toString());
            AFLogger.afRDLog(sb2.toString());
            f206 = true;
            AFExecutor instance = AFExecutor.getInstance();
            if (instance.f3 == null) {
                instance.f3 = Executors.newSingleThreadScheduledExecutor(instance.f1);
            }
            instance.f3.execute(new AnonymousClass2(uri, map, context));
            return;
        }
        f201 = uri;
        AppsFlyerLibCore.getInstance().handleDeepLinkCallback(context, map, uri);
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public static g m154() {
        if (f207 == null) {
            f207 = new g();
        }
        return f207;
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static Uri m153(Intent intent) {
        if (intent == null || !"android.intent.action.VIEW".equals(intent.getAction())) {
            return null;
        }
        return intent.getData();
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m155(Intent intent, Context context, Map<String, Object> map) {
        Uri data = (intent == null || !"android.intent.action.VIEW".equals(intent.getAction())) ? null : intent.getData();
        if (data != null) {
            boolean z = AppsFlyerProperties.getInstance().getBoolean("consumeAfDeepLink", false);
            boolean z2 = (intent.getFlags() & 4194304) == 0;
            if (intent.hasExtra("appsflyer_click_ts") && !z) {
                long longExtra = intent.getLongExtra("appsflyer_click_ts", 0);
                long j2 = AppsFlyerProperties.getInstance().getLong("appsflyer_click_consumed_ts", 0);
                if (longExtra == 0 || longExtra == j2) {
                    StringBuilder sb = new StringBuilder("skipping re-use of previously consumed deep link: ");
                    sb.append(data.toString());
                    sb.append(" w/Ex: ");
                    sb.append(String.valueOf(longExtra));
                    AFLogger.afInfoLog(sb.toString());
                    return;
                }
                m152(context, map, data);
                AppsFlyerProperties.getInstance().set("appsflyer_click_consumed_ts", longExtra);
            } else if (z || z2) {
                Boolean valueOf = Boolean.valueOf(z2);
                if (!data.equals(f201)) {
                    m152(context, map, data);
                    return;
                }
                StringBuilder sb2 = new StringBuilder("skipping re-use of previously consumed deep link: ");
                sb2.append(data.toString());
                sb2.append(valueOf.booleanValue() ? " w/sT" : " w/cAPI");
                AFLogger.afInfoLog(sb2.toString());
            } else {
                if (this.f208 != AppsFlyerProperties.getInstance().getInt("lastActivityHash", 0)) {
                    m152(context, map, data);
                    AppsFlyerProperties.getInstance().set("lastActivityHash", this.f208);
                    return;
                }
                StringBuilder sb3 = new StringBuilder("skipping re-use of previously consumed deep link: ");
                sb3.append(data.toString());
                sb3.append(" w/hC: ");
                sb3.append(String.valueOf(this.f208));
                AFLogger.afInfoLog(sb3.toString());
            }
        } else {
            Uri uri = f204;
            if (uri != null && uri != f201) {
                m152(context, map, uri);
                StringBuilder sb4 = new StringBuilder("using trampoline Intent fallback with URI : ");
                sb4.append(f204.toString());
                AFLogger.afInfoLog(sb4.toString());
                f204 = null;
            } else if (AppsFlyerLibCore.getInstance().latestDeepLink != null) {
                m152(context, map, AppsFlyerLibCore.getInstance().latestDeepLink);
            }
        }
    }
}
