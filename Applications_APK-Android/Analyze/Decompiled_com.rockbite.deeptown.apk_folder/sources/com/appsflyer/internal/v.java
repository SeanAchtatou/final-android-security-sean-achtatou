package com.appsflyer.internal;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

public final class v {

    /* renamed from: ˊ  reason: contains not printable characters */
    public final Context f288;

    public v(Context context) {
        this.f288 = context;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final String m190() {
        Cursor query = this.f288.getContentResolver().query(Uri.parse("content://com.facebook.katana.provider.AttributionIdProvider"), new String[]{"aid"}, null, null, null);
        String string = (query == null || !query.moveToFirst()) ? null : query.getString(query.getColumnIndex("aid"));
        if (query != null) {
            query.close();
        }
        return string;
    }
}
