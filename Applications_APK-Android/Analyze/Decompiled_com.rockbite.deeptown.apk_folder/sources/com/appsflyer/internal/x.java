package com.appsflyer.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.TrafficStats;
import android.os.AsyncTask;
import android.os.Build;
import com.appsflyer.AFFacebookDeferredDeeplink;
import com.appsflyer.AFLogger;
import com.appsflyer.AppsFlyerLibCore;
import com.appsflyer.ServerConfigHandler;
import com.google.firebase.perf.FirebasePerformance;
import com.google.firebase.perf.network.FirebasePerfUrlConnection;
import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import org.json.JSONObject;

public final class x extends AsyncTask<String, Void, String> {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f290 = false;

    /* renamed from: ʼ  reason: contains not printable characters */
    private String f291 = "";

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f292 = false;

    /* renamed from: ˊ  reason: contains not printable characters */
    HttpURLConnection f293;

    /* renamed from: ˋ  reason: contains not printable characters */
    boolean f294;

    /* renamed from: ˎ  reason: contains not printable characters */
    boolean f295;

    /* renamed from: ˏ  reason: contains not printable characters */
    public Map<String, String> f296;

    /* renamed from: ॱ  reason: contains not printable characters */
    String f297;

    /* renamed from: ॱॱ  reason: contains not printable characters */
    private URL f298;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private WeakReference<Context> f299;

    public x(Context context, boolean z) {
        this.f299 = new WeakReference<>(context);
        this.f294 = true;
        this.f295 = true;
        this.f292 = z;
    }

    /* access modifiers changed from: protected */
    public final void onCancelled() {
    }

    public final void onPreExecute() {
        if (this.f297 == null) {
            this.f297 = new JSONObject(this.f296).toString();
        }
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public final String doInBackground(String... strArr) {
        if (this.f292) {
            return null;
        }
        try {
            this.f298 = new URL(strArr[0]);
            if (this.f294) {
                if (aa.f104 == null) {
                    aa.f104 = new aa();
                }
                aa.f104.m108("server_request", this.f298.toString(), this.f297);
                int length = this.f297.getBytes("UTF-8").length;
                StringBuilder sb = new StringBuilder("call = ");
                sb.append(this.f298);
                sb.append("; size = ");
                sb.append(length);
                sb.append(" byte");
                sb.append(length > 1 ? "s" : "");
                sb.append("; body = ");
                sb.append(this.f297);
                AFFacebookDeferredDeeplink.AnonymousClass2.m5(sb.toString());
            }
            if (Build.VERSION.SDK_INT >= 14) {
                TrafficStats.setThreadStatsTag("AppsFlyer".hashCode());
            }
            this.f293 = (HttpURLConnection) ((URLConnection) FirebasePerfUrlConnection.instrument(this.f298.openConnection()));
            this.f293.setReadTimeout(30000);
            this.f293.setConnectTimeout(30000);
            this.f293.setRequestMethod(FirebasePerformance.HttpMethod.POST);
            this.f293.setDoInput(true);
            this.f293.setDoOutput(true);
            this.f293.setRequestProperty("Content-Type", "application/json");
            OutputStream outputStream = this.f293.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            bufferedWriter.write(this.f297);
            bufferedWriter.flush();
            bufferedWriter.close();
            outputStream.close();
            this.f293.connect();
            int responseCode = this.f293.getResponseCode();
            if (this.f295) {
                AppsFlyerLibCore.getInstance();
                this.f291 = AppsFlyerLibCore.m65(this.f293);
            }
            if (this.f294) {
                if (aa.f104 == null) {
                    aa.f104 = new aa();
                }
                aa.f104.m108("server_response", this.f298.toString(), String.valueOf(responseCode), this.f291);
            }
            if (responseCode == 200) {
                AFLogger.afInfoLog("Status 200 ok");
                Context context = this.f299.get();
                if (this.f298.toString().startsWith(ServerConfigHandler.getUrl(AppsFlyerLibCore.f30)) && context != null) {
                    SharedPreferences.Editor edit = AppsFlyerLibCore.m49(context).edit();
                    edit.putBoolean("sentRegisterRequestToAF", true);
                    edit.apply();
                    AFLogger.afDebugLog("Successfully registered for Uninstall Tracking");
                }
            } else {
                this.f290 = true;
            }
        } catch (Throwable th) {
            StringBuilder sb2 = new StringBuilder("Error while calling ");
            sb2.append(this.f298.toString());
            AFLogger.afErrorLog(sb2.toString(), th);
            this.f290 = true;
        }
        return this.f291;
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    public final void onPostExecute(String str) {
        if (this.f290) {
            AFLogger.afInfoLog("Connection error: ".concat(String.valueOf(str)));
        } else {
            AFLogger.afInfoLog("Connection call succeeded: ".concat(String.valueOf(str)));
        }
    }
}
