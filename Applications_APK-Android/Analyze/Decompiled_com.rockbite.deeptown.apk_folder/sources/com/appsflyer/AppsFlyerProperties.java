package com.appsflyer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import com.tapjoy.TapjoyConstants;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class AppsFlyerProperties {
    public static final String ADDITIONAL_CUSTOM_DATA = "additionalCustomData";
    public static final String AF_KEY = "AppsFlyerKey";
    public static final String AF_WAITFOR_CUSTOMERID = "waitForCustomerId";
    public static final String APP_ID = "appid";
    public static final String APP_USER_ID = "AppUserId";
    public static final String CHANNEL = "channel";
    public static final String COLLECT_ANDROID_ID = "collectAndroidId";
    public static final String COLLECT_ANDROID_ID_FORCE_BY_USER = "collectAndroidIdForceByUser";
    public static final String COLLECT_FACEBOOK_ATTR_ID = "collectFacebookAttrId";
    public static final String COLLECT_FINGER_PRINT = "collectFingerPrint";
    public static final String COLLECT_IMEI = "collectIMEI";
    public static final String COLLECT_IMEI_FORCE_BY_USER = "collectIMEIForceByUser";
    public static final String COLLECT_MAC = "collectMAC";
    public static final String COLLECT_OAID = "collectOAID";
    public static final String CURRENCY_CODE = "currencyCode";
    public static final String DEVICE_TRACKING_DISABLED = "deviceTrackingDisabled";
    public static final String DISABLE_KEYSTORE = "keyPropDisableAFKeystore";
    public static final String DISABLE_LOGS_COMPLETELY = "disableLogs";
    public static final String DISABLE_OTHER_SDK = "disableOtherSdk";
    public static final String DPM = "disableProxy";
    public static final String EMAIL_CRYPT_TYPE = "userEmailsCryptType";
    public static final String ENABLE_GPS_FALLBACK = "enableGpsFallback";
    public static final String EXTENSION = "sdkExtension";
    public static final String IS_MONITOR = "shouldMonitor";
    public static final String IS_UPDATE = "IS_UPDATE";
    public static final String LAUNCH_PROTECT_ENABLED = "launchProtectEnabled";
    public static final String ONELINK_DOMAIN = "onelinkDomain";
    public static final String ONELINK_ID = "oneLinkSlug";
    public static final String ONELINK_SCHEME = "onelinkScheme";
    public static final String USER_EMAIL = "userEmail";
    public static final String USER_EMAILS = "userEmails";
    public static final String USE_HTTP_FALLBACK = "useHttpFallback";

    /* renamed from: ॱ  reason: contains not printable characters */
    private static AppsFlyerProperties f85 = new AppsFlyerProperties();

    /* renamed from: ʽ  reason: contains not printable characters */
    private boolean f86 = false;

    /* renamed from: ˊ  reason: contains not printable characters */
    private boolean f87;

    /* renamed from: ˋ  reason: contains not printable characters */
    private Map<String, Object> f88 = Collections.synchronizedMap(new HashMap());

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean f89;

    /* renamed from: ˏ  reason: contains not printable characters */
    private String f90;

    public enum EmailsCryptType {
        NONE(0),
        SHA1(1),
        MD5(2),
        SHA256(3);
        

        /* renamed from: ˏ  reason: contains not printable characters */
        private final int f92;

        private EmailsCryptType(int i2) {
            this.f92 = i2;
        }

        public final int getValue() {
            return this.f92;
        }
    }

    private AppsFlyerProperties() {
    }

    public static AppsFlyerProperties getInstance() {
        return f85;
    }

    public boolean getBoolean(String str, boolean z) {
        String string = getString(str);
        if (string == null) {
            return z;
        }
        return Boolean.valueOf(string).booleanValue();
    }

    public int getInt(String str, int i2) {
        String string = getString(str);
        if (string == null) {
            return i2;
        }
        return Integer.valueOf(string).intValue();
    }

    public long getLong(String str, long j2) {
        String string = getString(str);
        if (string == null) {
            return j2;
        }
        return Long.valueOf(string).longValue();
    }

    public Object getObject(String str) {
        return this.f88.get(str);
    }

    public String getReferrer(Context context) {
        String str = this.f90;
        if (str != null) {
            return str;
        }
        if (getString("AF_REFERRER") != null) {
            return getString("AF_REFERRER");
        }
        if (context == null) {
            return null;
        }
        return AppsFlyerLibCore.m49(context).getString(TapjoyConstants.TJC_REFERRER, null);
    }

    public String getString(String str) {
        return (String) this.f88.get(str);
    }

    public boolean isEnableLog() {
        return getBoolean("shouldLog", true);
    }

    /* access modifiers changed from: protected */
    public boolean isFirstLaunchCalled() {
        return this.f89;
    }

    public boolean isLogsDisabledCompletely() {
        return getBoolean(DISABLE_LOGS_COMPLETELY, false);
    }

    /* access modifiers changed from: protected */
    public boolean isOnReceiveCalled() {
        return this.f87;
    }

    public boolean isOtherSdkStringDisabled() {
        return getBoolean(DISABLE_OTHER_SDK, false);
    }

    public void loadProperties(Context context) {
        String string;
        if (!this.f86 && (string = AppsFlyerLibCore.m49(context).getString("savedProperties", null)) != null) {
            AFLogger.afDebugLog("Loading properties..");
            try {
                JSONObject jSONObject = new JSONObject(string);
                Iterator<String> keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    if (this.f88.get(next) == null) {
                        this.f88.put(next, jSONObject.getString(next));
                    }
                }
                this.f86 = true;
            } catch (JSONException e2) {
                AFLogger.afErrorLog("Failed loading properties", e2);
            }
            StringBuilder sb = new StringBuilder("Done loading properties: ");
            sb.append(this.f86);
            AFLogger.afDebugLog(sb.toString());
        }
    }

    public void remove(String str) {
        this.f88.remove(str);
    }

    @SuppressLint({"CommitPrefEdits"})
    public void saveProperties(SharedPreferences sharedPreferences) {
        String jSONObject = new JSONObject(this.f88).toString();
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("savedProperties", jSONObject);
        edit.apply();
    }

    public void set(String str, String str2) {
        this.f88.put(str, str2);
    }

    public void setCustomData(String str) {
        this.f88.put(ADDITIONAL_CUSTOM_DATA, str);
    }

    /* access modifiers changed from: protected */
    public void setFirstLaunchCalled(boolean z) {
        this.f89 = z;
    }

    /* access modifiers changed from: protected */
    public void setOnReceiveCalled() {
        this.f87 = true;
    }

    /* access modifiers changed from: protected */
    public void setReferrer(String str) {
        set("AF_REFERRER", str);
        this.f90 = str;
    }

    public void setUserEmails(String str) {
        this.f88.put(USER_EMAILS, str);
    }

    public void set(String str, String[] strArr) {
        this.f88.put(str, strArr);
    }

    /* access modifiers changed from: protected */
    public void setFirstLaunchCalled() {
        this.f89 = true;
    }

    public void set(String str, int i2) {
        this.f88.put(str, Integer.toString(i2));
    }

    public void set(String str, long j2) {
        this.f88.put(str, Long.toString(j2));
    }

    public void set(String str, boolean z) {
        this.f88.put(str, Boolean.toString(z));
    }
}
