package com.appsflyer;

import android.content.Context;
import com.appsflyer.share.Constants;
import com.appsflyer.share.LinkGenerator;
import com.google.firebase.perf.FirebasePerformance;
import com.tapjoy.TJAdUnitConstants;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import org.json.JSONException;
import org.json.JSONObject;

public class CreateOneLinkHttpTask extends OneLinkHttpTask {

    /* renamed from: ʻ  reason: contains not printable characters */
    private Context f93;

    /* renamed from: ˊ  reason: contains not printable characters */
    private Map<String, String> f94;

    /* renamed from: ˋ  reason: contains not printable characters */
    private ResponseListener f95;

    /* renamed from: ˎ  reason: contains not printable characters */
    private String f96;

    /* renamed from: ॱ  reason: contains not printable characters */
    private String f97 = "";

    /* renamed from: ॱॱ  reason: contains not printable characters */
    private boolean f98 = false;

    public interface ResponseListener {
        void onResponse(String str);

        void onResponseError(String str);
    }

    public CreateOneLinkHttpTask(String str, Map<String, String> map, AppsFlyerLibCore appsFlyerLibCore, Context context, boolean z) {
        super(appsFlyerLibCore);
        this.f98 = z;
        this.f93 = context;
        if (this.f93 != null) {
            this.f97 = context.getPackageName();
        } else {
            AFLogger.afWarnLog("CreateOneLinkHttpTask: context can't be null");
        }
        this.f100 = str;
        this.f96 = "-1";
        this.f94 = map;
    }

    public void setListener(ResponseListener responseListener) {
        this.f95 = responseListener;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m90(HttpsURLConnection httpsURLConnection) throws JSONException, IOException {
        if (!this.f98) {
            httpsURLConnection.setRequestMethod(FirebasePerformance.HttpMethod.POST);
            httpsURLConnection.setDoInput(true);
            httpsURLConnection.setDoOutput(true);
            httpsURLConnection.setUseCaches(false);
            JSONObject jSONObject = new JSONObject();
            JSONObject jSONObject2 = new JSONObject(this.f94);
            jSONObject.put("ttl", this.f96);
            jSONObject.put(TJAdUnitConstants.String.DATA, jSONObject2);
            httpsURLConnection.connect();
            DataOutputStream dataOutputStream = new DataOutputStream(httpsURLConnection.getOutputStream());
            dataOutputStream.writeBytes(jSONObject.toString());
            dataOutputStream.flush();
            dataOutputStream.close();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public final void m91(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                this.f95.onResponse(jSONObject.optString(keys.next()));
            }
        } catch (JSONException e2) {
            this.f95.onResponseError("Can't parse one link data");
            AFLogger.afErrorLog("Error while parsing to json ".concat(String.valueOf(str)), e2);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public final String m92() {
        StringBuilder sb = new StringBuilder();
        sb.append(ServerConfigHandler.getUrl("https://%sonelink.%s/shortlink-sdk/v1"));
        sb.append(Constants.URL_PATH_DELIMITER);
        sb.append(this.f100);
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m89() {
        LinkGenerator addParameters = new LinkGenerator(Constants.USER_INVITE_LINK_TYPE).setBaseURL(this.f100, AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.ONELINK_DOMAIN), this.f97).addParameter(Constants.URL_SITE_ID, this.f97).addParameters(this.f94);
        String string = AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.APP_USER_ID);
        if (string != null) {
            addParameters.setReferrerCustomerId(string);
        }
        this.f95.onResponse(addParameters.generateLink());
    }
}
