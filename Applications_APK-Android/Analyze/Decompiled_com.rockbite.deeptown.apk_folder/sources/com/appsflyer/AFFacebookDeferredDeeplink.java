package com.appsflyer;

import android.os.Bundle;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Scanner;

public final class AFFacebookDeferredDeeplink {

    /* renamed from: ˊ  reason: contains not printable characters */
    String f8;

    /* renamed from: ˋ  reason: contains not printable characters */
    public String f9;

    /* renamed from: ˏ  reason: contains not printable characters */
    String f10;

    /* renamed from: ॱ  reason: contains not printable characters */
    String f11;

    /* renamed from: com.appsflyer.AFFacebookDeferredDeeplink$2  reason: invalid class name */
    public static class AnonymousClass2 implements InvocationHandler {

        /* renamed from: ˋ  reason: contains not printable characters */
        private static String f12;

        /* renamed from: ˏ  reason: contains not printable characters */
        private static String f13;

        /* renamed from: ˊ  reason: contains not printable characters */
        private /* synthetic */ Class f14;

        /* renamed from: ॱ  reason: contains not printable characters */
        private /* synthetic */ AppLinkFetchEvents f15;

        AnonymousClass2(Class cls, AppLinkFetchEvents appLinkFetchEvents) {
            this.f14 = cls;
            this.f15 = appLinkFetchEvents;
        }

        /* renamed from: ˊ  reason: contains not printable characters */
        static void m4(String str) {
            f13 = str;
            StringBuilder sb = new StringBuilder();
            for (int i2 = 0; i2 < str.length(); i2++) {
                if (i2 == 0 || i2 == str.length() - 1) {
                    sb.append(str.charAt(i2));
                } else {
                    sb.append("*");
                }
            }
            f12 = sb.toString();
        }

        /* renamed from: ˎ  reason: contains not printable characters */
        public static void m5(String str) {
            if (f13 == null) {
                m4(AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.AF_KEY));
            }
            String str2 = f13;
            if (str2 != null && str.contains(str2)) {
                AFLogger.afInfoLog(str.replace(f13, f12));
            }
        }

        public final Object invoke(Object obj, Method method, Object[] objArr) throws Throwable {
            String str;
            String str2;
            String str3;
            Bundle bundle;
            if (method.getName().equals("onDeferredAppLinkDataFetched")) {
                if (objArr[0] != null) {
                    Bundle cast = Bundle.class.cast(this.f14.getMethod("getArgumentBundle", new Class[0]).invoke(this.f14.cast(objArr[0]), new Object[0]));
                    if (cast != null) {
                        str = cast.getString("com.facebook.platform.APPLINK_NATIVE_URL");
                        str3 = cast.getString("target_url");
                        Bundle bundle2 = cast.getBundle("extras");
                        str2 = (bundle2 == null || (bundle = bundle2.getBundle("deeplink_context")) == null) ? null : bundle.getString("promo_code");
                    } else {
                        str2 = null;
                        str = null;
                        str3 = null;
                    }
                    AppLinkFetchEvents appLinkFetchEvents = this.f15;
                    if (appLinkFetchEvents != null) {
                        appLinkFetchEvents.onAppLinkFetchFinished(str, str3, str2);
                    }
                } else {
                    AppLinkFetchEvents appLinkFetchEvents2 = this.f15;
                    if (appLinkFetchEvents2 != null) {
                        appLinkFetchEvents2.onAppLinkFetchFinished(null, null, null);
                    }
                }
                return null;
            }
            AppLinkFetchEvents appLinkFetchEvents3 = this.f15;
            if (appLinkFetchEvents3 != null) {
                appLinkFetchEvents3.onAppLinkFetchFailed("onDeferredAppLinkDataFetched invocation failed");
            }
            return null;
        }

        AnonymousClass2() {
        }
    }

    public interface AppLinkFetchEvents {
        void onAppLinkFetchFailed(String str);

        void onAppLinkFetchFinished(String str, String str2, String str3);
    }

    AFFacebookDeferredDeeplink() {
    }

    AFFacebookDeferredDeeplink(String str, String str2, String str3) {
        this.f8 = str;
        this.f10 = str2;
        this.f11 = str3;
    }

    public AFFacebookDeferredDeeplink(char[] cArr) {
        Scanner scanner = new Scanner(new String(cArr));
        while (scanner.hasNextLine()) {
            String nextLine = scanner.nextLine();
            if (nextLine.startsWith("url=")) {
                this.f8 = nextLine.substring(4).trim();
            } else if (nextLine.startsWith("version=")) {
                this.f11 = nextLine.substring(8).trim();
            } else if (nextLine.startsWith("data=")) {
                this.f10 = nextLine.substring(5).trim();
            }
        }
        scanner.close();
    }
}
