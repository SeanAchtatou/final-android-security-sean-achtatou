package com.appsflyer.share;

import android.content.Context;
import com.appsflyer.AFLogger;
import com.appsflyer.AppsFlyerProperties;
import com.appsflyer.CreateOneLinkHttpTask;
import com.appsflyer.ServerConfigHandler;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class LinkGenerator {

    /* renamed from: ʻ  reason: contains not printable characters */
    private String f306;

    /* renamed from: ʼ  reason: contains not printable characters */
    private String f307;

    /* renamed from: ʽ  reason: contains not printable characters */
    private String f308;

    /* renamed from: ˊ  reason: contains not printable characters */
    private String f309;

    /* renamed from: ˋ  reason: contains not printable characters */
    private String f310;

    /* renamed from: ˎ  reason: contains not printable characters */
    private String f311;

    /* renamed from: ˏ  reason: contains not printable characters */
    String f312;

    /* renamed from: ˏॱ  reason: contains not printable characters */
    private Map<String, String> f313 = new HashMap();

    /* renamed from: ͺ  reason: contains not printable characters */
    private Map<String, String> f314 = new HashMap();

    /* renamed from: ॱ  reason: contains not printable characters */
    String f315;

    /* renamed from: ॱˊ  reason: contains not printable characters */
    private String f316;

    /* renamed from: ॱॱ  reason: contains not printable characters */
    private String f317;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private String f318;

    public LinkGenerator(String str) {
        this.f311 = str;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private static String m209(String str, String str2) {
        try {
            return URLEncoder.encode(str, "utf8");
        } catch (UnsupportedEncodingException unused) {
            StringBuilder sb = new StringBuilder("Illegal ");
            sb.append(str2);
            sb.append(": ");
            sb.append(str);
            AFLogger.afInfoLog(sb.toString());
            return "";
        } catch (Throwable unused2) {
            return "";
        }
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    private StringBuilder m210() {
        StringBuilder sb = new StringBuilder();
        String str = this.f312;
        if (str == null || !str.startsWith("http")) {
            sb.append(ServerConfigHandler.getUrl(Constants.BASE_URL_APP_APPSFLYER_COM));
        } else {
            sb.append(this.f312);
        }
        if (this.f315 != null) {
            sb.append('/');
            sb.append(this.f315);
        }
        this.f314.put(Constants.URL_MEDIA_SOURCE, this.f311);
        sb.append('?');
        sb.append("pid=");
        sb.append(m209(this.f311, "media source"));
        String str2 = this.f306;
        if (str2 != null) {
            this.f314.put(Constants.URL_REFERRER_UID, str2);
            sb.append('&');
            sb.append("af_referrer_uid=");
            sb.append(m209(this.f306, "referrerUID"));
        }
        String str3 = this.f310;
        if (str3 != null) {
            this.f314.put("af_channel", str3);
            sb.append('&');
            sb.append("af_channel=");
            sb.append(m209(this.f310, AppsFlyerProperties.CHANNEL));
        }
        String str4 = this.f317;
        if (str4 != null) {
            this.f314.put(Constants.URL_REFERRER_CUSTOMER_ID, str4);
            sb.append('&');
            sb.append("af_referrer_customer_id=");
            sb.append(m209(this.f317, "referrerCustomerId"));
        }
        String str5 = this.f309;
        if (str5 != null) {
            this.f314.put(Constants.URL_CAMPAIGN, str5);
            sb.append('&');
            sb.append("c=");
            sb.append(m209(this.f309, FirebaseAnalytics.Param.CAMPAIGN));
        }
        String str6 = this.f307;
        if (str6 != null) {
            this.f314.put(Constants.URL_REFERRER_NAME, str6);
            sb.append('&');
            sb.append("af_referrer_name=");
            sb.append(m209(this.f307, "referrerName"));
        }
        String str7 = this.f308;
        if (str7 != null) {
            this.f314.put(Constants.URL_REFERRER_IMAGE_URL, str7);
            sb.append('&');
            sb.append("af_referrer_image_url=");
            sb.append(m209(this.f308, "referrerImageURL"));
        }
        if (this.f316 != null) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(this.f316);
            String str8 = "";
            sb2.append(this.f316.endsWith(Constants.URL_PATH_DELIMITER) ? str8 : Constants.URL_PATH_DELIMITER);
            String str9 = this.f318;
            if (str9 != null) {
                sb2.append(str9);
            }
            this.f314.put(Constants.URL_BASE_DEEPLINK, sb2.toString());
            sb.append('&');
            sb.append("af_dp=");
            sb.append(m209(this.f316, "baseDeeplink"));
            if (this.f318 != null) {
                if (!this.f316.endsWith(Constants.URL_PATH_DELIMITER)) {
                    str8 = "%2F";
                }
                sb.append(str8);
                sb.append(m209(this.f318, "deeplinkPath"));
            }
        }
        for (String next : this.f313.keySet()) {
            String obj = sb.toString();
            StringBuilder sb3 = new StringBuilder();
            sb3.append(next);
            sb3.append("=");
            sb3.append(m209(this.f313.get(next), next));
            if (!obj.contains(sb3.toString())) {
                sb.append('&');
                sb.append(next);
                sb.append('=');
                sb.append(m209(this.f313.get(next), next));
            }
        }
        return sb;
    }

    public LinkGenerator addParameter(String str, String str2) {
        this.f313.put(str, str2);
        return this;
    }

    public LinkGenerator addParameters(Map<String, String> map) {
        if (map != null) {
            this.f313.putAll(map);
        }
        return this;
    }

    public String generateLink() {
        return m210().toString();
    }

    public String getCampaign() {
        return this.f309;
    }

    public String getChannel() {
        return this.f310;
    }

    public String getMediaSource() {
        return this.f311;
    }

    public Map<String, String> getParameters() {
        return this.f313;
    }

    public LinkGenerator setBaseDeeplink(String str) {
        this.f316 = str;
        return this;
    }

    public LinkGenerator setBaseURL(String str, String str2, String str3) {
        if (str == null || str.length() <= 0) {
            this.f312 = String.format(Constants.AF_BASE_URL_FORMAT, ServerConfigHandler.getUrl(Constants.APPSFLYER_DEFAULT_APP_DOMAIN), str3);
        } else {
            if (str2 == null || str2.length() < 5) {
                str2 = Constants.ONELINK_DEFAULT_DOMAIN;
            }
            this.f312 = String.format(Constants.AF_BASE_URL_FORMAT, str2, str);
        }
        return this;
    }

    public LinkGenerator setCampaign(String str) {
        this.f309 = str;
        return this;
    }

    public LinkGenerator setChannel(String str) {
        this.f310 = str;
        return this;
    }

    public LinkGenerator setDeeplinkPath(String str) {
        this.f318 = str;
        return this;
    }

    public LinkGenerator setReferrerCustomerId(String str) {
        this.f317 = str;
        return this;
    }

    public LinkGenerator setReferrerImageURL(String str) {
        this.f308 = str;
        return this;
    }

    public LinkGenerator setReferrerName(String str) {
        this.f307 = str;
        return this;
    }

    public LinkGenerator setReferrerUID(String str) {
        this.f306 = str;
        return this;
    }

    public void generateLink(Context context, CreateOneLinkHttpTask.ResponseListener responseListener) {
        String string = AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.ONELINK_ID);
        if (!this.f313.isEmpty()) {
            for (Map.Entry next : this.f313.entrySet()) {
                this.f314.put(next.getKey(), next.getValue());
            }
        }
        m210();
        ShareInviteHelper.generateUserInviteLink(context, string, this.f314, responseListener);
    }
}
