package com.appsflyer;

public final class R {

    public static final class xml {
        public static final int appsflyer_backup_rules = 2131623936;

        private xml() {
        }
    }

    private R() {
    }
}
