package com.chartboost.sdk;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import com.chartboost.sdk.a;
import com.chartboost.sdk.c.f;
import com.chartboost.sdk.c.g;
import com.chartboost.sdk.c.h;
import com.chartboost.sdk.d.a;
import com.chartboost.sdk.o.b1;
import com.chartboost.sdk.o.f1;
import com.chartboost.sdk.o.k;
import com.chartboost.sdk.o.l;
import com.chartboost.sdk.o.m;
import com.chartboost.sdk.o.n;
import com.chartboost.sdk.o.o;
import com.chartboost.sdk.o.p0;
import com.chartboost.sdk.o.r;
import com.chartboost.sdk.o.r0;
import com.chartboost.sdk.o.s;
import com.chartboost.sdk.o.y0;
import com.chartboost.sdk.o.z;
import com.chartboost.sdk.o.z0;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

public class m {
    private static m w;

    /* renamed from: a  reason: collision with root package name */
    private final f1 f5920a;

    /* renamed from: b  reason: collision with root package name */
    public final Executor f5921b;

    /* renamed from: c  reason: collision with root package name */
    final y0 f5922c;

    /* renamed from: d  reason: collision with root package name */
    public final f f5923d;

    /* renamed from: e  reason: collision with root package name */
    public final r0 f5924e;

    /* renamed from: f  reason: collision with root package name */
    public final p0 f5925f;

    /* renamed from: g  reason: collision with root package name */
    public final r0 f5926g;

    /* renamed from: h  reason: collision with root package name */
    public final p0 f5927h;

    /* renamed from: i  reason: collision with root package name */
    public final k f5928i;

    /* renamed from: j  reason: collision with root package name */
    public final z0 f5929j;

    /* renamed from: k  reason: collision with root package name */
    public final s f5930k;
    public final r0 l;
    public final p0 m;
    public final AtomicReference<com.chartboost.sdk.d.f> n;
    final SharedPreferences o;
    public final com.chartboost.sdk.e.a p;
    public final Handler q;
    public final h r;
    public final n s;
    boolean t;
    boolean u = false;
    boolean v = true;

    public class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final int f5933a;

        /* renamed from: b  reason: collision with root package name */
        String f5934b = null;

        /* renamed from: c  reason: collision with root package name */
        boolean f5935c = false;

        /* renamed from: d  reason: collision with root package name */
        boolean f5936d = false;

        b(int i2) {
            this.f5933a = i2;
        }

        public void run() {
            try {
                int i2 = this.f5933a;
                if (i2 == 0) {
                    m.this.c();
                } else if (i2 == 1) {
                    n.t = this.f5935c;
                } else if (i2 == 2) {
                    n.v = this.f5936d;
                    if (!this.f5936d || !m.f()) {
                        m.this.f5929j.b();
                    } else {
                        m.this.f5929j.a();
                    }
                } else if (i2 == 3) {
                    com.chartboost.sdk.o.m mVar = new com.chartboost.sdk.o.m("api/install", m.this.f5930k, m.this.p, 2, null);
                    mVar.m = true;
                    m.this.f5928i.a(mVar);
                    Executor executor = m.this.f5921b;
                    r0 r0Var = m.this.f5924e;
                    r0Var.getClass();
                    executor.execute(new r0.c(0, null, null, null));
                    Executor executor2 = m.this.f5921b;
                    r0 r0Var2 = m.this.f5926g;
                    r0Var2.getClass();
                    executor2.execute(new r0.c(0, null, null, null));
                    Executor executor3 = m.this.f5921b;
                    r0 r0Var3 = m.this.l;
                    r0Var3.getClass();
                    executor3.execute(new r0.c(0, null, null, null));
                    m.this.f5921b.execute(new b(4));
                    m.this.v = false;
                } else if (i2 == 4) {
                    m.this.f5929j.a();
                } else if (i2 == 5) {
                    if (n.f5941d != null) {
                        n.f5941d.didFailToLoadMoreApps(this.f5934b, a.c.END_POINT_DISABLED);
                    }
                }
            } catch (Exception e2) {
                com.chartboost.sdk.e.a.a(b.class, "run (" + this.f5933a + ")", e2);
            }
        }
    }

    m(Activity activity, String str, String str2, f1 f1Var, ScheduledExecutorService scheduledExecutorService, Handler handler, Executor executor) {
        JSONObject jSONObject;
        f1 f1Var2 = f1Var;
        ScheduledExecutorService scheduledExecutorService2 = scheduledExecutorService;
        Handler handler2 = handler;
        l a2 = l.a();
        n.n = activity.getApplicationContext();
        f fVar = new f(n.n);
        a2.a(fVar);
        this.f5923d = fVar;
        l lVar = new l();
        a2.a(lVar);
        l lVar2 = lVar;
        com.chartboost.sdk.c.k kVar = new com.chartboost.sdk.c.k();
        a2.a(kVar);
        com.chartboost.sdk.c.k kVar2 = kVar;
        r rVar = new r();
        a2.a(rVar);
        k kVar3 = new k(scheduledExecutorService, rVar, lVar2, kVar2, handler, executor);
        a2.a(kVar3);
        this.f5928i = kVar3;
        SharedPreferences a3 = a(n.n);
        try {
            jSONObject = new JSONObject(a3.getString("config", "{}"));
        } catch (Exception e2) {
            com.chartboost.sdk.c.a.b("Sdk", "Unable to process config");
            e2.printStackTrace();
            jSONObject = new JSONObject();
        }
        AtomicReference<com.chartboost.sdk.d.f> atomicReference = new AtomicReference<>(null);
        if (!g.a(atomicReference, jSONObject, a3)) {
            atomicReference.set(new com.chartboost.sdk.d.f(new JSONObject()));
        }
        this.f5920a = f1Var2;
        this.f5921b = scheduledExecutorService2;
        this.n = atomicReference;
        this.o = a3;
        this.q = handler2;
        h hVar = new h(f1Var2, n.n, atomicReference);
        if (!atomicReference.get().v) {
            n.w = "";
        } else {
            a(n.n, null, a3);
        }
        l lVar3 = lVar2;
        h hVar2 = hVar;
        AtomicReference<com.chartboost.sdk.d.f> atomicReference2 = atomicReference;
        SharedPreferences sharedPreferences = a3;
        s sVar = new s(n.n, str, this.f5923d, lVar3, atomicReference2, a3, kVar2);
        a2.a(sVar);
        this.f5930k = sVar;
        com.chartboost.sdk.e.a aVar = new com.chartboost.sdk.e.a(atomicReference);
        a2.a(aVar);
        this.p = aVar;
        y0 y0Var = new y0(scheduledExecutorService, hVar2, this.f5928i, lVar3, atomicReference2, kVar2, this.p);
        a2.a(y0Var);
        this.f5922c = y0Var;
        l a4 = l.a();
        z zVar = new z(handler2);
        a4.a(zVar);
        i iVar = new i(zVar, this.f5922c, atomicReference, handler2);
        a2.a(iVar);
        i iVar2 = iVar;
        n nVar = new n(scheduledExecutorService2, this.f5928i, lVar2, handler2);
        a2.a(nVar);
        this.s = nVar;
        l lVar4 = lVar2;
        l lVar5 = a2;
        h hVar3 = new h(activity, lVar4, this, this.p, handler, iVar2);
        lVar5.a(hVar3);
        this.r = hVar3;
        h hVar4 = hVar2;
        o oVar = new o(hVar4);
        lVar5.a(oVar);
        o oVar2 = oVar;
        this.f5925f = p0.c();
        this.f5927h = p0.a();
        this.m = p0.b();
        ScheduledExecutorService scheduledExecutorService3 = scheduledExecutorService;
        h hVar5 = hVar4;
        l lVar6 = lVar5;
        l lVar7 = lVar4;
        AtomicReference<com.chartboost.sdk.d.f> atomicReference3 = atomicReference;
        SharedPreferences sharedPreferences2 = sharedPreferences;
        com.chartboost.sdk.c.k kVar4 = kVar2;
        Handler handler3 = handler;
        r0 r0Var = new r0(this.f5925f, scheduledExecutorService3, this.f5922c, hVar5, this.f5928i, lVar7, this.f5930k, atomicReference3, sharedPreferences2, kVar4, this.p, handler3, this.r, this.s, iVar2, oVar2);
        lVar6.a(r0Var);
        this.f5924e = r0Var;
        r0 r0Var2 = new r0(this.f5927h, scheduledExecutorService, this.f5922c, hVar4, this.f5928i, lVar4, this.f5930k, atomicReference, sharedPreferences, kVar2, this.p, handler, this.r, this.s, iVar2, oVar2);
        lVar6.a(r0Var2);
        this.f5926g = r0Var2;
        r0 r0Var3 = new r0(this.m, scheduledExecutorService, this.f5922c, hVar4, this.f5928i, lVar4, this.f5930k, atomicReference, sharedPreferences, kVar2, this.p, handler, this.r, this.s, iVar2, oVar2);
        lVar6.a(r0Var3);
        this.l = r0Var3;
        z0 z0Var = new z0(this.f5922c, hVar4, this.f5928i, this.f5930k, this.p, atomicReference);
        lVar6.a(z0Var);
        this.f5929j = z0Var;
        n.l = str;
        n.m = str2;
        SharedPreferences sharedPreferences3 = sharedPreferences;
        if (!sharedPreferences3.contains("cbLimitTrack") || sharedPreferences3.contains("cbGDPR")) {
            n.x = a.c.a(sharedPreferences3.getInt("cbGDPR", n.x.a()));
        } else {
            n.x = sharedPreferences3.getBoolean("cbLimitTrack", false) ? a.c.NO_BEHAVIORAL : a.c.UNKNOWN;
        }
        if (f1.e().a(19)) {
            b1.a(activity.getApplication(), atomicReference.get().C, !atomicReference.get().D, !atomicReference.get().E);
        }
    }

    static void a(m mVar) {
        w = mVar;
    }

    public static m e() {
        return w;
    }

    static boolean f() {
        m e2 = e();
        if (e2 == null || !e2.n.get().f5843c) {
            return true;
        }
        try {
            throw new Exception("Chartboost Integration Warning: your account has been disabled for this session. This app has no active publishing campaigns, please create a publishing campaign in the Chartboost dashboard and wait at least 30 minutes to re-enable. If you need assistance, please visit http://chartboo.st/publishing .");
        } catch (Exception e3) {
            e3.printStackTrace();
            return false;
        }
    }

    private void g() {
        this.p.a();
        if (!this.v) {
            a(new b(3));
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.q.postDelayed(new b(0), 500);
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.p.b();
    }

    /* access modifiers changed from: package-private */
    public void d() {
        if (!this.u) {
            f fVar = n.f5941d;
            if (fVar != null) {
                fVar.didInitialize();
            }
            this.u = true;
        }
    }

    private static SharedPreferences a(Context context) {
        return context.getSharedPreferences("cbPrefs", 0);
    }

    public static void b(Runnable runnable) {
        f1 e2 = f1.e();
        if (!e2.d()) {
            e2.f6009a.post(runnable);
        } else {
            runnable.run();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Runnable runnable) {
        com.chartboost.sdk.o.m mVar = new com.chartboost.sdk.o.m("/api/config", this.f5930k, this.p, 1, new a(runnable));
        mVar.m = true;
        this.f5928i.a(mVar);
    }

    /* access modifiers changed from: package-private */
    public void a(Activity activity) {
        if (this.f5920a.a(23)) {
            g.a((Context) activity);
        }
        if (!this.v && !this.r.e()) {
            this.f5922c.c();
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (n.n == null) {
            com.chartboost.sdk.c.a.b("Sdk", "The context must be set through the Chartboost method onCreate() before calling startSession().");
        } else {
            g();
        }
    }

    class a implements m.a {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Runnable f5931a;

        a(Runnable runnable) {
            this.f5931a = runnable;
        }

        public void a(com.chartboost.sdk.o.m mVar, JSONObject jSONObject) {
            m.this.t = false;
            JSONObject a2 = g.a(jSONObject, "response");
            if (a2 != null) {
                m mVar2 = m.this;
                if (g.a(mVar2.n, a2, mVar2.o)) {
                    m.this.o.edit().putString("config", a2.toString()).apply();
                }
            }
            Runnable runnable = this.f5931a;
            if (runnable != null) {
                runnable.run();
            }
            if (!m.this.u) {
                f fVar = n.f5941d;
                if (fVar != null) {
                    fVar.didInitialize();
                }
                m.this.u = true;
            }
        }

        public void a(com.chartboost.sdk.o.m mVar, com.chartboost.sdk.d.a aVar) {
            m.this.t = false;
            Runnable runnable = this.f5931a;
            if (runnable != null) {
                runnable.run();
            }
            if (!m.this.u) {
                f fVar = n.f5941d;
                if (fVar != null) {
                    fVar.didInitialize();
                }
                m.this.u = true;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:19:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(android.content.Context r4, android.webkit.WebView r5, android.content.SharedPreferences r6) {
        /*
            java.lang.String r0 = "user_agent"
            java.lang.String r1 = "http.agent"
            java.lang.String r1 = java.lang.System.getProperty(r1)
            r2 = 16
            int r3 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x0048 }
            if (r3 == r2) goto L_0x0014
            java.lang.String r4 = android.webkit.WebSettings.getDefaultUserAgent(r4)     // Catch:{ Exception -> 0x0048 }
        L_0x0012:
            r1 = r4
            goto L_0x0049
        L_0x0014:
            if (r5 != 0) goto L_0x003f
            boolean r5 = r6.contains(r0)     // Catch:{ Exception -> 0x0048 }
            if (r5 != 0) goto L_0x0038
            android.os.Looper r5 = android.os.Looper.myLooper()     // Catch:{ Exception -> 0x0048 }
            android.os.Looper r3 = android.os.Looper.getMainLooper()     // Catch:{ Exception -> 0x0048 }
            if (r5 != r3) goto L_0x0049
            android.webkit.WebView r5 = new android.webkit.WebView     // Catch:{ Exception -> 0x0048 }
            android.content.Context r4 = r4.getApplicationContext()     // Catch:{ Exception -> 0x0048 }
            r5.<init>(r4)     // Catch:{ Exception -> 0x0048 }
            android.webkit.WebSettings r4 = r5.getSettings()     // Catch:{ Exception -> 0x0048 }
            java.lang.String r4 = r4.getUserAgentString()     // Catch:{ Exception -> 0x0048 }
            goto L_0x0012
        L_0x0038:
            java.lang.String r4 = com.chartboost.sdk.n.w     // Catch:{ Exception -> 0x0048 }
            java.lang.String r4 = r6.getString(r0, r4)     // Catch:{ Exception -> 0x0048 }
            goto L_0x0012
        L_0x003f:
            android.webkit.WebSettings r4 = r5.getSettings()     // Catch:{ Exception -> 0x0048 }
            java.lang.String r4 = r4.getUserAgentString()     // Catch:{ Exception -> 0x0048 }
            goto L_0x0012
        L_0x0048:
        L_0x0049:
            com.chartboost.sdk.n.w = r1
            int r4 = android.os.Build.VERSION.SDK_INT
            if (r4 != r2) goto L_0x005a
            android.content.SharedPreferences$Editor r4 = r6.edit()
            android.content.SharedPreferences$Editor r4 = r4.putString(r0, r1)
            r4.apply()
        L_0x005a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.m.a(android.content.Context, android.webkit.WebView, android.content.SharedPreferences):void");
    }

    static void a(Context context, a.c cVar) {
        n.x = cVar;
        SharedPreferences a2 = a(context);
        if (a2 != null) {
            a2.edit().putInt("cbGDPR", cVar.a()).apply();
        }
    }
}
