package com.chartboost.sdk;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import com.chartboost.sdk.c.a;
import com.chartboost.sdk.d.a;
import com.chartboost.sdk.d.f;
import com.chartboost.sdk.m;
import com.chartboost.sdk.o.f1;
import com.chartboost.sdk.o.p0;
import com.chartboost.sdk.o.r0;
import com.chartboost.sdk.o.t;
import java.util.HashMap;
import java.util.Map;

public class a {

    /* renamed from: com.chartboost.sdk.a$a  reason: collision with other inner class name */
    public enum C0125a {
        CBFrameworkUnity("Unity"),
        CBFrameworkCorona("Corona"),
        CBFrameworkAir("AIR"),
        CBFrameworkGameSalad("GameSalad"),
        CBFrameworkCordova("Cordova"),
        CBFrameworkCocoonJS("CocoonJS"),
        CBFrameworkCocos2dx("Cocos2dx"),
        CBFrameworkPrime31Unreal("Prime31Unreal"),
        CBFrameworkWeeby("Weeby"),
        CBFrameworkOther("Other");
        

        /* renamed from: a  reason: collision with root package name */
        private final String f5735a;

        private C0125a(String str) {
            this.f5735a = str;
        }

        public boolean a() {
            return this == CBFrameworkAir;
        }

        public String toString() {
            return this.f5735a;
        }
    }

    public enum b {
        CBMediationAdMarvel("AdMarvel"),
        CBMediationAdMob("AdMob"),
        CBMediationFuse("Fuse"),
        CBMediationFyber("Fyber"),
        CBMediationHeyZap("HeyZap"),
        CBMediationMoPub("MoPub"),
        CBMediationironSource("ironSource"),
        CBMediationHyprMX("HyprMX"),
        CBMediationAerServ("AerServ"),
        CBMediationHelium("Helium"),
        CBMediationOther("Other");
        

        /* renamed from: a  reason: collision with root package name */
        private final String f5746a;

        private b(String str) {
            this.f5746a = str;
        }

        public String toString() {
            return this.f5746a;
        }
    }

    private a() {
    }

    public static String a() {
        return "7.5.0";
    }

    public static void a(Activity activity, String str, String str2) {
        n.f5938a = "​!SDK-VERSION-STRING!:​com.chartboost.sdk:android-sdk:7.5.0";
        t.a("Chartboost.startWithAppId", activity);
        k kVar = new k(0);
        kVar.f5916i = activity;
        kVar.f5917j = str;
        kVar.f5918k = str2;
        m.b(kVar);
    }

    public static boolean b() {
        t.a("Chartboost.onBackPressed");
        m e2 = m.e();
        if (e2 == null) {
            return false;
        }
        return e2.r.j();
    }

    public static void c(Activity activity) {
        t.a("Chartboost.onPause", activity);
        m e2 = m.e();
        if (e2 != null && !n.s) {
            e2.r.g(activity);
        }
    }

    public static void d(Activity activity) {
        t.a("Chartboost.onResume", activity);
        m e2 = m.e();
        if (e2 != null && !n.s) {
            e2.r.f(activity);
        }
    }

    public static void e(Activity activity) {
        t.a("Chartboost.onStart", activity);
        m e2 = m.e();
        if (e2 != null && !n.s) {
            e2.r.d(activity);
        }
    }

    public static void f(Activity activity) {
        t.a("Chartboost.onStop", activity);
        m e2 = m.e();
        if (e2 != null && !n.s) {
            e2.r.h(activity);
        }
    }

    @TargetApi(28)
    public static void g(Activity activity) {
        if (activity != null && n.f5945h) {
            Window window = activity.getWindow();
            int i2 = 2;
            if (f1.e().a(16)) {
                i2 = 1798;
                if (Build.VERSION.SDK_INT >= 19) {
                    i2 = 5894;
                }
                if (Build.VERSION.SDK_INT >= 28) {
                    WindowManager.LayoutParams attributes = window.getAttributes();
                    attributes.layoutInDisplayCutoutMode = 1;
                    window.setAttributes(attributes);
                }
            }
            window.getDecorView().setSystemUiVisibility(i2);
        } else if ((activity.getWindow().getAttributes().flags & 1024) != 0) {
            com.chartboost.sdk.c.a.d("Chartboost", "Attempting to show Status and Navigation bars on a fullscreen activity. Please change your Chartboost activity theme to: \"@android:style/Theme.Translucent\"` in your Manifest file");
        }
    }

    public enum c {
        UNKNOWN(-1),
        NO_BEHAVIORAL(0),
        YES_BEHAVIORAL(1);
        

        /* renamed from: e  reason: collision with root package name */
        private static Map<Integer, c> f5750e = new HashMap();

        /* renamed from: a  reason: collision with root package name */
        private int f5752a;

        static {
            for (c cVar : values()) {
                f5750e.put(Integer.valueOf(cVar.f5752a), cVar);
            }
        }

        private c(int i2) {
            this.f5752a = i2;
        }

        public static c a(int i2) {
            c cVar = f5750e.get(Integer.valueOf(i2));
            return cVar == null ? UNKNOWN : cVar;
        }

        public int a() {
            return this.f5752a;
        }
    }

    public static void b(Activity activity) {
        t.a("Chartboost.onDestroy", activity);
        m e2 = m.e();
        if (e2 != null && !n.s) {
            e2.r.j(activity);
        }
    }

    public static boolean c(String str) {
        t.a("Chartboost.hasInterstitial", str);
        m e2 = m.e();
        if (e2 == null || !g.a() || e2.f5926g.a(str) == null) {
            return false;
        }
        return true;
    }

    public static boolean d(String str) {
        t.a("Chartboost.hasRewardedVideo", str);
        m e2 = m.e();
        if (e2 == null || !g.a() || e2.l.a(str) == null) {
            return false;
        }
        return true;
    }

    public static void e(String str) {
        t.a("Chartboost.showInterstitial", str);
        m e2 = m.e();
        if (e2 != null && g.a() && m.f()) {
            if (f1.e().a(str)) {
                com.chartboost.sdk.c.a.b("Chartboost", "showInterstitial location cannot be empty");
                Handler handler = e2.q;
                p0 p0Var = e2.f5927h;
                p0Var.getClass();
                handler.post(new p0.a(4, str, a.c.INVALID_LOCATION));
                return;
            }
            f fVar = e2.n.get();
            if ((!fVar.v || !fVar.w) && (!fVar.f5845e || !fVar.f5846f)) {
                Handler handler2 = e2.q;
                p0 p0Var2 = e2.f5927h;
                p0Var2.getClass();
                handler2.post(new p0.a(4, str, a.c.END_POINT_DISABLED));
                return;
            }
            r0 r0Var = e2.f5926g;
            r0Var.getClass();
            e2.f5921b.execute(new r0.c(4, str, null, null));
        }
    }

    public static void f(String str) {
        t.a("Chartboost.showRewardedVideo", str);
        m e2 = m.e();
        if (e2 != null && g.a() && m.f()) {
            if (f1.e().a(str)) {
                com.chartboost.sdk.c.a.b("Chartboost", "showRewardedVideo location cannot be empty");
                Handler handler = e2.q;
                p0 p0Var = e2.m;
                p0Var.getClass();
                handler.post(new p0.a(4, str, a.c.INVALID_LOCATION));
                return;
            }
            f fVar = e2.n.get();
            if ((!fVar.v || !fVar.z) && (!fVar.f5845e || !fVar.f5849i)) {
                Handler handler2 = e2.q;
                p0 p0Var2 = e2.m;
                p0Var2.getClass();
                handler2.post(new p0.a(4, str, a.c.END_POINT_DISABLED));
                return;
            }
            r0 r0Var = e2.l;
            r0Var.getClass();
            e2.f5921b.execute(new r0.c(4, str, null, null));
        }
    }

    public static void a(Context context, c cVar) {
        m.a(context, cVar);
    }

    public static void b(String str) {
        t.a("Chartboost.cacheRewardedVideo", str);
        m e2 = m.e();
        if (e2 != null && g.a() && m.f()) {
            if (f1.e().a(str)) {
                com.chartboost.sdk.c.a.b("Chartboost", "cacheRewardedVideo location cannot be empty");
                Handler handler = e2.q;
                p0 p0Var = e2.m;
                p0Var.getClass();
                handler.post(new p0.a(4, str, a.c.INVALID_LOCATION));
                return;
            }
            f fVar = e2.n.get();
            if ((!fVar.v || !fVar.z) && (!fVar.f5845e || !fVar.f5849i)) {
                Handler handler2 = e2.q;
                p0 p0Var2 = e2.m;
                p0Var2.getClass();
                handler2.post(new p0.a(4, str, a.c.END_POINT_DISABLED));
                return;
            }
            r0 r0Var = e2.l;
            r0Var.getClass();
            e2.f5921b.execute(new r0.c(3, str, null, null));
        }
    }

    public static void a(Activity activity) {
        t.a("Chartboost.onCreate", activity);
        m e2 = m.e();
        if (e2 != null && !n.s) {
            e2.r.b(activity);
        }
    }

    public static void a(String str) {
        t.a("Chartboost.cacheInterstitial", str);
        m e2 = m.e();
        if (e2 != null && g.a() && m.f()) {
            if (f1.e().a(str)) {
                com.chartboost.sdk.c.a.b("Chartboost", "cacheInterstitial location cannot be empty");
                Handler handler = e2.q;
                p0 p0Var = e2.f5927h;
                p0Var.getClass();
                handler.post(new p0.a(4, str, a.c.INVALID_LOCATION));
                return;
            }
            f fVar = e2.n.get();
            if ((!fVar.v || !fVar.w) && (!fVar.f5845e || !fVar.f5846f)) {
                Handler handler2 = e2.q;
                p0 p0Var2 = e2.f5927h;
                p0Var2.getClass();
                handler2.post(new p0.a(4, str, a.c.END_POINT_DISABLED));
                return;
            }
            r0 r0Var = e2.f5926g;
            r0Var.getClass();
            e2.f5921b.execute(new r0.c(3, str, null, null));
        }
    }

    public static void a(b bVar, String str) {
        t.a("Chartboost.setMediation");
        k kVar = new k(3);
        kVar.f5911d = bVar;
        kVar.f5912e = str;
        m.b(kVar);
    }

    public static void a(C0125a aVar, String str) {
        t.a("Chartboost.setFramework");
        k kVar = new k(4);
        kVar.f5910c = aVar;
        kVar.f5912e = str;
        m.b(kVar);
    }

    public static void a(a.C0126a aVar) {
        t.a("Chartboost.setLoggingLevel", aVar.toString());
        k kVar = new k(7);
        kVar.f5914g = aVar;
        m.b(kVar);
    }

    public static void a(b bVar) {
        t.a("Chartboost.setDelegate", bVar);
        k kVar = new k(8);
        kVar.f5915h = bVar;
        m.b(kVar);
    }

    public static void a(boolean z) {
        t.a("Chartboost.setAutoCacheAds", z);
        m e2 = m.e();
        if (e2 != null) {
            e2.getClass();
            m.b bVar = new m.b(1);
            bVar.f5935c = z;
            m.b(bVar);
        }
    }
}
