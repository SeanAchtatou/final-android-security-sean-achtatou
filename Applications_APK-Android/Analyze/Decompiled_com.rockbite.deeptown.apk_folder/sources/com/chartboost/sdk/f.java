package com.chartboost.sdk;

import com.chartboost.sdk.d.a;

public interface f {
    void didCacheInPlay(String str);

    void didCacheInterstitial(String str);

    void didCacheRewardedVideo(String str);

    void didClickInterstitial(String str);

    void didClickRewardedVideo(String str);

    void didCloseInterstitial(String str);

    void didCloseRewardedVideo(String str);

    void didCompleteInterstitial(String str);

    void didCompleteRewardedVideo(String str, int i2);

    void didDismissInterstitial(String str);

    void didDismissRewardedVideo(String str);

    void didDisplayInterstitial(String str);

    void didDisplayRewardedVideo(String str);

    void didFailToLoadInPlay(String str, a.c cVar);

    void didFailToLoadInterstitial(String str, a.c cVar);

    void didFailToLoadMoreApps(String str, a.c cVar);

    void didFailToLoadRewardedVideo(String str, a.c cVar);

    void didFailToRecordClick(String str, a.b bVar);

    void didInitialize();

    boolean shouldDisplayInterstitial(String str);

    boolean shouldDisplayRewardedVideo(String str);

    boolean shouldRequestInterstitial(String str);

    void willDisplayInterstitial(String str);

    void willDisplayVideo(String str);
}
