package com.chartboost.sdk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.text.TextUtils;
import com.chartboost.sdk.c.a;
import com.chartboost.sdk.d.f;
import com.chartboost.sdk.o.f1;
import com.facebook.internal.AnalyticsEvents;
import com.google.android.gms.ads.AdRequest;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

public final class g {
    static void a(String str) {
        if (n.f5942e == null) {
            a.b("CBConfig", "Set a valid CBFramework first");
        } else if (TextUtils.isEmpty(str)) {
            a.b("CBConfig", "Invalid Version String");
        } else {
            n.f5940c = str;
        }
    }

    static boolean b() {
        try {
            if (m.e() == null) {
                throw new Exception("SDK Initialization error. SDK seems to be not initialized properly, check for any integration issues");
            } else if (n.n == null) {
                throw new Exception("SDK Initialization error. Activity context seems to be not initialized properly, host activity or application context is being sent as null");
            } else if (TextUtils.isEmpty(n.l)) {
                throw new Exception("SDK Initialization error. AppId is missing");
            } else if (!TextUtils.isEmpty(n.m)) {
                return true;
            } else {
                throw new Exception("SDK Initialization error. AppSignature is missing");
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    private static boolean c() {
        m e2 = m.e();
        if (e2 == null) {
            return false;
        }
        if (e2.r.f5862f != null) {
            return true;
        }
        try {
            throw new Exception("Chartboost Weak Activity reference is null");
        } catch (Exception e3) {
            e3.printStackTrace();
            return false;
        }
    }

    public static boolean a(AtomicReference<f> atomicReference, JSONObject jSONObject, SharedPreferences sharedPreferences) {
        try {
            atomicReference.set(new f(jSONObject));
            return true;
        } catch (Exception e2) {
            com.chartboost.sdk.e.a.a(g.class, "updateConfig", e2);
            return false;
        }
    }

    public static boolean a() {
        return b() && c();
    }

    static boolean a(Activity activity) {
        if (activity != null) {
            return true;
        }
        try {
            throw new Exception("Invalid activity context: Host Activity object is null, Please send a valid activity object");
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public static boolean b(Context context) {
        List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(new Intent(context, CBImpressionActivity.class), 0);
        if (queryIntentActivities.isEmpty()) {
            return false;
        }
        ActivityInfo activityInfo = queryIntentActivities.get(0).activityInfo;
        int i2 = activityInfo.flags;
        if ((i2 & AdRequest.MAX_CONTENT_URL_LENGTH) == 0 || (i2 & 32) == 0) {
            return false;
        }
        int i3 = activityInfo.configChanges;
        if ((i3 & 128) == 0 || (i3 & 32) == 0 || (i3 & 1024) == 0) {
            return false;
        }
        return true;
    }

    public static boolean a(Context context) {
        int i2;
        int i3;
        int i4;
        int i5;
        if (context != null) {
            try {
                if (f1.e().a(23)) {
                    i5 = context.checkSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE");
                    i3 = context.checkSelfPermission("android.permission.ACCESS_NETWORK_STATE");
                    i4 = context.checkSelfPermission("android.permission.INTERNET");
                    i2 = context.checkSelfPermission("android.permission.READ_PHONE_STATE");
                } else {
                    i5 = context.checkCallingOrSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE");
                    i3 = context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE");
                    i4 = context.checkCallingOrSelfPermission("android.permission.INTERNET");
                    i2 = context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE");
                }
                n.o = i5 != 0;
                n.p = i4 != 0;
                n.q = i3 != 0;
                n.r = i2 != 0;
                if (n.p) {
                    throw new RuntimeException("Please add the permission : android.permission.INTERNET in your android manifest.xml");
                } else if (!n.q) {
                    return true;
                } else {
                    throw new RuntimeException("Please add the permission : android.permission.ACCESS_NETWORK_STATE in your android manifest.xml");
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                return false;
            }
        } else {
            throw new RuntimeException("Invalid activity context passed during intitalization");
        }
    }

    public static String a(f fVar) {
        return !fVar.v ? "native" : AnalyticsEvents.PARAMETER_SHARE_DIALOG_SHOW_WEB;
    }
}
