package com.chartboost.sdk;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.RelativeLayout;
import com.chartboost.sdk.c.g;
import com.chartboost.sdk.d.a;
import com.chartboost.sdk.d.d;
import com.esotericsoftware.spine.Animation;
import java.util.IdentityHashMap;
import java.util.Map;
import org.json.JSONObject;

public abstract class j {

    /* renamed from: a  reason: collision with root package name */
    public final Handler f5886a;

    /* renamed from: b  reason: collision with root package name */
    public final h f5887b;

    /* renamed from: c  reason: collision with root package name */
    public boolean f5888c = false;

    /* renamed from: d  reason: collision with root package name */
    private boolean f5889d;

    /* renamed from: e  reason: collision with root package name */
    protected JSONObject f5890e;

    /* renamed from: f  reason: collision with root package name */
    private b f5891f;

    /* renamed from: g  reason: collision with root package name */
    public final d f5892g;

    /* renamed from: h  reason: collision with root package name */
    protected int f5893h;

    /* renamed from: i  reason: collision with root package name */
    public final Map<View, Runnable> f5894i = new IdentityHashMap();

    /* renamed from: j  reason: collision with root package name */
    protected boolean f5895j = true;

    /* renamed from: k  reason: collision with root package name */
    protected boolean f5896k = true;

    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ boolean f5897a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ View f5898b;

        a(boolean z, View view) {
            this.f5897a = z;
            this.f5898b = view;
        }

        public void run() {
            if (!this.f5897a) {
                this.f5898b.setVisibility(8);
                this.f5898b.setClickable(false);
            }
            synchronized (j.this.f5894i) {
                j.this.f5894i.remove(this.f5898b);
            }
        }
    }

    public abstract class b extends RelativeLayout {

        /* renamed from: a  reason: collision with root package name */
        private boolean f5900a = false;

        /* renamed from: b  reason: collision with root package name */
        private int f5901b = -1;

        /* renamed from: c  reason: collision with root package name */
        private int f5902c = -1;

        /* renamed from: d  reason: collision with root package name */
        private int f5903d = -1;

        /* renamed from: e  reason: collision with root package name */
        private int f5904e = -1;

        /* renamed from: f  reason: collision with root package name */
        Integer f5905f = null;

        class a implements Runnable {
            a() {
            }

            public void run() {
                b.this.requestLayout();
            }
        }

        public b(Context context) {
            super(context);
            setFocusableInTouchMode(true);
            requestFocus();
        }

        private boolean b(int i2, int i3) {
            Integer num;
            d dVar = j.this.f5892g;
            boolean z = true;
            if (dVar != null && dVar.r.f5817b == 1) {
                return true;
            }
            if (this.f5900a) {
                return false;
            }
            int a2 = com.chartboost.sdk.c.b.a();
            if (this.f5901b == i2 && this.f5902c == i3 && (num = this.f5905f) != null && num.intValue() == a2) {
                return true;
            }
            this.f5900a = true;
            try {
                if (j.this.f5895j && com.chartboost.sdk.c.b.a(a2)) {
                    j.this.f5893h = a2;
                } else if (j.this.f5896k && com.chartboost.sdk.c.b.b(a2)) {
                    j.this.f5893h = a2;
                }
                a(i2, i3);
                post(new a());
                this.f5901b = i2;
                this.f5902c = i3;
                this.f5905f = Integer.valueOf(a2);
            } catch (Exception e2) {
                com.chartboost.sdk.c.a.a("CBViewProtocol", "Exception raised while layouting Subviews", e2);
                com.chartboost.sdk.e.a.a(getClass(), "tryLayout", e2);
                z = false;
            }
            this.f5900a = false;
            return z;
        }

        public final void a() {
            a(false);
        }

        /* access modifiers changed from: protected */
        public abstract void a(int i2, int i3);

        public void b() {
        }

        public void onDetachedFromWindow() {
            super.onDetachedFromWindow();
            synchronized (j.this.f5894i) {
                for (Runnable removeCallbacks : j.this.f5894i.values()) {
                    j.this.f5886a.removeCallbacks(removeCallbacks);
                }
                j.this.f5894i.clear();
            }
        }

        /* access modifiers changed from: protected */
        public void onSizeChanged(int i2, int i3, int i4, int i5) {
            d dVar;
            super.onSizeChanged(i2, i3, i4, i5);
            this.f5903d = i2;
            this.f5904e = i3;
            if (this.f5901b != -1 && this.f5902c != -1 && (dVar = j.this.f5892g) != null && dVar.r.f5817b == 0) {
                a();
            }
        }

        public final void a(boolean z) {
            if (z) {
                this.f5905f = null;
            }
            a((Activity) getContext());
        }

        public boolean a(Activity activity) {
            int i2;
            int i3;
            int i4;
            if (this.f5903d == -1 || this.f5904e == -1) {
                try {
                    i3 = getWidth();
                    i2 = getHeight();
                    if (i3 == 0 || i2 == 0) {
                        View findViewById = activity.getWindow().findViewById(16908290);
                        if (findViewById == null) {
                            findViewById = activity.getWindow().getDecorView();
                        }
                        int width = findViewById.getWidth();
                        i2 = findViewById.getHeight();
                        i3 = width;
                    }
                } catch (Exception unused) {
                    i3 = 0;
                    i2 = 0;
                }
                if (i3 == 0 || i2 == 0) {
                    DisplayMetrics displayMetrics = new DisplayMetrics();
                    activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                    i4 = displayMetrics.widthPixels;
                    i2 = displayMetrics.heightPixels;
                } else {
                    i4 = i3;
                }
                this.f5903d = i4;
                this.f5904e = i2;
            }
            return b(this.f5903d, this.f5904e);
        }

        public final void a(View view) {
            int i2 = 200;
            if (200 == getId()) {
                i2 = 201;
            }
            View findViewById = findViewById(i2);
            while (findViewById != null) {
                i2++;
                findViewById = findViewById(i2);
            }
            view.setId(i2);
            view.setSaveEnabled(false);
        }
    }

    public j(d dVar, Handler handler, h hVar) {
        this.f5886a = handler;
        this.f5887b = hVar;
        this.f5892g = dVar;
        this.f5891f = null;
        this.f5893h = com.chartboost.sdk.c.b.a();
        this.f5889d = false;
    }

    public static boolean b(Context context) {
        return (context.getResources().getConfiguration().screenLayout & 15) >= 4;
    }

    public int a() {
        return this.f5893h;
    }

    /* access modifiers changed from: protected */
    public abstract b a(Context context);

    public a.c c() {
        Activity b2 = this.f5887b.b();
        if (b2 == null) {
            this.f5891f = null;
            return a.c.NO_HOST_ACTIVITY;
        } else if (!this.f5896k && !this.f5895j) {
            return a.c.WRONG_ORIENTATION;
        } else {
            if (this.f5891f == null) {
                this.f5891f = a(b2);
            }
            if (this.f5892g.r.f5817b != 0 || this.f5891f.a(b2)) {
                return null;
            }
            this.f5891f = null;
            return a.c.ERROR_CREATING_VIEW;
        }
    }

    public void d() {
        f();
        synchronized (this.f5894i) {
            for (Runnable removeCallbacks : this.f5894i.values()) {
                this.f5886a.removeCallbacks(removeCallbacks);
            }
            this.f5894i.clear();
        }
    }

    public b e() {
        return this.f5891f;
    }

    public void f() {
        b bVar = this.f5891f;
        if (bVar != null) {
            bVar.b();
        }
        this.f5891f = null;
    }

    public JSONObject g() {
        return this.f5890e;
    }

    public void h() {
        if (!this.f5889d) {
            this.f5889d = true;
            this.f5892g.c();
        }
    }

    /* access modifiers changed from: protected */
    public void i() {
        this.f5892g.d();
    }

    public float j() {
        return Animation.CurveTimeline.LINEAR;
    }

    public float k() {
        return Animation.CurveTimeline.LINEAR;
    }

    public boolean l() {
        return false;
    }

    public void m() {
        if (this.f5888c) {
            this.f5888c = false;
        }
        b e2 = e();
        if (e2 == null) {
            return;
        }
        if (e2.f5905f == null || com.chartboost.sdk.c.b.a() != e2.f5905f.intValue()) {
            e2.a(false);
        }
    }

    public void n() {
        this.f5888c = true;
    }

    public boolean a(JSONObject jSONObject) {
        this.f5890e = g.a(jSONObject, "assets");
        if (this.f5890e != null) {
            return true;
        }
        this.f5890e = new JSONObject();
        com.chartboost.sdk.c.a.b("CBViewProtocol", "Media got from the response is null or empty");
        a(a.c.INVALID_RESPONSE);
        return false;
    }

    public void b() {
        i();
    }

    public boolean b(JSONObject jSONObject) {
        return this.f5892g.a(jSONObject);
    }

    public void a(a.c cVar) {
        this.f5892g.a(cVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.j.a(boolean, android.view.View, boolean):void
     arg types: [boolean, android.view.View, int]
     candidates:
      com.chartboost.sdk.j.a(android.view.View, java.lang.Runnable, long):void
      com.chartboost.sdk.j.a(boolean, android.view.View, boolean):void */
    public void a(boolean z, View view) {
        a(z, view, true);
    }

    public void a(boolean z, View view, boolean z2) {
        int i2 = 8;
        if ((z && view.getVisibility() == 0) || (!z && view.getVisibility() == 8)) {
            synchronized (this.f5894i) {
                if (!this.f5894i.containsKey(view)) {
                    return;
                }
            }
        }
        if (!z2) {
            if (z) {
                i2 = 0;
            }
            view.setVisibility(i2);
            view.setClickable(z);
            return;
        }
        a aVar = new a(z, view);
        int i3 = this.f5892g.r.f5817b;
        this.f5892g.f5838i.f5877a.a(z, view, 500);
        a(view, aVar, 500);
    }

    public void a(View view, Runnable runnable, long j2) {
        synchronized (this.f5894i) {
            Runnable runnable2 = this.f5894i.get(view);
            if (runnable2 != null) {
                this.f5886a.removeCallbacks(runnable2);
            }
            this.f5894i.put(view, runnable);
        }
        this.f5886a.postDelayed(runnable, j2);
    }

    public static int a(String str) {
        if (str != null) {
            if (!str.startsWith("#")) {
                try {
                    return Color.parseColor(str);
                } catch (IllegalArgumentException unused) {
                    str = "#" + str;
                }
            }
            if (str.length() == 4 || str.length() == 5) {
                StringBuilder sb = new StringBuilder((str.length() * 2) + 1);
                sb.append("#");
                int i2 = 0;
                while (i2 < str.length() - 1) {
                    i2++;
                    sb.append(str.charAt(i2));
                    sb.append(str.charAt(i2));
                }
                str = sb.toString();
            }
            try {
                return Color.parseColor(str);
            } catch (IllegalArgumentException e2) {
                com.chartboost.sdk.c.a.c("CBViewProtocol", "error parsing color " + str, e2);
            }
        }
        return 0;
    }
}
