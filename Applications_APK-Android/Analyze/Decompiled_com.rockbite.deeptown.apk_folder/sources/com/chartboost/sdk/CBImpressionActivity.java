package com.chartboost.sdk;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.chartboost.sdk.d.a;
import com.chartboost.sdk.d.d;
import com.chartboost.sdk.o.f1;

@SuppressLint({"Registered"})
public class CBImpressionActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    final com.chartboost.sdk.e.a f5720a;

    /* renamed from: b  reason: collision with root package name */
    final Handler f5721b;

    /* renamed from: c  reason: collision with root package name */
    final h f5722c;

    /* renamed from: d  reason: collision with root package name */
    private Activity f5723d;

    class a implements Runnable {
        a() {
        }

        public void run() {
            try {
                com.chartboost.sdk.c.a.e("VideoInit", "preparing activity for video surface");
                CBImpressionActivity.this.addContentView(new SurfaceView(CBImpressionActivity.this), new ViewGroup.LayoutParams(0, 0));
            } catch (Exception e2) {
                com.chartboost.sdk.e.a.a(CBImpressionActivity.class, "postCreateSurfaceView Runnable.run", e2);
            }
        }
    }

    public CBImpressionActivity() {
        this.f5720a = m.e() != null ? m.e().p : null;
        this.f5721b = m.e() != null ? m.e().q : null;
        this.f5722c = m.e() != null ? m.e().r : null;
        this.f5723d = null;
    }

    private void b() {
        if (!f1.e().a(14)) {
            this.f5721b.post(new a());
        }
    }

    public void a(Activity activity) {
        this.f5723d = activity;
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        Activity activity = this.f5723d;
        if (activity != null) {
            return activity.dispatchTouchEvent(motionEvent);
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public void onAttachedToWindow() {
        try {
            super.onAttachedToWindow();
            if (f1.e().a(14) && getWindow() != null && getWindow().getDecorView() != null && !getWindow().getDecorView().isHardwareAccelerated() && this.f5722c != null) {
                com.chartboost.sdk.c.a.b("CBImpressionActivity", "The activity passed down is not hardware accelerated, so Chartboost cannot show ads");
                d d2 = this.f5722c.d();
                if (d2 != null) {
                    d2.a(a.c.HARDWARE_ACCELERATION_DISABLED);
                }
                finish();
            }
        } catch (Exception e2) {
            com.chartboost.sdk.e.a.a(CBImpressionActivity.class, "onAttachedToWindow", e2);
        }
    }

    public void onBackPressed() {
        try {
            if (this.f5722c == null || !this.f5722c.k()) {
                super.onBackPressed();
            }
        } catch (Exception e2) {
            com.chartboost.sdk.e.a.a(CBImpressionActivity.class, "onBackPressed", e2);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if ((getIntent() != null && !getIntent().getBooleanExtra("isChartboost", false)) || this.f5720a == null || this.f5721b == null || this.f5722c == null) {
            com.chartboost.sdk.c.a.b("CBImpressionActivity", "This activity cannot be called from outside chartboost SDK");
            finish();
            return;
        }
        a();
        requestWindowFeature(1);
        getWindow().setWindowAnimations(0);
        this.f5722c.a(this);
        setContentView(new RelativeLayout(this));
        b();
        com.chartboost.sdk.c.a.a("CBImpressionActivity", "Impression Activity onCreate() called");
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        try {
            if (this.f5722c != null && !n.s) {
                this.f5722c.k(this);
            }
            super.onDestroy();
        } catch (Exception e2) {
            com.chartboost.sdk.e.a.a(CBImpressionActivity.class, "onDestroy", e2);
        } catch (Throwable th) {
            super.onDestroy();
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        try {
            super.onPause();
            if (this.f5722c != null && !n.s) {
                this.f5722c.a((Activity) this);
                this.f5722c.i();
            }
        } catch (Exception e2) {
            com.chartboost.sdk.e.a.a(CBImpressionActivity.class, "onPause", e2);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        try {
            super.onResume();
            if (this.f5722c != null && !n.s) {
                this.f5722c.a((Activity) this);
                this.f5722c.h();
            }
        } catch (Exception e2) {
            com.chartboost.sdk.e.a.a(CBImpressionActivity.class, "onResume", e2);
        }
        a.g(this);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        try {
            super.onStart();
            if (this.f5722c != null && !n.s) {
                this.f5722c.e(this);
            }
        } catch (Exception e2) {
            com.chartboost.sdk.e.a.a(CBImpressionActivity.class, "onStart", e2);
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        try {
            super.onStop();
            if (this.f5722c != null && !n.s) {
                this.f5722c.i(this);
            }
        } catch (Exception e2) {
            com.chartboost.sdk.e.a.a(CBImpressionActivity.class, "onStop", e2);
        }
    }

    @TargetApi(11)
    private void a() {
        if (f1.e().a(11)) {
            getWindow().setFlags(16777216, 16777216);
        }
    }
}
