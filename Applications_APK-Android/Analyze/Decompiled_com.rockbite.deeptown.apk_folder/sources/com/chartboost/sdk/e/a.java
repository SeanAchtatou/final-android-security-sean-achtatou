package com.chartboost.sdk.e;

import android.text.TextUtils;
import com.chartboost.sdk.c.d;
import com.chartboost.sdk.d.f;
import com.chartboost.sdk.g;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.tapjoy.TJAdUnitConstants;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

public class a implements d {

    /* renamed from: f  reason: collision with root package name */
    private static a f5852f;

    /* renamed from: g  reason: collision with root package name */
    private static final Long f5853g = Long.valueOf(TimeUnit.MINUTES.toMillis(5));

    /* renamed from: c  reason: collision with root package name */
    private final AtomicReference<f> f5854c;

    /* renamed from: d  reason: collision with root package name */
    private boolean f5855d = false;

    /* renamed from: e  reason: collision with root package name */
    private long f5856e = (System.currentTimeMillis() - f5853g.longValue());

    public a(AtomicReference<f> atomicReference) {
        f5852f = this;
        this.f5854c = atomicReference;
    }

    private void c() {
        if (this.f5854c.get().l) {
            a("session", "end", null, null, null, null, null, false);
            a("did-become-active");
        }
    }

    public void a() {
        a(TJAdUnitConstants.String.VIDEO_START);
        a("did-become-active");
    }

    public void b() {
        c();
    }

    public void d(String str, String str2, String str3) {
        if (this.f5854c.get().m) {
            a("ad-close", str, str2, str3, null, false);
        }
    }

    public void e(String str, String str2, String str3) {
        if (this.f5854c.get().m) {
            a("ad-dismiss", str, str2, str3, null, false);
        }
    }

    public void b(String str, String str2, String str3) {
        if (this.f5854c.get().m) {
            a("ad-show", str, str2, str3, null, false);
        }
    }

    private void a(String str) {
        if (this.f5854c.get().l) {
            a("session", str, null, null, null, false);
        }
    }

    public void d(String str, String str2) {
        if (this.f5854c.get().m) {
            a("playback-start", str, str2, null, null, false);
        }
    }

    public void e(String str, String str2) {
        if (this.f5854c.get().m) {
            a("playback-stop", str, str2, null, null, false);
        }
    }

    public void b(String str, String str2, String str3, String str4) {
        if (this.f5854c.get().m) {
            if (TextUtils.isEmpty(str3)) {
                str3 = "empty-adid";
            }
            a("ad-warning", str, str2, str3, str4, false);
        }
    }

    public void c(String str, String str2, String str3) {
        if (this.f5854c.get().m) {
            a("ad-click", str, str2, str3, null, false);
        }
    }

    public void a(String str, String str2, String str3, String str4) {
        if (this.f5854c.get().m) {
            a("webview-track", str, str2, str3, str4, null, null, false);
        }
    }

    public void c(String str, String str2) {
        if (this.f5854c.get().m) {
            a("replay", str, str2, null, null, false);
        }
    }

    public void a(JSONObject jSONObject) {
        f fVar = this.f5854c.get();
        if (fVar.m) {
            a("folder", g.a(fVar), null, null, null, null, jSONObject, false);
        }
    }

    public void b(String str, String str2) {
        if (this.f5854c.get().m) {
            a("playback-complete", str, str2, null, null, false);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0065, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void b(java.lang.Class r15, java.lang.String r16, java.lang.Exception r17) {
        /*
            r14 = this;
            r10 = r14
            monitor-enter(r14)
            java.util.concurrent.atomic.AtomicReference<com.chartboost.sdk.d.f> r0 = r10.f5854c     // Catch:{ all -> 0x0066 }
            if (r0 != 0) goto L_0x0008
            monitor-exit(r14)
            return
        L_0x0008:
            java.util.concurrent.atomic.AtomicReference<com.chartboost.sdk.d.f> r0 = r10.f5854c     // Catch:{ all -> 0x0066 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x0066 }
            com.chartboost.sdk.d.f r0 = (com.chartboost.sdk.d.f) r0     // Catch:{ all -> 0x0066 }
            if (r0 == 0) goto L_0x0064
            boolean r1 = r0.f5850j     // Catch:{ all -> 0x0066 }
            if (r1 == 0) goto L_0x0064
            boolean r1 = r10.f5855d     // Catch:{ all -> 0x0066 }
            if (r1 != 0) goto L_0x0064
            r1 = 1
            r10.f5855d = r1     // Catch:{ all -> 0x0066 }
            r11 = 0
            long r12 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x005c }
            long r1 = r10.f5856e     // Catch:{ Exception -> 0x005c }
            long r1 = r12 - r1
            java.lang.Long r3 = com.chartboost.sdk.e.a.f5853g     // Catch:{ Exception -> 0x005c }
            long r3 = r3.longValue()     // Catch:{ Exception -> 0x005c }
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 < 0) goto L_0x0057
            boolean r0 = r0.o     // Catch:{ Exception -> 0x005c }
            if (r0 == 0) goto L_0x0039
            java.lang.String r0 = android.util.Log.getStackTraceString(r17)     // Catch:{ Exception -> 0x005c }
            goto L_0x003a
        L_0x0039:
            r0 = 0
        L_0x003a:
            r7 = r0
            java.lang.String r2 = "exception"
            java.lang.String r3 = r15.getName()     // Catch:{ Exception -> 0x005c }
            java.lang.Class r0 = r17.getClass()     // Catch:{ Exception -> 0x005c }
            java.lang.String r5 = r0.getName()     // Catch:{ Exception -> 0x005c }
            java.lang.String r6 = r17.getMessage()     // Catch:{ Exception -> 0x005c }
            r8 = 0
            r9 = 1
            r1 = r14
            r4 = r16
            r1.a(r2, r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x005c }
            r10.f5856e = r12     // Catch:{ Exception -> 0x005c }
        L_0x0057:
            r10.f5855d = r11     // Catch:{ all -> 0x0066 }
            goto L_0x0064
        L_0x005a:
            r0 = move-exception
            goto L_0x0061
        L_0x005c:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x005a }
            goto L_0x0057
        L_0x0061:
            r10.f5855d = r11     // Catch:{ all -> 0x0066 }
            throw r0     // Catch:{ all -> 0x0066 }
        L_0x0064:
            monitor-exit(r14)
            return
        L_0x0066:
            r0 = move-exception
            monitor-exit(r14)
            goto L_0x006a
        L_0x0069:
            throw r0
        L_0x006a:
            goto L_0x0069
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.e.a.b(java.lang.Class, java.lang.String, java.lang.Exception):void");
    }

    public void a(String str, String str2, String str3) {
        if (this.f5854c.get().m) {
            a("load", str, str2, str3, null, false);
        }
    }

    public void a(String str, String str2, String str3, String str4, boolean z) {
        if (this.f5854c.get().m) {
            if (TextUtils.isEmpty(str3)) {
                str3 = "empty-adid";
            }
            a("ad-error", str, str2, str3, str4, z);
        }
    }

    public void a(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        if (this.f5854c.get().f5851k) {
            String str8 = str;
            String str9 = str2;
            String str10 = str3;
            String str11 = str4;
            a("ad-unit-error", str8, str9, str10, str11, null, com.chartboost.sdk.c.g.a(com.chartboost.sdk.c.g.a("adId", str5), com.chartboost.sdk.c.g.a((String) FirebaseAnalytics.Param.LOCATION, str6), com.chartboost.sdk.c.g.a("state", str7)), true);
        }
    }

    public void a(String str, String str2) {
        f fVar = this.f5854c.get();
        if (fVar.m) {
            a("download-asset-start", g.a(fVar), str, str2, null, null, null, false);
        }
    }

    public void a(String str, String str2, long j2, long j3, long j4) {
        if (this.f5854c.get().n) {
            String str3 = str;
            String str4 = str2;
            a("download-asset-failure", str3, str4, null, null, null, com.chartboost.sdk.c.g.a(com.chartboost.sdk.c.g.a("processingMs", Long.valueOf(j2)), com.chartboost.sdk.c.g.a("getResponseCodeMs", Long.valueOf(j3)), com.chartboost.sdk.c.g.a("readDataMs", Long.valueOf(j4))), false);
        }
    }

    public void a(String str, long j2, long j3, long j4) {
        if (this.f5854c.get().n) {
            String str2 = str;
            a("download-asset-success", str2, null, null, null, null, com.chartboost.sdk.c.g.a(com.chartboost.sdk.c.g.a("processingMs", Long.valueOf(j2)), com.chartboost.sdk.c.g.a("getResposeCodeMs", Long.valueOf(j3)), com.chartboost.sdk.c.g.a("readDataMs", Long.valueOf(j4))), false);
        }
    }

    public static void a(Class cls, String str, Exception exc) {
        exc.printStackTrace();
        a aVar = f5852f;
        if (aVar != null) {
            aVar.b(cls, str, exc);
        }
    }

    public void a(String str, String str2, String str3, String str4, String str5, String str6, JSONObject jSONObject) {
        if (this.f5854c.get().m) {
            a(str, str2, str3, str4, str5, str6, jSONObject, false);
        }
    }

    private void a(String str, String str2, String str3, String str4, String str5, boolean z) {
        a(str, str2, str3, str4, str5, null, new JSONObject(), z);
    }

    public void a(String str, String str2, String str3, String str4, String str5, String str6, JSONObject jSONObject, boolean z) {
        if (this.f5854c.get().m) {
            if (str == null) {
                str = "unknown event";
            }
            com.chartboost.sdk.c.a.a("CBTrack", str);
        }
    }
}
