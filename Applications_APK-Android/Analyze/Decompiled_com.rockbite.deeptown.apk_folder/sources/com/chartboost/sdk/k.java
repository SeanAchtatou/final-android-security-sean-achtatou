package com.chartboost.sdk;

import android.app.Activity;
import com.chartboost.sdk.a;
import com.chartboost.sdk.c.a;

class k implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final int f5908a;

    /* renamed from: b  reason: collision with root package name */
    boolean f5909b = false;

    /* renamed from: c  reason: collision with root package name */
    a.C0125a f5910c = null;

    /* renamed from: d  reason: collision with root package name */
    a.b f5911d = null;

    /* renamed from: e  reason: collision with root package name */
    String f5912e = null;

    /* renamed from: f  reason: collision with root package name */
    String f5913f = null;

    /* renamed from: g  reason: collision with root package name */
    a.C0126a f5914g = null;

    /* renamed from: h  reason: collision with root package name */
    b f5915h = null;

    /* renamed from: i  reason: collision with root package name */
    Activity f5916i = null;

    /* renamed from: j  reason: collision with root package name */
    String f5917j = null;

    /* renamed from: k  reason: collision with root package name */
    String f5918k = null;

    k(int i2) {
        this.f5908a = i2;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:81:?, code lost:
        return;
     */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x011a A[Catch:{ all -> 0x0114 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r10 = this;
            int r0 = r10.f5908a     // Catch:{ Exception -> 0x0134 }
            switch(r0) {
                case 0: goto L_0x0083;
                case 1: goto L_0x007d;
                case 2: goto L_0x0005;
                case 3: goto L_0x0059;
                case 4: goto L_0x002e;
                case 5: goto L_0x0027;
                case 6: goto L_0x0021;
                case 7: goto L_0x0014;
                case 8: goto L_0x0007;
                default: goto L_0x0005;
            }     // Catch:{ Exception -> 0x0134 }
        L_0x0005:
            goto L_0x0152
        L_0x0007:
            com.chartboost.sdk.b r0 = r10.f5915h     // Catch:{ Exception -> 0x0134 }
            com.chartboost.sdk.n.f5941d = r0     // Catch:{ Exception -> 0x0134 }
            java.lang.String r0 = "SdkSettings.assignDelegate"
            com.chartboost.sdk.b r1 = r10.f5915h     // Catch:{ Exception -> 0x0134 }
            com.chartboost.sdk.o.t.a(r0, r1)     // Catch:{ Exception -> 0x0134 }
            goto L_0x0152
        L_0x0014:
            boolean r0 = com.chartboost.sdk.g.b()     // Catch:{ Exception -> 0x0134 }
            if (r0 != 0) goto L_0x001b
            return
        L_0x001b:
            com.chartboost.sdk.c.a$a r0 = r10.f5914g     // Catch:{ Exception -> 0x0134 }
            com.chartboost.sdk.c.a.f5753a = r0     // Catch:{ Exception -> 0x0134 }
            goto L_0x0152
        L_0x0021:
            java.lang.String r0 = r10.f5913f     // Catch:{ Exception -> 0x0134 }
            com.chartboost.sdk.n.f5939b = r0     // Catch:{ Exception -> 0x0134 }
            goto L_0x0152
        L_0x0027:
            java.lang.String r0 = r10.f5912e     // Catch:{ Exception -> 0x0134 }
            com.chartboost.sdk.g.a(r0)     // Catch:{ Exception -> 0x0134 }
            goto L_0x0152
        L_0x002e:
            com.chartboost.sdk.a$a r0 = r10.f5910c     // Catch:{ Exception -> 0x0134 }
            if (r0 != 0) goto L_0x003a
            java.lang.String r0 = "ChartboostCommand"
            java.lang.String r1 = "Pass a valid CBFramework enum value"
            com.chartboost.sdk.c.a.b(r0, r1)     // Catch:{ Exception -> 0x0134 }
            return
        L_0x003a:
            com.chartboost.sdk.a$a r0 = r10.f5910c     // Catch:{ Exception -> 0x0134 }
            com.chartboost.sdk.n.f5942e = r0     // Catch:{ Exception -> 0x0134 }
            java.lang.String r0 = r10.f5912e     // Catch:{ Exception -> 0x0134 }
            com.chartboost.sdk.n.f5943f = r0     // Catch:{ Exception -> 0x0134 }
            java.lang.String r0 = "%s %s"
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x0134 }
            r2 = 0
            com.chartboost.sdk.a$a r3 = r10.f5910c     // Catch:{ Exception -> 0x0134 }
            r1[r2] = r3     // Catch:{ Exception -> 0x0134 }
            r2 = 1
            java.lang.String r3 = r10.f5912e     // Catch:{ Exception -> 0x0134 }
            r1[r2] = r3     // Catch:{ Exception -> 0x0134 }
            java.lang.String r0 = java.lang.String.format(r0, r1)     // Catch:{ Exception -> 0x0134 }
            com.chartboost.sdk.n.f5944g = r0     // Catch:{ Exception -> 0x0134 }
            goto L_0x0152
        L_0x0059:
            com.chartboost.sdk.a$b r0 = r10.f5911d     // Catch:{ Exception -> 0x0134 }
            com.chartboost.sdk.n.f5947j = r0     // Catch:{ Exception -> 0x0134 }
            java.lang.String r0 = r10.f5912e     // Catch:{ Exception -> 0x0134 }
            com.chartboost.sdk.n.f5948k = r0     // Catch:{ Exception -> 0x0134 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0134 }
            r0.<init>()     // Catch:{ Exception -> 0x0134 }
            com.chartboost.sdk.a$b r1 = com.chartboost.sdk.n.f5947j     // Catch:{ Exception -> 0x0134 }
            r0.append(r1)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r1 = " "
            r0.append(r1)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r1 = com.chartboost.sdk.n.f5948k     // Catch:{ Exception -> 0x0134 }
            r0.append(r1)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0134 }
            com.chartboost.sdk.n.f5946i = r0     // Catch:{ Exception -> 0x0134 }
            goto L_0x0152
        L_0x007d:
            boolean r0 = r10.f5909b     // Catch:{ Exception -> 0x0134 }
            com.chartboost.sdk.n.u = r0     // Catch:{ Exception -> 0x0134 }
            goto L_0x0152
        L_0x0083:
            com.chartboost.sdk.m r0 = com.chartboost.sdk.m.e()     // Catch:{ Exception -> 0x0134 }
            if (r0 != 0) goto L_0x0152
            java.lang.Class<com.chartboost.sdk.m> r0 = com.chartboost.sdk.m.class
            monitor-enter(r0)     // Catch:{ Exception -> 0x0134 }
            com.chartboost.sdk.m r1 = com.chartboost.sdk.m.e()     // Catch:{ all -> 0x0131 }
            if (r1 != 0) goto L_0x012f
            android.app.Activity r1 = r10.f5916i     // Catch:{ all -> 0x0131 }
            if (r1 != 0) goto L_0x009f
            java.lang.String r1 = "ChartboostCommand"
            java.lang.String r2 = "Activity object is null. Please pass a valid activity object"
            com.chartboost.sdk.c.a.b(r1, r2)     // Catch:{ all -> 0x0131 }
            monitor-exit(r0)     // Catch:{ all -> 0x0131 }
            return
        L_0x009f:
            android.app.Activity r1 = r10.f5916i     // Catch:{ all -> 0x0131 }
            boolean r1 = com.chartboost.sdk.g.a(r1)     // Catch:{ all -> 0x0131 }
            if (r1 != 0) goto L_0x00b0
            java.lang.String r1 = "ChartboostCommand"
            java.lang.String r2 = "Permissions not set correctly"
            com.chartboost.sdk.c.a.b(r1, r2)     // Catch:{ all -> 0x0131 }
            monitor-exit(r0)     // Catch:{ all -> 0x0131 }
            return
        L_0x00b0:
            android.app.Activity r1 = r10.f5916i     // Catch:{ all -> 0x0131 }
            boolean r1 = com.chartboost.sdk.g.b(r1)     // Catch:{ all -> 0x0131 }
            if (r1 != 0) goto L_0x00bf
            java.lang.String r1 = "ChartboostCommand"
            java.lang.String r2 = "Please add CBImpressionActivity in AndroidManifest.xml following README.md instructions."
            com.chartboost.sdk.c.a.b(r1, r2)     // Catch:{ all -> 0x0131 }
        L_0x00bf:
            java.lang.String r1 = r10.f5917j     // Catch:{ all -> 0x0131 }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ all -> 0x0131 }
            if (r1 != 0) goto L_0x0126
            java.lang.String r1 = r10.f5918k     // Catch:{ all -> 0x0131 }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ all -> 0x0131 }
            if (r1 == 0) goto L_0x00d0
            goto L_0x0126
        L_0x00d0:
            com.chartboost.sdk.o.f1 r6 = com.chartboost.sdk.o.f1.e()     // Catch:{ all -> 0x0131 }
            com.chartboost.sdk.l r1 = com.chartboost.sdk.l.a()     // Catch:{ all -> 0x0131 }
            android.os.Handler r8 = r6.f6009a     // Catch:{ all -> 0x0131 }
            com.chartboost.sdk.o.b1.a()     // Catch:{ all -> 0x0131 }
            r2 = 0
            java.util.concurrent.ScheduledExecutorService r3 = com.chartboost.sdk.o.f.a()     // Catch:{ all -> 0x0117 }
            r1.a(r3)     // Catch:{ all -> 0x0117 }
            r7 = r3
            java.util.concurrent.ScheduledExecutorService r7 = (java.util.concurrent.ScheduledExecutorService) r7     // Catch:{ all -> 0x0117 }
            r2 = 4
            java.util.concurrent.ExecutorService r2 = com.chartboost.sdk.o.f.a(r2)     // Catch:{ all -> 0x0114 }
            r1.a(r2)     // Catch:{ all -> 0x0114 }
            r9 = r2
            java.util.concurrent.ExecutorService r9 = (java.util.concurrent.ExecutorService) r9     // Catch:{ all -> 0x0114 }
            com.chartboost.sdk.m r1 = new com.chartboost.sdk.m     // Catch:{ all -> 0x0131 }
            android.app.Activity r3 = r10.f5916i     // Catch:{ all -> 0x0131 }
            java.lang.String r4 = r10.f5917j     // Catch:{ all -> 0x0131 }
            java.lang.String r5 = r10.f5918k     // Catch:{ all -> 0x0131 }
            r2 = r1
            r2.<init>(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ all -> 0x0131 }
            com.chartboost.sdk.m.a(r1)     // Catch:{ all -> 0x0131 }
            com.chartboost.sdk.o.y0 r2 = r1.f5922c     // Catch:{ all -> 0x0131 }
            r2.c()     // Catch:{ all -> 0x0131 }
            com.chartboost.sdk.m$b r2 = new com.chartboost.sdk.m$b     // Catch:{ all -> 0x0131 }
            r1.getClass()     // Catch:{ all -> 0x0131 }
            r3 = 3
            r2.<init>(r3)     // Catch:{ all -> 0x0131 }
            r1.a(r2)     // Catch:{ all -> 0x0131 }
            goto L_0x012f
        L_0x0114:
            r1 = move-exception
            r2 = r7
            goto L_0x0118
        L_0x0117:
            r1 = move-exception
        L_0x0118:
            if (r2 == 0) goto L_0x011d
            r2.shutdown()     // Catch:{ all -> 0x0131 }
        L_0x011d:
            java.lang.String r2 = "ChartboostCommand"
            java.lang.String r3 = "Unable to start threads"
            com.chartboost.sdk.c.a.a(r2, r3, r1)     // Catch:{ all -> 0x0131 }
            monitor-exit(r0)     // Catch:{ all -> 0x0131 }
            return
        L_0x0126:
            java.lang.String r1 = "ChartboostCommand"
            java.lang.String r2 = "AppId or AppSignature is null. Please pass a valid id's"
            com.chartboost.sdk.c.a.b(r1, r2)     // Catch:{ all -> 0x0131 }
            monitor-exit(r0)     // Catch:{ all -> 0x0131 }
            return
        L_0x012f:
            monitor-exit(r0)     // Catch:{ all -> 0x0131 }
            goto L_0x0152
        L_0x0131:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0131 }
            throw r1     // Catch:{ Exception -> 0x0134 }
        L_0x0134:
            r0 = move-exception
            java.lang.Class<com.chartboost.sdk.k> r1 = com.chartboost.sdk.k.class
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "run ("
            r2.append(r3)
            int r3 = r10.f5908a
            r2.append(r3)
            java.lang.String r3 = ")"
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.chartboost.sdk.e.a.a(r1, r2, r0)
        L_0x0152:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.k.run():void");
    }
}
