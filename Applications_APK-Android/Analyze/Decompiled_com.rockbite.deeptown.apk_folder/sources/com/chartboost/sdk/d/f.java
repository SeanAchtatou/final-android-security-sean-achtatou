package com.chartboost.sdk.d;

import android.os.Build;
import com.chartboost.sdk.a;
import com.chartboost.sdk.c.d;
import com.chartboost.sdk.n;
import com.chartboost.sdk.o.b1;
import com.chartboost.sdk.o.f1;
import com.facebook.internal.ServerProtocol;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TapjoyConstants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONObject;

public class f {
    public final String A;
    public final String B;
    public final boolean C;
    public final boolean D;
    public final boolean E;

    /* renamed from: a  reason: collision with root package name */
    public final String f5841a;

    /* renamed from: b  reason: collision with root package name */
    public final boolean f5842b;

    /* renamed from: c  reason: collision with root package name */
    public final boolean f5843c;

    /* renamed from: d  reason: collision with root package name */
    public final List<String> f5844d;

    /* renamed from: e  reason: collision with root package name */
    public final boolean f5845e;

    /* renamed from: f  reason: collision with root package name */
    public final boolean f5846f;

    /* renamed from: g  reason: collision with root package name */
    public final boolean f5847g;

    /* renamed from: h  reason: collision with root package name */
    public final int f5848h;

    /* renamed from: i  reason: collision with root package name */
    public final boolean f5849i;

    /* renamed from: j  reason: collision with root package name */
    public final boolean f5850j;

    /* renamed from: k  reason: collision with root package name */
    public final boolean f5851k;
    public final boolean l;
    public final boolean m;
    public final boolean n;
    public final boolean o;
    public final long p;
    public final int q;
    public final int r;
    public final int s;
    public final int t;
    public final List<String> u;
    public final boolean v;
    public final boolean w;
    public final boolean x;
    public final int y;
    public final boolean z;

    public f(JSONObject jSONObject) {
        boolean z2;
        JSONObject optJSONObject;
        JSONObject jSONObject2 = jSONObject;
        this.f5841a = jSONObject2.optString("configVariant");
        this.f5842b = jSONObject2.optBoolean("prefetchDisable");
        this.f5843c = jSONObject2.optBoolean("publisherDisable");
        ArrayList arrayList = new ArrayList();
        JSONArray optJSONArray = jSONObject2.optJSONArray("invalidateFolderList");
        if (optJSONArray != null) {
            int length = optJSONArray.length();
            for (int i2 = 0; i2 < length; i2++) {
                String optString = optJSONArray.optString(i2);
                if (!optString.isEmpty()) {
                    arrayList.add(optString);
                }
            }
        }
        this.f5844d = Collections.unmodifiableList(arrayList);
        JSONObject optJSONObject2 = jSONObject2.optJSONObject("native");
        optJSONObject2 = optJSONObject2 == null ? new JSONObject() : optJSONObject2;
        boolean z3 = true;
        this.f5845e = optJSONObject2.optBoolean(TJAdUnitConstants.String.ENABLED, true);
        optJSONObject2.optBoolean("inplayEnabled", true);
        this.f5846f = optJSONObject2.optBoolean("interstitialEnabled", true);
        this.f5847g = optJSONObject2.optBoolean("lockOrientation");
        this.f5848h = optJSONObject2.optInt("prefetchSession", 3);
        this.f5849i = optJSONObject2.optBoolean("rewardVideoEnabled", true);
        JSONObject optJSONObject3 = jSONObject2.optJSONObject("trackingLevels");
        optJSONObject3 = optJSONObject3 == null ? new JSONObject() : optJSONObject3;
        this.f5850j = optJSONObject3.optBoolean("critical", true);
        this.o = optJSONObject3.optBoolean("includeStackTrace", true);
        this.f5851k = optJSONObject3.optBoolean("error");
        optJSONObject3.optBoolean(TapjoyConstants.TJC_DEBUG);
        this.l = optJSONObject3.optBoolean("session");
        this.m = optJSONObject3.optBoolean("system");
        this.n = optJSONObject3.optBoolean("timing");
        optJSONObject3.optBoolean("user");
        this.p = jSONObject2.optLong("getAdRetryBaseMs", d.f5761b);
        this.q = jSONObject2.optInt("getAdRetryMaxBackoffExponent", 5);
        JSONObject optJSONObject4 = jSONObject2.optJSONObject("webview");
        optJSONObject4 = optJSONObject4 == null ? new JSONObject() : optJSONObject4;
        boolean z4 = !"Amazon".equalsIgnoreCase(Build.MANUFACTURER) || Build.VERSION.SDK_INT >= 21;
        this.r = optJSONObject4.optInt("cacheMaxBytes", 104857600);
        int i3 = 10;
        int optInt = optJSONObject4.optInt("cacheMaxUnits", 10);
        this.s = optInt > 0 ? optInt : i3;
        String str = "rewardVideoEnabled";
        this.t = (int) TimeUnit.SECONDS.toDays((long) optJSONObject4.optInt("cacheTTLs", d.f5760a));
        ArrayList arrayList2 = new ArrayList();
        JSONArray optJSONArray2 = optJSONObject4.optJSONArray("directories");
        if (optJSONArray2 != null) {
            int length2 = optJSONArray2.length();
            for (int i4 = 0; i4 < length2; i4++) {
                String optString2 = optJSONArray2.optString(i4);
                if (!optString2.isEmpty()) {
                    arrayList2.add(optString2);
                }
            }
        }
        this.u = Collections.unmodifiableList(arrayList2);
        this.v = z4 && optJSONObject4.optBoolean(TJAdUnitConstants.String.ENABLED, a());
        optJSONObject4.optBoolean("inplayEnabled", true);
        this.w = optJSONObject4.optBoolean("interstitialEnabled", true);
        int optInt2 = optJSONObject4.optInt("invalidatePendingImpression", 3);
        this.x = optJSONObject4.optBoolean("lockOrientation", true);
        this.y = optJSONObject4.optInt("prefetchSession", 3);
        this.z = optJSONObject4.optBoolean(str, true);
        this.A = optJSONObject4.optString(ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION, "v2");
        String.format("%s/%s%s", "webview", this.A, "/interstitial/get");
        this.B = String.format("%s/%s/%s", "webview", this.A, "prefetch");
        String.format("%s/%s%s", "webview", this.A, "/reward/get");
        ArrayList arrayList3 = new ArrayList();
        boolean z5 = n.x != a.c.NO_BEHAVIORAL;
        z3 = n.x == a.c.NO_BEHAVIORAL ? false : z3;
        JSONObject optJSONObject5 = jSONObject2.optJSONObject("certificationProviders");
        if (optJSONObject5 == null || (optJSONObject = optJSONObject5.optJSONObject("moat")) == null) {
            z2 = false;
        } else {
            arrayList3.add("moat");
            z2 = optJSONObject.optBoolean("loggingEnabled", false);
            z5 = optJSONObject.optBoolean("locationEnabled", z5);
            z3 = optJSONObject.optBoolean("idfaCollectionEnabled", z3);
        }
        this.C = z2;
        this.D = z5;
        this.E = z3;
        b1.a(arrayList3);
    }

    private static boolean a() {
        int[] iArr = {4, 4, 2};
        String c2 = f1.e().c();
        if (c2 != null && c2.length() > 0) {
            String[] split = c2.replaceAll("[^\\d.]", "").split("\\.");
            int i2 = 0;
            while (i2 < split.length && i2 < iArr.length) {
                try {
                    if (Integer.valueOf(split[i2]).intValue() > iArr[i2]) {
                        return true;
                    }
                    if (Integer.valueOf(split[i2]).intValue() < iArr[i2]) {
                        return false;
                    }
                    i2++;
                } catch (NumberFormatException unused) {
                }
            }
        }
        return false;
    }
}
