package com.chartboost.sdk.d;

import android.content.SharedPreferences;
import android.os.Handler;
import com.applovin.mediation.AppLovinNativeAdapter;
import com.chartboost.sdk.c.h;
import com.chartboost.sdk.d.a;
import com.chartboost.sdk.e.a;
import com.chartboost.sdk.f;
import com.chartboost.sdk.i;
import com.chartboost.sdk.j;
import com.chartboost.sdk.o.f1;
import com.chartboost.sdk.o.g0;
import com.chartboost.sdk.o.i1;
import com.chartboost.sdk.o.j0;
import com.chartboost.sdk.o.j1;
import com.chartboost.sdk.o.k;
import com.chartboost.sdk.o.m;
import com.chartboost.sdk.o.n;
import com.chartboost.sdk.o.o;
import com.chartboost.sdk.o.p0;
import com.chartboost.sdk.o.s;
import com.esotericsoftware.spine.Animation;
import com.facebook.internal.AnalyticsEvents;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.tapjoy.TJAdUnitConstants;
import java.util.Locale;
import org.json.JSONObject;

public class d {
    public boolean A = false;
    public boolean B = false;

    /* renamed from: a  reason: collision with root package name */
    public final p0 f5830a;

    /* renamed from: b  reason: collision with root package name */
    public final h f5831b;

    /* renamed from: c  reason: collision with root package name */
    public final k f5832c;

    /* renamed from: d  reason: collision with root package name */
    public final s f5833d;

    /* renamed from: e  reason: collision with root package name */
    public final a f5834e;

    /* renamed from: f  reason: collision with root package name */
    public final Handler f5835f;

    /* renamed from: g  reason: collision with root package name */
    public final com.chartboost.sdk.h f5836g;

    /* renamed from: h  reason: collision with root package name */
    public final n f5837h;

    /* renamed from: i  reason: collision with root package name */
    public final i f5838i;

    /* renamed from: j  reason: collision with root package name */
    public final o f5839j;

    /* renamed from: k  reason: collision with root package name */
    public final e f5840k;
    public int l;
    public final String m;
    public int n;
    private boolean o;
    private Boolean p = null;
    public final String q;
    public final b r;
    public final SharedPreferences s;
    public boolean t;
    private j u;
    public g0 v;
    private Runnable w;
    public boolean x = false;
    public m y;
    public boolean z;

    public d(b bVar, e eVar, h hVar, k kVar, s sVar, SharedPreferences sharedPreferences, a aVar, Handler handler, com.chartboost.sdk.h hVar2, n nVar, i iVar, o oVar, p0 p0Var, String str, String str2) {
        this.r = bVar;
        this.f5830a = p0Var;
        this.f5831b = hVar;
        this.f5832c = kVar;
        this.f5833d = sVar;
        this.f5834e = aVar;
        this.f5835f = handler;
        this.f5836g = hVar2;
        this.f5837h = nVar;
        this.f5838i = iVar;
        this.f5839j = oVar;
        this.f5840k = eVar;
        this.l = 0;
        this.t = false;
        this.z = false;
        this.B = true;
        this.n = 3;
        this.m = str;
        this.q = str2;
        this.o = true;
        this.s = sharedPreferences;
    }

    private boolean x() {
        return this.p != null;
    }

    private boolean y() {
        return this.p.booleanValue();
    }

    public boolean a() {
        this.l = 0;
        b bVar = this.r;
        if (bVar.f5817b == 0) {
            int i2 = this.f5830a.f6123a;
            if (i2 != 0) {
                if (i2 == 1) {
                    this.n = 2;
                    this.u = new j1(this, this.f5831b, this.f5835f, this.f5836g);
                    this.o = false;
                }
            } else if (bVar.p.equals(AnalyticsEvents.PARAMETER_SHARE_DIALOG_CONTENT_VIDEO)) {
                this.n = 1;
                this.u = new j1(this, this.f5831b, this.f5835f, this.f5836g);
                this.o = false;
            } else {
                this.n = 0;
                this.u = new i1(this, this.f5835f, this.f5836g);
            }
        } else {
            int i3 = this.f5830a.f6123a;
            if (i3 != 0) {
                if (i3 == 1) {
                    this.n = 2;
                    this.o = false;
                }
            } else if (bVar.p.equals(AnalyticsEvents.PARAMETER_SHARE_DIALOG_CONTENT_VIDEO)) {
                this.n = 1;
                this.o = false;
            } else {
                this.n = 0;
            }
            this.u = new j0(this, this.f5831b, this.f5832c, this.s, this.f5834e, this.f5835f, this.f5836g, this.f5838i);
        }
        return this.u.a(this.r.f5816a);
    }

    public boolean b() {
        return this.o;
    }

    public void c() {
        this.B = true;
        this.f5836g.b(this);
        this.f5840k.c(this);
    }

    public void d() {
        this.f5840k.a(this);
    }

    public void e() {
        f fVar;
        f fVar2;
        this.o = true;
        if (this.f5830a.f6123a == 1 && (fVar2 = com.chartboost.sdk.n.f5941d) != null) {
            fVar2.didCompleteRewardedVideo(this.m, this.r.f5826k);
        } else if (this.f5830a.f6123a == 0 && (fVar = com.chartboost.sdk.n.f5941d) != null) {
            fVar.didCompleteInterstitial(this.m);
        }
        w();
    }

    public void f() {
    }

    public boolean g() {
        j jVar = this.u;
        if (jVar != null) {
            jVar.b();
            if (this.u.e() != null) {
                return true;
            }
        } else {
            com.chartboost.sdk.c.a.b("CBImpression", "reinitializing -- no view protocol exists!!");
        }
        com.chartboost.sdk.c.a.e("CBImpression", "reinitializing -- view not yet created");
        return false;
    }

    public void h() {
        i();
        if (this.t) {
            j jVar = this.u;
            if (jVar != null) {
                jVar.d();
            }
            this.u = null;
            com.chartboost.sdk.c.a.e("CBImpression", "Destroying the view and view data");
        }
    }

    public void i() {
        g0 g0Var = this.v;
        if (g0Var != null) {
            g0Var.b();
            try {
                if (!(this.u == null || this.u.e() == null || this.u.e().getParent() == null)) {
                    this.v.removeView(this.u.e());
                }
            } catch (Exception e2) {
                com.chartboost.sdk.c.a.a("CBImpression", "Exception raised while cleaning up views", e2);
                a.a(d.class, "cleanUpViews", e2);
            }
            this.v = null;
        }
        j jVar = this.u;
        if (jVar != null) {
            jVar.f();
        }
        com.chartboost.sdk.c.a.e("CBImpression", "Destroying the view");
    }

    public a.c j() {
        try {
            if (this.u != null) {
                return this.u.c();
            }
        } catch (Exception e2) {
            com.chartboost.sdk.e.a.a(d.class, "tryCreatingView", e2);
        }
        return a.c.ERROR_CREATING_VIEW;
    }

    public j.b k() {
        j jVar = this.u;
        if (jVar != null) {
            return jVar.e();
        }
        return null;
    }

    public void l() {
        j jVar = this.u;
        if (jVar != null && jVar.e() != null) {
            this.u.e().setVisibility(8);
        }
    }

    public void m() {
        this.x = true;
    }

    public void n() {
        Runnable runnable = this.w;
        if (runnable != null) {
            runnable.run();
            this.w = null;
        }
        this.x = false;
    }

    public String o() {
        return this.r.f5821f;
    }

    public void p() {
        this.f5840k.b(this);
    }

    public boolean q() {
        j jVar = this.u;
        if (jVar != null) {
            return jVar.l();
        }
        return false;
    }

    public void r() {
        this.z = false;
        j jVar = this.u;
        if (jVar != null && this.A) {
            this.A = false;
            jVar.m();
        }
    }

    public void s() {
        this.z = false;
    }

    public void t() {
        j jVar = this.u;
        if (jVar != null && !this.A) {
            this.A = true;
            jVar.n();
        }
    }

    public j u() {
        return this.u;
    }

    public boolean v() {
        return this.B;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.o.m.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, boolean]
     candidates:
      com.chartboost.sdk.o.m.a(com.chartboost.sdk.o.j, com.chartboost.sdk.d.a):void
      com.chartboost.sdk.o.m.a(com.chartboost.sdk.d.a, com.chartboost.sdk.o.j):void
      com.chartboost.sdk.o.m.a(java.lang.Object, com.chartboost.sdk.o.j):void
      com.chartboost.sdk.o.m.a(org.json.JSONObject, com.chartboost.sdk.o.j):void
      com.chartboost.sdk.o.g.a(com.chartboost.sdk.d.a, com.chartboost.sdk.o.j):void
      com.chartboost.sdk.o.g.a(java.lang.Object, com.chartboost.sdk.o.j):void
      com.chartboost.sdk.o.m.a(java.lang.String, java.lang.Object):void */
    public void w() {
        m mVar = new m("/api/video-complete", this.f5833d, this.f5834e, 2, null);
        mVar.a(FirebaseAnalytics.Param.LOCATION, this.m);
        mVar.a("reward", Integer.valueOf(this.r.f5826k));
        mVar.a("currency-name", this.r.l);
        mVar.a(AppLovinNativeAdapter.KEY_EXTRA_AD_ID, o());
        mVar.a("force_close", (Object) false);
        if (!this.r.f5822g.isEmpty()) {
            mVar.a("cgn", this.r.f5822g);
        }
        j u2 = k() != null ? u() : null;
        if (u2 != null) {
            float k2 = u2.k();
            float j2 = u2.j();
            com.chartboost.sdk.c.a.a(d.class.getSimpleName(), String.format(Locale.US, "TotalDuration: %f PlaybackTime: %f", Float.valueOf(j2), Float.valueOf(k2)));
            float f2 = j2 / 1000.0f;
            mVar.a("total_time", Float.valueOf(f2));
            if (k2 <= Animation.CurveTimeline.LINEAR) {
                mVar.a("playback_time", Float.valueOf(f2));
            } else {
                mVar.a("playback_time", Float.valueOf(k2 / 1000.0f));
            }
        }
        this.f5832c.a(mVar);
        this.f5834e.b(this.f5830a.a(this.r.f5817b), o());
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x003b A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x003c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(org.json.JSONObject r7) {
        /*
            r6 = this;
            int r0 = r6.l
            r1 = 0
            r2 = 2
            if (r0 != r2) goto L_0x0045
            boolean r0 = r6.x
            if (r0 == 0) goto L_0x000b
            goto L_0x0045
        L_0x000b:
            com.chartboost.sdk.d.b r0 = r6.r
            java.lang.String r2 = r0.f5825j
            java.lang.String r0 = r0.f5824i
            boolean r3 = r0.isEmpty()
            if (r3 != 0) goto L_0x0037
            com.chartboost.sdk.o.n r3 = r6.f5837h     // Catch:{ Exception -> 0x002f }
            boolean r3 = r3.a(r0)     // Catch:{ Exception -> 0x002f }
            if (r3 == 0) goto L_0x002a
            java.lang.Boolean r2 = java.lang.Boolean.TRUE     // Catch:{ Exception -> 0x0025 }
            r6.p = r2     // Catch:{ Exception -> 0x0025 }
            r2 = r0
            goto L_0x0037
        L_0x0025:
            r2 = move-exception
            r5 = r2
            r2 = r0
            r0 = r5
            goto L_0x0030
        L_0x002a:
            java.lang.Boolean r0 = java.lang.Boolean.FALSE     // Catch:{ Exception -> 0x002f }
            r6.p = r0     // Catch:{ Exception -> 0x002f }
            goto L_0x0037
        L_0x002f:
            r0 = move-exception
        L_0x0030:
            java.lang.Class<com.chartboost.sdk.d.d> r3 = com.chartboost.sdk.d.d.class
            java.lang.String r4 = "onClick"
            com.chartboost.sdk.e.a.a(r3, r4, r0)
        L_0x0037:
            boolean r0 = r6.z
            if (r0 == 0) goto L_0x003c
            return r1
        L_0x003c:
            r0 = 1
            r6.z = r0
            r6.B = r1
            r6.a(r2, r7)
            return r0
        L_0x0045:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.d.d.a(org.json.JSONObject):boolean");
    }

    public void a(a.c cVar) {
        this.f5840k.a(this, cVar);
    }

    public void a(Runnable runnable) {
        this.w = runnable;
    }

    /* access modifiers changed from: package-private */
    public void a(String str, JSONObject jSONObject) {
        j jVar;
        i c2;
        Handler handler = this.f5835f;
        p0 p0Var = this.f5830a;
        p0Var.getClass();
        handler.post(new p0.a(1, this.m, null));
        if (b() && this.l == 2 && (c2 = this.f5836g.c()) != null) {
            c2.b(this);
        }
        if (!f1.e().a(str)) {
            m mVar = new m("/api/click", this.f5833d, this.f5834e, 2, null);
            if (!this.r.f5821f.isEmpty()) {
                mVar.a(AppLovinNativeAdapter.KEY_EXTRA_AD_ID, this.r.f5821f);
            }
            if (!this.r.m.isEmpty()) {
                mVar.a(TJAdUnitConstants.String.SPLIT_VIEW_TRIGGER_TO, this.r.m);
            }
            if (!this.r.f5822g.isEmpty()) {
                mVar.a("cgn", this.r.f5822g);
            }
            if (!this.r.f5823h.isEmpty()) {
                mVar.a("creative", this.r.f5823h);
            }
            int i2 = this.n;
            if (i2 == 1 || i2 == 2) {
                if (this.r.f5817b != 0 || k() == null) {
                    jVar = (this.r.f5817b != 1 || k() == null) ? null : (j0) u();
                } else {
                    jVar = (j1) u();
                }
                if (jVar != null) {
                    float k2 = jVar.k();
                    float j2 = jVar.j();
                    com.chartboost.sdk.c.a.a(d.class.getSimpleName(), String.format(Locale.US, "TotalDuration: %f PlaybackTime: %f", Float.valueOf(j2), Float.valueOf(k2)));
                    float f2 = j2 / 1000.0f;
                    mVar.a("total_time", Float.valueOf(f2));
                    if (k2 <= Animation.CurveTimeline.LINEAR) {
                        mVar.a("playback_time", Float.valueOf(f2));
                    } else {
                        mVar.a("playback_time", Float.valueOf(k2 / 1000.0f));
                    }
                }
            }
            if (jSONObject != null) {
                mVar.a("click_coordinates", jSONObject);
            }
            mVar.a(FirebaseAnalytics.Param.LOCATION, this.m);
            if (x()) {
                mVar.a("retarget_reinstall", Boolean.valueOf(y()));
            }
            this.y = mVar;
            this.f5837h.a(this, str, null);
        } else {
            this.f5837h.a(this, false, str, a.b.URI_INVALID, null);
        }
        this.f5834e.c(this.f5830a.a(this.r.f5817b), this.m, o());
    }
}
