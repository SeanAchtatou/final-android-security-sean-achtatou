package com.chartboost.sdk.d;

import com.appsflyer.share.Constants;
import com.chartboost.sdk.c.g;
import com.chartboost.sdk.e.a;
import com.facebook.internal.AnalyticsEvents;
import com.tapjoy.TJAdUnitConstants;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class c {

    /* renamed from: a  reason: collision with root package name */
    public final String f5827a;

    /* renamed from: b  reason: collision with root package name */
    public final String f5828b;

    /* renamed from: c  reason: collision with root package name */
    public final String f5829c;

    public c(String str, String str2, String str3) {
        this.f5827a = str;
        this.f5828b = str2;
        this.f5829c = str3;
    }

    public static Map<String, c> a(JSONObject jSONObject) {
        Class<c> cls = c.class;
        HashMap hashMap = new HashMap();
        try {
            JSONArray jSONArray = jSONObject.getJSONArray("videos");
            int length = jSONArray.length();
            for (int i2 = 0; i2 < length; i2++) {
                try {
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                    String string = jSONObject2.getString("id");
                    hashMap.put(string, new c("videos", string, jSONObject2.getString(AnalyticsEvents.PARAMETER_SHARE_DIALOG_CONTENT_VIDEO)));
                } catch (JSONException e2) {
                    a.a(cls, "deserializeNativeVideos (file)", e2);
                }
            }
        } catch (JSONException e3) {
            a.a(cls, "deserializeNativeVideos (videos array)", e3);
        }
        return hashMap;
    }

    private static Map<String, c> b(JSONObject jSONObject) throws JSONException {
        HashMap hashMap = new HashMap();
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            JSONObject jSONObject2 = jSONObject.getJSONObject(next);
            Iterator<String> keys2 = jSONObject2.keys();
            while (keys2.hasNext()) {
                String next2 = keys2.next();
                JSONObject jSONObject3 = jSONObject2.getJSONObject(next2);
                hashMap.put(next2, new c(next, jSONObject3.getString("filename"), jSONObject3.getString("url")));
            }
        }
        return hashMap;
    }

    private static JSONObject a(JSONArray jSONArray) throws JSONException {
        JSONObject a2 = g.a(new g.a[0]);
        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
            JSONObject jSONObject = jSONArray.getJSONObject(i2);
            String optString = jSONObject.optString("name");
            String optString2 = jSONObject.optString("type");
            String optString3 = jSONObject.optString("value");
            String optString4 = jSONObject.optString("param");
            if (!optString2.equals("param") && optString4.isEmpty()) {
                JSONObject optJSONObject = a2.optJSONObject(optString2);
                if (optJSONObject == null) {
                    optJSONObject = g.a(new g.a[0]);
                    a2.put(optString2, optJSONObject);
                }
                optJSONObject.put(optString2.equals(TJAdUnitConstants.String.HTML) ? "body" : optString, g.a(g.a("filename", optString), g.a("url", optString3)));
            }
        }
        return a2;
    }

    public static Map<String, c> a(JSONObject jSONObject, int i2) {
        HashMap hashMap = new HashMap();
        try {
            JSONObject jSONObject2 = jSONObject.getJSONObject("cache_assets");
            Iterator<String> keys = jSONObject2.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                int i3 = 0;
                if (next.equals("templates")) {
                    JSONArray optJSONArray = jSONObject2.optJSONArray("templates");
                    if (optJSONArray != null) {
                        int min = Math.min(i2, optJSONArray.length());
                        while (i3 < min) {
                            for (Map.Entry<String, c> value : b(a(optJSONArray.getJSONObject(i3).getJSONArray("elements"))).entrySet()) {
                                c cVar = (c) value.getValue();
                                hashMap.put(cVar.f5828b, cVar);
                            }
                            i3++;
                        }
                    }
                } else {
                    JSONArray jSONArray = jSONObject2.getJSONArray(next);
                    while (i3 < jSONArray.length()) {
                        JSONObject jSONObject3 = jSONArray.getJSONObject(i3);
                        String string = jSONObject3.getString("name");
                        hashMap.put(string, new c(next, string, jSONObject3.getString("value")));
                        i3++;
                    }
                }
            }
        } catch (JSONException e2) {
            a.a(c.class, "v2PrefetchToAssets", e2);
        }
        return hashMap;
    }

    public File a(File file) {
        return new File(file, this.f5827a + Constants.URL_PATH_DELIMITER + this.f5828b);
    }
}
