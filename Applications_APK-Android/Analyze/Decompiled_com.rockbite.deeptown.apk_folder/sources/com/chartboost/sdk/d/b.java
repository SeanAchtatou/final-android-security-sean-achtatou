package com.chartboost.sdk.d;

import com.applovin.mediation.AppLovinNativeAdapter;
import com.appsflyer.share.Constants;
import com.tapjoy.TJAdUnitConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class b {

    /* renamed from: a  reason: collision with root package name */
    public final JSONObject f5816a;

    /* renamed from: b  reason: collision with root package name */
    public int f5817b;

    /* renamed from: c  reason: collision with root package name */
    public final Map<String, c> f5818c = new HashMap();

    /* renamed from: d  reason: collision with root package name */
    public final Map<String, String> f5819d = new HashMap();

    /* renamed from: e  reason: collision with root package name */
    public final String f5820e;

    /* renamed from: f  reason: collision with root package name */
    public String f5821f;

    /* renamed from: g  reason: collision with root package name */
    public String f5822g;

    /* renamed from: h  reason: collision with root package name */
    public String f5823h;

    /* renamed from: i  reason: collision with root package name */
    public final String f5824i;

    /* renamed from: j  reason: collision with root package name */
    public final String f5825j;

    /* renamed from: k  reason: collision with root package name */
    public final int f5826k;
    public final String l;
    public final String m;
    public final Map<String, List<String>> n = new HashMap();
    public final int o;
    public String p;
    public c q;
    public final HashSet<String> r = new HashSet<>();

    public b(int i2, JSONObject jSONObject, boolean z) throws JSONException {
        String str;
        this.f5817b = i2;
        this.f5816a = jSONObject;
        this.f5821f = jSONObject.getString(AppLovinNativeAdapter.KEY_EXTRA_AD_ID);
        this.f5822g = jSONObject.getString("cgn");
        this.f5823h = jSONObject.getString("creative");
        this.f5824i = jSONObject.optString("deep-link");
        this.f5825j = jSONObject.getString("link");
        this.m = jSONObject.getString(TJAdUnitConstants.String.SPLIT_VIEW_TRIGGER_TO);
        this.o = jSONObject.optInt("animation");
        this.p = jSONObject.optString("media-type");
        jSONObject.optString("name");
        if (i2 == 1) {
            JSONObject jSONObject2 = jSONObject.getJSONObject("webview");
            JSONArray jSONArray = jSONObject2.getJSONArray("elements");
            String str2 = "";
            int i3 = 0;
            int i4 = 0;
            while (true) {
                str = "body";
                if (i3 >= jSONArray.length()) {
                    break;
                }
                JSONObject jSONObject3 = jSONArray.getJSONObject(i3);
                String string = jSONObject3.getString("name");
                String optString = jSONObject3.optString("param");
                String string2 = jSONObject3.getString("type");
                String string3 = jSONObject3.getString("value");
                if (string2.equals("param")) {
                    this.f5819d.put(optString, string3);
                    if (string.equals("reward_amount")) {
                        i4 = Integer.valueOf(string3).intValue();
                    } else if (string.equals("reward_currency")) {
                        str2 = string3;
                    }
                } else {
                    this.f5818c.put((!string2.equals(TJAdUnitConstants.String.HTML) || !optString.isEmpty()) ? optString.isEmpty() ? string : optString : str, new c(string2, string, string3));
                }
                i3++;
            }
            this.f5826k = i4;
            this.l = str2;
            this.q = this.f5818c.get(str);
            if (this.q != null) {
                this.f5820e = jSONObject2.getString("template");
                JSONObject optJSONObject = jSONObject.optJSONObject("events");
                if (optJSONObject != null) {
                    Iterator<String> keys = optJSONObject.keys();
                    while (keys.hasNext()) {
                        String next = keys.next();
                        JSONArray jSONArray2 = optJSONObject.getJSONArray(next);
                        ArrayList arrayList = new ArrayList();
                        for (int i5 = 0; i5 < jSONArray2.length(); i5++) {
                            arrayList.add(jSONArray2.getString(i5));
                        }
                        this.n.put(next, arrayList);
                    }
                }
                JSONArray optJSONArray = jSONObject.optJSONArray("certification_providers");
                if (optJSONArray != null) {
                    for (int i6 = 0; i6 < optJSONArray.length(); i6++) {
                        this.r.add(optJSONArray.getString(i6));
                    }
                    return;
                }
                return;
            }
            throw new RuntimeException("WebView AdUnit does not have a template html body asset");
        }
        if (z) {
            String string4 = jSONObject.getJSONObject("icons").getString("lg");
            this.f5818c.put("lg", new c("inPlayIcons", string4.substring(string4.lastIndexOf(Constants.URL_PATH_DELIMITER) + 1), string4));
            this.f5826k = 0;
            this.l = "";
        } else {
            JSONObject jSONObject4 = jSONObject.getJSONObject("assets");
            Iterator<String> keys2 = jSONObject4.keys();
            while (keys2.hasNext()) {
                String next2 = keys2.next();
                JSONObject jSONObject5 = jSONObject4.getJSONObject(next2);
                String str3 = (next2.equals("video-portrait") || next2.equals("video-landscape")) ? "videos" : "images";
                String optString2 = jSONObject5.optString("id", null);
                if (optString2 == null) {
                    optString2 = jSONObject5.getString("checksum") + ".png";
                }
                this.f5818c.put(next2, new c(str3, optString2, jSONObject5.getString("url")));
            }
            this.f5826k = jSONObject.optInt("reward");
            this.l = jSONObject.optString("currency-name");
        }
        this.q = null;
        this.f5820e = "";
    }
}
