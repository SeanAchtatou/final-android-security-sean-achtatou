package com.chartboost.sdk.c;

import com.chartboost.sdk.e.a;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

public final class e {
    private e() {
    }

    public static synchronized byte[] a(byte[] bArr) {
        Class<e> cls = e.class;
        synchronized (cls) {
            if (bArr == null) {
                return null;
            }
            try {
                MessageDigest instance = MessageDigest.getInstance("SHA-1");
                instance.update(bArr);
                byte[] digest = instance.digest();
                return digest;
            } catch (NoSuchAlgorithmException e2) {
                a.a(cls, "sha1", e2);
                return null;
            } catch (Exception e3) {
                a.a(cls, "sha1", e3);
                return null;
            }
        }
    }

    public static String b(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        BigInteger bigInteger = new BigInteger(1, bArr);
        Locale locale = Locale.US;
        return String.format(locale, "%0" + (bArr.length << 1) + "x", bigInteger);
    }
}
