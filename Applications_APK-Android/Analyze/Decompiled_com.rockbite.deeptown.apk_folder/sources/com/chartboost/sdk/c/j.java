package com.chartboost.sdk.c;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.File;
import org.json.JSONObject;

public class j {

    /* renamed from: a  reason: collision with root package name */
    private a f5777a;

    /* renamed from: b  reason: collision with root package name */
    private final com.chartboost.sdk.j f5778b;

    /* renamed from: c  reason: collision with root package name */
    private float f5779c = 1.0f;

    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private int f5780a;

        /* renamed from: b  reason: collision with root package name */
        private final String f5781b;

        /* renamed from: c  reason: collision with root package name */
        private final File f5782c;

        /* renamed from: d  reason: collision with root package name */
        private Bitmap f5783d;

        /* renamed from: e  reason: collision with root package name */
        private final h f5784e;

        /* renamed from: f  reason: collision with root package name */
        private int f5785f = -1;

        /* renamed from: g  reason: collision with root package name */
        private int f5786g = -1;

        public a(String str, File file, h hVar) {
            this.f5782c = file;
            this.f5781b = str;
            this.f5783d = null;
            this.f5780a = 1;
            this.f5784e = hVar;
        }

        private void f() {
            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(this.f5782c.getAbsolutePath(), options);
                this.f5785f = options.outWidth;
                this.f5786g = options.outHeight;
            } catch (Exception e2) {
                a.a("MemoryBitmap", "Error decoding file size", e2);
                com.chartboost.sdk.e.a.a(a.class, "decodeSize", e2);
            }
        }

        public Bitmap a() {
            if (this.f5783d == null) {
                b();
            }
            return this.f5783d;
        }

        public void b() {
            if (this.f5783d == null) {
                a.a("MemoryBitmap", "Loading image '" + this.f5781b + "' from cache");
                byte[] a2 = this.f5784e.a(this.f5782c);
                if (a2 == null) {
                    a.b("MemoryBitmap", "decode() - bitmap not found");
                    return;
                }
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeByteArray(a2, 0, a2.length, options);
                BitmapFactory.Options options2 = new BitmapFactory.Options();
                options2.inJustDecodeBounds = false;
                options2.inDither = false;
                options2.inPurgeable = true;
                options2.inInputShareable = true;
                options2.inTempStorage = new byte[32768];
                options2.inSampleSize = 1;
                while (true) {
                    if (options2.inSampleSize >= 32) {
                        break;
                    }
                    try {
                        this.f5783d = BitmapFactory.decodeByteArray(a2, 0, a2.length, options2);
                        break;
                    } catch (OutOfMemoryError e2) {
                        a.a("MemoryBitmap", "OutOfMemoryError suppressed - trying larger sample size", e2);
                        options2.inSampleSize *= 2;
                    } catch (Exception e3) {
                        a.a("MemoryBitmap", "Exception raised decoding bitmap", e3);
                        com.chartboost.sdk.e.a.a(a.class, "decodeByteArray", e3);
                    }
                }
                this.f5780a = options2.inSampleSize;
            }
            return;
            if (this.f5783d == null) {
                this.f5782c.delete();
                throw new RuntimeException("Unable to decode " + this.f5781b);
            }
            this.f5780a = options2.inSampleSize;
        }

        public int c() {
            return this.f5780a;
        }

        public int d() {
            Bitmap bitmap = this.f5783d;
            if (bitmap != null) {
                return bitmap.getWidth();
            }
            int i2 = this.f5785f;
            if (i2 >= 0) {
                return i2;
            }
            f();
            return this.f5785f;
        }

        public int e() {
            Bitmap bitmap = this.f5783d;
            if (bitmap != null) {
                return bitmap.getHeight();
            }
            int i2 = this.f5786g;
            if (i2 >= 0) {
                return i2;
            }
            f();
            return this.f5786g;
        }
    }

    public j(com.chartboost.sdk.j jVar) {
        this.f5778b = jVar;
    }

    public int a() {
        return this.f5777a.d() * this.f5777a.c();
    }

    public int b() {
        return this.f5777a.e() * this.f5777a.c();
    }

    public boolean c() {
        return this.f5777a != null;
    }

    public Bitmap d() {
        a aVar = this.f5777a;
        if (aVar != null) {
            return aVar.a();
        }
        return null;
    }

    public float e() {
        return this.f5779c;
    }

    public boolean a(String str) {
        return a(this.f5778b.g(), str);
    }

    public boolean a(JSONObject jSONObject, String str) {
        JSONObject a2 = g.a(jSONObject, str);
        if (a2 == null) {
            return true;
        }
        String optString = a2.optString("url");
        this.f5779c = (float) a2.optDouble("scale", 1.0d);
        if (optString.isEmpty()) {
            return true;
        }
        String optString2 = a2.optString("checksum");
        if (optString2.isEmpty()) {
            return false;
        }
        this.f5777a = this.f5778b.f5892g.f5839j.a(optString2);
        if (this.f5777a != null) {
            return true;
        }
        return false;
    }
}
