package com.chartboost.sdk.c;

import com.tapjoy.TJAdUnitConstants;
import java.io.File;

public class i {

    /* renamed from: a  reason: collision with root package name */
    public final File f5773a;

    /* renamed from: b  reason: collision with root package name */
    public final File f5774b;

    /* renamed from: c  reason: collision with root package name */
    public final File f5775c;

    /* renamed from: d  reason: collision with root package name */
    public final File f5776d;

    i(File file) {
        this.f5773a = new File(file, ".chartboost");
        if (!this.f5773a.exists()) {
            this.f5773a.mkdirs();
        }
        a(this.f5773a, "css");
        a(this.f5773a, TJAdUnitConstants.String.HTML);
        this.f5774b = a(this.f5773a, "images");
        a(this.f5773a, "js");
        this.f5775c = a(this.f5773a, "templates");
        this.f5776d = a(this.f5773a, "videos");
    }

    private static File a(File file, String str) {
        File file2 = new File(file, str);
        if (!file2.exists()) {
            file2.mkdir();
        }
        return file2;
    }
}
