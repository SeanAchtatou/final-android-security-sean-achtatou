package com.chartboost.sdk.c;

import android.util.Log;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    public static C0126a f5753a = C0126a.INTEGRATION;

    /* renamed from: com.chartboost.sdk.c.a$a  reason: collision with other inner class name */
    public enum C0126a {
        NONE,
        INTEGRATION,
        ALL
    }

    public static void a(String str, String str2) {
        if (f5753a == C0126a.ALL) {
            Log.d(str, str2);
        }
    }

    public static void b(String str, String str2) {
        if (f5753a == C0126a.ALL) {
            Log.e(str, str2);
        }
    }

    public static void c(String str, String str2) {
        if (f5753a == C0126a.ALL) {
            Log.v(str, str2);
        }
    }

    public static void d(String str, String str2) {
        if (f5753a == C0126a.ALL) {
            Log.w(str, str2);
        }
    }

    public static void e(String str, String str2) {
        if (f5753a == C0126a.ALL) {
            Log.i(str, str2);
        }
    }

    public static void a(String str, String str2, Throwable th) {
        if (f5753a == C0126a.ALL) {
            Log.e(str, str2, th);
        }
    }

    public static void b(String str, String str2, Throwable th) {
        if (f5753a == C0126a.ALL) {
            Log.v(str, str2, th);
        }
    }

    public static void c(String str, String str2, Throwable th) {
        if (f5753a == C0126a.ALL) {
            Log.w(str, str2, th);
        }
    }
}
