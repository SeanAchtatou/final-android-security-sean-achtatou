package com.chartboost.sdk.c;

import org.json.JSONException;
import org.json.JSONObject;

public class g {

    public static class a {

        /* renamed from: a  reason: collision with root package name */
        final String f5767a;

        /* renamed from: b  reason: collision with root package name */
        final Object f5768b;

        public a(String str, Object obj) {
            this.f5767a = str;
            this.f5768b = obj;
        }
    }

    public static JSONObject a(JSONObject jSONObject, String... strArr) {
        for (String str : strArr) {
            if (jSONObject == null) {
                break;
            }
            jSONObject = jSONObject.optJSONObject(str);
        }
        return jSONObject;
    }

    public static void a(JSONObject jSONObject, String str, Object obj) {
        try {
            jSONObject.put(str, obj);
        } catch (JSONException e2) {
            com.chartboost.sdk.e.a.a(g.class, "put (" + str + ")", e2);
        }
    }

    public static JSONObject a(a... aVarArr) {
        JSONObject jSONObject = new JSONObject();
        for (a aVar : aVarArr) {
            a(jSONObject, aVar.f5767a, aVar.f5768b);
        }
        return jSONObject;
    }

    public static a a(String str, Object obj) {
        return new a(str, obj);
    }
}
