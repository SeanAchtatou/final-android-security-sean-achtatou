package com.chartboost.sdk.c;

import android.content.ContentResolver;
import android.content.Context;
import android.os.Build;
import android.os.Looper;
import android.provider.Settings;
import android.util.Base64;
import com.chartboost.sdk.n;
import com.chartboost.sdk.o.u;
import com.chartboost.sdk.o.v;
import org.json.JSONObject;

public class f {

    /* renamed from: a  reason: collision with root package name */
    private int f5762a = -1;

    /* renamed from: b  reason: collision with root package name */
    private String f5763b = null;

    /* renamed from: c  reason: collision with root package name */
    private final String f5764c;

    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public final int f5765a;

        /* renamed from: b  reason: collision with root package name */
        public final String f5766b;

        public a(int i2, String str, String str2, String str3) {
            this.f5765a = i2;
            this.f5766b = str;
        }
    }

    public f(Context context) {
        this.f5764c = u.c(context);
    }

    private static boolean b() {
        return !"Amazon".equalsIgnoreCase(Build.MANUFACTURER);
    }

    private void c() {
        if (v.a(n.n)) {
            c cVar = new c(n.n);
            this.f5762a = cVar.f5758a;
            this.f5763b = cVar.f5759b;
        }
    }

    public synchronized a a() {
        if (Looper.myLooper() != Looper.getMainLooper() || "robolectric".equals(Build.FINGERPRINT)) {
            if (b()) {
                c();
            } else {
                a(n.n);
            }
            String str = this.f5763b;
            JSONObject jSONObject = new JSONObject();
            if (this.f5764c != null && str == null) {
                g.a(jSONObject, "uuid", this.f5764c);
            }
            if (str != null) {
                g.a(jSONObject, "gaid", str);
            }
            return new a(this.f5762a, Base64.encodeToString(jSONObject.toString().getBytes(), 0), str != null ? "000000000" : this.f5764c, str);
        }
        a.b("CBIdentity", "I must be called from a background thread");
        return null;
    }

    private void a(Context context) {
        try {
            ContentResolver contentResolver = context.getContentResolver();
            if (!(Settings.Secure.getInt(contentResolver, "limit_ad_tracking") != 0)) {
                String string = Settings.Secure.getString(contentResolver, "advertising_id");
                if ("00000000-0000-0000-0000-000000000000".equals(string)) {
                    this.f5762a = 1;
                    this.f5763b = null;
                    return;
                }
                this.f5762a = 0;
                this.f5763b = string;
                return;
            }
            this.f5762a = 1;
            this.f5763b = null;
        } catch (Settings.SettingNotFoundException unused) {
            this.f5762a = -1;
            this.f5763b = null;
        }
    }
}
