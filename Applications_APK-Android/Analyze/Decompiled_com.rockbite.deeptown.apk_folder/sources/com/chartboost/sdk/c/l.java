package com.chartboost.sdk.c;

import android.app.Activity;
import com.chartboost.sdk.o.t;
import java.lang.ref.WeakReference;

public final class l extends WeakReference<Activity> {

    /* renamed from: a  reason: collision with root package name */
    public final int f5787a;

    public l(Activity activity) {
        super(activity);
        t.a("WeakActivity.WeakActivity", activity);
        this.f5787a = activity.hashCode();
    }

    public boolean a(Activity activity) {
        return activity != null && activity.hashCode() == this.f5787a;
    }

    public int hashCode() {
        return this.f5787a;
    }
}
