package com.chartboost.sdk.c;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.view.Display;
import android.view.WindowManager;
import com.chartboost.sdk.a;
import com.chartboost.sdk.d.f;
import com.chartboost.sdk.n;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public final class b {
    public static float a(Context context) {
        return context.getResources().getDisplayMetrics().density;
    }

    public static boolean a(int i2) {
        return i2 == 0 || i2 == 2;
    }

    public static String b() {
        Object[] objArr = new Object[3];
        objArr[0] = "Chartboost-Android-SDK";
        Object obj = n.f5942e;
        if (obj == null) {
            obj = "";
        }
        objArr[1] = obj;
        objArr[2] = "7.5.0";
        return String.format("%s %s %s", objArr);
    }

    public static boolean b(int i2) {
        return i2 == 1 || i2 == 3;
    }

    public static boolean c() {
        return e() || f() || g();
    }

    public static String d() {
        SimpleDateFormat simpleDateFormat;
        if (Build.VERSION.SDK_INT >= 18) {
            simpleDateFormat = new SimpleDateFormat("ZZZZ", Locale.US);
        } else {
            simpleDateFormat = new SimpleDateFormat("'GMT'ZZZZ", Locale.US);
        }
        simpleDateFormat.setTimeZone(TimeZone.getDefault());
        return simpleDateFormat.format(new Date());
    }

    private static boolean e() {
        String str = Build.TAGS;
        return str != null && str.contains("test-keys");
    }

    private static boolean f() {
        return new File("/system/app/Superuser.apk").exists();
    }

    private static boolean g() {
        for (String file : new String[]{"/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su", "/system/bin/failsafe/su", "/data/local/su"}) {
            if (new File(file).exists()) {
                return true;
            }
        }
        return false;
    }

    public static int a(int i2, Context context) {
        return Math.round(((float) i2) * a(context));
    }

    public static void b(Activity activity, int i2, f fVar) {
        if (activity != null && !a(activity)) {
            if ((i2 == 1 && fVar.v && fVar.x) || (i2 == 0 && fVar.f5845e && fVar.f5847g)) {
                activity.setRequestedOrientation(-1);
            }
        }
    }

    public static float a(float f2, Context context) {
        return f2 * a(context);
    }

    public static int a() {
        Context context = n.n;
        Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        int rotation = defaultDisplay.getRotation();
        boolean z = defaultDisplay.getWidth() != defaultDisplay.getHeight() ? defaultDisplay.getWidth() < defaultDisplay.getHeight() : context.getResources().getConfiguration().orientation != 2;
        if (!(rotation == 0 || rotation == 2)) {
            z = !z;
        }
        if (z) {
            if (rotation == 1) {
                return 1;
            }
            if (rotation == 2) {
                return 2;
            }
            if (rotation != 3) {
                return 0;
            }
            return 3;
        } else if (rotation == 1) {
            return 2;
        } else {
            if (rotation != 2) {
                return rotation != 3 ? 1 : 0;
            }
            return 3;
        }
    }

    public static void a(Activity activity, int i2, f fVar) {
        if (activity != null && !a(activity)) {
            if ((i2 == 1 && fVar.v && fVar.x) || (i2 == 0 && fVar.f5845e && fVar.f5847g)) {
                int a2 = a();
                if (a2 == 0) {
                    activity.setRequestedOrientation(1);
                } else if (a2 == 2) {
                    activity.setRequestedOrientation(9);
                } else if (a2 == 1) {
                    activity.setRequestedOrientation(0);
                } else {
                    activity.setRequestedOrientation(8);
                }
            }
        }
    }

    public static boolean a(a.C0125a aVar) {
        a.C0125a aVar2 = n.f5942e;
        return aVar2 != null && aVar2 == aVar;
    }

    public static ArrayList<File> a(File file, boolean z) {
        if (file == null) {
            return null;
        }
        ArrayList<File> arrayList = new ArrayList<>();
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (File file2 : listFiles) {
                if (file2.isFile() && !file2.getName().equals(".nomedia")) {
                    arrayList.add(file2);
                } else if (file2.isDirectory() && z) {
                    arrayList.addAll(a(file2, z));
                }
            }
        }
        return arrayList;
    }

    public static boolean a(Activity activity) {
        if (activity == null || activity.getWindow() == null || activity.getWindow().getDecorView() == null || activity.getWindow().getDecorView().getBackground() == null) {
            return true;
        }
        if (Build.VERSION.SDK_INT != 26 || activity.getApplicationInfo().targetSdkVersion <= 26 || activity.getWindow().getDecorView().getBackground().getAlpha() == 255) {
            return false;
        }
        return true;
    }
}
