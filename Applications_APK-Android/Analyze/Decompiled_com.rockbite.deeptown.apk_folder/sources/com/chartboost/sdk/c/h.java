package com.chartboost.sdk.c;

import android.content.Context;
import com.chartboost.sdk.d.f;
import com.chartboost.sdk.e.a;
import com.chartboost.sdk.n;
import com.chartboost.sdk.o.f1;
import com.chartboost.sdk.o.l0;
import java.io.File;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONArray;
import org.json.JSONObject;

public class h {

    /* renamed from: a  reason: collision with root package name */
    private final AtomicReference<f> f5769a;

    /* renamed from: b  reason: collision with root package name */
    private final i f5770b;

    /* renamed from: c  reason: collision with root package name */
    private final AtomicReference<i> f5771c;

    /* renamed from: d  reason: collision with root package name */
    private f1 f5772d;

    public h(f1 f1Var, Context context, AtomicReference<f> atomicReference) {
        i[] iVarArr;
        i[] iVarArr2;
        h hVar = this;
        hVar.f5772d = f1Var;
        hVar.f5770b = new i(context.getCacheDir());
        hVar.f5771c = new AtomicReference<>();
        hVar.f5769a = atomicReference;
        try {
            File a2 = f1Var.a();
            if (a2 != null) {
                hVar.f5771c.set(new i(a2));
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        new File(hVar.f5770b.f5773a, "track");
        new File(hVar.f5770b.f5773a, "session");
        i[] iVarArr3 = {hVar.f5770b, hVar.f5771c.get()};
        int length = iVarArr3.length;
        int i2 = 0;
        while (i2 < length) {
            i iVar = iVarArr3[i2];
            try {
                boolean z = iVar == hVar.f5770b;
                if (iVar == null || (!z && !a())) {
                    iVarArr = iVarArr3;
                    i2++;
                    hVar = this;
                    iVarArr3 = iVarArr;
                } else {
                    long currentTimeMillis = System.currentTimeMillis() - TimeUnit.DAYS.toMillis((long) atomicReference.get().t);
                    File file = new File(iVar.f5773a, "templates");
                    if (file.exists()) {
                        File[] listFiles = file.listFiles();
                        if (listFiles != null) {
                            int length2 = listFiles.length;
                            int i3 = 0;
                            while (i3 < length2) {
                                File file2 = listFiles[i3];
                                if (file2.isDirectory()) {
                                    File[] listFiles2 = file2.listFiles();
                                    if (listFiles2 != null) {
                                        int length3 = listFiles2.length;
                                        int i4 = 0;
                                        while (i4 < length3) {
                                            File file3 = listFiles2[i4];
                                            if ((z || file3.lastModified() < currentTimeMillis) && !file3.delete()) {
                                                StringBuilder sb = new StringBuilder();
                                                sb.append("Unable to delete ");
                                                iVarArr = iVarArr3;
                                                try {
                                                    sb.append(file3.getPath());
                                                    a.b("FileCache", sb.toString());
                                                } catch (Exception e3) {
                                                    e = e3;
                                                    a.a("FileCache", "Exception while cleaning up templates directory at " + iVar.f5775c.getPath(), e);
                                                    e.printStackTrace();
                                                    i2++;
                                                    hVar = this;
                                                    iVarArr3 = iVarArr;
                                                }
                                            } else {
                                                iVarArr = iVarArr3;
                                            }
                                            i4++;
                                            AtomicReference<f> atomicReference2 = atomicReference;
                                            iVarArr3 = iVarArr;
                                        }
                                    }
                                    iVarArr2 = iVarArr3;
                                    File[] listFiles3 = file2.listFiles();
                                    if (listFiles3 != null && listFiles3.length == 0 && !file2.delete()) {
                                        a.b("FileCache", "Unable to delete " + file2.getPath());
                                    }
                                } else {
                                    iVarArr2 = iVarArr3;
                                }
                                i3++;
                                AtomicReference<f> atomicReference3 = atomicReference;
                                iVarArr3 = iVarArr2;
                            }
                        }
                    }
                    iVarArr = iVarArr3;
                    File file4 = new File(iVar.f5773a, ".adId");
                    if (file4.exists() && ((z || file4.lastModified() < currentTimeMillis) && !file4.delete())) {
                        a.b("FileCache", "Unable to delete " + file4.getPath());
                    }
                    i2++;
                    hVar = this;
                    iVarArr3 = iVarArr;
                }
            } catch (Exception e4) {
                e = e4;
                iVarArr = iVarArr3;
                a.a("FileCache", "Exception while cleaning up templates directory at " + iVar.f5775c.getPath(), e);
                e.printStackTrace();
                i2++;
                hVar = this;
                iVarArr3 = iVarArr;
            }
        }
    }

    public synchronized byte[] a(File file) {
        byte[] bArr;
        bArr = null;
        if (file == null) {
            return null;
        }
        try {
            bArr = l0.b(file);
        } catch (Exception e2) {
            a.a("FileCache", "Error loading cache from disk", e2);
            a.a(getClass(), "readByteArrayFromDisk", e2);
        }
        return bArr;
    }

    public boolean b(String str) {
        if (d().f5774b == null || str == null) {
            return false;
        }
        return new File(d().f5774b, str).exists();
    }

    public JSONObject c() {
        String[] list;
        JSONObject jSONObject = new JSONObject();
        try {
            File file = d().f5773a;
            for (String next : this.f5769a.get().u) {
                if (!next.equals("templates")) {
                    File file2 = new File(file, next);
                    JSONArray jSONArray = new JSONArray();
                    if (file2.exists() && (list = file2.list()) != null) {
                        for (String str : list) {
                            if (!str.equals(".nomedia") && !str.endsWith(".tmp")) {
                                jSONArray.put(str);
                            }
                        }
                    }
                    g.a(jSONObject, next, jSONArray);
                }
            }
        } catch (Exception e2) {
            a.a(h.class, "getWebViewCacheAssets", e2);
        }
        return jSONObject;
    }

    public i d() {
        if (a()) {
            i iVar = this.f5771c.get();
            if (iVar == null) {
                try {
                    File a2 = this.f5772d.a();
                    if (a2 != null) {
                        this.f5771c.compareAndSet(null, new i(a2));
                        iVar = this.f5771c.get();
                    }
                } catch (Exception e2) {
                    a.a(h.class, "currentLocations", e2);
                }
            }
            if (iVar != null) {
                return iVar;
            }
        }
        return this.f5770b;
    }

    public JSONObject e() {
        JSONObject jSONObject = new JSONObject();
        i iVar = this.f5771c.get();
        if (iVar != null) {
            g.a(jSONObject, ".chartboost-external-folder-size", Long.valueOf(b(iVar.f5773a)));
        }
        g.a(jSONObject, ".chartboost-internal-folder-size", Long.valueOf(b(this.f5770b.f5773a)));
        File file = d().f5773a;
        String[] list = file.list();
        if (list != null && list.length > 0) {
            for (String file2 : list) {
                File file3 = new File(file, file2);
                JSONObject jSONObject2 = new JSONObject();
                g.a(jSONObject2, file3.getName() + "-size", Long.valueOf(b(file3)));
                String[] list2 = file3.list();
                if (list2 != null) {
                    g.a(jSONObject2, "count", Integer.valueOf(list2.length));
                }
                g.a(jSONObject, file3.getName(), jSONObject2);
            }
        }
        return jSONObject;
    }

    public JSONArray b() {
        JSONArray jSONArray = new JSONArray();
        String[] list = d().f5776d.list();
        if (list != null) {
            for (String str : list) {
                if (!str.equals(".nomedia") && !str.endsWith(".tmp")) {
                    jSONArray.put(str);
                }
            }
        }
        return jSONArray;
    }

    public String a(String str) {
        File file = new File(d().f5776d, str);
        if (file.exists()) {
            return file.getPath();
        }
        return null;
    }

    public boolean a() {
        try {
            String b2 = this.f5772d.b();
            if (b2 != null && b2.equals("mounted") && !n.o) {
                return true;
            }
        } catch (Exception e2) {
            a.a(h.class, "isExternalStorageAvailable", e2);
        }
        a.e("FileCache", "External Storage unavailable");
        return false;
    }

    public long b(File file) {
        long j2 = 0;
        if (file != null) {
            try {
                if (file.isDirectory()) {
                    File[] listFiles = file.listFiles();
                    if (listFiles == null) {
                        return 0;
                    }
                    for (File b2 : listFiles) {
                        j2 += b(b2);
                    }
                    return j2;
                }
            } catch (Exception e2) {
                a.a(h.class, "getFolderSize", e2);
                return 0;
            }
        }
        if (file != null) {
            return file.length();
        }
        return 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0040 A[SYNTHETIC, Splitter:B:29:0x0040] */
    /* JADX WARNING: Removed duplicated region for block: B:34:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:35:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c(java.io.File r6) {
        /*
            r5 = this;
            java.lang.String r0 = "FileCache"
            r1 = 0
            java.io.RandomAccessFile r2 = new java.io.RandomAccessFile     // Catch:{ FileNotFoundException -> 0x0034, IOException -> 0x0028 }
            java.lang.String r3 = "rw"
            r2.<init>(r6, r3)     // Catch:{ FileNotFoundException -> 0x0034, IOException -> 0x0028 }
            r3 = 0
            r2.seek(r3)     // Catch:{ FileNotFoundException -> 0x0023, IOException -> 0x0020, all -> 0x001d }
            int r6 = r2.read()     // Catch:{ FileNotFoundException -> 0x0023, IOException -> 0x0020, all -> 0x001d }
            r2.seek(r3)     // Catch:{ FileNotFoundException -> 0x0023, IOException -> 0x0020, all -> 0x001d }
            r2.write(r6)     // Catch:{ FileNotFoundException -> 0x0023, IOException -> 0x0020, all -> 0x001d }
            r2.close()     // Catch:{ IOException -> 0x003d }
            goto L_0x003d
        L_0x001d:
            r6 = move-exception
            r1 = r2
            goto L_0x003e
        L_0x0020:
            r6 = move-exception
            r1 = r2
            goto L_0x0029
        L_0x0023:
            r6 = move-exception
            r1 = r2
            goto L_0x0035
        L_0x0026:
            r6 = move-exception
            goto L_0x003e
        L_0x0028:
            r6 = move-exception
        L_0x0029:
            java.lang.String r2 = "IOException when attempting to touch file"
            com.chartboost.sdk.c.a.a(r0, r2, r6)     // Catch:{ all -> 0x0026 }
            if (r1 == 0) goto L_0x003d
        L_0x0030:
            r1.close()     // Catch:{ IOException -> 0x003d }
            goto L_0x003d
        L_0x0034:
            r6 = move-exception
        L_0x0035:
            java.lang.String r2 = "File not found when attempting to touch"
            com.chartboost.sdk.c.a.a(r0, r2, r6)     // Catch:{ all -> 0x0026 }
            if (r1 == 0) goto L_0x003d
            goto L_0x0030
        L_0x003d:
            return
        L_0x003e:
            if (r1 == 0) goto L_0x0043
            r1.close()     // Catch:{ IOException -> 0x0043 }
        L_0x0043:
            goto L_0x0045
        L_0x0044:
            throw r6
        L_0x0045:
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.c.h.c(java.io.File):void");
    }
}
