package com.chartboost.sdk;

import android.app.Activity;
import android.os.Handler;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.chartboost.sdk.c.b;
import com.chartboost.sdk.d.a;
import com.chartboost.sdk.d.d;
import com.chartboost.sdk.d.f;
import com.chartboost.sdk.h;
import com.chartboost.sdk.o.f1;
import com.chartboost.sdk.o.g0;
import com.chartboost.sdk.o.p0;
import com.chartboost.sdk.o.y0;
import com.chartboost.sdk.o.z;
import java.util.concurrent.atomic.AtomicReference;

public class i {

    /* renamed from: a  reason: collision with root package name */
    final z f5877a;

    /* renamed from: b  reason: collision with root package name */
    private final y0 f5878b;

    /* renamed from: c  reason: collision with root package name */
    private final AtomicReference<f> f5879c;

    /* renamed from: d  reason: collision with root package name */
    private final Handler f5880d;

    /* renamed from: e  reason: collision with root package name */
    g0 f5881e = null;

    /* renamed from: f  reason: collision with root package name */
    private int f5882f = -1;

    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ d f5883a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Activity f5884b;

        a(d dVar, Activity activity) {
            this.f5883a = dVar;
            this.f5884b = activity;
        }

        public void run() {
            d dVar = this.f5883a;
            dVar.l = 4;
            int i2 = 1;
            if (dVar.r.f5817b == 1) {
                i2 = 6;
            }
            Integer a2 = z.a(this.f5883a.r.o);
            if (a2 != null) {
                i2 = a2.intValue();
            }
            h hVar = this.f5883a.f5836g;
            hVar.getClass();
            h.d dVar2 = new h.d(13);
            d dVar3 = this.f5883a;
            dVar2.f5875d = dVar3;
            dVar2.f5873b = this.f5884b;
            i.this.f5877a.a(i2, dVar3, dVar2);
        }
    }

    public i(z zVar, y0 y0Var, AtomicReference<f> atomicReference, Handler handler) {
        this.f5877a = zVar;
        this.f5878b = y0Var;
        this.f5879c = atomicReference;
        this.f5880d = handler;
    }

    private void e(d dVar) {
        int i2;
        g0 g0Var = this.f5881e;
        if (g0Var == null || g0Var.e() == dVar) {
            int i3 = 1;
            boolean z = dVar.l != 2;
            dVar.l = 2;
            Activity b2 = dVar.f5836g.b();
            a.c cVar = b2 == null ? a.c.NO_HOST_ACTIVITY : null;
            if (cVar == null) {
                cVar = dVar.j();
            }
            if (cVar != null) {
                com.chartboost.sdk.c.a.b("CBViewController", "Unable to create the view while trying th display the impression");
                dVar.a(cVar);
                return;
            }
            if (this.f5881e == null) {
                l a2 = l.a();
                g0 g0Var2 = new g0(b2, dVar);
                a2.a(g0Var2);
                this.f5881e = g0Var2;
                b2.addContentView(this.f5881e, new FrameLayout.LayoutParams(-1, -1));
            }
            b.a(b2, dVar.r.f5817b, this.f5879c.get());
            if (f1.e().a(11) && this.f5882f == -1 && ((i2 = dVar.n) == 1 || i2 == 2)) {
                this.f5882f = b2.getWindow().getDecorView().getSystemUiVisibility();
                a.g(b2);
            }
            this.f5881e.a();
            com.chartboost.sdk.c.a.e("CBViewController", "Displaying the impression");
            g0 g0Var3 = this.f5881e;
            dVar.v = g0Var3;
            if (z) {
                if (dVar.r.f5817b == 0) {
                    g0Var3.c().a(this.f5877a, dVar.r);
                }
                if (dVar.r.f5817b == 1) {
                    i3 = 6;
                }
                Integer a3 = z.a(dVar.r.o);
                if (a3 != null) {
                    i3 = a3.intValue();
                }
                dVar.m();
                h hVar = dVar.f5836g;
                hVar.getClass();
                h.d dVar2 = new h.d(12);
                dVar2.f5875d = dVar;
                this.f5877a.a(i3, dVar, dVar2, this);
                this.f5878b.a();
                return;
            }
            return;
        }
        com.chartboost.sdk.c.a.b("CBViewController", "Impression already visible");
        dVar.a(a.c.IMPRESSION_ALREADY_VISIBLE);
    }

    /* access modifiers changed from: package-private */
    public void a(d dVar) {
        if (dVar.l != 0) {
            e(dVar);
        }
    }

    public void b(d dVar) {
        com.chartboost.sdk.c.a.e("CBViewController", "Dismissing impression");
        a aVar = new a(dVar, dVar.f5836g.b());
        if (dVar.x) {
            dVar.a(aVar);
        } else {
            aVar.run();
        }
    }

    /* access modifiers changed from: package-private */
    public void c(d dVar) {
        com.chartboost.sdk.c.a.e("CBViewController", "Removing impression silently");
        dVar.i();
        try {
            ((ViewGroup) this.f5881e.getParent()).removeView(this.f5881e);
        } catch (Exception e2) {
            com.chartboost.sdk.c.a.a("CBViewController", "Exception removing impression silently", e2);
            com.chartboost.sdk.e.a.a(i.class, "removeImpressionSilently", e2);
        }
        this.f5881e = null;
    }

    public void d(d dVar) {
        com.chartboost.sdk.c.a.e("CBViewController", "Removing impression");
        dVar.l = 5;
        dVar.h();
        this.f5881e = null;
        this.f5878b.b();
        Handler handler = this.f5880d;
        p0 p0Var = dVar.f5830a;
        p0Var.getClass();
        handler.post(new p0.a(3, dVar.m, null));
        if (dVar.v()) {
            Handler handler2 = this.f5880d;
            p0 p0Var2 = dVar.f5830a;
            p0Var2.getClass();
            handler2.post(new p0.a(2, dVar.m, null));
        }
        a(dVar.f5836g);
    }

    /* access modifiers changed from: package-private */
    public void a(d dVar, Activity activity) {
        h hVar = dVar.f5836g;
        hVar.getClass();
        h.d dVar2 = new h.d(14);
        dVar2.f5875d = dVar;
        this.f5880d.post(dVar2);
        dVar.l();
        b.b(activity, dVar.r.f5817b, this.f5879c.get());
        if (this.f5882f != -1) {
            int i2 = dVar.n;
            if (i2 == 1 || i2 == 2) {
                activity.getWindow().getDecorView().setSystemUiVisibility(this.f5882f);
                this.f5882f = -1;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(h hVar) {
        com.chartboost.sdk.c.a.e("CBViewController", "Attempting to close impression activity");
        Activity b2 = hVar.b();
        if (b2 != null && (b2 instanceof CBImpressionActivity)) {
            com.chartboost.sdk.c.a.e("CBViewController", "Closing impression activity");
            hVar.f();
            b2.finish();
        }
    }

    public g0 a() {
        return this.f5881e;
    }
}
