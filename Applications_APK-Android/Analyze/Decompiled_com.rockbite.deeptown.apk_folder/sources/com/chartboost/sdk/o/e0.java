package com.chartboost.sdk.o;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public final class e0 extends Animation {

    /* renamed from: a  reason: collision with root package name */
    private final float f5997a;

    /* renamed from: b  reason: collision with root package name */
    private final float f5998b;

    /* renamed from: c  reason: collision with root package name */
    private final float f5999c;

    /* renamed from: d  reason: collision with root package name */
    private final float f6000d;

    /* renamed from: e  reason: collision with root package name */
    private boolean f6001e = true;

    /* renamed from: f  reason: collision with root package name */
    private Camera f6002f;

    public e0(float f2, float f3, float f4, float f5, boolean z) {
        this.f5997a = f2;
        this.f5998b = f3;
        this.f5999c = f4;
        this.f6000d = f5;
        this.f6001e = z;
    }

    /* access modifiers changed from: protected */
    public void applyTransformation(float f2, Transformation transformation) {
        float f3 = this.f5997a;
        float f4 = f3 + ((this.f5998b - f3) * f2);
        Camera camera = this.f6002f;
        Matrix matrix = transformation.getMatrix();
        camera.save();
        if (this.f6001e) {
            camera.rotateY(f4);
        } else {
            camera.rotateX(f4);
        }
        camera.getMatrix(matrix);
        camera.restore();
        matrix.preTranslate(-this.f5999c, -this.f6000d);
        matrix.postTranslate(this.f5999c, this.f6000d);
    }

    public void initialize(int i2, int i3, int i4, int i5) {
        super.initialize(i2, i3, i4, i5);
        this.f6002f = new Camera();
    }
}
