package com.chartboost.sdk.o;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;
import android.widget.TextView;
import com.chartboost.sdk.c.j;

public class c0 extends ImageView {

    /* renamed from: a  reason: collision with root package name */
    private j f5968a = null;

    /* renamed from: b  reason: collision with root package name */
    protected TextView f5969b = null;

    public c0(Context context) {
        super(context);
    }

    public void a(j jVar) {
        if (jVar != null && jVar.c() && this.f5968a != jVar) {
            this.f5968a = jVar;
            setImageDrawable(new BitmapDrawable(jVar.d()));
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        a(canvas);
    }

    public void setImageBitmap(Bitmap bitmap) {
        this.f5968a = null;
        setImageDrawable(new BitmapDrawable(bitmap));
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas) {
        TextView textView = this.f5969b;
        if (textView != null) {
            textView.layout(0, 0, canvas.getWidth(), canvas.getHeight());
            this.f5969b.setEnabled(isEnabled());
            this.f5969b.setSelected(isSelected());
            if (isFocused()) {
                this.f5969b.requestFocus();
            } else {
                this.f5969b.clearFocus();
            }
            this.f5969b.setPressed(isPressed());
            this.f5969b.draw(canvas);
        }
    }
}
