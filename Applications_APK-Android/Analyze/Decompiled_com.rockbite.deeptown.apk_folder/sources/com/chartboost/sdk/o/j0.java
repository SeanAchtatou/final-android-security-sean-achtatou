package com.chartboost.sdk.o;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Build;
import android.os.Handler;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Window;
import android.webkit.RenderProcessGoneDetail;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import com.appsflyer.share.Constants;
import com.chartboost.sdk.c.g;
import com.chartboost.sdk.c.h;
import com.chartboost.sdk.d.a;
import com.chartboost.sdk.i;
import com.chartboost.sdk.j;
import com.chartboost.sdk.l;
import com.chartboost.sdk.m;
import com.chartboost.sdk.n;
import com.esotericsoftware.spine.Animation;
import com.google.firebase.perf.FirebasePerformance;
import com.tapjoy.TJAdUnitConstants;
import java.io.File;
import java.util.List;
import org.json.JSONObject;

public class j0 extends j {
    int A = 0;
    int B = 0;
    int C = 0;
    int D = 0;
    int E = 0;
    int F = 0;
    int G = 0;
    int H = 0;
    int I = 0;
    int J = 0;
    int K = 0;
    int L = 0;
    int M = 0;
    int N = -1;
    boolean O = true;
    int P = -1;
    private int Q = 0;
    private final h l;
    private final k m;
    final com.chartboost.sdk.e.a n;
    final i o;
    final SharedPreferences p;
    public String q = "UNKNOWN";
    private String r = null;
    String s = null;
    protected int t = 1;
    private float u = Animation.CurveTimeline.LINEAR;
    private float v = Animation.CurveTimeline.LINEAR;
    private boolean w = false;
    long x = 0;
    long y = 0;
    boolean z = false;

    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ d f6052a;

        a(d dVar) {
            this.f6052a = dVar;
        }

        public void run() {
            if (this.f6052a.f6057h != null) {
                j0.this.g("onForeground");
                this.f6052a.f6057h.onResume();
            }
        }
    }

    class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ d f6054a;

        b(d dVar) {
            this.f6054a = dVar;
        }

        public void run() {
            if (this.f6054a.f6057h != null) {
                j0.this.g("onBackground");
                this.f6054a.f6057h.onPause();
            }
        }
    }

    private class c extends WebViewClient {
        private c() {
        }

        private void a(String str) {
            com.chartboost.sdk.c.a.b("CBWebViewProtocol", str);
            j0.this.a(a.c.WEB_VIEW_CLIENT_RECEIVED_ERROR);
            j0 j0Var = j0.this;
            j0Var.z = true;
            j0Var.o.d(j0Var.f5892g);
        }

        public void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            j0 j0Var = j0.this;
            j0Var.z = true;
            j0Var.y = System.currentTimeMillis();
            StringBuilder sb = new StringBuilder();
            sb.append("Total web view load response time ");
            j0 j0Var2 = j0.this;
            sb.append((j0Var2.y - j0Var2.x) / 1000);
            com.chartboost.sdk.c.a.a("CBWebViewProtocol", sb.toString());
            Context context = webView.getContext();
            if (context != null) {
                j0.this.c(context);
                j0.this.d(context);
                j0.this.o();
            }
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            super.onPageStarted(webView, str, bitmap);
            if (Build.VERSION.SDK_INT >= 26) {
                PackageInfo currentWebViewPackage = WebView.getCurrentWebViewPackage();
                if (currentWebViewPackage != null) {
                    com.chartboost.sdk.c.a.a("CBWebViewProtocol", "WebView version: " + currentWebViewPackage.versionName);
                    return;
                }
                a(u.a(n.n).equalsIgnoreCase("watch") ? "No webview support." : "Device was not set up correctly.");
            }
        }

        public void onReceivedError(WebView webView, int i2, String str, String str2) {
            a("Error loading " + str2 + ": " + str);
        }

        @TargetApi(23)
        public void onReceivedHttpError(WebView webView, WebResourceRequest webResourceRequest, WebResourceResponse webResourceResponse) {
            StringBuilder sb = new StringBuilder();
            sb.append("Error loading ");
            sb.append(webResourceRequest.getUrl().toString());
            sb.append(": ");
            sb.append(webResourceResponse == null ? "unknown error" : webResourceResponse.getReasonPhrase());
            com.chartboost.sdk.c.a.a("CBWebViewProtocol", sb.toString());
        }

        @TargetApi(26)
        public boolean onRenderProcessGone(WebView webView, RenderProcessGoneDetail renderProcessGoneDetail) {
            String str;
            if (renderProcessGoneDetail.didCrash()) {
                str = "Webview crashed: " + renderProcessGoneDetail.toString();
            } else {
                str = "Webview killed, likely due to low memory";
            }
            a(str);
            return true;
        }

        @TargetApi(24)
        public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
            return false;
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            return false;
        }

        /* synthetic */ c(j0 j0Var, a aVar) {
            this();
        }

        @TargetApi(23)
        public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
            if (webResourceRequest.isForMainFrame()) {
                a("Error loading " + webResourceRequest.getUrl().toString() + ": " + ((Object) webResourceError.getDescription()));
            }
        }
    }

    public class d extends j.b {

        /* renamed from: h  reason: collision with root package name */
        public i0 f6057h;

        /* renamed from: i  reason: collision with root package name */
        public h0 f6058i;

        /* renamed from: j  reason: collision with root package name */
        public RelativeLayout f6059j;

        /* renamed from: k  reason: collision with root package name */
        public RelativeLayout f6060k;

        class a implements Runnable {
            a(j0 j0Var) {
            }

            public void run() {
                if (!j0.this.z) {
                    com.chartboost.sdk.c.a.a("CBWebViewProtocol", "Webview seems to be taking more time loading the html content, so closing the view.");
                    j0.this.a(a.c.WEB_VIEW_PAGE_LOAD_TIMEOUT);
                }
            }
        }

        public d(Context context, String str) {
            super(context);
            setFocusable(false);
            l a2 = l.a();
            RelativeLayout relativeLayout = new RelativeLayout(context);
            a2.a(relativeLayout);
            this.f6059j = relativeLayout;
            RelativeLayout relativeLayout2 = new RelativeLayout(context);
            a2.a(relativeLayout2);
            this.f6060k = relativeLayout2;
            i0 i0Var = new i0(context);
            a2.a(i0Var);
            this.f6057h = i0Var;
            m.a(context, this.f6057h, j0.this.p);
            i0 i0Var2 = this.f6057h;
            c cVar = new c(j0.this, null);
            a2.a(cVar);
            i0Var2.setWebViewClient(cVar);
            h0 h0Var = new h0(this.f6059j, this.f6060k, null, this.f6057h, j0.this, j0.this.f5886a);
            a2.a(h0Var);
            this.f6058i = h0Var;
            this.f6057h.setWebChromeClient(this.f6058i);
            if (f1.e().a(19)) {
                WebView.setWebContentsDebuggingEnabled(true);
            }
            this.f6057h.loadDataWithBaseURL(j0.this.s, str, "text/html", "utf-8", null);
            this.f6059j.addView(this.f6057h);
            this.f6057h.getSettings().setSupportZoom(false);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
            this.f6059j.setLayoutParams(layoutParams);
            this.f6057h.setLayoutParams(layoutParams);
            this.f6057h.setBackgroundColor(0);
            this.f6060k.setVisibility(8);
            this.f6060k.setLayoutParams(layoutParams);
            addView(this.f6059j);
            addView(this.f6060k);
            j0.this.x = System.currentTimeMillis();
            if (context instanceof Activity) {
                j0.this.N = ((Activity) context).getRequestedOrientation();
            } else {
                j0.this.N = -1;
            }
            b1.a(this.f6057h, j0.this.f5892g.r.r);
            j0.this.f5886a.postDelayed(new a(j0.this), 3000);
        }

        /* access modifiers changed from: protected */
        public void a(int i2, int i3) {
        }
    }

    public j0(com.chartboost.sdk.d.d dVar, h hVar, k kVar, SharedPreferences sharedPreferences, com.chartboost.sdk.e.a aVar, Handler handler, com.chartboost.sdk.h hVar2, i iVar) {
        super(dVar, handler, hVar2);
        this.l = hVar;
        this.m = kVar;
        this.n = aVar;
        this.o = iVar;
        this.p = sharedPreferences;
    }

    /* access modifiers changed from: protected */
    public j.b a(Context context) {
        return new d(context, this.r);
    }

    public String a(int i2) {
        return i2 != -1 ? i2 != 0 ? i2 != 1 ? "error" : TJAdUnitConstants.String.PORTRAIT : TJAdUnitConstants.String.LANDSCAPE : "none";
    }

    public void b(String str) {
        List<String> list;
        if (this.f5892g.r.n != null && !TextUtils.isEmpty(str) && (list = this.f5892g.r.n.get(str)) != null) {
            for (String str2 : list) {
                if (!str2.isEmpty()) {
                    this.m.a(new g(FirebasePerformance.HttpMethod.GET, str2, 2, null));
                    com.chartboost.sdk.c.a.a("CBWebViewProtocol", "###### Sending VAST Tracking Event: " + str2);
                }
            }
        }
    }

    public void c(String str) {
        com.chartboost.sdk.e.a aVar = this.n;
        com.chartboost.sdk.d.d dVar = this.f5892g;
        String a2 = dVar.f5830a.a(dVar.r.f5817b);
        com.chartboost.sdk.d.d dVar2 = this.f5892g;
        aVar.a(a2, dVar2.m, dVar2.o(), str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.e.a.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, boolean):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      com.chartboost.sdk.e.a.a(java.lang.String, java.lang.String, long, long, long):void
      com.chartboost.sdk.e.a.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, boolean):void */
    public void d(String str) {
        if (f1.e().a(str)) {
            str = "Unknown Webview error";
        }
        com.chartboost.sdk.e.a aVar = this.n;
        com.chartboost.sdk.d.d dVar = this.f5892g;
        String a2 = dVar.f5830a.a(dVar.r.f5817b);
        com.chartboost.sdk.d.d dVar2 = this.f5892g;
        aVar.a(a2, dVar2.m, dVar2.o(), str, true);
        com.chartboost.sdk.c.a.b("CBWebViewProtocol", "Webview error occurred closing the webview" + str);
        a(a.c.ERROR_LOADING_WEB_VIEW);
        h();
    }

    public int f(String str) {
        if (str.equals(TJAdUnitConstants.String.PORTRAIT)) {
            return 1;
        }
        return str.equals(TJAdUnitConstants.String.LANDSCAPE) ? 0 : -1;
    }

    /* access modifiers changed from: package-private */
    public void g(String str) {
        if (e().f6057h != null) {
            String str2 = "javascript:Chartboost.EventHandler.handleNativeEvent(\"" + str + "\", \"\")";
            com.chartboost.sdk.c.a.a("CBWebViewProtocol", "Calling native to javascript: " + str2);
            e().f6057h.loadUrl(str2);
        }
    }

    public void h() {
        super.h();
        r();
    }

    public float j() {
        return this.u;
    }

    public float k() {
        return this.v;
    }

    public boolean l() {
        if (this.Q == 2 && this.f5892g.f5830a.f6123a == 1) {
            return true;
        }
        d();
        h();
        return true;
    }

    public void m() {
        super.m();
        d y2 = e();
        if (y2 != null && y2.f6057h != null) {
            this.f5886a.post(new a(y2));
            this.n.d(this.q, this.f5892g.o());
        }
    }

    public void n() {
        super.n();
        d y2 = e();
        if (y2 != null && y2.f6057h != null) {
            this.f5886a.post(new b(y2));
            this.n.e(this.q, this.f5892g.o());
        }
    }

    /* access modifiers changed from: package-private */
    public void o() {
        d y2 = e();
        if (y2 == null || !this.z) {
            this.J = this.F;
            this.K = this.G;
            this.L = this.H;
            this.M = this.I;
            return;
        }
        int[] iArr = new int[2];
        y2.getLocationInWindow(iArr);
        int i2 = iArr[0];
        int i3 = iArr[1] - this.E;
        int width = y2.getWidth();
        int height = y2.getHeight();
        this.F = i2;
        this.G = i3;
        this.H = i2 + width;
        this.I = i3 + height;
        this.J = this.F;
        this.K = this.G;
        this.L = this.H;
        this.M = this.I;
    }

    public String p() {
        return g.a(g.a("allowOrientationChange", Boolean.valueOf(this.O)), g.a("forceOrientation", a(this.P))).toString();
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002a, code lost:
        if (r0.getResources().getConfiguration().orientation == 1) goto L_0x0015;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void q() {
        /*
            r4 = this;
            com.chartboost.sdk.h r0 = r4.f5887b
            android.app.Activity r0 = r0.b()
            if (r0 == 0) goto L_0x0030
            boolean r1 = com.chartboost.sdk.c.b.a(r0)
            if (r1 == 0) goto L_0x000f
            goto L_0x0030
        L_0x000f:
            int r1 = r4.P
            r2 = 0
            r3 = 1
            if (r1 != r3) goto L_0x0017
        L_0x0015:
            r2 = 1
            goto L_0x002d
        L_0x0017:
            if (r1 != 0) goto L_0x001a
            goto L_0x002d
        L_0x001a:
            boolean r1 = r4.O
            if (r1 == 0) goto L_0x0020
            r2 = -1
            goto L_0x002d
        L_0x0020:
            android.content.res.Resources r1 = r0.getResources()
            android.content.res.Configuration r1 = r1.getConfiguration()
            int r1 = r1.orientation
            if (r1 != r3) goto L_0x002d
            goto L_0x0015
        L_0x002d:
            r0.setRequestedOrientation(r2)
        L_0x0030:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.o.j0.q():void");
    }

    /* access modifiers changed from: package-private */
    public void r() {
        Activity b2 = this.f5887b.b();
        if (b2 != null && !com.chartboost.sdk.c.b.a(b2)) {
            int requestedOrientation = b2.getRequestedOrientation();
            int i2 = this.N;
            if (requestedOrientation != i2) {
                b2.setRequestedOrientation(i2);
            }
            this.O = true;
            this.P = -1;
        }
    }

    public String s() {
        return g.a(g.a("width", Integer.valueOf(this.C)), g.a("height", Integer.valueOf(this.D))).toString();
    }

    public String t() {
        return g.a(g.a("width", Integer.valueOf(this.A)), g.a("height", Integer.valueOf(this.B))).toString();
    }

    public String u() {
        o();
        return g.a(g.a("x", Integer.valueOf(this.F)), g.a("y", Integer.valueOf(this.G)), g.a("width", Integer.valueOf(this.H)), g.a("height", Integer.valueOf(this.I))).toString();
    }

    public String v() {
        o();
        return g.a(g.a("x", Integer.valueOf(this.J)), g.a("y", Integer.valueOf(this.K)), g.a("width", Integer.valueOf(this.L)), g.a("height", Integer.valueOf(this.M))).toString();
    }

    public void w() {
        if (this.t <= 1) {
            this.f5892g.e();
            this.t++;
        }
    }

    public void x() {
        this.n.c(this.q, this.f5892g.o());
    }

    /* renamed from: y */
    public d e() {
        return (d) super.e();
    }

    public void z() {
        com.chartboost.sdk.d.d dVar = this.f5892g;
        if (dVar.l == 2 && !this.w) {
            this.n.d("", dVar.o());
            this.f5892g.p();
            this.w = true;
            b1.c();
        }
    }

    public boolean a(JSONObject jSONObject) {
        File file = this.l.d().f5773a;
        if (file == null) {
            com.chartboost.sdk.c.a.b("CBWebViewProtocol", "External Storage path is unavailable or media not mounted");
            a(a.c.ERROR_LOADING_WEB_VIEW);
            return false;
        }
        this.s = "file://" + file.getAbsolutePath() + Constants.URL_PATH_DELIMITER;
        if (f1.e().a(this.f5892g.r.f5820e)) {
            com.chartboost.sdk.c.a.b("CBWebViewProtocol", "Invalid adId being passed in the response");
            a(a.c.ERROR_DISPLAYING_VIEW);
            return false;
        }
        String str = this.f5892g.q;
        if (str == null) {
            com.chartboost.sdk.c.a.b("CBWebViewProtocol", "No html data found in memory");
            a(a.c.ERROR_LOADING_WEB_VIEW);
            return false;
        }
        this.r = str;
        return true;
    }

    public void c(JSONObject jSONObject) {
        this.O = jSONObject.optBoolean("allowOrientationChange", this.O);
        this.P = f(jSONObject.optString("forceOrientation", a(this.P)));
        q();
    }

    public void e(String str) {
        if (f1.e().a(str)) {
            str = "Unknown Webview warning message";
        }
        com.chartboost.sdk.e.a aVar = this.n;
        com.chartboost.sdk.d.d dVar = this.f5892g;
        String a2 = dVar.f5830a.a(dVar.r.f5817b);
        com.chartboost.sdk.d.d dVar2 = this.f5892g;
        aVar.b(a2, dVar2.m, dVar2.o(), str);
        com.chartboost.sdk.c.a.d("CBWebViewProtocol", "Webview warning occurred closing the webview" + str);
    }

    /* access modifiers changed from: package-private */
    public void d(Context context) {
        if (context instanceof Activity) {
            Window window = ((Activity) context).getWindow();
            Rect rect = new Rect();
            window.getDecorView().getWindowVisibleDisplayFrame(rect);
            this.E = a(window);
            if (this.A == 0 || this.B == 0) {
                c(context);
            }
            int width = rect.width();
            int i2 = this.B - this.E;
            if (width != this.C || i2 != this.D) {
                this.C = width;
                this.D = i2;
            }
        }
    }

    public void b(int i2) {
        this.Q = i2;
    }

    /* access modifiers changed from: package-private */
    public void c(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        this.A = displayMetrics.widthPixels;
        this.B = displayMetrics.heightPixels;
    }

    public void b(float f2) {
        this.u = f2;
    }

    /* access modifiers changed from: package-private */
    public int a(Window window) {
        return window.findViewById(16908290).getTop();
    }

    public void a(float f2) {
        this.v = f2;
    }

    public void d() {
        b1.d();
        d y2 = e();
        if (y2 != null) {
            if (y2.f6057h != null) {
                com.chartboost.sdk.c.a.a("CBWebViewProtocol", "Destroying the webview object and cleaning up the references");
                y2.f6057h.destroy();
                y2.f6057h = null;
            }
            if (y2.f6058i != null) {
                y2.f6058i = null;
            }
            if (y2.f6059j != null) {
                y2.f6059j = null;
            }
            if (y2.f6060k != null) {
                y2.f6060k = null;
            }
        }
        super.d();
    }
}
