package com.chartboost.sdk.o;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.chartboost.sdk.c.a;
import com.chartboost.sdk.o.y;
import com.tapjoy.TJAdUnitConstants;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;

public class w extends SurfaceView implements MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnVideoSizeChangedListener, SurfaceHolder.Callback, y.a {

    /* renamed from: a  reason: collision with root package name */
    private Uri f6198a;

    /* renamed from: b  reason: collision with root package name */
    private int f6199b;

    /* renamed from: c  reason: collision with root package name */
    private int f6200c = 0;

    /* renamed from: d  reason: collision with root package name */
    private int f6201d = 0;

    /* renamed from: e  reason: collision with root package name */
    private SurfaceHolder f6202e = null;

    /* renamed from: f  reason: collision with root package name */
    private MediaPlayer f6203f = null;

    /* renamed from: g  reason: collision with root package name */
    private int f6204g;

    /* renamed from: h  reason: collision with root package name */
    private int f6205h;

    /* renamed from: i  reason: collision with root package name */
    private int f6206i;

    /* renamed from: j  reason: collision with root package name */
    private int f6207j;

    /* renamed from: k  reason: collision with root package name */
    private MediaPlayer.OnCompletionListener f6208k;
    private MediaPlayer.OnPreparedListener l;
    private MediaPlayer.OnErrorListener m;
    private int n;

    public w(Context context) {
        super(context);
        f();
    }

    private void f() {
        this.f6204g = 0;
        this.f6205h = 0;
        getHolder().addCallback(this);
        getHolder().setType(3);
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus();
        this.f6200c = 0;
        this.f6201d = 0;
    }

    private void g() {
        if (this.f6198a != null && this.f6202e != null) {
            Intent intent = new Intent("com.android.music.musicservicecommand");
            intent.putExtra(TJAdUnitConstants.String.COMMAND, "pause");
            getContext().sendBroadcast(intent);
            a(false);
            try {
                this.f6203f = new MediaPlayer();
                this.f6203f.setOnPreparedListener(this);
                this.f6203f.setOnVideoSizeChangedListener(this);
                this.f6199b = -1;
                this.f6203f.setOnCompletionListener(this);
                this.f6203f.setOnErrorListener(this);
                this.f6203f.setOnBufferingUpdateListener(this);
                this.f6203f.setDisplay(this.f6202e);
                this.f6203f.setAudioStreamType(3);
                this.f6203f.setScreenOnWhilePlaying(true);
                FileInputStream fileInputStream = new FileInputStream(new File(this.f6198a.toString()));
                this.f6203f.setDataSource(fileInputStream.getFD());
                fileInputStream.close();
                this.f6203f.prepareAsync();
                this.f6200c = 1;
            } catch (IOException e2) {
                a.c("VideoSurfaceView", "Unable to open content: " + this.f6198a, e2);
                this.f6200c = -1;
                this.f6201d = -1;
                onError(this.f6203f, 1, 0);
            } catch (IllegalArgumentException e3) {
                a.c("VideoSurfaceView", "Unable to open content: " + this.f6198a, e3);
                this.f6200c = -1;
                this.f6201d = -1;
                onError(this.f6203f, 1, 0);
            }
        }
    }

    private boolean h() {
        int i2;
        return (this.f6203f == null || (i2 = this.f6200c) == -1 || i2 == 0 || i2 == 1) ? false : true;
    }

    public void a(int i2, int i3) {
    }

    public void a(Uri uri) {
        a(uri, (Map<String, String>) null);
    }

    public void b() {
        if (h() && this.f6203f.isPlaying()) {
            this.f6203f.pause();
            this.f6200c = 4;
        }
        this.f6201d = 4;
    }

    public int c() {
        if (h()) {
            int i2 = this.f6199b;
            if (i2 > 0) {
                return i2;
            }
            this.f6199b = this.f6203f.getDuration();
            return this.f6199b;
        }
        this.f6199b = -1;
        return this.f6199b;
    }

    public int d() {
        if (h()) {
            return this.f6203f.getCurrentPosition();
        }
        return 0;
    }

    public boolean e() {
        return h() && this.f6203f.isPlaying();
    }

    public void onBufferingUpdate(MediaPlayer mediaPlayer, int i2) {
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        this.f6201d = 5;
        if (this.f6200c != 5) {
            this.f6200c = 5;
            MediaPlayer.OnCompletionListener onCompletionListener = this.f6208k;
            if (onCompletionListener != null) {
                onCompletionListener.onCompletion(this.f6203f);
            }
        }
    }

    public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        a.a("VideoSurfaceView", "Error: " + i2 + "," + i3);
        this.f6200c = -1;
        this.f6201d = -1;
        MediaPlayer.OnErrorListener onErrorListener = this.m;
        if (onErrorListener == null || onErrorListener.onError(this.f6203f, i2, i3)) {
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int i4;
        int defaultSize = SurfaceView.getDefaultSize(0, i2);
        int defaultSize2 = SurfaceView.getDefaultSize(0, i3);
        int i5 = this.f6204g;
        if (i5 > 0 && (i4 = this.f6205h) > 0) {
            int min = Math.min(defaultSize2, Math.round((((float) i4) / ((float) i5)) * ((float) defaultSize)));
            defaultSize = Math.min(defaultSize, Math.round((((float) this.f6204g) / ((float) this.f6205h)) * ((float) defaultSize2)));
            defaultSize2 = min;
        }
        setMeasuredDimension(defaultSize, defaultSize2);
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        this.f6200c = 2;
        this.f6204g = mediaPlayer.getVideoWidth();
        this.f6205h = mediaPlayer.getVideoHeight();
        MediaPlayer.OnPreparedListener onPreparedListener = this.l;
        if (onPreparedListener != null) {
            onPreparedListener.onPrepared(this.f6203f);
        }
        int i2 = this.n;
        if (i2 != 0) {
            a(i2);
        }
        if (this.f6204g != 0 && this.f6205h != 0) {
            getHolder().setFixedSize(this.f6204g, this.f6205h);
            if (this.f6206i == this.f6204g && this.f6207j == this.f6205h && this.f6201d == 3) {
                a();
            }
        } else if (this.f6201d == 3) {
            a();
        }
    }

    public void onVideoSizeChanged(MediaPlayer mediaPlayer, int i2, int i3) {
        this.f6204g = mediaPlayer.getVideoWidth();
        this.f6205h = mediaPlayer.getVideoHeight();
        if (this.f6204g != 0 && this.f6205h != 0) {
            getHolder().setFixedSize(this.f6204g, this.f6205h);
        }
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
        this.f6206i = i3;
        this.f6207j = i4;
        boolean z = true;
        boolean z2 = this.f6201d == 3;
        if (!(this.f6204g == i3 && this.f6205h == i4)) {
            z = false;
        }
        if (this.f6203f != null && z2 && z) {
            int i5 = this.n;
            if (i5 != 0) {
                a(i5);
            }
            a();
        }
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        this.f6202e = surfaceHolder;
        g();
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        this.f6202e = null;
        a(true);
    }

    public void a(Uri uri, Map<String, String> map) {
        this.f6198a = uri;
        this.n = 0;
        g();
        requestLayout();
        invalidate();
    }

    public void a(MediaPlayer.OnPreparedListener onPreparedListener) {
        this.l = onPreparedListener;
    }

    public void a(MediaPlayer.OnCompletionListener onCompletionListener) {
        this.f6208k = onCompletionListener;
    }

    public void a(MediaPlayer.OnErrorListener onErrorListener) {
        this.m = onErrorListener;
    }

    private void a(boolean z) {
        MediaPlayer mediaPlayer = this.f6203f;
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            this.f6203f.release();
            this.f6203f = null;
            this.f6200c = 0;
            if (z) {
                this.f6201d = 0;
            }
        }
    }

    public void a() {
        if (h()) {
            this.f6203f.start();
            this.f6200c = 3;
        }
        this.f6201d = 3;
    }

    public void a(int i2) {
        if (h()) {
            this.f6203f.seekTo(i2);
            this.n = 0;
            return;
        }
        this.n = i2;
    }
}
