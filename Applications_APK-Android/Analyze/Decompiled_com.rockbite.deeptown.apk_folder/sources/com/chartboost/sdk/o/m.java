package com.chartboost.sdk.o;

import com.appsflyer.share.Constants;
import com.chartboost.sdk.c.b;
import com.chartboost.sdk.c.e;
import com.chartboost.sdk.c.f;
import com.chartboost.sdk.c.g;
import com.chartboost.sdk.d.a;
import com.chartboost.sdk.n;
import com.facebook.appevents.UserDataStore;
import com.google.firebase.perf.FirebasePerformance;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TapjoyConstants;
import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

public class m extends g<JSONObject> {

    /* renamed from: j  reason: collision with root package name */
    private final String f6101j;

    /* renamed from: k  reason: collision with root package name */
    public final JSONObject f6102k = new JSONObject();
    public final a l;
    public boolean m = false;
    protected final s n;
    private final com.chartboost.sdk.e.a o;

    public interface a {
        void a(m mVar, com.chartboost.sdk.d.a aVar);

        void a(m mVar, JSONObject jSONObject);
    }

    public m(String str, s sVar, com.chartboost.sdk.e.a aVar, int i2, a aVar2) {
        super(FirebasePerformance.HttpMethod.POST, a(str), i2, null);
        this.f6101j = str;
        this.n = sVar;
        this.o = aVar;
        this.l = aVar2;
    }

    /* access modifiers changed from: protected */
    public void c() {
        a(TapjoyConstants.TJC_APP_PLACEMENT, this.n.s);
        a("model", this.n.f6173f);
        a(TapjoyConstants.TJC_DEVICE_TYPE_NAME, this.n.t);
        a("actual_device_type", this.n.u);
        a("os", this.n.f6174g);
        a(UserDataStore.COUNTRY, this.n.f6175h);
        a("language", this.n.f6176i);
        a("sdk", this.n.l);
        a("user_agent", n.w);
        a("timestamp", String.valueOf(TimeUnit.MILLISECONDS.toSeconds(this.n.f6172e.a())));
        boolean z = false;
        a("session", Integer.valueOf(this.n.f6171d.getInt("cbPrefSessionCount", 0)));
        a("reachability", Integer.valueOf(this.n.f6169b.a()));
        a("scale", this.n.r);
        a("is_portrait", Boolean.valueOf(b.a(b.a())));
        a(TJAdUnitConstants.String.BUNDLE, this.n.f6177j);
        a("bundle_id", this.n.f6178k);
        a("carrier", this.n.v);
        a("custom_id", n.f5939b);
        a("mediation", n.f5946i);
        if (n.f5942e != null) {
            a("framework_version", n.f5944g);
            a("wrapper_version", n.f5940c);
        }
        a("rooted_device", Boolean.valueOf(this.n.w));
        a(TapjoyConstants.TJC_DEVICE_TIMEZONE, this.n.x);
        a("mobile_network", this.n.y);
        a("dw", this.n.o);
        a("dh", this.n.p);
        a("dpi", this.n.q);
        a("w", this.n.m);
        a("h", this.n.n);
        a("commit_hash", "7fc7bc32841a43689553f0e08928c7ad6ed7e23b");
        f.a a2 = this.n.f6168a.a();
        a("identity", a2.f5766b);
        int i2 = a2.f5765a;
        if (i2 != -1) {
            if (i2 == 1) {
                z = true;
            }
            a("limit_ad_tracking", Boolean.valueOf(z));
        }
        a("pidatauseconsent", Integer.valueOf(n.x.a()));
        String str = this.n.f6170c.get().f5841a;
        if (!f1.e().a(str)) {
            a("config_variant", str);
        }
        a("certification_providers", b1.e());
    }

    public String d() {
        return e();
    }

    public String e() {
        String str = this.f6101j;
        String str2 = Constants.URL_PATH_DELIMITER;
        if (str == null) {
            return str2;
        }
        StringBuilder sb = new StringBuilder();
        if (this.f6101j.startsWith(str2)) {
            str2 = "";
        }
        sb.append(str2);
        sb.append(this.f6101j);
        return sb.toString();
    }

    public static String a(String str) {
        Object[] objArr = new Object[3];
        objArr[0] = "https://live.chartboost.com";
        String str2 = Constants.URL_PATH_DELIMITER;
        if (str != null && str.startsWith(str2)) {
            str2 = "";
        }
        objArr[1] = str2;
        if (str == null) {
            str = "";
        }
        objArr[2] = str;
        return String.format("%s%s%s", objArr);
    }

    public void a(String str, Object obj) {
        g.a(this.f6102k, str, obj);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.c.g.a(java.lang.String, java.lang.Object):com.chartboost.sdk.c.g$a
     arg types: [java.lang.String, int]
     candidates:
      com.chartboost.sdk.c.g.a(org.json.JSONObject, java.lang.String[]):org.json.JSONObject
      com.chartboost.sdk.c.g.a(java.lang.String, java.lang.Object):com.chartboost.sdk.c.g$a */
    private void a(j jVar, com.chartboost.sdk.d.a aVar) {
        Object obj;
        String str;
        g.a[] aVarArr = new g.a[5];
        aVarArr[0] = g.a("endpoint", e());
        String str2 = "None";
        if (jVar == null) {
            obj = str2;
        } else {
            obj = Integer.valueOf(jVar.f6050a);
        }
        aVarArr[1] = g.a("statuscode", obj);
        if (aVar == null) {
            str = str2;
        } else {
            str = aVar.a().toString();
        }
        aVarArr[2] = g.a("error", str);
        if (aVar != null) {
            str2 = aVar.b();
        }
        aVarArr[3] = g.a("errorDescription", str2);
        aVarArr[4] = g.a("retryCount", (Object) 0);
        this.o.a("request_manager", "request", aVar == null ? "success" : "failure", (String) null, (String) null, (String) null, g.a(aVarArr));
    }

    public h a() {
        c();
        String jSONObject = this.f6102k.toString();
        String str = n.l;
        String str2 = n.m;
        String b2 = e.b(e.a(String.format(Locale.US, "%s %s\n%s\n%s", this.f6010a, d(), str2, jSONObject).getBytes()));
        HashMap hashMap = new HashMap();
        hashMap.put("Accept", "application/json");
        hashMap.put("X-Chartboost-Client", b.b());
        hashMap.put("X-Chartboost-API", "7.5.0");
        hashMap.put("X-Chartboost-App", str);
        hashMap.put("X-Chartboost-Signature", b2);
        return new h(hashMap, jSONObject.getBytes(), "application/json");
    }

    public i<JSONObject> a(j jVar) {
        try {
            if (jVar.f6051b == null) {
                return i.a(new com.chartboost.sdk.d.a(a.d.INVALID_RESPONSE, "Response is not a valid json object"));
            }
            JSONObject jSONObject = new JSONObject(new String(jVar.f6051b));
            com.chartboost.sdk.c.a.c("CBRequest", "Request " + e() + " succeeded. Response code: " + jVar.f6050a + ", body: " + jSONObject.toString(4));
            if (this.m) {
                int optInt = jSONObject.optInt("status");
                if (optInt == 404) {
                    return i.a(new com.chartboost.sdk.d.a(a.d.HTTP_NOT_FOUND, "404 error from server"));
                }
                if (optInt < 200 || optInt > 299) {
                    String str = "Request failed due to status code " + optInt + " in message";
                    com.chartboost.sdk.c.a.b("CBRequest", str);
                    return i.a(new com.chartboost.sdk.d.a(a.d.UNEXPECTED_RESPONSE, str));
                }
            }
            return i.a(jSONObject);
        } catch (Exception e2) {
            com.chartboost.sdk.e.a.a(getClass(), "parseServerResponse", e2);
            return i.a(new com.chartboost.sdk.d.a(a.d.MISCELLANEOUS, e2.getLocalizedMessage()));
        }
    }

    public void a(JSONObject jSONObject, j jVar) {
        a aVar = this.l;
        if (!(aVar == null || jSONObject == null)) {
            aVar.a(this, jSONObject);
        }
        if (this.o != null) {
            a(jVar, (com.chartboost.sdk.d.a) null);
        }
    }

    public void a(com.chartboost.sdk.d.a aVar, j jVar) {
        if (aVar != null) {
            a aVar2 = this.l;
            if (aVar2 != null) {
                aVar2.a(this, aVar);
            }
            if (this.o != null) {
                a(jVar, aVar);
            }
        }
    }
}
