package com.chartboost.sdk.o;

import com.google.firebase.perf.network.FirebasePerfUrlConnection;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class r {
    /* access modifiers changed from: package-private */
    public HttpURLConnection a(g<?> gVar) throws IOException {
        return (HttpURLConnection) FirebasePerfUrlConnection.instrument(new URL(gVar.f6011b).openConnection());
    }
}
