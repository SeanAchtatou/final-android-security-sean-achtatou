package com.chartboost.sdk.o;

import android.content.SharedPreferences;
import android.os.Handler;
import com.applovin.mediation.AppLovinNativeAdapter;
import com.chartboost.sdk.c.g;
import com.chartboost.sdk.c.h;
import com.chartboost.sdk.c.k;
import com.chartboost.sdk.d.a;
import com.chartboost.sdk.d.d;
import com.chartboost.sdk.d.f;
import com.chartboost.sdk.i;
import com.chartboost.sdk.l;
import com.chartboost.sdk.n;
import com.chartboost.sdk.o.m;
import com.chartboost.sdk.o.p0;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.internal.AnalyticsEvents;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONException;
import org.json.JSONObject;

public class r0 {
    private final long A = TimeUnit.SECONDS.toNanos(1);
    private final String[] B = {"ASKED_TO_CACHE", "ASKED_TO_SHOW", "REQUESTING_TO_CACHE", "REQUESTING_TO_SHOW", "DOWNLOADING_TO_CACHE", "DOWNLOADING_TO_SHOW", "READY", "ASKING_UI_TO_SHOW_AD", "DONE"};

    /* renamed from: a  reason: collision with root package name */
    final ScheduledExecutorService f6145a;

    /* renamed from: b  reason: collision with root package name */
    private final y0 f6146b;

    /* renamed from: c  reason: collision with root package name */
    public final h f6147c;

    /* renamed from: d  reason: collision with root package name */
    private final k f6148d;

    /* renamed from: e  reason: collision with root package name */
    private final l f6149e;

    /* renamed from: f  reason: collision with root package name */
    private final s f6150f;

    /* renamed from: g  reason: collision with root package name */
    private final AtomicReference<f> f6151g;

    /* renamed from: h  reason: collision with root package name */
    private final SharedPreferences f6152h;

    /* renamed from: i  reason: collision with root package name */
    final k f6153i;

    /* renamed from: j  reason: collision with root package name */
    private final com.chartboost.sdk.e.a f6154j;

    /* renamed from: k  reason: collision with root package name */
    final Handler f6155k;
    final com.chartboost.sdk.h l;
    private final n m;
    private final i n;
    private final o o;
    final p0 p;
    int q = 0;
    private int r;
    private boolean s;
    final Map<String, s0> t;
    final SortedSet<s0> u;
    final SortedSet<s0> v;
    private final Map<String, Long> w;
    private final Map<String, Integer> x;
    ScheduledFuture<?> y;
    private final long z = TimeUnit.SECONDS.toNanos(5);

    class b implements u0 {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ s0 f6161a;

        b(s0 s0Var) {
            this.f6161a = s0Var;
        }

        public void a(boolean z, int i2, int i3) {
            r0.this.a(this.f6161a, z, i2, i3);
        }
    }

    public class c implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final int f6163a;

        /* renamed from: b  reason: collision with root package name */
        final String f6164b;

        /* renamed from: c  reason: collision with root package name */
        final s0 f6165c;

        /* renamed from: d  reason: collision with root package name */
        final a.c f6166d;

        public c(int i2, String str, s0 s0Var, a.c cVar) {
            this.f6163a = i2;
            this.f6164b = str;
            this.f6165c = s0Var;
            this.f6166d = cVar;
        }

        public void run() {
            try {
                synchronized (r0.this) {
                    int i2 = this.f6163a;
                    if (i2 != 0) {
                        switch (i2) {
                            case 2:
                                r0.this.y = null;
                                r0.this.b();
                                break;
                            case 3:
                                r0.this.b(this.f6164b);
                                break;
                            case 4:
                                r0.this.c(this.f6164b);
                                break;
                            case 5:
                                r0.this.b(this.f6165c);
                                break;
                            case 6:
                                r0.this.a(this.f6165c, this.f6166d);
                                break;
                            case 7:
                                r0.this.a(this.f6165c);
                                break;
                            case 8:
                                r0.this.d(this.f6164b);
                                break;
                        }
                    } else {
                        r0.this.a();
                    }
                }
            } catch (Exception e2) {
                com.chartboost.sdk.e.a.a(c.class, "run", e2);
            }
        }
    }

    public r0(p0 p0Var, ScheduledExecutorService scheduledExecutorService, y0 y0Var, h hVar, k kVar, l lVar, s sVar, AtomicReference<f> atomicReference, SharedPreferences sharedPreferences, k kVar2, com.chartboost.sdk.e.a aVar, Handler handler, com.chartboost.sdk.h hVar2, n nVar, i iVar, o oVar) {
        this.f6145a = scheduledExecutorService;
        this.f6146b = y0Var;
        this.f6147c = hVar;
        this.f6148d = kVar;
        this.f6149e = lVar;
        this.f6150f = sVar;
        this.f6151g = atomicReference;
        this.f6152h = sharedPreferences;
        this.f6153i = kVar2;
        this.f6154j = aVar;
        this.f6155k = handler;
        this.l = hVar2;
        this.m = nVar;
        this.n = iVar;
        this.o = oVar;
        this.p = p0Var;
        this.r = 1;
        this.t = new HashMap();
        this.v = new TreeSet();
        this.u = new TreeSet();
        this.w = new HashMap();
        this.x = new HashMap();
        this.s = false;
    }

    private void c() {
        Long l2;
        boolean z2 = true;
        if (this.q == 1) {
            long b2 = this.f6153i.b();
            l2 = null;
            for (Map.Entry next : this.w.entrySet()) {
                if (this.t.get((String) next.getKey()) != null) {
                    long max = Math.max(this.z, ((Long) next.getValue()).longValue() - b2);
                    if (l2 == null || max < l2.longValue()) {
                        l2 = Long.valueOf(max);
                    }
                }
            }
        } else {
            l2 = null;
        }
        if (!(l2 == null || this.y == null)) {
            if (Math.abs(l2.longValue() - this.y.getDelay(TimeUnit.NANOSECONDS)) > TimeUnit.SECONDS.toNanos(5)) {
                z2 = false;
            }
            if (z2) {
                return;
            }
        }
        ScheduledFuture<?> scheduledFuture = this.y;
        if (scheduledFuture != null) {
            scheduledFuture.cancel(false);
            this.y = null;
        }
        if (l2 != null) {
            this.y = this.f6145a.schedule(new c(2, null, null, null), l2.longValue(), TimeUnit.NANOSECONDS);
        }
    }

    private void d() {
        long b2 = this.f6153i.b();
        Iterator<Long> it = this.w.values().iterator();
        while (it.hasNext()) {
            if (b2 - it.next().longValue() >= 0) {
                it.remove();
            }
        }
    }

    private boolean e(String str) {
        return this.w.containsKey(str);
    }

    private void f(s0 s0Var) {
        this.t.remove(s0Var.f6180b);
        s0Var.f6181c = 8;
        s0Var.f6182d = null;
    }

    private void g(s0 s0Var) {
        f fVar = this.f6151g.get();
        long j2 = fVar.p;
        int i2 = fVar.q;
        Integer num = this.x.get(s0Var.f6180b);
        if (num == null) {
            num = 0;
        }
        Integer valueOf = Integer.valueOf(Math.min(num.intValue(), i2));
        this.x.put(s0Var.f6180b, Integer.valueOf(valueOf.intValue() + 1));
        this.w.put(s0Var.f6180b, Long.valueOf(this.f6153i.b() + TimeUnit.MILLISECONDS.toNanos(j2 << valueOf.intValue())));
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x004f A[Catch:{ Exception -> 0x00a1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x008a A[Catch:{ Exception -> 0x00a1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00ad  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00d0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void h(com.chartboost.sdk.o.s0 r9) {
        /*
            r8 = this;
            com.chartboost.sdk.o.l r0 = r8.f6149e
            boolean r0 = r0.c()
            if (r0 != 0) goto L_0x001d
            android.os.Handler r0 = r8.f6155k
            com.chartboost.sdk.o.p0$a r1 = new com.chartboost.sdk.o.p0$a
            com.chartboost.sdk.o.p0 r2 = r8.p
            r2.getClass()
            r3 = 4
            java.lang.String r9 = r9.f6180b
            com.chartboost.sdk.d.a$c r4 = com.chartboost.sdk.d.a.c.INTERNET_UNAVAILABLE_AT_SHOW
            r1.<init>(r3, r9, r4)
            r0.post(r1)
            return
        L_0x001d:
            r0 = 0
            com.chartboost.sdk.d.b r1 = r9.f6182d     // Catch:{ Exception -> 0x00a1 }
            com.chartboost.sdk.c.h r2 = r8.f6147c     // Catch:{ Exception -> 0x00a1 }
            com.chartboost.sdk.c.i r2 = r2.d()     // Catch:{ Exception -> 0x00a1 }
            java.io.File r2 = r2.f5773a     // Catch:{ Exception -> 0x00a1 }
            int r3 = r1.f5817b     // Catch:{ Exception -> 0x00a1 }
            java.lang.String r4 = "AdUnitManager"
            if (r3 != 0) goto L_0x004c
            com.chartboost.sdk.o.p0 r3 = r8.p     // Catch:{ Exception -> 0x00a1 }
            boolean r3 = r3.f6129g     // Catch:{ Exception -> 0x00a1 }
            if (r3 != 0) goto L_0x003e
            java.lang.String r3 = r1.p     // Catch:{ Exception -> 0x00a1 }
            java.lang.String r5 = "video"
            boolean r3 = r3.equals(r5)     // Catch:{ Exception -> 0x00a1 }
            if (r3 == 0) goto L_0x004c
        L_0x003e:
            org.json.JSONObject r3 = r1.f5816a     // Catch:{ Exception -> 0x00a1 }
            com.chartboost.sdk.d.a$c r3 = r8.a(r3)     // Catch:{ Exception -> 0x00a1 }
            if (r3 == 0) goto L_0x004d
            java.lang.String r5 = "Video media unavailable for the impression"
            com.chartboost.sdk.c.a.b(r4, r5)     // Catch:{ Exception -> 0x00a1 }
            goto L_0x004d
        L_0x004c:
            r3 = r0
        L_0x004d:
            if (r3 != 0) goto L_0x0088
            java.util.Map<java.lang.String, com.chartboost.sdk.d.c> r5 = r1.f5818c     // Catch:{ Exception -> 0x00a1 }
            java.util.Collection r5 = r5.values()     // Catch:{ Exception -> 0x00a1 }
            java.util.Iterator r5 = r5.iterator()     // Catch:{ Exception -> 0x00a1 }
        L_0x0059:
            boolean r6 = r5.hasNext()     // Catch:{ Exception -> 0x00a1 }
            if (r6 == 0) goto L_0x0088
            java.lang.Object r6 = r5.next()     // Catch:{ Exception -> 0x00a1 }
            com.chartboost.sdk.d.c r6 = (com.chartboost.sdk.d.c) r6     // Catch:{ Exception -> 0x00a1 }
            java.io.File r7 = r6.a(r2)     // Catch:{ Exception -> 0x00a1 }
            boolean r7 = r7.exists()     // Catch:{ Exception -> 0x00a1 }
            if (r7 != 0) goto L_0x0059
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00a1 }
            r3.<init>()     // Catch:{ Exception -> 0x00a1 }
            java.lang.String r7 = "Asset does not exist: "
            r3.append(r7)     // Catch:{ Exception -> 0x00a1 }
            java.lang.String r6 = r6.f5828b     // Catch:{ Exception -> 0x00a1 }
            r3.append(r6)     // Catch:{ Exception -> 0x00a1 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00a1 }
            com.chartboost.sdk.c.a.b(r4, r3)     // Catch:{ Exception -> 0x00a1 }
            com.chartboost.sdk.d.a$c r3 = com.chartboost.sdk.d.a.c.ASSET_MISSING     // Catch:{ Exception -> 0x00a1 }
            goto L_0x0059
        L_0x0088:
            if (r3 != 0) goto L_0x00ab
            int r4 = r1.f5817b     // Catch:{ Exception -> 0x00a1 }
            r5 = 1
            if (r4 != r5) goto L_0x0099
            java.lang.String r1 = r8.a(r1, r2)     // Catch:{ Exception -> 0x00a1 }
            if (r1 != 0) goto L_0x009a
            com.chartboost.sdk.d.a$c r2 = com.chartboost.sdk.d.a.c.ERROR_LOADING_WEB_VIEW     // Catch:{ Exception -> 0x00a1 }
            r3 = r2
            goto L_0x009a
        L_0x0099:
            r1 = r0
        L_0x009a:
            if (r3 != 0) goto L_0x00ab
            com.chartboost.sdk.d.d r0 = r8.a(r9, r1)     // Catch:{ Exception -> 0x00a1 }
            goto L_0x00ab
        L_0x00a1:
            r1 = move-exception
            java.lang.Class<com.chartboost.sdk.o.r0> r2 = com.chartboost.sdk.o.r0.class
            java.lang.String r3 = "showReady"
            com.chartboost.sdk.e.a.a(r2, r3, r1)
            com.chartboost.sdk.d.a$c r3 = com.chartboost.sdk.d.a.c.INTERNAL
        L_0x00ab:
            if (r3 != 0) goto L_0x00d0
            r1 = 7
            r9.f6181c = r1
            com.chartboost.sdk.h$d r1 = new com.chartboost.sdk.h$d
            com.chartboost.sdk.h r2 = r8.l
            r2.getClass()
            r3 = 10
            r1.<init>(r3)
            r1.f5875d = r0
            com.chartboost.sdk.c.k r0 = r8.f6153i
            long r2 = r0.b()
            java.lang.Long r0 = java.lang.Long.valueOf(r2)
            r9.f6188j = r0
            android.os.Handler r9 = r8.f6155k
            r9.post(r1)
            goto L_0x00d6
        L_0x00d0:
            r8.b(r9, r3)
            r8.f(r9)
        L_0x00d6:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.o.r0.h(com.chartboost.sdk.o.s0):void");
    }

    private void i(s0 s0Var) {
        m mVar = new m(this.p.f6128f, this.f6150f, this.f6154j, 2, new t0(this, s0Var.f6180b));
        mVar.f6018i = 1;
        mVar.a("cached", AppEventsConstants.EVENT_PARAM_VALUE_NO);
        String str = s0Var.f6182d.f5821f;
        if (!str.isEmpty()) {
            mVar.a(AppLovinNativeAdapter.KEY_EXTRA_AD_ID, str);
        }
        mVar.a(FirebaseAnalytics.Param.LOCATION, s0Var.f6180b);
        this.f6148d.a(mVar);
        this.f6154j.b(this.p.a(s0Var.f6182d.f5817b), s0Var.f6180b, str);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.q == 0) {
            this.q = 1;
            b();
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: package-private */
    public void b() {
        if (!this.s) {
            try {
                this.s = true;
                d();
                if (this.q == 1 && !a(this.v, 1, 3, 1, "show")) {
                    a(this.u, 0, 2, 2, "cache");
                }
                c();
                this.s = false;
            } catch (Throwable th) {
                this.s = false;
                throw th;
            }
        }
    }

    private void e(s0 s0Var) {
        b(s0Var, a.c.ASSETS_DOWNLOAD_FAILURE);
        f(s0Var);
        g(s0Var);
    }

    private boolean a(SortedSet<s0> sortedSet, int i2, int i3, int i4, String str) {
        Iterator<s0> it = sortedSet.iterator();
        while (it.hasNext()) {
            s0 next = it.next();
            if (next.f6181c != i2 || next.f6182d != null) {
                it.remove();
            } else if (e(next.f6180b)) {
                continue;
            } else if (!this.p.g(next.f6180b)) {
                next.f6181c = 8;
                this.t.remove(next.f6180b);
                it.remove();
            } else {
                next.f6181c = i3;
                it.remove();
                a(next, i4, str);
                return true;
            }
        }
        return false;
    }

    private boolean e() {
        if (this.p.f6123a == 0 && !n.u && this.f6152h.getInt("cbPrefSessionCount", 0) == 1) {
            return true;
        }
        return false;
    }

    private void d(s0 s0Var) {
        int i2 = s0Var.f6181c;
        long b2 = this.f6153i.b();
        Long l2 = s0Var.f6186h;
        if (l2 != null) {
            s0Var.f6189k = Integer.valueOf((int) TimeUnit.NANOSECONDS.toMillis(b2 - l2.longValue()));
        }
        Long l3 = s0Var.f6187i;
        if (l3 != null) {
            s0Var.l = Integer.valueOf((int) TimeUnit.NANOSECONDS.toMillis(b2 - l3.longValue()));
        }
        b(s0Var, "ad-unit-cached");
        s0Var.f6181c = 6;
        if (s0Var.f6184f) {
            Handler handler = this.f6155k;
            p0 p0Var = this.p;
            p0Var.getClass();
            handler.post(new p0.a(0, s0Var.f6180b, null));
        }
        if (i2 == 5) {
            h(s0Var);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        if (e()) {
            p0 p0Var = this.p;
            p0Var.getClass();
            this.f6155k.postDelayed(new p0.a(4, str, a.c.FIRST_SESSION_INTERSTITIALS_DISABLED), this.A);
            return;
        }
        s0 s0Var = this.t.get(str);
        if (s0Var != null && s0Var.f6181c == 6 && !a(s0Var.f6182d)) {
            this.t.remove(str);
            s0Var = null;
        }
        if (s0Var == null) {
            int i2 = this.r;
            this.r = i2 + 1;
            s0Var = new s0(i2, str, 0);
            this.t.put(str, s0Var);
            this.u.add(s0Var);
        }
        s0Var.f6184f = true;
        if (s0Var.f6186h == null) {
            s0Var.f6186h = Long.valueOf(this.f6153i.b());
        }
        switch (s0Var.f6181c) {
            case 6:
            case 7:
                Handler handler = this.f6155k;
                p0 p0Var2 = this.p;
                p0Var2.getClass();
                handler.post(new p0.a(0, str, null));
                break;
        }
        b();
    }

    class a implements m.a {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ s0 f6156a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ long f6157b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ boolean f6158c;

        /* renamed from: d  reason: collision with root package name */
        final /* synthetic */ boolean f6159d;

        a(s0 s0Var, long j2, boolean z, boolean z2) {
            this.f6156a = s0Var;
            this.f6157b = j2;
            this.f6158c = z;
            this.f6159d = z2;
        }

        public void a(m mVar, JSONObject jSONObject) {
            com.chartboost.sdk.d.b bVar;
            try {
                this.f6156a.p = Integer.valueOf((int) TimeUnit.NANOSECONDS.toMillis(r0.this.f6153i.b() - this.f6157b));
                this.f6156a.q = Integer.valueOf((int) TimeUnit.NANOSECONDS.toMillis(mVar.f6016g));
                this.f6156a.r = Integer.valueOf((int) TimeUnit.NANOSECONDS.toMillis(mVar.f6017h));
                if (this.f6158c) {
                    bVar = new com.chartboost.sdk.d.b(0, jSONObject, true);
                } else if (this.f6159d) {
                    bVar = new com.chartboost.sdk.d.b(1, jSONObject, false);
                } else {
                    bVar = new com.chartboost.sdk.d.b(0, jSONObject, false);
                }
                r0.this.a(this.f6156a, bVar);
            } catch (JSONException e2) {
                com.chartboost.sdk.e.a.a(r0.class, "sendAdGetRequest.onSuccess", e2);
                r0.this.a(this.f6156a, new com.chartboost.sdk.d.a(a.d.UNEXPECTED_RESPONSE, "Response conversion failure"));
            }
        }

        public void a(m mVar, com.chartboost.sdk.d.a aVar) {
            r0.this.a(this.f6156a, aVar);
        }
    }

    public synchronized com.chartboost.sdk.d.b a(String str) {
        s0 s0Var = this.t.get(str);
        if (s0Var == null || (s0Var.f6181c != 6 && s0Var.f6181c != 7)) {
            return null;
        }
        return s0Var.f6182d;
    }

    /* access modifiers changed from: package-private */
    public void d(String str) {
        s0 s0Var = this.t.get(str);
        if (s0Var != null && s0Var.f6181c == 6) {
            f(s0Var);
            b();
        }
    }

    private void c(s0 s0Var) {
        if (s0Var.f6182d != null) {
            int i2 = s0Var.f6181c;
            if (i2 == 5 || i2 == 4) {
                int i3 = s0Var.f6181c == 5 ? 1 : 2;
                if (s0Var.f6185g > i3) {
                    b bVar = new b(s0Var);
                    s0Var.f6185g = i3;
                    y0 y0Var = this.f6146b;
                    Map<String, com.chartboost.sdk.d.c> map = s0Var.f6182d.f5818c;
                    AtomicInteger atomicInteger = new AtomicInteger();
                    l.a().a(bVar);
                    y0Var.a(i3, map, atomicInteger, bVar);
                }
            }
        }
    }

    private boolean a(com.chartboost.sdk.d.b bVar) {
        File file = this.f6147c.d().f5773a;
        for (com.chartboost.sdk.d.c next : bVar.f5818c.values()) {
            if (!next.a(file).exists()) {
                com.chartboost.sdk.c.a.b("AdUnitManager", "Asset does not exist: " + next.f5828b);
                return false;
            }
        }
        return true;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r0v5, types: [com.chartboost.sdk.o.g] */
    /* JADX WARN: Type inference failed for: r15v3, types: [com.chartboost.sdk.o.m] */
    /* JADX WARN: Type inference failed for: r15v4, types: [com.chartboost.sdk.o.p] */
    /* JADX WARN: Type inference failed for: r15v5, types: [com.chartboost.sdk.o.m] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.o.m.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, boolean]
     candidates:
      com.chartboost.sdk.o.m.a(com.chartboost.sdk.o.j, com.chartboost.sdk.d.a):void
      com.chartboost.sdk.o.m.a(com.chartboost.sdk.d.a, com.chartboost.sdk.o.j):void
      com.chartboost.sdk.o.m.a(java.lang.Object, com.chartboost.sdk.o.j):void
      com.chartboost.sdk.o.m.a(org.json.JSONObject, com.chartboost.sdk.o.j):void
      com.chartboost.sdk.o.g.a(com.chartboost.sdk.d.a, com.chartboost.sdk.o.j):void
      com.chartboost.sdk.o.g.a(java.lang.Object, com.chartboost.sdk.o.j):void
      com.chartboost.sdk.o.m.a(java.lang.String, java.lang.Object):void */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(com.chartboost.sdk.o.s0 r22, int r23, java.lang.String r24) {
        /*
            r21 = this;
            r8 = r21
            r9 = r22
            java.util.concurrent.atomic.AtomicReference<com.chartboost.sdk.d.f> r0 = r8.f6151g     // Catch:{ Exception -> 0x010a }
            java.lang.Object r0 = r0.get()     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.d.f r0 = (com.chartboost.sdk.d.f) r0     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.o.p0 r1 = r8.p     // Catch:{ Exception -> 0x010a }
            int r1 = r1.f6123a     // Catch:{ Exception -> 0x010a }
            r10 = 2
            r11 = 0
            r12 = 1
            if (r1 != r10) goto L_0x0017
            r13 = 1
            goto L_0x0018
        L_0x0017:
            r13 = 0
        L_0x0018:
            boolean r1 = r0.v     // Catch:{ Exception -> 0x010a }
            if (r1 == 0) goto L_0x0020
            if (r13 != 0) goto L_0x0020
            r14 = 1
            goto L_0x0021
        L_0x0020:
            r14 = 0
        L_0x0021:
            com.chartboost.sdk.c.k r1 = r8.f6153i     // Catch:{ Exception -> 0x010a }
            long r4 = r1.b()     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.o.r0$a r20 = new com.chartboost.sdk.o.r0$a     // Catch:{ Exception -> 0x010a }
            r1 = r20
            r2 = r21
            r3 = r22
            r6 = r13
            r7 = r14
            r1.<init>(r3, r4, r6, r7)     // Catch:{ Exception -> 0x010a }
            int r1 = r9.f6181c     // Catch:{ Exception -> 0x010a }
            if (r1 != r10) goto L_0x003a
            r1 = 1
            goto L_0x003b
        L_0x003a:
            r1 = 0
        L_0x003b:
            java.lang.String r2 = "cache"
            java.lang.String r3 = "location"
            if (r13 == 0) goto L_0x0076
            com.chartboost.sdk.o.m r0 = new com.chartboost.sdk.o.m     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.o.p0 r4 = r8.p     // Catch:{ Exception -> 0x010a }
            java.lang.String r4 = r4.f6126d     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.o.s r5 = r8.f6150f     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.e.a r6 = r8.f6154j     // Catch:{ Exception -> 0x010a }
            r15 = r0
            r16 = r4
            r17 = r5
            r18 = r6
            r19 = r23
            r15.<init>(r16, r17, r18, r19, r20)     // Catch:{ Exception -> 0x010a }
            r0.m = r12     // Catch:{ Exception -> 0x010a }
            java.lang.String r4 = r9.f6180b     // Catch:{ Exception -> 0x010a }
            r0.a(r3, r4)     // Catch:{ Exception -> 0x010a }
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)     // Catch:{ Exception -> 0x010a }
            r0.a(r2, r1)     // Catch:{ Exception -> 0x010a }
            java.lang.String r1 = "raw"
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r12)     // Catch:{ Exception -> 0x010a }
            r0.a(r1, r2)     // Catch:{ Exception -> 0x010a }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r11)     // Catch:{ Exception -> 0x010a }
            r9.f6183e = r1     // Catch:{ Exception -> 0x010a }
            goto L_0x00eb
        L_0x0076:
            if (r14 == 0) goto L_0x00b6
            com.chartboost.sdk.o.p0 r4 = r8.p     // Catch:{ Exception -> 0x010a }
            java.lang.String r4 = r4.f6127e     // Catch:{ Exception -> 0x010a }
            java.lang.Object[] r5 = new java.lang.Object[r12]     // Catch:{ Exception -> 0x010a }
            java.lang.String r0 = r0.A     // Catch:{ Exception -> 0x010a }
            r5[r11] = r0     // Catch:{ Exception -> 0x010a }
            java.lang.String r16 = java.lang.String.format(r4, r5)     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.o.p r0 = new com.chartboost.sdk.o.p     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.o.s r4 = r8.f6150f     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.e.a r5 = r8.f6154j     // Catch:{ Exception -> 0x010a }
            r15 = r0
            r17 = r4
            r18 = r5
            r19 = r23
            r15.<init>(r16, r17, r18, r19, r20)     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.c.h r4 = r8.f6147c     // Catch:{ Exception -> 0x010a }
            org.json.JSONObject r4 = r4.c()     // Catch:{ Exception -> 0x010a }
            java.lang.String r5 = "cache_assets"
            r0.a(r5, r4, r11)     // Catch:{ Exception -> 0x010a }
            java.lang.String r4 = r9.f6180b     // Catch:{ Exception -> 0x010a }
            r0.a(r3, r4, r11)     // Catch:{ Exception -> 0x010a }
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)     // Catch:{ Exception -> 0x010a }
            r0.a(r2, r1, r11)     // Catch:{ Exception -> 0x010a }
            r0.m = r12     // Catch:{ Exception -> 0x010a }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r12)     // Catch:{ Exception -> 0x010a }
            r9.f6183e = r1     // Catch:{ Exception -> 0x010a }
            goto L_0x00eb
        L_0x00b6:
            com.chartboost.sdk.o.m r0 = new com.chartboost.sdk.o.m     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.o.p0 r4 = r8.p     // Catch:{ Exception -> 0x010a }
            java.lang.String r4 = r4.f6126d     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.o.s r5 = r8.f6150f     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.e.a r6 = r8.f6154j     // Catch:{ Exception -> 0x010a }
            r15 = r0
            r16 = r4
            r17 = r5
            r18 = r6
            r19 = r23
            r15.<init>(r16, r17, r18, r19, r20)     // Catch:{ Exception -> 0x010a }
            java.lang.String r4 = "local-videos"
            com.chartboost.sdk.c.h r5 = r8.f6147c     // Catch:{ Exception -> 0x010a }
            org.json.JSONArray r5 = r5.b()     // Catch:{ Exception -> 0x010a }
            r0.a(r4, r5)     // Catch:{ Exception -> 0x010a }
            r0.m = r12     // Catch:{ Exception -> 0x010a }
            java.lang.String r4 = r9.f6180b     // Catch:{ Exception -> 0x010a }
            r0.a(r3, r4)     // Catch:{ Exception -> 0x010a }
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)     // Catch:{ Exception -> 0x010a }
            r0.a(r2, r1)     // Catch:{ Exception -> 0x010a }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r11)     // Catch:{ Exception -> 0x010a }
            r9.f6183e = r1     // Catch:{ Exception -> 0x010a }
        L_0x00eb:
            r0.f6018i = r12     // Catch:{ Exception -> 0x010a }
            r8.q = r10     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.o.k r1 = r8.f6148d     // Catch:{ Exception -> 0x010a }
            r1.a(r0)     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.e.a r0 = r8.f6154j     // Catch:{ Exception -> 0x010a }
            com.chartboost.sdk.o.p0 r1 = r8.p     // Catch:{ Exception -> 0x010a }
            java.lang.Integer r2 = r9.f6183e     // Catch:{ Exception -> 0x010a }
            int r2 = r2.intValue()     // Catch:{ Exception -> 0x010a }
            java.lang.String r1 = r1.a(r2)     // Catch:{ Exception -> 0x010a }
            java.lang.String r2 = r9.f6180b     // Catch:{ Exception -> 0x010a }
            r3 = r24
            r0.a(r1, r3, r2)     // Catch:{ Exception -> 0x010a }
            goto L_0x011e
        L_0x010a:
            r0 = move-exception
            java.lang.Class<com.chartboost.sdk.o.r0> r1 = com.chartboost.sdk.o.r0.class
            java.lang.String r2 = "sendAdGetRequest"
            com.chartboost.sdk.e.a.a(r1, r2, r0)
            com.chartboost.sdk.d.a r0 = new com.chartboost.sdk.d.a
            com.chartboost.sdk.d.a$d r1 = com.chartboost.sdk.d.a.d.MISCELLANEOUS
            java.lang.String r2 = "error sending ad-get request"
            r0.<init>(r1, r2)
            r8.a(r9, r0)
        L_0x011e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.o.r0.a(com.chartboost.sdk.o.s0, int, java.lang.String):void");
    }

    private void b(s0 s0Var, a.c cVar) {
        String str;
        Handler handler = this.f6155k;
        p0 p0Var = this.p;
        p0Var.getClass();
        handler.post(new p0.a(4, s0Var.f6180b, cVar));
        if (cVar != a.c.NO_AD_FOUND) {
            com.chartboost.sdk.d.b bVar = s0Var.f6182d;
            String str2 = null;
            String str3 = bVar != null ? bVar.f5821f : null;
            int i2 = s0Var.f6181c;
            String str4 = (i2 == 0 || i2 == 2 || i2 == 4) ? "cache" : "show";
            com.chartboost.sdk.d.b bVar2 = s0Var.f6182d;
            Integer valueOf = Integer.valueOf(bVar2 != null ? bVar2.f5817b : s0Var.f6183e.intValue());
            if (valueOf != null) {
                str2 = valueOf.intValue() == 0 ? "native" : AnalyticsEvents.PARAMETER_SHARE_DIALOG_SHOW_WEB;
            }
            String str5 = str2;
            int i3 = s0Var.f6181c;
            if (i3 >= 0) {
                String[] strArr = this.B;
                if (i3 < strArr.length) {
                    str = strArr[i3];
                    this.f6154j.a(this.p.f6124b, str4, str5, cVar.toString(), str3, s0Var.f6180b, str);
                }
            }
            str = "Unknown state: " + s0Var.f6181c;
            this.f6154j.a(this.p.f6124b, str4, str5, cVar.toString(), str3, s0Var.f6180b, str);
        }
    }

    /* access modifiers changed from: package-private */
    public void c(String str) {
        if (e()) {
            p0 p0Var = this.p;
            p0Var.getClass();
            this.f6155k.postDelayed(new p0.a(4, str, a.c.FIRST_SESSION_INTERSTITIALS_DISABLED), this.A);
            return;
        }
        s0 s0Var = this.t.get(str);
        if (s0Var == null) {
            int i2 = this.r;
            this.r = i2 + 1;
            s0Var = new s0(i2, str, 1);
            this.t.put(str, s0Var);
            this.v.add(s0Var);
        }
        if (s0Var.f6187i == null) {
            s0Var.f6187i = Long.valueOf(this.f6153i.b());
        }
        switch (s0Var.f6181c) {
            case 0:
                this.u.remove(s0Var);
                this.v.add(s0Var);
                s0Var.f6181c = 1;
                break;
            case 2:
                s0Var.f6181c = 3;
                break;
            case 4:
                s0Var.f6181c = 5;
                c(s0Var);
                break;
            case 6:
                h(s0Var);
                break;
        }
        b();
    }

    private void b(s0 s0Var, String str) {
        Integer num;
        String str2;
        s0 s0Var2 = s0Var;
        if (this.f6151g.get().n) {
            com.chartboost.sdk.d.b bVar = s0Var2.f6182d;
            String str3 = null;
            String str4 = bVar != null ? bVar.f5821f : null;
            int i2 = s0Var2.f6181c;
            String str5 = (i2 == 0 || i2 == 2 || i2 == 4) ? "cache" : "show";
            com.chartboost.sdk.d.b bVar2 = s0Var2.f6182d;
            if (bVar2 != null) {
                num = Integer.valueOf(bVar2.f5817b);
            } else {
                num = s0Var2.f6183e;
            }
            if (num != null) {
                str3 = num.intValue() == 0 ? "native" : AnalyticsEvents.PARAMETER_SHARE_DIALOG_SHOW_WEB;
            }
            String str6 = str3;
            int i3 = s0Var2.f6181c;
            if (i3 >= 0) {
                String[] strArr = this.B;
                if (i3 < strArr.length) {
                    str2 = strArr[i3];
                    this.f6154j.a(str, this.p.f6124b, str5, str6, null, null, g.a(g.a("adGetRequestSubmitToCallbackMs", s0Var2.p), g.a("downloadRequestToCompletionMs", s0Var2.n), g.a("downloadAccumulatedProcessingMs", s0Var2.o), g.a("adGetRequestGetResponseCodeMs", s0Var2.q), g.a("adGetRequestReadDataMs", s0Var2.r), g.a("cacheRequestToReadyMs", s0Var2.f6189k), g.a("showRequestToReadyMs", s0Var2.l), g.a("showRequestToShownMs", s0Var2.m), g.a("adId", str4), g.a((String) FirebaseAnalytics.Param.LOCATION, s0Var2.f6180b), g.a("state", str2)), false);
                }
            }
            str2 = "Unknown state: " + s0Var2.f6181c;
            this.f6154j.a(str, this.p.f6124b, str5, str6, null, null, g.a(g.a("adGetRequestSubmitToCallbackMs", s0Var2.p), g.a("downloadRequestToCompletionMs", s0Var2.n), g.a("downloadAccumulatedProcessingMs", s0Var2.o), g.a("adGetRequestGetResponseCodeMs", s0Var2.q), g.a("adGetRequestReadDataMs", s0Var2.r), g.a("cacheRequestToReadyMs", s0Var2.f6189k), g.a("showRequestToReadyMs", s0Var2.l), g.a("showRequestToShownMs", s0Var2.m), g.a("adId", str4), g.a((String) FirebaseAnalytics.Param.LOCATION, s0Var2.f6180b), g.a("state", str2)), false);
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(s0 s0Var, com.chartboost.sdk.d.b bVar) {
        this.q = 1;
        s0Var.f6181c = s0Var.f6181c == 2 ? 4 : 5;
        s0Var.f6182d = bVar;
        c(s0Var);
        b();
    }

    /* access modifiers changed from: package-private */
    public void b(s0 s0Var) {
        if (s0Var.f6181c == 7) {
            if (s0Var.f6187i != null && s0Var.m == null) {
                s0Var.m = Integer.valueOf((int) TimeUnit.NANOSECONDS.toMillis(this.f6153i.b() - s0Var.f6187i.longValue()));
            }
            b(s0Var, "ad-unit-shown");
            this.x.remove(s0Var.f6180b);
            Handler handler = this.f6155k;
            p0 p0Var = this.p;
            p0Var.getClass();
            handler.post(new p0.a(5, s0Var.f6180b, null));
            i(s0Var);
            f(s0Var);
            b();
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(s0 s0Var, boolean z2, int i2, int i3) {
        if (s0Var.f6181c == 4 || s0Var.f6181c == 5) {
            s0Var.n = Integer.valueOf(i2);
            s0Var.o = Integer.valueOf(i3);
            if (z2) {
                d(s0Var);
            } else {
                e(s0Var);
            }
        }
        b();
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(s0 s0Var, com.chartboost.sdk.d.a aVar) {
        if (this.q != 0) {
            this.q = 1;
            b(s0Var, aVar.c());
            f(s0Var);
            g(s0Var);
            b();
        }
    }

    private String a(com.chartboost.sdk.d.b bVar, File file) {
        com.chartboost.sdk.d.c cVar = bVar.q;
        if (cVar == null) {
            com.chartboost.sdk.c.a.b("AdUnitManager", "AdUnit does not have a template body");
            return null;
        }
        File a2 = cVar.a(file);
        HashMap hashMap = new HashMap();
        if (bVar instanceof g1) {
            hashMap.put("{% encoding %}", "base64");
            hashMap.put("{% adm %}", ((g1) bVar).s);
            String str = this.p.f6123a == 0 ? "8" : "9";
            String str2 = this.p.f6123a == 0 ? "true" : "false";
            hashMap.put("{{ ad_type }}", str);
            hashMap.put("{{ show_close_button }}", str2);
            hashMap.put("{{ preroll_popup }}", "false");
            hashMap.put("{{ post_video_reward_toaster_enabled }}", "false");
            if (str.equals("9")) {
                hashMap.put("{{ post_video_reward_toaster_enabled }}", "false");
            }
        }
        hashMap.putAll(bVar.f5819d);
        hashMap.put("{% certification_providers %}", b1.a(bVar.r));
        for (Map.Entry next : bVar.f5818c.entrySet()) {
            hashMap.put(next.getKey(), ((com.chartboost.sdk.d.c) next.getValue()).f5828b);
        }
        try {
            return a1.a(a2, hashMap);
        } catch (Exception e2) {
            com.chartboost.sdk.e.a.a(r0.class, "loadTemplateHtml", e2);
            return null;
        }
    }

    private d a(s0 s0Var, String str) {
        s0 s0Var2 = s0Var;
        return new d(s0Var2.f6182d, new q0(this, s0Var2), this.f6147c, this.f6148d, this.f6150f, this.f6152h, this.f6154j, this.f6155k, this.l, this.m, this.n, this.o, this.p, s0Var2.f6180b, str);
    }

    /* access modifiers changed from: package-private */
    public void a(s0 s0Var, a.c cVar) {
        b(s0Var, cVar);
        if (s0Var.f6181c != 7) {
            return;
        }
        if (cVar == a.c.IMPRESSION_ALREADY_VISIBLE) {
            s0Var.f6181c = 6;
            s0Var.f6188j = null;
            s0Var.f6187i = null;
            s0Var.m = null;
            return;
        }
        g(s0Var);
        f(s0Var);
        b();
    }

    /* access modifiers changed from: package-private */
    public void a(s0 s0Var) {
        if (s0Var.f6181c == 7) {
            s0Var.f6181c = 6;
            s0Var.f6188j = null;
            s0Var.f6187i = null;
            s0Var.m = null;
        }
    }

    /* access modifiers changed from: package-private */
    public a.c a(JSONObject jSONObject) {
        if (jSONObject == null) {
            return a.c.INVALID_RESPONSE;
        }
        JSONObject optJSONObject = jSONObject.optJSONObject("assets");
        if (optJSONObject == null) {
            return a.c.INVALID_RESPONSE;
        }
        JSONObject optJSONObject2 = optJSONObject.optJSONObject(com.chartboost.sdk.c.b.a(com.chartboost.sdk.c.b.a()) ? "video-portrait" : "video-landscape");
        if (optJSONObject2 == null) {
            return a.c.VIDEO_UNAVAILABLE_FOR_CURRENT_ORIENTATION;
        }
        String optString = optJSONObject2.optString("id");
        if (optString.isEmpty()) {
            return a.c.VIDEO_ID_MISSING;
        }
        if (new File(this.f6147c.d().f5776d, optString).exists()) {
            return null;
        }
        return a.c.VIDEO_UNAVAILABLE;
    }
}
