package com.chartboost.sdk.o;

import com.chartboost.sdk.c.b;
import com.chartboost.sdk.c.f;
import com.chartboost.sdk.c.g;
import com.chartboost.sdk.e.a;
import com.chartboost.sdk.n;
import com.chartboost.sdk.o.m;
import com.facebook.appevents.UserDataStore;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TapjoyConstants;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

public final class p extends m {
    private final JSONObject p = new JSONObject();
    private final JSONObject q = new JSONObject();
    private final JSONObject r = new JSONObject();
    private final JSONObject s = new JSONObject();

    public p(String str, s sVar, a aVar, int i2, m.a aVar2) {
        super(str, sVar, aVar, i2, aVar2);
    }

    public void a(String str, Object obj, int i2) {
        if (i2 == 0) {
            g.a(this.s, str, obj);
            a("ad", this.s);
        }
    }

    /* access modifiers changed from: protected */
    public void c() {
        g.a(this.q, TapjoyConstants.TJC_APP_PLACEMENT, this.n.s);
        g.a(this.q, TJAdUnitConstants.String.BUNDLE, this.n.f6177j);
        g.a(this.q, "bundle_id", this.n.f6178k);
        g.a(this.q, "custom_id", n.f5939b);
        g.a(this.q, TapjoyConstants.TJC_SESSION_ID, "");
        g.a(this.q, "ui", -1);
        g.a(this.q, "test_mode", false);
        g.a(this.q, "certification_providers", b1.f());
        a(TapjoyConstants.TJC_APP_PLACEMENT, this.q);
        boolean z = true;
        g.a(this.r, "carrier", g.a(g.a((String) TapjoyConstants.TJC_CARRIER_NAME, this.n.v.optString("carrier-name")), g.a((String) TapjoyConstants.TJC_MOBILE_COUNTRY_CODE, this.n.v.optString("mobile-country-code")), g.a((String) TapjoyConstants.TJC_MOBILE_NETWORK_CODE, this.n.v.optString("mobile-network-code")), g.a("iso_country_code", this.n.v.optString("iso-country-code")), g.a("phone_type", Integer.valueOf(this.n.v.optInt("phone-type")))));
        g.a(this.r, "model", this.n.f6173f);
        g.a(this.r, TapjoyConstants.TJC_DEVICE_TYPE_NAME, this.n.t);
        g.a(this.r, "actual_device_type", this.n.u);
        g.a(this.r, "os", this.n.f6174g);
        g.a(this.r, UserDataStore.COUNTRY, this.n.f6175h);
        g.a(this.r, "language", this.n.f6176i);
        g.a(this.r, "timestamp", String.valueOf(TimeUnit.MILLISECONDS.toSeconds(this.n.f6172e.a())));
        g.a(this.r, "reachability", Integer.valueOf(this.n.f6169b.a()));
        g.a(this.r, "scale", this.n.r);
        g.a(this.r, "is_portrait", Boolean.valueOf(b.a(b.a())));
        g.a(this.r, "rooted_device", Boolean.valueOf(this.n.w));
        g.a(this.r, TapjoyConstants.TJC_DEVICE_TIMEZONE, this.n.x);
        g.a(this.r, "mobile_network", this.n.y);
        g.a(this.r, "dw", this.n.o);
        g.a(this.r, "dh", this.n.p);
        g.a(this.r, "dpi", this.n.q);
        g.a(this.r, "w", this.n.m);
        g.a(this.r, "h", this.n.n);
        g.a(this.r, "user_agent", n.w);
        g.a(this.r, "device_family", "");
        g.a(this.r, "retina", false);
        f.a a2 = this.n.f6168a.a();
        g.a(this.r, "identity", a2.f5766b);
        int i2 = a2.f5765a;
        if (i2 != -1) {
            if (i2 != 1) {
                z = false;
            }
            g.a(this.r, "limit_ad_tracking", Boolean.valueOf(z));
        }
        g.a(this.r, "pidatauseconsent", Integer.valueOf(n.x.a()));
        a(TapjoyConstants.TJC_NOTIFICATION_DEVICE_PREFIX, this.r);
        g.a(this.p, "framework", "");
        g.a(this.p, "sdk", this.n.l);
        if (n.f5942e != null) {
            g.a(this.p, "framework_version", n.f5944g);
            g.a(this.p, "wrapper_version", n.f5940c);
        }
        g.a(this.p, "mediation", n.f5946i);
        g.a(this.p, "commit_hash", "7fc7bc32841a43689553f0e08928c7ad6ed7e23b");
        String str = this.n.f6170c.get().f5841a;
        if (!f1.e().a(str)) {
            g.a(this.p, "config_variant", str);
        }
        a("sdk", this.p);
        g.a(this.s, "session", Integer.valueOf(this.n.f6171d.getInt("cbPrefSessionCount", 0)));
        if (this.s.isNull("cache")) {
            g.a(this.s, "cache", false);
        }
        if (this.s.isNull("amount")) {
            g.a(this.s, "amount", 0);
        }
        if (this.s.isNull("retry_count")) {
            g.a(this.s, "retry_count", 0);
        }
        if (this.s.isNull(FirebaseAnalytics.Param.LOCATION)) {
            g.a(this.s, FirebaseAnalytics.Param.LOCATION, "");
        }
        a("ad", this.s);
    }
}
