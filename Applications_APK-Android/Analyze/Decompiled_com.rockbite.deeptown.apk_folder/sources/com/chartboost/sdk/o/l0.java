package com.chartboost.sdk.o;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.charset.Charset;

public class l0 {

    /* renamed from: a  reason: collision with root package name */
    public static final BigInteger f6087a = BigInteger.valueOf(1024);

    /* renamed from: b  reason: collision with root package name */
    public static final BigInteger f6088b;

    /* renamed from: c  reason: collision with root package name */
    public static final BigInteger f6089c = f6087a.multiply(f6088b);

    /* renamed from: d  reason: collision with root package name */
    public static final BigInteger f6090d = f6087a.multiply(f6089c);

    /* renamed from: e  reason: collision with root package name */
    public static final BigInteger f6091e = f6087a.multiply(f6090d);

    /* renamed from: f  reason: collision with root package name */
    public static final BigInteger f6092f = BigInteger.valueOf(1024).multiply(BigInteger.valueOf(1152921504606846976L));

    static {
        BigInteger bigInteger = f6087a;
        f6088b = bigInteger.multiply(bigInteger);
        f6087a.multiply(f6091e);
        f6087a.multiply(f6092f);
        Charset.forName("UTF-8");
    }

    public static FileInputStream a(File file) throws IOException {
        if (!file.exists()) {
            throw new FileNotFoundException("File '" + file + "' does not exist");
        } else if (file.isDirectory()) {
            throw new IOException("File '" + file + "' exists but is a directory");
        } else if (file.canRead()) {
            return new FileInputStream(file);
        } else {
            throw new IOException("File '" + file + "' cannot be read");
        }
    }

    public static byte[] b(File file) throws IOException {
        FileInputStream fileInputStream;
        try {
            fileInputStream = a(file);
            try {
                byte[] a2 = m0.a(fileInputStream, file.length());
                m0.a((InputStream) fileInputStream);
                return a2;
            } catch (Throwable th) {
                th = th;
                m0.a((InputStream) fileInputStream);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fileInputStream = null;
            m0.a((InputStream) fileInputStream);
            throw th;
        }
    }
}
