package com.chartboost.sdk.o;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.widget.LinearLayout;
import com.esotericsoftware.spine.Animation;

public class d extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private final Paint f5970a;

    /* renamed from: b  reason: collision with root package name */
    private int f5971b = 0;

    public d(Context context) {
        super(context);
        int round = Math.round(context.getResources().getDisplayMetrics().density * 5.0f);
        setPadding(round, round, round, round);
        setBaselineAligned(false);
        this.f5970a = new Paint();
        this.f5970a.setStyle(Paint.Style.FILL);
    }

    public void a(int i2) {
        this.f5970a.setColor(i2);
        invalidate();
    }

    public void b(int i2) {
        this.f5971b = i2;
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float f2 = getContext().getResources().getDisplayMetrics().density;
        if ((this.f5971b & 1) > 0) {
            canvas.drawRect(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, (float) canvas.getWidth(), f2 * 1.0f, this.f5970a);
        }
        if ((this.f5971b & 2) > 0) {
            canvas.drawRect(((float) canvas.getWidth()) - (f2 * 1.0f), Animation.CurveTimeline.LINEAR, (float) canvas.getWidth(), (float) canvas.getHeight(), this.f5970a);
        }
        if ((this.f5971b & 4) > 0) {
            canvas.drawRect(Animation.CurveTimeline.LINEAR, ((float) canvas.getHeight()) - (f2 * 1.0f), (float) canvas.getWidth(), (float) canvas.getHeight(), this.f5970a);
        }
        if ((this.f5971b & 8) > 0) {
            canvas.drawRect(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, f2 * 1.0f, (float) canvas.getHeight(), this.f5970a);
        }
    }
}
