package com.chartboost.sdk.o;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import com.esotericsoftware.spine.Animation;

public class l1 extends f0 {

    /* renamed from: c  reason: collision with root package name */
    private Paint f6093c;

    /* renamed from: d  reason: collision with root package name */
    private Paint f6094d;

    /* renamed from: e  reason: collision with root package name */
    private Path f6095e;

    /* renamed from: f  reason: collision with root package name */
    private RectF f6096f;

    /* renamed from: g  reason: collision with root package name */
    private RectF f6097g;

    /* renamed from: h  reason: collision with root package name */
    private int f6098h = 0;

    /* renamed from: i  reason: collision with root package name */
    private float f6099i;

    /* renamed from: j  reason: collision with root package name */
    private float f6100j;

    public l1(Context context) {
        super(context);
        a(context);
    }

    private void a(Context context) {
        float f2 = context.getResources().getDisplayMetrics().density;
        this.f6099i = 4.5f * f2;
        this.f6093c = new Paint();
        this.f6093c.setColor(-1);
        this.f6093c.setStyle(Paint.Style.STROKE);
        this.f6093c.setStrokeWidth(f2 * 1.0f);
        this.f6093c.setAntiAlias(true);
        this.f6094d = new Paint();
        this.f6094d.setColor(-855638017);
        this.f6094d.setStyle(Paint.Style.FILL);
        this.f6094d.setAntiAlias(true);
        this.f6095e = new Path();
        this.f6097g = new RectF();
        this.f6096f = new RectF();
    }

    public void b(int i2) {
        this.f6093c.setColor(i2);
        invalidate();
    }

    public void c(int i2) {
        this.f6094d.setColor(i2);
        invalidate();
    }

    public void b(float f2) {
        this.f6099i = f2;
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas) {
        float f2 = getContext().getResources().getDisplayMetrics().density;
        this.f6096f.set(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, (float) getWidth(), (float) getHeight());
        float min = (float) Math.min(1, Math.round(f2 * 0.5f));
        this.f6096f.inset(min, min);
        this.f6095e.reset();
        Path path = this.f6095e;
        RectF rectF = this.f6096f;
        float f3 = this.f6099i;
        path.addRoundRect(rectF, f3, f3, Path.Direction.CW);
        canvas.save();
        canvas.clipPath(this.f6095e);
        canvas.drawColor(this.f6098h);
        this.f6097g.set(this.f6096f);
        RectF rectF2 = this.f6097g;
        float f4 = rectF2.right;
        float f5 = rectF2.left;
        rectF2.right = ((f4 - f5) * this.f6100j) + f5;
        canvas.drawRect(rectF2, this.f6094d);
        canvas.restore();
        RectF rectF3 = this.f6096f;
        float f6 = this.f6099i;
        canvas.drawRoundRect(rectF3, f6, f6, this.f6093c);
    }

    public void a(int i2) {
        this.f6098h = i2;
        invalidate();
    }

    public void a(float f2) {
        this.f6100j = f2;
        if (getVisibility() != 8) {
            invalidate();
        }
    }
}
