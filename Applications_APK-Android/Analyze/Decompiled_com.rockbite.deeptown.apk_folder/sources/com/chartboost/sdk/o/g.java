package com.chartboost.sdk.o;

import com.chartboost.sdk.d.a;
import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;

public class g<T> {

    /* renamed from: a  reason: collision with root package name */
    public final String f6010a;

    /* renamed from: b  reason: collision with root package name */
    public final String f6011b;

    /* renamed from: c  reason: collision with root package name */
    public final int f6012c;

    /* renamed from: d  reason: collision with root package name */
    public final AtomicInteger f6013d = new AtomicInteger();

    /* renamed from: e  reason: collision with root package name */
    public final File f6014e;

    /* renamed from: f  reason: collision with root package name */
    public long f6015f;

    /* renamed from: g  reason: collision with root package name */
    public long f6016g;

    /* renamed from: h  reason: collision with root package name */
    public long f6017h;

    /* renamed from: i  reason: collision with root package name */
    public int f6018i;

    public g(String str, String str2, int i2, File file) {
        this.f6010a = str;
        this.f6011b = str2;
        this.f6012c = i2;
        this.f6014e = file;
        this.f6015f = 0;
        this.f6016g = 0;
        this.f6017h = 0;
        this.f6018i = 0;
    }

    public h a() {
        return new h(null, null, null);
    }

    public void a(a aVar, j jVar) {
    }

    public void a(Object obj, j jVar) {
    }

    public boolean b() {
        return this.f6013d.compareAndSet(0, -1);
    }

    public i<T> a(j jVar) {
        return i.a((Object) null);
    }
}
