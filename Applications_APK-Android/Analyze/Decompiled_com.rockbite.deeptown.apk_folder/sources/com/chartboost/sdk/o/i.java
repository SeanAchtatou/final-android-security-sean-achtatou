package com.chartboost.sdk.o;

import com.chartboost.sdk.d.a;

public class i<T> {

    /* renamed from: a  reason: collision with root package name */
    public final T f6041a;

    /* renamed from: b  reason: collision with root package name */
    public final a f6042b;

    private i(T t, a aVar) {
        this.f6041a = t;
        this.f6042b = aVar;
    }

    public static <T> i<T> a(Object obj) {
        return new i<>(obj, null);
    }

    public static <T> i<T> a(a aVar) {
        return new i<>(null, aVar);
    }
}
