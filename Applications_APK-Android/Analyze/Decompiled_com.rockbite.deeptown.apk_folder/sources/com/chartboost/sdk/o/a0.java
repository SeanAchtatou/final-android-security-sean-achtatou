package com.chartboost.sdk.o;

import android.content.Context;
import android.view.View;
import com.chartboost.sdk.d.b;

public final class a0 extends View {

    /* renamed from: a  reason: collision with root package name */
    private boolean f5953a = false;

    public a0(Context context) {
        super(context);
        setFocusable(false);
        setBackgroundColor(-1442840576);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.o.z.a(boolean, android.view.View, com.chartboost.sdk.d.b):void
     arg types: [int, com.chartboost.sdk.o.a0, com.chartboost.sdk.d.b]
     candidates:
      com.chartboost.sdk.o.z.a(int, com.chartboost.sdk.d.d, java.lang.Runnable):void
      com.chartboost.sdk.o.z.a(boolean, android.view.View, long):void
      com.chartboost.sdk.o.z.a(boolean, android.view.View, com.chartboost.sdk.d.b):void */
    public void a(z zVar, b bVar) {
        if (!this.f5953a) {
            zVar.a(true, (View) this, bVar);
            this.f5953a = true;
        }
    }
}
