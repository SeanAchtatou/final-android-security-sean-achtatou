package com.chartboost.sdk.o;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.chartboost.sdk.c.a;
import com.chartboost.sdk.c.b;
import com.chartboost.sdk.c.f;
import com.chartboost.sdk.c.g;
import com.chartboost.sdk.c.k;
import com.chartboost.sdk.l;
import com.underwater.demolisher.data.vo.RemoteConfigConst;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

public class s {

    /* renamed from: a  reason: collision with root package name */
    final f f6168a;

    /* renamed from: b  reason: collision with root package name */
    final l f6169b;

    /* renamed from: c  reason: collision with root package name */
    final AtomicReference<com.chartboost.sdk.d.f> f6170c;

    /* renamed from: d  reason: collision with root package name */
    final SharedPreferences f6171d;

    /* renamed from: e  reason: collision with root package name */
    final k f6172e;

    /* renamed from: f  reason: collision with root package name */
    final String f6173f;

    /* renamed from: g  reason: collision with root package name */
    final String f6174g;

    /* renamed from: h  reason: collision with root package name */
    final String f6175h;

    /* renamed from: i  reason: collision with root package name */
    final String f6176i;

    /* renamed from: j  reason: collision with root package name */
    String f6177j;

    /* renamed from: k  reason: collision with root package name */
    String f6178k;
    final String l;
    final Integer m;
    final Integer n;
    final Integer o;
    final Integer p;
    final String q;
    final Float r;
    final String s;
    final String t;
    final String u;
    final JSONObject v;
    final boolean w;
    final String x;
    final Integer y;

    public s(Context context, String str, f fVar, l lVar, AtomicReference<com.chartboost.sdk.d.f> atomicReference, SharedPreferences sharedPreferences, k kVar) {
        JSONObject jSONObject;
        int i2;
        int i3;
        WindowManager windowManager;
        String str2;
        String str3;
        String str4;
        this.f6168a = fVar;
        this.f6169b = lVar;
        this.f6170c = atomicReference;
        this.f6171d = sharedPreferences;
        this.f6172e = kVar;
        this.s = str;
        if ("sdk".equals(Build.PRODUCT) || "google_sdk".equals(Build.PRODUCT) || ((str4 = Build.MANUFACTURER) != null && str4.contains("Genymotion"))) {
            this.f6173f = "Android Simulator";
        } else {
            this.f6173f = Build.MODEL;
        }
        this.t = Build.MANUFACTURER + RemoteConfigConst.SHOP_SPECIAL_EVENT_OFFER + Build.MODEL;
        this.u = u.a(context);
        this.f6174g = "Android " + Build.VERSION.RELEASE;
        this.f6175h = Locale.getDefault().getCountry();
        this.f6176i = Locale.getDefault().getLanguage();
        this.l = "7.5.0";
        this.r = Float.valueOf(context.getResources().getDisplayMetrics().density);
        try {
            String packageName = context.getPackageName();
            this.f6177j = context.getPackageManager().getPackageInfo(packageName, 128).versionName;
            this.f6178k = packageName;
        } catch (Exception e2) {
            a.a("RequestBody", "Exception raised getting package mager object", e2);
        }
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        int i4 = 0;
        if (telephonyManager == null || telephonyManager.getPhoneType() == 0 || telephonyManager.getSimState() != 5) {
            jSONObject = new JSONObject();
        } else {
            String str5 = null;
            try {
                str2 = telephonyManager.getSimOperator();
            } catch (Exception e3) {
                com.chartboost.sdk.e.a.a(com.chartboost.sdk.a.class, "Unable to retrieve sim operator information", e3);
                str2 = null;
            }
            if (str2 == null || TextUtils.isEmpty(str2)) {
                str3 = null;
            } else {
                str5 = str2.substring(0, 3);
                str3 = str2.substring(3);
            }
            jSONObject = g.a(g.a("carrier-name", telephonyManager.getNetworkOperatorName()), g.a("mobile-country-code", str5), g.a("mobile-network-code", str3), g.a("iso-country-code", telephonyManager.getNetworkCountryIso()), g.a("phone-type", Integer.valueOf(telephonyManager.getPhoneType())));
        }
        this.v = jSONObject;
        this.w = b.c();
        this.x = b.d();
        this.y = Integer.valueOf(u.b(context));
        try {
            if (context instanceof Activity) {
                Rect rect = new Rect();
                ((Activity) context).getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
                i2 = rect.width();
                try {
                    i4 = rect.height();
                } catch (Exception e4) {
                    e = e4;
                    a.b("RequestBody", "Exception getting activity size", e);
                    l a2 = l.a();
                    DisplayMetrics displayMetrics = new DisplayMetrics();
                    a2.a(displayMetrics);
                    DisplayMetrics displayMetrics2 = displayMetrics;
                    displayMetrics2.setTo(context.getResources().getDisplayMetrics());
                    windowManager.getDefaultDisplay().getRealMetrics(displayMetrics2);
                    i3 = displayMetrics2.widthPixels;
                    int i5 = displayMetrics2.heightPixels;
                    this.o = Integer.valueOf(i3);
                    this.p = Integer.valueOf(i5);
                    this.q = "" + displayMetrics2.densityDpi;
                    i3 = i2;
                    this.m = Integer.valueOf(i3);
                    this.n = Integer.valueOf(i4);
                }
            } else {
                i2 = 0;
            }
        } catch (Exception e5) {
            e = e5;
            i2 = 0;
            a.b("RequestBody", "Exception getting activity size", e);
            l a22 = l.a();
            DisplayMetrics displayMetrics3 = new DisplayMetrics();
            a22.a(displayMetrics3);
            DisplayMetrics displayMetrics22 = displayMetrics3;
            displayMetrics22.setTo(context.getResources().getDisplayMetrics());
            windowManager.getDefaultDisplay().getRealMetrics(displayMetrics22);
            i3 = displayMetrics22.widthPixels;
            int i52 = displayMetrics22.heightPixels;
            this.o = Integer.valueOf(i3);
            this.p = Integer.valueOf(i52);
            this.q = "" + displayMetrics22.densityDpi;
            i3 = i2;
            this.m = Integer.valueOf(i3);
            this.n = Integer.valueOf(i4);
        }
        l a222 = l.a();
        DisplayMetrics displayMetrics32 = new DisplayMetrics();
        a222.a(displayMetrics32);
        DisplayMetrics displayMetrics222 = displayMetrics32;
        displayMetrics222.setTo(context.getResources().getDisplayMetrics());
        if (Build.VERSION.SDK_INT >= 17 && (windowManager = (WindowManager) context.getSystemService("window")) != null) {
            windowManager.getDefaultDisplay().getRealMetrics(displayMetrics222);
        }
        i3 = displayMetrics222.widthPixels;
        int i522 = displayMetrics222.heightPixels;
        this.o = Integer.valueOf(i3);
        this.p = Integer.valueOf(i522);
        this.q = "" + displayMetrics222.densityDpi;
        if (i2 > 0 && i2 <= i3) {
            i3 = i2;
        }
        i4 = (i4 <= 0 || i4 > i522) ? i522 : i4;
        this.m = Integer.valueOf(i3);
        this.n = Integer.valueOf(i4);
    }
}
