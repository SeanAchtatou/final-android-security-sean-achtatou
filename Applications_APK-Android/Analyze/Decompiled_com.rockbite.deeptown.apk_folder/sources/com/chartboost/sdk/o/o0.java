package com.chartboost.sdk.o;

import java.io.Serializable;
import java.io.Writer;

public class o0 extends Writer implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private final StringBuilder f6122a;

    public o0(int i2) {
        this.f6122a = new StringBuilder(i2);
    }

    public void close() {
    }

    public void flush() {
    }

    public String toString() {
        return this.f6122a.toString();
    }

    public void write(String str) {
        if (str != null) {
            this.f6122a.append(str);
        }
    }

    public void write(char[] cArr, int i2, int i3) {
        if (cArr != null) {
            this.f6122a.append(cArr, i2, i3);
        }
    }

    public Writer append(char c2) {
        this.f6122a.append(c2);
        return this;
    }

    public Writer append(CharSequence charSequence) {
        this.f6122a.append(charSequence);
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.StringBuilder}
     arg types: [java.lang.CharSequence, int, int]
     candidates:
      ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.StringBuilder.append(char[], int, int):java.lang.StringBuilder}
      ClspMth{java.lang.Appendable.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.StringBuilder} */
    public Writer append(CharSequence charSequence, int i2, int i3) {
        this.f6122a.append(charSequence, i2, i3);
        return this;
    }
}
