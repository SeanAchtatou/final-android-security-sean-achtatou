package com.chartboost.sdk.o;

import com.chartboost.sdk.c.h;
import com.chartboost.sdk.d.c;
import com.chartboost.sdk.d.f;
import com.chartboost.sdk.e.a;
import com.chartboost.sdk.n;
import com.chartboost.sdk.o.m;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

public class z0 implements m.a {

    /* renamed from: a  reason: collision with root package name */
    public final y0 f6250a;

    /* renamed from: b  reason: collision with root package name */
    private final h f6251b;

    /* renamed from: c  reason: collision with root package name */
    private final k f6252c;

    /* renamed from: d  reason: collision with root package name */
    private final s f6253d;

    /* renamed from: e  reason: collision with root package name */
    private final a f6254e;

    /* renamed from: f  reason: collision with root package name */
    private final AtomicReference<f> f6255f;

    /* renamed from: g  reason: collision with root package name */
    private int f6256g = 1;

    /* renamed from: h  reason: collision with root package name */
    private int f6257h = 0;

    /* renamed from: i  reason: collision with root package name */
    private long f6258i = 0;

    /* renamed from: j  reason: collision with root package name */
    private m f6259j = null;

    /* renamed from: k  reason: collision with root package name */
    private AtomicInteger f6260k = null;

    public z0(y0 y0Var, h hVar, k kVar, s sVar, a aVar, AtomicReference<f> atomicReference) {
        this.f6250a = y0Var;
        this.f6251b = hVar;
        this.f6252c = kVar;
        this.f6253d = sVar;
        this.f6254e = aVar;
        this.f6255f = atomicReference;
    }

    public synchronized void a() {
        try {
            com.chartboost.sdk.c.a.b("Chartboost SDK", "Sdk Version = 7.5.0, Commit: 7fc7bc32841a43689553f0e08928c7ad6ed7e23b");
            f fVar = this.f6255f.get();
            a(fVar);
            if (!fVar.f5843c && !fVar.f5842b) {
                if (n.v) {
                    if (this.f6256g == 3) {
                        if (this.f6260k.get() <= 0) {
                            com.chartboost.sdk.c.a.a("Prefetcher", "Change state to COOLDOWN");
                            this.f6256g = 4;
                            this.f6260k = null;
                        } else {
                            return;
                        }
                    }
                    if (this.f6256g == 4) {
                        if (this.f6258i - System.nanoTime() > 0) {
                            com.chartboost.sdk.c.a.a("Prefetcher", "Prefetch session is still active. Won't be making any new prefetch until the prefetch session expires");
                            return;
                        }
                        com.chartboost.sdk.c.a.a("Prefetcher", "Change state to IDLE");
                        this.f6256g = 1;
                        this.f6257h = 0;
                        this.f6258i = 0;
                    }
                    if (this.f6256g == 1) {
                        if (fVar.v) {
                            p pVar = new p(fVar.B, this.f6253d, this.f6254e, 2, this);
                            pVar.a("cache_assets", this.f6251b.c(), 0);
                            pVar.m = true;
                            com.chartboost.sdk.c.a.a("Prefetcher", "Change state to AWAIT_PREFETCH_RESPONSE");
                            this.f6256g = 2;
                            this.f6257h = 2;
                            this.f6258i = System.nanoTime() + TimeUnit.MINUTES.toNanos((long) fVar.y);
                            this.f6259j = pVar;
                        } else if (fVar.f5845e) {
                            m mVar = new m("/api/video-prefetch", this.f6253d, this.f6254e, 2, this);
                            mVar.a("local-videos", this.f6251b.b());
                            mVar.m = true;
                            com.chartboost.sdk.c.a.a("Prefetcher", "Change state to AWAIT_PREFETCH_RESPONSE");
                            this.f6256g = 2;
                            this.f6257h = 1;
                            this.f6258i = System.nanoTime() + TimeUnit.MINUTES.toNanos((long) fVar.f5848h);
                            this.f6259j = mVar;
                        } else {
                            com.chartboost.sdk.c.a.b("Prefetcher", "Did not prefetch because neither native nor webview are enabled.");
                            return;
                        }
                        this.f6252c.a(this.f6259j);
                    } else {
                        return;
                    }
                }
            }
            b();
        } catch (Exception e2) {
            if (this.f6256g == 2) {
                com.chartboost.sdk.c.a.a("Prefetcher", "Change state to COOLDOWN");
                this.f6256g = 4;
                this.f6259j = null;
            }
            a.a(getClass(), "prefetch", e2);
        }
    }

    public synchronized void b() {
        if (this.f6256g == 2) {
            com.chartboost.sdk.c.a.a("Prefetcher", "Change state to COOLDOWN");
            this.f6256g = 4;
            this.f6259j = null;
        } else if (this.f6256g == 3) {
            com.chartboost.sdk.c.a.a("Prefetcher", "Change state to COOLDOWN");
            this.f6256g = 4;
            AtomicInteger atomicInteger = this.f6260k;
            this.f6260k = null;
            if (atomicInteger != null) {
                this.f6250a.a(atomicInteger);
            }
        }
    }

    private void a(f fVar) {
        boolean z = fVar.v;
        if ((this.f6257h == 1 && !(!z && fVar.f5845e)) || (this.f6257h == 2 && !z)) {
            com.chartboost.sdk.c.a.a("Prefetcher", "Change state to IDLE");
            this.f6256g = 1;
            this.f6257h = 0;
            this.f6258i = 0;
            this.f6259j = null;
            AtomicInteger atomicInteger = this.f6260k;
            this.f6260k = null;
            if (atomicInteger != null) {
                this.f6250a.a(atomicInteger);
            }
        }
    }

    public synchronized void a(m mVar, JSONObject jSONObject) {
        try {
            if (this.f6256g == 2) {
                if (mVar == this.f6259j) {
                    com.chartboost.sdk.c.a.a("Prefetcher", "Change state to DOWNLOAD_ASSETS");
                    this.f6256g = 3;
                    this.f6259j = null;
                    this.f6260k = new AtomicInteger();
                    if (jSONObject != null) {
                        com.chartboost.sdk.c.a.a("Prefetcher", "Got Asset list for Prefetch from server :)" + jSONObject);
                        if (this.f6257h == 1) {
                            this.f6250a.a(3, c.a(jSONObject), this.f6260k, null);
                        } else if (this.f6257h == 2) {
                            this.f6250a.a(3, c.a(jSONObject, this.f6255f.get().s), this.f6260k, null);
                        }
                    }
                } else {
                    return;
                }
            } else {
                return;
            }
        } catch (Exception e2) {
            a.a(getClass(), "onSuccess", e2);
        }
        return;
    }

    public synchronized void a(m mVar, com.chartboost.sdk.d.a aVar) {
        if (this.f6256g == 2) {
            if (mVar == this.f6259j) {
                this.f6259j = null;
                com.chartboost.sdk.c.a.a("Prefetcher", "Change state to COOLDOWN");
                this.f6256g = 4;
            }
        }
    }
}
