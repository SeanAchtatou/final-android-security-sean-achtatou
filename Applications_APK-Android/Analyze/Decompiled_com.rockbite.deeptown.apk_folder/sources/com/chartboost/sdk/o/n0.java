package com.chartboost.sdk.o;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class n0 extends OutputStream {

    /* renamed from: f  reason: collision with root package name */
    private static final byte[] f6114f = new byte[0];

    /* renamed from: a  reason: collision with root package name */
    private final List<byte[]> f6115a;

    /* renamed from: b  reason: collision with root package name */
    private int f6116b;

    /* renamed from: c  reason: collision with root package name */
    private int f6117c;

    /* renamed from: d  reason: collision with root package name */
    private byte[] f6118d;

    /* renamed from: e  reason: collision with root package name */
    private int f6119e;

    public n0() {
        this(1024);
    }

    private void a(int i2) {
        if (this.f6116b < this.f6115a.size() - 1) {
            this.f6117c += this.f6118d.length;
            this.f6116b++;
            this.f6118d = this.f6115a.get(this.f6116b);
            return;
        }
        byte[] bArr = this.f6118d;
        if (bArr == null) {
            this.f6117c = 0;
        } else {
            i2 = Math.max(bArr.length << 1, i2 - this.f6117c);
            this.f6117c += this.f6118d.length;
        }
        this.f6116b++;
        this.f6118d = new byte[i2];
        this.f6115a.add(this.f6118d);
    }

    public void close() throws IOException {
    }

    public String toString() {
        return new String(a());
    }

    public void write(byte[] bArr, int i2, int i3) {
        int i4;
        if (i2 < 0 || i2 > bArr.length || i3 < 0 || (i4 = i2 + i3) > bArr.length || i4 < 0) {
            throw new IndexOutOfBoundsException();
        } else if (i3 != 0) {
            synchronized (this) {
                int i5 = this.f6119e + i3;
                int i6 = this.f6119e - this.f6117c;
                while (i3 > 0) {
                    int min = Math.min(i3, this.f6118d.length - i6);
                    System.arraycopy(bArr, i4 - i3, this.f6118d, i6, min);
                    i3 -= min;
                    if (i3 > 0) {
                        a(i5);
                        i6 = 0;
                    }
                }
                this.f6119e = i5;
            }
        }
    }

    public n0(int i2) {
        this.f6115a = new ArrayList();
        if (i2 >= 0) {
            synchronized (this) {
                a(i2);
            }
            return;
        }
        throw new IllegalArgumentException("Negative initial size: " + i2);
    }

    public synchronized void write(int i2) {
        int i3 = this.f6119e - this.f6117c;
        if (i3 == this.f6118d.length) {
            a(this.f6119e + 1);
            i3 = 0;
        }
        this.f6118d[i3] = (byte) i2;
        this.f6119e++;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002c, code lost:
        return r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized byte[] a() {
        /*
            r7 = this;
            monitor-enter(r7)
            int r0 = r7.f6119e     // Catch:{ all -> 0x002d }
            if (r0 != 0) goto L_0x0009
            byte[] r0 = com.chartboost.sdk.o.n0.f6114f     // Catch:{ all -> 0x002d }
            monitor-exit(r7)
            return r0
        L_0x0009:
            byte[] r1 = new byte[r0]     // Catch:{ all -> 0x002d }
            java.util.List<byte[]> r2 = r7.f6115a     // Catch:{ all -> 0x002d }
            java.util.Iterator r2 = r2.iterator()     // Catch:{ all -> 0x002d }
            r3 = 0
            r4 = 0
        L_0x0013:
            boolean r5 = r2.hasNext()     // Catch:{ all -> 0x002d }
            if (r5 == 0) goto L_0x002b
            java.lang.Object r5 = r2.next()     // Catch:{ all -> 0x002d }
            byte[] r5 = (byte[]) r5     // Catch:{ all -> 0x002d }
            int r6 = r5.length     // Catch:{ all -> 0x002d }
            int r6 = java.lang.Math.min(r6, r0)     // Catch:{ all -> 0x002d }
            java.lang.System.arraycopy(r5, r3, r1, r4, r6)     // Catch:{ all -> 0x002d }
            int r4 = r4 + r6
            int r0 = r0 - r6
            if (r0 != 0) goto L_0x0013
        L_0x002b:
            monitor-exit(r7)
            return r1
        L_0x002d:
            r0 = move-exception
            monitor-exit(r7)
            goto L_0x0031
        L_0x0030:
            throw r0
        L_0x0031:
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.o.n0.a():byte[]");
    }
}
