package com.chartboost.sdk.o;

import com.chartboost.sdk.c.h;
import com.chartboost.sdk.c.k;
import com.chartboost.sdk.d.c;
import com.chartboost.sdk.d.f;
import java.io.File;
import java.util.Comparator;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class y0 {

    /* renamed from: a  reason: collision with root package name */
    private final Executor f6233a;

    /* renamed from: b  reason: collision with root package name */
    private final k f6234b;

    /* renamed from: c  reason: collision with root package name */
    private final l f6235c;

    /* renamed from: d  reason: collision with root package name */
    private final AtomicReference<f> f6236d;

    /* renamed from: e  reason: collision with root package name */
    private final k f6237e;

    /* renamed from: f  reason: collision with root package name */
    private final com.chartboost.sdk.e.a f6238f;

    /* renamed from: g  reason: collision with root package name */
    private final h f6239g;

    /* renamed from: h  reason: collision with root package name */
    int f6240h = 1;

    /* renamed from: i  reason: collision with root package name */
    private x0 f6241i = null;

    /* renamed from: j  reason: collision with root package name */
    private final PriorityQueue<w0> f6242j;

    class a implements Comparator<File> {
        a(y0 y0Var) {
        }

        /* renamed from: a */
        public int compare(File file, File file2) {
            return Long.valueOf(file.lastModified()).compareTo(Long.valueOf(file2.lastModified()));
        }
    }

    public y0(Executor executor, h hVar, k kVar, l lVar, AtomicReference<f> atomicReference, k kVar2, com.chartboost.sdk.e.a aVar) {
        this.f6233a = executor;
        this.f6239g = hVar;
        this.f6234b = kVar;
        this.f6235c = lVar;
        this.f6236d = atomicReference;
        this.f6237e = kVar2;
        this.f6238f = aVar;
        this.f6242j = new PriorityQueue<>();
    }

    private void d() {
        w0 poll;
        w0 peek;
        if (!(this.f6241i == null || (peek = this.f6242j.peek()) == null)) {
            x0 x0Var = this.f6241i;
            if (x0Var.l.f6210b > peek.f6210b && x0Var.b()) {
                this.f6242j.add(this.f6241i.l);
                this.f6241i = null;
            }
        }
        while (this.f6241i == null && (poll = this.f6242j.poll()) != null) {
            if (poll.f6214f.get() > 0) {
                File file = new File(this.f6239g.d().f5773a, poll.f6213e);
                if (file.exists() || file.mkdirs() || file.isDirectory()) {
                    File file2 = new File(file, poll.f6211c);
                    if (file2.exists()) {
                        this.f6239g.c(file2);
                        poll.a(this.f6233a, true);
                    } else {
                        this.f6241i = new x0(this, this.f6235c, poll, file2);
                        this.f6234b.a(this.f6241i);
                        this.f6238f.a(poll.f6212d, poll.f6211c);
                    }
                } else {
                    com.chartboost.sdk.c.a.b("Downloader", "Unable to create directory " + file.getPath());
                    poll.a(this.f6233a, false);
                }
            }
        }
        if (this.f6241i != null) {
            if (this.f6240h != 2) {
                com.chartboost.sdk.c.a.a("Downloader", "Change state to DOWNLOADING");
                this.f6240h = 2;
            }
        } else if (this.f6240h != 1) {
            com.chartboost.sdk.c.a.a("Downloader", "Change state to IDLE");
            this.f6240h = 1;
        }
    }

    public synchronized void a(int i2, Map<String, c> map, AtomicInteger atomicInteger, u0 u0Var) {
        long b2 = this.f6237e.b();
        AtomicInteger atomicInteger2 = new AtomicInteger();
        AtomicReference atomicReference = new AtomicReference(u0Var);
        for (c next : map.values()) {
            long j2 = b2;
            long j3 = b2;
            w0 w0Var = r2;
            w0 w0Var2 = new w0(this.f6237e, i2, next.f5828b, next.f5829c, next.f5827a, atomicInteger, atomicReference, j2, atomicInteger2);
            this.f6242j.add(w0Var);
            b2 = j3;
        }
        if (this.f6240h == 1 || this.f6240h == 2) {
            d();
        }
    }

    public synchronized void b() {
        int i2 = this.f6240h;
        if (!(i2 == 1 || i2 == 2)) {
            if (i2 == 3) {
                com.chartboost.sdk.c.a.a("Downloader", "Change state to DOWNLOADING");
                this.f6240h = 2;
            } else if (i2 == 4) {
                com.chartboost.sdk.c.a.a("Downloader", "Change state to IDLE");
                this.f6240h = 1;
                d();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.c.b.a(java.io.File, boolean):java.util.ArrayList<java.io.File>
     arg types: [java.io.File, int]
     candidates:
      com.chartboost.sdk.c.b.a(float, android.content.Context):float
      com.chartboost.sdk.c.b.a(int, android.content.Context):int
      com.chartboost.sdk.c.b.a(java.io.File, boolean):java.util.ArrayList<java.io.File> */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0142 A[Catch:{ Exception -> 0x0198 }] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0183 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void c() {
        /*
            r24 = this;
            r1 = r24
            monitor-enter(r24)
            int r0 = r1.f6240h     // Catch:{ all -> 0x01a4 }
            r2 = 1
            if (r0 == r2) goto L_0x000a
            monitor-exit(r24)
            return
        L_0x000a:
            java.lang.String r0 = "Downloader"
            java.lang.String r3 = "########### Trimming the disk cache"
            com.chartboost.sdk.c.a.a(r0, r3)     // Catch:{ Exception -> 0x0198 }
            com.chartboost.sdk.c.h r0 = r1.f6239g     // Catch:{ Exception -> 0x0198 }
            com.chartboost.sdk.c.i r0 = r0.d()     // Catch:{ Exception -> 0x0198 }
            java.io.File r0 = r0.f5773a     // Catch:{ Exception -> 0x0198 }
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ Exception -> 0x0198 }
            r3.<init>()     // Catch:{ Exception -> 0x0198 }
            java.lang.String[] r4 = r0.list()     // Catch:{ Exception -> 0x0198 }
            if (r4 == 0) goto L_0x0065
            int r6 = r4.length     // Catch:{ Exception -> 0x0198 }
            if (r6 <= 0) goto L_0x0065
            int r6 = r4.length     // Catch:{ Exception -> 0x0198 }
            r7 = 0
        L_0x0029:
            if (r7 >= r6) goto L_0x0065
            r8 = r4[r7]     // Catch:{ Exception -> 0x0198 }
            java.lang.String r9 = "requests"
            boolean r9 = r8.equalsIgnoreCase(r9)     // Catch:{ Exception -> 0x0198 }
            if (r9 != 0) goto L_0x0062
            java.lang.String r9 = "track"
            boolean r9 = r8.equalsIgnoreCase(r9)     // Catch:{ Exception -> 0x0198 }
            if (r9 != 0) goto L_0x0062
            java.lang.String r9 = "session"
            boolean r9 = r8.equalsIgnoreCase(r9)     // Catch:{ Exception -> 0x0198 }
            if (r9 != 0) goto L_0x0062
            java.lang.String r9 = "videoCompletionEvents"
            boolean r9 = r8.equalsIgnoreCase(r9)     // Catch:{ Exception -> 0x0198 }
            if (r9 != 0) goto L_0x0062
            java.lang.String r9 = "."
            boolean r9 = r8.contains(r9)     // Catch:{ Exception -> 0x0198 }
            if (r9 == 0) goto L_0x0056
            goto L_0x0062
        L_0x0056:
            java.io.File r9 = new java.io.File     // Catch:{ Exception -> 0x0198 }
            r9.<init>(r0, r8)     // Catch:{ Exception -> 0x0198 }
            java.util.ArrayList r8 = com.chartboost.sdk.c.b.a(r9, r2)     // Catch:{ Exception -> 0x0198 }
            r3.addAll(r8)     // Catch:{ Exception -> 0x0198 }
        L_0x0062:
            int r7 = r7 + 1
            goto L_0x0029
        L_0x0065:
            int r0 = r3.size()     // Catch:{ Exception -> 0x0198 }
            java.io.File[] r0 = new java.io.File[r0]     // Catch:{ Exception -> 0x0198 }
            r3.toArray(r0)     // Catch:{ Exception -> 0x0198 }
            int r3 = r0.length     // Catch:{ Exception -> 0x0198 }
            if (r3 <= r2) goto L_0x0079
            com.chartboost.sdk.o.y0$a r3 = new com.chartboost.sdk.o.y0$a     // Catch:{ Exception -> 0x0198 }
            r3.<init>(r1)     // Catch:{ Exception -> 0x0198 }
            java.util.Arrays.sort(r0, r3)     // Catch:{ Exception -> 0x0198 }
        L_0x0079:
            int r3 = r0.length     // Catch:{ Exception -> 0x0198 }
            if (r3 <= 0) goto L_0x018c
            java.util.concurrent.atomic.AtomicReference<com.chartboost.sdk.d.f> r3 = r1.f6236d     // Catch:{ Exception -> 0x0198 }
            java.lang.Object r3 = r3.get()     // Catch:{ Exception -> 0x0198 }
            com.chartboost.sdk.d.f r3 = (com.chartboost.sdk.d.f) r3     // Catch:{ Exception -> 0x0198 }
            int r4 = r3.r     // Catch:{ Exception -> 0x0198 }
            long r6 = (long) r4     // Catch:{ Exception -> 0x0198 }
            com.chartboost.sdk.c.h r4 = r1.f6239g     // Catch:{ Exception -> 0x0198 }
            com.chartboost.sdk.c.h r8 = r1.f6239g     // Catch:{ Exception -> 0x0198 }
            com.chartboost.sdk.c.i r8 = r8.d()     // Catch:{ Exception -> 0x0198 }
            java.io.File r8 = r8.f5776d     // Catch:{ Exception -> 0x0198 }
            long r8 = r4.b(r8)     // Catch:{ Exception -> 0x0198 }
            com.chartboost.sdk.c.k r4 = r1.f6237e     // Catch:{ Exception -> 0x0198 }
            long r10 = r4.a()     // Catch:{ Exception -> 0x0198 }
            java.util.List<java.lang.String> r4 = r3.f5844d     // Catch:{ Exception -> 0x0198 }
            java.lang.String r12 = "Downloader"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
            r13.<init>()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r14 = "Total local file count:"
            r13.append(r14)     // Catch:{ Exception -> 0x0198 }
            int r14 = r0.length     // Catch:{ Exception -> 0x0198 }
            r13.append(r14)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r13 = r13.toString()     // Catch:{ Exception -> 0x0198 }
            com.chartboost.sdk.c.a.a(r12, r13)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r12 = "Downloader"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
            r13.<init>()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r14 = "Video Folder Size in bytes :"
            r13.append(r14)     // Catch:{ Exception -> 0x0198 }
            r13.append(r8)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r13 = r13.toString()     // Catch:{ Exception -> 0x0198 }
            com.chartboost.sdk.c.a.a(r12, r13)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r12 = "Downloader"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
            r13.<init>()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r14 = "Max Bytes allowed:"
            r13.append(r14)     // Catch:{ Exception -> 0x0198 }
            r13.append(r6)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r13 = r13.toString()     // Catch:{ Exception -> 0x0198 }
            com.chartboost.sdk.c.a.a(r12, r13)     // Catch:{ Exception -> 0x0198 }
            int r12 = r0.length     // Catch:{ Exception -> 0x0198 }
            r13 = r8
            r8 = 0
        L_0x00e3:
            if (r8 >= r12) goto L_0x018c
            r9 = r0[r8]     // Catch:{ Exception -> 0x0198 }
            java.util.concurrent.TimeUnit r15 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ Exception -> 0x0198 }
            long r16 = r9.lastModified()     // Catch:{ Exception -> 0x0198 }
            r18 = r6
            long r5 = r10 - r16
            long r5 = r15.toDays(r5)     // Catch:{ Exception -> 0x0198 }
            int r7 = r3.t     // Catch:{ Exception -> 0x0198 }
            r16 = r3
            long r2 = (long) r7     // Catch:{ Exception -> 0x0198 }
            int r7 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r7 < 0) goto L_0x0100
            r2 = 1
            goto L_0x0101
        L_0x0100:
            r2 = 0
        L_0x0101:
            java.lang.String r3 = r9.getName()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r5 = ".tmp"
            boolean r3 = r3.endsWith(r5)     // Catch:{ Exception -> 0x0198 }
            java.io.File r5 = r9.getParentFile()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r6 = r5.getAbsolutePath()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r7 = "/videos"
            boolean r6 = r6.contains(r7)     // Catch:{ Exception -> 0x0198 }
            int r7 = (r13 > r18 ? 1 : (r13 == r18 ? 0 : -1))
            if (r7 <= 0) goto L_0x0121
            if (r6 == 0) goto L_0x0121
            r7 = 1
            goto L_0x0122
        L_0x0121:
            r7 = 0
        L_0x0122:
            long r20 = r9.length()     // Catch:{ Exception -> 0x0198 }
            r22 = 0
            int r17 = (r20 > r22 ? 1 : (r20 == r22 ? 0 : -1))
            if (r17 == 0) goto L_0x013f
            if (r3 != 0) goto L_0x013f
            if (r2 != 0) goto L_0x013f
            java.lang.String r2 = r5.getName()     // Catch:{ Exception -> 0x0198 }
            boolean r2 = r4.contains(r2)     // Catch:{ Exception -> 0x0198 }
            if (r2 != 0) goto L_0x013f
            if (r7 == 0) goto L_0x013d
            goto L_0x013f
        L_0x013d:
            r2 = 0
            goto L_0x0140
        L_0x013f:
            r2 = 1
        L_0x0140:
            if (r2 == 0) goto L_0x0183
            if (r6 == 0) goto L_0x0149
            long r2 = r9.length()     // Catch:{ Exception -> 0x0198 }
            long r13 = r13 - r2
        L_0x0149:
            java.lang.String r2 = "Downloader"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
            r3.<init>()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r5 = "Deleting file at path:"
            r3.append(r5)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r5 = r9.getPath()     // Catch:{ Exception -> 0x0198 }
            r3.append(r5)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0198 }
            com.chartboost.sdk.c.a.a(r2, r3)     // Catch:{ Exception -> 0x0198 }
            boolean r2 = r9.delete()     // Catch:{ Exception -> 0x0198 }
            if (r2 != 0) goto L_0x0183
            java.lang.String r2 = "Downloader"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
            r3.<init>()     // Catch:{ Exception -> 0x0198 }
            java.lang.String r5 = "Unable to delete "
            r3.append(r5)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r5 = r9.getPath()     // Catch:{ Exception -> 0x0198 }
            r3.append(r5)     // Catch:{ Exception -> 0x0198 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0198 }
            com.chartboost.sdk.c.a.b(r2, r3)     // Catch:{ Exception -> 0x0198 }
        L_0x0183:
            int r8 = r8 + 1
            r3 = r16
            r6 = r18
            r2 = 1
            goto L_0x00e3
        L_0x018c:
            com.chartboost.sdk.c.h r0 = r1.f6239g     // Catch:{ Exception -> 0x0198 }
            org.json.JSONObject r0 = r0.e()     // Catch:{ Exception -> 0x0198 }
            com.chartboost.sdk.e.a r2 = r1.f6238f     // Catch:{ Exception -> 0x0198 }
            r2.a(r0)     // Catch:{ Exception -> 0x0198 }
            goto L_0x01a2
        L_0x0198:
            r0 = move-exception
            java.lang.Class r2 = r24.getClass()     // Catch:{ all -> 0x01a4 }
            java.lang.String r3 = "reduceCacheSize"
            com.chartboost.sdk.e.a.a(r2, r3, r0)     // Catch:{ all -> 0x01a4 }
        L_0x01a2:
            monitor-exit(r24)
            return
        L_0x01a4:
            r0 = move-exception
            monitor-exit(r24)
            goto L_0x01a8
        L_0x01a7:
            throw r0
        L_0x01a8:
            goto L_0x01a7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.o.y0.c():void");
    }

    public synchronized void a(AtomicInteger atomicInteger) {
        atomicInteger.set(-10000);
        int i2 = this.f6240h;
        boolean z = true;
        if (i2 != 1) {
            if (i2 == 2) {
                if (this.f6241i.l.f6214f != atomicInteger) {
                    z = false;
                }
                if (z && this.f6241i.b()) {
                    this.f6241i = null;
                    d();
                }
            }
        }
    }

    public synchronized void a() {
        int i2 = this.f6240h;
        if (i2 == 1) {
            com.chartboost.sdk.c.a.a("Downloader", "Change state to PAUSED");
            this.f6240h = 4;
        } else if (i2 == 2) {
            if (this.f6241i.b()) {
                this.f6242j.add(this.f6241i.l);
                this.f6241i = null;
                com.chartboost.sdk.c.a.a("Downloader", "Change state to PAUSED");
                this.f6240h = 4;
            } else {
                com.chartboost.sdk.c.a.a("Downloader", "Change state to PAUSING");
                this.f6240h = 3;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00d7, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a(com.chartboost.sdk.o.x0 r19, com.chartboost.sdk.d.a r20, com.chartboost.sdk.o.j r21) {
        /*
            r18 = this;
            r1 = r18
            r0 = r19
            r2 = r21
            monitor-enter(r18)
            int r3 = r1.f6240h     // Catch:{ all -> 0x00d8 }
            r4 = 1
            if (r3 == r4) goto L_0x00d6
            r5 = 2
            r6 = 3
            if (r3 == r5) goto L_0x0014
            if (r3 == r6) goto L_0x0014
            goto L_0x00d6
        L_0x0014:
            com.chartboost.sdk.o.x0 r3 = r1.f6241i     // Catch:{ all -> 0x00d8 }
            if (r0 == r3) goto L_0x001a
            monitor-exit(r18)
            return
        L_0x001a:
            com.chartboost.sdk.o.w0 r3 = r0.l     // Catch:{ all -> 0x00d8 }
            r5 = 0
            r1.f6241i = r5     // Catch:{ all -> 0x00d8 }
            java.util.concurrent.TimeUnit r5 = java.util.concurrent.TimeUnit.NANOSECONDS     // Catch:{ all -> 0x00d8 }
            long r7 = r0.f6015f     // Catch:{ all -> 0x00d8 }
            long r12 = r5.toMillis(r7)     // Catch:{ all -> 0x00d8 }
            java.util.concurrent.atomic.AtomicInteger r5 = r3.f6217i     // Catch:{ all -> 0x00d8 }
            int r7 = (int) r12     // Catch:{ all -> 0x00d8 }
            r5.addAndGet(r7)     // Catch:{ all -> 0x00d8 }
            java.util.concurrent.Executor r5 = r1.f6233a     // Catch:{ all -> 0x00d8 }
            if (r20 != 0) goto L_0x0032
            goto L_0x0033
        L_0x0032:
            r4 = 0
        L_0x0033:
            r3.a(r5, r4)     // Catch:{ all -> 0x00d8 }
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.NANOSECONDS     // Catch:{ all -> 0x00d8 }
            long r7 = r0.f6016g     // Catch:{ all -> 0x00d8 }
            long r14 = r4.toMillis(r7)     // Catch:{ all -> 0x00d8 }
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.NANOSECONDS     // Catch:{ all -> 0x00d8 }
            long r7 = r0.f6017h     // Catch:{ all -> 0x00d8 }
            long r16 = r4.toMillis(r7)     // Catch:{ all -> 0x00d8 }
            if (r20 != 0) goto L_0x006c
            com.chartboost.sdk.e.a r9 = r1.f6238f     // Catch:{ all -> 0x00d8 }
            java.lang.String r10 = r3.f6212d     // Catch:{ all -> 0x00d8 }
            r11 = r12
            r13 = r14
            r15 = r16
            r9.a(r10, r11, r13, r15)     // Catch:{ all -> 0x00d8 }
            java.lang.String r0 = "Downloader"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d8 }
            r2.<init>()     // Catch:{ all -> 0x00d8 }
            java.lang.String r4 = "Downloaded "
            r2.append(r4)     // Catch:{ all -> 0x00d8 }
            java.lang.String r3 = r3.f6212d     // Catch:{ all -> 0x00d8 }
            r2.append(r3)     // Catch:{ all -> 0x00d8 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x00d8 }
            com.chartboost.sdk.c.a.a(r0, r2)     // Catch:{ all -> 0x00d8 }
            goto L_0x00c4
        L_0x006c:
            java.lang.String r0 = r20.b()     // Catch:{ all -> 0x00d8 }
            com.chartboost.sdk.e.a r9 = r1.f6238f     // Catch:{ all -> 0x00d8 }
            java.lang.String r10 = r3.f6212d     // Catch:{ all -> 0x00d8 }
            r11 = r0
            r9.a(r10, r11, r12, r14, r16)     // Catch:{ all -> 0x00d8 }
            java.lang.String r4 = "Downloader"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d8 }
            r5.<init>()     // Catch:{ all -> 0x00d8 }
            java.lang.String r7 = "Failed to download "
            r5.append(r7)     // Catch:{ all -> 0x00d8 }
            java.lang.String r3 = r3.f6212d     // Catch:{ all -> 0x00d8 }
            r5.append(r3)     // Catch:{ all -> 0x00d8 }
            if (r2 == 0) goto L_0x009f
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d8 }
            r3.<init>()     // Catch:{ all -> 0x00d8 }
            java.lang.String r7 = " Status code="
            r3.append(r7)     // Catch:{ all -> 0x00d8 }
            int r2 = r2.f6050a     // Catch:{ all -> 0x00d8 }
            r3.append(r2)     // Catch:{ all -> 0x00d8 }
            java.lang.String r2 = r3.toString()     // Catch:{ all -> 0x00d8 }
            goto L_0x00a1
        L_0x009f:
            java.lang.String r2 = ""
        L_0x00a1:
            r5.append(r2)     // Catch:{ all -> 0x00d8 }
            if (r0 == 0) goto L_0x00b8
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d8 }
            r2.<init>()     // Catch:{ all -> 0x00d8 }
            java.lang.String r3 = " Error message="
            r2.append(r3)     // Catch:{ all -> 0x00d8 }
            r2.append(r0)     // Catch:{ all -> 0x00d8 }
            java.lang.String r0 = r2.toString()     // Catch:{ all -> 0x00d8 }
            goto L_0x00ba
        L_0x00b8:
            java.lang.String r0 = ""
        L_0x00ba:
            r5.append(r0)     // Catch:{ all -> 0x00d8 }
            java.lang.String r0 = r5.toString()     // Catch:{ all -> 0x00d8 }
            com.chartboost.sdk.c.a.a(r4, r0)     // Catch:{ all -> 0x00d8 }
        L_0x00c4:
            int r0 = r1.f6240h     // Catch:{ all -> 0x00d8 }
            if (r0 != r6) goto L_0x00d3
            java.lang.String r0 = "Downloader"
            java.lang.String r2 = "Change state to PAUSED"
            com.chartboost.sdk.c.a.a(r0, r2)     // Catch:{ all -> 0x00d8 }
            r0 = 4
            r1.f6240h = r0     // Catch:{ all -> 0x00d8 }
            goto L_0x00d6
        L_0x00d3:
            r18.d()     // Catch:{ all -> 0x00d8 }
        L_0x00d6:
            monitor-exit(r18)
            return
        L_0x00d8:
            r0 = move-exception
            monitor-exit(r18)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.o.y0.a(com.chartboost.sdk.o.x0, com.chartboost.sdk.d.a, com.chartboost.sdk.o.j):void");
    }
}
