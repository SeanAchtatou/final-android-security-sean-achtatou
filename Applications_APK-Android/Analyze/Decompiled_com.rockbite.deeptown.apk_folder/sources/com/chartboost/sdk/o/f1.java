package com.chartboost.sdk.o;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import java.io.File;
import java.io.IOException;

public class f1 {

    /* renamed from: b  reason: collision with root package name */
    private static f1 f6008b = new f1(new Handler(Looper.getMainLooper()));

    /* renamed from: a  reason: collision with root package name */
    public final Handler f6009a;

    public f1(Handler handler) {
        this.f6009a = handler;
    }

    public static f1 e() {
        return f6008b;
    }

    public File a() {
        return Environment.getExternalStorageDirectory();
    }

    public String b() {
        return Environment.getExternalStorageState();
    }

    public String c() {
        return Build.VERSION.RELEASE;
    }

    public boolean d() {
        return Looper.myLooper() == Looper.getMainLooper();
    }

    public boolean a(int i2) {
        return Build.VERSION.SDK_INT >= i2;
    }

    public AdvertisingIdClient.Info a(Context context) throws IOException, GooglePlayServicesNotAvailableException, GooglePlayServicesRepairableException, IllegalStateException {
        return AdvertisingIdClient.getAdvertisingIdInfo(context);
    }

    public boolean a(CharSequence charSequence) {
        return TextUtils.isEmpty(charSequence);
    }
}
