package com.chartboost.sdk.o;

import com.google.protobuf.CodedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

public class m0 {
    static {
        char c2 = File.separatorChar;
        o0 o0Var = new o0(4);
        PrintWriter printWriter = new PrintWriter(o0Var);
        printWriter.println();
        o0Var.toString();
        printWriter.close();
    }

    public static void a(InputStream inputStream) {
        a((Closeable) inputStream);
    }

    public static byte[] b(InputStream inputStream) throws IOException {
        n0 n0Var = new n0();
        a(inputStream, n0Var);
        return n0Var.a();
    }

    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException unused) {
            }
        }
    }

    public static byte[] a(InputStream inputStream, long j2) throws IOException {
        if (j2 <= 2147483647L) {
            return a(inputStream, (int) j2);
        }
        throw new IllegalArgumentException("Size cannot be greater than Integer max value: " + j2);
    }

    public static long b(InputStream inputStream, OutputStream outputStream) throws IOException {
        return a(inputStream, outputStream, new byte[CodedOutputStream.DEFAULT_BUFFER_SIZE]);
    }

    public static byte[] a(InputStream inputStream, int i2) throws IOException {
        if (i2 >= 0) {
            int i3 = 0;
            if (i2 == 0) {
                return new byte[0];
            }
            byte[] bArr = new byte[i2];
            while (i3 < i2) {
                int read = inputStream.read(bArr, i3, i2 - i3);
                if (read == -1) {
                    break;
                }
                i3 += read;
            }
            if (i3 == i2) {
                return bArr;
            }
            throw new IOException("Unexpected readed size. current: " + i3 + ", excepted: " + i2);
        }
        throw new IllegalArgumentException("Size must be equal or greater than zero: " + i2);
    }

    public static int a(InputStream inputStream, OutputStream outputStream) throws IOException {
        long b2 = b(inputStream, outputStream);
        if (b2 > 2147483647L) {
            return -1;
        }
        return (int) b2;
    }

    public static long a(InputStream inputStream, OutputStream outputStream, byte[] bArr) throws IOException {
        long j2 = 0;
        while (true) {
            int read = inputStream.read(bArr);
            if (-1 == read) {
                return j2;
            }
            outputStream.write(bArr, 0, read);
            j2 += (long) read;
        }
    }
}
