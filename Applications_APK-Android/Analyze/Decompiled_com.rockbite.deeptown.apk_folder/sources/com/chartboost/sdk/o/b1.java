package com.chartboost.sdk.o;

import android.app.Application;
import android.webkit.WebView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;

public class b1 {

    /* renamed from: a  reason: collision with root package name */
    static List<String> f5961a = new ArrayList();

    /* renamed from: b  reason: collision with root package name */
    static HashMap<String, c1> f5962b = new HashMap<>();

    public static void a() {
        f5961a.clear();
        if (b()) {
            f5961a.add("moat");
        }
    }

    static boolean b() {
        return true;
    }

    public static void c() {
        for (c1 next : f5962b.values()) {
            if (next != null) {
                next.a();
            }
        }
    }

    public static void d() {
        for (String str : f5962b.keySet()) {
            c1 c1Var = f5962b.get(str);
            if (c1Var != null) {
                c1Var.b();
            }
        }
    }

    public static JSONArray e() {
        JSONArray jSONArray = new JSONArray();
        List<String> list = f5961a;
        if (list != null) {
            for (String put : list) {
                jSONArray.put(put);
            }
        }
        return jSONArray;
    }

    public static JSONArray f() {
        JSONArray jSONArray = new JSONArray();
        HashMap<String, c1> hashMap = f5962b;
        if (hashMap != null) {
            for (String put : hashMap.keySet()) {
                jSONArray.put(put);
            }
        }
        return jSONArray;
    }

    public static void a(List<String> list) {
        for (String next : list) {
            if (f5961a.contains(next) && !f5962b.containsKey(next)) {
                f5962b.put(next, null);
            }
        }
    }

    public static void a(Application application, boolean z, boolean z2, boolean z3) {
        for (String next : f5962b.keySet()) {
            if (next.contains("moat")) {
                if (f5962b.get(next) != null) {
                    f5962b.get(next).b();
                }
                e1 e1Var = new e1();
                e1Var.a(application, z, z2, z3);
                f5962b.put("moat", e1Var);
            }
        }
    }

    public static void a(WebView webView, HashSet<String> hashSet) {
        Iterator<String> it = hashSet.iterator();
        while (it.hasNext()) {
            c1 c1Var = f5962b.get(it.next());
            if (c1Var != null) {
                c1Var.a(webView);
            }
        }
    }

    public static String a(HashSet<String> hashSet) {
        JSONArray jSONArray = new JSONArray();
        Iterator<String> it = hashSet.iterator();
        while (it.hasNext()) {
            String next = it.next();
            if (f5962b.get(next) != null) {
                jSONArray.put(next);
            }
        }
        return jSONArray.toString();
    }
}
