package com.chartboost.sdk.o;

import android.content.Context;
import android.graphics.Point;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import com.chartboost.sdk.c.g;
import com.chartboost.sdk.c.h;
import com.chartboost.sdk.c.j;
import com.chartboost.sdk.d.a;
import com.chartboost.sdk.d.d;
import com.chartboost.sdk.j;
import com.chartboost.sdk.l;
import com.chartboost.sdk.o.i1;
import com.esotericsoftware.spine.Animation;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.tapjoy.TJAdUnitConstants;
import java.io.File;
import org.json.JSONObject;

public class j1 extends i1 {
    private boolean A = false;
    private boolean B = false;
    JSONObject C;
    protected boolean D;
    protected boolean E;
    protected boolean F;
    protected boolean G;
    protected boolean H = false;
    protected int I;
    protected j J;
    protected j K;
    protected j L;
    protected j M;
    protected j N;
    protected j O;
    protected j P;
    protected j Q;
    protected boolean R = false;
    protected boolean S = false;
    protected boolean T = false;
    final h s;
    protected int t = 0;
    protected int u;
    protected String v;
    protected String w;
    private boolean x = false;
    protected int y = 0;
    protected int z = 0;

    public class b extends i1.a {
        private final d0 n;
        final e o;
        b p;
        private View q;
        final h1 r;
        final k1 s;
        private final d0 t;

        class a extends d0 {
            a(Context context, j1 j1Var) {
                super(context);
            }

            /* access modifiers changed from: protected */
            public void a(MotionEvent motionEvent) {
                b bVar = b.this;
                if (j1.this.f5892g.n == 2) {
                    bVar.s.a(false);
                }
                b bVar2 = b.this;
                if (j1.this.t == 1) {
                    bVar2.c(false);
                }
                b.this.b(true);
            }
        }

        /* renamed from: com.chartboost.sdk.o.j1$b$b  reason: collision with other inner class name */
        class C0130b extends d0 {
            C0130b(Context context, j1 j1Var) {
                super(context);
            }

            /* access modifiers changed from: protected */
            public void a(MotionEvent motionEvent) {
                b.this.d();
            }
        }

        class c implements Runnable {
            c() {
            }

            public void run() {
                Object[] objArr = new Object[1];
                objArr[0] = j1.this.D ? "hidden" : "shown";
                com.chartboost.sdk.c.a.c("InterstitialVideoViewProtocol", String.format("controls %s automatically from timer", objArr));
                b bVar = b.this;
                bVar.o.a(!j1.this.D, true);
                synchronized (j1.this.f5894i) {
                    j1.this.f5894i.remove(b.this.o);
                }
            }
        }

        class d implements Runnable {
            d() {
            }

            public void run() {
                b.this.s.a(false);
            }
        }

        class e implements Runnable {
            e() {
            }

            public void run() {
                try {
                    j1.this.h();
                } catch (Exception e2) {
                    com.chartboost.sdk.e.a.a(b.class, "onCloseButton Runnable.run", e2);
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.chartboost.sdk.o.j1.b.a(int, boolean):void
         arg types: [int, int]
         candidates:
          com.chartboost.sdk.o.j1.b.a(int, int):void
          com.chartboost.sdk.o.i1.a.a(int, int):void
          com.chartboost.sdk.j.b.a(int, int):void
          com.chartboost.sdk.o.j1.b.a(int, boolean):void */
        /* access modifiers changed from: protected */
        public void a(int i2, int i3) {
            super.a(i2, i3);
            a(j1.this.t, false);
            boolean a2 = com.chartboost.sdk.c.b.a(j1.this.a());
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
            RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -1);
            RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-1, -1);
            RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-1, -1);
            RelativeLayout.LayoutParams layoutParams6 = (RelativeLayout.LayoutParams) this.f6044h.getLayoutParams();
            j1 j1Var = j1.this;
            j1Var.a(layoutParams2, a2 ? j1Var.K : j1Var.J, 1.0f);
            Point b2 = j1.this.b(a2 ? "replay-portrait" : "replay-landscape");
            int round = Math.round(((((float) layoutParams6.leftMargin) + (((float) layoutParams6.width) / 2.0f)) + ((float) b2.x)) - (((float) layoutParams2.width) / 2.0f));
            int round2 = Math.round(((((float) layoutParams6.topMargin) + (((float) layoutParams6.height) / 2.0f)) + ((float) b2.y)) - (((float) layoutParams2.height) / 2.0f));
            layoutParams2.leftMargin = Math.min(Math.max(0, round), i2 - layoutParams2.width);
            layoutParams2.topMargin = Math.min(Math.max(0, round2), i3 - layoutParams2.height);
            this.n.bringToFront();
            if (a2) {
                this.n.a(j1.this.K);
            } else {
                this.n.a(j1.this.J);
            }
            RelativeLayout.LayoutParams layoutParams7 = (RelativeLayout.LayoutParams) this.f6047k.getLayoutParams();
            if (!j1.this.s()) {
                layoutParams3.width = layoutParams7.width;
                layoutParams3.height = layoutParams7.height;
                layoutParams3.leftMargin = layoutParams7.leftMargin;
                layoutParams3.topMargin = layoutParams7.topMargin;
                layoutParams4.width = layoutParams7.width;
                layoutParams4.height = layoutParams7.height;
                layoutParams4.leftMargin = layoutParams7.leftMargin;
                layoutParams4.topMargin = layoutParams7.topMargin;
            } else {
                RelativeLayout.LayoutParams layoutParams8 = new RelativeLayout.LayoutParams(-2, -2);
                j jVar = a2 ? j1.this.n : j1.this.o;
                j1.this.a(layoutParams8, jVar, 1.0f);
                layoutParams8.leftMargin = 0;
                layoutParams8.topMargin = 0;
                layoutParams8.addRule(11);
                this.t.setLayoutParams(layoutParams8);
                this.t.a(jVar);
            }
            layoutParams5.width = layoutParams7.width;
            layoutParams5.height = 72;
            layoutParams5.leftMargin = layoutParams7.leftMargin;
            layoutParams5.topMargin = (layoutParams7.topMargin + layoutParams7.height) - 72;
            if (j1.this.S) {
                this.q.setLayoutParams(layoutParams);
            }
            if (j1.this.f5892g.n == 2) {
                this.p.setLayoutParams(layoutParams3);
            }
            this.o.setLayoutParams(layoutParams4);
            this.r.setLayoutParams(layoutParams5);
            this.n.setLayoutParams(layoutParams2);
            if (j1.this.f5892g.n == 2) {
                this.p.a();
            }
            this.o.a();
        }

        /* access modifiers changed from: package-private */
        public void b(boolean z) {
            j1 j1Var = j1.this;
            if (j1Var.t != 1) {
                if (j1Var.E) {
                    a(0, z);
                    return;
                }
                a(1, z);
                JSONObject a2 = g.a(j1.this.C, "timer");
                if (j1.this.u >= 1 || a2 == null || a2.isNull("delay")) {
                    this.o.b(!j1.this.D);
                } else {
                    Object[] objArr = new Object[1];
                    objArr[0] = j1.this.D ? TJAdUnitConstants.String.VISIBLE : "hidden";
                    com.chartboost.sdk.c.a.c("InterstitialVideoViewProtocol", String.format("controls starting %s, setting timer", objArr));
                    this.o.b(j1.this.D);
                    j1.this.a(this.o, new c(), Math.round(a2.optDouble("delay", FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE) * 1000.0d));
                }
                this.o.e();
                j1 j1Var2 = j1.this;
                if (j1Var2.u <= 1) {
                    j1Var2.f5892g.f();
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.chartboost.sdk.o.j1.b.a(int, boolean):void
         arg types: [int, int]
         candidates:
          com.chartboost.sdk.o.j1.b.a(int, int):void
          com.chartboost.sdk.o.i1.a.a(int, int):void
          com.chartboost.sdk.j.b.a(int, int):void
          com.chartboost.sdk.o.j1.b.a(int, boolean):void */
        /* access modifiers changed from: protected */
        public void c() {
            super.c();
            j1 j1Var = j1.this;
            if (j1Var.t != 0 || (j1Var.E && !j1Var.o())) {
                a(j1.this.t, false);
            } else {
                b(false);
            }
        }

        /* access modifiers changed from: protected */
        public void d() {
            j1 j1Var = j1.this;
            if (j1Var.t != 1 || j1Var.f5892g.f5830a.f6123a != 1) {
                if (j1.this.t == 1) {
                    c(false);
                    this.o.h();
                    j1 j1Var2 = j1.this;
                    int i2 = j1Var2.u;
                    if (i2 < 1) {
                        j1Var2.u = i2 + 1;
                        j1Var2.f5892g.e();
                    }
                }
                j1.this.f5886a.post(new e());
            }
        }

        public void e() {
            c(true);
            this.o.h();
            j1 j1Var = j1.this;
            j1Var.u++;
            if (j1Var.u <= 1 && !j1Var.u()) {
                j1 j1Var2 = j1.this;
                if (j1Var2.y >= 1) {
                    j1Var2.f5892g.e();
                }
            }
        }

        /* access modifiers changed from: protected */
        public boolean f() {
            j1 j1Var = j1.this;
            if (j1Var.t == 1 && j1Var.u < 1) {
                StringBuilder sb = new StringBuilder();
                sb.append("close-");
                sb.append(com.chartboost.sdk.c.b.a(j1.this.a()) ? TJAdUnitConstants.String.PORTRAIT : TJAdUnitConstants.String.LANDSCAPE);
                JSONObject a2 = g.a(j1.this.g(), sb.toString());
                float optDouble = a2 != null ? (float) a2.optDouble("delay", -1.0d) : -1.0f;
                int round = optDouble >= Animation.CurveTimeline.LINEAR ? Math.round(optDouble * 1000.0f) : -1;
                j1.this.I = round;
                if (round < 0 || round > this.o.b().d()) {
                    return false;
                }
            }
            return true;
        }

        /* access modifiers changed from: protected */
        public void g() {
            j1.this.E = false;
            b(true);
        }

        private b(Context context) {
            super(context);
            JSONObject optJSONObject;
            JSONObject optJSONObject2;
            l a2 = l.a();
            if (j1.this.S) {
                this.q = new View(context);
                this.q.setBackgroundColor(-16777216);
                this.q.setVisibility(8);
                addView(this.q);
            }
            if (j1.this.f5892g.n == 2) {
                b bVar = new b(context, j1.this);
                a2.a(bVar);
                this.p = bVar;
                this.p.setVisibility(8);
                addView(this.p);
            }
            e eVar = new e(context, j1.this);
            a2.a(eVar);
            this.o = eVar;
            a(this.o.f5986g);
            this.o.setVisibility(8);
            addView(this.o);
            h1 h1Var = new h1(context, j1.this);
            a2.a(h1Var);
            this.r = h1Var;
            this.r.setVisibility(8);
            addView(this.r);
            if (j1.this.f5892g.n == 2) {
                k1 k1Var = new k1(context, j1.this);
                a2.a(k1Var);
                this.s = k1Var;
                this.s.setVisibility(8);
                addView(this.s);
            } else {
                this.s = null;
            }
            this.n = new a(getContext(), j1.this);
            this.n.setVisibility(8);
            addView(this.n);
            this.t = new C0130b(getContext(), j1.this);
            this.t.setVisibility(8);
            this.t.setContentDescription("CBClose");
            addView(this.t);
            JSONObject optJSONObject3 = j1.this.C.optJSONObject("progress");
            JSONObject optJSONObject4 = j1.this.C.optJSONObject("video-controls-background");
            if (optJSONObject3 != null && !optJSONObject3.isNull("background-color") && !optJSONObject3.isNull("border-color") && !optJSONObject3.isNull("progress-color") && !optJSONObject3.isNull("radius")) {
                j1.this.R = true;
                l1 c2 = this.o.c();
                c2.a(com.chartboost.sdk.j.a(optJSONObject3.optString("background-color")));
                c2.b(com.chartboost.sdk.j.a(optJSONObject3.optString("border-color")));
                c2.c(com.chartboost.sdk.j.a(optJSONObject3.optString("progress-color")));
                c2.b((float) optJSONObject3.optDouble("radius", FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE));
            }
            if (optJSONObject4 != null && !optJSONObject4.isNull("color")) {
                this.o.a(com.chartboost.sdk.j.a(optJSONObject4.optString("color")));
            }
            if (j1.this.f5892g.n == 2 && j1.this.F && (optJSONObject2 = j1.this.C.optJSONObject("post-video-toaster")) != null) {
                this.r.a(optJSONObject2.optString("title"), optJSONObject2.optString("tagline"));
            }
            if (j1.this.f5892g.n == 2 && j1.this.E && (optJSONObject = j1.this.C.optJSONObject("confirmation")) != null) {
                this.p.a(optJSONObject.optString("text"), com.chartboost.sdk.j.a(optJSONObject.optString("color")));
            }
            String str = "";
            if (j1.this.f5892g.n == 2 && j1.this.G) {
                JSONObject a3 = g.a(j1.this.C, "post-video-reward-toaster");
                this.s.a((a3 == null || !a3.optString("position").equals("inside-top")) ? 1 : 0);
                this.s.a(a3 != null ? a3.optString("text") : str);
                if (j1.this.O.c()) {
                    this.s.a(j1.this.Q);
                }
            }
            JSONObject g2 = j1.this.g();
            if (g2 == null || g2.isNull("video-click-button")) {
                this.o.d();
            }
            this.o.d(j1.this.C.optBoolean("video-progress-timer-enabled"));
            if (j1.this.T || j1.this.S) {
                this.l.setVisibility(4);
            }
            String[] strArr = new String[1];
            strArr[0] = com.chartboost.sdk.c.b.a(j1.this.a()) ? "video-portrait" : "video-landscape";
            JSONObject a4 = g.a(g2, strArr);
            j1.this.w = a4 != null ? a4.optString("id") : str;
            if (j1.this.w.isEmpty()) {
                j1.this.a(a.c.VIDEO_ID_MISSING);
                return;
            }
            if (j1.this.v == null) {
                j1.this.v = j1.this.s.a(j1.this.w);
            }
            String str2 = j1.this.v;
            if (str2 == null) {
                j1.this.a(a.c.VIDEO_UNAVAILABLE);
            } else {
                this.o.a(str2);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.chartboost.sdk.o.j1.b.a(int, boolean):void
         arg types: [int, int]
         candidates:
          com.chartboost.sdk.o.j1.b.a(int, int):void
          com.chartboost.sdk.o.i1.a.a(int, int):void
          com.chartboost.sdk.j.b.a(int, int):void
          com.chartboost.sdk.o.j1.b.a(int, boolean):void */
        /* access modifiers changed from: package-private */
        public void c(boolean z) {
            JSONObject jSONObject;
            this.o.f();
            j1 j1Var = j1.this;
            if (j1Var.t == 1 && z) {
                if (j1Var.u < 1 && (jSONObject = j1Var.C) != null && !jSONObject.isNull("post-video-reward-toaster")) {
                    j1 j1Var2 = j1.this;
                    if (j1Var2.G && j1Var2.O.c() && j1.this.P.c()) {
                        e(true);
                    }
                }
                a(2, true);
                if (com.chartboost.sdk.c.b.a(com.chartboost.sdk.c.b.a())) {
                    requestLayout();
                }
            }
        }

        private void e(boolean z) {
            if (z) {
                this.s.a(true);
            } else {
                this.s.setVisibility(0);
            }
            j1.this.f5886a.postDelayed(new d(), 2500);
        }

        public d0 d(boolean z) {
            return ((!j1.this.s() || !z) && (j1.this.s() || z)) ? this.f6045i : this.t;
        }

        public void b() {
            j1.this.n();
            super.b();
        }

        /* access modifiers changed from: protected */
        public void b(float f2, float f3, float f4, float f5) {
            if (j1.this.t == 1) {
                c(false);
            }
            j1.this.b(g.a(g.a("x", Float.valueOf(f2)), g.a("y", Float.valueOf(f3)), g.a("w", Float.valueOf(f4)), g.a("h", Float.valueOf(f5))));
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.chartboost.sdk.j.a(boolean, android.view.View, boolean):void
         arg types: [int, com.chartboost.sdk.o.d0, boolean]
         candidates:
          com.chartboost.sdk.o.i1.a(android.view.ViewGroup$LayoutParams, com.chartboost.sdk.c.j, float):void
          com.chartboost.sdk.j.a(android.view.View, java.lang.Runnable, long):void
          com.chartboost.sdk.j.a(boolean, android.view.View, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.chartboost.sdk.j.a(boolean, android.view.View, boolean):void
         arg types: [int, com.chartboost.sdk.o.b, boolean]
         candidates:
          com.chartboost.sdk.o.i1.a(android.view.ViewGroup$LayoutParams, com.chartboost.sdk.c.j, float):void
          com.chartboost.sdk.j.a(android.view.View, java.lang.Runnable, long):void
          com.chartboost.sdk.j.a(boolean, android.view.View, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.chartboost.sdk.j.a(boolean, android.view.View, boolean):void
         arg types: [int, android.view.View, boolean]
         candidates:
          com.chartboost.sdk.o.i1.a(android.view.ViewGroup$LayoutParams, com.chartboost.sdk.c.j, float):void
          com.chartboost.sdk.j.a(android.view.View, java.lang.Runnable, long):void
          com.chartboost.sdk.j.a(boolean, android.view.View, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.chartboost.sdk.j.a(boolean, android.view.View, boolean):void
         arg types: [int, com.chartboost.sdk.o.e, boolean]
         candidates:
          com.chartboost.sdk.o.i1.a(android.view.ViewGroup$LayoutParams, com.chartboost.sdk.c.j, float):void
          com.chartboost.sdk.j.a(android.view.View, java.lang.Runnable, long):void
          com.chartboost.sdk.j.a(boolean, android.view.View, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.chartboost.sdk.j.a(boolean, android.view.View, boolean):void
         arg types: [int, com.chartboost.sdk.o.h1, boolean]
         candidates:
          com.chartboost.sdk.o.i1.a(android.view.ViewGroup$LayoutParams, com.chartboost.sdk.c.j, float):void
          com.chartboost.sdk.j.a(android.view.View, java.lang.Runnable, long):void
          com.chartboost.sdk.j.a(boolean, android.view.View, boolean):void */
        private void a(int i2, boolean z) {
            j1 j1Var = j1.this;
            j1Var.t = i2;
            boolean z2 = true;
            if (i2 == 0) {
                j1Var.a(!j1Var.s(), this.f6047k, z);
                j1 j1Var2 = j1.this;
                if (j1Var2.f5892g.n == 2) {
                    j1Var2.a(true, (View) this.p, z);
                }
                j1 j1Var3 = j1.this;
                if (j1Var3.S) {
                    j1Var3.a(false, this.q, z);
                }
                j1.this.a(false, (View) this.o, z);
                j1.this.a(false, (View) this.n, z);
                j1.this.a(false, (View) this.r, z);
                this.f6047k.setEnabled(false);
                this.n.setEnabled(false);
                this.o.setEnabled(false);
            } else if (i2 == 1) {
                j1Var.a(false, (View) this.f6047k, z);
                j1 j1Var4 = j1.this;
                if (j1Var4.f5892g.n == 2) {
                    j1Var4.a(false, (View) this.p, z);
                }
                j1 j1Var5 = j1.this;
                if (j1Var5.S) {
                    j1Var5.a(true, this.q, z);
                }
                j1.this.a(true, (View) this.o, z);
                j1.this.a(false, (View) this.n, z);
                j1.this.a(false, (View) this.r, z);
                this.f6047k.setEnabled(true);
                this.n.setEnabled(false);
                this.o.setEnabled(true);
            } else if (i2 == 2) {
                j1Var.a(true, (View) this.f6047k, z);
                j1 j1Var6 = j1.this;
                if (j1Var6.f5892g.n == 2) {
                    j1Var6.a(false, (View) this.p, z);
                }
                j1 j1Var7 = j1.this;
                if (j1Var7.S) {
                    j1Var7.a(false, this.q, z);
                }
                j1.this.a(false, (View) this.o, z);
                j1.this.a(true, (View) this.n, z);
                j1.this.a(j1.this.P.c() && j1.this.O.c() && j1.this.F, this.r, z);
                this.n.setEnabled(true);
                this.f6047k.setEnabled(true);
                this.o.setEnabled(false);
                if (j1.this.H) {
                    e(false);
                }
            }
            boolean f2 = f();
            d0 d2 = d(true);
            d2.setEnabled(f2);
            j1.this.a(f2, d2, z);
            d0 d3 = d(false);
            d3.setEnabled(false);
            j1.this.a(false, (View) d3, z);
            j1 j1Var8 = j1.this;
            if (j1Var8.T || j1Var8.S) {
                j1 j1Var9 = j1.this;
                j1Var9.a(!j1Var9.s(), this.l, z);
            }
            j1 j1Var10 = j1.this;
            j1Var10.a(!j1Var10.s(), this.f6044h, z);
            if (i2 == 0) {
                z2 = false;
            }
            a(z2);
        }

        /* access modifiers changed from: protected */
        public void a(float f2, float f3, float f4, float f5) {
            j1 j1Var = j1.this;
            if ((!j1Var.D || j1Var.t != 1) && j1.this.t != 0) {
                b(f2, f3, f4, f5);
            }
        }
    }

    public j1(d dVar, h hVar, Handler handler, com.chartboost.sdk.h hVar2) {
        super(dVar, handler, hVar2);
        this.s = hVar;
        this.t = 0;
        this.J = new j(this);
        this.K = new j(this);
        this.L = new j(this);
        this.M = new j(this);
        this.N = new j(this);
        this.O = new j(this);
        this.P = new j(this);
        this.Q = new j(this);
        this.u = 0;
    }

    /* access modifiers changed from: protected */
    public j.b a(Context context) {
        return new b(context);
    }

    public void d() {
        super.d();
        this.J = null;
        this.K = null;
        this.N = null;
        this.O = null;
        this.P = null;
        this.L = null;
        this.M = null;
        this.Q = null;
    }

    /* access modifiers changed from: protected */
    public void i() {
        if (this.E && (!this.L.c() || !this.M.c())) {
            this.E = false;
        }
        super.i();
    }

    public float j() {
        return (float) this.z;
    }

    public float k() {
        return (float) this.y;
    }

    public boolean l() {
        e().d();
        return true;
    }

    public void m() {
        super.m();
        if (this.t == 1 && this.x) {
            e().o.b().a(this.y);
            e().o.e();
        }
        this.x = false;
    }

    public void n() {
        super.n();
        if (this.t == 1 && !this.x) {
            this.x = true;
            e().o.g();
        }
    }

    public boolean o() {
        return this.f5892g.n == 1;
    }

    public boolean p() {
        return this.t == 1;
    }

    /* renamed from: q */
    public b e() {
        return (b) super.e();
    }

    /* access modifiers changed from: protected */
    public void r() {
        this.f5892g.p();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:5:0x000b A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean s() {
        /*
            r4 = this;
            int r0 = r4.t
            r1 = 0
            r2 = 1
            if (r0 == 0) goto L_0x001c
            if (r0 == r2) goto L_0x000d
            r3 = 2
            if (r0 == r3) goto L_0x002b
        L_0x000b:
            r1 = 1
            goto L_0x002b
        L_0x000d:
            boolean r0 = r4.S
            if (r0 != 0) goto L_0x000b
            int r0 = com.chartboost.sdk.c.b.a()
            boolean r0 = com.chartboost.sdk.c.b.a(r0)
            if (r0 == 0) goto L_0x002b
            goto L_0x000b
        L_0x001c:
            boolean r0 = r4.T
            if (r0 != 0) goto L_0x000b
            int r0 = com.chartboost.sdk.c.b.a()
            boolean r0 = com.chartboost.sdk.c.b.a(r0)
            if (r0 == 0) goto L_0x002b
            goto L_0x000b
        L_0x002b:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.o.j1.s():boolean");
    }

    public boolean t() {
        return this.A;
    }

    public boolean u() {
        return this.B;
    }

    public void v() {
        String str = this.v;
        if (str != null) {
            new File(str).delete();
        }
        this.B = true;
        a(a.c.ERROR_PLAYING_VIDEO);
    }

    public boolean a(JSONObject jSONObject) {
        if (!super.a(jSONObject)) {
            return false;
        }
        this.C = jSONObject.optJSONObject("ux");
        if (this.C == null) {
            this.C = g.a(new g.a[0]);
        }
        if (this.f5890e.isNull("video-landscape") || this.f5890e.isNull("replay-landscape")) {
            this.f5896k = false;
        }
        if (!this.J.a("replay-landscape") || !this.K.a("replay-portrait") || !this.N.a("video-click-button") || !this.O.a("post-video-reward-icon") || !this.P.a("post-video-button") || !this.L.a("video-confirmation-button") || !this.M.a("video-confirmation-icon") || !this.Q.a("post-video-reward-icon")) {
            com.chartboost.sdk.c.a.b("InterstitialVideoViewProtocol", "Error while downloading the assets");
            a(a.c.ASSETS_DOWNLOAD_FAILURE);
            return false;
        }
        this.D = this.C.optBoolean("video-controls-togglable");
        this.S = jSONObject.optBoolean("fullscreen");
        this.T = jSONObject.optBoolean("preroll_popup_fullscreen");
        if (this.f5892g.n == 2) {
            JSONObject optJSONObject = this.C.optJSONObject("confirmation");
            JSONObject optJSONObject2 = this.C.optJSONObject("post-video-toaster");
            if (optJSONObject2 != null && !optJSONObject2.isNull("title") && !optJSONObject2.isNull("tagline")) {
                this.F = true;
            }
            if (optJSONObject != null && !optJSONObject.isNull("text") && !optJSONObject.isNull("color")) {
                this.E = true;
            }
            if (!this.C.isNull("post-video-reward-toaster")) {
                this.G = true;
            }
        }
        return true;
    }

    public void a(boolean z2) {
        this.A = z2;
    }
}
