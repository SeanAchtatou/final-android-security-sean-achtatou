package com.chartboost.sdk.o;

import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.webkit.JsPromptResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.FrameLayout;
import com.appsflyer.share.Constants;
import com.chartboost.sdk.c.g;
import com.chartboost.sdk.d.b;
import com.chartboost.sdk.d.c;
import com.chartboost.sdk.d.d;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TapjoyConstants;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class h0 extends WebChromeClient {

    /* renamed from: a  reason: collision with root package name */
    private View f6026a;

    /* renamed from: b  reason: collision with root package name */
    private ViewGroup f6027b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f6028c = false;

    /* renamed from: d  reason: collision with root package name */
    private FrameLayout f6029d;

    /* renamed from: e  reason: collision with root package name */
    private WebChromeClient.CustomViewCallback f6030e;

    /* renamed from: f  reason: collision with root package name */
    private a f6031f;

    /* renamed from: g  reason: collision with root package name */
    private final j0 f6032g;

    /* renamed from: h  reason: collision with root package name */
    private final Handler f6033h;

    public interface a {
        void a(boolean z);
    }

    public h0(View view, ViewGroup viewGroup, View view2, i0 i0Var, j0 j0Var, Handler handler) {
        this.f6026a = view;
        this.f6027b = viewGroup;
        this.f6032g = j0Var;
        this.f6033h = handler;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public String a(JSONObject jSONObject, String str) {
        char c2;
        b bVar;
        String str2 = str;
        int i2 = 4;
        switch (str.hashCode()) {
            case -2012425132:
                if (str2.equals("getDefaultPosition")) {
                    c2 = 18;
                    break;
                }
                c2 = 65535;
                break;
            case -1757019252:
                if (str2.equals("getCurrentPosition")) {
                    c2 = 17;
                    break;
                }
                c2 = 65535;
                break;
            case -1554056650:
                if (str2.equals("currentVideoDuration")) {
                    c2 = 7;
                    break;
                }
                c2 = 65535;
                break;
            case -1263203643:
                if (str2.equals("openUrl")) {
                    c2 = 14;
                    break;
                }
                c2 = 65535;
                break;
            case -1086137328:
                if (str2.equals("videoCompleted")) {
                    c2 = 3;
                    break;
                }
                c2 = 65535;
                break;
            case -715147645:
                if (str2.equals("getScreenSize")) {
                    c2 = 16;
                    break;
                }
                c2 = 65535;
                break;
            case -640720077:
                if (str2.equals("videoPlaying")) {
                    c2 = 4;
                    break;
                }
                c2 = 65535;
                break;
            case 3529469:
                if (str2.equals("show")) {
                    c2 = 9;
                    break;
                }
                c2 = 65535;
                break;
            case 94750088:
                if (str2.equals(TJAdUnitConstants.String.CLICK)) {
                    c2 = 1;
                    break;
                }
                c2 = 65535;
                break;
            case 94756344:
                if (str2.equals(TJAdUnitConstants.String.CLOSE)) {
                    c2 = 2;
                    break;
                }
                c2 = 65535;
                break;
            case 95458899:
                if (str2.equals(TapjoyConstants.TJC_DEBUG)) {
                    c2 = 12;
                    break;
                }
                c2 = 65535;
                break;
            case 96784904:
                if (str2.equals("error")) {
                    c2 = 10;
                    break;
                }
                c2 = 65535;
                break;
            case 133423073:
                if (str2.equals("setOrientationProperties")) {
                    c2 = 20;
                    break;
                }
                c2 = 65535;
                break;
            case 160987616:
                if (str2.equals("getParameters")) {
                    c2 = 0;
                    break;
                }
                c2 = 65535;
                break;
            case 937504109:
                if (str2.equals("getOrientationProperties")) {
                    c2 = 19;
                    break;
                }
                c2 = 65535;
                break;
            case 939594121:
                if (str2.equals("videoPaused")) {
                    c2 = 5;
                    break;
                }
                c2 = 65535;
                break;
            case 1000390722:
                if (str2.equals("videoReplay")) {
                    c2 = 6;
                    break;
                }
                c2 = 65535;
                break;
            case 1082777163:
                if (str2.equals("totalVideoDuration")) {
                    c2 = 8;
                    break;
                }
                c2 = 65535;
                break;
            case 1124446108:
                if (str2.equals("warning")) {
                    c2 = 11;
                    break;
                }
                c2 = 65535;
                break;
            case 1270488759:
                if (str2.equals("tracking")) {
                    c2 = 13;
                    break;
                }
                c2 = 65535;
                break;
            case 1880941391:
                if (str2.equals("getMaxSize")) {
                    c2 = 15;
                    break;
                }
                c2 = 65535;
                break;
            default:
                c2 = 65535;
                break;
        }
        switch (c2) {
            case 0:
                Log.d("CBWebChromeClient", "JavaScript to native " + str2 + " callback triggered.");
                d dVar = this.f6032g.f5892g;
                if (dVar == null || (bVar = dVar.r) == null) {
                    return "{}";
                }
                JSONObject a2 = g.a(new g.a[0]);
                for (Map.Entry next : bVar.f5819d.entrySet()) {
                    g.a(a2, (String) next.getKey(), next.getValue());
                }
                for (Map.Entry next2 : bVar.f5818c.entrySet()) {
                    c cVar = (c) next2.getValue();
                    g.a(a2, (String) next2.getKey(), cVar.f5827a + Constants.URL_PATH_DELIMITER + cVar.f5828b);
                }
                return a2.toString();
            case 1:
                i2 = 0;
                break;
            case 2:
                i2 = 1;
                break;
            case 3:
                i2 = 9;
                break;
            case 4:
                i2 = 11;
                break;
            case 5:
                i2 = 10;
                break;
            case 6:
                i2 = 12;
                break;
            case 7:
                i2 = 2;
                break;
            case 8:
                i2 = 7;
                break;
            case 9:
                i2 = 6;
                break;
            case 10:
                Log.d(i0.class.getName(), "Javascript Error occured");
                break;
            case 11:
                Log.d(i0.class.getName(), "Javascript warning occurred");
                i2 = 13;
                break;
            case 12:
                i2 = 3;
                break;
            case 13:
                i2 = 8;
                break;
            case 14:
                i2 = 5;
                break;
            case 15:
                Log.d("CBWebChromeClient", "JavaScript to native " + str2 + " callback triggered.");
                return this.f6032g.s();
            case 16:
                Log.d("CBWebChromeClient", "JavaScript to native " + str2 + " callback triggered.");
                return this.f6032g.t();
            case 17:
                Log.d("CBWebChromeClient", "JavaScript to native " + str2 + " callback triggered.");
                return this.f6032g.v();
            case 18:
                Log.d("CBWebChromeClient", "JavaScript to native " + str2 + " callback triggered.");
                return this.f6032g.u();
            case 19:
                Log.d("CBWebChromeClient", "JavaScript to native " + str2 + " callback triggered.");
                return this.f6032g.p();
            case 20:
                Log.d("CBWebChromeClient", "JavaScript to native " + str2 + " callback triggered.");
                i2 = 14;
                break;
            default:
                Log.e("CBWebChromeClient", "JavaScript to native " + str2 + " callback not recognized.");
                return "Function name not recognized.";
        }
        Log.d("CBWebChromeClient", "JavaScript to native " + str2 + " callback triggered.");
        this.f6033h.post(new k0(this, this.f6032g, i2, str, jSONObject));
        return "Native function successfully called.";
    }

    public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        String simpleName = h0.class.getSimpleName();
        Log.d(simpleName, "Chartboost Webview:" + consoleMessage.message() + " -- From line " + consoleMessage.lineNumber() + " of " + consoleMessage.sourceId());
        return true;
    }

    public void onHideCustomView() {
        if (this.f6028c) {
            this.f6027b.setVisibility(4);
            this.f6027b.removeView(this.f6029d);
            this.f6026a.setVisibility(0);
            WebChromeClient.CustomViewCallback customViewCallback = this.f6030e;
            if (customViewCallback != null && !customViewCallback.getClass().getName().contains(".chromium.")) {
                this.f6030e.onCustomViewHidden();
            }
            this.f6028c = false;
            this.f6029d = null;
            this.f6030e = null;
            a aVar = this.f6031f;
            if (aVar != null) {
                aVar.a(false);
            }
        }
    }

    public boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
        try {
            JSONObject jSONObject = new JSONObject(str2);
            jsPromptResult.confirm(a(jSONObject.getJSONObject("eventArgs"), jSONObject.getString("eventType")));
            return true;
        } catch (JSONException unused) {
            com.chartboost.sdk.c.a.b("CBWebChromeClient", "Exception caught parsing the function name from js to native");
            return true;
        }
    }

    public void onShowCustomView(View view, WebChromeClient.CustomViewCallback customViewCallback) {
        if (view instanceof FrameLayout) {
            this.f6028c = true;
            this.f6029d = (FrameLayout) view;
            this.f6030e = customViewCallback;
            this.f6026a.setVisibility(4);
            this.f6027b.addView(this.f6029d, new ViewGroup.LayoutParams(-1, -1));
            this.f6027b.setVisibility(0);
            a aVar = this.f6031f;
            if (aVar != null) {
                aVar.a(true);
            }
        }
    }

    public void onShowCustomView(View view, int i2, WebChromeClient.CustomViewCallback customViewCallback) {
        onShowCustomView(view, customViewCallback);
    }
}
