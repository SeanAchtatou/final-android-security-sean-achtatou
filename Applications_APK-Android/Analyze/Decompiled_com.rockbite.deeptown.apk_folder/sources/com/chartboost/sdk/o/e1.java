package com.chartboost.sdk.o;

import android.app.Application;
import android.webkit.WebView;
import com.chartboost.sdk.c.a;
import com.moat.analytics.mobile.cha.MoatAnalytics;
import com.moat.analytics.mobile.cha.MoatFactory;
import com.moat.analytics.mobile.cha.MoatOptions;
import com.moat.analytics.mobile.cha.WebAdTracker;

public class e1 implements d1 {

    /* renamed from: b  reason: collision with root package name */
    private static String f6003b = "e1";

    /* renamed from: a  reason: collision with root package name */
    WebAdTracker f6004a = null;

    public void a(Application application, boolean z, boolean z2, boolean z3) {
        String str = f6003b;
        StringBuilder sb = new StringBuilder();
        sb.append("start MOAT provider, Debugging Enabled: ");
        sb.append(z);
        sb.append("Location Enabled:");
        sb.append(!z2);
        sb.append("idfaCollectionEnabled:");
        sb.append(!z3);
        a.a(str, sb.toString());
        MoatOptions moatOptions = new MoatOptions();
        moatOptions.disableLocationServices = z2;
        moatOptions.disableAdIdCollection = z3;
        moatOptions.loggingEnabled = z;
        MoatAnalytics.getInstance().start(moatOptions, application);
    }

    public void b() {
        if (this.f6004a != null) {
            a.a(f6003b, "stop MOAT tracker");
            this.f6004a.stopTracking();
            this.f6004a = null;
        }
    }

    public void a(WebView webView) {
        this.f6004a = MoatFactory.create().createWebAdTracker(webView);
    }

    public void a() {
        if (this.f6004a != null) {
            a.a(f6003b, "start MOAT tracker");
            this.f6004a.startTracking();
        }
    }
}
