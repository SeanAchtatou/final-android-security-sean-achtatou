package com.chartboost.sdk.o;

import android.os.Handler;
import com.chartboost.sdk.c.k;
import com.chartboost.sdk.d.a;
import com.chartboost.sdk.e.a;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.concurrent.Executor;

public class q<T> implements Comparable<q>, Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final Executor f6135a;

    /* renamed from: b  reason: collision with root package name */
    private final r f6136b;

    /* renamed from: c  reason: collision with root package name */
    private final l f6137c;

    /* renamed from: d  reason: collision with root package name */
    private final k f6138d;

    /* renamed from: e  reason: collision with root package name */
    private final Handler f6139e;

    /* renamed from: f  reason: collision with root package name */
    public final g<T> f6140f;

    /* renamed from: g  reason: collision with root package name */
    private i<T> f6141g;

    /* renamed from: h  reason: collision with root package name */
    private j f6142h;

    q(Executor executor, r rVar, l lVar, k kVar, Handler handler, g<T> gVar) {
        this.f6135a = executor;
        this.f6136b = rVar;
        this.f6137c = lVar;
        this.f6138d = kVar;
        this.f6139e = handler;
        this.f6140f = gVar;
    }

    private j a(g<T> gVar) throws IOException {
        int i2 = 10000;
        int i3 = 0;
        while (true) {
            try {
                return a(gVar, i2);
            } catch (SocketTimeoutException e2) {
                if (i3 < 1) {
                    i2 *= 2;
                    i3++;
                } else {
                    throw e2;
                }
            }
        }
    }

    private static boolean a(int i2) {
        return ((100 <= i2 && i2 < 200) || i2 == 204 || i2 == 304) ? false : true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.o.g.a(java.lang.Object, com.chartboost.sdk.o.j):void
     arg types: [T, com.chartboost.sdk.o.j]
     candidates:
      com.chartboost.sdk.o.g.a(com.chartboost.sdk.d.a, com.chartboost.sdk.o.j):void
      com.chartboost.sdk.o.g.a(java.lang.Object, com.chartboost.sdk.o.j):void */
    public void run() {
        i<T> iVar = this.f6141g;
        if (iVar != null) {
            try {
                if (iVar.f6042b == null) {
                    this.f6140f.a((Object) iVar.f6041a, this.f6142h);
                } else {
                    this.f6140f.a(iVar.f6042b, this.f6142h);
                }
            } catch (Exception e2) {
                a.a(q.class, "deliver result", e2);
            }
        } else if (this.f6140f.f6013d.compareAndSet(0, 1)) {
            long b2 = this.f6138d.b();
            try {
                if (this.f6137c.c()) {
                    this.f6142h = a(this.f6140f);
                    int i2 = this.f6142h.f6050a;
                    if (i2 < 200 || i2 >= 300) {
                        a.d dVar = a.d.NETWORK_FAILURE;
                        this.f6141g = i.a(new com.chartboost.sdk.d.a(dVar, "Failure due to HTTP status code " + i2));
                    } else {
                        this.f6141g = this.f6140f.a(this.f6142h);
                    }
                } else {
                    this.f6141g = i.a(new com.chartboost.sdk.d.a(a.d.INTERNET_UNAVAILABLE, "Internet Unavailable"));
                }
                this.f6140f.f6015f = this.f6138d.b() - b2;
                int i3 = this.f6140f.f6018i;
                if (i3 != 0) {
                    if (i3 != 1) {
                        return;
                    }
                    this.f6135a.execute(this);
                    return;
                }
            } catch (Throwable th) {
                this.f6140f.f6015f = this.f6138d.b() - b2;
                int i4 = this.f6140f.f6018i;
                if (i4 == 0) {
                    this.f6139e.post(this);
                } else if (i4 == 1) {
                    this.f6135a.execute(this);
                }
                throw th;
            }
            this.f6139e.post(this);
        }
    }

    /* JADX WARN: Type inference failed for: r4v1, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r4v3, types: [java.io.FileOutputStream] */
    /* JADX WARN: Type inference failed for: r4v4 */
    /* JADX WARN: Type inference failed for: r4v5, types: [java.io.DataOutputStream] */
    /* JADX WARN: Type inference failed for: r4v6 */
    /* JADX WARNING: Can't wrap try/catch for region: R(2:80|81) */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:79|(2:89|90)|91|92) */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:20|21|(2:24|25)|26|27) */
    /* JADX WARNING: Can't wrap try/catch for region: R(6:62|63|(2:68|69)|(2:72|73)|74|75) */
    /* JADX WARNING: Can't wrap try/catch for region: R(7:36|37|(2:39|(13:41|42|43|44|45|46|47|(2:49|50)|51|52|53|54|(2:56|(2:58|59)(2:60|61)))(5:76|77|78|(1:83)(1:84)|(2:86|87)))(1:93)|94|95|96|97) */
    /* JADX WARNING: Can't wrap try/catch for region: R(8:46|47|(2:49|50)|51|52|53|54|(2:56|(2:58|59)(2:60|61))) */
    /* JADX WARNING: Code restructure failed: missing block: B:81:?, code lost:
        r0 = r2.getErrorStream();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x007d */
    /* JADX WARNING: Missing exception handler attribute for start block: B:51:0x00d4 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:53:0x00d7 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:74:0x014a */
    /* JADX WARNING: Missing exception handler attribute for start block: B:80:0x0153 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:91:0x0171 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:94:0x0174 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x007a A[SYNTHETIC, Splitter:B:24:0x007a] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00df A[Catch:{ all -> 0x0151, all -> 0x0186 }] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0140 A[SYNTHETIC, Splitter:B:68:0x0140] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0147 A[SYNTHETIC, Splitter:B:72:0x0147] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:91:0x0171=Splitter:B:91:0x0171, B:53:0x00d7=Splitter:B:53:0x00d7, B:74:0x014a=Splitter:B:74:0x014a} */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:94:0x0174=Splitter:B:94:0x0174, B:26:0x007d=Splitter:B:26:0x007d} */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.chartboost.sdk.o.j a(com.chartboost.sdk.o.g<T> r10, int r11) throws java.io.IOException {
        /*
            r9 = this;
            com.chartboost.sdk.o.h r0 = r10.a()
            java.util.Map<java.lang.String, java.lang.String> r1 = r0.f6023a
            com.chartboost.sdk.o.r r2 = r9.f6136b
            java.net.HttpURLConnection r2 = r2.a(r10)
            r2.setConnectTimeout(r11)
            r2.setReadTimeout(r11)
            r11 = 0
            r2.setUseCaches(r11)
            r3 = 1
            r2.setDoInput(r3)
            if (r1 == 0) goto L_0x003a
            java.util.Set r4 = r1.keySet()     // Catch:{ all -> 0x01a4 }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ all -> 0x01a4 }
        L_0x0024:
            boolean r5 = r4.hasNext()     // Catch:{ all -> 0x01a4 }
            if (r5 == 0) goto L_0x003a
            java.lang.Object r5 = r4.next()     // Catch:{ all -> 0x01a4 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ all -> 0x01a4 }
            java.lang.Object r6 = r1.get(r5)     // Catch:{ all -> 0x01a4 }
            java.lang.String r6 = (java.lang.String) r6     // Catch:{ all -> 0x01a4 }
            r2.addRequestProperty(r5, r6)     // Catch:{ all -> 0x01a4 }
            goto L_0x0024
        L_0x003a:
            java.lang.String r1 = r10.f6010a     // Catch:{ all -> 0x01a4 }
            r2.setRequestMethod(r1)     // Catch:{ all -> 0x01a4 }
            java.lang.String r1 = r10.f6010a     // Catch:{ all -> 0x01a4 }
            java.lang.String r4 = "POST"
            boolean r1 = r1.equals(r4)     // Catch:{ all -> 0x01a4 }
            r4 = 0
            if (r1 == 0) goto L_0x007e
            byte[] r1 = r0.f6024b     // Catch:{ all -> 0x01a4 }
            if (r1 == 0) goto L_0x007e
            r2.setDoOutput(r3)     // Catch:{ all -> 0x01a4 }
            byte[] r1 = r0.f6024b     // Catch:{ all -> 0x01a4 }
            int r1 = r1.length     // Catch:{ all -> 0x01a4 }
            r2.setFixedLengthStreamingMode(r1)     // Catch:{ all -> 0x01a4 }
            java.lang.String r1 = r0.f6025c     // Catch:{ all -> 0x01a4 }
            if (r1 == 0) goto L_0x0062
            java.lang.String r1 = "Content-Type"
            java.lang.String r3 = r0.f6025c     // Catch:{ all -> 0x01a4 }
            r2.addRequestProperty(r1, r3)     // Catch:{ all -> 0x01a4 }
        L_0x0062:
            java.io.DataOutputStream r1 = new java.io.DataOutputStream     // Catch:{ all -> 0x0077 }
            java.io.OutputStream r3 = r2.getOutputStream()     // Catch:{ all -> 0x0077 }
            r1.<init>(r3)     // Catch:{ all -> 0x0077 }
            byte[] r0 = r0.f6024b     // Catch:{ all -> 0x0074 }
            r1.write(r0)     // Catch:{ all -> 0x0074 }
            r1.close()     // Catch:{ IOException -> 0x007e }
            goto L_0x007e
        L_0x0074:
            r10 = move-exception
            r4 = r1
            goto L_0x0078
        L_0x0077:
            r10 = move-exception
        L_0x0078:
            if (r4 == 0) goto L_0x007d
            r4.close()     // Catch:{ IOException -> 0x007d }
        L_0x007d:
            throw r10     // Catch:{ all -> 0x01a4 }
        L_0x007e:
            com.chartboost.sdk.c.k r0 = r9.f6138d     // Catch:{ all -> 0x01a4 }
            long r0 = r0.b()     // Catch:{ all -> 0x01a4 }
            int r3 = r2.getResponseCode()     // Catch:{ all -> 0x0199 }
            com.chartboost.sdk.c.k r5 = r9.f6138d     // Catch:{ all -> 0x01a4 }
            long r5 = r5.b()     // Catch:{ all -> 0x01a4 }
            long r0 = r5 - r0
            r10.f6016g = r0     // Catch:{ all -> 0x01a4 }
            r0 = -1
            if (r3 == r0) goto L_0x0191
            boolean r0 = a(r3)     // Catch:{ all -> 0x0186 }
            if (r0 == 0) goto L_0x0172
            java.io.File r0 = r10.f6014e     // Catch:{ all -> 0x0186 }
            if (r0 == 0) goto L_0x014b
            java.io.File r0 = new java.io.File     // Catch:{ all -> 0x0186 }
            java.io.File r1 = r10.f6014e     // Catch:{ all -> 0x0186 }
            java.io.File r1 = r1.getParentFile()     // Catch:{ all -> 0x0186 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0186 }
            r7.<init>()     // Catch:{ all -> 0x0186 }
            java.io.File r8 = r10.f6014e     // Catch:{ all -> 0x0186 }
            java.lang.String r8 = r8.getName()     // Catch:{ all -> 0x0186 }
            r7.append(r8)     // Catch:{ all -> 0x0186 }
            java.lang.String r8 = ".tmp"
            r7.append(r8)     // Catch:{ all -> 0x0186 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x0186 }
            r0.<init>(r1, r7)     // Catch:{ all -> 0x0186 }
            byte[] r11 = new byte[r11]     // Catch:{ all -> 0x0186 }
            java.io.InputStream r1 = r2.getInputStream()     // Catch:{ all -> 0x013c }
            java.io.FileOutputStream r7 = new java.io.FileOutputStream     // Catch:{ all -> 0x013a }
            r7.<init>(r0)     // Catch:{ all -> 0x013a }
            com.chartboost.sdk.o.m0.a(r1, r7)     // Catch:{ all -> 0x0137 }
            if (r1 == 0) goto L_0x00d4
            r1.close()     // Catch:{ IOException -> 0x00d4 }
        L_0x00d4:
            r7.close()     // Catch:{ IOException -> 0x00d7 }
        L_0x00d7:
            java.io.File r1 = r10.f6014e     // Catch:{ all -> 0x0186 }
            boolean r1 = r0.renameTo(r1)     // Catch:{ all -> 0x0186 }
            if (r1 != 0) goto L_0x0174
            boolean r11 = r0.delete()     // Catch:{ all -> 0x0186 }
            if (r11 != 0) goto L_0x010e
            java.io.IOException r11 = new java.io.IOException     // Catch:{ all -> 0x0186 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0186 }
            r1.<init>()     // Catch:{ all -> 0x0186 }
            java.lang.String r3 = "Unable to delete "
            r1.append(r3)     // Catch:{ all -> 0x0186 }
            java.lang.String r0 = r0.getAbsolutePath()     // Catch:{ all -> 0x0186 }
            r1.append(r0)     // Catch:{ all -> 0x0186 }
            java.lang.String r0 = " after failing to rename to "
            r1.append(r0)     // Catch:{ all -> 0x0186 }
            java.io.File r0 = r10.f6014e     // Catch:{ all -> 0x0186 }
            java.lang.String r0 = r0.getAbsolutePath()     // Catch:{ all -> 0x0186 }
            r1.append(r0)     // Catch:{ all -> 0x0186 }
            java.lang.String r0 = r1.toString()     // Catch:{ all -> 0x0186 }
            r11.<init>(r0)     // Catch:{ all -> 0x0186 }
            throw r11     // Catch:{ all -> 0x0186 }
        L_0x010e:
            java.io.IOException r11 = new java.io.IOException     // Catch:{ all -> 0x0186 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0186 }
            r1.<init>()     // Catch:{ all -> 0x0186 }
            java.lang.String r3 = "Unable to move "
            r1.append(r3)     // Catch:{ all -> 0x0186 }
            java.lang.String r0 = r0.getAbsolutePath()     // Catch:{ all -> 0x0186 }
            r1.append(r0)     // Catch:{ all -> 0x0186 }
            java.lang.String r0 = " to "
            r1.append(r0)     // Catch:{ all -> 0x0186 }
            java.io.File r0 = r10.f6014e     // Catch:{ all -> 0x0186 }
            java.lang.String r0 = r0.getAbsolutePath()     // Catch:{ all -> 0x0186 }
            r1.append(r0)     // Catch:{ all -> 0x0186 }
            java.lang.String r0 = r1.toString()     // Catch:{ all -> 0x0186 }
            r11.<init>(r0)     // Catch:{ all -> 0x0186 }
            throw r11     // Catch:{ all -> 0x0186 }
        L_0x0137:
            r11 = move-exception
            r4 = r7
            goto L_0x013e
        L_0x013a:
            r11 = move-exception
            goto L_0x013e
        L_0x013c:
            r11 = move-exception
            r1 = r4
        L_0x013e:
            if (r1 == 0) goto L_0x0145
            r1.close()     // Catch:{ IOException -> 0x0144 }
            goto L_0x0145
        L_0x0144:
        L_0x0145:
            if (r4 == 0) goto L_0x014a
            r4.close()     // Catch:{ IOException -> 0x014a }
        L_0x014a:
            throw r11     // Catch:{ all -> 0x0186 }
        L_0x014b:
            java.io.InputStream r0 = r2.getInputStream()     // Catch:{ IOException -> 0x0153 }
        L_0x014f:
            r4 = r0
            goto L_0x0158
        L_0x0151:
            r11 = move-exception
            goto L_0x016c
        L_0x0153:
            java.io.InputStream r0 = r2.getErrorStream()     // Catch:{ all -> 0x0151 }
            goto L_0x014f
        L_0x0158:
            if (r4 == 0) goto L_0x0164
            java.io.BufferedInputStream r11 = new java.io.BufferedInputStream     // Catch:{ all -> 0x0151 }
            r11.<init>(r4)     // Catch:{ all -> 0x0151 }
            byte[] r11 = com.chartboost.sdk.o.m0.b(r11)     // Catch:{ all -> 0x0151 }
            goto L_0x0166
        L_0x0164:
            byte[] r11 = new byte[r11]     // Catch:{ all -> 0x0151 }
        L_0x0166:
            if (r4 == 0) goto L_0x0174
            r4.close()     // Catch:{ IOException -> 0x0174 }
            goto L_0x0174
        L_0x016c:
            if (r4 == 0) goto L_0x0171
            r4.close()     // Catch:{ IOException -> 0x0171 }
        L_0x0171:
            throw r11     // Catch:{ all -> 0x0186 }
        L_0x0172:
            byte[] r11 = new byte[r11]     // Catch:{ all -> 0x0186 }
        L_0x0174:
            com.chartboost.sdk.c.k r0 = r9.f6138d     // Catch:{ all -> 0x01a4 }
            long r0 = r0.b()     // Catch:{ all -> 0x01a4 }
            long r0 = r0 - r5
            r10.f6017h = r0     // Catch:{ all -> 0x01a4 }
            com.chartboost.sdk.o.j r10 = new com.chartboost.sdk.o.j     // Catch:{ all -> 0x01a4 }
            r10.<init>(r3, r11)     // Catch:{ all -> 0x01a4 }
            r2.disconnect()
            return r10
        L_0x0186:
            r11 = move-exception
            com.chartboost.sdk.c.k r0 = r9.f6138d     // Catch:{ all -> 0x01a4 }
            long r0 = r0.b()     // Catch:{ all -> 0x01a4 }
            long r0 = r0 - r5
            r10.f6017h = r0     // Catch:{ all -> 0x01a4 }
            throw r11     // Catch:{ all -> 0x01a4 }
        L_0x0191:
            java.io.IOException r10 = new java.io.IOException     // Catch:{ all -> 0x01a4 }
            java.lang.String r11 = "Could not retrieve response code from HttpUrlConnection."
            r10.<init>(r11)     // Catch:{ all -> 0x01a4 }
            throw r10     // Catch:{ all -> 0x01a4 }
        L_0x0199:
            r11 = move-exception
            com.chartboost.sdk.c.k r3 = r9.f6138d     // Catch:{ all -> 0x01a4 }
            long r3 = r3.b()     // Catch:{ all -> 0x01a4 }
            long r3 = r3 - r0
            r10.f6016g = r3     // Catch:{ all -> 0x01a4 }
            throw r11     // Catch:{ all -> 0x01a4 }
        L_0x01a4:
            r10 = move-exception
            r2.disconnect()
            goto L_0x01aa
        L_0x01a9:
            throw r10
        L_0x01aa:
            goto L_0x01a9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.o.q.a(com.chartboost.sdk.o.g, int):com.chartboost.sdk.o.j");
    }

    /* renamed from: a */
    public int compareTo(q qVar) {
        return this.f6140f.f6012c - qVar.f6140f.f6012c;
    }
}
