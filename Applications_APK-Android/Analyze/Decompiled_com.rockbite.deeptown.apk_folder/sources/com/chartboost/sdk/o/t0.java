package com.chartboost.sdk.o;

import com.chartboost.sdk.d.a;
import com.chartboost.sdk.n;
import com.chartboost.sdk.o.m;
import org.json.JSONObject;

public class t0 implements m.a {

    /* renamed from: a  reason: collision with root package name */
    private final r0 f6192a;

    /* renamed from: b  reason: collision with root package name */
    private final String f6193b;

    public t0(r0 r0Var, String str) {
        this.f6192a = r0Var;
        this.f6193b = str;
    }

    public void a(m mVar, JSONObject jSONObject) {
        if (this.f6192a.p.f6130h || n.t) {
            synchronized (this.f6192a) {
                this.f6192a.b(this.f6193b);
            }
        }
    }

    public void a(m mVar, a aVar) {
        r0 r0Var = this.f6192a;
        if (r0Var.p.f6130h) {
            synchronized (r0Var) {
                this.f6192a.b(this.f6193b);
            }
        }
    }
}
