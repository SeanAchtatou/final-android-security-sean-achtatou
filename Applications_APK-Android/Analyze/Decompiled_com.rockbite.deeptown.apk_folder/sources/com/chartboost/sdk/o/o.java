package com.chartboost.sdk.o;

import com.chartboost.sdk.c.h;
import com.chartboost.sdk.c.j;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class o {

    /* renamed from: a  reason: collision with root package name */
    private final h f6120a;

    /* renamed from: b  reason: collision with root package name */
    private final Map<String, j.a> f6121b = new HashMap();

    public o(h hVar) {
        this.f6120a = hVar;
    }

    private boolean b(String str) {
        return this.f6120a.b(String.format("%s%s", str, ".png"));
    }

    public j.a a(String str) {
        if (b(str)) {
            if (this.f6121b.containsKey(str)) {
                return this.f6121b.get(str);
            }
            j.a aVar = new j.a(str, new File(this.f6120a.d().f5774b, String.format("%s%s", str, ".png")), this.f6120a);
            this.f6121b.put(str, aVar);
            return aVar;
        } else if (!this.f6121b.containsKey(str)) {
            return null;
        } else {
            this.f6121b.remove(str);
            return null;
        }
    }
}
