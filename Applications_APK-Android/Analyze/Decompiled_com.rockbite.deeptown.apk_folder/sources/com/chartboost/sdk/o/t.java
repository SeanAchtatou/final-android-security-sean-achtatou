package com.chartboost.sdk.o;

import android.util.Log;
import com.underwater.demolisher.data.vo.RemoteConfigConst;
import java.io.File;

public class t {

    /* renamed from: a  reason: collision with root package name */
    private static String f6190a = "CBTrace";

    /* renamed from: b  reason: collision with root package name */
    private static final boolean f6191b = a();

    private static boolean a() {
        File a2;
        try {
            if (!Log.isLoggable(f6190a, 4) || !f1.e().b().equals("mounted") || (a2 = f1.e().a()) == null) {
                return false;
            }
            return new File(a2, ".chartboost/log_trace").exists();
        } catch (Throwable unused) {
            return false;
        }
    }

    public static void a(String str, String str2) {
        if (f6191b) {
            String str3 = f6190a;
            Log.i(str3, str + ": " + str2);
        }
    }

    public static void a(String str, boolean z) {
        if (f6191b) {
            String str2 = f6190a;
            Log.i(str2, str + ": " + z);
        }
    }

    public static void a(String str, Object obj) {
        if (!f6191b) {
            return;
        }
        if (obj != null) {
            String str2 = f6190a;
            Log.i(str2, str + ": " + obj.getClass().getName() + RemoteConfigConst.SHOP_SPECIAL_EVENT_OFFER + obj.hashCode());
            return;
        }
        String str3 = f6190a;
        Log.i(str3, str + ": null");
    }

    public static void a(String str) {
        if (f6191b) {
            Log.i(f6190a, str);
        }
    }
}
