package com.chartboost.sdk.o;

import android.content.Context;
import android.graphics.Point;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.chartboost.sdk.c.g;
import com.chartboost.sdk.d.a;
import com.chartboost.sdk.d.d;
import com.chartboost.sdk.h;
import com.chartboost.sdk.j;
import com.chartboost.sdk.l;
import org.json.JSONObject;

public class i1 extends j {
    com.chartboost.sdk.c.j l = new com.chartboost.sdk.c.j(this);
    com.chartboost.sdk.c.j m = new com.chartboost.sdk.c.j(this);
    com.chartboost.sdk.c.j n = new com.chartboost.sdk.c.j(this);
    com.chartboost.sdk.c.j o = new com.chartboost.sdk.c.j(this);
    com.chartboost.sdk.c.j p = new com.chartboost.sdk.c.j(this);
    com.chartboost.sdk.c.j q = new com.chartboost.sdk.c.j(this);
    protected float r = 1.0f;

    public i1(d dVar, Handler handler, h hVar) {
        super(dVar, handler, hVar);
    }

    /* access modifiers changed from: protected */
    public j.b a(Context context) {
        return new a(context);
    }

    /* access modifiers changed from: protected */
    public Point b(String str) {
        JSONObject a2 = g.a(this.f5890e, str, "offset");
        if (a2 != null) {
            return new Point(a2.optInt("x"), a2.optInt("y"));
        }
        return new Point(0, 0);
    }

    public void d() {
        super.d();
        this.m = null;
        this.l = null;
        this.o = null;
        this.n = null;
        this.q = null;
        this.p = null;
    }

    public boolean a(JSONObject jSONObject) {
        if (!super.a(jSONObject)) {
            return false;
        }
        if (this.f5890e.isNull("frame-portrait") || this.f5890e.isNull("close-portrait")) {
            this.f5895j = false;
        }
        if (this.f5890e.isNull("frame-landscape") || this.f5890e.isNull("close-landscape")) {
            this.f5896k = false;
        }
        if (this.f5890e.isNull("ad-portrait")) {
            this.f5895j = false;
        }
        if (this.f5890e.isNull("ad-landscape")) {
            this.f5896k = false;
        }
        if (this.m.a("frame-landscape") && this.l.a("frame-portrait") && this.o.a("close-landscape") && this.n.a("close-portrait") && this.q.a("ad-landscape") && this.p.a("ad-portrait")) {
            return true;
        }
        com.chartboost.sdk.c.a.b("ImageViewProtocol", "Error while downloading the assets");
        a(a.c.ASSETS_DOWNLOAD_FAILURE);
        return false;
    }

    public class a extends j.b {

        /* renamed from: h  reason: collision with root package name */
        protected c0 f6044h;

        /* renamed from: i  reason: collision with root package name */
        protected d0 f6045i;

        /* renamed from: j  reason: collision with root package name */
        private boolean f6046j = false;

        /* renamed from: k  reason: collision with root package name */
        protected d0 f6047k;
        protected ImageView l;

        /* renamed from: com.chartboost.sdk.o.i1$a$a  reason: collision with other inner class name */
        class C0129a extends d0 {
            C0129a(Context context, i1 i1Var) {
                super(context);
            }

            /* access modifiers changed from: protected */
            public void a(MotionEvent motionEvent) {
                a.this.a(motionEvent.getX(), motionEvent.getY(), (float) a.this.f6047k.getWidth(), (float) a.this.f6047k.getHeight());
            }
        }

        class b extends d0 {
            b(Context context) {
                super(context);
            }

            /* access modifiers changed from: protected */
            public void a(MotionEvent motionEvent) {
                a.this.d();
            }
        }

        protected a(Context context) {
            super(context);
            setBackgroundColor(0);
            setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            l a2 = l.a();
            c0 c0Var = new c0(context);
            a2.a(c0Var);
            this.f6044h = c0Var;
            addView(this.f6044h, new RelativeLayout.LayoutParams(-1, -1));
            C0129a aVar = new C0129a(context, i1.this);
            a2.a(aVar);
            this.f6047k = aVar;
            a(this.f6047k);
            this.f6047k.setContentDescription("CBAd");
            ImageView imageView = new ImageView(context);
            a2.a(imageView);
            this.l = imageView;
            this.l.setBackgroundColor(-16777216);
            addView(this.l);
            addView(this.f6047k);
        }

        /* access modifiers changed from: protected */
        public void a(float f2, float f3, float f4, float f5) {
            i1.this.b(g.a(g.a("x", Float.valueOf(f2)), g.a("y", Float.valueOf(f3)), g.a("w", Float.valueOf(f4)), g.a("h", Float.valueOf(f5))));
        }

        public void b() {
            super.b();
            this.f6044h = null;
            this.f6045i = null;
            this.f6047k = null;
            this.l = null;
        }

        /* access modifiers changed from: protected */
        public void c() {
            this.f6045i = new b(getContext());
            this.f6045i.setContentDescription("CBClose");
            addView(this.f6045i);
        }

        /* access modifiers changed from: protected */
        public void d() {
            i1.this.h();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.min(float, float):float}
         arg types: [float, int]
         candidates:
          ClspMth{java.lang.Math.min(double, double):double}
          ClspMth{java.lang.Math.min(long, long):long}
          ClspMth{java.lang.Math.min(int, int):int}
          ClspMth{java.lang.Math.min(float, float):float} */
        /* access modifiers changed from: protected */
        public void a(int i2, int i3) {
            int i4;
            int i5;
            if (!this.f6046j) {
                c();
                this.f6046j = true;
            }
            boolean a2 = com.chartboost.sdk.c.b.a(i1.this.a());
            i1 i1Var = i1.this;
            com.chartboost.sdk.c.j jVar = a2 ? i1Var.l : i1Var.m;
            i1 i1Var2 = i1.this;
            com.chartboost.sdk.c.j jVar2 = a2 ? i1Var2.n : i1Var2.o;
            if (!jVar.c()) {
                i1 i1Var3 = i1.this;
                com.chartboost.sdk.c.j jVar3 = i1Var3.l;
                jVar = jVar == jVar3 ? i1Var3.m : jVar3;
            }
            if (!jVar2.c()) {
                i1 i1Var4 = i1.this;
                com.chartboost.sdk.c.j jVar4 = i1Var4.n;
                jVar2 = jVar2 == jVar4 ? i1Var4.o : jVar4;
            }
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
            i1.this.a(layoutParams, jVar, 1.0f);
            i1.this.r = Math.min(Math.min(((float) i2) / ((float) layoutParams.width), ((float) i3) / ((float) layoutParams.height)), 1.0f);
            layoutParams.width = (int) (((float) layoutParams.width) * i1.this.r);
            i1 i1Var5 = i1.this;
            layoutParams.height = (int) (((float) layoutParams.height) * i1Var5.r);
            Point b2 = i1Var5.b(a2 ? "frame-portrait" : "frame-landscape");
            layoutParams.leftMargin = Math.round((((float) (i2 - layoutParams.width)) / 2.0f) + ((((float) b2.x) / jVar.e()) * i1.this.r));
            layoutParams.topMargin = Math.round((((float) (i3 - layoutParams.height)) / 2.0f) + ((((float) b2.y) / jVar.e()) * i1.this.r));
            i1.this.a(layoutParams2, jVar2, 1.0f);
            Point b3 = i1.this.b(a2 ? "close-portrait" : "close-landscape");
            if (b3.x == 0 && b3.y == 0) {
                i5 = layoutParams.leftMargin + layoutParams.width + Math.round(((float) (-layoutParams2.width)) / 2.0f);
                i4 = layoutParams.topMargin + Math.round(((float) (-layoutParams2.height)) / 2.0f);
            } else {
                int round = Math.round(((((float) layoutParams.leftMargin) + (((float) layoutParams.width) / 2.0f)) + ((float) b3.x)) - (((float) layoutParams2.width) / 2.0f));
                i4 = Math.round(((((float) layoutParams.topMargin) + (((float) layoutParams.height) / 2.0f)) + ((float) b3.y)) - (((float) layoutParams2.height) / 2.0f));
                i5 = round;
            }
            layoutParams2.leftMargin = Math.min(Math.max(0, i5), i2 - layoutParams2.width);
            layoutParams2.topMargin = Math.min(Math.max(0, i4), i3 - layoutParams2.height);
            this.f6044h.setLayoutParams(layoutParams);
            this.f6045i.setLayoutParams(layoutParams2);
            this.f6044h.setScaleType(ImageView.ScaleType.FIT_CENTER);
            this.f6044h.a(jVar);
            this.f6045i.a(jVar2);
            i1 i1Var6 = i1.this;
            com.chartboost.sdk.c.j jVar5 = a2 ? i1Var6.p : i1Var6.q;
            if (!jVar5.c()) {
                i1 i1Var7 = i1.this;
                com.chartboost.sdk.c.j jVar6 = i1Var7.p;
                jVar5 = jVar5 == jVar6 ? i1Var7.q : jVar6;
            }
            RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
            i1 i1Var8 = i1.this;
            i1Var8.a(layoutParams3, jVar5, i1Var8.r);
            Point b4 = i1.this.b(a2 ? "ad-portrait" : "ad-landscape");
            layoutParams3.leftMargin = Math.round((((float) (i2 - layoutParams3.width)) / 2.0f) + ((((float) b4.x) / jVar5.e()) * i1.this.r));
            layoutParams3.topMargin = Math.round((((float) (i3 - layoutParams3.height)) / 2.0f) + ((((float) b4.y) / jVar5.e()) * i1.this.r));
            this.l.setLayoutParams(layoutParams3);
            this.f6047k.setLayoutParams(layoutParams3);
            this.f6047k.a(ImageView.ScaleType.FIT_CENTER);
            this.f6047k.a(jVar5);
        }
    }

    public void a(ViewGroup.LayoutParams layoutParams, com.chartboost.sdk.c.j jVar, float f2) {
        if (jVar != null && jVar.c()) {
            layoutParams.width = (int) ((((float) jVar.a()) / jVar.e()) * f2);
            layoutParams.height = (int) ((((float) jVar.b()) / jVar.e()) * f2);
        }
    }
}
