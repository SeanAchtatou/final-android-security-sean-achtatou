package com.chartboost.sdk.o;

import com.chartboost.sdk.d.a;
import com.chartboost.sdk.f;
import com.chartboost.sdk.n;
import com.facebook.internal.AnalyticsEvents;

public class p0 {

    /* renamed from: a  reason: collision with root package name */
    public final int f6123a;

    /* renamed from: b  reason: collision with root package name */
    public final String f6124b;

    /* renamed from: c  reason: collision with root package name */
    public final String f6125c;

    /* renamed from: d  reason: collision with root package name */
    public final String f6126d;

    /* renamed from: e  reason: collision with root package name */
    public final String f6127e;

    /* renamed from: f  reason: collision with root package name */
    public final String f6128f;

    /* renamed from: g  reason: collision with root package name */
    public final boolean f6129g;

    /* renamed from: h  reason: collision with root package name */
    public final boolean f6130h;

    public class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private final int f6131a;

        /* renamed from: b  reason: collision with root package name */
        private final String f6132b;

        /* renamed from: c  reason: collision with root package name */
        private final a.c f6133c;

        public a(int i2, String str, a.c cVar) {
            this.f6131a = i2;
            this.f6132b = str;
            this.f6133c = cVar;
        }

        public void run() {
            int i2 = this.f6131a;
            if (i2 == 0) {
                p0.this.d(this.f6132b);
            } else if (i2 == 1) {
                p0.this.a(this.f6132b);
            } else if (i2 == 2) {
                p0.this.b(this.f6132b);
            } else if (i2 == 3) {
                p0.this.c(this.f6132b);
            } else if (i2 == 4) {
                p0.this.a(this.f6132b, this.f6133c);
            } else if (i2 == 5) {
                p0.this.e(this.f6132b);
            }
        }
    }

    private p0(int i2, String str, String str2, String str3, String str4, String str5, boolean z, boolean z2) {
        this.f6123a = i2;
        this.f6124b = str;
        this.f6125c = str2;
        this.f6126d = str3;
        this.f6127e = str4;
        this.f6128f = str5;
        this.f6129g = z;
        this.f6130h = z2;
    }

    public static p0 a() {
        return new p0(0, "interstitial", "interstitial", "/interstitial/get", "webview/%s/interstitial/get", "/interstitial/show", false, false);
    }

    public static p0 b() {
        return new p0(1, "rewarded", "rewarded-video", "/reward/get", "webview/%s/reward/get", "/reward/show", true, false);
    }

    public static p0 c() {
        return new p0(2, "inplay", null, "/inplay/get", "no webview endpoint", "/inplay/show", false, true);
    }

    public void d(String str) {
        f fVar = n.f5941d;
        if (fVar != null) {
            int i2 = this.f6123a;
            if (i2 == 0) {
                fVar.didCacheInterstitial(str);
            } else if (i2 == 1) {
                fVar.didCacheRewardedVideo(str);
            } else if (i2 == 2) {
                fVar.didCacheInPlay(str);
            }
        }
    }

    public void e(String str) {
        f fVar = n.f5941d;
        if (fVar != null) {
            int i2 = this.f6123a;
            if (i2 == 0) {
                fVar.didDisplayInterstitial(str);
            } else if (i2 == 1) {
                fVar.didDisplayRewardedVideo(str);
            }
        }
    }

    public boolean f(String str) {
        f fVar = n.f5941d;
        if (fVar == null) {
            return true;
        }
        int i2 = this.f6123a;
        if (i2 == 0) {
            return fVar.shouldDisplayInterstitial(str);
        }
        if (i2 != 1) {
            return true;
        }
        return fVar.shouldDisplayRewardedVideo(str);
    }

    public boolean g(String str) {
        f fVar = n.f5941d;
        if (fVar == null || this.f6123a != 0) {
            return true;
        }
        return fVar.shouldRequestInterstitial(str);
    }

    public String a(int i2) {
        Object[] objArr = new Object[2];
        objArr[0] = this.f6125c;
        objArr[1] = i2 == 1 ? AnalyticsEvents.PARAMETER_SHARE_DIALOG_SHOW_WEB : "native";
        return String.format("%s-%s", objArr);
    }

    public void b(String str) {
        f fVar = n.f5941d;
        if (fVar != null) {
            int i2 = this.f6123a;
            if (i2 == 0) {
                fVar.didCloseInterstitial(str);
            } else if (i2 == 1) {
                fVar.didCloseRewardedVideo(str);
            }
        }
    }

    public void c(String str) {
        f fVar = n.f5941d;
        if (fVar != null) {
            int i2 = this.f6123a;
            if (i2 == 0) {
                fVar.didDismissInterstitial(str);
            } else if (i2 == 1) {
                fVar.didDismissRewardedVideo(str);
            }
        }
    }

    public void a(String str) {
        f fVar = n.f5941d;
        if (fVar != null) {
            int i2 = this.f6123a;
            if (i2 == 0) {
                fVar.didClickInterstitial(str);
            } else if (i2 == 1) {
                fVar.didClickRewardedVideo(str);
            }
        }
    }

    public void a(String str, a.c cVar) {
        f fVar = n.f5941d;
        if (fVar != null) {
            int i2 = this.f6123a;
            if (i2 == 0) {
                fVar.didFailToLoadInterstitial(str, cVar);
            } else if (i2 == 1) {
                fVar.didFailToLoadRewardedVideo(str, cVar);
            } else if (i2 == 2) {
                fVar.didFailToLoadInPlay(str, cVar);
            }
        }
    }
}
