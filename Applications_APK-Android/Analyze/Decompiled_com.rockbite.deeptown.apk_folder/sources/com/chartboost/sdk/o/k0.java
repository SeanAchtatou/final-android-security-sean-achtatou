package com.chartboost.sdk.o;

import org.json.JSONObject;

public class k0 implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final h0 f6073a;

    /* renamed from: b  reason: collision with root package name */
    private final j0 f6074b;

    /* renamed from: c  reason: collision with root package name */
    private final int f6075c;

    /* renamed from: d  reason: collision with root package name */
    private final JSONObject f6076d;

    /* renamed from: e  reason: collision with root package name */
    private final String f6077e;

    k0(h0 h0Var, j0 j0Var, int i2, String str, JSONObject jSONObject) {
        this.f6073a = h0Var;
        this.f6074b = j0Var;
        this.f6075c = i2;
        this.f6077e = str;
        this.f6076d = jSONObject;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:28|29) */
    /* JADX WARNING: Can't wrap try/catch for region: R(2:35|36) */
    /* JADX WARNING: Can't wrap try/catch for region: R(3:22|23|101) */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:45|46|47|48|88) */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:74|75|76|77|113) */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:78|79|80|81|93) */
    /* JADX WARNING: Code restructure failed: missing block: B:101:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        com.chartboost.sdk.c.a.b("NativeBridgeCommand", "Cannot find video file name");
        r13.f6074b.e("Parsing exception unknown field for video replay");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        com.chartboost.sdk.c.a.b("NativeBridgeCommand", "Cannot find video file name");
        r13.f6074b.e("Parsing exception unknown field for video play");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        com.chartboost.sdk.c.a.b("NativeBridgeCommand", "Cannot find video file name");
        r13.f6074b.e("Parsing exception unknown field for video pause");
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:22:0x007d */
    /* JADX WARNING: Missing exception handler attribute for start block: B:28:0x009e */
    /* JADX WARNING: Missing exception handler attribute for start block: B:35:0x00c5 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:47:0x013d */
    /* JADX WARNING: Missing exception handler attribute for start block: B:76:0x022a */
    /* JADX WARNING: Missing exception handler attribute for start block: B:80:0x0256 */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:31:0x00b0=Splitter:B:31:0x00b0, B:17:0x0062=Splitter:B:17:0x0062, B:80:0x0256=Splitter:B:80:0x0256, B:24:0x0089=Splitter:B:24:0x0089, B:76:0x022a=Splitter:B:76:0x022a, B:47:0x013d=Splitter:B:47:0x013d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r13 = this;
            java.lang.String r0 = "http://"
            java.lang.String r1 = "Exception while opening a browser view with MRAID url"
            java.lang.String r2 = "ActivityNotFoundException occured when opening a url in a browser"
            java.lang.String r3 = "Exception occured while parsing the message for webview debug track event"
            int r4 = r13.f6075c     // Catch:{ Exception -> 0x026e }
            r5 = 1148846080(0x447a0000, float:1000.0)
            java.lang.String r6 = ""
            java.lang.String r7 = "Cannot find duration parameter for the video"
            java.lang.String r8 = "duration"
            java.lang.String r9 = "Cannot find video file name"
            java.lang.String r10 = "name"
            java.lang.String r11 = "message"
            java.lang.String r12 = "NativeBridgeCommand"
            switch(r4) {
                case 0: goto L_0x0267;
                case 1: goto L_0x0261;
                case 2: goto L_0x0233;
                case 3: goto L_0x0204;
                case 4: goto L_0x01d1;
                case 5: goto L_0x0150;
                case 6: goto L_0x0149;
                case 7: goto L_0x0119;
                case 8: goto L_0x00e9;
                case 9: goto L_0x00d7;
                case 10: goto L_0x00b0;
                case 11: goto L_0x0089;
                case 12: goto L_0x0062;
                case 13: goto L_0x002f;
                case 14: goto L_0x001f;
                default: goto L_0x001d;
            }
        L_0x001d:
            goto L_0x028c
        L_0x001f:
            com.chartboost.sdk.o.j0 r0 = r13.f6074b     // Catch:{ Exception -> 0x0028 }
            org.json.JSONObject r1 = r13.f6076d     // Catch:{ Exception -> 0x0028 }
            r0.c(r1)     // Catch:{ Exception -> 0x0028 }
            goto L_0x028c
        L_0x0028:
            java.lang.String r0 = "Invalid set orientation command"
            com.chartboost.sdk.c.a.b(r12, r0)     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x002f:
            org.json.JSONObject r0 = r13.f6076d     // Catch:{ Exception -> 0x0056 }
            java.lang.String r0 = r0.getString(r11)     // Catch:{ Exception -> 0x0056 }
            java.lang.Class<com.chartboost.sdk.o.i0> r1 = com.chartboost.sdk.o.i0.class
            java.lang.String r1 = r1.getName()     // Catch:{ Exception -> 0x0056 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0056 }
            r2.<init>()     // Catch:{ Exception -> 0x0056 }
            java.lang.String r3 = "JS->Native Warning message: "
            r2.append(r3)     // Catch:{ Exception -> 0x0056 }
            r2.append(r0)     // Catch:{ Exception -> 0x0056 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0056 }
            android.util.Log.d(r1, r2)     // Catch:{ Exception -> 0x0056 }
            com.chartboost.sdk.o.j0 r1 = r13.f6074b     // Catch:{ Exception -> 0x0056 }
            r1.e(r0)     // Catch:{ Exception -> 0x0056 }
            goto L_0x028c
        L_0x0056:
            java.lang.String r0 = "Warning message is empty"
            com.chartboost.sdk.c.a.b(r12, r0)     // Catch:{ Exception -> 0x026e }
            com.chartboost.sdk.o.j0 r0 = r13.f6074b     // Catch:{ Exception -> 0x026e }
            r0.e(r6)     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x0062:
            org.json.JSONObject r0 = r13.f6076d     // Catch:{ Exception -> 0x007d }
            java.lang.String r0 = r0.getString(r10)     // Catch:{ Exception -> 0x007d }
            com.chartboost.sdk.o.f1 r1 = com.chartboost.sdk.o.f1.e()     // Catch:{ Exception -> 0x007d }
            boolean r1 = r1.a(r0)     // Catch:{ Exception -> 0x007d }
            if (r1 != 0) goto L_0x0076
            com.chartboost.sdk.o.j0 r1 = r13.f6074b     // Catch:{ Exception -> 0x007d }
            r1.q = r0     // Catch:{ Exception -> 0x007d }
        L_0x0076:
            com.chartboost.sdk.o.j0 r0 = r13.f6074b     // Catch:{ Exception -> 0x007d }
            r0.x()     // Catch:{ Exception -> 0x007d }
            goto L_0x028c
        L_0x007d:
            com.chartboost.sdk.c.a.b(r12, r9)     // Catch:{ Exception -> 0x026e }
            com.chartboost.sdk.o.j0 r0 = r13.f6074b     // Catch:{ Exception -> 0x026e }
            java.lang.String r1 = "Parsing exception unknown field for video replay"
            r0.e(r1)     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x0089:
            org.json.JSONObject r0 = r13.f6076d     // Catch:{ Exception -> 0x009e }
            java.lang.String r0 = r0.getString(r10)     // Catch:{ Exception -> 0x009e }
            com.chartboost.sdk.o.f1 r1 = com.chartboost.sdk.o.f1.e()     // Catch:{ Exception -> 0x009e }
            boolean r1 = r1.a(r0)     // Catch:{ Exception -> 0x009e }
            if (r1 != 0) goto L_0x00a8
            com.chartboost.sdk.o.j0 r1 = r13.f6074b     // Catch:{ Exception -> 0x009e }
            r1.q = r0     // Catch:{ Exception -> 0x009e }
            goto L_0x00a8
        L_0x009e:
            com.chartboost.sdk.c.a.b(r12, r9)     // Catch:{ Exception -> 0x026e }
            com.chartboost.sdk.o.j0 r0 = r13.f6074b     // Catch:{ Exception -> 0x026e }
            java.lang.String r1 = "Parsing exception unknown field for video play"
            r0.e(r1)     // Catch:{ Exception -> 0x026e }
        L_0x00a8:
            com.chartboost.sdk.o.j0 r0 = r13.f6074b     // Catch:{ Exception -> 0x026e }
            r1 = 2
            r0.b(r1)     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x00b0:
            org.json.JSONObject r0 = r13.f6076d     // Catch:{ Exception -> 0x00c5 }
            java.lang.String r0 = r0.getString(r10)     // Catch:{ Exception -> 0x00c5 }
            com.chartboost.sdk.o.f1 r1 = com.chartboost.sdk.o.f1.e()     // Catch:{ Exception -> 0x00c5 }
            boolean r1 = r1.a(r0)     // Catch:{ Exception -> 0x00c5 }
            if (r1 != 0) goto L_0x00cf
            com.chartboost.sdk.o.j0 r1 = r13.f6074b     // Catch:{ Exception -> 0x00c5 }
            r1.q = r0     // Catch:{ Exception -> 0x00c5 }
            goto L_0x00cf
        L_0x00c5:
            com.chartboost.sdk.c.a.b(r12, r9)     // Catch:{ Exception -> 0x026e }
            com.chartboost.sdk.o.j0 r0 = r13.f6074b     // Catch:{ Exception -> 0x026e }
            java.lang.String r1 = "Parsing exception unknown field for video pause"
            r0.e(r1)     // Catch:{ Exception -> 0x026e }
        L_0x00cf:
            com.chartboost.sdk.o.j0 r0 = r13.f6074b     // Catch:{ Exception -> 0x026e }
            r1 = 3
            r0.b(r1)     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x00d7:
            com.chartboost.sdk.o.h0 r0 = r13.f6073a     // Catch:{ Exception -> 0x026e }
            r0.onHideCustomView()     // Catch:{ Exception -> 0x026e }
            com.chartboost.sdk.o.j0 r0 = r13.f6074b     // Catch:{ Exception -> 0x026e }
            r1 = 1
            r0.b(r1)     // Catch:{ Exception -> 0x026e }
            com.chartboost.sdk.o.j0 r0 = r13.f6074b     // Catch:{ Exception -> 0x026e }
            r0.w()     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x00e9:
            org.json.JSONObject r0 = r13.f6076d     // Catch:{ Exception -> 0x0112 }
            java.lang.String r1 = "event"
            java.lang.String r0 = r0.getString(r1)     // Catch:{ Exception -> 0x0112 }
            com.chartboost.sdk.o.j0 r1 = r13.f6074b     // Catch:{ Exception -> 0x0112 }
            r1.b(r0)     // Catch:{ Exception -> 0x0112 }
            java.lang.Class<com.chartboost.sdk.o.i0> r1 = com.chartboost.sdk.o.i0.class
            java.lang.String r1 = r1.getName()     // Catch:{ Exception -> 0x0112 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0112 }
            r2.<init>()     // Catch:{ Exception -> 0x0112 }
            java.lang.String r3 = "JS->Native Track VAST event message: "
            r2.append(r3)     // Catch:{ Exception -> 0x0112 }
            r2.append(r0)     // Catch:{ Exception -> 0x0112 }
            java.lang.String r0 = r2.toString()     // Catch:{ Exception -> 0x0112 }
            android.util.Log.d(r1, r0)     // Catch:{ Exception -> 0x0112 }
            goto L_0x028c
        L_0x0112:
            java.lang.String r0 = "Exception occured while parsing the message for webview tracking VAST events"
            com.chartboost.sdk.c.a.b(r12, r0)     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x0119:
            org.json.JSONObject r0 = r13.f6076d     // Catch:{ Exception -> 0x013d }
            double r0 = r0.getDouble(r8)     // Catch:{ Exception -> 0x013d }
            float r0 = (float) r0     // Catch:{ Exception -> 0x013d }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x013d }
            r1.<init>()     // Catch:{ Exception -> 0x013d }
            java.lang.String r2 = "######### JS->Native Video total player duration"
            r1.append(r2)     // Catch:{ Exception -> 0x013d }
            float r0 = r0 * r5
            r1.append(r0)     // Catch:{ Exception -> 0x013d }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x013d }
            com.chartboost.sdk.c.a.a(r12, r1)     // Catch:{ Exception -> 0x013d }
            com.chartboost.sdk.o.j0 r1 = r13.f6074b     // Catch:{ Exception -> 0x013d }
            r1.b(r0)     // Catch:{ Exception -> 0x013d }
            goto L_0x028c
        L_0x013d:
            com.chartboost.sdk.o.j0 r0 = r13.f6074b     // Catch:{ Exception -> 0x026e }
            java.lang.String r1 = "Parsing exception unknown field for total player duration"
            r0.e(r1)     // Catch:{ Exception -> 0x026e }
            com.chartboost.sdk.c.a.b(r12, r7)     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x0149:
            com.chartboost.sdk.o.j0 r0 = r13.f6074b     // Catch:{ Exception -> 0x026e }
            r0.z()     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x0150:
            org.json.JSONObject r3 = r13.f6076d     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            java.lang.String r4 = "url"
            java.lang.String r3 = r3.getString(r4)     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            boolean r4 = r3.startsWith(r0)     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            if (r4 != 0) goto L_0x0175
            java.lang.String r4 = "https://"
            boolean r4 = r3.startsWith(r4)     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            if (r4 != 0) goto L_0x0175
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            r4.<init>()     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            r4.append(r0)     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            r4.append(r3)     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            java.lang.String r3 = r4.toString()     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
        L_0x0175:
            android.content.Intent r0 = new android.content.Intent     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            java.lang.String r4 = "android.intent.action.VIEW"
            android.net.Uri r5 = android.net.Uri.parse(r3)     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            r0.<init>(r4, r5)     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            com.chartboost.sdk.o.j0 r4 = r13.f6074b     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            com.chartboost.sdk.o.j0$d r4 = r4.e()     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            if (r4 == 0) goto L_0x028c
            android.content.Context r4 = r4.getContext()     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            if (r4 == 0) goto L_0x028c
            android.content.pm.PackageManager r5 = r4.getPackageManager()     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            android.content.ComponentName r5 = r0.resolveActivity(r5)     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            if (r5 == 0) goto L_0x028c
            r4.startActivity(r0)     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            java.lang.Class<com.chartboost.sdk.o.i0> r0 = com.chartboost.sdk.o.i0.class
            java.lang.String r0 = r0.getName()     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            r4.<init>()     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            java.lang.String r5 = "JS->Native Track MRAID openUrl: "
            r4.append(r5)     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            r4.append(r3)     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            java.lang.String r3 = r4.toString()     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            com.chartboost.sdk.c.a.a(r0, r3)     // Catch:{ ActivityNotFoundException -> 0x01c4, Exception -> 0x01b7 }
            goto L_0x028c
        L_0x01b7:
            r0 = move-exception
            java.lang.Class r2 = r13.getClass()     // Catch:{ Exception -> 0x026e }
            com.chartboost.sdk.e.a.a(r2, r1, r0)     // Catch:{ Exception -> 0x026e }
            com.chartboost.sdk.c.a.b(r12, r1)     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x01c4:
            r0 = move-exception
            java.lang.Class r1 = r13.getClass()     // Catch:{ Exception -> 0x026e }
            com.chartboost.sdk.e.a.a(r1, r2, r0)     // Catch:{ Exception -> 0x026e }
            com.chartboost.sdk.c.a.b(r12, r2)     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x01d1:
            org.json.JSONObject r0 = r13.f6076d     // Catch:{ Exception -> 0x01f8 }
            java.lang.String r0 = r0.getString(r11)     // Catch:{ Exception -> 0x01f8 }
            java.lang.Class<com.chartboost.sdk.o.i0> r1 = com.chartboost.sdk.o.i0.class
            java.lang.String r1 = r1.getName()     // Catch:{ Exception -> 0x01f8 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01f8 }
            r2.<init>()     // Catch:{ Exception -> 0x01f8 }
            java.lang.String r3 = "JS->Native Error message: "
            r2.append(r3)     // Catch:{ Exception -> 0x01f8 }
            r2.append(r0)     // Catch:{ Exception -> 0x01f8 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x01f8 }
            android.util.Log.d(r1, r2)     // Catch:{ Exception -> 0x01f8 }
            com.chartboost.sdk.o.j0 r1 = r13.f6074b     // Catch:{ Exception -> 0x01f8 }
            r1.d(r0)     // Catch:{ Exception -> 0x01f8 }
            goto L_0x028c
        L_0x01f8:
            java.lang.String r0 = "Error message is empty"
            com.chartboost.sdk.c.a.b(r12, r0)     // Catch:{ Exception -> 0x026e }
            com.chartboost.sdk.o.j0 r0 = r13.f6074b     // Catch:{ Exception -> 0x026e }
            r0.d(r6)     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x0204:
            org.json.JSONObject r0 = r13.f6076d     // Catch:{ Exception -> 0x022a }
            java.lang.String r0 = r0.getString(r11)     // Catch:{ Exception -> 0x022a }
            java.lang.Class<com.chartboost.sdk.o.i0> r1 = com.chartboost.sdk.o.i0.class
            java.lang.String r1 = r1.getName()     // Catch:{ Exception -> 0x022a }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x022a }
            r2.<init>()     // Catch:{ Exception -> 0x022a }
            java.lang.String r4 = "JS->Native Debug message: "
            r2.append(r4)     // Catch:{ Exception -> 0x022a }
            r2.append(r0)     // Catch:{ Exception -> 0x022a }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x022a }
            android.util.Log.d(r1, r2)     // Catch:{ Exception -> 0x022a }
            com.chartboost.sdk.o.j0 r1 = r13.f6074b     // Catch:{ Exception -> 0x022a }
            r1.c(r0)     // Catch:{ Exception -> 0x022a }
            goto L_0x028c
        L_0x022a:
            com.chartboost.sdk.c.a.b(r12, r3)     // Catch:{ Exception -> 0x026e }
            com.chartboost.sdk.o.j0 r0 = r13.f6074b     // Catch:{ Exception -> 0x026e }
            r0.c(r3)     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x0233:
            org.json.JSONObject r0 = r13.f6076d     // Catch:{ Exception -> 0x0256 }
            double r0 = r0.getDouble(r8)     // Catch:{ Exception -> 0x0256 }
            float r0 = (float) r0     // Catch:{ Exception -> 0x0256 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0256 }
            r1.<init>()     // Catch:{ Exception -> 0x0256 }
            java.lang.String r2 = "######### JS->Native Video current player duration"
            r1.append(r2)     // Catch:{ Exception -> 0x0256 }
            float r0 = r0 * r5
            r1.append(r0)     // Catch:{ Exception -> 0x0256 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0256 }
            com.chartboost.sdk.c.a.a(r12, r1)     // Catch:{ Exception -> 0x0256 }
            com.chartboost.sdk.o.j0 r1 = r13.f6074b     // Catch:{ Exception -> 0x0256 }
            r1.a(r0)     // Catch:{ Exception -> 0x0256 }
            goto L_0x028c
        L_0x0256:
            com.chartboost.sdk.o.j0 r0 = r13.f6074b     // Catch:{ Exception -> 0x026e }
            java.lang.String r1 = "Parsing exception unknown field for current player duration"
            r0.e(r1)     // Catch:{ Exception -> 0x026e }
            com.chartboost.sdk.c.a.b(r12, r7)     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x0261:
            com.chartboost.sdk.o.j0 r0 = r13.f6074b     // Catch:{ Exception -> 0x026e }
            r0.h()     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x0267:
            com.chartboost.sdk.o.j0 r0 = r13.f6074b     // Catch:{ Exception -> 0x026e }
            r1 = 0
            r0.b(r1)     // Catch:{ Exception -> 0x026e }
            goto L_0x028c
        L_0x026e:
            r0 = move-exception
            java.lang.Class<com.chartboost.sdk.o.k0> r1 = com.chartboost.sdk.o.k0.class
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "run("
            r2.append(r3)
            java.lang.String r3 = r13.f6077e
            r2.append(r3)
            java.lang.String r3 = ")"
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.chartboost.sdk.e.a.a(r1, r2, r0)
        L_0x028c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.o.k0.run():void");
    }
}
