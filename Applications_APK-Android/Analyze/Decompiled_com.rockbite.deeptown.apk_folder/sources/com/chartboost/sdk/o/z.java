package com.chartboost.sdk.o;

import android.os.Handler;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import com.chartboost.sdk.d.b;
import com.chartboost.sdk.d.d;
import com.chartboost.sdk.i;
import com.esotericsoftware.spine.Animation;

public class z {

    /* renamed from: a  reason: collision with root package name */
    private final Handler f6243a;

    class a implements ViewTreeObserver.OnGlobalLayoutListener {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ View f6244a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ int f6245b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ d f6246c;

        /* renamed from: d  reason: collision with root package name */
        final /* synthetic */ Runnable f6247d;

        /* renamed from: e  reason: collision with root package name */
        final /* synthetic */ boolean f6248e;

        a(View view, int i2, d dVar, Runnable runnable, boolean z) {
            this.f6244a = view;
            this.f6245b = i2;
            this.f6246c = dVar;
            this.f6247d = runnable;
            this.f6248e = z;
        }

        public void onGlobalLayout() {
            this.f6244a.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            z.this.a(this.f6245b, this.f6246c, this.f6247d, this.f6248e);
        }
    }

    public z(Handler handler) {
        this.f6243a = handler;
    }

    public static Integer a(int i2) {
        if (i2 < 1 || i2 > 9) {
            return null;
        }
        return Integer.valueOf(i2);
    }

    public void a(int i2, d dVar, Runnable runnable, i iVar) {
        a(i2, dVar, runnable, true, iVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.o.z.a(int, com.chartboost.sdk.d.d, java.lang.Runnable, boolean):void
     arg types: [int, com.chartboost.sdk.d.d, java.lang.Runnable, int]
     candidates:
      com.chartboost.sdk.o.z.a(int, com.chartboost.sdk.d.d, java.lang.Runnable, com.chartboost.sdk.i):void
      com.chartboost.sdk.o.z.a(int, com.chartboost.sdk.d.d, java.lang.Runnable, boolean):void */
    public void a(int i2, d dVar, Runnable runnable) {
        a(i2, dVar, runnable, false);
    }

    private void a(int i2, d dVar, Runnable runnable, boolean z, i iVar) {
        g0 g0Var;
        if (i2 == 7) {
            if (runnable != null) {
                runnable.run();
            }
        } else if (dVar == null || (g0Var = dVar.v) == null) {
            com.chartboost.sdk.c.a.a("AnimationManager", "Transition of impression canceled due to lack of container");
        } else {
            View d2 = g0Var.d();
            if (d2 == null) {
                iVar.d(dVar);
                com.chartboost.sdk.c.a.a("AnimationManager", "Transition of impression canceled due to lack of view");
                return;
            }
            ViewTreeObserver viewTreeObserver = d2.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.addOnGlobalLayoutListener(new a(d2, i2, dVar, runnable, z));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, d dVar, Runnable runnable, boolean z) {
        g0 g0Var;
        e0 e0Var;
        ScaleAnimation scaleAnimation;
        TranslateAnimation translateAnimation;
        e0 e0Var2;
        ScaleAnimation scaleAnimation2;
        TranslateAnimation translateAnimation2;
        AlphaAnimation alphaAnimation;
        int i3 = i2;
        d dVar2 = dVar;
        Runnable runnable2 = runnable;
        AnimationSet animationSet = new AnimationSet(true);
        animationSet.addAnimation(new AlphaAnimation(1.0f, 1.0f));
        if (dVar2 == null || (g0Var = dVar2.v) == null) {
            com.chartboost.sdk.c.a.a("AnimationManager", "Transition of impression canceled due to lack of container");
            if (runnable2 != null) {
                runnable.run();
                return;
            }
            return;
        }
        View d2 = g0Var.d();
        if (d2 == null) {
            if (runnable2 != null) {
                runnable.run();
            }
            com.chartboost.sdk.c.a.a("AnimationManager", "Transition of impression canceled due to lack of view");
            return;
        }
        int i4 = dVar2.n;
        if (i4 == 2 || i4 == 1) {
            d2 = dVar2.v;
        }
        float width = (float) d2.getWidth();
        float height = (float) d2.getHeight();
        int i5 = dVar2.r.f5817b;
        switch (i3) {
            case 1:
                if (z) {
                    e0Var = new e0(-60.0f, Animation.CurveTimeline.LINEAR, width / 2.0f, height / 2.0f, true);
                } else {
                    e0Var = new e0(Animation.CurveTimeline.LINEAR, 60.0f, width / 2.0f, height / 2.0f, true);
                }
                e0Var.setDuration(500);
                e0Var.setFillAfter(true);
                animationSet.addAnimation(e0Var);
                if (z) {
                    scaleAnimation = new ScaleAnimation(0.4f, 1.0f, 0.4f, 1.0f);
                } else {
                    scaleAnimation = new ScaleAnimation(1.0f, 0.4f, 1.0f, 0.4f);
                }
                scaleAnimation.setDuration(500);
                scaleAnimation.setFillAfter(true);
                animationSet.addAnimation(scaleAnimation);
                if (z) {
                    translateAnimation = new TranslateAnimation((-width) * 0.4f, Animation.CurveTimeline.LINEAR, height * 0.3f, Animation.CurveTimeline.LINEAR);
                } else {
                    translateAnimation = new TranslateAnimation(Animation.CurveTimeline.LINEAR, width, Animation.CurveTimeline.LINEAR, height * 0.3f);
                }
                translateAnimation.setDuration(500);
                translateAnimation.setFillAfter(true);
                animationSet.addAnimation(translateAnimation);
                break;
            case 2:
                if (!z) {
                    ScaleAnimation scaleAnimation3 = new ScaleAnimation(1.0f, Animation.CurveTimeline.LINEAR, 1.0f, Animation.CurveTimeline.LINEAR, 1, 0.5f, 1, 0.5f);
                    scaleAnimation3.setDuration(500);
                    scaleAnimation3.setStartOffset(0);
                    scaleAnimation3.setFillAfter(true);
                    animationSet.addAnimation(scaleAnimation3);
                    break;
                } else {
                    ScaleAnimation scaleAnimation4 = new ScaleAnimation(0.6f, 1.1f, 0.6f, 1.1f, 1, 0.5f, 1, 0.5f);
                    float f2 = (float) 500;
                    float f3 = 0.6f * f2;
                    scaleAnimation4.setDuration((long) Math.round(f3));
                    scaleAnimation4.setStartOffset(0);
                    scaleAnimation4.setFillAfter(true);
                    animationSet.addAnimation(scaleAnimation4);
                    ScaleAnimation scaleAnimation5 = new ScaleAnimation(1.0f, 0.81818175f, 1.0f, 0.81818175f, 1, 0.5f, 1, 0.5f);
                    scaleAnimation5.setDuration((long) Math.round(0.19999999f * f2));
                    scaleAnimation5.setStartOffset((long) Math.round(f3));
                    scaleAnimation5.setFillAfter(true);
                    animationSet.addAnimation(scaleAnimation5);
                    ScaleAnimation scaleAnimation6 = new ScaleAnimation(1.0f, 1.1111112f, 1.0f, 1.1111112f, 1, 0.5f, 1, 0.5f);
                    scaleAnimation6.setDuration((long) Math.round(0.099999964f * f2));
                    scaleAnimation6.setStartOffset((long) Math.round(f2 * 0.8f));
                    scaleAnimation6.setFillAfter(true);
                    animationSet.addAnimation(scaleAnimation6);
                    break;
                }
            case 3:
                if (z) {
                    e0Var2 = new e0(-60.0f, Animation.CurveTimeline.LINEAR, width / 2.0f, height / 2.0f, false);
                } else {
                    e0Var2 = new e0(Animation.CurveTimeline.LINEAR, 60.0f, width / 2.0f, height / 2.0f, false);
                }
                e0Var2.setDuration(500);
                e0Var2.setFillAfter(true);
                animationSet.addAnimation(e0Var2);
                if (z) {
                    scaleAnimation2 = new ScaleAnimation(0.4f, 1.0f, 0.4f, 1.0f);
                } else {
                    scaleAnimation2 = new ScaleAnimation(1.0f, 0.4f, 1.0f, 0.4f);
                }
                scaleAnimation2.setDuration(500);
                scaleAnimation2.setFillAfter(true);
                animationSet.addAnimation(scaleAnimation2);
                if (z) {
                    translateAnimation2 = new TranslateAnimation(width * 0.3f, Animation.CurveTimeline.LINEAR, (-height) * 0.4f, Animation.CurveTimeline.LINEAR);
                } else {
                    translateAnimation2 = new TranslateAnimation(Animation.CurveTimeline.LINEAR, width * 0.3f, Animation.CurveTimeline.LINEAR, height);
                }
                translateAnimation2.setDuration(500);
                translateAnimation2.setFillAfter(true);
                animationSet.addAnimation(translateAnimation2);
                break;
            case 4:
                TranslateAnimation translateAnimation3 = new TranslateAnimation(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, z ? -height : Animation.CurveTimeline.LINEAR, z ? Animation.CurveTimeline.LINEAR : -height);
                translateAnimation3.setDuration(500);
                translateAnimation3.setFillAfter(true);
                animationSet.addAnimation(translateAnimation3);
                break;
            case 5:
                float f4 = z ? height : Animation.CurveTimeline.LINEAR;
                if (z) {
                    height = Animation.CurveTimeline.LINEAR;
                }
                TranslateAnimation translateAnimation4 = new TranslateAnimation(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, f4, height);
                translateAnimation4.setDuration(500);
                translateAnimation4.setFillAfter(true);
                animationSet.addAnimation(translateAnimation4);
                break;
            case 6:
                if (z) {
                    alphaAnimation = new AlphaAnimation((float) Animation.CurveTimeline.LINEAR, 1.0f);
                } else {
                    alphaAnimation = new AlphaAnimation(1.0f, (float) Animation.CurveTimeline.LINEAR);
                }
                alphaAnimation.setDuration(500);
                alphaAnimation.setFillAfter(true);
                animationSet = new AnimationSet(true);
                animationSet.addAnimation(alphaAnimation);
                break;
            case 8:
                float f5 = z ? width : Animation.CurveTimeline.LINEAR;
                if (z) {
                    width = Animation.CurveTimeline.LINEAR;
                }
                TranslateAnimation translateAnimation5 = new TranslateAnimation(f5, width, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
                translateAnimation5.setDuration(500);
                translateAnimation5.setFillAfter(true);
                animationSet.addAnimation(translateAnimation5);
                break;
            case 9:
                TranslateAnimation translateAnimation6 = new TranslateAnimation(z ? -width : Animation.CurveTimeline.LINEAR, z ? Animation.CurveTimeline.LINEAR : -width, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
                translateAnimation6.setDuration(500);
                translateAnimation6.setFillAfter(true);
                animationSet.addAnimation(translateAnimation6);
                break;
        }
        if (i3 == 7) {
            if (runnable2 != null) {
                runnable.run();
            }
            return;
        }
        if (runnable2 != null) {
            this.f6243a.postDelayed(runnable2, 500);
        }
        d2.startAnimation(animationSet);
    }

    public void a(boolean z, View view, b bVar) {
        int i2 = bVar.f5817b;
        a(z, view, 500);
    }

    public void a(boolean z, View view, long j2) {
        view.clearAnimation();
        if (z) {
            view.setVisibility(0);
        }
        float f2 = Animation.CurveTimeline.LINEAR;
        float f3 = z ? Animation.CurveTimeline.LINEAR : 1.0f;
        if (z) {
            f2 = 1.0f;
        }
        AlphaAnimation alphaAnimation = new AlphaAnimation(f3, f2);
        alphaAnimation.setDuration(j2);
        alphaAnimation.setFillBefore(true);
        view.startAnimation(alphaAnimation);
    }
}
