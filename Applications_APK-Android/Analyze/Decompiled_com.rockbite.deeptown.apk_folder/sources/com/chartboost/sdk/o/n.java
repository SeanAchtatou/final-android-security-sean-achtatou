package com.chartboost.sdk.o;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import com.chartboost.sdk.d.a;
import com.chartboost.sdk.d.d;
import com.chartboost.sdk.f;
import com.google.android.gms.drive.DriveFile;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.Executor;

public class n {

    /* renamed from: a  reason: collision with root package name */
    private final Executor f6103a;

    /* renamed from: b  reason: collision with root package name */
    private final k f6104b;

    /* renamed from: c  reason: collision with root package name */
    final l f6105c;

    /* renamed from: d  reason: collision with root package name */
    final Handler f6106d;

    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ String f6107a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ d f6108b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ Activity f6109c;

        /* renamed from: d  reason: collision with root package name */
        final /* synthetic */ m f6110d;

        /* renamed from: com.chartboost.sdk.o.n$a$a  reason: collision with other inner class name */
        class C0131a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ String f6112a;

            C0131a(String str) {
                this.f6112a = str;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.chartboost.sdk.o.n.a(com.chartboost.sdk.d.d, java.lang.String, android.content.Context, com.chartboost.sdk.o.m):void
             arg types: [com.chartboost.sdk.d.d, java.lang.String, android.app.Activity, com.chartboost.sdk.o.m]
             candidates:
              com.chartboost.sdk.o.n.a(com.chartboost.sdk.d.d, java.lang.String, android.app.Activity, com.chartboost.sdk.o.m):void
              com.chartboost.sdk.o.n.a(com.chartboost.sdk.d.d, java.lang.String, android.content.Context, com.chartboost.sdk.o.m):void */
            public void run() {
                try {
                    n.this.a(a.this.f6108b, this.f6112a, (Context) a.this.f6109c, a.this.f6110d);
                } catch (Exception e2) {
                    com.chartboost.sdk.e.a.a(n.class, "open openOnUiThread Runnable.run", e2);
                }
            }
        }

        a(String str, d dVar, Activity activity, m mVar) {
            this.f6107a = str;
            this.f6108b = dVar;
            this.f6109c = activity;
            this.f6110d = mVar;
        }

        private void a(String str) {
            C0131a aVar = new C0131a(str);
            Activity activity = this.f6109c;
            if (activity != null) {
                activity.runOnUiThread(aVar);
            } else {
                n.this.f6106d.post(aVar);
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:24:0x004f A[SYNTHETIC, Splitter:B:24:0x004f] */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x0055 A[Catch:{ Exception -> 0x005d }] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r6 = this;
                java.lang.String r0 = r6.f6107a     // Catch:{ Exception -> 0x005d }
                com.chartboost.sdk.o.n r1 = com.chartboost.sdk.o.n.this     // Catch:{ Exception -> 0x005d }
                com.chartboost.sdk.o.l r1 = r1.f6105c     // Catch:{ Exception -> 0x005d }
                boolean r1 = r1.c()     // Catch:{ Exception -> 0x005d }
                if (r1 == 0) goto L_0x0059
                r1 = 0
                java.net.URL r2 = new java.net.URL     // Catch:{ Exception -> 0x0045 }
                java.lang.String r3 = r6.f6107a     // Catch:{ Exception -> 0x0045 }
                r2.<init>(r3)     // Catch:{ Exception -> 0x0045 }
                java.net.URLConnection r2 = r2.openConnection()     // Catch:{ Exception -> 0x0045 }
                java.lang.Object r2 = com.google.firebase.perf.network.FirebasePerfUrlConnection.instrument(r2)     // Catch:{ Exception -> 0x0045 }
                java.net.URLConnection r2 = (java.net.URLConnection) r2     // Catch:{ Exception -> 0x0045 }
                java.net.HttpURLConnection r2 = (java.net.HttpURLConnection) r2     // Catch:{ Exception -> 0x0045 }
                r1 = 0
                r2.setInstanceFollowRedirects(r1)     // Catch:{ Exception -> 0x003e, all -> 0x003b }
                r1 = 10000(0x2710, float:1.4013E-41)
                r2.setConnectTimeout(r1)     // Catch:{ Exception -> 0x003e, all -> 0x003b }
                r2.setReadTimeout(r1)     // Catch:{ Exception -> 0x003e, all -> 0x003b }
                java.lang.String r1 = "Location"
                java.lang.String r1 = r2.getHeaderField(r1)     // Catch:{ Exception -> 0x003e, all -> 0x003b }
                if (r1 == 0) goto L_0x0035
                r0 = r1
            L_0x0035:
                if (r2 == 0) goto L_0x0059
                r2.disconnect()     // Catch:{ Exception -> 0x005d }
                goto L_0x0059
            L_0x003b:
                r0 = move-exception
                r1 = r2
                goto L_0x0053
            L_0x003e:
                r1 = move-exception
                r5 = r2
                r2 = r1
                r1 = r5
                goto L_0x0046
            L_0x0043:
                r0 = move-exception
                goto L_0x0053
            L_0x0045:
                r2 = move-exception
            L_0x0046:
                java.lang.String r3 = "CBURLOpener"
                java.lang.String r4 = "Exception raised while opening a HTTP Conection"
                com.chartboost.sdk.c.a.a(r3, r4, r2)     // Catch:{ all -> 0x0043 }
                if (r1 == 0) goto L_0x0059
                r1.disconnect()     // Catch:{ Exception -> 0x005d }
                goto L_0x0059
            L_0x0053:
                if (r1 == 0) goto L_0x0058
                r1.disconnect()     // Catch:{ Exception -> 0x005d }
            L_0x0058:
                throw r0     // Catch:{ Exception -> 0x005d }
            L_0x0059:
                r6.a(r0)     // Catch:{ Exception -> 0x005d }
                goto L_0x0065
            L_0x005d:
                r0 = move-exception
                java.lang.Class<com.chartboost.sdk.o.n> r1 = com.chartboost.sdk.o.n.class
                java.lang.String r2 = "open followTask"
                com.chartboost.sdk.e.a.a(r1, r2, r0)
            L_0x0065:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.o.n.a.run():void");
        }
    }

    public n(Executor executor, k kVar, l lVar, Handler handler) {
        this.f6103a = executor;
        this.f6104b = kVar;
        this.f6105c = lVar;
        this.f6106d = handler;
    }

    public void a(d dVar, boolean z, String str, a.b bVar, m mVar) {
        m mVar2;
        if (dVar != null) {
            dVar.z = false;
            if (dVar.b()) {
                dVar.l = 4;
            }
        }
        if (!z) {
            f fVar = com.chartboost.sdk.n.f5941d;
            if (fVar != null) {
                fVar.didFailToRecordClick(str, bVar);
            }
        } else if (dVar != null && (mVar2 = dVar.y) != null) {
            this.f6104b.a(mVar2);
        } else if (mVar != null) {
            this.f6104b.a(mVar);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.o.n.a(com.chartboost.sdk.d.d, java.lang.String, android.content.Context, com.chartboost.sdk.o.m):void
     arg types: [com.chartboost.sdk.d.d, java.lang.String, android.app.Activity, com.chartboost.sdk.o.m]
     candidates:
      com.chartboost.sdk.o.n.a(com.chartboost.sdk.d.d, java.lang.String, android.app.Activity, com.chartboost.sdk.o.m):void
      com.chartboost.sdk.o.n.a(com.chartboost.sdk.d.d, java.lang.String, android.content.Context, com.chartboost.sdk.o.m):void */
    public void a(d dVar, String str, Activity activity, m mVar) {
        try {
            String scheme = new URI(str).getScheme();
            if (scheme == null) {
                a(dVar, false, str, a.b.URI_INVALID, mVar);
            } else if (scheme.equals("http") || scheme.equals("https")) {
                this.f6103a.execute(new a(str, dVar, activity, mVar));
            } else {
                a(dVar, str, (Context) activity, mVar);
            }
        } catch (URISyntaxException unused) {
            a(dVar, false, str, a.b.URI_INVALID, mVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(d dVar, String str, Context context, m mVar) {
        if (dVar != null && dVar.b()) {
            dVar.l = 5;
        }
        if (context == null) {
            context = com.chartboost.sdk.n.n;
        }
        if (context == null) {
            a(dVar, false, str, a.b.NO_HOST_ACTIVITY, mVar);
            return;
        }
        try {
            Intent intent = new Intent("android.intent.action.VIEW");
            if (!(context instanceof Activity)) {
                intent.addFlags(DriveFile.MODE_READ_ONLY);
            }
            intent.setData(Uri.parse(str));
            context.startActivity(intent);
        } catch (Exception unused) {
            if (str.startsWith("market://")) {
                try {
                    str = "http://market.android.com/" + str.substring(9);
                    Intent intent2 = new Intent("android.intent.action.VIEW");
                    if (!(context instanceof Activity)) {
                        intent2.addFlags(DriveFile.MODE_READ_ONLY);
                    }
                    intent2.setData(Uri.parse(str));
                    context.startActivity(intent2);
                } catch (Exception e2) {
                    com.chartboost.sdk.c.a.a("CBURLOpener", "Exception raised openeing an inavld playstore URL", e2);
                    a(dVar, false, str, a.b.URI_UNRECOGNIZED, mVar);
                    return;
                }
            } else {
                a(dVar, false, str, a.b.URI_UNRECOGNIZED, mVar);
            }
        }
        a(dVar, true, str, null, mVar);
    }

    public boolean a(String str) {
        try {
            Context context = com.chartboost.sdk.n.n;
            Intent intent = new Intent("android.intent.action.VIEW");
            if (!(context instanceof Activity)) {
                intent.addFlags(DriveFile.MODE_READ_ONLY);
            }
            intent.setData(Uri.parse(str));
            if (context.getPackageManager().queryIntentActivities(intent, 65536).size() > 0) {
                return true;
            }
            return false;
        } catch (Exception e2) {
            com.chartboost.sdk.c.a.a("CBURLOpener", "Cannot open URL", e2);
            com.chartboost.sdk.e.a.a(n.class, "canOpenURL", e2);
            return false;
        }
    }

    public void a(d dVar, String str, m mVar) {
        a(dVar, str, dVar != null ? dVar.f5836g.a() : null, mVar);
    }
}
