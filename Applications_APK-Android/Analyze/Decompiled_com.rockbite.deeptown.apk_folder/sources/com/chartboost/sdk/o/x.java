package com.chartboost.sdk.o;

import android.content.Context;
import android.content.Intent;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.view.Surface;
import android.view.TextureView;
import com.chartboost.sdk.c.a;
import com.chartboost.sdk.o.y;
import com.tapjoy.TJAdUnitConstants;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;

public class x extends TextureView implements MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnVideoSizeChangedListener, TextureView.SurfaceTextureListener, y.a {

    /* renamed from: a  reason: collision with root package name */
    private Uri f6218a;

    /* renamed from: b  reason: collision with root package name */
    private int f6219b;

    /* renamed from: c  reason: collision with root package name */
    private int f6220c = 0;

    /* renamed from: d  reason: collision with root package name */
    private int f6221d = 0;

    /* renamed from: e  reason: collision with root package name */
    private Surface f6222e = null;

    /* renamed from: f  reason: collision with root package name */
    private MediaPlayer f6223f = null;

    /* renamed from: g  reason: collision with root package name */
    private int f6224g;

    /* renamed from: h  reason: collision with root package name */
    private int f6225h;

    /* renamed from: i  reason: collision with root package name */
    private MediaPlayer.OnCompletionListener f6226i;

    /* renamed from: j  reason: collision with root package name */
    private MediaPlayer.OnPreparedListener f6227j;

    /* renamed from: k  reason: collision with root package name */
    private MediaPlayer.OnErrorListener f6228k;
    private int l;

    public x(Context context) {
        super(context);
        f();
    }

    private void f() {
        this.f6224g = 0;
        this.f6225h = 0;
        setSurfaceTextureListener(this);
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus();
        this.f6220c = 0;
        this.f6221d = 0;
    }

    private void g() {
        if (this.f6218a != null && this.f6222e != null) {
            Intent intent = new Intent("com.android.music.musicservicecommand");
            intent.putExtra(TJAdUnitConstants.String.COMMAND, "pause");
            getContext().sendBroadcast(intent);
            a(false);
            h();
            try {
                this.f6223f = new MediaPlayer();
                this.f6223f.setOnPreparedListener(this);
                this.f6223f.setOnVideoSizeChangedListener(this);
                this.f6219b = -1;
                this.f6223f.setOnCompletionListener(this);
                this.f6223f.setOnErrorListener(this);
                this.f6223f.setOnBufferingUpdateListener(this);
                FileInputStream fileInputStream = new FileInputStream(new File(this.f6218a.toString()));
                this.f6223f.setDataSource(fileInputStream.getFD());
                fileInputStream.close();
                this.f6223f.setSurface(this.f6222e);
                this.f6223f.setAudioStreamType(3);
                this.f6223f.setScreenOnWhilePlaying(true);
                this.f6223f.prepareAsync();
                this.f6220c = 1;
            } catch (IOException e2) {
                a.c("VideoTextureView", "Unable to open content: " + this.f6218a, e2);
                this.f6220c = -1;
                this.f6221d = -1;
                onError(this.f6223f, 1, 0);
            } catch (IllegalArgumentException e3) {
                a.c("VideoTextureView", "Unable to open content: " + this.f6218a, e3);
                this.f6220c = -1;
                this.f6221d = -1;
                onError(this.f6223f, 1, 0);
            }
        }
    }

    private void h() {
        try {
            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
            if (f1.e().a(16)) {
                mediaMetadataRetriever.setDataSource(this.f6218a.toString());
            } else {
                FileInputStream fileInputStream = new FileInputStream(this.f6218a.toString());
                mediaMetadataRetriever.setDataSource(fileInputStream.getFD());
                fileInputStream.close();
            }
            String extractMetadata = mediaMetadataRetriever.extractMetadata(19);
            String extractMetadata2 = mediaMetadataRetriever.extractMetadata(18);
            this.f6225h = Integer.parseInt(extractMetadata);
            this.f6224g = Integer.parseInt(extractMetadata2);
        } catch (Exception e2) {
            a.c("play video", "read size error", e2);
        }
    }

    private boolean i() {
        int i2;
        return (this.f6223f == null || (i2 = this.f6220c) == -1 || i2 == 0 || i2 == 1) ? false : true;
    }

    public void a(Uri uri) {
        a(uri, (Map<String, String>) null);
    }

    public void b() {
        if (i() && this.f6223f.isPlaying()) {
            this.f6223f.pause();
            this.f6220c = 4;
        }
        this.f6221d = 4;
    }

    public int c() {
        if (i()) {
            int i2 = this.f6219b;
            if (i2 > 0) {
                return i2;
            }
            this.f6219b = this.f6223f.getDuration();
            return this.f6219b;
        }
        this.f6219b = -1;
        return this.f6219b;
    }

    public int d() {
        if (i()) {
            return this.f6223f.getCurrentPosition();
        }
        return 0;
    }

    public boolean e() {
        return i() && this.f6223f.isPlaying();
    }

    public void onBufferingUpdate(MediaPlayer mediaPlayer, int i2) {
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        this.f6221d = 5;
        if (this.f6220c != 5) {
            this.f6220c = 5;
            MediaPlayer.OnCompletionListener onCompletionListener = this.f6226i;
            if (onCompletionListener != null) {
                onCompletionListener.onCompletion(this.f6223f);
            }
        }
    }

    public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        a.a("VideoTextureView", "Error: " + i2 + "," + i3);
        if (i2 == 100) {
            g();
            return true;
        }
        this.f6220c = -1;
        this.f6221d = -1;
        MediaPlayer.OnErrorListener onErrorListener = this.f6228k;
        if (onErrorListener == null || onErrorListener.onError(this.f6223f, i2, i3)) {
        }
        return true;
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        this.f6220c = 2;
        this.f6224g = mediaPlayer.getVideoWidth();
        this.f6225h = mediaPlayer.getVideoHeight();
        MediaPlayer.OnPreparedListener onPreparedListener = this.f6227j;
        if (onPreparedListener != null) {
            onPreparedListener.onPrepared(this.f6223f);
        }
        int i2 = this.l;
        if (i2 != 0) {
            a(i2);
        }
        if (this.f6221d == 3) {
            a();
        }
    }

    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i2, int i3) {
        this.f6222e = new Surface(surfaceTexture);
        g();
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        this.f6222e = null;
        a(true);
        return true;
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i2, int i3) {
        boolean z = this.f6221d == 3;
        if (this.f6223f != null && z) {
            int i4 = this.l;
            if (i4 != 0) {
                a(i4);
            }
            a();
        }
    }

    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }

    public void onVideoSizeChanged(MediaPlayer mediaPlayer, int i2, int i3) {
        this.f6224g = mediaPlayer.getVideoWidth();
        this.f6225h = mediaPlayer.getVideoHeight();
        if (this.f6224g != 0 && this.f6225h != 0) {
            a(getWidth(), getHeight());
        }
    }

    public void a(int i2, int i3) {
        int i4;
        int i5 = this.f6224g;
        if (i5 != 0 && (i4 = this.f6225h) != 0 && i2 != 0 && i3 != 0) {
            float f2 = (float) i2;
            float f3 = (float) i3;
            float min = Math.min(f2 / ((float) i5), f3 / ((float) i4));
            float f4 = ((float) this.f6224g) * min;
            float f5 = min * ((float) this.f6225h);
            Matrix matrix = new Matrix();
            matrix.setScale(f4 / f2, f5 / f3, f2 / 2.0f, f3 / 2.0f);
            setTransform(matrix);
        }
    }

    public void a(Uri uri, Map<String, String> map) {
        this.f6218a = uri;
        this.l = 0;
        g();
        requestLayout();
        invalidate();
    }

    public void a(MediaPlayer.OnPreparedListener onPreparedListener) {
        this.f6227j = onPreparedListener;
    }

    public void a(MediaPlayer.OnCompletionListener onCompletionListener) {
        this.f6226i = onCompletionListener;
    }

    public void a(MediaPlayer.OnErrorListener onErrorListener) {
        this.f6228k = onErrorListener;
    }

    private void a(boolean z) {
        MediaPlayer mediaPlayer = this.f6223f;
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            this.f6223f.release();
            this.f6223f = null;
            this.f6220c = 0;
            if (z) {
                this.f6221d = 0;
            }
        }
    }

    public void a() {
        if (i()) {
            this.f6223f.start();
            this.f6220c = 3;
        }
        this.f6221d = 3;
    }

    public void a(int i2) {
        if (i()) {
            this.f6223f.seekTo(i2);
            this.l = 0;
            return;
        }
        this.l = i2;
    }
}
