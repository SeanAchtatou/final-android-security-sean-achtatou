package com.chartboost.sdk.o;

import com.chartboost.sdk.c.b;
import com.chartboost.sdk.d.a;
import com.chartboost.sdk.n;
import com.google.firebase.perf.FirebasePerformance;
import java.io.File;
import java.util.HashMap;

class x0 extends g<Void> {

    /* renamed from: j  reason: collision with root package name */
    private final y0 f6229j;

    /* renamed from: k  reason: collision with root package name */
    private final l f6230k;
    final w0 l;

    x0(y0 y0Var, l lVar, w0 w0Var, File file) {
        super(FirebasePerformance.HttpMethod.GET, w0Var.f6212d, 2, file);
        this.f6018i = 1;
        this.f6229j = y0Var;
        this.f6230k = lVar;
        this.l = w0Var;
    }

    public h a() {
        HashMap hashMap = new HashMap();
        hashMap.put("X-Chartboost-App", n.l);
        hashMap.put("X-Chartboost-Client", b.b());
        hashMap.put("X-Chartboost-Reachability", Integer.toString(this.f6230k.a()));
        return new h(hashMap, null, null);
    }

    public void a(Void voidR, j jVar) {
        this.f6229j.a(this, null, null);
    }

    public void a(a aVar, j jVar) {
        this.f6229j.a(this, aVar, jVar);
    }
}
