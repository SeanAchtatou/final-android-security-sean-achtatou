package com.chartboost.sdk.o;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.chartboost.sdk.c.j;

public abstract class d0 extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    final c f5972a;

    /* renamed from: b  reason: collision with root package name */
    private final Rect f5973b;

    /* renamed from: c  reason: collision with root package name */
    protected Button f5974c;

    /* renamed from: d  reason: collision with root package name */
    boolean f5975d;

    class a implements View.OnTouchListener {
        a() {
        }

        public boolean onTouch(View view, MotionEvent motionEvent) {
            boolean a2 = d0.this.a(view, motionEvent);
            int actionMasked = motionEvent.getActionMasked();
            if (actionMasked != 0) {
                if (actionMasked == 1) {
                    if (d0.this.getVisibility() == 0 && d0.this.isEnabled() && a2) {
                        d0.this.a(motionEvent);
                    }
                    d0.this.f5972a.a(false);
                } else if (actionMasked == 2) {
                    d0.this.f5972a.a(a2);
                } else if (actionMasked == 3 || actionMasked == 4) {
                    d0.this.f5972a.a(false);
                }
                return true;
            }
            d0.this.f5972a.a(a2);
            return a2;
        }
    }

    class b implements View.OnClickListener {
        b() {
        }

        public void onClick(View view) {
            d0.this.a((MotionEvent) null);
        }
    }

    private class c extends c0 {

        /* renamed from: c  reason: collision with root package name */
        private boolean f5978c;

        public c(Context context) {
            super(context);
            this.f5978c = false;
            this.f5978c = false;
        }

        /* access modifiers changed from: protected */
        public void a(boolean z) {
            if (!d0.this.f5975d || !z) {
                if (this.f5978c) {
                    if (getDrawable() != null) {
                        getDrawable().clearColorFilter();
                    } else if (getBackground() != null) {
                        getBackground().clearColorFilter();
                    }
                    invalidate();
                    this.f5978c = false;
                }
            } else if (!this.f5978c) {
                if (getDrawable() != null) {
                    getDrawable().setColorFilter(1996488704, PorterDuff.Mode.SRC_ATOP);
                } else if (getBackground() != null) {
                    getBackground().setColorFilter(1996488704, PorterDuff.Mode.SRC_ATOP);
                }
                invalidate();
                this.f5978c = true;
            }
        }

        public boolean performClick() {
            super.performClick();
            return true;
        }
    }

    public d0(Context context) {
        this(context, null);
    }

    /* access modifiers changed from: protected */
    public abstract void a(MotionEvent motionEvent);

    /* access modifiers changed from: package-private */
    public boolean a(View view, MotionEvent motionEvent) {
        view.getLocalVisibleRect(this.f5973b);
        this.f5973b.left += view.getPaddingLeft();
        this.f5973b.top += view.getPaddingTop();
        this.f5973b.right -= view.getPaddingRight();
        this.f5973b.bottom -= view.getPaddingBottom();
        return this.f5973b.contains(Math.round(motionEvent.getX()), Math.round(motionEvent.getY()));
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public d0(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f5973b = new Rect();
        this.f5974c = null;
        this.f5975d = true;
        this.f5972a = new c(getContext());
        this.f5972a.setOnTouchListener(new a());
        addView(this.f5972a, new RelativeLayout.LayoutParams(-1, -1));
    }

    public void a(String str) {
        if (str != null) {
            a().setText(str);
            addView(a(), new RelativeLayout.LayoutParams(-1, -1));
            this.f5972a.setVisibility(8);
            a(false);
            this.f5974c.setOnClickListener(new b());
        } else if (this.f5974c != null) {
            removeView(a());
            this.f5974c = null;
            this.f5972a.setVisibility(0);
            a(true);
        }
    }

    public TextView a() {
        if (this.f5974c == null) {
            this.f5974c = new Button(getContext());
            this.f5974c.setGravity(17);
        }
        this.f5974c.postInvalidate();
        return this.f5974c;
    }

    public void a(j jVar) {
        if (jVar != null && jVar.c()) {
            this.f5972a.a(jVar);
            a((String) null);
        }
    }

    public void a(ImageView.ScaleType scaleType) {
        this.f5972a.setScaleType(scaleType);
    }

    public void a(boolean z) {
        this.f5975d = z;
    }
}
