package com.chartboost.sdk.o;

import android.content.Context;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;
import com.chartboost.sdk.c.b;
import com.esotericsoftware.spine.Animation;

public abstract class c extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    protected j1 f5963a;

    /* renamed from: b  reason: collision with root package name */
    private d f5964b;

    /* renamed from: c  reason: collision with root package name */
    private int f5965c = 1;

    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ boolean f5966a;

        a(boolean z) {
            this.f5966a = z;
        }

        public void run() {
            if (!this.f5966a) {
                c.this.setVisibility(8);
                c.this.clearAnimation();
            }
            synchronized (c.this.f5963a.f5894i) {
                c.this.f5963a.f5894i.remove(c.this);
            }
        }
    }

    public c(Context context, j1 j1Var) {
        super(context);
        this.f5963a = j1Var;
        a(context);
    }

    /* access modifiers changed from: protected */
    public abstract View a();

    public void a(int i2) {
        RelativeLayout.LayoutParams layoutParams;
        this.f5965c = i2;
        setClickable(false);
        int b2 = b();
        int i3 = this.f5965c;
        if (i3 == 0) {
            layoutParams = new RelativeLayout.LayoutParams(-1, b.a(b2, getContext()));
            layoutParams.addRule(10);
            this.f5964b.b(1);
        } else if (i3 == 1) {
            layoutParams = new RelativeLayout.LayoutParams(-1, b.a(b2, getContext()));
            layoutParams.addRule(12);
            this.f5964b.b(4);
        } else if (i3 == 2) {
            layoutParams = new RelativeLayout.LayoutParams(b.a(b2, getContext()), -1);
            layoutParams.addRule(9);
            this.f5964b.b(8);
        } else if (i3 != 3) {
            layoutParams = null;
        } else {
            layoutParams = new RelativeLayout.LayoutParams(b.a(b2, getContext()), -1);
            layoutParams.addRule(11);
            this.f5964b.b(2);
        }
        setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: protected */
    public abstract int b();

    private void a(Context context) {
        Context context2 = getContext();
        setGravity(17);
        this.f5964b = new d(context2);
        this.f5964b.a(-1);
        this.f5964b.setBackgroundColor(-855638017);
        addView(this.f5964b, new RelativeLayout.LayoutParams(-1, -1));
        addView(a(), new RelativeLayout.LayoutParams(-1, -1));
    }

    public void a(boolean z) {
        a(z, 500);
    }

    private void a(boolean z, long j2) {
        this.f5963a.H = z;
        if (z && getVisibility() == 0) {
            return;
        }
        if (z || getVisibility() != 8) {
            a aVar = new a(z);
            if (z) {
                setVisibility(0);
            }
            float a2 = b.a((float) b(), getContext());
            TranslateAnimation translateAnimation = null;
            int i2 = this.f5965c;
            if (i2 == 0) {
                translateAnimation = new TranslateAnimation(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, z ? -a2 : Animation.CurveTimeline.LINEAR, z ? Animation.CurveTimeline.LINEAR : -a2);
            } else if (i2 == 1) {
                float f2 = z ? a2 : Animation.CurveTimeline.LINEAR;
                if (z) {
                    a2 = Animation.CurveTimeline.LINEAR;
                }
                translateAnimation = new TranslateAnimation(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, f2, a2);
            } else if (i2 == 2) {
                translateAnimation = new TranslateAnimation(z ? -a2 : Animation.CurveTimeline.LINEAR, z ? Animation.CurveTimeline.LINEAR : -a2, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
            } else if (i2 == 3) {
                float f3 = z ? a2 : Animation.CurveTimeline.LINEAR;
                if (z) {
                    a2 = Animation.CurveTimeline.LINEAR;
                }
                translateAnimation = new TranslateAnimation(f3, a2, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
            }
            translateAnimation.setDuration(j2);
            translateAnimation.setFillAfter(!z);
            startAnimation(translateAnimation);
            synchronized (this.f5963a.f5894i) {
                this.f5963a.f5894i.put(this, aVar);
            }
            this.f5963a.f5886a.postDelayed(aVar, j2);
        }
    }
}
