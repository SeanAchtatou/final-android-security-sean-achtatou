package com.chartboost.sdk.o;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import com.tapjoy.TapjoyConstants;
import java.util.UUID;

public class u {
    public static String a(Context context) {
        PackageManager packageManager = context.getPackageManager();
        int i2 = context.getResources().getConfiguration().uiMode & 15;
        int i3 = context.getResources().getConfiguration().screenLayout & 15;
        if (packageManager.hasSystemFeature("org.chromium.arc.device_management")) {
            return "chromebook";
        }
        String str = Build.BRAND;
        if (str != null && str.equals("chromium") && Build.MANUFACTURER.equals("chromium")) {
            return "chromebook";
        }
        String str2 = Build.DEVICE;
        if (str2 != null && str2.matches(".+_cheets")) {
            return "chromebook";
        }
        if (packageManager.hasSystemFeature("android.hardware.type.watch") || i2 == 6) {
            return "watch";
        }
        if (packageManager.hasSystemFeature("android.hardware.type.television") || i2 == 4) {
            return "tv";
        }
        String str3 = Build.MANUFACTURER;
        return ((str3 == null || !str3.equalsIgnoreCase("Amazon")) && i3 != 4) ? "phone" : "tablet";
    }

    public static int b(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager != null) {
            return telephonyManager.getNetworkType();
        }
        return 0;
    }

    public static String c(Context context) {
        String str = null;
        try {
            Object obj = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.get("cb.limit.aid");
            if ((obj instanceof Integer) && ((Integer) obj).intValue() == 1) {
                return null;
            }
        } catch (Exception unused) {
        }
        String string = Settings.Secure.getString(context.getContentResolver(), TapjoyConstants.TJC_ANDROID_ID);
        if (!"9774d56d682e549c".equals(string)) {
            str = string;
        }
        return str == null ? d(context) : str;
    }

    private static String d(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("cbPrefs", 0);
        if (sharedPreferences == null) {
            return UUID.randomUUID().toString();
        }
        String string = sharedPreferences.getString("cbUUID", null);
        if (string != null) {
            return string;
        }
        String uuid = UUID.randomUUID().toString();
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("cbUUID", uuid);
        edit.apply();
        return uuid;
    }
}
