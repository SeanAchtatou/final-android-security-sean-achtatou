package com.chartboost.sdk.o;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import com.chartboost.sdk.n;

public class l {

    /* renamed from: a  reason: collision with root package name */
    int f6081a = 1;

    /* renamed from: b  reason: collision with root package name */
    private boolean f6082b = false;

    /* renamed from: c  reason: collision with root package name */
    private final b f6083c = new b();

    /* renamed from: d  reason: collision with root package name */
    private a f6084d;

    class a extends ConnectivityManager.NetworkCallback {
        a() {
        }

        public void onAvailable(Network network) {
            l.this.a(network);
        }

        public void onCapabilitiesChanged(Network network, NetworkCapabilities networkCapabilities) {
            l.this.a(networkCapabilities);
        }

        public void onLost(Network network) {
            l.this.f6081a = 0;
            com.chartboost.sdk.c.a.a("CBReachability", "NETWORK TYPE: NO Network");
        }
    }

    class b extends BroadcastReceiver {
        b() {
        }

        public void onReceive(Context context, Intent intent) {
            if (intent.getBooleanExtra("noConnectivity", false)) {
                l.this.f6081a = 0;
                com.chartboost.sdk.c.a.a("CBReachability", "NETWORK TYPE: NO Network");
            } else if (Build.VERSION.SDK_INT >= 23) {
                l.this.a((Network) null);
            } else {
                l.this.b();
            }
        }
    }

    public int a() {
        return this.f6081a;
    }

    public void b() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) n.n.getSystemService("connectivity");
            if (connectivityManager == null) {
                this.f6081a = -1;
                com.chartboost.sdk.c.a.a("CBReachability", "NETWORK TYPE: unknown");
                return;
            }
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
                this.f6081a = 0;
                com.chartboost.sdk.c.a.a("CBReachability", "NETWORK TYPE: NO Network");
            } else if (activeNetworkInfo.getType() == 1) {
                this.f6081a = 1;
                com.chartboost.sdk.c.a.a("CBReachability", "NETWORK TYPE: TYPE_WIFI");
            } else {
                this.f6081a = 2;
                com.chartboost.sdk.c.a.a("CBReachability", "NETWORK TYPE: TYPE_MOBILE");
            }
        } catch (SecurityException unused) {
            this.f6081a = -1;
            com.chartboost.sdk.c.a.b("CBReachability", "Chartboost SDK requires 'android.permission.ACCESS_NETWORK_STATE' permission set in your AndroidManifest.xml");
        }
    }

    public boolean c() {
        int i2 = this.f6081a;
        return (i2 == -1 || i2 == 0) ? false : true;
    }

    public void d() {
        if (this.f6082b) {
            return;
        }
        if (Build.VERSION.SDK_INT > 23) {
            ConnectivityManager connectivityManager = (ConnectivityManager) n.n.getSystemService("connectivity");
            if (connectivityManager != null) {
                this.f6084d = new a();
                connectivityManager.registerDefaultNetworkCallback(this.f6084d);
                this.f6082b = true;
                com.chartboost.sdk.c.a.a("CBReachability", "Network broadcast receiver successfully registered");
            }
        } else if (n.n.registerReceiver(this.f6083c, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE")) != null) {
            this.f6082b = true;
            com.chartboost.sdk.c.a.a("CBReachability", "Network broadcast receiver successfully registered");
        } else {
            com.chartboost.sdk.c.a.b("CBReachability", "Network broadcast receiver could not be registered");
        }
    }

    public void e() {
        if (!this.f6082b) {
            return;
        }
        if (Build.VERSION.SDK_INT <= 23) {
            n.n.unregisterReceiver(this.f6083c);
            this.f6082b = false;
            com.chartboost.sdk.c.a.a("CBReachability", "Network broadcast successfully unregistered");
            return;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) n.n.getSystemService("connectivity");
        if (connectivityManager != null) {
            connectivityManager.unregisterNetworkCallback(this.f6084d);
            this.f6082b = false;
            com.chartboost.sdk.c.a.a("CBReachability", "Network broadcast successfully unregistered");
        }
    }

    /* access modifiers changed from: package-private */
    public void a(NetworkCapabilities networkCapabilities) {
        if (networkCapabilities == null || networkCapabilities.getLinkDownstreamBandwidthKbps() <= 6000) {
            this.f6081a = 2;
            com.chartboost.sdk.c.a.a("CBReachability", "NETWORK TYPE: low speed");
            return;
        }
        this.f6081a = 1;
        com.chartboost.sdk.c.a.a("CBReachability", "NETWORK TYPE: high speed");
    }

    /* access modifiers changed from: package-private */
    public void a(Network network) {
        ConnectivityManager connectivityManager = (ConnectivityManager) n.n.getSystemService("connectivity");
        if (connectivityManager != null) {
            if (network == null) {
                network = connectivityManager.getActiveNetwork();
            }
            a(connectivityManager.getNetworkCapabilities(network));
        }
    }
}
