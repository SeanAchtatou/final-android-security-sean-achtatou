package com.chartboost.sdk.o;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.chartboost.sdk.c.b;
import com.chartboost.sdk.c.j;

@SuppressLint({"ViewConstructor"})
public class k1 extends c {

    /* renamed from: d  reason: collision with root package name */
    private LinearLayout f6078d;

    /* renamed from: e  reason: collision with root package name */
    private c0 f6079e;

    /* renamed from: f  reason: collision with root package name */
    private TextView f6080f;

    public k1(Context context, j1 j1Var) {
        super(context, j1Var);
    }

    /* access modifiers changed from: protected */
    public View a() {
        Context context = getContext();
        int round = Math.round(getContext().getResources().getDisplayMetrics().density * 8.0f);
        this.f6078d = new LinearLayout(context);
        this.f6078d.setOrientation(0);
        this.f6078d.setGravity(17);
        int a2 = b.a(36, context);
        this.f6079e = new c0(context);
        this.f6079e.setPadding(round, round, round, round);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(a2, a2);
        this.f6079e.setScaleType(ImageView.ScaleType.FIT_CENTER);
        this.f6080f = new TextView(context);
        this.f6080f.setPadding(round / 2, round, round, round);
        this.f6080f.setTextColor(-15264491);
        this.f6080f.setTextSize(2, 16.0f);
        this.f6080f.setTypeface(null, 1);
        this.f6080f.setGravity(17);
        this.f6078d.addView(this.f6079e, layoutParams);
        this.f6078d.addView(this.f6080f, new LinearLayout.LayoutParams(-2, -1));
        return this.f6078d;
    }

    /* access modifiers changed from: protected */
    public int b() {
        return 48;
    }

    public void a(j jVar) {
        this.f6079e.a(jVar);
        this.f6079e.setScaleType(ImageView.ScaleType.FIT_CENTER);
    }

    public void a(String str) {
        this.f6080f.setText(str);
    }
}
