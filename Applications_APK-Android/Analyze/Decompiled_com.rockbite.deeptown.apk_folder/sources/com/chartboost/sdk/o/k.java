package com.chartboost.sdk.o;

import android.os.Handler;
import java.util.concurrent.Executor;

public class k {

    /* renamed from: a  reason: collision with root package name */
    private final Executor f6067a;

    /* renamed from: b  reason: collision with root package name */
    private final Executor f6068b;

    /* renamed from: c  reason: collision with root package name */
    private final r f6069c;

    /* renamed from: d  reason: collision with root package name */
    private final l f6070d;

    /* renamed from: e  reason: collision with root package name */
    private final com.chartboost.sdk.c.k f6071e;

    /* renamed from: f  reason: collision with root package name */
    private final Handler f6072f;

    public k(Executor executor, r rVar, l lVar, com.chartboost.sdk.c.k kVar, Handler handler, Executor executor2) {
        this.f6067a = executor2;
        this.f6068b = executor;
        this.f6069c = rVar;
        this.f6070d = lVar;
        this.f6071e = kVar;
        this.f6072f = handler;
    }

    public <T> void a(g<T> gVar) {
        this.f6067a.execute(new q(this.f6068b, this.f6069c, this.f6070d, this.f6071e, this.f6072f, gVar));
    }
}
