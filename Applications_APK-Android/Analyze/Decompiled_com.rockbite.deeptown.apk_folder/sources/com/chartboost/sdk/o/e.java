package com.chartboost.sdk.o;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.chartboost.sdk.c.g;
import com.chartboost.sdk.c.j;
import com.chartboost.sdk.l;
import com.chartboost.sdk.o.j1;
import com.chartboost.sdk.o.y;
import com.esotericsoftware.spine.Animation;
import java.util.Locale;
import org.json.JSONObject;

@SuppressLint({"ViewConstructor"})
public class e extends RelativeLayout implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener {
    private static final CharSequence o = "00:00";

    /* renamed from: a  reason: collision with root package name */
    final RelativeLayout f5980a;

    /* renamed from: b  reason: collision with root package name */
    final d f5981b;

    /* renamed from: c  reason: collision with root package name */
    final d f5982c;

    /* renamed from: d  reason: collision with root package name */
    final d0 f5983d;

    /* renamed from: e  reason: collision with root package name */
    final TextView f5984e;

    /* renamed from: f  reason: collision with root package name */
    final l1 f5985f;

    /* renamed from: g  reason: collision with root package name */
    final y f5986g;

    /* renamed from: h  reason: collision with root package name */
    final j1 f5987h;

    /* renamed from: i  reason: collision with root package name */
    private boolean f5988i = false;

    /* renamed from: j  reason: collision with root package name */
    private boolean f5989j = false;

    /* renamed from: k  reason: collision with root package name */
    final Handler f5990k;
    private final Runnable l = new b();
    private final Runnable m = new c();
    final Runnable n = new d();

    class a extends d0 {
        a(Context context) {
            super(context);
        }

        /* access modifiers changed from: protected */
        public void a(MotionEvent motionEvent) {
            e.this.f5987h.b(g.a(g.a("x", Float.valueOf(motionEvent.getX())), g.a("y", Float.valueOf(motionEvent.getY())), g.a("w", Integer.valueOf(e.this.f5983d.getWidth())), g.a("h", Integer.valueOf(e.this.f5983d.getHeight()))));
        }
    }

    class b implements Runnable {
        b() {
        }

        public void run() {
            e.this.a(false);
        }
    }

    class c implements Runnable {
        c() {
        }

        public void run() {
            d dVar = e.this.f5981b;
            if (dVar != null) {
                dVar.setVisibility(8);
            }
            e eVar = e.this;
            if (eVar.f5987h.R) {
                eVar.f5985f.setVisibility(8);
            }
            e.this.f5982c.setVisibility(8);
            d0 d0Var = e.this.f5983d;
            if (d0Var != null) {
                d0Var.setEnabled(false);
            }
        }
    }

    class d implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private int f5994a = 0;

        d() {
        }

        public void run() {
            j1.b q = e.this.f5987h.e();
            if (q != null) {
                if (e.this.f5986g.a().e()) {
                    int d2 = e.this.f5986g.a().d();
                    if (d2 > 0) {
                        j1 j1Var = e.this.f5987h;
                        j1Var.y = d2;
                        if (((float) j1Var.y) / 1000.0f > Animation.CurveTimeline.LINEAR && !j1Var.t()) {
                            e.this.f5987h.r();
                            e.this.f5987h.a(true);
                        }
                    }
                    float c2 = ((float) d2) / ((float) e.this.f5986g.a().c());
                    e eVar = e.this;
                    if (eVar.f5987h.R) {
                        eVar.f5985f.a(c2);
                    }
                    int i2 = d2 / 1000;
                    if (this.f5994a != i2) {
                        this.f5994a = i2;
                        e.this.f5984e.setText(String.format(Locale.US, "%02d:%02d", Integer.valueOf(i2 / 60), Integer.valueOf(i2 % 60)));
                    }
                }
                if (q.f()) {
                    d0 d3 = q.d(true);
                    if (d3.getVisibility() == 8) {
                        e.this.f5987h.a(true, d3);
                        d3.setEnabled(true);
                    }
                }
                e eVar2 = e.this;
                eVar2.f5990k.removeCallbacks(eVar2.n);
                e eVar3 = e.this;
                eVar3.f5990k.postDelayed(eVar3.n, 16);
            }
        }
    }

    /* renamed from: com.chartboost.sdk.o.e$e  reason: collision with other inner class name */
    class C0128e implements Runnable {
        C0128e() {
        }

        public void run() {
            e.this.f5986g.setVisibility(0);
        }
    }

    public e(Context context, j1 j1Var) {
        super(context);
        this.f5987h = j1Var;
        this.f5990k = j1Var.f5886a;
        JSONObject g2 = j1Var.g();
        float f2 = context.getResources().getDisplayMetrics().density;
        float f3 = 10.0f * f2;
        int round = Math.round(f3);
        l a2 = l.a();
        y yVar = new y(context);
        a2.a(yVar);
        this.f5986g = yVar;
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(13);
        addView(this.f5986g, layoutParams);
        RelativeLayout relativeLayout = new RelativeLayout(context);
        a2.a(relativeLayout);
        this.f5980a = relativeLayout;
        if (g2 == null || g2.isNull("video-click-button")) {
            this.f5981b = null;
            this.f5983d = null;
        } else {
            d dVar = new d(context);
            a2.a(dVar);
            this.f5981b = dVar;
            this.f5981b.setVisibility(8);
            this.f5983d = new a(context);
            this.f5983d.a(ImageView.ScaleType.FIT_CENTER);
            j jVar = j1Var.N;
            Point b2 = j1Var.b("video-click-button");
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
            layoutParams2.leftMargin = Math.round(((float) b2.x) / jVar.e());
            layoutParams2.topMargin = Math.round(((float) b2.y) / jVar.e());
            j1Var.a(layoutParams2, jVar, 1.0f);
            this.f5983d.a(jVar);
            this.f5981b.addView(this.f5983d, layoutParams2);
            RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, Math.round(((float) layoutParams2.height) + f3));
            layoutParams3.addRule(10);
            this.f5980a.addView(this.f5981b, layoutParams3);
        }
        d dVar2 = new d(context);
        a2.a(dVar2);
        this.f5982c = dVar2;
        this.f5982c.setVisibility(8);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-1, Math.round(f2 * 32.5f));
        layoutParams4.addRule(12);
        this.f5980a.addView(this.f5982c, layoutParams4);
        this.f5982c.setGravity(16);
        this.f5982c.setPadding(round, round, round, round);
        TextView textView = new TextView(context);
        a2.a(textView);
        this.f5984e = textView;
        this.f5984e.setTextColor(-1);
        this.f5984e.setTextSize(2, 11.0f);
        this.f5984e.setText(o);
        this.f5984e.setPadding(0, 0, round, 0);
        this.f5984e.setSingleLine();
        this.f5984e.measure(0, 0);
        int measuredWidth = this.f5984e.getMeasuredWidth();
        this.f5984e.setGravity(17);
        this.f5982c.addView(this.f5984e, new LinearLayout.LayoutParams(measuredWidth, -1));
        l1 l1Var = new l1(context);
        a2.a(l1Var);
        this.f5985f = l1Var;
        this.f5985f.setVisibility(8);
        LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(-1, Math.round(f3));
        layoutParams5.setMargins(0, com.chartboost.sdk.c.b.a(1, context), 0, 0);
        this.f5982c.addView(this.f5985f, layoutParams5);
        RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams6.addRule(6, this.f5986g.getId());
        layoutParams6.addRule(8, this.f5986g.getId());
        layoutParams6.addRule(5, this.f5986g.getId());
        layoutParams6.addRule(7, this.f5986g.getId());
        addView(this.f5980a, layoutParams6);
        a();
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        a(!this.f5988i, z);
    }

    public void b(boolean z) {
        d dVar;
        this.f5990k.removeCallbacks(this.l);
        this.f5990k.removeCallbacks(this.m);
        if (z) {
            if (!this.f5989j && (dVar = this.f5981b) != null) {
                dVar.setVisibility(0);
            }
            if (this.f5987h.R) {
                this.f5985f.setVisibility(0);
            }
            this.f5982c.setVisibility(0);
            d0 d0Var = this.f5983d;
            if (d0Var != null) {
                d0Var.setEnabled(true);
            }
        } else {
            d dVar2 = this.f5981b;
            if (dVar2 != null) {
                dVar2.clearAnimation();
                this.f5981b.setVisibility(8);
            }
            this.f5982c.clearAnimation();
            if (this.f5987h.R) {
                this.f5985f.setVisibility(8);
            }
            this.f5982c.setVisibility(8);
            d0 d0Var2 = this.f5983d;
            if (d0Var2 != null) {
                d0Var2.setEnabled(false);
            }
        }
        this.f5988i = z;
    }

    public void c(boolean z) {
        setBackgroundColor(z ? -16777216 : 0);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        if (!z) {
            layoutParams.addRule(6, this.f5986g.getId());
            layoutParams.addRule(8, this.f5986g.getId());
            layoutParams.addRule(5, this.f5986g.getId());
            layoutParams.addRule(7, this.f5986g.getId());
        }
        this.f5980a.setLayoutParams(layoutParams);
        d dVar = this.f5981b;
        if (dVar != null) {
            dVar.setGravity(8388627);
            this.f5981b.requestLayout();
        }
    }

    public void d() {
        d dVar = this.f5981b;
        if (dVar != null) {
            dVar.setVisibility(8);
        }
        this.f5989j = true;
        d0 d0Var = this.f5983d;
        if (d0Var != null) {
            d0Var.setEnabled(false);
        }
    }

    public void e() {
        this.f5990k.postDelayed(new C0128e(), 500);
        this.f5986g.a().a();
        this.f5990k.removeCallbacks(this.n);
        this.f5990k.postDelayed(this.n, 16);
    }

    public void f() {
        if (this.f5986g.a().e()) {
            this.f5987h.y = this.f5986g.a().d();
            this.f5986g.a().b();
        }
        if (this.f5987h.e().f6047k.getVisibility() == 0) {
            this.f5987h.e().f6047k.postInvalidate();
        }
        this.f5990k.removeCallbacks(this.n);
    }

    public void g() {
        if (this.f5986g.a().e()) {
            this.f5987h.y = this.f5986g.a().d();
        }
        this.f5986g.a().b();
        this.f5990k.removeCallbacks(this.n);
    }

    public void h() {
        this.f5986g.setVisibility(8);
        invalidate();
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        this.f5987h.y = this.f5986g.a().c();
        if (this.f5987h.e() != null) {
            this.f5987h.e().e();
        }
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.f5990k.removeCallbacks(this.n);
    }

    public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        this.f5987h.v();
        return false;
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        this.f5987h.z = this.f5986g.a().c();
        this.f5987h.e().a(true);
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.f5986g.a().e() || motionEvent.getActionMasked() != 0) {
            return false;
        }
        if (this.f5987h != null) {
            a(true);
        }
        return true;
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        d0 d0Var = this.f5983d;
        if (d0Var != null) {
            d0Var.setEnabled(z);
        }
        if (z) {
            b(false);
        }
    }

    /* access modifiers changed from: protected */
    public void a(boolean z, boolean z2) {
        d dVar;
        this.f5990k.removeCallbacks(this.l);
        this.f5990k.removeCallbacks(this.m);
        j1 j1Var = this.f5987h;
        if (j1Var.D && j1Var.p() && z != this.f5988i) {
            this.f5988i = z;
            AlphaAnimation alphaAnimation = this.f5988i ? new AlphaAnimation((float) Animation.CurveTimeline.LINEAR, 1.0f) : new AlphaAnimation(1.0f, (float) Animation.CurveTimeline.LINEAR);
            alphaAnimation.setDuration(z2 ? 100 : 200);
            alphaAnimation.setFillAfter(true);
            if (!this.f5989j && (dVar = this.f5981b) != null) {
                dVar.setVisibility(0);
                this.f5981b.startAnimation(alphaAnimation);
                d0 d0Var = this.f5983d;
                if (d0Var != null) {
                    d0Var.setEnabled(true);
                }
            }
            if (this.f5987h.R) {
                this.f5985f.setVisibility(0);
            }
            this.f5982c.setVisibility(0);
            this.f5982c.startAnimation(alphaAnimation);
            if (this.f5988i) {
                this.f5990k.postDelayed(this.l, 3000);
            } else {
                this.f5990k.postDelayed(this.m, alphaAnimation.getDuration());
            }
        }
    }

    public void d(boolean z) {
        this.f5984e.setVisibility(z ? 0 : 8);
    }

    public l1 c() {
        return this.f5985f;
    }

    public y.a b() {
        return this.f5986g.a();
    }

    public void a() {
        c(com.chartboost.sdk.c.b.a(com.chartboost.sdk.c.b.a()));
    }

    public void a(int i2) {
        d dVar = this.f5981b;
        if (dVar != null) {
            dVar.setBackgroundColor(i2);
        }
        this.f5982c.setBackgroundColor(i2);
    }

    public void a(String str) {
        this.f5986g.a().a((MediaPlayer.OnCompletionListener) this);
        this.f5986g.a().a((MediaPlayer.OnErrorListener) this);
        this.f5986g.a().a((MediaPlayer.OnPreparedListener) this);
        this.f5986g.a().a(Uri.parse(str));
    }
}
