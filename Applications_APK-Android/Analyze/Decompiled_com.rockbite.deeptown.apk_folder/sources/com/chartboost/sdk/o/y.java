package com.chartboost.sdk.o;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.view.SurfaceView;
import android.view.View;
import android.widget.FrameLayout;
import com.chartboost.sdk.l;

public class y extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    private View f6231a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f6232b;

    public interface a {
        void a();

        void a(int i2);

        void a(int i2, int i3);

        void a(MediaPlayer.OnCompletionListener onCompletionListener);

        void a(MediaPlayer.OnErrorListener onErrorListener);

        void a(MediaPlayer.OnPreparedListener onPreparedListener);

        void a(Uri uri);

        void b();

        int c();

        int d();

        boolean e();
    }

    public y(Context context) {
        super(context);
        b();
    }

    private void b() {
        this.f6232b = true;
        StringBuilder sb = new StringBuilder();
        sb.append("Choosing ");
        sb.append(this.f6232b ? "texture" : "surface");
        sb.append(" solution for video playback");
        com.chartboost.sdk.c.a.e("VideoInit", sb.toString());
        l a2 = l.a();
        if (this.f6232b) {
            x xVar = new x(getContext());
            a2.a(xVar);
            this.f6231a = xVar;
        } else {
            w wVar = new w(getContext());
            a2.a(wVar);
            this.f6231a = wVar;
        }
        this.f6231a.setContentDescription("CBVideo");
        addView(this.f6231a, new FrameLayout.LayoutParams(-1, -1));
        if (!this.f6232b) {
            ((SurfaceView) this.f6231a).setZOrderMediaOverlay(true);
        }
    }

    public a a() {
        return (a) this.f6231a;
    }

    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        a().a(i2, i3);
    }
}
