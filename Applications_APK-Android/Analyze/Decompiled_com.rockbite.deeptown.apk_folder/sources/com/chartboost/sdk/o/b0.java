package com.chartboost.sdk.o;

public class b0 {
    public static int[] a(char[] cArr, int[] iArr, boolean z) {
        int i2 = (cArr[0] << 16) + cArr[1];
        int i3 = (cArr[2] << 16) + cArr[3];
        if (!z) {
            a(iArr);
        }
        int i4 = i3;
        int i5 = i2;
        int i6 = 0;
        while (i6 < 16) {
            int i7 = i5 ^ iArr[i6];
            int[][] iArr2 = a.f5949d.f5950a;
            i6++;
            int i8 = i4 ^ (((iArr2[0][i7 >>> 24] + iArr2[1][(i7 >>> 16) & 255]) ^ iArr2[2][(i7 >>> 8) & 255]) + iArr2[3][i7 & 255]);
            i4 = i7;
            i5 = i8;
        }
        int i9 = iArr[16] ^ i5;
        int i10 = iArr[17] ^ i4;
        int[] iArr3 = {i10, i9};
        cArr[0] = i10 >>> 16;
        cArr[1] = (char) i10;
        cArr[2] = i9 >>> 16;
        cArr[3] = (char) i9;
        if (!z) {
            a(iArr);
        }
        return iArr3;
    }

    private static void a(int[] iArr) {
        for (int i2 = 0; i2 < iArr.length / 2; i2++) {
            int i3 = iArr[i2];
            iArr[i2] = iArr[(iArr.length - i2) - 1];
            iArr[(iArr.length - i2) - 1] = i3;
        }
    }
}
