package com.chartboost.sdk.o;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import com.chartboost.sdk.d.d;
import com.chartboost.sdk.j;

@SuppressLint({"ViewConstructor"})
public class g0 extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private j.b f6019a;

    /* renamed from: b  reason: collision with root package name */
    private a0 f6020b;

    /* renamed from: c  reason: collision with root package name */
    private a0 f6021c;

    /* renamed from: d  reason: collision with root package name */
    private final d f6022d;

    public g0(Context context, d dVar) {
        super(context);
        this.f6022d = dVar;
        if (dVar.r.f5817b == 0) {
            this.f6020b = new a0(context);
            addView(this.f6020b, new RelativeLayout.LayoutParams(-1, -1));
            this.f6021c = new a0(context);
            addView(this.f6021c, new RelativeLayout.LayoutParams(-1, -1));
            this.f6021c.setVisibility(8);
        }
    }

    public void a() {
        if (this.f6019a == null) {
            this.f6019a = this.f6022d.k();
            j.b bVar = this.f6019a;
            if (bVar != null) {
                addView(bVar, new RelativeLayout.LayoutParams(-1, -1));
                this.f6019a.a();
            }
        }
    }

    public void b() {
    }

    public a0 c() {
        return this.f6020b;
    }

    public View d() {
        return this.f6019a;
    }

    public d e() {
        return this.f6022d;
    }

    public boolean f() {
        j.b bVar = this.f6019a;
        return bVar != null && bVar.getVisibility() == 0;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        performClick();
        return true;
    }

    public boolean performClick() {
        super.performClick();
        return true;
    }
}
