package com.chartboost.sdk.o;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;
import com.esotericsoftware.spine.Animation;

public abstract class f0 extends View {

    /* renamed from: a  reason: collision with root package name */
    private Bitmap f6006a = null;

    /* renamed from: b  reason: collision with root package name */
    private Canvas f6007b = null;

    public f0(Context context) {
        super(context);
        a(context);
    }

    private void a(Context context) {
        try {
            getClass().getMethod("setLayerType", Integer.TYPE, Paint.class).invoke(this, 1, null);
        } catch (Exception unused) {
        }
    }

    private boolean b(Canvas canvas) {
        try {
            return ((Boolean) Canvas.class.getMethod("isHardwareAccelerated", new Class[0]).invoke(canvas, new Object[0])).booleanValue();
        } catch (Exception unused) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a(Canvas canvas);

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Bitmap bitmap = this.f6006a;
        if (bitmap != null && !bitmap.isRecycled()) {
            this.f6006a.recycle();
        }
        this.f6006a = null;
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        Canvas canvas2;
        boolean b2 = b(canvas);
        if (b2) {
            Bitmap bitmap = this.f6006a;
            if (!(bitmap != null && bitmap.getWidth() == canvas.getWidth() && this.f6006a.getHeight() == canvas.getHeight())) {
                Bitmap bitmap2 = this.f6006a;
                if (bitmap2 != null && !bitmap2.isRecycled()) {
                    this.f6006a.recycle();
                }
                try {
                    this.f6006a = Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(), Bitmap.Config.ARGB_8888);
                    this.f6007b = new Canvas(this.f6006a);
                } catch (Throwable unused) {
                    return;
                }
            }
            this.f6006a.eraseColor(0);
            canvas2 = canvas;
            canvas = this.f6007b;
        } else {
            canvas2 = null;
        }
        a(canvas);
        if (b2) {
            canvas2.drawBitmap(this.f6006a, (float) Animation.CurveTimeline.LINEAR, (float) Animation.CurveTimeline.LINEAR, (Paint) null);
        }
    }
}
