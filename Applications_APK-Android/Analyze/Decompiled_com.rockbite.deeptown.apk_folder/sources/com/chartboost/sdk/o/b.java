package com.chartboost.sdk.o;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.chartboost.sdk.j;

@SuppressLint({"ViewConstructor"})
public class b extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    final j1 f5954a;

    /* renamed from: b  reason: collision with root package name */
    private LinearLayout f5955b;

    /* renamed from: c  reason: collision with root package name */
    private c0 f5956c;

    /* renamed from: d  reason: collision with root package name */
    private TextView f5957d;

    /* renamed from: e  reason: collision with root package name */
    final d0 f5958e;

    /* renamed from: f  reason: collision with root package name */
    private int f5959f = Integer.MIN_VALUE;

    class a extends d0 {
        a(Context context) {
            super(context);
        }

        /* access modifiers changed from: protected */
        public void a(MotionEvent motionEvent) {
            b.this.f5958e.setEnabled(false);
            b.this.f5954a.e().g();
        }
    }

    public b(Context context, j1 j1Var) {
        super(context);
        this.f5954a = j1Var;
        int round = Math.round(context.getResources().getDisplayMetrics().density * 8.0f);
        setOrientation(1);
        setGravity(17);
        this.f5955b = new LinearLayout(context);
        this.f5955b.setGravity(17);
        this.f5955b.setOrientation(0);
        this.f5955b.setPadding(round, round, round, round);
        this.f5956c = new c0(context);
        this.f5956c.setScaleType(ImageView.ScaleType.FIT_CENTER);
        this.f5956c.setPadding(0, 0, round, 0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        j1Var.a(layoutParams, j1Var.M, 1.0f);
        this.f5957d = new TextView(getContext());
        this.f5957d.setTextColor(-1);
        this.f5957d.setTypeface(null, 1);
        this.f5957d.setGravity(17);
        this.f5957d.setTextSize(2, j.b(context) ? 26.0f : 16.0f);
        this.f5955b.addView(this.f5956c, layoutParams);
        this.f5955b.addView(this.f5957d, new LinearLayout.LayoutParams(-2, -2));
        this.f5958e = new a(getContext());
        this.f5958e.setContentDescription("CBWatch");
        this.f5958e.setPadding(0, 0, 0, round);
        this.f5958e.a(ImageView.ScaleType.FIT_CENTER);
        this.f5958e.setPadding(round, round, round, round);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
        j1Var.a(layoutParams2, j1Var.L, 1.0f);
        this.f5956c.a(j1Var.M);
        this.f5958e.a(j1Var.L);
        addView(this.f5955b, new LinearLayout.LayoutParams(-2, -2));
        addView(this.f5958e, layoutParams2);
        a();
    }

    public void a(boolean z) {
        setBackgroundColor(z ? -16777216 : this.f5959f);
    }

    public void a(String str, int i2) {
        this.f5957d.setText(str);
        this.f5959f = i2;
        a(this.f5954a.s());
    }

    public void a() {
        a(this.f5954a.s());
    }
}
