package com.chartboost.sdk.o;

import com.chartboost.sdk.c.k;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

class w0 implements Comparable<w0> {

    /* renamed from: a  reason: collision with root package name */
    private final k f6209a;

    /* renamed from: b  reason: collision with root package name */
    final int f6210b;

    /* renamed from: c  reason: collision with root package name */
    final String f6211c;

    /* renamed from: d  reason: collision with root package name */
    final String f6212d;

    /* renamed from: e  reason: collision with root package name */
    final String f6213e;

    /* renamed from: f  reason: collision with root package name */
    final AtomicInteger f6214f;

    /* renamed from: g  reason: collision with root package name */
    private final AtomicReference<u0> f6215g;

    /* renamed from: h  reason: collision with root package name */
    private final long f6216h;

    /* renamed from: i  reason: collision with root package name */
    final AtomicInteger f6217i;

    w0(k kVar, int i2, String str, String str2, String str3, AtomicInteger atomicInteger, AtomicReference<u0> atomicReference, long j2, AtomicInteger atomicInteger2) {
        this.f6209a = kVar;
        this.f6210b = i2;
        this.f6211c = str;
        this.f6212d = str2;
        this.f6213e = str3;
        this.f6214f = atomicInteger;
        this.f6215g = atomicReference;
        this.f6216h = j2;
        this.f6217i = atomicInteger2;
        atomicInteger.incrementAndGet();
    }

    /* renamed from: a */
    public int compareTo(w0 w0Var) {
        return this.f6210b - w0Var.f6210b;
    }

    /* access modifiers changed from: package-private */
    public void a(Executor executor, boolean z) {
        u0 andSet;
        if ((this.f6214f.decrementAndGet() == 0 || !z) && (andSet = this.f6215g.getAndSet(null)) != null) {
            executor.execute(new v0(andSet, z, (int) TimeUnit.NANOSECONDS.toMillis(this.f6209a.b() - this.f6216h), this.f6217i.get()));
        }
    }
}
