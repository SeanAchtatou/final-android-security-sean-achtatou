package com.chartboost.sdk.o;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

@SuppressLint({"ViewConstructor"})
public class h1 extends c {

    /* renamed from: d  reason: collision with root package name */
    private LinearLayout f6034d;

    /* renamed from: e  reason: collision with root package name */
    private LinearLayout f6035e;

    /* renamed from: f  reason: collision with root package name */
    private c0 f6036f;

    /* renamed from: g  reason: collision with root package name */
    private d0 f6037g;

    /* renamed from: h  reason: collision with root package name */
    private TextView f6038h;

    /* renamed from: i  reason: collision with root package name */
    private TextView f6039i;

    class a extends d0 {
        a(Context context) {
            super(context);
        }

        /* access modifiers changed from: protected */
        public void a(MotionEvent motionEvent) {
            h1.this.f5963a.e().b(motionEvent.getX(), motionEvent.getY(), (float) super.getWidth(), (float) super.getHeight());
        }
    }

    public h1(Context context, j1 j1Var) {
        super(context, j1Var);
    }

    /* access modifiers changed from: protected */
    public View a() {
        Context context = getContext();
        int round = Math.round(getContext().getResources().getDisplayMetrics().density * 6.0f);
        this.f6034d = new LinearLayout(context);
        this.f6034d.setOrientation(0);
        this.f6034d.setGravity(17);
        this.f6035e = new LinearLayout(context);
        this.f6035e.setOrientation(1);
        this.f6035e.setGravity(8388627);
        this.f6036f = new c0(context);
        this.f6036f.setPadding(round, round, round, round);
        if (this.f5963a.O.c()) {
            this.f6036f.a(this.f5963a.O);
        }
        this.f6037g = new a(context);
        this.f6037g.setPadding(round, round, round, round);
        if (this.f5963a.P.c()) {
            this.f6037g.a(this.f5963a.P);
        }
        this.f6038h = new TextView(getContext());
        this.f6038h.setTextColor(-15264491);
        this.f6038h.setTypeface(null, 1);
        this.f6038h.setGravity(8388611);
        this.f6038h.setPadding(round, round, round, round / 2);
        this.f6039i = new TextView(getContext());
        this.f6039i.setTextColor(-15264491);
        this.f6039i.setTypeface(null, 1);
        this.f6039i.setGravity(8388611);
        this.f6039i.setPadding(round, 0, round, round);
        this.f6038h.setTextSize(2, 14.0f);
        this.f6039i.setTextSize(2, 11.0f);
        this.f6035e.addView(this.f6038h);
        this.f6035e.addView(this.f6039i);
        this.f6034d.addView(this.f6036f);
        this.f6034d.addView(this.f6035e, new LinearLayout.LayoutParams(0, -2, 1.0f));
        this.f6034d.addView(this.f6037g);
        return this.f6034d;
    }

    /* access modifiers changed from: protected */
    public int b() {
        return 72;
    }

    public void a(String str, String str2) {
        this.f6038h.setText(str);
        this.f6039i.setText(str2);
    }
}
