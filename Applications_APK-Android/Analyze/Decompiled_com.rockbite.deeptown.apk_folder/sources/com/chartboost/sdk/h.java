package com.chartboost.sdk;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import com.chartboost.sdk.a;
import com.chartboost.sdk.d.a;
import com.chartboost.sdk.o.f1;
import com.chartboost.sdk.o.g0;
import com.chartboost.sdk.o.l;
import com.chartboost.sdk.o.t;
import java.util.HashSet;

public class h {

    /* renamed from: a  reason: collision with root package name */
    private final l f5857a;

    /* renamed from: b  reason: collision with root package name */
    final m f5858b;

    /* renamed from: c  reason: collision with root package name */
    private final com.chartboost.sdk.e.a f5859c;

    /* renamed from: d  reason: collision with root package name */
    final Handler f5860d;

    /* renamed from: e  reason: collision with root package name */
    public final i f5861e;

    /* renamed from: f  reason: collision with root package name */
    com.chartboost.sdk.c.l f5862f;

    /* renamed from: g  reason: collision with root package name */
    CBImpressionActivity f5863g = null;

    /* renamed from: h  reason: collision with root package name */
    com.chartboost.sdk.d.d f5864h = null;

    /* renamed from: i  reason: collision with root package name */
    private boolean f5865i = false;

    /* renamed from: j  reason: collision with root package name */
    private final HashSet<Integer> f5866j = new HashSet<>();

    /* renamed from: k  reason: collision with root package name */
    Runnable f5867k;
    private com.chartboost.sdk.c.l l;

    @TargetApi(14)
    private class b implements Application.ActivityLifecycleCallbacks {
        private b() {
        }

        public void onActivityCreated(Activity activity, Bundle bundle) {
            t.a("CBUIManager.ActivityCallbackListener.onActivityCreated", activity);
            com.chartboost.sdk.c.a.a("CBUIManager", "######## onActivityCreated callback called");
            if (!(activity instanceof CBImpressionActivity)) {
                h.this.b(activity);
            }
        }

        public void onActivityDestroyed(Activity activity) {
            t.a("CBUIManager.ActivityCallbackListener.onActivityDestroyed", activity);
            if (!(activity instanceof CBImpressionActivity)) {
                com.chartboost.sdk.c.a.a("CBUIManager", "######## onActivityDestroyed callback called from developer side");
                h.this.j(activity);
                return;
            }
            com.chartboost.sdk.c.a.a("CBUIManager", "######## onActivityDestroyed callback called from CBImpressionactivity");
            h.this.k(activity);
        }

        public void onActivityPaused(Activity activity) {
            t.a("CBUIManager.ActivityCallbackListener.onActivityPaused", activity);
            if (!(activity instanceof CBImpressionActivity)) {
                com.chartboost.sdk.c.a.a("CBUIManager", "######## onActivityPaused callback called from developer side");
                h.this.g(activity);
                return;
            }
            com.chartboost.sdk.c.a.a("CBUIManager", "######## onActivityPaused callback called from CBImpressionactivity");
            h.this.a(activity);
            h.this.i();
        }

        public void onActivityResumed(Activity activity) {
            t.a("CBUIManager.ActivityCallbackListener.onActivityResumed", activity);
            if (!(activity instanceof CBImpressionActivity)) {
                com.chartboost.sdk.c.a.a("CBUIManager", "######## onActivityResumed callback called from developer side");
                h.this.f(activity);
                return;
            }
            com.chartboost.sdk.c.a.a("CBUIManager", "######## onActivityResumed callback called from CBImpressionactivity");
            h.this.a(activity);
            h.this.h();
        }

        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        public void onActivityStarted(Activity activity) {
            t.a("CBUIManager.ActivityCallbackListener.onActivityStarted", activity);
            if (!(activity instanceof CBImpressionActivity)) {
                com.chartboost.sdk.c.a.a("CBUIManager", "######## onActivityStarted callback called from developer side");
                h.this.d(activity);
                return;
            }
            com.chartboost.sdk.c.a.a("CBUIManager", "######## onActivityStarted callback called from CBImpressionactivity");
            h.this.e(activity);
        }

        public void onActivityStopped(Activity activity) {
            t.a("CBUIManager.ActivityCallbackListener.onActivityStopped", activity);
            if (!(activity instanceof CBImpressionActivity)) {
                com.chartboost.sdk.c.a.a("CBUIManager", "######## onActivityStopped callback called from developer side");
                h.this.h(activity);
                return;
            }
            com.chartboost.sdk.c.a.a("CBUIManager", "######## onActivityStopped callback called from CBImpressionactivity");
            h.this.i(activity);
        }
    }

    class c implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private final int f5869a;

        /* renamed from: b  reason: collision with root package name */
        private final int f5870b;

        c() {
            f a2 = a();
            CBImpressionActivity cBImpressionActivity = h.this.f5863g;
            if (cBImpressionActivity != null) {
                cBImpressionActivity.hashCode();
            }
            com.chartboost.sdk.c.l lVar = h.this.f5862f;
            int i2 = -1;
            this.f5869a = lVar == null ? -1 : lVar.hashCode();
            this.f5870b = a2 != null ? a2.hashCode() : i2;
        }

        private f a() {
            return n.f5941d;
        }

        public void run() {
            t.a("ClearMemoryRunnable.run");
            f a2 = a();
            com.chartboost.sdk.c.l lVar = h.this.f5862f;
            if (lVar != null && lVar.hashCode() == this.f5869a) {
                h.this.f5862f = null;
                t.a("CBUIManager.clearHostActivityRef");
            }
            if (a2 != null && a2.hashCode() == this.f5870b) {
                n.f5941d = null;
                t.a("SdkSettings.clearDelegate");
            }
        }
    }

    public class d implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        public final int f5872a;

        /* renamed from: b  reason: collision with root package name */
        Activity f5873b = null;

        /* renamed from: c  reason: collision with root package name */
        boolean f5874c = false;

        /* renamed from: d  reason: collision with root package name */
        public com.chartboost.sdk.d.d f5875d = null;

        public d(int i2) {
            this.f5872a = i2;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.chartboost.sdk.h.a(com.chartboost.sdk.c.l, boolean):void
         arg types: [com.chartboost.sdk.c.l, int]
         candidates:
          com.chartboost.sdk.h.a(int, boolean):void
          com.chartboost.sdk.h.a(android.app.Activity, com.chartboost.sdk.d.d):void
          com.chartboost.sdk.h.a(android.app.Activity, boolean):void
          com.chartboost.sdk.h.a(com.chartboost.sdk.c.l, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.chartboost.sdk.h.a(android.app.Activity, boolean):void
         arg types: [android.app.Activity, int]
         candidates:
          com.chartboost.sdk.h.a(int, boolean):void
          com.chartboost.sdk.h.a(android.app.Activity, com.chartboost.sdk.d.d):void
          com.chartboost.sdk.h.a(com.chartboost.sdk.c.l, boolean):void
          com.chartboost.sdk.h.a(android.app.Activity, boolean):void */
        public void run() {
            try {
                switch (this.f5872a) {
                    case 0:
                        h.this.c(this.f5873b);
                        return;
                    case 1:
                        h.this.f5860d.removeCallbacks(h.this.f5867k);
                        if (h.this.f5862f != null && !h.this.f5862f.a(this.f5873b) && h.this.g()) {
                            h.this.b(h.this.f5862f);
                            h.this.a(h.this.f5862f, false);
                        }
                        h.this.a(this.f5873b, true);
                        h.this.f5862f = h.this.a(this.f5873b);
                        h.this.f5858b.a();
                        h.this.f5858b.a(this.f5873b);
                        h.this.e(this.f5873b);
                        return;
                    case 2:
                        if (h.this.a(h.this.a(this.f5873b))) {
                            h.this.h();
                            return;
                        } else if (com.chartboost.sdk.c.b.a(a.C0125a.CBFrameworkUnity)) {
                            h.this.f5858b.a();
                            return;
                        } else {
                            return;
                        }
                    case 3:
                        if (h.this.a(h.this.a(this.f5873b))) {
                            h.this.i();
                            return;
                        }
                        return;
                    case 4:
                        com.chartboost.sdk.c.l a2 = h.this.a(this.f5873b);
                        if (h.this.a(a2)) {
                            h.this.b(a2);
                            return;
                        }
                        return;
                    case 5:
                        if (h.this.f5862f == null || h.this.f5862f.a(this.f5873b)) {
                            h.this.f5867k = new c();
                            h.this.f5867k.run();
                        }
                        h.this.k(this.f5873b);
                        return;
                    case 6:
                        if (h.this.f5863g == null) {
                            return;
                        }
                        if (this.f5874c) {
                            h.this.f5863g.a(h.this.a());
                            return;
                        } else {
                            h.this.f5863g.a(null);
                            return;
                        }
                    case 7:
                        h.this.l();
                        return;
                    case 8:
                    default:
                        return;
                    case 9:
                        h.this.a(this.f5873b, this.f5875d);
                        return;
                    case 10:
                        if (this.f5875d.a()) {
                            this.f5875d.u().b();
                            return;
                        }
                        return;
                    case 11:
                        i c2 = h.this.c();
                        if (this.f5875d.l == 2 && c2 != null) {
                            c2.b(this.f5875d);
                            return;
                        }
                        return;
                    case 12:
                        this.f5875d.n();
                        return;
                    case 13:
                        h.this.f5861e.a(this.f5875d, this.f5873b);
                        return;
                    case 14:
                        h.this.f5861e.d(this.f5875d);
                        return;
                }
            } catch (Exception e2) {
                com.chartboost.sdk.e.a.a(d.class, "run (" + this.f5872a + ")", e2);
            }
        }
    }

    public h(Activity activity, l lVar, m mVar, com.chartboost.sdk.e.a aVar, Handler handler, i iVar) {
        this.f5857a = lVar;
        this.f5858b = mVar;
        this.f5859c = aVar;
        this.f5860d = handler;
        this.f5861e = iVar;
        this.f5862f = a(activity);
        t.a("CBUIManager.assignHostActivityRef", this.f5862f);
        this.f5867k = new c();
        if (f1.e().a(14)) {
            new b();
        }
    }

    private void b(com.chartboost.sdk.c.l lVar, boolean z) {
    }

    private boolean l(Activity activity) {
        return this.f5863g == activity;
    }

    private boolean m() {
        t.a("CBUIManager.closeImpressionImpl");
        com.chartboost.sdk.d.d d2 = d();
        if (d2 == null || d2.l != 2) {
            return false;
        }
        if (d2.q()) {
            return true;
        }
        m.b(new d(7));
        return true;
    }

    /* access modifiers changed from: package-private */
    public com.chartboost.sdk.c.l a(Activity activity) {
        com.chartboost.sdk.c.l lVar = this.l;
        if (lVar == null || lVar.f5787a != activity.hashCode()) {
            this.l = new com.chartboost.sdk.c.l(activity);
        }
        return this.l;
    }

    public Activity b() {
        return this.f5863g;
    }

    public i c() {
        if (b() == null) {
            return null;
        }
        return this.f5861e;
    }

    /* access modifiers changed from: package-private */
    public com.chartboost.sdk.d.d d() {
        g0 g0Var;
        i c2 = c();
        if (c2 == null) {
            g0Var = null;
        } else {
            g0Var = c2.a();
        }
        if (g0Var == null || !g0Var.f()) {
            return null;
        }
        return g0Var.e();
    }

    public boolean e() {
        return d() != null;
    }

    /* access modifiers changed from: package-private */
    public void f() {
        t.a("CBUIManager.clearImpressionActivity");
        this.f5863g = null;
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return a(this.f5862f);
    }

    /* access modifiers changed from: package-private */
    public void h() {
        t.a("CBUIManager.onResumeImpl", (String) null);
        this.f5857a.d();
        com.chartboost.sdk.d.d d2 = d();
        if (com.chartboost.sdk.c.b.a(a.C0125a.CBFrameworkUnity)) {
            this.f5858b.a();
        }
        if (d2 != null) {
            d2.r();
        }
    }

    /* access modifiers changed from: package-private */
    public void i() {
        t.a("CBUIManager.onPauseImpl", (String) null);
        com.chartboost.sdk.d.d d2 = d();
        if (d2 != null) {
            d2.t();
        }
        this.f5857a.e();
    }

    /* access modifiers changed from: package-private */
    public boolean j() {
        t.a("CBUIManager.onBackPressedCallback");
        if (!g.b()) {
            return false;
        }
        if (this.f5862f == null) {
            com.chartboost.sdk.c.a.b("CBUIManager", "The Chartboost methods onCreate(), onStart(), onStop(), and onDestroy() must be called in the corresponding methods of your activity in order for Chartboost to function properly.");
            return false;
        } else if (!this.f5865i) {
            return false;
        } else {
            this.f5865i = false;
            k();
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean k() {
        t.a("CBUIManager.onBackPressedImpl");
        return m();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.h.a(com.chartboost.sdk.c.l, boolean):void
     arg types: [com.chartboost.sdk.c.l, int]
     candidates:
      com.chartboost.sdk.h.a(int, boolean):void
      com.chartboost.sdk.h.a(android.app.Activity, com.chartboost.sdk.d.d):void
      com.chartboost.sdk.h.a(android.app.Activity, boolean):void
      com.chartboost.sdk.h.a(com.chartboost.sdk.c.l, boolean):void */
    /* access modifiers changed from: package-private */
    public void b(com.chartboost.sdk.c.l lVar) {
        t.a("CBUIManager.onStop", lVar);
        if (!(lVar.get() instanceof CBImpressionActivity)) {
            a(lVar, false);
        }
        this.f5858b.b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.h.a(com.chartboost.sdk.c.l, boolean):void
     arg types: [com.chartboost.sdk.c.l, int]
     candidates:
      com.chartboost.sdk.h.a(int, boolean):void
      com.chartboost.sdk.h.a(android.app.Activity, com.chartboost.sdk.d.d):void
      com.chartboost.sdk.h.a(android.app.Activity, boolean):void
      com.chartboost.sdk.h.a(com.chartboost.sdk.c.l, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.h.b(com.chartboost.sdk.c.l, boolean):void
     arg types: [com.chartboost.sdk.c.l, int]
     candidates:
      com.chartboost.sdk.h.b(android.app.Activity, com.chartboost.sdk.d.d):boolean
      com.chartboost.sdk.h.b(com.chartboost.sdk.c.l, boolean):void */
    /* access modifiers changed from: package-private */
    public void e(Activity activity) {
        t.a("CBUIManager.onStartImpl", activity);
        n.n = activity.getApplicationContext();
        boolean z = activity instanceof CBImpressionActivity;
        if (!z) {
            this.f5862f = a(activity);
            t.a("CBUIManager.assignHostActivityRef", this.f5862f);
            a(this.f5862f, true);
        } else {
            a((CBImpressionActivity) activity);
        }
        this.f5860d.removeCallbacks(this.f5867k);
        a.C0125a aVar = n.f5942e;
        boolean z2 = aVar != null && aVar.a();
        if (activity == null) {
            return;
        }
        if (z2 || l(activity)) {
            b(a(activity), true);
            if (z) {
                this.f5865i = false;
            }
            if (b(activity, this.f5864h)) {
                this.f5864h = null;
            }
            com.chartboost.sdk.d.d d2 = d();
            if (d2 != null) {
                d2.s();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void g(Activity activity) {
        t.a("CBUIManager.onPauseCallback", activity);
        if (g.b() && g.a(activity)) {
            d dVar = new d(3);
            dVar.f5873b = activity;
            m.b(dVar);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean l() {
        com.chartboost.sdk.d.d d2 = d();
        if (d2 == null) {
            return false;
        }
        d2.B = true;
        b(d2);
        return true;
    }

    private boolean c(com.chartboost.sdk.c.l lVar) {
        if (lVar == null) {
            return this.f5863g == null;
        }
        return lVar.a(this.f5863g);
    }

    /* access modifiers changed from: package-private */
    public void f(Activity activity) {
        t.a("CBUIManager.onResumeCallback", activity);
        if (g.b() && g.a(activity)) {
            this.f5858b.d();
            d dVar = new d(2);
            dVar.f5873b = activity;
            m.b(dVar);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.h.b(com.chartboost.sdk.c.l, boolean):void
     arg types: [com.chartboost.sdk.c.l, int]
     candidates:
      com.chartboost.sdk.h.b(android.app.Activity, com.chartboost.sdk.d.d):boolean
      com.chartboost.sdk.h.b(com.chartboost.sdk.c.l, boolean):void */
    /* access modifiers changed from: package-private */
    public void k(Activity activity) {
        com.chartboost.sdk.d.d dVar;
        t.a("CBUIManager.onDestroyImpl", activity);
        b(a(activity), false);
        com.chartboost.sdk.d.d d2 = d();
        if (!(d2 == null && activity == this.f5863g && (dVar = this.f5864h) != null)) {
            dVar = d2;
        }
        i c2 = c();
        if (!(c2 == null || dVar == null)) {
            c2.d(dVar);
        }
        this.f5864h = null;
    }

    public Activity a() {
        com.chartboost.sdk.c.l lVar = this.f5862f;
        if (lVar != null) {
            return (Activity) lVar.get();
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void a(CBImpressionActivity cBImpressionActivity) {
        t.a("CBUIManager.setImpressionActivity", cBImpressionActivity);
        if (this.f5863g == null) {
            n.n = cBImpressionActivity.getApplicationContext();
            this.f5863g = cBImpressionActivity;
        }
        this.f5860d.removeCallbacks(this.f5867k);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.h.a(com.chartboost.sdk.c.l, boolean):void
     arg types: [com.chartboost.sdk.c.l, int]
     candidates:
      com.chartboost.sdk.h.a(int, boolean):void
      com.chartboost.sdk.h.a(android.app.Activity, com.chartboost.sdk.d.d):void
      com.chartboost.sdk.h.a(android.app.Activity, boolean):void
      com.chartboost.sdk.h.a(com.chartboost.sdk.c.l, boolean):void */
    /* access modifiers changed from: package-private */
    public void c(Activity activity) {
        t.a("CBUIManager.onCreateImpl", activity);
        com.chartboost.sdk.c.l lVar = this.f5862f;
        if (lVar != null && !lVar.a(activity) && g()) {
            b(this.f5862f);
            a(this.f5862f, false);
        }
        this.f5860d.removeCallbacks(this.f5867k);
        this.f5862f = a(activity);
        t.a("CBUIManager.assignHostActivityRef", this.f5862f);
    }

    /* access modifiers changed from: package-private */
    public void d(Activity activity) {
        t.a("CBUIManager.onStartCallback", activity);
        if (g.b() && g.a(activity)) {
            d dVar = new d(1);
            dVar.f5873b = activity;
            m.b(dVar);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.h.b(com.chartboost.sdk.c.l, boolean):void
     arg types: [com.chartboost.sdk.c.l, int]
     candidates:
      com.chartboost.sdk.h.b(android.app.Activity, com.chartboost.sdk.d.d):boolean
      com.chartboost.sdk.h.b(com.chartboost.sdk.c.l, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chartboost.sdk.h.a(com.chartboost.sdk.c.l, boolean):void
     arg types: [com.chartboost.sdk.c.l, int]
     candidates:
      com.chartboost.sdk.h.a(int, boolean):void
      com.chartboost.sdk.h.a(android.app.Activity, com.chartboost.sdk.d.d):void
      com.chartboost.sdk.h.a(android.app.Activity, boolean):void
      com.chartboost.sdk.h.a(com.chartboost.sdk.c.l, boolean):void */
    /* access modifiers changed from: package-private */
    public void i(Activity activity) {
        com.chartboost.sdk.c.l a2 = a(activity);
        t.a("CBUIManager.onStopImpl", a2);
        com.chartboost.sdk.d.d d2 = d();
        if (d2 != null && d2.r.f5817b == 0) {
            i c2 = c();
            if (c(a2) && c2 != null) {
                c2.c(d2);
                this.f5864h = d2;
                b(a2, false);
            }
            if (!(a2.get() instanceof CBImpressionActivity)) {
                a(a2, false);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Activity activity) {
        t.a("CBUIManager.onCreateCallback", activity);
        if (g.b() && g.a(activity)) {
            d dVar = new d(0);
            dVar.f5873b = activity;
            m.b(dVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void h(Activity activity) {
        t.a("CBUIManager.onStopCallback", activity);
        if (g.b() && g.a(activity)) {
            d dVar = new d(4);
            dVar.f5873b = activity;
            m.b(dVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void j(Activity activity) {
        t.a("CBUIManager.onDestroyCallback", activity);
        if (g.b() && g.a(activity)) {
            d dVar = new d(5);
            dVar.f5873b = activity;
            m.b(dVar);
        }
    }

    private void a(int i2, boolean z) {
        if (z) {
            this.f5866j.add(Integer.valueOf(i2));
        } else {
            this.f5866j.remove(Integer.valueOf(i2));
        }
    }

    public void b(com.chartboost.sdk.d.d dVar) {
        i c2;
        int i2 = dVar.l;
        if (i2 == 2) {
            i c3 = c();
            if (c3 != null) {
                c3.b(dVar);
            }
        } else if (dVar.r.f5817b == 1 && i2 == 1 && (c2 = c()) != null) {
            c2.d(dVar);
        }
        if (dVar.v()) {
            this.f5859c.d(dVar.f5830a.a(dVar.r.f5817b), dVar.m, dVar.o());
        } else {
            this.f5859c.e(dVar.f5830a.a(dVar.r.f5817b), dVar.m, dVar.o());
        }
    }

    /* access modifiers changed from: package-private */
    public void a(com.chartboost.sdk.c.l lVar, boolean z) {
        if (lVar != null) {
            a(lVar.f5787a, z);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Activity activity, boolean z) {
        if (activity != null) {
            a(activity.hashCode(), z);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(com.chartboost.sdk.c.l lVar) {
        if (lVar == null) {
            return false;
        }
        return this.f5866j.contains(Integer.valueOf(lVar.f5787a));
    }

    public void a(com.chartboost.sdk.d.d dVar) {
        t.a("CBUIManager.queueDisplayView", dVar);
        if (e()) {
            dVar.a(a.c.IMPRESSION_ALREADY_VISIBLE);
        } else if (this.f5863g != null) {
            this.f5861e.a(dVar);
        } else if (!g()) {
            dVar.a(a.c.NO_HOST_ACTIVITY);
        } else {
            Activity a2 = a();
            if (a2 == null) {
                com.chartboost.sdk.c.a.b("CBUIManager", "Failed to display impression as the host activity reference has been lost!");
                dVar.a(a.c.NO_HOST_ACTIVITY);
                return;
            }
            com.chartboost.sdk.d.d dVar2 = this.f5864h;
            if (dVar2 == null || dVar2 == dVar) {
                this.f5864h = dVar;
                f fVar = n.f5941d;
                if (fVar != null) {
                    int i2 = dVar.n;
                    if (i2 == 1 || i2 == 2) {
                        n.f5941d.willDisplayVideo(dVar.m);
                    } else if (i2 == 0) {
                        fVar.willDisplayInterstitial(dVar.m);
                    }
                }
                if (n.f5942e != null) {
                    d dVar3 = new d(9);
                    dVar3.f5873b = a2;
                    dVar3.f5875d = dVar;
                    this.f5860d.postDelayed(dVar3, (long) 1);
                    return;
                }
                a(a2, dVar);
                return;
            }
            dVar.a(a.c.IMPRESSION_ALREADY_VISIBLE);
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000d, code lost:
        if (r1 != 3) goto L_0x004b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean b(android.app.Activity r4, com.chartboost.sdk.d.d r5) {
        /*
            r3 = this;
            r0 = 1
            if (r5 == 0) goto L_0x004b
            int r1 = r5.l
            if (r1 == 0) goto L_0x004b
            if (r1 == r0) goto L_0x0048
            r2 = 2
            if (r1 == r2) goto L_0x0010
            r4 = 3
            if (r1 == r4) goto L_0x0048
            goto L_0x004b
        L_0x0010:
            boolean r1 = r5.g()
            if (r1 != 0) goto L_0x004b
            com.chartboost.sdk.a$a r1 = com.chartboost.sdk.n.f5942e
            if (r1 == 0) goto L_0x0026
            boolean r1 = r1.a()
            if (r1 == 0) goto L_0x0026
            boolean r4 = r4 instanceof com.chartboost.sdk.CBImpressionActivity
            if (r4 != 0) goto L_0x0026
            r4 = 0
            return r4
        L_0x0026:
            com.chartboost.sdk.i r4 = r3.c()
            if (r4 == 0) goto L_0x004b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Error onActivityStart "
            r1.append(r2)
            int r2 = r5.l
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.lang.String r2 = "CBUIManager"
            com.chartboost.sdk.c.a.b(r2, r1)
            r4.d(r5)
            goto L_0x004b
        L_0x0048:
            r3.a(r5)
        L_0x004b:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chartboost.sdk.h.b(android.app.Activity, com.chartboost.sdk.d.d):boolean");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void a(Activity activity, com.chartboost.sdk.d.d dVar) {
        Intent intent = new Intent(activity, CBImpressionActivity.class);
        boolean z = false;
        boolean z2 = (activity.getWindow().getAttributes().flags & 1024) != 0;
        boolean z3 = (activity.getWindow().getAttributes().flags & 2048) != 0;
        if (z2 && !z3) {
            z = true;
        }
        intent.putExtra("paramFullscreen", z);
        intent.putExtra("isChartboost", true);
        try {
            activity.startActivity(intent);
            this.f5865i = true;
        } catch (ActivityNotFoundException unused) {
            com.chartboost.sdk.c.a.b("CBUIManager", "Please add CBImpressionActivity in AndroidManifest.xml following README.md instructions.");
            this.f5864h = null;
            dVar.a(a.c.ACTIVITY_MISSING_IN_MANIFEST);
        }
    }
}
