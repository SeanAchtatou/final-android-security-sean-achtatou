package com.applovin.communicator;

import android.content.Context;
import com.applovin.impl.communicator.MessagingServiceImpl;
import com.applovin.impl.communicator.a;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.q;
import java.util.Collections;
import java.util.List;

public final class AppLovinCommunicator {

    /* renamed from: d  reason: collision with root package name */
    private static AppLovinCommunicator f3215d;

    /* renamed from: e  reason: collision with root package name */
    private static final Object f3216e = new Object();

    /* renamed from: a  reason: collision with root package name */
    private k f3217a;

    /* renamed from: b  reason: collision with root package name */
    private final a f3218b;

    /* renamed from: c  reason: collision with root package name */
    private final MessagingServiceImpl f3219c;

    private AppLovinCommunicator(Context context) {
        this.f3218b = new a(context);
        this.f3219c = new MessagingServiceImpl(context);
    }

    public static AppLovinCommunicator getInstance(Context context) {
        synchronized (f3216e) {
            if (f3215d == null) {
                f3215d = new AppLovinCommunicator(context.getApplicationContext());
            }
        }
        return f3215d;
    }

    public void a(k kVar) {
        q.f("AppLovinCommunicator", "Attaching SDK instance: " + kVar + "...");
        this.f3217a = kVar;
    }

    public AppLovinCommunicatorMessagingService getMessagingService() {
        return this.f3219c;
    }

    public void subscribe(AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber, String str) {
        subscribe(appLovinCommunicatorSubscriber, Collections.singletonList(str));
    }

    public void subscribe(AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber, List<String> list) {
        for (String next : list) {
            q.f("AppLovinCommunicator", "Subscribing " + appLovinCommunicatorSubscriber + " to topic: " + next);
            if (this.f3218b.a(appLovinCommunicatorSubscriber, next)) {
                q.f("AppLovinCommunicator", "Subscribed " + appLovinCommunicatorSubscriber + " to topic: " + next);
                this.f3219c.maybeFlushStickyMessages(next);
            } else {
                q.f("AppLovinCommunicator", "Unable to subscribe " + appLovinCommunicatorSubscriber + " to topic: " + next);
            }
        }
    }

    public String toString() {
        return "AppLovinCommunicator{sdk=" + this.f3217a + '}';
    }

    public void unsubscribe(AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber, String str) {
        unsubscribe(appLovinCommunicatorSubscriber, Collections.singletonList(str));
    }

    public void unsubscribe(AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber, List<String> list) {
        for (String next : list) {
            q.f("AppLovinCommunicator", "Unsubscribing " + appLovinCommunicatorSubscriber + " from topic: " + next);
            this.f3218b.b(appLovinCommunicatorSubscriber, next);
        }
    }
}
