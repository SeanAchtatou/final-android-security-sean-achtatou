package com.applovin.adview;

import android.content.Context;
import com.applovin.impl.adview.InterstitialAdDialogCreatorImpl;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkUtils;

public class AppLovinInterstitialAd {

    static class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AppLovinSdk f3212a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Context f3213b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ String f3214c;

        a(AppLovinSdk appLovinSdk, Context context, String str) {
            this.f3212a = appLovinSdk;
            this.f3213b = context;
            this.f3214c = str;
        }

        public void run() {
            new InterstitialAdDialogCreatorImpl().createInterstitialAdDialog(this.f3212a, this.f3213b).show(this.f3214c);
        }
    }

    public static AppLovinInterstitialAdDialog create(AppLovinSdk appLovinSdk, Context context) {
        if (appLovinSdk == null) {
            throw new IllegalArgumentException("No sdk specified");
        } else if (context != null) {
            return new InterstitialAdDialogCreatorImpl().createInterstitialAdDialog(appLovinSdk, context);
        } else {
            throw new IllegalArgumentException("No context specified");
        }
    }

    @Deprecated
    public static boolean isAdReadyToDisplay(Context context) {
        return AppLovinSdk.getInstance(context).getAdService().hasPreloadedAd(AppLovinAdSize.INTERSTITIAL);
    }

    public static void show(Context context) {
        show(context, null);
    }

    @Deprecated
    public static void show(Context context, String str) {
        if (context != null) {
            AppLovinSdk instance = AppLovinSdk.getInstance(context);
            if (instance != null && !instance.hasCriticalErrors()) {
                show(instance, context, str);
                return;
            }
            return;
        }
        throw new IllegalArgumentException("No context specified");
    }

    @Deprecated
    public static void show(AppLovinSdk appLovinSdk, Context context, String str) {
        if (appLovinSdk == null) {
            throw new IllegalArgumentException("No sdk specified");
        } else if (context != null) {
            AppLovinSdkUtils.runOnUiThread(new a(appLovinSdk, context, str));
        } else {
            throw new IllegalArgumentException("No context specified");
        }
    }

    public String toString() {
        return "AppLovinInterstitialAd{}";
    }
}
