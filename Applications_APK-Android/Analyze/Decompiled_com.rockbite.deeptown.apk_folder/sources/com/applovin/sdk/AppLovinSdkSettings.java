package com.applovin.sdk;

import android.content.Context;
import com.applovin.impl.sdk.q;
import com.applovin.impl.sdk.utils.p;
import java.util.HashMap;
import java.util.Map;

public class AppLovinSdkSettings {

    /* renamed from: a  reason: collision with root package name */
    private boolean f4600a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f4601b;

    /* renamed from: c  reason: collision with root package name */
    private long f4602c;

    /* renamed from: d  reason: collision with root package name */
    private String f4603d;

    /* renamed from: e  reason: collision with root package name */
    private String f4604e;

    /* renamed from: f  reason: collision with root package name */
    private boolean f4605f;
    private final Map<String, Object> localSettings;

    public AppLovinSdkSettings() {
        this(null);
    }

    public AppLovinSdkSettings(Context context) {
        this.localSettings = new HashMap();
        this.f4601b = p.c(context);
        this.f4600a = p.b(context);
        this.f4602c = -1;
        this.f4603d = AppLovinAdSize.INTERSTITIAL.getLabel() + "," + AppLovinAdSize.BANNER.getLabel() + "," + AppLovinAdSize.MREC.getLabel();
        this.f4604e = AppLovinAdType.INCENTIVIZED.getLabel() + "," + AppLovinAdType.REGULAR.getLabel() + "," + AppLovinAdType.NATIVE.getLabel();
    }

    @Deprecated
    public String getAutoPreloadSizes() {
        return this.f4603d;
    }

    @Deprecated
    public String getAutoPreloadTypes() {
        return this.f4604e;
    }

    @Deprecated
    public long getBannerAdRefreshSeconds() {
        return this.f4602c;
    }

    public boolean isMuted() {
        return this.f4605f;
    }

    public boolean isTestAdsEnabled() {
        return this.f4600a;
    }

    public boolean isVerboseLoggingEnabled() {
        return this.f4601b;
    }

    @Deprecated
    public void setAutoPreloadSizes(String str) {
        this.f4603d = str;
    }

    @Deprecated
    public void setAutoPreloadTypes(String str) {
        this.f4604e = str;
    }

    @Deprecated
    public void setBannerAdRefreshSeconds(long j2) {
        this.f4602c = j2;
    }

    public void setMuted(boolean z) {
        this.f4605f = z;
    }

    public void setTestAdsEnabled(boolean z) {
        this.f4600a = z;
    }

    public void setVerboseLogging(boolean z) {
        if (p.a()) {
            q.i("AppLovinSdkSettings", "Ignoring setting of verbose logging - it is configured from Android manifest already or AppLovinSdkSettings was initialized without a context.");
        } else {
            this.f4601b = z;
        }
    }

    public String toString() {
        return "AppLovinSdkSettings{isTestAdsEnabled=" + this.f4600a + ", isVerboseLoggingEnabled=" + this.f4601b + ", muted=" + this.f4605f + '}';
    }
}
