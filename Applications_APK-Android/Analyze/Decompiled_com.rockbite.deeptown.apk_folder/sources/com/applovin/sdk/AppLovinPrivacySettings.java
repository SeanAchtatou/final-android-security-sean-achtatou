package com.applovin.sdk;

import android.content.Context;
import com.applovin.impl.sdk.h;

public class AppLovinPrivacySettings {
    public static boolean hasUserConsent(Context context) {
        Boolean a2 = h.a(context);
        if (a2 != null) {
            return a2.booleanValue();
        }
        return false;
    }

    public static boolean isAgeRestrictedUser(Context context) {
        Boolean b2 = h.b(context);
        if (b2 != null) {
            return b2.booleanValue();
        }
        return false;
    }

    public static void setHasUserConsent(boolean z, Context context) {
        if (h.a(z, context)) {
            AppLovinSdk.reinitializeAll(Boolean.valueOf(z));
        }
    }

    public static void setIsAgeRestrictedUser(boolean z, Context context) {
        if (h.b(z, context)) {
            AppLovinSdk.reinitializeAll();
        }
    }
}
