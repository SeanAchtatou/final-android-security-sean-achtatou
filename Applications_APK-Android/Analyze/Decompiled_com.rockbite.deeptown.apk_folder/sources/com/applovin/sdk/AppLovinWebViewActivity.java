package com.applovin.sdk;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.applovin.impl.sdk.q;
import com.applovin.impl.sdk.utils.m;
import java.util.Set;

public class AppLovinWebViewActivity extends Activity {
    public static final String EVENT_DISMISSED_VIA_BACK_BUTTON = "dismissed_via_back_button";
    public static final String INTENT_EXTRA_KEY_IMMERSIVE_MODE_ON = "immersive_mode_on";
    public static final String INTENT_EXTRA_KEY_SDK_KEY = "sdk_key";

    /* renamed from: a  reason: collision with root package name */
    private WebView f4607a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public EventListener f4608b;

    public interface EventListener {
        void onReceivedEvent(String str);
    }

    class a extends WebViewClient {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AppLovinSdk f4609a;

        a(AppLovinSdk appLovinSdk) {
            this.f4609a = appLovinSdk;
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            Uri parse = Uri.parse(str);
            String scheme = parse.getScheme();
            String host = parse.getHost();
            String path = parse.getPath();
            q logger = this.f4609a.getLogger();
            logger.b("AppLovinWebViewActivity", "Handling url load: " + str);
            if (!"applovin".equalsIgnoreCase(scheme) || !"com.applovin.sdk".equalsIgnoreCase(host) || AppLovinWebViewActivity.this.f4608b == null) {
                return super.shouldOverrideUrlLoading(webView, str);
            }
            if (!path.endsWith("webview_event")) {
                return true;
            }
            Set<String> queryParameterNames = parse.getQueryParameterNames();
            String str2 = queryParameterNames.isEmpty() ? "" : (String) queryParameterNames.toArray()[0];
            if (m.b(str2)) {
                String queryParameter = parse.getQueryParameter(str2);
                q logger2 = this.f4609a.getLogger();
                logger2.b("AppLovinWebViewActivity", "Parsed WebView event parameter name: " + str2 + " and value: " + queryParameter);
                AppLovinWebViewActivity.this.f4608b.onReceivedEvent(queryParameter);
                return true;
            }
            this.f4609a.getLogger().e("AppLovinWebViewActivity", "Failed to parse WebView event parameter");
            return true;
        }
    }

    public void loadUrl(String str, EventListener eventListener) {
        this.f4608b = eventListener;
        this.f4607a.loadUrl(str);
    }

    public void onBackPressed() {
        EventListener eventListener = this.f4608b;
        if (eventListener != null) {
            eventListener.onReceivedEvent(EVENT_DISMISSED_VIA_BACK_BUTTON);
        }
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        String stringExtra = getIntent().getStringExtra(INTENT_EXTRA_KEY_SDK_KEY);
        if (!TextUtils.isEmpty(stringExtra)) {
            AppLovinSdk instance = AppLovinSdk.getInstance(stringExtra, new AppLovinSdkSettings(), getApplicationContext());
            this.f4607a = new WebView(this);
            setContentView(this.f4607a);
            WebSettings settings = this.f4607a.getSettings();
            settings.setSupportMultipleWindows(false);
            settings.setJavaScriptEnabled(true);
            this.f4607a.setVerticalScrollBarEnabled(true);
            this.f4607a.setHorizontalScrollBarEnabled(true);
            this.f4607a.setScrollBarStyle(33554432);
            this.f4607a.setWebViewClient(new a(instance));
            if (getIntent().getBooleanExtra(INTENT_EXTRA_KEY_IMMERSIVE_MODE_ON, false)) {
                getWindow().getDecorView().setSystemUiVisibility(5894);
                return;
            }
            return;
        }
        throw new IllegalArgumentException("No SDK key specified");
    }
}
