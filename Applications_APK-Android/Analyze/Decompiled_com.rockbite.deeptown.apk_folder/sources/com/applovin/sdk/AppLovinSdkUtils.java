package com.applovin.sdk;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.TypedValue;
import android.widget.ImageView;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.p;
import java.io.File;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class AppLovinSdkUtils {

    /* renamed from: a  reason: collision with root package name */
    private static final Handler f4606a = new Handler(Looper.getMainLooper());

    private static void a(ImageView imageView) {
        if (imageView != null) {
            Drawable drawable = imageView.getDrawable();
            if (drawable instanceof BitmapDrawable) {
                ((BitmapDrawable) drawable).getBitmap().recycle();
            }
        }
    }

    public static int dpToPx(Context context, int i2) {
        return (int) TypedValue.applyDimension(1, (float) i2, context.getResources().getDisplayMetrics());
    }

    public static boolean isTablet(Context context) {
        Configuration configuration = context.getResources().getConfiguration();
        return configuration != null && configuration.smallestScreenWidthDp >= 600;
    }

    public static boolean isValidString(String str) {
        return !TextUtils.isEmpty(str);
    }

    public static int pxToDp(Context context, int i2) {
        return (int) Math.ceil((double) (((float) i2) / context.getResources().getDisplayMetrics().density));
    }

    public static void runOnUiThread(Runnable runnable) {
        runOnUiThread(false, runnable);
    }

    public static void runOnUiThread(boolean z, Runnable runnable) {
        if (z || !p.b()) {
            f4606a.post(runnable);
        } else {
            runnable.run();
        }
    }

    public static void runOnUiThreadDelayed(Runnable runnable, long j2) {
        if (j2 > 0) {
            f4606a.postDelayed(runnable, j2);
        } else if (p.b()) {
            runnable.run();
        } else {
            f4606a.post(runnable);
        }
    }

    public static void safePopulateImageView(Context context, ImageView imageView, int i2, int i3) {
        a(imageView);
        Bitmap a2 = p.a(context, i2, i3);
        if (a2 != null) {
            imageView.setImageBitmap(a2);
        }
    }

    public static void safePopulateImageView(ImageView imageView, Bitmap bitmap) {
        a(imageView);
        if (imageView != null && bitmap != null) {
            imageView.setImageBitmap(bitmap);
        }
    }

    public static void safePopulateImageView(ImageView imageView, Uri uri, int i2) {
        a(imageView);
        Bitmap a2 = p.a(new File(uri.getPath()), i2);
        if (a2 != null) {
            imageView.setImageBitmap(a2);
        }
    }

    public static Map<String, String> toMap(JSONObject jSONObject) throws JSONException {
        return i.a(jSONObject);
    }
}
