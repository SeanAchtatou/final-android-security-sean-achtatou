package com.applovin.nativeAds;

public interface AppLovinNativeAdPrecacheListener {
    void onNativeAdImagePrecachingFailed(AppLovinNativeAd appLovinNativeAd, int i2);

    void onNativeAdImagesPrecached(AppLovinNativeAd appLovinNativeAd);

    void onNativeAdVideoPrecachingFailed(AppLovinNativeAd appLovinNativeAd, int i2);

    void onNativeAdVideoPreceached(AppLovinNativeAd appLovinNativeAd);
}
