package com.applovin.nativeAds;

import java.util.List;

public interface AppLovinNativeAdLoadListener {
    void onNativeAdsFailedToLoad(int i2);

    void onNativeAdsLoaded(List<AppLovinNativeAd> list);
}
