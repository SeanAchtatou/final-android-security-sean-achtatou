package com.applovin.impl.communicator;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.applovin.communicator.AppLovinCommunicatorMessage;
import com.applovin.communicator.AppLovinCommunicatorSubscriber;
import com.applovin.impl.sdk.q;
import java.lang.ref.WeakReference;
import java.util.LinkedHashSet;
import java.util.Set;

public class b extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private boolean f3453a = true;

    /* renamed from: b  reason: collision with root package name */
    private final String f3454b;

    /* renamed from: c  reason: collision with root package name */
    private final WeakReference<AppLovinCommunicatorSubscriber> f3455c;

    /* renamed from: d  reason: collision with root package name */
    private final Set<CommunicatorMessageImpl> f3456d = new LinkedHashSet();

    /* renamed from: e  reason: collision with root package name */
    private final Object f3457e = new Object();

    b(String str, AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber) {
        this.f3454b = str;
        this.f3455c = new WeakReference<>(appLovinCommunicatorSubscriber);
    }

    public String a() {
        return this.f3454b;
    }

    public void a(boolean z) {
        this.f3453a = z;
    }

    public AppLovinCommunicatorSubscriber b() {
        return this.f3455c.get();
    }

    public boolean c() {
        return this.f3453a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        if (a().equals(bVar.a())) {
            if (this.f3455c.get() != null) {
                if (this.f3455c.get().equals(bVar.f3455c.get())) {
                    return true;
                }
            } else if (this.f3455c.get() == bVar.f3455c.get()) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        return (this.f3454b.hashCode() * 31) + (this.f3455c.get() != null ? this.f3455c.get().hashCode() : 0);
    }

    public void onReceive(Context context, Intent intent) {
        if (b() == null) {
            q.i("AppLovinCommunicator", "Message received for GC'd subscriber");
            return;
        }
        CommunicatorMessageImpl communicatorMessageImpl = (CommunicatorMessageImpl) intent;
        boolean z = false;
        synchronized (this.f3457e) {
            if (!this.f3456d.contains(communicatorMessageImpl)) {
                this.f3456d.add(communicatorMessageImpl);
                z = true;
            }
        }
        if (z) {
            b().onMessageReceived((AppLovinCommunicatorMessage) communicatorMessageImpl);
        }
    }
}
