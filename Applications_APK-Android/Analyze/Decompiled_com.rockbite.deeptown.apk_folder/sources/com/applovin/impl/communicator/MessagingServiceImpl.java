package com.applovin.impl.communicator;

import android.content.Context;
import com.applovin.communicator.AppLovinCommunicatorMessage;
import com.applovin.communicator.AppLovinCommunicatorMessagingService;
import com.applovin.impl.sdk.AppLovinBroadcastManager;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;

public class MessagingServiceImpl implements AppLovinCommunicatorMessagingService {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final Context f3444a;

    /* renamed from: b  reason: collision with root package name */
    private final ScheduledThreadPoolExecutor f3445b;

    /* renamed from: c  reason: collision with root package name */
    private final Queue<CommunicatorMessageImpl> f3446c = new LinkedList();

    /* renamed from: d  reason: collision with root package name */
    private final Object f3447d = new Object();

    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ CommunicatorMessageImpl f3448a;

        a(CommunicatorMessageImpl communicatorMessageImpl) {
            this.f3448a = communicatorMessageImpl;
        }

        public void run() {
            AppLovinBroadcastManager.getInstance(MessagingServiceImpl.this.f3444a).sendBroadcastSync(this.f3448a);
        }
    }

    class b implements ThreadFactory {
        b(MessagingServiceImpl messagingServiceImpl) {
        }

        public Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable, "AppLovinSdk:com.applovin.communicator");
            thread.setPriority(10);
            thread.setDaemon(true);
            return thread;
        }
    }

    public MessagingServiceImpl(Context context) {
        this.f3444a = context;
        this.f3445b = a();
    }

    private Queue<CommunicatorMessageImpl> a(String str) {
        LinkedList linkedList;
        synchronized (this.f3447d) {
            linkedList = new LinkedList();
            for (CommunicatorMessageImpl next : this.f3446c) {
                if (next.a() && next.getTopic().equals(str)) {
                    linkedList.add(next);
                }
            }
        }
        return linkedList;
    }

    private ScheduledThreadPoolExecutor a() {
        return new ScheduledThreadPoolExecutor(16, new b(this));
    }

    private void a(CommunicatorMessageImpl communicatorMessageImpl) {
        this.f3445b.execute(new a(communicatorMessageImpl));
    }

    public void maybeFlushStickyMessages(String str) {
        for (CommunicatorMessageImpl a2 : a(str)) {
            a(a2);
        }
    }

    public void publish(AppLovinCommunicatorMessage appLovinCommunicatorMessage) {
        a(appLovinCommunicatorMessage);
        synchronized (this.f3447d) {
            this.f3446c.add(appLovinCommunicatorMessage);
        }
    }

    public String toString() {
        return "MessagingServiceImpl{}";
    }
}
