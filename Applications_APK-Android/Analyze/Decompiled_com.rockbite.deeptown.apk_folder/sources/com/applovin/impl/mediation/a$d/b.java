package com.applovin.impl.mediation.a$d;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.applovin.impl.mediation.a;
import com.applovin.sdk.c;
import java.util.ArrayList;
import java.util.List;

public abstract class b extends BaseAdapter implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final LayoutInflater f3514a;

    /* renamed from: b  reason: collision with root package name */
    protected final Context f3515b;

    /* renamed from: c  reason: collision with root package name */
    protected final List<a.b.c> f3516c = new ArrayList();

    protected b(Context context) {
        this.f3515b = context;
        this.f3514a = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    /* renamed from: a */
    public a.b.c getItem(int i2) {
        return this.f3516c.get(i2);
    }

    /* access modifiers changed from: protected */
    public abstract void a(a.b.c cVar);

    public boolean areAllItemsEnabled() {
        return false;
    }

    public int getCount() {
        return this.f3516c.size();
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    public int getItemViewType(int i2) {
        return getItem(i2).d();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i2, View view, ViewGroup viewGroup) {
        a.b.C0083b bVar;
        a.b.c a2 = getItem(i2);
        if (view == null) {
            view = this.f3514a.inflate(a2.e(), viewGroup, false);
            bVar = new a.b.C0083b();
            bVar.f3544a = (TextView) view.findViewById(16908308);
            bVar.f3545b = (TextView) view.findViewById(16908309);
            bVar.f3546c = (ImageView) view.findViewById(c.imageView);
            view.setTag(bVar);
            view.setOnClickListener(this);
        } else {
            bVar = (a.b.C0083b) view.getTag();
        }
        bVar.a(a2);
        view.setEnabled(a2.a());
        return view;
    }

    public int getViewTypeCount() {
        return a.b.c.h();
    }

    public boolean isEnabled(int i2) {
        return getItem(i2).a();
    }

    public void onClick(View view) {
        a(((a.b.C0083b) view.getTag()).a());
    }
}
