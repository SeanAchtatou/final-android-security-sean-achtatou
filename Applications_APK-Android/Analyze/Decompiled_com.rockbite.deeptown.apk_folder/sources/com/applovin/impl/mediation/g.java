package com.applovin.impl.mediation;

import android.os.Bundle;

public class g {

    /* renamed from: a  reason: collision with root package name */
    private final Bundle f3713a;

    public static class b {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final Bundle f3714a = new Bundle();

        public b a(String str) {
            if (str != null) {
                this.f3714a.remove(str);
                return this;
            }
            throw new IllegalArgumentException("No key specified.");
        }

        public b a(String str, String str2) {
            if (str != null) {
                this.f3714a.putString(str, str2);
                return this;
            }
            throw new IllegalArgumentException("No key specified");
        }

        public g a() {
            return new g(this);
        }
    }

    private g(b bVar) {
        this.f3713a = bVar.f3714a;
    }

    public Bundle a() {
        return this.f3713a;
    }

    public String toString() {
        return "MediatedRequestParameters{extraParameters=" + this.f3713a + '}';
    }
}
