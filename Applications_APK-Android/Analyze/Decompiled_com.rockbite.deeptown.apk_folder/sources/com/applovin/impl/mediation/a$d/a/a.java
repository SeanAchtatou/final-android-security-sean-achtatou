package com.applovin.impl.mediation.a$d.a;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.SpannedString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.applovin.impl.mediation.a;
import com.applovin.impl.mediation.a$d.a.b;
import com.applovin.impl.sdk.utils.f;
import com.applovin.mediation.MaxDebuggerDetailActivity;
import com.applovin.sdk.d;

public class a extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private b f3495a;

    /* renamed from: b  reason: collision with root package name */
    private DataSetObserver f3496b;

    /* renamed from: c  reason: collision with root package name */
    private FrameLayout f3497c;

    /* renamed from: d  reason: collision with root package name */
    private ListView f3498d;

    /* renamed from: e  reason: collision with root package name */
    private com.applovin.impl.adview.a f3499e;

    /* renamed from: com.applovin.impl.mediation.a$d.a.a$a  reason: collision with other inner class name */
    class C0075a extends DataSetObserver {
        C0075a() {
        }

        public void onChanged() {
            a.this.c();
        }
    }

    class b implements b.C0077b {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ com.applovin.impl.sdk.b f3501a;

        /* renamed from: com.applovin.impl.mediation.a$d.a.a$b$a  reason: collision with other inner class name */
        class C0076a extends com.applovin.impl.sdk.utils.a {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ a.b.d f3503a;

            C0076a(a.b.d dVar) {
                this.f3503a = dVar;
            }

            public void onActivityDestroyed(Activity activity) {
                if (activity instanceof com.applovin.impl.mediation.a$d.c.a) {
                    b.this.f3501a.b(this);
                }
            }

            public void onActivityStarted(Activity activity) {
                if (activity instanceof com.applovin.impl.mediation.a$d.c.a) {
                    ((com.applovin.impl.mediation.a$d.c.a) activity).setNetwork(this.f3503a);
                }
            }
        }

        b(com.applovin.impl.sdk.b bVar) {
            this.f3501a = bVar;
        }

        public void a(a.b.d dVar) {
            this.f3501a.a(new C0076a(dVar));
            a.this.a();
        }
    }

    public class c extends a.b.c {

        /* renamed from: d  reason: collision with root package name */
        private final a.b.d f3505d;

        /* renamed from: e  reason: collision with root package name */
        private final Context f3506e;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(a.b.d dVar, Context context) {
            super(dVar.a() == a.b.d.C0085a.MISSING ? a.b.c.C0084a.SIMPLE : a.b.c.C0084a.DETAIL);
            this.f3505d = dVar;
            this.f3506e = context;
        }

        private SpannedString a(String str, int i2) {
            return a(str, i2, 16);
        }

        private SpannedString a(String str, int i2, int i3) {
            SpannableString spannableString = new SpannableString(str);
            spannableString.setSpan(new ForegroundColorSpan(i2), 0, spannableString.length(), 33);
            spannableString.setSpan(new AbsoluteSizeSpan(i3, true), 0, spannableString.length(), 33);
            return new SpannedString(spannableString);
        }

        private SpannedString j() {
            int i2;
            String str;
            if (this.f3505d.b()) {
                if (!TextUtils.isEmpty(this.f3505d.e())) {
                    str = "SDK " + this.f3505d.e();
                } else {
                    str = "SDK Found";
                }
                i2 = -7829368;
            } else {
                i2 = -65536;
                str = "SDK Missing";
            }
            return a(str, i2);
        }

        private SpannedString k() {
            int i2;
            String str;
            if (this.f3505d.c()) {
                if (!TextUtils.isEmpty(this.f3505d.f())) {
                    str = "Adapter " + this.f3505d.f();
                } else {
                    str = "Adapter Found";
                }
                i2 = -7829368;
            } else {
                i2 = -65536;
                str = "Adapter Missing";
            }
            return a(str, i2);
        }

        private SpannedString l() {
            return a("Invalid Integration", -65536);
        }

        public boolean a() {
            return this.f3505d.a() != a.b.d.C0085a.MISSING;
        }

        public SpannedString b() {
            SpannedString spannedString = this.f3549b;
            if (spannedString != null) {
                return spannedString;
            }
            this.f3549b = a(this.f3505d.d(), this.f3505d.a() == a.b.d.C0085a.MISSING ? -7829368 : -16777216, 18);
            return this.f3549b;
        }

        public SpannedString c() {
            SpannedString spannedString = this.f3550c;
            if (spannedString != null) {
                return spannedString;
            }
            if (this.f3505d.a() != a.b.d.C0085a.MISSING) {
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
                spannableStringBuilder.append((CharSequence) j());
                spannableStringBuilder.append((CharSequence) a(", ", -7829368));
                spannableStringBuilder.append((CharSequence) k());
                if (this.f3505d.a() == a.b.d.C0085a.INVALID_INTEGRATION) {
                    spannableStringBuilder.append((CharSequence) new SpannableString("\n"));
                    spannableStringBuilder.append((CharSequence) l());
                }
                this.f3550c = new SpannedString(spannableStringBuilder);
            } else {
                this.f3550c = new SpannedString("");
            }
            return this.f3550c;
        }

        public int f() {
            return a() ? com.applovin.sdk.b.applovin_ic_disclosure_arrow : super.f();
        }

        public int g() {
            return f.a(com.applovin.sdk.a.applovin_sdk_disclosureButtonColor, this.f3506e);
        }

        public a.b.d i() {
            return this.f3505d;
        }

        public String toString() {
            return "MediatedNetworkListItemViewModel{text=" + ((Object) this.f3549b) + ", detailText=" + ((Object) this.f3550c) + ", network=" + this.f3505d + "}";
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        startActivity(new Intent(this, MaxDebuggerDetailActivity.class));
    }

    private void b() {
        c();
        this.f3499e = new com.applovin.impl.adview.a(this, 50, 16842874);
        this.f3499e.setColor(-3355444);
        this.f3497c.addView(this.f3499e, new FrameLayout.LayoutParams(-1, -1, 17));
        this.f3497c.bringChildToFront(this.f3499e);
        this.f3499e.a();
    }

    /* access modifiers changed from: private */
    public void c() {
        com.applovin.impl.adview.a aVar = this.f3499e;
        if (aVar != null) {
            aVar.b();
            this.f3497c.removeView(this.f3499e);
            this.f3499e = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle("MAX Mediation Debugger");
        setContentView(d.mediation_debugger_activity);
        this.f3497c = (FrameLayout) findViewById(16908290);
        this.f3498d = (ListView) findViewById(com.applovin.sdk.c.listView);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.f3495a.unregisterDataSetObserver(this.f3496b);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.f3498d.setAdapter((ListAdapter) this.f3495a);
        if (!this.f3495a.a()) {
            b();
        }
    }

    public void setListAdapter(b bVar, com.applovin.impl.sdk.b bVar2) {
        DataSetObserver dataSetObserver;
        b bVar3 = this.f3495a;
        if (!(bVar3 == null || (dataSetObserver = this.f3496b) == null)) {
            bVar3.unregisterDataSetObserver(dataSetObserver);
        }
        this.f3495a = bVar;
        this.f3496b = new C0075a();
        this.f3495a.registerDataSetObserver(this.f3496b);
        this.f3495a.a(new b(bVar2));
    }
}
