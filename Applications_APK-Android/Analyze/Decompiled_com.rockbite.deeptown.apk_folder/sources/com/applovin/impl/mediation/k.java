package com.applovin.impl.mediation;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.applovin.impl.mediation.b;
import com.applovin.impl.mediation.g;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.n;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.MaxRewardedAdListener;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class k {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final List<c> f3807a = Collections.synchronizedList(new ArrayList());

    /* renamed from: b  reason: collision with root package name */
    private final b f3808b;

    /* renamed from: c  reason: collision with root package name */
    private final b f3809c;

    private static class b extends BroadcastReceiver implements MaxAdListener, MaxRewardedAdListener {

        /* renamed from: a  reason: collision with root package name */
        private final com.applovin.impl.sdk.k f3810a;

        /* renamed from: b  reason: collision with root package name */
        private final k f3811b;

        /* renamed from: c  reason: collision with root package name */
        private final MaxAdFormat f3812c;

        /* renamed from: d  reason: collision with root package name */
        private final c.e<String> f3813d;
        /* access modifiers changed from: private */

        /* renamed from: e  reason: collision with root package name */
        public MaxAdListener f3814e;
        /* access modifiers changed from: private */

        /* renamed from: f  reason: collision with root package name */
        public b.d f3815f;

        /* renamed from: g  reason: collision with root package name */
        private final Object f3816g;

        /* renamed from: h  reason: collision with root package name */
        private n f3817h;

        /* renamed from: i  reason: collision with root package name */
        private long f3818i;

        /* renamed from: j  reason: collision with root package name */
        private final AtomicBoolean f3819j;

        /* renamed from: k  reason: collision with root package name */
        private volatile boolean f3820k;

        class a implements Runnable {
            a() {
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.applovin.impl.mediation.k.b.a(com.applovin.impl.mediation.k$b, boolean):void
             arg types: [com.applovin.impl.mediation.k$b, int]
             candidates:
              com.applovin.impl.mediation.k.b.a(com.applovin.impl.mediation.k$b, com.applovin.mediation.MaxAdListener):com.applovin.mediation.MaxAdListener
              com.applovin.impl.mediation.k.b.a(com.applovin.impl.mediation.k$b, boolean):void */
            public void run() {
                b.this.b(true);
            }
        }

        /* renamed from: com.applovin.impl.mediation.k$b$b  reason: collision with other inner class name */
        class C0096b implements Runnable {
            C0096b() {
            }

            public void run() {
                b.this.b();
            }
        }

        private b(c.e<String> eVar, MaxAdFormat maxAdFormat, k kVar, com.applovin.impl.sdk.k kVar2) {
            this.f3816g = new Object();
            this.f3819j = new AtomicBoolean();
            this.f3811b = kVar;
            this.f3810a = kVar2;
            this.f3813d = eVar;
            this.f3812c = maxAdFormat;
            kVar2.D().registerReceiver(this, new IntentFilter("com.applovin.application_paused"));
            kVar2.D().registerReceiver(this, new IntentFilter("com.applovin.application_resumed"));
        }

        private void a(long j2) {
            if (j2 > 0) {
                this.f3818i = System.currentTimeMillis() + j2;
                this.f3817h = n.a(j2, this.f3810a, new a());
            }
        }

        private void a(boolean z) {
            if (this.f3810a.x().a()) {
                this.f3820k = z;
                this.f3819j.set(true);
                return;
            }
            String str = (String) this.f3810a.a(this.f3813d);
            if (m.b(str)) {
                g.b bVar = new g.b();
                bVar.a("fa", String.valueOf(true));
                bVar.a("faie", String.valueOf(z));
                this.f3810a.c0().loadAd(str, this.f3812c, bVar.a(), true, this.f3810a.E(), this);
            }
        }

        /* access modifiers changed from: private */
        public void b() {
            a(false);
        }

        /* access modifiers changed from: private */
        public void b(boolean z) {
            synchronized (this.f3816g) {
                this.f3818i = 0;
                c();
                this.f3815f = null;
            }
            a(z);
        }

        private void c() {
            synchronized (this.f3816g) {
                if (this.f3817h != null) {
                    this.f3817h.d();
                    this.f3817h = null;
                }
            }
        }

        public void a() {
            if (this.f3819j.compareAndSet(true, false)) {
                a(this.f3820k);
                return;
            }
            long j2 = this.f3818i;
            if (j2 != 0) {
                long currentTimeMillis = j2 - System.currentTimeMillis();
                if (currentTimeMillis <= 0) {
                    b(true);
                } else {
                    a(currentTimeMillis);
                }
            }
        }

        public void onAdClicked(MaxAd maxAd) {
            this.f3814e.onAdClicked(maxAd);
        }

        public void onAdDisplayFailed(MaxAd maxAd, int i2) {
            this.f3814e.onAdDisplayFailed(maxAd, i2);
        }

        public void onAdDisplayed(MaxAd maxAd) {
            this.f3814e.onAdDisplayed(maxAd);
            b(false);
        }

        public void onAdHidden(MaxAd maxAd) {
            this.f3814e.onAdHidden(maxAd);
            this.f3814e = null;
        }

        public void onAdLoadFailed(String str, int i2) {
            AppLovinSdkUtils.runOnUiThreadDelayed(new C0096b(), TimeUnit.SECONDS.toMillis(((Long) this.f3810a.a(c.d.J4)).longValue()));
        }

        public void onAdLoaded(MaxAd maxAd) {
            this.f3815f = (b.d) maxAd;
            a(this.f3815f.z());
            Iterator it = new ArrayList(this.f3811b.f3807a).iterator();
            while (it.hasNext()) {
                ((c) it.next()).a(this.f3815f);
            }
        }

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if ("com.applovin.application_paused".equals(action)) {
                c();
            } else if ("com.applovin.application_resumed".equals(action)) {
                a();
            }
        }

        public void onRewardedVideoCompleted(MaxAd maxAd) {
            MaxAdListener maxAdListener = this.f3814e;
            if (maxAdListener instanceof MaxRewardedAdListener) {
                ((MaxRewardedAdListener) maxAdListener).onRewardedVideoCompleted(maxAd);
            }
        }

        public void onRewardedVideoStarted(MaxAd maxAd) {
            MaxAdListener maxAdListener = this.f3814e;
            if (maxAdListener instanceof MaxRewardedAdListener) {
                ((MaxRewardedAdListener) maxAdListener).onRewardedVideoStarted(maxAd);
            }
        }

        public void onUserRewarded(MaxAd maxAd, MaxReward maxReward) {
            MaxAdListener maxAdListener = this.f3814e;
            if (maxAdListener instanceof MaxRewardedAdListener) {
                ((MaxRewardedAdListener) maxAdListener).onUserRewarded(maxAd, maxReward);
            }
        }
    }

    public interface c {
        void a(b.d dVar);
    }

    public k(com.applovin.impl.sdk.k kVar) {
        this.f3808b = new b(c.d.G4, MaxAdFormat.INTERSTITIAL, this, kVar);
        this.f3809c = new b(c.d.H4, MaxAdFormat.REWARDED, this, kVar);
    }

    private b b(MaxAdFormat maxAdFormat) {
        if (MaxAdFormat.INTERSTITIAL == maxAdFormat) {
            return this.f3808b;
        }
        if (MaxAdFormat.REWARDED == maxAdFormat) {
            return this.f3809c;
        }
        return null;
    }

    public b.d a(MaxAdFormat maxAdFormat) {
        b b2 = b(maxAdFormat);
        if (b2 != null) {
            return b2.f3815f;
        }
        return null;
    }

    public void a() {
        this.f3808b.b();
        this.f3809c.b();
    }

    public void a(c cVar) {
        this.f3807a.add(cVar);
    }

    public void a(MaxAdListener maxAdListener, MaxAdFormat maxAdFormat) {
        b b2 = b(maxAdFormat);
        if (b2 != null) {
            MaxAdListener unused = b2.f3814e = maxAdListener;
        }
    }

    public void b(c cVar) {
        this.f3807a.remove(cVar);
    }
}
