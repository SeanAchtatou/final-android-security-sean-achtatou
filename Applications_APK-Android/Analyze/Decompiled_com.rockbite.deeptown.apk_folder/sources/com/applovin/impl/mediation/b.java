package com.applovin.impl.mediation;

import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import com.applovin.impl.mediation.a;
import com.applovin.impl.mediation.c;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.p;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinPrivacySettings;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class b implements a.C0081a, c.b {

    /* renamed from: a  reason: collision with root package name */
    private final a f3632a;

    /* renamed from: b  reason: collision with root package name */
    private final c f3633b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public final MaxAdListener f3634c;

    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ d f3635a;

        a(d dVar) {
            this.f3635a = dVar;
        }

        public void run() {
            b.this.f3634c.onAdHidden(this.f3635a);
        }
    }

    /* renamed from: com.applovin.impl.mediation.b$b  reason: collision with other inner class name */
    public abstract class C0087b extends f implements MaxAd {

        /* renamed from: g  reason: collision with root package name */
        private final AtomicBoolean f3637g = new AtomicBoolean();

        /* renamed from: h  reason: collision with root package name */
        protected j f3638h;

        protected C0087b(JSONObject jSONObject, JSONObject jSONObject2, j jVar, k kVar) {
            super(jSONObject, jSONObject2, kVar);
            this.f3638h = jVar;
        }

        private long x() {
            return b("load_started_time_ms", 0);
        }

        public abstract C0087b a(j jVar);

        public String getAdUnitId() {
            return a("ad_unit_id", "");
        }

        public MaxAdFormat getFormat() {
            return p.c(a("ad_format", (String) null));
        }

        public boolean o() {
            j jVar = this.f3638h;
            return jVar != null && jVar.c() && this.f3638h.d();
        }

        public String p() {
            return a("event_id", "");
        }

        public j q() {
            return this.f3638h;
        }

        public String r() {
            return b("bid_response", (String) null);
        }

        public String s() {
            return b("third_party_ad_placement_id", (String) null);
        }

        public long t() {
            if (x() > 0) {
                return SystemClock.elapsedRealtime() - x();
            }
            return -1;
        }

        public void u() {
            c("load_started_time_ms", SystemClock.elapsedRealtime());
        }

        public AtomicBoolean v() {
            return this.f3637g;
        }

        public void w() {
            this.f3638h = null;
        }
    }

    public class c extends C0087b {

        /* renamed from: i  reason: collision with root package name */
        private static final int f3639i = AppLovinAdSize.BANNER.getHeight();

        /* renamed from: j  reason: collision with root package name */
        private static final int f3640j = AppLovinAdSize.LEADER.getHeight();

        private c(c cVar, j jVar) {
            super(cVar.l(), cVar.k(), jVar, cVar.f3644a);
        }

        public c(JSONObject jSONObject, JSONObject jSONObject2, k kVar) {
            super(jSONObject, jSONObject2, null, kVar);
        }

        public long A() {
            return b("viewability_imp_delay_ms", ((Long) this.f3644a.a(c.e.b1)).longValue());
        }

        public int B() {
            return a("viewability_min_width", ((Integer) this.f3644a.a(getFormat() == MaxAdFormat.BANNER ? c.e.c1 : getFormat() == MaxAdFormat.MREC ? c.e.e1 : c.e.g1)).intValue());
        }

        public int C() {
            return a("viewability_min_height", ((Integer) this.f3644a.a(getFormat() == MaxAdFormat.BANNER ? c.e.d1 : getFormat() == MaxAdFormat.MREC ? c.e.f1 : c.e.h1)).intValue());
        }

        public float D() {
            return a("viewability_min_alpha", ((Float) this.f3644a.a(c.e.i1)).floatValue() / 100.0f);
        }

        public int E() {
            return a("viewability_min_pixels", -1);
        }

        public boolean F() {
            return E() >= 0;
        }

        public long G() {
            return b("viewability_timer_min_visible_ms", ((Long) this.f3644a.a(c.e.j1)).longValue());
        }

        public boolean H() {
            return b("proe", (Boolean) this.f3644a.a(c.d.F4));
        }

        public long I() {
            return p.f(b("bg_color", (String) null));
        }

        public C0087b a(j jVar) {
            return new c(this, jVar);
        }

        public int x() {
            int a2 = a("ad_view_width", ((Integer) this.f3644a.a(c.d.i4)).intValue());
            return a2 == -2 ? AppLovinSdkUtils.isTablet(this.f3644a.d()) ? 728 : 320 : a2;
        }

        public int y() {
            int a2 = a("ad_view_height", ((Integer) this.f3644a.a(c.d.j4)).intValue());
            return a2 == -2 ? AppLovinSdkUtils.isTablet(this.f3644a.d()) ? f3640j : f3639i : a2;
        }

        public View z() {
            j jVar;
            if (!o() || (jVar = this.f3638h) == null) {
                return null;
            }
            View a2 = jVar.a();
            if (a2 != null) {
                return a2;
            }
            throw new IllegalStateException("Ad-view based ad is missing an ad view");
        }
    }

    public class d extends C0087b {

        /* renamed from: i  reason: collision with root package name */
        private String f3641i;

        /* renamed from: j  reason: collision with root package name */
        private final AtomicReference<com.applovin.impl.sdk.a.c> f3642j;

        /* renamed from: k  reason: collision with root package name */
        private final AtomicBoolean f3643k;

        private d(d dVar, j jVar) {
            super(dVar.l(), dVar.k(), jVar, dVar.f3644a);
            this.f3642j = dVar.f3642j;
            this.f3643k = dVar.f3643k;
        }

        public d(JSONObject jSONObject, JSONObject jSONObject2, k kVar) {
            super(jSONObject, jSONObject2, null, kVar);
            this.f3642j = new AtomicReference<>();
            this.f3643k = new AtomicBoolean();
        }

        public String A() {
            return this.f3641i;
        }

        public long B() {
            long b2 = b("ad_expiration_ms", -1);
            return b2 >= 0 ? b2 : a("ad_expiration_ms", ((Long) this.f3644a.a(c.d.z4)).longValue());
        }

        public long C() {
            long b2 = b("ad_hidden_timeout_ms", -1);
            return b2 >= 0 ? b2 : a("ad_hidden_timeout_ms", ((Long) this.f3644a.a(c.d.C4)).longValue());
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.applovin.impl.mediation.b.f.b(java.lang.String, java.lang.Boolean):boolean
         arg types: [java.lang.String, int]
         candidates:
          com.applovin.impl.mediation.b.f.b(java.lang.String, long):long
          com.applovin.impl.mediation.b.f.b(java.lang.String, java.lang.String):java.lang.String
          com.applovin.impl.mediation.b.f.b(java.lang.String, org.json.JSONArray):org.json.JSONArray
          com.applovin.impl.mediation.b.f.b(java.lang.String, java.lang.Boolean):boolean */
        public boolean D() {
            if (b("schedule_ad_hidden_on_ad_dismiss", (Boolean) false)) {
                return true;
            }
            return a("schedule_ad_hidden_on_ad_dismiss", (Boolean) this.f3644a.a(c.d.D4));
        }

        public long E() {
            long b2 = b("ad_hidden_on_ad_dismiss_callback_delay_ms", -1);
            return b2 >= 0 ? b2 : a("ad_hidden_on_ad_dismiss_callback_delay_ms", ((Long) this.f3644a.a(c.d.E4)).longValue());
        }

        public String F() {
            return b("bcode", "");
        }

        public String G() {
            return a("mcode", "");
        }

        public boolean H() {
            return this.f3643k.get();
        }

        public void I() {
            this.f3643k.set(true);
        }

        public com.applovin.impl.sdk.a.c J() {
            return this.f3642j.getAndSet(null);
        }

        public C0087b a(j jVar) {
            return new d(this, jVar);
        }

        public void a(com.applovin.impl.sdk.a.c cVar) {
            this.f3642j.set(cVar);
        }

        public void d(String str) {
            this.f3641i = str;
        }

        public String toString() {
            return "MediatedFullscreenAd{format=" + getFormat() + ", adUnitId=" + getAdUnitId() + ", isReady=" + o() + ", adapterClass='" + m() + "', adapterName='" + n() + "', isTesting=" + a() + ", isRefreshEnabled=" + e() + ", getAdRefreshMillis=" + f() + '}';
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.applovin.impl.mediation.b.f.b(java.lang.String, java.lang.Boolean):boolean
         arg types: [java.lang.String, int]
         candidates:
          com.applovin.impl.mediation.b.f.b(java.lang.String, long):long
          com.applovin.impl.mediation.b.f.b(java.lang.String, java.lang.String):java.lang.String
          com.applovin.impl.mediation.b.f.b(java.lang.String, org.json.JSONArray):org.json.JSONArray
          com.applovin.impl.mediation.b.f.b(java.lang.String, java.lang.Boolean):boolean */
        public boolean x() {
            return b("fa", (Boolean) false);
        }

        public long y() {
            return b("ifacd_ms", -1);
        }

        public long z() {
            return b("fard_ms", TimeUnit.HOURS.toMillis(1));
        }
    }

    public class e extends C0087b {
        private e(e eVar, j jVar) {
            super(eVar.l(), eVar.k(), jVar, eVar.f3644a);
        }

        public e(JSONObject jSONObject, JSONObject jSONObject2, k kVar) {
            super(jSONObject, jSONObject2, null, kVar);
        }

        public C0087b a(j jVar) {
            return new e(this, jVar);
        }

        public String toString() {
            return "MediatedNativeAd{format=" + getFormat() + ", adUnitId=" + getAdUnitId() + ", isReady=" + o() + ", adapterClass='" + m() + "', adapterName='" + n() + "', isTesting=" + a() + ", isRefreshEnabled=" + e() + ", getAdRefreshMillis=" + f() + '}';
        }
    }

    public class f {

        /* renamed from: a  reason: collision with root package name */
        protected final k f3644a;

        /* renamed from: b  reason: collision with root package name */
        private final JSONObject f3645b;

        /* renamed from: c  reason: collision with root package name */
        private final JSONObject f3646c;

        /* renamed from: d  reason: collision with root package name */
        private final Object f3647d = new Object();

        /* renamed from: e  reason: collision with root package name */
        private final Object f3648e = new Object();

        /* renamed from: f  reason: collision with root package name */
        private volatile String f3649f;

        public f(JSONObject jSONObject, JSONObject jSONObject2, k kVar) {
            if (kVar == null) {
                throw new IllegalArgumentException("No sdk specified");
            } else if (jSONObject2 == null) {
                throw new IllegalArgumentException("No full response specified");
            } else if (jSONObject != null) {
                this.f3644a = kVar;
                this.f3645b = jSONObject2;
                this.f3646c = jSONObject;
            } else {
                throw new IllegalArgumentException("No spec object specified");
            }
        }

        private List<String> a(List<String> list, Map<String, String> map) {
            Map<String, String> o = o();
            ArrayList arrayList = new ArrayList(list.size());
            Iterator<String> it = list.iterator();
            while (it.hasNext()) {
                String next = it.next();
                for (String next2 : o.keySet()) {
                    next = next.replace(next2, f(o.get(next2)));
                }
                for (String next3 : map.keySet()) {
                    next = next.replace(next3, map.get(next3));
                }
                arrayList.add(next);
            }
            return arrayList;
        }

        private List<String> d(String str) {
            try {
                return i.b(a(str, new JSONArray()));
            } catch (JSONException unused) {
                return Collections.EMPTY_LIST;
            }
        }

        private List<String> e(String str) {
            try {
                return i.b(b(str, new JSONArray()));
            } catch (JSONException unused) {
                return Collections.EMPTY_LIST;
            }
        }

        private String f(String str) {
            String b2 = b(str, "");
            return m.b(b2) ? b2 : a(str, "");
        }

        private Map<String, String> o() {
            try {
                return i.a(new JSONObject((String) this.f3644a.a(c.d.b4)));
            } catch (JSONException unused) {
                return Collections.EMPTY_MAP;
            }
        }

        /* access modifiers changed from: protected */
        public float a(String str, float f2) {
            float a2;
            synchronized (this.f3647d) {
                a2 = i.a(this.f3646c, str, f2, this.f3644a);
            }
            return a2;
        }

        /* access modifiers changed from: protected */
        public int a(String str, int i2) {
            int b2;
            synchronized (this.f3647d) {
                b2 = i.b(this.f3646c, str, i2, this.f3644a);
            }
            return b2;
        }

        /* access modifiers changed from: protected */
        public long a(String str, long j2) {
            long a2;
            synchronized (this.f3648e) {
                a2 = i.a(this.f3645b, str, j2, this.f3644a);
            }
            return a2;
        }

        /* access modifiers changed from: protected */
        public String a(String str, String str2) {
            String b2;
            synchronized (this.f3648e) {
                b2 = i.b(this.f3645b, str, str2, this.f3644a);
            }
            return b2;
        }

        public List<String> a(String str, Map<String, String> map) {
            if (str != null) {
                List<String> d2 = d(str);
                List<String> e2 = e(str);
                ArrayList arrayList = new ArrayList(d2.size() + e2.size());
                arrayList.addAll(d2);
                arrayList.addAll(e2);
                return a(arrayList, map);
            }
            throw new IllegalArgumentException("No key specified");
        }

        /* access modifiers changed from: protected */
        public JSONArray a(String str, JSONArray jSONArray) {
            JSONArray b2;
            synchronized (this.f3648e) {
                b2 = i.b(this.f3645b, str, jSONArray, this.f3644a);
            }
            return b2;
        }

        /* access modifiers changed from: protected */
        public JSONObject a(String str, JSONObject jSONObject) {
            JSONObject b2;
            synchronized (this.f3647d) {
                b2 = i.b(this.f3646c, str, jSONObject, this.f3644a);
            }
            return b2;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.applovin.impl.mediation.b.f.b(java.lang.String, java.lang.Boolean):boolean
         arg types: [java.lang.String, int]
         candidates:
          com.applovin.impl.mediation.b.f.b(java.lang.String, long):long
          com.applovin.impl.mediation.b.f.b(java.lang.String, java.lang.String):java.lang.String
          com.applovin.impl.mediation.b.f.b(java.lang.String, org.json.JSONArray):org.json.JSONArray
          com.applovin.impl.mediation.b.f.b(java.lang.String, java.lang.Boolean):boolean */
        public boolean a() {
            return b("is_testing", (Boolean) false);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.applovin.impl.mediation.b.f.b(java.lang.String, java.lang.Boolean):boolean
         arg types: [java.lang.String, int]
         candidates:
          com.applovin.impl.mediation.b.f.b(java.lang.String, long):long
          com.applovin.impl.mediation.b.f.b(java.lang.String, java.lang.String):java.lang.String
          com.applovin.impl.mediation.b.f.b(java.lang.String, org.json.JSONArray):org.json.JSONArray
          com.applovin.impl.mediation.b.f.b(java.lang.String, java.lang.Boolean):boolean */
        public boolean a(Context context) {
            return a("huc") ? b("huc", (Boolean) false) : a("huc", Boolean.valueOf(AppLovinPrivacySettings.hasUserConsent(context)));
        }

        /* access modifiers changed from: protected */
        public boolean a(String str) {
            boolean has;
            synchronized (this.f3647d) {
                has = this.f3646c.has(str);
            }
            return has;
        }

        /* access modifiers changed from: protected */
        public boolean a(String str, Boolean bool) {
            boolean booleanValue;
            synchronized (this.f3648e) {
                booleanValue = i.a(this.f3645b, str, bool, this.f3644a).booleanValue();
            }
            return booleanValue;
        }

        /* access modifiers changed from: protected */
        public long b(String str, long j2) {
            long a2;
            synchronized (this.f3647d) {
                a2 = i.a(this.f3646c, str, j2, this.f3644a);
            }
            return a2;
        }

        /* access modifiers changed from: protected */
        public Object b(String str) {
            Object opt;
            synchronized (this.f3647d) {
                opt = this.f3646c.opt(str);
            }
            return opt;
        }

        /* access modifiers changed from: protected */
        public String b(String str, String str2) {
            String b2;
            synchronized (this.f3647d) {
                b2 = i.b(this.f3646c, str, str2, this.f3644a);
            }
            return b2;
        }

        /* access modifiers changed from: protected */
        public JSONArray b(String str, JSONArray jSONArray) {
            JSONArray b2;
            synchronized (this.f3647d) {
                b2 = i.b(this.f3646c, str, jSONArray, this.f3644a);
            }
            return b2;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.applovin.impl.mediation.b.f.b(java.lang.String, java.lang.Boolean):boolean
         arg types: [java.lang.String, int]
         candidates:
          com.applovin.impl.mediation.b.f.b(java.lang.String, long):long
          com.applovin.impl.mediation.b.f.b(java.lang.String, java.lang.String):java.lang.String
          com.applovin.impl.mediation.b.f.b(java.lang.String, org.json.JSONArray):org.json.JSONArray
          com.applovin.impl.mediation.b.f.b(java.lang.String, java.lang.Boolean):boolean */
        public boolean b() {
            return b("run_on_ui_thread", (Boolean) true);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.applovin.impl.mediation.b.f.b(java.lang.String, java.lang.Boolean):boolean
         arg types: [java.lang.String, int]
         candidates:
          com.applovin.impl.mediation.b.f.b(java.lang.String, long):long
          com.applovin.impl.mediation.b.f.b(java.lang.String, java.lang.String):java.lang.String
          com.applovin.impl.mediation.b.f.b(java.lang.String, org.json.JSONArray):org.json.JSONArray
          com.applovin.impl.mediation.b.f.b(java.lang.String, java.lang.Boolean):boolean */
        public boolean b(Context context) {
            return a("aru") ? b("aru", (Boolean) false) : a("aru", Boolean.valueOf(AppLovinPrivacySettings.isAgeRestrictedUser(context)));
        }

        /* access modifiers changed from: protected */
        public boolean b(String str, Boolean bool) {
            boolean booleanValue;
            synchronized (this.f3647d) {
                booleanValue = i.a(this.f3646c, str, bool, this.f3644a).booleanValue();
            }
            return booleanValue;
        }

        public Bundle c() {
            JSONObject a2;
            return (!(b("server_parameters") instanceof JSONObject) || (a2 = a("server_parameters", null)) == null) ? Bundle.EMPTY : i.c(a2);
        }

        public void c(String str) {
            this.f3649f = str;
        }

        /* access modifiers changed from: protected */
        public void c(String str, long j2) {
            synchronized (this.f3647d) {
                i.b(this.f3646c, str, j2, this.f3644a);
            }
        }

        public long d() {
            return b("adapter_timeout_ms", ((Long) this.f3644a.a(c.d.h4)).longValue());
        }

        public boolean e() {
            return f() >= 0;
        }

        public long f() {
            long b2 = b("ad_refresh_ms", -1);
            return b2 >= 0 ? b2 : a("ad_refresh_ms", ((Long) this.f3644a.a(c.d.k4)).longValue());
        }

        public long g() {
            long b2 = b("fullscreen_display_delay_ms", -1);
            return b2 >= 0 ? b2 : ((Long) this.f3644a.a(c.d.t4)).longValue();
        }

        public long h() {
            return b("init_completion_delay_ms", -1);
        }

        public long i() {
            return b("ahdm", ((Long) this.f3644a.a(c.d.u4)).longValue());
        }

        public String j() {
            return this.f3649f;
        }

        /* access modifiers changed from: protected */
        public JSONObject k() {
            JSONObject jSONObject;
            synchronized (this.f3648e) {
                jSONObject = this.f3645b;
            }
            return jSONObject;
        }

        /* access modifiers changed from: protected */
        public JSONObject l() {
            JSONObject jSONObject;
            synchronized (this.f3647d) {
                jSONObject = this.f3646c;
            }
            return jSONObject;
        }

        public String m() {
            return b("class", (String) null);
        }

        public String n() {
            return b("name", (String) null);
        }

        public String toString() {
            return "MediationAdapterSpec{adapterClass='" + m() + "', adapterName='" + n() + "', isTesting=" + a() + ", isRefreshEnabled=" + e() + ", getAdRefreshMillis=" + f() + '}';
        }
    }

    public class g {

        /* renamed from: a  reason: collision with root package name */
        private final h f3650a;

        /* renamed from: b  reason: collision with root package name */
        private final String f3651b;

        /* renamed from: c  reason: collision with root package name */
        private final String f3652c;

        /* renamed from: d  reason: collision with root package name */
        private final String f3653d;

        /* renamed from: e  reason: collision with root package name */
        private final String f3654e;

        public interface a {
            void a(g gVar);
        }

        private g(h hVar, j jVar, String str, String str2) {
            this.f3650a = hVar;
            this.f3654e = str2;
            if (str != null) {
                this.f3653d = str.substring(0, Math.min(str.length(), hVar.o()));
            } else {
                this.f3653d = null;
            }
            if (jVar != null) {
                this.f3651b = jVar.e();
                this.f3652c = jVar.f();
                return;
            }
            this.f3651b = null;
            this.f3652c = null;
        }

        public static g a(h hVar, j jVar, String str) {
            if (hVar == null) {
                throw new IllegalArgumentException("No spec specified");
            } else if (jVar != null) {
                return new g(hVar, jVar, str, null);
            } else {
                throw new IllegalArgumentException("No adapterWrapper specified");
            }
        }

        public static g a(h hVar, String str) {
            return b(hVar, null, str);
        }

        public static g b(h hVar, j jVar, String str) {
            if (hVar != null) {
                return new g(hVar, jVar, null, str);
            }
            throw new IllegalArgumentException("No spec specified");
        }

        public h a() {
            return this.f3650a;
        }

        public String b() {
            return this.f3651b;
        }

        public String c() {
            return this.f3652c;
        }

        public String d() {
            return this.f3653d;
        }

        public String e() {
            return this.f3654e;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("SignalCollectionResult{mSignalProviderSpec=");
            sb.append(this.f3650a);
            sb.append(", mSdkVersion='");
            sb.append(this.f3651b);
            sb.append('\'');
            sb.append(", mAdapterVersion='");
            sb.append(this.f3652c);
            sb.append('\'');
            sb.append(", mSignalDataLength='");
            String str = this.f3653d;
            sb.append(str != null ? str.length() : 0);
            sb.append('\'');
            sb.append(", mErrorMessage=");
            sb.append(this.f3654e);
            sb.append('}');
            return sb.toString();
        }
    }

    public class h extends f {
        public h(JSONObject jSONObject, JSONObject jSONObject2, k kVar) {
            super(jSONObject, jSONObject2, kVar);
        }

        /* access modifiers changed from: package-private */
        public int o() {
            return a("max_signal_length", 2048);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.applovin.impl.mediation.b.f.b(java.lang.String, java.lang.Boolean):boolean
         arg types: [java.lang.String, int]
         candidates:
          com.applovin.impl.mediation.b.f.b(java.lang.String, long):long
          com.applovin.impl.mediation.b.f.b(java.lang.String, java.lang.String):java.lang.String
          com.applovin.impl.mediation.b.f.b(java.lang.String, org.json.JSONArray):org.json.JSONArray
          com.applovin.impl.mediation.b.f.b(java.lang.String, java.lang.Boolean):boolean */
        public boolean p() {
            return b("only_collect_signal_when_initialized", (Boolean) false);
        }

        public String toString() {
            return "SignalProviderSpec{specObject=" + l() + '}';
        }
    }

    public b(k kVar, MaxAdListener maxAdListener) {
        this.f3634c = maxAdListener;
        this.f3632a = new a(kVar);
        this.f3633b = new c(kVar, this);
    }

    public void a(d dVar) {
        AppLovinSdkUtils.runOnUiThreadDelayed(new a(dVar), dVar.E());
    }

    public void a(MaxAd maxAd) {
        this.f3633b.a();
        this.f3632a.a();
    }

    public void b(d dVar) {
        this.f3634c.onAdHidden(dVar);
    }

    public void c(d dVar) {
        long C = dVar.C();
        if (C >= 0) {
            this.f3633b.a(dVar, C);
        }
        if (dVar.D()) {
            this.f3632a.a(dVar, this);
        }
    }
}
