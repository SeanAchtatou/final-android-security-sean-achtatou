package com.applovin.impl.mediation.a$d.c;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.SpannedString;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.applovin.impl.mediation.a;
import com.applovin.impl.mediation.a$d.c.b;
import com.applovin.sdk.c;
import com.applovin.sdk.d;
import com.applovin.sdk.e;

public class a extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private ListView f3517a;

    /* renamed from: com.applovin.impl.mediation.a$d.c.a$a  reason: collision with other inner class name */
    class C0078a implements b.a {
        C0078a() {
        }

        public void a(String str) {
            new AlertDialog.Builder(a.this, 16974130).setTitle(e.applovin_instructions_dialog_title).setMessage(str).setNegativeButton(17039370, (DialogInterface.OnClickListener) null).create().show();
        }
    }

    public class b extends a.b.c {

        /* renamed from: d  reason: collision with root package name */
        final String f3519d;

        /* renamed from: e  reason: collision with root package name */
        final int f3520e;

        /* renamed from: f  reason: collision with root package name */
        final int f3521f;

        /* renamed from: g  reason: collision with root package name */
        final boolean f3522g;

        /* renamed from: com.applovin.impl.mediation.a$d.c.a$b$b  reason: collision with other inner class name */
        public static class C0080b {

            /* renamed from: a  reason: collision with root package name */
            SpannedString f3523a;

            /* renamed from: b  reason: collision with root package name */
            SpannedString f3524b;

            /* renamed from: c  reason: collision with root package name */
            String f3525c;

            /* renamed from: d  reason: collision with root package name */
            a.b.c.C0084a f3526d = a.b.c.C0084a.DETAIL;

            /* renamed from: e  reason: collision with root package name */
            int f3527e;

            /* renamed from: f  reason: collision with root package name */
            int f3528f;

            /* renamed from: g  reason: collision with root package name */
            boolean f3529g = false;

            public C0080b a(int i2) {
                this.f3527e = i2;
                return this;
            }

            public C0080b a(SpannedString spannedString) {
                this.f3524b = spannedString;
                return this;
            }

            public C0080b a(a.b.c.C0084a aVar) {
                this.f3526d = aVar;
                return this;
            }

            public C0080b a(String str) {
                this.f3523a = new SpannedString(str);
                return this;
            }

            public C0080b a(boolean z) {
                this.f3529g = z;
                return this;
            }

            public b a() {
                return new b(this);
            }

            public C0080b b(int i2) {
                this.f3528f = i2;
                return this;
            }

            public C0080b b(String str) {
                a(new SpannedString(str));
                return this;
            }

            public C0080b c(String str) {
                this.f3525c = str;
                return this;
            }
        }

        private b(C0080b bVar) {
            super(bVar.f3526d);
            this.f3549b = bVar.f3523a;
            this.f3550c = bVar.f3524b;
            this.f3519d = bVar.f3525c;
            this.f3520e = bVar.f3527e;
            this.f3521f = bVar.f3528f;
            this.f3522g = bVar.f3529g;
        }

        public static C0080b j() {
            return new C0080b();
        }

        public boolean a() {
            return this.f3522g;
        }

        public int f() {
            return this.f3520e;
        }

        public int g() {
            return this.f3521f;
        }

        public String i() {
            return this.f3519d;
        }

        public String toString() {
            return "NetworkDetailListItemViewModel{text=" + ((Object) this.f3549b) + ", detailText=" + ((Object) this.f3549b) + "}";
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(d.mediation_debugger_detail_activity);
        this.f3517a = (ListView) findViewById(c.listView);
    }

    public void setNetwork(a.b.d dVar) {
        setTitle(dVar.d());
        b bVar = new b(dVar, this);
        bVar.a(new C0078a());
        this.f3517a.setAdapter((ListAdapter) bVar);
    }
}
