package com.applovin.impl.mediation.d;

import android.text.TextUtils;
import com.applovin.impl.sdk.f;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.q;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.sdk.AppLovinSdk;
import com.facebook.internal.ServerProtocol;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private static final List<String> f3709a = new ArrayList();

    /* renamed from: b  reason: collision with root package name */
    private static b f3710b;

    public static class b {

        /* renamed from: a  reason: collision with root package name */
        private final JSONArray f3711a;

        /* renamed from: b  reason: collision with root package name */
        private final JSONArray f3712b;

        private b(JSONArray jSONArray, JSONArray jSONArray2) {
            this.f3711a = jSONArray;
            this.f3712b = jSONArray2;
        }

        public JSONArray a() {
            return this.f3711a;
        }

        public JSONArray b() {
            return this.f3712b;
        }
    }

    static {
        f3709a.add("com.applovin.mediation.adapters.AdColonyMediationAdapter");
        f3709a.add("com.applovin.mediation.adapters.AmazonMediationAdapter");
        f3709a.add("com.applovin.mediation.adapters.AmazonBiddingMediationAdapter");
        f3709a.add("com.applovin.mediation.adapters.AppLovinMediationAdapter");
        f3709a.add("com.applovin.mediation.adapters.ChartboostMediationAdapter");
        f3709a.add("com.applovin.mediation.adapters.FacebookMediationAdapter");
        f3709a.add("com.applovin.mediation.adapters.GoogleMediationAdapter");
        f3709a.add("com.applovin.mediation.adapters.HyperMXMediationAdapter");
        f3709a.add("com.applovin.mediation.adapters.IMobileMediationAdapter");
        f3709a.add("com.applovin.mediation.adapters.InMobiMediationAdapter");
        f3709a.add("com.applovin.mediation.adapters.InneractiveMediationAdapter");
        f3709a.add("com.applovin.mediation.adapters.IronSourceMediationAdapter");
        f3709a.add("com.applovin.mediation.adapters.LeadboltMediationAdapter");
        f3709a.add("com.applovin.mediation.adapters.MadvertiseMediationAdapter");
        f3709a.add("com.applovin.mediation.adapters.MaioMediationAdapter");
        f3709a.add("com.applovin.mediation.adapters.MintegralMediationAdapter");
        f3709a.add("com.applovin.mediation.adapters.MoPubMediationAdapter");
        f3709a.add("com.applovin.mediation.adapters.MyTargetMediationAdapter");
        f3709a.add("com.applovin.mediation.adapters.NendMediationAdapter");
        f3709a.add("com.applovin.mediation.adapters.OguryMediationAdapter");
        f3709a.add("com.applovin.mediation.adapters.SmaatoMediationAdapter");
        f3709a.add("com.applovin.mediation.adapters.TapjoyMediationAdapter");
        f3709a.add("com.applovin.mediation.adapters.TencentMediationAdapter");
        f3709a.add("com.applovin.mediation.adapters.UnityAdsMediationAdapter");
        f3709a.add("com.applovin.mediation.adapters.VerizonAdsMediationAdapter");
        f3709a.add("com.applovin.mediation.adapters.VungleMediationAdapter");
        f3709a.add("com.applovin.mediation.adapters.YandexMediationAdapter");
    }

    public static b a(k kVar) {
        b bVar = f3710b;
        if (bVar != null) {
            return bVar;
        }
        JSONArray jSONArray = new JSONArray();
        JSONArray jSONArray2 = new JSONArray();
        for (String next : f3709a) {
            MaxAdapter a2 = a(next, kVar);
            if (a2 != null) {
                JSONObject jSONObject = new JSONObject();
                try {
                    jSONObject.put("class", next);
                    jSONObject.put("sdk_version", a2.getSdkVersion());
                    jSONObject.put(ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION, a2.getAdapterVersion());
                } catch (Throwable unused) {
                }
                jSONArray.put(jSONObject);
            } else {
                jSONArray2.put(next);
            }
        }
        f3710b = new b(jSONArray, jSONArray2);
        return f3710b;
    }

    public static f.y.b a(MaxAdFormat maxAdFormat) {
        return maxAdFormat == MaxAdFormat.INTERSTITIAL ? f.y.b.MEDIATION_INTERSTITIAL : maxAdFormat == MaxAdFormat.REWARDED ? f.y.b.MEDIATION_INCENTIVIZED : f.y.b.MEDIATION_BANNER;
    }

    public static MaxAdapter a(String str, k kVar) {
        Class<MaxAdapter> cls = MaxAdapter.class;
        if (TextUtils.isEmpty(str)) {
            kVar.Z().e("AppLovinSdk", "Failed to create adapter instance. No class name provided");
            return null;
        }
        try {
            Class<?> cls2 = Class.forName(str);
            if (cls.isAssignableFrom(cls2)) {
                return (MaxAdapter) cls2.getConstructor(AppLovinSdk.class).newInstance(kVar.r());
            }
            q Z = kVar.Z();
            Z.e("AppLovinSdk", str + " error: not an instance of '" + cls.getName() + "'.");
            return null;
        } catch (ClassNotFoundException unused) {
        } catch (Throwable th) {
            q Z2 = kVar.Z();
            Z2.b("AppLovinSdk", "Failed to load: " + str, th);
        }
    }

    public static String b(MaxAdFormat maxAdFormat) {
        return maxAdFormat.getLabel();
    }
}
