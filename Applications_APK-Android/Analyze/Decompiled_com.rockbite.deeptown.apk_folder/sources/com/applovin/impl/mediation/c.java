package com.applovin.impl.mediation;

import android.app.Activity;
import android.graphics.Point;
import android.text.TextUtils;
import com.applovin.impl.mediation.b;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.f;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.network.a;
import com.applovin.impl.sdk.network.b;
import com.applovin.impl.sdk.network.f;
import com.applovin.impl.sdk.network.g;
import com.applovin.impl.sdk.q;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.p;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxErrorCodes;
import com.applovin.mediation.adapter.MaxAdapterError;
import com.applovin.sdk.AppLovinMediationProvider;
import com.applovin.sdk.AppLovinPostbackListener;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkUtils;
import com.applovin.sdk.AppLovinWebViewActivity;
import com.facebook.internal.AnalyticsEvents;
import com.facebook.internal.NativeProtocol;
import com.google.ads.mediation.inmobi.InMobiNetworkValues;
import com.google.firebase.perf.FirebasePerformance;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TapjoyConstants;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private final k f3655a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final q f3656b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public final b f3657c;

    /* renamed from: d  reason: collision with root package name */
    private com.applovin.impl.sdk.utils.d f3658d;

    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ b.d f3659a;

        a(b.d dVar) {
            this.f3659a = dVar;
        }

        public void run() {
            c.this.f3656b.b("AdHiddenCallbackTimeoutManager", "Timing out...");
            c.this.f3657c.b(this.f3659a);
        }
    }

    public interface b {
        void b(b.d dVar);
    }

    /* renamed from: com.applovin.impl.mediation.c$c  reason: collision with other inner class name */
    public class C0088c extends f.c {
        /* access modifiers changed from: private */

        /* renamed from: f  reason: collision with root package name */
        public final Activity f3661f;

        /* renamed from: com.applovin.impl.mediation.c$c$a */
        class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ b.f f3662a;

            a(b.f fVar) {
                this.f3662a = fVar;
            }

            public void run() {
                C0088c cVar = C0088c.this;
                cVar.a("Auto-initing adapter: " + this.f3662a);
                C0088c.this.f4130a.b0().a(this.f3662a, C0088c.this.f3661f);
            }
        }

        public C0088c(Activity activity, k kVar) {
            super("TaskAutoInitAdapters", kVar, true);
            this.f3661f = activity;
        }

        private List<b.f> a(JSONArray jSONArray, JSONObject jSONObject) {
            ArrayList arrayList = new ArrayList(jSONArray.length());
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                arrayList.add(new b.f(com.applovin.impl.sdk.utils.i.a(jSONArray, i2, (JSONObject) null, this.f4130a), jSONObject, this.f4130a));
            }
            return arrayList;
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.C;
        }

        public void run() {
            String str;
            String str2 = (String) this.f4130a.a(c.g.y);
            if (m.b(str2)) {
                if (this.f3661f == null) {
                    q.i("AppLovinSdk", "Failed to initialize 3rd-party SDKs. Please contact us at devsupport@applovin.com for more information.");
                    this.f4130a.k().b(com.applovin.impl.sdk.d.g.r, 1);
                    return;
                }
                try {
                    JSONObject jSONObject = new JSONObject(str2);
                    boolean a2 = com.applovin.impl.sdk.utils.i.a(this.f4130a.n().d().f4279b, com.applovin.impl.sdk.utils.i.b(jSONObject, "test_mode_idfas", new JSONArray(), this.f4130a));
                    List<b.f> a3 = a(com.applovin.impl.sdk.utils.i.b(jSONObject, a2 ? "test_mode_auto_init_adapters" : "auto_init_adapters", new JSONArray(), this.f4130a), jSONObject);
                    if (a3.size() > 0) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Auto-initing ");
                        sb.append(a3.size());
                        sb.append(" adapters");
                        sb.append(a2 ? " in test mode" : "");
                        sb.append("...");
                        a(sb.toString());
                        this.f4130a.c(AppLovinMediationProvider.MAX);
                        for (b.f aVar : a3) {
                            this.f4130a.j().b().execute(new a(aVar));
                        }
                        return;
                    }
                    d("No auto-init adapters found");
                } catch (JSONException e2) {
                    th = e2;
                    str = "Failed to parse auto-init adapters JSON";
                    a(str, th);
                } catch (Throwable th) {
                    th = th;
                    str = "Failed to auto-init adapters";
                    a(str, th);
                }
            }
        }
    }

    public class d extends f.c {

        /* renamed from: j  reason: collision with root package name */
        private static String f3664j;
        /* access modifiers changed from: private */

        /* renamed from: f  reason: collision with root package name */
        public final MaxAdFormat f3665f;

        /* renamed from: g  reason: collision with root package name */
        private final boolean f3666g;
        /* access modifiers changed from: private */

        /* renamed from: h  reason: collision with root package name */
        public final Activity f3667h;

        /* renamed from: i  reason: collision with root package name */
        private final C0090c f3668i;

        class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ b.h f3669a;

            /* renamed from: b  reason: collision with root package name */
            final /* synthetic */ AtomicBoolean f3670b;

            /* renamed from: c  reason: collision with root package name */
            final /* synthetic */ List f3671c;

            /* renamed from: d  reason: collision with root package name */
            final /* synthetic */ CountDownLatch f3672d;

            /* renamed from: com.applovin.impl.mediation.c$d$a$a  reason: collision with other inner class name */
            class C0089a implements b.g.a {
                C0089a() {
                }

                public void a(b.g gVar) {
                    if (a.this.f3670b.get() && gVar != null) {
                        a.this.f3671c.add(gVar);
                    }
                    a.this.f3672d.countDown();
                }
            }

            a(b.h hVar, AtomicBoolean atomicBoolean, List list, CountDownLatch countDownLatch) {
                this.f3669a = hVar;
                this.f3670b = atomicBoolean;
                this.f3671c = list;
                this.f3672d = countDownLatch;
            }

            public void run() {
                d.this.a(this.f3669a, new C0089a());
            }
        }

        class b implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ b.h f3675a;

            /* renamed from: b  reason: collision with root package name */
            final /* synthetic */ b.g.a f3676b;

            b(b.h hVar, b.g.a aVar) {
                this.f3675a = hVar;
                this.f3676b = aVar;
            }

            public void run() {
                d.this.f4130a.c0().collectSignal(d.this.f3665f, this.f3675a, d.this.f3667h, this.f3676b);
            }
        }

        /* renamed from: com.applovin.impl.mediation.c$d$c  reason: collision with other inner class name */
        public interface C0090c {
            void a(JSONArray jSONArray);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
          ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
          ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
          ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
          ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
        static {
            try {
                JSONArray jSONArray = new JSONArray();
                jSONArray.put(a("APPLOVIN_NETWORK", "com.applovin.mediation.adapters.AppLovinMediationAdapter"));
                a("FACEBOOK_NETWORK", "com.applovin.mediation.adapters.FacebookMediationAdapter").put("run_on_ui_thread", false);
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("signal_providers", jSONArray);
                f3664j = jSONObject.toString();
            } catch (JSONException unused) {
            }
        }

        public d(MaxAdFormat maxAdFormat, boolean z, Activity activity, k kVar, C0090c cVar) {
            super("TaskCollectSignals", kVar);
            this.f3665f = maxAdFormat;
            this.f3666g = z;
            this.f3667h = activity;
            this.f3668i = cVar;
        }

        private String a(String str, c.e<Integer> eVar) {
            int intValue;
            return (!TextUtils.isEmpty(str) && (intValue = ((Integer) this.f4130a.a(eVar)).intValue()) > 0) ? str.substring(0, Math.min(str.length(), intValue)) : "";
        }

        private static JSONObject a(String str, String str2) throws JSONException {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("name", str);
            jSONObject.put("class", str2);
            jSONObject.put("adapter_timeout_ms", 30000);
            jSONObject.put("max_signal_length", 32768);
            jSONObject.put("scode", "");
            return jSONObject;
        }

        /* access modifiers changed from: private */
        public void a(b.h hVar, b.g.a aVar) {
            b bVar = new b(hVar, aVar);
            if (hVar.b()) {
                a("Running signal collection for " + hVar + " on the main thread");
                this.f3667h.runOnUiThread(bVar);
                return;
            }
            a("Running signal collection for " + hVar + " on the background thread");
            bVar.run();
        }

        private void a(Collection<b.g> collection) {
            String str;
            String a2;
            JSONArray jSONArray = new JSONArray();
            for (b.g next : collection) {
                try {
                    JSONObject jSONObject = new JSONObject();
                    b.h a3 = next.a();
                    jSONObject.put("name", a3.n());
                    jSONObject.put("class", a3.m());
                    jSONObject.put(TapjoyConstants.TJC_ADAPTER_VERSION, a(next.c(), c.d.e4));
                    jSONObject.put("sdk_version", a(next.b(), c.d.f4));
                    JSONObject jSONObject2 = new JSONObject();
                    if (m.b(next.e())) {
                        str = AnalyticsEvents.PARAMETER_SHARE_ERROR_MESSAGE;
                        a2 = next.e();
                    } else {
                        str = "signal";
                        a2 = a(next.d(), c.d.g4);
                    }
                    jSONObject2.put(str, a2);
                    jSONObject.put(TJAdUnitConstants.String.DATA, jSONObject2);
                    jSONArray.put(jSONObject);
                    a("Collected signal from " + a3);
                } catch (JSONException e2) {
                    a("Failed to create signal data", e2);
                }
            }
            a(jSONArray);
        }

        private void a(JSONArray jSONArray) {
            C0090c cVar = this.f3668i;
            if (cVar != null) {
                cVar.a(jSONArray);
            }
        }

        private void a(JSONArray jSONArray, JSONObject jSONObject) throws JSONException, InterruptedException {
            a("Collecting signals from " + jSONArray.length() + " signal providers(s)...");
            List a2 = com.applovin.impl.sdk.utils.e.a(jSONArray.length());
            AtomicBoolean atomicBoolean = new AtomicBoolean(true);
            CountDownLatch countDownLatch = new CountDownLatch(jSONArray.length());
            ScheduledExecutorService b2 = this.f4130a.j().b();
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                b2.execute(new a(new b.h(jSONArray.getJSONObject(i2), jSONObject, this.f4130a), atomicBoolean, a2, countDownLatch));
            }
            countDownLatch.await(((Long) this.f4130a.a(c.d.d4)).longValue(), TimeUnit.MILLISECONDS);
            atomicBoolean.set(false);
            a(a2);
        }

        private void b(String str, Throwable th) {
            a("No signals collected: " + str, th);
            a(new JSONArray());
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.D;
        }

        public void run() {
            String str;
            try {
                JSONObject jSONObject = new JSONObject((String) this.f4130a.b(c.g.x, f3664j));
                JSONArray b2 = com.applovin.impl.sdk.utils.i.b(jSONObject, "signal_providers", (JSONArray) null, this.f4130a);
                if (this.f3666g) {
                    List<String> b3 = this.f4130a.b(c.d.I4);
                    JSONArray jSONArray = new JSONArray();
                    for (int i2 = 0; i2 < b2.length(); i2++) {
                        JSONObject a2 = com.applovin.impl.sdk.utils.i.a(b2, i2, (JSONObject) null, this.f4130a);
                        if (b3.contains(com.applovin.impl.sdk.utils.i.b(a2, "class", (String) null, this.f4130a))) {
                            jSONArray.put(a2);
                        }
                    }
                    b2 = jSONArray;
                }
                if (b2.length() == 0) {
                    b("No signal providers found", null);
                } else {
                    a(b2, jSONObject);
                }
            } catch (JSONException e2) {
                th = e2;
                str = "Failed to parse signals JSON";
                b(str, th);
            } catch (InterruptedException e3) {
                th = e3;
                str = "Failed to wait for signals";
                b(str, th);
            } catch (Throwable th) {
                th = th;
                str = "Failed to collect signals";
                b(str, th);
            }
        }
    }

    public class e extends f.c {

        /* renamed from: f  reason: collision with root package name */
        private final String f3678f;

        /* renamed from: g  reason: collision with root package name */
        private final MaxAdFormat f3679g;

        /* renamed from: h  reason: collision with root package name */
        private final g f3680h;

        /* renamed from: i  reason: collision with root package name */
        private final JSONArray f3681i;

        /* renamed from: j  reason: collision with root package name */
        private final Activity f3682j;

        /* renamed from: k  reason: collision with root package name */
        private final MaxAdListener f3683k;

        class a extends f.e0<JSONObject> {
            a(com.applovin.impl.sdk.network.b bVar, k kVar) {
                super(bVar, kVar);
            }

            public void a(int i2) {
                e.this.a(i2);
            }

            public void a(JSONObject jSONObject, int i2) {
                if (i2 == 200) {
                    com.applovin.impl.sdk.utils.i.b(jSONObject, "ad_fetch_latency_millis", this.f4146k.a(), this.f4130a);
                    com.applovin.impl.sdk.utils.i.b(jSONObject, "ad_fetch_response_size", this.f4146k.b(), this.f4130a);
                    e.this.a(jSONObject);
                    return;
                }
                e.this.a(i2);
            }
        }

        public e(String str, MaxAdFormat maxAdFormat, g gVar, JSONArray jSONArray, Activity activity, k kVar, MaxAdListener maxAdListener) {
            super("TaskFetchMediatedAd " + str, kVar);
            this.f3678f = str;
            this.f3679g = maxAdFormat;
            this.f3680h = gVar;
            this.f3681i = jSONArray;
            this.f3682j = activity;
            this.f3683k = maxAdListener;
        }

        /* access modifiers changed from: private */
        public void a(int i2) {
            boolean z = i2 != 204;
            q Z = this.f4130a.Z();
            String c2 = c();
            Boolean valueOf = Boolean.valueOf(z);
            Z.a(c2, valueOf, "Unable to fetch " + this.f3678f + " ad: server returned " + i2);
            if (i2 == -800) {
                this.f4130a.k().a(com.applovin.impl.sdk.d.g.q);
            }
            b(i2);
        }

        private void a(com.applovin.impl.sdk.d.h hVar) {
            long b2 = hVar.b(com.applovin.impl.sdk.d.g.f4077f);
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - b2 > TimeUnit.MINUTES.toMillis((long) ((Integer) this.f4130a.a(c.e.H2)).intValue())) {
                hVar.b(com.applovin.impl.sdk.d.g.f4077f, currentTimeMillis);
                hVar.c(com.applovin.impl.sdk.d.g.f4078g);
            }
        }

        /* access modifiers changed from: private */
        public void a(JSONObject jSONObject) {
            try {
                com.applovin.impl.sdk.utils.h.b(jSONObject, this.f4130a);
                com.applovin.impl.sdk.utils.h.a(jSONObject, this.f4130a);
                com.applovin.impl.sdk.utils.h.d(jSONObject, this.f4130a);
                com.applovin.impl.mediation.d.b.e(jSONObject, this.f4130a);
                com.applovin.impl.mediation.d.b.f(jSONObject, this.f4130a);
                this.f4130a.j().a(b(jSONObject));
            } catch (Throwable th) {
                a("Unable to process mediated ad response", th);
                b(-800);
            }
        }

        private h b(JSONObject jSONObject) {
            return new h(this.f3678f, this.f3679g, jSONObject, this.f3682j, this.f4130a, this.f3683k);
        }

        private void b(int i2) {
            com.applovin.impl.sdk.utils.j.a(this.f3683k, this.f3678f, i2);
        }

        private void c(JSONObject jSONObject) {
            try {
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("loaded", new JSONArray((Collection) this.f4130a.a0().a()));
                jSONObject2.put("failed", new JSONArray((Collection) this.f4130a.a0().b()));
                jSONObject.put("classname_info", jSONObject2);
                jSONObject.put("initialized_adapters", this.f4130a.b0().c());
                jSONObject.put("initialized_adapter_classnames", new JSONArray((Collection) this.f4130a.b0().b()));
                jSONObject.put("installed_mediation_adapters", com.applovin.impl.mediation.d.c.a(this.f4130a).a());
            } catch (Exception e2) {
                a("Failed to populate adapter classnames", e2);
            }
        }

        private void d(JSONObject jSONObject) throws JSONException {
            JSONArray jSONArray = this.f3681i;
            if (jSONArray != null) {
                jSONObject.put("signal_data", jSONArray);
            }
        }

        private void e(JSONObject jSONObject) throws JSONException {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("ad_unit_id", this.f3678f);
            jSONObject2.put("ad_format", com.applovin.impl.mediation.d.c.b(this.f3679g));
            if (this.f3680h != null && ((Boolean) this.f4130a.a(c.d.a4)).booleanValue()) {
                jSONObject2.put("extra_parameters", com.applovin.impl.sdk.utils.i.a((Map<String, ?>) com.applovin.impl.sdk.utils.i.a(this.f3680h.a())));
            }
            if (((Boolean) this.f4130a.a(c.e.l)).booleanValue()) {
                jSONObject2.put("n", String.valueOf(this.f4130a.B().a(this.f3678f)));
            }
            jSONObject.put("ad_info", jSONObject2);
        }

        private String f() {
            return com.applovin.impl.mediation.d.b.g(this.f4130a);
        }

        private void f(JSONObject jSONObject) throws JSONException {
            l n = this.f4130a.n();
            l.f b2 = n.b();
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("brand", b2.f4292d);
            jSONObject2.put("brand_name", b2.f4293e);
            jSONObject2.put("hardware", b2.f4294f);
            jSONObject2.put("api_level", b2.f4296h);
            jSONObject2.put("carrier", b2.f4298j);
            jSONObject2.put(TapjoyConstants.TJC_DEVICE_COUNTRY_CODE, b2.f4297i);
            jSONObject2.put("locale", b2.f4299k);
            jSONObject2.put("model", b2.f4289a);
            jSONObject2.put("os", b2.f4290b);
            jSONObject2.put(TapjoyConstants.TJC_PLATFORM, b2.f4291c);
            jSONObject2.put("revision", b2.f4295g);
            jSONObject2.put("orientation_lock", b2.l);
            jSONObject2.put("tz_offset", b2.r);
            jSONObject2.put("aida", m.a(b2.I));
            jSONObject2.put("wvvc", b2.s);
            jSONObject2.put("adns", (double) b2.m);
            jSONObject2.put("adnsd", b2.n);
            jSONObject2.put("xdpi", (double) b2.o);
            jSONObject2.put("ydpi", (double) b2.p);
            jSONObject2.put("screen_size_in", b2.q);
            jSONObject2.put("sim", m.a(b2.x));
            jSONObject2.put("gy", m.a(b2.y));
            jSONObject2.put("is_tablet", m.a(b2.z));
            jSONObject2.put("tv", m.a(b2.A));
            jSONObject2.put("vs", m.a(b2.B));
            jSONObject2.put("lpm", b2.C);
            jSONObject2.put("fs", b2.E);
            jSONObject2.put("fm", b2.F.f4301b);
            jSONObject2.put("tm", b2.F.f4300a);
            jSONObject2.put("lmt", b2.F.f4302c);
            jSONObject2.put("lm", b2.F.f4303d);
            jSONObject2.put("adr", m.a(b2.t));
            jSONObject2.put("volume", b2.v);
            jSONObject2.put("network", com.applovin.impl.sdk.utils.h.b(this.f4130a));
            if (m.b(b2.w)) {
                jSONObject2.put("ua", b2.w);
            }
            if (m.b(b2.D)) {
                jSONObject2.put("so", b2.D);
            }
            l.e eVar = b2.u;
            if (eVar != null) {
                jSONObject2.put("act", eVar.f4287a);
                jSONObject2.put("acm", eVar.f4288b);
            }
            Boolean bool = b2.G;
            if (bool != null) {
                jSONObject2.put("huc", bool.toString());
            }
            Boolean bool2 = b2.H;
            if (bool2 != null) {
                jSONObject2.put("aru", bool2.toString());
            }
            Point a2 = com.applovin.impl.sdk.utils.g.a(d());
            jSONObject2.put("dx", Integer.toString(a2.x));
            jSONObject2.put("dy", Integer.toString(a2.y));
            g(jSONObject2);
            jSONObject.put("device_info", jSONObject2);
            l.d c2 = n.c();
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put(InMobiNetworkValues.PACKAGE_NAME, c2.f4282c);
            jSONObject3.put("installer_name", c2.f4283d);
            jSONObject3.put(NativeProtocol.BRIDGE_ARG_APP_NAME_STRING, c2.f4280a);
            jSONObject3.put(TapjoyConstants.TJC_APP_VERSION_NAME, c2.f4281b);
            jSONObject3.put("installed_at", c2.f4286g);
            jSONObject3.put("tg", c2.f4284e);
            jSONObject3.put("api_did", this.f4130a.a(c.e.f4001f));
            jSONObject3.put("sdk_version", AppLovinSdk.VERSION);
            jSONObject3.put("build", 131);
            jSONObject3.put("test_ads", this.f4130a.P().isTestAdsEnabled());
            jSONObject3.put("first_install", String.valueOf(this.f4130a.g()));
            jSONObject3.put("first_install_v2", String.valueOf(!this.f4130a.h()));
            jSONObject3.put(TapjoyConstants.TJC_DEBUG, Boolean.toString(p.b(this.f4130a)));
            String M = this.f4130a.M();
            if (((Boolean) this.f4130a.a(c.e.O2)).booleanValue() && m.b(M)) {
                jSONObject3.put("cuid", M);
            }
            if (((Boolean) this.f4130a.a(c.e.R2)).booleanValue()) {
                jSONObject3.put("compass_random_token", this.f4130a.N());
            }
            if (((Boolean) this.f4130a.a(c.e.T2)).booleanValue()) {
                jSONObject3.put("applovin_random_token", this.f4130a.O());
            }
            String str = (String) this.f4130a.a(c.e.V2);
            if (m.b(str)) {
                jSONObject3.put("plugin_version", str);
            }
            jSONObject.put("app_info", jSONObject3);
            a.b a3 = this.f4130a.i().a();
            if (a3 != null) {
                JSONObject jSONObject4 = new JSONObject();
                jSONObject4.put("lrm_ts_ms", String.valueOf(a3.a()));
                jSONObject4.put("lrm_url", a3.b());
                jSONObject4.put("lrm_ct_ms", String.valueOf(a3.d()));
                jSONObject4.put("lrm_rs", String.valueOf(a3.c()));
                jSONObject.put("connection_info", jSONObject4);
            }
        }

        private String g() {
            return com.applovin.impl.mediation.d.b.h(this.f4130a);
        }

        private void g(JSONObject jSONObject) {
            try {
                l.c d2 = this.f4130a.n().d();
                String str = d2.f4279b;
                if (m.b(str)) {
                    jSONObject.put("idfa", str);
                }
                jSONObject.put("dnt", d2.f4278a);
            } catch (Throwable th) {
                a("Failed to populate advertising info", th);
            }
        }

        private JSONObject h() throws JSONException {
            JSONObject jSONObject = new JSONObject();
            e(jSONObject);
            f(jSONObject);
            d(jSONObject);
            c(jSONObject);
            jSONObject.put("sc", m.d((String) this.f4130a.a(c.e.f4004i)));
            jSONObject.put("sc2", m.d((String) this.f4130a.a(c.e.f4005j)));
            jSONObject.put("server_installed_at", m.d((String) this.f4130a.a(c.e.f4006k)));
            String str = (String) this.f4130a.a(c.g.z);
            if (m.b(str)) {
                jSONObject.put("persisted_data", m.d(str));
            }
            if (((Boolean) this.f4130a.a(c.e.m3)).booleanValue()) {
                h(jSONObject);
            }
            jSONObject.put("pnr", Boolean.toString(this.f4130a.L()));
            jSONObject.put("mediation_provider", this.f4130a.R());
            return jSONObject;
        }

        private void h(JSONObject jSONObject) {
            try {
                com.applovin.impl.sdk.d.h k2 = this.f4130a.k();
                jSONObject.put("li", String.valueOf(k2.b(com.applovin.impl.sdk.d.g.f4076e)));
                jSONObject.put("si", String.valueOf(k2.b(com.applovin.impl.sdk.d.g.f4078g)));
                jSONObject.put("pf", String.valueOf(k2.b(com.applovin.impl.sdk.d.g.f4082k)));
                jSONObject.put("mpf", String.valueOf(k2.b(com.applovin.impl.sdk.d.g.q)));
                jSONObject.put("gpf", String.valueOf(k2.b(com.applovin.impl.sdk.d.g.l)));
            } catch (Throwable th) {
                a("Failed to populate ad serving info", th);
            }
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.E;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Boolean, com.applovin.impl.sdk.k):java.lang.Boolean
         arg types: [org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.k]
         candidates:
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, float, com.applovin.impl.sdk.k):float
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, long, com.applovin.impl.sdk.k):long
          com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, java.lang.Object, com.applovin.impl.sdk.k):java.lang.Object
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Object, com.applovin.impl.sdk.k):java.lang.Object
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.util.List, com.applovin.impl.sdk.k):java.util.List
          com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, org.json.JSONObject, com.applovin.impl.sdk.k):org.json.JSONObject
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.k):void
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.String, com.applovin.impl.sdk.k):void
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONArray, com.applovin.impl.sdk.k):void
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONObject, com.applovin.impl.sdk.k):void
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Boolean, com.applovin.impl.sdk.k):java.lang.Boolean */
        public void run() {
            a("Fetching next ad for ad unit id: " + this.f3678f + " and format: " + this.f3679g);
            if (((Boolean) this.f4130a.a(c.e.c3)).booleanValue() && p.d()) {
                a("User is connected to a VPN");
            }
            com.applovin.impl.sdk.d.h k2 = this.f4130a.k();
            k2.a(com.applovin.impl.sdk.d.g.p);
            if (k2.b(com.applovin.impl.sdk.d.g.f4077f) == 0) {
                k2.b(com.applovin.impl.sdk.d.g.f4077f, System.currentTimeMillis());
            }
            try {
                JSONObject h2 = h();
                HashMap hashMap = new HashMap();
                hashMap.put("rid", UUID.randomUUID().toString());
                if (h2.has("huc")) {
                    hashMap.put("huc", String.valueOf(com.applovin.impl.sdk.utils.i.a(h2, "huc", (Boolean) false, this.f4130a)));
                }
                if (h2.has("aru")) {
                    hashMap.put("aru", String.valueOf(com.applovin.impl.sdk.utils.i.a(h2, "aru", (Boolean) false, this.f4130a)));
                }
                if (!((Boolean) this.f4130a.a(c.e.G3)).booleanValue()) {
                    hashMap.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.f4130a.X());
                }
                a(k2);
                b.a c2 = com.applovin.impl.sdk.network.b.a(this.f4130a).b(FirebasePerformance.HttpMethod.POST).a(f()).c(g()).a((Map<String, String>) hashMap).a(h2).a((Object) new JSONObject()).b(((Long) this.f4130a.a(c.d.Y3)).intValue()).a(((Integer) this.f4130a.a(c.e.w2)).intValue()).c(((Long) this.f4130a.a(c.d.X3)).intValue());
                c2.b(true);
                a aVar = new a(c2.a(), this.f4130a);
                aVar.a(c.d.V3);
                aVar.b(c.d.W3);
                this.f4130a.j().a(aVar);
            } catch (Throwable th) {
                a("Unable to fetch ad " + this.f3678f, th);
                a(0);
                this.f4130a.l().a(a());
            }
        }
    }

    public class f extends f.c {

        /* renamed from: f  reason: collision with root package name */
        private final String f3684f;

        /* renamed from: g  reason: collision with root package name */
        private final String f3685g;

        /* renamed from: h  reason: collision with root package name */
        private final b.f f3686h;

        /* renamed from: i  reason: collision with root package name */
        private final Map<String, String> f3687i;

        /* renamed from: j  reason: collision with root package name */
        private final Map<String, String> f3688j;

        /* renamed from: k  reason: collision with root package name */
        private final f f3689k;

        class a implements AppLovinPostbackListener {
            a() {
            }

            public void onPostbackFailure(String str, int i2) {
                f fVar = f.this;
                fVar.d("Failed to fire postback with code: " + i2 + " and url: " + str);
            }

            public void onPostbackSuccess(String str) {
                f fVar = f.this;
                fVar.a("Successfully fired postback: " + str);
            }
        }

        public f(String str, Map<String, String> map, f fVar, b.f fVar2, k kVar) {
            super("TaskFireMediationPostbacks", kVar);
            this.f3684f = str;
            this.f3685g = str + "_urls";
            this.f3687i = p.b(map);
            this.f3689k = fVar == null ? f.EMPTY : fVar;
            this.f3686h = fVar2;
            HashMap hashMap = new HashMap(4);
            hashMap.put("Ad-Network-Name", fVar2.n());
            if (fVar2 instanceof b.C0087b) {
                b.C0087b bVar = (b.C0087b) fVar2;
                hashMap.put("Ad-Unit-Id", bVar.getAdUnitId());
                hashMap.put("Ad-Format", bVar.getFormat().getLabel());
                if (bVar instanceof b.d) {
                    hashMap.put("Ad-Is-Fallback", String.valueOf(((b.d) bVar).x()));
                }
            }
            this.f3688j = hashMap;
        }

        private com.applovin.impl.sdk.network.g a(String str, f fVar, Map<String, String> map) {
            String a2 = a(str, fVar);
            g.a b2 = com.applovin.impl.sdk.network.g.b(b());
            b2.d(a2);
            b2.c(false);
            b2.c(map);
            return b2.a();
        }

        private String a(String str, f fVar) {
            int i2;
            String str2;
            if (fVar instanceof MaxAdapterError) {
                MaxAdapterError maxAdapterError = (MaxAdapterError) fVar;
                i2 = maxAdapterError.getThirdPartySdkErrorCode();
                str2 = maxAdapterError.getThirdPartySdkErrorMessage();
            } else {
                i2 = 0;
                str2 = "";
            }
            return str.replace("{ERROR_CODE}", String.valueOf(fVar.getErrorCode())).replace("{ERROR_MESSAGE}", m.d(fVar.getErrorMessage())).replace("{THIRD_PARTY_SDK_ERROR_CODE}", String.valueOf(i2)).replace("{THIRD_PARTY_SDK_ERROR_MESSAGE}", m.d(str2));
        }

        private com.applovin.impl.sdk.network.f b(String str, f fVar, Map<String, String> map) {
            String a2 = a(str, fVar);
            f.b k2 = com.applovin.impl.sdk.network.f.k();
            k2.a(a2);
            k2.a(false);
            k2.b(map);
            return k2.a();
        }

        private void f() {
            List<String> a2 = this.f3686h.a(this.f3685g, this.f3687i);
            if (!a2.isEmpty()) {
                a("Firing " + a2.size() + " '" + this.f3684f + "' postback(s)");
                for (String a3 : a2) {
                    b().q().dispatchPostbackRequest(a(a3, this.f3689k, this.f3688j), f.y.b.MEDIATION_POSTBACKS, new a());
                }
                return;
            }
            a("No postbacks to fire for event: " + this.f3684f);
        }

        private void g() {
            List<String> a2 = this.f3686h.a(this.f3685g, this.f3687i);
            if (!a2.isEmpty()) {
                a("Firing " + a2.size() + " '" + this.f3684f + "' persistent postback(s)");
                for (String b2 : a2) {
                    b().m().a(b(b2, this.f3689k, this.f3688j));
                }
                return;
            }
            a("No persistent postbacks to fire for event: " + this.f3684f);
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.K;
        }

        public void run() {
            if (((Boolean) b().a(c.d.c4)).booleanValue()) {
                g();
            } else {
                f();
            }
        }
    }

    public class g extends f.c {

        /* renamed from: f  reason: collision with root package name */
        private final String f3691f;

        /* renamed from: g  reason: collision with root package name */
        private final JSONObject f3692g;

        /* renamed from: h  reason: collision with root package name */
        private final JSONObject f3693h;

        /* renamed from: i  reason: collision with root package name */
        private final MaxAdListener f3694i;

        /* renamed from: j  reason: collision with root package name */
        private final Activity f3695j;

        g(String str, JSONObject jSONObject, JSONObject jSONObject2, k kVar, Activity activity, MaxAdListener maxAdListener) {
            super("TaskLoadAdapterAd " + str, kVar);
            this.f3691f = str;
            this.f3692g = jSONObject;
            this.f3693h = jSONObject2;
            this.f3695j = activity;
            this.f3694i = maxAdListener;
        }

        private b.C0087b f() throws JSONException {
            String string = this.f3693h.getString("ad_format");
            MaxAdFormat c2 = p.c(string);
            if (c2 == MaxAdFormat.BANNER || c2 == MaxAdFormat.MREC || c2 == MaxAdFormat.LEADER) {
                return new b.c(this.f3692g, this.f3693h, this.f4130a);
            }
            if (c2 == MaxAdFormat.NATIVE) {
                return new b.e(this.f3692g, this.f3693h, this.f4130a);
            }
            if (c2 == MaxAdFormat.INTERSTITIAL || c2 == MaxAdFormat.REWARDED) {
                return new b.d(this.f3692g, this.f3693h, this.f4130a);
            }
            throw new IllegalArgumentException("Unsupported ad format: " + string);
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.F;
        }

        public void run() {
            try {
                this.f4130a.c0().loadThirdPartyMediatedAd(this.f3691f, f(), this.f3695j, this.f3694i);
            } catch (Throwable th) {
                a("Unable to process adapter ad", th);
                this.f4130a.l().a(a());
                com.applovin.impl.sdk.utils.j.a(this.f3694i, this.f3691f, (int) MaxErrorCodes.MEDIATION_ADAPTER_LOAD_FAILED);
            }
        }
    }

    public class h extends f.c {
        /* access modifiers changed from: private */

        /* renamed from: f  reason: collision with root package name */
        public final String f3696f;
        /* access modifiers changed from: private */

        /* renamed from: g  reason: collision with root package name */
        public final MaxAdFormat f3697g;
        /* access modifiers changed from: private */

        /* renamed from: h  reason: collision with root package name */
        public final JSONObject f3698h;
        /* access modifiers changed from: private */

        /* renamed from: i  reason: collision with root package name */
        public final MaxAdListener f3699i;
        /* access modifiers changed from: private */

        /* renamed from: j  reason: collision with root package name */
        public final Activity f3700j;

        class a implements Runnable {
            a() {
            }

            public void run() {
                h.this.a(204);
            }
        }

        private class b extends f.c {

            /* renamed from: f  reason: collision with root package name */
            private final JSONArray f3702f;

            /* renamed from: g  reason: collision with root package name */
            private final int f3703g;

            class a extends com.applovin.impl.mediation.d.a {
                a(MaxAdListener maxAdListener, k kVar) {
                    super(maxAdListener, kVar);
                }

                public void onAdLoadFailed(String str, int i2) {
                    b bVar = b.this;
                    bVar.e("failed to load ad: " + i2);
                    b.this.g();
                }

                public void onAdLoaded(MaxAd maxAd) {
                    b.this.e("loaded ad");
                    h.this.a(maxAd);
                }
            }

            b(int i2, JSONArray jSONArray) {
                super("TaskProcessNextWaterfallAd", h.this.f4130a);
                if (i2 < 0 || i2 >= jSONArray.length()) {
                    throw new IllegalArgumentException("Invalid ad index specified: " + i2);
                }
                this.f3702f = jSONArray;
                this.f3703g = i2;
            }

            private String a(int i2) {
                return (i2 < 0 || i2 >= this.f3702f.length()) ? "undefined" : com.applovin.impl.sdk.utils.i.b(com.applovin.impl.sdk.utils.i.a(this.f3702f, i2, new JSONObject(), this.f4130a), "type", "undefined", this.f4130a);
            }

            /* access modifiers changed from: private */
            public void e(String str) {
            }

            private void f() throws JSONException {
                JSONObject jSONObject = this.f3702f.getJSONObject(this.f3703g);
                String a2 = a(this.f3703g);
                if ("adapter".equalsIgnoreCase(a2)) {
                    a("Starting task for adapter ad...");
                    e("started to load ad");
                    this.f4130a.j().a(new g(h.this.f3696f, jSONObject, h.this.f3698h, this.f4130a, h.this.f3700j, new a(h.this.f3699i, this.f4130a)));
                    return;
                }
                d("Unable to process ad of unknown type: " + a2);
                h.this.a(-800);
            }

            /* access modifiers changed from: private */
            public void g() {
                if (this.f3703g < this.f3702f.length() - 1) {
                    b("Attempting to load next ad (" + this.f3703g + ") after failure...");
                    this.f4130a.j().a(new b(this.f3703g + 1, this.f3702f), com.applovin.impl.mediation.d.c.a(h.this.f3697g));
                    return;
                }
                h.this.f();
            }

            public com.applovin.impl.sdk.d.i a() {
                return com.applovin.impl.sdk.d.i.H;
            }

            public void run() {
                try {
                    f();
                } catch (Throwable th) {
                    a("Encountered error while processing ad number " + this.f3703g, th);
                    this.f4130a.l().a(a());
                    h.this.f();
                }
            }
        }

        h(String str, MaxAdFormat maxAdFormat, JSONObject jSONObject, Activity activity, k kVar, MaxAdListener maxAdListener) {
            super("TaskProcessMediationWaterfall " + str, kVar);
            this.f3696f = str;
            this.f3697g = maxAdFormat;
            this.f3698h = jSONObject;
            this.f3699i = maxAdListener;
            this.f3700j = activity;
        }

        /* access modifiers changed from: private */
        public void a(int i2) {
            com.applovin.impl.sdk.d.h k2;
            com.applovin.impl.sdk.d.g gVar;
            if (i2 == 204) {
                k2 = this.f4130a.k();
                gVar = com.applovin.impl.sdk.d.g.s;
            } else if (i2 == -5001) {
                k2 = this.f4130a.k();
                gVar = com.applovin.impl.sdk.d.g.t;
            } else {
                k2 = this.f4130a.k();
                gVar = com.applovin.impl.sdk.d.g.u;
            }
            k2.a(gVar);
            b("Notifying parent of ad load failure for ad unit " + this.f3696f + ": " + i2);
            com.applovin.impl.sdk.utils.j.a(this.f3699i, this.f3696f, i2);
        }

        /* access modifiers changed from: private */
        public void a(MaxAd maxAd) {
            b("Notifying parent of ad load success for ad unit " + this.f3696f);
            com.applovin.impl.sdk.utils.j.a(this.f3699i, maxAd);
        }

        /* access modifiers changed from: private */
        public void f() {
            a((int) MaxErrorCodes.MEDIATION_ADAPTER_LOAD_FAILED);
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.G;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, long, com.applovin.impl.sdk.k):long
         arg types: [org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.k]
         candidates:
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, float, com.applovin.impl.sdk.k):float
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Boolean, com.applovin.impl.sdk.k):java.lang.Boolean
          com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, java.lang.Object, com.applovin.impl.sdk.k):java.lang.Object
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Object, com.applovin.impl.sdk.k):java.lang.Object
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.util.List, com.applovin.impl.sdk.k):java.util.List
          com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, org.json.JSONObject, com.applovin.impl.sdk.k):org.json.JSONObject
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.k):void
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.String, com.applovin.impl.sdk.k):void
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONArray, com.applovin.impl.sdk.k):void
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONObject, com.applovin.impl.sdk.k):void
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, long, com.applovin.impl.sdk.k):long */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Boolean, com.applovin.impl.sdk.k):java.lang.Boolean
         arg types: [org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.k]
         candidates:
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, float, com.applovin.impl.sdk.k):float
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, long, com.applovin.impl.sdk.k):long
          com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, java.lang.Object, com.applovin.impl.sdk.k):java.lang.Object
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Object, com.applovin.impl.sdk.k):java.lang.Object
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.util.List, com.applovin.impl.sdk.k):java.util.List
          com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, org.json.JSONObject, com.applovin.impl.sdk.k):org.json.JSONObject
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.k):void
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.String, com.applovin.impl.sdk.k):void
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONArray, com.applovin.impl.sdk.k):void
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONObject, com.applovin.impl.sdk.k):void
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Boolean, com.applovin.impl.sdk.k):java.lang.Boolean */
        public void run() {
            JSONArray optJSONArray = this.f3698h.optJSONArray("ads");
            int length = optJSONArray != null ? optJSONArray.length() : 0;
            if (length > 0) {
                a("Loading the first out of " + length + " ads...");
                this.f4130a.j().a(new b(0, optJSONArray));
                return;
            }
            c("No ads were returned from the server");
            p.a(this.f3696f, this.f3698h, this.f4130a);
            JSONObject b2 = com.applovin.impl.sdk.utils.i.b(this.f3698h, "settings", new JSONObject(), this.f4130a);
            long a2 = com.applovin.impl.sdk.utils.i.a(b2, "alfdcs", 0L, this.f4130a);
            if (a2 > 0) {
                long millis = TimeUnit.SECONDS.toMillis(a2);
                a aVar = new a();
                if (com.applovin.impl.sdk.utils.i.a(b2, "alfdcs_iba", (Boolean) false, this.f4130a).booleanValue()) {
                    com.applovin.impl.sdk.utils.d.a(millis, this.f4130a, aVar);
                } else {
                    AppLovinSdkUtils.runOnUiThreadDelayed(aVar, millis);
                }
            } else {
                a(204);
            }
        }
    }

    public class i extends f.g0 {

        /* renamed from: f  reason: collision with root package name */
        private final b.d f3706f;

        public i(b.d dVar, k kVar) {
            super("TaskReportMaxReward", kVar);
            this.f3706f = dVar;
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.N;
        }

        /* access modifiers changed from: protected */
        public void a(int i2) {
            a("Failed to report reward for mediated ad: " + this.f3706f + " - error code: " + i2);
        }

        /* access modifiers changed from: protected */
        public void a(JSONObject jSONObject) {
            com.applovin.impl.sdk.utils.i.a(jSONObject, "ad_unit_id", this.f3706f.getAdUnitId(), this.f4130a);
            com.applovin.impl.sdk.utils.i.a(jSONObject, "placement", this.f3706f.j(), this.f4130a);
            String G = this.f3706f.G();
            if (!m.b(G)) {
                G = "NO_MCODE";
            }
            com.applovin.impl.sdk.utils.i.a(jSONObject, "mcode", G, this.f4130a);
            String F = this.f3706f.F();
            if (!m.b(F)) {
                F = "NO_BCODE";
            }
            com.applovin.impl.sdk.utils.i.a(jSONObject, "bcode", F, this.f4130a);
        }

        /* access modifiers changed from: protected */
        public void b(JSONObject jSONObject) {
            a("Reported reward successfully for mediated ad: " + this.f3706f);
        }

        /* access modifiers changed from: protected */
        public String f() {
            return "2.0/mcr";
        }

        /* access modifiers changed from: protected */
        public com.applovin.impl.sdk.a.c h() {
            return this.f3706f.J();
        }

        /* access modifiers changed from: protected */
        public void i() {
            d("No reward result was found for mediated ad: " + this.f3706f);
        }
    }

    public class j extends f.h {

        /* renamed from: f  reason: collision with root package name */
        private final b.d f3707f;

        public j(b.d dVar, k kVar) {
            super("TaskValidateMaxReward", kVar);
            this.f3707f = dVar;
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.M;
        }

        /* access modifiers changed from: protected */
        public void a(int i2) {
            if (!h()) {
                this.f3707f.a(com.applovin.impl.sdk.a.c.a((i2 < 400 || i2 >= 500) ? "network_timeout" : "rejected"));
            }
        }

        /* access modifiers changed from: protected */
        public void a(com.applovin.impl.sdk.a.c cVar) {
            if (!h()) {
                this.f3707f.a(cVar);
            }
        }

        /* access modifiers changed from: protected */
        public void a(JSONObject jSONObject) {
            com.applovin.impl.sdk.utils.i.a(jSONObject, "ad_unit_id", this.f3707f.getAdUnitId(), this.f4130a);
            com.applovin.impl.sdk.utils.i.a(jSONObject, "placement", this.f3707f.j(), this.f4130a);
            String G = this.f3707f.G();
            if (!m.b(G)) {
                G = "NO_MCODE";
            }
            com.applovin.impl.sdk.utils.i.a(jSONObject, "mcode", G, this.f4130a);
            String F = this.f3707f.F();
            if (!m.b(F)) {
                F = "NO_BCODE";
            }
            com.applovin.impl.sdk.utils.i.a(jSONObject, "bcode", F, this.f4130a);
        }

        /* access modifiers changed from: protected */
        public String f() {
            return "2.0/mvr";
        }

        /* access modifiers changed from: protected */
        public boolean h() {
            return this.f3707f.H();
        }
    }

    c(k kVar, b bVar) {
        this.f3655a = kVar;
        this.f3656b = kVar.Z();
        this.f3657c = bVar;
    }

    public void a() {
        this.f3656b.b("AdHiddenCallbackTimeoutManager", "Cancelling timeout");
        com.applovin.impl.sdk.utils.d dVar = this.f3658d;
        if (dVar != null) {
            dVar.a();
            this.f3658d = null;
        }
    }

    public void a(b.d dVar, long j2) {
        q qVar = this.f3656b;
        qVar.b("AdHiddenCallbackTimeoutManager", "Scheduling in " + j2 + "ms...");
        this.f3658d = com.applovin.impl.sdk.utils.d.a(j2, this.f3655a, new a(dVar));
    }
}
