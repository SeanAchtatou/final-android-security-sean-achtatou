package com.applovin.impl.mediation.a$d.a;

import android.content.Context;
import com.applovin.impl.mediation.a;
import com.applovin.impl.mediation.a$d.a.a;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class b extends com.applovin.impl.mediation.a$d.b {

    /* renamed from: d  reason: collision with root package name */
    private final AtomicBoolean f3507d = new AtomicBoolean();

    /* renamed from: e  reason: collision with root package name */
    private final a.b.c f3508e = new a.b.g("INCOMPLETE INTEGRATIONS");

    /* renamed from: f  reason: collision with root package name */
    private final a.b.c f3509f = new a.b.g("COMPLETED INTEGRATIONS");

    /* renamed from: g  reason: collision with root package name */
    private final a.b.c f3510g = new a.b.g("MISSING INTEGRATIONS");

    /* renamed from: h  reason: collision with root package name */
    private final a.b.c f3511h = new a.b.g("");

    /* renamed from: i  reason: collision with root package name */
    private C0077b f3512i;

    class a implements Runnable {
        a() {
        }

        public void run() {
            b.this.notifyDataSetChanged();
        }
    }

    /* renamed from: com.applovin.impl.mediation.a$d.a.b$b  reason: collision with other inner class name */
    public interface C0077b {
        void a(a.b.d dVar);
    }

    public b(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void a(a.b.c cVar) {
        if (this.f3512i != null && (cVar instanceof a.c)) {
            this.f3512i.a(((a.c) cVar).i());
        }
    }

    public void a(C0077b bVar) {
        this.f3512i = bVar;
    }

    public void a(List<a.b.d> list) {
        if (list != null && this.f3507d.compareAndSet(false, true)) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            for (a.b.d next : list) {
                a.c cVar = new a.c(next, this.f3515b);
                if (next.a() == a.b.d.C0085a.INCOMPLETE_INTEGRATION || next.a() == a.b.d.C0085a.INVALID_INTEGRATION) {
                    arrayList.add(cVar);
                } else if (next.a() == a.b.d.C0085a.COMPLETE) {
                    arrayList2.add(cVar);
                } else if (next.a() == a.b.d.C0085a.MISSING) {
                    arrayList3.add(cVar);
                }
            }
            if (arrayList.size() > 0) {
                this.f3516c.add(this.f3508e);
                this.f3516c.addAll(arrayList);
            }
            if (arrayList2.size() > 0) {
                this.f3516c.add(this.f3509f);
                this.f3516c.addAll(arrayList2);
            }
            if (arrayList3.size() > 0) {
                this.f3516c.add(this.f3510g);
                this.f3516c.addAll(arrayList3);
            }
            this.f3516c.add(this.f3511h);
        }
        AppLovinSdkUtils.runOnUiThread(new a());
    }

    public boolean a() {
        return this.f3507d.get();
    }

    public String toString() {
        return "MediationDebuggerListAdapter{networksInitialized=" + this.f3507d.get() + ", listItems=" + this.f3516c + "}";
    }
}
