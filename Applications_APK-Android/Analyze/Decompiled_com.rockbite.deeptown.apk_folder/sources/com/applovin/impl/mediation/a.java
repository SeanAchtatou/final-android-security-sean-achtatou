package com.applovin.impl.mediation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannedString;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;
import com.applovin.impl.mediation.b;
import com.applovin.impl.sdk.f;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.network.a;
import com.applovin.impl.sdk.q;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.p;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxDebuggerActivity;
import com.applovin.mediation.adapter.MaxAdViewAdapter;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.mediation.adapter.MaxInterstitialAdapter;
import com.applovin.mediation.adapter.MaxRewardedAdapter;
import com.facebook.internal.NativeProtocol;
import com.google.ads.mediation.inmobi.InMobiNetworkValues;
import com.google.android.gms.drive.DriveFile;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class a extends com.applovin.impl.sdk.utils.a {

    /* renamed from: a  reason: collision with root package name */
    private final com.applovin.impl.sdk.b f3488a;

    /* renamed from: b  reason: collision with root package name */
    private final q f3489b;

    /* renamed from: c  reason: collision with root package name */
    private C0081a f3490c;

    /* renamed from: d  reason: collision with root package name */
    private b.d f3491d;

    /* renamed from: e  reason: collision with root package name */
    private int f3492e;

    /* renamed from: f  reason: collision with root package name */
    private boolean f3493f;

    /* renamed from: com.applovin.impl.mediation.a$a  reason: collision with other inner class name */
    public interface C0081a {
        void a(b.d dVar);
    }

    public class b implements a.c<JSONObject> {
        /* access modifiers changed from: private */

        /* renamed from: f  reason: collision with root package name */
        public static WeakReference<MaxDebuggerActivity> f3536f;
        /* access modifiers changed from: private */

        /* renamed from: g  reason: collision with root package name */
        public static final AtomicBoolean f3537g = new AtomicBoolean();
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final k f3538a;

        /* renamed from: b  reason: collision with root package name */
        private final q f3539b;
        /* access modifiers changed from: private */

        /* renamed from: c  reason: collision with root package name */
        public final com.applovin.impl.mediation.a$d.a.b f3540c;

        /* renamed from: d  reason: collision with root package name */
        private final AtomicBoolean f3541d = new AtomicBoolean();

        /* renamed from: e  reason: collision with root package name */
        private boolean f3542e;

        /* renamed from: com.applovin.impl.mediation.a$b$a  reason: collision with other inner class name */
        class C0082a extends com.applovin.impl.sdk.utils.a {
            C0082a() {
            }

            public void onActivityDestroyed(Activity activity) {
                if (activity instanceof MaxDebuggerActivity) {
                    b.this.f3538a.A().b(this);
                    WeakReference unused = b.f3536f = (WeakReference) null;
                }
            }

            public void onActivityStarted(Activity activity) {
                if (activity instanceof MaxDebuggerActivity) {
                    if (!b.this.f() || b.f3536f.get() != activity) {
                        MaxDebuggerActivity maxDebuggerActivity = (MaxDebuggerActivity) activity;
                        WeakReference unused = b.f3536f = new WeakReference(maxDebuggerActivity);
                        maxDebuggerActivity.setListAdapter(b.this.f3540c, b.this.f3538a.A());
                    }
                    b.f3537g.set(false);
                }
            }
        }

        /* renamed from: com.applovin.impl.mediation.a$b$b  reason: collision with other inner class name */
        public class C0083b {

            /* renamed from: a  reason: collision with root package name */
            public TextView f3544a;

            /* renamed from: b  reason: collision with root package name */
            public TextView f3545b;

            /* renamed from: c  reason: collision with root package name */
            public ImageView f3546c;

            /* renamed from: d  reason: collision with root package name */
            private c f3547d;

            public c a() {
                return this.f3547d;
            }

            public void a(c cVar) {
                this.f3547d = cVar;
                this.f3544a.setText(cVar.b());
                if (this.f3545b != null) {
                    if (!TextUtils.isEmpty(cVar.c())) {
                        this.f3545b.setVisibility(0);
                        this.f3545b.setText(cVar.c());
                    } else {
                        this.f3545b.setVisibility(8);
                    }
                }
                if (this.f3546c == null) {
                    return;
                }
                if (cVar.f() > 0) {
                    this.f3546c.setImageResource(cVar.f());
                    this.f3546c.setColorFilter(cVar.g());
                    this.f3546c.setVisibility(0);
                    return;
                }
                this.f3546c.setVisibility(8);
            }
        }

        public abstract class c {

            /* renamed from: a  reason: collision with root package name */
            protected C0084a f3548a;

            /* renamed from: b  reason: collision with root package name */
            protected SpannedString f3549b;

            /* renamed from: c  reason: collision with root package name */
            protected SpannedString f3550c;

            /* renamed from: com.applovin.impl.mediation.a$b$c$a  reason: collision with other inner class name */
            public enum C0084a {
                SECTION(0),
                SIMPLE(1),
                DETAIL(2),
                RIGHT_DETAIL(3),
                COUNT(4);
                

                /* renamed from: a  reason: collision with root package name */
                private final int f3557a;

                private C0084a(int i2) {
                    this.f3557a = i2;
                }

                public int a() {
                    return this.f3557a;
                }

                public int b() {
                    if (this == SECTION) {
                        return com.applovin.sdk.d.list_section;
                    }
                    if (this == SIMPLE) {
                        return 17367043;
                    }
                    return this == DETAIL ? com.applovin.sdk.d.list_item_detail : com.applovin.sdk.d.list_item_right_detail;
                }
            }

            public c(C0084a aVar) {
                this.f3548a = aVar;
            }

            public static int h() {
                return C0084a.COUNT.a();
            }

            public boolean a() {
                return false;
            }

            public SpannedString b() {
                return this.f3549b;
            }

            public SpannedString c() {
                return this.f3550c;
            }

            public int d() {
                return this.f3548a.a();
            }

            public int e() {
                return this.f3548a.b();
            }

            public int f() {
                return 0;
            }

            public int g() {
                return -16777216;
            }
        }

        public class d implements Comparable<d> {

            /* renamed from: i  reason: collision with root package name */
            private static String f3558i = "MediatedNetwork";

            /* renamed from: a  reason: collision with root package name */
            private final C0085a f3559a;

            /* renamed from: b  reason: collision with root package name */
            private final boolean f3560b;

            /* renamed from: c  reason: collision with root package name */
            private final boolean f3561c;

            /* renamed from: d  reason: collision with root package name */
            private final String f3562d;

            /* renamed from: e  reason: collision with root package name */
            private final String f3563e;

            /* renamed from: f  reason: collision with root package name */
            private final String f3564f;

            /* renamed from: g  reason: collision with root package name */
            private final List<f> f3565g;

            /* renamed from: h  reason: collision with root package name */
            private final e f3566h;

            /* renamed from: com.applovin.impl.mediation.a$b$d$a  reason: collision with other inner class name */
            public enum C0085a {
                MISSING,
                INCOMPLETE_INTEGRATION,
                INVALID_INTEGRATION,
                COMPLETE
            }

            public d(JSONObject jSONObject, k kVar) {
                String str;
                String str2 = "";
                this.f3562d = i.b(jSONObject, "display_name", str2, kVar);
                i.b(jSONObject, "name", str2, kVar);
                JSONObject b2 = i.b(jSONObject, "configuration", new JSONObject(), kVar);
                this.f3565g = a(b2, kVar, kVar.d());
                this.f3566h = new e(b2, kVar);
                this.f3560b = p.e(i.b(jSONObject, "existence_class", str2, kVar));
                Collections.emptyList();
                MaxAdapter a2 = com.applovin.impl.mediation.d.c.a(i.b(jSONObject, "adapter_class", str2, kVar), kVar);
                if (a2 != null) {
                    this.f3561c = true;
                    try {
                        String adapterVersion = a2.getAdapterVersion();
                        try {
                            str2 = a2.getSdkVersion();
                            a(a2);
                            String str3 = str2;
                            str2 = adapterVersion;
                            str = str3;
                        } catch (Throwable th) {
                            th = th;
                            String str4 = str2;
                            str2 = adapterVersion;
                            str = str4;
                            String str5 = f3558i;
                            q.i(str5, "Failed to load adapter for network " + this.f3562d + ". Please check that you have a compatible network SDK integrated. Error: " + th);
                            this.f3564f = str2;
                            this.f3563e = str;
                            this.f3559a = i();
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        str = str2;
                        String str52 = f3558i;
                        q.i(str52, "Failed to load adapter for network " + this.f3562d + ". Please check that you have a compatible network SDK integrated. Error: " + th);
                        this.f3564f = str2;
                        this.f3563e = str;
                        this.f3559a = i();
                    }
                } else {
                    this.f3561c = false;
                    str = str2;
                }
                this.f3564f = str2;
                this.f3563e = str;
                this.f3559a = i();
            }

            private List<MaxAdFormat> a(MaxAdapter maxAdapter) {
                ArrayList arrayList = new ArrayList(5);
                if (maxAdapter instanceof MaxInterstitialAdapter) {
                    arrayList.add(MaxAdFormat.INTERSTITIAL);
                }
                if (maxAdapter instanceof MaxRewardedAdapter) {
                    arrayList.add(MaxAdFormat.REWARDED);
                }
                if (maxAdapter instanceof MaxAdViewAdapter) {
                    arrayList.add(MaxAdFormat.BANNER);
                    arrayList.add(MaxAdFormat.LEADER);
                    arrayList.add(MaxAdFormat.MREC);
                }
                return arrayList;
            }

            private List<f> a(JSONObject jSONObject, k kVar, Context context) {
                ArrayList arrayList = new ArrayList();
                JSONObject b2 = i.b(jSONObject, NativeProtocol.RESULT_ARGS_PERMISSIONS, new JSONObject(), kVar);
                Iterator<String> keys = b2.keys();
                while (keys.hasNext()) {
                    try {
                        String next = keys.next();
                        arrayList.add(new f(next, b2.getString(next), context));
                    } catch (JSONException unused) {
                    }
                }
                return arrayList;
            }

            private C0085a i() {
                if (!this.f3560b && !this.f3561c) {
                    return C0085a.MISSING;
                }
                for (f c2 : this.f3565g) {
                    if (!c2.c()) {
                        return C0085a.INVALID_INTEGRATION;
                    }
                }
                return (!this.f3566h.a() || this.f3566h.b()) ? (!this.f3560b || !this.f3561c) ? C0085a.INCOMPLETE_INTEGRATION : C0085a.COMPLETE : C0085a.INVALID_INTEGRATION;
            }

            /* renamed from: a */
            public int compareTo(d dVar) {
                return this.f3562d.compareToIgnoreCase(dVar.f3562d);
            }

            public C0085a a() {
                return this.f3559a;
            }

            public boolean b() {
                return this.f3560b;
            }

            public boolean c() {
                return this.f3561c;
            }

            public String d() {
                return this.f3562d;
            }

            public String e() {
                return this.f3563e;
            }

            public String f() {
                return this.f3564f;
            }

            public List<f> g() {
                return this.f3565g;
            }

            public final e h() {
                return this.f3566h;
            }

            public String toString() {
                return "MediatedNetwork{name=" + this.f3562d + ", sdkAvailable=" + this.f3560b + ", sdkVersion=" + this.f3563e + ", adapterAvailable=" + this.f3561c + ", adapterVersion=" + this.f3564f + "}";
            }
        }

        public class e {

            /* renamed from: a  reason: collision with root package name */
            private final boolean f3572a;

            /* renamed from: b  reason: collision with root package name */
            private final boolean f3573b;

            /* renamed from: c  reason: collision with root package name */
            private final boolean f3574c;

            /* renamed from: d  reason: collision with root package name */
            private final String f3575d;

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.util.List, com.applovin.impl.sdk.k):java.util.List
             arg types: [org.json.JSONObject, java.lang.String, java.util.ArrayList, com.applovin.impl.sdk.k]
             candidates:
              com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, float, com.applovin.impl.sdk.k):float
              com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, long, com.applovin.impl.sdk.k):long
              com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Boolean, com.applovin.impl.sdk.k):java.lang.Boolean
              com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, java.lang.Object, com.applovin.impl.sdk.k):java.lang.Object
              com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Object, com.applovin.impl.sdk.k):java.lang.Object
              com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, org.json.JSONObject, com.applovin.impl.sdk.k):org.json.JSONObject
              com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.k):void
              com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.String, com.applovin.impl.sdk.k):void
              com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONArray, com.applovin.impl.sdk.k):void
              com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONObject, com.applovin.impl.sdk.k):void
              com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.util.List, com.applovin.impl.sdk.k):java.util.List */
            public e(JSONObject jSONObject, k kVar) {
                this.f3572a = com.applovin.impl.sdk.utils.c.a(kVar.d()).a();
                JSONObject b2 = i.b(jSONObject, "cleartext_traffic", (JSONObject) null, kVar);
                boolean z = false;
                if (b2 != null) {
                    this.f3573b = true;
                    this.f3575d = i.b(b2, InMobiNetworkValues.DESCRIPTION, "", kVar);
                    if (h.a()) {
                        this.f3574c = true;
                        return;
                    }
                    List a2 = i.a(b2, "domains", (List) new ArrayList(), kVar);
                    if (a2.size() > 0) {
                        Iterator it = a2.iterator();
                        while (true) {
                            if (it.hasNext()) {
                                if (!h.a((String) it.next())) {
                                    break;
                                }
                            } else {
                                z = true;
                                break;
                            }
                        }
                    }
                    this.f3574c = z;
                    return;
                }
                this.f3573b = false;
                this.f3575d = "";
                this.f3574c = h.a();
            }

            public boolean a() {
                return this.f3573b;
            }

            public boolean b() {
                return this.f3574c;
            }

            public String c() {
                return this.f3572a ? this.f3575d : "You must include an entry in your AndroidManifest.xml to point to your network_security_config.xml.\n\nFor more information, visit: https://developer.android.com/training/articles/security-config";
            }
        }

        public class f {

            /* renamed from: a  reason: collision with root package name */
            private final String f3576a;

            /* renamed from: b  reason: collision with root package name */
            private final String f3577b;

            /* renamed from: c  reason: collision with root package name */
            private final boolean f3578c;

            f(String str, String str2, Context context) {
                this.f3576a = str;
                this.f3577b = str2;
                this.f3578c = com.applovin.impl.sdk.utils.g.a(str, context);
            }

            public String a() {
                return this.f3576a;
            }

            public String b() {
                return this.f3577b;
            }

            public boolean c() {
                return this.f3578c;
            }
        }

        public class g extends c {
            public g(String str) {
                super(c.C0084a.SECTION);
                this.f3549b = new SpannedString(str);
            }

            public String toString() {
                return "SectionListItemViewModel{text=" + ((Object) this.f3549b) + "}";
            }
        }

        public b(k kVar) {
            this.f3538a = kVar;
            this.f3539b = kVar.Z();
            this.f3540c = new com.applovin.impl.mediation.a$d.a.b(kVar.d());
        }

        private void a(JSONArray jSONArray) {
            this.f3539b.b("MediationDebuggerService", "Updating networks...");
            try {
                ArrayList arrayList = new ArrayList(jSONArray.length());
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    JSONObject a2 = i.a(jSONArray, i2, (JSONObject) null, this.f3538a);
                    if (a2 != null) {
                        arrayList.add(new d(a2, this.f3538a));
                    }
                }
                Collections.sort(arrayList);
                this.f3540c.a(arrayList);
            } catch (Throwable th) {
                this.f3539b.b("MediationDebuggerService", "Failed to parse mediated network json object.", th);
            }
        }

        private void e() {
            if (this.f3541d.compareAndSet(false, true)) {
                this.f3538a.j().a(new com.applovin.impl.mediation.a$c.a(this, this.f3538a), f.y.b.MEDIATION_MAIN);
            }
        }

        /* access modifiers changed from: private */
        public boolean f() {
            WeakReference<MaxDebuggerActivity> weakReference = f3536f;
            return (weakReference == null || weakReference.get() == null) ? false : true;
        }

        public void a(int i2) {
            q qVar = this.f3539b;
            qVar.e("MediationDebuggerService", "Unable to fetch mediation debugger info: server returned " + i2);
            q.i("AppLovinSdk", "Unable to show mediation debugger.");
            this.f3540c.a((List<d>) null);
            this.f3541d.set(false);
        }

        public void a(JSONObject jSONObject, int i2) {
            a(i.b(jSONObject, "networks", new JSONArray(), this.f3538a));
        }

        public void a(boolean z) {
            this.f3542e = z;
        }

        public boolean a() {
            return this.f3542e;
        }

        public void b() {
            e();
            if (f() || !f3537g.compareAndSet(false, true)) {
                q.i("AppLovinSdk", "Mediation Debugger is already showing.");
                return;
            }
            this.f3538a.A().a(new C0082a());
            Context d2 = this.f3538a.d();
            Intent intent = new Intent(d2, MaxDebuggerActivity.class);
            intent.setFlags(DriveFile.MODE_READ_ONLY);
            d2.startActivity(intent);
        }

        public String toString() {
            return "MediationDebuggerService{, listAdapter=" + this.f3540c + "}";
        }
    }

    a(k kVar) {
        this.f3489b = kVar.Z();
        this.f3488a = kVar.A();
    }

    public void a() {
        this.f3489b.b("AdActivityObserver", "Cancelling...");
        this.f3488a.b(this);
        this.f3490c = null;
        this.f3491d = null;
        this.f3492e = 0;
        this.f3493f = false;
    }

    public void a(b.d dVar, C0081a aVar) {
        q qVar = this.f3489b;
        qVar.b("AdActivityObserver", "Starting for ad " + dVar.getAdUnitId() + "...");
        a();
        this.f3490c = aVar;
        this.f3491d = dVar;
        this.f3488a.a(this);
    }

    public void onActivityCreated(Activity activity, Bundle bundle) {
        if (!this.f3493f) {
            this.f3493f = true;
        }
        this.f3492e++;
        this.f3489b.b("AdActivityObserver", "Created Activity: " + activity + ", counter is " + this.f3492e);
    }

    public void onActivityDestroyed(Activity activity) {
        if (this.f3493f) {
            this.f3492e--;
            this.f3489b.b("AdActivityObserver", "Destroyed Activity: " + activity + ", counter is " + this.f3492e);
            if (this.f3492e <= 0) {
                this.f3489b.b("AdActivityObserver", "Last ad Activity destroyed");
                if (this.f3490c != null) {
                    this.f3489b.b("AdActivityObserver", "Invoking callback...");
                    this.f3490c.a(this.f3491d);
                }
                a();
            }
        }
    }
}
