package com.applovin.impl.mediation.ads;

import android.app.Activity;
import com.applovin.impl.mediation.MediationServiceImpl;
import com.applovin.impl.mediation.b;
import com.applovin.impl.mediation.k;
import com.applovin.impl.mediation.m;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.q;
import com.applovin.impl.sdk.utils.j;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxErrorCodes;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.MaxRewardedAdListener;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class MaxFullscreenAdImpl extends a implements c.b {

    /* renamed from: a  reason: collision with root package name */
    private final d f3601a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final com.applovin.impl.sdk.c f3602b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public final com.applovin.impl.mediation.b f3603c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public final Object f3604d = new Object();

    /* renamed from: e  reason: collision with root package name */
    private b.d f3605e = null;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public b.d f3606f = null;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public b.d f3607g = null;

    /* renamed from: h  reason: collision with root package name */
    private f f3608h = f.IDLE;
    /* access modifiers changed from: private */

    /* renamed from: i  reason: collision with root package name */
    public final AtomicBoolean f3609i = new AtomicBoolean();
    protected final e listenerWrapper;

    class a implements Runnable {
        a() {
        }

        public void run() {
            synchronized (MaxFullscreenAdImpl.this.f3604d) {
                if (MaxFullscreenAdImpl.this.f3606f != null) {
                    q qVar = MaxFullscreenAdImpl.this.logger;
                    String str = MaxFullscreenAdImpl.this.tag;
                    qVar.b(str, "Destroying ad for '" + MaxFullscreenAdImpl.this.adUnitId + "'; current ad: " + MaxFullscreenAdImpl.this.f3606f + "...");
                    MaxFullscreenAdImpl.this.sdk.c0().destroyAd(MaxFullscreenAdImpl.this.f3606f);
                }
            }
        }
    }

    class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Activity f3611a;

        class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ b.d f3613a;

            a(b.d dVar) {
                this.f3613a = dVar;
            }

            public void run() {
                j.a(MaxFullscreenAdImpl.this.adListener, this.f3613a);
            }
        }

        b(Activity activity) {
            this.f3611a = activity;
        }

        public void run() {
            b.d c2 = MaxFullscreenAdImpl.this.a();
            if (c2 == null || c2.x()) {
                MaxFullscreenAdImpl.this.sdk.b().a(MaxFullscreenAdImpl.this.listenerWrapper);
                MediationServiceImpl c0 = MaxFullscreenAdImpl.this.sdk.c0();
                MaxFullscreenAdImpl maxFullscreenAdImpl = MaxFullscreenAdImpl.this;
                c0.loadAd(maxFullscreenAdImpl.adUnitId, maxFullscreenAdImpl.adFormat, maxFullscreenAdImpl.loadRequestBuilder.a(), false, this.f3611a, MaxFullscreenAdImpl.this.listenerWrapper);
                return;
            }
            MaxFullscreenAdImpl.this.a(f.READY, new a(c2));
        }
    }

    class c implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ String f3615a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Activity f3616b;

        c(String str, Activity activity) {
            this.f3615a = str;
            this.f3616b = activity;
        }

        public void run() {
            MaxFullscreenAdImpl.this.a(this.f3615a, this.f3616b);
        }
    }

    public interface d {
        Activity getActivity();
    }

    private class e implements k.c, MaxAdListener, MaxRewardedAdListener {

        class a implements Runnable {
            a() {
            }

            public void run() {
                MaxFullscreenAdImpl maxFullscreenAdImpl = MaxFullscreenAdImpl.this;
                j.a(maxFullscreenAdImpl.adListener, maxFullscreenAdImpl.d());
            }
        }

        class b implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ String f3620a;

            /* renamed from: b  reason: collision with root package name */
            final /* synthetic */ int f3621b;

            b(String str, int i2) {
                this.f3620a = str;
                this.f3621b = i2;
            }

            public void run() {
                j.a(MaxFullscreenAdImpl.this.adListener, this.f3620a, this.f3621b);
            }
        }

        class c implements Runnable {
            c() {
            }

            public void run() {
                MaxFullscreenAdImpl.this.b();
                MaxFullscreenAdImpl maxFullscreenAdImpl = MaxFullscreenAdImpl.this;
                j.c(maxFullscreenAdImpl.adListener, maxFullscreenAdImpl.d());
            }
        }

        class d implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ int f3624a;

            d(int i2) {
                this.f3624a = i2;
            }

            public void run() {
                MaxFullscreenAdImpl.this.f3602b.a();
                MaxFullscreenAdImpl.this.b();
                MaxFullscreenAdImpl maxFullscreenAdImpl = MaxFullscreenAdImpl.this;
                j.a(maxFullscreenAdImpl.adListener, maxFullscreenAdImpl.d(), this.f3624a);
            }
        }

        private e() {
        }

        /* synthetic */ e(MaxFullscreenAdImpl maxFullscreenAdImpl, a aVar) {
            this();
        }

        public void a(b.d dVar) {
            if (dVar.getFormat() == MaxFullscreenAdImpl.this.adFormat) {
                onAdLoaded(dVar);
            }
        }

        public void onAdClicked(MaxAd maxAd) {
            MaxFullscreenAdImpl maxFullscreenAdImpl = MaxFullscreenAdImpl.this;
            j.d(maxFullscreenAdImpl.adListener, maxFullscreenAdImpl.d());
        }

        public void onAdDisplayFailed(MaxAd maxAd, int i2) {
            MaxFullscreenAdImpl.this.a(f.IDLE, new d(i2));
        }

        public void onAdDisplayed(MaxAd maxAd) {
            if (!((b.d) maxAd).x()) {
                MaxFullscreenAdImpl.this.f3602b.a();
            }
            MaxFullscreenAdImpl maxFullscreenAdImpl = MaxFullscreenAdImpl.this;
            j.b(maxFullscreenAdImpl.adListener, maxFullscreenAdImpl.d());
        }

        public void onAdHidden(MaxAd maxAd) {
            MaxFullscreenAdImpl.this.f3603c.a(maxAd);
            MaxFullscreenAdImpl.this.a(f.IDLE, new c());
        }

        public void onAdLoadFailed(String str, int i2) {
            MaxFullscreenAdImpl.this.c();
            if (MaxFullscreenAdImpl.this.f3607g == null) {
                MaxFullscreenAdImpl.this.a(f.IDLE, new b(str, i2));
            }
        }

        public void onAdLoaded(MaxAd maxAd) {
            b.d dVar = (b.d) maxAd;
            MaxFullscreenAdImpl.this.a(dVar);
            if (dVar.x() || !MaxFullscreenAdImpl.this.f3609i.compareAndSet(true, false)) {
                MaxFullscreenAdImpl.this.a(f.READY, new a());
            } else {
                MaxFullscreenAdImpl.this.loadRequestBuilder.a("expired_ad_ad_unit_id");
            }
        }

        public void onRewardedVideoCompleted(MaxAd maxAd) {
            MaxFullscreenAdImpl maxFullscreenAdImpl = MaxFullscreenAdImpl.this;
            j.f(maxFullscreenAdImpl.adListener, maxFullscreenAdImpl.d());
        }

        public void onRewardedVideoStarted(MaxAd maxAd) {
            MaxFullscreenAdImpl maxFullscreenAdImpl = MaxFullscreenAdImpl.this;
            j.e(maxFullscreenAdImpl.adListener, maxFullscreenAdImpl.d());
        }

        public void onUserRewarded(MaxAd maxAd, MaxReward maxReward) {
            MaxFullscreenAdImpl maxFullscreenAdImpl = MaxFullscreenAdImpl.this;
            j.a(maxFullscreenAdImpl.adListener, maxFullscreenAdImpl.d(), maxReward);
        }
    }

    public enum f {
        IDLE,
        LOADING,
        READY,
        SHOWING,
        DESTROYED
    }

    public MaxFullscreenAdImpl(String str, MaxAdFormat maxAdFormat, d dVar, String str2, com.applovin.impl.sdk.k kVar) {
        super(str, maxAdFormat, str2, kVar);
        this.f3601a = dVar;
        this.listenerWrapper = new e(this, null);
        this.f3602b = new com.applovin.impl.sdk.c(kVar, this);
        this.f3603c = new com.applovin.impl.mediation.b(kVar, this.listenerWrapper);
        q.f(str2, "Created new " + str2 + " (" + this + ")");
    }

    /* access modifiers changed from: private */
    public b.d a() {
        b.d dVar;
        synchronized (this.f3604d) {
            dVar = this.f3606f != null ? this.f3606f : this.f3607g;
        }
        return dVar;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0144  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x016e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.f r7, java.lang.Runnable r8) {
        /*
            r6 = this;
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$f r0 = r6.f3608h
            java.lang.Object r1 = r6.f3604d
            monitor-enter(r1)
            com.applovin.impl.sdk.q r2 = r6.logger     // Catch:{ all -> 0x0199 }
            java.lang.String r3 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0199 }
            r4.<init>()     // Catch:{ all -> 0x0199 }
            java.lang.String r5 = "Attempting state transition from "
            r4.append(r5)     // Catch:{ all -> 0x0199 }
            r4.append(r0)     // Catch:{ all -> 0x0199 }
            java.lang.String r5 = " to "
            r4.append(r5)     // Catch:{ all -> 0x0199 }
            r4.append(r7)     // Catch:{ all -> 0x0199 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0199 }
            r2.b(r3, r4)     // Catch:{ all -> 0x0199 }
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$f r2 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.f.IDLE     // Catch:{ all -> 0x0199 }
            r3 = 1
            r4 = 0
            if (r0 != r2) goto L_0x005e
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$f r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.f.LOADING     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x0032
        L_0x002f:
            r4 = 1
            goto L_0x0142
        L_0x0032:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$f r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.f.DESTROYED     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x0037
            goto L_0x002f
        L_0x0037:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$f r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.f.SHOWING     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x0044
            java.lang.String r0 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = "No ad is loading or loaded"
        L_0x003f:
            com.applovin.impl.sdk.q.i(r0, r2)     // Catch:{ all -> 0x0199 }
            goto L_0x0142
        L_0x0044:
            com.applovin.impl.sdk.q r0 = r6.logger     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0199 }
            r3.<init>()     // Catch:{ all -> 0x0199 }
            java.lang.String r5 = "Unable to transition to: "
            r3.append(r5)     // Catch:{ all -> 0x0199 }
            r3.append(r7)     // Catch:{ all -> 0x0199 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0199 }
        L_0x0059:
            r0.e(r2, r3)     // Catch:{ all -> 0x0199 }
            goto L_0x0142
        L_0x005e:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$f r2 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.f.LOADING     // Catch:{ all -> 0x0199 }
            if (r0 != r2) goto L_0x0099
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$f r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.f.IDLE     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x0067
            goto L_0x002f
        L_0x0067:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$f r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.f.LOADING     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x0070
            java.lang.String r0 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = "An ad is already loading"
            goto L_0x003f
        L_0x0070:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$f r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.f.READY     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x0075
            goto L_0x002f
        L_0x0075:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$f r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.f.SHOWING     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x007e
            java.lang.String r0 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = "An ad is not ready to be shown yet"
            goto L_0x003f
        L_0x007e:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$f r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.f.DESTROYED     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x0083
            goto L_0x002f
        L_0x0083:
            com.applovin.impl.sdk.q r0 = r6.logger     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0199 }
            r3.<init>()     // Catch:{ all -> 0x0199 }
            java.lang.String r5 = "Unable to transition to: "
            r3.append(r5)     // Catch:{ all -> 0x0199 }
            r3.append(r7)     // Catch:{ all -> 0x0199 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0199 }
            goto L_0x0059
        L_0x0099:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$f r2 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.f.READY     // Catch:{ all -> 0x0199 }
            if (r0 != r2) goto L_0x00d8
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$f r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.f.IDLE     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x00a2
            goto L_0x002f
        L_0x00a2:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$f r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.f.LOADING     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x00ab
            java.lang.String r0 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = "An ad is already loaded"
            goto L_0x003f
        L_0x00ab:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$f r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.f.READY     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x00b6
            com.applovin.impl.sdk.q r0 = r6.logger     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.String r3 = "An ad is already marked as ready"
            goto L_0x0059
        L_0x00b6:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$f r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.f.SHOWING     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x00bc
            goto L_0x002f
        L_0x00bc:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$f r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.f.DESTROYED     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x00c2
            goto L_0x002f
        L_0x00c2:
            com.applovin.impl.sdk.q r0 = r6.logger     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0199 }
            r3.<init>()     // Catch:{ all -> 0x0199 }
            java.lang.String r5 = "Unable to transition to: "
            r3.append(r5)     // Catch:{ all -> 0x0199 }
            r3.append(r7)     // Catch:{ all -> 0x0199 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0199 }
            goto L_0x0059
        L_0x00d8:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$f r2 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.f.SHOWING     // Catch:{ all -> 0x0199 }
            if (r0 != r2) goto L_0x011f
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$f r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.f.IDLE     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x00e2
            goto L_0x002f
        L_0x00e2:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$f r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.f.LOADING     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x00ec
            java.lang.String r0 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = "Can not load another ad while the ad is showing"
            goto L_0x003f
        L_0x00ec:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$f r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.f.READY     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x00f8
            com.applovin.impl.sdk.q r0 = r6.logger     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.String r3 = "An ad is already showing, ignoring"
            goto L_0x0059
        L_0x00f8:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$f r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.f.SHOWING     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x0102
            java.lang.String r0 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = "The ad is already showing, not showing another one"
            goto L_0x003f
        L_0x0102:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$f r0 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.f.DESTROYED     // Catch:{ all -> 0x0199 }
            if (r7 != r0) goto L_0x0108
            goto L_0x002f
        L_0x0108:
            com.applovin.impl.sdk.q r0 = r6.logger     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0199 }
            r3.<init>()     // Catch:{ all -> 0x0199 }
            java.lang.String r5 = "Unable to transition to: "
            r3.append(r5)     // Catch:{ all -> 0x0199 }
            r3.append(r7)     // Catch:{ all -> 0x0199 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0199 }
            goto L_0x0059
        L_0x011f:
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$f r2 = com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.f.DESTROYED     // Catch:{ all -> 0x0199 }
            if (r0 != r2) goto L_0x0129
            java.lang.String r0 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = "No operations are allowed on a destroyed instance"
            goto L_0x003f
        L_0x0129:
            com.applovin.impl.sdk.q r0 = r6.logger     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0199 }
            r3.<init>()     // Catch:{ all -> 0x0199 }
            java.lang.String r5 = "Unknown state: "
            r3.append(r5)     // Catch:{ all -> 0x0199 }
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$f r5 = r6.f3608h     // Catch:{ all -> 0x0199 }
            r3.append(r5)     // Catch:{ all -> 0x0199 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0199 }
            goto L_0x0059
        L_0x0142:
            if (r4 == 0) goto L_0x016e
            com.applovin.impl.sdk.q r0 = r6.logger     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0199 }
            r3.<init>()     // Catch:{ all -> 0x0199 }
            java.lang.String r5 = "Transitioning from "
            r3.append(r5)     // Catch:{ all -> 0x0199 }
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$f r5 = r6.f3608h     // Catch:{ all -> 0x0199 }
            r3.append(r5)     // Catch:{ all -> 0x0199 }
            java.lang.String r5 = " to "
            r3.append(r5)     // Catch:{ all -> 0x0199 }
            r3.append(r7)     // Catch:{ all -> 0x0199 }
            java.lang.String r5 = "..."
            r3.append(r5)     // Catch:{ all -> 0x0199 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0199 }
            r0.b(r2, r3)     // Catch:{ all -> 0x0199 }
            r6.f3608h = r7     // Catch:{ all -> 0x0199 }
            goto L_0x0190
        L_0x016e:
            com.applovin.impl.sdk.q r0 = r6.logger     // Catch:{ all -> 0x0199 }
            java.lang.String r2 = r6.tag     // Catch:{ all -> 0x0199 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0199 }
            r3.<init>()     // Catch:{ all -> 0x0199 }
            java.lang.String r5 = "Not allowed transition from "
            r3.append(r5)     // Catch:{ all -> 0x0199 }
            com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$f r5 = r6.f3608h     // Catch:{ all -> 0x0199 }
            r3.append(r5)     // Catch:{ all -> 0x0199 }
            java.lang.String r5 = " to "
            r3.append(r5)     // Catch:{ all -> 0x0199 }
            r3.append(r7)     // Catch:{ all -> 0x0199 }
            java.lang.String r7 = r3.toString()     // Catch:{ all -> 0x0199 }
            r0.d(r2, r7)     // Catch:{ all -> 0x0199 }
        L_0x0190:
            monitor-exit(r1)     // Catch:{ all -> 0x0199 }
            if (r4 == 0) goto L_0x0198
            if (r8 == 0) goto L_0x0198
            r8.run()
        L_0x0198:
            return
        L_0x0199:
            r7 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0199 }
            goto L_0x019d
        L_0x019c:
            throw r7
        L_0x019d:
            goto L_0x019c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.mediation.ads.MaxFullscreenAdImpl.a(com.applovin.impl.mediation.ads.MaxFullscreenAdImpl$f, java.lang.Runnable):void");
    }

    /* access modifiers changed from: private */
    public void a(b.d dVar) {
        if (dVar.x()) {
            this.f3607g = dVar;
            q qVar = this.logger;
            String str = this.tag;
            qVar.b(str, "Handle ad loaded for fallback ad: " + dVar);
            return;
        }
        this.f3606f = dVar;
        q qVar2 = this.logger;
        String str2 = this.tag;
        qVar2.b(str2, "Handle ad loaded for regular ad: " + dVar);
        b(dVar);
    }

    /* access modifiers changed from: private */
    public void a(String str, Activity activity) {
        synchronized (this.f3604d) {
            this.f3605e = a();
            this.sdk.b().b(this.listenerWrapper);
            if (this.f3605e.x()) {
                if (this.f3605e.v().get()) {
                    q qVar = this.logger;
                    String str2 = this.tag;
                    qVar.e(str2, "Failed to display ad: " + this.f3605e + " - displayed already");
                    this.sdk.c0().maybeScheduleAdDisplayErrorPostback(new com.applovin.impl.mediation.f(-5201, "Ad displayed already"), this.f3605e);
                    j.a(this.adListener, d(), -5201);
                    return;
                }
                this.sdk.b().a(this.listenerWrapper, this.adFormat);
            }
            this.f3605e.d(this.adUnitId);
            this.f3603c.c(this.f3605e);
            q qVar2 = this.logger;
            String str3 = this.tag;
            qVar2.b(str3, "Showing ad for '" + this.adUnitId + "'; loaded ad: " + this.f3605e + "...");
            this.sdk.c0().showFullscreenAd(this.f3605e, str, activity);
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        b.d dVar;
        synchronized (this.f3604d) {
            dVar = this.f3605e;
            this.f3605e = null;
            if (dVar == this.f3607g) {
                this.f3607g = null;
            } else if (dVar == this.f3606f) {
                this.f3606f = null;
            }
        }
        this.sdk.c0().destroyAd(dVar);
    }

    private void b(b.d dVar) {
        long B = dVar.B();
        if (B >= 0) {
            q qVar = this.logger;
            String str = this.tag;
            qVar.b(str, "Scheduling ad expiration " + TimeUnit.MILLISECONDS.toMinutes(B) + " minutes from now for " + getAdUnitId() + " ...");
            this.f3602b.a(B);
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        b.d dVar;
        if (this.f3609i.compareAndSet(true, false)) {
            synchronized (this.f3604d) {
                dVar = this.f3606f;
                this.f3606f = null;
            }
            this.sdk.c0().destroyAd(dVar);
            this.loadRequestBuilder.a("expired_ad_ad_unit_id");
        }
    }

    /* access modifiers changed from: private */
    public m d() {
        return new m(this.adUnitId, this.adFormat);
    }

    public void destroy() {
        a(f.DESTROYED, new a());
    }

    public boolean isReady() {
        boolean z;
        synchronized (this.f3604d) {
            z = a() != null && a().o() && this.f3608h == f.READY;
        }
        return z;
    }

    public void loadAd(Activity activity) {
        q qVar = this.logger;
        String str = this.tag;
        qVar.b(str, "Loading ad for '" + this.adUnitId + "'...");
        if (isReady()) {
            q qVar2 = this.logger;
            String str2 = this.tag;
            qVar2.b(str2, "An ad is already loaded for '" + this.adUnitId + "'");
            j.a(this.adListener, d());
            return;
        }
        a(f.LOADING, new b(activity));
    }

    public void onAdExpired() {
        q qVar = this.logger;
        String str = this.tag;
        qVar.b(str, "Ad expired " + getAdUnitId());
        Activity activity = this.f3601a.getActivity();
        if (activity == null) {
            activity = this.sdk.A().a();
            if (!((Boolean) this.sdk.a(c.d.B4)).booleanValue() || activity == null) {
                this.listenerWrapper.onAdLoadFailed(this.adUnitId, MaxErrorCodes.NO_ACTIVITY);
                return;
            }
        }
        this.f3609i.set(true);
        this.loadRequestBuilder.a("expired_ad_ad_unit_id", getAdUnitId());
        this.sdk.c0().loadAd(this.adUnitId, this.adFormat, this.loadRequestBuilder.a(), false, activity, this.listenerWrapper);
    }

    public void showAd(String str, Activity activity) {
        if (!((Boolean) this.sdk.a(c.d.y4)).booleanValue() || (!this.sdk.y().a() && !this.sdk.y().b())) {
            a(f.SHOWING, new c(str, activity));
            return;
        }
        q.i(this.tag, "Attempting to show ad when another fullscreen ad is already showing");
        j.a(this.adListener, a(), -23);
    }

    public String toString() {
        return this.tag + "{adUnitId='" + this.adUnitId + '\'' + ", adListener=" + this.adListener + ", isReady=" + isReady() + '}';
    }
}
