package com.applovin.impl.mediation.d;

import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.i;
import org.json.JSONObject;

public class b extends h {
    public static void e(JSONObject jSONObject, k kVar) {
        if (i.a(jSONObject, "signal_providers")) {
            kVar.a(c.g.x, jSONObject.toString());
        }
    }

    public static void f(JSONObject jSONObject, k kVar) {
        if (i.a(jSONObject, "auto_init_adapters")) {
            kVar.a(c.g.y, jSONObject.toString());
        }
    }

    public static String g(k kVar) {
        return h.a((String) kVar.a(c.d.V3), "1.0/mediate", kVar);
    }

    public static String h(k kVar) {
        return h.a((String) kVar.a(c.d.W3), "1.0/mediate", kVar);
    }

    public static String i(k kVar) {
        return h.a((String) kVar.a(c.d.V3), "1.0/mediate_debug", kVar);
    }

    public static String j(k kVar) {
        return h.a((String) kVar.a(c.d.W3), "1.0/mediate_debug", kVar);
    }
}
