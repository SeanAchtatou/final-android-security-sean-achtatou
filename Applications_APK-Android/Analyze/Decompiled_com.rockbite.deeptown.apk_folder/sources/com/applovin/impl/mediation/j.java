package com.applovin.impl.mediation;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.view.View;
import com.applovin.impl.mediation.b;
import com.applovin.impl.sdk.f;
import com.applovin.impl.sdk.q;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxAdViewAdListener;
import com.applovin.mediation.MaxErrorCodes;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.MaxRewardedAdListener;
import com.applovin.mediation.adapter.MaxAdViewAdapter;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.mediation.adapter.MaxAdapterError;
import com.applovin.mediation.adapter.MaxInterstitialAdapter;
import com.applovin.mediation.adapter.MaxRewardedAdapter;
import com.applovin.mediation.adapter.MaxSignalProvider;
import com.applovin.mediation.adapter.listeners.MaxAdViewAdapterListener;
import com.applovin.mediation.adapter.listeners.MaxInterstitialAdapterListener;
import com.applovin.mediation.adapter.listeners.MaxRewardedAdapterListener;
import com.applovin.mediation.adapter.listeners.MaxSignalCollectionListener;
import com.applovin.mediation.adapter.parameters.MaxAdapterInitializationParameters;
import com.applovin.mediation.adapter.parameters.MaxAdapterResponseParameters;
import com.applovin.mediation.adapter.parameters.MaxAdapterSignalCollectionParameters;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.concurrent.atomic.AtomicBoolean;

public class j {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final Handler f3726a = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final com.applovin.impl.sdk.k f3727b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public final q f3728c;

    /* renamed from: d  reason: collision with root package name */
    private final String f3729d;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public final b.f f3730e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public final String f3731f;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public MaxAdapter f3732g;
    /* access modifiers changed from: private */

    /* renamed from: h  reason: collision with root package name */
    public String f3733h;
    /* access modifiers changed from: private */

    /* renamed from: i  reason: collision with root package name */
    public b.C0087b f3734i;
    /* access modifiers changed from: private */

    /* renamed from: j  reason: collision with root package name */
    public View f3735j;
    /* access modifiers changed from: private */

    /* renamed from: k  reason: collision with root package name */
    public final l f3736k = new l(this, null);
    /* access modifiers changed from: private */
    public MaxAdapterResponseParameters l;
    private final AtomicBoolean m = new AtomicBoolean(true);
    /* access modifiers changed from: private */
    public final AtomicBoolean n = new AtomicBoolean(false);
    /* access modifiers changed from: private */
    public final AtomicBoolean o = new AtomicBoolean(false);

    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ MaxAdapterInitializationParameters f3737a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Activity f3738b;

        /* renamed from: com.applovin.impl.mediation.j$a$a  reason: collision with other inner class name */
        class C0091a implements MaxAdapter.OnCompletionListener {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ long f3740a;

            /* renamed from: com.applovin.impl.mediation.j$a$a$a  reason: collision with other inner class name */
            class C0092a implements Runnable {
                C0092a() {
                }

                public void run() {
                    long elapsedRealtime = SystemClock.elapsedRealtime();
                    C0091a aVar = C0091a.this;
                    j.this.f3727b.b0().a(j.this.f3730e, elapsedRealtime - aVar.f3740a, MaxAdapter.InitializationStatus.ADAPTER_NOT_SUPPORTED, null);
                }
            }

            /* renamed from: com.applovin.impl.mediation.j$a$a$b */
            class b implements Runnable {

                /* renamed from: a  reason: collision with root package name */
                final /* synthetic */ MaxAdapter.InitializationStatus f3743a;

                /* renamed from: b  reason: collision with root package name */
                final /* synthetic */ String f3744b;

                b(MaxAdapter.InitializationStatus initializationStatus, String str) {
                    this.f3743a = initializationStatus;
                    this.f3744b = str;
                }

                public void run() {
                    long elapsedRealtime = SystemClock.elapsedRealtime();
                    C0091a aVar = C0091a.this;
                    j.this.f3727b.b0().a(j.this.f3730e, elapsedRealtime - aVar.f3740a, this.f3743a, this.f3744b);
                }
            }

            C0091a(long j2) {
                this.f3740a = j2;
            }

            public void onCompletion() {
                AppLovinSdkUtils.runOnUiThreadDelayed(new C0092a(), j.this.f3730e.h());
            }

            public void onCompletion(MaxAdapter.InitializationStatus initializationStatus, String str) {
                AppLovinSdkUtils.runOnUiThreadDelayed(new b(initializationStatus, str), j.this.f3730e.h());
            }
        }

        a(MaxAdapterInitializationParameters maxAdapterInitializationParameters, Activity activity) {
            this.f3737a = maxAdapterInitializationParameters;
            this.f3738b = activity;
        }

        public void run() {
            j.this.f3732g.initialize(this.f3737a, this.f3738b, new C0091a(SystemClock.elapsedRealtime()));
        }
    }

    class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Runnable f3746a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ b.C0087b f3747b;

        b(Runnable runnable, b.C0087b bVar) {
            this.f3746a = runnable;
            this.f3747b = bVar;
        }

        public void run() {
            try {
                this.f3746a.run();
            } catch (Throwable th) {
                q e2 = j.this.f3728c;
                e2.b("MediationAdapterWrapper", "Failed to start displaying ad" + this.f3747b, th);
                j.this.f3736k.b("ad_render", MaxAdapterError.ERROR_CODE_UNSPECIFIED);
            }
        }
    }

    class c implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ MaxSignalProvider f3749a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ MaxAdapterSignalCollectionParameters f3750b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ Activity f3751c;

        /* renamed from: d  reason: collision with root package name */
        final /* synthetic */ m f3752d;

        /* renamed from: e  reason: collision with root package name */
        final /* synthetic */ b.h f3753e;

        class a implements MaxSignalCollectionListener {
            a() {
            }

            public void onSignalCollected(String str) {
                c cVar = c.this;
                j.this.a(str, cVar.f3752d);
            }

            public void onSignalCollectionFailed(String str) {
                c cVar = c.this;
                j.this.b(str, cVar.f3752d);
            }
        }

        c(MaxSignalProvider maxSignalProvider, MaxAdapterSignalCollectionParameters maxAdapterSignalCollectionParameters, Activity activity, m mVar, b.h hVar) {
            this.f3749a = maxSignalProvider;
            this.f3750b = maxAdapterSignalCollectionParameters;
            this.f3751c = activity;
            this.f3752d = mVar;
            this.f3753e = hVar;
        }

        public void run() {
            StringBuilder sb;
            this.f3749a.collectSignal(this.f3750b, this.f3751c, new a());
            if (this.f3752d.f3803c.get()) {
                return;
            }
            if (this.f3753e.d() == 0) {
                q e2 = j.this.f3728c;
                e2.b("MediationAdapterWrapper", "Failing signal collection " + this.f3753e + " since it has 0 timeout");
                j jVar = j.this;
                jVar.b("The adapter (" + j.this.f3731f + ") has 0 timeout", this.f3752d);
                return;
            }
            int i2 = (this.f3753e.d() > 0 ? 1 : (this.f3753e.d() == 0 ? 0 : -1));
            q e3 = j.this.f3728c;
            if (i2 > 0) {
                sb.append("Setting timeout ");
                sb.append(this.f3753e.d());
                sb.append("ms. for ");
                sb.append(this.f3753e);
                e3.b("MediationAdapterWrapper", sb.toString());
                j.this.f3727b.j().a(new o(j.this, this.f3752d, null), f.y.b.MEDIATION_TIMEOUT, this.f3753e.d());
                return;
            }
            sb = new StringBuilder();
            sb.append("Negative timeout set for ");
            sb.append(this.f3753e);
            sb.append(", not scheduling a timeout");
            e3.b("MediationAdapterWrapper", sb.toString());
        }
    }

    class d implements Runnable {
        d() {
        }

        public void run() {
            j.this.a("destroy");
            j.this.f3732g.onDestroy();
            MaxAdapter unused = j.this.f3732g = (MaxAdapter) null;
        }
    }

    class e implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ String f3757a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Runnable f3758b;

        e(String str, Runnable runnable) {
            this.f3757a = str;
            this.f3758b = runnable;
        }

        public void run() {
            try {
                q e2 = j.this.f3728c;
                e2.b("MediationAdapterWrapper", j.this.f3731f + ": running " + this.f3757a + "...");
                this.f3758b.run();
                q e3 = j.this.f3728c;
                e3.b("MediationAdapterWrapper", j.this.f3731f + ": finished " + this.f3757a + "");
            } catch (Throwable th) {
                q e4 = j.this.f3728c;
                e4.b("MediationAdapterWrapper", "Unable to run adapter operation " + this.f3757a + ", marking " + j.this.f3731f + " as disabled", th);
                j jVar = j.this;
                StringBuilder sb = new StringBuilder();
                sb.append("fail_");
                sb.append(this.f3757a);
                jVar.a(sb.toString());
            }
        }
    }

    class f implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ MaxAdapterResponseParameters f3760a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Activity f3761b;

        f(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity) {
            this.f3760a = maxAdapterResponseParameters;
            this.f3761b = activity;
        }

        public void run() {
            ((MaxInterstitialAdapter) j.this.f3732g).loadInterstitialAd(this.f3760a, this.f3761b, j.this.f3736k);
        }
    }

    class g implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ MaxAdapterResponseParameters f3763a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Activity f3764b;

        g(MaxAdapterResponseParameters maxAdapterResponseParameters, Activity activity) {
            this.f3763a = maxAdapterResponseParameters;
            this.f3764b = activity;
        }

        public void run() {
            ((MaxRewardedAdapter) j.this.f3732g).loadRewardedAd(this.f3763a, this.f3764b, j.this.f3736k);
        }
    }

    class h implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ MaxAdapterResponseParameters f3766a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ b.C0087b f3767b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ Activity f3768c;

        h(MaxAdapterResponseParameters maxAdapterResponseParameters, b.C0087b bVar, Activity activity) {
            this.f3766a = maxAdapterResponseParameters;
            this.f3767b = bVar;
            this.f3768c = activity;
        }

        public void run() {
            ((MaxAdViewAdapter) j.this.f3732g).loadAdViewAd(this.f3766a, this.f3767b.getFormat(), this.f3768c, j.this.f3736k);
        }
    }

    class i implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Runnable f3770a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ b.C0087b f3771b;

        i(Runnable runnable, b.C0087b bVar) {
            this.f3770a = runnable;
            this.f3771b = bVar;
        }

        public void run() {
            try {
                this.f3770a.run();
            } catch (Throwable th) {
                q e2 = j.this.f3728c;
                e2.b("MediationAdapterWrapper", "Failed start loading " + this.f3771b, th);
                j.this.f3736k.a("loadAd", -1);
            }
            if (!j.this.n.get()) {
                long d2 = j.this.f3730e.d();
                if (d2 == 0) {
                    q e3 = j.this.f3728c;
                    e3.b("MediationAdapterWrapper", "Failing ad " + this.f3771b + " since it has 0 timeout");
                    j.this.f3736k.a("loadAd", (int) MaxErrorCodes.MEDIATION_ADAPTER_IMMEDIATE_TIMEOUT);
                } else if (d2 > 0) {
                    q e4 = j.this.f3728c;
                    e4.b("MediationAdapterWrapper", "Setting timeout " + d2 + "ms. for " + this.f3771b);
                    j.this.f3727b.j().a(new n(j.this, null), f.y.b.MEDIATION_TIMEOUT, d2);
                } else {
                    q e5 = j.this.f3728c;
                    e5.b("MediationAdapterWrapper", "Negative timeout set for " + this.f3771b + ", not scheduling a timeout");
                }
            }
        }
    }

    /* renamed from: com.applovin.impl.mediation.j$j  reason: collision with other inner class name */
    class C0093j implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Activity f3773a;

        C0093j(Activity activity) {
            this.f3773a = activity;
        }

        public void run() {
            ((MaxInterstitialAdapter) j.this.f3732g).showInterstitialAd(j.this.l, this.f3773a, j.this.f3736k);
        }
    }

    class k implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Activity f3775a;

        k(Activity activity) {
            this.f3775a = activity;
        }

        public void run() {
            ((MaxRewardedAdapter) j.this.f3732g).showRewardedAd(j.this.l, this.f3775a, j.this.f3736k);
        }
    }

    private class l implements MaxAdViewAdapterListener, MaxInterstitialAdapterListener, MaxRewardedAdapterListener {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public e f3777a;

        class a implements Runnable {
            a() {
            }

            public void run() {
                l.this.f3777a.onAdDisplayed(j.this.f3734i);
            }
        }

        class b implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ MaxAdapterError f3780a;

            b(MaxAdapterError maxAdapterError) {
                this.f3780a = maxAdapterError;
            }

            public void run() {
                l.this.f3777a.a(j.this.f3734i, this.f3780a);
            }
        }

        class c implements Runnable {
            c() {
            }

            public void run() {
                l.this.f3777a.onAdClicked(j.this.f3734i);
            }
        }

        class d implements Runnable {
            d() {
            }

            public void run() {
                l.this.f3777a.onAdHidden(j.this.f3734i);
            }
        }

        class e implements Runnable {
            e() {
            }

            public void run() {
                l.this.f3777a.onAdClicked(j.this.f3734i);
            }
        }

        class f implements Runnable {
            f() {
            }

            public void run() {
                l.this.f3777a.onAdHidden(j.this.f3734i);
            }
        }

        class g implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ MaxReward f3786a;

            g(MaxReward maxReward) {
                this.f3786a = maxReward;
            }

            public void run() {
                if (l.this.f3777a instanceof MaxRewardedAdListener) {
                    ((MaxRewardedAdListener) l.this.f3777a).onUserRewarded(j.this.f3734i, this.f3786a);
                }
            }
        }

        class h implements Runnable {
            h() {
            }

            public void run() {
                if (j.this.n.compareAndSet(false, true)) {
                    l.this.f3777a.onAdLoaded(j.this.f3734i);
                }
            }
        }

        class i implements Runnable {
            i() {
            }

            public void run() {
                if (l.this.f3777a instanceof MaxRewardedAdListener) {
                    ((MaxRewardedAdListener) l.this.f3777a).onRewardedVideoStarted(j.this.f3734i);
                }
            }
        }

        /* renamed from: com.applovin.impl.mediation.j$l$j  reason: collision with other inner class name */
        class C0094j implements Runnable {
            C0094j() {
            }

            public void run() {
                if (l.this.f3777a instanceof MaxRewardedAdListener) {
                    ((MaxRewardedAdListener) l.this.f3777a).onRewardedVideoCompleted(j.this.f3734i);
                }
            }
        }

        class k implements Runnable {
            k() {
            }

            public void run() {
                l.this.f3777a.onAdClicked(j.this.f3734i);
            }
        }

        /* renamed from: com.applovin.impl.mediation.j$l$l  reason: collision with other inner class name */
        class C0095l implements Runnable {
            C0095l() {
            }

            public void run() {
                l.this.f3777a.onAdHidden(j.this.f3734i);
            }
        }

        class m implements Runnable {
            m() {
            }

            public void run() {
                if (l.this.f3777a instanceof MaxAdViewAdListener) {
                    ((MaxAdViewAdListener) l.this.f3777a).onAdExpanded(j.this.f3734i);
                }
            }
        }

        class n implements Runnable {
            n() {
            }

            public void run() {
                if (l.this.f3777a instanceof MaxAdViewAdListener) {
                    ((MaxAdViewAdListener) l.this.f3777a).onAdCollapsed(j.this.f3734i);
                }
            }
        }

        class o implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ Runnable f3795a;

            /* renamed from: b  reason: collision with root package name */
            final /* synthetic */ MaxAdListener f3796b;

            /* renamed from: c  reason: collision with root package name */
            final /* synthetic */ String f3797c;

            o(Runnable runnable, MaxAdListener maxAdListener, String str) {
                this.f3795a = runnable;
                this.f3796b = maxAdListener;
                this.f3797c = str;
            }

            public void run() {
                try {
                    this.f3795a.run();
                } catch (Exception e2) {
                    MaxAdListener maxAdListener = this.f3796b;
                    String name = maxAdListener != null ? maxAdListener.getClass().getName() : null;
                    q e3 = j.this.f3728c;
                    e3.b("MediationAdapterWrapper", "Failed to forward call (" + this.f3797c + ") to " + name, e2);
                }
            }
        }

        class p implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ MaxAdapterError f3799a;

            p(MaxAdapterError maxAdapterError) {
                this.f3799a = maxAdapterError;
            }

            public void run() {
                if (j.this.n.compareAndSet(false, true)) {
                    l.this.f3777a.a(j.this.f3733h, this.f3799a);
                }
            }
        }

        private l() {
        }

        /* synthetic */ l(j jVar, a aVar) {
            this();
        }

        /* access modifiers changed from: private */
        public void a(e eVar) {
            if (eVar != null) {
                this.f3777a = eVar;
                return;
            }
            throw new IllegalArgumentException("No listener specified");
        }

        private void a(String str) {
            j.this.o.set(true);
            a(str, this.f3777a, new h());
        }

        /* access modifiers changed from: private */
        public void a(String str, int i2) {
            a(str, new MaxAdapterError(i2));
        }

        private void a(String str, MaxAdListener maxAdListener, Runnable runnable) {
            j.this.f3726a.post(new o(runnable, maxAdListener, str));
        }

        private void a(String str, MaxAdapterError maxAdapterError) {
            a(str, this.f3777a, new p(maxAdapterError));
        }

        private void b(String str) {
            if (j.this.f3734i.v().compareAndSet(false, true)) {
                a(str, this.f3777a, new a());
            }
        }

        /* access modifiers changed from: private */
        public void b(String str, int i2) {
            b(str, new MaxAdapterError(i2));
        }

        private void b(String str, MaxAdapterError maxAdapterError) {
            a(str, this.f3777a, new b(maxAdapterError));
        }

        public void onAdViewAdClicked() {
            q e2 = j.this.f3728c;
            e2.c("MediationAdapterWrapper", j.this.f3731f + ": adview ad clicked");
            a("onAdViewAdClicked", this.f3777a, new k());
        }

        public void onAdViewAdCollapsed() {
            q e2 = j.this.f3728c;
            e2.c("MediationAdapterWrapper", j.this.f3731f + ": adview ad collapsed");
            a("onAdViewAdCollapsed", this.f3777a, new n());
        }

        public void onAdViewAdDisplayFailed(MaxAdapterError maxAdapterError) {
            q e2 = j.this.f3728c;
            e2.d("MediationAdapterWrapper", j.this.f3731f + ": adview ad failed to display with code: " + maxAdapterError);
            b("onAdViewAdDisplayFailed", maxAdapterError);
        }

        public void onAdViewAdDisplayed() {
            q e2 = j.this.f3728c;
            e2.c("MediationAdapterWrapper", j.this.f3731f + ": adview ad displayed");
            b("onAdViewAdDisplayed");
        }

        public void onAdViewAdExpanded() {
            q e2 = j.this.f3728c;
            e2.c("MediationAdapterWrapper", j.this.f3731f + ": adview ad expanded");
            a("onAdViewAdExpanded", this.f3777a, new m());
        }

        public void onAdViewAdHidden() {
            q e2 = j.this.f3728c;
            e2.c("MediationAdapterWrapper", j.this.f3731f + ": adview ad hidden");
            a("onAdViewAdHidden", this.f3777a, new C0095l());
        }

        public void onAdViewAdLoadFailed(MaxAdapterError maxAdapterError) {
            q e2 = j.this.f3728c;
            e2.d("MediationAdapterWrapper", j.this.f3731f + ": adview ad ad failed to load with code: " + maxAdapterError);
            a("onAdViewAdLoadFailed", maxAdapterError);
        }

        public void onAdViewAdLoaded(View view) {
            q e2 = j.this.f3728c;
            e2.c("MediationAdapterWrapper", j.this.f3731f + ": adview ad loaded");
            View unused = j.this.f3735j = view;
            a("onAdViewAdLoaded");
        }

        public void onInterstitialAdClicked() {
            q e2 = j.this.f3728c;
            e2.c("MediationAdapterWrapper", j.this.f3731f + ": interstitial ad clicked");
            a("onInterstitialAdClicked", this.f3777a, new c());
        }

        public void onInterstitialAdDisplayFailed(MaxAdapterError maxAdapterError) {
            q e2 = j.this.f3728c;
            e2.d("MediationAdapterWrapper", j.this.f3731f + ": interstitial ad failed to display with code " + maxAdapterError);
            b("onInterstitialAdDisplayFailed", maxAdapterError);
        }

        public void onInterstitialAdDisplayed() {
            q e2 = j.this.f3728c;
            e2.c("MediationAdapterWrapper", j.this.f3731f + ": interstitial ad displayed");
            b("onInterstitialAdDisplayed");
        }

        public void onInterstitialAdHidden() {
            q e2 = j.this.f3728c;
            e2.c("MediationAdapterWrapper", j.this.f3731f + ": interstitial ad hidden");
            a("onInterstitialAdHidden", this.f3777a, new d());
        }

        public void onInterstitialAdLoadFailed(MaxAdapterError maxAdapterError) {
            q e2 = j.this.f3728c;
            e2.d("MediationAdapterWrapper", j.this.f3731f + ": interstitial ad failed to load with error " + maxAdapterError);
            a("onInterstitialAdLoadFailed", maxAdapterError);
        }

        public void onInterstitialAdLoaded() {
            q e2 = j.this.f3728c;
            e2.c("MediationAdapterWrapper", j.this.f3731f + ": interstitial ad loaded");
            a("onInterstitialAdLoaded");
        }

        public void onRewardedAdClicked() {
            q e2 = j.this.f3728c;
            e2.c("MediationAdapterWrapper", j.this.f3731f + ": rewarded ad clicked");
            a("onRewardedAdClicked", this.f3777a, new e());
        }

        public void onRewardedAdDisplayFailed(MaxAdapterError maxAdapterError) {
            q e2 = j.this.f3728c;
            e2.d("MediationAdapterWrapper", j.this.f3731f + ": rewarded ad display failed with error: " + maxAdapterError);
            b("onRewardedAdDisplayFailed", maxAdapterError);
        }

        public void onRewardedAdDisplayed() {
            q e2 = j.this.f3728c;
            e2.c("MediationAdapterWrapper", j.this.f3731f + ": rewarded ad displayed");
            b("onRewardedAdDisplayed");
        }

        public void onRewardedAdHidden() {
            q e2 = j.this.f3728c;
            e2.c("MediationAdapterWrapper", j.this.f3731f + ": rewarded ad hidden");
            a("onRewardedAdHidden", this.f3777a, new f());
        }

        public void onRewardedAdLoadFailed(MaxAdapterError maxAdapterError) {
            q e2 = j.this.f3728c;
            e2.d("MediationAdapterWrapper", j.this.f3731f + ": rewarded ad failed to load with code: " + maxAdapterError);
            a("onRewardedAdLoadFailed", maxAdapterError);
        }

        public void onRewardedAdLoaded() {
            q e2 = j.this.f3728c;
            e2.c("MediationAdapterWrapper", j.this.f3731f + ": rewarded ad loaded");
            a("onRewardedAdLoaded");
        }

        public void onRewardedAdVideoCompleted() {
            q e2 = j.this.f3728c;
            e2.c("MediationAdapterWrapper", j.this.f3731f + ": rewarded video completed");
            a("onRewardedAdVideoCompleted", this.f3777a, new C0094j());
        }

        public void onRewardedAdVideoStarted() {
            q e2 = j.this.f3728c;
            e2.c("MediationAdapterWrapper", j.this.f3731f + ": rewarded video started");
            a("onRewardedAdVideoStarted", this.f3777a, new i());
        }

        public void onUserRewarded(MaxReward maxReward) {
            q e2 = j.this.f3728c;
            e2.c("MediationAdapterWrapper", j.this.f3731f + ": user was rewarded: " + maxReward);
            a("onUserRewarded", this.f3777a, new g(maxReward));
        }
    }

    private static class m {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final b.h f3801a;
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public final MaxSignalCollectionListener f3802b;
        /* access modifiers changed from: private */

        /* renamed from: c  reason: collision with root package name */
        public final AtomicBoolean f3803c = new AtomicBoolean();

        m(b.h hVar, MaxSignalCollectionListener maxSignalCollectionListener) {
            this.f3801a = hVar;
            this.f3802b = maxSignalCollectionListener;
        }
    }

    private class n extends f.c {
        private n() {
            super("TaskTimeoutMediatedAd", j.this.f3727b);
        }

        /* synthetic */ n(j jVar, a aVar) {
            this();
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.I;
        }

        public void run() {
            if (!j.this.o.get()) {
                d(j.this.f3731f + " is timing out " + j.this.f3734i + "...");
                this.f4130a.a().a(j.this.f3734i);
                j.this.f3736k.a(c(), (int) MaxErrorCodes.MEDIATION_ADAPTER_TIMEOUT);
            }
        }
    }

    private class o extends f.c {

        /* renamed from: f  reason: collision with root package name */
        private final m f3805f;

        private o(m mVar) {
            super("TaskTimeoutSignalCollection", j.this.f3727b);
            this.f3805f = mVar;
        }

        /* synthetic */ o(j jVar, m mVar, a aVar) {
            this(mVar);
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.J;
        }

        public void run() {
            if (!this.f3805f.f3803c.get()) {
                d(j.this.f3731f + " is timing out " + this.f3805f.f3801a + "...");
                j jVar = j.this;
                jVar.b("The adapter (" + j.this.f3731f + ") timed out", this.f3805f);
            }
        }
    }

    j(b.f fVar, MaxAdapter maxAdapter, com.applovin.impl.sdk.k kVar) {
        if (fVar == null) {
            throw new IllegalArgumentException("No adapter name specified");
        } else if (maxAdapter == null) {
            throw new IllegalArgumentException("No adapter specified");
        } else if (kVar != null) {
            this.f3729d = fVar.n();
            this.f3732g = maxAdapter;
            this.f3727b = kVar;
            this.f3728c = kVar.Z();
            this.f3730e = fVar;
            this.f3731f = maxAdapter.getClass().getSimpleName();
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        q qVar = this.f3728c;
        qVar.c("MediationAdapterWrapper", "Marking " + this.f3731f + " as disabled due to: " + str);
        this.m.set(false);
    }

    /* access modifiers changed from: private */
    public void a(String str, m mVar) {
        if (mVar.f3803c.compareAndSet(false, true) && mVar.f3802b != null) {
            mVar.f3802b.onSignalCollected(str);
        }
    }

    private void a(String str, Runnable runnable) {
        e eVar = new e(str, runnable);
        if (this.f3730e.b()) {
            this.f3726a.post(eVar);
        } else {
            eVar.run();
        }
    }

    /* access modifiers changed from: private */
    public void b(String str, m mVar) {
        if (mVar.f3803c.compareAndSet(false, true) && mVar.f3802b != null) {
            mVar.f3802b.onSignalCollectionFailed(str);
        }
    }

    public View a() {
        return this.f3735j;
    }

    /* access modifiers changed from: package-private */
    public void a(b.C0087b bVar, Activity activity) {
        Runnable runnable;
        if (bVar == null) {
            throw new IllegalArgumentException("No mediated ad specified");
        } else if (bVar.q() == null) {
            this.f3736k.b("ad_show", -5201);
        } else if (bVar.q() != this) {
            throw new IllegalArgumentException("Mediated ad belongs to a different adapter");
        } else if (activity == null) {
            throw new IllegalArgumentException("No activity specified");
        } else if (!this.m.get()) {
            q.i("MediationAdapterWrapper", "Mediation adapter '" + this.f3731f + "' is disabled. Showing ads with this adapter is disabled.");
            this.f3736k.b("ad_show", MaxErrorCodes.MEDIATION_ADAPTER_DISABLED);
        } else if (!d()) {
            q.i("MediationAdapterWrapper", "Mediation adapter '" + this.f3731f + "' does not have an ad loaded. Please load an ad first");
            this.f3736k.b("ad_show", MaxErrorCodes.MEDIATION_ADAPTER_AD_NOT_READY);
        } else {
            if (bVar.getFormat() == MaxAdFormat.INTERSTITIAL) {
                if (this.f3732g instanceof MaxInterstitialAdapter) {
                    runnable = new C0093j(activity);
                } else {
                    q.i("MediationAdapterWrapper", "Mediation adapter '" + this.f3731f + "' is not an interstitial adapter.");
                    this.f3736k.b("showFullscreenAd", MaxErrorCodes.MEDIATION_ADAPTER_WRONG_TYPE);
                    return;
                }
            } else if (bVar.getFormat() != MaxAdFormat.REWARDED) {
                q.i("MediationAdapterWrapper", "Failed to show " + bVar + ": " + bVar.getFormat() + " is not a supported ad format");
                this.f3736k.b("showFullscreenAd", MaxErrorCodes.MEDIATION_ADAPTER_WRONG_TYPE);
                return;
            } else if (this.f3732g instanceof MaxRewardedAdapter) {
                runnable = new k(activity);
            } else {
                q.i("MediationAdapterWrapper", "Mediation adapter '" + this.f3731f + "' is not an incentivized adapter.");
                this.f3736k.b("showFullscreenAd", MaxErrorCodes.MEDIATION_ADAPTER_WRONG_TYPE);
                return;
            }
            a("ad_render", new b(runnable, bVar));
        }
    }

    /* access modifiers changed from: package-private */
    public void a(MaxAdapterInitializationParameters maxAdapterInitializationParameters, Activity activity) {
        a("initialize", new a(maxAdapterInitializationParameters, activity));
    }

    /* access modifiers changed from: package-private */
    public void a(MaxAdapterSignalCollectionParameters maxAdapterSignalCollectionParameters, b.h hVar, Activity activity, MaxSignalCollectionListener maxSignalCollectionListener) {
        if (maxSignalCollectionListener == null) {
            throw new IllegalArgumentException("No callback specified");
        } else if (!this.m.get()) {
            q.i("MediationAdapterWrapper", "Mediation adapter '" + this.f3731f + "' is disabled. Signal collection ads with this adapter is disabled.");
            maxSignalCollectionListener.onSignalCollectionFailed("The adapter (" + this.f3731f + ") is disabled");
        } else {
            m mVar = new m(hVar, maxSignalCollectionListener);
            MaxAdapter maxAdapter = this.f3732g;
            if (maxAdapter instanceof MaxSignalProvider) {
                a("collect_signal", new c((MaxSignalProvider) maxAdapter, maxAdapterSignalCollectionParameters, activity, mVar, hVar));
                return;
            }
            b("The adapter (" + this.f3731f + ") does not support signal collection", mVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str, b.C0087b bVar) {
        this.f3733h = str;
        this.f3734i = bVar;
    }

    /* access modifiers changed from: package-private */
    public void a(String str, MaxAdapterResponseParameters maxAdapterResponseParameters, b.C0087b bVar, Activity activity, e eVar) {
        Runnable runnable;
        if (bVar == null) {
            throw new IllegalArgumentException("No mediated ad specified");
        } else if (!this.m.get()) {
            q.i("MediationAdapterWrapper", "Mediation adapter '" + this.f3731f + "' was disabled due to earlier failures. Loading ads with this adapter is disabled.");
            eVar.onAdLoadFailed(str, MaxErrorCodes.MEDIATION_ADAPTER_DISABLED);
        } else {
            this.l = maxAdapterResponseParameters;
            this.f3736k.a(eVar);
            if (bVar.getFormat() == MaxAdFormat.INTERSTITIAL) {
                if (this.f3732g instanceof MaxInterstitialAdapter) {
                    runnable = new f(maxAdapterResponseParameters, activity);
                } else {
                    q.i("MediationAdapterWrapper", "Mediation adapter '" + this.f3731f + "' is not an interstitial adapter.");
                    this.f3736k.a("loadAd", (int) MaxErrorCodes.MEDIATION_ADAPTER_WRONG_TYPE);
                    return;
                }
            } else if (bVar.getFormat() == MaxAdFormat.REWARDED) {
                if (this.f3732g instanceof MaxRewardedAdapter) {
                    runnable = new g(maxAdapterResponseParameters, activity);
                } else {
                    q.i("MediationAdapterWrapper", "Mediation adapter '" + this.f3731f + "' is not an incentivized adapter.");
                    this.f3736k.a("loadAd", (int) MaxErrorCodes.MEDIATION_ADAPTER_WRONG_TYPE);
                    return;
                }
            } else if (bVar.getFormat() != MaxAdFormat.BANNER && bVar.getFormat() != MaxAdFormat.LEADER && bVar.getFormat() != MaxAdFormat.MREC) {
                q.i("MediationAdapterWrapper", "Failed to load " + bVar + ": " + bVar.getFormat() + " is not a supported ad format");
                this.f3736k.a("loadAd", (int) MaxErrorCodes.FORMAT_TYPE_NOT_SUPPORTED);
                return;
            } else if (this.f3732g instanceof MaxAdViewAdapter) {
                runnable = new h(maxAdapterResponseParameters, bVar, activity);
            } else {
                q.i("MediationAdapterWrapper", "Mediation adapter '" + this.f3731f + "' is not an adview-based adapter.");
                this.f3736k.a("loadAd", (int) MaxErrorCodes.MEDIATION_ADAPTER_WRONG_TYPE);
                return;
            }
            a("ad_load", new i(runnable, bVar));
        }
    }

    public String b() {
        return this.f3729d;
    }

    public boolean c() {
        return this.m.get();
    }

    public boolean d() {
        return this.n.get() && this.o.get();
    }

    public String e() {
        MaxAdapter maxAdapter = this.f3732g;
        if (maxAdapter == null) {
            return null;
        }
        try {
            return maxAdapter.getSdkVersion();
        } catch (Throwable th) {
            q qVar = this.f3728c;
            qVar.b("MediationAdapterWrapper", "Unable to get adapter's SDK version, marking " + this + " as disabled", th);
            a("fail_version");
            return null;
        }
    }

    public String f() {
        MaxAdapter maxAdapter = this.f3732g;
        if (maxAdapter == null) {
            return null;
        }
        try {
            return maxAdapter.getAdapterVersion();
        } catch (Throwable th) {
            q qVar = this.f3728c;
            qVar.b("MediationAdapterWrapper", "Unable to get adapter version, marking " + this + " as disabled", th);
            a("fail_version");
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public void g() {
        a("destroy", new d());
    }

    public String toString() {
        return "MediationAdapterWrapper{adapterTag='" + this.f3731f + "'" + '}';
    }
}
