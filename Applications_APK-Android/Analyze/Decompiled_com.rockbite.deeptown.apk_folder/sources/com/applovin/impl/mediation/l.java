package com.applovin.impl.mediation;

import com.applovin.impl.mediation.b;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class l {

    /* renamed from: a  reason: collision with root package name */
    private final List<a> f3823a = Collections.synchronizedList(new ArrayList());

    public interface a {
        void a(b.C0087b bVar);
    }

    public void a(b.C0087b bVar) {
        Iterator it = new ArrayList(this.f3823a).iterator();
        while (it.hasNext()) {
            ((a) it.next()).a(bVar);
        }
    }

    public void a(a aVar) {
        this.f3823a.add(aVar);
    }

    public void b(a aVar) {
        this.f3823a.remove(aVar);
    }
}
