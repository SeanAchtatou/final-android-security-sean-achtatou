package com.applovin.impl.mediation;

import android.app.Activity;
import com.applovin.impl.mediation.b;
import com.applovin.impl.mediation.c;
import com.applovin.impl.sdk.f;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.q;
import com.applovin.impl.sdk.utils.i;
import com.applovin.mediation.adapter.MaxAdapter;
import com.facebook.internal.AnalyticsEvents;
import java.util.LinkedHashSet;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;
import org.json.JSONObject;

public class h {

    /* renamed from: a  reason: collision with root package name */
    private final k f3715a;

    /* renamed from: b  reason: collision with root package name */
    private final q f3716b;

    /* renamed from: c  reason: collision with root package name */
    private final AtomicBoolean f3717c = new AtomicBoolean();

    /* renamed from: d  reason: collision with root package name */
    private final JSONArray f3718d = new JSONArray();

    /* renamed from: e  reason: collision with root package name */
    private final LinkedHashSet<String> f3719e = new LinkedHashSet<>();

    /* renamed from: f  reason: collision with root package name */
    private final Object f3720f = new Object();

    public h(k kVar) {
        this.f3715a = kVar;
        this.f3716b = kVar.Z();
    }

    public void a(Activity activity) {
        if (this.f3717c.compareAndSet(false, true)) {
            this.f3715a.j().a(new c.C0088c(activity, this.f3715a), f.y.b.MEDIATION_MAIN);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(b.f fVar, long j2, MaxAdapter.InitializationStatus initializationStatus, String str) {
        boolean z;
        if (initializationStatus != null && initializationStatus != MaxAdapter.InitializationStatus.INITIALIZING) {
            synchronized (this.f3720f) {
                z = !a(fVar);
                if (z) {
                    this.f3719e.add(fVar.m());
                    JSONObject jSONObject = new JSONObject();
                    i.a(jSONObject, "class", fVar.m(), this.f3715a);
                    i.a(jSONObject, "init_status", String.valueOf(initializationStatus.getCode()), this.f3715a);
                    i.a(jSONObject, AnalyticsEvents.PARAMETER_SHARE_ERROR_MESSAGE, JSONObject.quote(str), this.f3715a);
                    this.f3718d.put(jSONObject);
                }
            }
            if (z) {
                this.f3715a.a(fVar);
                this.f3715a.c0().maybeScheduleAdapterInitializationPostback(fVar, j2, initializationStatus, str);
            }
        }
    }

    public void a(b.f fVar, Activity activity) {
        j a2 = this.f3715a.a0().a(fVar);
        if (a2 != null) {
            q qVar = this.f3716b;
            qVar.c("MediationAdapterInitializationManager", "Initializing adapter " + fVar);
            a2.a(MaxAdapterParametersImpl.a(fVar, activity.getApplicationContext()), activity);
        }
    }

    public boolean a() {
        return this.f3717c.get();
    }

    /* access modifiers changed from: package-private */
    public boolean a(b.f fVar) {
        boolean contains;
        synchronized (this.f3720f) {
            contains = this.f3719e.contains(fVar.m());
        }
        return contains;
    }

    public LinkedHashSet<String> b() {
        LinkedHashSet<String> linkedHashSet;
        synchronized (this.f3720f) {
            linkedHashSet = this.f3719e;
        }
        return linkedHashSet;
    }

    public JSONArray c() {
        JSONArray jSONArray;
        synchronized (this.f3720f) {
            jSONArray = this.f3718d;
        }
        return jSONArray;
    }
}
