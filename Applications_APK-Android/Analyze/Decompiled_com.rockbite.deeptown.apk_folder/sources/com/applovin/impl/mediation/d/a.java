package com.applovin.impl.mediation.d;

import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.utils.j;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxAdViewAdListener;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.MaxRewardedAdListener;

public class a implements MaxAdListener, MaxAdViewAdListener, MaxRewardedAdListener {

    /* renamed from: a  reason: collision with root package name */
    private final MaxAdListener f3708a;

    public a(MaxAdListener maxAdListener, k kVar) {
        this.f3708a = maxAdListener;
    }

    public void onAdClicked(MaxAd maxAd) {
        j.d(this.f3708a, maxAd);
    }

    public void onAdCollapsed(MaxAd maxAd) {
        j.h(this.f3708a, maxAd);
    }

    public void onAdDisplayFailed(MaxAd maxAd, int i2) {
        j.a(this.f3708a, maxAd, i2);
    }

    public void onAdDisplayed(MaxAd maxAd) {
        j.b(this.f3708a, maxAd);
    }

    public void onAdExpanded(MaxAd maxAd) {
        j.g(this.f3708a, maxAd);
    }

    public void onAdHidden(MaxAd maxAd) {
        j.c(this.f3708a, maxAd);
    }

    public void onRewardedVideoCompleted(MaxAd maxAd) {
        j.f(this.f3708a, maxAd);
    }

    public void onRewardedVideoStarted(MaxAd maxAd) {
        j.e(this.f3708a, maxAd);
    }

    public void onUserRewarded(MaxAd maxAd, MaxReward maxReward) {
        j.a(this.f3708a, maxAd, maxReward);
    }
}
