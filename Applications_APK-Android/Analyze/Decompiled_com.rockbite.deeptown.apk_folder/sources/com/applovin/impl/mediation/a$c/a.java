package com.applovin.impl.mediation.a$c;

import android.os.Build;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.d.i;
import com.applovin.impl.sdk.f;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.network.a;
import com.applovin.impl.sdk.network.b;
import com.applovin.impl.sdk.utils.m;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinWebViewActivity;
import com.google.ads.mediation.inmobi.InMobiNetworkValues;
import com.google.firebase.perf.FirebasePerformance;
import com.tapjoy.TapjoyConstants;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class a extends f.c {
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public final a.c<JSONObject> f3494f;

    /* renamed from: com.applovin.impl.mediation.a$c.a$a  reason: collision with other inner class name */
    class C0074a extends f.e0<JSONObject> {
        C0074a(b bVar, k kVar, boolean z) {
            super(bVar, kVar, z);
        }

        public void a(int i2) {
            a.this.f3494f.a(i2);
        }

        public void a(JSONObject jSONObject, int i2) {
            a.this.f3494f.a(jSONObject, i2);
        }
    }

    public a(a.c<JSONObject> cVar, k kVar) {
        super("TaskFetchMediationDebuggerInfo", kVar, true);
        this.f3494f = cVar;
    }

    public i a() {
        return i.L;
    }

    /* access modifiers changed from: protected */
    public Map<String, String> f() {
        HashMap hashMap = new HashMap();
        hashMap.put("sdk_version", AppLovinSdk.VERSION);
        hashMap.put("build", String.valueOf(131));
        if (!((Boolean) this.f4130a.a(c.e.G3)).booleanValue()) {
            hashMap.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.f4130a.X());
        }
        l.d c2 = this.f4130a.n().c();
        hashMap.put(InMobiNetworkValues.PACKAGE_NAME, m.d(c2.f4282c));
        hashMap.put(TapjoyConstants.TJC_APP_VERSION_NAME, m.d(c2.f4281b));
        hashMap.put(TapjoyConstants.TJC_PLATFORM, "android");
        hashMap.put("os", m.d(Build.VERSION.RELEASE));
        return hashMap;
    }

    public void run() {
        C0074a aVar = new C0074a(b.a(this.f4130a).a(com.applovin.impl.mediation.d.b.i(this.f4130a)).c(com.applovin.impl.mediation.d.b.j(this.f4130a)).a(f()).b(FirebasePerformance.HttpMethod.GET).a((Object) new JSONObject()).b(((Long) this.f4130a.a(c.d.Z3)).intValue()).a(), this.f4130a, e());
        aVar.a(c.d.V3);
        aVar.b(c.d.W3);
        this.f4130a.j().a(aVar);
    }
}
