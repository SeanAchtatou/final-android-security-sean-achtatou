package com.applovin.impl.mediation;

import android.content.Context;
import android.os.Bundle;
import com.applovin.impl.mediation.b;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.adapter.parameters.MaxAdapterInitializationParameters;
import com.applovin.mediation.adapter.parameters.MaxAdapterResponseParameters;
import com.applovin.mediation.adapter.parameters.MaxAdapterSignalCollectionParameters;

public class MaxAdapterParametersImpl implements MaxAdapterInitializationParameters, MaxAdapterResponseParameters, MaxAdapterSignalCollectionParameters {

    /* renamed from: a  reason: collision with root package name */
    private Bundle f3458a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f3459b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f3460c;

    /* renamed from: d  reason: collision with root package name */
    private boolean f3461d;

    /* renamed from: e  reason: collision with root package name */
    private String f3462e;

    /* renamed from: f  reason: collision with root package name */
    private String f3463f;

    /* renamed from: g  reason: collision with root package name */
    private MaxAdFormat f3464g;

    private MaxAdapterParametersImpl() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.mediation.MaxAdapterParametersImpl.a(com.applovin.impl.mediation.b$f, android.content.Context):com.applovin.impl.mediation.MaxAdapterParametersImpl
     arg types: [com.applovin.impl.mediation.b$b, android.content.Context]
     candidates:
      com.applovin.impl.mediation.MaxAdapterParametersImpl.a(com.applovin.impl.mediation.b$b, android.content.Context):com.applovin.impl.mediation.MaxAdapterParametersImpl
      com.applovin.impl.mediation.MaxAdapterParametersImpl.a(com.applovin.impl.mediation.b$f, android.content.Context):com.applovin.impl.mediation.MaxAdapterParametersImpl */
    static MaxAdapterParametersImpl a(b.C0087b bVar, Context context) {
        MaxAdapterParametersImpl a2 = a((b.f) bVar, context);
        a2.f3462e = bVar.s();
        a2.f3463f = bVar.r();
        return a2;
    }

    static MaxAdapterParametersImpl a(b.f fVar, Context context) {
        MaxAdapterParametersImpl maxAdapterParametersImpl = new MaxAdapterParametersImpl();
        maxAdapterParametersImpl.f3459b = fVar.b(context);
        maxAdapterParametersImpl.f3460c = fVar.a(context);
        maxAdapterParametersImpl.f3458a = fVar.c();
        maxAdapterParametersImpl.f3461d = fVar.a();
        return maxAdapterParametersImpl;
    }

    static MaxAdapterParametersImpl a(b.h hVar, MaxAdFormat maxAdFormat, Context context) {
        MaxAdapterParametersImpl a2 = a(hVar, context);
        a2.f3464g = maxAdFormat;
        return a2;
    }

    public MaxAdFormat getAdFormat() {
        return this.f3464g;
    }

    public String getBidResponse() {
        return this.f3463f;
    }

    public Bundle getServerParameters() {
        return this.f3458a;
    }

    public String getThirdPartyAdPlacementId() {
        return this.f3462e;
    }

    public boolean hasUserConsent() {
        return this.f3460c;
    }

    public boolean isAgeRestrictedUser() {
        return this.f3459b;
    }

    public boolean isTesting() {
        return this.f3461d;
    }
}
