package com.applovin.impl.mediation;

public class f {
    public static final f EMPTY = new f(0);
    private final int errorCode;
    private final String errorMessage;

    public f(int i2) {
        this(i2, "");
    }

    public f(int i2, String str) {
        this.errorCode = i2;
        this.errorMessage = str;
    }

    public f(String str) {
        this(-1, str);
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public String toString() {
        return "MaxError{errorCode=" + getErrorCode() + ", errorMessage='" + getErrorMessage() + '\'' + '}';
    }
}
