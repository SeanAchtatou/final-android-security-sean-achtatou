package com.applovin.impl.mediation;

import android.text.TextUtils;
import com.applovin.impl.mediation.b;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.q;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.mediation.adapters.MediationAdapterBase;
import com.applovin.sdk.AppLovinSdk;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class i {

    /* renamed from: a  reason: collision with root package name */
    private final k f3721a;

    /* renamed from: b  reason: collision with root package name */
    private final q f3722b;

    /* renamed from: c  reason: collision with root package name */
    private final Object f3723c = new Object();

    /* renamed from: d  reason: collision with root package name */
    private final Map<String, Class<? extends MaxAdapter>> f3724d = new HashMap();

    /* renamed from: e  reason: collision with root package name */
    private final Set<String> f3725e = new HashSet();

    public i(k kVar) {
        if (kVar != null) {
            this.f3721a = kVar;
            this.f3722b = kVar.Z();
            return;
        }
        throw new IllegalArgumentException("No sdk specified");
    }

    private j a(b.f fVar, Class<? extends MaxAdapter> cls) {
        try {
            j jVar = new j(fVar, (MediationAdapterBase) cls.getConstructor(AppLovinSdk.class).newInstance(this.f3721a.r()), this.f3721a);
            if (jVar.c()) {
                return jVar;
            }
            q.i("MediationAdapterManager", "Adapter is disabled after initialization: " + fVar);
            return null;
        } catch (Throwable th) {
            q.c("MediationAdapterManager", "Failed to load adapter: " + fVar, th);
            return null;
        }
    }

    private Class<? extends MaxAdapter> a(String str) {
        Class<MaxAdapter> cls = MaxAdapter.class;
        try {
            Class<?> cls2 = Class.forName(str);
            if (cls.isAssignableFrom(cls2)) {
                return cls2.asSubclass(cls);
            }
            q.i("MediationAdapterManager", str + " error: not an instance of '" + cls.getName() + "'.");
            return null;
        } catch (Throwable th) {
            q.c("MediationAdapterManager", "Failed to load: " + str, th);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public j a(b.f fVar) {
        Class<? extends MaxAdapter> cls;
        q qVar;
        String str;
        if (fVar != null) {
            String n = fVar.n();
            String m = fVar.m();
            if (TextUtils.isEmpty(n)) {
                qVar = this.f3722b;
                str = "No adapter name provided for " + m + ", not loading the adapter ";
            } else if (TextUtils.isEmpty(m)) {
                qVar = this.f3722b;
                str = "Unable to find default classname for '" + n + "'";
            } else {
                synchronized (this.f3723c) {
                    if (!this.f3725e.contains(m)) {
                        if (this.f3724d.containsKey(m)) {
                            cls = this.f3724d.get(m);
                        } else {
                            cls = a(m);
                            if (cls == null) {
                                this.f3725e.add(m);
                                return null;
                            }
                        }
                        j a2 = a(fVar, cls);
                        if (a2 != null) {
                            this.f3722b.b("MediationAdapterManager", "Loaded " + n);
                            this.f3724d.put(m, cls);
                            return a2;
                        }
                        this.f3722b.e("MediationAdapterManager", "Failed to load " + n);
                        this.f3725e.add(m);
                        return null;
                    }
                    this.f3722b.b("MediationAdapterManager", "Not attempting to load " + n + " due to prior errors");
                    return null;
                }
            }
            qVar.e("MediationAdapterManager", str);
            return null;
        }
        throw new IllegalArgumentException("No adapter spec specified");
    }

    public Collection<String> a() {
        Set unmodifiableSet;
        synchronized (this.f3723c) {
            HashSet hashSet = new HashSet(this.f3724d.size());
            for (Class<? extends MaxAdapter> name : this.f3724d.values()) {
                hashSet.add(name.getName());
            }
            unmodifiableSet = Collections.unmodifiableSet(hashSet);
        }
        return unmodifiableSet;
    }

    public Collection<String> b() {
        Set unmodifiableSet;
        synchronized (this.f3723c) {
            unmodifiableSet = Collections.unmodifiableSet(this.f3725e);
        }
        return unmodifiableSet;
    }
}
