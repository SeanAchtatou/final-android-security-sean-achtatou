package com.applovin.impl.mediation;

import android.app.Activity;
import android.text.TextUtils;
import com.applovin.impl.mediation.b;
import com.applovin.impl.mediation.c;
import com.applovin.impl.mediation.g;
import com.applovin.impl.sdk.f;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.q;
import com.applovin.impl.sdk.utils.j;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxAdViewAdListener;
import com.applovin.mediation.MaxErrorCodes;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.MaxRewardedAdListener;
import com.applovin.mediation.adapter.MaxAdapter;
import com.applovin.mediation.adapter.listeners.MaxSignalCollectionListener;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;

public class MediationServiceImpl {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final k f3465a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final q f3466b;

    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ MaxAdListener f3467a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ b.d f3468b;

        a(MediationServiceImpl mediationServiceImpl, MaxAdListener maxAdListener, b.d dVar) {
            this.f3467a = maxAdListener;
            this.f3468b = dVar;
        }

        public void run() {
            this.f3467a.onAdLoaded(this.f3468b);
        }
    }

    class b implements c.d.C0090c {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ g f3469a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ String f3470b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ MaxAdFormat f3471c;

        /* renamed from: d  reason: collision with root package name */
        final /* synthetic */ Activity f3472d;

        /* renamed from: e  reason: collision with root package name */
        final /* synthetic */ MaxAdListener f3473e;

        b(g gVar, String str, MaxAdFormat maxAdFormat, Activity activity, MaxAdListener maxAdListener) {
            this.f3469a = gVar;
            this.f3470b = str;
            this.f3471c = maxAdFormat;
            this.f3472d = activity;
            this.f3473e = maxAdListener;
        }

        public void a(JSONArray jSONArray) {
            g gVar = this.f3469a;
            if (gVar == null) {
                gVar = new g.b().a();
            }
            MediationServiceImpl.this.f3465a.j().a(new c.e(this.f3470b, this.f3471c, gVar, jSONArray, this.f3472d, MediationServiceImpl.this.f3465a, this.f3473e));
        }
    }

    class c implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ b.d f3475a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ j f3476b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ Activity f3477c;

        c(b.d dVar, j jVar, Activity activity) {
            this.f3475a = dVar;
            this.f3476b = jVar;
            this.f3477c = activity;
        }

        public void run() {
            if (this.f3475a.getFormat() == MaxAdFormat.REWARDED) {
                MediationServiceImpl.this.f3465a.j().a(new c.j(this.f3475a, MediationServiceImpl.this.f3465a), f.y.b.MEDIATION_REWARD);
            }
            this.f3476b.a(this.f3475a, this.f3477c);
            MediationServiceImpl.this.f3465a.y().a(false);
            MediationServiceImpl.this.f3466b.b("MediationService", "Scheduling impression for ad manually...");
            MediationServiceImpl.this.maybeScheduleRawAdImpressionPostback(this.f3475a);
        }
    }

    class d implements MaxSignalCollectionListener {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ b.g.a f3479a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ b.h f3480b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ j f3481c;

        d(b.g.a aVar, b.h hVar, j jVar) {
            this.f3479a = aVar;
            this.f3480b = hVar;
            this.f3481c = jVar;
        }

        public void onSignalCollected(String str) {
            this.f3479a.a(b.g.a(this.f3480b, this.f3481c, str));
        }

        public void onSignalCollectionFailed(String str) {
            MediationServiceImpl.this.a(str, this.f3480b);
            this.f3479a.a(b.g.b(this.f3480b, this.f3481c, str));
        }
    }

    private class e implements e, MaxAdViewAdListener, MaxRewardedAdListener {

        /* renamed from: a  reason: collision with root package name */
        private final b.C0087b f3483a;
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public final MaxAdListener f3484b;

        class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ MaxAd f3486a;

            a(MaxAd maxAd) {
                this.f3486a = maxAd;
            }

            public void run() {
                if (this.f3486a.getFormat() == MaxAdFormat.INTERSTITIAL || this.f3486a.getFormat() == MaxAdFormat.REWARDED) {
                    MediationServiceImpl.this.f3465a.y().d();
                }
                j.c(e.this.f3484b, this.f3486a);
            }
        }

        private e(b.C0087b bVar, MaxAdListener maxAdListener) {
            this.f3483a = bVar;
            this.f3484b = maxAdListener;
        }

        /* synthetic */ e(MediationServiceImpl mediationServiceImpl, b.C0087b bVar, MaxAdListener maxAdListener, a aVar) {
            this(bVar, maxAdListener);
        }

        public void a(MaxAd maxAd, f fVar) {
            MediationServiceImpl.this.b(this.f3483a, fVar, this.f3484b);
            if (maxAd.getFormat() == MaxAdFormat.REWARDED && (maxAd instanceof b.d)) {
                ((b.d) maxAd).I();
            }
        }

        public void a(String str, f fVar) {
            MediationServiceImpl.this.a(this.f3483a, fVar, this.f3484b);
        }

        public void onAdClicked(MaxAd maxAd) {
            MediationServiceImpl.this.f3465a.C().a((b.C0087b) maxAd, "DID_CLICKED");
            MediationServiceImpl.this.c(this.f3483a);
            j.d(this.f3484b, maxAd);
        }

        public void onAdCollapsed(MaxAd maxAd) {
            j.h(this.f3484b, maxAd);
        }

        public void onAdDisplayFailed(MaxAd maxAd, int i2) {
            MediationServiceImpl.this.b(this.f3483a, new f(i2), this.f3484b);
        }

        public void onAdDisplayed(MaxAd maxAd) {
            MediationServiceImpl.this.f3466b.b("MediationService", "Scheduling impression for ad via callback...");
            MediationServiceImpl.this.maybeScheduleCallbackAdImpressionPostback(this.f3483a);
            if (maxAd.getFormat() == MaxAdFormat.INTERSTITIAL || maxAd.getFormat() == MaxAdFormat.REWARDED) {
                MediationServiceImpl.this.f3465a.y().c();
            }
            j.b(this.f3484b, maxAd);
        }

        public void onAdExpanded(MaxAd maxAd) {
            j.g(this.f3484b, maxAd);
        }

        public void onAdHidden(MaxAd maxAd) {
            MediationServiceImpl.this.f3465a.C().a((b.C0087b) maxAd, "DID_HIDE");
            AppLovinSdkUtils.runOnUiThreadDelayed(new a(maxAd), maxAd instanceof b.f ? ((b.f) maxAd).i() : 0);
        }

        public void onAdLoadFailed(String str, int i2) {
            MediationServiceImpl.this.a(this.f3483a, new f(i2), this.f3484b);
        }

        public void onAdLoaded(MaxAd maxAd) {
            MediationServiceImpl.this.b(this.f3483a);
            j.a(this.f3484b, maxAd);
        }

        public void onRewardedVideoCompleted(MaxAd maxAd) {
            j.f(this.f3484b, maxAd);
        }

        public void onRewardedVideoStarted(MaxAd maxAd) {
            j.e(this.f3484b, maxAd);
        }

        public void onUserRewarded(MaxAd maxAd, MaxReward maxReward) {
            j.a(this.f3484b, maxAd, maxReward);
            MediationServiceImpl.this.f3465a.j().a(new c.i((b.d) maxAd, MediationServiceImpl.this.f3465a), f.y.b.MEDIATION_REWARD);
        }
    }

    public MediationServiceImpl(k kVar) {
        this.f3465a = kVar;
        this.f3466b = kVar.Z();
    }

    private void a(b.C0087b bVar) {
        q qVar = this.f3466b;
        qVar.b("MediationService", "Firing ad preload postback for " + bVar.n());
        a("mpreload", bVar);
    }

    /* access modifiers changed from: private */
    public void a(b.C0087b bVar, f fVar, MaxAdListener maxAdListener) {
        a(fVar, bVar);
        destroyAd(bVar);
        j.a(maxAdListener, bVar.getAdUnitId(), fVar.getErrorCode());
    }

    private void a(f fVar, b.C0087b bVar) {
        long t = bVar.t();
        q qVar = this.f3466b;
        qVar.b("MediationService", "Firing ad load failure postback with load time: " + t);
        HashMap hashMap = new HashMap(1);
        hashMap.put("{LOAD_TIME_MS}", String.valueOf(t));
        a("mlerr", hashMap, fVar, bVar);
    }

    private void a(String str, b.f fVar) {
        a(str, Collections.EMPTY_MAP, (f) null, fVar);
    }

    /* access modifiers changed from: private */
    public void a(String str, b.h hVar) {
        a("serr", Collections.EMPTY_MAP, new f(str), hVar);
    }

    private void a(String str, Map<String, String> map, b.f fVar) {
        a(str, map, (f) null, fVar);
    }

    private void a(String str, Map<String, String> map, f fVar, b.f fVar2) {
        HashMap hashMap = new HashMap(map);
        String str2 = "";
        hashMap.put("{PLACEMENT}", fVar2.j() != null ? fVar2.j() : str2);
        if (fVar2 instanceof b.d) {
            b.d dVar = (b.d) fVar2;
            if (dVar.A() != null) {
                str2 = dVar.A();
            }
            hashMap.put("{PUBLISHER_AD_UNIT_ID}", str2);
        }
        this.f3465a.j().a(new c.f(str, hashMap, fVar, fVar2, this.f3465a), f.y.b.MEDIATION_POSTBACKS);
    }

    /* access modifiers changed from: private */
    public void b(b.C0087b bVar) {
        long t = bVar.t();
        q qVar = this.f3466b;
        qVar.b("MediationService", "Firing ad load success postback with load time: " + t);
        HashMap hashMap = new HashMap(1);
        hashMap.put("{LOAD_TIME_MS}", String.valueOf(t));
        a("load", hashMap, bVar);
    }

    /* access modifiers changed from: private */
    public void b(b.C0087b bVar, f fVar, MaxAdListener maxAdListener) {
        this.f3465a.C().a(bVar, "DID_FAIL_DISPLAY");
        maybeScheduleAdDisplayErrorPostback(fVar, bVar);
        if (bVar.v().compareAndSet(false, true)) {
            j.a(maxAdListener, bVar, fVar.getErrorCode());
        }
    }

    /* access modifiers changed from: private */
    public void c(b.C0087b bVar) {
        a("mclick", bVar);
    }

    public void collectSignal(MaxAdFormat maxAdFormat, b.h hVar, Activity activity, b.g.a aVar) {
        String str;
        q qVar;
        String str2;
        StringBuilder sb;
        if (hVar == null) {
            throw new IllegalArgumentException("No spec specified");
        } else if (activity == null) {
            throw new IllegalArgumentException("No activity specified");
        } else if (aVar != null) {
            j a2 = this.f3465a.a0().a(hVar);
            if (a2 != null) {
                MaxAdapterParametersImpl a3 = MaxAdapterParametersImpl.a(hVar, maxAdFormat, activity.getApplicationContext());
                a2.a(a3, activity);
                d dVar = new d(aVar, hVar, a2);
                if (!hVar.p()) {
                    qVar = this.f3466b;
                    sb = new StringBuilder();
                    str2 = "Collecting signal for adapter: ";
                } else if (this.f3465a.b0().a(hVar)) {
                    qVar = this.f3466b;
                    sb = new StringBuilder();
                    str2 = "Collecting signal for now-initialized adapter: ";
                } else {
                    q qVar2 = this.f3466b;
                    qVar2.e("MediationService", "Skip collecting signal for not-initialized adapter: " + a2.b());
                    str = "Adapter not initialized yet";
                }
                sb.append(str2);
                sb.append(a2.b());
                qVar.b("MediationService", sb.toString());
                a2.a(a3, hVar, activity, dVar);
                return;
            }
            str = "Could not load adapter";
            aVar.a(b.g.a(hVar, str));
        } else {
            throw new IllegalArgumentException("No callback specified");
        }
    }

    public void destroyAd(MaxAd maxAd) {
        if (maxAd instanceof b.C0087b) {
            q qVar = this.f3466b;
            qVar.c("MediationService", "Destroying " + maxAd);
            b.C0087b bVar = (b.C0087b) maxAd;
            j q = bVar.q();
            if (q != null) {
                q.g();
                bVar.w();
            }
        }
    }

    public void loadAd(String str, MaxAdFormat maxAdFormat, g gVar, boolean z, Activity activity, MaxAdListener maxAdListener) {
        MaxAdListener maxAdListener2 = maxAdListener;
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("No ad unit ID specified");
        } else if (activity == null) {
            throw new IllegalArgumentException("No activity specified");
        } else if (maxAdListener2 != null) {
            if (!this.f3465a.H()) {
                q.h("AppLovinSdk", "Attempted to load ad before SDK initialization. Please wait until after the SDK has initialized, e.g. AppLovinSdk.initializeSdk(Context, SdkInitializationListener).");
            }
            this.f3465a.z();
            b.d a2 = this.f3465a.b().a(maxAdFormat);
            if (a2 != null) {
                AppLovinSdkUtils.runOnUiThreadDelayed(new a(this, maxAdListener2, a2), a2.y());
            }
            this.f3465a.j().a(new c.d(maxAdFormat, z, activity, this.f3465a, new b(gVar, str, maxAdFormat, activity, maxAdListener)), com.applovin.impl.mediation.d.c.a(maxAdFormat));
        } else {
            throw new IllegalArgumentException("No listener specified");
        }
    }

    public void loadThirdPartyMediatedAd(String str, b.C0087b bVar, Activity activity, MaxAdListener maxAdListener) {
        if (bVar != null) {
            q qVar = this.f3466b;
            qVar.b("MediationService", "Loading " + bVar + "...");
            this.f3465a.C().a(bVar, "WILL_LOAD");
            a(bVar);
            j a2 = this.f3465a.a0().a(bVar);
            if (a2 != null) {
                MaxAdapterParametersImpl a3 = MaxAdapterParametersImpl.a(bVar, activity.getApplicationContext());
                a2.a(a3, activity);
                b.C0087b a4 = bVar.a(a2);
                a2.a(str, a4);
                a4.u();
                a2.a(str, a3, a4, activity, new e(this, a4, maxAdListener, null));
                return;
            }
            q qVar2 = this.f3466b;
            qVar2.d("MediationService", "Failed to load " + bVar + ": adapter not loaded");
            a(bVar, new f((int) MaxErrorCodes.MEDIATION_ADAPTER_LOAD_FAILED), maxAdListener);
            return;
        }
        throw new IllegalArgumentException("No mediated ad specified");
    }

    public void maybeScheduleAdDisplayErrorPostback(f fVar, b.C0087b bVar) {
        a("mierr", Collections.EMPTY_MAP, fVar, bVar);
    }

    public void maybeScheduleAdapterInitializationPostback(b.f fVar, long j2, MaxAdapter.InitializationStatus initializationStatus, String str) {
        HashMap hashMap = new HashMap(3);
        hashMap.put("{INIT_STATUS}", String.valueOf(initializationStatus.getCode()));
        hashMap.put("{INIT_TIME_MS}", String.valueOf(j2));
        a("minit", hashMap, new f(str), fVar);
    }

    public void maybeScheduleCallbackAdImpressionPostback(b.C0087b bVar) {
        a("mcimp", bVar);
    }

    public void maybeScheduleRawAdImpressionPostback(b.C0087b bVar) {
        this.f3465a.C().a(bVar, "WILL_DISPLAY");
        a("mimp", bVar);
    }

    public void maybeScheduleViewabilityAdImpressionPostback(b.c cVar, long j2) {
        HashMap hashMap = new HashMap(1);
        hashMap.put("{VIEWABILITY_FLAGS}", String.valueOf(j2));
        hashMap.put("{USED_VIEWABILITY_TIMER}", String.valueOf(cVar.F()));
        a("mvimp", hashMap, cVar);
    }

    public void showFullscreenAd(MaxAd maxAd, String str, Activity activity) {
        if (maxAd == null) {
            throw new IllegalArgumentException("No ad specified");
        } else if (activity == null) {
            throw new IllegalArgumentException("No activity specified");
        } else if (maxAd instanceof b.d) {
            this.f3465a.y().a(true);
            b.d dVar = (b.d) maxAd;
            j q = dVar.q();
            if (q != null) {
                dVar.c(str);
                long g2 = dVar.g();
                q qVar = this.f3466b;
                qVar.c("MediationService", "Showing ad " + maxAd.getAdUnitId() + " with delay of " + g2 + "ms...");
                AppLovinSdkUtils.runOnUiThreadDelayed(new c(dVar, q, activity), g2);
                return;
            }
            this.f3465a.y().a(false);
            q qVar2 = this.f3466b;
            qVar2.d("MediationService", "Failed to show " + maxAd + ": adapter not found");
            q.i("MediationService", "There may be an integration problem with the adapter for ad unit id '" + dVar.getAdUnitId() + "'. Please check if you have a supported version of that SDK integrated into your project.");
            throw new IllegalStateException("Could not find adapter for provided ad");
        } else {
            q.i("MediationService", "Unable to show ad for '" + maxAd.getAdUnitId() + "': only REWARDED or INTERSTITIAL ads are eligible for showFullscreenAd(). " + maxAd.getFormat() + " ad was provided.");
            throw new IllegalArgumentException("Provided ad is not a MediatedFullscreenAd");
        }
    }
}
