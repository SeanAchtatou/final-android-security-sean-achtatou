package com.applovin.impl.mediation.ads;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.applovin.impl.mediation.MediationServiceImpl;
import com.applovin.impl.mediation.b;
import com.applovin.impl.mediation.g;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.f;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.q;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.p;
import com.applovin.impl.sdk.w;
import com.applovin.impl.sdk.x;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxAdViewAdListener;
import com.applovin.mediation.ads.MaxAdView;
import com.applovin.sdk.AppLovinSdkUtils;
import com.esotericsoftware.spine.Animation;
import java.util.concurrent.TimeUnit;

public class MaxAdViewImpl extends a implements f.b, x.c {
    private static final int[] q = {10, 14};
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final Activity f3579a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final MaxAdView f3580b;

    /* renamed from: c  reason: collision with root package name */
    private final View f3581c;

    /* renamed from: d  reason: collision with root package name */
    private long f3582d = Long.MAX_VALUE;

    /* renamed from: e  reason: collision with root package name */
    private b.c f3583e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public String f3584f;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public final d f3585g;
    /* access modifiers changed from: private */

    /* renamed from: h  reason: collision with root package name */
    public final f f3586h;
    /* access modifiers changed from: private */

    /* renamed from: i  reason: collision with root package name */
    public final com.applovin.impl.sdk.f f3587i;
    /* access modifiers changed from: private */

    /* renamed from: j  reason: collision with root package name */
    public final w f3588j;
    /* access modifiers changed from: private */

    /* renamed from: k  reason: collision with root package name */
    public final x f3589k;
    /* access modifiers changed from: private */
    public final Object l = new Object();
    /* access modifiers changed from: private */
    public b.c m = null;
    private boolean n;
    private boolean o;
    private boolean p = false;

    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ MaxAdListener f3590a;

        a(MaxAdListener maxAdListener) {
            this.f3590a = maxAdListener;
        }

        public void run() {
            if (MaxAdViewImpl.this.m != null) {
                long a2 = MaxAdViewImpl.this.f3588j.a(MaxAdViewImpl.this.m);
                MaxAdViewImpl maxAdViewImpl = MaxAdViewImpl.this;
                g.b bVar = maxAdViewImpl.loadRequestBuilder;
                bVar.a("visible_ad_ad_unit_id", maxAdViewImpl.m.getAdUnitId());
                bVar.a("viewability_flags", String.valueOf(a2));
            } else {
                g.b bVar2 = MaxAdViewImpl.this.loadRequestBuilder;
                bVar2.a("visible_ad_ad_unit_id");
                bVar2.a("viewability_flags");
            }
            MaxAdViewImpl maxAdViewImpl2 = MaxAdViewImpl.this;
            q qVar = maxAdViewImpl2.logger;
            String str = maxAdViewImpl2.tag;
            qVar.b(str, "Loading banner ad for '" + MaxAdViewImpl.this.adUnitId + "' and notifying " + this.f3590a + "...");
            MediationServiceImpl c0 = MaxAdViewImpl.this.sdk.c0();
            MaxAdViewImpl maxAdViewImpl3 = MaxAdViewImpl.this;
            c0.loadAd(maxAdViewImpl3.adUnitId, maxAdViewImpl3.adFormat, maxAdViewImpl3.loadRequestBuilder.a(), false, MaxAdViewImpl.this.f3579a, this.f3590a);
        }
    }

    class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ b.c f3592a;

        class a extends AnimatorListenerAdapter {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ MaxAdView f3594a;

            /* renamed from: com.applovin.impl.mediation.ads.MaxAdViewImpl$b$a$a  reason: collision with other inner class name */
            class C0086a implements Runnable {
                C0086a() {
                }

                public void run() {
                    long a2 = MaxAdViewImpl.this.f3588j.a(b.this.f3592a);
                    if (!b.this.f3592a.F()) {
                        b bVar = b.this;
                        MaxAdViewImpl.this.a(bVar.f3592a, a2);
                    }
                    MaxAdViewImpl.this.a(a2);
                }
            }

            a(MaxAdView maxAdView) {
                this.f3594a = maxAdView;
            }

            public void onAnimationEnd(Animator animator) {
                super.onAnimationEnd(animator);
                MaxAdViewImpl.this.a();
                if (b.this.f3592a.F()) {
                    MaxAdViewImpl.this.f3589k.a(MaxAdViewImpl.this.f3579a, b.this.f3592a);
                }
                b bVar = b.this;
                MaxAdViewImpl.this.a(bVar.f3592a, this.f3594a);
                synchronized (MaxAdViewImpl.this.l) {
                    b.c unused = MaxAdViewImpl.this.m = b.this.f3592a;
                }
                MaxAdViewImpl maxAdViewImpl = MaxAdViewImpl.this;
                maxAdViewImpl.logger.b(maxAdViewImpl.tag, "Scheduling impression for ad manually...");
                MaxAdViewImpl.this.sdk.c0().maybeScheduleRawAdImpressionPostback(b.this.f3592a);
                AppLovinSdkUtils.runOnUiThreadDelayed(new C0086a(), b.this.f3592a.A());
            }
        }

        b(b.c cVar) {
            this.f3592a = cVar;
        }

        public void run() {
            String str;
            q qVar;
            String str2;
            if (this.f3592a.z() != null) {
                MaxAdView d2 = MaxAdViewImpl.this.f3580b;
                if (d2 != null) {
                    MaxAdViewImpl.this.a(new a(d2));
                    return;
                }
                MaxAdViewImpl maxAdViewImpl = MaxAdViewImpl.this;
                qVar = maxAdViewImpl.logger;
                str2 = maxAdViewImpl.tag;
                str = "Max ad view does not have a parent View";
            } else {
                MaxAdViewImpl maxAdViewImpl2 = MaxAdViewImpl.this;
                qVar = maxAdViewImpl2.logger;
                str2 = maxAdViewImpl2.tag;
                str = "Max ad does not have a loaded ad view";
            }
            qVar.e(str2, str);
            MaxAdViewImpl.this.f3585g.onAdDisplayFailed(this.f3592a, -5201);
        }
    }

    class c implements Runnable {
        c() {
        }

        public void run() {
            MaxAdViewImpl maxAdViewImpl = MaxAdViewImpl.this;
            maxAdViewImpl.a(maxAdViewImpl.f3586h);
        }
    }

    private class d extends e {
        private d() {
            super(MaxAdViewImpl.this, null);
        }

        /* synthetic */ d(MaxAdViewImpl maxAdViewImpl, a aVar) {
            this();
        }

        public void onAdLoadFailed(String str, int i2) {
            j.a(MaxAdViewImpl.this.adListener, str, i2);
            MaxAdViewImpl.this.a(i2);
        }

        public void onAdLoaded(MaxAd maxAd) {
            if (maxAd instanceof b.c) {
                b.c cVar = (b.c) maxAd;
                cVar.c(MaxAdViewImpl.this.f3584f);
                MaxAdViewImpl.this.a(cVar);
                if (cVar.e()) {
                    long f2 = cVar.f();
                    q Z = MaxAdViewImpl.this.sdk.Z();
                    String str = MaxAdViewImpl.this.tag;
                    Z.b(str, "Scheduling banner ad refresh " + f2 + " milliseconds from now for '" + MaxAdViewImpl.this.adUnitId + "'...");
                    MaxAdViewImpl.this.f3587i.a(f2);
                }
                j.a(MaxAdViewImpl.this.adListener, maxAd);
                return;
            }
            MaxAdViewImpl maxAdViewImpl = MaxAdViewImpl.this;
            q qVar = maxAdViewImpl.logger;
            String str2 = maxAdViewImpl.tag;
            qVar.e(str2, "Not a MediatedAdViewAd received: " + maxAd);
            onAdLoadFailed(MaxAdViewImpl.this.adUnitId, -5201);
        }
    }

    private abstract class e implements MaxAdListener, MaxAdViewAdListener {
        private e() {
        }

        /* synthetic */ e(MaxAdViewImpl maxAdViewImpl, a aVar) {
            this();
        }

        public void onAdClicked(MaxAd maxAd) {
            if (maxAd.equals(MaxAdViewImpl.this.m)) {
                j.d(MaxAdViewImpl.this.adListener, maxAd);
            }
        }

        public void onAdCollapsed(MaxAd maxAd) {
            if (maxAd.equals(MaxAdViewImpl.this.m)) {
                if (MaxAdViewImpl.this.m.H()) {
                    MaxAdViewImpl.this.startAutoRefresh();
                }
                j.h(MaxAdViewImpl.this.adListener, maxAd);
            }
        }

        public void onAdDisplayFailed(MaxAd maxAd, int i2) {
            if (maxAd.equals(MaxAdViewImpl.this.m)) {
                j.a(MaxAdViewImpl.this.adListener, maxAd, i2);
            }
        }

        public void onAdDisplayed(MaxAd maxAd) {
            if (maxAd.equals(MaxAdViewImpl.this.m)) {
                j.b(MaxAdViewImpl.this.adListener, maxAd);
            }
        }

        public void onAdExpanded(MaxAd maxAd) {
            if (maxAd.equals(MaxAdViewImpl.this.m)) {
                if (MaxAdViewImpl.this.m.H()) {
                    MaxAdViewImpl.this.stopAutoRefresh();
                }
                j.g(MaxAdViewImpl.this.adListener, maxAd);
            }
        }

        public void onAdHidden(MaxAd maxAd) {
            if (maxAd.equals(MaxAdViewImpl.this.m)) {
                j.c(MaxAdViewImpl.this.adListener, maxAd);
            }
        }
    }

    private class f extends e {
        private f() {
            super(MaxAdViewImpl.this, null);
        }

        /* synthetic */ f(MaxAdViewImpl maxAdViewImpl, a aVar) {
            this();
        }

        public void onAdLoadFailed(String str, int i2) {
            MaxAdViewImpl maxAdViewImpl = MaxAdViewImpl.this;
            q qVar = maxAdViewImpl.logger;
            String str2 = maxAdViewImpl.tag;
            qVar.b(str2, "Failed to pre-cache ad for refresh with error code " + i2);
            MaxAdViewImpl.this.a(i2);
        }

        public void onAdLoaded(MaxAd maxAd) {
            MaxAdViewImpl maxAdViewImpl = MaxAdViewImpl.this;
            maxAdViewImpl.logger.b(maxAdViewImpl.tag, "Successfully pre-cached ad for refresh");
            MaxAdViewImpl.this.a(maxAd);
        }
    }

    public MaxAdViewImpl(String str, MaxAdView maxAdView, View view, k kVar, Activity activity) {
        super(str, MaxAdFormat.BANNER, "MaxAdView", kVar);
        if (activity != null) {
            this.f3579a = activity;
            this.f3580b = maxAdView;
            this.f3581c = view;
            this.f3585g = new d(this, null);
            this.f3586h = new f(this, null);
            this.f3587i = new com.applovin.impl.sdk.f(kVar, this);
            this.f3588j = new w(maxAdView, kVar);
            this.f3589k = new x(maxAdView, kVar, this);
            q qVar = this.logger;
            String str2 = this.tag;
            qVar.b(str2, "Created new MaxAdView (" + this + ")");
            return;
        }
        throw new IllegalArgumentException("No activity specified");
    }

    /* access modifiers changed from: private */
    public void a() {
        b.c cVar;
        MaxAdView maxAdView = this.f3580b;
        if (maxAdView != null) {
            com.applovin.impl.sdk.utils.b.a(maxAdView, this.f3581c);
        }
        this.f3589k.a();
        synchronized (this.l) {
            cVar = this.m;
        }
        if (cVar != null) {
            this.sdk.c0().destroyAd(cVar);
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        if (this.sdk.b(c.d.m4).contains(String.valueOf(i2))) {
            q Z = this.sdk.Z();
            String str = this.tag;
            Z.b(str, "Ignoring banner ad refresh for error code '" + i2 + "'...");
            return;
        }
        this.n = true;
        long longValue = ((Long) this.sdk.a(c.d.l4)).longValue();
        if (longValue >= 0) {
            q Z2 = this.sdk.Z();
            String str2 = this.tag;
            Z2.b(str2, "Scheduling failed banner ad refresh " + longValue + " milliseconds from now for '" + this.adUnitId + "'...");
            this.f3587i.a(longValue);
        }
    }

    /* access modifiers changed from: private */
    public void a(long j2) {
        if (p.a(j2, ((Long) this.sdk.a(c.d.v4)).longValue())) {
            q qVar = this.logger;
            String str = this.tag;
            qVar.b(str, "Undesired flags matched - current: " + Long.toBinaryString(j2) + ", undesired: " + Long.toBinaryString(j2));
            this.logger.b(this.tag, "Waiting for refresh timer to manually fire request");
            this.n = true;
            return;
        }
        this.logger.b(this.tag, "No undesired viewability flags matched - scheduling viewability");
        this.n = false;
        b();
    }

    /* access modifiers changed from: private */
    public void a(AnimatorListenerAdapter animatorListenerAdapter) {
        b.c cVar = this.m;
        if (cVar == null || cVar.z() == null) {
            animatorListenerAdapter.onAnimationEnd(null);
            return;
        }
        View z = this.m.z();
        z.animate().alpha(Animation.CurveTimeline.LINEAR).setDuration(((Long) this.sdk.a(c.d.s4)).longValue()).setListener(animatorListenerAdapter).start();
    }

    private void a(View view, b.c cVar) {
        int x = cVar.x();
        int y = cVar.y();
        int i2 = -1;
        int dpToPx = x == -1 ? -1 : AppLovinSdkUtils.dpToPx(view.getContext(), x);
        if (y != -1) {
            i2 = AppLovinSdkUtils.dpToPx(view.getContext(), y);
        }
        int height = this.f3580b.getHeight();
        int width = this.f3580b.getWidth();
        if ((height > 0 && height < i2) || (width > 0 && width < dpToPx)) {
            int pxToDp = AppLovinSdkUtils.pxToDp(view.getContext(), height);
            q.h("AppLovinSdk", "\n**************************************************\n`MaxAdView` size " + AppLovinSdkUtils.pxToDp(view.getContext(), width) + "x" + pxToDp + " dp smaller than required size: " + x + "x" + y + " dp\n**************************************************\n");
        }
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = new RelativeLayout.LayoutParams(dpToPx, i2);
        } else {
            layoutParams.width = dpToPx;
            layoutParams.height = i2;
        }
        if (layoutParams instanceof RelativeLayout.LayoutParams) {
            this.logger.b(this.tag, "Pinning ad view to MAX ad view with width: " + dpToPx + " and height: " + i2 + ".");
            RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) layoutParams;
            for (int addRule : com.applovin.impl.sdk.utils.g.c() ? com.applovin.impl.sdk.utils.q.a(this.f3580b.getGravity(), 10, 14) : q) {
                layoutParams2.addRule(addRule);
            }
        }
        view.setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: private */
    public void a(b.c cVar) {
        AppLovinSdkUtils.runOnUiThread(new b(cVar));
    }

    /* access modifiers changed from: private */
    public void a(b.c cVar, long j2) {
        this.logger.b(this.tag, "Scheduling viewability impression for ad...");
        this.sdk.c0().maybeScheduleViewabilityAdImpressionPostback(cVar, j2);
    }

    /* access modifiers changed from: private */
    public void a(b.c cVar, MaxAdView maxAdView) {
        View z = cVar.z();
        z.setAlpha(Animation.CurveTimeline.LINEAR);
        if (cVar.I() != Long.MAX_VALUE) {
            this.f3581c.setBackgroundColor((int) cVar.I());
        } else {
            long j2 = this.f3582d;
            if (j2 != Long.MAX_VALUE) {
                this.f3581c.setBackgroundColor((int) j2);
            } else {
                this.f3581c.setBackgroundColor(0);
            }
        }
        maxAdView.addView(z);
        a(z, cVar);
        z.animate().alpha(1.0f).setDuration(((Long) this.sdk.a(c.d.r4)).longValue()).start();
    }

    /* access modifiers changed from: private */
    public void a(MaxAd maxAd) {
        if (this.o) {
            this.o = false;
            q qVar = this.logger;
            String str = this.tag;
            qVar.b(str, "Rendering precache request ad: " + maxAd.getAdUnitId() + "...");
            this.f3585g.onAdLoaded(maxAd);
            return;
        }
        this.f3583e = (b.c) maxAd;
    }

    /* access modifiers changed from: private */
    public void a(MaxAdListener maxAdListener) {
        if (d()) {
            q.i(this.tag, "Unable to load new ad; ad is already destroyed");
            j.a(this.adListener, this.adUnitId, -1);
            return;
        }
        AppLovinSdkUtils.runOnUiThread(new a(maxAdListener));
    }

    private void b() {
        if (c()) {
            long longValue = ((Long) this.sdk.a(c.d.w4)).longValue();
            q qVar = this.logger;
            String str = this.tag;
            qVar.b(str, "Scheduling refresh precache request in " + TimeUnit.MICROSECONDS.toSeconds(longValue) + " seconds...");
            this.sdk.j().a(new f.C0104f(this.sdk, new c()), com.applovin.impl.mediation.d.c.a(this.adFormat), longValue);
        }
    }

    private boolean c() {
        return ((Long) this.sdk.a(c.d.w4)).longValue() > 0;
    }

    private boolean d() {
        boolean z;
        synchronized (this.l) {
            z = this.p;
        }
        return z;
    }

    public void destroy() {
        a();
        synchronized (this.l) {
            this.p = true;
        }
        this.f3587i.c();
    }

    public String getPlacement() {
        return this.f3584f;
    }

    public void loadAd() {
        q qVar = this.logger;
        String str = this.tag;
        qVar.b(str, "" + this + " Loading ad for " + this.adUnitId + "...");
        if (d()) {
            q.i(this.tag, "Unable to load new ad; ad is already destroyed");
            j.a(this.adListener, this.adUnitId, -1);
        } else if (!((Boolean) this.sdk.a(c.d.x4)).booleanValue() || !this.f3587i.a()) {
            a(this.f3585g);
        } else {
            String str2 = this.tag;
            q.i(str2, "Unable to load a new ad. An ad refresh has already been scheduled in " + TimeUnit.MILLISECONDS.toSeconds(this.f3587i.b()) + " seconds.");
        }
    }

    public void onAdRefresh() {
        String str;
        String str2;
        q qVar;
        this.o = false;
        if (this.f3583e != null) {
            q qVar2 = this.logger;
            String str3 = this.tag;
            qVar2.b(str3, "Refreshing for cached ad: " + this.f3583e.getAdUnitId() + "...");
            this.f3585g.onAdLoaded(this.f3583e);
            this.f3583e = null;
            return;
        }
        if (!c()) {
            qVar = this.logger;
            str2 = this.tag;
            str = "Refreshing ad from network...";
        } else if (this.n) {
            qVar = this.logger;
            str2 = this.tag;
            str = "Refreshing ad from network due to viewability requirements not met for refresh request...";
        } else {
            this.logger.e(this.tag, "Ignoring attempt to refresh ad - either still waiting for precache or did not attempt request due to visibility requirement not met");
            this.o = true;
            return;
        }
        qVar.b(str2, str);
        loadAd();
    }

    public void onLogVisibilityImpression() {
        a(this.m, this.f3588j.a(this.m));
    }

    public void onWindowVisibilityChanged(int i2) {
        if (((Boolean) this.sdk.a(c.d.q4)).booleanValue() && this.f3587i.a()) {
            if (com.applovin.impl.sdk.utils.q.a(i2)) {
                this.logger.b(this.tag, "Ad view visible");
                this.f3587i.g();
                return;
            }
            this.logger.b(this.tag, "Ad view hidden");
            this.f3587i.f();
        }
    }

    public void setPlacement(String str) {
        this.f3584f = str;
    }

    public void setPublisherBackgroundColor(int i2) {
        this.f3582d = (long) i2;
    }

    public void startAutoRefresh() {
        this.f3587i.e();
        q qVar = this.logger;
        String str = this.tag;
        qVar.b(str, "Resumed auto-refresh with remaining time: " + this.f3587i.b());
    }

    public void stopAutoRefresh() {
        if (this.m != null) {
            q qVar = this.logger;
            String str = this.tag;
            qVar.b(str, "Pausing auto-refresh with remaining time: " + this.f3587i.b());
            this.f3587i.d();
            return;
        }
        q.h(this.tag, "Stopping auto-refresh has no effect until after the first ad has been loaded.");
    }

    public String toString() {
        return "MaxAdView{adUnitId='" + this.adUnitId + '\'' + ", adListener=" + this.adListener + ", isDestroyed=" + d() + '}';
    }
}
