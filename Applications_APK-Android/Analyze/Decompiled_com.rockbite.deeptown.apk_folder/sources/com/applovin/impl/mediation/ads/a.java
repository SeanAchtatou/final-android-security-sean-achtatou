package com.applovin.impl.mediation.ads;

import com.applovin.impl.mediation.g;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.q;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;

public abstract class a {
    protected final MaxAdFormat adFormat;
    protected MaxAdListener adListener = null;
    protected final String adUnitId;
    protected final g.b loadRequestBuilder;
    protected final q logger;
    protected final k sdk;
    protected final String tag;

    protected a(String str, MaxAdFormat maxAdFormat, String str2, k kVar) {
        this.adUnitId = str;
        this.adFormat = maxAdFormat;
        this.sdk = kVar;
        this.tag = str2;
        this.logger = kVar.Z();
        this.loadRequestBuilder = new g.b();
    }

    public String getAdUnitId() {
        return this.adUnitId;
    }

    public void setExtraParameter(String str, String str2) {
        if (str != null) {
            this.loadRequestBuilder.a(str, str2);
            return;
        }
        throw new IllegalArgumentException("No key specified");
    }

    public void setListener(MaxAdListener maxAdListener) {
        q qVar = this.logger;
        String str = this.tag;
        qVar.b(str, "Setting listener: " + maxAdListener);
        this.adListener = maxAdListener;
    }
}
