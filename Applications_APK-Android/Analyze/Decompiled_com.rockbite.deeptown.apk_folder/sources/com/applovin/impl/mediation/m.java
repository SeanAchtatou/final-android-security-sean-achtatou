package com.applovin.impl.mediation;

import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;

public class m implements MaxAd {

    /* renamed from: a  reason: collision with root package name */
    private final String f3824a;

    /* renamed from: b  reason: collision with root package name */
    private final MaxAdFormat f3825b;

    public m(String str, MaxAdFormat maxAdFormat) {
        this.f3824a = str;
        this.f3825b = maxAdFormat;
    }

    public String getAdUnitId() {
        return this.f3824a;
    }

    public MaxAdFormat getFormat() {
        return this.f3825b;
    }
}
