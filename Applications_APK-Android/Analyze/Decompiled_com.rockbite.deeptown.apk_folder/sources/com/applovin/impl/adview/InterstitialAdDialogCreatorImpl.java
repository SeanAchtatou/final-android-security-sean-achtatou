package com.applovin.impl.adview;

import android.content.Context;
import com.applovin.adview.AppLovinInterstitialAdDialog;
import com.applovin.adview.InterstitialAdDialogCreator;
import com.applovin.sdk.AppLovinSdk;
import java.lang.ref.WeakReference;

public class InterstitialAdDialogCreatorImpl implements InterstitialAdDialogCreator {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f3251a = new Object();

    /* renamed from: b  reason: collision with root package name */
    private static WeakReference<n> f3252b = new WeakReference<>(null);

    /* renamed from: c  reason: collision with root package name */
    private static WeakReference<Context> f3253c = new WeakReference<>(null);

    public AppLovinInterstitialAdDialog createInterstitialAdDialog(AppLovinSdk appLovinSdk, Context context) {
        n nVar;
        if (appLovinSdk == null) {
            appLovinSdk = AppLovinSdk.getInstance(context);
        }
        synchronized (f3251a) {
            nVar = f3252b.get();
            if (nVar != null && nVar.isShowing()) {
                if (f3253c.get() == context) {
                    appLovinSdk.getLogger().d("InterstitialAdDialogCreator", "An interstitial dialog is already showing, returning it");
                }
            }
            nVar = new n(appLovinSdk, context);
            f3252b = new WeakReference<>(nVar);
            f3253c = new WeakReference<>(context);
        }
        return nVar;
    }
}
