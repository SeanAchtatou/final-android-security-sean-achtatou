package com.applovin.impl.adview;

import android.annotation.SuppressLint;
import android.content.Context;
import com.applovin.impl.adview.h;
import com.applovin.impl.sdk.k;

@SuppressLint({"ViewConstructor"})
public final class o extends h {

    /* renamed from: a  reason: collision with root package name */
    private float f3411a = 1.0f;

    public o(k kVar, Context context) {
        super(kVar, context);
    }

    public void a(int i2) {
        setViewScale(((float) i2) / 30.0f);
    }

    public h.a getStyle() {
        return h.a.Invisible;
    }

    public float getViewScale() {
        return this.f3411a;
    }

    public void setViewScale(float f2) {
        this.f3411a = f2;
    }
}
