package com.applovin.impl.adview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.PointF;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.View;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.k;

public class AppLovinTouchToClickListener implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private final long f3245a;

    /* renamed from: b  reason: collision with root package name */
    private final int f3246b;

    /* renamed from: c  reason: collision with root package name */
    private long f3247c;

    /* renamed from: d  reason: collision with root package name */
    private PointF f3248d;

    /* renamed from: e  reason: collision with root package name */
    private final Context f3249e;

    /* renamed from: f  reason: collision with root package name */
    private final OnClickListener f3250f;

    public interface OnClickListener {
        void onClick(View view, PointF pointF);
    }

    public AppLovinTouchToClickListener(k kVar, Context context, OnClickListener onClickListener) {
        this.f3245a = ((Long) kVar.a(c.e.O)).longValue();
        this.f3246b = ((Integer) kVar.a(c.e.P)).intValue();
        this.f3249e = context;
        this.f3250f = onClickListener;
    }

    private float a(float f2) {
        return f2 / this.f3249e.getResources().getDisplayMetrics().density;
    }

    private float a(PointF pointF, PointF pointF2) {
        float f2 = pointF.x - pointF2.x;
        float f3 = pointF.y - pointF2.y;
        return a((float) Math.sqrt((double) ((f2 * f2) + (f3 * f3))));
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouch(View view, MotionEvent motionEvent) {
        int i2;
        int action = motionEvent.getAction();
        if (action == 0) {
            this.f3247c = SystemClock.elapsedRealtime();
            this.f3248d = new PointF(motionEvent.getX(), motionEvent.getY());
        } else if (action == 1) {
            long elapsedRealtime = SystemClock.elapsedRealtime() - this.f3247c;
            float a2 = a(this.f3248d, new PointF(motionEvent.getX(), motionEvent.getY()));
            long j2 = this.f3245a;
            if ((j2 < 0 || elapsedRealtime < j2) && ((i2 = this.f3246b) < 0 || a2 < ((float) i2))) {
                this.f3250f.onClick(view, new PointF(motionEvent.getRawX(), motionEvent.getRawY()));
            }
        }
        return true;
    }
}
