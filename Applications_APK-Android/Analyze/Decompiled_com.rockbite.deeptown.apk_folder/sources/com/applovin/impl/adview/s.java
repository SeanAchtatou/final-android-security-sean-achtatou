package com.applovin.impl.adview;

import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.q;
import com.applovin.impl.sdk.utils.i;
import com.esotericsoftware.spine.Animation;
import com.tapjoy.TJAdUnitConstants;
import org.json.JSONObject;

public class s {

    /* renamed from: a  reason: collision with root package name */
    private final int f3425a;

    /* renamed from: b  reason: collision with root package name */
    private final int f3426b;

    /* renamed from: c  reason: collision with root package name */
    private final int f3427c;

    /* renamed from: d  reason: collision with root package name */
    private final int f3428d;

    /* renamed from: e  reason: collision with root package name */
    private final boolean f3429e;

    /* renamed from: f  reason: collision with root package name */
    private final int f3430f;

    /* renamed from: g  reason: collision with root package name */
    private final int f3431g;

    /* renamed from: h  reason: collision with root package name */
    private final int f3432h;

    /* renamed from: i  reason: collision with root package name */
    private final float f3433i;

    /* renamed from: j  reason: collision with root package name */
    private final float f3434j;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Boolean, com.applovin.impl.sdk.k):java.lang.Boolean
     arg types: [org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.k]
     candidates:
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, float, com.applovin.impl.sdk.k):float
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, long, com.applovin.impl.sdk.k):long
      com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, java.lang.Object, com.applovin.impl.sdk.k):java.lang.Object
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Object, com.applovin.impl.sdk.k):java.lang.Object
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.util.List, com.applovin.impl.sdk.k):java.util.List
      com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, org.json.JSONObject, com.applovin.impl.sdk.k):org.json.JSONObject
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.k):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.String, com.applovin.impl.sdk.k):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONArray, com.applovin.impl.sdk.k):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONObject, com.applovin.impl.sdk.k):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Boolean, com.applovin.impl.sdk.k):java.lang.Boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, float, com.applovin.impl.sdk.k):float
     arg types: [org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.k]
     candidates:
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, long, com.applovin.impl.sdk.k):long
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Boolean, com.applovin.impl.sdk.k):java.lang.Boolean
      com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, java.lang.Object, com.applovin.impl.sdk.k):java.lang.Object
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Object, com.applovin.impl.sdk.k):java.lang.Object
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.util.List, com.applovin.impl.sdk.k):java.util.List
      com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, org.json.JSONObject, com.applovin.impl.sdk.k):org.json.JSONObject
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.k):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.String, com.applovin.impl.sdk.k):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONArray, com.applovin.impl.sdk.k):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONObject, com.applovin.impl.sdk.k):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, float, com.applovin.impl.sdk.k):float */
    public s(JSONObject jSONObject, k kVar) {
        q Z = kVar.Z();
        Z.c("VideoButtonProperties", "Updating video button properties with JSON = " + i.d(jSONObject));
        this.f3425a = i.b(jSONObject, "width", 64, kVar);
        this.f3426b = i.b(jSONObject, "height", 7, kVar);
        this.f3427c = i.b(jSONObject, "margin", 20, kVar);
        this.f3428d = i.b(jSONObject, "gravity", 85, kVar);
        this.f3429e = i.a(jSONObject, "tap_to_fade", (Boolean) false, kVar).booleanValue();
        this.f3430f = i.b(jSONObject, "tap_to_fade_duration_milliseconds", (int) TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL, kVar);
        this.f3431g = i.b(jSONObject, "fade_in_duration_milliseconds", (int) TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL, kVar);
        this.f3432h = i.b(jSONObject, "fade_out_duration_milliseconds", (int) TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL, kVar);
        this.f3433i = i.a(jSONObject, "fade_in_delay_seconds", 1.0f, kVar);
        this.f3434j = i.a(jSONObject, "fade_out_delay_seconds", 6.0f, kVar);
    }

    public int a() {
        return this.f3425a;
    }

    public int b() {
        return this.f3426b;
    }

    public int c() {
        return this.f3427c;
    }

    public int d() {
        return this.f3428d;
    }

    public boolean e() {
        return this.f3429e;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || s.class != obj.getClass()) {
            return false;
        }
        s sVar = (s) obj;
        return this.f3425a == sVar.f3425a && this.f3426b == sVar.f3426b && this.f3427c == sVar.f3427c && this.f3428d == sVar.f3428d && this.f3429e == sVar.f3429e && this.f3430f == sVar.f3430f && this.f3431g == sVar.f3431g && this.f3432h == sVar.f3432h && Float.compare(sVar.f3433i, this.f3433i) == 0 && Float.compare(sVar.f3434j, this.f3434j) == 0;
    }

    public long f() {
        return (long) this.f3430f;
    }

    public long g() {
        return (long) this.f3431g;
    }

    public long h() {
        return (long) this.f3432h;
    }

    public int hashCode() {
        int i2 = ((((((((((((((this.f3425a * 31) + this.f3426b) * 31) + this.f3427c) * 31) + this.f3428d) * 31) + (this.f3429e ? 1 : 0)) * 31) + this.f3430f) * 31) + this.f3431g) * 31) + this.f3432h) * 31;
        float f2 = this.f3433i;
        int i3 = 0;
        int floatToIntBits = (i2 + (f2 != Animation.CurveTimeline.LINEAR ? Float.floatToIntBits(f2) : 0)) * 31;
        float f3 = this.f3434j;
        if (f3 != Animation.CurveTimeline.LINEAR) {
            i3 = Float.floatToIntBits(f3);
        }
        return floatToIntBits + i3;
    }

    public float i() {
        return this.f3433i;
    }

    public float j() {
        return this.f3434j;
    }

    public String toString() {
        return "VideoButtonProperties{widthPercentOfScreen=" + this.f3425a + ", heightPercentOfScreen=" + this.f3426b + ", margin=" + this.f3427c + ", gravity=" + this.f3428d + ", tapToFade=" + this.f3429e + ", tapToFadeDurationMillis=" + this.f3430f + ", fadeInDurationMillis=" + this.f3431g + ", fadeOutDurationMillis=" + this.f3432h + ", fadeInDelay=" + this.f3433i + ", fadeOutDelay=" + this.f3434j + '}';
    }
}
