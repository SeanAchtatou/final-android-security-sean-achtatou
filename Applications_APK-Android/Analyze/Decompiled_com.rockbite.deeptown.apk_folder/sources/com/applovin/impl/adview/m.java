package com.applovin.impl.adview;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.PointF;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.applovin.impl.adview.AppLovinTouchToClickListener;
import com.applovin.impl.adview.i;
import com.applovin.impl.adview.p;
import com.applovin.impl.adview.v;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.f;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinSdkUtils;
import com.esotericsoftware.spine.Animation;
import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class m extends Activity implements j {
    public static final String KEY_WRAPPER_ID = "com.applovin.interstitial.wrapper_id";
    public static volatile n lastKnownWrapper;
    private FrameLayout A;
    /* access modifiers changed from: private */
    public h B;
    /* access modifiers changed from: private */
    public View C;
    /* access modifiers changed from: private */
    public h D;
    /* access modifiers changed from: private */
    public View E;
    /* access modifiers changed from: private */
    public f F;
    private ImageView G;
    /* access modifiers changed from: private */
    public WeakReference<MediaPlayer> H = new WeakReference<>(null);
    private com.applovin.impl.sdk.a.b I;
    /* access modifiers changed from: private */
    public u J;
    /* access modifiers changed from: private */
    public ProgressBar K;
    private v.a L;
    private a M;
    private com.applovin.impl.sdk.utils.n N;
    private com.applovin.impl.sdk.utils.a O;

    /* renamed from: a  reason: collision with root package name */
    private l f3337a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public n f3338b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public com.applovin.impl.sdk.d.d f3339c;
    protected int computedLengthSeconds = 0;
    protected i countdownManager;
    public volatile com.applovin.impl.sdk.ad.f currentAd;

    /* renamed from: d  reason: collision with root package name */
    private volatile boolean f3340d = false;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public volatile boolean f3341e = false;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public volatile boolean f3342f = false;

    /* renamed from: g  reason: collision with root package name */
    private volatile boolean f3343g = false;

    /* renamed from: h  reason: collision with root package name */
    private volatile boolean f3344h = false;
    /* access modifiers changed from: private */

    /* renamed from: i  reason: collision with root package name */
    public volatile boolean f3345i = false;
    /* access modifiers changed from: private */

    /* renamed from: j  reason: collision with root package name */
    public volatile boolean f3346j = false;

    /* renamed from: k  reason: collision with root package name */
    private boolean f3347k = false;
    private volatile boolean l = false;
    public com.applovin.impl.sdk.q logger;
    private boolean m = true;
    /* access modifiers changed from: private */
    public boolean n = false;
    private long o = 0;
    /* access modifiers changed from: private */
    public long p = 0;
    protected volatile boolean poststitialWasDisplayed = false;
    /* access modifiers changed from: private */
    public long q = 0;
    /* access modifiers changed from: private */
    public long r = 0;
    /* access modifiers changed from: private */
    public long s = -2;
    public com.applovin.impl.sdk.k sdk;
    private int t = 0;
    private int u = Integer.MIN_VALUE;
    private AtomicBoolean v = new AtomicBoolean(false);
    protected volatile boolean videoMuted = false;
    public t videoView;
    private AtomicBoolean w = new AtomicBoolean(false);
    private AtomicBoolean x = new AtomicBoolean(false);
    private final Handler y = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public final Handler z = new Handler(Looper.getMainLooper());

    class a extends com.applovin.impl.sdk.utils.a {

        /* renamed from: com.applovin.impl.adview.m$a$a  reason: collision with other inner class name */
        class C0071a implements Runnable {
            C0071a() {
            }

            public void run() {
                com.applovin.impl.sdk.q.i("AppLovinInterstitialActivity", "Dismissing on-screen ad due to app relaunched via launcher.");
                m.this.dismiss();
            }
        }

        a() {
        }

        public void onActivityCreated(Activity activity, Bundle bundle) {
            com.applovin.impl.sdk.k kVar = m.this.sdk;
            if (kVar != null && ((Boolean) kVar.a(c.e.Q3)).booleanValue() && !m.this.f3342f) {
                if (activity.getClass().getName().equals(com.applovin.impl.sdk.utils.p.f(m.this.getApplicationContext()))) {
                    m.this.sdk.j().a(new f.C0104f(m.this.sdk, new C0071a()), f.y.b.MAIN);
                }
            }
        }
    }

    class a0 implements Runnable {
        a0() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.applovin.impl.adview.m.b(com.applovin.impl.adview.m, boolean):boolean
         arg types: [com.applovin.impl.adview.m, int]
         candidates:
          com.applovin.impl.adview.m.b(com.applovin.impl.adview.m, long):long
          com.applovin.impl.adview.m.b(int, boolean):void
          com.applovin.impl.adview.m.b(com.applovin.impl.adview.m, com.applovin.sdk.AppLovinAd):void
          com.applovin.impl.adview.m.b(com.applovin.impl.adview.m, boolean):boolean */
        public void run() {
            try {
                if (!m.this.f3346j && m.this.D != null) {
                    long unused = m.this.s = -1;
                    long unused2 = m.this.r = SystemClock.elapsedRealtime();
                    boolean unused3 = m.this.f3346j = true;
                    m.this.D.setVisibility(0);
                    AlphaAnimation alphaAnimation = new AlphaAnimation((float) Animation.CurveTimeline.LINEAR, 1.0f);
                    alphaAnimation.setDuration((long) ((Integer) m.this.sdk.a(c.e.D1)).intValue());
                    alphaAnimation.setRepeatCount(0);
                    m.this.D.startAnimation(alphaAnimation);
                    if (m.this.D() && m.this.E != null) {
                        m.this.E.setVisibility(0);
                        m.this.E.bringToFront();
                    }
                }
            } catch (Throwable th) {
                com.applovin.impl.sdk.q qVar = m.this.logger;
                qVar.d("InterActivity", "Unable to show skip button: " + th);
            }
        }
    }

    class b implements i.b {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ long f3351a;

        b(long j2) {
            this.f3351a = j2;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.applovin.impl.adview.m.c(com.applovin.impl.adview.m, boolean):boolean
         arg types: [com.applovin.impl.adview.m, int]
         candidates:
          com.applovin.impl.adview.m.c(com.applovin.impl.adview.m, long):long
          com.applovin.impl.adview.m.c(com.applovin.impl.adview.m, boolean):boolean */
        public void a() {
            if (m.this.F != null) {
                long seconds = TimeUnit.MILLISECONDS.toSeconds(this.f3351a - ((long) m.this.videoView.getCurrentPosition()));
                if (seconds <= 0) {
                    m.this.F.setVisibility(8);
                    boolean unused = m.this.n = true;
                } else if (m.this.H()) {
                    m.this.F.setProgress((int) seconds);
                }
            }
        }

        public boolean b() {
            return m.this.H();
        }
    }

    class b0 implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ h f3353a;

        b0(h hVar) {
            this.f3353a = hVar;
        }

        public void run() {
            if (this.f3353a.equals(m.this.B)) {
                m.this.C();
            } else if (this.f3353a.equals(m.this.D)) {
                m.this.E();
            }
        }
    }

    class c implements i.b {
        c() {
        }

        public void a() {
            if (m.this.K == null) {
                return;
            }
            if (m.this.shouldContinueFullLengthVideoCountdown()) {
                m.this.K.setProgress((int) ((((float) m.this.videoView.getCurrentPosition()) / ((float) m.this.videoView.getDuration())) * ((float) ((Integer) m.this.sdk.a(c.e.f2)).intValue())));
                return;
            }
            m.this.K.setVisibility(8);
        }

        public boolean b() {
            return m.this.shouldContinueFullLengthVideoCountdown();
        }
    }

    class d implements Runnable {
        d() {
        }

        public void run() {
            m.this.r();
        }
    }

    class e implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ s f3357a;

        e(s sVar) {
            this.f3357a = sVar;
        }

        public void run() {
            long g2 = this.f3357a.g();
            m mVar = m.this;
            mVar.a(mVar.J, true, g2);
            m.this.J.bringToFront();
        }
    }

    class f implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ s f3359a;

        f(s sVar) {
            this.f3359a = sVar;
        }

        public void run() {
            long h2 = this.f3359a.h();
            m mVar = m.this;
            mVar.a(mVar.J, false, h2);
        }
    }

    class g implements v.a {
        g() {
        }

        public void a(u uVar) {
            m.this.logger.b("InterActivity", "Skipping video from video button...");
            m.this.skipVideo();
        }

        public void b(u uVar) {
            m.this.logger.b("InterActivity", "Closing ad from video button...");
            m.this.dismiss();
        }

        public void c(u uVar) {
            m.this.logger.b("InterActivity", "Clicking through from video button...");
            m.this.clickThroughFromVideo(uVar.getAndClearLastClickLocation());
        }
    }

    class h implements Runnable {
        h() {
        }

        public void run() {
            if (m.this.currentAd != null && !m.this.currentAd.M().getAndSet(true)) {
                m.this.sdk.j().a(new f.f0(m.this.currentAd, m.this.sdk), f.y.b.REWARD);
            }
        }
    }

    class i implements AppLovinAdDisplayListener {
        i() {
        }

        public void adDisplayed(AppLovinAd appLovinAd) {
            if (!m.this.f3341e) {
                m.this.a(appLovinAd);
            }
        }

        public void adHidden(AppLovinAd appLovinAd) {
            m.this.b(appLovinAd);
        }
    }

    class j implements AppLovinAdClickListener {
        j() {
        }

        public void adClicked(AppLovinAd appLovinAd) {
            com.applovin.impl.sdk.utils.j.a(m.this.f3338b.e(), appLovinAd);
        }
    }

    class k implements Runnable {
        k() {
        }

        public void run() {
            m.this.c("javascript:al_onPoststitialShow();");
        }
    }

    class l implements Runnable {
        l() {
        }

        public void run() {
            m.this.o();
        }
    }

    /* renamed from: com.applovin.impl.adview.m$m  reason: collision with other inner class name */
    class C0072m implements Runnable {
        C0072m() {
        }

        public void run() {
            m mVar = m.this;
            mVar.b(mVar.videoMuted);
        }
    }

    class n implements Animation.AnimationListener {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ View f3368a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ boolean f3369b;

        n(m mVar, View view, boolean z) {
            this.f3368a = view;
            this.f3369b = z;
        }

        public void onAnimationEnd(android.view.animation.Animation animation) {
            if (!this.f3369b) {
                this.f3368a.setVisibility(4);
            }
        }

        public void onAnimationRepeat(android.view.animation.Animation animation) {
        }

        public void onAnimationStart(android.view.animation.Animation animation) {
            this.f3368a.setVisibility(0);
        }
    }

    class o implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AppLovinAdDisplayListener f3370a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ String f3371b;

        o(m mVar, AppLovinAdDisplayListener appLovinAdDisplayListener, String str) {
            this.f3370a = appLovinAdDisplayListener;
            this.f3371b = str;
        }

        public void run() {
            ((com.applovin.impl.sdk.ad.i) this.f3370a).onAdDisplayFailed(this.f3371b);
        }
    }

    class p implements p.b {
        p() {
        }

        public void a(String str) {
            m.this.handleMediaError(str);
        }
    }

    class q implements MediaPlayer.OnPreparedListener {

        class a implements MediaPlayer.OnErrorListener {

            /* renamed from: com.applovin.impl.adview.m$q$a$a  reason: collision with other inner class name */
            class C0073a implements Runnable {

                /* renamed from: a  reason: collision with root package name */
                final /* synthetic */ int f3375a;

                /* renamed from: b  reason: collision with root package name */
                final /* synthetic */ int f3376b;

                C0073a(int i2, int i3) {
                    this.f3375a = i2;
                    this.f3376b = i3;
                }

                public void run() {
                    m mVar = m.this;
                    mVar.handleMediaError("Media player error (" + this.f3375a + "," + this.f3376b + ")");
                }
            }

            a() {
            }

            public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
                m.this.z.post(new C0073a(i2, i3));
                return true;
            }
        }

        class b implements MediaPlayer.OnInfoListener {
            b() {
            }

            public boolean onInfo(MediaPlayer mediaPlayer, int i2, int i3) {
                if (i2 != 3) {
                    if (i2 == 701) {
                        m.this.O();
                        if (m.this.f3339c == null) {
                            return false;
                        }
                        m.this.f3339c.h();
                        return false;
                    } else if (i2 != 702) {
                        return false;
                    }
                }
                m.this.P();
                return false;
            }
        }

        q() {
        }

        public void onPrepared(MediaPlayer mediaPlayer) {
            WeakReference unused = m.this.H = new WeakReference(mediaPlayer);
            boolean i2 = m.this.y();
            float f2 = i2 ^ true ? 1.0f : 0.0f;
            mediaPlayer.setVolume(f2, f2);
            if (m.this.f3339c != null) {
                m.this.f3339c.e(i2 ? 1 : 0);
            }
            int videoWidth = mediaPlayer.getVideoWidth();
            int videoHeight = mediaPlayer.getVideoHeight();
            m.this.computedLengthSeconds = (int) TimeUnit.MILLISECONDS.toSeconds((long) mediaPlayer.getDuration());
            m.this.videoView.setVideoSize(videoWidth, videoHeight);
            t tVar = m.this.videoView;
            if (tVar instanceof AppLovinVideoView) {
                SurfaceHolder holder = ((AppLovinVideoView) tVar).getHolder();
                if (holder.getSurface() != null) {
                    mediaPlayer.setDisplay(holder);
                }
            }
            mediaPlayer.setOnErrorListener(new a());
            mediaPlayer.setOnInfoListener(new b());
            if (m.this.p == 0) {
                m.this.G();
                m.this.A();
                m.this.L();
                m.this.K();
                m.this.playVideo();
                m.this.i();
            }
        }
    }

    class r implements MediaPlayer.OnCompletionListener {
        r() {
        }

        public void onCompletion(MediaPlayer mediaPlayer) {
            m.this.x();
        }
    }

    class s implements MediaPlayer.OnErrorListener {

        class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ int f3381a;

            /* renamed from: b  reason: collision with root package name */
            final /* synthetic */ int f3382b;

            a(int i2, int i3) {
                this.f3381a = i2;
                this.f3382b = i3;
            }

            public void run() {
                m mVar = m.this;
                mVar.handleMediaError("Video view error (" + this.f3381a + "," + this.f3382b);
            }
        }

        s() {
        }

        public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
            m.this.z.post(new a(i2, i3));
            return true;
        }
    }

    class t implements AppLovinTouchToClickListener.OnClickListener {
        t() {
        }

        public void onClick(View view, PointF pointF) {
            m.this.a(pointF);
        }
    }

    class u implements View.OnClickListener {
        u() {
        }

        public void onClick(View view) {
            m.this.b();
        }
    }

    class v implements View.OnClickListener {
        v() {
        }

        public void onClick(View view) {
            m.this.a();
        }
    }

    class w implements View.OnClickListener {
        w() {
        }

        public void onClick(View view) {
            m.this.B.performClick();
        }
    }

    class x implements View.OnClickListener {
        x() {
        }

        public void onClick(View view) {
            m.this.D.performClick();
        }
    }

    class y implements View.OnClickListener {
        y() {
        }

        public void onClick(View view) {
            m.this.toggleMute();
        }
    }

    class z implements Runnable {
        z() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.applovin.impl.adview.m.a(com.applovin.impl.adview.m, boolean):boolean
         arg types: [com.applovin.impl.adview.m, int]
         candidates:
          com.applovin.impl.adview.m.a(int, boolean):int
          com.applovin.impl.adview.m.a(com.applovin.impl.adview.m, long):long
          com.applovin.impl.adview.m.a(com.applovin.impl.adview.m, java.lang.ref.WeakReference):java.lang.ref.WeakReference
          com.applovin.impl.adview.m.a(long, com.applovin.impl.adview.h):void
          com.applovin.impl.adview.m.a(com.applovin.impl.adview.m, android.graphics.PointF):void
          com.applovin.impl.adview.m.a(com.applovin.impl.adview.m, com.applovin.sdk.AppLovinAd):void
          com.applovin.impl.adview.m.a(com.applovin.impl.adview.m, java.lang.String):void
          com.applovin.impl.adview.m.a(com.applovin.impl.adview.m, boolean):boolean */
        public void run() {
            try {
                if (m.this.f3345i) {
                    m.this.B.setVisibility(0);
                    return;
                }
                long unused = m.this.q = SystemClock.elapsedRealtime();
                boolean unused2 = m.this.f3345i = true;
                if (m.this.D() && m.this.C != null) {
                    m.this.C.setVisibility(0);
                    m.this.C.bringToFront();
                }
                m.this.B.setVisibility(0);
                m.this.B.bringToFront();
                AlphaAnimation alphaAnimation = new AlphaAnimation((float) Animation.CurveTimeline.LINEAR, 1.0f);
                alphaAnimation.setDuration((long) ((Integer) m.this.sdk.a(c.e.D1)).intValue());
                alphaAnimation.setRepeatCount(0);
                m.this.B.startAnimation(alphaAnimation);
            } catch (Throwable unused3) {
                m.this.dismiss();
            }
        }
    }

    /* access modifiers changed from: private */
    public void A() {
        if (this.G == null) {
            try {
                this.videoMuted = y();
                this.G = new ImageView(this);
                if (!B()) {
                    int a2 = a(((Integer) this.sdk.a(c.e.a2)).intValue());
                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(a2, a2, ((Integer) this.sdk.a(c.e.c2)).intValue());
                    this.G.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    int a3 = a(((Integer) this.sdk.a(c.e.b2)).intValue());
                    layoutParams.setMargins(a3, a3, a3, a3);
                    if ((this.videoMuted ? this.currentAd.b0() : this.currentAd.c0()) != null) {
                        com.applovin.impl.sdk.q Z = this.sdk.Z();
                        Z.b("InterActivity", "Added mute button with params: " + layoutParams);
                        a(this.videoMuted);
                        this.G.setClickable(true);
                        this.G.setOnClickListener(new y());
                        this.A.addView(this.G, layoutParams);
                        this.G.bringToFront();
                        return;
                    }
                    this.sdk.Z().e("InterActivity", "Attempting to add mute button but could not find uri");
                    return;
                }
                this.sdk.Z().b("InterActivity", "Mute button should be hidden");
            } catch (Exception e2) {
                this.sdk.Z().a("InterActivity", "Failed to attach mute button", e2);
            }
        }
    }

    private boolean B() {
        if (!((Boolean) this.sdk.a(c.e.V1)).booleanValue()) {
            return true;
        }
        if (!((Boolean) this.sdk.a(c.e.W1)).booleanValue() || y()) {
            return false;
        }
        return !((Boolean) this.sdk.a(c.e.Y1)).booleanValue();
    }

    /* access modifiers changed from: private */
    public void C() {
        runOnUiThread(new z());
    }

    /* access modifiers changed from: private */
    public boolean D() {
        return ((Integer) this.sdk.a(c.e.a1)).intValue() > 0;
    }

    /* access modifiers changed from: private */
    public void E() {
        runOnUiThread(new a0());
    }

    private void F() {
        h hVar;
        if (this.currentAd.h0() >= Animation.CurveTimeline.LINEAR) {
            if (!this.f3347k || (hVar = this.D) == null) {
                hVar = this.B;
            }
            a(com.applovin.impl.sdk.utils.p.b(this.currentAd.h0()), hVar);
        }
    }

    /* access modifiers changed from: private */
    public void G() {
        boolean z2 = ((Boolean) this.sdk.a(c.e.K1)).booleanValue() && J() > 0;
        if (this.F == null && z2) {
            this.F = new f(this);
            int b2 = this.currentAd.b();
            this.F.setTextColor(b2);
            this.F.setTextSize((float) ((Integer) this.sdk.a(c.e.J1)).intValue());
            this.F.setFinishedStrokeColor(b2);
            this.F.setFinishedStrokeWidth((float) ((Integer) this.sdk.a(c.e.I1)).intValue());
            this.F.setMax(J());
            this.F.setProgress(J());
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(a(((Integer) this.sdk.a(c.e.H1)).intValue()), a(((Integer) this.sdk.a(c.e.H1)).intValue()), ((Integer) this.sdk.a(c.e.G1)).intValue());
            int a2 = a(((Integer) this.sdk.a(c.e.F1)).intValue());
            layoutParams.setMargins(a2, a2, a2, a2);
            this.A.addView(this.F, layoutParams);
            this.F.bringToFront();
            this.F.setVisibility(0);
            this.countdownManager.a("COUNTDOWN_CLOCK", 1000, new b(I()));
        }
    }

    /* access modifiers changed from: private */
    public boolean H() {
        return !this.n && !this.poststitialWasDisplayed && this.videoView.isPlaying();
    }

    private long I() {
        return TimeUnit.SECONDS.toMillis((long) J());
    }

    private int J() {
        int a2 = this.currentAd.a();
        return (a2 <= 0 && ((Boolean) this.sdk.a(c.e.k2)).booleanValue()) ? this.computedLengthSeconds + 1 : a2;
    }

    /* access modifiers changed from: private */
    @SuppressLint({"NewApi"})
    public void K() {
        if (this.K == null && this.currentAd.l()) {
            this.logger.c("InterActivity", "Attaching video progress bar...");
            this.K = new ProgressBar(this, null, 16842872);
            this.K.setMax(((Integer) this.sdk.a(c.e.f2)).intValue());
            this.K.setPadding(0, 0, 0, 0);
            if (com.applovin.impl.sdk.utils.g.f()) {
                try {
                    this.K.setProgressTintList(ColorStateList.valueOf(this.currentAd.m()));
                } catch (Throwable th) {
                    this.logger.b("InterActivity", "Unable to update progress bar color.", th);
                }
            }
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(this.videoView.getWidth(), 20, 80);
            layoutParams.setMargins(0, 0, 0, ((Integer) this.sdk.a(c.e.g2)).intValue());
            this.A.addView(this.K, layoutParams);
            this.K.bringToFront();
            this.countdownManager.a("PROGRESS_BAR", ((Long) this.sdk.a(c.e.e2)).longValue(), new c());
        }
    }

    /* access modifiers changed from: private */
    public void L() {
        s o0 = this.currentAd.o0();
        if (com.applovin.impl.sdk.utils.m.b(this.currentAd.n0()) && o0 != null && this.J == null) {
            this.logger.c("InterActivity", "Attaching video button...");
            this.J = M();
            double a2 = (double) o0.a();
            Double.isNaN(a2);
            double b2 = (double) o0.b();
            Double.isNaN(b2);
            int width = this.videoView.getWidth();
            int height = this.videoView.getHeight();
            double d2 = (double) width;
            Double.isNaN(d2);
            double d3 = (double) height;
            Double.isNaN(d3);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams((int) ((a2 / 100.0d) * d2), (int) ((b2 / 100.0d) * d3), o0.d());
            int a3 = a(o0.c());
            layoutParams.setMargins(a3, a3, a3, a3);
            this.A.addView(this.J, layoutParams);
            this.J.bringToFront();
            if (o0.i() > Animation.CurveTimeline.LINEAR) {
                this.J.setVisibility(4);
                this.z.postDelayed(new e(o0), com.applovin.impl.sdk.utils.p.b(o0.i()));
            }
            if (o0.j() > Animation.CurveTimeline.LINEAR) {
                this.z.postDelayed(new f(o0), com.applovin.impl.sdk.utils.p.b(o0.j()));
            }
        }
    }

    private u M() {
        com.applovin.impl.sdk.q qVar = this.logger;
        qVar.b("InterActivity", "Create video button with HTML = " + this.currentAd.n0());
        v vVar = new v(this.sdk);
        this.L = new g();
        vVar.a(new WeakReference(this.L));
        u uVar = new u(vVar, getApplicationContext());
        uVar.a(this.currentAd.n0());
        return uVar;
    }

    private void N() {
        if (this.l && this.currentAd.n()) {
            this.M = new a(this, ((Integer) this.sdk.a(c.e.j2)).intValue(), this.currentAd.p());
            this.M.setColor(this.currentAd.q());
            this.M.setBackgroundColor(this.currentAd.r());
            this.M.setVisibility(8);
            this.A.addView(this.M, new FrameLayout.LayoutParams(-1, -1, 17));
            this.A.bringChildToFront(this.M);
        }
    }

    /* access modifiers changed from: private */
    public void O() {
        a aVar = this.M;
        if (aVar != null) {
            aVar.a();
        }
    }

    /* access modifiers changed from: private */
    public void P() {
        a aVar = this.M;
        if (aVar != null) {
            aVar.b();
        }
    }

    private int a(int i2) {
        return AppLovinSdkUtils.dpToPx(this, i2);
    }

    private int a(int i2, boolean z2) {
        if (z2) {
            if (i2 == 0) {
                return 0;
            }
            if (i2 == 1) {
                return 9;
            }
            if (i2 == 2) {
                return 8;
            }
            return i2 == 3 ? 1 : -1;
        } else if (i2 == 0) {
            return 1;
        } else {
            if (i2 == 1) {
                return 0;
            }
            if (i2 == 2) {
                return 9;
            }
            return i2 == 3 ? 8 : -1;
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        if (c()) {
            m();
            pauseReportRewardTask();
            this.logger.b("InterActivity", "Prompting incentivized ad close warning");
            this.I.b();
            return;
        }
        skipVideo();
    }

    private void a(long j2, h hVar) {
        this.z.postDelayed(new b0(hVar), j2);
    }

    /* access modifiers changed from: private */
    public void a(PointF pointF) {
        if (!this.currentAd.p0() || this.currentAd.f0() == null) {
            u();
            v();
            return;
        }
        this.sdk.Z().b("InterActivity", "Clicking through video...");
        clickThroughFromVideo(pointF);
    }

    private void a(Uri uri) {
        this.videoView = this.currentAd.Y() ? new p(this.sdk, this, new p()) : new AppLovinVideoView(this, this.sdk);
        if (uri != null) {
            this.videoView.setOnPreparedListener(new q());
            this.videoView.setOnCompletionListener(new r());
            this.videoView.setOnErrorListener(new s());
            StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
            this.videoView.setVideoURI(uri);
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        }
        this.videoView.setLayoutParams(new FrameLayout.LayoutParams(-1, -1, 17));
        this.videoView.setOnTouchListener(new AppLovinTouchToClickListener(this.sdk, this, new t()));
        this.A.addView((View) this.videoView);
        setContentView(this.A);
        F();
        N();
    }

    /* access modifiers changed from: private */
    public void a(View view, boolean z2, long j2) {
        float f2 = Animation.CurveTimeline.LINEAR;
        float f3 = z2 ? Animation.CurveTimeline.LINEAR : 1.0f;
        if (z2) {
            f2 = 1.0f;
        }
        AlphaAnimation alphaAnimation = new AlphaAnimation(f3, f2);
        alphaAnimation.setDuration(j2);
        alphaAnimation.setAnimationListener(new n(this, view, z2));
        view.startAnimation(alphaAnimation);
    }

    /* access modifiers changed from: private */
    public void a(AppLovinAd appLovinAd) {
        com.applovin.impl.sdk.utils.j.a(this.f3338b.d(), appLovinAd);
        this.f3341e = true;
        this.sdk.y().c();
        AppLovinSdkUtils.runOnUiThreadDelayed(new C0072m(), ((Long) this.sdk.a(c.e.l2)).longValue());
    }

    private void a(AppLovinAd appLovinAd, double d2, boolean z2) {
        this.f3344h = true;
        com.applovin.impl.sdk.utils.j.a(this.f3338b.c(), appLovinAd, d2, z2);
    }

    private void a(String str) {
        n nVar = this.f3338b;
        if (nVar != null) {
            AppLovinAdDisplayListener d2 = nVar.d();
            if ((d2 instanceof com.applovin.impl.sdk.ad.i) && this.x.compareAndSet(false, true)) {
                runOnUiThread(new o(this, d2, str));
            }
        }
    }

    private void a(boolean z2) {
        Uri b02 = z2 ? this.currentAd.b0() : this.currentAd.c0();
        int a2 = a(((Integer) this.sdk.a(c.e.a2)).intValue());
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        AppLovinSdkUtils.safePopulateImageView(this.G, b02, a2);
        StrictMode.setThreadPolicy(allowThreadDiskReads);
    }

    /* access modifiers changed from: private */
    public void b() {
        c adWebView;
        if (this.currentAd.w() && (adWebView = ((AdViewControllerImpl) this.f3337a.getAdViewController()).getAdWebView()) != null) {
            adWebView.a("javascript:al_onCloseButtonTapped();");
        }
        if (d()) {
            this.logger.b("InterActivity", "Prompting incentivized non-video ad close warning");
            this.I.c();
            return;
        }
        dismiss();
    }

    private void b(int i2) {
        try {
            setRequestedOrientation(i2);
        } catch (Throwable th) {
            this.sdk.Z().b("InterActivity", "Failed to set requested orientation", th);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0050, code lost:
        if (r7 == 2) goto L_0x0028;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005f, code lost:
        if (r7 == 1) goto L_0x0053;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0026, code lost:
        if (r7 == 1) goto L_0x0028;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(int r7, boolean r8) {
        /*
            r6 = this;
            com.applovin.impl.sdk.k r0 = r6.sdk
            com.applovin.impl.sdk.c$e<java.lang.Boolean> r1 = com.applovin.impl.sdk.c.e.R1
            java.lang.Object r0 = r0.a(r1)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            com.applovin.impl.adview.n r1 = r6.f3338b
            com.applovin.impl.sdk.ad.f$b r1 = r1.f()
            com.applovin.impl.sdk.ad.f$b r2 = com.applovin.impl.sdk.ad.f.b.ACTIVITY_PORTRAIT
            r3 = 3
            r4 = 2
            r5 = 1
            if (r1 != r2) goto L_0x003a
            r1 = 9
            if (r8 == 0) goto L_0x002c
            if (r7 == r5) goto L_0x0024
            if (r7 == r3) goto L_0x0024
            goto L_0x0030
        L_0x0024:
            if (r0 == 0) goto L_0x0062
            if (r7 != r5) goto L_0x0030
        L_0x0028:
            r6.b(r1)
            goto L_0x0062
        L_0x002c:
            if (r7 == 0) goto L_0x0034
            if (r7 == r4) goto L_0x0034
        L_0x0030:
            r6.b(r5)
            goto L_0x0062
        L_0x0034:
            if (r0 == 0) goto L_0x0062
            if (r7 != 0) goto L_0x0028
            r1 = 1
            goto L_0x0028
        L_0x003a:
            com.applovin.impl.adview.n r1 = r6.f3338b
            com.applovin.impl.sdk.ad.f$b r1 = r1.f()
            com.applovin.impl.sdk.ad.f$b r2 = com.applovin.impl.sdk.ad.f.b.ACTIVITY_LANDSCAPE
            if (r1 != r2) goto L_0x0062
            r1 = 8
            r2 = 0
            if (r8 == 0) goto L_0x0055
            if (r7 == 0) goto L_0x004e
            if (r7 == r4) goto L_0x004e
            goto L_0x0059
        L_0x004e:
            if (r0 == 0) goto L_0x0062
            if (r7 != r4) goto L_0x0053
            goto L_0x0028
        L_0x0053:
            r1 = 0
            goto L_0x0028
        L_0x0055:
            if (r7 == r5) goto L_0x005d
            if (r7 == r3) goto L_0x005d
        L_0x0059:
            r6.b(r2)
            goto L_0x0062
        L_0x005d:
            if (r0 == 0) goto L_0x0062
            if (r7 != r5) goto L_0x0028
            goto L_0x0053
        L_0x0062:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.adview.m.b(int, boolean):void");
    }

    /* access modifiers changed from: private */
    public void b(AppLovinAd appLovinAd) {
        dismiss();
        c(appLovinAd);
    }

    private void b(String str) {
        com.applovin.impl.sdk.ad.f fVar = this.currentAd;
        if (fVar != null && fVar.y()) {
            c(str);
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        c adWebView;
        if (this.currentAd.v() && (adWebView = ((AdViewControllerImpl) this.f3337a.getAdViewController()).getAdWebView()) != null) {
            try {
                adWebView.a(z2 ? "javascript:al_mute();" : "javascript:al_unmute();");
            } catch (Throwable th) {
                this.logger.b("InterActivity", "Unable to forward mute setting to template.", th);
            }
        }
    }

    private void c(AppLovinAd appLovinAd) {
        if (!this.f3342f) {
            this.f3342f = true;
            n nVar = this.f3338b;
            if (nVar != null) {
                com.applovin.impl.sdk.utils.j.b(nVar.d(), appLovinAd);
            }
            this.sdk.y().d();
        }
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        c adWebView = ((AdViewControllerImpl) this.f3337a.getAdViewController()).getAdWebView();
        if (adWebView != null && com.applovin.impl.sdk.utils.m.b(str)) {
            adWebView.a(str);
        }
    }

    private void c(boolean z2) {
        this.videoMuted = z2;
        MediaPlayer mediaPlayer = this.H.get();
        if (mediaPlayer != null) {
            float f2 = (float) (z2 ? 0 : 1);
            try {
                mediaPlayer.setVolume(f2, f2);
            } catch (IllegalStateException e2) {
                com.applovin.impl.sdk.q qVar = this.logger;
                qVar.b("InterActivity", "Failed to set MediaPlayer muted: " + z2, e2);
            }
        }
    }

    private boolean c() {
        return g() && !isFullyWatched() && ((Boolean) this.sdk.a(c.e.K0)).booleanValue() && this.I != null;
    }

    private void d(AppLovinAd appLovinAd) {
        if (!this.f3343g) {
            this.f3343g = true;
            com.applovin.impl.sdk.utils.j.a(this.f3338b.c(), appLovinAd);
        }
    }

    private boolean d() {
        return h() && !f() && ((Boolean) this.sdk.a(c.e.P0)).booleanValue() && this.I != null;
    }

    private int e() {
        if (!(this.currentAd instanceof com.applovin.impl.sdk.ad.a)) {
            return 0;
        }
        float z0 = ((com.applovin.impl.sdk.ad.a) this.currentAd).z0();
        if (z0 <= Animation.CurveTimeline.LINEAR) {
            z0 = this.currentAd.i0();
        }
        double a2 = com.applovin.impl.sdk.utils.p.a(System.currentTimeMillis() - this.o);
        double d2 = (double) z0;
        Double.isNaN(d2);
        return (int) Math.min((a2 / d2) * 100.0d, 100.0d);
    }

    private boolean f() {
        return e() >= this.currentAd.t();
    }

    private boolean g() {
        return AppLovinAdType.INCENTIVIZED.equals(this.currentAd.getType());
    }

    private boolean h() {
        return !this.currentAd.hasVideoUrl() && g();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0066, code lost:
        if (r0 > 0) goto L_0x009a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0098, code lost:
        if (r0 > 0) goto L_0x009a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x009a, code lost:
        r0 = java.util.concurrent.TimeUnit.SECONDS.toMillis((long) r0);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void i() {
        /*
            r7 = this;
            com.applovin.impl.sdk.ad.f r0 = r7.currentAd
            if (r0 == 0) goto L_0x00e6
            com.applovin.impl.sdk.ad.f r0 = r7.currentAd
            long r0 = r0.J()
            r2 = 0
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 >= 0) goto L_0x0018
            com.applovin.impl.sdk.ad.f r0 = r7.currentAd
            int r0 = r0.K()
            if (r0 < 0) goto L_0x00e6
        L_0x0018:
            com.applovin.impl.sdk.utils.n r0 = r7.N
            if (r0 != 0) goto L_0x00e6
            com.applovin.impl.sdk.ad.f r0 = r7.currentAd
            long r0 = r0.J()
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 < 0) goto L_0x002e
            com.applovin.impl.sdk.ad.f r0 = r7.currentAd
            long r0 = r0.J()
            goto L_0x00b6
        L_0x002e:
            boolean r0 = r7.isVastAd()
            if (r0 == 0) goto L_0x0069
            com.applovin.impl.sdk.ad.f r0 = r7.currentAd
            e.c.a.a.a r0 = (e.c.a.a.a) r0
            e.c.a.a.j r1 = r0.D0()
            if (r1 == 0) goto L_0x0051
            int r4 = r1.b()
            if (r4 <= 0) goto L_0x0051
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.SECONDS
            int r1 = r1.b()
            long r5 = (long) r1
            long r4 = r4.toMillis(r5)
            long r2 = r2 + r4
            goto L_0x005b
        L_0x0051:
            com.applovin.impl.adview.t r1 = r7.videoView
            int r1 = r1.getDuration()
            if (r1 <= 0) goto L_0x005b
            long r4 = (long) r1
            long r2 = r2 + r4
        L_0x005b:
            boolean r1 = r0.L()
            if (r1 == 0) goto L_0x00a2
            float r0 = r0.i0()
            int r0 = (int) r0
            if (r0 <= 0) goto L_0x00a2
            goto L_0x009a
        L_0x0069:
            com.applovin.impl.sdk.ad.f r0 = r7.currentAd
            boolean r0 = r0 instanceof com.applovin.impl.sdk.ad.a
            if (r0 == 0) goto L_0x00a2
            com.applovin.impl.sdk.ad.f r0 = r7.currentAd
            com.applovin.impl.sdk.ad.a r0 = (com.applovin.impl.sdk.ad.a) r0
            com.applovin.impl.adview.t r1 = r7.videoView
            int r1 = r1.getDuration()
            if (r1 <= 0) goto L_0x007d
            long r4 = (long) r1
            long r2 = r2 + r4
        L_0x007d:
            boolean r1 = r0.L()
            if (r1 == 0) goto L_0x00a2
            float r1 = r0.z0()
            int r1 = (int) r1
            if (r1 <= 0) goto L_0x0093
            java.util.concurrent.TimeUnit r0 = java.util.concurrent.TimeUnit.SECONDS
            long r4 = (long) r1
            long r0 = r0.toMillis(r4)
        L_0x0091:
            long r2 = r2 + r0
            goto L_0x00a2
        L_0x0093:
            float r0 = r0.i0()
            int r0 = (int) r0
            if (r0 <= 0) goto L_0x00a2
        L_0x009a:
            java.util.concurrent.TimeUnit r1 = java.util.concurrent.TimeUnit.SECONDS
            long r4 = (long) r0
            long r0 = r1.toMillis(r4)
            goto L_0x0091
        L_0x00a2:
            double r0 = (double) r2
            com.applovin.impl.sdk.ad.f r2 = r7.currentAd
            int r2 = r2.K()
            double r2 = (double) r2
            r4 = 4636737291354636288(0x4059000000000000, double:100.0)
            java.lang.Double.isNaN(r2)
            double r2 = r2 / r4
            java.lang.Double.isNaN(r0)
            double r0 = r0 * r2
            long r0 = (long) r0
        L_0x00b6:
            com.applovin.impl.sdk.q r2 = r7.logger
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Scheduling report reward in "
            r3.append(r4)
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.MILLISECONDS
            long r4 = r4.toSeconds(r0)
            r3.append(r4)
            java.lang.String r4 = " seconds..."
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            java.lang.String r4 = "InterActivity"
            r2.b(r4, r3)
            com.applovin.impl.sdk.k r2 = r7.sdk
            com.applovin.impl.adview.m$h r3 = new com.applovin.impl.adview.m$h
            r3.<init>()
            com.applovin.impl.sdk.utils.n r0 = com.applovin.impl.sdk.utils.n.a(r0, r2, r3)
            r7.N = r0
        L_0x00e6:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.adview.m.i():void");
    }

    private void j() {
        View view;
        String str;
        StringBuilder sb;
        com.applovin.impl.sdk.q qVar;
        l lVar = this.f3337a;
        if (lVar != null) {
            lVar.setAdDisplayListener(new i());
            this.f3337a.setAdClickListener(new j());
            this.currentAd = (com.applovin.impl.sdk.ad.f) this.f3338b.b();
            if (this.w.compareAndSet(false, true)) {
                this.sdk.S().trackImpression(this.currentAd);
                this.currentAd.setHasShown(true);
            }
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
            this.A = new FrameLayout(this);
            this.A.setLayoutParams(layoutParams);
            this.A.setBackgroundColor(this.currentAd.c());
            this.countdownManager = new i(this.y, this.sdk);
            z();
            if (this.currentAd.isVideoAd()) {
                this.l = this.currentAd.d0();
                if (this.l) {
                    qVar = this.logger;
                    sb = new StringBuilder();
                    str = "Preparing stream for ";
                } else {
                    qVar = this.logger;
                    sb = new StringBuilder();
                    str = "Preparing cached video playback for ";
                }
                sb.append(str);
                sb.append(this.currentAd.e0());
                qVar.b("InterActivity", sb.toString());
                com.applovin.impl.sdk.d.d dVar = this.f3339c;
                if (dVar != null) {
                    dVar.b(this.l ? 1 : 0);
                }
            }
            this.videoMuted = y();
            Uri e0 = this.currentAd.e0();
            a(e0);
            if (e0 == null) {
                i();
            }
            this.B.bringToFront();
            if (D() && (view = this.C) != null) {
                view.bringToFront();
            }
            h hVar = this.D;
            if (hVar != null) {
                hVar.bringToFront();
            }
            this.f3337a.renderAd(this.currentAd);
            this.f3338b.a(true);
            if (!this.currentAd.hasVideoUrl()) {
                if (h() && ((Boolean) this.sdk.a(c.e.U0)).booleanValue()) {
                    d(this.currentAd);
                }
                showPoststitial();
                return;
            }
            return;
        }
        exitWithError("AdView was null");
    }

    private void k() {
        if (this.videoView != null) {
            this.t = getVideoPercentViewed();
            this.videoView.stopPlayback();
        }
    }

    private boolean l() {
        return this.videoMuted;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.k.a(com.applovin.impl.sdk.c$g, java.lang.Object):void
     arg types: [com.applovin.impl.sdk.c$g<java.lang.Boolean>, boolean]
     candidates:
      com.applovin.impl.sdk.k.a(java.lang.String, com.applovin.impl.sdk.c$e):com.applovin.impl.sdk.c$e<ST>
      com.applovin.impl.sdk.k.a(com.applovin.impl.sdk.c$g, java.lang.Object):void */
    private void m() {
        t tVar = this.videoView;
        this.sdk.a(c.g.v, Integer.valueOf(tVar != null ? tVar.getCurrentPosition() : 0));
        this.sdk.a((c.g) c.g.w, (Object) true);
        try {
            this.countdownManager.c();
        } catch (Throwable th) {
            this.logger.b("InterActivity", "Unable to pause countdown timers", th);
        }
        this.videoView.pause();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    private void n() {
        long max = Math.max(0L, ((Long) this.sdk.a(c.e.h2)).longValue());
        if (max > 0) {
            com.applovin.impl.sdk.q Z = this.sdk.Z();
            Z.b("InterActivity", "Resuming video with delay of " + max);
            this.z.postDelayed(new l(), max);
            return;
        }
        this.sdk.Z().b("InterActivity", "Resuming video immediately");
        o();
    }

    /* access modifiers changed from: private */
    public void o() {
        t tVar;
        if (!this.poststitialWasDisplayed && (tVar = this.videoView) != null && !tVar.isPlaying()) {
            this.videoView.seekTo(((Integer) this.sdk.b(c.g.v, Integer.valueOf(this.videoView.getDuration()))).intValue());
            this.videoView.start();
            this.countdownManager.a();
        }
    }

    private void p() {
        if (!this.f3344h) {
            try {
                int videoPercentViewed = getVideoPercentViewed();
                if (this.currentAd.hasVideoUrl()) {
                    a(this.currentAd, (double) videoPercentViewed, isFullyWatched());
                    if (this.f3339c != null) {
                        this.f3339c.c((long) videoPercentViewed);
                    }
                } else if ((this.currentAd instanceof com.applovin.impl.sdk.ad.a) && h() && ((Boolean) this.sdk.a(c.e.U0)).booleanValue()) {
                    int e2 = e();
                    com.applovin.impl.sdk.q qVar = this.logger;
                    qVar.b("InterActivity", "Rewarded playable engaged at " + e2 + " percent");
                    a(this.currentAd, (double) e2, e2 >= this.currentAd.t());
                }
                this.sdk.S().trackVideoEnd(this.currentAd, TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - this.o), videoPercentViewed, this.l);
                this.sdk.S().trackFullScreenAdClosed(this.currentAd, SystemClock.elapsedRealtime() - this.q, this.s);
            } catch (Throwable th) {
                com.applovin.impl.sdk.q qVar2 = this.logger;
                if (qVar2 != null) {
                    qVar2.b("InterActivity", "Failed to notify end listener.", th);
                }
            }
        }
    }

    private boolean q() {
        int identifier = getResources().getIdentifier((String) this.sdk.a(c.e.T1), "bool", "android");
        return identifier > 0 && getResources().getBoolean(identifier);
    }

    /* access modifiers changed from: private */
    @TargetApi(16)
    public void r() {
        getWindow().getDecorView().setSystemUiVisibility(5894);
    }

    private boolean s() {
        com.applovin.impl.sdk.k kVar;
        if (this.f3338b == null || (kVar = this.sdk) == null || ((Boolean) kVar.a(c.e.L1)).booleanValue()) {
            return true;
        }
        if (!((Boolean) this.sdk.a(c.e.M1)).booleanValue() || !this.f3345i) {
            return ((Boolean) this.sdk.a(c.e.N1)).booleanValue() && this.poststitialWasDisplayed;
        }
        return true;
    }

    @SuppressLint({"WrongConstant"})
    private void t() {
        int i2;
        if (this.sdk == null || !isFinishing()) {
            if (!(this.currentAd == null || (i2 = this.u) == Integer.MIN_VALUE)) {
                b(i2);
            }
            finish();
        }
    }

    private void u() {
        f fVar;
        if (((Boolean) this.sdk.a(c.e.U1)).booleanValue() && (fVar = this.F) != null && fVar.getVisibility() != 8) {
            a(this.F, this.F.getVisibility() == 4, 750);
        }
    }

    private void v() {
        u uVar;
        s o0 = this.currentAd.o0();
        if (o0 != null && o0.e() && !this.poststitialWasDisplayed && (uVar = this.J) != null) {
            a(this.J, uVar.getVisibility() == 4, o0.f());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.k.a(com.applovin.impl.sdk.c$g, java.lang.Object):void
     arg types: [com.applovin.impl.sdk.c$g<java.lang.Boolean>, boolean]
     candidates:
      com.applovin.impl.sdk.k.a(java.lang.String, com.applovin.impl.sdk.c$e):com.applovin.impl.sdk.c$e<ST>
      com.applovin.impl.sdk.k.a(com.applovin.impl.sdk.c$g, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.k.a(com.applovin.impl.sdk.c$g, java.lang.Object):void
     arg types: [com.applovin.impl.sdk.c$g<java.lang.Integer>, int]
     candidates:
      com.applovin.impl.sdk.k.a(java.lang.String, com.applovin.impl.sdk.c$e):com.applovin.impl.sdk.c$e<ST>
      com.applovin.impl.sdk.k.a(com.applovin.impl.sdk.c$g, java.lang.Object):void */
    private void w() {
        com.applovin.impl.sdk.k kVar = this.sdk;
        if (kVar != null) {
            kVar.a((c.g) c.g.w, (Object) false);
            this.sdk.a((c.g) c.g.v, (Object) 0);
        }
    }

    /* access modifiers changed from: private */
    public void x() {
        this.f3340d = true;
        showPoststitial();
    }

    /* access modifiers changed from: private */
    public boolean y() {
        return ((Integer) this.sdk.b(c.g.v, 0)).intValue() > 0 ? this.videoMuted : ((Boolean) this.sdk.a(c.e.Z1)).booleanValue() ? this.sdk.P().isMuted() : ((Boolean) this.sdk.a(c.e.X1)).booleanValue();
    }

    private void z() {
        this.B = h.a(this.sdk, this, this.currentAd.j0());
        this.B.setVisibility(8);
        this.B.setOnClickListener(new u());
        int a2 = a(this.currentAd.z());
        int i2 = 3;
        int i3 = (this.currentAd.F() ? 3 : 5) | 48;
        if (!this.currentAd.G()) {
            i2 = 5;
        }
        int i4 = i2 | 48;
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(a2, a2, i3 | 48);
        this.B.a(a2);
        int a3 = a(this.currentAd.D());
        int a4 = a(this.currentAd.E());
        layoutParams.setMargins(a4, a3, a4, a3);
        this.A.addView(this.B, layoutParams);
        this.D = h.a(this.sdk, this, this.currentAd.k0());
        this.D.setVisibility(8);
        this.D.setOnClickListener(new v());
        FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(a2, a2, i4);
        layoutParams2.setMargins(a4, a3, a4, a3);
        this.D.a(a2);
        this.A.addView(this.D, layoutParams2);
        this.D.bringToFront();
        if (D()) {
            int a5 = a(((Integer) this.sdk.a(c.e.a1)).intValue());
            this.C = new View(this);
            this.C.setBackgroundColor(0);
            this.C.setVisibility(8);
            this.E = new View(this);
            this.E.setBackgroundColor(0);
            this.E.setVisibility(8);
            int i5 = a2 + a5;
            int a6 = a3 - a(5);
            FrameLayout.LayoutParams layoutParams3 = new FrameLayout.LayoutParams(i5, i5, i3);
            layoutParams3.setMargins(a6, a6, a6, a6);
            FrameLayout.LayoutParams layoutParams4 = new FrameLayout.LayoutParams(i5, i5, i4);
            layoutParams4.setMargins(a6, a6, a6, a6);
            this.C.setOnClickListener(new w());
            this.E.setOnClickListener(new x());
            this.A.addView(this.C, layoutParams3);
            this.C.bringToFront();
            this.A.addView(this.E, layoutParams4);
            this.E.bringToFront();
        }
    }

    public void clickThroughFromVideo(PointF pointF) {
        try {
            if (this.currentAd.N() && this.f3347k) {
                E();
            }
            this.sdk.S().trackAndLaunchVideoClick(this.currentAd, this.f3337a, this.currentAd.f0(), pointF);
            com.applovin.impl.sdk.utils.j.a(this.f3338b.e(), this.currentAd);
            if (this.f3339c != null) {
                this.f3339c.b();
            }
        } catch (Throwable th) {
            this.sdk.Z().b("InterActivity", "Encountered error while clicking through video.", th);
        }
    }

    public void continueVideo() {
        o();
    }

    public void dismiss() {
        long currentTimeMillis = System.currentTimeMillis() - this.o;
        com.applovin.impl.sdk.q.f("InterActivity", "Dismissing ad after " + currentTimeMillis + " milliseconds elapsed");
        w();
        p();
        if (this.f3338b != null) {
            if (this.currentAd != null) {
                c(this.currentAd);
                com.applovin.impl.sdk.d.d dVar = this.f3339c;
                if (dVar != null) {
                    dVar.c();
                    this.f3339c = null;
                }
            }
            this.f3338b.a(false);
            this.f3338b.g();
        }
        lastKnownWrapper = null;
        t();
    }

    public void exitWithError(String str) {
        a(str);
        try {
            com.applovin.impl.sdk.q.c("InterActivity", "Failed to properly render an Interstitial Activity, due to error: " + str, new Throwable("Initialized = " + n.l + "; CleanedUp = " + n.m));
            c(new com.applovin.impl.sdk.ad.h());
        } catch (Exception e2) {
            com.applovin.impl.sdk.q.c("InterActivity", "Failed to show a video ad due to error:", e2);
        }
        dismiss();
    }

    public boolean getPoststitialWasDisplayed() {
        return this.poststitialWasDisplayed;
    }

    public int getVideoPercentViewed() {
        if (this.f3340d) {
            return 100;
        }
        t tVar = this.videoView;
        if (tVar != null) {
            int duration = tVar.getDuration();
            if (duration <= 0) {
                return this.t;
            }
            double currentPosition = (double) this.videoView.getCurrentPosition();
            double d2 = (double) duration;
            Double.isNaN(currentPosition);
            Double.isNaN(d2);
            return (int) ((currentPosition / d2) * 100.0d);
        }
        this.logger.e("InterActivity", "No video view detected on video end");
        return 0;
    }

    public void handleMediaError(String str) {
        this.logger.e("InterActivity", str);
        if (this.v.compareAndSet(false, true) && this.currentAd.h()) {
            a(str);
            dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public boolean isFullyWatched() {
        return getVideoPercentViewed() >= this.currentAd.t();
    }

    /* access modifiers changed from: protected */
    public boolean isVastAd() {
        return this.currentAd instanceof e.c.a.a.a;
    }

    public void onBackPressed() {
        h hVar;
        if (this.currentAd != null) {
            if (this.currentAd.Z() && !this.poststitialWasDisplayed) {
                return;
            }
            if (this.currentAd.a0() && this.poststitialWasDisplayed) {
                return;
            }
        }
        if (s()) {
            this.logger.b("InterActivity", "Back button was pressed; forwarding to Android for handling...");
        } else {
            try {
                if (!this.poststitialWasDisplayed && this.f3347k && this.D != null && this.D.getVisibility() == 0 && this.D.getAlpha() > Animation.CurveTimeline.LINEAR) {
                    this.logger.b("InterActivity", "Back button was pressed; forwarding as a click to skip button.");
                    hVar = this.D;
                } else if (this.B == null || this.B.getVisibility() != 0 || this.B.getAlpha() <= Animation.CurveTimeline.LINEAR) {
                    this.logger.b("InterActivity", "Back button was pressed, but was not eligible for dismissal.");
                    b("javascript:al_onBackPressed();");
                    return;
                } else {
                    this.logger.b("InterActivity", "Back button was pressed; forwarding as a click to close button.");
                    hVar = this.B;
                }
                hVar.performClick();
                b("javascript:al_onBackPressed();");
                return;
            } catch (Exception unused) {
            }
        }
        super.onBackPressed();
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (configuration.orientation != 0 && (this.videoView instanceof p) && this.H.get() != null) {
            MediaPlayer mediaPlayer = this.H.get();
            this.videoView.setVideoSize(mediaPlayer.getVideoWidth(), mediaPlayer.getVideoHeight());
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0183  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r8) {
        /*
            r7 = this;
            java.lang.String r0 = "InterActivity"
            super.onCreate(r8)
            if (r8 == 0) goto L_0x0012
            java.lang.String r1 = "instance_impression_tracked"
            boolean r1 = r8.getBoolean(r1)
            java.util.concurrent.atomic.AtomicBoolean r2 = r7.w
            r2.set(r1)
        L_0x0012:
            r1 = 1
            r7.requestWindowFeature(r1)
            android.os.StrictMode$ThreadPolicy r1 = android.os.StrictMode.allowThreadDiskReads()
            android.content.Intent r2 = r7.getIntent()     // Catch:{ all -> 0x0168 }
            java.lang.String r3 = "com.applovin.interstitial.wrapper_id"
            java.lang.String r2 = r2.getStringExtra(r3)     // Catch:{ all -> 0x0168 }
            if (r2 == 0) goto L_0x0165
            boolean r3 = r2.isEmpty()     // Catch:{ all -> 0x0168 }
            if (r3 != 0) goto L_0x0165
            com.applovin.impl.adview.n r2 = com.applovin.impl.adview.n.a(r2)     // Catch:{ all -> 0x0168 }
            r7.f3338b = r2     // Catch:{ all -> 0x0168 }
            com.applovin.impl.adview.n r2 = r7.f3338b     // Catch:{ all -> 0x0168 }
            if (r2 != 0) goto L_0x003e
            com.applovin.impl.adview.n r2 = com.applovin.impl.adview.m.lastKnownWrapper     // Catch:{ all -> 0x0168 }
            if (r2 == 0) goto L_0x003e
            com.applovin.impl.adview.n r2 = com.applovin.impl.adview.m.lastKnownWrapper     // Catch:{ all -> 0x0168 }
            r7.f3338b = r2     // Catch:{ all -> 0x0168 }
        L_0x003e:
            com.applovin.impl.adview.n r2 = r7.f3338b     // Catch:{ all -> 0x0168 }
            if (r2 == 0) goto L_0x0151
            com.applovin.impl.adview.n r2 = r7.f3338b     // Catch:{ all -> 0x0168 }
            com.applovin.sdk.AppLovinAd r2 = r2.b()     // Catch:{ all -> 0x0168 }
            com.applovin.impl.adview.n r3 = r7.f3338b     // Catch:{ all -> 0x0168 }
            com.applovin.impl.sdk.k r3 = r3.a()     // Catch:{ all -> 0x0168 }
            r7.sdk = r3     // Catch:{ all -> 0x0168 }
            com.applovin.impl.adview.n r3 = r7.f3338b     // Catch:{ all -> 0x0168 }
            com.applovin.impl.sdk.k r3 = r3.a()     // Catch:{ all -> 0x0168 }
            com.applovin.impl.sdk.q r3 = r3.Z()     // Catch:{ all -> 0x0168 }
            r7.logger = r3     // Catch:{ all -> 0x0168 }
            if (r2 == 0) goto L_0x014b
            com.applovin.impl.sdk.ad.f r2 = (com.applovin.impl.sdk.ad.f) r2     // Catch:{ all -> 0x0168 }
            com.applovin.impl.sdk.d.d r3 = new com.applovin.impl.sdk.d.d     // Catch:{ all -> 0x0168 }
            com.applovin.impl.sdk.k r4 = r7.sdk     // Catch:{ all -> 0x0168 }
            r3.<init>(r2, r4)     // Catch:{ all -> 0x0168 }
            r7.f3339c = r3     // Catch:{ all -> 0x0168 }
            r3 = 16908290(0x1020002, float:2.3877235E-38)
            android.view.View r3 = r7.findViewById(r3)     // Catch:{ all -> 0x0168 }
            if (r3 == 0) goto L_0x0085
            boolean r4 = r2.hasVideoUrl()     // Catch:{ all -> 0x0168 }
            if (r4 == 0) goto L_0x0080
            int r4 = r2.c()     // Catch:{ all -> 0x0168 }
        L_0x007c:
            r3.setBackgroundColor(r4)     // Catch:{ all -> 0x0168 }
            goto L_0x0085
        L_0x0080:
            int r4 = r2.d()     // Catch:{ all -> 0x0168 }
            goto L_0x007c
        L_0x0085:
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0168 }
            r7.o = r3     // Catch:{ all -> 0x0168 }
            boolean r3 = r2.q0()     // Catch:{ all -> 0x0168 }
            if (r3 == 0) goto L_0x009a
            android.view.Window r3 = r7.getWindow()     // Catch:{ all -> 0x0168 }
            r4 = 16777216(0x1000000, float:2.3509887E-38)
            r3.setFlags(r4, r4)     // Catch:{ all -> 0x0168 }
        L_0x009a:
            boolean r3 = r2.r0()     // Catch:{ all -> 0x0168 }
            if (r3 == 0) goto L_0x00a9
            android.view.Window r3 = r7.getWindow()     // Catch:{ all -> 0x0168 }
            r4 = 128(0x80, float:1.794E-43)
            r3.addFlags(r4)     // Catch:{ all -> 0x0168 }
        L_0x00a9:
            int r3 = com.applovin.impl.sdk.utils.p.g(r7)     // Catch:{ all -> 0x0168 }
            boolean r4 = com.applovin.sdk.AppLovinSdkUtils.isTablet(r7)     // Catch:{ all -> 0x0168 }
            int r5 = r7.a(r3, r4)     // Catch:{ all -> 0x0168 }
            if (r8 != 0) goto L_0x00ba
            r7.u = r5     // Catch:{ all -> 0x0168 }
            goto L_0x00c2
        L_0x00ba:
            java.lang.String r6 = "original_orientation"
            int r8 = r8.getInt(r6, r5)     // Catch:{ all -> 0x0168 }
            r7.u = r8     // Catch:{ all -> 0x0168 }
        L_0x00c2:
            boolean r8 = r2.u0()     // Catch:{ all -> 0x0168 }
            if (r8 == 0) goto L_0x00f0
            r8 = -1
            if (r5 == r8) goto L_0x00e5
            com.applovin.impl.sdk.q r8 = r7.logger     // Catch:{ all -> 0x0168 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0168 }
            r2.<init>()     // Catch:{ all -> 0x0168 }
            java.lang.String r3 = "Locking activity orientation to current orientation: "
            r2.append(r3)     // Catch:{ all -> 0x0168 }
            r2.append(r5)     // Catch:{ all -> 0x0168 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0168 }
            r8.b(r0, r2)     // Catch:{ all -> 0x0168 }
            r7.b(r5)     // Catch:{ all -> 0x0168 }
            goto L_0x00f8
        L_0x00e5:
            com.applovin.impl.sdk.q r8 = r7.logger     // Catch:{ all -> 0x0168 }
            java.lang.String r2 = "Unable to detect current orientation. Locking to targeted orientation..."
            r8.e(r0, r2)     // Catch:{ all -> 0x0168 }
        L_0x00ec:
            r7.b(r3, r4)     // Catch:{ all -> 0x0168 }
            goto L_0x00f8
        L_0x00f0:
            com.applovin.impl.sdk.q r8 = r7.logger     // Catch:{ all -> 0x0168 }
            java.lang.String r2 = "Locking activity orientation to targeted orientation..."
            r8.b(r0, r2)     // Catch:{ all -> 0x0168 }
            goto L_0x00ec
        L_0x00f8:
            com.applovin.impl.adview.l r8 = new com.applovin.impl.adview.l     // Catch:{ all -> 0x0168 }
            com.applovin.impl.sdk.k r2 = r7.sdk     // Catch:{ all -> 0x0168 }
            com.applovin.sdk.AppLovinSdk r2 = r2.r()     // Catch:{ all -> 0x0168 }
            com.applovin.sdk.AppLovinAdSize r3 = com.applovin.sdk.AppLovinAdSize.INTERSTITIAL     // Catch:{ all -> 0x0168 }
            r8.<init>(r2, r3, r7)     // Catch:{ all -> 0x0168 }
            r7.f3337a = r8     // Catch:{ all -> 0x0168 }
            com.applovin.impl.adview.l r8 = r7.f3337a     // Catch:{ all -> 0x0168 }
            r2 = 0
            r8.setAutoDestroy(r2)     // Catch:{ all -> 0x0168 }
            com.applovin.impl.adview.l r8 = r7.f3337a     // Catch:{ all -> 0x0168 }
            com.applovin.adview.AdViewController r8 = r8.getAdViewController()     // Catch:{ all -> 0x0168 }
            com.applovin.impl.adview.AdViewControllerImpl r8 = (com.applovin.impl.adview.AdViewControllerImpl) r8     // Catch:{ all -> 0x0168 }
            com.applovin.impl.sdk.d.d r2 = r7.f3339c     // Catch:{ all -> 0x0168 }
            r8.setStatsManagerHelper(r2)     // Catch:{ all -> 0x0168 }
            com.applovin.impl.adview.n r8 = r7.f3338b     // Catch:{ all -> 0x0168 }
            r8.a(r7)     // Catch:{ all -> 0x0168 }
            com.applovin.impl.sdk.k r8 = r7.sdk     // Catch:{ all -> 0x0168 }
            com.applovin.impl.sdk.c$e<java.lang.Boolean> r2 = com.applovin.impl.sdk.c.e.i2     // Catch:{ all -> 0x0168 }
            java.lang.Object r8 = r8.a(r2)     // Catch:{ all -> 0x0168 }
            java.lang.Boolean r8 = (java.lang.Boolean) r8     // Catch:{ all -> 0x0168 }
            boolean r8 = r8.booleanValue()     // Catch:{ all -> 0x0168 }
            r7.f3347k = r8     // Catch:{ all -> 0x0168 }
            com.applovin.impl.sdk.a.b r8 = new com.applovin.impl.sdk.a.b     // Catch:{ all -> 0x0168 }
            com.applovin.impl.sdk.k r2 = r7.sdk     // Catch:{ all -> 0x0168 }
            r8.<init>(r7, r2)     // Catch:{ all -> 0x0168 }
            r7.I = r8     // Catch:{ all -> 0x0168 }
            com.applovin.impl.adview.m$a r8 = new com.applovin.impl.adview.m$a     // Catch:{ all -> 0x0168 }
            r8.<init>()     // Catch:{ all -> 0x0168 }
            r7.O = r8     // Catch:{ all -> 0x0168 }
            com.applovin.impl.sdk.k r8 = r7.sdk     // Catch:{ all -> 0x0168 }
            com.applovin.impl.sdk.b r8 = r8.A()     // Catch:{ all -> 0x0168 }
            com.applovin.impl.sdk.utils.a r2 = r7.O     // Catch:{ all -> 0x0168 }
            r8.a(r2)     // Catch:{ all -> 0x0168 }
            goto L_0x0179
        L_0x014b:
            java.lang.String r8 = "No current ad found."
        L_0x014d:
            r7.exitWithError(r8)     // Catch:{ all -> 0x0168 }
            goto L_0x0179
        L_0x0151:
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x0168 }
            r8.<init>()     // Catch:{ all -> 0x0168 }
            java.lang.String r2 = "Wrapper is null; initialized state: "
            r8.append(r2)     // Catch:{ all -> 0x0168 }
            boolean r2 = com.applovin.impl.adview.n.l     // Catch:{ all -> 0x0168 }
            r8.append(r2)     // Catch:{ all -> 0x0168 }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x0168 }
            goto L_0x014d
        L_0x0165:
            java.lang.String r8 = "Wrapper ID is null"
            goto L_0x014d
        L_0x0168:
            r8 = move-exception
            com.applovin.impl.sdk.q r2 = r7.logger     // Catch:{ all -> 0x018a }
            if (r2 == 0) goto L_0x0174
            com.applovin.impl.sdk.q r2 = r7.logger     // Catch:{ all -> 0x018a }
            java.lang.String r3 = "Encountered error during onCreate."
            r2.b(r0, r3, r8)     // Catch:{ all -> 0x018a }
        L_0x0174:
            java.lang.String r8 = "An error was encountered during interstitial ad creation."
            r7.exitWithError(r8)     // Catch:{ all -> 0x018a }
        L_0x0179:
            android.os.StrictMode.setThreadPolicy(r1)
            r7.w()
            com.applovin.impl.sdk.d.d r8 = r7.f3339c
            if (r8 == 0) goto L_0x0186
            r8.a()
        L_0x0186:
            r7.j()
            return
        L_0x018a:
            r8 = move-exception
            android.os.StrictMode.setThreadPolicy(r1)
            goto L_0x0190
        L_0x018f:
            throw r8
        L_0x0190:
            goto L_0x018f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.adview.m.onCreate(android.os.Bundle):void");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0064, code lost:
        if (r4.currentAd != null) goto L_0x0079;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0077, code lost:
        if (r4.currentAd == null) goto L_0x0081;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0079, code lost:
        p();
        c(r4.currentAd);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0081, code lost:
        super.onDestroy();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0084, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onDestroy() {
        /*
            r4 = this;
            com.applovin.impl.adview.l r0 = r4.f3337a     // Catch:{ all -> 0x0067 }
            r1 = 0
            if (r0 == 0) goto L_0x001d
            com.applovin.impl.adview.l r0 = r4.f3337a     // Catch:{ all -> 0x0067 }
            android.view.ViewParent r0 = r0.getParent()     // Catch:{ all -> 0x0067 }
            boolean r2 = r0 instanceof android.view.ViewGroup     // Catch:{ all -> 0x0067 }
            if (r2 == 0) goto L_0x0016
            android.view.ViewGroup r0 = (android.view.ViewGroup) r0     // Catch:{ all -> 0x0067 }
            com.applovin.impl.adview.l r2 = r4.f3337a     // Catch:{ all -> 0x0067 }
            r0.removeView(r2)     // Catch:{ all -> 0x0067 }
        L_0x0016:
            com.applovin.impl.adview.l r0 = r4.f3337a     // Catch:{ all -> 0x0067 }
            r0.destroy()     // Catch:{ all -> 0x0067 }
            r4.f3337a = r1     // Catch:{ all -> 0x0067 }
        L_0x001d:
            com.applovin.impl.adview.t r0 = r4.videoView     // Catch:{ all -> 0x0067 }
            if (r0 == 0) goto L_0x002b
            com.applovin.impl.adview.t r0 = r4.videoView     // Catch:{ all -> 0x0067 }
            r0.pause()     // Catch:{ all -> 0x0067 }
            com.applovin.impl.adview.t r0 = r4.videoView     // Catch:{ all -> 0x0067 }
            r0.stopPlayback()     // Catch:{ all -> 0x0067 }
        L_0x002b:
            com.applovin.impl.sdk.k r0 = r4.sdk     // Catch:{ all -> 0x0067 }
            if (r0 == 0) goto L_0x0047
            java.lang.ref.WeakReference<android.media.MediaPlayer> r0 = r4.H     // Catch:{ all -> 0x0067 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x0067 }
            android.media.MediaPlayer r0 = (android.media.MediaPlayer) r0     // Catch:{ all -> 0x0067 }
            if (r0 == 0) goto L_0x003c
            r0.release()     // Catch:{ all -> 0x0067 }
        L_0x003c:
            com.applovin.impl.sdk.k r0 = r4.sdk     // Catch:{ all -> 0x0067 }
            com.applovin.impl.sdk.b r0 = r0.A()     // Catch:{ all -> 0x0067 }
            com.applovin.impl.sdk.utils.a r2 = r4.O     // Catch:{ all -> 0x0067 }
            r0.b(r2)     // Catch:{ all -> 0x0067 }
        L_0x0047:
            com.applovin.impl.adview.i r0 = r4.countdownManager     // Catch:{ all -> 0x0067 }
            if (r0 == 0) goto L_0x0050
            com.applovin.impl.adview.i r0 = r4.countdownManager     // Catch:{ all -> 0x0067 }
            r0.b()     // Catch:{ all -> 0x0067 }
        L_0x0050:
            android.os.Handler r0 = r4.z     // Catch:{ all -> 0x0067 }
            if (r0 == 0) goto L_0x0059
            android.os.Handler r0 = r4.z     // Catch:{ all -> 0x0067 }
            r0.removeCallbacksAndMessages(r1)     // Catch:{ all -> 0x0067 }
        L_0x0059:
            android.os.Handler r0 = r4.y     // Catch:{ all -> 0x0067 }
            if (r0 == 0) goto L_0x0062
            android.os.Handler r0 = r4.y     // Catch:{ all -> 0x0067 }
            r0.removeCallbacksAndMessages(r1)     // Catch:{ all -> 0x0067 }
        L_0x0062:
            com.applovin.impl.sdk.ad.f r0 = r4.currentAd
            if (r0 == 0) goto L_0x0081
            goto L_0x0079
        L_0x0067:
            r0 = move-exception
            com.applovin.impl.sdk.q r1 = r4.logger     // Catch:{ all -> 0x0085 }
            if (r1 == 0) goto L_0x0075
            com.applovin.impl.sdk.q r1 = r4.logger     // Catch:{ all -> 0x0085 }
            java.lang.String r2 = "InterActivity"
            java.lang.String r3 = "Unable to destroy video view"
            r1.a(r2, r3, r0)     // Catch:{ all -> 0x0085 }
        L_0x0075:
            com.applovin.impl.sdk.ad.f r0 = r4.currentAd
            if (r0 == 0) goto L_0x0081
        L_0x0079:
            r4.p()
            com.applovin.impl.sdk.ad.f r0 = r4.currentAd
            r4.c(r0)
        L_0x0081:
            super.onDestroy()
            return
        L_0x0085:
            r0 = move-exception
            com.applovin.impl.sdk.ad.f r1 = r4.currentAd
            if (r1 == 0) goto L_0x0092
            r4.p()
            com.applovin.impl.sdk.ad.f r1 = r4.currentAd
            r4.c(r1)
        L_0x0092:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.adview.m.onDestroy():void");
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if ((i2 == 25 || i2 == 24) && this.currentAd.x() && l()) {
            toggleMute();
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.logger.b("InterActivity", "App paused...");
        this.p = System.currentTimeMillis();
        if (this.poststitialWasDisplayed) {
            m();
        }
        this.f3338b.a(false);
        this.I.a();
        pauseReportRewardTask();
        b("javascript:al_onAppPaused();");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0073, code lost:
        if (r0 != null) goto L_0x00ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00ac, code lost:
        if (r1 == false) goto L_0x00ae;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onResume() {
        /*
            r6 = this;
            super.onResume()
            com.applovin.impl.sdk.q r0 = r6.logger
            java.lang.String r1 = "InterActivity"
            java.lang.String r2 = "App resumed..."
            r0.b(r1, r2)
            com.applovin.impl.adview.n r0 = r6.f3338b
            r1 = 1
            r0.a(r1)
            boolean r0 = r6.m
            if (r0 != 0) goto L_0x00b5
            com.applovin.impl.sdk.d.d r0 = r6.f3339c
            if (r0 == 0) goto L_0x0024
            long r2 = java.lang.System.currentTimeMillis()
            long r4 = r6.p
            long r2 = r2 - r4
            r0.d(r2)
        L_0x0024:
            com.applovin.impl.sdk.k r0 = r6.sdk
            com.applovin.impl.sdk.c$g<java.lang.Boolean> r2 = com.applovin.impl.sdk.c.g.w
            r3 = 0
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r3)
            java.lang.Object r0 = r0.b(r2, r4)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            r4 = 0
            if (r0 == 0) goto L_0x0076
            com.applovin.impl.sdk.a.b r0 = r6.I
            boolean r0 = r0.d()
            if (r0 != 0) goto L_0x0076
            boolean r0 = r6.poststitialWasDisplayed
            if (r0 != 0) goto L_0x0076
            r6.n()
            r6.O()
            com.applovin.impl.sdk.ad.f r0 = r6.currentAd
            if (r0 == 0) goto L_0x00b1
            com.applovin.impl.sdk.k r0 = r6.sdk
            com.applovin.impl.sdk.c$e<java.lang.Boolean> r1 = com.applovin.impl.sdk.c.e.E1
            java.lang.Object r0 = r0.a(r1)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x00b1
            com.applovin.impl.sdk.ad.f r0 = r6.currentAd
            boolean r0 = r0.t0()
            if (r0 != 0) goto L_0x00b1
            boolean r0 = r6.poststitialWasDisplayed
            if (r0 != 0) goto L_0x00b1
            boolean r0 = r6.f3347k
            if (r0 == 0) goto L_0x00b1
            com.applovin.impl.adview.h r0 = r6.D
            if (r0 == 0) goto L_0x00b1
            goto L_0x00ae
        L_0x0076:
            com.applovin.impl.sdk.ad.f r0 = r6.currentAd
            boolean r0 = r0 instanceof com.applovin.impl.sdk.ad.a
            if (r0 == 0) goto L_0x0087
            com.applovin.impl.sdk.ad.f r0 = r6.currentAd
            com.applovin.impl.sdk.ad.a r0 = (com.applovin.impl.sdk.ad.a) r0
            boolean r0 = r0.A0()
            if (r0 == 0) goto L_0x0087
            goto L_0x0088
        L_0x0087:
            r1 = 0
        L_0x0088:
            com.applovin.impl.sdk.ad.f r0 = r6.currentAd
            if (r0 == 0) goto L_0x00b1
            com.applovin.impl.sdk.k r0 = r6.sdk
            com.applovin.impl.sdk.c$e<java.lang.Boolean> r2 = com.applovin.impl.sdk.c.e.E1
            java.lang.Object r0 = r0.a(r2)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x00b1
            com.applovin.impl.sdk.ad.f r0 = r6.currentAd
            boolean r0 = r0.s0()
            if (r0 != 0) goto L_0x00b1
            boolean r0 = r6.poststitialWasDisplayed
            if (r0 == 0) goto L_0x00b1
            com.applovin.impl.adview.h r0 = r6.B
            if (r0 == 0) goto L_0x00b1
            if (r1 != 0) goto L_0x00b1
        L_0x00ae:
            r6.a(r4, r0)
        L_0x00b1:
            r6.resumeReportRewardTask()
            goto L_0x00d0
        L_0x00b5:
            com.applovin.impl.sdk.a.b r0 = r6.I
            boolean r0 = r0.d()
            if (r0 != 0) goto L_0x00d0
            boolean r0 = r6.poststitialWasDisplayed
            if (r0 != 0) goto L_0x00d0
            com.applovin.impl.sdk.ad.f r0 = r6.currentAd
            if (r0 == 0) goto L_0x00d0
            com.applovin.impl.sdk.ad.f r0 = r6.currentAd
            boolean r0 = r0.o()
            if (r0 == 0) goto L_0x00d0
            r6.O()
        L_0x00d0:
            java.lang.String r0 = "javascript:al_onAppResumed();"
            r6.b(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.adview.m.onResume():void");
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("instance_impression_tracked", this.w.get());
        bundle.putInt("original_orientation", this.u);
    }

    public void onWindowFocusChanged(boolean z2) {
        String str;
        super.onWindowFocusChanged(z2);
        com.applovin.impl.sdk.k kVar = this.sdk;
        if (z2) {
            if (kVar != null) {
                this.logger.b("InterActivity", "Window gained focus");
                try {
                    if (!com.applovin.impl.sdk.utils.g.e() || !((Boolean) this.sdk.a(c.e.d2)).booleanValue() || !q()) {
                        getWindow().setFlags(1024, 1024);
                    } else {
                        r();
                        if (((Long) this.sdk.a(c.e.O1)).longValue() > 0) {
                            this.z.postDelayed(new d(), ((Long) this.sdk.a(c.e.O1)).longValue());
                        }
                    }
                    if (((Boolean) this.sdk.a(c.e.P1)).booleanValue() && !this.poststitialWasDisplayed) {
                        n();
                        resumeReportRewardTask();
                    }
                } catch (Throwable th) {
                    this.logger.b("InterActivity", "Setting window flags failed.", th);
                }
                this.m = false;
                b("javascript:al_onWindowFocusChanged( " + z2 + " );");
            }
            str = "Window gained focus. SDK is null.";
        } else if (kVar != null) {
            this.logger.b("InterActivity", "Window lost focus");
            if (((Boolean) this.sdk.a(c.e.P1)).booleanValue() && !this.poststitialWasDisplayed) {
                m();
                pauseReportRewardTask();
            }
            this.m = false;
            b("javascript:al_onWindowFocusChanged( " + z2 + " );");
        } else {
            str = "Window lost focus. SDK is null.";
        }
        com.applovin.impl.sdk.q.f("InterActivity", str);
        this.m = false;
        b("javascript:al_onWindowFocusChanged( " + z2 + " );");
    }

    public void pauseReportRewardTask() {
        com.applovin.impl.sdk.utils.n nVar = this.N;
        if (nVar != null) {
            nVar.b();
        }
    }

    /* access modifiers changed from: protected */
    public void playVideo() {
        d(this.currentAd);
        this.videoView.start();
        this.countdownManager.a();
    }

    public void resumeReportRewardTask() {
        com.applovin.impl.sdk.utils.n nVar = this.N;
        if (nVar != null) {
            nVar.c();
        }
    }

    /* access modifiers changed from: protected */
    public boolean shouldContinueFullLengthVideoCountdown() {
        return !this.f3340d && !this.poststitialWasDisplayed;
    }

    public void showPoststitial() {
        long j2;
        h hVar;
        try {
            if (this.f3339c != null) {
                this.f3339c.g();
            }
            if (!this.currentAd.H()) {
                k();
            }
            if (this.f3337a != null) {
                ViewParent parent = this.f3337a.getParent();
                if (parent instanceof ViewGroup) {
                    ((ViewGroup) parent).removeView(this.f3337a);
                }
                FrameLayout frameLayout = new FrameLayout(this);
                frameLayout.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
                frameLayout.setBackgroundColor(this.currentAd.d());
                frameLayout.addView(this.f3337a);
                if (this.currentAd.H()) {
                    k();
                }
                if (this.A != null) {
                    this.A.removeAllViewsInLayout();
                }
                if (D() && this.C != null) {
                    if (this.C.getParent() instanceof ViewGroup) {
                        ((ViewGroup) this.C.getParent()).removeView(this.C);
                    }
                    frameLayout.addView(this.C);
                    this.C.bringToFront();
                }
                if (this.B != null) {
                    ViewParent parent2 = this.B.getParent();
                    if (parent2 instanceof ViewGroup) {
                        ((ViewGroup) parent2).removeView(this.B);
                    }
                    frameLayout.addView(this.B);
                    this.B.bringToFront();
                }
                setContentView(frameLayout);
                if (((Boolean) this.sdk.a(c.e.N3)).booleanValue()) {
                    this.f3337a.setVisibility(4);
                    this.f3337a.setVisibility(0);
                }
                int u2 = this.currentAd.u();
                if (u2 >= 0) {
                    this.z.postDelayed(new k(), (long) u2);
                }
            }
            if (!((this.currentAd instanceof com.applovin.impl.sdk.ad.a) && ((com.applovin.impl.sdk.ad.a) this.currentAd).A0())) {
                if (this.currentAd.i0() >= Animation.CurveTimeline.LINEAR) {
                    j2 = com.applovin.impl.sdk.utils.p.b(this.currentAd.i0());
                    hVar = this.B;
                } else if (this.currentAd.i0() == -2.0f) {
                    this.B.setVisibility(0);
                } else {
                    j2 = 0;
                    hVar = this.B;
                }
                a(j2, hVar);
            } else {
                this.logger.b("InterActivity", "Skip showing of close button");
            }
            this.poststitialWasDisplayed = true;
        } catch (Throwable th) {
            this.logger.b("InterActivity", "Encountered error while showing poststitial. Dismissing...", th);
            dismiss();
        }
    }

    public void skipVideo() {
        this.s = SystemClock.elapsedRealtime() - this.r;
        com.applovin.impl.sdk.d.d dVar = this.f3339c;
        if (dVar != null) {
            dVar.f();
        }
        if (this.currentAd.l0()) {
            dismiss();
        } else {
            showPoststitial();
        }
    }

    public void toggleMute() {
        boolean z2 = !l();
        com.applovin.impl.sdk.d.d dVar = this.f3339c;
        if (dVar != null) {
            dVar.i();
        }
        try {
            c(z2);
            a(z2);
            b(z2);
        } catch (Throwable th) {
            this.logger.b("InterActivity", "Unable to set volume to " + z2, th);
        }
    }
}
