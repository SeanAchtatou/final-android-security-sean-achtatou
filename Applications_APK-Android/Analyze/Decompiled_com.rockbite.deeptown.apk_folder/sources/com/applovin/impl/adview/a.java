package com.applovin.impl.adview;

import android.app.Activity;
import android.graphics.PorterDuff;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.applovin.sdk.AppLovinSdkUtils;

public class a extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private final ProgressBar f3254a;

    public a(Activity activity, int i2, int i3) {
        super(activity);
        RelativeLayout.LayoutParams layoutParams;
        setClickable(false);
        this.f3254a = new ProgressBar(activity, null, i3);
        this.f3254a.setIndeterminate(true);
        this.f3254a.setClickable(false);
        if (i2 == -2 || i2 == -1) {
            layoutParams = new RelativeLayout.LayoutParams(i2, i2);
        } else {
            int dpToPx = AppLovinSdkUtils.dpToPx(activity, i2);
            layoutParams = new RelativeLayout.LayoutParams(dpToPx, dpToPx);
        }
        layoutParams.addRule(13);
        this.f3254a.setLayoutParams(layoutParams);
        addView(this.f3254a);
    }

    public void a() {
        setVisibility(0);
    }

    public void b() {
        setVisibility(8);
    }

    public void setColor(int i2) {
        this.f3254a.getIndeterminateDrawable().setColorFilter(i2, PorterDuff.Mode.SRC_IN);
    }
}
