package com.applovin.impl.adview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import com.applovin.impl.adview.h;
import com.applovin.impl.sdk.k;

@SuppressLint({"ViewConstructor"})
public final class q extends h {

    /* renamed from: b  reason: collision with root package name */
    private static final Paint f3421b = new Paint(1);

    /* renamed from: c  reason: collision with root package name */
    private static final Paint f3422c = new Paint(1);

    /* renamed from: a  reason: collision with root package name */
    private float f3423a = 1.0f;

    public q(k kVar, Context context) {
        super(kVar, context);
        f3421b.setARGB(80, 0, 0, 0);
        f3422c.setColor(-1);
        f3422c.setStyle(Paint.Style.STROKE);
    }

    public void a(int i2) {
        setViewScale(((float) i2) / 30.0f);
    }

    /* access modifiers changed from: protected */
    public float getCenter() {
        return getSize() / 2.0f;
    }

    /* access modifiers changed from: protected */
    public float getCrossOffset() {
        return this.f3423a * 8.0f;
    }

    /* access modifiers changed from: protected */
    public float getInnerCircleOffset() {
        return this.f3423a * 2.0f;
    }

    /* access modifiers changed from: protected */
    public float getInnerCircleRadius() {
        return getCenter() - getInnerCircleOffset();
    }

    /* access modifiers changed from: protected */
    public float getSize() {
        return this.f3423a * 30.0f;
    }

    /* access modifiers changed from: protected */
    public float getStrokeWidth() {
        return this.f3423a * 2.0f;
    }

    public h.a getStyle() {
        return h.a.WhiteXOnTransparentGrey;
    }

    public float getViewScale() {
        return this.f3423a;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float center = getCenter();
        canvas.drawCircle(center, center, center, f3421b);
        float crossOffset = getCrossOffset();
        float size = getSize() - crossOffset;
        f3422c.setStrokeWidth(getStrokeWidth());
        Canvas canvas2 = canvas;
        float f2 = crossOffset;
        float f3 = size;
        canvas2.drawLine(f2, crossOffset, f3, size, f3422c);
        canvas2.drawLine(f2, size, f3, crossOffset, f3422c);
    }

    public void setViewScale(float f2) {
        this.f3423a = f2;
    }
}
