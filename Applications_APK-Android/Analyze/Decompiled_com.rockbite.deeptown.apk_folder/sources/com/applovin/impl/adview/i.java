package com.applovin.impl.adview;

import android.os.Handler;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.q;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public final class i {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final q f3313a;

    /* renamed from: b  reason: collision with root package name */
    private final Handler f3314b;

    /* renamed from: c  reason: collision with root package name */
    private final Set<c> f3315c = new HashSet();
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public final AtomicInteger f3316d = new AtomicInteger();

    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ c f3317a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ int f3318b;

        a(c cVar, int i2) {
            this.f3317a = cVar;
            this.f3318b = i2;
        }

        public void run() {
            b b2 = this.f3317a.c();
            if (!b2.b()) {
                q b3 = i.this.f3313a;
                b3.b("CountdownManager", "Ending countdown for " + this.f3317a.a());
            } else if (i.this.f3316d.get() == this.f3318b) {
                try {
                    b2.a();
                } catch (Throwable th) {
                    q b4 = i.this.f3313a;
                    b4.b("CountdownManager", "Encountered error on countdown step for: " + this.f3317a.a(), th);
                }
                i.this.a(this.f3317a, this.f3318b);
            } else {
                q b5 = i.this.f3313a;
                b5.d("CountdownManager", "Killing duplicate countdown from previous generation: " + this.f3317a.a());
            }
        }
    }

    interface b {
        void a();

        boolean b();
    }

    private static class c {

        /* renamed from: a  reason: collision with root package name */
        private final String f3320a;

        /* renamed from: b  reason: collision with root package name */
        private final b f3321b;

        /* renamed from: c  reason: collision with root package name */
        private final long f3322c;

        private c(String str, long j2, b bVar) {
            this.f3320a = str;
            this.f3322c = j2;
            this.f3321b = bVar;
        }

        /* synthetic */ c(String str, long j2, b bVar, a aVar) {
            this(str, j2, bVar);
        }

        /* access modifiers changed from: private */
        public String a() {
            return this.f3320a;
        }

        /* access modifiers changed from: private */
        public long b() {
            return this.f3322c;
        }

        /* access modifiers changed from: private */
        public b c() {
            return this.f3321b;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof c)) {
                return false;
            }
            String str = this.f3320a;
            String str2 = ((c) obj).f3320a;
            return str != null ? str.equalsIgnoreCase(str2) : str2 == null;
        }

        public int hashCode() {
            String str = this.f3320a;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return "CountdownProxy{identifier='" + this.f3320a + '\'' + ", countdownStepMillis=" + this.f3322c + '}';
        }
    }

    public i(Handler handler, k kVar) {
        if (handler == null) {
            throw new IllegalArgumentException("No handler specified.");
        } else if (kVar != null) {
            this.f3314b = handler;
            this.f3313a = kVar.Z();
        } else {
            throw new IllegalArgumentException("No sdk specified.");
        }
    }

    /* access modifiers changed from: private */
    public void a(c cVar, int i2) {
        this.f3314b.postDelayed(new a(cVar, i2), cVar.b());
    }

    public void a() {
        HashSet<c> hashSet = new HashSet<>(this.f3315c);
        q qVar = this.f3313a;
        qVar.b("CountdownManager", "Starting " + hashSet.size() + " countdowns...");
        int incrementAndGet = this.f3316d.incrementAndGet();
        for (c cVar : hashSet) {
            q qVar2 = this.f3313a;
            qVar2.b("CountdownManager", "Starting countdown: " + cVar.a() + " for generation " + incrementAndGet + "...");
            a(cVar, incrementAndGet);
        }
    }

    public void a(String str, long j2, b bVar) {
        if (j2 <= 0) {
            throw new IllegalArgumentException("Invalid step specified.");
        } else if (this.f3314b != null) {
            q qVar = this.f3313a;
            qVar.b("CountdownManager", "Adding countdown: " + str);
            this.f3315c.add(new c(str, j2, bVar, null));
        } else {
            throw new IllegalArgumentException("No handler specified.");
        }
    }

    public void b() {
        this.f3313a.b("CountdownManager", "Removing all countdowns...");
        c();
        this.f3315c.clear();
    }

    public void c() {
        this.f3313a.b("CountdownManager", "Stopping countdowns...");
        this.f3316d.incrementAndGet();
        this.f3314b.removeCallbacksAndMessages(null);
    }
}
