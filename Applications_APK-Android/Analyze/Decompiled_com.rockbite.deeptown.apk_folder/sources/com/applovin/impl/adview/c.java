package com.applovin.impl.adview;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Rect;
import android.net.Uri;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.applovin.impl.sdk.c;
import com.applovin.sdk.AppLovinAd;
import com.appsflyer.share.Constants;
import e.c.a.a.e;

class c extends g {
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final com.applovin.impl.sdk.q f3256b;

    /* renamed from: c  reason: collision with root package name */
    private final com.applovin.impl.sdk.k f3257c;

    /* renamed from: d  reason: collision with root package name */
    private com.applovin.impl.sdk.d.d f3258d;

    /* renamed from: e  reason: collision with root package name */
    private AppLovinAd f3259e = null;

    /* renamed from: f  reason: collision with root package name */
    private boolean f3260f = false;

    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ WebSettings f3261a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Integer f3262b;

        a(c cVar, WebSettings webSettings, Integer num) {
            this.f3261a = webSettings;
            this.f3262b = num;
        }

        @TargetApi(21)
        public void run() {
            this.f3261a.setMixedContentMode(this.f3262b.intValue());
        }
    }

    class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ WebSettings f3263a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Boolean f3264b;

        b(c cVar, WebSettings webSettings, Boolean bool) {
            this.f3263a = webSettings;
            this.f3264b = bool;
        }

        @TargetApi(23)
        public void run() {
            this.f3263a.setOffscreenPreRaster(this.f3264b.booleanValue());
        }
    }

    /* renamed from: com.applovin.impl.adview.c$c  reason: collision with other inner class name */
    class C0070c implements View.OnLongClickListener {
        C0070c() {
        }

        public boolean onLongClick(View view) {
            c.this.f3256b.b("AdWebView", "Received a LongClick event.");
            return true;
        }
    }

    class d implements Runnable {
        d() {
        }

        public void run() {
            c.this.loadUrl("about:blank");
        }
    }

    class e implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ com.applovin.impl.sdk.ad.f f3267a;

        e(com.applovin.impl.sdk.ad.f fVar) {
            this.f3267a = fVar;
        }

        @TargetApi(17)
        public void run() {
            c.this.getSettings().setMediaPlaybackRequiresUserGesture(this.f3267a.R());
        }
    }

    class f implements Runnable {
        f(c cVar) {
        }

        @TargetApi(19)
        public void run() {
            WebView.setWebContentsDebuggingEnabled(true);
        }
    }

    class g implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ WebSettings f3269a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ WebSettings.PluginState f3270b;

        g(c cVar, WebSettings webSettings, WebSettings.PluginState pluginState) {
            this.f3269a = webSettings;
            this.f3270b = pluginState;
        }

        public void run() {
            this.f3269a.setPluginState(this.f3270b);
        }
    }

    class h implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ WebSettings f3271a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Boolean f3272b;

        h(c cVar, WebSettings webSettings, Boolean bool) {
            this.f3271a = webSettings;
            this.f3272b = bool;
        }

        public void run() {
            this.f3271a.setAllowFileAccess(this.f3272b.booleanValue());
        }
    }

    class i implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ WebSettings f3273a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Boolean f3274b;

        i(c cVar, WebSettings webSettings, Boolean bool) {
            this.f3273a = webSettings;
            this.f3274b = bool;
        }

        public void run() {
            this.f3273a.setLoadWithOverviewMode(this.f3274b.booleanValue());
        }
    }

    class j implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ WebSettings f3275a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Boolean f3276b;

        j(c cVar, WebSettings webSettings, Boolean bool) {
            this.f3275a = webSettings;
            this.f3276b = bool;
        }

        public void run() {
            this.f3275a.setUseWideViewPort(this.f3276b.booleanValue());
        }
    }

    class k implements View.OnTouchListener {
        k(c cVar) {
        }

        @SuppressLint({"ClickableViewAccessibility"})
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (view.hasFocus()) {
                return false;
            }
            view.requestFocus();
            return false;
        }
    }

    class l implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ WebSettings f3277a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Boolean f3278b;

        l(c cVar, WebSettings webSettings, Boolean bool) {
            this.f3277a = webSettings;
            this.f3278b = bool;
        }

        public void run() {
            this.f3277a.setAllowContentAccess(this.f3278b.booleanValue());
        }
    }

    class m implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ WebSettings f3279a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Boolean f3280b;

        m(c cVar, WebSettings webSettings, Boolean bool) {
            this.f3279a = webSettings;
            this.f3280b = bool;
        }

        public void run() {
            this.f3279a.setBuiltInZoomControls(this.f3280b.booleanValue());
        }
    }

    class n implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ WebSettings f3281a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Boolean f3282b;

        n(c cVar, WebSettings webSettings, Boolean bool) {
            this.f3281a = webSettings;
            this.f3282b = bool;
        }

        public void run() {
            this.f3281a.setDisplayZoomControls(this.f3282b.booleanValue());
        }
    }

    class o implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ WebSettings f3283a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Boolean f3284b;

        o(c cVar, WebSettings webSettings, Boolean bool) {
            this.f3283a = webSettings;
            this.f3284b = bool;
        }

        public void run() {
            this.f3283a.setSaveFormData(this.f3284b.booleanValue());
        }
    }

    class p implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ WebSettings f3285a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Boolean f3286b;

        p(c cVar, WebSettings webSettings, Boolean bool) {
            this.f3285a = webSettings;
            this.f3286b = bool;
        }

        public void run() {
            this.f3285a.setGeolocationEnabled(this.f3286b.booleanValue());
        }
    }

    class q implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ WebSettings f3287a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Boolean f3288b;

        q(c cVar, WebSettings webSettings, Boolean bool) {
            this.f3287a = webSettings;
            this.f3288b = bool;
        }

        public void run() {
            this.f3287a.setNeedInitialFocus(this.f3288b.booleanValue());
        }
    }

    class r implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ WebSettings f3289a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Boolean f3290b;

        r(c cVar, WebSettings webSettings, Boolean bool) {
            this.f3289a = webSettings;
            this.f3290b = bool;
        }

        @TargetApi(16)
        public void run() {
            this.f3289a.setAllowFileAccessFromFileURLs(this.f3290b.booleanValue());
        }
    }

    class s implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ WebSettings f3291a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Boolean f3292b;

        s(c cVar, WebSettings webSettings, Boolean bool) {
            this.f3291a = webSettings;
            this.f3292b = bool;
        }

        @TargetApi(16)
        public void run() {
            this.f3291a.setAllowUniversalAccessFromFileURLs(this.f3292b.booleanValue());
        }
    }

    c(d dVar, com.applovin.impl.sdk.k kVar, Context context) {
        super(context);
        if (kVar != null) {
            this.f3257c = kVar;
            this.f3256b = kVar.Z();
            setBackgroundColor(0);
            WebSettings settings = getSettings();
            settings.setSupportMultipleWindows(false);
            settings.setJavaScriptEnabled(true);
            setWebViewClient(dVar);
            setWebChromeClient(new b(kVar));
            setVerticalScrollBarEnabled(false);
            setHorizontalScrollBarEnabled(false);
            setScrollBarStyle(33554432);
            if (com.applovin.impl.sdk.utils.g.i()) {
                setWebViewRenderProcessClient(new e(kVar));
            }
            setOnTouchListener(new k(this));
            setOnLongClickListener(new C0070c());
            return;
        }
        throw new IllegalArgumentException("No sdk specified.");
    }

    private String a(String str, String str2) {
        if (com.applovin.impl.sdk.utils.m.b(str)) {
            return com.applovin.impl.sdk.utils.p.b(str).replace("{SOURCE}", str2);
        }
        return null;
    }

    private void a(com.applovin.impl.sdk.ad.f fVar) {
        Boolean n2;
        Integer a2;
        try {
            if (((Boolean) this.f3257c.a(c.e.M3)).booleanValue() || fVar.S()) {
                a(new d());
            }
            if (com.applovin.impl.sdk.utils.g.d()) {
                a(new e(fVar));
            }
            if (com.applovin.impl.sdk.utils.g.e() && fVar.U()) {
                a(new f(this));
            }
            w V = fVar.V();
            if (V != null) {
                WebSettings settings = getSettings();
                WebSettings.PluginState b2 = V.b();
                if (b2 != null) {
                    a(new g(this, settings, b2));
                }
                Boolean c2 = V.c();
                if (c2 != null) {
                    a(new h(this, settings, c2));
                }
                Boolean d2 = V.d();
                if (d2 != null) {
                    a(new i(this, settings, d2));
                }
                Boolean e2 = V.e();
                if (e2 != null) {
                    a(new j(this, settings, e2));
                }
                Boolean f2 = V.f();
                if (f2 != null) {
                    a(new l(this, settings, f2));
                }
                Boolean g2 = V.g();
                if (g2 != null) {
                    a(new m(this, settings, g2));
                }
                Boolean h2 = V.h();
                if (h2 != null) {
                    a(new n(this, settings, h2));
                }
                Boolean i2 = V.i();
                if (i2 != null) {
                    a(new o(this, settings, i2));
                }
                Boolean j2 = V.j();
                if (j2 != null) {
                    a(new p(this, settings, j2));
                }
                Boolean k2 = V.k();
                if (k2 != null) {
                    a(new q(this, settings, k2));
                }
                if (com.applovin.impl.sdk.utils.g.c()) {
                    Boolean l2 = V.l();
                    if (l2 != null) {
                        a(new r(this, settings, l2));
                    }
                    Boolean m2 = V.m();
                    if (m2 != null) {
                        a(new s(this, settings, m2));
                    }
                }
                if (com.applovin.impl.sdk.utils.g.f() && (a2 = V.a()) != null) {
                    a(new a(this, settings, a2));
                }
                if (com.applovin.impl.sdk.utils.g.g() && (n2 = V.n()) != null) {
                    a(new b(this, settings, n2));
                }
            }
        } catch (Throwable th) {
            this.f3256b.b("AdWebView", "Unable to apply WebView settings", th);
        }
    }

    private void a(Runnable runnable) {
        try {
            runnable.run();
        } catch (Throwable th) {
            this.f3256b.b("AdWebView", "Unable to apply WebView setting", th);
        }
    }

    private void a(String str, String str2, String str3, com.applovin.impl.sdk.k kVar) {
        String a2 = a(str3, str);
        if (com.applovin.impl.sdk.utils.m.b(a2)) {
            com.applovin.impl.sdk.q qVar = this.f3256b;
            qVar.b("AdWebView", "Rendering webview for VAST ad with resourceContents : " + a2);
            loadDataWithBaseURL(str2, a2, "text/html", null, "");
            return;
        }
        String a3 = a((String) kVar.a(c.e.w3), str);
        if (com.applovin.impl.sdk.utils.m.b(a3)) {
            com.applovin.impl.sdk.q qVar2 = this.f3256b;
            qVar2.b("AdWebView", "Rendering webview for VAST ad with resourceContents : " + a3);
            loadDataWithBaseURL(str2, a3, "text/html", null, "");
            return;
        }
        com.applovin.impl.sdk.q qVar3 = this.f3256b;
        qVar3.b("AdWebView", "Rendering webview for VAST ad with resourceURL : " + str);
        loadUrl(str);
    }

    /* access modifiers changed from: package-private */
    public AppLovinAd a() {
        return this.f3259e;
    }

    public void a(com.applovin.impl.sdk.d.d dVar) {
        this.f3258d = dVar;
    }

    public void a(AppLovinAd appLovinAd) {
        com.applovin.impl.sdk.q qVar;
        String str;
        com.applovin.impl.sdk.q qVar2;
        String str2;
        String str3;
        String T;
        String str4;
        String str5;
        String str6;
        String T2;
        com.applovin.impl.sdk.k kVar;
        if (!this.f3260f) {
            this.f3259e = appLovinAd;
            try {
                if (appLovinAd instanceof com.applovin.impl.sdk.ad.h) {
                    loadDataWithBaseURL(Constants.URL_PATH_DELIMITER, ((com.applovin.impl.sdk.ad.h) appLovinAd).a(), "text/html", null, "");
                    qVar = this.f3256b;
                    str = "Empty ad rendered";
                } else {
                    com.applovin.impl.sdk.ad.f fVar = (com.applovin.impl.sdk.ad.f) appLovinAd;
                    a(fVar);
                    if (fVar.I()) {
                        setVisibility(0);
                    }
                    if (appLovinAd instanceof com.applovin.impl.sdk.ad.a) {
                        loadDataWithBaseURL(fVar.T(), com.applovin.impl.sdk.utils.p.b(((com.applovin.impl.sdk.ad.a) appLovinAd).v0()), "text/html", null, "");
                        qVar = this.f3256b;
                        str = "AppLovinAd rendered";
                    } else if (appLovinAd instanceof e.c.a.a.a) {
                        e.c.a.a.a aVar = (e.c.a.a.a) appLovinAd;
                        e.c.a.a.b F0 = aVar.F0();
                        if (F0 != null) {
                            e.c.a.a.e b2 = F0.b();
                            Uri b3 = b2.b();
                            String uri = b3 != null ? b3.toString() : "";
                            String c2 = b2.c();
                            String w0 = aVar.w0();
                            if (!com.applovin.impl.sdk.utils.m.b(uri)) {
                                if (!com.applovin.impl.sdk.utils.m.b(c2)) {
                                    qVar2 = this.f3256b;
                                    str2 = "Unable to load companion ad. No resources provided.";
                                    qVar2.e("AdWebView", str2);
                                    return;
                                }
                            }
                            if (b2.a() == e.a.STATIC) {
                                this.f3256b.b("AdWebView", "Rendering WebView for static VAST ad");
                                loadDataWithBaseURL(fVar.T(), a((String) this.f3257c.a(c.e.v3), uri), "text/html", null, "");
                                return;
                            }
                            if (b2.a() == e.a.HTML) {
                                if (com.applovin.impl.sdk.utils.m.b(c2)) {
                                    String a2 = a(w0, c2);
                                    str3 = com.applovin.impl.sdk.utils.m.b(a2) ? a2 : c2;
                                    com.applovin.impl.sdk.q qVar3 = this.f3256b;
                                    qVar3.b("AdWebView", "Rendering WebView for HTML VAST ad with resourceContents: " + str3);
                                    T = fVar.T();
                                    str4 = "text/html";
                                    str5 = null;
                                    str6 = "";
                                } else if (com.applovin.impl.sdk.utils.m.b(uri)) {
                                    this.f3256b.b("AdWebView", "Preparing to load HTML VAST ad resourceUri");
                                    T2 = fVar.T();
                                    kVar = this.f3257c;
                                    a(uri, T2, w0, kVar);
                                    return;
                                } else {
                                    return;
                                }
                            } else if (b2.a() != e.a.IFRAME) {
                                qVar2 = this.f3256b;
                                str2 = "Failed to render VAST companion ad of invalid type";
                                qVar2.e("AdWebView", str2);
                                return;
                            } else if (com.applovin.impl.sdk.utils.m.b(uri)) {
                                this.f3256b.b("AdWebView", "Preparing to load iFrame VAST ad resourceUri");
                                T2 = fVar.T();
                                kVar = this.f3257c;
                                a(uri, T2, w0, kVar);
                                return;
                            } else if (com.applovin.impl.sdk.utils.m.b(c2)) {
                                String a3 = a(w0, c2);
                                str3 = com.applovin.impl.sdk.utils.m.b(a3) ? a3 : c2;
                                com.applovin.impl.sdk.q qVar4 = this.f3256b;
                                qVar4.b("AdWebView", "Rendering WebView for iFrame VAST ad with resourceContents: " + str3);
                                T = fVar.T();
                                str4 = "text/html";
                                str5 = null;
                                str6 = "";
                            } else {
                                return;
                            }
                            loadDataWithBaseURL(T, str3, str4, str5, str6);
                            return;
                        }
                        qVar = this.f3256b;
                        str = "No companion ad provided.";
                    } else {
                        return;
                    }
                }
                qVar.b("AdWebView", str);
            } catch (Throwable th) {
                this.f3256b.b("AdWebView", "Unable to render AppLovinAd", th);
            }
        } else {
            com.applovin.impl.sdk.q.i("AdWebView", "Ad can not be loaded in a destroyed webview");
        }
    }

    public void a(String str) {
        a(str, (Runnable) null);
    }

    public void a(String str, Runnable runnable) {
        try {
            com.applovin.impl.sdk.q qVar = this.f3256b;
            qVar.b("AdWebView", "Forwarding \"" + str + "\" to ad template");
            loadUrl(str);
        } catch (Throwable th) {
            this.f3256b.b("AdWebView", "Unable to forward to template", th);
            if (runnable != null) {
                runnable.run();
            }
        }
    }

    public com.applovin.impl.sdk.d.d b() {
        return this.f3258d;
    }

    public void computeScroll() {
    }

    public void destroy() {
        this.f3260f = true;
        try {
            super.destroy();
            this.f3256b.b("AdWebView", "Web view destroyed");
        } catch (Throwable th) {
            com.applovin.impl.sdk.q qVar = this.f3256b;
            if (qVar != null) {
                qVar.b("AdWebView", "destroy() threw exception", th);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onFocusChanged(boolean z, int i2, Rect rect) {
        try {
            super.onFocusChanged(z, i2, rect);
        } catch (Exception e2) {
            this.f3256b.b("AdWebView", "onFocusChanged() threw exception", e2);
        }
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int i2, int i3, int i4, int i5) {
    }

    public void onWindowFocusChanged(boolean z) {
        try {
            super.onWindowFocusChanged(z);
        } catch (Exception e2) {
            this.f3256b.b("AdWebView", "onWindowFocusChanged() threw exception", e2);
        }
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        try {
            super.onWindowVisibilityChanged(i2);
        } catch (Exception e2) {
            this.f3256b.b("AdWebView", "onWindowVisibilityChanged() threw exception", e2);
        }
    }

    public boolean requestFocus(int i2, Rect rect) {
        try {
            return super.requestFocus(i2, rect);
        } catch (Exception e2) {
            this.f3256b.b("AdWebView", "requestFocus() threw exception", e2);
            return false;
        }
    }

    public void scrollTo(int i2, int i3) {
    }
}
