package com.applovin.impl.adview;

import android.annotation.TargetApi;
import android.webkit.WebSettings;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.m;
import com.tapjoy.TJAdUnitConstants;
import org.json.JSONObject;

public final class w {

    /* renamed from: a  reason: collision with root package name */
    private k f3437a;

    /* renamed from: b  reason: collision with root package name */
    private JSONObject f3438b;

    public w(JSONObject jSONObject, k kVar) {
        this.f3437a = kVar;
        this.f3438b = jSONObject;
    }

    /* access modifiers changed from: package-private */
    @TargetApi(21)
    public Integer a() {
        int i2;
        String b2 = i.b(this.f3438b, "mixed_content_mode", (String) null, this.f3437a);
        if (m.b(b2)) {
            if ("always_allow".equalsIgnoreCase(b2)) {
                i2 = 0;
            } else if ("never_allow".equalsIgnoreCase(b2)) {
                i2 = 1;
            } else if ("compatibility_mode".equalsIgnoreCase(b2)) {
                i2 = 2;
            }
            return Integer.valueOf(i2);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public WebSettings.PluginState b() {
        String b2 = i.b(this.f3438b, "plugin_state", (String) null, this.f3437a);
        if (m.b(b2)) {
            if (TJAdUnitConstants.String.SPLIT_VIEW_TRIGGER_ON.equalsIgnoreCase(b2)) {
                return WebSettings.PluginState.ON;
            }
            if ("on_demand".equalsIgnoreCase(b2)) {
                return WebSettings.PluginState.ON_DEMAND;
            }
            if ("off".equalsIgnoreCase(b2)) {
                return WebSettings.PluginState.OFF;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public Boolean c() {
        return i.a(this.f3438b, "allow_file_access", (Boolean) null, this.f3437a);
    }

    /* access modifiers changed from: package-private */
    public Boolean d() {
        return i.a(this.f3438b, "load_with_overview_mode", (Boolean) null, this.f3437a);
    }

    /* access modifiers changed from: package-private */
    public Boolean e() {
        return i.a(this.f3438b, "use_wide_view_port", (Boolean) null, this.f3437a);
    }

    /* access modifiers changed from: package-private */
    public Boolean f() {
        return i.a(this.f3438b, "allow_content_access", (Boolean) null, this.f3437a);
    }

    /* access modifiers changed from: package-private */
    public Boolean g() {
        return i.a(this.f3438b, "use_built_in_zoom_controls", (Boolean) null, this.f3437a);
    }

    /* access modifiers changed from: package-private */
    public Boolean h() {
        return i.a(this.f3438b, "display_zoom_controls", (Boolean) null, this.f3437a);
    }

    /* access modifiers changed from: package-private */
    public Boolean i() {
        return i.a(this.f3438b, "save_form_data", (Boolean) null, this.f3437a);
    }

    /* access modifiers changed from: package-private */
    public Boolean j() {
        return i.a(this.f3438b, "geolocation_enabled", (Boolean) null, this.f3437a);
    }

    /* access modifiers changed from: package-private */
    public Boolean k() {
        return i.a(this.f3438b, "need_initial_focus", (Boolean) null, this.f3437a);
    }

    /* access modifiers changed from: package-private */
    public Boolean l() {
        return i.a(this.f3438b, "allow_file_access_from_file_urls", (Boolean) null, this.f3437a);
    }

    /* access modifiers changed from: package-private */
    public Boolean m() {
        return i.a(this.f3438b, "allow_universal_access_from_file_urls", (Boolean) null, this.f3437a);
    }

    /* access modifiers changed from: package-private */
    public Boolean n() {
        return i.a(this.f3438b, "offscreen_pre_raster", (Boolean) null, this.f3437a);
    }
}
