package com.applovin.impl.adview;

import android.content.Context;
import android.view.View;
import com.applovin.impl.sdk.k;

public abstract class h extends View {

    public enum a {
        WhiteXOnOpaqueBlack(0),
        WhiteXOnTransparentGrey(1),
        Invisible(2);
        

        /* renamed from: a  reason: collision with root package name */
        private final int f3312a;

        private a(int i2) {
            this.f3312a = i2;
        }

        public int a() {
            return this.f3312a;
        }
    }

    h(k kVar, Context context) {
        super(context);
    }

    public static h a(k kVar, Context context, a aVar) {
        return aVar.equals(a.Invisible) ? new o(kVar, context) : aVar.equals(a.WhiteXOnTransparentGrey) ? new q(kVar, context) : new x(kVar, context);
    }

    public abstract void a(int i2);

    public abstract a getStyle();

    public abstract float getViewScale();

    public abstract void setViewScale(float f2);
}
