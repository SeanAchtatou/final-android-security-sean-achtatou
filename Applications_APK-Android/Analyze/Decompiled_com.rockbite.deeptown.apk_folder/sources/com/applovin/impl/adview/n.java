package com.applovin.impl.adview;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.StrictMode;
import com.applovin.adview.AppLovinInterstitialActivity;
import com.applovin.adview.AppLovinInterstitialAdDialog;
import com.applovin.impl.sdk.ad.f;
import com.applovin.impl.sdk.ad.i;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.q;
import com.applovin.impl.sdk.utils.p;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkUtils;
import com.google.android.gms.drive.DriveFile;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class n implements AppLovinInterstitialAdDialog {

    /* renamed from: k  reason: collision with root package name */
    private static final Map<String, n> f3391k = Collections.synchronizedMap(new HashMap());
    public static volatile boolean l = false;
    public static volatile boolean m = false;
    private static volatile boolean n;

    /* renamed from: a  reason: collision with root package name */
    private final String f3392a;

    /* renamed from: b  reason: collision with root package name */
    protected final k f3393b;

    /* renamed from: c  reason: collision with root package name */
    private final WeakReference<Context> f3394c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public volatile AppLovinAdLoadListener f3395d;

    /* renamed from: e  reason: collision with root package name */
    private volatile AppLovinAdDisplayListener f3396e;

    /* renamed from: f  reason: collision with root package name */
    private volatile AppLovinAdVideoPlaybackListener f3397f;

    /* renamed from: g  reason: collision with root package name */
    private volatile AppLovinAdClickListener f3398g;

    /* renamed from: h  reason: collision with root package name */
    private volatile f f3399h;

    /* renamed from: i  reason: collision with root package name */
    private volatile f.b f3400i;
    /* access modifiers changed from: private */

    /* renamed from: j  reason: collision with root package name */
    public volatile j f3401j;

    class a implements AppLovinAdLoadListener {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ String f3402a;

        a(String str) {
            this.f3402a = str;
        }

        public void adReceived(AppLovinAd appLovinAd) {
            n.this.b(appLovinAd);
            n.this.showAndRender(appLovinAd, this.f3402a);
        }

        public void failedToReceiveAd(int i2) {
            n.this.a(i2);
        }
    }

    class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Context f3404a;

        b(Context context) {
            this.f3404a = context;
        }

        public void run() {
            n.this.a(this.f3404a);
        }
    }

    class c implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AppLovinAd f3406a;

        c(AppLovinAd appLovinAd) {
            this.f3406a = appLovinAd;
        }

        public void run() {
            if (n.this.f3395d != null) {
                n.this.f3395d.adReceived(this.f3406a);
            }
        }
    }

    class d implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ int f3408a;

        d(int i2) {
            this.f3408a = i2;
        }

        public void run() {
            if (n.this.f3395d != null) {
                n.this.f3395d.failedToReceiveAd(this.f3408a);
            }
        }
    }

    class e implements Runnable {
        e() {
        }

        public void run() {
            if (n.this.f3401j != null) {
                n.this.f3401j.dismiss();
            }
        }
    }

    n(AppLovinSdk appLovinSdk, Context context) {
        if (appLovinSdk == null) {
            throw new IllegalArgumentException("No sdk specified");
        } else if (context != null) {
            this.f3393b = p.a(appLovinSdk);
            this.f3392a = UUID.randomUUID().toString();
            this.f3394c = new WeakReference<>(context);
            l = true;
            m = false;
        } else {
            throw new IllegalArgumentException("No context specified");
        }
    }

    public static n a(String str) {
        return f3391k.get(str);
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        AppLovinSdkUtils.runOnUiThread(new d(i2));
    }

    /* access modifiers changed from: private */
    public void a(Context context) {
        Intent intent = new Intent(context, AppLovinInterstitialActivity.class);
        intent.putExtra(m.KEY_WRAPPER_ID, this.f3392a);
        m.lastKnownWrapper = this;
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        if (context instanceof Activity) {
            try {
                context.startActivity(intent);
                ((Activity) context).overridePendingTransition(0, 0);
            } catch (Throwable th) {
                this.f3393b.Z().b("InterstitialAdDialogWrapper", "Unable to remove pending transition animations", th);
            }
        } else {
            intent.setFlags(DriveFile.MODE_READ_ONLY);
            context.startActivity(intent);
        }
        StrictMode.setThreadPolicy(allowThreadDiskReads);
        a(true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    private void a(f fVar, Context context) {
        f3391k.put(this.f3392a, this);
        this.f3399h = fVar;
        this.f3400i = this.f3399h != null ? this.f3399h.g0() : f.b.DEFAULT;
        long max = Math.max(0L, ((Long) this.f3393b.a(c.e.Q1)).longValue());
        q Z = this.f3393b.Z();
        Z.b("InterstitialAdDialogWrapper", "Presenting ad with delay of " + max);
        new Handler(context.getMainLooper()).postDelayed(new b(context), max);
    }

    private void a(AppLovinAd appLovinAd) {
        if (this.f3396e != null) {
            this.f3396e.adHidden(appLovinAd);
        }
        n = false;
    }

    /* access modifiers changed from: private */
    public void b(AppLovinAd appLovinAd) {
        AppLovinSdkUtils.runOnUiThread(new c(appLovinAd));
    }

    private Context h() {
        WeakReference<Context> weakReference = this.f3394c;
        if (weakReference != null) {
            return weakReference.get();
        }
        return null;
    }

    public k a() {
        return this.f3393b;
    }

    public void a(j jVar) {
        this.f3401j = jVar;
    }

    /* access modifiers changed from: protected */
    public void a(AppLovinAdLoadListener appLovinAdLoadListener) {
        this.f3393b.S().loadNextAd(AppLovinAdSize.INTERSTITIAL, appLovinAdLoadListener);
    }

    public void a(boolean z) {
        n = z;
    }

    public AppLovinAd b() {
        return this.f3399h;
    }

    public AppLovinAdVideoPlaybackListener c() {
        return this.f3397f;
    }

    public AppLovinAdDisplayListener d() {
        return this.f3396e;
    }

    public void dismiss() {
        AppLovinSdkUtils.runOnUiThread(new e());
    }

    public AppLovinAdClickListener e() {
        return this.f3398g;
    }

    public f.b f() {
        return this.f3400i;
    }

    public void g() {
        l = false;
        m = true;
        f3391k.remove(this.f3392a);
        if (this.f3399h != null && this.f3399h.s()) {
            this.f3401j = null;
        }
    }

    public boolean isAdReadyToDisplay() {
        return this.f3393b.S().hasPreloadedAd(AppLovinAdSize.INTERSTITIAL);
    }

    public boolean isShowing() {
        return n;
    }

    public void setAdClickListener(AppLovinAdClickListener appLovinAdClickListener) {
        this.f3398g = appLovinAdClickListener;
    }

    public void setAdDisplayListener(AppLovinAdDisplayListener appLovinAdDisplayListener) {
        this.f3396e = appLovinAdDisplayListener;
    }

    public void setAdLoadListener(AppLovinAdLoadListener appLovinAdLoadListener) {
        this.f3395d = appLovinAdLoadListener;
    }

    public void setAdVideoPlaybackListener(AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener) {
        this.f3397f = appLovinAdVideoPlaybackListener;
    }

    public void show() {
        show(null);
    }

    public void show(String str) {
        a(new a(str));
    }

    public void showAndRender(AppLovinAd appLovinAd) {
        showAndRender(appLovinAd, null);
    }

    public void showAndRender(AppLovinAd appLovinAd, String str) {
        q qVar;
        String str2;
        if (!isShowing() || ((Boolean) this.f3393b.a(c.e.O3)).booleanValue()) {
            Context h2 = h();
            if (h2 != null) {
                AppLovinAd a2 = p.a(appLovinAd, this.f3393b);
                if (a2 == null) {
                    qVar = this.f3393b.Z();
                    str2 = "Failed to show ad: " + appLovinAd;
                } else if (a2 instanceof f) {
                    a((f) a2, h2);
                    return;
                } else {
                    this.f3393b.Z().e("InterstitialAdDialogWrapper", "Failed to show interstitial: unknown ad type provided: '" + a2 + "'");
                    a(a2);
                    return;
                }
            } else {
                qVar = this.f3393b.Z();
                str2 = "Failed to show interstitial: stale activity reference provided";
            }
            qVar.e("InterstitialAdDialogWrapper", str2);
            a(appLovinAd);
            return;
        }
        q.i("AppLovinInterstitialAdDialog", "Attempted to show an interstitial while one is already displayed; ignoring.");
        if (this.f3396e instanceof i) {
            ((i) this.f3396e).onAdDisplayFailed("Attempted to show an interstitial while one is already displayed; ignoring.");
        }
    }

    public String toString() {
        return "AppLovinInterstitialAdDialog{}";
    }
}
