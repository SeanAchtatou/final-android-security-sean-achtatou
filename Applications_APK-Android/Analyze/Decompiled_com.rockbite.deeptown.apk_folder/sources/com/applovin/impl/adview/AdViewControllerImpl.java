package com.applovin.impl.adview;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PointF;
import android.net.Uri;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import com.applovin.adview.AdViewController;
import com.applovin.adview.AppLovinAdView;
import com.applovin.adview.AppLovinAdViewEventListener;
import com.applovin.impl.sdk.AppLovinAdServiceImpl;
import com.applovin.impl.sdk.ad.f;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.q;
import com.applovin.impl.sdk.utils.p;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkUtils;
import com.appsflyer.share.Constants;
import java.util.concurrent.atomic.AtomicReference;

public class AdViewControllerImpl implements AdViewController {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f3220a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public ViewGroup f3221b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public k f3222c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public AppLovinAdServiceImpl f3223d;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public q f3224e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public AppLovinAdSize f3225f;

    /* renamed from: g  reason: collision with root package name */
    private String f3226g;
    /* access modifiers changed from: private */

    /* renamed from: h  reason: collision with root package name */
    public com.applovin.impl.sdk.d.d f3227h;

    /* renamed from: i  reason: collision with root package name */
    private d f3228i;

    /* renamed from: j  reason: collision with root package name */
    private j f3229j;
    /* access modifiers changed from: private */

    /* renamed from: k  reason: collision with root package name */
    public c f3230k;
    private AppLovinAd l;
    private Runnable m;
    private Runnable n;
    /* access modifiers changed from: private */
    public volatile AppLovinAd o = null;
    private volatile AppLovinAd p = null;
    /* access modifiers changed from: private */
    public k q = null;
    /* access modifiers changed from: private */
    public k r = null;
    private final AtomicReference<AppLovinAd> s = new AtomicReference<>();
    private volatile boolean t = false;
    private volatile boolean u = true;
    /* access modifiers changed from: private */
    public volatile boolean v = false;
    /* access modifiers changed from: private */
    public volatile AppLovinAdLoadListener w;
    private volatile AppLovinAdDisplayListener x;
    /* access modifiers changed from: private */
    public volatile AppLovinAdViewEventListener y;
    private volatile AppLovinAdClickListener z;

    class a implements Runnable {
        a() {
        }

        public void run() {
            if (AdViewControllerImpl.this.q != null) {
                q b2 = AdViewControllerImpl.this.f3224e;
                b2.b("AppLovinAdView", "Detaching expanded ad: " + AdViewControllerImpl.this.q.a());
                AdViewControllerImpl adViewControllerImpl = AdViewControllerImpl.this;
                k unused = adViewControllerImpl.r = adViewControllerImpl.q;
                k unused2 = AdViewControllerImpl.this.q = (k) null;
                AdViewControllerImpl adViewControllerImpl2 = AdViewControllerImpl.this;
                adViewControllerImpl2.a(adViewControllerImpl2.f3225f);
            }
        }
    }

    class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ PointF f3232a;

        class a implements DialogInterface.OnDismissListener {
            a() {
            }

            public void onDismiss(DialogInterface dialogInterface) {
                AdViewControllerImpl.this.contractAd();
            }
        }

        b(PointF pointF) {
            this.f3232a = pointF;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.applovin.impl.sdk.utils.p.a(android.view.View, com.applovin.impl.sdk.k):android.app.Activity
         arg types: [com.applovin.impl.adview.c, com.applovin.impl.sdk.k]
         candidates:
          com.applovin.impl.sdk.utils.p.a(java.io.File, int):android.graphics.Bitmap
          com.applovin.impl.sdk.utils.p.a(android.content.Context, android.view.View):android.view.View
          com.applovin.impl.sdk.utils.p.a(org.json.JSONObject, com.applovin.impl.sdk.k):com.applovin.impl.sdk.ad.d
          com.applovin.impl.sdk.utils.p.a(com.applovin.sdk.AppLovinAd, com.applovin.impl.sdk.k):com.applovin.sdk.AppLovinAd
          com.applovin.impl.sdk.utils.p.a(java.lang.Object, com.applovin.impl.sdk.k):java.lang.Object
          com.applovin.impl.sdk.utils.p.a(java.lang.Class, java.lang.String):java.lang.reflect.Field
          com.applovin.impl.sdk.utils.p.a(java.util.List<java.lang.String>, com.applovin.impl.sdk.k):java.util.List<java.lang.Class>
          com.applovin.impl.sdk.utils.p.a(java.io.Closeable, com.applovin.impl.sdk.k):void
          com.applovin.impl.sdk.utils.p.a(java.net.HttpURLConnection, com.applovin.impl.sdk.k):void
          com.applovin.impl.sdk.utils.p.a(long, long):boolean
          com.applovin.impl.sdk.utils.p.a(android.view.View, android.app.Activity):boolean
          com.applovin.impl.sdk.utils.p.a(android.view.View, android.view.View):boolean
          com.applovin.impl.sdk.utils.p.a(java.lang.String, java.util.List<java.lang.String>):boolean
          com.applovin.impl.sdk.utils.p.a(android.view.View, com.applovin.impl.sdk.k):android.app.Activity */
        public void run() {
            if (AdViewControllerImpl.this.q == null && (AdViewControllerImpl.this.o instanceof com.applovin.impl.sdk.ad.a) && AdViewControllerImpl.this.f3230k != null) {
                com.applovin.impl.sdk.ad.a aVar = (com.applovin.impl.sdk.ad.a) AdViewControllerImpl.this.o;
                Activity a2 = AdViewControllerImpl.this.f3220a instanceof Activity ? (Activity) AdViewControllerImpl.this.f3220a : p.a((View) AdViewControllerImpl.this.f3230k, AdViewControllerImpl.this.f3222c);
                if (a2 != null) {
                    if (AdViewControllerImpl.this.f3221b != null) {
                        AdViewControllerImpl.this.f3221b.removeView(AdViewControllerImpl.this.f3230k);
                    }
                    AdViewControllerImpl adViewControllerImpl = AdViewControllerImpl.this;
                    k unused = adViewControllerImpl.q = new k(aVar, adViewControllerImpl.f3230k, a2, AdViewControllerImpl.this.f3222c);
                    AdViewControllerImpl.this.q.setOnDismissListener(new a());
                    AdViewControllerImpl.this.q.show();
                    com.applovin.impl.sdk.utils.j.a(AdViewControllerImpl.this.y, AdViewControllerImpl.this.o, (AppLovinAdView) AdViewControllerImpl.this.f3221b);
                    if (AdViewControllerImpl.this.f3227h != null) {
                        AdViewControllerImpl.this.f3227h.d();
                        return;
                    }
                    return;
                }
                q.i("AppLovinAdView", "Unable to expand ad. No Activity found.");
                Uri y0 = aVar.y0();
                if (y0 != null && ((Boolean) AdViewControllerImpl.this.f3222c.a(c.e.u1)).booleanValue()) {
                    AdViewControllerImpl.this.f3223d.trackAndLaunchClick(aVar, AdViewControllerImpl.this.getParentView(), AdViewControllerImpl.this, y0, this.f3232a);
                    if (AdViewControllerImpl.this.f3227h != null) {
                        AdViewControllerImpl.this.f3227h.b();
                    }
                }
                AdViewControllerImpl.this.f3230k.a("javascript:al_onFailedExpand();");
            }
        }
    }

    class c implements Runnable {
        c() {
        }

        public void run() {
            AdViewControllerImpl.this.d();
            if (AdViewControllerImpl.this.f3221b != null && AdViewControllerImpl.this.f3230k != null && AdViewControllerImpl.this.f3230k.getParent() == null) {
                AdViewControllerImpl.this.f3221b.addView(AdViewControllerImpl.this.f3230k);
                AdViewControllerImpl.b(AdViewControllerImpl.this.f3230k, AdViewControllerImpl.this.o.getSize());
            }
        }
    }

    class d implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AppLovinAd f3236a;

        d(AppLovinAd appLovinAd) {
            this.f3236a = appLovinAd;
        }

        public void run() {
            try {
                if (AdViewControllerImpl.this.w != null) {
                    AdViewControllerImpl.this.w.adReceived(this.f3236a);
                }
            } catch (Throwable th) {
                q.i("AppLovinAdView", "Exception while running ad load callback: " + th.getMessage());
            }
        }
    }

    class e implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ int f3238a;

        e(int i2) {
            this.f3238a = i2;
        }

        public void run() {
            try {
                if (AdViewControllerImpl.this.w != null) {
                    AdViewControllerImpl.this.w.failedToReceiveAd(this.f3238a);
                }
            } catch (Throwable th) {
                q.c("AppLovinAdView", "Exception while running app load  callback", th);
            }
        }
    }

    class f implements Runnable {
        f() {
        }

        public void run() {
            com.applovin.impl.sdk.ad.a aVar;
            if (AdViewControllerImpl.this.r != null || AdViewControllerImpl.this.q != null) {
                if (AdViewControllerImpl.this.r != null) {
                    aVar = AdViewControllerImpl.this.r.a();
                    AdViewControllerImpl.this.r.dismiss();
                    k unused = AdViewControllerImpl.this.r = (k) null;
                } else {
                    aVar = AdViewControllerImpl.this.q.a();
                    AdViewControllerImpl.this.q.dismiss();
                    k unused2 = AdViewControllerImpl.this.q = (k) null;
                }
                com.applovin.impl.sdk.utils.j.b(AdViewControllerImpl.this.y, aVar, (AppLovinAdView) AdViewControllerImpl.this.f3221b);
            }
        }
    }

    private class g implements Runnable {
        private g() {
        }

        /* synthetic */ g(AdViewControllerImpl adViewControllerImpl, a aVar) {
            this();
        }

        public void run() {
            if (AdViewControllerImpl.this.f3230k != null) {
                AdViewControllerImpl.this.f3230k.setVisibility(8);
            }
        }
    }

    private class h implements Runnable {
        private h() {
        }

        /* synthetic */ h(AdViewControllerImpl adViewControllerImpl, a aVar) {
            this();
        }

        public void run() {
            if (AdViewControllerImpl.this.f3230k != null) {
                try {
                    AdViewControllerImpl.this.f3230k.loadDataWithBaseURL(Constants.URL_PATH_DELIMITER, "<html></html>", "text/html", null, "");
                } catch (Exception unused) {
                }
            }
        }
    }

    private class i implements Runnable {
        private i() {
        }

        /* synthetic */ i(AdViewControllerImpl adViewControllerImpl, a aVar) {
            this();
        }

        public void run() {
            if (AdViewControllerImpl.this.o == null) {
                return;
            }
            if (AdViewControllerImpl.this.f3230k != null) {
                q b2 = AdViewControllerImpl.this.f3224e;
                b2.b("AppLovinAdView", "Rendering advertisement ad for #" + AdViewControllerImpl.this.o.getAdIdNumber() + "...");
                AdViewControllerImpl.b(AdViewControllerImpl.this.f3230k, AdViewControllerImpl.this.o.getSize());
                AdViewControllerImpl.this.f3230k.a(AdViewControllerImpl.this.o);
                if (AdViewControllerImpl.this.o.getSize() != AppLovinAdSize.INTERSTITIAL && !AdViewControllerImpl.this.v && (AdViewControllerImpl.this.o instanceof com.applovin.impl.sdk.ad.f)) {
                    com.applovin.impl.sdk.ad.f fVar = (com.applovin.impl.sdk.ad.f) AdViewControllerImpl.this.o;
                    AdViewControllerImpl adViewControllerImpl = AdViewControllerImpl.this;
                    com.applovin.impl.sdk.d.d unused = adViewControllerImpl.f3227h = new com.applovin.impl.sdk.d.d(fVar, adViewControllerImpl.f3222c);
                    AdViewControllerImpl.this.f3227h.a();
                    AdViewControllerImpl.this.f3230k.a(AdViewControllerImpl.this.f3227h);
                    fVar.setHasShown(true);
                }
                if (AdViewControllerImpl.this.f3230k.b() != null && (AdViewControllerImpl.this.o instanceof com.applovin.impl.sdk.ad.f)) {
                    AdViewControllerImpl.this.f3230k.b().a(((com.applovin.impl.sdk.ad.f) AdViewControllerImpl.this.o).m0() ? 0 : 1);
                    return;
                }
                return;
            }
            q.i("AppLovinAdView", "Unable to render advertisement for ad #" + AdViewControllerImpl.this.o.getAdIdNumber() + ". Please make sure you are not calling AppLovinAdView.destroy() prematurely.");
        }
    }

    static class j implements AppLovinAdLoadListener {

        /* renamed from: a  reason: collision with root package name */
        private final AdViewControllerImpl f3244a;

        j(AdViewControllerImpl adViewControllerImpl, k kVar) {
            if (adViewControllerImpl == null) {
                throw new IllegalArgumentException("No view specified");
            } else if (kVar != null) {
                kVar.Z();
                kVar.S();
                this.f3244a = adViewControllerImpl;
            } else {
                throw new IllegalArgumentException("No sdk specified");
            }
        }

        private AdViewControllerImpl a() {
            return this.f3244a;
        }

        public void adReceived(AppLovinAd appLovinAd) {
            AdViewControllerImpl a2 = a();
            if (a2 != null) {
                a2.a(appLovinAd);
            } else {
                q.i("AppLovinAdView", "Ad view has been garbage collected by the time an ad was received");
            }
        }

        public void failedToReceiveAd(int i2) {
            AdViewControllerImpl a2 = a();
            if (a2 != null) {
                a2.a(i2);
            }
        }
    }

    private void a(AppLovinAdView appLovinAdView, k kVar, AppLovinAdSize appLovinAdSize, String str, Context context) {
        if (appLovinAdView == null) {
            throw new IllegalArgumentException("No parent view specified");
        } else if (kVar == null) {
            throw new IllegalArgumentException("No sdk specified");
        } else if (appLovinAdSize != null) {
            this.f3222c = kVar;
            this.f3223d = kVar.S();
            this.f3224e = kVar.Z();
            this.f3225f = appLovinAdSize;
            this.f3226g = str;
            this.f3220a = context;
            this.f3221b = appLovinAdView;
            this.l = new com.applovin.impl.sdk.ad.h();
            this.f3228i = new d(this, kVar);
            this.n = new g(this, null);
            this.m = new i(this, null);
            this.f3229j = new j(this, kVar);
            a(appLovinAdSize);
        } else {
            throw new IllegalArgumentException("No ad size specified");
        }
    }

    /* access modifiers changed from: private */
    public void a(AppLovinAdSize appLovinAdSize) {
        try {
            this.f3230k = new c(this.f3228i, this.f3222c, this.f3220a);
            this.f3230k.setBackgroundColor(0);
            this.f3230k.setWillNotCacheDrawing(false);
            this.f3221b.setBackgroundColor(0);
            this.f3221b.addView(this.f3230k);
            b(this.f3230k, appLovinAdSize);
            if (!this.t) {
                a(this.n);
            }
            if (((Boolean) this.f3222c.a(c.e.P3)).booleanValue()) {
                a(new h(this, null));
            }
            this.t = true;
        } catch (Throwable th) {
            q.i("AppLovinAdView", "Failed to create AdView: " + th.getMessage());
        }
    }

    private void a(Runnable runnable) {
        AppLovinSdkUtils.runOnUiThread(runnable);
    }

    private void b() {
        q qVar = this.f3224e;
        if (qVar != null) {
            qVar.b("AppLovinAdView", "Destroying...");
        }
        c cVar = this.f3230k;
        if (cVar != null) {
            try {
                ViewParent parent = cVar.getParent();
                if (parent instanceof ViewGroup) {
                    ((ViewGroup) parent).removeView(this.f3230k);
                }
                this.f3230k.removeAllViews();
                if (((Boolean) this.f3222c.a(c.e.L3)).booleanValue()) {
                    this.f3230k.loadUrl("about:blank");
                    this.f3230k.onPause();
                    this.f3230k.destroyDrawingCache();
                }
            } catch (Throwable th) {
                this.f3224e.a("AppLovinAdView", "Unable to destroy ad view", th);
            }
            this.f3230k.destroy();
            this.f3230k = null;
        }
        this.v = true;
    }

    /* access modifiers changed from: private */
    public static void b(View view, AppLovinAdSize appLovinAdSize) {
        if (view != null) {
            DisplayMetrics displayMetrics = view.getResources().getDisplayMetrics();
            int i2 = -1;
            int applyDimension = appLovinAdSize.getLabel().equals(AppLovinAdSize.INTERSTITIAL.getLabel()) ? -1 : appLovinAdSize.getWidth() == -1 ? displayMetrics.widthPixels : (int) TypedValue.applyDimension(1, (float) appLovinAdSize.getWidth(), displayMetrics);
            if (!appLovinAdSize.getLabel().equals(AppLovinAdSize.INTERSTITIAL.getLabel())) {
                i2 = appLovinAdSize.getHeight() == -1 ? displayMetrics.heightPixels : (int) TypedValue.applyDimension(1, (float) appLovinAdSize.getHeight(), displayMetrics);
            }
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            if (layoutParams == null) {
                layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            }
            layoutParams.width = applyDimension;
            layoutParams.height = i2;
            if (layoutParams instanceof RelativeLayout.LayoutParams) {
                RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) layoutParams;
                layoutParams2.addRule(10);
                layoutParams2.addRule(9);
            }
            view.setLayoutParams(layoutParams);
        }
    }

    private void c() {
        a(new a());
    }

    /* access modifiers changed from: private */
    public void d() {
        a(new f());
    }

    private void e() {
        com.applovin.impl.sdk.d.d dVar = this.f3227h;
        if (dVar != null) {
            dVar.c();
            this.f3227h = null;
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.q == null && this.r == null) {
            q qVar = this.f3224e;
            qVar.b("AppLovinAdView", "Ad: " + this.o + " closed.");
            a(this.n);
            com.applovin.impl.sdk.utils.j.b(this.x, this.o);
            this.o = null;
        } else if (((Boolean) this.f3222c.a(c.e.n1)).booleanValue()) {
            contractAd();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        if (!this.v) {
            a(this.n);
        }
        a(new e(i2));
    }

    /* access modifiers changed from: package-private */
    public void a(AppLovinAd appLovinAd) {
        if (appLovinAd != null) {
            if (!this.v) {
                renderAd(appLovinAd);
            } else {
                this.s.set(appLovinAd);
                this.f3224e.b("AppLovinAdView", "Ad view has paused when an ad was received, ad saved for later");
            }
            a(new d(appLovinAd));
            return;
        }
        this.f3224e.e("AppLovinAdView", "No provided when to the view controller");
        a(-1);
    }

    /* access modifiers changed from: package-private */
    public void a(AppLovinAd appLovinAd, AppLovinAdView appLovinAdView, Uri uri, PointF pointF) {
        String str;
        q qVar;
        com.applovin.impl.sdk.utils.j.a(this.z, appLovinAd);
        if (appLovinAdView == null) {
            qVar = this.f3224e;
            str = "Unable to process ad click - AppLovinAdView destroyed prematurely";
        } else if (appLovinAd instanceof com.applovin.impl.sdk.ad.f) {
            this.f3223d.trackAndLaunchClick(appLovinAd, appLovinAdView, this, uri, pointF);
            return;
        } else {
            qVar = this.f3224e;
            str = "Unable to process ad click - EmptyAd is not supported.";
        }
        qVar.e("AppLovinAdView", str);
    }

    public void contractAd() {
        a(new c());
    }

    public void destroy() {
        if (!(this.f3230k == null || this.q == null)) {
            contractAd();
        }
        b();
    }

    public void dismissInterstitialIfRequired() {
        if ((this.f3220a instanceof m) && (this.o instanceof com.applovin.impl.sdk.ad.f)) {
            boolean z2 = ((com.applovin.impl.sdk.ad.f) this.o).e() == f.a.DISMISS;
            m mVar = (m) this.f3220a;
            if (z2 && mVar.getPoststitialWasDisplayed()) {
                mVar.dismiss();
            }
        }
    }

    public void expandAd(PointF pointF) {
        a(new b(pointF));
    }

    public AppLovinAdViewEventListener getAdViewEventListener() {
        return this.y;
    }

    public c getAdWebView() {
        return this.f3230k;
    }

    public AppLovinAd getCurrentAd() {
        return this.o;
    }

    public AppLovinAdView getParentView() {
        return (AppLovinAdView) this.f3221b;
    }

    public k getSdk() {
        return this.f3222c;
    }

    public AppLovinAdSize getSize() {
        return this.f3225f;
    }

    public String getZoneId() {
        return this.f3226g;
    }

    public void initializeAdView(AppLovinAdView appLovinAdView, Context context, AppLovinAdSize appLovinAdSize, String str, AppLovinSdk appLovinSdk, AttributeSet attributeSet) {
        if (appLovinAdView == null) {
            throw new IllegalArgumentException("No parent view specified");
        } else if (context == null) {
            q.i("AppLovinAdView", "Unable to build AppLovinAdView: no context provided. Please use a different constructor for this view.");
        } else {
            if (appLovinAdSize == null && (appLovinAdSize = com.applovin.impl.sdk.utils.b.a(attributeSet)) == null) {
                appLovinAdSize = AppLovinAdSize.BANNER;
            }
            AppLovinAdSize appLovinAdSize2 = appLovinAdSize;
            if (appLovinSdk == null) {
                appLovinSdk = AppLovinSdk.getInstance(context);
            }
            if (appLovinSdk != null && !appLovinSdk.hasCriticalErrors()) {
                a(appLovinAdView, p.a(appLovinSdk), appLovinAdSize2, str, context);
                if (com.applovin.impl.sdk.utils.b.b(attributeSet)) {
                    loadNextAd();
                }
            }
        }
    }

    public boolean isAdReadyToDisplay() {
        return !TextUtils.isEmpty(this.f3226g) ? this.f3223d.hasPreloadedAdForZoneId(this.f3226g) : this.f3223d.hasPreloadedAd(this.f3225f);
    }

    public boolean isAutoDestroy() {
        return this.u;
    }

    public void loadNextAd() {
        if (this.f3222c == null || this.f3229j == null || this.f3220a == null || !this.t) {
            q.g("AppLovinAdView", "Unable to load next ad: AppLovinAdView is not initialized.");
        } else {
            this.f3223d.loadNextAd(this.f3226g, this.f3225f, this.f3229j);
        }
    }

    public void onAdHtmlLoaded(WebView webView) {
        if (this.o instanceof com.applovin.impl.sdk.ad.f) {
            webView.setVisibility(0);
            try {
                if (this.o != this.p && this.x != null) {
                    this.p = this.o;
                    com.applovin.impl.sdk.utils.j.a(this.x, this.o);
                }
            } catch (Throwable th) {
                q.c("AppLovinAdView", "Exception while notifying ad display listener", th);
            }
        }
    }

    public void onDetachedFromWindow() {
        if (this.t) {
            if (this.o != this.l) {
                com.applovin.impl.sdk.utils.j.b(this.x, this.o);
            }
            if (this.f3230k == null || this.q == null) {
                this.f3224e.b("AppLovinAdView", "onDetachedFromWindowCalled without an expanded ad present");
            } else {
                this.f3224e.b("AppLovinAdView", "onDetachedFromWindowCalled with expanded ad present");
                if (((Boolean) this.f3222c.a(c.e.m1)).booleanValue()) {
                    contractAd();
                } else {
                    c();
                }
            }
            if (this.u) {
                b();
            }
        }
    }

    public void onVisibilityChanged(int i2) {
        if (!this.t || !this.u) {
            return;
        }
        if (i2 == 8 || i2 == 4) {
            pause();
        } else if (i2 == 0) {
            resume();
        }
    }

    public void pause() {
        if (this.t && !this.v) {
            AppLovinAd appLovinAd = this.o;
            renderAd(this.l);
            if (appLovinAd != null) {
                this.s.set(appLovinAd);
            }
            this.v = true;
        }
    }

    public void renderAd(AppLovinAd appLovinAd) {
        renderAd(appLovinAd, null);
    }

    public void renderAd(AppLovinAd appLovinAd, String str) {
        String str2;
        AppLovinAdSize appLovinAdSize;
        if (appLovinAd != null) {
            p.b(appLovinAd, this.f3222c);
            if (this.t) {
                AppLovinAd a2 = p.a(appLovinAd, this.f3222c);
                if (a2 == null || a2 == this.o) {
                    q qVar = this.f3224e;
                    if (a2 == null) {
                        str2 = "Unable to render ad. Ad is null. Internal inconsistency error.";
                    } else {
                        str2 = "Ad #" + a2.getAdIdNumber() + " is already showing, ignoring";
                    }
                    qVar.d("AppLovinAdView", str2);
                    return;
                }
                this.f3224e.b("AppLovinAdView", "Rendering ad #" + a2.getAdIdNumber() + " (" + a2.getSize() + ")");
                if (!(this.o instanceof com.applovin.impl.sdk.ad.h)) {
                    com.applovin.impl.sdk.utils.j.b(this.x, this.o);
                    if (!(a2 instanceof com.applovin.impl.sdk.ad.h) && a2.getSize() != AppLovinAdSize.INTERSTITIAL) {
                        e();
                    }
                }
                this.s.set(null);
                this.p = null;
                this.o = a2;
                if ((appLovinAd instanceof com.applovin.impl.sdk.ad.f) && !this.v && ((appLovinAdSize = this.f3225f) == AppLovinAdSize.BANNER || appLovinAdSize == AppLovinAdSize.MREC || appLovinAdSize == AppLovinAdSize.LEADER)) {
                    this.f3222c.S().trackImpression((com.applovin.impl.sdk.ad.f) appLovinAd);
                }
                boolean z2 = a2 instanceof com.applovin.impl.sdk.ad.h;
                if (!z2 && this.q != null) {
                    if (((Boolean) this.f3222c.a(c.e.l1)).booleanValue()) {
                        d();
                        this.f3224e.b("AppLovinAdView", "Fade out the old ad scheduled");
                    } else {
                        c();
                    }
                }
                if (!z2 || (this.q == null && this.r == null)) {
                    a(this.m);
                } else {
                    this.f3224e.b("AppLovinAdView", "Ignoring empty ad render with expanded ad");
                }
            } else {
                q.g("AppLovinAdView", "Unable to render ad: AppLovinAdView is not initialized.");
            }
        } else {
            throw new IllegalArgumentException("No ad specified");
        }
    }

    public void resume() {
        if (this.t) {
            AppLovinAd andSet = this.s.getAndSet(null);
            if (andSet != null) {
                renderAd(andSet);
            }
            this.v = false;
        }
    }

    public void setAdClickListener(AppLovinAdClickListener appLovinAdClickListener) {
        this.z = appLovinAdClickListener;
    }

    public void setAdDisplayListener(AppLovinAdDisplayListener appLovinAdDisplayListener) {
        this.x = appLovinAdDisplayListener;
    }

    public void setAdLoadListener(AppLovinAdLoadListener appLovinAdLoadListener) {
        this.w = appLovinAdLoadListener;
    }

    public void setAdVideoPlaybackListener(AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener) {
    }

    public void setAdViewEventListener(AppLovinAdViewEventListener appLovinAdViewEventListener) {
        this.y = appLovinAdViewEventListener;
    }

    public void setAutoDestroy(boolean z2) {
        this.u = z2;
    }

    public void setStatsManagerHelper(com.applovin.impl.sdk.d.d dVar) {
        c cVar = this.f3230k;
        if (cVar != null) {
            cVar.a(dVar);
        }
    }
}
