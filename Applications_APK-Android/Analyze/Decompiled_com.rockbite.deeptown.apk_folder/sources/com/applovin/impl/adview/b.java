package com.applovin.impl.adview;

import android.webkit.ConsoleMessage;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.q;

class b extends WebChromeClient {

    /* renamed from: a  reason: collision with root package name */
    private final q f3255a;

    public b(k kVar) {
        this.f3255a = kVar.Z();
    }

    public void onConsoleMessage(String str, int i2, String str2) {
        q qVar = this.f3255a;
        qVar.d("AdWebView", "console.log[" + i2 + "] :" + str);
    }

    public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        this.f3255a.b("AdWebView", consoleMessage.sourceId() + ": " + consoleMessage.lineNumber() + ": " + consoleMessage.message());
        return true;
    }

    public boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
        q qVar = this.f3255a;
        qVar.d("AdWebView", "Alert attempted: " + str2);
        return true;
    }

    public boolean onJsBeforeUnload(WebView webView, String str, String str2, JsResult jsResult) {
        q qVar = this.f3255a;
        qVar.d("AdWebView", "JS onBeforeUnload attempted: " + str2);
        return true;
    }

    public boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
        q qVar = this.f3255a;
        qVar.d("AdWebView", "JS confirm attempted: " + str2);
        return true;
    }
}
