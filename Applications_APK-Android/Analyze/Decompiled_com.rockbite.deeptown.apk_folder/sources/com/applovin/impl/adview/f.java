package com.applovin.impl.adview;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import com.applovin.sdk.AppLovinMediationProvider;

public class f extends View {
    private static final int w = Color.rgb(66, 145, 241);
    private static final int x = Color.rgb(66, 145, 241);
    private static final int y = Color.rgb(66, 145, 241);

    /* renamed from: a  reason: collision with root package name */
    private Paint f3296a;

    /* renamed from: b  reason: collision with root package name */
    private Paint f3297b;

    /* renamed from: c  reason: collision with root package name */
    protected Paint f3298c;

    /* renamed from: d  reason: collision with root package name */
    protected Paint f3299d;

    /* renamed from: e  reason: collision with root package name */
    private RectF f3300e;

    /* renamed from: f  reason: collision with root package name */
    private float f3301f;

    /* renamed from: g  reason: collision with root package name */
    private int f3302g;

    /* renamed from: h  reason: collision with root package name */
    private int f3303h;

    /* renamed from: i  reason: collision with root package name */
    private int f3304i;

    /* renamed from: j  reason: collision with root package name */
    private int f3305j;

    /* renamed from: k  reason: collision with root package name */
    private int f3306k;
    private float l;
    private int m;
    private String n;
    private String o;
    private float p;
    private String q;
    private float r;
    private final float s;
    private final float t;
    private final float u;
    private final int v;

    private static class a {
        /* access modifiers changed from: private */
        public static float c(Resources resources, float f2) {
            return (f2 * resources.getDisplayMetrics().density) + 0.5f;
        }

        /* access modifiers changed from: private */
        public static float d(Resources resources, float f2) {
            return f2 * resources.getDisplayMetrics().scaledDensity;
        }
    }

    public f(Context context) {
        this(context, null);
    }

    public f(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public f(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f3300e = new RectF();
        this.f3304i = 0;
        this.n = "";
        this.o = "";
        this.q = "";
        this.t = a.d(getResources(), 14.0f);
        this.v = (int) a.c(getResources(), 100.0f);
        this.s = a.c(getResources(), 4.0f);
        this.u = a.d(getResources(), 18.0f);
        b();
        a();
    }

    private int a(int i2) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824) {
            return size;
        }
        int i3 = this.v;
        return mode == Integer.MIN_VALUE ? Math.min(i3, size) : i3;
    }

    private float getProgressAngle() {
        return (((float) getProgress()) / ((float) this.f3305j)) * 360.0f;
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.f3298c = new TextPaint();
        this.f3298c.setColor(this.f3302g);
        this.f3298c.setTextSize(this.f3301f);
        this.f3298c.setAntiAlias(true);
        this.f3299d = new TextPaint();
        this.f3299d.setColor(this.f3303h);
        this.f3299d.setTextSize(this.p);
        this.f3299d.setAntiAlias(true);
        this.f3296a = new Paint();
        this.f3296a.setColor(this.f3306k);
        this.f3296a.setStyle(Paint.Style.STROKE);
        this.f3296a.setAntiAlias(true);
        this.f3296a.setStrokeWidth(this.l);
        this.f3297b = new Paint();
        this.f3297b.setColor(this.m);
        this.f3297b.setAntiAlias(true);
    }

    /* access modifiers changed from: protected */
    public void b() {
        this.f3306k = w;
        this.f3302g = x;
        this.f3301f = this.t;
        setMax(100);
        setProgress(0);
        this.l = this.s;
        this.m = 0;
        this.p = this.u;
        this.f3303h = y;
    }

    public int getFinishedStrokeColor() {
        return this.f3306k;
    }

    public float getFinishedStrokeWidth() {
        return this.l;
    }

    public int getInnerBackgroundColor() {
        return this.m;
    }

    public String getInnerBottomText() {
        return this.q;
    }

    public int getInnerBottomTextColor() {
        return this.f3303h;
    }

    public float getInnerBottomTextSize() {
        return this.p;
    }

    public int getMax() {
        return this.f3305j;
    }

    public String getPrefixText() {
        return this.n;
    }

    public int getProgress() {
        return this.f3304i;
    }

    public String getSuffixText() {
        return this.o;
    }

    public int getTextColor() {
        return this.f3302g;
    }

    public float getTextSize() {
        return this.f3301f;
    }

    public void invalidate() {
        a();
        super.invalidate();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float f2 = this.l;
        this.f3300e.set(f2, f2, ((float) getWidth()) - f2, ((float) getHeight()) - f2);
        float f3 = this.l;
        canvas.drawCircle(((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f, ((((float) getWidth()) - f3) + f3) / 2.0f, this.f3297b);
        canvas.drawArc(this.f3300e, 270.0f, -getProgressAngle(), false, this.f3296a);
        String str = this.n + this.f3304i + this.o;
        if (!TextUtils.isEmpty(str)) {
            canvas.drawText(str, (((float) getWidth()) - this.f3298c.measureText(str)) / 2.0f, (((float) getWidth()) - (this.f3298c.descent() + this.f3298c.ascent())) / 2.0f, this.f3298c);
        }
        if (!TextUtils.isEmpty(getInnerBottomText())) {
            this.f3299d.setTextSize(this.p);
            canvas.drawText(getInnerBottomText(), (((float) getWidth()) - this.f3299d.measureText(getInnerBottomText())) / 2.0f, (((float) getHeight()) - this.r) - ((this.f3298c.descent() + this.f3298c.ascent()) / 2.0f), this.f3299d);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        setMeasuredDimension(a(i2), a(i3));
        this.r = (float) (getHeight() - ((getHeight() * 3) / 4));
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            this.f3302g = bundle.getInt("text_color");
            this.f3301f = bundle.getFloat("text_size");
            this.p = bundle.getFloat("inner_bottom_text_size");
            this.q = bundle.getString("inner_bottom_text");
            this.f3303h = bundle.getInt("inner_bottom_text_color");
            this.f3306k = bundle.getInt("finished_stroke_color");
            this.l = bundle.getFloat("finished_stroke_width");
            this.m = bundle.getInt("inner_background_color");
            a();
            setMax(bundle.getInt(AppLovinMediationProvider.MAX));
            setProgress(bundle.getInt("progress"));
            this.n = bundle.getString("prefix");
            this.o = bundle.getString("suffix");
            super.onRestoreInstanceState(bundle.getParcelable("saved_instance"));
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("saved_instance", super.onSaveInstanceState());
        bundle.putInt("text_color", getTextColor());
        bundle.putFloat("text_size", getTextSize());
        bundle.putFloat("inner_bottom_text_size", getInnerBottomTextSize());
        bundle.putFloat("inner_bottom_text_color", (float) getInnerBottomTextColor());
        bundle.putString("inner_bottom_text", getInnerBottomText());
        bundle.putInt("inner_bottom_text_color", getInnerBottomTextColor());
        bundle.putInt("finished_stroke_color", getFinishedStrokeColor());
        bundle.putInt(AppLovinMediationProvider.MAX, getMax());
        bundle.putInt("progress", getProgress());
        bundle.putString("suffix", getSuffixText());
        bundle.putString("prefix", getPrefixText());
        bundle.putFloat("finished_stroke_width", getFinishedStrokeWidth());
        bundle.putInt("inner_background_color", getInnerBackgroundColor());
        return bundle;
    }

    public void setFinishedStrokeColor(int i2) {
        this.f3306k = i2;
        invalidate();
    }

    public void setFinishedStrokeWidth(float f2) {
        this.l = f2;
        invalidate();
    }

    public void setInnerBackgroundColor(int i2) {
        this.m = i2;
        invalidate();
    }

    public void setInnerBottomText(String str) {
        this.q = str;
        invalidate();
    }

    public void setInnerBottomTextColor(int i2) {
        this.f3303h = i2;
        invalidate();
    }

    public void setInnerBottomTextSize(float f2) {
        this.p = f2;
        invalidate();
    }

    public void setMax(int i2) {
        if (i2 > 0) {
            this.f3305j = i2;
            invalidate();
        }
    }

    public void setPrefixText(String str) {
        this.n = str;
        invalidate();
    }

    public void setProgress(int i2) {
        this.f3304i = i2;
        if (this.f3304i > getMax()) {
            this.f3304i %= getMax();
        }
        invalidate();
    }

    public void setSuffixText(String str) {
        this.o = str;
        invalidate();
    }

    public void setTextColor(int i2) {
        this.f3302g = i2;
        invalidate();
    }

    public void setTextSize(float f2) {
        this.f3301f = f2;
        invalidate();
    }
}
