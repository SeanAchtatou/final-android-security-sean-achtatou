package com.applovin.impl.adview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import com.applovin.impl.adview.h;
import com.applovin.impl.sdk.k;

@SuppressLint({"ViewConstructor"})
public final class x extends h {

    /* renamed from: b  reason: collision with root package name */
    private static final Paint f3439b = new Paint(1);

    /* renamed from: c  reason: collision with root package name */
    private static final Paint f3440c = new Paint(1);

    /* renamed from: d  reason: collision with root package name */
    private static final Paint f3441d = new Paint(1);

    /* renamed from: a  reason: collision with root package name */
    private float f3442a = 1.0f;

    public x(k kVar, Context context) {
        super(kVar, context);
        f3439b.setColor(-1);
        f3440c.setColor(-16777216);
        f3441d.setColor(-1);
        f3441d.setStyle(Paint.Style.STROKE);
    }

    public void a(int i2) {
        setViewScale(((float) i2) / 30.0f);
    }

    /* access modifiers changed from: protected */
    public float getCenter() {
        return getSize() / 2.0f;
    }

    /* access modifiers changed from: protected */
    public float getCrossOffset() {
        return this.f3442a * 10.0f;
    }

    /* access modifiers changed from: protected */
    public float getInnerCircleOffset() {
        return this.f3442a * 2.0f;
    }

    /* access modifiers changed from: protected */
    public float getInnerCircleRadius() {
        return getCenter() - getInnerCircleOffset();
    }

    /* access modifiers changed from: protected */
    public float getSize() {
        return this.f3442a * 30.0f;
    }

    /* access modifiers changed from: protected */
    public float getStrokeWidth() {
        return this.f3442a * 3.0f;
    }

    public h.a getStyle() {
        return h.a.WhiteXOnOpaqueBlack;
    }

    public float getViewScale() {
        return this.f3442a;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float center = getCenter();
        canvas.drawCircle(center, center, center, f3439b);
        canvas.drawCircle(center, center, getInnerCircleRadius(), f3440c);
        float crossOffset = getCrossOffset();
        float size = getSize() - crossOffset;
        f3441d.setStrokeWidth(getStrokeWidth());
        Canvas canvas2 = canvas;
        float f2 = crossOffset;
        float f3 = size;
        canvas2.drawLine(f2, crossOffset, f3, size, f3441d);
        canvas2.drawLine(f2, size, f3, crossOffset, f3441d);
    }

    public void setViewScale(float f2) {
        this.f3442a = f2;
    }
}
