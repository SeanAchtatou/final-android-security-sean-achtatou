package com.applovin.impl.adview;

import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.q;
import java.lang.ref.WeakReference;

public class v extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    private final q f3435a;

    /* renamed from: b  reason: collision with root package name */
    private WeakReference<a> f3436b;

    public interface a {
        void a(u uVar);

        void b(u uVar);

        void c(u uVar);
    }

    public v(k kVar) {
        this.f3435a = kVar.Z();
    }

    private void a(WebView webView, String str) {
        q qVar = this.f3435a;
        qVar.c("WebViewButtonClient", "Processing click on ad URL \"" + str + "\"");
        if (str != null && (webView instanceof u)) {
            u uVar = (u) webView;
            Uri parse = Uri.parse(str);
            String scheme = parse.getScheme();
            String host = parse.getHost();
            String path = parse.getPath();
            a aVar = this.f3436b.get();
            if ("applovin".equalsIgnoreCase(scheme) && "com.applovin.sdk".equalsIgnoreCase(host) && aVar != null) {
                if ("/track_click".equals(path)) {
                    aVar.c(uVar);
                } else if ("/close_ad".equals(path)) {
                    aVar.b(uVar);
                } else if ("/skip_ad".equals(path)) {
                    aVar.a(uVar);
                } else {
                    q qVar2 = this.f3435a;
                    qVar2.d("WebViewButtonClient", "Unknown URL: " + str);
                    q qVar3 = this.f3435a;
                    qVar3.d("WebViewButtonClient", "Path: " + path);
                }
            }
        }
    }

    public void a(WeakReference<a> weakReference) {
        this.f3436b = weakReference;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        a(webView, str);
        return true;
    }
}
