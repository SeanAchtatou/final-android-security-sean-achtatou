package com.applovin.impl.adview;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.RelativeLayout;
import com.applovin.impl.adview.h;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.q;
import com.applovin.sdk.AppLovinSdkUtils;
import com.esotericsoftware.spine.Animation;

class k extends Dialog implements j {

    /* renamed from: a  reason: collision with root package name */
    private final Activity f3323a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final com.applovin.impl.sdk.k f3324b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public final q f3325c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public final c f3326d;

    /* renamed from: e  reason: collision with root package name */
    private final com.applovin.impl.sdk.ad.a f3327e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public RelativeLayout f3328f;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public h f3329g;

    class a implements Runnable {
        a() {
        }

        public void run() {
            k.this.dismiss();
        }
    }

    class b implements Runnable {
        b() {
        }

        public void run() {
            k.this.dismiss();
        }
    }

    class c implements Runnable {
        c() {
        }

        public void run() {
            k.this.f3328f.removeView(k.this.f3326d);
            k.super.dismiss();
        }
    }

    class d implements View.OnClickListener {
        d() {
        }

        public void onClick(View view) {
            k.this.c();
        }
    }

    class e implements View.OnClickListener {
        e() {
        }

        public void onClick(View view) {
            if (k.this.f3329g.isClickable()) {
                k.this.f3329g.performClick();
            }
        }
    }

    class f implements Runnable {

        class a implements Animation.AnimationListener {
            a() {
            }

            public void onAnimationEnd(Animation animation) {
                k.this.f3329g.setClickable(true);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        }

        f() {
        }

        public void run() {
            try {
                if (k.this.f3329g == null) {
                    k.this.c();
                }
                k.this.f3329g.setVisibility(0);
                k.this.f3329g.bringToFront();
                AlphaAnimation alphaAnimation = new AlphaAnimation((float) Animation.CurveTimeline.LINEAR, 1.0f);
                alphaAnimation.setDuration(((Long) k.this.f3324b.a(c.e.o1)).longValue());
                alphaAnimation.setAnimationListener(new a());
                k.this.f3329g.startAnimation(alphaAnimation);
            } catch (Throwable th) {
                k.this.f3325c.b("ExpandedAdDialog", "Unable to fade in close button", th);
                k.this.c();
            }
        }
    }

    k(com.applovin.impl.sdk.ad.a aVar, c cVar, Activity activity, com.applovin.impl.sdk.k kVar) {
        super(activity, 16973840);
        if (aVar == null) {
            throw new IllegalArgumentException("No ad specified");
        } else if (cVar == null) {
            throw new IllegalArgumentException("No main view specified");
        } else if (kVar == null) {
            throw new IllegalArgumentException("No sdk specified");
        } else if (activity != null) {
            this.f3324b = kVar;
            this.f3325c = kVar.Z();
            this.f3323a = activity;
            this.f3326d = cVar;
            this.f3327e = aVar;
            requestWindowFeature(1);
            setCancelable(false);
        } else {
            throw new IllegalArgumentException("No activity specified");
        }
    }

    private int a(int i2) {
        return AppLovinSdkUtils.dpToPx(this.f3323a, i2);
    }

    private void a(h.a aVar) {
        if (this.f3329g != null) {
            this.f3325c.d("ExpandedAdDialog", "Attempting to create duplicate close button");
            return;
        }
        this.f3329g = h.a(this.f3324b, getContext(), aVar);
        this.f3329g.setVisibility(8);
        this.f3329g.setOnClickListener(new d());
        this.f3329g.setClickable(false);
        int a2 = a(((Integer) this.f3324b.a(c.e.p1)).intValue());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(a2, a2);
        layoutParams.addRule(10);
        int i2 = 9;
        layoutParams.addRule(((Boolean) this.f3324b.a(c.e.s1)).booleanValue() ? 9 : 11);
        this.f3329g.a(a2);
        int a3 = a(((Integer) this.f3324b.a(c.e.r1)).intValue());
        int a4 = a(((Integer) this.f3324b.a(c.e.q1)).intValue());
        layoutParams.setMargins(a4, a3, a4, 0);
        this.f3328f.addView(this.f3329g, layoutParams);
        this.f3329g.bringToFront();
        int a5 = a(((Integer) this.f3324b.a(c.e.t1)).intValue());
        View view = new View(this.f3323a);
        view.setBackgroundColor(0);
        int i3 = a2 + a5;
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(i3, i3);
        layoutParams2.addRule(10);
        if (!((Boolean) this.f3324b.a(c.e.s1)).booleanValue()) {
            i2 = 11;
        }
        layoutParams2.addRule(i2);
        layoutParams2.setMargins(a4 - a(5), a3 - a(5), a4 - a(5), 0);
        view.setOnClickListener(new e());
        this.f3328f.addView(view, layoutParams2);
        view.bringToFront();
    }

    private void b() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(13);
        this.f3326d.setLayoutParams(layoutParams);
        this.f3328f = new RelativeLayout(this.f3323a);
        this.f3328f.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this.f3328f.setBackgroundColor(-1157627904);
        this.f3328f.addView(this.f3326d);
        if (!this.f3327e.B0()) {
            a(this.f3327e.C0());
            d();
        }
        setContentView(this.f3328f);
    }

    /* access modifiers changed from: private */
    public void c() {
        this.f3326d.a("javascript:al_onCloseTapped();", new a());
    }

    private void d() {
        this.f3323a.runOnUiThread(new f());
    }

    public com.applovin.impl.sdk.ad.a a() {
        return this.f3327e;
    }

    public void dismiss() {
        com.applovin.impl.sdk.d.d b2 = this.f3326d.b();
        if (b2 != null) {
            b2.e();
        }
        this.f3323a.runOnUiThread(new c());
    }

    public void onBackPressed() {
        this.f3326d.a("javascript:al_onBackPressed();", new b());
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        b();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        try {
            Window window = getWindow();
            if (window != null) {
                window.setFlags(this.f3323a.getWindow().getAttributes().flags, this.f3323a.getWindow().getAttributes().flags);
                if (this.f3327e.q0()) {
                    window.addFlags(16777216);
                    return;
                }
                return;
            }
            this.f3325c.e("ExpandedAdDialog", "Unable to turn on hardware acceleration - window is null");
        } catch (Throwable th) {
            this.f3325c.b("ExpandedAdDialog", "Setting window flags failed.", th);
        }
    }
}
