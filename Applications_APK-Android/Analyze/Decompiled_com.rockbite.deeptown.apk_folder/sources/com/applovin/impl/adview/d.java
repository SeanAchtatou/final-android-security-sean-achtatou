package com.applovin.impl.adview;

import android.annotation.TargetApi;
import android.graphics.Color;
import android.graphics.PointF;
import android.net.Uri;
import android.net.http.SslError;
import android.view.ViewParent;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.applovin.adview.AppLovinAdView;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.d.c;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.q;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.p;
import com.applovin.sdk.AppLovinAd;
import com.tapjoy.TapjoyConstants;
import e.c.a.a.a;
import e.c.a.a.b;
import e.c.a.a.i;

class d extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    private final k f3293a;

    /* renamed from: b  reason: collision with root package name */
    private final q f3294b;

    /* renamed from: c  reason: collision with root package name */
    private final AdViewControllerImpl f3295c;

    public d(AdViewControllerImpl adViewControllerImpl, k kVar) {
        this.f3293a = kVar;
        this.f3294b = kVar.Z();
        this.f3295c = adViewControllerImpl;
    }

    private void a() {
        this.f3295c.a();
    }

    private void a(PointF pointF) {
        this.f3295c.expandAd(pointF);
    }

    private void a(Uri uri, c cVar) {
        q qVar;
        String str;
        try {
            String queryParameter = uri.getQueryParameter("n");
            if (m.b(queryParameter)) {
                String queryParameter2 = uri.getQueryParameter("load_type");
                if ("external".equalsIgnoreCase(queryParameter2)) {
                    q qVar2 = this.f3294b;
                    qVar2.b("AdWebView", "Loading new page externally: " + queryParameter);
                    p.a(cVar.getContext(), Uri.parse(queryParameter), this.f3293a);
                    j.c(this.f3295c.getAdViewEventListener(), this.f3295c.getCurrentAd(), this.f3295c.getParentView());
                    return;
                } else if (TapjoyConstants.LOG_LEVEL_INTERNAL.equalsIgnoreCase(queryParameter2)) {
                    q qVar3 = this.f3294b;
                    qVar3.b("AdWebView", "Loading new page in WebView: " + queryParameter);
                    cVar.loadUrl(queryParameter);
                    String queryParameter3 = uri.getQueryParameter("bg_color");
                    if (m.b(queryParameter3)) {
                        cVar.setBackgroundColor(Color.parseColor(queryParameter3));
                        return;
                    }
                    return;
                } else {
                    qVar = this.f3294b;
                    str = "Could not find load type in original uri";
                }
            } else {
                qVar = this.f3294b;
                str = "Could not find url to load from query in original uri";
            }
            qVar.e("AdWebView", str);
        } catch (Throwable unused) {
            this.f3294b.e("AdWebView", "Failed to load new page from query in original uri");
        }
    }

    private void a(c cVar) {
        ViewParent parent = cVar.getParent();
        if (parent instanceof AppLovinAdView) {
            ((AppLovinAdView) parent).loadNextAd();
        }
    }

    private void a(c cVar, Uri uri) {
        AppLovinAd a2 = cVar.a();
        AppLovinAdView parentView = this.f3295c.getParentView();
        if (parentView == null || a2 == null) {
            q qVar = this.f3294b;
            qVar.e("AdWebView", "Attempting to track click that is null or not an ApplovinAdView instance for clickedUri = " + uri);
            return;
        }
        com.applovin.impl.sdk.d.d b2 = cVar.b();
        if (b2 != null) {
            b2.b();
        }
        this.f3295c.a(a2, parentView, uri, cVar.getAndClearLastClickLocation());
    }

    private void a(a aVar, c cVar) {
        b F0 = aVar.F0();
        if (F0 != null) {
            i.a(F0.c(), this.f3295c.getSdk());
            a(cVar, F0.a());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:61:0x016e, code lost:
        if (r7.G0() != false) goto L_0x00a9;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean a(android.webkit.WebView r10, java.lang.String r11, boolean r12) {
        /*
            r9 = this;
            com.applovin.impl.sdk.q r0 = r9.f3294b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Processing click on ad URL \""
            r1.append(r2)
            r1.append(r11)
            java.lang.String r2 = "\""
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.lang.String r2 = "AdWebView"
            r0.c(r2, r1)
            r0 = 1
            if (r11 == 0) goto L_0x0180
            boolean r1 = r10 instanceof com.applovin.impl.adview.c
            if (r1 == 0) goto L_0x0180
            android.net.Uri r1 = android.net.Uri.parse(r11)
            r3 = r10
            com.applovin.impl.adview.c r3 = (com.applovin.impl.adview.c) r3
            java.lang.String r4 = r1.getScheme()
            java.lang.String r5 = r1.getHost()
            java.lang.String r6 = r1.getPath()
            com.applovin.impl.adview.AdViewControllerImpl r7 = r9.f3295c
            com.applovin.sdk.AppLovinAd r7 = r7.getCurrentAd()
            java.lang.String r8 = "applovin"
            boolean r8 = r8.equals(r4)
            if (r8 == 0) goto L_0x0135
            java.lang.String r8 = "com.applovin.sdk"
            boolean r8 = r8.equals(r5)
            if (r8 == 0) goto L_0x0135
            java.lang.String r12 = "/adservice/next_ad"
            boolean r12 = r12.equals(r6)
            if (r12 == 0) goto L_0x005a
            r9.a(r3)
            goto L_0x0180
        L_0x005a:
            java.lang.String r12 = "/adservice/close_ad"
            boolean r12 = r12.equals(r6)
            if (r12 == 0) goto L_0x0067
            r9.a()
            goto L_0x0180
        L_0x0067:
            java.lang.String r12 = "/adservice/expand_ad"
            boolean r12 = r12.equals(r6)
            if (r12 == 0) goto L_0x0078
            android.graphics.PointF r10 = r3.getAndClearLastClickLocation()
            r9.a(r10)
            goto L_0x0180
        L_0x0078:
            java.lang.String r12 = "/adservice/contract_ad"
            boolean r12 = r12.equals(r6)
            if (r12 == 0) goto L_0x0085
            r9.b()
            goto L_0x0180
        L_0x0085:
            java.lang.String r12 = com.applovin.impl.sdk.AppLovinAdServiceImpl.URI_NO_OP
            boolean r12 = r12.equals(r6)
            if (r12 == 0) goto L_0x008e
            return r0
        L_0x008e:
            java.lang.String r12 = com.applovin.impl.sdk.AppLovinAdServiceImpl.URI_LOAD_URL
            boolean r12 = r12.equals(r6)
            if (r12 == 0) goto L_0x009b
            r9.a(r1, r3)
            goto L_0x0180
        L_0x009b:
            java.lang.String r12 = com.applovin.impl.sdk.AppLovinAdServiceImpl.URI_TRACK_CLICK_IMMEDIATELY
            boolean r12 = r12.equals(r6)
            if (r12 == 0) goto L_0x00b9
            boolean r10 = r7 instanceof e.c.a.a.a
            if (r10 == 0) goto L_0x00ae
            e.c.a.a.a r7 = (e.c.a.a.a) r7
        L_0x00a9:
            r9.a(r7, r3)
            goto L_0x0180
        L_0x00ae:
            java.lang.String r10 = com.applovin.impl.sdk.AppLovinAdServiceImpl.URI_TRACK_CLICK_IMMEDIATELY
            android.net.Uri r10 = android.net.Uri.parse(r10)
            r9.a(r3, r10)
            goto L_0x0180
        L_0x00b9:
            if (r6 == 0) goto L_0x0108
            java.lang.String r12 = "/launch/"
            boolean r12 = r6.startsWith(r12)
            if (r12 == 0) goto L_0x0108
            java.util.List r11 = r1.getPathSegments()
            if (r11 == 0) goto L_0x0180
            int r12 = r11.size()
            if (r12 <= r0) goto L_0x0180
            int r12 = r11.size()
            int r12 = r12 - r0
            java.lang.Object r11 = r11.get(r12)
            java.lang.String r11 = (java.lang.String) r11
            android.content.Context r10 = r10.getContext()     // Catch:{ all -> 0x00ef }
            android.content.pm.PackageManager r12 = r10.getPackageManager()     // Catch:{ all -> 0x00ef }
            android.content.Intent r12 = r12.getLaunchIntentForPackage(r11)     // Catch:{ all -> 0x00ef }
            r10.startActivity(r12)     // Catch:{ all -> 0x00ef }
            r10 = 0
            r9.a(r3, r10)     // Catch:{ all -> 0x00ef }
            goto L_0x0180
        L_0x00ef:
            r10 = move-exception
            com.applovin.impl.sdk.q r12 = r9.f3294b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "Threw Exception Trying to Launch App for Package: "
            r1.append(r3)
            r1.append(r11)
            java.lang.String r11 = r1.toString()
            r12.b(r2, r11, r10)
            goto L_0x0180
        L_0x0108:
            com.applovin.impl.sdk.q r10 = r9.f3294b
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r1 = "Unknown URL: "
            r12.append(r1)
            r12.append(r11)
            java.lang.String r11 = r12.toString()
            r10.d(r2, r11)
            com.applovin.impl.sdk.q r10 = r9.f3294b
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "Path: "
            r11.append(r12)
            r11.append(r6)
            java.lang.String r11 = r11.toString()
            r10.d(r2, r11)
            goto L_0x0180
        L_0x0135:
            if (r12 == 0) goto L_0x017e
            boolean r10 = r7 instanceof com.applovin.impl.sdk.ad.f
            if (r10 == 0) goto L_0x0176
            r10 = r7
            com.applovin.impl.sdk.ad.f r10 = (com.applovin.impl.sdk.ad.f) r10
            java.util.List r11 = r10.W()
            java.util.List r10 = r10.X()
            boolean r12 = r11.isEmpty()
            if (r12 != 0) goto L_0x0152
            boolean r11 = r11.contains(r4)
            if (r11 == 0) goto L_0x015f
        L_0x0152:
            boolean r11 = r10.isEmpty()
            if (r11 != 0) goto L_0x0164
            boolean r10 = r10.contains(r5)
            if (r10 == 0) goto L_0x015f
            goto L_0x0164
        L_0x015f:
            com.applovin.impl.sdk.q r10 = r9.f3294b
            java.lang.String r11 = "URL is not whitelisted - bypassing click"
            goto L_0x017a
        L_0x0164:
            boolean r10 = r7 instanceof e.c.a.a.a
            if (r10 == 0) goto L_0x0172
            e.c.a.a.a r7 = (e.c.a.a.a) r7
            boolean r10 = r7.G0()
            if (r10 == 0) goto L_0x0172
            goto L_0x00a9
        L_0x0172:
            r9.a(r3, r1)
            goto L_0x0180
        L_0x0176:
            com.applovin.impl.sdk.q r10 = r9.f3294b
            java.lang.String r11 = "Bypassing click for ad of invalid type"
        L_0x017a:
            r10.e(r2, r11)
            goto L_0x0180
        L_0x017e:
            r10 = 0
            return r10
        L_0x0180:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.adview.d.a(android.webkit.WebView, java.lang.String, boolean):boolean");
    }

    private void b() {
        this.f3295c.contractAd();
    }

    public void onLoadResource(WebView webView, String str) {
        super.onLoadResource(webView, str);
        q qVar = this.f3294b;
        qVar.c("AdWebView", "Loaded resource: " + str);
    }

    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        this.f3295c.onAdHtmlLoaded(webView);
    }

    public void onReceivedError(WebView webView, int i2, String str, String str2) {
        super.onReceivedError(webView, i2, str, str2);
        AppLovinAd currentAd = this.f3295c.getCurrentAd();
        String str3 = "Received error with error code: " + i2 + " with description \\'" + str + "\\' for URL: " + str2;
        if (currentAd instanceof AppLovinAdBase) {
            c.C0103c a2 = this.f3293a.w().a((AppLovinAdBase) currentAd);
            a2.a(com.applovin.impl.sdk.d.b.F, str3);
            a2.a();
        }
        this.f3294b.e("AdWebView", str3 + " for ad: " + currentAd);
    }

    @TargetApi(23)
    public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
        super.onReceivedError(webView, webResourceRequest, webResourceError);
        onReceivedError(webView, webResourceError.getErrorCode(), webResourceError.getDescription().toString(), webResourceRequest.getUrl().toString());
    }

    @TargetApi(21)
    public void onReceivedHttpError(WebView webView, WebResourceRequest webResourceRequest, WebResourceResponse webResourceResponse) {
        super.onReceivedHttpError(webView, webResourceRequest, webResourceResponse);
        AppLovinAd currentAd = this.f3295c.getCurrentAd();
        if (currentAd instanceof AppLovinAdBase) {
            c.C0103c a2 = this.f3293a.w().a((AppLovinAdBase) currentAd);
            a2.a(com.applovin.impl.sdk.d.b.G);
            a2.a();
        }
        q qVar = this.f3294b;
        qVar.e("AdWebView", "Received HTTP error: " + webResourceResponse + "for url: " + webResourceRequest.getUrl() + " and ad: " + currentAd);
    }

    public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
        super.onReceivedSslError(webView, sslErrorHandler, sslError);
        AppLovinAd currentAd = this.f3295c.getCurrentAd();
        String str = "Received SSL error: " + sslError;
        if (currentAd instanceof AppLovinAdBase) {
            c.C0103c a2 = this.f3293a.w().a((AppLovinAdBase) currentAd);
            a2.a(com.applovin.impl.sdk.d.b.H, str);
            a2.a();
        }
        this.f3294b.e("AdWebView", str + " for ad: " + currentAd);
    }

    @TargetApi(24)
    public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
        boolean hasGesture = ((Boolean) this.f3293a.a(c.e.Z0)).booleanValue() ? webResourceRequest.hasGesture() : true;
        Uri url = webResourceRequest.getUrl();
        if (url != null) {
            return a(webView, url.toString(), hasGesture);
        }
        this.f3294b.e("AdWebView", "No url found for request");
        return false;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        return a(webView, str, true);
    }
}
