package com.applovin.impl.adview;

import android.graphics.PointF;
import android.net.Uri;
import android.os.Bundle;
import com.applovin.impl.adview.i;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.q;
import com.tapjoy.TJAdUnitConstants;
import e.c.a.a.a;
import e.c.a.a.d;
import e.c.a.a.g;
import e.c.a.a.h;
import e.c.a.a.k;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class r extends m {
    private final Set<g> P = new HashSet();

    class a implements i.b {
        a() {
        }

        public void a() {
            r.this.handleCountdownStep();
        }

        public boolean b() {
            return r.this.shouldContinueFullLengthVideoCountdown();
        }
    }

    private void a() {
        if (isFullyWatched() && !this.P.isEmpty()) {
            q qVar = this.logger;
            qVar.d("InterstitialActivity", "Firing " + this.P.size() + " un-fired video progress trackers when video was completed.");
            a(this.P);
        }
    }

    private void a(a.d dVar) {
        a(dVar, d.UNSPECIFIED);
    }

    private void a(a.d dVar, d dVar2) {
        a(dVar, "", dVar2);
    }

    private void a(a.d dVar, String str) {
        a(dVar, str, d.UNSPECIFIED);
    }

    private void a(a.d dVar, String str, d dVar2) {
        if (isVastAd()) {
            a(((e.c.a.a.a) this.currentAd).a(dVar, str), dVar2);
        }
    }

    private void a(Set<g> set) {
        a(set, d.UNSPECIFIED);
    }

    private void a(Set<g> set, d dVar) {
        if (isVastAd() && set != null && !set.isEmpty()) {
            long seconds = TimeUnit.MILLISECONDS.toSeconds((long) this.videoView.getCurrentPosition());
            k E0 = b().E0();
            Uri a2 = E0 != null ? E0.a() : null;
            q qVar = this.logger;
            qVar.b("InterstitialActivity", "Firing " + set.size() + " tracker(s): " + set);
            e.c.a.a.i.a(set, seconds, a2, dVar, this.sdk);
        }
    }

    private e.c.a.a.a b() {
        if (this.currentAd instanceof e.c.a.a.a) {
            return (e.c.a.a.a) this.currentAd;
        }
        return null;
    }

    public void clickThroughFromVideo(PointF pointF) {
        super.clickThroughFromVideo(pointF);
        a(a.d.VIDEO_CLICK);
    }

    public void dismiss() {
        if (isVastAd()) {
            a(a.d.VIDEO, TJAdUnitConstants.String.CLOSE);
            a(a.d.COMPANION, TJAdUnitConstants.String.CLOSE);
        }
        super.dismiss();
    }

    public void handleCountdownStep() {
        if (isVastAd()) {
            long seconds = ((long) this.computedLengthSeconds) - TimeUnit.MILLISECONDS.toSeconds((long) (this.videoView.getDuration() - this.videoView.getCurrentPosition()));
            HashSet hashSet = new HashSet();
            for (g gVar : new HashSet(this.P)) {
                if (gVar.a(seconds, getVideoPercentViewed())) {
                    hashSet.add(gVar);
                    this.P.remove(gVar);
                }
            }
            a(hashSet);
        }
    }

    public void handleMediaError(String str) {
        a(a.d.ERROR, d.MEDIA_FILE_ERROR);
        super.handleMediaError(str);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (isVastAd()) {
            this.P.addAll(b().a(a.d.VIDEO, h.f6702a));
            a(a.d.IMPRESSION);
            a(a.d.VIDEO, "creativeView");
        }
    }

    public void playVideo() {
        this.countdownManager.a("PROGRESS_TRACKING", ((Long) this.sdk.a(c.e.z3)).longValue(), new a());
        super.playVideo();
    }

    public void showPoststitial() {
        if (isVastAd()) {
            a();
            if (!e.c.a.a.i.c(b())) {
                dismiss();
                return;
            } else if (!this.poststitialWasDisplayed) {
                a(a.d.COMPANION, "creativeView");
            } else {
                return;
            }
        }
        super.showPoststitial();
    }

    public void skipVideo() {
        a(a.d.VIDEO, "skip");
        super.skipVideo();
    }

    public void toggleMute() {
        String str;
        a.d dVar;
        super.toggleMute();
        if (this.videoMuted) {
            dVar = a.d.VIDEO;
            str = "mute";
        } else {
            dVar = a.d.VIDEO;
            str = "unmute";
        }
        a(dVar, str);
    }
}
