package com.applovin.impl.adview;

import android.content.Context;
import android.widget.VideoView;
import com.applovin.impl.sdk.k;

public class AppLovinVideoView extends VideoView implements t {
    public AppLovinVideoView(Context context, k kVar) {
        super(context, null, 0);
    }

    public void setVideoSize(int i2, int i3) {
        try {
            getHolder().setFixedSize(i2, i3);
            requestLayout();
            invalidate();
        } catch (Exception unused) {
        }
    }
}
