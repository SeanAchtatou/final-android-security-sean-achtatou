package com.applovin.impl.adview;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.net.Uri;
import android.view.Surface;
import android.view.TextureView;
import android.widget.FrameLayout;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.q;
import com.applovin.sdk.AppLovinSdkUtils;

class p extends FrameLayout implements TextureView.SurfaceTextureListener, t {

    /* renamed from: a  reason: collision with root package name */
    private final q f3412a;

    /* renamed from: b  reason: collision with root package name */
    private final TextureView f3413b;

    /* renamed from: c  reason: collision with root package name */
    private final MediaPlayer f3414c = new MediaPlayer();
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public final b f3415d;

    /* renamed from: e  reason: collision with root package name */
    private int f3416e;

    /* renamed from: f  reason: collision with root package name */
    private int f3417f;

    /* renamed from: g  reason: collision with root package name */
    private int f3418g;

    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ String f3419a;

        a(String str) {
            this.f3419a = str;
        }

        public void run() {
            p.this.f3415d.a(this.f3419a);
        }
    }

    interface b {
        void a(String str);
    }

    p(k kVar, Context context, b bVar) {
        super(context);
        this.f3412a = kVar.Z();
        this.f3415d = bVar;
        this.f3413b = new TextureView(context);
        this.f3413b.setLayoutParams(new FrameLayout.LayoutParams(-1, -1, 17));
        this.f3413b.setSurfaceTextureListener(this);
        addView(this.f3413b);
    }

    private void a(String str) {
        this.f3412a.e("TextureVideoView", str);
        AppLovinSdkUtils.runOnUiThreadDelayed(new a(str), 250);
    }

    public int getCurrentPosition() {
        return this.f3414c.getCurrentPosition();
    }

    public int getDuration() {
        return this.f3414c.getDuration();
    }

    public boolean isPlaying() {
        return this.f3414c.isPlaying();
    }

    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i2, int i3) {
        Surface surface = new Surface(surfaceTexture);
        try {
            this.f3414c.setSurface(surface);
            this.f3414c.setAudioStreamType(3);
            this.f3414c.prepareAsync();
        } catch (Throwable unused) {
            surface.release();
            a("Failed to prepare media player");
        }
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        return true;
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i2, int i3) {
    }

    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }

    public void pause() {
        this.f3414c.pause();
    }

    public void seekTo(int i2) {
        this.f3414c.seekTo(i2);
    }

    public void setOnCompletionListener(MediaPlayer.OnCompletionListener onCompletionListener) {
        this.f3414c.setOnCompletionListener(onCompletionListener);
    }

    public void setOnErrorListener(MediaPlayer.OnErrorListener onErrorListener) {
        this.f3414c.setOnErrorListener(onErrorListener);
    }

    public void setOnPreparedListener(MediaPlayer.OnPreparedListener onPreparedListener) {
        this.f3414c.setOnPreparedListener(onPreparedListener);
    }

    public void setVideoSize(int i2, int i3) {
        int i4;
        int i5;
        int i6;
        int d2 = com.applovin.impl.sdk.utils.p.d(getContext());
        int i7 = this.f3416e;
        if (i7 == 0) {
            i5 = this.f3413b.getWidth();
            i4 = this.f3413b.getHeight();
            this.f3416e = d2;
            this.f3417f = i5;
            this.f3418g = i4;
        } else if (d2 == i7) {
            i5 = this.f3417f;
            i4 = this.f3418g;
        } else {
            i5 = this.f3418g;
            i4 = this.f3417f;
        }
        float f2 = ((float) i3) / ((float) i2);
        float f3 = (float) i5;
        int i8 = (int) (f3 * f2);
        if (i4 >= i8) {
            i6 = i5;
        } else {
            i6 = (int) (((float) i4) / f2);
            i8 = i4;
        }
        try {
            Matrix matrix = new Matrix();
            this.f3413b.getTransform(matrix);
            matrix.setScale(((float) i6) / f3, ((float) i8) / ((float) i4));
            matrix.postTranslate((float) ((i5 - i6) / 2), (float) ((i4 - i8) / 2));
            this.f3413b.setTransform(matrix);
            invalidate();
            requestLayout();
        } catch (Throwable unused) {
            a("Failed to set video size to width: " + i2 + " height: " + i3);
        }
    }

    public void setVideoURI(Uri uri) {
        try {
            this.f3414c.setDataSource(uri.toString());
        } catch (Throwable th) {
            a("Failed to set video URI: " + uri + " at " + th);
        }
    }

    public void start() {
        this.f3414c.start();
    }

    public void stopPlayback() {
        this.f3414c.stop();
    }
}
