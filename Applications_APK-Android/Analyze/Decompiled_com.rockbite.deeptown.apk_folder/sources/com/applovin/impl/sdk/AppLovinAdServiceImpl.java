package com.applovin.impl.sdk;

import android.graphics.PointF;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.text.TextUtils;
import com.applovin.adview.AppLovinAdView;
import com.applovin.impl.adview.AdViewControllerImpl;
import com.applovin.impl.sdk.ad.NativeAdImpl;
import com.applovin.impl.sdk.ad.f;
import com.applovin.impl.sdk.ad.g;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.f;
import com.applovin.impl.sdk.network.e;
import com.applovin.impl.sdk.network.f;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.p;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdService;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinAdUpdateListener;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class AppLovinAdServiceImpl implements AppLovinAdService {
    public static String URI_LOAD_URL = "/adservice/load_url";
    public static String URI_NO_OP = "/adservice/no_op";
    public static String URI_TRACK_CLICK_IMMEDIATELY = "/adservice/track_click_now";
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final k f3830a;

    /* renamed from: b  reason: collision with root package name */
    private final q f3831b;

    /* renamed from: c  reason: collision with root package name */
    private final Handler f3832c = new Handler(Looper.getMainLooper());

    /* renamed from: d  reason: collision with root package name */
    private final Map<com.applovin.impl.sdk.ad.d, d> f3833d;

    /* renamed from: e  reason: collision with root package name */
    private final Object f3834e = new Object();

    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AppLovinAdLoadListener f3835a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ AppLovinAd f3836b;

        a(AppLovinAdServiceImpl appLovinAdServiceImpl, AppLovinAdLoadListener appLovinAdLoadListener, AppLovinAd appLovinAd) {
            this.f3835a = appLovinAdLoadListener;
            this.f3836b = appLovinAd;
        }

        public void run() {
            try {
                this.f3835a.adReceived(this.f3836b);
            } catch (Throwable th) {
                q.c("AppLovinAdService", "Unable to notify listener about a newly loaded ad", th);
            }
        }
    }

    class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AppLovinAdLoadListener f3837a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ int f3838b;

        b(AppLovinAdServiceImpl appLovinAdServiceImpl, AppLovinAdLoadListener appLovinAdLoadListener, int i2) {
            this.f3837a = appLovinAdLoadListener;
            this.f3838b = i2;
        }

        public void run() {
            try {
                this.f3837a.failedToReceiveAd(this.f3838b);
            } catch (Throwable th) {
                q.c("AppLovinAdService", "Unable to notify listener about ad load failure", th);
            }
        }
    }

    private class c implements AppLovinAdLoadListener {

        /* renamed from: a  reason: collision with root package name */
        private final d f3839a;

        private c(d dVar) {
            this.f3839a = dVar;
        }

        /* synthetic */ c(AppLovinAdServiceImpl appLovinAdServiceImpl, d dVar, a aVar) {
            this(dVar);
        }

        public void adReceived(AppLovinAd appLovinAd) {
            HashSet<AppLovinAdLoadListener> hashSet;
            com.applovin.impl.sdk.ad.d adZone = ((AppLovinAdBase) appLovinAd).getAdZone();
            if (!(appLovinAd instanceof g) && adZone.i()) {
                AppLovinAdServiceImpl.this.f3830a.s().adReceived(appLovinAd);
                appLovinAd = new g(adZone, AppLovinAdServiceImpl.this.f3830a);
            }
            synchronized (this.f3839a.f3841a) {
                hashSet = new HashSet<>(this.f3839a.f3843c);
                this.f3839a.f3843c.clear();
                this.f3839a.f3842b = false;
            }
            for (AppLovinAdLoadListener a2 : hashSet) {
                AppLovinAdServiceImpl.this.a(appLovinAd, a2);
            }
        }

        public void failedToReceiveAd(int i2) {
            HashSet<AppLovinAdLoadListener> hashSet;
            synchronized (this.f3839a.f3841a) {
                hashSet = new HashSet<>(this.f3839a.f3843c);
                this.f3839a.f3843c.clear();
                this.f3839a.f3842b = false;
            }
            for (AppLovinAdLoadListener a2 : hashSet) {
                AppLovinAdServiceImpl.this.a(i2, a2);
            }
        }
    }

    private static class d {

        /* renamed from: a  reason: collision with root package name */
        final Object f3841a;

        /* renamed from: b  reason: collision with root package name */
        boolean f3842b;

        /* renamed from: c  reason: collision with root package name */
        final Collection<AppLovinAdLoadListener> f3843c;

        private d() {
            this.f3841a = new Object();
            this.f3843c = new HashSet();
        }

        /* synthetic */ d(a aVar) {
            this();
        }

        public String toString() {
            return "AdLoadState{, isWaitingForAd=" + this.f3842b + ", pendingAdListeners=" + this.f3843c + '}';
        }
    }

    AppLovinAdServiceImpl(k kVar) {
        this.f3830a = kVar;
        this.f3831b = kVar.Z();
        this.f3833d = new HashMap(5);
        this.f3833d.put(com.applovin.impl.sdk.ad.d.c(kVar), new d(null));
        this.f3833d.put(com.applovin.impl.sdk.ad.d.d(kVar), new d(null));
        this.f3833d.put(com.applovin.impl.sdk.ad.d.e(kVar), new d(null));
        this.f3833d.put(com.applovin.impl.sdk.ad.d.f(kVar), new d(null));
        this.f3833d.put(com.applovin.impl.sdk.ad.d.g(kVar), new d(null));
    }

    private d a(com.applovin.impl.sdk.ad.d dVar) {
        d dVar2;
        synchronized (this.f3834e) {
            dVar2 = this.f3833d.get(dVar);
            if (dVar2 == null) {
                dVar2 = new d(null);
                this.f3833d.put(dVar, dVar2);
            }
        }
        return dVar2;
    }

    private String a(String str, long j2, int i2, String str2, boolean z) {
        try {
            if (!m.b(str)) {
                return null;
            }
            if (i2 < 0 || i2 > 100) {
                i2 = 0;
            }
            return Uri.parse(str).buildUpon().appendQueryParameter("et_s", Long.toString(j2)).appendQueryParameter(NativeAdImpl.QUERY_PARAM_VIDEO_PERCENT_VIEWED, Integer.toString(i2)).appendQueryParameter("vid_ts", str2).appendQueryParameter("uvs", Boolean.toString(z)).build().toString();
        } catch (Throwable th) {
            q qVar = this.f3831b;
            qVar.b("AppLovinAdService", "Unknown error parsing the video end url: " + str, th);
            return null;
        }
    }

    private String a(String str, long j2, long j3) {
        if (m.b(str)) {
            return Uri.parse(str).buildUpon().appendQueryParameter("et_ms", Long.toString(j2)).appendQueryParameter("vs_ms", Long.toString(j3)).build().toString();
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void a(int i2, AppLovinAdLoadListener appLovinAdLoadListener) {
        this.f3832c.post(new b(this, appLovinAdLoadListener, i2));
    }

    private void a(Uri uri, f fVar, AppLovinAdView appLovinAdView, AdViewControllerImpl adViewControllerImpl) {
        if (appLovinAdView != null) {
            if (p.a(appLovinAdView.getContext(), uri, this.f3830a)) {
                j.c(adViewControllerImpl.getAdViewEventListener(), fVar, appLovinAdView);
            }
            adViewControllerImpl.dismissInterstitialIfRequired();
            return;
        }
        this.f3831b.e("AppLovinAdService", "Unable to launch click - adView has been prematurely destroyed");
    }

    private void a(com.applovin.impl.sdk.ad.d dVar, c cVar) {
        AppLovinAd appLovinAd = (AppLovinAd) this.f3830a.s().e(dVar);
        if (appLovinAd != null) {
            q qVar = this.f3831b;
            qVar.b("AppLovinAdService", "Using pre-loaded ad: " + appLovinAd + " for " + dVar);
            cVar.adReceived(appLovinAd);
        } else {
            a(new f.t(dVar, cVar, this.f3830a), cVar);
        }
        if (dVar.i() && appLovinAd == null) {
            return;
        }
        if (dVar.j() || (appLovinAd != null && dVar.g() > 0)) {
            this.f3830a.s().i(dVar);
        }
    }

    private void a(com.applovin.impl.sdk.ad.d dVar, AppLovinAdLoadListener appLovinAdLoadListener) {
        q qVar;
        String str;
        String str2;
        if (dVar == null) {
            throw new IllegalArgumentException("No zone specified");
        } else if (appLovinAdLoadListener != null) {
            q Z = this.f3830a.Z();
            Z.b("AppLovinAdService", "Loading next ad of zone {" + dVar + "}...");
            d a2 = a(dVar);
            synchronized (a2.f3841a) {
                a2.f3843c.add(appLovinAdLoadListener);
                if (!a2.f3842b) {
                    this.f3831b.b("AppLovinAdService", "Loading next ad...");
                    a2.f3842b = true;
                    c cVar = new c(this, a2, null);
                    if (!dVar.h()) {
                        this.f3831b.b("AppLovinAdService", "Task merge not necessary.");
                    } else if (this.f3830a.s().a(dVar, cVar)) {
                        qVar = this.f3831b;
                        str = "AppLovinAdService";
                        str2 = "Attaching load listener to initial preload task...";
                    } else {
                        this.f3831b.b("AppLovinAdService", "Skipped attach of initial preload callback.");
                    }
                    a(dVar, cVar);
                } else {
                    qVar = this.f3831b;
                    str = "AppLovinAdService";
                    str2 = "Already waiting on an ad load...";
                }
                qVar.b(str, str2);
            }
        } else {
            throw new IllegalArgumentException("No callback specified");
        }
    }

    private void a(com.applovin.impl.sdk.d.a aVar) {
        if (m.b(aVar.a())) {
            String b2 = p.b(aVar.a());
            String b3 = m.b(aVar.b()) ? p.b(aVar.b()) : null;
            e m = this.f3830a.m();
            f.b k2 = com.applovin.impl.sdk.network.f.k();
            k2.a(b2);
            k2.b(b3);
            k2.a(false);
            m.a(k2.a());
            return;
        }
        this.f3831b.d("AppLovinAdService", "Requested a postback dispatch for a null URL; nothing to do...");
    }

    private void a(f.c cVar, AppLovinAdLoadListener appLovinAdLoadListener) {
        if (!this.f3830a.H()) {
            q.h("AppLovinSdk", "Attempted to load ad before SDK initialization. Please wait until after the SDK has initialized, e.g. AppLovinSdk.initializeSdk(Context, SdkInitializationListener).");
        }
        this.f3830a.z();
        this.f3830a.j().a(cVar, f.y.b.MAIN);
    }

    /* access modifiers changed from: private */
    public void a(AppLovinAd appLovinAd, AppLovinAdLoadListener appLovinAdLoadListener) {
        this.f3832c.post(new a(this, appLovinAdLoadListener, appLovinAd));
    }

    private void a(List<com.applovin.impl.sdk.d.a> list) {
        if (list != null && !list.isEmpty()) {
            for (com.applovin.impl.sdk.d.a a2 : list) {
                a(a2);
            }
        }
    }

    public void addAdUpdateListener(AppLovinAdUpdateListener appLovinAdUpdateListener) {
    }

    public void addAdUpdateListener(AppLovinAdUpdateListener appLovinAdUpdateListener, AppLovinAdSize appLovinAdSize) {
    }

    public AppLovinAd dequeueAd(com.applovin.impl.sdk.ad.d dVar) {
        AppLovinAd appLovinAd = (AppLovinAd) this.f3830a.s().d(dVar);
        q qVar = this.f3831b;
        qVar.b("AppLovinAdService", "Dequeued ad: " + appLovinAd + " for zone: " + dVar + "...");
        return appLovinAd;
    }

    public String getBidToken() {
        if (!((Boolean) this.f3830a.a(c.e.c0)).booleanValue()) {
            return "NONE";
        }
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        String a2 = this.f3830a.n().a();
        StrictMode.setThreadPolicy(allowThreadDiskReads);
        return a2;
    }

    public boolean hasPreloadedAd(AppLovinAdSize appLovinAdSize) {
        return this.f3830a.s().g(com.applovin.impl.sdk.ad.d.a(appLovinAdSize, AppLovinAdType.REGULAR, this.f3830a));
    }

    public boolean hasPreloadedAdForZoneId(String str) {
        if (TextUtils.isEmpty(str)) {
            q.i("AppLovinAdService", "Unable to check if ad is preloaded - invalid zone id");
            return false;
        }
        return this.f3830a.s().g(com.applovin.impl.sdk.ad.d.a(str, this.f3830a));
    }

    public void loadNextAd(AppLovinAdSize appLovinAdSize, AppLovinAdLoadListener appLovinAdLoadListener) {
        a(com.applovin.impl.sdk.ad.d.a(appLovinAdSize, AppLovinAdType.REGULAR, this.f3830a), appLovinAdLoadListener);
    }

    public void loadNextAd(String str, AppLovinAdSize appLovinAdSize, AppLovinAdLoadListener appLovinAdLoadListener) {
        q qVar = this.f3831b;
        qVar.b("AppLovinAdService", "Loading next ad of zone {" + str + "} with size " + appLovinAdSize);
        a(com.applovin.impl.sdk.ad.d.a(appLovinAdSize, AppLovinAdType.REGULAR, str, this.f3830a), appLovinAdLoadListener);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r11v20, types: [com.applovin.impl.sdk.f$v] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadNextAdForAdToken(java.lang.String r11, com.applovin.sdk.AppLovinAdLoadListener r12) {
        /*
            r10 = this;
            if (r11 == 0) goto L_0x0007
            java.lang.String r11 = r11.trim()
            goto L_0x0008
        L_0x0007:
            r11 = 0
        L_0x0008:
            boolean r0 = android.text.TextUtils.isEmpty(r11)
            r1 = -8
            java.lang.String r2 = "AppLovinAdService"
            if (r0 == 0) goto L_0x001a
            java.lang.String r11 = "Invalid ad token specified"
            com.applovin.impl.sdk.q.i(r2, r11)
            r10.a(r1, r12)
            return
        L_0x001a:
            com.applovin.impl.sdk.ad.c r0 = new com.applovin.impl.sdk.ad.c
            com.applovin.impl.sdk.k r3 = r10.f3830a
            r0.<init>(r11, r3)
            com.applovin.impl.sdk.ad.c$a r11 = r0.b()
            com.applovin.impl.sdk.ad.c$a r3 = com.applovin.impl.sdk.ad.c.a.REGULAR
            if (r11 != r3) goto L_0x004b
            com.applovin.impl.sdk.q r11 = r10.f3831b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "Loading next ad for token: "
            r1.append(r3)
            r1.append(r0)
            java.lang.String r1 = r1.toString()
            r11.b(r2, r1)
            com.applovin.impl.sdk.f$v r11 = new com.applovin.impl.sdk.f$v
            com.applovin.impl.sdk.k r1 = r10.f3830a
            r11.<init>(r0, r12, r1)
        L_0x0046:
            r10.a(r11, r12)
            goto L_0x00ed
        L_0x004b:
            com.applovin.impl.sdk.ad.c$a r11 = r0.b()
            com.applovin.impl.sdk.ad.c$a r3 = com.applovin.impl.sdk.ad.c.a.AD_RESPONSE_JSON
            if (r11 != r3) goto L_0x00d6
            org.json.JSONObject r5 = r0.d()
            if (r5 == 0) goto L_0x00bf
            com.applovin.impl.sdk.k r11 = r10.f3830a
            com.applovin.impl.sdk.utils.h.d(r5, r11)
            com.applovin.impl.sdk.k r11 = r10.f3830a
            com.applovin.impl.sdk.utils.h.b(r5, r11)
            com.applovin.impl.sdk.k r11 = r10.f3830a
            com.applovin.impl.sdk.utils.h.a(r5, r11)
            org.json.JSONArray r11 = new org.json.JSONArray
            r11.<init>()
            com.applovin.impl.sdk.k r1 = r10.f3830a
            java.lang.String r3 = "ads"
            org.json.JSONArray r11 = com.applovin.impl.sdk.utils.i.b(r5, r3, r11, r1)
            int r11 = r11.length()
            if (r11 <= 0) goto L_0x00a3
            com.applovin.impl.sdk.q r11 = r10.f3831b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "Rendering ad for token: "
            r1.append(r3)
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            r11.b(r2, r0)
            com.applovin.impl.sdk.k r11 = r10.f3830a
            com.applovin.impl.sdk.ad.d r6 = com.applovin.impl.sdk.utils.p.a(r5, r11)
            com.applovin.impl.sdk.f$z r11 = new com.applovin.impl.sdk.f$z
            com.applovin.impl.sdk.ad.b r7 = com.applovin.impl.sdk.ad.b.DECODED_AD_TOKEN_JSON
            com.applovin.impl.sdk.k r9 = r10.f3830a
            r4 = r11
            r8 = r12
            r4.<init>(r5, r6, r7, r8, r9)
            goto L_0x0046
        L_0x00a3:
            com.applovin.impl.sdk.q r11 = r10.f3831b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "No ad returned from the server for token: "
            r1.append(r3)
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            r11.e(r2, r0)
            r11 = 204(0xcc, float:2.86E-43)
            r12.failedToReceiveAd(r11)
            goto L_0x00ed
        L_0x00bf:
            com.applovin.impl.sdk.q r11 = r10.f3831b
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Unable to retrieve ad response JSON from token: "
            r3.append(r4)
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r11.e(r2, r0)
            goto L_0x00ea
        L_0x00d6:
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r3 = "Invalid ad token specified: "
            r11.append(r3)
            r11.append(r0)
            java.lang.String r11 = r11.toString()
            com.applovin.impl.sdk.q.i(r2, r11)
        L_0x00ea:
            r12.failedToReceiveAd(r1)
        L_0x00ed:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.AppLovinAdServiceImpl.loadNextAdForAdToken(java.lang.String, com.applovin.sdk.AppLovinAdLoadListener):void");
    }

    public void loadNextAdForZoneId(String str, AppLovinAdLoadListener appLovinAdLoadListener) {
        if (!TextUtils.isEmpty(str)) {
            q qVar = this.f3831b;
            qVar.b("AppLovinAdService", "Loading next ad of zone {" + str + "}");
            a(com.applovin.impl.sdk.ad.d.a(str, this.f3830a), appLovinAdLoadListener);
            return;
        }
        throw new IllegalArgumentException("No zone id specified");
    }

    public void loadNextAdForZoneIds(List<String> list, AppLovinAdLoadListener appLovinAdLoadListener) {
        List<String> a2 = com.applovin.impl.sdk.utils.e.a(list);
        if (a2 == null || a2.isEmpty()) {
            q.i("AppLovinAdService", "No zones were provided");
            a(-7, appLovinAdLoadListener);
            return;
        }
        q qVar = this.f3831b;
        qVar.b("AppLovinAdService", "Loading next ad for zones: " + a2);
        a(new f.s(a2, appLovinAdLoadListener, this.f3830a), appLovinAdLoadListener);
    }

    public void loadNextIncentivizedAd(String str, AppLovinAdLoadListener appLovinAdLoadListener) {
        q qVar = this.f3831b;
        qVar.b("AppLovinAdService", "Loading next incentivized ad of zone {" + str + "}");
        a(com.applovin.impl.sdk.ad.d.c(str, this.f3830a), appLovinAdLoadListener);
    }

    public void preloadAd(AppLovinAdSize appLovinAdSize) {
        this.f3830a.z();
        this.f3830a.s().i(com.applovin.impl.sdk.ad.d.a(appLovinAdSize, AppLovinAdType.REGULAR, this.f3830a));
    }

    public void preloadAdForZoneId(String str) {
        if (TextUtils.isEmpty(str)) {
            q.i("AppLovinAdService", "Unable to preload ad for invalid zone identifier");
            return;
        }
        com.applovin.impl.sdk.ad.d a2 = com.applovin.impl.sdk.ad.d.a(str, this.f3830a);
        this.f3830a.s().h(a2);
        this.f3830a.s().i(a2);
    }

    public void preloadAds(com.applovin.impl.sdk.ad.d dVar) {
        this.f3830a.s().h(dVar);
        int g2 = dVar.g();
        if (g2 == 0 && this.f3830a.s().b(dVar)) {
            g2 = 1;
        }
        this.f3830a.s().b(dVar, g2);
    }

    public void removeAdUpdateListener(AppLovinAdUpdateListener appLovinAdUpdateListener, AppLovinAdSize appLovinAdSize) {
    }

    public String toString() {
        return "AppLovinAdService{adLoadStates=" + this.f3833d + '}';
    }

    public void trackAndLaunchClick(AppLovinAd appLovinAd, AppLovinAdView appLovinAdView, AdViewControllerImpl adViewControllerImpl, Uri uri, PointF pointF) {
        if (appLovinAd == null) {
            this.f3831b.e("AppLovinAdService", "Unable to track ad view click. No ad specified");
            return;
        }
        this.f3831b.b("AppLovinAdService", "Tracking click on an ad...");
        com.applovin.impl.sdk.ad.f fVar = (com.applovin.impl.sdk.ad.f) appLovinAd;
        a(fVar.a(pointF));
        a(uri, fVar, appLovinAdView, adViewControllerImpl);
    }

    public void trackAndLaunchVideoClick(AppLovinAd appLovinAd, AppLovinAdView appLovinAdView, Uri uri, PointF pointF) {
        if (appLovinAd == null) {
            this.f3831b.e("AppLovinAdService", "Unable to track video click. No ad specified");
            return;
        }
        this.f3831b.b("AppLovinAdService", "Tracking VIDEO click on an ad...");
        a(((com.applovin.impl.sdk.ad.f) appLovinAd).b(pointF));
        p.a(appLovinAdView.getContext(), uri, this.f3830a);
    }

    public void trackFullScreenAdClosed(com.applovin.impl.sdk.ad.f fVar, long j2, long j3) {
        if (fVar == null) {
            this.f3831b.e("AppLovinAdService", "Unable to track ad closed. No ad specified.");
            return;
        }
        this.f3831b.b("AppLovinAdService", "Tracking ad closed...");
        List<com.applovin.impl.sdk.d.a> P = fVar.P();
        if (P == null || P.isEmpty()) {
            q qVar = this.f3831b;
            qVar.d("AppLovinAdService", "Unable to track ad closed for AD #" + fVar.getAdIdNumber() + ". Missing ad close tracking URL." + fVar.getAdIdNumber());
            return;
        }
        for (com.applovin.impl.sdk.d.a next : P) {
            String a2 = a(next.a(), j2, j3);
            String a3 = a(next.b(), j2, j3);
            if (m.b(a2)) {
                a(new com.applovin.impl.sdk.d.a(a2, a3));
            } else {
                q qVar2 = this.f3831b;
                qVar2.e("AppLovinAdService", "Failed to parse url: " + next.a());
            }
        }
    }

    public void trackImpression(com.applovin.impl.sdk.ad.f fVar) {
        if (fVar == null) {
            this.f3831b.e("AppLovinAdService", "Unable to track impression click. No ad specified");
            return;
        }
        this.f3831b.b("AppLovinAdService", "Tracking impression on ad...");
        a(fVar.Q());
    }

    public void trackVideoEnd(com.applovin.impl.sdk.ad.f fVar, long j2, int i2, boolean z) {
        q qVar = this.f3831b;
        if (fVar == null) {
            qVar.e("AppLovinAdService", "Unable to track video end. No ad specified");
            return;
        }
        qVar.b("AppLovinAdService", "Tracking video end on ad...");
        List<com.applovin.impl.sdk.d.a> O = fVar.O();
        if (O == null || O.isEmpty()) {
            q qVar2 = this.f3831b;
            qVar2.d("AppLovinAdService", "Unable to submit persistent postback for AD #" + fVar.getAdIdNumber() + ". Missing video end tracking URL.");
            return;
        }
        String l = Long.toString(System.currentTimeMillis());
        for (com.applovin.impl.sdk.d.a next : O) {
            if (m.b(next.a())) {
                long j3 = j2;
                int i3 = i2;
                String str = l;
                boolean z2 = z;
                String a2 = a(next.a(), j3, i3, str, z2);
                String a3 = a(next.b(), j3, i3, str, z2);
                if (a2 != null) {
                    a(new com.applovin.impl.sdk.d.a(a2, a3));
                } else {
                    q qVar3 = this.f3831b;
                    qVar3.e("AppLovinAdService", "Failed to parse url: " + next.a());
                }
            } else {
                this.f3831b.d("AppLovinAdService", "Requested a postback dispatch for an empty video end URL; nothing to do...");
            }
        }
    }
}
