package com.applovin.impl.sdk.network;

import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.k;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class b<T> {

    /* renamed from: a  reason: collision with root package name */
    private String f4318a;

    /* renamed from: b  reason: collision with root package name */
    private String f4319b;

    /* renamed from: c  reason: collision with root package name */
    private Map<String, String> f4320c;

    /* renamed from: d  reason: collision with root package name */
    private Map<String, String> f4321d;

    /* renamed from: e  reason: collision with root package name */
    private final JSONObject f4322e;

    /* renamed from: f  reason: collision with root package name */
    private String f4323f;

    /* renamed from: g  reason: collision with root package name */
    private final T f4324g;

    /* renamed from: h  reason: collision with root package name */
    private final boolean f4325h;

    /* renamed from: i  reason: collision with root package name */
    private final int f4326i;

    /* renamed from: j  reason: collision with root package name */
    private int f4327j;

    /* renamed from: k  reason: collision with root package name */
    private final int f4328k;
    private final int l;
    private final boolean m;
    private final boolean n;

    public static class a<T> {

        /* renamed from: a  reason: collision with root package name */
        String f4329a;

        /* renamed from: b  reason: collision with root package name */
        String f4330b;

        /* renamed from: c  reason: collision with root package name */
        String f4331c;

        /* renamed from: d  reason: collision with root package name */
        Map<String, String> f4332d;

        /* renamed from: e  reason: collision with root package name */
        Map<String, String> f4333e;

        /* renamed from: f  reason: collision with root package name */
        JSONObject f4334f;

        /* renamed from: g  reason: collision with root package name */
        T f4335g;

        /* renamed from: h  reason: collision with root package name */
        boolean f4336h = true;

        /* renamed from: i  reason: collision with root package name */
        int f4337i = 1;

        /* renamed from: j  reason: collision with root package name */
        int f4338j;

        /* renamed from: k  reason: collision with root package name */
        int f4339k;
        boolean l;
        boolean m;

        public a(k kVar) {
            this.f4338j = ((Integer) kVar.a(c.e.s2)).intValue();
            this.f4339k = ((Integer) kVar.a(c.e.r2)).intValue();
            this.l = ((Boolean) kVar.a(c.e.G3)).booleanValue();
            this.f4332d = new HashMap();
        }

        public a<T> a(int i2) {
            this.f4337i = i2;
            return this;
        }

        public a<T> a(Object obj) {
            this.f4335g = obj;
            return this;
        }

        public a<T> a(String str) {
            this.f4330b = str;
            return this;
        }

        public a<T> a(Map<String, String> map) {
            this.f4332d = map;
            return this;
        }

        public a<T> a(JSONObject jSONObject) {
            this.f4334f = jSONObject;
            return this;
        }

        public a<T> a(boolean z) {
            this.l = z;
            return this;
        }

        public b<T> a() {
            return new b<>(this);
        }

        public a<T> b(int i2) {
            this.f4338j = i2;
            return this;
        }

        public a<T> b(String str) {
            this.f4329a = str;
            return this;
        }

        public a<T> b(boolean z) {
            this.m = z;
            return this;
        }

        public a<T> c(int i2) {
            this.f4339k = i2;
            return this;
        }

        public a<T> c(String str) {
            this.f4331c = str;
            return this;
        }
    }

    protected b(a<T> aVar) {
        this.f4318a = aVar.f4330b;
        this.f4319b = aVar.f4329a;
        this.f4320c = aVar.f4332d;
        this.f4321d = aVar.f4333e;
        this.f4322e = aVar.f4334f;
        this.f4323f = aVar.f4331c;
        this.f4324g = aVar.f4335g;
        this.f4325h = aVar.f4336h;
        int i2 = aVar.f4337i;
        this.f4326i = i2;
        this.f4327j = i2;
        this.f4328k = aVar.f4338j;
        this.l = aVar.f4339k;
        this.m = aVar.l;
        this.n = aVar.m;
    }

    public static <T> a<T> a(k kVar) {
        return new a<>(kVar);
    }

    public String a() {
        return this.f4318a;
    }

    public void a(int i2) {
        this.f4327j = i2;
    }

    public void a(String str) {
        this.f4318a = str;
    }

    public String b() {
        return this.f4319b;
    }

    public void b(String str) {
        this.f4319b = str;
    }

    public Map<String, String> c() {
        return this.f4320c;
    }

    public Map<String, String> d() {
        return this.f4321d;
    }

    public JSONObject e() {
        return this.f4322e;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public boolean equals(java.lang.Object r5) {
        /*
            r4 = this;
            r0 = 1
            if (r4 != r5) goto L_0x0004
            return r0
        L_0x0004:
            boolean r1 = r5 instanceof com.applovin.impl.sdk.network.b
            r2 = 0
            if (r1 != 0) goto L_0x000a
            return r2
        L_0x000a:
            com.applovin.impl.sdk.network.b r5 = (com.applovin.impl.sdk.network.b) r5
            java.lang.String r1 = r4.f4318a
            if (r1 == 0) goto L_0x0019
            java.lang.String r3 = r5.f4318a
            boolean r1 = r1.equals(r3)
            if (r1 != 0) goto L_0x001e
            goto L_0x001d
        L_0x0019:
            java.lang.String r1 = r5.f4318a
            if (r1 == 0) goto L_0x001e
        L_0x001d:
            return r2
        L_0x001e:
            java.util.Map<java.lang.String, java.lang.String> r1 = r4.f4320c
            if (r1 == 0) goto L_0x002b
            java.util.Map<java.lang.String, java.lang.String> r3 = r5.f4320c
            boolean r1 = r1.equals(r3)
            if (r1 != 0) goto L_0x0030
            goto L_0x002f
        L_0x002b:
            java.util.Map<java.lang.String, java.lang.String> r1 = r5.f4320c
            if (r1 == 0) goto L_0x0030
        L_0x002f:
            return r2
        L_0x0030:
            java.util.Map<java.lang.String, java.lang.String> r1 = r4.f4321d
            if (r1 == 0) goto L_0x003d
            java.util.Map<java.lang.String, java.lang.String> r3 = r5.f4321d
            boolean r1 = r1.equals(r3)
            if (r1 != 0) goto L_0x0042
            goto L_0x0041
        L_0x003d:
            java.util.Map<java.lang.String, java.lang.String> r1 = r5.f4321d
            if (r1 == 0) goto L_0x0042
        L_0x0041:
            return r2
        L_0x0042:
            java.lang.String r1 = r4.f4323f
            if (r1 == 0) goto L_0x004f
            java.lang.String r3 = r5.f4323f
            boolean r1 = r1.equals(r3)
            if (r1 != 0) goto L_0x0054
            goto L_0x0053
        L_0x004f:
            java.lang.String r1 = r5.f4323f
            if (r1 == 0) goto L_0x0054
        L_0x0053:
            return r2
        L_0x0054:
            java.lang.String r1 = r4.f4319b
            if (r1 == 0) goto L_0x0061
            java.lang.String r3 = r5.f4319b
            boolean r1 = r1.equals(r3)
            if (r1 != 0) goto L_0x0066
            goto L_0x0065
        L_0x0061:
            java.lang.String r1 = r5.f4319b
            if (r1 == 0) goto L_0x0066
        L_0x0065:
            return r2
        L_0x0066:
            org.json.JSONObject r1 = r4.f4322e
            if (r1 == 0) goto L_0x0073
            org.json.JSONObject r3 = r5.f4322e
            boolean r1 = r1.equals(r3)
            if (r1 != 0) goto L_0x0078
            goto L_0x0077
        L_0x0073:
            org.json.JSONObject r1 = r5.f4322e
            if (r1 == 0) goto L_0x0078
        L_0x0077:
            return r2
        L_0x0078:
            T r1 = r4.f4324g
            if (r1 == 0) goto L_0x0085
            T r3 = r5.f4324g
            boolean r1 = r1.equals(r3)
            if (r1 != 0) goto L_0x008a
            goto L_0x0089
        L_0x0085:
            T r1 = r5.f4324g
            if (r1 == 0) goto L_0x008a
        L_0x0089:
            return r2
        L_0x008a:
            boolean r1 = r4.f4325h
            boolean r3 = r5.f4325h
            if (r1 == r3) goto L_0x0091
            return r2
        L_0x0091:
            int r1 = r4.f4326i
            int r3 = r5.f4326i
            if (r1 == r3) goto L_0x0098
            return r2
        L_0x0098:
            int r1 = r4.f4327j
            int r3 = r5.f4327j
            if (r1 == r3) goto L_0x009f
            return r2
        L_0x009f:
            int r1 = r4.f4328k
            int r3 = r5.f4328k
            if (r1 == r3) goto L_0x00a6
            return r2
        L_0x00a6:
            int r1 = r4.l
            int r3 = r5.l
            if (r1 == r3) goto L_0x00ad
            return r2
        L_0x00ad:
            boolean r1 = r4.m
            boolean r3 = r5.m
            if (r1 == r3) goto L_0x00b4
            return r2
        L_0x00b4:
            boolean r1 = r4.n
            boolean r5 = r5.n
            if (r1 == r5) goto L_0x00bb
            return r2
        L_0x00bb:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.network.b.equals(java.lang.Object):boolean");
    }

    public String f() {
        return this.f4323f;
    }

    public T g() {
        return this.f4324g;
    }

    public boolean h() {
        return this.f4325h;
    }

    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        String str = this.f4318a;
        int i2 = 0;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.f4323f;
        int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.f4319b;
        int hashCode4 = (hashCode3 + (str3 != null ? str3.hashCode() : 0)) * 31;
        T t = this.f4324g;
        if (t != null) {
            i2 = t.hashCode();
        }
        int i3 = ((((((((((((((hashCode4 + i2) * 31) + (this.f4325h ? 1 : 0)) * 31) + this.f4326i) * 31) + this.f4327j) * 31) + this.f4328k) * 31) + this.l) * 31) + (this.m ? 1 : 0)) * 31) + (this.n ? 1 : 0);
        Map<String, String> map = this.f4320c;
        if (map != null) {
            i3 = (i3 * 31) + map.hashCode();
        }
        Map<String, String> map2 = this.f4321d;
        if (map2 != null) {
            i3 = (i3 * 31) + map2.hashCode();
        }
        JSONObject jSONObject = this.f4322e;
        if (jSONObject == null) {
            return i3;
        }
        char[] charArray = jSONObject.toString().toCharArray();
        Arrays.sort(charArray);
        return (i3 * 31) + new String(charArray).hashCode();
    }

    public int i() {
        return this.f4326i - this.f4327j;
    }

    public int j() {
        return this.f4327j;
    }

    public int k() {
        return this.f4328k;
    }

    public int l() {
        return this.l;
    }

    public boolean m() {
        return this.m;
    }

    public boolean n() {
        return this.n;
    }

    public String toString() {
        return "HttpRequest {endpoint=" + this.f4318a + ", backupEndpoint=" + this.f4323f + ", httpMethod=" + this.f4319b + ", httpHeaders=" + this.f4321d + ", body=" + this.f4322e + ", emptyResponse=" + ((Object) this.f4324g) + ", requiresResponse=" + this.f4325h + ", initialRetryAttempts=" + this.f4326i + ", retryAttemptsLeft=" + this.f4327j + ", timeoutMillis=" + this.f4328k + ", retryDelayMillis=" + this.l + ", encodingEnabled=" + this.m + ", trackConnectionSpeed=" + this.n + '}';
    }
}
