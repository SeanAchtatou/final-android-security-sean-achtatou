package com.applovin.impl.sdk.a;

import android.app.AlertDialog;
import android.content.DialogInterface;
import com.applovin.impl.adview.m;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.k;

public class b {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final k f3913a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final m f3914b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public AlertDialog f3915c;

    class a implements Runnable {
        a() {
        }

        public void run() {
            if (b.this.f3915c != null) {
                b.this.f3915c.dismiss();
            }
        }
    }

    /* renamed from: com.applovin.impl.sdk.a.b$b  reason: collision with other inner class name */
    class C0100b implements Runnable {

        /* renamed from: com.applovin.impl.sdk.a.b$b$a */
        class a implements DialogInterface.OnClickListener {
            a() {
            }

            public void onClick(DialogInterface dialogInterface, int i2) {
                b.this.f3914b.continueVideo();
                b.this.f3914b.resumeReportRewardTask();
            }
        }

        /* renamed from: com.applovin.impl.sdk.a.b$b$b  reason: collision with other inner class name */
        class C0101b implements DialogInterface.OnClickListener {
            C0101b() {
            }

            public void onClick(DialogInterface dialogInterface, int i2) {
                b.this.f3914b.skipVideo();
                b.this.f3914b.resumeReportRewardTask();
            }
        }

        C0100b() {
        }

        public void run() {
            AlertDialog.Builder builder = new AlertDialog.Builder(b.this.f3914b);
            builder.setTitle((CharSequence) b.this.f3913a.a(c.e.L0));
            builder.setMessage((CharSequence) b.this.f3913a.a(c.e.M0));
            builder.setCancelable(false);
            builder.setPositiveButton((CharSequence) b.this.f3913a.a(c.e.O0), new a());
            builder.setNegativeButton((CharSequence) b.this.f3913a.a(c.e.N0), new C0101b());
            AlertDialog unused = b.this.f3915c = builder.show();
        }
    }

    class c implements Runnable {

        class a implements DialogInterface.OnClickListener {
            a() {
            }

            public void onClick(DialogInterface dialogInterface, int i2) {
                b.this.f3914b.dismiss();
            }
        }

        c() {
        }

        public void run() {
            AlertDialog.Builder builder = new AlertDialog.Builder(b.this.f3914b);
            builder.setTitle((CharSequence) b.this.f3913a.a(c.e.Q0));
            builder.setMessage((CharSequence) b.this.f3913a.a(c.e.R0));
            builder.setCancelable(false);
            builder.setPositiveButton((CharSequence) b.this.f3913a.a(c.e.T0), (DialogInterface.OnClickListener) null);
            builder.setNegativeButton((CharSequence) b.this.f3913a.a(c.e.S0), new a());
            AlertDialog unused = b.this.f3915c = builder.show();
        }
    }

    public b(m mVar, k kVar) {
        this.f3913a = kVar;
        this.f3914b = mVar;
    }

    public void a() {
        this.f3914b.runOnUiThread(new a());
    }

    public void b() {
        this.f3914b.runOnUiThread(new C0100b());
    }

    public void c() {
        this.f3914b.runOnUiThread(new c());
    }

    public boolean d() {
        AlertDialog alertDialog = this.f3915c;
        if (alertDialog != null) {
            return alertDialog.isShowing();
        }
        return false;
    }
}
