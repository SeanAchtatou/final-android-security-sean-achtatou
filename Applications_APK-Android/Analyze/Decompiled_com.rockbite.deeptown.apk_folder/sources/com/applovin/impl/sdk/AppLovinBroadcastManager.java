package com.applovin.impl.sdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class AppLovinBroadcastManager {

    /* renamed from: f  reason: collision with root package name */
    private static AppLovinBroadcastManager f3844f;

    /* renamed from: g  reason: collision with root package name */
    private static final Object f3845g = new Object();

    /* renamed from: a  reason: collision with root package name */
    private final Context f3846a;

    /* renamed from: b  reason: collision with root package name */
    private final HashMap<BroadcastReceiver, ArrayList<c>> f3847b = new HashMap<>();

    /* renamed from: c  reason: collision with root package name */
    private final HashMap<String, ArrayList<c>> f3848c = new HashMap<>();

    /* renamed from: d  reason: collision with root package name */
    private final ArrayList<b> f3849d = new ArrayList<>();

    /* renamed from: e  reason: collision with root package name */
    private final Handler f3850e = new a(Looper.getMainLooper());

    class a extends Handler {
        a(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message message) {
            if (message.what == 1) {
                AppLovinBroadcastManager.this.a();
            } else {
                super.handleMessage(message);
            }
        }
    }

    private static class b {

        /* renamed from: a  reason: collision with root package name */
        final Intent f3852a;

        /* renamed from: b  reason: collision with root package name */
        final List<c> f3853b;

        b(Intent intent, List<c> list) {
            this.f3852a = intent;
            this.f3853b = list;
        }
    }

    private static class c {

        /* renamed from: a  reason: collision with root package name */
        final IntentFilter f3854a;

        /* renamed from: b  reason: collision with root package name */
        final BroadcastReceiver f3855b;

        /* renamed from: c  reason: collision with root package name */
        boolean f3856c;

        /* renamed from: d  reason: collision with root package name */
        boolean f3857d;

        c(IntentFilter intentFilter, BroadcastReceiver broadcastReceiver) {
            this.f3854a = intentFilter;
            this.f3855b = broadcastReceiver;
        }
    }

    private AppLovinBroadcastManager(Context context) {
        this.f3846a = context;
    }

    private List<c> a(Intent intent) {
        synchronized (this.f3847b) {
            String action = intent.getAction();
            String resolveTypeIfNeeded = intent.resolveTypeIfNeeded(this.f3846a.getContentResolver());
            Uri data = intent.getData();
            String scheme = intent.getScheme();
            Set<String> categories = intent.getCategories();
            List<c> list = this.f3848c.get(action);
            if (list == null) {
                return null;
            }
            ArrayList<c> arrayList = null;
            for (c cVar : list) {
                if (!cVar.f3856c) {
                    c cVar2 = cVar;
                    if (cVar.f3854a.match(action, resolveTypeIfNeeded, scheme, data, categories, "AppLovinBroadcastManager") >= 0) {
                        ArrayList arrayList2 = arrayList == null ? new ArrayList() : arrayList;
                        arrayList2.add(cVar2);
                        cVar2.f3856c = true;
                        arrayList = arrayList2;
                    }
                }
            }
            if (arrayList == null) {
                return null;
            }
            for (c cVar3 : arrayList) {
                cVar3.f3856c = false;
            }
            return arrayList;
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001c, code lost:
        if (r2 >= r0) goto L_0x0000;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001e, code lost:
        r3 = r1[r2];
        r4 = r3.f3853b.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002a, code lost:
        if (r4.hasNext() == false) goto L_0x0040;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002c, code lost:
        r5 = r4.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0034, code lost:
        if (r5.f3857d != false) goto L_0x0026;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0036, code lost:
        r5.f3855b.onReceive(r8.f3846a, r3.f3852a);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0040, code lost:
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001a, code lost:
        r0 = r1.length;
        r2 = 0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a() {
        /*
            r8 = this;
        L_0x0000:
            java.util.HashMap<android.content.BroadcastReceiver, java.util.ArrayList<com.applovin.impl.sdk.AppLovinBroadcastManager$c>> r0 = r8.f3847b
            monitor-enter(r0)
            java.util.ArrayList<com.applovin.impl.sdk.AppLovinBroadcastManager$b> r1 = r8.f3849d     // Catch:{ all -> 0x0043 }
            int r1 = r1.size()     // Catch:{ all -> 0x0043 }
            if (r1 > 0) goto L_0x000d
            monitor-exit(r0)     // Catch:{ all -> 0x0043 }
            return
        L_0x000d:
            com.applovin.impl.sdk.AppLovinBroadcastManager$b[] r1 = new com.applovin.impl.sdk.AppLovinBroadcastManager.b[r1]     // Catch:{ all -> 0x0043 }
            java.util.ArrayList<com.applovin.impl.sdk.AppLovinBroadcastManager$b> r2 = r8.f3849d     // Catch:{ all -> 0x0043 }
            r2.toArray(r1)     // Catch:{ all -> 0x0043 }
            java.util.ArrayList<com.applovin.impl.sdk.AppLovinBroadcastManager$b> r2 = r8.f3849d     // Catch:{ all -> 0x0043 }
            r2.clear()     // Catch:{ all -> 0x0043 }
            monitor-exit(r0)     // Catch:{ all -> 0x0043 }
            int r0 = r1.length
            r2 = 0
        L_0x001c:
            if (r2 >= r0) goto L_0x0000
            r3 = r1[r2]
            java.util.List<com.applovin.impl.sdk.AppLovinBroadcastManager$c> r4 = r3.f3853b
            java.util.Iterator r4 = r4.iterator()
        L_0x0026:
            boolean r5 = r4.hasNext()
            if (r5 == 0) goto L_0x0040
            java.lang.Object r5 = r4.next()
            com.applovin.impl.sdk.AppLovinBroadcastManager$c r5 = (com.applovin.impl.sdk.AppLovinBroadcastManager.c) r5
            boolean r6 = r5.f3857d
            if (r6 != 0) goto L_0x0026
            android.content.BroadcastReceiver r5 = r5.f3855b
            android.content.Context r6 = r8.f3846a
            android.content.Intent r7 = r3.f3852a
            r5.onReceive(r6, r7)
            goto L_0x0026
        L_0x0040:
            int r2 = r2 + 1
            goto L_0x001c
        L_0x0043:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0043 }
            goto L_0x0047
        L_0x0046:
            throw r1
        L_0x0047:
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.AppLovinBroadcastManager.a():void");
    }

    public static AppLovinBroadcastManager getInstance(Context context) {
        AppLovinBroadcastManager appLovinBroadcastManager;
        synchronized (f3845g) {
            if (f3844f == null) {
                f3844f = new AppLovinBroadcastManager(context.getApplicationContext());
            }
            appLovinBroadcastManager = f3844f;
        }
        return appLovinBroadcastManager;
    }

    public void registerReceiver(BroadcastReceiver broadcastReceiver, IntentFilter intentFilter) {
        synchronized (this.f3847b) {
            c cVar = new c(intentFilter, broadcastReceiver);
            ArrayList arrayList = this.f3847b.get(broadcastReceiver);
            if (arrayList == null) {
                arrayList = new ArrayList(1);
                this.f3847b.put(broadcastReceiver, arrayList);
            }
            arrayList.add(cVar);
            Iterator<String> actionsIterator = intentFilter.actionsIterator();
            while (actionsIterator.hasNext()) {
                String next = actionsIterator.next();
                ArrayList arrayList2 = this.f3848c.get(next);
                if (arrayList2 == null) {
                    arrayList2 = new ArrayList(1);
                    this.f3848c.put(next, arrayList2);
                }
                arrayList2.add(cVar);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0025, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean sendBroadcast(android.content.Intent r5) {
        /*
            r4 = this;
            java.util.HashMap<android.content.BroadcastReceiver, java.util.ArrayList<com.applovin.impl.sdk.AppLovinBroadcastManager$c>> r0 = r4.f3847b
            monitor-enter(r0)
            java.util.List r1 = r4.a(r5)     // Catch:{ all -> 0x0026 }
            if (r1 != 0) goto L_0x000c
            r5 = 0
            monitor-exit(r0)     // Catch:{ all -> 0x0026 }
            return r5
        L_0x000c:
            java.util.ArrayList<com.applovin.impl.sdk.AppLovinBroadcastManager$b> r2 = r4.f3849d     // Catch:{ all -> 0x0026 }
            com.applovin.impl.sdk.AppLovinBroadcastManager$b r3 = new com.applovin.impl.sdk.AppLovinBroadcastManager$b     // Catch:{ all -> 0x0026 }
            r3.<init>(r5, r1)     // Catch:{ all -> 0x0026 }
            r2.add(r3)     // Catch:{ all -> 0x0026 }
            android.os.Handler r5 = r4.f3850e     // Catch:{ all -> 0x0026 }
            r1 = 1
            boolean r5 = r5.hasMessages(r1)     // Catch:{ all -> 0x0026 }
            if (r5 != 0) goto L_0x0024
            android.os.Handler r5 = r4.f3850e     // Catch:{ all -> 0x0026 }
            r5.sendEmptyMessage(r1)     // Catch:{ all -> 0x0026 }
        L_0x0024:
            monitor-exit(r0)     // Catch:{ all -> 0x0026 }
            return r1
        L_0x0026:
            r5 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0026 }
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.AppLovinBroadcastManager.sendBroadcast(android.content.Intent):boolean");
    }

    public void sendBroadcastSync(Intent intent) {
        List<c> a2 = a(intent);
        if (a2 != null) {
            for (c next : a2) {
                if (!next.f3857d) {
                    next.f3855b.onReceive(this.f3846a, intent);
                }
            }
        }
    }

    public void sendBroadcastSyncWithPendingBroadcasts(Intent intent) {
        if (sendBroadcast(intent)) {
            a();
        }
    }

    public void unregisterReceiver(BroadcastReceiver broadcastReceiver) {
        synchronized (this.f3847b) {
            List<c> remove = this.f3847b.remove(broadcastReceiver);
            if (remove != null) {
                for (c cVar : remove) {
                    cVar.f3857d = true;
                    Iterator<String> actionsIterator = cVar.f3854a.actionsIterator();
                    while (actionsIterator.hasNext()) {
                        String next = actionsIterator.next();
                        List list = this.f3848c.get(next);
                        if (list != null) {
                            Iterator it = list.iterator();
                            while (it.hasNext()) {
                                if (((c) it.next()).f3855b == broadcastReceiver) {
                                    cVar.f3857d = true;
                                    it.remove();
                                }
                            }
                            if (list.size() <= 0) {
                                this.f3848c.remove(next);
                            }
                        }
                    }
                }
            }
        }
    }
}
