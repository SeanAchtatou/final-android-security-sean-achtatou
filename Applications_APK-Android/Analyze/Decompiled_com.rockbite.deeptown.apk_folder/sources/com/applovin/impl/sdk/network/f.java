package com.applovin.impl.sdk.network;

import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.utils.i;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;

public class f {

    /* renamed from: a  reason: collision with root package name */
    private String f4352a;

    /* renamed from: b  reason: collision with root package name */
    private String f4353b;

    /* renamed from: c  reason: collision with root package name */
    private String f4354c;

    /* renamed from: d  reason: collision with root package name */
    private Map<String, String> f4355d;

    /* renamed from: e  reason: collision with root package name */
    private Map<String, String> f4356e;

    /* renamed from: f  reason: collision with root package name */
    private Map<String, Object> f4357f;

    /* renamed from: g  reason: collision with root package name */
    private boolean f4358g;

    /* renamed from: h  reason: collision with root package name */
    private int f4359h;

    public static class b {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public String f4360a;
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public String f4361b;
        /* access modifiers changed from: private */

        /* renamed from: c  reason: collision with root package name */
        public Map<String, String> f4362c;
        /* access modifiers changed from: private */

        /* renamed from: d  reason: collision with root package name */
        public Map<String, String> f4363d;
        /* access modifiers changed from: private */

        /* renamed from: e  reason: collision with root package name */
        public Map<String, Object> f4364e;
        /* access modifiers changed from: private */

        /* renamed from: f  reason: collision with root package name */
        public boolean f4365f;

        public b a(String str) {
            this.f4360a = str;
            return this;
        }

        public b a(Map<String, String> map) {
            this.f4362c = map;
            return this;
        }

        public b a(boolean z) {
            this.f4365f = z;
            return this;
        }

        public f a() {
            return new f(this);
        }

        public b b(String str) {
            this.f4361b = str;
            return this;
        }

        public b b(Map<String, String> map) {
            this.f4363d = map;
            return this;
        }

        public b c(Map<String, Object> map) {
            this.f4364e = map;
            return this;
        }
    }

    private f(b bVar) {
        this.f4352a = UUID.randomUUID().toString();
        this.f4353b = bVar.f4360a;
        this.f4354c = bVar.f4361b;
        this.f4355d = bVar.f4362c;
        this.f4356e = bVar.f4363d;
        this.f4357f = bVar.f4364e;
        this.f4358g = bVar.f4365f;
        this.f4359h = 0;
    }

    f(JSONObject jSONObject, k kVar) throws Exception {
        String b2 = i.b(jSONObject, "uniqueId", UUID.randomUUID().toString(), kVar);
        String string = jSONObject.getString("targetUrl");
        String b3 = i.b(jSONObject, "backupUrl", "", kVar);
        int i2 = jSONObject.getInt("attemptNumber");
        Map<String, String> a2 = i.a(jSONObject, "parameters") ? i.a(jSONObject.getJSONObject("parameters")) : Collections.emptyMap();
        Map<String, String> a3 = i.a(jSONObject, "httpHeaders") ? i.a(jSONObject.getJSONObject("httpHeaders")) : Collections.emptyMap();
        Map<String, Object> b4 = i.a(jSONObject, "requestBody") ? i.b(jSONObject.getJSONObject("requestBody")) : Collections.emptyMap();
        this.f4352a = b2;
        this.f4353b = string;
        this.f4354c = b3;
        this.f4355d = a2;
        this.f4356e = a3;
        this.f4357f = b4;
        this.f4358g = jSONObject.optBoolean("isEncodingEnabled", false);
        this.f4359h = i2;
    }

    public static b k() {
        return new b();
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.f4353b;
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return this.f4354c;
    }

    /* access modifiers changed from: package-private */
    public Map<String, String> c() {
        return this.f4355d;
    }

    /* access modifiers changed from: package-private */
    public Map<String, String> d() {
        return this.f4356e;
    }

    /* access modifiers changed from: package-private */
    public Map<String, Object> e() {
        return this.f4357f;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || f.class != obj.getClass()) {
            return false;
        }
        return this.f4352a.equals(((f) obj).f4352a);
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        return this.f4358g;
    }

    /* access modifiers changed from: package-private */
    public int g() {
        return this.f4359h;
    }

    /* access modifiers changed from: package-private */
    public void h() {
        this.f4359h++;
    }

    public int hashCode() {
        return this.f4352a.hashCode();
    }

    /* access modifiers changed from: package-private */
    public void i() {
        HashMap hashMap = new HashMap();
        Map<String, String> map = this.f4355d;
        if (map != null) {
            hashMap.putAll(map);
        }
        hashMap.put("postback_ts", String.valueOf(System.currentTimeMillis()));
        this.f4355d = hashMap;
    }

    /* access modifiers changed from: package-private */
    public JSONObject j() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("uniqueId", this.f4352a);
        jSONObject.put("targetUrl", this.f4353b);
        jSONObject.put("backupUrl", this.f4354c);
        jSONObject.put("isEncodingEnabled", this.f4358g);
        jSONObject.put("attemptNumber", this.f4359h);
        Map<String, String> map = this.f4355d;
        if (map != null) {
            jSONObject.put("parameters", new JSONObject(map));
        }
        Map<String, String> map2 = this.f4356e;
        if (map2 != null) {
            jSONObject.put("httpHeaders", new JSONObject(map2));
        }
        Map<String, Object> map3 = this.f4357f;
        if (map3 != null) {
            jSONObject.put("requestBody", new JSONObject(map3));
        }
        return jSONObject;
    }

    public String toString() {
        return "PostbackRequest{uniqueId='" + this.f4352a + '\'' + "targetUrl='" + this.f4353b + '\'' + ", backupUrl='" + this.f4354c + '\'' + ", attemptNumber=" + this.f4359h + ", isEncodingEnabled=" + this.f4358g + '}';
    }
}
