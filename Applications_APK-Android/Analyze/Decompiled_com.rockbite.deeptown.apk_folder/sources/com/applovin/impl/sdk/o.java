package com.applovin.impl.sdk;

import android.content.Context;
import android.net.Uri;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.d.e;
import com.applovin.impl.sdk.d.g;
import com.applovin.impl.sdk.utils.m;
import com.appsflyer.share.Constants;
import com.google.ads.mediation.inmobi.InMobiNetworkValues;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class o {

    /* renamed from: a  reason: collision with root package name */
    private final k f4366a;

    /* renamed from: b  reason: collision with root package name */
    private final q f4367b;

    /* renamed from: c  reason: collision with root package name */
    private final Object f4368c = new Object();

    o(k kVar) {
        this.f4366a = kVar;
        this.f4367b = kVar.Z();
    }

    private long a() {
        long longValue = ((Long) this.f4366a.a(c.e.C0)).longValue();
        if (longValue < 0 || !b()) {
            return -1;
        }
        return longValue;
    }

    private long a(long j2) {
        return j2 / 1048576;
    }

    private void a(long j2, Context context) {
        q qVar;
        String str;
        if (b()) {
            long intValue = (long) ((Integer) this.f4366a.a(c.e.D0)).intValue();
            if (intValue == -1) {
                qVar = this.f4367b;
                str = "Cache has no maximum size set; skipping drop...";
            } else {
                int i2 = (a(j2) > intValue ? 1 : (a(j2) == intValue ? 0 : -1));
                qVar = this.f4367b;
                if (i2 > 0) {
                    qVar.b("FileManager", "Cache has exceeded maximum size; dropping...");
                    for (File b2 : e(context)) {
                        b(b2);
                    }
                    this.f4366a.k().a(g.f4080i);
                    return;
                }
                str = "Cache is present but under size limit; not dropping...";
            }
            qVar.b("FileManager", str);
        }
    }

    private boolean a(File file, String str, List<String> list, boolean z, e eVar) {
        if (file == null || !file.exists() || file.isDirectory()) {
            ByteArrayOutputStream a2 = a(str, list, z);
            if (!(eVar == null || a2 == null)) {
                eVar.a((long) a2.size());
            }
            return a(a2, file);
        }
        q qVar = this.f4367b;
        qVar.b("FileManager", "File exists for " + str);
        if (eVar == null) {
            return true;
        }
        eVar.b(file.length());
        return true;
    }

    private boolean b() {
        return ((Boolean) this.f4366a.a(c.e.B0)).booleanValue();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.utils.p.a(java.io.Closeable, com.applovin.impl.sdk.k):void
     arg types: [java.io.FileOutputStream, com.applovin.impl.sdk.k]
     candidates:
      com.applovin.impl.sdk.utils.p.a(android.view.View, com.applovin.impl.sdk.k):android.app.Activity
      com.applovin.impl.sdk.utils.p.a(java.io.File, int):android.graphics.Bitmap
      com.applovin.impl.sdk.utils.p.a(android.content.Context, android.view.View):android.view.View
      com.applovin.impl.sdk.utils.p.a(org.json.JSONObject, com.applovin.impl.sdk.k):com.applovin.impl.sdk.ad.d
      com.applovin.impl.sdk.utils.p.a(com.applovin.sdk.AppLovinAd, com.applovin.impl.sdk.k):com.applovin.sdk.AppLovinAd
      com.applovin.impl.sdk.utils.p.a(java.lang.Object, com.applovin.impl.sdk.k):java.lang.Object
      com.applovin.impl.sdk.utils.p.a(java.lang.Class, java.lang.String):java.lang.reflect.Field
      com.applovin.impl.sdk.utils.p.a(java.util.List<java.lang.String>, com.applovin.impl.sdk.k):java.util.List<java.lang.Class>
      com.applovin.impl.sdk.utils.p.a(java.net.HttpURLConnection, com.applovin.impl.sdk.k):void
      com.applovin.impl.sdk.utils.p.a(long, long):boolean
      com.applovin.impl.sdk.utils.p.a(android.view.View, android.app.Activity):boolean
      com.applovin.impl.sdk.utils.p.a(android.view.View, android.view.View):boolean
      com.applovin.impl.sdk.utils.p.a(java.lang.String, java.util.List<java.lang.String>):boolean
      com.applovin.impl.sdk.utils.p.a(java.io.Closeable, com.applovin.impl.sdk.k):void */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:16:0x0038=Splitter:B:16:0x0038, B:22:0x0048=Splitter:B:22:0x0048} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean b(java.io.ByteArrayOutputStream r5, java.io.File r6) {
        /*
            r4 = this;
            com.applovin.impl.sdk.q r0 = r4.f4367b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Writing resource to filesystem: "
            r1.append(r2)
            java.lang.String r2 = r6.getName()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.lang.String r2 = "FileManager"
            r0.b(r2, r1)
            java.lang.Object r0 = r4.f4368c
            monitor-enter(r0)
            r1 = 0
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0047, all -> 0x0037 }
            r2.<init>(r6)     // Catch:{ IOException -> 0x0047, all -> 0x0037 }
            r5.writeTo(r2)     // Catch:{ IOException -> 0x0034, all -> 0x0031 }
            r5 = 1
            com.applovin.impl.sdk.k r6 = r4.f4366a     // Catch:{ all -> 0x002f }
            com.applovin.impl.sdk.utils.p.a(r2, r6)     // Catch:{ all -> 0x002f }
            goto L_0x0055
        L_0x002f:
            r5 = move-exception
            goto L_0x005e
        L_0x0031:
            r5 = move-exception
            r1 = r2
            goto L_0x0038
        L_0x0034:
            r5 = move-exception
            r1 = r2
            goto L_0x0048
        L_0x0037:
            r5 = move-exception
        L_0x0038:
            com.applovin.impl.sdk.q r6 = r4.f4367b     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = "FileManager"
            java.lang.String r3 = "Unknown failure to write file."
            r6.b(r2, r3, r5)     // Catch:{ all -> 0x0057 }
            com.applovin.impl.sdk.k r5 = r4.f4366a     // Catch:{ all -> 0x002f }
        L_0x0043:
            com.applovin.impl.sdk.utils.p.a(r1, r5)     // Catch:{ all -> 0x002f }
            goto L_0x0054
        L_0x0047:
            r5 = move-exception
        L_0x0048:
            com.applovin.impl.sdk.q r6 = r4.f4367b     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = "FileManager"
            java.lang.String r3 = "Unable to write data to file."
            r6.b(r2, r3, r5)     // Catch:{ all -> 0x0057 }
            com.applovin.impl.sdk.k r5 = r4.f4366a     // Catch:{ all -> 0x002f }
            goto L_0x0043
        L_0x0054:
            r5 = 0
        L_0x0055:
            monitor-exit(r0)     // Catch:{ all -> 0x002f }
            return r5
        L_0x0057:
            r5 = move-exception
            com.applovin.impl.sdk.k r6 = r4.f4366a     // Catch:{ all -> 0x002f }
            com.applovin.impl.sdk.utils.p.a(r1, r6)     // Catch:{ all -> 0x002f }
            throw r5     // Catch:{ all -> 0x002f }
        L_0x005e:
            monitor-exit(r0)     // Catch:{ all -> 0x002f }
            goto L_0x0061
        L_0x0060:
            throw r5
        L_0x0061:
            goto L_0x0060
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.o.b(java.io.ByteArrayOutputStream, java.io.File):boolean");
    }

    private boolean b(File file) {
        boolean delete;
        q qVar = this.f4367b;
        qVar.b("FileManager", "Removing file " + file.getName() + " from filesystem...");
        synchronized (this.f4368c) {
            try {
                delete = file.delete();
            } catch (Exception e2) {
                q qVar2 = this.f4367b;
                qVar2.b("FileManager", "Failed to remove file " + file.getName() + " from filesystem!", e2);
                return false;
            } catch (Throwable th) {
                throw th;
            }
        }
        return delete;
    }

    private long c(Context context) {
        long j2;
        long a2 = a();
        long seconds = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
        synchronized (this.f4368c) {
            j2 = 0;
            for (File next : e(context)) {
                boolean z = true;
                if (!(a2 != -1) || seconds - TimeUnit.MILLISECONDS.toSeconds(next.lastModified()) <= a2) {
                    z = false;
                } else {
                    q qVar = this.f4367b;
                    qVar.b("FileManager", "File " + next.getName() + " has expired, removing...");
                    b(next);
                }
                if (z) {
                    this.f4366a.k().a(g.f4079h);
                } else {
                    j2 += next.length();
                }
            }
        }
        return j2;
    }

    private boolean d(Context context) {
        if (com.applovin.impl.sdk.utils.g.e() || com.applovin.impl.sdk.utils.g.a("android.permission.WRITE_EXTERNAL_STORAGE", context)) {
            return true;
        }
        q.i("FileManager", "Application lacks required WRITE_EXTERNAL_STORAGE manifest permission.");
        return false;
    }

    private List<File> e(Context context) {
        List<File> asList;
        File f2 = f(context);
        if (!f2.isDirectory()) {
            return Collections.emptyList();
        }
        synchronized (this.f4368c) {
            asList = Arrays.asList(f2.listFiles());
        }
        return asList;
    }

    private File f(Context context) {
        String str = (String) this.f4366a.a(c.e.A0);
        return "external".equals(str) ? d(context) ? new File(context.getExternalFilesDir(null), "al") : new File(context.getCacheDir(), "al") : "file".equals(str) ? new File(context.getFilesDir(), "al") : new File(context.getCacheDir(), "al");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.utils.p.a(java.io.Closeable, com.applovin.impl.sdk.k):void
     arg types: [java.io.ByteArrayOutputStream, com.applovin.impl.sdk.k]
     candidates:
      com.applovin.impl.sdk.utils.p.a(android.view.View, com.applovin.impl.sdk.k):android.app.Activity
      com.applovin.impl.sdk.utils.p.a(java.io.File, int):android.graphics.Bitmap
      com.applovin.impl.sdk.utils.p.a(android.content.Context, android.view.View):android.view.View
      com.applovin.impl.sdk.utils.p.a(org.json.JSONObject, com.applovin.impl.sdk.k):com.applovin.impl.sdk.ad.d
      com.applovin.impl.sdk.utils.p.a(com.applovin.sdk.AppLovinAd, com.applovin.impl.sdk.k):com.applovin.sdk.AppLovinAd
      com.applovin.impl.sdk.utils.p.a(java.lang.Object, com.applovin.impl.sdk.k):java.lang.Object
      com.applovin.impl.sdk.utils.p.a(java.lang.Class, java.lang.String):java.lang.reflect.Field
      com.applovin.impl.sdk.utils.p.a(java.util.List<java.lang.String>, com.applovin.impl.sdk.k):java.util.List<java.lang.Class>
      com.applovin.impl.sdk.utils.p.a(java.net.HttpURLConnection, com.applovin.impl.sdk.k):void
      com.applovin.impl.sdk.utils.p.a(long, long):boolean
      com.applovin.impl.sdk.utils.p.a(android.view.View, android.app.Activity):boolean
      com.applovin.impl.sdk.utils.p.a(android.view.View, android.view.View):boolean
      com.applovin.impl.sdk.utils.p.a(java.lang.String, java.util.List<java.lang.String>):boolean
      com.applovin.impl.sdk.utils.p.a(java.io.Closeable, com.applovin.impl.sdk.k):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.utils.p.a(java.io.Closeable, com.applovin.impl.sdk.k):void
     arg types: [java.io.FileInputStream, com.applovin.impl.sdk.k]
     candidates:
      com.applovin.impl.sdk.utils.p.a(android.view.View, com.applovin.impl.sdk.k):android.app.Activity
      com.applovin.impl.sdk.utils.p.a(java.io.File, int):android.graphics.Bitmap
      com.applovin.impl.sdk.utils.p.a(android.content.Context, android.view.View):android.view.View
      com.applovin.impl.sdk.utils.p.a(org.json.JSONObject, com.applovin.impl.sdk.k):com.applovin.impl.sdk.ad.d
      com.applovin.impl.sdk.utils.p.a(com.applovin.sdk.AppLovinAd, com.applovin.impl.sdk.k):com.applovin.sdk.AppLovinAd
      com.applovin.impl.sdk.utils.p.a(java.lang.Object, com.applovin.impl.sdk.k):java.lang.Object
      com.applovin.impl.sdk.utils.p.a(java.lang.Class, java.lang.String):java.lang.reflect.Field
      com.applovin.impl.sdk.utils.p.a(java.util.List<java.lang.String>, com.applovin.impl.sdk.k):java.util.List<java.lang.Class>
      com.applovin.impl.sdk.utils.p.a(java.net.HttpURLConnection, com.applovin.impl.sdk.k):void
      com.applovin.impl.sdk.utils.p.a(long, long):boolean
      com.applovin.impl.sdk.utils.p.a(android.view.View, android.app.Activity):boolean
      com.applovin.impl.sdk.utils.p.a(android.view.View, android.view.View):boolean
      com.applovin.impl.sdk.utils.p.a(java.lang.String, java.util.List<java.lang.String>):boolean
      com.applovin.impl.sdk.utils.p.a(java.io.Closeable, com.applovin.impl.sdk.k):void */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0050, code lost:
        r9 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0052, code lost:
        r3 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0054, code lost:
        r9 = e;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x003d */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0050 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:7:0x0028] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:43:0x0092=Splitter:B:43:0x0092, B:27:0x0058=Splitter:B:27:0x0058, B:35:0x006a=Splitter:B:35:0x006a} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.io.ByteArrayOutputStream a(java.io.File r9) {
        /*
            r8 = this;
            r0 = 0
            if (r9 != 0) goto L_0x0004
            return r0
        L_0x0004:
            com.applovin.impl.sdk.q r1 = r8.f4367b
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Reading resource from filesystem: "
            r2.append(r3)
            java.lang.String r3 = r9.getName()
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            java.lang.String r3 = "FileManager"
            r1.b(r3, r2)
            java.lang.Object r1 = r8.f4368c
            monitor-enter(r1)
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x0090, IOException -> 0x0068, all -> 0x0056 }
            r2.<init>(r9)     // Catch:{ FileNotFoundException -> 0x0090, IOException -> 0x0068, all -> 0x0056 }
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x0052, all -> 0x0050 }
            r3.<init>()     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x0052, all -> 0x0050 }
            r4 = 1024(0x400, float:1.435E-42)
            byte[] r4 = new byte[r4]     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x0052, all -> 0x0050 }
        L_0x0031:
            int r5 = r4.length     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x0052, all -> 0x0050 }
            r6 = 0
            int r5 = r2.read(r4, r6, r5)     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x0052, all -> 0x0050 }
            if (r5 < 0) goto L_0x0049
            r3.write(r4, r6, r5)     // Catch:{ Exception -> 0x003d, all -> 0x0050 }
            goto L_0x0031
        L_0x003d:
            com.applovin.impl.sdk.k r4 = r8.f4366a     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x0052, all -> 0x0050 }
            com.applovin.impl.sdk.utils.p.a(r3, r4)     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x0052, all -> 0x0050 }
            com.applovin.impl.sdk.k r9 = r8.f4366a     // Catch:{ all -> 0x00b8 }
            com.applovin.impl.sdk.utils.p.a(r2, r9)     // Catch:{ all -> 0x00b8 }
            monitor-exit(r1)     // Catch:{ all -> 0x00b8 }
            return r0
        L_0x0049:
            com.applovin.impl.sdk.k r9 = r8.f4366a     // Catch:{ all -> 0x00b8 }
            com.applovin.impl.sdk.utils.p.a(r2, r9)     // Catch:{ all -> 0x00b8 }
            monitor-exit(r1)     // Catch:{ all -> 0x00b8 }
            return r3
        L_0x0050:
            r9 = move-exception
            goto L_0x0058
        L_0x0052:
            r3 = move-exception
            goto L_0x006a
        L_0x0054:
            r9 = move-exception
            goto L_0x0092
        L_0x0056:
            r9 = move-exception
            r2 = r0
        L_0x0058:
            com.applovin.impl.sdk.q r3 = r8.f4367b     // Catch:{ all -> 0x00b1 }
            java.lang.String r4 = "FileManager"
            java.lang.String r5 = "Unknown failure to read file."
            r3.b(r4, r5, r9)     // Catch:{ all -> 0x00b1 }
            com.applovin.impl.sdk.k r9 = r8.f4366a     // Catch:{ all -> 0x00b8 }
            com.applovin.impl.sdk.utils.p.a(r2, r9)     // Catch:{ all -> 0x00b8 }
            monitor-exit(r1)     // Catch:{ all -> 0x00b8 }
            return r0
        L_0x0068:
            r3 = move-exception
            r2 = r0
        L_0x006a:
            com.applovin.impl.sdk.q r4 = r8.f4367b     // Catch:{ all -> 0x00b1 }
            java.lang.String r5 = "FileManager"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x00b1 }
            r6.<init>()     // Catch:{ all -> 0x00b1 }
            java.lang.String r7 = "Failed to read file: "
            r6.append(r7)     // Catch:{ all -> 0x00b1 }
            java.lang.String r9 = r9.getName()     // Catch:{ all -> 0x00b1 }
            r6.append(r9)     // Catch:{ all -> 0x00b1 }
            r6.append(r3)     // Catch:{ all -> 0x00b1 }
            java.lang.String r9 = r6.toString()     // Catch:{ all -> 0x00b1 }
            r4.b(r5, r9)     // Catch:{ all -> 0x00b1 }
            com.applovin.impl.sdk.k r9 = r8.f4366a     // Catch:{ all -> 0x00b8 }
            com.applovin.impl.sdk.utils.p.a(r2, r9)     // Catch:{ all -> 0x00b8 }
            monitor-exit(r1)     // Catch:{ all -> 0x00b8 }
            return r0
        L_0x0090:
            r9 = move-exception
            r2 = r0
        L_0x0092:
            com.applovin.impl.sdk.q r3 = r8.f4367b     // Catch:{ all -> 0x00b1 }
            java.lang.String r4 = "FileManager"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x00b1 }
            r5.<init>()     // Catch:{ all -> 0x00b1 }
            java.lang.String r6 = "File not found. "
            r5.append(r6)     // Catch:{ all -> 0x00b1 }
            r5.append(r9)     // Catch:{ all -> 0x00b1 }
            java.lang.String r9 = r5.toString()     // Catch:{ all -> 0x00b1 }
            r3.c(r4, r9)     // Catch:{ all -> 0x00b1 }
            com.applovin.impl.sdk.k r9 = r8.f4366a     // Catch:{ all -> 0x00b8 }
            com.applovin.impl.sdk.utils.p.a(r2, r9)     // Catch:{ all -> 0x00b8 }
            monitor-exit(r1)     // Catch:{ all -> 0x00b8 }
            return r0
        L_0x00b1:
            r9 = move-exception
            com.applovin.impl.sdk.k r0 = r8.f4366a     // Catch:{ all -> 0x00b8 }
            com.applovin.impl.sdk.utils.p.a(r2, r0)     // Catch:{ all -> 0x00b8 }
            throw r9     // Catch:{ all -> 0x00b8 }
        L_0x00b8:
            r9 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00b8 }
            goto L_0x00bc
        L_0x00bb:
            throw r9
        L_0x00bc:
            goto L_0x00bb
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.o.a(java.io.File):java.io.ByteArrayOutputStream");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v4, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v5, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v6, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v3, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v7, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v8, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v9, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v10, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v15, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v4, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v18, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.utils.p.a(java.io.Closeable, com.applovin.impl.sdk.k):void
     arg types: [java.io.ByteArrayOutputStream, com.applovin.impl.sdk.k]
     candidates:
      com.applovin.impl.sdk.utils.p.a(android.view.View, com.applovin.impl.sdk.k):android.app.Activity
      com.applovin.impl.sdk.utils.p.a(java.io.File, int):android.graphics.Bitmap
      com.applovin.impl.sdk.utils.p.a(android.content.Context, android.view.View):android.view.View
      com.applovin.impl.sdk.utils.p.a(org.json.JSONObject, com.applovin.impl.sdk.k):com.applovin.impl.sdk.ad.d
      com.applovin.impl.sdk.utils.p.a(com.applovin.sdk.AppLovinAd, com.applovin.impl.sdk.k):com.applovin.sdk.AppLovinAd
      com.applovin.impl.sdk.utils.p.a(java.lang.Object, com.applovin.impl.sdk.k):java.lang.Object
      com.applovin.impl.sdk.utils.p.a(java.lang.Class, java.lang.String):java.lang.reflect.Field
      com.applovin.impl.sdk.utils.p.a(java.util.List<java.lang.String>, com.applovin.impl.sdk.k):java.util.List<java.lang.Class>
      com.applovin.impl.sdk.utils.p.a(java.net.HttpURLConnection, com.applovin.impl.sdk.k):void
      com.applovin.impl.sdk.utils.p.a(long, long):boolean
      com.applovin.impl.sdk.utils.p.a(android.view.View, android.app.Activity):boolean
      com.applovin.impl.sdk.utils.p.a(android.view.View, android.view.View):boolean
      com.applovin.impl.sdk.utils.p.a(java.lang.String, java.util.List<java.lang.String>):boolean
      com.applovin.impl.sdk.utils.p.a(java.io.Closeable, com.applovin.impl.sdk.k):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.utils.p.a(java.io.Closeable, com.applovin.impl.sdk.k):void
     arg types: [java.io.InputStream, com.applovin.impl.sdk.k]
     candidates:
      com.applovin.impl.sdk.utils.p.a(android.view.View, com.applovin.impl.sdk.k):android.app.Activity
      com.applovin.impl.sdk.utils.p.a(java.io.File, int):android.graphics.Bitmap
      com.applovin.impl.sdk.utils.p.a(android.content.Context, android.view.View):android.view.View
      com.applovin.impl.sdk.utils.p.a(org.json.JSONObject, com.applovin.impl.sdk.k):com.applovin.impl.sdk.ad.d
      com.applovin.impl.sdk.utils.p.a(com.applovin.sdk.AppLovinAd, com.applovin.impl.sdk.k):com.applovin.sdk.AppLovinAd
      com.applovin.impl.sdk.utils.p.a(java.lang.Object, com.applovin.impl.sdk.k):java.lang.Object
      com.applovin.impl.sdk.utils.p.a(java.lang.Class, java.lang.String):java.lang.reflect.Field
      com.applovin.impl.sdk.utils.p.a(java.util.List<java.lang.String>, com.applovin.impl.sdk.k):java.util.List<java.lang.Class>
      com.applovin.impl.sdk.utils.p.a(java.net.HttpURLConnection, com.applovin.impl.sdk.k):void
      com.applovin.impl.sdk.utils.p.a(long, long):boolean
      com.applovin.impl.sdk.utils.p.a(android.view.View, android.app.Activity):boolean
      com.applovin.impl.sdk.utils.p.a(android.view.View, android.view.View):boolean
      com.applovin.impl.sdk.utils.p.a(java.lang.String, java.util.List<java.lang.String>):boolean
      com.applovin.impl.sdk.utils.p.a(java.io.Closeable, com.applovin.impl.sdk.k):void */
    /* JADX WARNING: Can't wrap try/catch for region: R(3:28|29|30) */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        com.applovin.impl.sdk.utils.p.a((java.io.Closeable) r9, r7.f4366a);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00cd, code lost:
        com.applovin.impl.sdk.utils.p.a((java.io.Closeable) r2, r7.f4366a);
        com.applovin.impl.sdk.utils.p.a((java.io.Closeable) r9, r7.f4366a);
        com.applovin.impl.sdk.utils.p.a(r10, r7.f4366a);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00dc, code lost:
        return null;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:30:0x00c8 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.io.ByteArrayOutputStream a(java.lang.String r8, java.util.List<java.lang.String> r9, boolean r10) {
        /*
            r7 = this;
            java.lang.String r0 = "FileManager"
            r1 = 0
            if (r10 == 0) goto L_0x0022
            boolean r9 = com.applovin.impl.sdk.utils.p.a(r8, r9)
            if (r9 != 0) goto L_0x0022
            com.applovin.impl.sdk.q r9 = r7.f4367b
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r2 = "Domain is not whitelisted, skipping precache for url: "
            r10.append(r2)
            r10.append(r8)
            java.lang.String r8 = r10.toString()
            r9.b(r0, r8)
            return r1
        L_0x0022:
            com.applovin.impl.sdk.k r9 = r7.f4366a
            com.applovin.impl.sdk.c$e<java.lang.Boolean> r10 = com.applovin.impl.sdk.c.e.u2
            java.lang.Object r9 = r9.a(r10)
            java.lang.Boolean r9 = (java.lang.Boolean) r9
            boolean r9 = r9.booleanValue()
            if (r9 == 0) goto L_0x0047
            java.lang.String r9 = "https://"
            boolean r10 = r8.contains(r9)
            if (r10 != 0) goto L_0x0047
            com.applovin.impl.sdk.q r10 = r7.f4367b
            java.lang.String r2 = "Plaintext HTTP operation requested; upgrading to HTTPS due to universal SSL setting..."
            r10.d(r0, r2)
            java.lang.String r10 = "http://"
            java.lang.String r8 = r8.replace(r10, r9)
        L_0x0047:
            com.applovin.impl.sdk.q r9 = r7.f4367b
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r2 = "Loading "
            r10.append(r2)
            r10.append(r8)
            java.lang.String r2 = "..."
            r10.append(r2)
            java.lang.String r10 = r10.toString()
            r9.b(r0, r10)
            java.io.ByteArrayOutputStream r9 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x0124, all -> 0x0120 }
            r9.<init>()     // Catch:{ IOException -> 0x0124, all -> 0x0120 }
            java.net.URL r10 = new java.net.URL     // Catch:{ IOException -> 0x011d, all -> 0x011a }
            r10.<init>(r8)     // Catch:{ IOException -> 0x011d, all -> 0x011a }
            java.net.URLConnection r10 = r10.openConnection()     // Catch:{ IOException -> 0x011d, all -> 0x011a }
            java.lang.Object r10 = com.google.firebase.perf.network.FirebasePerfUrlConnection.instrument(r10)     // Catch:{ IOException -> 0x011d, all -> 0x011a }
            java.net.URLConnection r10 = (java.net.URLConnection) r10     // Catch:{ IOException -> 0x011d, all -> 0x011a }
            java.net.HttpURLConnection r10 = (java.net.HttpURLConnection) r10     // Catch:{ IOException -> 0x011d, all -> 0x011a }
            com.applovin.impl.sdk.k r2 = r7.f4366a     // Catch:{ IOException -> 0x0117, all -> 0x0115 }
            com.applovin.impl.sdk.c$e<java.lang.Integer> r3 = com.applovin.impl.sdk.c.e.s2     // Catch:{ IOException -> 0x0117, all -> 0x0115 }
            java.lang.Object r2 = r2.a(r3)     // Catch:{ IOException -> 0x0117, all -> 0x0115 }
            java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ IOException -> 0x0117, all -> 0x0115 }
            int r2 = r2.intValue()     // Catch:{ IOException -> 0x0117, all -> 0x0115 }
            r10.setConnectTimeout(r2)     // Catch:{ IOException -> 0x0117, all -> 0x0115 }
            com.applovin.impl.sdk.k r2 = r7.f4366a     // Catch:{ IOException -> 0x0117, all -> 0x0115 }
            com.applovin.impl.sdk.c$e<java.lang.Integer> r3 = com.applovin.impl.sdk.c.e.t2     // Catch:{ IOException -> 0x0117, all -> 0x0115 }
            java.lang.Object r2 = r2.a(r3)     // Catch:{ IOException -> 0x0117, all -> 0x0115 }
            java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ IOException -> 0x0117, all -> 0x0115 }
            int r2 = r2.intValue()     // Catch:{ IOException -> 0x0117, all -> 0x0115 }
            r10.setReadTimeout(r2)     // Catch:{ IOException -> 0x0117, all -> 0x0115 }
            r2 = 1
            r10.setDefaultUseCaches(r2)     // Catch:{ IOException -> 0x0117, all -> 0x0115 }
            r10.setUseCaches(r2)     // Catch:{ IOException -> 0x0117, all -> 0x0115 }
            r3 = 0
            r10.setAllowUserInteraction(r3)     // Catch:{ IOException -> 0x0117, all -> 0x0115 }
            r10.setInstanceFollowRedirects(r2)     // Catch:{ IOException -> 0x0117, all -> 0x0115 }
            int r2 = r10.getResponseCode()     // Catch:{ IOException -> 0x0117, all -> 0x0115 }
            r4 = 200(0xc8, float:2.8E-43)
            if (r2 < r4) goto L_0x0105
            r4 = 300(0x12c, float:4.2E-43)
            if (r2 < r4) goto L_0x00b5
            goto L_0x0105
        L_0x00b5:
            java.io.InputStream r2 = r10.getInputStream()     // Catch:{ IOException -> 0x0117, all -> 0x0115 }
            r4 = 1024(0x400, float:1.435E-42)
            byte[] r4 = new byte[r4]     // Catch:{ IOException -> 0x0103 }
        L_0x00bd:
            int r5 = r4.length     // Catch:{ IOException -> 0x0103 }
            int r5 = r2.read(r4, r3, r5)     // Catch:{ IOException -> 0x0103 }
            if (r5 < 0) goto L_0x00dd
            r9.write(r4, r3, r5)     // Catch:{ Exception -> 0x00c8 }
            goto L_0x00bd
        L_0x00c8:
            com.applovin.impl.sdk.k r3 = r7.f4366a     // Catch:{ IOException -> 0x0103 }
            com.applovin.impl.sdk.utils.p.a(r9, r3)     // Catch:{ IOException -> 0x0103 }
            com.applovin.impl.sdk.k r8 = r7.f4366a
            com.applovin.impl.sdk.utils.p.a(r2, r8)
            com.applovin.impl.sdk.k r8 = r7.f4366a
            com.applovin.impl.sdk.utils.p.a(r9, r8)
            com.applovin.impl.sdk.k r8 = r7.f4366a
            com.applovin.impl.sdk.utils.p.a(r10, r8)
            return r1
        L_0x00dd:
            com.applovin.impl.sdk.q r3 = r7.f4367b     // Catch:{ IOException -> 0x0103 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0103 }
            r4.<init>()     // Catch:{ IOException -> 0x0103 }
            java.lang.String r5 = "Loaded resource at "
            r4.append(r5)     // Catch:{ IOException -> 0x0103 }
            r4.append(r8)     // Catch:{ IOException -> 0x0103 }
            java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x0103 }
            r3.b(r0, r4)     // Catch:{ IOException -> 0x0103 }
            com.applovin.impl.sdk.k r8 = r7.f4366a
            com.applovin.impl.sdk.utils.p.a(r2, r8)
            com.applovin.impl.sdk.k r8 = r7.f4366a
            com.applovin.impl.sdk.utils.p.a(r9, r8)
            com.applovin.impl.sdk.k r8 = r7.f4366a
            com.applovin.impl.sdk.utils.p.a(r10, r8)
            return r9
        L_0x0103:
            r3 = move-exception
            goto L_0x0128
        L_0x0105:
            com.applovin.impl.sdk.k r8 = r7.f4366a
            com.applovin.impl.sdk.utils.p.a(r1, r8)
            com.applovin.impl.sdk.k r8 = r7.f4366a
            com.applovin.impl.sdk.utils.p.a(r9, r8)
            com.applovin.impl.sdk.k r8 = r7.f4366a
            com.applovin.impl.sdk.utils.p.a(r10, r8)
            return r1
        L_0x0115:
            r8 = move-exception
            goto L_0x0150
        L_0x0117:
            r3 = move-exception
            r2 = r1
            goto L_0x0128
        L_0x011a:
            r8 = move-exception
            r10 = r1
            goto L_0x0150
        L_0x011d:
            r3 = move-exception
            r10 = r1
            goto L_0x0127
        L_0x0120:
            r8 = move-exception
            r9 = r1
            r10 = r9
            goto L_0x0150
        L_0x0124:
            r3 = move-exception
            r9 = r1
            r10 = r9
        L_0x0127:
            r2 = r10
        L_0x0128:
            com.applovin.impl.sdk.q r4 = r7.f4367b     // Catch:{ all -> 0x014e }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x014e }
            r5.<init>()     // Catch:{ all -> 0x014e }
            java.lang.String r6 = "Error loading "
            r5.append(r6)     // Catch:{ all -> 0x014e }
            r5.append(r8)     // Catch:{ all -> 0x014e }
            java.lang.String r8 = r5.toString()     // Catch:{ all -> 0x014e }
            r4.b(r0, r8, r3)     // Catch:{ all -> 0x014e }
            com.applovin.impl.sdk.k r8 = r7.f4366a
            com.applovin.impl.sdk.utils.p.a(r2, r8)
            com.applovin.impl.sdk.k r8 = r7.f4366a
            com.applovin.impl.sdk.utils.p.a(r9, r8)
            com.applovin.impl.sdk.k r8 = r7.f4366a
            com.applovin.impl.sdk.utils.p.a(r10, r8)
            return r1
        L_0x014e:
            r8 = move-exception
            r1 = r2
        L_0x0150:
            com.applovin.impl.sdk.k r0 = r7.f4366a
            com.applovin.impl.sdk.utils.p.a(r1, r0)
            com.applovin.impl.sdk.k r0 = r7.f4366a
            com.applovin.impl.sdk.utils.p.a(r9, r0)
            com.applovin.impl.sdk.k r9 = r7.f4366a
            com.applovin.impl.sdk.utils.p.a(r10, r9)
            goto L_0x0161
        L_0x0160:
            throw r8
        L_0x0161:
            goto L_0x0160
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.o.a(java.lang.String, java.util.List, boolean):java.io.ByteArrayOutputStream");
    }

    public File a(String str, Context context) {
        File file;
        if (!m.b(str)) {
            this.f4367b.b("FileManager", "Nothing to look up, skipping...");
            return null;
        }
        q qVar = this.f4367b;
        qVar.b("FileManager", "Looking up cached resource: " + str);
        if (str.contains(InMobiNetworkValues.ICON)) {
            str = str.replace(Constants.URL_PATH_DELIMITER, "_").replace(".", "_");
        }
        synchronized (this.f4368c) {
            File f2 = f(context);
            file = new File(f2, str);
            try {
                f2.mkdirs();
            } catch (Throwable th) {
                q qVar2 = this.f4367b;
                qVar2.b("FileManager", "Unable to make cache directory at " + f2, th);
                return null;
            }
        }
        return file;
    }

    public String a(Context context, String str, String str2, List<String> list, boolean z, e eVar) {
        return a(context, str, str2, list, z, false, eVar);
    }

    public String a(Context context, String str, String str2, List<String> list, boolean z, boolean z2, e eVar) {
        if (!m.b(str)) {
            this.f4367b.b("FileManager", "Nothing to cache, skipping...");
            return null;
        }
        String lastPathSegment = Uri.parse(str).getLastPathSegment();
        if (m.b(lastPathSegment) && m.b(str2)) {
            lastPathSegment = str2 + lastPathSegment;
        }
        String str3 = lastPathSegment;
        File a2 = a(str3, context);
        if (!a(a2, str, list, z, eVar)) {
            return null;
        }
        this.f4367b.b("FileManager", "Caching succeeded for file " + str3);
        return z2 ? Uri.fromFile(a2).toString() : str3;
    }

    public void a(Context context) {
        if (b() && this.f4366a.G()) {
            this.f4367b.b("FileManager", "Compacting cache...");
            synchronized (this.f4368c) {
                a(c(context), context);
            }
        }
    }

    public boolean a(ByteArrayOutputStream byteArrayOutputStream, File file) {
        if (file == null) {
            return false;
        }
        q qVar = this.f4367b;
        qVar.b("FileManager", "Caching " + file.getAbsolutePath() + "...");
        if (byteArrayOutputStream == null || byteArrayOutputStream.size() <= 0) {
            q qVar2 = this.f4367b;
            qVar2.d("FileManager", "No data for " + file.getAbsolutePath());
            return false;
        } else if (!b(byteArrayOutputStream, file)) {
            q qVar3 = this.f4367b;
            qVar3.e("FileManager", "Unable to cache " + file.getAbsolutePath());
            return false;
        } else {
            q qVar4 = this.f4367b;
            qVar4.b("FileManager", "Caching completed for " + file);
            return true;
        }
    }

    public boolean a(File file, String str, List<String> list, e eVar) {
        return a(file, str, list, true, eVar);
    }

    public void b(Context context) {
        try {
            a(".nomedia", context);
            File file = new File(f(context), ".nomedia");
            if (!file.exists()) {
                q qVar = this.f4367b;
                qVar.b("FileManager", "Creating .nomedia file at " + file.getAbsolutePath());
                if (!file.createNewFile()) {
                    this.f4367b.e("FileManager", "Failed to create .nomedia file");
                }
            }
        } catch (IOException e2) {
            this.f4367b.b("FileManager", "Failed to create .nomedia file", e2);
        }
    }
}
