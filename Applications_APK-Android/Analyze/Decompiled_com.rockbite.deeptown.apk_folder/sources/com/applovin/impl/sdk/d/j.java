package com.applovin.impl.sdk.d;

import android.text.TextUtils;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.q;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class j {

    /* renamed from: a  reason: collision with root package name */
    private final k f4097a;

    /* renamed from: b  reason: collision with root package name */
    private final q f4098b;

    /* renamed from: c  reason: collision with root package name */
    private final Object f4099c = new Object();

    /* renamed from: d  reason: collision with root package name */
    private final Map<String, b> f4100d = new HashMap();

    private static class a {

        /* renamed from: a  reason: collision with root package name */
        private static final Set<String> f4101a = new HashSet(7);

        /* renamed from: b  reason: collision with root package name */
        static final String f4102b = "tk";

        /* renamed from: c  reason: collision with root package name */
        static final String f4103c = "tc";

        /* renamed from: d  reason: collision with root package name */
        static final String f4104d = "ec";

        /* renamed from: e  reason: collision with root package name */
        static final String f4105e = "dm";

        /* renamed from: f  reason: collision with root package name */
        static final String f4106f = "dv";

        /* renamed from: g  reason: collision with root package name */
        static final String f4107g = "dh";

        /* renamed from: h  reason: collision with root package name */
        static final String f4108h = "dl";

        static {
            a("tk");
            a("tc");
            a("ec");
            a("dm");
            a("dv");
            a("dh");
            a("dl");
        }

        private static String a(String str) {
            if (TextUtils.isEmpty(str)) {
                throw new IllegalArgumentException("No key name specified");
            } else if (!f4101a.contains(str)) {
                f4101a.add(str);
                return str;
            } else {
                throw new IllegalArgumentException("Key has already been used: " + str);
            }
        }
    }

    private static class b {

        /* renamed from: a  reason: collision with root package name */
        private final String f4109a;

        /* renamed from: b  reason: collision with root package name */
        private int f4110b = 0;

        /* renamed from: c  reason: collision with root package name */
        private int f4111c = 0;

        /* renamed from: d  reason: collision with root package name */
        private double f4112d = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;

        /* renamed from: e  reason: collision with root package name */
        private double f4113e = FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;

        /* renamed from: f  reason: collision with root package name */
        private Long f4114f = null;

        /* renamed from: g  reason: collision with root package name */
        private Long f4115g = null;

        b(String str) {
            this.f4109a = str;
        }

        b(JSONObject jSONObject) throws JSONException {
            this.f4109a = jSONObject.getString(a.f4102b);
            this.f4110b = jSONObject.getInt(a.f4103c);
            this.f4111c = jSONObject.getInt(a.f4104d);
            this.f4112d = jSONObject.getDouble(a.f4105e);
            this.f4113e = jSONObject.getDouble(a.f4106f);
            this.f4114f = Long.valueOf(jSONObject.optLong(a.f4107g));
            this.f4115g = Long.valueOf(jSONObject.optLong(a.f4108h));
        }

        /* access modifiers changed from: package-private */
        public String a() {
            return this.f4109a;
        }

        /* access modifiers changed from: package-private */
        public void a(long j2) {
            int i2 = this.f4110b;
            double d2 = this.f4112d;
            double d3 = this.f4113e;
            this.f4110b = i2 + 1;
            double d4 = (double) i2;
            Double.isNaN(d4);
            double d5 = (double) j2;
            Double.isNaN(d5);
            int i3 = this.f4110b;
            double d6 = (double) i3;
            Double.isNaN(d6);
            this.f4112d = ((d2 * d4) + d5) / d6;
            double d7 = (double) i3;
            Double.isNaN(d4);
            Double.isNaN(d7);
            double d8 = d4 / d7;
            Double.isNaN(d5);
            double pow = Math.pow(d2 - d5, 2.0d);
            double d9 = (double) this.f4110b;
            Double.isNaN(d9);
            this.f4113e = d8 * (d3 + (pow / d9));
            Long l = this.f4114f;
            if (l == null || j2 > l.longValue()) {
                this.f4114f = Long.valueOf(j2);
            }
            Long l2 = this.f4115g;
            if (l2 == null || j2 < l2.longValue()) {
                this.f4115g = Long.valueOf(j2);
            }
        }

        /* access modifiers changed from: package-private */
        public void b() {
            this.f4111c++;
        }

        /* access modifiers changed from: package-private */
        public JSONObject c() throws JSONException {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put(a.f4102b, this.f4109a);
            jSONObject.put(a.f4103c, this.f4110b);
            jSONObject.put(a.f4104d, this.f4111c);
            jSONObject.put(a.f4105e, this.f4112d);
            jSONObject.put(a.f4106f, this.f4113e);
            jSONObject.put(a.f4107g, this.f4114f);
            jSONObject.put(a.f4108h, this.f4115g);
            return jSONObject;
        }

        public String toString() {
            try {
                return "TaskStats{n='" + this.f4109a + '\'' + ", stats=" + c().toString() + '}';
            } catch (JSONException unused) {
                return "TaskStats{n='" + this.f4109a + '\'' + ", count=" + this.f4110b + '}';
            }
        }
    }

    public j(k kVar) {
        this.f4097a = kVar;
        this.f4098b = kVar.Z();
        c();
    }

    private b b(i iVar) {
        b bVar;
        synchronized (this.f4099c) {
            String a2 = iVar.a();
            bVar = this.f4100d.get(a2);
            if (bVar == null) {
                bVar = new b(a2);
                this.f4100d.put(a2, bVar);
            }
        }
        return bVar;
    }

    private void c() {
        Set<String> set = (Set) this.f4097a.a(c.g.p);
        if (set != null) {
            synchronized (this.f4099c) {
                try {
                    for (String jSONObject : set) {
                        b bVar = new b(new JSONObject(jSONObject));
                        this.f4100d.put(bVar.a(), bVar);
                    }
                } catch (JSONException e2) {
                    this.f4098b.b("TaskStatsManager", "Failed to convert stats json.", e2);
                }
            }
        }
    }

    private void d() {
        HashSet hashSet;
        synchronized (this.f4099c) {
            hashSet = new HashSet(this.f4100d.size());
            for (b next : this.f4100d.values()) {
                try {
                    hashSet.add(next.c().toString());
                } catch (JSONException e2) {
                    q qVar = this.f4098b;
                    qVar.b("TaskStatsManager", "Failed to serialize " + next, e2);
                }
            }
        }
        this.f4097a.a(c.g.p, hashSet);
    }

    public JSONArray a() {
        JSONArray jSONArray;
        synchronized (this.f4099c) {
            jSONArray = new JSONArray();
            for (b next : this.f4100d.values()) {
                try {
                    jSONArray.put(next.c());
                } catch (JSONException e2) {
                    q qVar = this.f4098b;
                    qVar.b("TaskStatsManager", "Failed to serialize " + next, e2);
                }
            }
        }
        return jSONArray;
    }

    public void a(i iVar) {
        a(iVar, false, 0);
    }

    public void a(i iVar, long j2) {
        if (iVar == null) {
            throw new IllegalArgumentException("No key specified");
        } else if (((Boolean) this.f4097a.a(c.e.s3)).booleanValue()) {
            synchronized (this.f4099c) {
                b(iVar).a(j2);
                d();
            }
        }
    }

    public void a(i iVar, boolean z, long j2) {
        if (iVar == null) {
            throw new IllegalArgumentException("No key specified");
        } else if (((Boolean) this.f4097a.a(c.e.s3)).booleanValue()) {
            synchronized (this.f4099c) {
                b b2 = b(iVar);
                b2.b();
                if (z) {
                    b2.a(j2);
                }
                d();
            }
        }
    }

    public void b() {
        synchronized (this.f4099c) {
            this.f4100d.clear();
            this.f4097a.b(c.g.p);
        }
    }
}
