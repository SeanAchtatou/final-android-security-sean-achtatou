package com.applovin.impl.sdk.utils;

import android.util.Xml;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.q;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.concurrent.TimeUnit;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

public class s {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final q f4494a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public Stack<b> f4495b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public StringBuilder f4496c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public long f4497d;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public b f4498e;

    class a implements ContentHandler {
        a() {
        }

        public void characters(char[] cArr, int i2, int i3) {
            String trim = new String(Arrays.copyOfRange(cArr, i2, i3)).trim();
            if (m.b(trim)) {
                s.this.f4496c.append(trim);
            }
        }

        public void endDocument() {
            long seconds = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()) - s.this.f4497d;
            q a2 = s.this.f4494a;
            a2.b("XmlParser", "Finished parsing in " + seconds + " seconds");
        }

        public void endElement(String str, String str2, String str3) {
            s sVar = s.this;
            b unused = sVar.f4498e = (b) sVar.f4495b.pop();
            s.this.f4498e.d(s.this.f4496c.toString().trim());
            s.this.f4496c.setLength(0);
        }

        public void endPrefixMapping(String str) {
        }

        public void ignorableWhitespace(char[] cArr, int i2, int i3) {
        }

        public void processingInstruction(String str, String str2) {
        }

        public void setDocumentLocator(Locator locator) {
        }

        public void skippedEntity(String str) {
        }

        public void startDocument() {
            s.this.f4494a.b("XmlParser", "Begin parsing...");
            long unused = s.this.f4497d = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
        }

        public void startElement(String str, String str2, String str3, Attributes attributes) throws SAXException {
            b bVar = null;
            try {
                if (!s.this.f4495b.isEmpty()) {
                    bVar = (b) s.this.f4495b.peek();
                }
                b bVar2 = new b(str2, s.this.a(attributes), bVar);
                if (bVar != null) {
                    bVar.a(bVar2);
                }
                s.this.f4495b.push(bVar2);
            } catch (Exception e2) {
                q a2 = s.this.f4494a;
                a2.b("XmlParser", "Unable to process element <" + str2 + ">", e2);
                throw new SAXException("Failed to start element", e2);
            }
        }

        public void startPrefixMapping(String str, String str2) {
        }
    }

    private static class b extends r {
        b(String str, Map<String, String> map, r rVar) {
            super(str, map, rVar);
        }

        /* access modifiers changed from: package-private */
        public void a(r rVar) {
            if (rVar != null) {
                this.f4493d.add(rVar);
                return;
            }
            throw new IllegalArgumentException("None specified.");
        }

        /* access modifiers changed from: package-private */
        public void d(String str) {
            this.f4492c = str;
        }
    }

    s(k kVar) {
        if (kVar != null) {
            this.f4494a = kVar.Z();
            return;
        }
        throw new IllegalArgumentException("No sdk specified.");
    }

    public static r a(String str, k kVar) throws SAXException {
        return new s(kVar).a(str);
    }

    /* access modifiers changed from: private */
    public Map<String, String> a(Attributes attributes) {
        if (attributes == null) {
            return Collections.emptyMap();
        }
        int length = attributes.getLength();
        HashMap hashMap = new HashMap(length);
        for (int i2 = 0; i2 < length; i2++) {
            hashMap.put(attributes.getQName(i2), attributes.getValue(i2));
        }
        return hashMap;
    }

    public r a(String str) throws SAXException {
        if (str != null) {
            this.f4496c = new StringBuilder();
            this.f4495b = new Stack<>();
            this.f4498e = null;
            Xml.parse(str, new a());
            b bVar = this.f4498e;
            if (bVar != null) {
                return bVar;
            }
            throw new SAXException("Unable to parse XML into node");
        }
        throw new IllegalArgumentException("Unable to parse. No XML specified.");
    }
}
