package com.applovin.impl.sdk;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.n;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.concurrent.atomic.AtomicBoolean;

class i extends BroadcastReceiver {
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public static AlertDialog f4224c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public static final AtomicBoolean f4225d = new AtomicBoolean();
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final j f4226a;

    /* renamed from: b  reason: collision with root package name */
    private n f4227b;

    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ k f4228a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ b f4229b;

        /* renamed from: com.applovin.impl.sdk.i$a$a  reason: collision with other inner class name */
        class C0105a implements Runnable {

            /* renamed from: com.applovin.impl.sdk.i$a$a$a  reason: collision with other inner class name */
            class C0106a implements DialogInterface.OnClickListener {
                C0106a() {
                }

                public void onClick(DialogInterface dialogInterface, int i2) {
                    a.this.f4229b.b();
                    dialogInterface.dismiss();
                    i.f4225d.set(false);
                    long longValue = ((Long) a.this.f4228a.a(c.e.I)).longValue();
                    a aVar = a.this;
                    i.this.a(longValue, aVar.f4228a, aVar.f4229b);
                }
            }

            /* renamed from: com.applovin.impl.sdk.i$a$a$b */
            class b implements DialogInterface.OnClickListener {
                b() {
                }

                public void onClick(DialogInterface dialogInterface, int i2) {
                    a.this.f4229b.a();
                    dialogInterface.dismiss();
                    i.f4225d.set(false);
                }
            }

            C0105a() {
            }

            public void run() {
                AlertDialog unused = i.f4224c = new AlertDialog.Builder(a.this.f4228a.A().a()).setTitle((CharSequence) a.this.f4228a.a(c.e.K)).setMessage((CharSequence) a.this.f4228a.a(c.e.L)).setCancelable(false).setPositiveButton((CharSequence) a.this.f4228a.a(c.e.M), new b()).setNegativeButton((CharSequence) a.this.f4228a.a(c.e.N), new C0106a()).create();
                i.f4224c.show();
            }
        }

        a(k kVar, b bVar) {
            this.f4228a = kVar;
            this.f4229b = bVar;
        }

        public void run() {
            String str;
            q qVar;
            if (i.this.f4226a.c()) {
                this.f4228a.Z().e("ConsentAlertManager", "Consent dialog already showing, skip showing of consent alert");
                return;
            }
            Activity a2 = this.f4228a.A().a();
            if (a2 == null || !h.a(this.f4228a.d())) {
                if (a2 == null) {
                    qVar = this.f4228a.Z();
                    str = "No parent Activity found - rescheduling consent alert...";
                } else {
                    qVar = this.f4228a.Z();
                    str = "No internet available - rescheduling consent alert...";
                }
                qVar.e("ConsentAlertManager", str);
                i.f4225d.set(false);
                i.this.a(((Long) this.f4228a.a(c.e.J)).longValue(), this.f4228a, this.f4229b);
                return;
            }
            AppLovinSdkUtils.runOnUiThread(new C0105a());
        }
    }

    public interface b {
        void a();

        void b();
    }

    i(j jVar, k kVar) {
        this.f4226a = jVar;
        kVar.D().registerReceiver(this, new IntentFilter("com.applovin.application_paused"));
        kVar.D().registerReceiver(this, new IntentFilter("com.applovin.application_resumed"));
    }

    public void a(long j2, k kVar, b bVar) {
        if (j2 > 0) {
            AlertDialog alertDialog = f4224c;
            if (alertDialog == null || !alertDialog.isShowing()) {
                if (f4225d.getAndSet(true)) {
                    if (j2 < this.f4227b.a()) {
                        q Z = kVar.Z();
                        Z.b("ConsentAlertManager", "Scheduling consent alert earlier (" + j2 + "ms) than remaining scheduled time (" + this.f4227b.a() + "ms)");
                        this.f4227b.d();
                    } else {
                        q Z2 = kVar.Z();
                        Z2.d("ConsentAlertManager", "Skip scheduling consent alert - one scheduled already with remaining time of " + this.f4227b.a() + " milliseconds");
                        return;
                    }
                }
                q Z3 = kVar.Z();
                Z3.b("ConsentAlertManager", "Scheduling consent alert for " + j2 + " milliseconds");
                this.f4227b = n.a(j2, kVar, new a(kVar, bVar));
            }
        }
    }

    public void onReceive(Context context, Intent intent) {
        if (this.f4227b != null) {
            String action = intent.getAction();
            if ("com.applovin.application_paused".equals(action)) {
                this.f4227b.b();
            } else if ("com.applovin.application_resumed".equals(action)) {
                this.f4227b.c();
            }
        }
    }
}
