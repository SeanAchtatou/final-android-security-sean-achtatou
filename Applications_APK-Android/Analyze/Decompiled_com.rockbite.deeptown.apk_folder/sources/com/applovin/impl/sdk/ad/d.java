package com.applovin.impl.sdk.ad;

import android.text.TextUtils;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.q;
import com.applovin.impl.sdk.utils.i;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import com.facebook.appevents.AppEventsConstants;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Map;
import org.json.JSONObject;

public final class d {

    /* renamed from: h  reason: collision with root package name */
    private static final Map<String, d> f3960h = new HashMap();

    /* renamed from: i  reason: collision with root package name */
    private static final Object f3961i = new Object();

    /* renamed from: a  reason: collision with root package name */
    private k f3962a;

    /* renamed from: b  reason: collision with root package name */
    private q f3963b;

    /* renamed from: c  reason: collision with root package name */
    private JSONObject f3964c;

    /* renamed from: d  reason: collision with root package name */
    private final String f3965d;

    /* renamed from: e  reason: collision with root package name */
    private String f3966e;

    /* renamed from: f  reason: collision with root package name */
    private AppLovinAdSize f3967f;

    /* renamed from: g  reason: collision with root package name */
    private AppLovinAdType f3968g;

    private d(AppLovinAdSize appLovinAdSize, AppLovinAdType appLovinAdType, String str, k kVar) {
        if (!TextUtils.isEmpty(str) || !(appLovinAdType == null || appLovinAdSize == null)) {
            this.f3962a = kVar;
            this.f3963b = kVar != null ? kVar.Z() : null;
            this.f3967f = appLovinAdSize;
            this.f3968g = appLovinAdType;
            if (!TextUtils.isEmpty(str)) {
                this.f3965d = str.toLowerCase(Locale.ENGLISH);
                this.f3966e = str.toLowerCase(Locale.ENGLISH);
                return;
            }
            this.f3965d = (appLovinAdSize.getLabel() + "_" + appLovinAdType.getLabel()).toLowerCase(Locale.ENGLISH);
            return;
        }
        throw new IllegalArgumentException("No zone identifier or type or size specified");
    }

    public static d a(AppLovinAdSize appLovinAdSize, AppLovinAdType appLovinAdType, k kVar) {
        return a(appLovinAdSize, appLovinAdType, null, kVar);
    }

    public static d a(AppLovinAdSize appLovinAdSize, AppLovinAdType appLovinAdType, String str, k kVar) {
        d dVar = new d(appLovinAdSize, appLovinAdType, str, kVar);
        synchronized (f3961i) {
            String str2 = dVar.f3965d;
            if (f3960h.containsKey(str2)) {
                dVar = f3960h.get(str2);
            } else {
                f3960h.put(str2, dVar);
            }
        }
        return dVar;
    }

    public static d a(String str, k kVar) {
        return a(null, null, str, kVar);
    }

    public static d a(String str, JSONObject jSONObject, k kVar) {
        d a2 = a(str, kVar);
        a2.f3964c = jSONObject;
        return a2;
    }

    private <ST> c.e<ST> a(String str, c.e eVar) {
        return this.f3962a.a(str + this.f3965d, eVar);
    }

    private boolean a(c.e<String> eVar, AppLovinAdSize appLovinAdSize) {
        return ((String) this.f3962a.a(eVar)).toUpperCase(Locale.ENGLISH).contains(appLovinAdSize.getLabel());
    }

    public static d b(String str, k kVar) {
        return a(AppLovinAdSize.NATIVE, AppLovinAdType.NATIVE, str, kVar);
    }

    public static Collection<d> b(k kVar) {
        LinkedHashSet linkedHashSet = new LinkedHashSet(8);
        Collections.addAll(linkedHashSet, c(kVar), d(kVar), e(kVar), f(kVar), g(kVar), h(kVar));
        return Collections.unmodifiableSet(linkedHashSet);
    }

    public static d c(k kVar) {
        return a(AppLovinAdSize.BANNER, AppLovinAdType.REGULAR, kVar);
    }

    public static d c(String str, k kVar) {
        return a(AppLovinAdSize.INTERSTITIAL, AppLovinAdType.INCENTIVIZED, str, kVar);
    }

    public static d d(k kVar) {
        return a(AppLovinAdSize.MREC, AppLovinAdType.REGULAR, kVar);
    }

    public static d e(k kVar) {
        return a(AppLovinAdSize.LEADER, AppLovinAdType.REGULAR, kVar);
    }

    public static d f(k kVar) {
        return a(AppLovinAdSize.INTERSTITIAL, AppLovinAdType.REGULAR, kVar);
    }

    public static d g(k kVar) {
        return a(AppLovinAdSize.INTERSTITIAL, AppLovinAdType.INCENTIVIZED, kVar);
    }

    public static d h(k kVar) {
        return a(AppLovinAdSize.NATIVE, AppLovinAdType.NATIVE, kVar);
    }

    private boolean k() {
        try {
            if (!TextUtils.isEmpty(this.f3966e)) {
                return true;
            }
            return AppLovinAdType.INCENTIVIZED.equals(c()) ? ((Boolean) this.f3962a.a(c.e.o0)).booleanValue() : a(c.e.n0, b());
        } catch (Throwable th) {
            this.f3963b.b("AdZone", "Unable to safely test preload merge capability", th);
            return false;
        }
    }

    public String a() {
        return this.f3965d;
    }

    /* access modifiers changed from: package-private */
    public void a(k kVar) {
        this.f3962a = kVar;
        this.f3963b = kVar.Z();
    }

    public AppLovinAdSize b() {
        if (this.f3967f == null && i.a(this.f3964c, "ad_size")) {
            this.f3967f = AppLovinAdSize.fromString(i.b(this.f3964c, "ad_size", (String) null, this.f3962a));
        }
        return this.f3967f;
    }

    public AppLovinAdType c() {
        if (this.f3968g == null && i.a(this.f3964c, AppEventsConstants.EVENT_PARAM_AD_TYPE)) {
            this.f3968g = AppLovinAdType.fromString(i.b(this.f3964c, AppEventsConstants.EVENT_PARAM_AD_TYPE, (String) null, this.f3962a));
        }
        return this.f3968g;
    }

    public boolean d() {
        return AppLovinAdSize.NATIVE.equals(b()) && AppLovinAdType.NATIVE.equals(c());
    }

    public int e() {
        if (i.a(this.f3964c, "capacity")) {
            return i.b(this.f3964c, "capacity", 0, this.f3962a);
        }
        if (!TextUtils.isEmpty(this.f3966e)) {
            return d() ? ((Integer) this.f3962a.a(c.e.v0)).intValue() : ((Integer) this.f3962a.a(c.e.u0)).intValue();
        }
        return ((Integer) this.f3962a.a(a("preload_capacity_", c.e.r0))).intValue();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || d.class != obj.getClass()) {
            return false;
        }
        return this.f3965d.equalsIgnoreCase(((d) obj).f3965d);
    }

    public int f() {
        if (i.a(this.f3964c, "extended_capacity")) {
            return i.b(this.f3964c, "extended_capacity", 0, this.f3962a);
        }
        if (TextUtils.isEmpty(this.f3966e)) {
            return ((Integer) this.f3962a.a(a("extended_preload_capacity_", c.e.t0))).intValue();
        } else if (d()) {
            return 0;
        } else {
            return ((Integer) this.f3962a.a(c.e.w0)).intValue();
        }
    }

    public int g() {
        return i.b(this.f3964c, "preload_count", 0, this.f3962a);
    }

    public boolean h() {
        if (!((Boolean) this.f3962a.a(c.e.m0)).booleanValue() || !k()) {
            return false;
        }
        if (TextUtils.isEmpty(this.f3966e)) {
            c.e a2 = a("preload_merge_init_tasks_", (c.e) null);
            return a2 != null && ((Boolean) this.f3962a.a(a2)).booleanValue() && e() > 0;
        } else if (this.f3964c != null && g() == 0) {
            return false;
        } else {
            String upperCase = ((String) this.f3962a.a(c.e.n0)).toUpperCase(Locale.ENGLISH);
            return (upperCase.contains(AppLovinAdSize.INTERSTITIAL.getLabel()) || upperCase.contains(AppLovinAdSize.BANNER.getLabel()) || upperCase.contains(AppLovinAdSize.MREC.getLabel()) || upperCase.contains(AppLovinAdSize.LEADER.getLabel())) ? ((Boolean) this.f3962a.a(c.e.x0)).booleanValue() : this.f3962a.v().a(this) && g() > 0 && ((Boolean) this.f3962a.a(c.e.F2)).booleanValue();
        }
    }

    public int hashCode() {
        return this.f3965d.hashCode();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Boolean, com.applovin.impl.sdk.k):java.lang.Boolean
     arg types: [org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.k]
     candidates:
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, float, com.applovin.impl.sdk.k):float
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, long, com.applovin.impl.sdk.k):long
      com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, java.lang.Object, com.applovin.impl.sdk.k):java.lang.Object
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Object, com.applovin.impl.sdk.k):java.lang.Object
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.util.List, com.applovin.impl.sdk.k):java.util.List
      com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, org.json.JSONObject, com.applovin.impl.sdk.k):org.json.JSONObject
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.k):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.String, com.applovin.impl.sdk.k):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONArray, com.applovin.impl.sdk.k):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONObject, com.applovin.impl.sdk.k):void
      com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Boolean, com.applovin.impl.sdk.k):java.lang.Boolean */
    public boolean i() {
        return i.a(this.f3964c, "wrapped_ads_enabled") ? i.a(this.f3964c, "wrapped_ads_enabled", (Boolean) false, this.f3962a).booleanValue() : b() != null ? this.f3962a.b(c.e.W0).contains(b().getLabel()) : ((Boolean) this.f3962a.a(c.e.V0)).booleanValue();
    }

    public boolean j() {
        return b(this.f3962a).contains(this);
    }

    public String toString() {
        return "AdZone{identifier=" + this.f3965d + ", zoneObject=" + this.f3964c + '}';
    }
}
