package com.applovin.impl.sdk.d;

import android.text.TextUtils;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.q;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class f {

    /* renamed from: a  reason: collision with root package name */
    private final List<b> f4065a;

    /* renamed from: b  reason: collision with root package name */
    private final Object f4066b = new Object();

    /* renamed from: c  reason: collision with root package name */
    private final k f4067c;

    /* renamed from: d  reason: collision with root package name */
    private final q f4068d;

    private static class b {

        /* renamed from: a  reason: collision with root package name */
        private final Long f4069a;

        /* renamed from: b  reason: collision with root package name */
        private final String f4070b;

        /* renamed from: c  reason: collision with root package name */
        private final String f4071c;

        /* renamed from: d  reason: collision with root package name */
        private final String f4072d;

        private b(String str, Throwable th) {
            this.f4070b = str;
            this.f4069a = Long.valueOf(System.currentTimeMillis());
            String str2 = null;
            this.f4071c = th != null ? th.getClass().getName() : null;
            this.f4072d = th != null ? th.getMessage() : str2;
        }

        private b(JSONObject jSONObject) throws JSONException {
            this.f4070b = jSONObject.getString("ms");
            this.f4069a = Long.valueOf(jSONObject.getLong("ts"));
            JSONObject optJSONObject = jSONObject.optJSONObject("ex");
            String str = null;
            this.f4071c = optJSONObject != null ? optJSONObject.getString("nm") : null;
            this.f4072d = optJSONObject != null ? optJSONObject.getString("rn") : str;
        }

        /* access modifiers changed from: private */
        public JSONObject a() throws JSONException {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("ms", this.f4070b);
            jSONObject.put("ts", this.f4069a);
            if (!TextUtils.isEmpty(this.f4071c)) {
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("nm", this.f4071c);
                if (!TextUtils.isEmpty(this.f4072d)) {
                    jSONObject2.put("rn", this.f4072d);
                }
                jSONObject.put("ex", jSONObject2);
            }
            return jSONObject;
        }

        public String toString() {
            return "ErrorLog{timestampMillis=" + this.f4069a + ",message='" + this.f4070b + '\'' + ",throwableName='" + this.f4071c + '\'' + ",throwableReason='" + this.f4072d + '\'' + '}';
        }
    }

    public f(k kVar) {
        this.f4067c = kVar;
        this.f4068d = kVar.Z();
        this.f4065a = new ArrayList();
    }

    private void d() {
        JSONArray jSONArray = new JSONArray();
        synchronized (this.f4066b) {
            for (b next : this.f4065a) {
                try {
                    jSONArray.put(next.a());
                } catch (JSONException e2) {
                    this.f4068d.a("ErrorManager", false, "Failed to convert error log into json.", e2);
                    this.f4065a.remove(next);
                }
            }
        }
        this.f4067c.a(c.g.o, jSONArray.toString());
    }

    public JSONArray a() {
        JSONArray jSONArray;
        synchronized (this.f4066b) {
            jSONArray = new JSONArray();
            for (b a2 : this.f4065a) {
                try {
                    jSONArray.put(a2.a());
                } catch (JSONException e2) {
                    this.f4068d.a("ErrorManager", false, "Failed to convert error log into json.", e2);
                }
            }
        }
        return jSONArray;
    }

    public void a(String str, Throwable th) {
        if (!TextUtils.isEmpty(str)) {
            synchronized (this.f4066b) {
                if (this.f4065a.size() < ((Integer) this.f4067c.a(c.e.u3)).intValue()) {
                    this.f4065a.add(new b(str, th));
                    d();
                }
            }
        }
    }

    public void b() {
        String str = (String) this.f4067c.b(c.g.o, null);
        if (str != null) {
            synchronized (this.f4066b) {
                try {
                    this.f4065a.clear();
                    JSONArray jSONArray = new JSONArray(str);
                    for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                        try {
                            this.f4065a.add(new b(jSONArray.getJSONObject(i2)));
                        } catch (JSONException e2) {
                            this.f4068d.a("ErrorManager", false, "Failed to convert error json into a log.", e2);
                        }
                    }
                } catch (JSONException e3) {
                    this.f4068d.b("ErrorManager", "Unable to convert String to json.", e3);
                }
            }
        }
    }

    public void c() {
        synchronized (this.f4066b) {
            this.f4065a.clear();
            this.f4067c.b(c.g.o);
        }
    }
}
