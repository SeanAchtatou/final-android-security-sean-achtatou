package com.applovin.impl.sdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.StrictMode;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.p;
import com.applovin.mediation.adapters.AppLovinMediationAdapter;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinSdkSettings;
import com.esotericsoftware.spine.Animation;
import com.google.android.gms.ads.AdRequest;
import com.tapjoy.TJAdUnitConstants;
import e.c.a.a.j;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;

public class c extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private final k f3992a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final b f3993b;

    /* renamed from: c  reason: collision with root package name */
    private n f3994c;

    /* renamed from: d  reason: collision with root package name */
    private final Object f3995d = new Object();

    /* renamed from: e  reason: collision with root package name */
    private long f3996e;

    class a implements Runnable {
        a() {
        }

        public void run() {
            c.this.a();
            c.this.f3993b.onAdExpired();
        }
    }

    public interface b {
        void onAdExpired();
    }

    /* renamed from: com.applovin.impl.sdk.c$c  reason: collision with other inner class name */
    public class C0102c<T> extends e<T> {
        public static final e<String> T3 = e.a("c_sticky_topics", "safedk_init,max_ad_events,test_mode_enabled,test_mode_networkssend_http_request");
    }

    public class d<T> extends e<T> {
        public static final e<Boolean> A4 = e.a("saewib", false);
        public static final e<Boolean> B4 = e.a("utaoae", false);
        public static final e<Long> C4 = e.a("ad_hidden_timeout_ms", -1L);
        public static final e<Boolean> D4 = e.a("schedule_ad_hidden_on_ad_dismiss", false);
        public static final e<Long> E4 = e.a("ad_hidden_on_ad_dismiss_callback_delay_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(1)));
        public static final e<Boolean> F4 = e.a("proe", false);
        public static final e<String> G4 = e.a("fitaui", "");
        public static final e<String> H4 = e.a("finaui", "");
        public static final e<String> I4 = e.a("faespcn", AppLovinMediationAdapter.class.getName());
        public static final e<Long> J4 = e.a("fard_s", 3L);
        public static final e<String> T3 = e.a("afi", "");
        public static final e<Long> U3 = e.a("afi_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(5)));
        public static final e<String> V3 = e.a("mediation_endpoint", "https://ms.applovin.com/");
        public static final e<String> W3 = e.a("mediation_backup_endpoint", "https://ms.applvn.com/");
        public static final e<Long> X3 = e.a("fetch_next_ad_retry_delay_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(2)));
        public static final e<Long> Y3 = e.a("fetch_next_ad_timeout_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(5)));
        public static final e<Long> Z3 = e.a("fetch_mediation_debugger_info_timeout_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(7)));
        public static final e<Boolean> a4 = e.a("pass_extra_parameters", true);
        public static final e<String> b4 = e.a("postback_macros", "{\"{MCODE}\":\"mcode\",\"{BCODE}\":\"bcode\",\"{ICODE}\":\"icode\",\"{SCODE}\":\"scode\"}");
        public static final e<Boolean> c4 = e.a("persistent_mediated_postbacks", false);
        public static final e<Long> d4 = e.a("max_signal_provider_latency_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(30)));
        public static final e<Integer> e4 = e.a("max_adapter_version_length", 20);
        public static final e<Integer> f4 = e.a("max_adapter_sdk_version_length", 20);
        public static final e<Integer> g4 = e.a("max_adapter_signal_length", 5120);
        public static final e<Long> h4 = e.a("default_adapter_timeout_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(10)));
        public static final e<Integer> i4 = e.a("default_ad_view_width", -2);
        public static final e<Integer> j4 = e.a("default_ad_view_height", -2);
        public static final e<Long> k4 = e.a("ad_refresh_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(30)));
        public static final e<Long> l4 = e.a("ad_load_failure_refresh_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(30)));
        public static final e<String> m4 = e.a("ad_load_failure_refresh_ignore_error_codes", "204");
        public static final e<Long> n4 = e.a("refresh_ad_on_app_resume_elapsed_threshold_ms", 0L);
        public static final e<Boolean> o4 = e.a("refresh_ad_view_timer_responds_to_background", true);
        public static final e<Boolean> p4 = e.a("refresh_ad_view_timer_responds_to_store_kit", true);
        public static final e<Boolean> q4 = e.a("refresh_ad_view_timer_responds_to_window_visibility_changed", false);
        public static final e<Long> r4 = e.a("ad_view_fade_in_animation_ms", 150L);
        public static final e<Long> s4 = e.a("ad_view_fade_out_animation_ms", 150L);
        public static final e<Long> t4 = e.a("fullscreen_display_delay_ms", 600L);
        public static final e<Long> u4 = e.a("ahdm", 500L);
        public static final e<Long> v4 = e.a("ad_view_refresh_precache_request_viewability_undesired_flags", 118L);
        public static final e<Long> w4 = e.a("ad_view_refresh_precache_request_delay_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(2)));
        public static final e<Boolean> x4 = e.a("ad_view_block_publisher_load_if_refresh_scheduled", true);
        public static final e<Boolean> y4 = e.a("fullscreen_ads_block_publisher_load_if_another_showing", true);
        public static final e<Long> z4 = e.a("ad_expiration_ms", Long.valueOf(TimeUnit.HOURS.toMillis(4)));
    }

    public class e<T> implements Comparable {
        public static final e<Boolean> A = a("alert_consent_for_dialog_rejected", false);
        public static final e<String> A0 = a("cdt", "external");
        public static final e<Integer> A1 = a("close_button_right_margin_video", 4);
        public static final e<Integer> A2 = a("fetch_basic_settings_retry_count", 3);
        public static final e<String> A3 = a("vast_unsupported_video_extensions", "ogv,flv");
        public static final e<Boolean> B = a("alert_consent_for_dialog_closed", false);
        public static final e<Boolean> B0 = a("cache_cleanup_enabled", false);
        public static final e<Integer> B1 = a("close_button_size_video", 30);
        public static final e<Boolean> B2 = a("fetch_basic_settings_on_reconnect", false);
        public static final e<String> B3 = a("vast_unsupported_video_types", "video/ogg,video/x-flv");
        public static final e<Boolean> C = a("alert_consent_for_dialog_closed_with_back_button", false);
        public static final e<Long> C0 = a("cache_file_ttl_seconds", Long.valueOf(TimeUnit.DAYS.toSeconds(1)));
        public static final e<Integer> C1 = a("close_button_top_margin_video", 8);
        public static final e<Boolean> C2 = a("skip_fetch_basic_settings_if_not_connected", false);
        public static final e<Boolean> C3 = a("vast_validate_with_extension_if_no_video_type", true);
        public static final e<Boolean> D = a("alert_consent_after_init", false);
        public static final e<Integer> D0 = a("cache_max_size_mb", -1);
        public static final e<Integer> D1 = a("close_fade_in_time", 400);
        public static final e<Integer> D2 = a("fetch_basic_settings_retry_delay_ms", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(2)));
        public static final e<Integer> D3 = a("vast_video_selection_policy", Integer.valueOf(j.b.MEDIUM.ordinal()));
        public static final e<Long> E = a("alert_consent_after_init_interval_ms", Long.valueOf(TimeUnit.MINUTES.toMillis(5)));
        public static final e<String> E0 = a("precache_delimiters", ")]',");
        public static final e<Boolean> E1 = a("show_close_on_exit", true);
        public static final e<Integer> E2 = a("fetch_variables_connection_timeout_ms", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(5)));
        public static final e<Integer> E3 = a("vast_wrapper_resolution_retry_count_v1", 1);
        public static final e<Long> F = a("alert_consent_after_dialog_rejection_interval_ms", Long.valueOf(TimeUnit.MINUTES.toMillis(30)));
        public static final e<Boolean> F0 = a("native_auto_cache_preload_resources", true);
        public static final e<Integer> F1 = a("video_countdown_clock_margin", 10);
        public static final e<Boolean> F2 = a("preload_persisted_zones", true);
        public static final e<Integer> F3 = a("vast_wrapper_resolution_connection_timeout", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(30)));
        public static final e<Long> G = a("alert_consent_after_dialog_close_interval_ms", Long.valueOf(TimeUnit.MINUTES.toMillis(5)));
        public static final e<Boolean> G0 = a("ad_resource_caching_enabled", true);
        public static final e<Integer> G1 = a("video_countdown_clock_gravity", 83);
        public static final e<Boolean> G2 = a("persist_zones", true);
        public static final e<Boolean> G3 = a("ree", true);
        public static final e<Long> H = a("alert_consent_after_dialog_close_with_back_button_interval_ms", Long.valueOf(TimeUnit.MINUTES.toMillis(5)));
        public static final e<Boolean> H0 = a("fail_ad_load_on_failed_video_cache", true);
        public static final e<Integer> H1 = a("countdown_clock_size", 32);
        public static final e<Integer> H2 = a("ad_session_minutes", 60);
        public static final e<Boolean> H3 = a("btee", true);
        public static final e<Long> I = a("alert_consent_after_cancel_interval_ms", Long.valueOf(TimeUnit.MINUTES.toMillis(10)));
        public static final e<String> I0 = a("resource_cache_prefix", "https://vid.applovin.com/,https://pdn.applovin.com/,https://img.applovin.com/,https://d.applovin.com/,https://assets.applovin.com/,https://cdnjs.cloudflare.com/,http://vid.applovin.com/,http://pdn.applovin.com/,http://img.applovin.com/,http://d.applovin.com/,http://assets.applovin.com/,http://cdnjs.cloudflare.com/");
        public static final e<Integer> I1 = a("countdown_clock_stroke_size", 4);
        public static final e<Boolean> I2 = a("session_tracking_cooldown_on_event_fire", true);
        public static final e<Long> I3 = a("server_timestamp_ms", 0L);
        public static final e<Long> J = a("alert_consent_reschedule_interval_ms", Long.valueOf(TimeUnit.MINUTES.toMillis(5)));
        public static final e<Integer> J0 = a("cr_retry_count_v1", 1);
        public static final e<Integer> J1 = a("countdown_clock_text_size", 28);
        public static final e<Long> J2 = a("session_tracking_resumed_cooldown_minutes", 90L);
        public static final e<Long> J3 = a("device_timestamp_ms", 0L);
        public static final e<String> K = a("text_alert_consent_title", "Make this App Better and Stay Free!");
        public static final e<Boolean> K0 = a("incent_warning_enabled", false);
        public static final e<Boolean> K1 = a("draw_countdown_clock", true);
        public static final e<Long> K2 = a("session_tracking_paused_cooldown_minutes", 90L);
        public static final e<Boolean> K3 = a("immediate_render", false);
        public static final e<String> L = a("text_alert_consent_body", "If you don't give us consent to use your data, you will be making our ability to support this app harder, which may negatively affect the user experience.");
        public static final e<String> L0 = a("text_incent_warning_title", "Attention!");
        public static final e<Boolean> L1 = a("force_back_button_enabled_always", false);
        public static final e<Boolean> L2 = a("track_app_paused", false);
        public static final e<Boolean> L3 = a("cleanup_webview", false);
        public static final e<String> M = a("text_alert_consent_yes_option", "I Agree");
        public static final e<String> M0 = a("text_incent_warning_body", "You won’t get your reward if the video hasn’t finished.");
        public static final e<Boolean> M1 = a("force_back_button_enabled_close_button", false);
        public static final e<Boolean> M2 = a("qq", false);
        public static final e<Boolean> M3 = a("sanitize_webview", false);
        public static final e<String> N = a("text_alert_consent_no_option", "Cancel");
        public static final e<String> N0 = a("text_incent_warning_close_option", "Close");
        public static final e<Boolean> N1 = a("force_back_button_enabled_poststitial", false);
        public static final e<Boolean> N2 = a("qq1", true);
        public static final e<Boolean> N3 = a("force_rerender", false);
        public static final e<Long> O = a("ttc_max_click_duration_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(1)));
        public static final e<String> O0 = a("text_incent_warning_continue_option", "Keep Watching");
        public static final e<Long> O1 = a("force_hide_status_bar_delay_ms", 0L);
        public static final e<Boolean> O2 = a("qq3", true);
        public static final e<Boolean> O3 = a("ignore_is_showing", false);
        public static final e<Integer> P = a("ttc_max_click_distance_dp", 10);
        public static final e<Boolean> P0 = a("incent_nonvideo_warning_enabled", false);
        public static final e<Boolean> P1 = a("handle_window_actions", false);
        public static final e<Boolean> P2 = a("qq4", true);
        public static final e<Boolean> P3 = a("render_empty_adview", true);
        public static final e<String> Q = a("whitelisted_postback_endpoints", "https://prod-a.applovin.com,https://rt.applovin.com/4.0/pix, https://rt.applvn.com/4.0/pix,https://ms.applovin.com/,https://ms.applvn.com/");
        public static final e<String> Q0 = a("text_incent_nonvideo_warning_title", "Attention!");
        public static final e<Long> Q1 = a("inter_display_delay", 200L);
        public static final e<Boolean> Q2 = a("qq5", true);
        public static final e<Boolean> Q3 = a("daostr", false);
        public static final e<String> R = a("fetch_settings_endpoint", "https://ms.applovin.com/");
        public static final e<String> R0 = a("text_incent_nonvideo_warning_body", "You won’t get your reward if the game hasn’t finished.");
        public static final e<Boolean> R1 = a("lock_specific_orientation", false);
        public static final e<Boolean> R2 = a("qq6", true);
        public static final e<Boolean> R3 = a("urrr", false);
        public static final e<String> S = a("fetch_settings_backup_endpoint", "https://ms.applvn.com/");
        public static final e<String> S0 = a("text_incent_nonvideo_warning_close_option", "Close");
        public static final e<Boolean> S1 = a("lhs_skip_button", true);
        public static final e<Boolean> S2 = a("qq7", true);
        public static final e<String> S3 = a("config_consent_dialog_state", "unknown");
        public static final e<String> T = a("adserver_endpoint", "https://a.applovin.com/");
        public static final e<String> T0 = a("text_incent_nonvideo_warning_continue_option", "Keep Playing");
        public static final e<String> T1 = a("soft_buttons_resource_id", "config_showNavigationBar");
        public static final e<Boolean> T2 = a("qq8", true);
        public static final e<String> U = a("adserver_backup_endpoint", "https://a.applvn.com/");
        public static final e<Boolean> U0 = a("video_callbacks_for_incent_nonvideo_ads_enabled", true);
        public static final e<Boolean> U1 = a("countdown_toggleable", false);
        public static final e<Boolean> U2 = a("pui", true);
        public static final e<String> V = a("api_endpoint", "https://d.applovin.com/");
        public static final e<Boolean> V0 = a("wrapped_zones", false);
        public static final e<Boolean> V1 = a("mute_controls_enabled", false);
        public static final e<String> V2 = a("plugin_version", "");
        public static final e<String> W = a("api_backup_endpoint", "https://d.applvn.com/");
        public static final e<String> W0 = a("wrapped_sizes", "");
        public static final e<Boolean> W1 = a("allow_user_muting", true);
        public static final e<Boolean> W2 = a("hgn", false);
        public static final e<Boolean> X0 = a("return_wrapped_ad_on_empty_queue", false);
        public static final e<Boolean> X1 = a("mute_videos", false);
        public static final e<Boolean> X2 = a("citab", false);
        public static final e<String> Y = a("event_tracking_endpoint_v2", "https://rt.applovin.com/");
        public static final e<Boolean> Y0 = a("consider_wrapped_ad_preloaded", false);
        public static final e<Boolean> Y1 = a("show_mute_by_default", false);
        public static final e<Boolean> Y2 = a("cit", false);
        public static final e<String> Z = a("event_tracking_backup_endpoint_v2", "https://rt.applvn.com/");
        public static final e<Boolean> Z0 = a("check_webview_has_gesture", false);
        public static final e<Boolean> Z1 = a("mute_with_user_settings", true);
        public static final e<Boolean> Z2 = a("cso", false);
        public static final e<String> a0 = a("fetch_variables_endpoint", "https://ms.applovin.com/");
        public static final e<Integer> a1 = a("close_button_touch_area", 0);
        public static final e<Integer> a2 = a("mute_button_size", 32);
        public static final e<Boolean> a3 = a("cfs", false);
        public static final e<String> b0 = a("fetch_variables_backup_endpoint", "https://ms.applvn.com/");
        public static final e<Long> b1 = a("viewability_adview_imp_delay_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(1)));
        public static final e<Integer> b2 = a("mute_button_margin", 10);
        public static final e<Boolean> b3 = a("cmi", false);

        /* renamed from: c  reason: collision with root package name */
        private static final List<?> f3998c = Arrays.asList(Boolean.class, Float.class, Integer.class, Long.class, String.class);
        public static final e<Boolean> c0 = a("bte", true);
        public static final e<Integer> c1 = a("viewability_adview_banner_min_width", 320);
        public static final e<Integer> c2 = a("mute_button_gravity", 85);
        public static final e<Boolean> c3 = a("cvs", false);

        /* renamed from: d  reason: collision with root package name */
        private static final Map<String, e<?>> f3999d = new HashMap((int) AdRequest.MAX_CONTENT_URL_LENGTH);
        public static final e<String> d0 = a("token_type_prefixes_r", "4!");
        public static final e<Integer> d1 = a("viewability_adview_banner_min_height", Integer.valueOf(AppLovinAdSize.BANNER.getHeight()));
        public static final e<Boolean> d2 = a("video_immersive_mode_enabled", false);
        public static final e<String> d3 = a("emulator_hardware_list", "ranchu,goldfish,vbox");

        /* renamed from: e  reason: collision with root package name */
        public static final e<Boolean> f4000e = a("is_disabled", false);
        public static final e<String> e0 = a("token_type_prefixes_arj", "json_v3!");
        public static final e<Integer> e1 = a("viewability_adview_mrec_min_width", Integer.valueOf(AppLovinAdSize.MREC.getWidth()));
        public static final e<Long> e2 = a("progress_bar_step", 25L);
        public static final e<String> e3 = a("emulator_device_list", "generic,vbox");

        /* renamed from: f  reason: collision with root package name */
        public static final e<String> f4001f = a("device_id", "");
        public static final e<String> f0 = a("top_level_events", "landing,paused,resumed,checkout,iap");
        public static final e<Integer> f1 = a("viewability_adview_mrec_min_height", Integer.valueOf(AppLovinAdSize.MREC.getWidth()));
        public static final e<Integer> f2 = a("progress_bar_scale", 10000);
        public static final e<String> f3 = a("emulator_manufacturer_list", "Genymotion");

        /* renamed from: g  reason: collision with root package name */
        public static final e<String> f4002g = a("device_token", "");
        public static final e<Boolean> g0 = a("events_enabled", true);
        public static final e<Integer> g1 = a("viewability_adview_leader_min_width", 728);
        public static final e<Integer> g2 = a("progress_bar_vertical_padding", -8);
        public static final e<String> g3 = a("emulator_model_list", "Android SDK built for x86");

        /* renamed from: h  reason: collision with root package name */
        public static final e<Boolean> f4003h = a("is_verbose_logging", false);
        public static final e<String> h0 = a("valid_super_property_types", String.class.getName() + "," + Integer.class.getName() + "," + Long.class.getName() + "," + Double.class.getName() + "," + Float.class.getName() + "," + Date.class.getName() + "," + Uri.class.getName() + "," + List.class.getName() + "," + Map.class.getName());
        public static final e<Integer> h1 = a("viewability_adview_leader_min_height", Integer.valueOf(AppLovinAdSize.LEADER.getWidth()));
        public static final e<Long> h2 = a("video_resume_delay", 250L);
        public static final e<Boolean> h3 = a("adr", false);

        /* renamed from: i  reason: collision with root package name */
        public static final e<String> f4004i = a("sc", "");
        public static final e<Boolean> i0 = a("persist_super_properties", true);
        public static final e<Float> i1 = a("viewability_adview_min_alpha", Float.valueOf(10.0f));
        public static final e<Boolean> i2 = a("is_video_skippable", false);
        public static final e<Float> i3 = a("volume_normalization_factor", Float.valueOf(6.6666665f));

        /* renamed from: j  reason: collision with root package name */
        public static final e<String> f4005j = a("sc2", "");
        public static final e<Integer> j0 = a("super_property_string_max_length", 1024);
        public static final e<Long> j1 = a("viewability_timer_min_visible_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(1)));
        public static final e<Integer> j2 = a("vs_buffer_indicator_size", 50);
        public static final e<Boolean> j3 = a("user_agent_collection_enabled", false);

        /* renamed from: k  reason: collision with root package name */
        public static final e<String> f4006k = a("server_installed_at", "");
        public static final e<Integer> k0 = a("super_property_url_max_length", 1024);
        public static final e<Long> k1 = a("viewability_timer_interval_ms", 100L);
        public static final e<Boolean> k2 = a("video_zero_length_as_computed", false);
        public static final e<Long> k3 = a("user_agent_collection_timeout_ms", 600L);
        public static final e<Boolean> l = a("trn", false);
        public static final e<Integer> l0 = a("preload_callback_timeout_seconds", -1);
        public static final e<Boolean> l1 = a("dismiss_expanded_adview_on_refresh", false);
        public static final e<Long> l2 = a("set_poststitial_muted_initial_delay_ms", 500L);
        public static final e<String> l3 = a("webview_package_name", "com.google.android.webview");
        public static final e<Boolean> m = a("honor_publisher_settings", true);
        public static final e<Boolean> m0 = a("ad_preload_enabled", true);
        public static final e<Boolean> m1 = a("dismiss_expanded_adview_on_detach", false);
        public static final e<Boolean> m2 = a("widget_fail_on_slot_count_diff", true);
        public static final e<Boolean> m3 = a("is_track_ad_info", true);
        public static final e<Boolean> n = a("track_network_response_codes", false);
        public static final e<String> n0 = a("ad_auto_preload_sizes", "");
        public static final e<Boolean> n1 = a("contract_expanded_ad_on_close", true);
        public static final e<Integer> n2 = a("native_batch_precache_count", 1);
        public static final e<Boolean> n3 = a("submit_ad_stats_enabled", false);
        public static final e<Boolean> o = a("submit_network_response_codes", false);
        public static final e<Boolean> o0 = a("ad_auto_preload_incent", true);
        public static final e<Long> o1 = a("expandable_close_button_animation_duration_ms", 300L);
        public static final e<Integer> o2 = a("submit_postback_timeout", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(10)));
        public static final e<Integer> o3 = a("submit_ad_stats_connection_timeout", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(30)));
        public static final e<Boolean> p = a("clear_network_response_codes_on_request", true);
        public static final e<Boolean> p0 = a("ad_auto_preload_native", false);
        public static final e<Integer> p1 = a("expandable_close_button_size", 27);
        public static final e<Integer> p2 = a("submit_postback_retries", 4);
        public static final e<Integer> p3 = a("submit_ad_stats_retry_count", 1);
        public static final e<Boolean> q = a("preserve_network_response_codes", false);
        public static final e<Boolean> q0 = a("preload_native_ad_on_dequeue", false);
        public static final e<Integer> q1 = a("expandable_h_close_button_margin", 10);
        public static final e<Integer> q2 = a("max_postback_attempts", 3);
        public static final e<Integer> q3 = a("submit_ad_stats_max_count", Integer.valueOf((int) TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL));
        public static final e<Boolean> r = a("clear_completion_callback_on_failure", false);
        public static final e<Integer> r0 = a("preload_capacity_banner_regular", 0);
        public static final e<Integer> r1 = a("expandable_t_close_button_margin", 10);
        public static final e<Integer> r2 = a("get_retry_delay_v1", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(10)));
        public static final e<Boolean> r3 = a("asdm", false);
        public static final e<Long> s = a("sicd_ms", 0L);
        public static final e<Boolean> s0 = a("use_per_format_cache_queues", true);
        public static final e<Boolean> s1 = a("expandable_lhs_close_button", false);
        public static final e<Integer> s2 = a("http_connection_timeout", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(30)));
        public static final e<Boolean> s3 = a("task_stats_enabled", false);
        public static final e<Integer> t = a("logcat_max_line_size", 1000);
        public static final e<Integer> t0 = a("extended_preload_capacity_banner_regular", 15);
        public static final e<Integer> t1 = a("expandable_close_button_touch_area", 0);
        public static final e<Integer> t2 = a("http_socket_timeout", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(20)));
        public static final e<Boolean> t3 = a("error_reporting_enabled", false);
        public static final e<Integer> u = a("stps", 32);
        public static final e<Integer> u0 = a("preload_capacity_zone", 1);
        public static final e<Boolean> u1 = a("click_failed_expand", false);
        public static final e<Boolean> u2 = a("force_ssl", false);
        public static final e<Integer> u3 = a("error_reporting_log_limit", 100);
        public static final e<Boolean> v = a("ustp", false);
        public static final e<Integer> v0 = a("preload_capacity_zone_native", 1);
        public static final e<Integer> v1 = a("auxiliary_operations_threads", 3);
        public static final e<Integer> v2 = a("fetch_ad_connection_timeout", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(30)));
        public static final e<String> v3 = a("vast_image_html", "<html><head><style>html,body{height:100%;width:100%}body{background-image:url({SOURCE});background-repeat:no-repeat;background-size:contain;background-position:center;}a{position:absolute;top:0;bottom:0;left:0;right:0}</style></head><body><a href=\"applovin://com.applovin.sdk/adservice/track_click_now\"></a></body></html>");
        public static final e<Boolean> w = a("publisher_can_show_consent_dialog", true);
        public static final e<Integer> w0 = a("extended_preload_capacity_zone", 15);
        public static final e<Integer> w1 = a("caching_operations_threads", 8);
        public static final e<Integer> w2 = a("fetch_ad_retry_count_v1", 1);
        public static final e<String> w3 = a("vast_link_html", "<html><head><style>html,body,iframe{height:100%;width:100%;}body{margin:0}iframe{border:0;overflow:hidden;position:absolute}</style></head><body><iframe src={SOURCE} frameborder=0></iframe></body></html>");
        public static final e<String> x = a("consent_dialog_url", "https://assets.applovin.com/gdpr/flow_v1/gdpr-flow-1.html");
        public static final e<Boolean> x0 = a("preload_merge_init_tasks_zones", false);
        public static final e<Long> x1 = a("fullscreen_ad_pending_display_state_timeout_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(10)));
        public static final e<Integer> x2 = a("submit_data_retry_count_v1", 1);
        public static final e<Integer> x3 = a("vast_max_response_length", 640000);
        public static final e<Boolean> y = a("consent_dialog_immersive_mode_on", false);
        public static final e<Boolean> y0 = a("honor_publisher_settings_auto_preload_ad_sizes", true);
        public static final e<Long> y1 = a("fullscreen_ad_showing_state_timeout_ms", Long.valueOf(TimeUnit.MINUTES.toMillis(2)));
        public static final e<Integer> y2 = a("response_buffer_size", 16000);
        public static final e<Integer> y3 = a("vast_max_wrapper_depth", 5);
        public static final e<Long> z = a("consent_dialog_show_from_alert_delay_ms", 450L);
        public static final e<Boolean> z0 = a("honor_publisher_settings_auto_preload_ad_types", true);
        public static final e<Boolean> z1 = a("lhs_close_button_video", false);
        public static final e<Integer> z2 = a("fetch_basic_settings_connection_timeout_ms", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(10)));
        public static final e<Long> z3 = a("vast_progress_tracking_countdown_step", Long.valueOf(TimeUnit.SECONDS.toMillis(1)));

        /* renamed from: a  reason: collision with root package name */
        private final String f4007a;

        /* renamed from: b  reason: collision with root package name */
        private final T f4008b;

        static {
            a("preload_capacity_mrec_regular", 0);
            a("preload_capacity_leader_regular", 0);
            a("preload_capacity_inter_regular", 0);
            a("preload_capacity_inter_videoa", 0);
            a("extended_preload_capacity_mrec_regular", 15);
            a("extended_preload_capacity_leader_regular", 15);
            a("extended_preload_capacity_inter_regular", 15);
            a("extended_preload_capacity_inter_videoa", 15);
            a("preload_capacity_native_native", 0);
            a("preload_merge_init_tasks_inter_regular", false);
            a("preload_merge_init_tasks_inter_videoa", false);
            a("preload_merge_init_tasks_banner_regular", false);
            a("preload_merge_init_tasks_mrec_regular", false);
            a("preload_merge_init_tasks_leader_regular", false);
            a("honor_publisher_settings_verbose_logging", true);
            a("vr_retry_count_v1", 1);
            a("network_available_if_none_detected", true);
        }

        public e(String str, T t4) {
            if (str == null) {
                throw new IllegalArgumentException("No name specified");
            } else if (t4 != null) {
                this.f4007a = str;
                this.f4008b = t4;
            } else {
                throw new IllegalArgumentException("No default value specified");
            }
        }

        protected static <T> e<T> a(String str, T t4) {
            if (t4 == null) {
                throw new IllegalArgumentException("No default value specified");
            } else if (f3998c.contains(t4.getClass())) {
                e<T> eVar = new e<>(str, t4);
                if (!f3999d.containsKey(str)) {
                    f3999d.put(str, eVar);
                    return eVar;
                }
                throw new IllegalArgumentException("Setting has already been used: " + str);
            } else {
                throw new IllegalArgumentException("Unsupported value type: " + t4.getClass());
            }
        }

        public static Collection<e<?>> c() {
            return Collections.unmodifiableCollection(f3999d.values());
        }

        /* access modifiers changed from: package-private */
        public T a(Object obj) {
            return this.f4008b.getClass().cast(obj);
        }

        public String a() {
            return this.f4007a;
        }

        public T b() {
            return this.f4008b;
        }

        public int compareTo(Object obj) {
            if (!(obj instanceof e)) {
                return 0;
            }
            return this.f4007a.compareTo(((e) obj).a());
        }
    }

    public class f {

        /* renamed from: a  reason: collision with root package name */
        protected final k f4009a;

        /* renamed from: b  reason: collision with root package name */
        protected final q f4010b;

        /* renamed from: c  reason: collision with root package name */
        protected final Context f4011c;

        /* renamed from: d  reason: collision with root package name */
        protected final SharedPreferences f4012d;

        /* renamed from: e  reason: collision with root package name */
        private final Map<String, Object> f4013e = new HashMap();

        /* renamed from: f  reason: collision with root package name */
        private Map<String, Object> f4014f;

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0035 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public f(com.applovin.impl.sdk.k r4) {
            /*
                r3 = this;
                r3.<init>()
                java.util.HashMap r0 = new java.util.HashMap
                r0.<init>()
                r3.f4013e = r0
                r3.f4009a = r4
                com.applovin.impl.sdk.q r0 = r4.Z()
                r3.f4010b = r0
                android.content.Context r0 = r4.d()
                r3.f4011c = r0
                android.content.Context r0 = r3.f4011c
                java.lang.String r1 = "com.applovin.sdk.1"
                r2 = 0
                android.content.SharedPreferences r0 = r0.getSharedPreferences(r1, r2)
                r3.f4012d = r0
                java.lang.Class<com.applovin.impl.sdk.c$e> r0 = com.applovin.impl.sdk.c.e.class
                java.lang.String r0 = r0.getName()     // Catch:{ all -> 0x0035 }
                java.lang.Class.forName(r0)     // Catch:{ all -> 0x0035 }
                java.lang.Class<com.applovin.impl.sdk.c$d> r0 = com.applovin.impl.sdk.c.d.class
                java.lang.String r0 = r0.getName()     // Catch:{ all -> 0x0035 }
                java.lang.Class.forName(r0)     // Catch:{ all -> 0x0035 }
            L_0x0035:
                com.applovin.sdk.AppLovinSdkSettings r0 = r4.P()     // Catch:{ all -> 0x0053 }
                java.lang.Class r0 = r0.getClass()     // Catch:{ all -> 0x0053 }
                java.lang.String r1 = "localSettings"
                java.lang.reflect.Field r0 = com.applovin.impl.sdk.utils.p.a(r0, r1)     // Catch:{ all -> 0x0053 }
                r1 = 1
                r0.setAccessible(r1)     // Catch:{ all -> 0x0053 }
                com.applovin.sdk.AppLovinSdkSettings r4 = r4.P()     // Catch:{ all -> 0x0053 }
                java.lang.Object r4 = r0.get(r4)     // Catch:{ all -> 0x0053 }
                java.util.HashMap r4 = (java.util.HashMap) r4     // Catch:{ all -> 0x0053 }
                r3.f4014f = r4     // Catch:{ all -> 0x0053 }
            L_0x0053:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.c.f.<init>(com.applovin.impl.sdk.k):void");
        }

        private static Object a(String str, JSONObject jSONObject, Object obj) throws JSONException {
            if (obj instanceof Boolean) {
                return Boolean.valueOf(jSONObject.getBoolean(str));
            }
            if (obj instanceof Float) {
                return Float.valueOf((float) jSONObject.getDouble(str));
            }
            if (obj instanceof Integer) {
                return Integer.valueOf(jSONObject.getInt(str));
            }
            if (obj instanceof Long) {
                return Long.valueOf(jSONObject.getLong(str));
            }
            if (obj instanceof String) {
                return jSONObject.getString(str);
            }
            throw new RuntimeException("SDK Error: unknown value type: " + obj.getClass());
        }

        private <T> T c(e<T> eVar) {
            try {
                return eVar.a(this.f4014f.get(eVar.a()));
            } catch (Throwable unused) {
                return null;
            }
        }

        private String e() {
            return "com.applovin.sdk." + p.a(this.f4009a.X()) + ".";
        }

        public <ST> e<ST> a(String str, e eVar) {
            for (e<ST> next : e.c()) {
                if (next.a().equals(str)) {
                    return next;
                }
            }
            return eVar;
        }

        public <T> T a(e eVar) {
            if (eVar != null) {
                synchronized (this.f4013e) {
                    try {
                        T c2 = c(eVar);
                        if (c2 != null) {
                            return c2;
                        }
                        Object obj = this.f4013e.get(eVar.a());
                        if (obj != null) {
                            T a2 = eVar.a(obj);
                            return a2;
                        }
                        T b2 = eVar.b();
                        return b2;
                    } catch (Throwable unused) {
                        q Z = this.f4009a.Z();
                        Z.e("SettingsManager", "Unable to retrieve value for setting " + eVar.a() + "; using default...");
                        return eVar.b();
                    }
                }
            } else {
                throw new IllegalArgumentException("No setting type specified");
            }
        }

        public void a() {
            if (this.f4011c != null) {
                String e2 = e();
                synchronized (this.f4013e) {
                    SharedPreferences.Editor edit = this.f4012d.edit();
                    for (e next : e.c()) {
                        Object obj = this.f4013e.get(next.a());
                        if (obj != null) {
                            this.f4009a.a(e2 + next.a(), obj, edit);
                        }
                    }
                    edit.apply();
                }
                return;
            }
            throw new IllegalArgumentException("No context specified");
        }

        public <T> void a(e<?> eVar, Object obj) {
            if (eVar == null) {
                throw new IllegalArgumentException("No setting type specified");
            } else if (obj != null) {
                synchronized (this.f4013e) {
                    this.f4013e.put(eVar.a(), obj);
                }
            } else {
                throw new IllegalArgumentException("No new value specified");
            }
        }

        public void a(AppLovinSdkSettings appLovinSdkSettings) {
            boolean z;
            boolean z2;
            if (appLovinSdkSettings != null) {
                synchronized (this.f4013e) {
                    if (((Boolean) this.f4009a.a(e.f4003h)).booleanValue()) {
                        this.f4013e.put(e.f4003h.a(), Boolean.valueOf(appLovinSdkSettings.isVerboseLoggingEnabled()));
                    }
                    if (((Boolean) this.f4009a.a(e.y0)).booleanValue()) {
                        String autoPreloadSizes = appLovinSdkSettings.getAutoPreloadSizes();
                        if (!m.b(autoPreloadSizes)) {
                            autoPreloadSizes = "NONE";
                        }
                        if (autoPreloadSizes.equals("NONE")) {
                            this.f4013e.put(e.n0.a(), "");
                        } else {
                            this.f4013e.put(e.n0.a(), autoPreloadSizes);
                        }
                    }
                    if (((Boolean) this.f4009a.a(e.z0)).booleanValue()) {
                        String autoPreloadTypes = appLovinSdkSettings.getAutoPreloadTypes();
                        if (!m.b(autoPreloadTypes)) {
                            autoPreloadTypes = "NONE";
                        }
                        boolean z3 = false;
                        if (!"NONE".equals(autoPreloadTypes)) {
                            z2 = false;
                            z = false;
                            for (String next : com.applovin.impl.sdk.utils.e.a(autoPreloadTypes)) {
                                if (next.equals(AppLovinAdType.REGULAR.getLabel())) {
                                    z3 = true;
                                } else {
                                    if (!next.equals(AppLovinAdType.INCENTIVIZED.getLabel()) && !next.contains("INCENT")) {
                                        if (!next.contains("REWARD")) {
                                            if (next.equals(AppLovinAdType.NATIVE.getLabel())) {
                                                z = true;
                                            }
                                        }
                                    }
                                    z2 = true;
                                }
                            }
                        } else {
                            z2 = false;
                            z = false;
                        }
                        if (!z3) {
                            this.f4013e.put(e.n0.a(), "");
                        }
                        this.f4013e.put(e.o0.a(), Boolean.valueOf(z2));
                        this.f4013e.put(e.p0.a(), Boolean.valueOf(z));
                    }
                }
            }
        }

        public void a(JSONObject jSONObject) {
            q qVar;
            String str;
            String str2;
            synchronized (this.f4013e) {
                Iterator<String> keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    if (next != null && next.length() > 0) {
                        try {
                            e<Long> a2 = a(next, (e) null);
                            if (a2 != null) {
                                this.f4013e.put(a2.a(), a(next, jSONObject, a2.b()));
                                if (a2 == e.I3) {
                                    this.f4013e.put(e.J3.a(), Long.valueOf(System.currentTimeMillis()));
                                }
                            }
                        } catch (JSONException e2) {
                            th = e2;
                            qVar = this.f4010b;
                            str = "SettingsManager";
                            str2 = "Unable to parse JSON settingsValues array";
                            qVar.b(str, str2, th);
                        } catch (Throwable th) {
                            th = th;
                            qVar = this.f4010b;
                            str = "SettingsManager";
                            str2 = "Unable to convert setting object ";
                            qVar.b(str, str2, th);
                        }
                    }
                }
            }
        }

        public List<String> b(e<String> eVar) {
            return com.applovin.impl.sdk.utils.e.a((String) a(eVar));
        }

        public void b() {
            if (this.f4011c != null) {
                String e2 = e();
                synchronized (this.f4013e) {
                    for (e next : e.c()) {
                        try {
                            Object a2 = this.f4009a.a(e2 + next.a(), null, next.b().getClass(), this.f4012d);
                            if (a2 != null) {
                                this.f4013e.put(next.a(), a2);
                            }
                        } catch (Exception e3) {
                            q qVar = this.f4010b;
                            qVar.b("SettingsManager", "Unable to load \"" + next.a() + "\"", e3);
                        }
                    }
                }
                return;
            }
            throw new IllegalArgumentException("No context specified");
        }

        public void c() {
            synchronized (this.f4013e) {
                this.f4013e.clear();
            }
            this.f4009a.a(this.f4012d);
        }

        public boolean d() {
            return this.f4009a.P().isVerboseLoggingEnabled() || ((Boolean) a(e.f4003h)).booleanValue();
        }
    }

    public class g<T> {
        public static final g<String> A = new g<>("com.applovin.sdk.mediation_provider", String.class);

        /* renamed from: c  reason: collision with root package name */
        public static final g<String> f4015c = new g<>("com.applovin.sdk.impl.isFirstRun", String.class);

        /* renamed from: d  reason: collision with root package name */
        public static final g<Boolean> f4016d = new g<>("com.applovin.sdk.launched_before", Boolean.class);

        /* renamed from: e  reason: collision with root package name */
        public static final g<String> f4017e = new g<>("com.applovin.sdk.user_id", String.class);

        /* renamed from: f  reason: collision with root package name */
        public static final g<String> f4018f = new g<>("com.applovin.sdk.compass_id", String.class);

        /* renamed from: g  reason: collision with root package name */
        public static final g<String> f4019g = new g<>("com.applovin.sdk.compass_random_token", String.class);

        /* renamed from: h  reason: collision with root package name */
        public static final g<String> f4020h = new g<>("com.applovin.sdk.applovin_random_token", String.class);

        /* renamed from: i  reason: collision with root package name */
        public static final g<String> f4021i = new g<>("com.applovin.sdk.device_test_group", String.class);

        /* renamed from: j  reason: collision with root package name */
        public static final g<String> f4022j = new g<>("com.applovin.sdk.variables", String.class);

        /* renamed from: k  reason: collision with root package name */
        public static final g<Boolean> f4023k = new g<>("com.applovin.sdk.compliance.has_user_consent", Boolean.class);
        public static final g<Boolean> l = new g<>("com.applovin.sdk.compliance.is_age_restricted_user", Boolean.class);
        public static final g<HashSet> m = new g<>("com.applovin.sdk.impl.postbackQueue.key", HashSet.class);
        public static final g<String> n = new g<>("com.applovin.sdk.stats", String.class);
        public static final g<String> o = new g<>("com.applovin.sdk.errors", String.class);
        public static final g<HashSet> p = new g<>("com.applovin.sdk.task.stats", HashSet.class);
        public static final g<String> q = new g<>("com.applovin.sdk.network_response_code_mapping", String.class);
        public static final g<String> r = new g<>("com.applovin.sdk.event_tracking.super_properties", String.class);
        public static final g<String> s = new g<>("com.applovin.sdk.request_tracker.counter", String.class);
        public static final g<String> t = new g<>("com.applovin.sdk.zones", String.class);
        public static final g<HashSet> u = new g<>("com.applovin.sdk.ad.stats", HashSet.class);
        public static final g<Integer> v = new g<>("com.applovin.sdk.last_video_position", Integer.class);
        public static final g<Boolean> w = new g<>("com.applovin.sdk.should_resume_video", Boolean.class);
        public static final g<String> x = new g<>("com.applovin.sdk.mediation.signal_providers", String.class);
        public static final g<String> y = new g<>("com.applovin.sdk.mediation.auto_init_adapters", String.class);
        public static final g<String> z = new g<>("com.applovin.sdk.persisted_data", String.class);

        /* renamed from: a  reason: collision with root package name */
        private final String f4024a;

        /* renamed from: b  reason: collision with root package name */
        private final Class<T> f4025b;

        static {
            new g("com.applovin.sdk.mediation.test_mode_network", String.class);
        }

        public g(String str, Class<T> cls) {
            this.f4024a = str;
            this.f4025b = cls;
        }

        public String a() {
            return this.f4024a;
        }

        public Class<T> b() {
            return this.f4025b;
        }

        public String toString() {
            return "Key{name='" + this.f4024a + '\'' + ", type=" + this.f4025b + '}';
        }
    }

    public final class h {

        /* renamed from: b  reason: collision with root package name */
        private static SharedPreferences f4026b;

        /* renamed from: a  reason: collision with root package name */
        private final SharedPreferences f4027a;

        public h(k kVar) {
            this.f4027a = kVar.d().getSharedPreferences("com.applovin.sdk.preferences." + kVar.X(), 0);
        }

        private static SharedPreferences a(Context context) {
            if (f4026b == null) {
                f4026b = context.getSharedPreferences("com.applovin.sdk.shared", 0);
            }
            return f4026b;
        }

        public static <T> T a(String str, Object obj, Class cls, SharedPreferences sharedPreferences) {
            Object obj2;
            StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
            try {
                if (sharedPreferences.contains(str)) {
                    if (Boolean.class.equals(cls)) {
                        obj2 = Boolean.valueOf(obj != null ? sharedPreferences.getBoolean(str, ((Boolean) obj).booleanValue()) : sharedPreferences.getBoolean(str, false));
                    } else if (Float.class.equals(cls)) {
                        obj2 = Float.valueOf(obj != null ? sharedPreferences.getFloat(str, ((Float) obj).floatValue()) : sharedPreferences.getFloat(str, Animation.CurveTimeline.LINEAR));
                    } else if (Integer.class.equals(cls)) {
                        obj2 = Integer.valueOf(obj != null ? sharedPreferences.getInt(str, ((Integer) obj).intValue()) : sharedPreferences.getInt(str, 0));
                    } else if (Long.class.equals(cls)) {
                        obj2 = Long.valueOf(obj != null ? sharedPreferences.getLong(str, ((Long) obj).longValue()) : sharedPreferences.getLong(str, 0));
                    } else {
                        obj2 = String.class.equals(cls) ? sharedPreferences.getString(str, (String) obj) : Set.class.isAssignableFrom(cls) ? sharedPreferences.getStringSet(str, (Set) obj) : obj;
                    }
                    if (obj2 != null) {
                        return cls.cast(obj2);
                    }
                    StrictMode.setThreadPolicy(allowThreadDiskReads);
                    return obj;
                }
                StrictMode.setThreadPolicy(allowThreadDiskReads);
                return obj;
            } catch (Throwable th) {
                q.c("SharedPreferencesManager", "Error getting value for key: " + str, th);
                return obj;
            } finally {
                StrictMode.setThreadPolicy(allowThreadDiskReads);
            }
        }

        public static <T> void a(g gVar, Context context) {
            a(context).edit().remove(gVar.a()).apply();
        }

        public static <T> void a(g gVar, Object obj, Context context) {
            a(gVar.a(), obj, a(context), (SharedPreferences.Editor) null);
        }

        private static <T> void a(String str, Object obj, SharedPreferences sharedPreferences, SharedPreferences.Editor editor) {
            boolean z = true;
            boolean z2 = editor != null;
            if (!z2) {
                editor = sharedPreferences.edit();
            }
            if (obj instanceof Boolean) {
                editor.putBoolean(str, ((Boolean) obj).booleanValue());
            } else if (obj instanceof Float) {
                editor.putFloat(str, ((Float) obj).floatValue());
            } else if (obj instanceof Integer) {
                editor.putInt(str, ((Integer) obj).intValue());
            } else if (obj instanceof Long) {
                editor.putLong(str, ((Long) obj).longValue());
            } else if (obj instanceof String) {
                editor.putString(str, (String) obj);
            } else if (obj instanceof Set) {
                editor.putStringSet(str, (Set) obj);
            } else {
                q.i("SharedPreferencesManager", "Unable to put default value of invalid type: " + obj);
                z = false;
            }
            if (z && !z2) {
                editor.apply();
            }
        }

        public static <T> T b(g gVar, Object obj, Context context) {
            return a(gVar.a(), obj, gVar.b(), a(context));
        }

        public void a(SharedPreferences sharedPreferences) {
            sharedPreferences.edit().clear().apply();
        }

        public <T> void a(g gVar) {
            this.f4027a.edit().remove(gVar.a()).apply();
        }

        public <T> void a(g gVar, Object obj) {
            a(gVar, obj, this.f4027a);
        }

        public <T> void a(g gVar, Object obj, SharedPreferences sharedPreferences) {
            a(gVar.a(), obj, sharedPreferences);
        }

        public <T> void a(String str, Object obj, SharedPreferences.Editor editor) {
            a(str, obj, (SharedPreferences) null, editor);
        }

        public <T> void a(String str, Object obj, SharedPreferences sharedPreferences) {
            a(str, obj, sharedPreferences, (SharedPreferences.Editor) null);
        }

        public <T> T b(g<T> gVar, T t) {
            return b(gVar, t, this.f4027a);
        }

        public <T> T b(g gVar, Object obj, SharedPreferences sharedPreferences) {
            return a(gVar.a(), obj, gVar.b(), sharedPreferences);
        }
    }

    public c(k kVar, b bVar) {
        this.f3992a = kVar;
        this.f3993b = bVar;
    }

    private void b() {
        n nVar = this.f3994c;
        if (nVar != null) {
            nVar.d();
            this.f3994c = null;
        }
    }

    private void c() {
        synchronized (this.f3995d) {
            b();
        }
    }

    private void d() {
        boolean z;
        synchronized (this.f3995d) {
            long currentTimeMillis = this.f3996e - System.currentTimeMillis();
            if (currentTimeMillis <= 0) {
                a();
                z = true;
            } else {
                a(currentTimeMillis);
                z = false;
            }
        }
        if (z) {
            this.f3993b.onAdExpired();
        }
    }

    public void a() {
        synchronized (this.f3995d) {
            b();
            this.f3992a.D().unregisterReceiver(this);
        }
    }

    public void a(long j2) {
        synchronized (this.f3995d) {
            a();
            this.f3996e = System.currentTimeMillis() + j2;
            this.f3992a.D().registerReceiver(this, new IntentFilter("com.applovin.application_paused"));
            this.f3992a.D().registerReceiver(this, new IntentFilter("com.applovin.application_resumed"));
            if (((Boolean) this.f3992a.a(d.A4)).booleanValue() || !this.f3992a.x().a()) {
                this.f3994c = n.a(j2, this.f3992a, new a());
            }
        }
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if ("com.applovin.application_paused".equals(action)) {
            c();
        } else if ("com.applovin.application_resumed".equals(action)) {
            d();
        }
    }
}
