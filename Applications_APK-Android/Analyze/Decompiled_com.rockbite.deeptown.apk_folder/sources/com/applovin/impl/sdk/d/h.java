package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.k;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class h {

    /* renamed from: a  reason: collision with root package name */
    private final k f4084a;

    /* renamed from: b  reason: collision with root package name */
    private final Map<String, Long> f4085b = new HashMap();

    public h(k kVar) {
        if (kVar != null) {
            this.f4084a = kVar;
            return;
        }
        throw new IllegalArgumentException("No sdk specified");
    }

    private void e() {
        try {
            this.f4084a.a(c.g.n, c().toString());
        } catch (Throwable th) {
            this.f4084a.Z().b("GlobalStatsManager", "Unable to save stats", th);
        }
    }

    public long a(g gVar) {
        return a(gVar, 1);
    }

    /* access modifiers changed from: package-private */
    public long a(g gVar, long j2) {
        long longValue;
        synchronized (this.f4085b) {
            Long l = this.f4085b.get(gVar.a());
            if (l == null) {
                l = 0L;
            }
            longValue = l.longValue() + j2;
            this.f4085b.put(gVar.a(), Long.valueOf(longValue));
        }
        e();
        return longValue;
    }

    public void a() {
        synchronized (this.f4085b) {
            this.f4085b.clear();
        }
        e();
    }

    public long b(g gVar) {
        long longValue;
        synchronized (this.f4085b) {
            Long l = this.f4085b.get(gVar.a());
            if (l == null) {
                l = 0L;
            }
            longValue = l.longValue();
        }
        return longValue;
    }

    public void b() {
        synchronized (this.f4085b) {
            for (g a2 : g.b()) {
                this.f4085b.remove(a2.a());
            }
            e();
        }
    }

    public void b(g gVar, long j2) {
        synchronized (this.f4085b) {
            this.f4085b.put(gVar.a(), Long.valueOf(j2));
        }
        e();
    }

    public JSONObject c() throws JSONException {
        JSONObject jSONObject;
        synchronized (this.f4085b) {
            jSONObject = new JSONObject();
            for (Map.Entry next : this.f4085b.entrySet()) {
                jSONObject.put((String) next.getKey(), next.getValue());
            }
        }
        return jSONObject;
    }

    public void c(g gVar) {
        synchronized (this.f4085b) {
            this.f4085b.remove(gVar.a());
        }
        e();
    }

    public void d() {
        try {
            JSONObject jSONObject = new JSONObject((String) this.f4084a.b(c.g.n, "{}"));
            synchronized (this.f4085b) {
                Iterator<String> keys = jSONObject.keys();
                while (keys.hasNext()) {
                    try {
                        String next = keys.next();
                        this.f4085b.put(next, Long.valueOf(jSONObject.getLong(next)));
                    } catch (JSONException unused) {
                    }
                }
            }
        } catch (Throwable th) {
            this.f4084a.Z().b("GlobalStatsManager", "Unable to load stats", th);
        }
    }
}
