package com.applovin.impl.sdk.network;

import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.d.g;
import com.applovin.impl.sdk.d.h;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.q;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.r;
import com.applovin.impl.sdk.utils.s;
import com.applovin.sdk.AppLovinErrorCodes;
import com.google.firebase.perf.network.FirebasePerfUrlConnection;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Locale;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.SAXException;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private final k f4309a;

    /* renamed from: b  reason: collision with root package name */
    private final q f4310b;

    /* renamed from: c  reason: collision with root package name */
    private b f4311c;

    /* renamed from: com.applovin.impl.sdk.network.a$a  reason: collision with other inner class name */
    public static class C0107a {

        /* renamed from: a  reason: collision with root package name */
        private long f4312a;

        /* renamed from: b  reason: collision with root package name */
        private long f4313b;

        /* access modifiers changed from: private */
        public void a(long j2) {
            this.f4312a = j2;
        }

        /* access modifiers changed from: private */
        public void b(long j2) {
            this.f4313b = j2;
        }

        public long a() {
            return this.f4312a;
        }

        public long b() {
            return this.f4313b;
        }
    }

    public static class b {

        /* renamed from: a  reason: collision with root package name */
        private final long f4314a = System.currentTimeMillis();

        /* renamed from: b  reason: collision with root package name */
        private final String f4315b;

        /* renamed from: c  reason: collision with root package name */
        private final long f4316c;

        /* renamed from: d  reason: collision with root package name */
        private final long f4317d;

        b(String str, long j2, long j3) {
            this.f4315b = str;
            this.f4316c = j2;
            this.f4317d = j3;
        }

        public long a() {
            return this.f4314a;
        }

        public String b() {
            return this.f4315b;
        }

        public long c() {
            return this.f4316c;
        }

        public long d() {
            return this.f4317d;
        }

        public String toString() {
            return "RequestMeasurement{timestampMillis=" + this.f4314a + ", urlHostAndPathString='" + this.f4315b + '\'' + ", responseSize=" + this.f4316c + ", connectionTimeMillis=" + this.f4317d + '}';
        }
    }

    public interface c<T> {
        void a(int i2);

        void a(Object obj, int i2);
    }

    public a(k kVar) {
        this.f4309a = kVar;
        this.f4310b = kVar.Z();
    }

    private int a(Throwable th) {
        String message;
        return th instanceof UnknownHostException ? AppLovinErrorCodes.NO_NETWORK : th instanceof SocketTimeoutException ? AppLovinErrorCodes.FETCH_AD_TIMEOUT : th instanceof IOException ? (!((Boolean) this.f4309a.a(c.e.q)).booleanValue() && (message = th.getMessage()) != null && message.toLowerCase(Locale.ENGLISH).contains("authentication challenge")) ? 401 : -100 : th instanceof JSONException ? -104 : -1;
    }

    private <T> T a(String str, T t) throws JSONException, SAXException, ClassCastException {
        if (t == null) {
            return str;
        }
        if (str != null && str.length() >= 3) {
            if (t instanceof JSONObject) {
                return new JSONObject(str);
            }
            if (t instanceof r) {
                return s.a(str, this.f4309a);
            }
            if (t instanceof String) {
                return str;
            }
            q qVar = this.f4310b;
            qVar.e("ConnectionManager", "Failed to process response of type '" + t.getClass().getName() + "'");
        }
        return t;
    }

    private HttpURLConnection a(String str, String str2, Map<String, String> map, int i2) throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) FirebasePerfUrlConnection.instrument(new URL(str).openConnection());
        httpURLConnection.setRequestMethod(str2);
        httpURLConnection.setConnectTimeout(i2 < 0 ? ((Integer) this.f4309a.a(c.e.s2)).intValue() : i2);
        if (i2 < 0) {
            i2 = ((Integer) this.f4309a.a(c.e.t2)).intValue();
        }
        httpURLConnection.setReadTimeout(i2);
        httpURLConnection.setDefaultUseCaches(false);
        httpURLConnection.setAllowUserInteraction(false);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setInstanceFollowRedirects(true);
        httpURLConnection.setDoInput(true);
        if (map != null && map.size() > 0) {
            for (String next : map.keySet()) {
                httpURLConnection.setRequestProperty("AppLovin-" + next, map.get(next));
            }
        }
        return httpURLConnection;
    }

    private void a(int i2, String str) {
        if (((Boolean) this.f4309a.a(c.e.n)).booleanValue()) {
            try {
                d.a(i2, str, this.f4309a.d());
            } catch (Throwable th) {
                q Z = this.f4309a.Z();
                Z.b("ConnectionManager", "Failed to track response code for URL: " + str, th);
            }
        }
    }

    private void a(String str) {
        h hVar;
        g gVar;
        if (m.a(str, com.applovin.impl.sdk.utils.h.c(this.f4309a)) || m.a(str, com.applovin.impl.sdk.utils.h.d(this.f4309a))) {
            hVar = this.f4309a.k();
            gVar = g.f4082k;
        } else if (m.a(str, com.applovin.impl.mediation.d.b.g(this.f4309a)) || m.a(str, com.applovin.impl.mediation.d.b.h(this.f4309a))) {
            hVar = this.f4309a.k();
            gVar = g.q;
        } else {
            hVar = this.f4309a.k();
            gVar = g.l;
        }
        hVar.a(gVar);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r8v0, types: [T, java.lang.Object] */
    /* JADX WARN: Type inference failed for: r8v2, types: [java.lang.Object] */
    /* JADX WARN: Type inference failed for: r5v7, types: [com.applovin.impl.sdk.utils.r] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=T, code=org.json.JSONObject, for r8v0, types: [T, java.lang.Object] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private <T> void a(java.lang.String r5, int r6, java.lang.String r7, org.json.JSONObject r8, boolean r9, com.applovin.impl.sdk.network.a.c<T> r10) {
        /*
            r4 = this;
            com.applovin.impl.sdk.q r0 = r4.f4310b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r6)
            java.lang.String r2 = " received from \""
            r1.append(r2)
            r1.append(r7)
            java.lang.String r1 = r1.toString()
            java.lang.String r2 = "ConnectionManager"
            r0.b(r2, r1)
            com.applovin.impl.sdk.q r0 = r4.f4310b
            r0.a(r2, r5)
            java.lang.String r0 = "\""
            r1 = 200(0xc8, float:2.8E-43)
            if (r6 < r1) goto L_0x00b7
            r1 = 300(0x12c, float:4.2E-43)
            if (r6 >= r1) goto L_0x00b7
            if (r9 == 0) goto L_0x0036
            com.applovin.impl.sdk.k r9 = r4.f4309a
            java.lang.String r9 = r9.X()
            java.lang.String r5 = com.applovin.impl.sdk.utils.l.a(r5, r9)
        L_0x0036:
            if (r5 == 0) goto L_0x0041
            int r9 = r5.length()
            r1 = 2
            if (r9 <= r1) goto L_0x0041
            r9 = 1
            goto L_0x0042
        L_0x0041:
            r9 = 0
        L_0x0042:
            r1 = 204(0xcc, float:2.86E-43)
            if (r6 == r1) goto L_0x00b3
            if (r9 == 0) goto L_0x00b3
            boolean r9 = r8 instanceof java.lang.String     // Catch:{ JSONException -> 0x0096, SAXException -> 0x0088 }
            if (r9 == 0) goto L_0x004e
        L_0x004c:
            r8 = r5
            goto L_0x00b3
        L_0x004e:
            boolean r9 = r8 instanceof com.applovin.impl.sdk.utils.r     // Catch:{ JSONException -> 0x0096, SAXException -> 0x0088 }
            if (r9 == 0) goto L_0x0059
            com.applovin.impl.sdk.k r9 = r4.f4309a     // Catch:{ JSONException -> 0x0096, SAXException -> 0x0088 }
            com.applovin.impl.sdk.utils.r r5 = com.applovin.impl.sdk.utils.s.a(r5, r9)     // Catch:{ JSONException -> 0x0096, SAXException -> 0x0088 }
            goto L_0x004c
        L_0x0059:
            boolean r9 = r8 instanceof org.json.JSONObject     // Catch:{ JSONException -> 0x0096, SAXException -> 0x0088 }
            if (r9 == 0) goto L_0x0064
            org.json.JSONObject r9 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0096, SAXException -> 0x0088 }
            r9.<init>(r5)     // Catch:{ JSONException -> 0x0096, SAXException -> 0x0088 }
            r8 = r9
            goto L_0x00b3
        L_0x0064:
            com.applovin.impl.sdk.q r5 = r4.f4310b     // Catch:{ JSONException -> 0x0096, SAXException -> 0x0088 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0096, SAXException -> 0x0088 }
            r9.<init>()     // Catch:{ JSONException -> 0x0096, SAXException -> 0x0088 }
            java.lang.String r1 = "Unable to handle '"
            r9.append(r1)     // Catch:{ JSONException -> 0x0096, SAXException -> 0x0088 }
            java.lang.Class r1 = r8.getClass()     // Catch:{ JSONException -> 0x0096, SAXException -> 0x0088 }
            java.lang.String r1 = r1.getName()     // Catch:{ JSONException -> 0x0096, SAXException -> 0x0088 }
            r9.append(r1)     // Catch:{ JSONException -> 0x0096, SAXException -> 0x0088 }
            java.lang.String r1 = "'"
            r9.append(r1)     // Catch:{ JSONException -> 0x0096, SAXException -> 0x0088 }
            java.lang.String r9 = r9.toString()     // Catch:{ JSONException -> 0x0096, SAXException -> 0x0088 }
            r5.e(r2, r9)     // Catch:{ JSONException -> 0x0096, SAXException -> 0x0088 }
            goto L_0x00b3
        L_0x0088:
            r5 = move-exception
            r4.a(r7)
            com.applovin.impl.sdk.q r9 = r4.f4310b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "Invalid XML returned from \""
            goto L_0x00a3
        L_0x0096:
            r5 = move-exception
            r4.a(r7)
            com.applovin.impl.sdk.q r9 = r4.f4310b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "Invalid JSON returned from \""
        L_0x00a3:
            r1.append(r3)
            r1.append(r7)
            r1.append(r0)
            java.lang.String r7 = r1.toString()
            r9.b(r2, r7, r5)
        L_0x00b3:
            r10.a(r8, r6)
            goto L_0x00d6
        L_0x00b7:
            com.applovin.impl.sdk.q r5 = r4.f4310b
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            r8.append(r6)
            java.lang.String r9 = " error received from \""
            r8.append(r9)
            r8.append(r7)
            r8.append(r0)
            java.lang.String r7 = r8.toString()
            r5.e(r2, r7)
            r10.a(r6)
        L_0x00d6:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.network.a.a(java.lang.String, int, java.lang.String, java.lang.Object, boolean, com.applovin.impl.sdk.network.a$c):void");
    }

    private void a(String str, String str2, int i2, long j2) {
        q qVar = this.f4310b;
        qVar.c("ConnectionManager", "Successful " + str + " returned " + i2 + " in " + (((float) (System.currentTimeMillis() - j2)) / 1000.0f) + " s over " + com.applovin.impl.sdk.utils.h.b(this.f4309a) + " to \"" + str2 + "\"");
    }

    private void a(String str, String str2, int i2, long j2, Throwable th) {
        q qVar = this.f4310b;
        qVar.b("ConnectionManager", "Failed " + str + " returned " + i2 + " in " + (((float) (System.currentTimeMillis() - j2)) / 1000.0f) + " s over " + com.applovin.impl.sdk.utils.h.b(this.f4309a) + " to \"" + str2 + "\"", th);
    }

    public b a() {
        return this.f4311c;
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:84:0x0214 */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:87:0x0225 */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v13, resolved type: java.io.InputStream} */
    /* JADX WARN: Type inference failed for: r11v0, types: [boolean] */
    /* JADX WARN: Type inference failed for: r11v15 */
    /* JADX WARN: Type inference failed for: r11v16 */
    /* JADX WARN: Type inference failed for: r11v19 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.utils.p.a(java.io.Closeable, com.applovin.impl.sdk.k):void
     arg types: [java.io.InputStream, com.applovin.impl.sdk.k]
     candidates:
      com.applovin.impl.sdk.utils.p.a(android.view.View, com.applovin.impl.sdk.k):android.app.Activity
      com.applovin.impl.sdk.utils.p.a(java.io.File, int):android.graphics.Bitmap
      com.applovin.impl.sdk.utils.p.a(android.content.Context, android.view.View):android.view.View
      com.applovin.impl.sdk.utils.p.a(org.json.JSONObject, com.applovin.impl.sdk.k):com.applovin.impl.sdk.ad.d
      com.applovin.impl.sdk.utils.p.a(com.applovin.sdk.AppLovinAd, com.applovin.impl.sdk.k):com.applovin.sdk.AppLovinAd
      com.applovin.impl.sdk.utils.p.a(java.lang.Object, com.applovin.impl.sdk.k):java.lang.Object
      com.applovin.impl.sdk.utils.p.a(java.lang.Class, java.lang.String):java.lang.reflect.Field
      com.applovin.impl.sdk.utils.p.a(java.util.List<java.lang.String>, com.applovin.impl.sdk.k):java.util.List<java.lang.Class>
      com.applovin.impl.sdk.utils.p.a(java.net.HttpURLConnection, com.applovin.impl.sdk.k):void
      com.applovin.impl.sdk.utils.p.a(long, long):boolean
      com.applovin.impl.sdk.utils.p.a(android.view.View, android.app.Activity):boolean
      com.applovin.impl.sdk.utils.p.a(android.view.View, android.view.View):boolean
      com.applovin.impl.sdk.utils.p.a(java.lang.String, java.util.List<java.lang.String>):boolean
      com.applovin.impl.sdk.utils.p.a(java.io.Closeable, com.applovin.impl.sdk.k):void */
    /* JADX WARNING: Code restructure failed: missing block: B:175:0x0379, code lost:
        if (r26.h() == false) goto L_0x0382;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:173:0x0375 A[SYNTHETIC, Splitter:B:173:0x0375] */
    /* JADX WARNING: Removed duplicated region for block: B:185:0x0396  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T> void a(com.applovin.impl.sdk.network.b<T> r26, com.applovin.impl.sdk.network.a.C0107a r27, com.applovin.impl.sdk.network.a.c<T> r28) {
        /*
            r25 = this;
            r8 = r25
            r0 = r27
            r9 = r28
            if (r26 == 0) goto L_0x041f
            java.lang.String r1 = r26.a()
            java.lang.String r10 = r26.b()
            if (r1 == 0) goto L_0x0417
            if (r10 == 0) goto L_0x040f
            if (r9 == 0) goto L_0x0407
            java.lang.String r2 = r1.toLowerCase()
            java.lang.String r3 = "http"
            boolean r2 = r2.startsWith(r3)
            java.lang.String r7 = "ConnectionManager"
            if (r2 != 0) goto L_0x0043
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "Requested postback submission to non HTTP endpoint "
            r0.append(r2)
            r0.append(r1)
            java.lang.String r1 = "; skipping..."
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.applovin.impl.sdk.q.i(r7, r0)
            r0 = -900(0xfffffffffffffc7c, float:NaN)
            r9.a(r0)
            return
        L_0x0043:
            com.applovin.impl.sdk.k r2 = r8.f4309a
            com.applovin.impl.sdk.c$e<java.lang.Boolean> r3 = com.applovin.impl.sdk.c.e.u2
            java.lang.Object r2 = r2.a(r3)
            java.lang.Boolean r2 = (java.lang.Boolean) r2
            boolean r2 = r2.booleanValue()
            if (r2 == 0) goto L_0x006c
            java.lang.String r2 = "https://"
            boolean r3 = r1.contains(r2)
            if (r3 != 0) goto L_0x006c
            com.applovin.impl.sdk.k r3 = r8.f4309a
            com.applovin.impl.sdk.q r3 = r3.Z()
            java.lang.String r4 = "Plaintext HTTP operation requested; upgrading to HTTPS due to universal SSL setting..."
            r3.d(r7, r4)
            java.lang.String r3 = "http://"
            java.lang.String r1 = r1.replace(r3, r2)
        L_0x006c:
            boolean r11 = r26.m()
            com.applovin.impl.sdk.k r2 = r8.f4309a
            long r2 = com.applovin.impl.sdk.utils.p.a(r2)
            java.util.Map r4 = r26.c()
            if (r4 == 0) goto L_0x0086
            java.util.Map r4 = r26.c()
            boolean r4 = r4.isEmpty()
            if (r4 == 0) goto L_0x008c
        L_0x0086:
            int r4 = r26.i()
            if (r4 < 0) goto L_0x00c0
        L_0x008c:
            java.util.Map r4 = r26.c()
            if (r4 == 0) goto L_0x00a5
            int r5 = r26.i()
            if (r5 < 0) goto L_0x00a5
            int r5 = r26.i()
            java.lang.String r5 = java.lang.String.valueOf(r5)
            java.lang.String r6 = "current_retry_attempt"
            r4.put(r6, r5)
        L_0x00a5:
            if (r11 == 0) goto L_0x00bc
            java.lang.String r4 = com.applovin.impl.sdk.utils.p.a(r4)
            com.applovin.impl.sdk.k r5 = r8.f4309a
            java.lang.String r5 = r5.X()
            java.lang.String r4 = com.applovin.impl.sdk.utils.l.a(r4, r5, r2)
            java.lang.String r5 = "p"
            java.lang.String r1 = com.applovin.impl.sdk.utils.m.a(r1, r5, r4)
            goto L_0x00c0
        L_0x00bc:
            java.lang.String r1 = com.applovin.impl.sdk.utils.m.b(r1, r4)
        L_0x00c0:
            r12 = r1
            long r13 = java.lang.System.currentTimeMillis()
            com.applovin.impl.sdk.q r4 = r8.f4310b     // Catch:{ all -> 0x03c2 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x03c2 }
            r5.<init>()     // Catch:{ all -> 0x03c2 }
            java.lang.String r6 = "Sending "
            r5.append(r6)     // Catch:{ all -> 0x03c2 }
            r5.append(r10)     // Catch:{ all -> 0x03c2 }
            java.lang.String r6 = " request to \""
            r5.append(r6)     // Catch:{ all -> 0x03c2 }
            r5.append(r12)     // Catch:{ all -> 0x03c2 }
            java.lang.String r6 = "\"..."
            r5.append(r6)     // Catch:{ all -> 0x03c2 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x03c2 }
            r4.c(r7, r5)     // Catch:{ all -> 0x03c2 }
            java.util.Map r4 = r26.d()     // Catch:{ all -> 0x03c2 }
            int r5 = r26.k()     // Catch:{ all -> 0x03c2 }
            java.net.HttpURLConnection r5 = r8.a(r12, r10, r4, r5)     // Catch:{ all -> 0x03c2 }
            org.json.JSONObject r4 = r26.e()     // Catch:{ all -> 0x03bb }
            if (r4 == 0) goto L_0x016e
            if (r11 == 0) goto L_0x010f
            org.json.JSONObject r4 = r26.e()     // Catch:{ all -> 0x0165 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0165 }
            com.applovin.impl.sdk.k r6 = r8.f4309a     // Catch:{ all -> 0x0165 }
            java.lang.String r6 = r6.X()     // Catch:{ all -> 0x0165 }
            java.lang.String r2 = com.applovin.impl.sdk.utils.l.a(r4, r6, r2)     // Catch:{ all -> 0x0165 }
            goto L_0x0117
        L_0x010f:
            org.json.JSONObject r2 = r26.e()     // Catch:{ all -> 0x0165 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0165 }
        L_0x0117:
            com.applovin.impl.sdk.q r3 = r8.f4310b     // Catch:{ all -> 0x0165 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0165 }
            r4.<init>()     // Catch:{ all -> 0x0165 }
            java.lang.String r6 = "Request to \""
            r4.append(r6)     // Catch:{ all -> 0x0165 }
            r4.append(r12)     // Catch:{ all -> 0x0165 }
            java.lang.String r6 = "\" is "
            r4.append(r6)     // Catch:{ all -> 0x0165 }
            r4.append(r2)     // Catch:{ all -> 0x0165 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0165 }
            r3.b(r7, r4)     // Catch:{ all -> 0x0165 }
            java.lang.String r3 = "Content-Type"
            java.lang.String r4 = "application/json; charset=utf-8"
            r5.setRequestProperty(r3, r4)     // Catch:{ all -> 0x0165 }
            r3 = 1
            r5.setDoOutput(r3)     // Catch:{ all -> 0x0165 }
            java.lang.String r3 = "UTF-8"
            java.nio.charset.Charset r3 = java.nio.charset.Charset.forName(r3)     // Catch:{ all -> 0x0165 }
            byte[] r3 = r2.getBytes(r3)     // Catch:{ all -> 0x0165 }
            int r3 = r3.length     // Catch:{ all -> 0x0165 }
            r5.setFixedLengthStreamingMode(r3)     // Catch:{ all -> 0x0165 }
            java.io.PrintWriter r3 = new java.io.PrintWriter     // Catch:{ all -> 0x0165 }
            java.io.OutputStreamWriter r4 = new java.io.OutputStreamWriter     // Catch:{ all -> 0x0165 }
            java.io.OutputStream r6 = r5.getOutputStream()     // Catch:{ all -> 0x0165 }
            java.lang.String r1 = "UTF8"
            r4.<init>(r6, r1)     // Catch:{ all -> 0x0165 }
            r3.<init>(r4)     // Catch:{ all -> 0x0165 }
            r3.print(r2)     // Catch:{ all -> 0x0165 }
            r3.close()     // Catch:{ all -> 0x0165 }
            goto L_0x016e
        L_0x0165:
            r0 = move-exception
            r7 = r0
            r24 = r10
            r11 = 0
            r15 = 0
            r10 = r5
            goto L_0x03c9
        L_0x016e:
            int r6 = r5.getResponseCode()     // Catch:{ MalformedURLException -> 0x0358, all -> 0x034e }
            if (r6 <= 0) goto L_0x032a
            com.applovin.impl.sdk.k r1 = r8.f4309a     // Catch:{ MalformedURLException -> 0x0323, all -> 0x031c }
            com.applovin.impl.sdk.c$e<java.lang.Boolean> r2 = com.applovin.impl.sdk.c.e.R3     // Catch:{ MalformedURLException -> 0x0323, all -> 0x031c }
            java.lang.Object r1 = r1.a(r2)     // Catch:{ MalformedURLException -> 0x0323, all -> 0x031c }
            java.lang.Boolean r1 = (java.lang.Boolean) r1     // Catch:{ MalformedURLException -> 0x0323, all -> 0x031c }
            boolean r1 = r1.booleanValue()     // Catch:{ MalformedURLException -> 0x0323, all -> 0x031c }
            if (r1 == 0) goto L_0x019d
            r1 = r25
            r2 = r10
            r3 = r12
            r4 = r6
            r17 = r5
            r15 = r6
            r5 = r13
            r1.a(r2, r3, r4, r5)     // Catch:{ MalformedURLException -> 0x0197, all -> 0x0191 }
            goto L_0x01a0
        L_0x0191:
            r0 = move-exception
            r7 = r0
            r24 = r10
            goto L_0x0345
        L_0x0197:
            r0 = move-exception
            r7 = r0
            r24 = r10
            goto L_0x034c
        L_0x019d:
            r17 = r5
            r15 = r6
        L_0x01a0:
            java.io.InputStream r6 = r17.getInputStream()     // Catch:{ MalformedURLException -> 0x031a, all -> 0x0318 }
            r8.a(r15, r12)     // Catch:{ MalformedURLException -> 0x0312, all -> 0x030c }
            long r1 = java.lang.System.currentTimeMillis()     // Catch:{ MalformedURLException -> 0x0312, all -> 0x030c }
            long r4 = r1 - r13
            com.applovin.impl.sdk.k r1 = r8.f4309a     // Catch:{ MalformedURLException -> 0x0312, all -> 0x030c }
            com.applovin.impl.sdk.c$e<java.lang.Boolean> r2 = com.applovin.impl.sdk.c.e.R3     // Catch:{ MalformedURLException -> 0x0312, all -> 0x030c }
            java.lang.Object r1 = r1.a(r2)     // Catch:{ MalformedURLException -> 0x0312, all -> 0x030c }
            java.lang.Boolean r1 = (java.lang.Boolean) r1     // Catch:{ MalformedURLException -> 0x0312, all -> 0x030c }
            boolean r1 = r1.booleanValue()     // Catch:{ MalformedURLException -> 0x0312, all -> 0x030c }
            if (r1 == 0) goto L_0x0251
            com.applovin.impl.sdk.k r1 = r8.f4309a     // Catch:{ MalformedURLException -> 0x0249, all -> 0x0242 }
            java.lang.String r2 = com.applovin.impl.sdk.utils.h.a(r6, r1)     // Catch:{ MalformedURLException -> 0x0249, all -> 0x0242 }
            boolean r1 = r26.h()     // Catch:{ MalformedURLException -> 0x0249, all -> 0x0242 }
            if (r1 == 0) goto L_0x0231
            if (r0 == 0) goto L_0x0212
            if (r2 == 0) goto L_0x01fc
            int r1 = r2.length()     // Catch:{ MalformedURLException -> 0x01f8, all -> 0x01f4 }
            r16 = r6
            long r6 = (long) r1
            r0.b(r6)     // Catch:{ MalformedURLException -> 0x020a, all -> 0x0202 }
            boolean r1 = r26.n()     // Catch:{ MalformedURLException -> 0x020a, all -> 0x0202 }
            if (r1 == 0) goto L_0x01fe
            com.applovin.impl.sdk.network.a$b r1 = new com.applovin.impl.sdk.network.a$b     // Catch:{ MalformedURLException -> 0x020a, all -> 0x0202 }
            java.lang.String r19 = r26.a()     // Catch:{ MalformedURLException -> 0x020a, all -> 0x0202 }
            int r3 = r2.length()     // Catch:{ MalformedURLException -> 0x020a, all -> 0x0202 }
            long r6 = (long) r3     // Catch:{ MalformedURLException -> 0x020a, all -> 0x0202 }
            r18 = r1
            r20 = r6
            r22 = r4
            r18.<init>(r19, r20, r22)     // Catch:{ MalformedURLException -> 0x020a, all -> 0x0202 }
            r8.f4311c = r1     // Catch:{ MalformedURLException -> 0x020a, all -> 0x0202 }
            goto L_0x01fe
        L_0x01f4:
            r0 = move-exception
            r16 = r6
            goto L_0x0203
        L_0x01f8:
            r0 = move-exception
            r16 = r6
            goto L_0x020b
        L_0x01fc:
            r16 = r6
        L_0x01fe:
            r0.a(r4)     // Catch:{ MalformedURLException -> 0x020a, all -> 0x0202 }
            goto L_0x0214
        L_0x0202:
            r0 = move-exception
        L_0x0203:
            r7 = r0
            r24 = r10
            r11 = r16
            goto L_0x037f
        L_0x020a:
            r0 = move-exception
        L_0x020b:
            r7 = r0
            r24 = r10
            r10 = r16
            goto L_0x0360
        L_0x0212:
            r16 = r6
        L_0x0214:
            int r3 = r17.getResponseCode()     // Catch:{ MalformedURLException -> 0x022d, all -> 0x0229 }
            java.lang.Object r5 = r26.g()     // Catch:{ MalformedURLException -> 0x022d, all -> 0x0229 }
            r1 = r25
            r4 = r12
            r7 = r16
            r6 = r11
            r11 = r7
            r7 = r28
            r1.a(r2, r3, r4, r5, r6, r7)     // Catch:{ MalformedURLException -> 0x023a, all -> 0x0238 }
            goto L_0x023f
        L_0x0229:
            r0 = move-exception
            r11 = r16
            goto L_0x0244
        L_0x022d:
            r0 = move-exception
            r11 = r16
            goto L_0x024b
        L_0x0231:
            r11 = r6
            if (r0 == 0) goto L_0x023c
            r0.a(r4)     // Catch:{ MalformedURLException -> 0x023a, all -> 0x0238 }
            goto L_0x023c
        L_0x0238:
            r0 = move-exception
            goto L_0x0244
        L_0x023a:
            r0 = move-exception
            goto L_0x024b
        L_0x023c:
            r9.a(r2, r15)     // Catch:{ MalformedURLException -> 0x023a, all -> 0x0238 }
        L_0x023f:
            r10 = r11
            goto L_0x0307
        L_0x0242:
            r0 = move-exception
            r11 = r6
        L_0x0244:
            r7 = r0
        L_0x0245:
            r24 = r10
            goto L_0x037f
        L_0x0249:
            r0 = move-exception
            r11 = r6
        L_0x024b:
            r7 = r0
            r24 = r10
            r10 = r11
            goto L_0x0360
        L_0x0251:
            r1 = 200(0xc8, float:2.8E-43)
            if (r15 < r1) goto L_0x02f6
            r1 = 400(0x190, float:5.6E-43)
            if (r15 >= r1) goto L_0x02f6
            if (r0 == 0) goto L_0x026a
            r0.a(r4)     // Catch:{ MalformedURLException -> 0x0263, all -> 0x025f }
            goto L_0x026a
        L_0x025f:
            r0 = move-exception
            r7 = r0
            r11 = r6
            goto L_0x0245
        L_0x0263:
            r0 = move-exception
            r7 = r0
            r24 = r10
            r10 = r6
            goto L_0x0360
        L_0x026a:
            r1 = r25
            r2 = r10
            r3 = r12
            r22 = r4
            r4 = r15
            r24 = r10
            r10 = r6
            r5 = r13
            r1.a(r2, r3, r4, r5)     // Catch:{ MalformedURLException -> 0x030a }
            com.applovin.impl.sdk.k r1 = r8.f4309a     // Catch:{ MalformedURLException -> 0x030a }
            java.lang.String r1 = com.applovin.impl.sdk.utils.h.a(r10, r1)     // Catch:{ MalformedURLException -> 0x030a }
            if (r1 == 0) goto L_0x02ee
            com.applovin.impl.sdk.q r2 = r8.f4310b     // Catch:{ MalformedURLException -> 0x030a }
            r2.a(r7, r1)     // Catch:{ MalformedURLException -> 0x030a }
            if (r0 == 0) goto L_0x028f
            int r2 = r1.length()     // Catch:{ MalformedURLException -> 0x030a }
            long r2 = (long) r2     // Catch:{ MalformedURLException -> 0x030a }
            r0.b(r2)     // Catch:{ MalformedURLException -> 0x030a }
        L_0x028f:
            boolean r0 = r26.n()     // Catch:{ MalformedURLException -> 0x030a }
            if (r0 == 0) goto L_0x02a9
            com.applovin.impl.sdk.network.a$b r0 = new com.applovin.impl.sdk.network.a$b     // Catch:{ MalformedURLException -> 0x030a }
            java.lang.String r19 = r26.a()     // Catch:{ MalformedURLException -> 0x030a }
            int r2 = r1.length()     // Catch:{ MalformedURLException -> 0x030a }
            long r2 = (long) r2     // Catch:{ MalformedURLException -> 0x030a }
            r18 = r0
            r20 = r2
            r18.<init>(r19, r20, r22)     // Catch:{ MalformedURLException -> 0x030a }
            r8.f4311c = r0     // Catch:{ MalformedURLException -> 0x030a }
        L_0x02a9:
            if (r11 == 0) goto L_0x02b5
            com.applovin.impl.sdk.k r0 = r8.f4309a     // Catch:{ MalformedURLException -> 0x030a }
            java.lang.String r0 = r0.X()     // Catch:{ MalformedURLException -> 0x030a }
            java.lang.String r1 = com.applovin.impl.sdk.utils.l.a(r1, r0)     // Catch:{ MalformedURLException -> 0x030a }
        L_0x02b5:
            java.lang.Object r0 = r26.g()     // Catch:{ all -> 0x02c1 }
            java.lang.Object r0 = r8.a(r1, r0)     // Catch:{ all -> 0x02c1 }
            r9.a(r0, r15)     // Catch:{ all -> 0x02c1 }
            goto L_0x0307
        L_0x02c1:
            r0 = move-exception
            com.applovin.impl.sdk.q r1 = r8.f4310b     // Catch:{ MalformedURLException -> 0x030a }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x030a }
            r2.<init>()     // Catch:{ MalformedURLException -> 0x030a }
            java.lang.String r3 = "Unable to parse response from \""
            r2.append(r3)     // Catch:{ MalformedURLException -> 0x030a }
            r2.append(r12)     // Catch:{ MalformedURLException -> 0x030a }
            java.lang.String r3 = "\""
            r2.append(r3)     // Catch:{ MalformedURLException -> 0x030a }
            java.lang.String r2 = r2.toString()     // Catch:{ MalformedURLException -> 0x030a }
            r1.b(r7, r2, r0)     // Catch:{ MalformedURLException -> 0x030a }
            com.applovin.impl.sdk.k r0 = r8.f4309a     // Catch:{ MalformedURLException -> 0x030a }
            com.applovin.impl.sdk.d.h r0 = r0.k()     // Catch:{ MalformedURLException -> 0x030a }
            com.applovin.impl.sdk.d.g r1 = com.applovin.impl.sdk.d.g.l     // Catch:{ MalformedURLException -> 0x030a }
            r0.a(r1)     // Catch:{ MalformedURLException -> 0x030a }
            r0 = -800(0xfffffffffffffce0, float:NaN)
            r9.a(r0)     // Catch:{ MalformedURLException -> 0x030a }
            goto L_0x0307
        L_0x02ee:
            java.lang.Object r0 = r26.g()     // Catch:{ MalformedURLException -> 0x030a }
            r9.a(r0, r15)     // Catch:{ MalformedURLException -> 0x030a }
            goto L_0x0307
        L_0x02f6:
            r24 = r10
            r10 = r6
            r7 = 0
            r1 = r25
            r2 = r24
            r3 = r12
            r4 = r15
            r5 = r13
            r1.a(r2, r3, r4, r5, r7)     // Catch:{ MalformedURLException -> 0x030a }
            r9.a(r15)     // Catch:{ MalformedURLException -> 0x030a }
        L_0x0307:
            r18 = r10
            goto L_0x033f
        L_0x030a:
            r0 = move-exception
            goto L_0x0316
        L_0x030c:
            r0 = move-exception
            r24 = r10
            r10 = r6
            goto L_0x037d
        L_0x0312:
            r0 = move-exception
            r24 = r10
            r10 = r6
        L_0x0316:
            r7 = r0
            goto L_0x0360
        L_0x0318:
            r0 = move-exception
            goto L_0x0320
        L_0x031a:
            r0 = move-exception
            goto L_0x0327
        L_0x031c:
            r0 = move-exception
            r17 = r5
            r15 = r6
        L_0x0320:
            r24 = r10
            goto L_0x0344
        L_0x0323:
            r0 = move-exception
            r17 = r5
            r15 = r6
        L_0x0327:
            r24 = r10
            goto L_0x034b
        L_0x032a:
            r17 = r5
            r15 = r6
            r24 = r10
            r7 = 0
            r1 = r25
            r2 = r24
            r3 = r12
            r4 = r15
            r5 = r13
            r1.a(r2, r3, r4, r5, r7)     // Catch:{ MalformedURLException -> 0x034a, all -> 0x0343 }
            r9.a(r15)     // Catch:{ MalformedURLException -> 0x034a, all -> 0x0343 }
            r18 = 0
        L_0x033f:
            r10 = r18
            goto L_0x03a7
        L_0x0343:
            r0 = move-exception
        L_0x0344:
            r7 = r0
        L_0x0345:
            r10 = r17
            r11 = 0
            goto L_0x03c9
        L_0x034a:
            r0 = move-exception
        L_0x034b:
            r7 = r0
        L_0x034c:
            r10 = 0
            goto L_0x0360
        L_0x034e:
            r0 = move-exception
            r17 = r5
            r24 = r10
            r7 = r0
            r10 = r17
            goto L_0x03c7
        L_0x0358:
            r0 = move-exception
            r17 = r5
            r24 = r10
            r7 = r0
            r10 = 0
            r15 = 0
        L_0x0360:
            r0 = -901(0xfffffffffffffc7b, float:NaN)
            r8.a(r0, r12)     // Catch:{ all -> 0x03b4 }
            com.applovin.impl.sdk.k r1 = r8.f4309a     // Catch:{ all -> 0x03b4 }
            com.applovin.impl.sdk.c$e<java.lang.Boolean> r2 = com.applovin.impl.sdk.c.e.R3     // Catch:{ all -> 0x03b4 }
            java.lang.Object r1 = r1.a(r2)     // Catch:{ all -> 0x03b4 }
            java.lang.Boolean r1 = (java.lang.Boolean) r1     // Catch:{ all -> 0x03b4 }
            boolean r1 = r1.booleanValue()     // Catch:{ all -> 0x03b4 }
            if (r1 == 0) goto L_0x0382
            boolean r1 = r26.h()     // Catch:{ all -> 0x037c }
            if (r1 != 0) goto L_0x0388
            goto L_0x0382
        L_0x037c:
            r0 = move-exception
        L_0x037d:
            r7 = r0
            r11 = r10
        L_0x037f:
            r10 = r17
            goto L_0x03c9
        L_0x0382:
            java.lang.Object r1 = r26.g()     // Catch:{ all -> 0x03b4 }
            if (r1 == 0) goto L_0x0396
        L_0x0388:
            r1 = r25
            r2 = r24
            r3 = r12
            r4 = r15
            r5 = r13
            r1.a(r2, r3, r4, r5, r7)     // Catch:{ all -> 0x037c }
            r9.a(r0)     // Catch:{ all -> 0x037c }
            goto L_0x03a7
        L_0x0396:
            r1 = r25
            r2 = r24
            r3 = r12
            r4 = r15
            r5 = r13
            r1.a(r2, r3, r4, r5)     // Catch:{ all -> 0x03b4 }
            java.lang.Object r1 = r26.g()     // Catch:{ all -> 0x03b4 }
            r9.a(r1, r0)     // Catch:{ all -> 0x03b4 }
        L_0x03a7:
            com.applovin.impl.sdk.k r0 = r8.f4309a
            com.applovin.impl.sdk.utils.p.a(r10, r0)
            com.applovin.impl.sdk.k r0 = r8.f4309a
            r1 = r17
            com.applovin.impl.sdk.utils.p.a(r1, r0)
            goto L_0x03fa
        L_0x03b4:
            r0 = move-exception
            r1 = r17
            r7 = r0
            r11 = r10
            r10 = r1
            goto L_0x03c9
        L_0x03bb:
            r0 = move-exception
            r1 = r5
            r24 = r10
            r7 = r0
            r10 = r1
            goto L_0x03c7
        L_0x03c2:
            r0 = move-exception
            r24 = r10
            r7 = r0
            r10 = 0
        L_0x03c7:
            r11 = 0
            r15 = 0
        L_0x03c9:
            com.applovin.impl.sdk.k r0 = r8.f4309a     // Catch:{ all -> 0x03fb }
            com.applovin.impl.sdk.c$e<java.lang.Boolean> r1 = com.applovin.impl.sdk.c.e.q     // Catch:{ all -> 0x03fb }
            java.lang.Object r0 = r0.a(r1)     // Catch:{ all -> 0x03fb }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ all -> 0x03fb }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x03fb }
            if (r0 == 0) goto L_0x03db
            if (r15 != 0) goto L_0x03e0
        L_0x03db:
            int r0 = r8.a(r7)     // Catch:{ all -> 0x03fb }
            r15 = r0
        L_0x03e0:
            r8.a(r15, r12)     // Catch:{ all -> 0x03fb }
            r1 = r25
            r2 = r24
            r3 = r12
            r4 = r15
            r5 = r13
            r1.a(r2, r3, r4, r5, r7)     // Catch:{ all -> 0x03fb }
            r9.a(r15)     // Catch:{ all -> 0x03fb }
            com.applovin.impl.sdk.k r0 = r8.f4309a
            com.applovin.impl.sdk.utils.p.a(r11, r0)
            com.applovin.impl.sdk.k r0 = r8.f4309a
            com.applovin.impl.sdk.utils.p.a(r10, r0)
        L_0x03fa:
            return
        L_0x03fb:
            r0 = move-exception
            com.applovin.impl.sdk.k r1 = r8.f4309a
            com.applovin.impl.sdk.utils.p.a(r11, r1)
            com.applovin.impl.sdk.k r1 = r8.f4309a
            com.applovin.impl.sdk.utils.p.a(r10, r1)
            throw r0
        L_0x0407:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "No callback specified"
            r0.<init>(r1)
            throw r0
        L_0x040f:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "No method specified"
            r0.<init>(r1)
            throw r0
        L_0x0417:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "No endpoint specified"
            r0.<init>(r1)
            throw r0
        L_0x041f:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "No request specified"
            r0.<init>(r1)
            goto L_0x0428
        L_0x0427:
            throw r0
        L_0x0428:
            goto L_0x0427
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.network.a.a(com.applovin.impl.sdk.network.b, com.applovin.impl.sdk.network.a$a, com.applovin.impl.sdk.network.a$c):void");
    }
}
