package com.applovin.impl.sdk;

import com.applovin.impl.sdk.c;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class p {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final k f4369a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final AtomicBoolean f4370b = new AtomicBoolean();

    /* renamed from: c  reason: collision with root package name */
    private final List<c> f4371c = Collections.synchronizedList(new ArrayList());
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public long f4372d;

    /* renamed from: e  reason: collision with root package name */
    private final Object f4373e = new Object();
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public final AtomicBoolean f4374f = new AtomicBoolean();
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public long f4375g;

    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ long f4376a;

        a(long j2) {
            this.f4376a = j2;
        }

        public void run() {
            if (p.this.a() && System.currentTimeMillis() - p.this.f4375g >= this.f4376a) {
                p.this.f4369a.Z().b("FullScreenAdTracker", "Resetting \"pending display\" state...");
                p.this.f4374f.set(false);
            }
        }
    }

    class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ long f4378a;

        b(long j2) {
            this.f4378a = j2;
        }

        public void run() {
            if (p.this.f4370b.get() && System.currentTimeMillis() - p.this.f4372d >= this.f4378a) {
                p.this.f4369a.Z().b("FullScreenAdTracker", "Resetting \"display\" state...");
                p.this.d();
            }
        }
    }

    interface c {
        void h();

        void i();
    }

    p(k kVar) {
        this.f4369a = kVar;
    }

    public void a(c cVar) {
        this.f4371c.add(cVar);
    }

    public void a(boolean z) {
        synchronized (this.f4373e) {
            this.f4374f.set(z);
            if (z) {
                this.f4375g = System.currentTimeMillis();
                q Z = this.f4369a.Z();
                Z.b("FullScreenAdTracker", "Setting fullscreen ad pending display: " + this.f4375g);
                long longValue = ((Long) this.f4369a.a(c.e.x1)).longValue();
                if (longValue >= 0) {
                    AppLovinSdkUtils.runOnUiThreadDelayed(new a(longValue), longValue);
                }
            } else {
                this.f4375g = 0;
                q Z2 = this.f4369a.Z();
                Z2.b("FullScreenAdTracker", "Setting fullscreen ad not pending display: " + System.currentTimeMillis());
            }
        }
    }

    public boolean a() {
        return this.f4374f.get();
    }

    public void b(c cVar) {
        this.f4371c.remove(cVar);
    }

    public boolean b() {
        return this.f4370b.get();
    }

    public void c() {
        if (this.f4370b.compareAndSet(false, true)) {
            this.f4372d = System.currentTimeMillis();
            q Z = this.f4369a.Z();
            Z.b("FullScreenAdTracker", "Setting fullscreen ad displayed: " + this.f4372d);
            Iterator it = new ArrayList(this.f4371c).iterator();
            while (it.hasNext()) {
                ((c) it.next()).h();
            }
            long longValue = ((Long) this.f4369a.a(c.e.y1)).longValue();
            if (longValue >= 0) {
                AppLovinSdkUtils.runOnUiThreadDelayed(new b(longValue), longValue);
            }
        }
    }

    public void d() {
        if (this.f4370b.compareAndSet(true, false)) {
            q Z = this.f4369a.Z();
            Z.b("FullScreenAdTracker", "Setting fullscreen ad hidden: " + System.currentTimeMillis());
            Iterator it = new ArrayList(this.f4371c).iterator();
            while (it.hasNext()) {
                ((c) it.next()).i();
            }
        }
    }
}
