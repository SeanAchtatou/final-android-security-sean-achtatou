package com.applovin.impl.sdk;

import android.content.Context;
import com.applovin.impl.sdk.c;

public class h {
    public static Boolean a(Context context) {
        return a(c.g.f4023k, context);
    }

    private static Boolean a(c.g<Boolean> gVar, Context context) {
        return (Boolean) c.h.b(gVar, (Object) null, context);
    }

    private static boolean a(c.g<Boolean> gVar, Boolean bool, Context context) {
        Boolean a2 = a(gVar, context);
        c.h.a(gVar, bool, context);
        return a2 == null || a2 != bool;
    }

    public static boolean a(boolean z, Context context) {
        return a(c.g.f4023k, Boolean.valueOf(z), context);
    }

    public static Boolean b(Context context) {
        return a(c.g.l, context);
    }

    public static boolean b(boolean z, Context context) {
        return a(c.g.l, Boolean.valueOf(z), context);
    }
}
