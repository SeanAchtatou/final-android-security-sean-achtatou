package com.applovin.impl.sdk.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class r {

    /* renamed from: e  reason: collision with root package name */
    public static final r f4489e = new r();

    /* renamed from: a  reason: collision with root package name */
    private final String f4490a;

    /* renamed from: b  reason: collision with root package name */
    private final Map<String, String> f4491b;

    /* renamed from: c  reason: collision with root package name */
    protected String f4492c;

    /* renamed from: d  reason: collision with root package name */
    protected final List<r> f4493d;

    private r() {
        this.f4490a = "";
        this.f4491b = Collections.emptyMap();
        this.f4492c = "";
        this.f4493d = Collections.emptyList();
    }

    public r(String str, Map<String, String> map, r rVar) {
        this.f4490a = str;
        this.f4491b = Collections.unmodifiableMap(map);
        this.f4493d = new ArrayList();
    }

    public String a() {
        return this.f4490a;
    }

    public List<r> a(String str) {
        if (str != null) {
            ArrayList arrayList = new ArrayList(this.f4493d.size());
            for (r next : this.f4493d) {
                if (str.equalsIgnoreCase(next.a())) {
                    arrayList.add(next);
                }
            }
            return arrayList;
        }
        throw new IllegalArgumentException("No name specified.");
    }

    public r b(String str) {
        if (str != null) {
            for (r next : this.f4493d) {
                if (str.equalsIgnoreCase(next.a())) {
                    return next;
                }
            }
            return null;
        }
        throw new IllegalArgumentException("No name specified.");
    }

    public Map<String, String> b() {
        return this.f4491b;
    }

    public r c(String str) {
        if (str == null) {
            throw new IllegalArgumentException("No name specified.");
        } else if (this.f4493d.size() <= 0) {
            return null;
        } else {
            ArrayList arrayList = new ArrayList();
            arrayList.add(this);
            while (!arrayList.isEmpty()) {
                r rVar = (r) arrayList.get(0);
                arrayList.remove(0);
                if (str.equalsIgnoreCase(rVar.a())) {
                    return rVar;
                }
                arrayList.addAll(rVar.d());
            }
            return null;
        }
    }

    public String c() {
        return this.f4492c;
    }

    public List<r> d() {
        return Collections.unmodifiableList(this.f4493d);
    }

    public String toString() {
        return "XmlNode{elementName='" + this.f4490a + '\'' + ", text='" + this.f4492c + '\'' + ", attributes=" + this.f4491b + '}';
    }
}
