package com.applovin.impl.sdk;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import com.applovin.impl.sdk.utils.a;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class b implements Application.ActivityLifecycleCallbacks {

    /* renamed from: a  reason: collision with root package name */
    private final List<a> f3990a = Collections.synchronizedList(new ArrayList());

    /* renamed from: b  reason: collision with root package name */
    private WeakReference<Activity> f3991b = new WeakReference<>(null);

    public b(Context context) {
        if (context instanceof Activity) {
            this.f3991b = new WeakReference<>((Activity) context);
        }
        ((Application) context.getApplicationContext()).registerActivityLifecycleCallbacks(this);
    }

    public Activity a() {
        return this.f3991b.get();
    }

    public void a(a aVar) {
        this.f3990a.add(aVar);
    }

    public void b(a aVar) {
        this.f3990a.remove(aVar);
    }

    public void onActivityCreated(Activity activity, Bundle bundle) {
        for (a onActivityCreated : new ArrayList(this.f3990a)) {
            onActivityCreated.onActivityCreated(activity, bundle);
        }
    }

    public void onActivityDestroyed(Activity activity) {
        for (a onActivityDestroyed : new ArrayList(this.f3990a)) {
            onActivityDestroyed.onActivityDestroyed(activity);
        }
    }

    public void onActivityPaused(Activity activity) {
        for (a onActivityPaused : new ArrayList(this.f3990a)) {
            onActivityPaused.onActivityPaused(activity);
        }
    }

    public void onActivityResumed(Activity activity) {
        this.f3991b = new WeakReference<>(activity);
        for (a onActivityResumed : new ArrayList(this.f3990a)) {
            onActivityResumed.onActivityResumed(activity);
        }
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        for (a onActivitySaveInstanceState : new ArrayList(this.f3990a)) {
            onActivitySaveInstanceState.onActivitySaveInstanceState(activity, bundle);
        }
    }

    public void onActivityStarted(Activity activity) {
        for (a onActivityStarted : new ArrayList(this.f3990a)) {
            onActivityStarted.onActivityStarted(activity);
        }
    }

    public void onActivityStopped(Activity activity) {
        for (a onActivityStopped : new ArrayList(this.f3990a)) {
            onActivityStopped.onActivityStopped(activity);
        }
    }
}
