package com.applovin.impl.sdk;

import com.applovin.impl.sdk.ad.j;
import java.util.LinkedList;
import java.util.Queue;

class t {

    /* renamed from: a  reason: collision with root package name */
    private int f4392a;

    /* renamed from: b  reason: collision with root package name */
    private final Queue<j> f4393b = new LinkedList();

    /* renamed from: c  reason: collision with root package name */
    private final Object f4394c = new Object();

    t(int i2) {
        a(i2);
    }

    /* access modifiers changed from: package-private */
    public int a() {
        int size;
        synchronized (this.f4394c) {
            size = this.f4393b.size();
        }
        return size;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        if (i2 > 25) {
            i2 = 25;
        }
        this.f4392a = i2;
    }

    /* access modifiers changed from: package-private */
    public void a(j jVar) {
        synchronized (this.f4394c) {
            if (a() <= 25) {
                this.f4393b.offer(jVar);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.f4392a;
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        boolean z;
        synchronized (this.f4394c) {
            z = a() >= this.f4392a;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        boolean z;
        synchronized (this.f4394c) {
            z = a() == 0;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public j e() {
        j poll;
        try {
            synchronized (this.f4394c) {
                poll = !d() ? this.f4393b.poll() : null;
            }
            return poll;
        } catch (Exception unused) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public j f() {
        j peek;
        synchronized (this.f4394c) {
            peek = this.f4393b.peek();
        }
        return peek;
    }
}
