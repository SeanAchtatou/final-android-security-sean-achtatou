package com.applovin.impl.sdk.utils;

import android.content.Context;
import android.content.res.XmlResourceParser;
import com.applovin.impl.sdk.q;

public class c {

    /* renamed from: b  reason: collision with root package name */
    private static c f4398b;

    /* renamed from: c  reason: collision with root package name */
    private static final Object f4399c = new Object();

    /* renamed from: a  reason: collision with root package name */
    private final int f4400a;

    private c(Context context) {
        int i2;
        try {
            XmlResourceParser openXmlResourceParser = context.getAssets().openXmlResourceParser("AndroidManifest.xml");
            int eventType = openXmlResourceParser.getEventType();
            i2 = 0;
            do {
                if (2 == eventType) {
                    try {
                        if (openXmlResourceParser.getName().equals("application")) {
                            int i3 = 0;
                            while (true) {
                                if (i3 >= openXmlResourceParser.getAttributeCount()) {
                                    break;
                                }
                                String attributeName = openXmlResourceParser.getAttributeName(i3);
                                String attributeValue = openXmlResourceParser.getAttributeValue(i3);
                                if (attributeName.equals("networkSecurityConfig")) {
                                    i2 = Integer.valueOf(attributeValue).intValue();
                                    break;
                                }
                                i3++;
                            }
                        }
                    } catch (Throwable th) {
                        th = th;
                        try {
                            q.c("AndroidManifest", "Failed to parse AndroidManifest.xml.", th);
                            this.f4400a = i2;
                        } catch (Throwable th2) {
                            this.f4400a = i2;
                            throw th2;
                        }
                    }
                }
                eventType = openXmlResourceParser.next();
            } while (eventType != 1);
        } catch (Throwable th3) {
            th = th3;
            i2 = 0;
            q.c("AndroidManifest", "Failed to parse AndroidManifest.xml.", th);
            this.f4400a = i2;
        }
        this.f4400a = i2;
    }

    public static c a(Context context) {
        c cVar;
        synchronized (f4399c) {
            if (f4398b == null) {
                f4398b = new c(context);
            }
            cVar = f4398b;
        }
        return cVar;
    }

    public boolean a() {
        return this.f4400a != 0;
    }
}
