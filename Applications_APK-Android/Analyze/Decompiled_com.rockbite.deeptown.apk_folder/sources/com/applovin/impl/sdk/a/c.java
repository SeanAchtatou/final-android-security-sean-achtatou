package com.applovin.impl.sdk.a;

import java.util.Map;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private final String f3922a;

    /* renamed from: b  reason: collision with root package name */
    private Map<String, String> f3923b;

    private c(String str, Map<String, String> map) {
        this.f3922a = str;
        this.f3923b = map;
    }

    public static c a(String str) {
        return a(str, null);
    }

    public static c a(String str, Map<String, String> map) {
        return new c(str, map);
    }

    public Map<String, String> a() {
        return this.f3923b;
    }

    public String b() {
        return this.f3922a;
    }
}
