package com.applovin.impl.sdk.ad;

import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.q;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import org.json.JSONObject;

public final class g extends AppLovinAdBase {

    /* renamed from: e  reason: collision with root package name */
    private AppLovinAd f3988e;

    /* renamed from: f  reason: collision with root package name */
    private final d f3989f;

    public g(d dVar, k kVar) {
        super(new JSONObject(), new JSONObject(), b.UNKNOWN, kVar);
        this.f3989f = dVar;
    }

    private AppLovinAd c() {
        return (AppLovinAd) this.sdk.s().c(this.f3989f);
    }

    private String d() {
        d adZone = getAdZone();
        if (adZone == null || adZone.j()) {
            return null;
        }
        return adZone.a();
    }

    public AppLovinAd a() {
        return this.f3988e;
    }

    public void a(AppLovinAd appLovinAd) {
        this.f3988e = appLovinAd;
    }

    public AppLovinAd b() {
        AppLovinAd appLovinAd = this.f3988e;
        return appLovinAd != null ? appLovinAd : c();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || g.class != obj.getClass()) {
            return false;
        }
        AppLovinAd b2 = b();
        return b2 != null ? b2.equals(obj) : super.equals(obj);
    }

    public long getAdIdNumber() {
        try {
            AppLovinAd b2 = b();
            if (b2 != null) {
                return b2.getAdIdNumber();
            }
            return 0;
        } catch (Throwable th) {
            q.c("AppLovinAd", "Failed to retrieve ad id number", th);
            return 0;
        }
    }

    public d getAdZone() {
        AppLovinAdBase appLovinAdBase = (AppLovinAdBase) b();
        return appLovinAdBase != null ? appLovinAdBase.getAdZone() : this.f3989f;
    }

    public AppLovinAdSize getSize() {
        AppLovinAdSize appLovinAdSize = AppLovinAdSize.INTERSTITIAL;
        try {
            return getAdZone().b();
        } catch (Throwable th) {
            q.c("AppLovinAd", "Failed to retrieve ad size", th);
            return appLovinAdSize;
        }
    }

    public b getSource() {
        AppLovinAdBase appLovinAdBase = (AppLovinAdBase) b();
        return appLovinAdBase != null ? appLovinAdBase.getSource() : b.UNKNOWN;
    }

    public AppLovinAdType getType() {
        AppLovinAdType appLovinAdType = AppLovinAdType.REGULAR;
        try {
            return getAdZone().c();
        } catch (Throwable th) {
            q.c("AppLovinAd", "Failed to retrieve ad type", th);
            return appLovinAdType;
        }
    }

    public String getZoneId() {
        try {
            if (!this.f3989f.j()) {
                return this.f3989f.a();
            }
            return null;
        } catch (Throwable th) {
            q.c("AppLovinAd", "Failed to return zone id", th);
            return null;
        }
    }

    public int hashCode() {
        AppLovinAd b2 = b();
        return b2 != null ? b2.hashCode() : super.hashCode();
    }

    public boolean isVideoAd() {
        try {
            AppLovinAd b2 = b();
            if (b2 != null) {
                return b2.isVideoAd();
            }
            return false;
        } catch (Throwable th) {
            q.c("AppLovinAd", "Failed to return whether ad is video ad", th);
            return false;
        }
    }

    public String toString() {
        return "AppLovinAd{ #" + getAdIdNumber() + ", adType=" + getType() + ", adSize=" + getSize() + ", zoneId='" + d() + '\'' + '}';
    }
}
