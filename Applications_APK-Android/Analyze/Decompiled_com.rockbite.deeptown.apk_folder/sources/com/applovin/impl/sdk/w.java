package com.applovin.impl.sdk;

import android.app.Activity;
import android.graphics.Point;
import android.graphics.Rect;
import android.view.animation.Animation;
import com.applovin.impl.mediation.b;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.p;
import com.applovin.mediation.ads.MaxAdView;
import com.applovin.sdk.AppLovinSdkUtils;

public class w {

    /* renamed from: a  reason: collision with root package name */
    private final k f4509a;

    /* renamed from: b  reason: collision with root package name */
    private final q f4510b;

    /* renamed from: c  reason: collision with root package name */
    private final MaxAdView f4511c;

    public w(MaxAdView maxAdView, k kVar) {
        this.f4509a = kVar;
        this.f4510b = kVar.Z();
        this.f4511c = maxAdView;
    }

    public long a(b.c cVar) {
        long j2;
        this.f4510b.b("ViewabilityTracker", "Checking visibility...");
        if (!this.f4511c.isShown()) {
            this.f4510b.e("ViewabilityTracker", "View is hidden");
            j2 = 2;
        } else {
            j2 = 0;
        }
        if (this.f4511c.getAlpha() < cVar.D()) {
            this.f4510b.e("ViewabilityTracker", "View is transparent");
            j2 |= 4;
        }
        Animation animation = this.f4511c.getAnimation();
        if (animation != null && animation.hasStarted() && !animation.hasEnded()) {
            this.f4510b.e("ViewabilityTracker", "View is animating");
            j2 |= 8;
        }
        if (this.f4511c.getParent() == null) {
            this.f4510b.e("ViewabilityTracker", "No parent view found");
            j2 |= 16;
        }
        int pxToDp = AppLovinSdkUtils.pxToDp(this.f4511c.getContext(), this.f4511c.getWidth());
        if (pxToDp < cVar.B()) {
            q qVar = this.f4510b;
            qVar.e("ViewabilityTracker", "View has width (" + pxToDp + ") below threshold");
            j2 |= 32;
        }
        int pxToDp2 = AppLovinSdkUtils.pxToDp(this.f4511c.getContext(), this.f4511c.getHeight());
        if (pxToDp2 < cVar.C()) {
            q qVar2 = this.f4510b;
            qVar2.e("ViewabilityTracker", "View has height (" + pxToDp2 + ") below threshold");
            j2 |= 64;
        }
        Point a2 = g.a(this.f4511c.getContext());
        Rect rect = new Rect(0, 0, a2.x, a2.y);
        int[] iArr = {-1, -1};
        this.f4511c.getLocationOnScreen(iArr);
        Rect rect2 = new Rect(iArr[0], iArr[1], iArr[0] + this.f4511c.getWidth(), iArr[1] + this.f4511c.getHeight());
        if (!Rect.intersects(rect, rect2)) {
            q qVar3 = this.f4510b;
            qVar3.e("ViewabilityTracker", "Rect (" + rect2 + ") outside of screen's bounds (" + rect + ")");
            j2 |= 128;
        }
        Activity a3 = this.f4509a.A().a();
        if (a3 != null && !p.a(this.f4511c, a3)) {
            this.f4510b.e("ViewabilityTracker", "View is not in top activity's view hierarchy");
            j2 |= 256;
        }
        q qVar4 = this.f4510b;
        qVar4.b("ViewabilityTracker", "Returning flags: " + Long.toBinaryString(j2));
        return j2;
    }
}
