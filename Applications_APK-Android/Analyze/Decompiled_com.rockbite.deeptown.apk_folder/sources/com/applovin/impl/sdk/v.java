package com.applovin.impl.sdk;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.d.g;
import com.applovin.impl.sdk.utils.p;
import com.tapjoy.TJAdUnitConstants;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class v {

    /* renamed from: a  reason: collision with root package name */
    final k f4500a;

    /* renamed from: b  reason: collision with root package name */
    private final AtomicBoolean f4501b = new AtomicBoolean();

    /* renamed from: c  reason: collision with root package name */
    private final AtomicBoolean f4502c = new AtomicBoolean();

    /* renamed from: d  reason: collision with root package name */
    private final AtomicBoolean f4503d = new AtomicBoolean();

    /* renamed from: e  reason: collision with root package name */
    private Date f4504e;

    /* renamed from: f  reason: collision with root package name */
    private Date f4505f;

    class a extends com.applovin.impl.sdk.utils.a {
        a() {
        }

        public void onActivityResumed(Activity activity) {
            super.onActivityResumed(activity);
            v.this.e();
        }
    }

    class b implements ComponentCallbacks2 {
        b() {
        }

        public void onConfigurationChanged(Configuration configuration) {
        }

        public void onLowMemory() {
        }

        public void onTrimMemory(int i2) {
            if (i2 == 20) {
                v.this.f();
            }
        }
    }

    class c extends BroadcastReceiver {
        c() {
        }

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if ("android.intent.action.USER_PRESENT".equals(action)) {
                if (p.c()) {
                    v.this.e();
                }
            } else if ("android.intent.action.SCREEN_OFF".equals(action)) {
                v.this.f();
            }
        }
    }

    v(k kVar) {
        this.f4500a = kVar;
        Application application = (Application) kVar.d();
        application.registerActivityLifecycleCallbacks(new a());
        application.registerComponentCallbacks(new b());
        IntentFilter intentFilter = new IntentFilter("android.intent.action.SCREEN_OFF");
        intentFilter.addAction("android.intent.action.USER_PRESENT");
        application.registerReceiver(new c(), intentFilter);
    }

    /* access modifiers changed from: private */
    public void e() {
        if (this.f4503d.compareAndSet(true, false)) {
            h();
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        if (this.f4503d.compareAndSet(false, true)) {
            g();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.EventServiceImpl.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.applovin.impl.sdk.EventServiceImpl.a(com.applovin.impl.sdk.m, com.applovin.impl.sdk.l$c):java.util.HashMap<java.lang.String, java.lang.String>
      com.applovin.impl.sdk.EventServiceImpl.a(com.applovin.impl.sdk.l$c, java.util.Map<java.lang.String, java.lang.String>):void
      com.applovin.impl.sdk.EventServiceImpl.a(java.lang.String, boolean):void */
    private void g() {
        this.f4500a.Z().b("SessionTracker", "Application Paused");
        this.f4500a.D().sendBroadcastSync(new Intent("com.applovin.application_paused"));
        if (!this.f4502c.get() && ((Boolean) this.f4500a.a(c.e.L2)).booleanValue()) {
            boolean booleanValue = ((Boolean) this.f4500a.a(c.e.I2)).booleanValue();
            long millis = TimeUnit.MINUTES.toMillis(((Long) this.f4500a.a(c.e.K2)).longValue());
            if (this.f4504e == null || System.currentTimeMillis() - this.f4504e.getTime() >= millis) {
                ((EventServiceImpl) this.f4500a.U()).a(TJAdUnitConstants.String.VIDEO_PAUSED, false);
                if (booleanValue) {
                    this.f4504e = new Date();
                }
            }
            if (!booleanValue) {
                this.f4504e = new Date();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.EventServiceImpl.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.applovin.impl.sdk.EventServiceImpl.a(com.applovin.impl.sdk.m, com.applovin.impl.sdk.l$c):java.util.HashMap<java.lang.String, java.lang.String>
      com.applovin.impl.sdk.EventServiceImpl.a(com.applovin.impl.sdk.l$c, java.util.Map<java.lang.String, java.lang.String>):void
      com.applovin.impl.sdk.EventServiceImpl.a(java.lang.String, boolean):void */
    private void h() {
        this.f4500a.Z().b("SessionTracker", "Application Resumed");
        boolean booleanValue = ((Boolean) this.f4500a.a(c.e.I2)).booleanValue();
        long longValue = ((Long) this.f4500a.a(c.e.J2)).longValue();
        this.f4500a.D().sendBroadcastSync(new Intent("com.applovin.application_resumed"));
        if (!this.f4502c.getAndSet(false)) {
            long millis = TimeUnit.MINUTES.toMillis(longValue);
            if (this.f4505f == null || System.currentTimeMillis() - this.f4505f.getTime() >= millis) {
                ((EventServiceImpl) this.f4500a.U()).a("resumed", false);
                if (booleanValue) {
                    this.f4505f = new Date();
                }
            }
            if (!booleanValue) {
                this.f4505f = new Date();
            }
            this.f4500a.k().a(g.n);
            this.f4501b.set(true);
        }
    }

    public boolean a() {
        return this.f4503d.get();
    }

    public void b() {
        this.f4502c.set(true);
    }

    public void c() {
        this.f4502c.set(false);
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return this.f4501b.getAndSet(false);
    }
}
