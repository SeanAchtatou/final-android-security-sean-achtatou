package com.applovin.impl.sdk.d;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private final String f4028a;

    /* renamed from: b  reason: collision with root package name */
    private final String f4029b;

    public a(String str, String str2) {
        this.f4028a = str;
        this.f4029b = str2;
    }

    public String a() {
        return this.f4028a;
    }

    public String b() {
        return this.f4029b;
    }

    public String toString() {
        return "AdEventPostback{url='" + this.f4028a + '\'' + ", backupUrl='" + this.f4029b + '\'' + '}';
    }
}
