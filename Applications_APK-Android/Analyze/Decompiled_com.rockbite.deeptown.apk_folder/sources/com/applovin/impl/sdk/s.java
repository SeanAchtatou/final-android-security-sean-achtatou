package com.applovin.impl.sdk;

import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.ad.g;
import com.applovin.impl.sdk.ad.j;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.f;
import com.applovin.nativeAds.AppLovinNativeAdLoadListener;
import com.applovin.sdk.AppLovinErrorCodes;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

abstract class s implements n, AppLovinNativeAdLoadListener {

    /* renamed from: a  reason: collision with root package name */
    protected final k f4382a;

    /* renamed from: b  reason: collision with root package name */
    protected final q f4383b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public final Object f4384c = new Object();

    /* renamed from: d  reason: collision with root package name */
    private final Map<d, t> f4385d = new HashMap();

    /* renamed from: e  reason: collision with root package name */
    private final Map<d, t> f4386e = new HashMap();
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public final Map<d, Object> f4387f = new HashMap();

    /* renamed from: g  reason: collision with root package name */
    private final Set<d> f4388g = new HashSet();

    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ d f4389a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ int f4390b;

        a(d dVar, int i2) {
            this.f4389a = dVar;
            this.f4390b = i2;
        }

        public void run() {
            synchronized (s.this.f4384c) {
                Object obj = s.this.f4387f.get(this.f4389a);
                if (obj != null) {
                    s.this.f4387f.remove(this.f4389a);
                    q qVar = s.this.f4383b;
                    qVar.e("PreloadManager", "Load callback for zone " + this.f4389a + " timed out after " + this.f4390b + " seconds");
                    s.this.a(obj, this.f4389a, AppLovinErrorCodes.FETCH_AD_TIMEOUT);
                }
            }
        }
    }

    s(k kVar) {
        this.f4382a = kVar;
        this.f4383b = kVar.Z();
    }

    private void b(d dVar, Object obj) {
        synchronized (this.f4384c) {
            if (this.f4387f.containsKey(dVar)) {
                this.f4383b.d("PreloadManager", "Possibly missing prior registered preload callback.");
            }
            this.f4387f.put(dVar, obj);
        }
        int intValue = ((Integer) this.f4382a.a(c.e.l0)).intValue();
        if (intValue > 0) {
            AppLovinSdkUtils.runOnUiThreadDelayed(new a(dVar, intValue), TimeUnit.SECONDS.toMillis((long) intValue));
        }
    }

    private void c(j jVar) {
        i(a(jVar));
    }

    private t j(d dVar) {
        return this.f4385d.get(dVar);
    }

    private t k(d dVar) {
        return this.f4386e.get(dVar);
    }

    private boolean l(d dVar) {
        boolean z;
        synchronized (this.f4384c) {
            t j2 = j(dVar);
            z = j2 != null && j2.c();
        }
        return z;
    }

    private t m(d dVar) {
        synchronized (this.f4384c) {
            t k2 = k(dVar);
            if (k2 != null && k2.a() > 0) {
                return k2;
            }
            t j2 = j(dVar);
            return j2;
        }
    }

    private boolean n(d dVar) {
        boolean contains;
        synchronized (this.f4384c) {
            contains = this.f4388g.contains(dVar);
        }
        return contains;
    }

    /* access modifiers changed from: package-private */
    public abstract d a(j jVar);

    /* access modifiers changed from: package-private */
    public abstract f.c a(d dVar);

    /* access modifiers changed from: package-private */
    public abstract void a(Object obj, d dVar, int i2);

    /* access modifiers changed from: package-private */
    public abstract void a(Object obj, j jVar);

    public void a(LinkedHashSet<d> linkedHashSet) {
        Map<d, Object> map = this.f4387f;
        if (map != null && !map.isEmpty()) {
            synchronized (this.f4384c) {
                Iterator<d> it = this.f4387f.keySet().iterator();
                while (it.hasNext()) {
                    d next = it.next();
                    if (!next.j() && !linkedHashSet.contains(next)) {
                        Object obj = this.f4387f.get(next);
                        it.remove();
                        q.i("AppLovinAdService", "Failed to load ad for zone (" + next.a() + "). Please check that the zone has been added to your AppLovin account and given at least 30 minutes to fully propagate.");
                        a(obj, next, -7);
                    }
                }
            }
        }
    }

    public boolean a(d dVar, Object obj) {
        boolean z;
        synchronized (this.f4384c) {
            if (!n(dVar)) {
                b(dVar, obj);
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    public void b(d dVar, int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            i(dVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(j jVar) {
        Object obj;
        q qVar;
        String str;
        String str2;
        d a2 = a(jVar);
        boolean i2 = a2.i();
        synchronized (this.f4384c) {
            obj = this.f4387f.get(a2);
            this.f4387f.remove(a2);
            this.f4388g.add(a2);
            if (obj != null) {
                if (!i2) {
                    qVar = this.f4383b;
                    str = "PreloadManager";
                    str2 = "Additional callback found or dummy ads are enabled; skipping enqueue...";
                    qVar.b(str, str2);
                }
            }
            j(a2).a(jVar);
            qVar = this.f4383b;
            str = "PreloadManager";
            str2 = "Ad enqueued: " + jVar;
            qVar.b(str, str2);
        }
        if (obj != null) {
            this.f4383b.b("PreloadManager", "Called additional callback regarding " + jVar);
            if (i2) {
                try {
                    a(obj, new g(a2, this.f4382a));
                } catch (Throwable th) {
                    q.c("PreloadManager", "Encountered throwable while notifying user callback", th);
                }
            } else {
                a(obj, jVar);
                c(jVar);
            }
        }
        this.f4383b.b("PreloadManager", "Pulled ad from network and saved to preload cache: " + jVar);
    }

    public boolean b(d dVar) {
        return this.f4387f.containsKey(dVar);
    }

    public j c(d dVar) {
        j f2;
        synchronized (this.f4384c) {
            t m = m(dVar);
            f2 = m != null ? m.f() : null;
        }
        return f2;
    }

    /* access modifiers changed from: package-private */
    public void c(d dVar, int i2) {
        Object remove;
        q qVar = this.f4383b;
        qVar.b("PreloadManager", "Failed to pre-load an ad of zone " + dVar + ", error code " + i2);
        synchronized (this.f4384c) {
            remove = this.f4387f.remove(dVar);
            this.f4388g.add(dVar);
        }
        if (remove != null) {
            try {
                a(remove, dVar, i2);
            } catch (Throwable th) {
                q.c("PreloadManager", "Encountered exception while invoking user callback", th);
            }
        }
    }

    public j d(d dVar) {
        j e2;
        synchronized (this.f4384c) {
            t m = m(dVar);
            e2 = m != null ? m.e() : null;
        }
        return e2;
    }

    public j e(d dVar) {
        j jVar;
        StringBuilder sb;
        String str;
        g gVar;
        synchronized (this.f4384c) {
            t j2 = j(dVar);
            jVar = null;
            if (j2 != null) {
                if (dVar.i()) {
                    t k2 = k(dVar);
                    if (k2.c()) {
                        gVar = new g(dVar, this.f4382a);
                    } else if (j2.a() > 0) {
                        k2.a(j2.e());
                        gVar = new g(dVar, this.f4382a);
                    } else if (k2.a() > 0 && ((Boolean) this.f4382a.a(c.e.X0)).booleanValue()) {
                        gVar = new g(dVar, this.f4382a);
                    }
                    jVar = gVar;
                } else {
                    jVar = j2.e();
                }
            }
        }
        q qVar = this.f4383b;
        if (jVar != null) {
            str = "Retrieved ad of zone ";
        } else {
            sb = new StringBuilder();
            str = "Unable to retrieve ad of zone ";
        }
        sb.append(str);
        sb.append(dVar);
        sb.append("...");
        qVar.b("PreloadManager", sb.toString());
        return jVar;
    }

    public void f(d dVar) {
        if (dVar != null) {
            int i2 = 0;
            synchronized (this.f4384c) {
                t j2 = j(dVar);
                if (j2 != null) {
                    i2 = j2.b() - j2.a();
                }
            }
            b(dVar, i2);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0031, code lost:
        return r3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean g(com.applovin.impl.sdk.ad.d r5) {
        /*
            r4 = this;
            java.lang.Object r0 = r4.f4384c
            monitor-enter(r0)
            com.applovin.impl.sdk.t r1 = r4.k(r5)     // Catch:{ all -> 0x0032 }
            com.applovin.impl.sdk.k r2 = r4.f4382a     // Catch:{ all -> 0x0032 }
            com.applovin.impl.sdk.c$e<java.lang.Boolean> r3 = com.applovin.impl.sdk.c.e.Y0     // Catch:{ all -> 0x0032 }
            java.lang.Object r2 = r2.a(r3)     // Catch:{ all -> 0x0032 }
            java.lang.Boolean r2 = (java.lang.Boolean) r2     // Catch:{ all -> 0x0032 }
            boolean r2 = r2.booleanValue()     // Catch:{ all -> 0x0032 }
            r3 = 1
            if (r2 == 0) goto L_0x0022
            if (r1 == 0) goto L_0x0022
            int r1 = r1.a()     // Catch:{ all -> 0x0032 }
            if (r1 <= 0) goto L_0x0022
            monitor-exit(r0)     // Catch:{ all -> 0x0032 }
            return r3
        L_0x0022:
            com.applovin.impl.sdk.t r5 = r4.j(r5)     // Catch:{ all -> 0x0032 }
            if (r5 == 0) goto L_0x002f
            boolean r5 = r5.d()     // Catch:{ all -> 0x0032 }
            if (r5 != 0) goto L_0x002f
            goto L_0x0030
        L_0x002f:
            r3 = 0
        L_0x0030:
            monitor-exit(r0)     // Catch:{ all -> 0x0032 }
            return r3
        L_0x0032:
            r5 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0032 }
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.s.g(com.applovin.impl.sdk.ad.d):boolean");
    }

    public void h(d dVar) {
        synchronized (this.f4384c) {
            t j2 = j(dVar);
            if (j2 != null) {
                j2.a(dVar.e());
            } else {
                this.f4385d.put(dVar, new t(dVar.e()));
            }
            t k2 = k(dVar);
            if (k2 != null) {
                k2.a(dVar.f());
            } else {
                this.f4386e.put(dVar, new t(dVar.f()));
            }
        }
    }

    public void i(d dVar) {
        if (((Boolean) this.f4382a.a(c.e.m0)).booleanValue() && !l(dVar)) {
            q qVar = this.f4383b;
            qVar.b("PreloadManager", "Preloading ad for zone " + dVar + "...");
            this.f4382a.j().a(a(dVar), f.y.b.MAIN, 500);
        }
    }
}
