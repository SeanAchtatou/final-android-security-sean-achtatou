package com.applovin.impl.sdk.utils;

import com.applovin.adview.AppLovinAdView;
import com.applovin.adview.AppLovinAdViewEventListener;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxAdViewAdListener;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.MaxRewardedAdListener;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdRewardListener;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.applovin.sdk.AppLovinPostbackListener;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.Map;

public class j {

    static class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ MaxAdListener f4409a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ MaxAd f4410b;

        a(MaxAdListener maxAdListener, MaxAd maxAd) {
            this.f4409a = maxAdListener;
            this.f4410b = maxAd;
        }

        public void run() {
            try {
                this.f4409a.onAdClicked(this.f4410b);
            } catch (Throwable th) {
                com.applovin.impl.sdk.q.c("ListenerCallbackInvoker", "Unable to notify ad event listener about ad being clicked", th);
            }
        }
    }

    static class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AppLovinAdDisplayListener f4411a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ AppLovinAd f4412b;

        b(AppLovinAdDisplayListener appLovinAdDisplayListener, AppLovinAd appLovinAd) {
            this.f4411a = appLovinAdDisplayListener;
            this.f4412b = appLovinAd;
        }

        public void run() {
            try {
                this.f4411a.adDisplayed(j.b(this.f4412b));
            } catch (Throwable th) {
                com.applovin.impl.sdk.q.c("ListenerCallbackInvoker", "Unable to notify ad event listener about ad being displayed", th);
            }
        }
    }

    static class c implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ MaxAdListener f4413a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ MaxAd f4414b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ int f4415c;

        c(MaxAdListener maxAdListener, MaxAd maxAd, int i2) {
            this.f4413a = maxAdListener;
            this.f4414b = maxAd;
            this.f4415c = i2;
        }

        public void run() {
            try {
                this.f4413a.onAdDisplayFailed(this.f4414b, this.f4415c);
            } catch (Throwable th) {
                com.applovin.impl.sdk.q.c("ListenerCallbackInvoker", "Unable to notify ad event listener about ad failing to display", th);
            }
        }
    }

    static class d implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ MaxAdListener f4416a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ MaxAd f4417b;

        d(MaxAdListener maxAdListener, MaxAd maxAd) {
            this.f4416a = maxAdListener;
            this.f4417b = maxAd;
        }

        public void run() {
            try {
                ((MaxRewardedAdListener) this.f4416a).onRewardedVideoStarted(this.f4417b);
            } catch (Throwable th) {
                com.applovin.impl.sdk.q.c("ListenerCallbackInvoker", "Unable to notify ad event listener about rewarded video starting", th);
            }
        }
    }

    static class e implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ MaxAdListener f4418a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ MaxAd f4419b;

        e(MaxAdListener maxAdListener, MaxAd maxAd) {
            this.f4418a = maxAdListener;
            this.f4419b = maxAd;
        }

        public void run() {
            try {
                ((MaxRewardedAdListener) this.f4418a).onRewardedVideoCompleted(this.f4419b);
            } catch (Throwable th) {
                com.applovin.impl.sdk.q.c("ListenerCallbackInvoker", "Unable to notify ad event listener about rewarded video completing", th);
            }
        }
    }

    static class f implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ MaxAdListener f4420a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ MaxAd f4421b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ MaxReward f4422c;

        f(MaxAdListener maxAdListener, MaxAd maxAd, MaxReward maxReward) {
            this.f4420a = maxAdListener;
            this.f4421b = maxAd;
            this.f4422c = maxReward;
        }

        public void run() {
            try {
                ((MaxRewardedAdListener) this.f4420a).onUserRewarded(this.f4421b, this.f4422c);
            } catch (Throwable th) {
                com.applovin.impl.sdk.q.c("ListenerCallbackInvoker", "Unable to notify ad event listener about user being rewarded", th);
            }
        }
    }

    static class g implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ MaxAdListener f4423a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ MaxAd f4424b;

        g(MaxAdListener maxAdListener, MaxAd maxAd) {
            this.f4423a = maxAdListener;
            this.f4424b = maxAd;
        }

        public void run() {
            try {
                ((MaxAdViewAdListener) this.f4423a).onAdExpanded(this.f4424b);
            } catch (Throwable th) {
                com.applovin.impl.sdk.q.c("ListenerCallbackInvoker", "Unable to notify ad event listener about ad being expanded", th);
            }
        }
    }

    static class h implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ MaxAdListener f4425a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ MaxAd f4426b;

        h(MaxAdListener maxAdListener, MaxAd maxAd) {
            this.f4425a = maxAdListener;
            this.f4426b = maxAd;
        }

        public void run() {
            try {
                ((MaxAdViewAdListener) this.f4425a).onAdCollapsed(this.f4426b);
            } catch (Throwable th) {
                com.applovin.impl.sdk.q.c("ListenerCallbackInvoker", "Unable to notify ad event listener about ad being collapsed", th);
            }
        }
    }

    static class i implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AppLovinPostbackListener f4427a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ String f4428b;

        i(AppLovinPostbackListener appLovinPostbackListener, String str) {
            this.f4427a = appLovinPostbackListener;
            this.f4428b = str;
        }

        public void run() {
            try {
                this.f4427a.onPostbackSuccess(this.f4428b);
            } catch (Throwable th) {
                com.applovin.impl.sdk.q.c("ListenerCallbackInvoker", "Unable to notify AppLovinPostbackListener about postback URL (" + this.f4428b + ") executed", th);
            }
        }
    }

    /* renamed from: com.applovin.impl.sdk.utils.j$j  reason: collision with other inner class name */
    static class C0108j implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AppLovinPostbackListener f4429a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ String f4430b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ int f4431c;

        C0108j(AppLovinPostbackListener appLovinPostbackListener, String str, int i2) {
            this.f4429a = appLovinPostbackListener;
            this.f4430b = str;
            this.f4431c = i2;
        }

        public void run() {
            try {
                this.f4429a.onPostbackFailure(this.f4430b, this.f4431c);
            } catch (Throwable th) {
                com.applovin.impl.sdk.q.c("ListenerCallbackInvoker", "Unable to notify AppLovinPostbackListener about postback URL (" + this.f4430b + ") failing to execute with error code (" + this.f4431c + "):", th);
            }
        }
    }

    static class k implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AppLovinAdDisplayListener f4432a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ String f4433b;

        k(AppLovinAdDisplayListener appLovinAdDisplayListener, String str) {
            this.f4432a = appLovinAdDisplayListener;
            this.f4433b = str;
        }

        public void run() {
            ((com.applovin.impl.sdk.ad.i) this.f4432a).onAdDisplayFailed(this.f4433b);
        }
    }

    static class l implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AppLovinAdDisplayListener f4434a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ AppLovinAd f4435b;

        l(AppLovinAdDisplayListener appLovinAdDisplayListener, AppLovinAd appLovinAd) {
            this.f4434a = appLovinAdDisplayListener;
            this.f4435b = appLovinAd;
        }

        public void run() {
            try {
                this.f4434a.adHidden(j.b(this.f4435b));
            } catch (Throwable th) {
                com.applovin.impl.sdk.q.c("ListenerCallbackInvoker", "Unable to notify ad event listener about ad being hidden", th);
            }
        }
    }

    static class m implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AppLovinAdClickListener f4436a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ AppLovinAd f4437b;

        m(AppLovinAdClickListener appLovinAdClickListener, AppLovinAd appLovinAd) {
            this.f4436a = appLovinAdClickListener;
            this.f4437b = appLovinAd;
        }

        public void run() {
            try {
                this.f4436a.adClicked(j.b(this.f4437b));
            } catch (Throwable th) {
                com.applovin.impl.sdk.q.c("ListenerCallbackInvoker", "Unable to notify ad event listener about ad being clicked", th);
            }
        }
    }

    static class n implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AppLovinAdVideoPlaybackListener f4438a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ AppLovinAd f4439b;

        n(AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener, AppLovinAd appLovinAd) {
            this.f4438a = appLovinAdVideoPlaybackListener;
            this.f4439b = appLovinAd;
        }

        public void run() {
            try {
                this.f4438a.videoPlaybackBegan(j.b(this.f4439b));
            } catch (Throwable th) {
                com.applovin.impl.sdk.q.c("ListenerCallbackInvoker", "Unable to notify ad event listener about ad playback began", th);
            }
        }
    }

    static class o implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AppLovinAdVideoPlaybackListener f4440a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ AppLovinAd f4441b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ double f4442c;

        /* renamed from: d  reason: collision with root package name */
        final /* synthetic */ boolean f4443d;

        o(AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener, AppLovinAd appLovinAd, double d2, boolean z) {
            this.f4440a = appLovinAdVideoPlaybackListener;
            this.f4441b = appLovinAd;
            this.f4442c = d2;
            this.f4443d = z;
        }

        public void run() {
            try {
                this.f4440a.videoPlaybackEnded(j.b(this.f4441b), this.f4442c, this.f4443d);
            } catch (Throwable th) {
                com.applovin.impl.sdk.q.c("ListenerCallbackInvoker", "Unable to notify ad event listener about ad playback ended", th);
            }
        }
    }

    static class p implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AppLovinAdViewEventListener f4444a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ AppLovinAd f4445b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ AppLovinAdView f4446c;

        p(AppLovinAdViewEventListener appLovinAdViewEventListener, AppLovinAd appLovinAd, AppLovinAdView appLovinAdView) {
            this.f4444a = appLovinAdViewEventListener;
            this.f4445b = appLovinAd;
            this.f4446c = appLovinAdView;
        }

        public void run() {
            try {
                this.f4444a.adOpenedFullscreen(j.b(this.f4445b), this.f4446c);
            } catch (Throwable th) {
                com.applovin.impl.sdk.q.c("ListenerCallbackInvoker", "Unable to notify ad event listener about fullscreen opened event", th);
            }
        }
    }

    static class q implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AppLovinAdViewEventListener f4447a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ AppLovinAd f4448b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ AppLovinAdView f4449c;

        q(AppLovinAdViewEventListener appLovinAdViewEventListener, AppLovinAd appLovinAd, AppLovinAdView appLovinAdView) {
            this.f4447a = appLovinAdViewEventListener;
            this.f4448b = appLovinAd;
            this.f4449c = appLovinAdView;
        }

        public void run() {
            try {
                this.f4447a.adClosedFullscreen(j.b(this.f4448b), this.f4449c);
            } catch (Throwable th) {
                com.applovin.impl.sdk.q.c("ListenerCallbackInvoker", "Unable to notify ad event listener about fullscreen closed event", th);
            }
        }
    }

    static class r implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AppLovinAdViewEventListener f4450a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ AppLovinAd f4451b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ AppLovinAdView f4452c;

        r(AppLovinAdViewEventListener appLovinAdViewEventListener, AppLovinAd appLovinAd, AppLovinAdView appLovinAdView) {
            this.f4450a = appLovinAdViewEventListener;
            this.f4451b = appLovinAd;
            this.f4452c = appLovinAdView;
        }

        public void run() {
            try {
                this.f4450a.adLeftApplication(j.b(this.f4451b), this.f4452c);
            } catch (Throwable th) {
                com.applovin.impl.sdk.q.c("ListenerCallbackInvoker", "Unable to notify ad event listener about application leave event", th);
            }
        }
    }

    static class s implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AppLovinAdRewardListener f4453a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ AppLovinAd f4454b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ Map f4455c;

        s(AppLovinAdRewardListener appLovinAdRewardListener, AppLovinAd appLovinAd, Map map) {
            this.f4453a = appLovinAdRewardListener;
            this.f4454b = appLovinAd;
            this.f4455c = map;
        }

        public void run() {
            try {
                this.f4453a.userRewardVerified(j.b(this.f4454b), this.f4455c);
            } catch (Throwable th) {
                com.applovin.impl.sdk.q.c("ListenerCallbackInvoker", "Unable to notify ad reward listener about successful reward validation request", th);
            }
        }
    }

    static class t implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AppLovinAdRewardListener f4456a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ AppLovinAd f4457b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ Map f4458c;

        t(AppLovinAdRewardListener appLovinAdRewardListener, AppLovinAd appLovinAd, Map map) {
            this.f4456a = appLovinAdRewardListener;
            this.f4457b = appLovinAd;
            this.f4458c = map;
        }

        public void run() {
            try {
                this.f4456a.userOverQuota(j.b(this.f4457b), this.f4458c);
            } catch (Throwable th) {
                com.applovin.impl.sdk.q.c("ListenerCallbackInvoker", "Unable to notify ad reward listener about exceeding quota", th);
            }
        }
    }

    static class u implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AppLovinAdRewardListener f4459a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ AppLovinAd f4460b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ Map f4461c;

        u(AppLovinAdRewardListener appLovinAdRewardListener, AppLovinAd appLovinAd, Map map) {
            this.f4459a = appLovinAdRewardListener;
            this.f4460b = appLovinAd;
            this.f4461c = map;
        }

        public void run() {
            try {
                this.f4459a.userRewardRejected(j.b(this.f4460b), this.f4461c);
            } catch (Throwable th) {
                com.applovin.impl.sdk.q.c("ListenerCallbackInvoker", "Unable to notify ad reward listener about reward validation request being rejected", th);
            }
        }
    }

    static class v implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AppLovinAdRewardListener f4462a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ AppLovinAd f4463b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ int f4464c;

        v(AppLovinAdRewardListener appLovinAdRewardListener, AppLovinAd appLovinAd, int i2) {
            this.f4462a = appLovinAdRewardListener;
            this.f4463b = appLovinAd;
            this.f4464c = i2;
        }

        public void run() {
            try {
                this.f4462a.validationRequestFailed(j.b(this.f4463b), this.f4464c);
            } catch (Throwable th) {
                com.applovin.impl.sdk.q.c("ListenerCallbackInvoker", "Unable to notify ad reward listener about reward validation request failing", th);
            }
        }
    }

    static class w implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ MaxAdListener f4465a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ MaxAd f4466b;

        w(MaxAdListener maxAdListener, MaxAd maxAd) {
            this.f4465a = maxAdListener;
            this.f4466b = maxAd;
        }

        public void run() {
            try {
                this.f4465a.onAdLoaded(this.f4466b);
            } catch (Throwable th) {
                com.applovin.impl.sdk.q.c("ListenerCallbackInvoker", "Unable to notify ad event listener about ad being loaded", th);
            }
        }
    }

    static class x implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ MaxAdListener f4467a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ String f4468b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ int f4469c;

        x(MaxAdListener maxAdListener, String str, int i2) {
            this.f4467a = maxAdListener;
            this.f4468b = str;
            this.f4469c = i2;
        }

        public void run() {
            try {
                this.f4467a.onAdLoadFailed(this.f4468b, this.f4469c);
            } catch (Throwable th) {
                com.applovin.impl.sdk.q.c("ListenerCallbackInvoker", "Unable to notify ad event listener about ad failing to load", th);
            }
        }
    }

    static class y implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ MaxAdListener f4470a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ MaxAd f4471b;

        y(MaxAdListener maxAdListener, MaxAd maxAd) {
            this.f4470a = maxAdListener;
            this.f4471b = maxAd;
        }

        public void run() {
            try {
                this.f4470a.onAdDisplayed(this.f4471b);
            } catch (Throwable th) {
                com.applovin.impl.sdk.q.c("ListenerCallbackInvoker", "Unable to notify ad event listener about ad being displayed", th);
            }
        }
    }

    static class z implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ MaxAdListener f4472a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ MaxAd f4473b;

        z(MaxAdListener maxAdListener, MaxAd maxAd) {
            this.f4472a = maxAdListener;
            this.f4473b = maxAd;
        }

        public void run() {
            try {
                this.f4472a.onAdHidden(this.f4473b);
            } catch (Throwable th) {
                com.applovin.impl.sdk.q.c("ListenerCallbackInvoker", "Unable to notify ad event listener about ad being hidden", th);
            }
        }
    }

    public static void a(AppLovinAdViewEventListener appLovinAdViewEventListener, AppLovinAd appLovinAd, AppLovinAdView appLovinAdView) {
        if (appLovinAd != null && appLovinAdViewEventListener != null) {
            AppLovinSdkUtils.runOnUiThread(new p(appLovinAdViewEventListener, appLovinAd, appLovinAdView));
        }
    }

    public static void a(MaxAdListener maxAdListener, MaxAd maxAd) {
        if (maxAd != null && maxAdListener != null) {
            AppLovinSdkUtils.runOnUiThread(new w(maxAdListener, maxAd));
        }
    }

    public static void a(MaxAdListener maxAdListener, MaxAd maxAd, int i2) {
        if (maxAd != null && maxAdListener != null) {
            AppLovinSdkUtils.runOnUiThread(new c(maxAdListener, maxAd, i2));
        }
    }

    public static void a(MaxAdListener maxAdListener, MaxAd maxAd, MaxReward maxReward) {
        if (maxAd != null && (maxAdListener instanceof MaxRewardedAdListener)) {
            AppLovinSdkUtils.runOnUiThread(new f(maxAdListener, maxAd, maxReward));
        }
    }

    public static void a(MaxAdListener maxAdListener, String str, int i2) {
        if (str != null && maxAdListener != null) {
            AppLovinSdkUtils.runOnUiThread(new x(maxAdListener, str, i2));
        }
    }

    public static void a(AppLovinAdClickListener appLovinAdClickListener, AppLovinAd appLovinAd) {
        if (appLovinAd != null && appLovinAdClickListener != null) {
            AppLovinSdkUtils.runOnUiThread(new m(appLovinAdClickListener, appLovinAd));
        }
    }

    public static void a(AppLovinAdDisplayListener appLovinAdDisplayListener, AppLovinAd appLovinAd) {
        if (appLovinAd != null && appLovinAdDisplayListener != null) {
            AppLovinSdkUtils.runOnUiThread(new b(appLovinAdDisplayListener, appLovinAd));
        }
    }

    public static void a(AppLovinAdDisplayListener appLovinAdDisplayListener, String str) {
        if (appLovinAdDisplayListener instanceof com.applovin.impl.sdk.ad.i) {
            AppLovinSdkUtils.runOnUiThread(new k(appLovinAdDisplayListener, str));
        }
    }

    public static void a(AppLovinAdRewardListener appLovinAdRewardListener, AppLovinAd appLovinAd, int i2) {
        if (appLovinAd != null && appLovinAdRewardListener != null) {
            AppLovinSdkUtils.runOnUiThread(new v(appLovinAdRewardListener, appLovinAd, i2));
        }
    }

    public static void a(AppLovinAdRewardListener appLovinAdRewardListener, AppLovinAd appLovinAd, Map<String, String> map) {
        if (appLovinAd != null && appLovinAdRewardListener != null) {
            AppLovinSdkUtils.runOnUiThread(new s(appLovinAdRewardListener, appLovinAd, map));
        }
    }

    public static void a(AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener, AppLovinAd appLovinAd) {
        if (appLovinAd != null && appLovinAdVideoPlaybackListener != null) {
            AppLovinSdkUtils.runOnUiThread(new n(appLovinAdVideoPlaybackListener, appLovinAd));
        }
    }

    public static void a(AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener, AppLovinAd appLovinAd, double d2, boolean z2) {
        if (appLovinAd != null && appLovinAdVideoPlaybackListener != null) {
            AppLovinSdkUtils.runOnUiThread(new o(appLovinAdVideoPlaybackListener, appLovinAd, d2, z2));
        }
    }

    public static void a(AppLovinPostbackListener appLovinPostbackListener, String str) {
        if (appLovinPostbackListener != null) {
            AppLovinSdkUtils.runOnUiThread(new i(appLovinPostbackListener, str));
        }
    }

    public static void a(AppLovinPostbackListener appLovinPostbackListener, String str, int i2) {
        if (appLovinPostbackListener != null) {
            AppLovinSdkUtils.runOnUiThread(new C0108j(appLovinPostbackListener, str, i2));
        }
    }

    /* access modifiers changed from: private */
    public static AppLovinAd b(AppLovinAd appLovinAd) {
        AppLovinAdBase appLovinAdBase = (AppLovinAdBase) appLovinAd;
        return appLovinAdBase.getDummyAd() != null ? appLovinAdBase.getDummyAd() : appLovinAd;
    }

    public static void b(AppLovinAdViewEventListener appLovinAdViewEventListener, AppLovinAd appLovinAd, AppLovinAdView appLovinAdView) {
        if (appLovinAd != null && appLovinAdViewEventListener != null) {
            AppLovinSdkUtils.runOnUiThread(new q(appLovinAdViewEventListener, appLovinAd, appLovinAdView));
        }
    }

    public static void b(MaxAdListener maxAdListener, MaxAd maxAd) {
        if (maxAd != null && maxAdListener != null) {
            AppLovinSdkUtils.runOnUiThread(new y(maxAdListener, maxAd));
        }
    }

    public static void b(AppLovinAdDisplayListener appLovinAdDisplayListener, AppLovinAd appLovinAd) {
        if (appLovinAd != null && appLovinAdDisplayListener != null) {
            AppLovinSdkUtils.runOnUiThread(new l(appLovinAdDisplayListener, appLovinAd));
        }
    }

    public static void b(AppLovinAdRewardListener appLovinAdRewardListener, AppLovinAd appLovinAd, Map<String, String> map) {
        if (appLovinAd != null && appLovinAdRewardListener != null) {
            AppLovinSdkUtils.runOnUiThread(new t(appLovinAdRewardListener, appLovinAd, map));
        }
    }

    public static void c(AppLovinAdViewEventListener appLovinAdViewEventListener, AppLovinAd appLovinAd, AppLovinAdView appLovinAdView) {
        if (appLovinAd != null && appLovinAdViewEventListener != null) {
            AppLovinSdkUtils.runOnUiThread(new r(appLovinAdViewEventListener, appLovinAd, appLovinAdView));
        }
    }

    public static void c(MaxAdListener maxAdListener, MaxAd maxAd) {
        if (maxAd != null && maxAdListener != null) {
            AppLovinSdkUtils.runOnUiThread(new z(maxAdListener, maxAd));
        }
    }

    public static void c(AppLovinAdRewardListener appLovinAdRewardListener, AppLovinAd appLovinAd, Map<String, String> map) {
        if (appLovinAd != null && appLovinAdRewardListener != null) {
            AppLovinSdkUtils.runOnUiThread(new u(appLovinAdRewardListener, appLovinAd, map));
        }
    }

    public static void d(MaxAdListener maxAdListener, MaxAd maxAd) {
        if (maxAd != null && maxAdListener != null) {
            AppLovinSdkUtils.runOnUiThread(new a(maxAdListener, maxAd));
        }
    }

    public static void e(MaxAdListener maxAdListener, MaxAd maxAd) {
        if (maxAd != null && (maxAdListener instanceof MaxRewardedAdListener)) {
            AppLovinSdkUtils.runOnUiThread(new d(maxAdListener, maxAd));
        }
    }

    public static void f(MaxAdListener maxAdListener, MaxAd maxAd) {
        if (maxAd != null && (maxAdListener instanceof MaxRewardedAdListener)) {
            AppLovinSdkUtils.runOnUiThread(new e(maxAdListener, maxAd));
        }
    }

    public static void g(MaxAdListener maxAdListener, MaxAd maxAd) {
        if (maxAd != null && (maxAdListener instanceof MaxAdViewAdListener)) {
            AppLovinSdkUtils.runOnUiThread(new g(maxAdListener, maxAd));
        }
    }

    public static void h(MaxAdListener maxAdListener, MaxAd maxAd) {
        if (maxAd != null && (maxAdListener instanceof MaxAdViewAdListener)) {
            AppLovinSdkUtils.runOnUiThread(new h(maxAdListener, maxAd));
        }
    }
}
