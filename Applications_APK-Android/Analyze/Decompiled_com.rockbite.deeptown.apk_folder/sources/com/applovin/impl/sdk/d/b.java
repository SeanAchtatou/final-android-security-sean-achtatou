package com.applovin.impl.sdk.d;

import android.text.TextUtils;
import java.util.HashSet;
import java.util.Set;

public class b {
    static final b A = a("svmi", "INTERSTITIAL_VIDEO_MUTED_INITIALLY");
    static final b B = a("stvm", "TIME_TO_TOGGLE_VIDEO_MUTE");
    static final b C = a("schc", "AD_CANCELLED_HTML_CACHING");
    static final b D = a("smwm", "AD_SHOWN_IN_MULTIWINDOW_MODE");
    static final b E = a("vssc", "VIDEO_STREAM_STALLED_COUNT");
    public static final b F = a("wvem", "WEB_VIEW_ERROR_MESSAGES");
    public static final b G = a("wvhec", "WEB_VIEW_HTTP_ERROR_COUNT");
    public static final b H = a("wvsem", "WEB_VIEW_SSL_ERROR_MESSAGES");

    /* renamed from: c  reason: collision with root package name */
    private static final Set<String> f4030c = new HashSet(32);

    /* renamed from: d  reason: collision with root package name */
    static final b f4031d = a("sas", "AD_SOURCE");

    /* renamed from: e  reason: collision with root package name */
    static final b f4032e = a("srt", "AD_RENDER_TIME");

    /* renamed from: f  reason: collision with root package name */
    static final b f4033f = a("sft", "AD_FETCH_TIME");

    /* renamed from: g  reason: collision with root package name */
    static final b f4034g = a("sfs", "AD_FETCH_SIZE");

    /* renamed from: h  reason: collision with root package name */
    static final b f4035h = a("sadb", "AD_DOWNLOADED_BYTES");

    /* renamed from: i  reason: collision with root package name */
    static final b f4036i = a("sacb", "AD_CACHED_BYTES");

    /* renamed from: j  reason: collision with root package name */
    static final b f4037j = a("stdl", "TIME_TO_DISPLAY_FROM_LOAD");

    /* renamed from: k  reason: collision with root package name */
    static final b f4038k = a("stdi", "TIME_TO_DISPLAY_FROM_INIT");
    static final b l = a("snas", "AD_NUMBER_IN_SESSION");
    static final b m = a("snat", "AD_NUMBER_TOTAL");
    static final b n = a("stah", "TIME_AD_HIDDEN_FROM_SHOW");
    static final b o = a("stas", "TIME_TO_SKIP_FROM_SHOW");
    static final b p = a("stac", "TIME_TO_CLICK_FROM_SHOW");
    static final b q = a("stbe", "TIME_TO_EXPAND_FROM_SHOW");
    static final b r = a("stbc", "TIME_TO_CONTRACT_FROM_SHOW");
    static final b s = a("saan", "AD_SHOWN_WITH_ACTIVE_NETWORK");
    static final b t = a("suvs", "INTERSTITIAL_USED_VIDEO_STREAM");
    static final b u = a("sugs", "AD_USED_GRAPHIC_STREAM");
    static final b v = a("svpv", "INTERSTITIAL_VIDEO_PERCENT_VIEWED");
    static final b w = a("stpd", "INTERSTITIAL_PAUSED_DURATION");
    static final b x = a("sspe", "INTERSTITIAL_SHOW_POSTSTITIAL_CODE_EXECUTED");
    static final b y = a("shsc", "HTML_RESOURCE_CACHE_SUCCESS_COUNT");
    static final b z = a("shfc", "HTML_RESOURCE_CACHE_FAILURE_COUNT");

    /* renamed from: a  reason: collision with root package name */
    private final String f4039a;

    /* renamed from: b  reason: collision with root package name */
    private final String f4040b;

    static {
        a("sisw", "IS_STREAMING_WEBKIT");
        a("surw", "UNABLE_TO_RETRIEVE_WEBKIT_HTML_STRING");
        a("surp", "UNABLE_TO_PERSIST_WEBKIT_HTML_PLACEMENT_REPLACED_STRING");
        a("swhp", "SUCCESSFULLY_PERSISTED_WEBKIT_HTML_STRING");
        a("skr", "STOREKIT_REDIRECTED");
        a("sklf", "STOREKIT_LOAD_FAILURE");
        a("skps", "STOREKIT_PRELOAD_SKIPPED");
        a("wvruc", "WEB_VIEW_RENDERER_UNRESPONSIVE_COUNT");
    }

    private b(String str, String str2) {
        this.f4039a = str;
        this.f4040b = str2;
    }

    private static b a(String str, String str2) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("No key name specified");
        } else if (f4030c.contains(str)) {
            throw new IllegalArgumentException("Key has already been used: " + str);
        } else if (!TextUtils.isEmpty(str2)) {
            f4030c.add(str);
            return new b(str, str2);
        } else {
            throw new IllegalArgumentException("No debug name specified");
        }
    }

    public String a() {
        return this.f4039a;
    }

    public String b() {
        return this.f4040b;
    }
}
