package com.applovin.impl.sdk.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.applovin.impl.sdk.k;
import java.util.HashSet;
import java.util.Set;

public class d extends BroadcastReceiver {

    /* renamed from: b  reason: collision with root package name */
    private static final Set<d> f4401b = new HashSet();

    /* renamed from: a  reason: collision with root package name */
    private final n f4402a;

    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ k f4403a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Runnable f4404b;

        a(k kVar, Runnable runnable) {
            this.f4403a = kVar;
            this.f4404b = runnable;
        }

        public void run() {
            this.f4403a.D().unregisterReceiver(d.this);
            d.this.a();
            Runnable runnable = this.f4404b;
            if (runnable != null) {
                runnable.run();
            }
        }
    }

    private d(long j2, k kVar, Runnable runnable) {
        this.f4402a = n.a(j2, kVar, new a(kVar, runnable));
        f4401b.add(this);
        kVar.D().registerReceiver(this, new IntentFilter("com.applovin.application_paused"));
        kVar.D().registerReceiver(this, new IntentFilter("com.applovin.application_resumed"));
    }

    public static d a(long j2, k kVar, Runnable runnable) {
        return new d(j2, kVar, runnable);
    }

    public void a() {
        this.f4402a.d();
        f4401b.remove(this);
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if ("com.applovin.application_paused".equals(action)) {
            this.f4402a.b();
        } else if ("com.applovin.application_resumed".equals(action)) {
            this.f4402a.c();
        }
    }
}
