package com.applovin.impl.sdk.utils;

import android.net.Uri;
import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.Map;

public class m {

    /* renamed from: a  reason: collision with root package name */
    private static final char[] f4474a = "0123456789abcdef".toCharArray();

    public static int a(String str) {
        return a(str, 0);
    }

    public static int a(String str, int i2) {
        return c(str) ? Integer.parseInt(str) : i2;
    }

    public static String a(int i2, String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        if (i2 > str.length()) {
            i2 = str.length();
        }
        return str.substring(0, i2);
    }

    private static String a(String str, Integer num) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-1");
            instance.update(str.getBytes("UTF-8"));
            String a2 = a(instance.digest());
            return num.intValue() > 0 ? a2.substring(0, Math.min(num.intValue(), a2.length())) : a2;
        } catch (Throwable th) {
            throw new RuntimeException("SHA-1 for \"" + str + "\" failed.", th);
        }
    }

    public static String a(String str, String str2, String str3) {
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            return str;
        }
        Uri.Builder buildUpon = Uri.parse(str).buildUpon();
        buildUpon.appendQueryParameter(str2, str3);
        return buildUpon.build().toString();
    }

    public static String a(String str, Map<String, String> map) {
        if (!(str == null || map == null)) {
            for (Map.Entry next : map.entrySet()) {
                str = str.replace((CharSequence) next.getKey(), (CharSequence) next.getValue());
            }
        }
        return str;
    }

    public static String a(boolean z) {
        return z ? "1" : AppEventsConstants.EVENT_PARAM_VALUE_NO;
    }

    public static String a(byte[] bArr) {
        if (bArr != null) {
            char[] cArr = new char[(bArr.length * 2)];
            for (int i2 = 0; i2 < bArr.length; i2++) {
                int i3 = i2 * 2;
                char[] cArr2 = f4474a;
                cArr[i3] = cArr2[(bArr[i2] & 240) >>> 4];
                cArr[i3 + 1] = cArr2[bArr[i2] & 15];
            }
            return new String(cArr);
        }
        throw new IllegalArgumentException("No data specified");
    }

    public static boolean a(String str, String str2) {
        return b(str) && b(str2) && str.toLowerCase().contains(str2.toLowerCase());
    }

    public static String b(String str, Map<String, String> map) {
        if (TextUtils.isEmpty(str) || map == null || map.isEmpty()) {
            return str;
        }
        Uri.Builder buildUpon = Uri.parse(str).buildUpon();
        for (Map.Entry next : map.entrySet()) {
            buildUpon.appendQueryParameter((String) next.getKey(), (String) next.getValue());
        }
        return buildUpon.build().toString();
    }

    public static boolean b(String str) {
        return !TextUtils.isEmpty(str);
    }

    public static boolean c(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        char charAt = str.charAt(0);
        int i2 = (charAt == '-' || charAt == '+') ? 1 : 0;
        int length = str.length();
        if (i2 == 1 && length == 1) {
            return false;
        }
        while (i2 < length) {
            if (!Character.isDigit(str.charAt(i2))) {
                return false;
            }
            i2++;
        }
        return true;
    }

    public static String d(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e2) {
            throw new UnsupportedOperationException(e2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.utils.m.a(java.lang.String, java.lang.Integer):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      com.applovin.impl.sdk.utils.m.a(java.lang.String, int):int
      com.applovin.impl.sdk.utils.m.a(int, java.lang.String):java.lang.String
      com.applovin.impl.sdk.utils.m.a(java.lang.String, java.util.Map<java.lang.String, java.lang.String>):java.lang.String
      com.applovin.impl.sdk.utils.m.a(java.lang.String, java.lang.String):boolean
      com.applovin.impl.sdk.utils.m.a(java.lang.String, java.lang.Integer):java.lang.String */
    public static String e(String str) {
        return a(str, (Integer) 16);
    }
}
