package com.applovin.impl.sdk.d;

import android.text.TextUtils;
import java.util.HashSet;
import java.util.Set;

public class i {
    public static final i A = a("rvw");
    public static final i B = a("vr");
    public static final i C = a("aia");
    public static final i D = a("cs");
    public static final i E = a("fnma");
    public static final i F = a("lad");
    public static final i G = a("pmw");
    public static final i H = a("pnma");
    public static final i I = a("tma");
    public static final i J = a("tsc");
    public static final i K = a("fmp");
    public static final i L = a("fmdi");
    public static final i M = a("vmr");
    public static final i N = a("rmr");

    /* renamed from: b  reason: collision with root package name */
    private static final Set<String> f4086b = new HashSet(64);

    /* renamed from: c  reason: collision with root package name */
    public static final i f4087c = a("is");

    /* renamed from: d  reason: collision with root package name */
    public static final i f4088d = a("cai");

    /* renamed from: e  reason: collision with root package name */
    public static final i f4089e = a("dp");

    /* renamed from: f  reason: collision with root package name */
    public static final i f4090f = a("fbs");

    /* renamed from: g  reason: collision with root package name */
    public static final i f4091g = a("rr");

    /* renamed from: h  reason: collision with root package name */
    public static final i f4092h = a("rt");

    /* renamed from: i  reason: collision with root package name */
    public static final i f4093i = a("ito");

    /* renamed from: j  reason: collision with root package name */
    public static final i f4094j = a("asd");

    /* renamed from: k  reason: collision with root package name */
    public static final i f4095k = a("caa");
    public static final i l = a("cnai");
    public static final i m = a("cnav");
    public static final i n = a("cva");
    public static final i o = a("fma");
    public static final i p = a("fna");
    public static final i q = a("fnna");
    public static final i r = a("fta");
    public static final i s = a("fvs");
    public static final i t = a("par");
    public static final i u = a("psvr");
    public static final i v = a("pvwr");
    public static final i w = a("raa");
    public static final i x = a("rna");
    public static final i y = a("rva");
    public static final i z = a("rrwd");

    /* renamed from: a  reason: collision with root package name */
    private final String f4096a;

    static {
        a("das");
        a("bt");
    }

    protected i(String str) {
        this.f4096a = str;
    }

    private static i a(String str) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("No key name specified");
        } else if (!f4086b.contains(str)) {
            f4086b.add(str);
            return new i(str);
        } else {
            throw new IllegalArgumentException("Key has already been used: " + str);
        }
    }

    public String a() {
        return this.f4096a;
    }
}
