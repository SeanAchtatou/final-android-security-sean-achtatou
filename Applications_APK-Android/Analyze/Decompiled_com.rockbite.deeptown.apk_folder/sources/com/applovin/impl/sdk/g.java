package com.applovin.impl.sdk;

import android.os.Bundle;
import com.applovin.communicator.AppLovinCommunicator;
import com.applovin.communicator.AppLovinCommunicatorMessage;
import com.applovin.communicator.AppLovinCommunicatorPublisher;
import com.applovin.communicator.AppLovinCommunicatorSubscriber;
import com.applovin.impl.communicator.AppLovinSdkTopic;
import com.applovin.impl.communicator.CommunicatorMessageImpl;
import com.applovin.impl.mediation.b;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.network.f;
import com.applovin.impl.sdk.utils.BundleUtils;
import com.applovin.impl.sdk.utils.i;
import com.applovin.sdk.AppLovinSdkUtils;
import com.applovin.sdk.AppLovinWebViewActivity;
import com.tapjoy.TapjoyConstants;
import java.util.Map;
import org.json.JSONObject;

public class g implements AppLovinCommunicatorPublisher, AppLovinCommunicatorSubscriber {

    /* renamed from: a  reason: collision with root package name */
    private final k f4222a;

    /* renamed from: b  reason: collision with root package name */
    private final AppLovinCommunicator f4223b;

    g(k kVar) {
        this.f4222a = kVar;
        this.f4223b = AppLovinCommunicator.getInstance(kVar.d());
        if (!"HSrCHRtOan6wp2kwOIGJC1RDtuSrF2mWVbio2aBcMHX9KF3iTJ1lLSzCKP1ZSo5yNolPNw1kCTtWpxELFF4ah1".equalsIgnoreCase(kVar.X())) {
            this.f4223b.a(kVar);
            this.f4223b.subscribe(this, AppLovinSdkTopic.ALL_TOPICS);
        }
    }

    private void a(Bundle bundle, String str) {
        if (!"log".equals(str)) {
            q Z = this.f4222a.Z();
            Z.b("CommunicatorService", "Sending message " + bundle + " for topic: " + str + "...");
        }
        this.f4223b.getMessagingService().publish(CommunicatorMessageImpl.create(bundle, str, this, this.f4222a.b(c.C0102c.T3).contains(str)));
    }

    public void a(b.C0087b bVar, String str) {
        boolean x = bVar instanceof b.d ? ((b.d) bVar).x() : false;
        Bundle bundle = new Bundle();
        bundle.putString("type", str);
        bundle.putString("id", bVar.p());
        bundle.putString("network_name", bVar.n());
        bundle.putString("max_ad_unit_id", bVar.getAdUnitId());
        bundle.putString("third_party_ad_placement_id", bVar.s());
        bundle.putString("ad_format", bVar.getFormat().getLabel());
        bundle.putString("is_fallback_ad", String.valueOf(x));
        a(bundle, "max_ad_events");
    }

    public void a(JSONObject jSONObject, boolean z) {
        Bundle c2 = i.c(i.b(i.b(jSONObject, "communicator_settings", new JSONObject(), this.f4222a), "safedk_settings", new JSONObject(), this.f4222a));
        Bundle bundle = new Bundle();
        bundle.putString(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.f4222a.X());
        bundle.putString("applovin_random_token", this.f4222a.O());
        bundle.putString(TapjoyConstants.TJC_DEVICE_TYPE_NAME, AppLovinSdkUtils.isTablet(this.f4222a.d()) ? "tablet" : "phone");
        bundle.putString("init_success", String.valueOf(z));
        bundle.putBundle("settings", c2);
        bundle.putBoolean("debug_mode", ((Boolean) this.f4222a.a(c.e.G3)).booleanValue());
        a(bundle, "safedk_init");
    }

    public String getCommunicatorId() {
        return "applovin_sdk";
    }

    public void onMessageReceived(AppLovinCommunicatorMessage appLovinCommunicatorMessage) {
        if (AppLovinSdkTopic.HTTP_REQUEST.equalsIgnoreCase(appLovinCommunicatorMessage.getTopic())) {
            Bundle messageData = appLovinCommunicatorMessage.getMessageData();
            Map<String, String> a2 = i.a(messageData.getBundle("query_params"));
            Map<String, Object> map = BundleUtils.toMap(messageData.getBundle("post_body"));
            Map<String, String> a3 = i.a(messageData.getBundle("headers"));
            if (!map.containsKey(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY)) {
                map.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.f4222a.X());
            }
            f.b bVar = new f.b();
            bVar.a(messageData.getString("url"));
            bVar.b(messageData.getString("backup_url"));
            bVar.a(a2);
            bVar.c(map);
            bVar.b(a3);
            bVar.a(((Boolean) this.f4222a.a(c.e.G3)).booleanValue());
            this.f4222a.m().a(bVar.a());
        }
    }
}
