package com.applovin.impl.sdk.utils;

import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.q;
import java.util.Locale;
import java.util.UUID;

public final class o {

    /* renamed from: a  reason: collision with root package name */
    private final k f4483a;

    /* renamed from: b  reason: collision with root package name */
    private String f4484b = d();

    /* renamed from: c  reason: collision with root package name */
    private final String f4485c;

    /* renamed from: d  reason: collision with root package name */
    private final String f4486d;

    public o(k kVar) {
        this.f4483a = kVar;
        this.f4485c = a(c.g.f4019g, (String) c.h.b(c.g.f4018f, (Object) null, kVar.d()));
        this.f4486d = a(c.g.f4020h, (String) kVar.a(c.e.f4001f));
    }

    private String a(c.g<String> gVar, String str) {
        String str2 = (String) c.h.b(gVar, (Object) null, this.f4483a.d());
        if (m.b(str2)) {
            return str2;
        }
        if (!m.b(str)) {
            str = UUID.randomUUID().toString().toLowerCase(Locale.US);
        }
        c.h.a(gVar, str, this.f4483a.d());
        return str;
    }

    private String d() {
        if (!((Boolean) this.f4483a.a(c.e.U2)).booleanValue()) {
            this.f4483a.b(c.g.f4017e);
        }
        String str = (String) this.f4483a.a(c.g.f4017e);
        if (!m.b(str)) {
            return null;
        }
        q Z = this.f4483a.Z();
        Z.b("AppLovinSdk", "Using identifier (" + str + ") from previous session");
        this.f4484b = str;
        return null;
    }

    public String a() {
        return this.f4484b;
    }

    public void a(String str) {
        if (((Boolean) this.f4483a.a(c.e.U2)).booleanValue()) {
            this.f4483a.a(c.g.f4017e, str);
        }
        this.f4484b = str;
    }

    public String b() {
        return this.f4485c;
    }

    public String c() {
        return this.f4486d;
    }
}
