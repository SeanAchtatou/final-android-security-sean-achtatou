package com.applovin.impl.sdk.d;

import android.text.TextUtils;
import java.util.HashSet;
import java.util.Set;

public class g {

    /* renamed from: b  reason: collision with root package name */
    private static final Set<String> f4073b = new HashSet(32);

    /* renamed from: c  reason: collision with root package name */
    private static final Set<g> f4074c = new HashSet(16);

    /* renamed from: d  reason: collision with root package name */
    public static final g f4075d = a("ad_req");

    /* renamed from: e  reason: collision with root package name */
    public static final g f4076e = a("ad_imp");

    /* renamed from: f  reason: collision with root package name */
    public static final g f4077f = a("ad_session_start");

    /* renamed from: g  reason: collision with root package name */
    public static final g f4078g = a("ad_imp_session");

    /* renamed from: h  reason: collision with root package name */
    public static final g f4079h = a("cached_files_expired");

    /* renamed from: i  reason: collision with root package name */
    public static final g f4080i = a("cache_drop_count");

    /* renamed from: j  reason: collision with root package name */
    public static final g f4081j = a("sdk_reset_state_count", true);

    /* renamed from: k  reason: collision with root package name */
    public static final g f4082k = a("ad_response_process_failures", true);
    public static final g l = a("response_process_failures", true);
    public static final g m = a("incent_failed_to_display_count", true);
    public static final g n = a("app_paused_and_resumed");
    public static final g o = a("ad_rendered_with_mismatched_sdk_key", true);
    public static final g p = a("med_ad_req");
    public static final g q = a("med_ad_response_process_failures", true);
    public static final g r = a("med_adapters_failed_init_missing_activity", true);
    public static final g s = a("med_waterfall_ad_no_fill", true);
    public static final g t = a("med_waterfall_ad_adapter_load_failed", true);
    public static final g u = a("med_waterfall_ad_invalid_response", true);

    /* renamed from: a  reason: collision with root package name */
    private final String f4083a;

    static {
        a("fullscreen_ad_nil_vc_count");
        a("applovin_bundle_missing");
    }

    private g(String str) {
        this.f4083a = str;
    }

    private static g a(String str) {
        return a(str, false);
    }

    private static g a(String str, boolean z) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("No key name specified");
        } else if (!f4073b.contains(str)) {
            f4073b.add(str);
            g gVar = new g(str);
            if (z) {
                f4074c.add(gVar);
            }
            return gVar;
        } else {
            throw new IllegalArgumentException("Key has already been used: " + str);
        }
    }

    public static Set<g> b() {
        return f4074c;
    }

    public String a() {
        return this.f4083a;
    }
}
