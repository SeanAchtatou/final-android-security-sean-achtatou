package com.applovin.impl.sdk.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.security.NetworkSecurityPolicy;
import android.text.TextUtils;
import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.q;
import com.tapjoy.TapjoyConstants;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class h {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f4406a = {7, 4, 2, 1, 11};

    /* renamed from: b  reason: collision with root package name */
    private static final int[] f4407b = {5, 6, 10, 3, 9, 8, 14};

    /* renamed from: c  reason: collision with root package name */
    private static final int[] f4408c = {15, 12, 13};

    public static String a(InputStream inputStream, k kVar) {
        if (inputStream == null) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            byte[] bArr = new byte[((Integer) kVar.a(c.e.y2)).intValue()];
            while (true) {
                int read = inputStream.read(bArr);
                if (read <= 0) {
                    return byteArrayOutputStream.toString("UTF-8");
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
        } catch (Throwable th) {
            kVar.Z().b("ConnectionUtils", "Encountered error while reading stream", th);
            return null;
        }
    }

    public static String a(String str, k kVar) {
        return a((String) kVar.a(c.e.V), str, kVar);
    }

    public static String a(String str, String str2, k kVar) {
        if (str == null || str.length() < 4) {
            throw new IllegalArgumentException("Invalid domain specified");
        } else if (str2 == null) {
            throw new IllegalArgumentException("No endpoint specified");
        } else if (kVar != null) {
            return str + str2;
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    public static Map<String, String> a(k kVar) {
        String str;
        HashMap hashMap = new HashMap();
        String str2 = (String) kVar.a(c.e.f4002g);
        if (m.b(str2)) {
            str = "device_token";
        } else {
            if (!((Boolean) kVar.a(c.e.G3)).booleanValue()) {
                str2 = kVar.X();
                str = TapjoyConstants.TJC_API_KEY;
            }
            hashMap.put("sc", m.d((String) kVar.a(c.e.f4004i)));
            hashMap.put("sc2", m.d((String) kVar.a(c.e.f4005j)));
            hashMap.put("server_installed_at", m.d((String) kVar.a(c.e.f4006k)));
            p.a("persisted_data", m.d((String) kVar.a(c.g.z)), hashMap);
            return hashMap;
        }
        hashMap.put(str, str2);
        hashMap.put("sc", m.d((String) kVar.a(c.e.f4004i)));
        hashMap.put("sc2", m.d((String) kVar.a(c.e.f4005j)));
        hashMap.put("server_installed_at", m.d((String) kVar.a(c.e.f4006k)));
        p.a("persisted_data", m.d((String) kVar.a(c.g.z)), hashMap);
        return hashMap;
    }

    public static JSONObject a(JSONObject jSONObject) throws JSONException {
        return (JSONObject) jSONObject.getJSONArray("results").get(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.c.f.a(com.applovin.impl.sdk.c$e<?>, java.lang.Object):void
     arg types: [com.applovin.impl.sdk.c$e<java.lang.Boolean>, boolean]
     candidates:
      com.applovin.impl.sdk.c.f.a(java.lang.String, com.applovin.impl.sdk.c$e):com.applovin.impl.sdk.c$e<ST>
      com.applovin.impl.sdk.c.f.a(com.applovin.impl.sdk.c$e<?>, java.lang.Object):void */
    public static void a(int i2, k kVar) {
        StringBuilder sb;
        String str;
        c.f c2 = kVar.c();
        if (i2 == 401) {
            c2.a(c.e.f4001f, "");
            c2.a(c.e.f4002g, "");
            c2.a();
            sb = new StringBuilder();
            sb.append("SDK key \"");
            sb.append(kVar.X());
            str = "\" is rejected by AppLovin. Please make sure the SDK key is correct.";
        } else if (i2 == 418) {
            c2.a((c.e<?>) c.e.f4000e, (Object) true);
            c2.a();
            sb = new StringBuilder();
            sb.append("SDK key \"");
            sb.append(kVar.X());
            str = "\" has been blocked. Please contact AppLovin support at support@applovin.com.";
        } else if ((i2 >= 400 && i2 < 500) || i2 == -1) {
            kVar.J();
            return;
        } else {
            return;
        }
        sb.append(str);
        q.i("AppLovinSdk", sb.toString());
    }

    public static void a(JSONObject jSONObject, k kVar) {
        String b2 = i.b(jSONObject, "persisted_data", (String) null, kVar);
        if (m.b(b2)) {
            kVar.a(c.g.z, b2);
            kVar.Z().c("ConnectionUtils", "Updated persisted data");
        }
    }

    public static void a(JSONObject jSONObject, boolean z, k kVar) {
        kVar.C().a(jSONObject, z);
    }

    public static boolean a() {
        return a((String) null);
    }

    private static boolean a(int i2, int[] iArr) {
        for (int i3 : iArr) {
            if (i3 == i2) {
                return true;
            }
        }
        return false;
    }

    public static boolean a(Context context) {
        if (context.getSystemService("connectivity") == null) {
            return true;
        }
        NetworkInfo b2 = b(context);
        if (b2 != null) {
            return b2.isConnected();
        }
        return false;
    }

    public static boolean a(String str) {
        if (g.g()) {
            return (!g.h() || TextUtils.isEmpty(str)) ? NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted() : NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted(str);
        }
        return true;
    }

    private static NetworkInfo b(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager != null) {
            return connectivityManager.getActiveNetworkInfo();
        }
        return null;
    }

    public static String b(k kVar) {
        NetworkInfo b2 = b(kVar.d());
        if (b2 == null) {
            return "unknown";
        }
        int type = b2.getType();
        int subtype = b2.getSubtype();
        return type == 1 ? TapjoyConstants.TJC_CONNECTION_TYPE_WIFI : type == 0 ? a(subtype, f4406a) ? "2g" : a(subtype, f4407b) ? "3g" : a(subtype, f4408c) ? "4g" : TapjoyConstants.TJC_CONNECTION_TYPE_MOBILE : "unknown";
    }

    public static String b(String str, k kVar) {
        return a((String) kVar.a(c.e.W), str, kVar);
    }

    public static void b(JSONObject jSONObject, k kVar) {
        if (jSONObject == null) {
            throw new IllegalArgumentException("No response specified");
        } else if (kVar != null) {
            try {
                if (jSONObject.has("settings")) {
                    c.f c2 = kVar.c();
                    if (!jSONObject.isNull("settings")) {
                        c2.a(jSONObject.getJSONObject("settings"));
                        c2.a();
                        kVar.Z().b("ConnectionUtils", "New settings processed");
                    }
                }
            } catch (JSONException e2) {
                kVar.Z().b("ConnectionUtils", "Unable to parse settings out of API response", e2);
            }
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    public static String c(k kVar) {
        return a((String) kVar.a(c.e.T), "4.0/ad", kVar);
    }

    public static void c(JSONObject jSONObject, k kVar) {
        JSONArray b2 = i.b(jSONObject, "zones", (JSONArray) null, kVar);
        if (b2 != null) {
            Iterator<d> it = kVar.v().a(b2).iterator();
            while (it.hasNext()) {
                d next = it.next();
                if (next.d()) {
                    kVar.T().preloadAds(next);
                } else {
                    kVar.S().preloadAds(next);
                }
            }
            kVar.s().a(kVar.v().a());
            kVar.t().a(kVar.v().a());
        }
    }

    public static String d(k kVar) {
        return a((String) kVar.a(c.e.U), "4.0/ad", kVar);
    }

    public static void d(JSONObject jSONObject, k kVar) {
        JSONObject b2 = i.b(jSONObject, "variables", (JSONObject) null, kVar);
        if (b2 != null) {
            kVar.W().updateVariables(b2);
        }
    }

    public static String e(k kVar) {
        return a((String) kVar.a(c.e.a0), "1.0/variable_config", kVar);
    }

    public static String f(k kVar) {
        return a((String) kVar.a(c.e.b0), "1.0/variable_config", kVar);
    }
}
