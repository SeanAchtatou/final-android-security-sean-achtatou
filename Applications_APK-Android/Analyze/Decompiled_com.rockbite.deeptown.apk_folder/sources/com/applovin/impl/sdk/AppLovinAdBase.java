package com.applovin.impl.sdk;

import android.text.TextUtils;
import com.applovin.impl.sdk.ad.b;
import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.ad.g;
import com.applovin.impl.sdk.ad.j;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.m;
import com.applovin.mediation.AppLovinNativeAdapter;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import com.facebook.appevents.AppEventsConstants;
import java.util.Arrays;
import org.json.JSONObject;

public abstract class AppLovinAdBase implements j, AppLovinAd {

    /* renamed from: a  reason: collision with root package name */
    private final int f3826a;
    protected final JSONObject adObject;
    protected final Object adObjectLock;

    /* renamed from: b  reason: collision with root package name */
    private d f3827b;

    /* renamed from: c  reason: collision with root package name */
    private final long f3828c;

    /* renamed from: d  reason: collision with root package name */
    private g f3829d;
    protected final JSONObject fullResponse;
    protected final Object fullResponseLock;
    protected final k sdk;
    protected final b source;

    protected AppLovinAdBase(JSONObject jSONObject, JSONObject jSONObject2, b bVar, k kVar) {
        if (jSONObject == null) {
            throw new IllegalArgumentException("No ad object specified");
        } else if (jSONObject2 == null) {
            throw new IllegalArgumentException("No response specified");
        } else if (kVar != null) {
            this.adObject = jSONObject;
            this.fullResponse = jSONObject2;
            this.source = bVar;
            this.sdk = kVar;
            this.adObjectLock = new Object();
            this.fullResponseLock = new Object();
            this.f3828c = System.currentTimeMillis();
            char[] charArray = jSONObject.toString().toCharArray();
            Arrays.sort(charArray);
            this.f3826a = new String(charArray).hashCode();
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    /* access modifiers changed from: protected */
    public boolean containsKeyForAdObject(String str) {
        boolean has;
        synchronized (this.adObjectLock) {
            has = this.adObject.has(str);
        }
        return has;
    }

    public boolean equals(Object obj) {
        AppLovinAd b2;
        if ((obj instanceof g) && (b2 = ((g) obj).b()) != null) {
            obj = b2;
        }
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        AppLovinAdBase appLovinAdBase = (AppLovinAdBase) obj;
        d dVar = this.f3827b;
        if (dVar == null ? appLovinAdBase.f3827b == null : dVar.equals(appLovinAdBase.f3827b)) {
            return this.source == appLovinAdBase.source && this.f3826a == appLovinAdBase.f3826a;
        }
        return false;
    }

    public long getAdIdNumber() {
        return getLongFromAdObject(AppLovinNativeAdapter.KEY_EXTRA_AD_ID, -1);
    }

    public String getAdValue(String str) {
        JSONObject jsonObjectFromAdObject;
        if (!TextUtils.isEmpty(str) && (jsonObjectFromAdObject = getJsonObjectFromAdObject("ad_values", null)) != null && jsonObjectFromAdObject.length() > 0) {
            return i.b(jsonObjectFromAdObject, str, (String) null, this.sdk);
        }
        return null;
    }

    public d getAdZone() {
        d dVar = this.f3827b;
        if (dVar != null) {
            return dVar;
        }
        this.f3827b = d.a(getSize(), getType(), getStringFromFullResponse("zone_id", null), this.sdk);
        return this.f3827b;
    }

    /* access modifiers changed from: protected */
    public boolean getBooleanFromAdObject(String str, Boolean bool) {
        boolean booleanValue;
        synchronized (this.adObjectLock) {
            booleanValue = i.a(this.adObject, str, bool, this.sdk).booleanValue();
        }
        return booleanValue;
    }

    /* access modifiers changed from: protected */
    public boolean getBooleanFromFullResponse(String str, boolean z) {
        boolean booleanValue;
        synchronized (this.fullResponseLock) {
            booleanValue = i.a(this.fullResponse, str, Boolean.valueOf(z), this.sdk).booleanValue();
        }
        return booleanValue;
    }

    public String getClCode() {
        String stringFromAdObject = getStringFromAdObject("clcode", "");
        return m.b(stringFromAdObject) ? stringFromAdObject : getStringFromFullResponse("clcode", "");
    }

    public long getCreatedAtMillis() {
        return this.f3828c;
    }

    public g getDummyAd() {
        return this.f3829d;
    }

    public long getFetchLatencyMillis() {
        return getLongFromFullResponse("ad_fetch_latency_millis", -1);
    }

    public long getFetchResponseSize() {
        return getLongFromFullResponse("ad_fetch_response_size", -1);
    }

    /* access modifiers changed from: protected */
    public float getFloatFromAdObject(String str, float f2) {
        float a2;
        synchronized (this.adObjectLock) {
            a2 = i.a(this.adObject, str, f2, this.sdk);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public float getFloatFromFullResponse(String str, float f2) {
        float a2;
        synchronized (this.fullResponseLock) {
            a2 = i.a(this.fullResponse, str, f2, this.sdk);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public int getIntFromAdObject(String str, int i2) {
        int b2;
        synchronized (this.adObjectLock) {
            b2 = i.b(this.adObject, str, i2, this.sdk);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public int getIntFromFullResponse(String str, int i2) {
        int b2;
        synchronized (this.fullResponseLock) {
            b2 = i.b(this.fullResponse, str, i2, this.sdk);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public JSONObject getJsonObjectFromAdObject(String str, JSONObject jSONObject) {
        JSONObject b2;
        synchronized (this.adObjectLock) {
            b2 = i.b(this.adObject, str, jSONObject, this.sdk);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public JSONObject getJsonObjectFromFullResponse(String str, JSONObject jSONObject) {
        JSONObject b2;
        synchronized (this.fullResponseLock) {
            b2 = i.b(this.fullResponse, str, jSONObject, this.sdk);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public long getLongFromAdObject(String str, long j2) {
        long a2;
        synchronized (this.adObjectLock) {
            a2 = i.a(this.adObject, str, j2, this.sdk);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public long getLongFromFullResponse(String str, long j2) {
        long a2;
        synchronized (this.fullResponseLock) {
            a2 = i.a(this.fullResponse, str, j2, this.sdk);
        }
        return a2;
    }

    public String getPrimaryKey() {
        return getStringFromAdObject("pk", "NA");
    }

    public k getSdk() {
        return this.sdk;
    }

    public String getSecondaryKey1() {
        return getStringFromAdObject("sk1", null);
    }

    public String getSecondaryKey2() {
        return getStringFromAdObject("sk2", null);
    }

    public AppLovinAdSize getSize() {
        return AppLovinAdSize.fromString(getStringFromFullResponse("ad_size", null));
    }

    public b getSource() {
        return this.source;
    }

    /* access modifiers changed from: protected */
    public String getStringFromAdObject(String str, String str2) {
        String b2;
        synchronized (this.adObjectLock) {
            b2 = i.b(this.adObject, str, str2, this.sdk);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public String getStringFromFullResponse(String str, String str2) {
        String b2;
        synchronized (this.fullResponseLock) {
            b2 = i.b(this.fullResponse, str, str2, this.sdk);
        }
        return b2;
    }

    public AppLovinAdType getType() {
        return AppLovinAdType.fromString(getStringFromFullResponse(AppEventsConstants.EVENT_PARAM_AD_TYPE, null));
    }

    public String getZoneId() {
        if (getAdZone().j()) {
            return null;
        }
        return getStringFromFullResponse("zone_id", null);
    }

    public boolean hasShown() {
        return getBooleanFromAdObject("shown", false);
    }

    public boolean hasVideoUrl() {
        this.sdk.Z().e("AppLovinAdBase", "Attempting to invoke hasVideoUrl() from base ad class");
        return false;
    }

    public int hashCode() {
        return this.f3826a;
    }

    public boolean isVideoAd() {
        return this.adObject.has("is_video_ad") ? getBooleanFromAdObject("is_video_ad", false) : hasVideoUrl();
    }

    public void setDummyAd(g gVar) {
        this.f3829d = gVar;
    }

    public void setHasShown(boolean z) {
        try {
            synchronized (this.adObjectLock) {
                this.adObject.put("shown", z);
            }
        } catch (Throwable unused) {
        }
    }

    public boolean shouldCancelHtmlCachingIfShown() {
        return getBooleanFromAdObject("chcis", false);
    }

    public String toString() {
        return "AppLovinAd{adIdNumber" + getAdIdNumber() + ", source=" + getSource() + ", zoneId='" + getZoneId() + "'" + '}';
    }
}
