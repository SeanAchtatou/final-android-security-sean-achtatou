package com.applovin.impl.sdk;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import com.applovin.impl.mediation.MediationServiceImpl;
import com.applovin.impl.mediation.a;
import com.applovin.impl.mediation.b;
import com.applovin.impl.mediation.h;
import com.applovin.impl.mediation.i;
import com.applovin.impl.mediation.l;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.d.g;
import com.applovin.impl.sdk.d.j;
import com.applovin.impl.sdk.f;
import com.applovin.impl.sdk.network.PostbackServiceImpl;
import com.applovin.impl.sdk.network.c;
import com.applovin.impl.sdk.network.e;
import com.applovin.impl.sdk.utils.o;
import com.applovin.impl.sdk.utils.p;
import com.applovin.sdk.AppLovinEventService;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkConfiguration;
import com.applovin.sdk.AppLovinSdkSettings;
import com.applovin.sdk.AppLovinSdkUtils;
import com.applovin.sdk.AppLovinUserService;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class k {
    protected static Context Y;
    private v A;
    private b B;
    private p C;
    private u D;
    /* access modifiers changed from: private */
    public com.applovin.impl.sdk.network.c E;
    private g F;
    private PostbackServiceImpl G;
    private e H;
    private i I;
    private h J;
    private MediationServiceImpl K;
    private l L;
    private a.b M;
    private com.applovin.impl.mediation.k N;
    /* access modifiers changed from: private */
    public final Object O = new Object();
    private final AtomicBoolean P = new AtomicBoolean(true);
    /* access modifiers changed from: private */
    public boolean Q = false;
    private boolean R = false;
    private boolean S = false;
    private boolean T = false;
    private boolean U = false;
    private AppLovinSdk.SdkInitializationListener V;
    private AppLovinSdk.SdkInitializationListener W;
    /* access modifiers changed from: private */
    public AppLovinSdkConfiguration X;

    /* renamed from: a  reason: collision with root package name */
    private String f4251a;

    /* renamed from: b  reason: collision with root package name */
    private WeakReference<Activity> f4252b;

    /* renamed from: c  reason: collision with root package name */
    private long f4253c;

    /* renamed from: d  reason: collision with root package name */
    private AppLovinSdkSettings f4254d;

    /* renamed from: e  reason: collision with root package name */
    private AppLovinAdServiceImpl f4255e;

    /* renamed from: f  reason: collision with root package name */
    private NativeAdServiceImpl f4256f;

    /* renamed from: g  reason: collision with root package name */
    private EventServiceImpl f4257g;

    /* renamed from: h  reason: collision with root package name */
    private UserServiceImpl f4258h;

    /* renamed from: i  reason: collision with root package name */
    private VariableServiceImpl f4259i;

    /* renamed from: j  reason: collision with root package name */
    private AppLovinSdk f4260j;
    /* access modifiers changed from: private */

    /* renamed from: k  reason: collision with root package name */
    public q f4261k;
    /* access modifiers changed from: private */
    public f.y l;
    protected c.f m;
    private com.applovin.impl.sdk.network.a n;
    private com.applovin.impl.sdk.d.h o;
    private j p;
    private l q;
    private c.h r;
    private com.applovin.impl.sdk.d.f s;
    private j t;
    private o u;
    private e v;
    private r w;
    private o x;
    private com.applovin.impl.sdk.ad.e y;
    private com.applovin.impl.sdk.d.c z;

    class a implements Runnable {
        a() {
        }

        public void run() {
            if (!k.this.l.a()) {
                k.this.f4261k.b("AppLovinSdk", "Timing out adapters init...");
                k.this.l.e();
                k.this.I();
            }
        }
    }

    class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AppLovinSdk.SdkInitializationListener f4263a;

        b(AppLovinSdk.SdkInitializationListener sdkInitializationListener) {
            this.f4263a = sdkInitializationListener;
        }

        public void run() {
            k.this.f4261k.b("AppLovinSdk", "Calling back publisher's initialization completion handler...");
            this.f4263a.onSdkInitialized(k.this.X);
        }
    }

    class c implements c.a {
        c() {
        }

        public void a() {
            k.this.f4261k.c("AppLovinSdk", "Connected to internet - re-initializing SDK");
            synchronized (k.this.O) {
                if (!k.this.Q) {
                    k.this.F();
                }
            }
            k.this.E.b(this);
        }

        public void b() {
        }
    }

    public static Context e0() {
        return Y;
    }

    private void f0() {
        this.E.a(new c());
    }

    public b A() {
        return this.B;
    }

    public u B() {
        return this.D;
    }

    public g C() {
        return this.F;
    }

    public AppLovinBroadcastManager D() {
        return AppLovinBroadcastManager.getInstance(Y);
    }

    public Activity E() {
        Activity e2 = e();
        if (e2 != null) {
            return e2;
        }
        Activity a2 = A().a();
        if (a2 != null) {
            return a2;
        }
        return null;
    }

    public void F() {
        synchronized (this.O) {
            this.Q = true;
            j().d();
            j().a(new f.r(this), f.y.b.MAIN);
        }
    }

    public boolean G() {
        boolean z2;
        synchronized (this.O) {
            z2 = this.Q;
        }
        return z2;
    }

    public boolean H() {
        boolean z2;
        synchronized (this.O) {
            z2 = this.R;
        }
        return z2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public void I() {
        AppLovinSdk.SdkInitializationListener sdkInitializationListener = this.V;
        if (sdkInitializationListener != null) {
            if (H()) {
                this.V = null;
                this.W = null;
            } else if (this.W != sdkInitializationListener) {
                if (((Boolean) a(c.e.r)).booleanValue()) {
                    this.V = null;
                } else {
                    this.W = sdkInitializationListener;
                }
            } else {
                return;
            }
            AppLovinSdkUtils.runOnUiThreadDelayed(new b(sdkInitializationListener), Math.max(0L, ((Long) a(c.e.s)).longValue()));
        }
    }

    public void J() {
        long b2 = this.o.b(g.f4081j);
        this.m.c();
        this.m.a();
        this.o.a();
        this.z.b();
        this.p.b();
        this.o.b(g.f4081j, b2 + 1);
        if (this.P.compareAndSet(true, false)) {
            F();
        } else {
            this.P.set(true);
        }
    }

    public void K() {
        this.M.b();
    }

    public boolean L() {
        return this.A.d();
    }

    public String M() {
        return this.u.a();
    }

    public String N() {
        return this.u.b();
    }

    public String O() {
        return this.u.c();
    }

    public AppLovinSdkSettings P() {
        return this.f4254d;
    }

    public AppLovinSdkConfiguration Q() {
        return this.X;
    }

    public String R() {
        return (String) a(c.g.A);
    }

    public AppLovinAdServiceImpl S() {
        return this.f4255e;
    }

    public NativeAdServiceImpl T() {
        return this.f4256f;
    }

    public AppLovinEventService U() {
        return this.f4257g;
    }

    public AppLovinUserService V() {
        return this.f4258h;
    }

    public VariableServiceImpl W() {
        return this.f4259i;
    }

    public String X() {
        return this.f4251a;
    }

    public boolean Y() {
        return this.S;
    }

    public q Z() {
        return this.f4261k;
    }

    public l a() {
        return this.L;
    }

    public <ST> c.e<ST> a(String str, c.e<ST> eVar) {
        return this.m.a(str, eVar);
    }

    public <T> T a(c.e eVar) {
        return this.m.a(eVar);
    }

    public <T> T a(c.g gVar) {
        return b(gVar, null);
    }

    public <T> T a(String str, T t2, Class cls, SharedPreferences sharedPreferences) {
        return c.h.a(str, t2, cls, sharedPreferences);
    }

    public void a(long j2) {
        this.t.a(j2);
    }

    public void a(SharedPreferences sharedPreferences) {
        this.r.a(sharedPreferences);
    }

    public void a(b.f fVar) {
        if (!this.l.a()) {
            List<String> b2 = b(c.d.T3);
            if (b2.size() > 0 && this.J.b().containsAll(b2)) {
                this.f4261k.b("AppLovinSdk", "All required adapters initialized");
                this.l.e();
                I();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.c.h.a(com.applovin.impl.sdk.c$g, java.lang.Object):void
     arg types: [com.applovin.impl.sdk.c$g<T>, T]
     candidates:
      com.applovin.impl.sdk.c.h.a(com.applovin.impl.sdk.c$g, android.content.Context):void
      com.applovin.impl.sdk.c.h.a(com.applovin.impl.sdk.c$g, java.lang.Object):void */
    public <T> void a(c.g<T> gVar, T t2) {
        this.r.a((c.g) gVar, (Object) t2);
    }

    public <T> void a(c.g gVar, Object obj, SharedPreferences sharedPreferences) {
        this.r.a(gVar, obj, sharedPreferences);
    }

    public void a(AppLovinSdk.SdkInitializationListener sdkInitializationListener) {
        if (!H()) {
            this.V = sdkInitializationListener;
        } else if (sdkInitializationListener != null) {
            sdkInitializationListener.onSdkInitialized(this.X);
        }
    }

    public void a(AppLovinSdk appLovinSdk) {
        this.f4260j = appLovinSdk;
    }

    public void a(String str) {
        q.f("AppLovinSdk", "Setting plugin version: " + str);
        this.m.a(c.e.V2, str);
        this.m.a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.c.h.a(com.applovin.impl.sdk.c$g, java.lang.Object):void
     arg types: [com.applovin.impl.sdk.c$g<java.lang.Boolean>, boolean]
     candidates:
      com.applovin.impl.sdk.c.h.a(com.applovin.impl.sdk.c$g, android.content.Context):void
      com.applovin.impl.sdk.c.h.a(com.applovin.impl.sdk.c$g, java.lang.Object):void */
    public void a(String str, AppLovinSdkSettings appLovinSdkSettings, Context context) {
        c.h hVar;
        c.g<String> gVar;
        String bool;
        this.f4251a = str;
        this.f4253c = System.currentTimeMillis();
        this.f4254d = appLovinSdkSettings;
        this.X = new SdkConfigurationImpl(this);
        Y = context.getApplicationContext();
        if (context instanceof Activity) {
            this.f4252b = new WeakReference<>((Activity) context);
        }
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            this.f4261k = new q(this);
            this.r = new c.h(this);
            this.m = new c.f(this);
            this.m.b();
            this.s = new com.applovin.impl.sdk.d.f(this);
            this.s.b();
            this.x = new o(this);
            this.v = new e(this);
            this.w = new r(this);
            this.y = new com.applovin.impl.sdk.ad.e(this);
            this.f4257g = new EventServiceImpl(this);
            this.f4258h = new UserServiceImpl(this);
            this.f4259i = new VariableServiceImpl(this);
            this.z = new com.applovin.impl.sdk.d.c(this);
            this.l = new f.y(this);
            this.n = new com.applovin.impl.sdk.network.a(this);
            this.o = new com.applovin.impl.sdk.d.h(this);
            this.p = new j(this);
            this.q = new l(this);
            this.B = new b(context);
            this.f4255e = new AppLovinAdServiceImpl(this);
            this.f4256f = new NativeAdServiceImpl(this);
            this.A = new v(this);
            this.C = new p(this);
            this.G = new PostbackServiceImpl(this);
            this.H = new e(this);
            this.I = new i(this);
            this.J = new h(this);
            this.K = new MediationServiceImpl(this);
            this.M = new a.b(this);
            this.L = new l();
            this.N = new com.applovin.impl.mediation.k(this);
            this.t = new j(this);
            this.u = new o(this);
            this.D = new u(this);
            this.F = new g(this);
            if (((Boolean) this.m.a(c.e.B2)).booleanValue()) {
                this.E = new com.applovin.impl.sdk.network.c(context);
            }
            if (TextUtils.isEmpty(str)) {
                this.S = true;
                q.i("AppLovinSdk", "Unable to find AppLovin SDK key. Please add  meta-data android:name=\"applovin.sdk.key\" android:value=\"YOUR_SDK_KEY_HERE\" into AndroidManifest.xml.");
                StringWriter stringWriter = new StringWriter();
                new Throwable("").printStackTrace(new PrintWriter(stringWriter));
                String stringWriter2 = stringWriter.toString();
                q.i("AppLovinSdk", "Called with an invalid SDK key from: " + stringWriter2);
            }
            if (!Y()) {
                if (((Boolean) this.m.a(c.e.m)).booleanValue()) {
                    appLovinSdkSettings.setTestAdsEnabled(p.b(context));
                    appLovinSdkSettings.setVerboseLogging(p.c(context));
                    c().a(appLovinSdkSettings);
                    c().a();
                }
                SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                if (TextUtils.isEmpty((String) this.r.b(c.g.f4015c, (Object) null, defaultSharedPreferences))) {
                    this.T = true;
                    hVar = this.r;
                    gVar = c.g.f4015c;
                    bool = Boolean.toString(true);
                } else {
                    hVar = this.r;
                    gVar = c.g.f4015c;
                    bool = Boolean.toString(false);
                }
                hVar.a(gVar, bool, defaultSharedPreferences);
                if (((Boolean) this.r.b(c.g.f4016d, false)).booleanValue()) {
                    this.f4261k.b("AppLovinSdk", "Initializing SDK for non-maiden launch");
                    this.U = true;
                } else {
                    this.f4261k.b("AppLovinSdk", "Initializing SDK for maiden launch");
                    this.r.a((c.g) c.g.f4016d, (Object) true);
                }
                if (TextUtils.isEmpty((String) a(c.g.f4021i))) {
                    a(c.g.f4021i, String.valueOf(((int) (Math.random() * 100.0d)) + 1));
                }
                boolean a2 = com.applovin.impl.sdk.utils.h.a(d());
                if (!((Boolean) this.m.a(c.e.C2)).booleanValue() || a2) {
                    F();
                }
                if (((Boolean) this.m.a(c.e.B2)).booleanValue() && !a2) {
                    this.f4261k.c("AppLovinSdk", "SDK initialized with no internet connection - listening for connection");
                    f0();
                }
            } else {
                a(false);
            }
        } catch (Throwable th) {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
            throw th;
        }
        StrictMode.setThreadPolicy(allowThreadDiskReads);
    }

    public <T> void a(String str, Object obj, SharedPreferences.Editor editor) {
        this.r.a(str, obj, editor);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.f.y.a(com.applovin.impl.sdk.f$c, com.applovin.impl.sdk.f$y$b, long, boolean):void
     arg types: [com.applovin.impl.sdk.f$f, com.applovin.impl.sdk.f$y$b, long, int]
     candidates:
      com.applovin.impl.sdk.f.y.a(java.lang.Runnable, long, java.util.concurrent.ScheduledExecutorService, boolean):void
      com.applovin.impl.sdk.f.y.a(com.applovin.impl.sdk.f$c, com.applovin.impl.sdk.f$y$b, long, boolean):void */
    public void a(boolean z2) {
        synchronized (this.O) {
            this.Q = false;
            this.R = z2;
        }
        List<String> b2 = b(c.d.T3);
        if (b2.isEmpty()) {
            this.l.e();
            I();
            return;
        }
        long longValue = ((Long) a(c.d.U3)).longValue();
        f.C0104f fVar = new f.C0104f(this, true, new a());
        q qVar = this.f4261k;
        qVar.b("AppLovinSdk", "Waiting for required adapters to init: " + b2 + " - timing out in " + longValue + "ms...");
        this.l.a((f.c) fVar, f.y.b.MEDIATION_TIMEOUT, longValue, true);
    }

    public i a0() {
        return this.I;
    }

    public com.applovin.impl.mediation.k b() {
        return this.N;
    }

    public <T> T b(c.g<T> gVar, T t2) {
        return this.r.b(gVar, t2);
    }

    public <T> T b(c.g<T> gVar, T t2, SharedPreferences sharedPreferences) {
        return this.r.b(gVar, t2, sharedPreferences);
    }

    public List<String> b(c.e eVar) {
        return this.m.b(eVar);
    }

    public <T> void b(c.g gVar) {
        this.r.a(gVar);
    }

    public void b(String str) {
        q.f("AppLovinSdk", "Setting user id: " + str);
        this.u.a(str);
    }

    public h b0() {
        return this.J;
    }

    public c.f c() {
        return this.m;
    }

    public void c(String str) {
        a(c.g.A, str);
    }

    public MediationServiceImpl c0() {
        return this.K;
    }

    public Context d() {
        return Y;
    }

    public a.b d0() {
        return this.M;
    }

    public Activity e() {
        WeakReference<Activity> weakReference = this.f4252b;
        if (weakReference != null) {
            return weakReference.get();
        }
        return null;
    }

    public long f() {
        return this.f4253c;
    }

    public boolean g() {
        return this.T;
    }

    public boolean h() {
        return this.U;
    }

    public com.applovin.impl.sdk.network.a i() {
        return this.n;
    }

    public f.y j() {
        return this.l;
    }

    public com.applovin.impl.sdk.d.h k() {
        return this.o;
    }

    public j l() {
        return this.p;
    }

    public e m() {
        return this.H;
    }

    public l n() {
        return this.q;
    }

    public com.applovin.impl.sdk.d.f o() {
        return this.s;
    }

    public j p() {
        return this.t;
    }

    public PostbackServiceImpl q() {
        return this.G;
    }

    public AppLovinSdk r() {
        return this.f4260j;
    }

    public e s() {
        return this.v;
    }

    public r t() {
        return this.w;
    }

    public String toString() {
        return "CoreSdk{sdkKey='" + this.f4251a + '\'' + ", enabled=" + this.R + ", isFirstSession=" + this.T + '}';
    }

    public o u() {
        return this.x;
    }

    public com.applovin.impl.sdk.ad.e v() {
        return this.y;
    }

    public com.applovin.impl.sdk.d.c w() {
        return this.z;
    }

    public v x() {
        return this.A;
    }

    public p y() {
        return this.C;
    }

    public void z() {
        synchronized (this.O) {
            if (!this.Q && !this.R) {
                F();
            }
        }
    }
}
