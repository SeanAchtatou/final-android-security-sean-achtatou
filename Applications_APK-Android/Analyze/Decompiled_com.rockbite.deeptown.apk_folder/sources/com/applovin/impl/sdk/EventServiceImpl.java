package com.applovin.impl.sdk;

import android.content.Intent;
import android.text.TextUtils;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.f;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.network.f;
import com.applovin.impl.sdk.network.g;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.p;
import com.applovin.sdk.AppLovinEventParameters;
import com.applovin.sdk.AppLovinEventService;
import com.applovin.sdk.AppLovinEventTypes;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinWebViewActivity;
import com.facebook.appevents.AppEventsConstants;
import com.google.ads.mediation.inmobi.InMobiNetworkValues;
import com.tapjoy.TapjoyConstants;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class EventServiceImpl implements AppLovinEventService {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final k f3858a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final Map<String, Object> f3859b;

    /* renamed from: c  reason: collision with root package name */
    private final AtomicBoolean f3860c = new AtomicBoolean();

    class a implements f.p.a {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ String f3861a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Map f3862b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ boolean f3863c;

        /* renamed from: d  reason: collision with root package name */
        final /* synthetic */ Map f3864d;

        a(String str, Map map, boolean z, Map map2) {
            this.f3861a = str;
            this.f3862b = map;
            this.f3863c = z;
            this.f3864d = map2;
        }

        public void a(l.c cVar) {
            m mVar = new m(this.f3861a, this.f3862b, EventServiceImpl.this.f3859b);
            try {
                if (this.f3863c) {
                    f.b k2 = com.applovin.impl.sdk.network.f.k();
                    k2.a(EventServiceImpl.this.a());
                    k2.b(EventServiceImpl.this.b());
                    k2.a(EventServiceImpl.this.a(mVar, cVar));
                    k2.b(this.f3864d);
                    k2.c(mVar.b());
                    k2.a(((Boolean) EventServiceImpl.this.f3858a.a(c.e.G3)).booleanValue());
                    EventServiceImpl.this.f3858a.m().a(k2.a());
                    return;
                }
                g.a b2 = g.b(EventServiceImpl.this.f3858a);
                b2.d(EventServiceImpl.this.a());
                b2.e(EventServiceImpl.this.b());
                b2.b((Map<String, String>) EventServiceImpl.this.a(mVar, cVar));
                b2.c(this.f3864d);
                b2.b(i.a((Map<String, ?>) mVar.b()));
                b2.c(((Boolean) EventServiceImpl.this.f3858a.a(c.e.G3)).booleanValue());
                EventServiceImpl.this.f3858a.q().dispatchPostbackRequest(b2.a(), null);
            } catch (Throwable th) {
                q Z = EventServiceImpl.this.f3858a.Z();
                Z.b("AppLovinEventService", "Unable to track event: " + mVar, th);
            }
        }
    }

    public EventServiceImpl(k kVar) {
        this.f3858a = kVar;
        if (((Boolean) kVar.a(c.e.i0)).booleanValue()) {
            this.f3859b = i.a((String) this.f3858a.b(c.g.r, "{}"), new HashMap(), this.f3858a);
            return;
        }
        this.f3859b = new HashMap();
        kVar.a(c.g.r, "{}");
    }

    /* access modifiers changed from: private */
    public String a() {
        return ((String) this.f3858a.a(c.e.Y)) + "4.0/pix";
    }

    /* access modifiers changed from: private */
    public HashMap<String, String> a(m mVar, l.c cVar) {
        l n = this.f3858a.n();
        l.f b2 = n.b();
        l.d c2 = n.c();
        boolean contains = this.f3858a.b(c.e.f0).contains(mVar.a());
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(TapjoyConstants.TJC_SDK_TYPE_DEFAULT, contains ? m.d(mVar.a()) : "postinstall");
        hashMap.put("ts", Long.toString(mVar.c()));
        hashMap.put(TapjoyConstants.TJC_PLATFORM, m.d(b2.f4291c));
        hashMap.put("model", m.d(b2.f4289a));
        hashMap.put(InMobiNetworkValues.PACKAGE_NAME, m.d(c2.f4282c));
        hashMap.put("installer_name", m.d(c2.f4283d));
        hashMap.put("ia", Long.toString(c2.f4286g));
        hashMap.put("api_did", this.f3858a.a(c.e.f4001f));
        hashMap.put("brand", m.d(b2.f4292d));
        hashMap.put("brand_name", m.d(b2.f4293e));
        hashMap.put("hardware", m.d(b2.f4294f));
        hashMap.put("revision", m.d(b2.f4295g));
        hashMap.put("sdk_version", AppLovinSdk.VERSION);
        hashMap.put("os", m.d(b2.f4290b));
        hashMap.put("orientation_lock", b2.l);
        hashMap.put(TapjoyConstants.TJC_APP_VERSION_NAME, m.d(c2.f4281b));
        hashMap.put(TapjoyConstants.TJC_DEVICE_COUNTRY_CODE, m.d(b2.f4297i));
        hashMap.put("carrier", m.d(b2.f4298j));
        hashMap.put("tz_offset", String.valueOf(b2.r));
        hashMap.put("aida", String.valueOf(b2.I));
        String str = "1";
        hashMap.put("adr", b2.t ? str : AppEventsConstants.EVENT_PARAM_VALUE_NO);
        hashMap.put("volume", String.valueOf(b2.v));
        if (!b2.x) {
            str = AppEventsConstants.EVENT_PARAM_VALUE_NO;
        }
        hashMap.put("sim", str);
        hashMap.put("gy", String.valueOf(b2.y));
        hashMap.put("is_tablet", String.valueOf(b2.z));
        hashMap.put("tv", String.valueOf(b2.A));
        hashMap.put("vs", String.valueOf(b2.B));
        hashMap.put("lpm", String.valueOf(b2.C));
        hashMap.put("tg", c2.f4284e);
        hashMap.put("fs", String.valueOf(b2.E));
        hashMap.put("fm", String.valueOf(b2.F.f4301b));
        hashMap.put("tm", String.valueOf(b2.F.f4300a));
        hashMap.put("lmt", String.valueOf(b2.F.f4302c));
        hashMap.put("lm", String.valueOf(b2.F.f4303d));
        hashMap.put("adns", String.valueOf(b2.m));
        hashMap.put("adnsd", String.valueOf(b2.n));
        hashMap.put("xdpi", String.valueOf(b2.o));
        hashMap.put("ydpi", String.valueOf(b2.p));
        hashMap.put("screen_size_in", String.valueOf(b2.q));
        hashMap.put(TapjoyConstants.TJC_DEBUG, Boolean.toString(p.b(this.f3858a)));
        if (!((Boolean) this.f3858a.a(c.e.G3)).booleanValue()) {
            hashMap.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.f3858a.X());
        }
        a(cVar, hashMap);
        if (((Boolean) this.f3858a.a(c.e.O2)).booleanValue()) {
            p.a("cuid", this.f3858a.M(), hashMap);
        }
        if (((Boolean) this.f3858a.a(c.e.R2)).booleanValue()) {
            hashMap.put("compass_random_token", this.f3858a.N());
        }
        if (((Boolean) this.f3858a.a(c.e.T2)).booleanValue()) {
            hashMap.put("applovin_random_token", this.f3858a.O());
        }
        Boolean bool = b2.G;
        if (bool != null) {
            hashMap.put("huc", bool.toString());
        }
        Boolean bool2 = b2.H;
        if (bool2 != null) {
            hashMap.put("aru", bool2.toString());
        }
        l.e eVar = b2.u;
        if (eVar != null) {
            hashMap.put("act", String.valueOf(eVar.f4287a));
            hashMap.put("acm", String.valueOf(eVar.f4288b));
        }
        String str2 = b2.w;
        if (m.b(str2)) {
            hashMap.put("ua", m.d(str2));
        }
        String str3 = b2.D;
        if (!TextUtils.isEmpty(str3)) {
            hashMap.put("so", m.d(str3));
        }
        if (!contains) {
            hashMap.put("sub_event", m.d(mVar.a()));
        }
        hashMap.put("sc", m.d((String) this.f3858a.a(c.e.f4004i)));
        hashMap.put("sc2", m.d((String) this.f3858a.a(c.e.f4005j)));
        hashMap.put("server_installed_at", m.d((String) this.f3858a.a(c.e.f4006k)));
        p.a("persisted_data", m.d((String) this.f3858a.a(c.g.z)), hashMap);
        p.a("plugin_version", m.d((String) this.f3858a.a(c.e.V2)), hashMap);
        p.a("mediation_provider", m.d(this.f3858a.R()), hashMap);
        return hashMap;
    }

    private void a(f.p.a aVar) {
        this.f3858a.j().a(new f.p(this.f3858a, aVar), f.y.b.ADVERTISING_INFO_COLLECTION);
    }

    private void a(l.c cVar, Map<String, String> map) {
        String str = cVar.f4279b;
        if (m.b(str)) {
            map.put("idfa", str);
        }
        map.put("dnt", Boolean.toString(cVar.f4278a));
    }

    /* access modifiers changed from: private */
    public String b() {
        return ((String) this.f3858a.a(c.e.Z)) + "4.0/pix";
    }

    private void c() {
        if (((Boolean) this.f3858a.a(c.e.i0)).booleanValue()) {
            this.f3858a.a(c.g.r, i.a(this.f3859b, "{}", this.f3858a));
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str, boolean z) {
        trackEvent(str, new HashMap(), null, z);
    }

    public Map<String, Object> getSuperProperties() {
        return new HashMap(this.f3859b);
    }

    public void maybeTrackAppOpenEvent() {
        if (this.f3860c.compareAndSet(false, true)) {
            this.f3858a.U().trackEvent("landing");
        }
    }

    public void setSuperProperty(Object obj, String str) {
        if (TextUtils.isEmpty(str)) {
            q.i("AppLovinEventService", "Super property key cannot be null or empty");
        } else if (obj == null) {
            this.f3859b.remove(str);
            c();
        } else {
            List<String> b2 = this.f3858a.b(c.e.h0);
            if (!p.a(obj, b2, this.f3858a)) {
                q.i("AppLovinEventService", "Failed to set super property '" + obj + "' for key '" + str + "' - valid super property types include: " + b2);
                return;
            }
            this.f3859b.put(str, p.a(obj, this.f3858a));
            c();
        }
    }

    public String toString() {
        return "EventService{}";
    }

    public void trackCheckout(String str, Map<String, String> map) {
        HashMap hashMap;
        if (map == null) {
            hashMap = new HashMap(1);
        }
        hashMap.put("transaction_id", str);
        trackEvent(AppLovinEventTypes.USER_COMPLETED_CHECKOUT, hashMap);
    }

    public void trackEvent(String str) {
        trackEvent(str, new HashMap());
    }

    public void trackEvent(String str, Map<String, String> map) {
        trackEvent(str, map, null, true);
    }

    public void trackEvent(String str, Map<String, String> map, Map<String, String> map2, boolean z) {
        if (((Boolean) this.f3858a.a(c.e.g0)).booleanValue()) {
            q Z = this.f3858a.Z();
            Z.b("AppLovinEventService", "Tracking event: \"" + str + "\" with parameters: " + map);
            a(new a(str, map, z, map2));
        }
    }

    public void trackInAppPurchase(Intent intent, Map<String, String> map) {
        HashMap hashMap;
        if (map == null) {
            hashMap = new HashMap();
        }
        try {
            hashMap.put(AppLovinEventParameters.IN_APP_PURCHASE_DATA, intent.getStringExtra("INAPP_PURCHASE_DATA"));
            hashMap.put(AppLovinEventParameters.IN_APP_DATA_SIGNATURE, intent.getStringExtra("INAPP_DATA_SIGNATURE"));
        } catch (Throwable th) {
            q.c("AppLovinEventService", "Unable to track in app purchase - invalid purchase intent", th);
        }
        trackEvent(AppLovinEventTypes.USER_COMPLETED_IN_APP_PURCHASE, hashMap);
    }
}
