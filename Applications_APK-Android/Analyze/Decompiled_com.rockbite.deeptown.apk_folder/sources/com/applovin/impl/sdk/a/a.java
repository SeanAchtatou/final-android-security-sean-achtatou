package com.applovin.impl.sdk.a;

import android.content.Context;
import com.applovin.adview.AppLovinInterstitialAd;
import com.applovin.adview.AppLovinInterstitialAdDialog;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.AppLovinAdServiceImpl;
import com.applovin.impl.sdk.ad.f;
import com.applovin.impl.sdk.ad.g;
import com.applovin.impl.sdk.ad.i;
import com.applovin.impl.sdk.f;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.q;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.p;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdRewardListener;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.applovin.sdk.AppLovinErrorCodes;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkUtils;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import java.lang.ref.SoftReference;
import java.util.Map;

public class a {

    /* renamed from: a  reason: collision with root package name */
    protected final k f3892a;

    /* renamed from: b  reason: collision with root package name */
    protected final AppLovinAdServiceImpl f3893b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public AppLovinAd f3894c;

    /* renamed from: d  reason: collision with root package name */
    private String f3895d;

    /* renamed from: e  reason: collision with root package name */
    private SoftReference<AppLovinAdLoadListener> f3896e;

    /* renamed from: f  reason: collision with root package name */
    private final Object f3897f = new Object();

    /* renamed from: g  reason: collision with root package name */
    private volatile String f3898g;
    /* access modifiers changed from: private */

    /* renamed from: h  reason: collision with root package name */
    public volatile boolean f3899h = false;

    /* renamed from: i  reason: collision with root package name */
    private SoftReference<AppLovinInterstitialAdDialog> f3900i;

    /* renamed from: com.applovin.impl.sdk.a.a$a  reason: collision with other inner class name */
    class C0097a implements AppLovinAdRewardListener {
        C0097a() {
        }

        public void userDeclinedToViewAd(AppLovinAd appLovinAd) {
            a.this.f3892a.Z().b("IncentivizedAdController", "User declined to view");
        }

        public void userOverQuota(AppLovinAd appLovinAd, Map<String, String> map) {
            q Z = a.this.f3892a.Z();
            Z.b("IncentivizedAdController", "User over quota: " + map);
        }

        public void userRewardRejected(AppLovinAd appLovinAd, Map<String, String> map) {
            q Z = a.this.f3892a.Z();
            Z.b("IncentivizedAdController", "Reward rejected: " + map);
        }

        public void userRewardVerified(AppLovinAd appLovinAd, Map<String, String> map) {
            q Z = a.this.f3892a.Z();
            Z.b("IncentivizedAdController", "Reward validated: " + map);
        }

        public void validationRequestFailed(AppLovinAd appLovinAd, int i2) {
            q Z = a.this.f3892a.Z();
            Z.b("IncentivizedAdController", "Reward validation failed: " + i2);
        }
    }

    private class b implements AppLovinAdLoadListener {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final AppLovinAdLoadListener f3902a;

        /* renamed from: com.applovin.impl.sdk.a.a$b$a  reason: collision with other inner class name */
        class C0098a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ AppLovinAd f3904a;

            C0098a(AppLovinAd appLovinAd) {
                this.f3904a = appLovinAd;
            }

            public void run() {
                try {
                    b.this.f3902a.adReceived(this.f3904a);
                } catch (Throwable th) {
                    q.c("AppLovinIncentivizedInterstitial", "Unable to notify ad listener about a newly loaded ad", th);
                }
            }
        }

        /* renamed from: com.applovin.impl.sdk.a.a$b$b  reason: collision with other inner class name */
        class C0099b implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ int f3906a;

            C0099b(int i2) {
                this.f3906a = i2;
            }

            public void run() {
                try {
                    b.this.f3902a.failedToReceiveAd(this.f3906a);
                } catch (Throwable th) {
                    q.c("AppLovinIncentivizedInterstitial", "Unable to notify listener about ad load failure", th);
                }
            }
        }

        b(AppLovinAdLoadListener appLovinAdLoadListener) {
            this.f3902a = appLovinAdLoadListener;
        }

        public void adReceived(AppLovinAd appLovinAd) {
            AppLovinAd unused = a.this.f3894c = appLovinAd;
            if (this.f3902a != null) {
                AppLovinSdkUtils.runOnUiThread(new C0098a(appLovinAd));
            }
        }

        public void failedToReceiveAd(int i2) {
            if (this.f3902a != null) {
                AppLovinSdkUtils.runOnUiThread(new C0099b(i2));
            }
        }
    }

    private class c implements i, AppLovinAdClickListener, AppLovinAdRewardListener, AppLovinAdVideoPlaybackListener {

        /* renamed from: a  reason: collision with root package name */
        private final AppLovinAdDisplayListener f3908a;

        /* renamed from: b  reason: collision with root package name */
        private final AppLovinAdClickListener f3909b;

        /* renamed from: c  reason: collision with root package name */
        private final AppLovinAdVideoPlaybackListener f3910c;

        /* renamed from: d  reason: collision with root package name */
        private final AppLovinAdRewardListener f3911d;

        private c(AppLovinAdRewardListener appLovinAdRewardListener, AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener, AppLovinAdDisplayListener appLovinAdDisplayListener, AppLovinAdClickListener appLovinAdClickListener) {
            this.f3908a = appLovinAdDisplayListener;
            this.f3909b = appLovinAdClickListener;
            this.f3910c = appLovinAdVideoPlaybackListener;
            this.f3911d = appLovinAdRewardListener;
        }

        /* synthetic */ c(a aVar, AppLovinAdRewardListener appLovinAdRewardListener, AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener, AppLovinAdDisplayListener appLovinAdDisplayListener, AppLovinAdClickListener appLovinAdClickListener, C0097a aVar2) {
            this(appLovinAdRewardListener, appLovinAdVideoPlaybackListener, appLovinAdDisplayListener, appLovinAdClickListener);
        }

        private void a(f fVar) {
            String str;
            int i2;
            if (!m.b(a.this.e()) || !a.this.f3899h) {
                fVar.B();
                if (a.this.f3899h) {
                    i2 = AppLovinErrorCodes.INCENTIVIZED_SERVER_TIMEOUT;
                    str = "network_timeout";
                } else {
                    i2 = AppLovinErrorCodes.INCENTIVIZED_USER_CLOSED_VIDEO;
                    str = "user_closed_video";
                }
                fVar.a(c.a(str));
                j.a(this.f3911d, fVar, i2);
            }
            a.this.a(fVar);
            j.b(this.f3908a, fVar);
            if (!fVar.M().getAndSet(true)) {
                a.this.f3892a.j().a(new f.f0(fVar, a.this.f3892a), f.y.b.REWARD);
            }
        }

        public void adClicked(AppLovinAd appLovinAd) {
            j.a(this.f3909b, appLovinAd);
        }

        public void adDisplayed(AppLovinAd appLovinAd) {
            j.a(this.f3908a, appLovinAd);
        }

        public void adHidden(AppLovinAd appLovinAd) {
            if (appLovinAd instanceof g) {
                appLovinAd = ((g) appLovinAd).a();
            }
            if (appLovinAd instanceof com.applovin.impl.sdk.ad.f) {
                a((com.applovin.impl.sdk.ad.f) appLovinAd);
                return;
            }
            q Z = a.this.f3892a.Z();
            Z.e("IncentivizedAdController", "Something is terribly wrong. Received `adHidden` callback for invalid ad of type: " + appLovinAd);
        }

        public void onAdDisplayFailed(String str) {
            j.a(this.f3908a, str);
        }

        public void userDeclinedToViewAd(AppLovinAd appLovinAd) {
        }

        public void userOverQuota(AppLovinAd appLovinAd, Map<String, String> map) {
            a.this.a("quota_exceeded");
            j.b(this.f3911d, appLovinAd, map);
        }

        public void userRewardRejected(AppLovinAd appLovinAd, Map<String, String> map) {
            a.this.a("rejected");
            j.c(this.f3911d, appLovinAd, map);
        }

        public void userRewardVerified(AppLovinAd appLovinAd, Map<String, String> map) {
            a.this.a("accepted");
            j.a(this.f3911d, appLovinAd, map);
        }

        public void validationRequestFailed(AppLovinAd appLovinAd, int i2) {
            a.this.a("network_timeout");
            j.a(this.f3911d, appLovinAd, i2);
        }

        public void videoPlaybackBegan(AppLovinAd appLovinAd) {
            j.a(this.f3910c, appLovinAd);
        }

        public void videoPlaybackEnded(AppLovinAd appLovinAd, double d2, boolean z) {
            j.a(this.f3910c, appLovinAd, d2, z);
            boolean unused = a.this.f3899h = z;
        }
    }

    public a(String str, AppLovinSdk appLovinSdk) {
        this.f3892a = p.a(appLovinSdk);
        this.f3893b = (AppLovinAdServiceImpl) appLovinSdk.getAdService();
        this.f3895d = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.utils.p.a(com.applovin.sdk.AppLovinAd, com.applovin.impl.sdk.k):com.applovin.sdk.AppLovinAd
     arg types: [com.applovin.impl.sdk.AppLovinAdBase, com.applovin.impl.sdk.k]
     candidates:
      com.applovin.impl.sdk.utils.p.a(android.view.View, com.applovin.impl.sdk.k):android.app.Activity
      com.applovin.impl.sdk.utils.p.a(java.io.File, int):android.graphics.Bitmap
      com.applovin.impl.sdk.utils.p.a(android.content.Context, android.view.View):android.view.View
      com.applovin.impl.sdk.utils.p.a(org.json.JSONObject, com.applovin.impl.sdk.k):com.applovin.impl.sdk.ad.d
      com.applovin.impl.sdk.utils.p.a(java.lang.Object, com.applovin.impl.sdk.k):java.lang.Object
      com.applovin.impl.sdk.utils.p.a(java.lang.Class, java.lang.String):java.lang.reflect.Field
      com.applovin.impl.sdk.utils.p.a(java.util.List<java.lang.String>, com.applovin.impl.sdk.k):java.util.List<java.lang.Class>
      com.applovin.impl.sdk.utils.p.a(java.io.Closeable, com.applovin.impl.sdk.k):void
      com.applovin.impl.sdk.utils.p.a(java.net.HttpURLConnection, com.applovin.impl.sdk.k):void
      com.applovin.impl.sdk.utils.p.a(long, long):boolean
      com.applovin.impl.sdk.utils.p.a(android.view.View, android.app.Activity):boolean
      com.applovin.impl.sdk.utils.p.a(android.view.View, android.view.View):boolean
      com.applovin.impl.sdk.utils.p.a(java.lang.String, java.util.List<java.lang.String>):boolean
      com.applovin.impl.sdk.utils.p.a(com.applovin.sdk.AppLovinAd, com.applovin.impl.sdk.k):com.applovin.sdk.AppLovinAd */
    private void a(AppLovinAdBase appLovinAdBase, Context context, AppLovinAdRewardListener appLovinAdRewardListener, AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener, AppLovinAdDisplayListener appLovinAdDisplayListener, AppLovinAdClickListener appLovinAdClickListener) {
        if (!appLovinAdBase.getType().equals(AppLovinAdType.INCENTIVIZED)) {
            q Z = this.f3892a.Z();
            Z.e("IncentivizedAdController", "Failed to render an ad of type " + appLovinAdBase.getType() + " in an Incentivized Ad interstitial.");
            a(appLovinAdBase, appLovinAdVideoPlaybackListener, appLovinAdDisplayListener);
            return;
        }
        AppLovinAd a2 = p.a((AppLovinAd) appLovinAdBase, this.f3892a);
        if (a2 != null) {
            AppLovinInterstitialAdDialog create = AppLovinInterstitialAd.create(this.f3892a.r(), context);
            c cVar = new c(this, appLovinAdRewardListener, appLovinAdVideoPlaybackListener, appLovinAdDisplayListener, appLovinAdClickListener, null);
            create.setAdDisplayListener(cVar);
            create.setAdVideoPlaybackListener(cVar);
            create.setAdClickListener(cVar);
            create.showAndRender(a2);
            this.f3900i = new SoftReference<>(create);
            if (a2 instanceof com.applovin.impl.sdk.ad.f) {
                a((com.applovin.impl.sdk.ad.f) a2, cVar);
                return;
            }
            return;
        }
        a(appLovinAdBase, appLovinAdVideoPlaybackListener, appLovinAdDisplayListener);
    }

    private void a(com.applovin.impl.sdk.ad.f fVar, AppLovinAdRewardListener appLovinAdRewardListener) {
        this.f3892a.j().a(new f.g(fVar, appLovinAdRewardListener, this.f3892a), f.y.b.REWARD);
    }

    /* access modifiers changed from: private */
    public void a(AppLovinAd appLovinAd) {
        AppLovinAd appLovinAd2 = this.f3894c;
        if (appLovinAd2 != null) {
            if (appLovinAd2 instanceof g) {
                if (appLovinAd != ((g) appLovinAd2).a()) {
                    return;
                }
            } else if (appLovinAd != appLovinAd2) {
                return;
            }
            this.f3894c = null;
        }
    }

    private void a(AppLovinAd appLovinAd, Context context, AppLovinAdRewardListener appLovinAdRewardListener, AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener, AppLovinAdDisplayListener appLovinAdDisplayListener, AppLovinAdClickListener appLovinAdClickListener) {
        if (appLovinAd == null) {
            appLovinAd = this.f3894c;
        }
        AppLovinAdBase appLovinAdBase = (AppLovinAdBase) appLovinAd;
        if (appLovinAdBase != null) {
            a(appLovinAdBase, context, appLovinAdRewardListener, appLovinAdVideoPlaybackListener, appLovinAdDisplayListener, appLovinAdClickListener);
            return;
        }
        q.i("IncentivizedAdController", "Skipping incentivized video playback: user attempted to play an incentivized video before one was preloaded.");
        d();
    }

    private void a(AppLovinAd appLovinAd, AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener, AppLovinAdDisplayListener appLovinAdDisplayListener) {
        this.f3892a.k().a(com.applovin.impl.sdk.d.g.m);
        j.a(appLovinAdVideoPlaybackListener, appLovinAd, FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE, false);
        j.b(appLovinAdDisplayListener, appLovinAd);
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        synchronized (this.f3897f) {
            this.f3898g = str;
        }
    }

    private void b(AppLovinAdLoadListener appLovinAdLoadListener) {
        this.f3893b.loadNextIncentivizedAd(this.f3895d, appLovinAdLoadListener);
    }

    private void d() {
        AppLovinAdLoadListener appLovinAdLoadListener;
        SoftReference<AppLovinAdLoadListener> softReference = this.f3896e;
        if (softReference != null && (appLovinAdLoadListener = softReference.get()) != null) {
            appLovinAdLoadListener.failedToReceiveAd(AppLovinErrorCodes.INCENTIVIZED_NO_AD_PRELOADED);
        }
    }

    /* access modifiers changed from: private */
    public String e() {
        String str;
        synchronized (this.f3897f) {
            str = this.f3898g;
        }
        return str;
    }

    private AppLovinAdRewardListener f() {
        return new C0097a();
    }

    public void a(AppLovinAd appLovinAd, Context context, String str, AppLovinAdRewardListener appLovinAdRewardListener, AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener, AppLovinAdDisplayListener appLovinAdDisplayListener, AppLovinAdClickListener appLovinAdClickListener) {
        if (appLovinAdRewardListener == null) {
            appLovinAdRewardListener = f();
        }
        a(appLovinAd, context, appLovinAdRewardListener, appLovinAdVideoPlaybackListener, appLovinAdDisplayListener, appLovinAdClickListener);
    }

    public void a(AppLovinAdLoadListener appLovinAdLoadListener) {
        this.f3892a.Z().b("IncentivizedAdController", "User requested preload of incentivized ad...");
        this.f3896e = new SoftReference<>(appLovinAdLoadListener);
        if (a()) {
            q.i("IncentivizedAdController", "Attempted to call preloadAndNotify: while an ad was already loaded or currently being played. Do not call preloadAndNotify: again until the last ad has been closed (adHidden).");
            if (appLovinAdLoadListener != null) {
                appLovinAdLoadListener.adReceived(this.f3894c);
                return;
            }
            return;
        }
        b(new b(appLovinAdLoadListener));
    }

    public boolean a() {
        return this.f3894c != null;
    }

    public String b() {
        return this.f3895d;
    }

    public void c() {
        AppLovinInterstitialAdDialog appLovinInterstitialAdDialog;
        SoftReference<AppLovinInterstitialAdDialog> softReference = this.f3900i;
        if (softReference != null && (appLovinInterstitialAdDialog = softReference.get()) != null) {
            appLovinInterstitialAdDialog.dismiss();
        }
    }
}
