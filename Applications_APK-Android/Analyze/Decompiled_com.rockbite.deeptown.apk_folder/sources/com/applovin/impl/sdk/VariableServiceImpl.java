package com.applovin.impl.sdk;

import android.os.Bundle;
import android.text.TextUtils;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.f;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.m;
import com.applovin.sdk.AppLovinVariableService;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

public class VariableServiceImpl implements AppLovinVariableService {

    /* renamed from: a  reason: collision with root package name */
    private final k f3883a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final AtomicBoolean f3884b = new AtomicBoolean();

    /* renamed from: c  reason: collision with root package name */
    private final AtomicBoolean f3885c = new AtomicBoolean();
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public AppLovinVariableService.OnVariablesUpdateListener f3886d;

    /* renamed from: e  reason: collision with root package name */
    private Bundle f3887e;

    /* renamed from: f  reason: collision with root package name */
    private final Object f3888f = new Object();

    class a implements f.w.b {
        a() {
        }

        public void a() {
            VariableServiceImpl.this.f3884b.set(false);
        }
    }

    class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Bundle f3890a;

        b(Bundle bundle) {
            this.f3890a = bundle;
        }

        public void run() {
            VariableServiceImpl.this.f3886d.onVariablesUpdate(this.f3890a);
        }
    }

    VariableServiceImpl(k kVar) {
        this.f3883a = kVar;
        String str = (String) kVar.a(c.g.f4022j);
        if (m.b(str)) {
            updateVariables(i.a(str, kVar));
        }
    }

    private Object a(String str, Object obj, Class<?> cls) {
        if (TextUtils.isEmpty(str)) {
            q.i("AppLovinVariableService", "Unable to retrieve variable value for empty name");
            return obj;
        }
        if (!this.f3883a.H()) {
            q.h("AppLovinSdk", "Attempted to retrieve variable before SDK initialization. Please wait until after the SDK has initialized, e.g. AppLovinSdk.initializeSdk(Context, SdkInitializationListener).");
        }
        synchronized (this.f3888f) {
            if (this.f3887e == null) {
                q.i("AppLovinVariableService", "Unable to retrieve variable value for name \"" + str + "\", none retrieved from server yet. Please set a listener to be notified when values are retrieved from the server.");
                return obj;
            } else if (cls.equals(String.class)) {
                String string = this.f3887e.getString(str, (String) obj);
                return string;
            } else if (cls.equals(Boolean.class)) {
                Boolean valueOf = Boolean.valueOf(this.f3887e.getBoolean(str, ((Boolean) obj).booleanValue()));
                return valueOf;
            } else {
                throw new IllegalStateException("Unable to retrieve variable value for " + str);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0020, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a() {
        /*
            r4 = this;
            java.lang.Object r0 = r4.f3888f
            monitor-enter(r0)
            com.applovin.sdk.AppLovinVariableService$OnVariablesUpdateListener r1 = r4.f3886d     // Catch:{ all -> 0x0021 }
            if (r1 == 0) goto L_0x001f
            android.os.Bundle r1 = r4.f3887e     // Catch:{ all -> 0x0021 }
            if (r1 != 0) goto L_0x000c
            goto L_0x001f
        L_0x000c:
            android.os.Bundle r1 = r4.f3887e     // Catch:{ all -> 0x0021 }
            java.lang.Object r1 = r1.clone()     // Catch:{ all -> 0x0021 }
            android.os.Bundle r1 = (android.os.Bundle) r1     // Catch:{ all -> 0x0021 }
            r2 = 1
            com.applovin.impl.sdk.VariableServiceImpl$b r3 = new com.applovin.impl.sdk.VariableServiceImpl$b     // Catch:{ all -> 0x0021 }
            r3.<init>(r1)     // Catch:{ all -> 0x0021 }
            com.applovin.sdk.AppLovinSdkUtils.runOnUiThread(r2, r3)     // Catch:{ all -> 0x0021 }
            monitor-exit(r0)     // Catch:{ all -> 0x0021 }
            return
        L_0x001f:
            monitor-exit(r0)     // Catch:{ all -> 0x0021 }
            return
        L_0x0021:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0021 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.VariableServiceImpl.a():void");
    }

    public boolean getBoolean(String str) {
        return getBoolean(str, false);
    }

    public boolean getBoolean(String str, boolean z) {
        return ((Boolean) a(str, Boolean.valueOf(z), Boolean.class)).booleanValue();
    }

    public String getString(String str) {
        return getString(str, null);
    }

    public String getString(String str, String str2) {
        return (String) a(str, str2, String.class);
    }

    public void loadVariables() {
        String str;
        if (!this.f3883a.H()) {
            str = "The AppLovin SDK is waiting for the initial variables to be returned upon completing initialization.";
        } else if (this.f3884b.compareAndSet(false, true)) {
            this.f3883a.j().a(new f.w(this.f3883a, new a()), f.y.b.BACKGROUND);
            return;
        } else {
            str = "Ignored explicit variables load. Service is already in the process of retrieving the latest set of variables.";
        }
        q.i("AppLovinVariableService", str);
    }

    public void setOnVariablesUpdateListener(AppLovinVariableService.OnVariablesUpdateListener onVariablesUpdateListener) {
        this.f3886d = onVariablesUpdateListener;
        synchronized (this.f3888f) {
            if (onVariablesUpdateListener != null) {
                if (this.f3887e != null && this.f3885c.compareAndSet(false, true)) {
                    this.f3883a.Z().b("AppLovinVariableService", "Setting initial listener");
                    a();
                }
            }
        }
    }

    public String toString() {
        return "VariableService{variables=" + this.f3887e + ", listener=" + this.f3886d + '}';
    }

    public void updateVariables(JSONObject jSONObject) {
        q Z = this.f3883a.Z();
        Z.b("AppLovinVariableService", "Updating variables: " + jSONObject + "...");
        synchronized (this.f3888f) {
            this.f3887e = i.c(jSONObject);
            a();
            this.f3883a.a(c.g.f4022j, jSONObject.toString());
        }
    }
}
