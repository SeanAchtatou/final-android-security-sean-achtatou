package com.applovin.impl.sdk.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.ad.g;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.d.a;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.n;
import com.applovin.impl.sdk.q;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinSdk;
import com.facebook.appevents.AppEventsConstants;
import com.google.android.gms.drive.DriveFile;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

public abstract class p {

    /* renamed from: a  reason: collision with root package name */
    private static ApplicationInfo f4487a;

    /* renamed from: b  reason: collision with root package name */
    private static Boolean f4488b;

    public static double a(long j2) {
        double d2 = (double) j2;
        Double.isNaN(d2);
        return d2 / 1000.0d;
    }

    public static float a(float f2) {
        return f2 * 1000.0f;
    }

    public static int a(JSONObject jSONObject) {
        int b2 = i.b(jSONObject, "video_completion_percent", -1, (k) null);
        if (b2 < 0 || b2 > 100) {
            return 95;
        }
        return b2;
    }

    public static long a(k kVar) {
        long longValue = ((Long) kVar.a(c.e.I3)).longValue();
        long longValue2 = ((Long) kVar.a(c.e.J3)).longValue();
        long currentTimeMillis = System.currentTimeMillis();
        return (longValue <= 0 || longValue2 <= 0) ? currentTimeMillis : currentTimeMillis + (longValue - longValue2);
    }

    public static Activity a(View view, k kVar) {
        if (view == null) {
            return null;
        }
        int i2 = 0;
        while (i2 < 1000) {
            i2++;
            try {
                Context context = view.getContext();
                if (context instanceof Activity) {
                    return (Activity) context;
                }
                ViewParent parent = view.getParent();
                if (!(parent instanceof View)) {
                    return null;
                }
                view = (View) parent;
            } catch (Throwable th) {
                kVar.Z().b("Utils", "Encountered error while retrieving activity from view", th);
            }
        }
        return null;
    }

    public static Bitmap a(Context context, int i2, int i3) {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            int i4 = 1;
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeResource(context.getResources(), i2);
            if (options.outHeight > i3 || options.outWidth > i3) {
                double d2 = (double) i3;
                double max = (double) Math.max(options.outHeight, options.outWidth);
                Double.isNaN(d2);
                Double.isNaN(max);
                i4 = (int) Math.pow(2.0d, (double) ((int) Math.ceil(Math.log(d2 / max) / Math.log(0.5d))));
            }
            new BitmapFactory.Options().inSampleSize = i4;
            return BitmapFactory.decodeResource(context.getResources(), i2);
        } catch (Exception unused) {
            return null;
        } finally {
            a((Closeable) null, (k) null);
            a((Closeable) null, (k) null);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.utils.p.a(java.io.Closeable, com.applovin.impl.sdk.k):void
     arg types: [java.io.FileInputStream, ?[OBJECT, ARRAY]]
     candidates:
      com.applovin.impl.sdk.utils.p.a(android.view.View, com.applovin.impl.sdk.k):android.app.Activity
      com.applovin.impl.sdk.utils.p.a(java.io.File, int):android.graphics.Bitmap
      com.applovin.impl.sdk.utils.p.a(android.content.Context, android.view.View):android.view.View
      com.applovin.impl.sdk.utils.p.a(org.json.JSONObject, com.applovin.impl.sdk.k):com.applovin.impl.sdk.ad.d
      com.applovin.impl.sdk.utils.p.a(com.applovin.sdk.AppLovinAd, com.applovin.impl.sdk.k):com.applovin.sdk.AppLovinAd
      com.applovin.impl.sdk.utils.p.a(java.lang.Object, com.applovin.impl.sdk.k):java.lang.Object
      com.applovin.impl.sdk.utils.p.a(java.lang.Class, java.lang.String):java.lang.reflect.Field
      com.applovin.impl.sdk.utils.p.a(java.util.List<java.lang.String>, com.applovin.impl.sdk.k):java.util.List<java.lang.Class>
      com.applovin.impl.sdk.utils.p.a(java.net.HttpURLConnection, com.applovin.impl.sdk.k):void
      com.applovin.impl.sdk.utils.p.a(long, long):boolean
      com.applovin.impl.sdk.utils.p.a(android.view.View, android.app.Activity):boolean
      com.applovin.impl.sdk.utils.p.a(android.view.View, android.view.View):boolean
      com.applovin.impl.sdk.utils.p.a(java.lang.String, java.util.List<java.lang.String>):boolean
      com.applovin.impl.sdk.utils.p.a(java.io.Closeable, com.applovin.impl.sdk.k):void */
    public static Bitmap a(File file, int i2) {
        FileInputStream fileInputStream;
        FileInputStream fileInputStream2;
        BitmapFactory.Options options;
        try {
            BitmapFactory.Options options2 = new BitmapFactory.Options();
            int i3 = 1;
            options2.inJustDecodeBounds = true;
            fileInputStream = new FileInputStream(file);
            try {
                BitmapFactory.decodeStream(fileInputStream, null, options2);
                fileInputStream.close();
                if (options2.outHeight > i2 || options2.outWidth > i2) {
                    double d2 = (double) i2;
                    double max = (double) Math.max(options2.outHeight, options2.outWidth);
                    Double.isNaN(d2);
                    Double.isNaN(max);
                    i3 = (int) Math.pow(2.0d, (double) ((int) Math.ceil(Math.log(d2 / max) / Math.log(0.5d))));
                }
                options = new BitmapFactory.Options();
                options.inSampleSize = i3;
                fileInputStream2 = new FileInputStream(file);
            } catch (Exception unused) {
                fileInputStream2 = null;
                a((Closeable) fileInputStream, (k) null);
                a((Closeable) fileInputStream2, (k) null);
                return null;
            } catch (Throwable th) {
                th = th;
                fileInputStream2 = null;
                a((Closeable) fileInputStream, (k) null);
                a((Closeable) fileInputStream2, (k) null);
                throw th;
            }
            try {
                Bitmap decodeStream = BitmapFactory.decodeStream(fileInputStream2, null, options);
                fileInputStream2.close();
                a((Closeable) fileInputStream, (k) null);
                a((Closeable) fileInputStream2, (k) null);
                return decodeStream;
            } catch (Exception unused2) {
                a((Closeable) fileInputStream, (k) null);
                a((Closeable) fileInputStream2, (k) null);
                return null;
            } catch (Throwable th2) {
                th = th2;
                a((Closeable) fileInputStream, (k) null);
                a((Closeable) fileInputStream2, (k) null);
                throw th;
            }
        } catch (Exception unused3) {
            fileInputStream2 = null;
            fileInputStream = null;
            a((Closeable) fileInputStream, (k) null);
            a((Closeable) fileInputStream2, (k) null);
            return null;
        } catch (Throwable th3) {
            th = th3;
            fileInputStream2 = null;
            fileInputStream = null;
            a((Closeable) fileInputStream, (k) null);
            a((Closeable) fileInputStream2, (k) null);
            throw th;
        }
    }

    public static View a(Context context, View view) {
        View e2 = e(context);
        return e2 != null ? e2 : a(view);
    }

    public static View a(View view) {
        View rootView;
        if (view == null || (rootView = view.getRootView()) == null) {
            return null;
        }
        View findViewById = rootView.findViewById(16908290);
        return findViewById != null ? findViewById : rootView;
    }

    public static d a(JSONObject jSONObject, k kVar) {
        return d.a(AppLovinAdSize.fromString(i.b(jSONObject, "ad_size", (String) null, kVar)), AppLovinAdType.fromString(i.b(jSONObject, AppEventsConstants.EVENT_PARAM_AD_TYPE, (String) null, kVar)), i.b(jSONObject, "zone_id", (String) null, kVar), kVar);
    }

    public static k a(AppLovinSdk appLovinSdk) {
        try {
            Field declaredField = appLovinSdk.getClass().getDeclaredField("mSdkImpl");
            declaredField.setAccessible(true);
            return (k) declaredField.get(appLovinSdk);
        } catch (Throwable th) {
            throw new IllegalStateException("Internal error - unable to retrieve SDK implementation: " + th);
        }
    }

    public static AppLovinAd a(AppLovinAd appLovinAd, k kVar) {
        if (!(appLovinAd instanceof g)) {
            return appLovinAd;
        }
        g gVar = (g) appLovinAd;
        AppLovinAd dequeueAd = kVar.S().dequeueAd(gVar.getAdZone());
        q Z = kVar.Z();
        Z.b("Utils", "Dequeued ad for dummy ad: " + dequeueAd);
        if (dequeueAd == null) {
            return gVar.a();
        }
        gVar.a(dequeueAd);
        ((AppLovinAdBase) dequeueAd).setDummyAd(gVar);
        return dequeueAd;
    }

    public static Object a(Object obj, k kVar) {
        int i2;
        if (obj instanceof Map) {
            Map map = (Map) obj;
            HashMap hashMap = new HashMap(map.size());
            for (Map.Entry entry : map.entrySet()) {
                Object key = entry.getKey();
                hashMap.put(key instanceof String ? (String) key : String.valueOf(key), a(entry.getValue(), kVar));
            }
            return hashMap;
        } else if (obj instanceof List) {
            List<Object> list = (List) obj;
            ArrayList arrayList = new ArrayList(list.size());
            for (Object a2 : list) {
                arrayList.add(a(a2, kVar));
            }
            return arrayList;
        } else if (obj instanceof Date) {
            return String.valueOf(((Date) obj).getTime());
        } else {
            String valueOf = String.valueOf(obj);
            if (obj instanceof String) {
                i2 = ((Integer) kVar.a(c.e.j0)).intValue();
                if (i2 <= 0 || valueOf.length() <= i2) {
                    return valueOf;
                }
            } else if (!(obj instanceof Uri) || (i2 = ((Integer) kVar.a(c.e.k0)).intValue()) <= 0 || valueOf.length() <= i2) {
                return valueOf;
            }
            return valueOf.substring(0, i2);
        }
    }

    public static String a(Context context) {
        Bundle h2 = h(context);
        if (h2 == null) {
            return null;
        }
        String string = h2.getString("applovin.sdk.key");
        return string != null ? string : "";
    }

    public static String a(String str) {
        return (str == null || str.length() <= 4) ? "NOKEY" : str.substring(str.length() - 4);
    }

    public static String a(Map<String, String> map) {
        if (map == null || map.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (Map.Entry next : map.entrySet()) {
            if (sb.length() > 0) {
                sb.append("&");
            }
            sb.append(next.getKey());
            sb.append('=');
            sb.append(next.getValue());
        }
        return sb.toString();
    }

    public static Field a(Class cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException unused) {
            Class superclass = cls.getSuperclass();
            if (superclass == null) {
                return null;
            }
            return a(superclass, str);
        }
    }

    public static List<a> a(String str, JSONObject jSONObject, String str2, String str3, k kVar) {
        return a(str, jSONObject, str2, null, str3, kVar);
    }

    public static List<a> a(String str, JSONObject jSONObject, String str2, String str3, String str4, k kVar) {
        HashMap hashMap = new HashMap(2);
        hashMap.put("{CLCODE}", str2);
        if (str3 == null) {
            str3 = "";
        }
        hashMap.put("{EVENT_ID}", str3);
        return a(str, jSONObject, hashMap, str4, kVar);
    }

    public static List<a> a(String str, JSONObject jSONObject, Map<String, String> map, String str2, k kVar) {
        JSONObject b2 = i.b(jSONObject, str, new JSONObject(), kVar);
        ArrayList arrayList = new ArrayList(b2.length() + 1);
        if (m.b(str2)) {
            arrayList.add(new a(str2, null));
        }
        if (b2.length() > 0) {
            Iterator<String> keys = b2.keys();
            while (keys.hasNext()) {
                try {
                    String next = keys.next();
                    if (!TextUtils.isEmpty(next)) {
                        String optString = b2.optString(next);
                        String a2 = m.a(next, map);
                        if (!TextUtils.isEmpty(optString)) {
                            optString = m.a(optString, map);
                        }
                        arrayList.add(new a(a2, optString));
                    }
                } catch (Throwable th) {
                    kVar.Z().b("Utils", "Failed to create and add postback url.", th);
                }
            }
        }
        return arrayList;
    }

    private static List<Class> a(List<String> list, k kVar) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (String next : list) {
            try {
                arrayList.add(Class.forName(next));
            } catch (ClassNotFoundException unused) {
                q Z = kVar.Z();
                Z.e("Utils", "Failed to create class for name: " + next);
            }
        }
        return arrayList;
    }

    public static void a(AppLovinAdLoadListener appLovinAdLoadListener, d dVar, int i2, k kVar) {
        if (appLovinAdLoadListener != null) {
            try {
                if (appLovinAdLoadListener instanceof n) {
                    ((n) appLovinAdLoadListener).a(dVar, i2);
                } else {
                    appLovinAdLoadListener.failedToReceiveAd(i2);
                }
            } catch (Throwable th) {
                kVar.Z().b("Utils", "Unable process a failure to receive an ad", th);
            }
        }
    }

    public static void a(Closeable closeable, k kVar) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Throwable th) {
                if (kVar != null) {
                    q Z = kVar.Z();
                    Z.b("Utils", "Unable to close stream: " + closeable, th);
                }
            }
        }
    }

    public static void a(String str, Boolean bool, Map<String, String> map) {
        if (bool.booleanValue()) {
            map.put(str, Boolean.toString(true));
        }
    }

    public static void a(String str, String str2, Map<String, String> map) {
        if (!TextUtils.isEmpty(str2)) {
            map.put(str, str2);
        }
    }

    public static void a(String str, JSONObject jSONObject, k kVar) {
        if (jSONObject.has("no_fill_reason")) {
            Object a2 = i.a(jSONObject, "no_fill_reason", new Object(), kVar);
            q.i("AppLovinSdk", "\n**************************************************\nNO FILL received:\n..ID: \"" + str + "\"\n..SDK KEY: \"" + kVar.X() + "\"\n..Reason: " + a2 + "\n**************************************************\n");
        }
    }

    public static void a(HttpURLConnection httpURLConnection, k kVar) {
        if (httpURLConnection != null) {
            try {
                httpURLConnection.disconnect();
            } catch (Throwable th) {
                if (kVar != null) {
                    q Z = kVar.Z();
                    Z.b("Utils", "Unable to disconnect connection: " + httpURLConnection, th);
                }
            }
        }
    }

    public static boolean a() {
        Bundle h2;
        Context e0 = k.e0();
        return (e0 == null || (h2 = h(e0)) == null || !h2.containsKey("applovin.sdk.verbose_logging")) ? false : true;
    }

    public static boolean a(long j2, long j3) {
        return (j2 & j3) != 0;
    }

    public static boolean a(Context context, Uri uri, k kVar) {
        boolean z;
        try {
            Intent intent = new Intent("android.intent.action.VIEW", uri);
            if (!(context instanceof Activity)) {
                intent.setFlags(DriveFile.MODE_READ_ONLY);
            }
            kVar.x().b();
            context.startActivity(intent);
            z = true;
        } catch (Throwable th) {
            q Z = kVar.Z();
            Z.b("Utils", "Unable to open \"" + uri + "\".", th);
            z = false;
        }
        if (!z) {
            kVar.x().c();
        }
        return z;
    }

    public static boolean a(View view, Activity activity) {
        View rootView;
        if (!(activity == null || view == null)) {
            Window window = activity.getWindow();
            if (window != null) {
                rootView = window.getDecorView();
            } else {
                View findViewById = activity.findViewById(16908290);
                if (findViewById != null) {
                    rootView = findViewById.getRootView();
                }
            }
            return a(view, rootView);
        }
        return false;
    }

    public static boolean a(View view, View view2) {
        if (view == view2) {
            return true;
        }
        if (view2 instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view2;
            for (int i2 = 0; i2 < viewGroup.getChildCount(); i2++) {
                if (a(view, viewGroup.getChildAt(i2))) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean a(Object obj, List<String> list, k kVar) {
        if (list == null) {
            return false;
        }
        for (Class isInstance : a(list, kVar)) {
            if (isInstance.isInstance(obj)) {
                if (obj instanceof Map) {
                    for (Map.Entry entry : ((Map) obj).entrySet()) {
                        if (!(entry.getKey() instanceof String)) {
                            kVar.Z().b("Utils", "Invalid key type used. Map keys should be of type String.");
                            return false;
                        } else if (!a(entry.getValue(), list, kVar)) {
                            return false;
                        }
                    }
                    return true;
                } else if (!(obj instanceof List)) {
                    return true;
                } else {
                    for (Object a2 : (List) obj) {
                        if (!a(a2, list, kVar)) {
                            return false;
                        }
                    }
                    return true;
                }
            }
        }
        q Z = kVar.Z();
        Z.b("Utils", "Object '" + obj + "' does not match any of the required types '" + list + "'.");
        return false;
    }

    public static boolean a(String str, List<String> list) {
        for (String startsWith : list) {
            if (str.startsWith(startsWith)) {
                return true;
            }
        }
        return false;
    }

    public static long b(float f2) {
        return c(a(f2));
    }

    public static String b(Class cls, String str) {
        try {
            Field a2 = a(cls, str);
            a2.setAccessible(true);
            return (String) a2.get(null);
        } catch (Throwable unused) {
            return null;
        }
    }

    public static String b(String str) {
        return str.replace("{PLACEMENT}", "");
    }

    public static Map<String, String> b(Map<String, String> map) {
        HashMap hashMap = new HashMap(map);
        for (String str : hashMap.keySet()) {
            String str2 = (String) hashMap.get(str);
            if (str2 != null) {
                hashMap.put(str, m.d(str2));
            }
        }
        return hashMap;
    }

    public static void b(AppLovinAd appLovinAd, k kVar) {
        if (appLovinAd instanceof AppLovinAdBase) {
            String X = kVar.X();
            String X2 = ((AppLovinAdBase) appLovinAd).getSdk().X();
            if (!X.equals(X2)) {
                q.i("AppLovinAd", "Ad was loaded from sdk with key: " + X2 + ", but is being rendered from sdk with key: " + X);
                kVar.k().a(com.applovin.impl.sdk.d.g.o);
            }
        }
    }

    public static boolean b() {
        return Looper.myLooper() == Looper.getMainLooper();
    }

    public static boolean b(Context context) {
        Bundle h2;
        if (context == null) {
            context = k.e0();
        }
        return (context == null || (h2 = h(context)) == null || !h2.getBoolean("applovin.sdk.test_ads", false)) ? false : true;
    }

    public static boolean b(k kVar) {
        if (f4488b == null) {
            try {
                Context e0 = k.e0();
                f4488b = Boolean.valueOf(Class.forName(e0.getPackageName() + ".BuildConfig").getField("DEBUG").getBoolean(null));
            } catch (Throwable th) {
                kVar.Z().b("Utils", "Failed to retrieve BuildConfig.DEBUG", th);
                f4488b = false;
            }
        }
        return f4488b.booleanValue();
    }

    private static long c(float f2) {
        return (long) Math.round(f2);
    }

    public static MaxAdFormat c(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (str.equalsIgnoreCase("banner")) {
            return MaxAdFormat.BANNER;
        }
        if (str.equalsIgnoreCase("mrec")) {
            return MaxAdFormat.MREC;
        }
        if (str.equalsIgnoreCase("leaderboard") || str.equalsIgnoreCase("leader")) {
            return MaxAdFormat.LEADER;
        }
        if (str.equalsIgnoreCase("interstitial") || str.equalsIgnoreCase("inter")) {
            return MaxAdFormat.INTERSTITIAL;
        }
        if (str.equalsIgnoreCase("rewarded") || str.equalsIgnoreCase("reward")) {
            return MaxAdFormat.REWARDED;
        }
        throw new IllegalArgumentException("Unknown format: " + str);
    }

    public static boolean c() {
        ActivityManager.RunningAppProcessInfo runningAppProcessInfo = new ActivityManager.RunningAppProcessInfo();
        ActivityManager.getMyMemoryState(runningAppProcessInfo);
        int i2 = runningAppProcessInfo.importance;
        return i2 == 100 || i2 == 200;
    }

    public static boolean c(Context context) {
        Bundle h2;
        if (context == null) {
            context = k.e0();
        }
        return (context == null || (h2 = h(context)) == null || !h2.getBoolean("applovin.sdk.verbose_logging", false)) ? false : true;
    }

    public static int d(Context context) {
        Resources resources;
        Configuration configuration;
        if (context == null || (resources = context.getResources()) == null || (configuration = resources.getConfiguration()) == null) {
            return 0;
        }
        return configuration.orientation;
    }

    public static String d(String str) {
        Uri parse = Uri.parse(str);
        return new Uri.Builder().scheme(parse.getScheme()).authority(parse.getAuthority()).path(parse.getPath()).build().toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:4:0x000a A[Catch:{ all -> 0x002e }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean d() {
        /*
            java.util.Enumeration r0 = java.net.NetworkInterface.getNetworkInterfaces()     // Catch:{ all -> 0x002e }
        L_0x0004:
            boolean r1 = r0.hasMoreElements()     // Catch:{ all -> 0x002e }
            if (r1 == 0) goto L_0x0036
            java.lang.Object r1 = r0.nextElement()     // Catch:{ all -> 0x002e }
            java.net.NetworkInterface r1 = (java.net.NetworkInterface) r1     // Catch:{ all -> 0x002e }
            java.lang.String r1 = r1.getDisplayName()     // Catch:{ all -> 0x002e }
            java.lang.String r2 = "tun"
            boolean r2 = r1.contains(r2)     // Catch:{ all -> 0x002e }
            if (r2 != 0) goto L_0x002c
            java.lang.String r2 = "ppp"
            boolean r2 = r1.contains(r2)     // Catch:{ all -> 0x002e }
            if (r2 != 0) goto L_0x002c
            java.lang.String r2 = "ipsec"
            boolean r1 = r1.contains(r2)     // Catch:{ all -> 0x002e }
            if (r1 == 0) goto L_0x0004
        L_0x002c:
            r0 = 1
            return r0
        L_0x002e:
            r0 = move-exception
            java.lang.String r1 = "Utils"
            java.lang.String r2 = "Unable to check Network Interfaces"
            com.applovin.impl.sdk.q.c(r1, r2, r0)
        L_0x0036:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.utils.p.d():boolean");
    }

    public static View e(Context context) {
        if (!(context instanceof Activity)) {
            return null;
        }
        return ((Activity) context).getWindow().getDecorView().findViewById(16908290);
    }

    public static boolean e(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        try {
            Class.forName(str);
            return true;
        } catch (Throwable unused) {
            return false;
        }
    }

    public static long f(String str) {
        if (!m.b(str)) {
            return Long.MAX_VALUE;
        }
        try {
            return (long) Color.parseColor(str);
        } catch (Throwable unused) {
            return Long.MAX_VALUE;
        }
    }

    public static String f(Context context) {
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.setPackage(context.getPackageName());
        List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 0);
        if (!queryIntentActivities.isEmpty()) {
            return queryIntentActivities.get(0).activityInfo.name;
        }
        return null;
    }

    public static int g(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService("window");
        if (windowManager == null) {
            return 0;
        }
        return windowManager.getDefaultDisplay().getRotation();
    }

    public static int g(String str) {
        int i2 = 0;
        for (String str2 : str.split("\\.")) {
            if (str2.length() > 2) {
                q.i("Utils", "Version number components cannot be longer than two digits -> " + str);
                return i2;
            }
            i2 = (i2 * 100) + Integer.valueOf(str2).intValue();
        }
        return i2;
    }

    private static Bundle h(Context context) {
        ApplicationInfo applicationInfo = f4487a;
        if (applicationInfo != null) {
            return applicationInfo.metaData;
        }
        try {
            f4487a = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            return f4487a.metaData;
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }
}
