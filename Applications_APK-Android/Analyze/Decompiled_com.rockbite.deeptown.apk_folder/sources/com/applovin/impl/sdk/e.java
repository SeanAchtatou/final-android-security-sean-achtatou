package com.applovin.impl.sdk;

import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.ad.j;
import com.applovin.impl.sdk.f;
import com.applovin.nativeAds.AppLovinNativeAd;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdLoadListener;
import java.util.List;

public class e extends s {
    e(k kVar) {
        super(kVar);
    }

    /* access modifiers changed from: package-private */
    public d a(j jVar) {
        return ((AppLovinAdBase) jVar).getAdZone();
    }

    /* access modifiers changed from: package-private */
    public f.c a(d dVar) {
        f.t tVar = new f.t(dVar, this, this.f4382a);
        tVar.a(true);
        return tVar;
    }

    public void a() {
        for (d next : d.b(this.f4382a)) {
            if (!next.d()) {
                h(next);
            }
        }
    }

    public void a(d dVar, int i2) {
        c(dVar, i2);
    }

    /* access modifiers changed from: package-private */
    public void a(Object obj, d dVar, int i2) {
        if (obj instanceof n) {
            ((n) obj).a(dVar, i2);
        }
        if (obj instanceof AppLovinAdLoadListener) {
            ((AppLovinAdLoadListener) obj).failedToReceiveAd(i2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Object obj, j jVar) {
        ((AppLovinAdLoadListener) obj).adReceived((AppLovinAd) jVar);
    }

    public void adReceived(AppLovinAd appLovinAd) {
        b((j) appLovinAd);
    }

    public void failedToReceiveAd(int i2) {
    }

    public void onNativeAdsFailedToLoad(int i2) {
    }

    public void onNativeAdsLoaded(List<AppLovinNativeAd> list) {
    }
}
