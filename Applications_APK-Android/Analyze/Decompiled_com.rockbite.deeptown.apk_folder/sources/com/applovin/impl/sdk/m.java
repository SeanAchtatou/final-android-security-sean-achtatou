package com.applovin.impl.sdk;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class m {

    /* renamed from: a  reason: collision with root package name */
    private final String f4304a = UUID.randomUUID().toString();

    /* renamed from: b  reason: collision with root package name */
    private final String f4305b;

    /* renamed from: c  reason: collision with root package name */
    private final Map<String, Object> f4306c;

    /* renamed from: d  reason: collision with root package name */
    private final long f4307d;

    public m(String str, Map<String, String> map, Map<String, Object> map2) {
        this.f4305b = str;
        this.f4306c = new HashMap();
        this.f4306c.putAll(map);
        this.f4306c.put("applovin_sdk_super_properties", map2);
        this.f4307d = System.currentTimeMillis();
    }

    public String a() {
        return this.f4305b;
    }

    public Map<String, Object> b() {
        return this.f4306c;
    }

    public long c() {
        return this.f4307d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || m.class != obj.getClass()) {
            return false;
        }
        m mVar = (m) obj;
        if (this.f4307d != mVar.f4307d) {
            return false;
        }
        String str = this.f4305b;
        if (str == null ? mVar.f4305b != null : !str.equals(mVar.f4305b)) {
            return false;
        }
        Map<String, Object> map = this.f4306c;
        if (map == null ? mVar.f4306c != null : !map.equals(mVar.f4306c)) {
            return false;
        }
        String str2 = this.f4304a;
        String str3 = mVar.f4304a;
        if (str2 != null) {
            return str2.equals(str3);
        }
        if (str3 == null) {
            return true;
        }
    }

    public int hashCode() {
        String str = this.f4305b;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Map<String, Object> map = this.f4306c;
        int hashCode2 = map != null ? map.hashCode() : 0;
        long j2 = this.f4307d;
        int i3 = (((hashCode + hashCode2) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31;
        String str2 = this.f4304a;
        if (str2 != null) {
            i2 = str2.hashCode();
        }
        return i3 + i2;
    }

    public String toString() {
        return "Event{name='" + this.f4305b + '\'' + ", id='" + this.f4304a + '\'' + ", creationTimestampMillis=" + this.f4307d + ", parameters=" + this.f4306c + '}';
    }
}
