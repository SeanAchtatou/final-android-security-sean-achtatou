package com.applovin.impl.sdk.utils;

import com.applovin.impl.sdk.k;
import java.util.Timer;
import java.util.TimerTask;

public class n {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final k f4475a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public Timer f4476b;

    /* renamed from: c  reason: collision with root package name */
    private long f4477c;

    /* renamed from: d  reason: collision with root package name */
    private long f4478d;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public final Runnable f4479e;

    /* renamed from: f  reason: collision with root package name */
    private long f4480f;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public final Object f4481g = new Object();

    class a extends TimerTask {
        a() {
        }

        /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r5 = this;
                r0 = 0
                com.applovin.impl.sdk.utils.n r1 = com.applovin.impl.sdk.utils.n.this     // Catch:{ all -> 0x001b }
                java.lang.Runnable r1 = r1.f4479e     // Catch:{ all -> 0x001b }
                r1.run()     // Catch:{ all -> 0x001b }
                com.applovin.impl.sdk.utils.n r1 = com.applovin.impl.sdk.utils.n.this
                java.lang.Object r1 = r1.f4481g
                monitor-enter(r1)
                com.applovin.impl.sdk.utils.n r2 = com.applovin.impl.sdk.utils.n.this     // Catch:{ all -> 0x0018 }
                java.util.Timer unused = r2.f4476b = r0     // Catch:{ all -> 0x0018 }
                monitor-exit(r1)     // Catch:{ all -> 0x0018 }
                goto L_0x0042
            L_0x0018:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x0018 }
                throw r0
            L_0x001b:
                r1 = move-exception
                com.applovin.impl.sdk.utils.n r2 = com.applovin.impl.sdk.utils.n.this     // Catch:{ all -> 0x0046 }
                com.applovin.impl.sdk.k r2 = r2.f4475a     // Catch:{ all -> 0x0046 }
                if (r2 == 0) goto L_0x0035
                com.applovin.impl.sdk.utils.n r2 = com.applovin.impl.sdk.utils.n.this     // Catch:{ all -> 0x0046 }
                com.applovin.impl.sdk.k r2 = r2.f4475a     // Catch:{ all -> 0x0046 }
                com.applovin.impl.sdk.q r2 = r2.Z()     // Catch:{ all -> 0x0046 }
                java.lang.String r3 = "Timer"
                java.lang.String r4 = "Encountered error while executing timed task"
                r2.b(r3, r4, r1)     // Catch:{ all -> 0x0046 }
            L_0x0035:
                com.applovin.impl.sdk.utils.n r1 = com.applovin.impl.sdk.utils.n.this
                java.lang.Object r1 = r1.f4481g
                monitor-enter(r1)
                com.applovin.impl.sdk.utils.n r2 = com.applovin.impl.sdk.utils.n.this     // Catch:{ all -> 0x0043 }
                java.util.Timer unused = r2.f4476b = r0     // Catch:{ all -> 0x0043 }
                monitor-exit(r1)     // Catch:{ all -> 0x0043 }
            L_0x0042:
                return
            L_0x0043:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x0043 }
                throw r0
            L_0x0046:
                r1 = move-exception
                com.applovin.impl.sdk.utils.n r2 = com.applovin.impl.sdk.utils.n.this
                java.lang.Object r2 = r2.f4481g
                monitor-enter(r2)
                com.applovin.impl.sdk.utils.n r3 = com.applovin.impl.sdk.utils.n.this     // Catch:{ all -> 0x0055 }
                java.util.Timer unused = r3.f4476b = r0     // Catch:{ all -> 0x0055 }
                monitor-exit(r2)     // Catch:{ all -> 0x0055 }
                throw r1
            L_0x0055:
                r0 = move-exception
                monitor-exit(r2)     // Catch:{ all -> 0x0055 }
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.utils.n.a.run():void");
        }
    }

    private n(k kVar, Runnable runnable) {
        this.f4475a = kVar;
        this.f4479e = runnable;
    }

    public static n a(long j2, k kVar, Runnable runnable) {
        if (j2 < 0) {
            throw new IllegalArgumentException("Cannot create a scheduled timer. Invalid fire time passed in: " + j2 + ".");
        } else if (runnable != null) {
            n nVar = new n(kVar, runnable);
            nVar.f4477c = System.currentTimeMillis();
            nVar.f4478d = j2;
            try {
                nVar.f4476b = new Timer();
                nVar.f4476b.schedule(nVar.e(), j2);
            } catch (OutOfMemoryError e2) {
                kVar.Z().b("Timer", "Failed to create timer due to OOM error", e2);
            }
            return nVar;
        } else {
            throw new IllegalArgumentException("Cannot create a scheduled timer. Runnable is null.");
        }
    }

    private TimerTask e() {
        return new a();
    }

    public long a() {
        if (this.f4476b == null) {
            return this.f4478d - this.f4480f;
        }
        return this.f4478d - (System.currentTimeMillis() - this.f4477c);
    }

    public void b() {
        synchronized (this.f4481g) {
            if (this.f4476b != null) {
                try {
                    this.f4476b.cancel();
                    this.f4480f = System.currentTimeMillis() - this.f4477c;
                } catch (Throwable th) {
                    this.f4476b = null;
                    throw th;
                }
                this.f4476b = null;
            }
        }
    }

    public void c() {
        synchronized (this.f4481g) {
            if (this.f4480f > 0) {
                try {
                    this.f4478d -= this.f4480f;
                    if (this.f4478d < 0) {
                        this.f4478d = 0;
                    }
                    this.f4476b = new Timer();
                    this.f4476b.schedule(e(), this.f4478d);
                    this.f4477c = System.currentTimeMillis();
                } catch (Throwable th) {
                    this.f4480f = 0;
                    throw th;
                }
                this.f4480f = 0;
            }
        }
    }

    public void d() {
        synchronized (this.f4481g) {
            if (this.f4476b != null) {
                try {
                    this.f4476b.cancel();
                    this.f4476b = null;
                } catch (Throwable th) {
                    this.f4476b = null;
                    this.f4480f = 0;
                    throw th;
                }
                this.f4480f = 0;
            }
        }
    }
}
