package com.applovin.impl.sdk.ad;

import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PointF;
import android.net.Uri;
import com.applovin.impl.adview.h;
import com.applovin.impl.adview.s;
import com.applovin.impl.adview.w;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.a.c;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.utils.e;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.p;
import com.appsflyer.share.Constants;
import com.esotericsoftware.spine.Animation;
import com.tapjoy.TapjoyConstants;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

public abstract class f extends AppLovinAdBase {

    /* renamed from: e  reason: collision with root package name */
    private final AtomicBoolean f3974e = new AtomicBoolean();

    /* renamed from: f  reason: collision with root package name */
    private final AtomicBoolean f3975f = new AtomicBoolean();

    /* renamed from: g  reason: collision with root package name */
    private final AtomicReference<c> f3976g = new AtomicReference<>();

    /* renamed from: h  reason: collision with root package name */
    private List<com.applovin.impl.sdk.d.a> f3977h;

    /* renamed from: i  reason: collision with root package name */
    private List<com.applovin.impl.sdk.d.a> f3978i;

    /* renamed from: j  reason: collision with root package name */
    private List<com.applovin.impl.sdk.d.a> f3979j;

    public enum a {
        UNSPECIFIED,
        DISMISS,
        DO_NOT_DISMISS
    }

    public enum b {
        DEFAULT,
        ACTIVITY_PORTRAIT,
        ACTIVITY_LANDSCAPE
    }

    public f(JSONObject jSONObject, JSONObject jSONObject2, b bVar, k kVar) {
        super(jSONObject, jSONObject2, bVar, kVar);
    }

    private List<com.applovin.impl.sdk.d.a> a(PointF pointF, boolean z) {
        List<com.applovin.impl.sdk.d.a> a2;
        synchronized (this.adObjectLock) {
            a2 = p.a("click_tracking_urls", this.adObject, c(pointF, z), b(pointF, z), this.sdk);
        }
        return a2;
    }

    private h.a b(boolean z) {
        return z ? h.a.WhiteXOnTransparentGrey : h.a.WhiteXOnOpaqueBlack;
    }

    private String b(PointF pointF, boolean z) {
        String stringFromAdObject = getStringFromAdObject("click_tracking_url", null);
        Map<String, String> c2 = c(pointF, z);
        if (stringFromAdObject != null) {
            return m.a(stringFromAdObject, c2);
        }
        return null;
    }

    private Map<String, String> c(PointF pointF, boolean z) {
        Point a2 = g.a(this.sdk.d());
        HashMap hashMap = new HashMap(5);
        hashMap.put("{CLCODE}", getClCode());
        hashMap.put("{CLICK_X}", String.valueOf(pointF.x));
        hashMap.put("{CLICK_Y}", String.valueOf(pointF.y));
        hashMap.put("{SCREEN_WIDTH}", String.valueOf(a2.x));
        hashMap.put("{SCREEN_HEIGHT}", String.valueOf(a2.y));
        hashMap.put("{IS_VIDEO_CLICK}", String.valueOf(z));
        return hashMap;
    }

    private String v0() {
        String stringFromAdObject = getStringFromAdObject("video_end_url", null);
        if (stringFromAdObject != null) {
            return stringFromAdObject.replace("{CLCODE}", getClCode());
        }
        return null;
    }

    public boolean A() {
        return this.f3975f.get();
    }

    public void B() {
        this.f3975f.set(true);
    }

    public c C() {
        return this.f3976g.getAndSet(null);
    }

    public int D() {
        return getIntFromAdObject("close_button_top_margin", ((Integer) this.sdk.a(c.e.C1)).intValue());
    }

    public int E() {
        return getIntFromAdObject("close_button_horizontal_margin", ((Integer) this.sdk.a(c.e.A1)).intValue());
    }

    public boolean F() {
        return getBooleanFromAdObject("lhs_close_button", (Boolean) this.sdk.a(c.e.z1));
    }

    public boolean G() {
        return getBooleanFromAdObject("lhs_skip_button", (Boolean) this.sdk.a(c.e.S1));
    }

    public boolean H() {
        return getBooleanFromAdObject("stop_video_player_after_poststitial_render", false);
    }

    public boolean I() {
        return getBooleanFromAdObject("unhide_adview_on_render", false);
    }

    public long J() {
        long longFromAdObject = getLongFromAdObject("report_reward_duration", -1);
        if (longFromAdObject >= 0) {
            return TimeUnit.SECONDS.toMillis(longFromAdObject);
        }
        return -1;
    }

    public int K() {
        return getIntFromAdObject("report_reward_percent", -1);
    }

    public boolean L() {
        return getBooleanFromAdObject("report_reward_percent_include_close_delay", true);
    }

    public AtomicBoolean M() {
        return this.f3974e;
    }

    public boolean N() {
        return getBooleanFromAdObject("show_skip_button_on_click", false);
    }

    public List<com.applovin.impl.sdk.d.a> O() {
        List<com.applovin.impl.sdk.d.a> list = this.f3977h;
        if (list != null) {
            return list;
        }
        synchronized (this.adObjectLock) {
            this.f3977h = p.a("video_end_urls", this.adObject, getClCode(), v0(), this.sdk);
        }
        return this.f3977h;
    }

    public List<com.applovin.impl.sdk.d.a> P() {
        List<com.applovin.impl.sdk.d.a> list = this.f3978i;
        if (list != null) {
            return list;
        }
        synchronized (this.adObjectLock) {
            this.f3978i = p.a("ad_closed_urls", this.adObject, getClCode(), (String) null, this.sdk);
        }
        return this.f3978i;
    }

    public List<com.applovin.impl.sdk.d.a> Q() {
        List<com.applovin.impl.sdk.d.a> list = this.f3979j;
        if (list != null) {
            return list;
        }
        synchronized (this.adObjectLock) {
            this.f3979j = p.a("imp_urls", this.adObject, getClCode(), (String) null, this.sdk);
        }
        return this.f3979j;
    }

    public boolean R() {
        return getBooleanFromAdObject("playback_requires_user_action", true);
    }

    public boolean S() {
        return getBooleanFromAdObject("sanitize_webview", false);
    }

    public String T() {
        String stringFromAdObject = getStringFromAdObject("base_url", Constants.URL_PATH_DELIMITER);
        if ("null".equalsIgnoreCase(stringFromAdObject)) {
            return null;
        }
        return stringFromAdObject;
    }

    public boolean U() {
        return getBooleanFromAdObject("web_contents_debugging_enabled", false);
    }

    public w V() {
        JSONObject jsonObjectFromAdObject = getJsonObjectFromAdObject("web_view_settings", null);
        if (jsonObjectFromAdObject != null) {
            return new w(jsonObjectFromAdObject, this.sdk);
        }
        return null;
    }

    public List<String> W() {
        return e.a(getStringFromAdObject("wls", ""));
    }

    public List<String> X() {
        return e.a(getStringFromAdObject("wlh", null));
    }

    public boolean Y() {
        return getBooleanFromAdObject("tvv", false);
    }

    public boolean Z() {
        return getBooleanFromAdObject("ibbdfs", false);
    }

    public int a() {
        return getIntFromAdObject("countdown_length", 0);
    }

    /* access modifiers changed from: protected */
    public h.a a(int i2) {
        return i2 == 1 ? h.a.WhiteXOnTransparentGrey : i2 == 2 ? h.a.Invisible : h.a.WhiteXOnOpaqueBlack;
    }

    public List<com.applovin.impl.sdk.d.a> a(PointF pointF) {
        return a(pointF, false);
    }

    public void a(Uri uri) {
        try {
            synchronized (this.adObjectLock) {
                this.adObject.put("mute_image", uri);
            }
        } catch (Throwable unused) {
        }
    }

    public void a(com.applovin.impl.sdk.a.c cVar) {
        this.f3976g.set(cVar);
    }

    public void a(boolean z) {
        try {
            synchronized (this.adObjectLock) {
                this.adObject.put("html_resources_cached", z);
            }
        } catch (Throwable unused) {
        }
    }

    public boolean a0() {
        return getBooleanFromAdObject("ibbdfc", false);
    }

    public int b() {
        int parseColor = Color.parseColor("#C8FFFFFF");
        String stringFromAdObject = getStringFromAdObject("countdown_color", null);
        if (!m.b(stringFromAdObject)) {
            return parseColor;
        }
        try {
            return Color.parseColor(stringFromAdObject);
        } catch (Throwable th) {
            this.sdk.Z().b("DirectAd", "Unable to parse countdown color", th);
            return parseColor;
        }
    }

    public List<com.applovin.impl.sdk.d.a> b(PointF pointF) {
        List<com.applovin.impl.sdk.d.a> a2;
        synchronized (this.adObjectLock) {
            a2 = p.a("video_click_tracking_urls", this.adObject, c(pointF, true), (String) null, this.sdk);
        }
        return a2.isEmpty() ? a(pointF, true) : a2;
    }

    public void b(Uri uri) {
        try {
            synchronized (this.adObjectLock) {
                this.adObject.put("unmute_image", uri);
            }
        } catch (Throwable unused) {
        }
    }

    public Uri b0() {
        String stringFromAdObject = getStringFromAdObject("mute_image", null);
        if (!m.b(stringFromAdObject)) {
            return null;
        }
        try {
            return Uri.parse(stringFromAdObject);
        } catch (Throwable unused) {
            return null;
        }
    }

    public int c() {
        String stringFromAdObject = getStringFromAdObject("video_background_color", null);
        if (m.b(stringFromAdObject)) {
            try {
                return Color.parseColor(stringFromAdObject);
            } catch (Throwable unused) {
            }
        }
        return -16777216;
    }

    public Uri c0() {
        String stringFromAdObject = getStringFromAdObject("unmute_image", "");
        if (m.b(stringFromAdObject)) {
            try {
                return Uri.parse(stringFromAdObject);
            } catch (Throwable unused) {
            }
        }
        return null;
    }

    public int d() {
        int i2 = hasVideoUrl() ? -16777216 : -1157627904;
        String stringFromAdObject = getStringFromAdObject("graphic_background_color", null);
        if (!m.b(stringFromAdObject)) {
            return i2;
        }
        try {
            return Color.parseColor(stringFromAdObject);
        } catch (Throwable unused) {
            return i2;
        }
    }

    public boolean d0() {
        this.sdk.Z().e("DirectAd", "Attempting to invoke isVideoStream() from base ad class");
        return false;
    }

    public a e() {
        String stringFromAdObject = getStringFromAdObject("poststitial_dismiss_type", null);
        if (m.b(stringFromAdObject)) {
            if (TapjoyConstants.TJC_FULLSCREEN_AD_DISMISS_URL.equalsIgnoreCase(stringFromAdObject)) {
                return a.DISMISS;
            }
            if ("no_dismiss".equalsIgnoreCase(stringFromAdObject)) {
                return a.DO_NOT_DISMISS;
            }
        }
        return a.UNSPECIFIED;
    }

    public Uri e0() {
        this.sdk.Z().e("DirectAd", "Attempting to invoke getVideoUri() from base ad class");
        return null;
    }

    public List<String> f() {
        String stringFromAdObject = getStringFromAdObject("resource_cache_prefix", null);
        return stringFromAdObject != null ? e.a(stringFromAdObject) : this.sdk.b(c.e.I0);
    }

    public Uri f0() {
        this.sdk.Z().e("DirectAd", "Attempting to invoke getVideoClickDestinationUri() from base ad class");
        return null;
    }

    public String g() {
        return getStringFromAdObject("cache_prefix", null);
    }

    public b g0() {
        String upperCase = getStringFromAdObject("ad_target", b.DEFAULT.toString()).toUpperCase(Locale.ENGLISH);
        return "ACTIVITY_PORTRAIT".equalsIgnoreCase(upperCase) ? b.ACTIVITY_PORTRAIT : "ACTIVITY_LANDSCAPE".equalsIgnoreCase(upperCase) ? b.ACTIVITY_LANDSCAPE : b.DEFAULT;
    }

    public boolean h() {
        return getBooleanFromAdObject("daome", true);
    }

    public float h0() {
        return getFloatFromAdObject("close_delay", Animation.CurveTimeline.LINEAR);
    }

    public boolean i() {
        return getBooleanFromAdObject("utpfc", false);
    }

    public float i0() {
        return getFloatFromAdObject("close_delay_graphic", Animation.CurveTimeline.LINEAR);
    }

    public boolean j() {
        return getBooleanFromAdObject("sscomt", false);
    }

    public h.a j0() {
        int intFromAdObject = getIntFromAdObject("close_style", -1);
        return intFromAdObject == -1 ? b(hasVideoUrl()) : a(intFromAdObject);
    }

    public String k() {
        return getStringFromFullResponse("event_id", null);
    }

    public h.a k0() {
        int intFromAdObject = getIntFromAdObject("skip_style", -1);
        return intFromAdObject == -1 ? j0() : a(intFromAdObject);
    }

    public boolean l() {
        return getBooleanFromAdObject("progress_bar_enabled", false);
    }

    public boolean l0() {
        return getBooleanFromAdObject("dismiss_on_skip", false);
    }

    public int m() {
        String stringFromAdObject = getStringFromAdObject("progress_bar_color", "#C8FFFFFF");
        if (m.b(stringFromAdObject)) {
            try {
                return Color.parseColor(stringFromAdObject);
            } catch (Throwable unused) {
            }
        }
        return 0;
    }

    public boolean m0() {
        return getBooleanFromAdObject("html_resources_cached", false);
    }

    public boolean n() {
        return getBooleanFromAdObject("vs_buffer_indicator_enabled", false);
    }

    public String n0() {
        JSONObject jsonObjectFromAdObject = getJsonObjectFromAdObject("video_button_properties", null);
        return jsonObjectFromAdObject != null ? i.b(jsonObjectFromAdObject, "video_button_html", "", this.sdk) : "";
    }

    public boolean o() {
        return getBooleanFromAdObject("vs_buffer_indicator_initial_load_enabled", false);
    }

    public s o0() {
        return new s(getJsonObjectFromAdObject("video_button_properties", null), this.sdk);
    }

    public int p() {
        return getIntFromAdObject("vs_buffer_indicator_style", 16842874);
    }

    public boolean p0() {
        return getBooleanFromAdObject("video_clickable", false);
    }

    public int q() {
        String stringFromAdObject = getStringFromAdObject("vs_buffer_indicator_color", null);
        if (m.b(stringFromAdObject)) {
            try {
                return Color.parseColor(stringFromAdObject);
            } catch (Throwable unused) {
            }
        }
        return -1;
    }

    public boolean q0() {
        return getBooleanFromAdObject("accelerate_hardware", false);
    }

    public int r() {
        int parseColor = Color.parseColor("#66000000");
        String stringFromAdObject = getStringFromAdObject("vs_buffer_indicator_bg_color", null);
        if (!m.b(stringFromAdObject)) {
            return parseColor;
        }
        try {
            return Color.parseColor(stringFromAdObject);
        } catch (Throwable unused) {
            return parseColor;
        }
    }

    public boolean r0() {
        return getBooleanFromAdObject("keep_screen_on", false);
    }

    public boolean s() {
        return getBooleanFromAdObject("clear_dismissible", false);
    }

    public boolean s0() {
        return getBooleanFromAdObject("hide_close_on_exit_graphic", false);
    }

    public int t() {
        int a2;
        synchronized (this.adObjectLock) {
            a2 = p.a(this.adObject);
        }
        return a2;
    }

    public boolean t0() {
        return getBooleanFromAdObject("hide_close_on_exit", false);
    }

    public int u() {
        return getIntFromAdObject("poststitial_shown_forward_delay_millis", -1);
    }

    public boolean u0() {
        return getBooleanFromAdObject("lock_current_orientation", false);
    }

    public boolean v() {
        return getBooleanFromAdObject("should_apply_mute_setting_to_poststitial", false);
    }

    public boolean w() {
        return getBooleanFromAdObject("should_forward_close_button_tapped_to_poststitial", false);
    }

    public boolean x() {
        return getBooleanFromAdObject("vkuv", false);
    }

    public boolean y() {
        return getBooleanFromAdObject("forward_lifecycle_events_to_webview", false);
    }

    public int z() {
        return getIntFromAdObject("close_button_size", ((Integer) this.sdk.a(c.e.B1)).intValue());
    }
}
