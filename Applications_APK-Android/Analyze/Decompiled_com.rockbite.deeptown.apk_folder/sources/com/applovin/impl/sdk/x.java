package com.applovin.impl.sdk;

import android.content.Context;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.view.View;
import android.view.ViewTreeObserver;
import com.applovin.impl.mediation.b;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.utils.p;
import com.applovin.mediation.ads.MaxAdView;
import com.applovin.sdk.AppLovinSdkUtils;
import java.lang.ref.WeakReference;

public class x {

    /* renamed from: a  reason: collision with root package name */
    private final k f4512a;

    /* renamed from: b  reason: collision with root package name */
    private final Object f4513b = new Object();

    /* renamed from: c  reason: collision with root package name */
    private final Rect f4514c = new Rect();

    /* renamed from: d  reason: collision with root package name */
    private final Handler f4515d;

    /* renamed from: e  reason: collision with root package name */
    private final Runnable f4516e;

    /* renamed from: f  reason: collision with root package name */
    private final ViewTreeObserver.OnPreDrawListener f4517f;

    /* renamed from: g  reason: collision with root package name */
    private WeakReference<ViewTreeObserver> f4518g;
    /* access modifiers changed from: private */

    /* renamed from: h  reason: collision with root package name */
    public View f4519h;

    /* renamed from: i  reason: collision with root package name */
    private int f4520i;

    /* renamed from: j  reason: collision with root package name */
    private long f4521j;

    /* renamed from: k  reason: collision with root package name */
    private long f4522k = Long.MIN_VALUE;

    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ MaxAdView f4523a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ c f4524b;

        a(MaxAdView maxAdView, c cVar) {
            this.f4523a = maxAdView;
            this.f4524b = cVar;
        }

        public void run() {
            if (x.this.f4519h != null) {
                x xVar = x.this;
                if (xVar.b(this.f4523a, xVar.f4519h)) {
                    x.this.a();
                    this.f4524b.onLogVisibilityImpression();
                    return;
                }
                x.this.b();
            }
        }
    }

    class b implements ViewTreeObserver.OnPreDrawListener {
        b() {
        }

        public boolean onPreDraw() {
            x.this.b();
            return true;
        }
    }

    public interface c {
        void onLogVisibilityImpression();
    }

    public x(MaxAdView maxAdView, k kVar, c cVar) {
        this.f4512a = kVar;
        this.f4515d = new Handler(Looper.getMainLooper());
        this.f4516e = new a(maxAdView, cVar);
        this.f4517f = new b();
    }

    private void a(Context context, View view) {
        View a2 = p.a(context, view);
        if (a2 == null) {
            this.f4512a.Z().b("VisibilityTracker", "Unable to set view tree observer due to no root view.");
            return;
        }
        ViewTreeObserver viewTreeObserver = a2.getViewTreeObserver();
        if (!viewTreeObserver.isAlive()) {
            this.f4512a.Z().d("VisibilityTracker", "Unable to set view tree observer since the view tree observer is not alive.");
            return;
        }
        this.f4518g = new WeakReference<>(viewTreeObserver);
        viewTreeObserver.addOnPreDrawListener(this.f4517f);
    }

    private boolean a(View view, View view2) {
        return view2 != null && view2.getVisibility() == 0 && view.getParent() != null && view2.getWidth() > 0 && view2.getHeight() > 0 && view2.getGlobalVisibleRect(this.f4514c) && ((long) (AppLovinSdkUtils.pxToDp(view2.getContext(), this.f4514c.width()) * AppLovinSdkUtils.pxToDp(view2.getContext(), this.f4514c.height()))) >= ((long) this.f4520i);
    }

    /* access modifiers changed from: private */
    public void b() {
        this.f4515d.postDelayed(this.f4516e, ((Long) this.f4512a.a(c.e.k1)).longValue());
    }

    /* access modifiers changed from: private */
    public boolean b(View view, View view2) {
        if (!a(view, view2)) {
            return false;
        }
        if (this.f4522k == Long.MIN_VALUE) {
            this.f4522k = SystemClock.uptimeMillis();
        }
        return SystemClock.uptimeMillis() - this.f4522k >= this.f4521j;
    }

    public void a() {
        synchronized (this.f4513b) {
            this.f4515d.removeMessages(0);
            if (this.f4518g != null) {
                ViewTreeObserver viewTreeObserver = this.f4518g.get();
                if (viewTreeObserver != null && viewTreeObserver.isAlive()) {
                    viewTreeObserver.removeOnPreDrawListener(this.f4517f);
                }
                this.f4518g.clear();
            }
            this.f4522k = Long.MIN_VALUE;
            this.f4519h = null;
        }
    }

    public void a(Context context, b.c cVar) {
        synchronized (this.f4513b) {
            a();
            this.f4519h = cVar.z();
            this.f4520i = cVar.E();
            this.f4521j = cVar.G();
            a(context, this.f4519h);
        }
    }
}
