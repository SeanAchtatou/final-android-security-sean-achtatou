package com.applovin.impl.sdk;

import android.util.Log;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.utils.m;

public class q {

    /* renamed from: a  reason: collision with root package name */
    private final k f4380a;

    q(k kVar) {
        this.f4380a = kVar;
    }

    private boolean a() {
        return this.f4380a.c().d();
    }

    public static void c(String str, String str2, Throwable th) {
        Log.e("AppLovinSdk", "[" + str + "] " + str2, th);
    }

    public static void f(String str, String str2) {
        Log.d("AppLovinSdk", "[" + str + "] " + str2);
    }

    public static void g(String str, String str2) {
        Log.i("AppLovinSdk", "[" + str + "] " + str2);
    }

    public static void h(String str, String str2) {
        Log.w("AppLovinSdk", "[" + str + "] " + str2);
    }

    public static void i(String str, String str2) {
        c(str, str2, null);
    }

    private void j(String str, String str2) {
    }

    public void a(String str, Boolean bool, String str2) {
        a(str, bool, str2, null);
    }

    public void a(String str, Boolean bool, String str2, Throwable th) {
        if (a()) {
            String str3 = "[" + str + "] " + str2;
            Log.e("AppLovinSdk", str3, th);
            j("ERROR", str3 + " : " + th);
        }
        if (bool.booleanValue() && ((Boolean) this.f4380a.a(c.e.t3)).booleanValue() && this.f4380a.o() != null) {
            this.f4380a.o().a(str2, th);
        }
    }

    public void a(String str, String str2) {
        int intValue;
        if (a() && m.b(str2) && (intValue = ((Integer) this.f4380a.a(c.e.t)).intValue()) > 0) {
            int length = str2.length();
            int i2 = ((length + intValue) - 1) / intValue;
            for (int i3 = 0; i3 < i2; i3++) {
                int i4 = i3 * intValue;
                b(str, str2.substring(i4, Math.min(length, i4 + intValue)));
            }
        }
    }

    public void a(String str, String str2, Throwable th) {
        if (a()) {
            String str3 = "[" + str + "] " + str2;
            Log.w("AppLovinSdk", str3, th);
            j("WARN", str3);
        }
    }

    public void b(String str, String str2) {
        if (a()) {
            String str3 = "[" + str + "] " + str2;
            Log.d("AppLovinSdk", str3);
            j("DEBUG", str3);
        }
    }

    public void b(String str, String str2, Throwable th) {
        a(str, true, str2, th);
    }

    public void c(String str, String str2) {
        if (a()) {
            String str3 = "[" + str + "] " + str2;
            Log.i("AppLovinSdk", str3);
            j("INFO", str3);
        }
    }

    public void d(String str, String str2) {
        a(str, str2, (Throwable) null);
    }

    public void e(String str, String str2) {
        b(str, str2, null);
    }
}
