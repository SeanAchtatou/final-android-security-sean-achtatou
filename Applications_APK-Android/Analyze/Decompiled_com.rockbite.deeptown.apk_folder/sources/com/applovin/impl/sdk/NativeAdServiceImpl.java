package com.applovin.impl.sdk;

import android.text.TextUtils;
import com.applovin.impl.sdk.ad.NativeAdImpl;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.f;
import com.applovin.nativeAds.AppLovinNativeAd;
import com.applovin.nativeAds.AppLovinNativeAdLoadListener;
import com.applovin.nativeAds.AppLovinNativeAdPrecacheListener;
import com.applovin.nativeAds.AppLovinNativeAdService;
import com.applovin.sdk.AppLovinErrorCodes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NativeAdServiceImpl implements AppLovinNativeAdService {

    /* renamed from: a  reason: collision with root package name */
    private final k f3866a;

    class a implements AppLovinNativeAdLoadListener {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AppLovinNativeAdLoadListener f3867a;

        a(AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
            this.f3867a = appLovinNativeAdLoadListener;
        }

        public void onNativeAdsFailedToLoad(int i2) {
            NativeAdServiceImpl.this.a(this.f3867a, i2);
        }

        public void onNativeAdsLoaded(List<AppLovinNativeAd> list) {
            NativeAdServiceImpl.this.a(list, this.f3867a);
        }
    }

    class b implements AppLovinNativeAdPrecacheListener {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AppLovinNativeAdPrecacheListener f3869a;

        b(AppLovinNativeAdPrecacheListener appLovinNativeAdPrecacheListener) {
            this.f3869a = appLovinNativeAdPrecacheListener;
        }

        public void onNativeAdImagePrecachingFailed(AppLovinNativeAd appLovinNativeAd, int i2) {
            NativeAdServiceImpl.this.a(this.f3869a, appLovinNativeAd, i2, false);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.applovin.impl.sdk.NativeAdServiceImpl.a(com.applovin.impl.sdk.NativeAdServiceImpl, com.applovin.nativeAds.AppLovinNativeAdPrecacheListener, com.applovin.nativeAds.AppLovinNativeAd, boolean):void
         arg types: [com.applovin.impl.sdk.NativeAdServiceImpl, com.applovin.nativeAds.AppLovinNativeAdPrecacheListener, com.applovin.nativeAds.AppLovinNativeAd, int]
         candidates:
          com.applovin.impl.sdk.NativeAdServiceImpl.a(com.applovin.nativeAds.AppLovinNativeAdPrecacheListener, com.applovin.nativeAds.AppLovinNativeAd, int, boolean):void
          com.applovin.impl.sdk.NativeAdServiceImpl.a(com.applovin.impl.sdk.NativeAdServiceImpl, com.applovin.nativeAds.AppLovinNativeAdPrecacheListener, com.applovin.nativeAds.AppLovinNativeAd, boolean):void */
        public void onNativeAdImagesPrecached(AppLovinNativeAd appLovinNativeAd) {
            NativeAdServiceImpl.this.a(this.f3869a, appLovinNativeAd, false);
            NativeAdServiceImpl.this.a(appLovinNativeAd, this.f3869a);
        }

        public void onNativeAdVideoPrecachingFailed(AppLovinNativeAd appLovinNativeAd, int i2) {
        }

        public void onNativeAdVideoPreceached(AppLovinNativeAd appLovinNativeAd) {
        }
    }

    class c implements AppLovinNativeAdPrecacheListener {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AppLovinNativeAdPrecacheListener f3871a;

        c(AppLovinNativeAdPrecacheListener appLovinNativeAdPrecacheListener) {
            this.f3871a = appLovinNativeAdPrecacheListener;
        }

        public void onNativeAdImagePrecachingFailed(AppLovinNativeAd appLovinNativeAd, int i2) {
        }

        public void onNativeAdImagesPrecached(AppLovinNativeAd appLovinNativeAd) {
        }

        public void onNativeAdVideoPrecachingFailed(AppLovinNativeAd appLovinNativeAd, int i2) {
            NativeAdServiceImpl.this.a(this.f3871a, appLovinNativeAd, i2, true);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.applovin.impl.sdk.NativeAdServiceImpl.a(com.applovin.impl.sdk.NativeAdServiceImpl, com.applovin.nativeAds.AppLovinNativeAdPrecacheListener, com.applovin.nativeAds.AppLovinNativeAd, boolean):void
         arg types: [com.applovin.impl.sdk.NativeAdServiceImpl, com.applovin.nativeAds.AppLovinNativeAdPrecacheListener, com.applovin.nativeAds.AppLovinNativeAd, int]
         candidates:
          com.applovin.impl.sdk.NativeAdServiceImpl.a(com.applovin.nativeAds.AppLovinNativeAdPrecacheListener, com.applovin.nativeAds.AppLovinNativeAd, int, boolean):void
          com.applovin.impl.sdk.NativeAdServiceImpl.a(com.applovin.impl.sdk.NativeAdServiceImpl, com.applovin.nativeAds.AppLovinNativeAdPrecacheListener, com.applovin.nativeAds.AppLovinNativeAd, boolean):void */
        public void onNativeAdVideoPreceached(AppLovinNativeAd appLovinNativeAd) {
            NativeAdServiceImpl.this.a(this.f3871a, appLovinNativeAd, true);
        }
    }

    class d implements AppLovinNativeAdLoadListener {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ List f3873a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ AppLovinNativeAdLoadListener f3874b;

        /* renamed from: c  reason: collision with root package name */
        final /* synthetic */ List f3875c;

        class a implements AppLovinNativeAdLoadListener {
            a() {
            }

            public void onNativeAdsFailedToLoad(int i2) {
                AppLovinNativeAdLoadListener appLovinNativeAdLoadListener = d.this.f3874b;
                if (appLovinNativeAdLoadListener != null) {
                    appLovinNativeAdLoadListener.onNativeAdsFailedToLoad(i2);
                }
            }

            public void onNativeAdsLoaded(List<AppLovinNativeAd> list) {
                if (d.this.f3874b != null) {
                    ArrayList arrayList = new ArrayList();
                    arrayList.addAll(d.this.f3873a);
                    arrayList.addAll(d.this.f3875c);
                    d.this.f3874b.onNativeAdsLoaded(arrayList);
                }
            }
        }

        d(List list, AppLovinNativeAdLoadListener appLovinNativeAdLoadListener, List list2) {
            this.f3873a = list;
            this.f3874b = appLovinNativeAdLoadListener;
            this.f3875c = list2;
        }

        public void onNativeAdsFailedToLoad(int i2) {
            AppLovinNativeAdLoadListener appLovinNativeAdLoadListener = this.f3874b;
            if (appLovinNativeAdLoadListener != null) {
                appLovinNativeAdLoadListener.onNativeAdsFailedToLoad(i2);
            }
        }

        public void onNativeAdsLoaded(List<AppLovinNativeAd> list) {
            NativeAdServiceImpl.this.c(this.f3873a, new a());
        }
    }

    class e implements AppLovinNativeAdLoadListener {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AppLovinNativeAdLoadListener f3878a;

        e(NativeAdServiceImpl nativeAdServiceImpl, AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
            this.f3878a = appLovinNativeAdLoadListener;
        }

        public void onNativeAdsFailedToLoad(int i2) {
            AppLovinNativeAdLoadListener appLovinNativeAdLoadListener = this.f3878a;
            if (appLovinNativeAdLoadListener != null) {
                appLovinNativeAdLoadListener.onNativeAdsFailedToLoad(i2);
            }
        }

        public void onNativeAdsLoaded(List<AppLovinNativeAd> list) {
            AppLovinNativeAdLoadListener appLovinNativeAdLoadListener = this.f3878a;
            if (appLovinNativeAdLoadListener != null) {
                appLovinNativeAdLoadListener.onNativeAdsLoaded(list);
            }
        }
    }

    class f implements AppLovinNativeAdLoadListener {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AppLovinNativeAdLoadListener f3879a;

        f(AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
            this.f3879a = appLovinNativeAdLoadListener;
        }

        public void onNativeAdsFailedToLoad(int i2) {
            NativeAdServiceImpl.this.a(this.f3879a, i2);
        }

        public void onNativeAdsLoaded(List<AppLovinNativeAd> list) {
            NativeAdServiceImpl.this.a(this.f3879a, list);
        }
    }

    NativeAdServiceImpl(k kVar) {
        this.f3866a = kVar;
        kVar.Z();
    }

    /* access modifiers changed from: private */
    public void a(AppLovinNativeAd appLovinNativeAd, AppLovinNativeAdPrecacheListener appLovinNativeAdPrecacheListener) {
        if (appLovinNativeAd.isVideoPrecached()) {
            appLovinNativeAdPrecacheListener.onNativeAdVideoPreceached(appLovinNativeAd);
            return;
        }
        this.f3866a.j().a(new f.n(Arrays.asList((NativeAdImpl) appLovinNativeAd), this.f3866a, new c(appLovinNativeAdPrecacheListener)), f.y.b.CACHING_OTHER);
    }

    /* access modifiers changed from: private */
    public void a(AppLovinNativeAdLoadListener appLovinNativeAdLoadListener, int i2) {
        if (appLovinNativeAdLoadListener != null) {
            try {
                appLovinNativeAdLoadListener.onNativeAdsFailedToLoad(i2);
            } catch (Exception e2) {
                q.c("NativeAdService", "Encountered exception whilst notifying user callback", e2);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(AppLovinNativeAdLoadListener appLovinNativeAdLoadListener, List<AppLovinNativeAd> list) {
        if (appLovinNativeAdLoadListener != null) {
            try {
                appLovinNativeAdLoadListener.onNativeAdsLoaded(list);
            } catch (Exception e2) {
                q.c("NativeAdService", "Encountered exception whilst notifying user callback", e2);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(AppLovinNativeAdPrecacheListener appLovinNativeAdPrecacheListener, AppLovinNativeAd appLovinNativeAd, int i2, boolean z) {
        if (appLovinNativeAdPrecacheListener == null) {
            return;
        }
        if (z) {
            try {
                appLovinNativeAdPrecacheListener.onNativeAdVideoPrecachingFailed(appLovinNativeAd, i2);
            } catch (Exception e2) {
                q.c("NativeAdService", "Encountered exception whilst notifying user callback", e2);
            }
        } else {
            appLovinNativeAdPrecacheListener.onNativeAdImagePrecachingFailed(appLovinNativeAd, i2);
        }
    }

    /* access modifiers changed from: private */
    public void a(AppLovinNativeAdPrecacheListener appLovinNativeAdPrecacheListener, AppLovinNativeAd appLovinNativeAd, boolean z) {
        if (appLovinNativeAdPrecacheListener == null) {
            return;
        }
        if (z) {
            try {
                appLovinNativeAdPrecacheListener.onNativeAdVideoPreceached(appLovinNativeAd);
            } catch (Exception e2) {
                q.c("NativeAdService", "Encountered exception whilst notifying user callback", e2);
            }
        } else {
            appLovinNativeAdPrecacheListener.onNativeAdImagesPrecached(appLovinNativeAd);
        }
    }

    private void a(String str, int i2, AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
        this.f3866a.j().a(new f.u(str, i2, this.f3866a, new a(appLovinNativeAdLoadListener)), f.y.b.MAIN);
    }

    /* access modifiers changed from: private */
    public void a(List<AppLovinNativeAd> list, AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
        int intValue = ((Integer) this.f3866a.a(c.e.n2)).intValue();
        if (intValue > 0) {
            int size = list.size();
            if (size != 0) {
                int min = Math.min(intValue, size);
                List<AppLovinNativeAd> subList = list.subList(0, min);
                b(subList, new d(subList, appLovinNativeAdLoadListener, list.subList(min, size)));
            } else if (appLovinNativeAdLoadListener != null) {
                appLovinNativeAdLoadListener.onNativeAdsFailedToLoad(AppLovinErrorCodes.UNABLE_TO_PREPARE_NATIVE_AD);
            }
        } else if (appLovinNativeAdLoadListener != null) {
            appLovinNativeAdLoadListener.onNativeAdsLoaded(list);
        }
    }

    private void b(List<NativeAdImpl> list, AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
        this.f3866a.j().a(new f.l(list, this.f3866a, new e(this, appLovinNativeAdLoadListener)), f.y.b.CACHING_OTHER);
    }

    /* access modifiers changed from: private */
    public void c(List<NativeAdImpl> list, AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
        this.f3866a.j().a(new f.n(list, this.f3866a, new f(appLovinNativeAdLoadListener)), f.y.b.CACHING_OTHER);
    }

    public boolean hasPreloadedAdForZoneId(String str) {
        if (TextUtils.isEmpty(str)) {
            q.i("NativeAdService", "Unable to check if ad is preloaded - invalid zone id.");
            return false;
        }
        return this.f3866a.t().g(com.applovin.impl.sdk.ad.d.a(str, this.f3866a));
    }

    public void loadNativeAds(int i2, AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
        loadNativeAds(i2, null, appLovinNativeAdLoadListener);
    }

    public void loadNativeAds(int i2, String str, AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
        if (i2 > 0) {
            this.f3866a.z();
            if (i2 == 1) {
                com.applovin.impl.sdk.ad.d b2 = com.applovin.impl.sdk.ad.d.b(str, this.f3866a);
                AppLovinNativeAd appLovinNativeAd = (AppLovinNativeAd) this.f3866a.t().e(b2);
                if (appLovinNativeAd != null) {
                    a(appLovinNativeAdLoadListener, Arrays.asList(appLovinNativeAd));
                } else {
                    a(str, 1, appLovinNativeAdLoadListener);
                }
                if (((Boolean) this.f3866a.a(c.e.q0)).booleanValue()) {
                    this.f3866a.t().i(b2);
                    return;
                }
                return;
            }
            a(str, i2, appLovinNativeAdLoadListener);
            return;
        }
        q.i("NativeAdService", "Requested invalid number of native ads: " + i2);
    }

    public void loadNextAd(AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
        loadNativeAds(1, appLovinNativeAdLoadListener);
    }

    public void precacheResources(AppLovinNativeAd appLovinNativeAd, AppLovinNativeAdPrecacheListener appLovinNativeAdPrecacheListener) {
        this.f3866a.z();
        if (appLovinNativeAd.isImagePrecached()) {
            appLovinNativeAdPrecacheListener.onNativeAdImagesPrecached(appLovinNativeAd);
            a(appLovinNativeAd, appLovinNativeAdPrecacheListener);
            return;
        }
        this.f3866a.j().a(new f.l(Arrays.asList((NativeAdImpl) appLovinNativeAd), this.f3866a, new b(appLovinNativeAdPrecacheListener)), f.y.b.CACHING_OTHER);
    }

    public void preloadAdForZoneId(String str) {
        if (TextUtils.isEmpty(str)) {
            q.i("NativeAdService", "Unable to preload zone for invalid zone id.");
            return;
        }
        this.f3866a.z();
        com.applovin.impl.sdk.ad.d a2 = com.applovin.impl.sdk.ad.d.a(str, this.f3866a);
        this.f3866a.s().h(a2);
        this.f3866a.s().i(a2);
    }

    public void preloadAds(com.applovin.impl.sdk.ad.d dVar) {
        this.f3866a.s().h(dVar);
        int g2 = dVar.g();
        if (g2 == 0 && this.f3866a.s().b(dVar)) {
            g2 = 1;
        }
        this.f3866a.s().b(dVar, g2);
    }

    public String toString() {
        return "NativeAdServiceImpl{}";
    }
}
