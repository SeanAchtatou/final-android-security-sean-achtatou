package com.applovin.impl.sdk.d;

import android.annotation.TargetApi;
import android.app.Activity;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.d.c;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.h;

public class d {

    /* renamed from: a  reason: collision with root package name */
    private final k f4050a;

    /* renamed from: b  reason: collision with root package name */
    private final h f4051b;

    /* renamed from: c  reason: collision with root package name */
    private final c.C0103c f4052c;

    /* renamed from: d  reason: collision with root package name */
    private final Object f4053d = new Object();

    /* renamed from: e  reason: collision with root package name */
    private final long f4054e;

    /* renamed from: f  reason: collision with root package name */
    private long f4055f;

    /* renamed from: g  reason: collision with root package name */
    private long f4056g;

    /* renamed from: h  reason: collision with root package name */
    private long f4057h;

    /* renamed from: i  reason: collision with root package name */
    private long f4058i;

    /* renamed from: j  reason: collision with root package name */
    private boolean f4059j;

    public d(AppLovinAdBase appLovinAdBase, k kVar) {
        if (appLovinAdBase == null) {
            throw new IllegalArgumentException("No ad specified");
        } else if (kVar != null) {
            this.f4050a = kVar;
            this.f4051b = kVar.k();
            this.f4052c = kVar.w().a(appLovinAdBase);
            c.C0103c cVar = this.f4052c;
            cVar.a(b.f4031d, (long) appLovinAdBase.getSource().ordinal());
            cVar.a();
            this.f4054e = appLovinAdBase.getCreatedAtMillis();
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    public static void a(long j2, AppLovinAdBase appLovinAdBase, k kVar) {
        if (appLovinAdBase != null && kVar != null) {
            c.C0103c a2 = kVar.w().a(appLovinAdBase);
            a2.a(b.f4032e, j2);
            a2.a();
        }
    }

    public static void a(AppLovinAdBase appLovinAdBase, k kVar) {
        if (appLovinAdBase != null && kVar != null) {
            c.C0103c a2 = kVar.w().a(appLovinAdBase);
            a2.a(b.f4033f, appLovinAdBase.getFetchLatencyMillis());
            a2.a(b.f4034g, appLovinAdBase.getFetchResponseSize());
            a2.a();
        }
    }

    private void a(b bVar) {
        synchronized (this.f4053d) {
            if (this.f4055f > 0) {
                long currentTimeMillis = System.currentTimeMillis() - this.f4055f;
                c.C0103c cVar = this.f4052c;
                cVar.a(bVar, currentTimeMillis);
                cVar.a();
            }
        }
    }

    public static void a(e eVar, AppLovinAdBase appLovinAdBase, k kVar) {
        if (appLovinAdBase != null && kVar != null && eVar != null) {
            c.C0103c a2 = kVar.w().a(appLovinAdBase);
            a2.a(b.f4035h, eVar.c());
            a2.a(b.f4036i, eVar.d());
            a2.a(b.y, eVar.g());
            a2.a(b.z, eVar.h());
            a2.a(b.C, eVar.b() ? 1 : 0);
            a2.a();
        }
    }

    @TargetApi(24)
    public void a() {
        long a2 = this.f4051b.a(g.f4076e);
        long a3 = this.f4051b.a(g.f4078g);
        c.C0103c cVar = this.f4052c;
        cVar.a(b.m, a2);
        cVar.a(b.l, a3);
        synchronized (this.f4053d) {
            long j2 = 0;
            if (this.f4054e > 0) {
                this.f4055f = System.currentTimeMillis();
                long f2 = this.f4055f - this.f4050a.f();
                long j3 = this.f4055f - this.f4054e;
                long j4 = h.a(this.f4050a.d()) ? 1 : 0;
                Activity a4 = this.f4050a.A().a();
                if (g.h() && a4 != null && a4.isInMultiWindowMode()) {
                    j2 = 1;
                }
                c.C0103c cVar2 = this.f4052c;
                cVar2.a(b.f4038k, f2);
                cVar2.a(b.f4037j, j3);
                cVar2.a(b.s, j4);
                cVar2.a(b.D, j2);
            }
        }
        this.f4052c.a();
    }

    public void a(long j2) {
        c.C0103c cVar = this.f4052c;
        cVar.a(b.u, j2);
        cVar.a();
    }

    public void b() {
        synchronized (this.f4053d) {
            if (this.f4056g < 1) {
                this.f4056g = System.currentTimeMillis();
                if (this.f4055f > 0) {
                    long j2 = this.f4056g - this.f4055f;
                    c.C0103c cVar = this.f4052c;
                    cVar.a(b.p, j2);
                    cVar.a();
                }
            }
        }
    }

    public void b(long j2) {
        c.C0103c cVar = this.f4052c;
        cVar.a(b.t, j2);
        cVar.a();
    }

    public void c() {
        a(b.n);
    }

    public void c(long j2) {
        c.C0103c cVar = this.f4052c;
        cVar.a(b.v, j2);
        cVar.a();
    }

    public void d() {
        a(b.q);
    }

    public void d(long j2) {
        synchronized (this.f4053d) {
            if (this.f4057h < 1) {
                this.f4057h = j2;
                c.C0103c cVar = this.f4052c;
                cVar.a(b.w, j2);
                cVar.a();
            }
        }
    }

    public void e() {
        a(b.r);
    }

    public void e(long j2) {
        synchronized (this.f4053d) {
            if (!this.f4059j) {
                this.f4059j = true;
                c.C0103c cVar = this.f4052c;
                cVar.a(b.A, j2);
                cVar.a();
            }
        }
    }

    public void f() {
        a(b.o);
    }

    public void g() {
        c.C0103c cVar = this.f4052c;
        cVar.a(b.x, 1);
        cVar.a();
    }

    public void h() {
        c.C0103c cVar = this.f4052c;
        cVar.a(b.E);
        cVar.a();
    }

    public void i() {
        synchronized (this.f4053d) {
            if (this.f4058i < 1) {
                this.f4058i = System.currentTimeMillis();
                if (this.f4055f > 0) {
                    long j2 = this.f4058i - this.f4055f;
                    c.C0103c cVar = this.f4052c;
                    cVar.a(b.B, j2);
                    cVar.a();
                }
            }
        }
    }
}
