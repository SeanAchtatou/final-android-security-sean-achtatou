package com.applovin.impl.sdk.network;

import android.content.SharedPreferences;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.q;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.m;
import com.applovin.sdk.AppLovinPostbackListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import org.json.JSONObject;

public class e {

    /* renamed from: a  reason: collision with root package name */
    private final k f4342a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final q f4343b;

    /* renamed from: c  reason: collision with root package name */
    private final SharedPreferences f4344c;

    /* renamed from: d  reason: collision with root package name */
    private final Object f4345d = new Object();

    /* renamed from: e  reason: collision with root package name */
    private final ArrayList<f> f4346e;

    /* renamed from: f  reason: collision with root package name */
    private final ArrayList<f> f4347f = new ArrayList<>();

    /* renamed from: g  reason: collision with root package name */
    private final Set<f> f4348g = new HashSet();

    class a implements AppLovinPostbackListener {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ f f4349a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ AppLovinPostbackListener f4350b;

        a(f fVar, AppLovinPostbackListener appLovinPostbackListener) {
            this.f4349a = fVar;
            this.f4350b = appLovinPostbackListener;
        }

        public void onPostbackFailure(String str, int i2) {
            q a2 = e.this.f4343b;
            a2.c("PersistentPostbackManager", "Failed to submit postback with errorCode " + i2 + ". Will retry later...  Postback: " + this.f4349a);
            e.this.e(this.f4349a);
            j.a(this.f4350b, str, i2);
        }

        public void onPostbackSuccess(String str) {
            e.this.d(this.f4349a);
            q a2 = e.this.f4343b;
            a2.b("PersistentPostbackManager", "Successfully submitted postback: " + this.f4349a);
            e.this.d();
            j.a(this.f4350b, str);
        }
    }

    public e(k kVar) {
        if (kVar != null) {
            this.f4342a = kVar;
            this.f4343b = kVar.Z();
            this.f4344c = kVar.d().getSharedPreferences("com.applovin.sdk.impl.postbackQueue.domain", 0);
            this.f4346e = b();
            return;
        }
        throw new IllegalArgumentException("No sdk specified");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x005a, code lost:
        r0 = ((java.lang.Integer) r4.f4342a.a(com.applovin.impl.sdk.c.e.q2)).intValue();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x006c, code lost:
        if (r5.g() <= r0) goto L_0x0092;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x006e, code lost:
        r6 = r4.f4343b;
        r6.d("PersistentPostbackManager", "Exceeded maximum persisted attempt count of " + r0 + ". Dequeuing postback: " + r5);
        d(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0092, code lost:
        r1 = r4.f4345d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0094, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r4.f4348g.add(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x009a, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x009f, code lost:
        if (r5.e() == null) goto L_0x00ab;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00a1, code lost:
        r0 = new org.json.JSONObject(r5.e());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00ab, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00ac, code lost:
        r1 = com.applovin.impl.sdk.network.g.b(r4.f4342a);
        r1.d(r5.a());
        r1.e(r5.b());
        r1.b(r5.c());
        r1.c(r5.d());
        r1.b(r0);
        r1.c(r5.f());
        r4.f4342a.q().dispatchPostbackRequest(r1.b(), new com.applovin.impl.sdk.network.e.a(r4, r5, r6));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(com.applovin.impl.sdk.network.f r5, com.applovin.sdk.AppLovinPostbackListener r6) {
        /*
            r4 = this;
            com.applovin.impl.sdk.q r0 = r4.f4343b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Preparing to submit postback..."
            r1.append(r2)
            r1.append(r5)
            java.lang.String r1 = r1.toString()
            java.lang.String r2 = "PersistentPostbackManager"
            r0.b(r2, r1)
            com.applovin.impl.sdk.k r0 = r4.f4342a
            boolean r0 = r0.G()
            if (r0 == 0) goto L_0x002a
            com.applovin.impl.sdk.q r5 = r4.f4343b
            java.lang.String r6 = "PersistentPostbackManager"
            java.lang.String r0 = "Skipping postback dispatch because SDK is still initializing - postback will be dispatched afterwards"
            r5.b(r6, r0)
            return
        L_0x002a:
            java.lang.Object r0 = r4.f4345d
            monitor-enter(r0)
            java.util.Set<com.applovin.impl.sdk.network.f> r1 = r4.f4348g     // Catch:{ all -> 0x00ee }
            boolean r1 = r1.contains(r5)     // Catch:{ all -> 0x00ee }
            if (r1 == 0) goto L_0x0053
            com.applovin.impl.sdk.q r6 = r4.f4343b     // Catch:{ all -> 0x00ee }
            java.lang.String r1 = "PersistentPostbackManager"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ee }
            r2.<init>()     // Catch:{ all -> 0x00ee }
            java.lang.String r3 = "Skip pending postback: "
            r2.append(r3)     // Catch:{ all -> 0x00ee }
            java.lang.String r5 = r5.a()     // Catch:{ all -> 0x00ee }
            r2.append(r5)     // Catch:{ all -> 0x00ee }
            java.lang.String r5 = r2.toString()     // Catch:{ all -> 0x00ee }
            r6.b(r1, r5)     // Catch:{ all -> 0x00ee }
            monitor-exit(r0)     // Catch:{ all -> 0x00ee }
            return
        L_0x0053:
            r5.h()     // Catch:{ all -> 0x00ee }
            r4.c()     // Catch:{ all -> 0x00ee }
            monitor-exit(r0)     // Catch:{ all -> 0x00ee }
            com.applovin.impl.sdk.k r0 = r4.f4342a
            com.applovin.impl.sdk.c$e<java.lang.Integer> r1 = com.applovin.impl.sdk.c.e.q2
            java.lang.Object r0 = r0.a(r1)
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            int r1 = r5.g()
            if (r1 <= r0) goto L_0x0092
            com.applovin.impl.sdk.q r6 = r4.f4343b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Exceeded maximum persisted attempt count of "
            r1.append(r2)
            r1.append(r0)
            java.lang.String r0 = ". Dequeuing postback: "
            r1.append(r0)
            r1.append(r5)
            java.lang.String r0 = r1.toString()
            java.lang.String r1 = "PersistentPostbackManager"
            r6.d(r1, r0)
            r4.d(r5)
            goto L_0x00ea
        L_0x0092:
            java.lang.Object r1 = r4.f4345d
            monitor-enter(r1)
            java.util.Set<com.applovin.impl.sdk.network.f> r0 = r4.f4348g     // Catch:{ all -> 0x00eb }
            r0.add(r5)     // Catch:{ all -> 0x00eb }
            monitor-exit(r1)     // Catch:{ all -> 0x00eb }
            java.util.Map r0 = r5.e()
            if (r0 == 0) goto L_0x00ab
            org.json.JSONObject r0 = new org.json.JSONObject
            java.util.Map r1 = r5.e()
            r0.<init>(r1)
            goto L_0x00ac
        L_0x00ab:
            r0 = 0
        L_0x00ac:
            com.applovin.impl.sdk.k r1 = r4.f4342a
            com.applovin.impl.sdk.network.g$a r1 = com.applovin.impl.sdk.network.g.b(r1)
            java.lang.String r2 = r5.a()
            r1.d(r2)
            java.lang.String r2 = r5.b()
            r1.e(r2)
            java.util.Map r2 = r5.c()
            r1.b(r2)
            java.util.Map r2 = r5.d()
            r1.c(r2)
            r1.b(r0)
            boolean r0 = r5.f()
            r1.c(r0)
            com.applovin.impl.sdk.network.g r0 = r1.a()
            com.applovin.impl.sdk.k r1 = r4.f4342a
            com.applovin.impl.sdk.network.PostbackServiceImpl r1 = r1.q()
            com.applovin.impl.sdk.network.e$a r2 = new com.applovin.impl.sdk.network.e$a
            r2.<init>(r5, r6)
            r1.dispatchPostbackRequest(r0, r2)
        L_0x00ea:
            return
        L_0x00eb:
            r5 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00eb }
            throw r5
        L_0x00ee:
            r5 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00ee }
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.network.e.a(com.applovin.impl.sdk.network.f, com.applovin.sdk.AppLovinPostbackListener):void");
    }

    private ArrayList<f> b() {
        Set<String> set = (Set) this.f4342a.b(c.g.m, new LinkedHashSet(0), this.f4344c);
        ArrayList<f> arrayList = new ArrayList<>(Math.max(1, set.size()));
        int intValue = ((Integer) this.f4342a.a(c.e.q2)).intValue();
        q qVar = this.f4343b;
        qVar.b("PersistentPostbackManager", "Deserializing " + set.size() + " postback(s).");
        for (String str : set) {
            try {
                f fVar = new f(new JSONObject(str), this.f4342a);
                if (fVar.g() < intValue) {
                    arrayList.add(fVar);
                } else {
                    q qVar2 = this.f4343b;
                    qVar2.b("PersistentPostbackManager", "Skipping deserialization because maximum attempt count exceeded for postback: " + fVar);
                }
            } catch (Throwable th) {
                q qVar3 = this.f4343b;
                qVar3.b("PersistentPostbackManager", "Unable to deserialize postback request from json: " + str, th);
            }
        }
        q qVar4 = this.f4343b;
        qVar4.b("PersistentPostbackManager", "Successfully loaded postback queue with " + arrayList.size() + " postback(s).");
        return arrayList;
    }

    private void b(f fVar) {
        synchronized (this.f4345d) {
            this.f4346e.add(fVar);
            c();
            q qVar = this.f4343b;
            qVar.b("PersistentPostbackManager", "Enqueued postback: " + fVar);
        }
    }

    private void c() {
        String str;
        q qVar;
        if (g.b()) {
            LinkedHashSet linkedHashSet = new LinkedHashSet(this.f4346e.size());
            Iterator<f> it = this.f4346e.iterator();
            while (it.hasNext()) {
                try {
                    linkedHashSet.add(it.next().j().toString());
                } catch (Throwable th) {
                    this.f4343b.b("PersistentPostbackManager", "Unable to serialize postback request to JSON.", th);
                }
            }
            this.f4342a.a(c.g.m, linkedHashSet, this.f4344c);
            qVar = this.f4343b;
            str = "Wrote updated postback queue to disk.";
        } else {
            qVar = this.f4343b;
            str = "Skipping writing postback queue to disk due to old Android version...";
        }
        qVar.b("PersistentPostbackManager", str);
    }

    private void c(f fVar) {
        a(fVar, (AppLovinPostbackListener) null);
    }

    /* access modifiers changed from: private */
    public void d() {
        synchronized (this.f4345d) {
            Iterator<f> it = this.f4347f.iterator();
            while (it.hasNext()) {
                c(it.next());
            }
            this.f4347f.clear();
        }
    }

    /* access modifiers changed from: private */
    public void d(f fVar) {
        synchronized (this.f4345d) {
            this.f4348g.remove(fVar);
            this.f4346e.remove(fVar);
            c();
        }
        q qVar = this.f4343b;
        qVar.b("PersistentPostbackManager", "Dequeued successfully transmitted postback: " + fVar);
    }

    /* access modifiers changed from: private */
    public void e(f fVar) {
        synchronized (this.f4345d) {
            this.f4348g.remove(fVar);
            this.f4347f.add(fVar);
        }
    }

    public void a() {
        synchronized (this.f4345d) {
            if (this.f4346e != null) {
                for (f c2 : new ArrayList(this.f4346e)) {
                    c(c2);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.network.e.a(com.applovin.impl.sdk.network.f, boolean):void
     arg types: [com.applovin.impl.sdk.network.f, int]
     candidates:
      com.applovin.impl.sdk.network.e.a(com.applovin.impl.sdk.network.e, com.applovin.impl.sdk.network.f):void
      com.applovin.impl.sdk.network.e.a(com.applovin.impl.sdk.network.f, com.applovin.sdk.AppLovinPostbackListener):void
      com.applovin.impl.sdk.network.e.a(com.applovin.impl.sdk.network.f, boolean):void */
    public void a(f fVar) {
        a(fVar, true);
    }

    public void a(f fVar, boolean z) {
        a(fVar, z, null);
    }

    public void a(f fVar, boolean z, AppLovinPostbackListener appLovinPostbackListener) {
        if (m.b(fVar.a())) {
            if (z) {
                fVar.i();
            }
            synchronized (this.f4345d) {
                b(fVar);
                a(fVar, appLovinPostbackListener);
            }
        }
    }
}
