package com.applovin.impl.sdk.ad;

import android.net.Uri;
import com.applovin.impl.adview.h;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.m;
import com.esotericsoftware.spine.Animation;
import com.facebook.internal.AnalyticsEvents;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TapjoyConstants;
import org.json.JSONObject;

public final class a extends f {
    public a(JSONObject jSONObject, JSONObject jSONObject2, b bVar, k kVar) {
        super(jSONObject, jSONObject2, bVar, kVar);
    }

    private String D0() {
        return getStringFromAdObject("stream_url", "");
    }

    public boolean A0() {
        return getBooleanFromAdObject("close_button_graphic_hidden", false);
    }

    public boolean B0() {
        if (this.adObject.has("close_button_expandable_hidden")) {
            return getBooleanFromAdObject("close_button_expandable_hidden", false);
        }
        return true;
    }

    public h.a C0() {
        return a(getIntFromAdObject("expandable_style", h.a.Invisible.a()));
    }

    public void a(String str) {
        try {
            synchronized (this.adObjectLock) {
                this.adObject.put(TJAdUnitConstants.String.HTML, str);
            }
        } catch (Throwable unused) {
        }
    }

    public void c(Uri uri) {
        try {
            synchronized (this.adObjectLock) {
                this.adObject.put(AnalyticsEvents.PARAMETER_SHARE_DIALOG_CONTENT_VIDEO, uri.toString());
            }
        } catch (Throwable unused) {
        }
    }

    public boolean d0() {
        return this.adObject.has("stream_url");
    }

    public Uri e0() {
        String D0 = D0();
        if (m.b(D0)) {
            return Uri.parse(D0);
        }
        String x0 = x0();
        if (m.b(x0)) {
            return Uri.parse(x0);
        }
        return null;
    }

    public Uri f0() {
        String stringFromAdObject = getStringFromAdObject("video_click_url", "");
        return m.b(stringFromAdObject) ? Uri.parse(stringFromAdObject) : y0();
    }

    public boolean hasVideoUrl() {
        return e0() != null;
    }

    public String v0() {
        String b2;
        synchronized (this.adObjectLock) {
            b2 = i.b(this.adObject, TJAdUnitConstants.String.HTML, (String) null, this.sdk);
        }
        return b2;
    }

    public void w0() {
        synchronized (this.adObjectLock) {
            this.adObject.remove("stream_url");
        }
    }

    public String x0() {
        return getStringFromAdObject(AnalyticsEvents.PARAMETER_SHARE_DIALOG_CONTENT_VIDEO, "");
    }

    public Uri y0() {
        String stringFromAdObject = getStringFromAdObject(TapjoyConstants.TJC_CLICK_URL, "");
        if (m.b(stringFromAdObject)) {
            return Uri.parse(stringFromAdObject);
        }
        return null;
    }

    public float z0() {
        return getFloatFromAdObject("mraid_close_delay_graphic", Animation.CurveTimeline.LINEAR);
    }
}
