package com.applovin.impl.sdk.d;

import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.f;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.q;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.m;
import com.google.firebase.perf.FirebasePerformance;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class c {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final k f4041a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final q f4042b;

    /* renamed from: c  reason: collision with root package name */
    private final Object f4043c = new Object();

    /* renamed from: d  reason: collision with root package name */
    private final d f4044d = new d(this, null);

    class a extends f.e0<Object> {
        a(com.applovin.impl.sdk.network.b bVar, k kVar) {
            super(bVar, kVar);
        }

        public void a(int i2) {
            q a2 = c.this.f4042b;
            a2.e("AdEventStatsManager", "Failed to submitted ad stats: " + i2);
        }

        public void a(Object obj, int i2) {
            q a2 = c.this.f4042b;
            a2.b("AdEventStatsManager", "Ad stats submitted: " + i2);
        }
    }

    private static class b {

        /* renamed from: a  reason: collision with root package name */
        private final k f4045a;

        /* renamed from: b  reason: collision with root package name */
        private final JSONObject f4046b;

        private b(String str, String str2, String str3, k kVar) {
            this.f4046b = new JSONObject();
            this.f4045a = kVar;
            i.a(this.f4046b, "pk", str, kVar);
            i.b(this.f4046b, "ts", System.currentTimeMillis(), kVar);
            if (m.b(str2)) {
                i.a(this.f4046b, "sk1", str2, kVar);
            }
            if (m.b(str3)) {
                i.a(this.f4046b, "sk2", str3, kVar);
            }
        }

        /* synthetic */ b(String str, String str2, String str3, k kVar, a aVar) {
            this(str, str2, str3, kVar);
        }

        /* access modifiers changed from: private */
        public String a() throws OutOfMemoryError {
            return this.f4046b.toString();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, long, com.applovin.impl.sdk.k):long
         arg types: [org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.k]
         candidates:
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, float, com.applovin.impl.sdk.k):float
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Boolean, com.applovin.impl.sdk.k):java.lang.Boolean
          com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, java.lang.Object, com.applovin.impl.sdk.k):java.lang.Object
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Object, com.applovin.impl.sdk.k):java.lang.Object
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.util.List, com.applovin.impl.sdk.k):java.util.List
          com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, org.json.JSONObject, com.applovin.impl.sdk.k):org.json.JSONObject
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.k):void
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.String, com.applovin.impl.sdk.k):void
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONArray, com.applovin.impl.sdk.k):void
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONObject, com.applovin.impl.sdk.k):void
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, long, com.applovin.impl.sdk.k):long */
        /* access modifiers changed from: package-private */
        public void a(String str, long j2) {
            b(str, i.a(this.f4046b, str, 0L, this.f4045a) + j2);
        }

        /* access modifiers changed from: package-private */
        public void a(String str, String str2) {
            JSONArray b2 = i.b(this.f4046b, str, new JSONArray(), this.f4045a);
            b2.put(str2);
            i.a(this.f4046b, str, b2, this.f4045a);
        }

        /* access modifiers changed from: package-private */
        public void b(String str, long j2) {
            i.b(this.f4046b, str, j2, this.f4045a);
        }

        public String toString() {
            return "AdEventStats{stats='" + this.f4046b + '\'' + '}';
        }
    }

    /* renamed from: com.applovin.impl.sdk.d.c$c  reason: collision with other inner class name */
    public class C0103c {

        /* renamed from: a  reason: collision with root package name */
        private final AppLovinAdBase f4047a;

        /* renamed from: b  reason: collision with root package name */
        private final c f4048b;

        public C0103c(c cVar, AppLovinAdBase appLovinAdBase, c cVar2) {
            this.f4047a = appLovinAdBase;
            this.f4048b = cVar2;
        }

        public C0103c a(b bVar) {
            this.f4048b.a(bVar, 1, this.f4047a);
            return this;
        }

        public C0103c a(b bVar, long j2) {
            this.f4048b.b(bVar, j2, this.f4047a);
            return this;
        }

        public C0103c a(b bVar, String str) {
            this.f4048b.a(bVar, str, this.f4047a);
            return this;
        }

        public void a() {
            this.f4048b.e();
        }
    }

    private class d extends LinkedHashMap<String, b> {
        private d() {
        }

        /* synthetic */ d(c cVar, a aVar) {
            this();
        }

        /* access modifiers changed from: protected */
        public boolean removeEldestEntry(Map.Entry<String, b> entry) {
            return size() > ((Integer) c.this.f4041a.a(c.e.q3)).intValue();
        }
    }

    public c(k kVar) {
        if (kVar != null) {
            this.f4041a = kVar;
            this.f4042b = kVar.Z();
            return;
        }
        throw new IllegalArgumentException("No sdk specified");
    }

    /* access modifiers changed from: private */
    public void a(b bVar, long j2, AppLovinAdBase appLovinAdBase) {
        if (appLovinAdBase == null) {
            throw new IllegalArgumentException("No ad specified");
        } else if (bVar == null) {
            throw new IllegalArgumentException("No key specified");
        } else if (((Boolean) this.f4041a.a(c.e.n3)).booleanValue()) {
            synchronized (this.f4043c) {
                b(appLovinAdBase).a(((Boolean) this.f4041a.a(c.e.r3)).booleanValue() ? bVar.b() : bVar.a(), j2);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(b bVar, String str, AppLovinAdBase appLovinAdBase) {
        if (appLovinAdBase == null) {
            throw new IllegalArgumentException("No ad specified");
        } else if (bVar == null) {
            throw new IllegalArgumentException("No key specified");
        } else if (((Boolean) this.f4041a.a(c.e.n3)).booleanValue()) {
            synchronized (this.f4044d) {
                b(appLovinAdBase).a(((Boolean) this.f4041a.a(c.e.r3)).booleanValue() ? bVar.b() : bVar.a(), str);
            }
        }
    }

    private void a(JSONObject jSONObject) {
        a aVar = new a(com.applovin.impl.sdk.network.b.a(this.f4041a).a(c()).c(d()).a(h.a(this.f4041a)).b(FirebasePerformance.HttpMethod.POST).a(jSONObject).b(((Integer) this.f4041a.a(c.e.o3)).intValue()).a(((Integer) this.f4041a.a(c.e.p3)).intValue()).a(), this.f4041a);
        aVar.a(c.e.V);
        aVar.b(c.e.W);
        this.f4041a.j().a(aVar, f.y.b.BACKGROUND);
    }

    private b b(AppLovinAdBase appLovinAdBase) {
        b bVar;
        synchronized (this.f4043c) {
            String primaryKey = appLovinAdBase.getPrimaryKey();
            bVar = (b) this.f4044d.get(primaryKey);
            if (bVar == null) {
                b bVar2 = new b(primaryKey, appLovinAdBase.getSecondaryKey1(), appLovinAdBase.getSecondaryKey2(), this.f4041a, null);
                this.f4044d.put(primaryKey, bVar2);
                bVar = bVar2;
            }
        }
        return bVar;
    }

    /* access modifiers changed from: private */
    public void b(b bVar, long j2, AppLovinAdBase appLovinAdBase) {
        if (appLovinAdBase == null) {
            throw new IllegalArgumentException("No ad specified");
        } else if (bVar == null) {
            throw new IllegalArgumentException("No key specified");
        } else if (((Boolean) this.f4041a.a(c.e.n3)).booleanValue()) {
            synchronized (this.f4043c) {
                b(appLovinAdBase).b(((Boolean) this.f4041a.a(c.e.r3)).booleanValue() ? bVar.b() : bVar.a(), j2);
            }
        }
    }

    private String c() {
        return h.a("2.0/s", this.f4041a);
    }

    private String d() {
        return h.b("2.0/s", this.f4041a);
    }

    /* access modifiers changed from: private */
    public void e() {
        HashSet hashSet;
        if (((Boolean) this.f4041a.a(c.e.n3)).booleanValue()) {
            synchronized (this.f4043c) {
                hashSet = new HashSet(this.f4044d.size());
                for (b bVar : this.f4044d.values()) {
                    try {
                        hashSet.add(bVar.a());
                    } catch (OutOfMemoryError e2) {
                        q qVar = this.f4042b;
                        qVar.b("AdEventStatsManager", "Failed to serialize " + bVar + " due to OOM error", e2);
                        b();
                    }
                }
            }
            this.f4041a.a(c.g.u, hashSet);
        }
    }

    public C0103c a(AppLovinAdBase appLovinAdBase) {
        return new C0103c(this, appLovinAdBase, this);
    }

    public void a() {
        if (((Boolean) this.f4041a.a(c.e.n3)).booleanValue()) {
            Set<String> set = (Set) this.f4041a.b(c.g.u, new HashSet(0));
            this.f4041a.b(c.g.u);
            if (set == null || set.isEmpty()) {
                this.f4042b.b("AdEventStatsManager", "No serialized ad events found");
                return;
            }
            q qVar = this.f4042b;
            qVar.b("AdEventStatsManager", "De-serializing " + set.size() + " stat ad events");
            JSONArray jSONArray = new JSONArray();
            for (String str : set) {
                try {
                    jSONArray.put(new JSONObject(str));
                } catch (JSONException e2) {
                    q qVar2 = this.f4042b;
                    qVar2.b("AdEventStatsManager", "Failed to parse: " + str, e2);
                }
            }
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("stats", jSONArray);
                a(jSONObject);
            } catch (JSONException e3) {
                this.f4042b.b("AdEventStatsManager", "Failed to create stats to submit", e3);
            }
        }
    }

    public void b() {
        synchronized (this.f4043c) {
            this.f4042b.b("AdEventStatsManager", "Clearing ad stats...");
            this.f4044d.clear();
        }
    }
}
