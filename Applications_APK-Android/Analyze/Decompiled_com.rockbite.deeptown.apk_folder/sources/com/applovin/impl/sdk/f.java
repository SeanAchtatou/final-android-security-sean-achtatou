package com.applovin.impl.sdk;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.webkit.URLUtil;
import com.applovin.impl.mediation.b;
import com.applovin.impl.mediation.d.c;
import com.applovin.impl.mediation.l;
import com.applovin.impl.sdk.ad.NativeAdImpl;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.network.a;
import com.applovin.impl.sdk.network.b;
import com.applovin.impl.sdk.p;
import com.applovin.mediation.AppLovinNativeAdapter;
import com.applovin.nativeAds.AppLovinNativeAd;
import com.applovin.nativeAds.AppLovinNativeAdLoadListener;
import com.applovin.nativeAds.AppLovinNativeAdPrecacheListener;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdRewardListener;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinErrorCodes;
import com.applovin.sdk.AppLovinPostbackListener;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinWebViewActivity;
import com.appsflyer.share.Constants;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.internal.NativeProtocol;
import com.google.ads.mediation.inmobi.InMobiNetworkValues;
import com.google.firebase.perf.FirebasePerformance;
import com.tapjoy.TapjoyConstants;
import e.c.a.a.a;
import e.c.a.a.e;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class f extends BroadcastReceiver implements p.c {

    /* renamed from: a  reason: collision with root package name */
    private com.applovin.impl.sdk.utils.n f4116a;

    /* renamed from: b  reason: collision with root package name */
    private final Object f4117b = new Object();

    /* renamed from: c  reason: collision with root package name */
    private final k f4118c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public final b f4119d;

    /* renamed from: e  reason: collision with root package name */
    private long f4120e;

    class a implements Runnable {
        a() {
        }

        public void run() {
            f.this.j();
            f.this.f4119d.onAdRefresh();
        }
    }

    abstract class a0 extends c {

        /* renamed from: f  reason: collision with root package name */
        private final AppLovinAdLoadListener f4122f;

        /* renamed from: g  reason: collision with root package name */
        private final a f4123g;

        private static final class a extends e.c.a.a.c {
            a(JSONObject jSONObject, JSONObject jSONObject2, com.applovin.impl.sdk.ad.b bVar, k kVar) {
                super(jSONObject, jSONObject2, bVar, kVar);
            }

            /* access modifiers changed from: package-private */
            public void a(com.applovin.impl.sdk.utils.r rVar) {
                if (rVar != null) {
                    this.f6671b.add(rVar);
                    return;
                }
                throw new IllegalArgumentException("No aggregated vast response specified");
            }
        }

        private static final class b extends a0 {

            /* renamed from: h  reason: collision with root package name */
            private final JSONObject f4124h;

            b(e.c.a.a.c cVar, AppLovinAdLoadListener appLovinAdLoadListener, k kVar) {
                super(cVar, appLovinAdLoadListener, kVar);
                if (appLovinAdLoadListener != null) {
                    this.f4124h = cVar.c();
                    return;
                }
                throw new IllegalArgumentException("No callback specified.");
            }

            public com.applovin.impl.sdk.d.i a() {
                return com.applovin.impl.sdk.d.i.u;
            }

            public void run() {
                e.c.a.a.d dVar;
                a("Processing SDK JSON response...");
                String b2 = com.applovin.impl.sdk.utils.i.b(this.f4124h, "xml", (String) null, this.f4130a);
                if (!com.applovin.impl.sdk.utils.m.b(b2)) {
                    d("No VAST response received.");
                    dVar = e.c.a.a.d.NO_WRAPPER_RESPONSE;
                } else if (b2.length() < ((Integer) this.f4130a.a(c.e.x3)).intValue()) {
                    try {
                        a(com.applovin.impl.sdk.utils.s.a(b2, this.f4130a));
                        return;
                    } catch (Throwable th) {
                        a("Unable to parse VAST response", th);
                        a(e.c.a.a.d.XML_PARSING);
                        this.f4130a.l().a(a());
                        return;
                    }
                } else {
                    d("VAST response is over max length");
                    dVar = e.c.a.a.d.XML_PARSING;
                }
                a(dVar);
            }
        }

        private static final class c extends a0 {

            /* renamed from: h  reason: collision with root package name */
            private final com.applovin.impl.sdk.utils.r f4125h;

            c(com.applovin.impl.sdk.utils.r rVar, e.c.a.a.c cVar, AppLovinAdLoadListener appLovinAdLoadListener, k kVar) {
                super(cVar, appLovinAdLoadListener, kVar);
                if (rVar == null) {
                    throw new IllegalArgumentException("No response specified.");
                } else if (cVar == null) {
                    throw new IllegalArgumentException("No context specified.");
                } else if (appLovinAdLoadListener != null) {
                    this.f4125h = rVar;
                } else {
                    throw new IllegalArgumentException("No callback specified.");
                }
            }

            public com.applovin.impl.sdk.d.i a() {
                return com.applovin.impl.sdk.d.i.v;
            }

            public void run() {
                a("Processing VAST Wrapper response...");
                a(this.f4125h);
            }
        }

        a0(e.c.a.a.c cVar, AppLovinAdLoadListener appLovinAdLoadListener, k kVar) {
            super("TaskProcessVastResponse", kVar);
            if (cVar != null) {
                this.f4122f = appLovinAdLoadListener;
                this.f4123g = (a) cVar;
                return;
            }
            throw new IllegalArgumentException("No context specified.");
        }

        public static a0 a(com.applovin.impl.sdk.utils.r rVar, e.c.a.a.c cVar, AppLovinAdLoadListener appLovinAdLoadListener, k kVar) {
            return new c(rVar, cVar, appLovinAdLoadListener, kVar);
        }

        public static a0 a(JSONObject jSONObject, JSONObject jSONObject2, com.applovin.impl.sdk.ad.b bVar, AppLovinAdLoadListener appLovinAdLoadListener, k kVar) {
            return new b(new a(jSONObject, jSONObject2, bVar, kVar), appLovinAdLoadListener, kVar);
        }

        /* access modifiers changed from: package-private */
        public void a(com.applovin.impl.sdk.utils.r rVar) {
            e.c.a.a.d dVar;
            c cVar;
            int a2 = this.f4123g.a();
            a("Finished parsing XML at depth " + a2);
            this.f4123g.a(rVar);
            if (e.c.a.a.i.a(rVar)) {
                int intValue = ((Integer) this.f4130a.a(c.e.y3)).intValue();
                if (a2 < intValue) {
                    a("VAST response is wrapper. Resolving...");
                    cVar = new d(this.f4123g, this.f4122f, this.f4130a);
                } else {
                    d("Reached beyond max wrapper depth of " + intValue);
                    dVar = e.c.a.a.d.WRAPPER_LIMIT_REACHED;
                    a(dVar);
                    return;
                }
            } else if (e.c.a.a.i.b(rVar)) {
                a("VAST response is inline. Rendering ad...");
                cVar = new d0(this.f4123g, this.f4122f, this.f4130a);
            } else {
                d("VAST response is an error");
                dVar = e.c.a.a.d.NO_WRAPPER_RESPONSE;
                a(dVar);
                return;
            }
            this.f4130a.j().a(cVar);
        }

        /* access modifiers changed from: package-private */
        public void a(e.c.a.a.d dVar) {
            d("Failed to process VAST response due to VAST error code " + dVar);
            e.c.a.a.i.a(this.f4123g, this.f4122f, dVar, -6, this.f4130a);
        }
    }

    public interface b {
        void onAdRefresh();
    }

    class b0 extends c {

        /* renamed from: f  reason: collision with root package name */
        private final JSONObject f4126f;

        /* renamed from: g  reason: collision with root package name */
        private final JSONObject f4127g;

        /* renamed from: h  reason: collision with root package name */
        private final AppLovinAdLoadListener f4128h;

        /* renamed from: i  reason: collision with root package name */
        private final com.applovin.impl.sdk.ad.b f4129i;

        b0(JSONObject jSONObject, JSONObject jSONObject2, com.applovin.impl.sdk.ad.b bVar, AppLovinAdLoadListener appLovinAdLoadListener, k kVar) {
            super("TaskRenderAppLovinAd", kVar);
            this.f4126f = jSONObject;
            this.f4127g = jSONObject2;
            this.f4129i = bVar;
            this.f4128h = appLovinAdLoadListener;
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.w;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Boolean, com.applovin.impl.sdk.k):java.lang.Boolean
         arg types: [org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.k]
         candidates:
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, float, com.applovin.impl.sdk.k):float
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, long, com.applovin.impl.sdk.k):long
          com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, java.lang.Object, com.applovin.impl.sdk.k):java.lang.Object
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Object, com.applovin.impl.sdk.k):java.lang.Object
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.util.List, com.applovin.impl.sdk.k):java.util.List
          com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, org.json.JSONObject, com.applovin.impl.sdk.k):org.json.JSONObject
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.k):void
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.String, com.applovin.impl.sdk.k):void
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONArray, com.applovin.impl.sdk.k):void
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONObject, com.applovin.impl.sdk.k):void
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Boolean, com.applovin.impl.sdk.k):java.lang.Boolean */
        public void run() {
            a("Rendering ad...");
            com.applovin.impl.sdk.ad.a aVar = new com.applovin.impl.sdk.ad.a(this.f4126f, this.f4127g, this.f4129i, this.f4130a);
            boolean booleanValue = com.applovin.impl.sdk.utils.i.a(this.f4126f, "gs_load_immediately", (Boolean) false, this.f4130a).booleanValue();
            boolean booleanValue2 = com.applovin.impl.sdk.utils.i.a(this.f4126f, "vs_load_immediately", (Boolean) true, this.f4130a).booleanValue();
            k kVar = new k(aVar, this.f4130a, this.f4128h);
            kVar.a(booleanValue2);
            kVar.b(booleanValue);
            y.b bVar = y.b.CACHING_OTHER;
            if (((Boolean) this.f4130a.a(c.e.s0)).booleanValue()) {
                if (aVar.getSize() == AppLovinAdSize.INTERSTITIAL && aVar.getType() == AppLovinAdType.REGULAR) {
                    bVar = y.b.CACHING_INTERSTITIAL;
                } else if (aVar.getSize() == AppLovinAdSize.INTERSTITIAL && aVar.getType() == AppLovinAdType.INCENTIVIZED) {
                    bVar = y.b.CACHING_INCENTIVIZED;
                }
            }
            this.f4130a.j().a(kVar, bVar);
        }
    }

    public abstract class c implements Runnable {
        /* access modifiers changed from: protected */

        /* renamed from: a  reason: collision with root package name */
        public final k f4130a;

        /* renamed from: b  reason: collision with root package name */
        private final String f4131b;

        /* renamed from: c  reason: collision with root package name */
        private final q f4132c;

        /* renamed from: d  reason: collision with root package name */
        private final Context f4133d;

        /* renamed from: e  reason: collision with root package name */
        private final boolean f4134e;

        public c(String str, k kVar) {
            this(str, kVar, false);
        }

        public c(String str, k kVar, boolean z) {
            this.f4131b = str;
            this.f4130a = kVar;
            this.f4132c = kVar.Z();
            this.f4133d = kVar.d();
            this.f4134e = z;
        }

        public abstract com.applovin.impl.sdk.d.i a();

        /* access modifiers changed from: protected */
        public void a(String str) {
            this.f4132c.b(this.f4131b, str);
        }

        /* access modifiers changed from: protected */
        public void a(String str, Throwable th) {
            this.f4132c.b(this.f4131b, str, th);
        }

        /* access modifiers changed from: protected */
        public k b() {
            return this.f4130a;
        }

        /* access modifiers changed from: protected */
        public void b(String str) {
            this.f4132c.c(this.f4131b, str);
        }

        public String c() {
            return this.f4131b;
        }

        /* access modifiers changed from: protected */
        public void c(String str) {
            this.f4132c.d(this.f4131b, str);
        }

        /* access modifiers changed from: protected */
        public Context d() {
            return this.f4133d;
        }

        /* access modifiers changed from: protected */
        public void d(String str) {
            this.f4132c.e(this.f4131b, str);
        }

        public boolean e() {
            return this.f4134e;
        }
    }

    class c0 extends c {

        /* renamed from: f  reason: collision with root package name */
        private final AppLovinNativeAdLoadListener f4135f;

        /* renamed from: g  reason: collision with root package name */
        private final JSONObject f4136g;

        c0(JSONObject jSONObject, k kVar, AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
            super("TaskRenderNativeAd", kVar);
            this.f4135f = appLovinNativeAdLoadListener;
            this.f4136g = jSONObject;
        }

        private String a(String str, Map<String, String> map, String str2) {
            String str3 = map.get(str);
            if (str3 != null) {
                return str3.replace("{CLCODE}", str2);
            }
            return null;
        }

        private String a(Map<String, String> map, String str, String str2) {
            String str3 = map.get(TapjoyConstants.TJC_CLICK_URL);
            if (str2 == null) {
                str2 = "";
            }
            return str3.replace("{CLCODE}", str).replace("{EVENT_ID}", str2);
        }

        private void a(JSONObject jSONObject) throws JSONException {
            JSONObject jSONObject2 = jSONObject;
            JSONArray optJSONArray = jSONObject2.optJSONArray("native_ads");
            JSONObject optJSONObject = jSONObject2.optJSONObject("native_settings");
            if (optJSONArray == null || optJSONArray.length() <= 0) {
                c("No ads were returned from the server");
                this.f4135f.onNativeAdsFailedToLoad(204);
                return;
            }
            List b2 = com.applovin.impl.sdk.utils.i.b(optJSONArray);
            ArrayList arrayList = new ArrayList(b2.size());
            Map a2 = optJSONObject != null ? com.applovin.impl.sdk.utils.i.a(optJSONObject) : new HashMap(0);
            Iterator it = b2.iterator();
            while (it.hasNext()) {
                Map map = (Map) it.next();
                String str = (String) map.get("clcode");
                String b3 = com.applovin.impl.sdk.utils.i.b(jSONObject2, "zone_id", (String) null, this.f4130a);
                String str2 = (String) map.get("event_id");
                com.applovin.impl.sdk.ad.d b4 = com.applovin.impl.sdk.ad.d.b(b3, this.f4130a);
                String a3 = a("simp_url", a2, str);
                String a4 = a(a2, str, str2);
                List<com.applovin.impl.sdk.d.a> a5 = com.applovin.impl.sdk.utils.p.a("simp_urls", optJSONObject, str, a3, this.f4130a);
                Iterator it2 = it;
                String str3 = a4;
                String str4 = com.applovin.impl.sdk.utils.i.a(optJSONObject, "should_post_click_url", true, this.f4130a).booleanValue() ? a4 : null;
                JSONObject jSONObject3 = optJSONObject;
                String str5 = a3;
                List<com.applovin.impl.sdk.d.a> a6 = com.applovin.impl.sdk.utils.p.a("click_tracking_urls", optJSONObject, str, str2, str4, this.f4130a);
                if (a5.size() == 0) {
                    throw new IllegalArgumentException("No impression URL available");
                } else if (a6.size() != 0) {
                    String str6 = (String) map.get("resource_cache_prefix");
                    List<String> a7 = str6 != null ? com.applovin.impl.sdk.utils.e.a(str6) : this.f4130a.b(c.e.I0);
                    NativeAdImpl.b bVar = new NativeAdImpl.b();
                    bVar.a(b4);
                    bVar.e(b3);
                    bVar.f((String) map.get("title"));
                    bVar.g((String) map.get(InMobiNetworkValues.DESCRIPTION));
                    bVar.h((String) map.get("caption"));
                    bVar.q((String) map.get(InMobiNetworkValues.CTA));
                    bVar.a((String) map.get("icon_url"));
                    bVar.b((String) map.get("image_url"));
                    bVar.d((String) map.get(TapjoyConstants.TJC_VIDEO_URL));
                    bVar.c((String) map.get("star_rating_url"));
                    bVar.i((String) map.get("icon_url"));
                    bVar.j((String) map.get("image_url"));
                    bVar.k((String) map.get(TapjoyConstants.TJC_VIDEO_URL));
                    bVar.a(Float.parseFloat((String) map.get("star_rating")));
                    bVar.p(str);
                    bVar.l(str3);
                    bVar.m(str5);
                    bVar.n(a("video_start_url", a2, str));
                    bVar.o(a("video_end_url", a2, str));
                    bVar.a(a5);
                    bVar.b(a6);
                    bVar.a(Long.parseLong((String) map.get(AppLovinNativeAdapter.KEY_EXTRA_AD_ID)));
                    bVar.c(a7);
                    bVar.a(this.f4130a);
                    NativeAdImpl a8 = bVar.a();
                    arrayList.add(a8);
                    a("Prepared native ad: " + a8.getAdId());
                    jSONObject2 = jSONObject;
                    optJSONObject = jSONObject3;
                    it = it2;
                } else {
                    throw new IllegalArgumentException("No click tracking URL available");
                }
            }
            AppLovinNativeAdLoadListener appLovinNativeAdLoadListener = this.f4135f;
            if (appLovinNativeAdLoadListener != null) {
                appLovinNativeAdLoadListener.onNativeAdsLoaded(arrayList);
            }
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.x;
        }

        /* access modifiers changed from: package-private */
        public void a(int i2) {
            try {
                if (this.f4135f != null) {
                    this.f4135f.onNativeAdsFailedToLoad(i2);
                }
            } catch (Exception e2) {
                a("Unable to notify listener about failure.", e2);
            }
        }

        public void run() {
            try {
                if (this.f4136g != null) {
                    if (this.f4136g.length() != 0) {
                        a(this.f4136g);
                        return;
                    }
                }
                a((int) AppLovinErrorCodes.UNABLE_TO_PREPARE_NATIVE_AD);
            } catch (Exception e2) {
                a("Unable to render native ad.", e2);
                a((int) AppLovinErrorCodes.UNABLE_TO_PREPARE_NATIVE_AD);
                this.f4130a.l().a(a());
            }
        }
    }

    class d extends c {
        /* access modifiers changed from: private */

        /* renamed from: f  reason: collision with root package name */
        public e.c.a.a.c f4137f;
        /* access modifiers changed from: private */

        /* renamed from: g  reason: collision with root package name */
        public final AppLovinAdLoadListener f4138g;

        class a extends e0<com.applovin.impl.sdk.utils.r> {
            a(com.applovin.impl.sdk.network.b bVar, k kVar) {
                super(bVar, kVar);
            }

            public void a(int i2) {
                d("Unable to resolve VAST wrapper. Server returned " + i2);
                d.this.a(i2);
            }

            public void a(com.applovin.impl.sdk.utils.r rVar, int i2) {
                this.f4130a.j().a(a0.a(rVar, d.this.f4137f, d.this.f4138g, d.this.f4130a));
            }
        }

        d(e.c.a.a.c cVar, AppLovinAdLoadListener appLovinAdLoadListener, k kVar) {
            super("TaskResolveVastWrapper", kVar);
            this.f4138g = appLovinAdLoadListener;
            this.f4137f = cVar;
        }

        /* access modifiers changed from: private */
        public void a(int i2) {
            d("Failed to resolve VAST wrapper due to error code " + i2);
            if (i2 == -103) {
                com.applovin.impl.sdk.utils.p.a(this.f4138g, this.f4137f.g(), i2, this.f4130a);
            } else {
                e.c.a.a.i.a(this.f4137f, this.f4138g, i2 == -102 ? e.c.a.a.d.TIMED_OUT : e.c.a.a.d.GENERAL_WRAPPER_ERROR, i2, this.f4130a);
            }
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.A;
        }

        public void run() {
            String a2 = e.c.a.a.i.a(this.f4137f);
            if (com.applovin.impl.sdk.utils.m.b(a2)) {
                a("Resolving VAST ad with depth " + this.f4137f.a() + " at " + a2);
                try {
                    this.f4130a.j().a(new a(com.applovin.impl.sdk.network.b.a(this.f4130a).a(a2).b(FirebasePerformance.HttpMethod.GET).a(com.applovin.impl.sdk.utils.r.f4489e).a(((Integer) this.f4130a.a(c.e.E3)).intValue()).b(((Integer) this.f4130a.a(c.e.F3)).intValue()).a(false).a(), this.f4130a));
                } catch (Throwable th) {
                    a("Unable to resolve VAST wrapper", th);
                    a(-1);
                    this.f4130a.l().a(a());
                }
            } else {
                d("Resolving VAST failed. Could not find resolution URL");
                a(-1);
            }
        }
    }

    class d0 extends c {

        /* renamed from: f  reason: collision with root package name */
        private e.c.a.a.c f4139f;

        /* renamed from: g  reason: collision with root package name */
        private final AppLovinAdLoadListener f4140g;

        d0(e.c.a.a.c cVar, AppLovinAdLoadListener appLovinAdLoadListener, k kVar) {
            super("TaskRenderVastAd", kVar);
            this.f4140g = appLovinAdLoadListener;
            this.f4139f = cVar;
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.y;
        }

        public void run() {
            a("Rendering VAST ad...");
            int size = this.f4139f.b().size();
            HashSet hashSet = new HashSet(size);
            HashSet hashSet2 = new HashSet(size);
            String str = "";
            e.c.a.a.f fVar = null;
            e.c.a.a.j jVar = null;
            e.c.a.a.b bVar = null;
            String str2 = str;
            for (com.applovin.impl.sdk.utils.r next : this.f4139f.b()) {
                com.applovin.impl.sdk.utils.r c2 = next.c(e.c.a.a.i.a(next) ? "Wrapper" : "InLine");
                if (c2 != null) {
                    com.applovin.impl.sdk.utils.r c3 = c2.c("AdSystem");
                    if (c3 != null) {
                        fVar = e.c.a.a.f.a(c3, fVar, this.f4130a);
                    }
                    str = e.c.a.a.i.a(c2, "AdTitle", str);
                    str2 = e.c.a.a.i.a(c2, "Description", str2);
                    e.c.a.a.i.a(c2.a("Impression"), hashSet, this.f4139f, this.f4130a);
                    e.c.a.a.i.a(c2.a("Error"), hashSet2, this.f4139f, this.f4130a);
                    com.applovin.impl.sdk.utils.r b2 = c2.b("Creatives");
                    if (b2 != null) {
                        for (com.applovin.impl.sdk.utils.r next2 : b2.d()) {
                            com.applovin.impl.sdk.utils.r b3 = next2.b("Linear");
                            if (b3 != null) {
                                jVar = e.c.a.a.j.a(b3, jVar, this.f4139f, this.f4130a);
                            } else {
                                com.applovin.impl.sdk.utils.r c4 = next2.c("CompanionAds");
                                if (c4 != null) {
                                    com.applovin.impl.sdk.utils.r c5 = c4.c("Companion");
                                    if (c5 != null) {
                                        bVar = e.c.a.a.b.a(c5, bVar, this.f4139f, this.f4130a);
                                    }
                                } else {
                                    d("Received and will skip rendering for an unidentified creative: " + next2);
                                }
                            }
                        }
                    }
                } else {
                    d("Did not find wrapper or inline response for node: " + next);
                }
            }
            a.b H0 = e.c.a.a.a.H0();
            H0.a(this.f4130a);
            H0.a(this.f4139f.c());
            H0.b(this.f4139f.d());
            H0.a(this.f4139f.e());
            H0.a(this.f4139f.f());
            H0.a(str);
            H0.b(str2);
            H0.a(fVar);
            H0.a(jVar);
            H0.a(bVar);
            H0.a(hashSet);
            H0.b(hashSet2);
            e.c.a.a.a a2 = H0.a();
            e.c.a.a.d a3 = e.c.a.a.i.a(a2);
            if (a3 == null) {
                o oVar = new o(a2, this.f4130a, this.f4140g);
                y.b bVar2 = y.b.CACHING_OTHER;
                if (((Boolean) this.f4130a.a(c.e.s0)).booleanValue()) {
                    if (a2.getType() == AppLovinAdType.REGULAR) {
                        bVar2 = y.b.CACHING_INTERSTITIAL;
                    } else if (a2.getType() == AppLovinAdType.INCENTIVIZED) {
                        bVar2 = y.b.CACHING_INCENTIVIZED;
                    }
                }
                this.f4130a.j().a(oVar, bVar2);
                return;
            }
            e.c.a.a.i.a(this.f4139f, this.f4140g, a3, -6, this.f4130a);
        }
    }

    public abstract class e extends c {

        class a extends e0<JSONObject> {
            final /* synthetic */ a.c l;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            a(e eVar, com.applovin.impl.sdk.network.b bVar, k kVar, a.c cVar) {
                super(bVar, kVar);
                this.l = cVar;
            }

            public void a(int i2) {
                this.l.a(i2);
            }

            public void a(JSONObject jSONObject, int i2) {
                this.l.a(jSONObject, i2);
            }
        }

        protected e(String str, k kVar) {
            super(str, kVar);
        }

        /* access modifiers changed from: protected */
        public abstract void a(JSONObject jSONObject);

        /* access modifiers changed from: package-private */
        public void a(JSONObject jSONObject, a.c<JSONObject> cVar) {
            a aVar = new a(this, com.applovin.impl.sdk.network.b.a(this.f4130a).a(com.applovin.impl.sdk.utils.h.a(f(), this.f4130a)).c(com.applovin.impl.sdk.utils.h.b(f(), this.f4130a)).a(com.applovin.impl.sdk.utils.h.a(this.f4130a)).b(FirebasePerformance.HttpMethod.POST).a(jSONObject).a((Object) new JSONObject()).a(((Integer) this.f4130a.a(c.e.J0)).intValue()).a(), this.f4130a, cVar);
            aVar.a(c.e.V);
            aVar.b(c.e.W);
            this.f4130a.j().a(aVar);
        }

        /* access modifiers changed from: protected */
        public abstract String f();

        /* access modifiers changed from: protected */
        public JSONObject g() {
            JSONObject jSONObject = new JSONObject();
            String M = this.f4130a.M();
            if (((Boolean) this.f4130a.a(c.e.P2)).booleanValue() && com.applovin.impl.sdk.utils.m.b(M)) {
                com.applovin.impl.sdk.utils.i.a(jSONObject, "cuid", M, this.f4130a);
            }
            if (((Boolean) this.f4130a.a(c.e.R2)).booleanValue()) {
                com.applovin.impl.sdk.utils.i.a(jSONObject, "compass_random_token", this.f4130a.N(), this.f4130a);
            }
            if (((Boolean) this.f4130a.a(c.e.T2)).booleanValue()) {
                com.applovin.impl.sdk.utils.i.a(jSONObject, "applovin_random_token", this.f4130a.O(), this.f4130a);
            }
            a(jSONObject);
            return jSONObject;
        }
    }

    public abstract class e0<T> extends c implements a.c<T> {
        /* access modifiers changed from: private */

        /* renamed from: f  reason: collision with root package name */
        public final com.applovin.impl.sdk.network.b<T> f4141f;

        /* renamed from: g  reason: collision with root package name */
        private final a.c<T> f4142g;
        /* access modifiers changed from: private */

        /* renamed from: h  reason: collision with root package name */
        public y.b f4143h;
        /* access modifiers changed from: private */

        /* renamed from: i  reason: collision with root package name */
        public c.e<String> f4144i;
        /* access modifiers changed from: private */

        /* renamed from: j  reason: collision with root package name */
        public c.e<String> f4145j;

        /* renamed from: k  reason: collision with root package name */
        protected a.C0107a f4146k;

        class a implements a.c<T> {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ k f4147a;

            a(k kVar) {
                this.f4147a = kVar;
            }

            public void a(int i2) {
                c.e eVar;
                e0 e0Var;
                boolean z = false;
                boolean z2 = i2 < 200 || i2 >= 500;
                boolean z3 = i2 == 429;
                if (i2 != -103) {
                    z = true;
                }
                if (z && (z2 || z3)) {
                    String f2 = e0.this.f4141f.f();
                    if (e0.this.f4141f.j() > 0) {
                        e0 e0Var2 = e0.this;
                        e0Var2.c("Unable to send request due to server failure (code " + i2 + "). " + e0.this.f4141f.j() + " attempts left, retrying in " + TimeUnit.MILLISECONDS.toSeconds((long) e0.this.f4141f.l()) + " seconds...");
                        int j2 = e0.this.f4141f.j() - 1;
                        e0.this.f4141f.a(j2);
                        if (j2 == 0) {
                            e0 e0Var3 = e0.this;
                            e0Var3.c(e0Var3.f4144i);
                            if (com.applovin.impl.sdk.utils.m.b(f2) && f2.length() >= 4) {
                                e0.this.f4141f.a(f2);
                                e0 e0Var4 = e0.this;
                                e0Var4.b("Switching to backup endpoint " + f2);
                            }
                        }
                        y j3 = this.f4147a.j();
                        e0 e0Var5 = e0.this;
                        j3.a(e0Var5, e0Var5.f4143h, (long) e0.this.f4141f.l());
                        return;
                    }
                    if (f2 == null || !f2.equals(e0.this.f4141f.a())) {
                        e0Var = e0.this;
                        eVar = e0Var.f4144i;
                    } else {
                        e0Var = e0.this;
                        eVar = e0Var.f4145j;
                    }
                    e0Var.c(eVar);
                }
                e0.this.a(i2);
            }

            public void a(T t, int i2) {
                e0.this.f4141f.a(0);
                e0.this.a(t, i2);
            }
        }

        public e0(com.applovin.impl.sdk.network.b<T> bVar, k kVar) {
            this(bVar, kVar, false);
        }

        public e0(com.applovin.impl.sdk.network.b<T> bVar, k kVar, boolean z) {
            super("TaskRepeatRequest", kVar, z);
            this.f4143h = y.b.BACKGROUND;
            this.f4144i = null;
            this.f4145j = null;
            if (bVar != null) {
                this.f4141f = bVar;
                this.f4146k = new a.C0107a();
                this.f4142g = new a(kVar);
                return;
            }
            throw new IllegalArgumentException("No request specified");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.applovin.impl.sdk.c.f.a(com.applovin.impl.sdk.c$e<?>, java.lang.Object):void
         arg types: [com.applovin.impl.sdk.c$e<ST>, ST]
         candidates:
          com.applovin.impl.sdk.c.f.a(java.lang.String, com.applovin.impl.sdk.c$e):com.applovin.impl.sdk.c$e<ST>
          com.applovin.impl.sdk.c.f.a(com.applovin.impl.sdk.c$e<?>, java.lang.Object):void */
        /* access modifiers changed from: private */
        public <ST> void c(c.e<ST> eVar) {
            if (eVar != null) {
                c.f c2 = b().c();
                c2.a((c.e<?>) eVar, (Object) eVar.b());
                c2.a();
            }
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.f4091g;
        }

        public abstract void a(int i2);

        public void a(c.e<String> eVar) {
            this.f4144i = eVar;
        }

        public void a(y.b bVar) {
            this.f4143h = bVar;
        }

        public abstract void a(T t, int i2);

        public void b(c.e<String> eVar) {
            this.f4145j = eVar;
        }

        public void run() {
            int i2;
            com.applovin.impl.sdk.network.a i3 = b().i();
            if (!b().G() && !b().H()) {
                d("AppLovin SDK is disabled: please check your connection");
                q.i("AppLovinSdk", "AppLovin SDK is disabled: please check your connection");
                i2 = -22;
            } else if (!com.applovin.impl.sdk.utils.m.b(this.f4141f.a()) || this.f4141f.a().length() < 4) {
                d("Task has an invalid or null request endpoint.");
                i2 = AppLovinErrorCodes.INVALID_URL;
            } else {
                if (TextUtils.isEmpty(this.f4141f.b())) {
                    this.f4141f.b(this.f4141f.e() != null ? FirebasePerformance.HttpMethod.POST : FirebasePerformance.HttpMethod.GET);
                }
                i3.a(this.f4141f, this.f4146k, this.f4142g);
                return;
            }
            a(i2);
        }
    }

    /* renamed from: com.applovin.impl.sdk.f$f  reason: collision with other inner class name */
    public class C0104f extends c {

        /* renamed from: f  reason: collision with root package name */
        private final Runnable f4149f;

        public C0104f(k kVar, Runnable runnable) {
            this(kVar, false, runnable);
        }

        public C0104f(k kVar, boolean z, Runnable runnable) {
            super("TaskRunnable", kVar, z);
            this.f4149f = runnable;
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.f4092h;
        }

        public void run() {
            this.f4149f.run();
        }
    }

    public class f0 extends g0 {

        /* renamed from: f  reason: collision with root package name */
        private final com.applovin.impl.sdk.ad.f f4150f;

        public f0(com.applovin.impl.sdk.ad.f fVar, k kVar) {
            super("TaskReportAppLovinReward", kVar);
            this.f4150f = fVar;
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.z;
        }

        /* access modifiers changed from: protected */
        public void a(int i2) {
            d("Failed to report reward for ad: " + this.f4150f + " - error code: " + i2);
        }

        /* access modifiers changed from: protected */
        public void a(JSONObject jSONObject) {
            com.applovin.impl.sdk.utils.i.a(jSONObject, "zone_id", this.f4150f.getAdZone().a(), this.f4130a);
            com.applovin.impl.sdk.utils.i.a(jSONObject, "fire_percent", this.f4150f.K(), this.f4130a);
            String clCode = this.f4150f.getClCode();
            if (!com.applovin.impl.sdk.utils.m.b(clCode)) {
                clCode = "NO_CLCODE";
            }
            com.applovin.impl.sdk.utils.i.a(jSONObject, "clcode", clCode, this.f4130a);
        }

        /* access modifiers changed from: protected */
        public void b(JSONObject jSONObject) {
            a("Reported reward successfully for ad: " + this.f4150f);
        }

        /* access modifiers changed from: protected */
        public String f() {
            return "2.0/cr";
        }

        /* access modifiers changed from: protected */
        public com.applovin.impl.sdk.a.c h() {
            return this.f4150f.C();
        }

        /* access modifiers changed from: protected */
        public void i() {
            d("No reward result was found for ad: " + this.f4150f);
        }
    }

    public class g extends h {

        /* renamed from: f  reason: collision with root package name */
        private final com.applovin.impl.sdk.ad.f f4151f;

        /* renamed from: g  reason: collision with root package name */
        private final AppLovinAdRewardListener f4152g;

        public g(com.applovin.impl.sdk.ad.f fVar, AppLovinAdRewardListener appLovinAdRewardListener, k kVar) {
            super("TaskValidateAppLovinReward", kVar);
            this.f4151f = fVar;
            this.f4152g = appLovinAdRewardListener;
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.B;
        }

        /* access modifiers changed from: protected */
        public void a(int i2) {
            String str;
            if (!h()) {
                if (i2 < 400 || i2 >= 500) {
                    this.f4152g.validationRequestFailed(this.f4151f, i2);
                    str = "network_timeout";
                } else {
                    this.f4152g.userRewardRejected(this.f4151f, Collections.emptyMap());
                    str = "rejected";
                }
                this.f4151f.a(com.applovin.impl.sdk.a.c.a(str));
            }
        }

        /* access modifiers changed from: protected */
        public void a(com.applovin.impl.sdk.a.c cVar) {
            if (!h()) {
                this.f4151f.a(cVar);
                String b2 = cVar.b();
                Map<String, String> a2 = cVar.a();
                if (b2.equals("accepted")) {
                    this.f4152g.userRewardVerified(this.f4151f, a2);
                } else if (b2.equals("quota_exceeded")) {
                    this.f4152g.userOverQuota(this.f4151f, a2);
                } else if (b2.equals("rejected")) {
                    this.f4152g.userRewardRejected(this.f4151f, a2);
                } else {
                    this.f4152g.validationRequestFailed(this.f4151f, AppLovinErrorCodes.INCENTIVIZED_UNKNOWN_SERVER_ERROR);
                }
            }
        }

        /* access modifiers changed from: protected */
        public void a(JSONObject jSONObject) {
            com.applovin.impl.sdk.utils.i.a(jSONObject, "zone_id", this.f4151f.getAdZone().a(), this.f4130a);
            String clCode = this.f4151f.getClCode();
            if (!com.applovin.impl.sdk.utils.m.b(clCode)) {
                clCode = "NO_CLCODE";
            }
            com.applovin.impl.sdk.utils.i.a(jSONObject, "clcode", clCode, this.f4130a);
        }

        public String f() {
            return "2.0/vr";
        }

        /* access modifiers changed from: protected */
        public boolean h() {
            return this.f4151f.A();
        }
    }

    public abstract class g0 extends e {

        class a implements a.c<JSONObject> {
            a() {
            }

            public void a(int i2) {
                g0.this.a(i2);
            }

            public void a(JSONObject jSONObject, int i2) {
                g0.this.b(jSONObject);
            }
        }

        protected g0(String str, k kVar) {
            super(str, kVar);
        }

        private JSONObject a(com.applovin.impl.sdk.a.c cVar) {
            JSONObject g2 = g();
            com.applovin.impl.sdk.utils.i.a(g2, "result", cVar.b(), this.f4130a);
            Map<String, String> a2 = cVar.a();
            if (a2 != null) {
                com.applovin.impl.sdk.utils.i.a(g2, NativeProtocol.WEB_DIALOG_PARAMS, new JSONObject(a2), this.f4130a);
            }
            return g2;
        }

        /* access modifiers changed from: protected */
        public abstract void a(int i2);

        /* access modifiers changed from: protected */
        public abstract void b(JSONObject jSONObject);

        /* access modifiers changed from: protected */
        public abstract com.applovin.impl.sdk.a.c h();

        /* access modifiers changed from: protected */
        public abstract void i();

        public void run() {
            com.applovin.impl.sdk.a.c h2 = h();
            if (h2 != null) {
                a(a(h2), new a());
            } else {
                i();
            }
        }
    }

    public abstract class h extends e {

        class a implements a.c<JSONObject> {
            a() {
            }

            public void a(int i2) {
                h.this.a(i2);
            }

            public void a(JSONObject jSONObject, int i2) {
                h.this.b(jSONObject);
            }
        }

        protected h(String str, k kVar) {
            super(str, kVar);
        }

        /* access modifiers changed from: private */
        public void b(JSONObject jSONObject) {
            com.applovin.impl.sdk.a.c c2;
            if (!h() && (c2 = c(jSONObject)) != null) {
                a(c2);
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:6:?, code lost:
            r3 = java.util.Collections.emptyMap();
         */
        /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001b */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private com.applovin.impl.sdk.a.c c(org.json.JSONObject r3) {
            /*
                r2 = this;
                org.json.JSONObject r0 = com.applovin.impl.sdk.utils.h.a(r3)     // Catch:{ JSONException -> 0x002d }
                com.applovin.impl.sdk.k r1 = r2.f4130a     // Catch:{ JSONException -> 0x002d }
                com.applovin.impl.sdk.utils.h.b(r0, r1)     // Catch:{ JSONException -> 0x002d }
                com.applovin.impl.sdk.k r1 = r2.f4130a     // Catch:{ JSONException -> 0x002d }
                com.applovin.impl.sdk.utils.h.a(r3, r1)     // Catch:{ JSONException -> 0x002d }
                java.lang.String r3 = "params"
                java.lang.Object r3 = r0.get(r3)     // Catch:{ all -> 0x001b }
                org.json.JSONObject r3 = (org.json.JSONObject) r3     // Catch:{ all -> 0x001b }
                java.util.Map r3 = com.applovin.impl.sdk.utils.i.a(r3)     // Catch:{ all -> 0x001b }
                goto L_0x001f
            L_0x001b:
                java.util.Map r3 = java.util.Collections.emptyMap()     // Catch:{ JSONException -> 0x002d }
            L_0x001f:
                java.lang.String r1 = "result"
                java.lang.String r0 = r0.getString(r1)     // Catch:{ all -> 0x0026 }
                goto L_0x0028
            L_0x0026:
                java.lang.String r0 = "network_timeout"
            L_0x0028:
                com.applovin.impl.sdk.a.c r3 = com.applovin.impl.sdk.a.c.a(r0, r3)     // Catch:{ JSONException -> 0x002d }
                return r3
            L_0x002d:
                r3 = move-exception
                java.lang.String r0 = "Unable to parse API response"
                r2.a(r0, r3)
                r3 = 0
                return r3
            */
            throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.f.h.c(org.json.JSONObject):com.applovin.impl.sdk.a.c");
        }

        /* access modifiers changed from: protected */
        public abstract void a(int i2);

        /* access modifiers changed from: protected */
        public abstract void a(com.applovin.impl.sdk.a.c cVar);

        /* access modifiers changed from: protected */
        public abstract boolean h();

        public void run() {
            a(g(), new a());
        }
    }

    class i extends c {

        class a extends e0<JSONObject> {
            a(com.applovin.impl.sdk.network.b bVar, k kVar) {
                super(bVar, kVar);
            }

            public void a(int i2) {
                com.applovin.impl.sdk.utils.h.a(i2, this.f4130a);
            }

            public void a(JSONObject jSONObject, int i2) {
                i.this.a(jSONObject);
            }
        }

        i(k kVar) {
            super("TaskApiSubmitData", kVar);
        }

        /* access modifiers changed from: private */
        public void a(JSONObject jSONObject) {
            try {
                this.f4130a.o().c();
                JSONObject a2 = com.applovin.impl.sdk.utils.h.a(jSONObject);
                this.f4130a.c().a(c.e.f4001f, a2.getString("device_id"));
                this.f4130a.c().a(c.e.f4002g, a2.getString("device_token"));
                this.f4130a.c().a();
                com.applovin.impl.sdk.utils.h.b(a2, this.f4130a);
                com.applovin.impl.sdk.utils.h.c(a2, this.f4130a);
                String b2 = com.applovin.impl.sdk.utils.i.b(a2, "latest_version", "", this.f4130a);
                if (e(b2)) {
                    String str = "Current SDK version (" + AppLovinSdk.VERSION + ") is outdated. Please integrate the latest version of the AppLovin SDK (" + b2 + "). Doing so will improve your CPMs and ensure you have access to the latest revenue earning features.";
                    if (com.applovin.impl.sdk.utils.i.a(a2, "sdk_update_message")) {
                        str = com.applovin.impl.sdk.utils.i.b(a2, "sdk_update_message", str, this.f4130a);
                    }
                    q.h("AppLovinSdk", str);
                }
                this.f4130a.k().b();
                this.f4130a.l().b();
            } catch (Throwable th) {
                a("Unable to parse API response", th);
            }
        }

        private void b(JSONObject jSONObject) throws JSONException {
            l n = this.f4130a.n();
            l.d c2 = n.c();
            l.f b2 = n.b();
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("model", b2.f4289a);
            jSONObject2.put("os", b2.f4290b);
            jSONObject2.put("brand", b2.f4292d);
            jSONObject2.put("brand_name", b2.f4293e);
            jSONObject2.put("hardware", b2.f4294f);
            jSONObject2.put("sdk_version", b2.f4296h);
            jSONObject2.put("revision", b2.f4295g);
            jSONObject2.put("adns", (double) b2.m);
            jSONObject2.put("adnsd", b2.n);
            jSONObject2.put("xdpi", String.valueOf(b2.o));
            jSONObject2.put("ydpi", String.valueOf(b2.p));
            jSONObject2.put("screen_size_in", String.valueOf(b2.q));
            jSONObject2.put("gy", com.applovin.impl.sdk.utils.m.a(b2.y));
            jSONObject2.put(TapjoyConstants.TJC_DEVICE_COUNTRY_CODE, b2.f4297i);
            jSONObject2.put("carrier", b2.f4298j);
            jSONObject2.put("orientation_lock", b2.l);
            jSONObject2.put("tz_offset", b2.r);
            jSONObject2.put("aida", String.valueOf(b2.I));
            jSONObject2.put("adr", com.applovin.impl.sdk.utils.m.a(b2.t));
            jSONObject2.put("wvvc", b2.s);
            jSONObject2.put("volume", b2.v);
            jSONObject2.put("type", "android");
            jSONObject2.put("sim", com.applovin.impl.sdk.utils.m.a(b2.x));
            jSONObject2.put("is_tablet", com.applovin.impl.sdk.utils.m.a(b2.z));
            jSONObject2.put("lpm", b2.C);
            jSONObject2.put("tv", com.applovin.impl.sdk.utils.m.a(b2.A));
            jSONObject2.put("vs", com.applovin.impl.sdk.utils.m.a(b2.B));
            jSONObject2.put("fs", b2.E);
            jSONObject2.put("fm", String.valueOf(b2.F.f4301b));
            jSONObject2.put("tm", String.valueOf(b2.F.f4300a));
            jSONObject2.put("lmt", String.valueOf(b2.F.f4302c));
            jSONObject2.put("lm", String.valueOf(b2.F.f4303d));
            g(jSONObject2);
            Boolean bool = b2.G;
            if (bool != null) {
                jSONObject2.put("huc", bool.toString());
            }
            Boolean bool2 = b2.H;
            if (bool2 != null) {
                jSONObject2.put("aru", bool2.toString());
            }
            l.e eVar = b2.u;
            if (eVar != null) {
                jSONObject2.put("act", eVar.f4287a);
                jSONObject2.put("acm", eVar.f4288b);
            }
            String str = b2.w;
            if (com.applovin.impl.sdk.utils.m.b(str)) {
                jSONObject2.put("ua", com.applovin.impl.sdk.utils.m.d(str));
            }
            String str2 = b2.D;
            if (!TextUtils.isEmpty(str2)) {
                jSONObject2.put("so", com.applovin.impl.sdk.utils.m.d(str2));
            }
            Locale locale = b2.f4299k;
            if (locale != null) {
                jSONObject2.put("locale", com.applovin.impl.sdk.utils.m.d(locale.toString()));
            }
            jSONObject.put("device_info", jSONObject2);
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put(InMobiNetworkValues.PACKAGE_NAME, c2.f4282c);
            jSONObject3.put("installer_name", c2.f4283d);
            jSONObject3.put(NativeProtocol.BRIDGE_ARG_APP_NAME_STRING, c2.f4280a);
            jSONObject3.put(TapjoyConstants.TJC_APP_VERSION_NAME, c2.f4281b);
            jSONObject3.put("installed_at", c2.f4286g);
            jSONObject3.put("tg", c2.f4284e);
            jSONObject3.put("applovin_sdk_version", AppLovinSdk.VERSION);
            jSONObject3.put("first_install", String.valueOf(this.f4130a.g()));
            jSONObject3.put("first_install_v2", String.valueOf(!this.f4130a.h()));
            jSONObject3.put(TapjoyConstants.TJC_DEBUG, Boolean.toString(com.applovin.impl.sdk.utils.p.b(this.f4130a)));
            String str3 = (String) this.f4130a.a(c.e.V2);
            if (com.applovin.impl.sdk.utils.m.b(str3)) {
                jSONObject3.put("plugin_version", str3);
            }
            if (((Boolean) this.f4130a.a(c.e.O2)).booleanValue() && com.applovin.impl.sdk.utils.m.b(this.f4130a.M())) {
                jSONObject3.put("cuid", this.f4130a.M());
            }
            if (((Boolean) this.f4130a.a(c.e.R2)).booleanValue()) {
                jSONObject3.put("compass_random_token", this.f4130a.N());
            }
            if (((Boolean) this.f4130a.a(c.e.T2)).booleanValue()) {
                jSONObject3.put("applovin_random_token", this.f4130a.O());
            }
            jSONObject.put("app_info", jSONObject3);
        }

        private void c(JSONObject jSONObject) throws JSONException {
            if (((Boolean) this.f4130a.a(c.e.m3)).booleanValue()) {
                jSONObject.put("stats", this.f4130a.k().c());
            }
            if (((Boolean) this.f4130a.a(c.e.o)).booleanValue()) {
                JSONObject b2 = com.applovin.impl.sdk.network.d.b(d());
                if (b2.length() > 0) {
                    jSONObject.put("network_response_codes", b2);
                }
                if (((Boolean) this.f4130a.a(c.e.p)).booleanValue()) {
                    com.applovin.impl.sdk.network.d.a(d());
                }
            }
        }

        private void d(JSONObject jSONObject) throws JSONException {
            JSONArray a2;
            if (((Boolean) this.f4130a.a(c.e.t3)).booleanValue() && (a2 = this.f4130a.o().a()) != null && a2.length() > 0) {
                jSONObject.put("errors", a2);
            }
        }

        private void e(JSONObject jSONObject) throws JSONException {
            JSONArray a2;
            if (((Boolean) this.f4130a.a(c.e.s3)).booleanValue() && (a2 = this.f4130a.l().a()) != null && a2.length() > 0) {
                jSONObject.put("tasks", a2);
            }
        }

        private boolean e(String str) {
            return com.applovin.impl.sdk.utils.m.b(str) && !AppLovinSdk.VERSION.equals(str) && com.applovin.impl.sdk.utils.p.g(str) > AppLovinSdk.VERSION_CODE;
        }

        private void f(JSONObject jSONObject) {
            a aVar = new a(com.applovin.impl.sdk.network.b.a(this.f4130a).a(com.applovin.impl.sdk.utils.h.a("2.0/device", this.f4130a)).c(com.applovin.impl.sdk.utils.h.b("2.0/device", this.f4130a)).a(com.applovin.impl.sdk.utils.h.a(this.f4130a)).b(FirebasePerformance.HttpMethod.POST).a(jSONObject).a((Object) new JSONObject()).a(((Integer) this.f4130a.a(c.e.x2)).intValue()).a(), this.f4130a);
            aVar.a(c.e.V);
            aVar.b(c.e.W);
            this.f4130a.j().a(aVar);
        }

        private void g(JSONObject jSONObject) {
            try {
                l.c d2 = this.f4130a.n().d();
                String str = d2.f4279b;
                if (com.applovin.impl.sdk.utils.m.b(str)) {
                    jSONObject.put("idfa", str);
                }
                jSONObject.put("dnt", Boolean.toString(d2.f4278a));
            } catch (Throwable th) {
                a("Failed to populate advertising info", th);
            }
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.f4094j;
        }

        public void run() {
            try {
                b("Submitting user data...");
                JSONObject jSONObject = new JSONObject();
                b(jSONObject);
                c(jSONObject);
                d(jSONObject);
                e(jSONObject);
                f(jSONObject);
            } catch (JSONException e2) {
                a("Unable to build JSON message with collected data", e2);
                this.f4130a.l().a(a());
            }
        }
    }

    abstract class j extends c implements l.a {

        /* renamed from: f  reason: collision with root package name */
        protected final com.applovin.impl.sdk.ad.f f4155f;

        /* renamed from: g  reason: collision with root package name */
        private AppLovinAdLoadListener f4156g;

        /* renamed from: h  reason: collision with root package name */
        private final o f4157h;

        /* renamed from: i  reason: collision with root package name */
        private final Collection<Character> f4158i;

        /* renamed from: j  reason: collision with root package name */
        private final com.applovin.impl.sdk.d.e f4159j;

        /* renamed from: k  reason: collision with root package name */
        private boolean f4160k;

        class a implements a.c<String> {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ AtomicReference f4161a;

            /* renamed from: b  reason: collision with root package name */
            final /* synthetic */ String f4162b;

            a(AtomicReference atomicReference, String str) {
                this.f4161a = atomicReference;
                this.f4162b = str;
            }

            public void a(int i2) {
                j jVar = j.this;
                jVar.d("Failed to load resource from '" + this.f4162b + "'");
            }

            public void a(String str, int i2) {
                this.f4161a.set(str);
            }
        }

        j(String str, com.applovin.impl.sdk.ad.f fVar, k kVar, AppLovinAdLoadListener appLovinAdLoadListener) {
            super(str, kVar);
            if (fVar != null) {
                this.f4155f = fVar;
                this.f4156g = appLovinAdLoadListener;
                this.f4157h = kVar.u();
                this.f4158i = j();
                this.f4159j = new com.applovin.impl.sdk.d.e();
                return;
            }
            throw new IllegalArgumentException("No ad specified.");
        }

        private Uri a(Uri uri, String str) {
            String str2;
            StringBuilder sb;
            if (uri != null) {
                String uri2 = uri.toString();
                if (com.applovin.impl.sdk.utils.m.b(uri2)) {
                    a("Caching " + str + " image...");
                    return g(uri2);
                }
                sb = new StringBuilder();
                sb.append("Failed to cache ");
                sb.append(str);
                str2 = " image";
            } else {
                sb = new StringBuilder();
                sb.append("No ");
                sb.append(str);
                str2 = " image to cache";
            }
            sb.append(str2);
            a(sb.toString());
            return null;
        }

        private String a(String str, String str2) {
            StringBuilder sb;
            String replace = str2.replace(Constants.URL_PATH_DELIMITER, "_");
            String g2 = this.f4155f.g();
            if (com.applovin.impl.sdk.utils.m.b(g2)) {
                replace = g2 + replace;
            }
            File a2 = this.f4157h.a(replace, this.f4130a.d());
            if (a2 == null) {
                return null;
            }
            if (a2.exists()) {
                this.f4159j.b(a2.length());
                sb = new StringBuilder();
            } else {
                if (!this.f4157h.a(a2, str + str2, Arrays.asList(str), this.f4159j)) {
                    return null;
                }
                sb = new StringBuilder();
            }
            sb.append("file://");
            sb.append(a2.getAbsolutePath());
            return sb.toString();
        }

        private Uri g(String str) {
            return b(str, this.f4155f.f(), true);
        }

        private Collection<Character> j() {
            HashSet hashSet = new HashSet();
            for (char valueOf : ((String) this.f4130a.a(c.e.E0)).toCharArray()) {
                hashSet.add(Character.valueOf(valueOf));
            }
            hashSet.add('\"');
            return hashSet;
        }

        /* access modifiers changed from: package-private */
        public Uri a(String str, List<String> list, boolean z) {
            String str2;
            try {
                if (com.applovin.impl.sdk.utils.m.b(str)) {
                    a("Caching video " + str + "...");
                    String a2 = this.f4157h.a(d(), str, this.f4155f.g(), list, z, this.f4159j);
                    if (com.applovin.impl.sdk.utils.m.b(a2)) {
                        File a3 = this.f4157h.a(a2, d());
                        if (a3 != null) {
                            Uri fromFile = Uri.fromFile(a3);
                            if (fromFile != null) {
                                a("Finish caching video for ad #" + this.f4155f.getAdIdNumber() + ". Updating ad with cachedVideoFilename = " + a2);
                                return fromFile;
                            }
                            str2 = "Unable to create URI from cached video file = " + a3;
                        } else {
                            str2 = "Unable to cache video = " + str + "Video file was missing or null - please make sure your app has the WRITE_EXTERNAL_STORAGE permission!";
                        }
                    } else if (((Boolean) this.f4130a.a(c.e.H0)).booleanValue()) {
                        d("Failed to cache video");
                        com.applovin.impl.sdk.utils.p.a(this.f4156g, this.f4155f.getAdZone(), AppLovinErrorCodes.UNABLE_TO_PRECACHE_VIDEO_RESOURCES, this.f4130a);
                        this.f4156g = null;
                    } else {
                        str2 = "Failed to cache video, but not failing ad load";
                    }
                    d(str2);
                }
            } catch (Exception e2) {
                a("Encountered exception while attempting to cache video.", e2);
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        public String a(String str, List<String> list) {
            if (com.applovin.impl.sdk.utils.m.b(str)) {
                Uri parse = Uri.parse(str);
                if (parse == null) {
                    a("Nothing to cache, skipping...");
                    return null;
                }
                String lastPathSegment = parse.getLastPathSegment();
                if (com.applovin.impl.sdk.utils.m.b(this.f4155f.g())) {
                    lastPathSegment = this.f4155f.g() + lastPathSegment;
                }
                File a2 = this.f4157h.a(lastPathSegment, d());
                ByteArrayOutputStream a3 = (a2 == null || !a2.exists()) ? null : this.f4157h.a(a2);
                if (a3 == null) {
                    a3 = this.f4157h.a(str, list, true);
                    if (a3 != null) {
                        this.f4157h.a(a3, a2);
                        this.f4159j.a((long) a3.size());
                    }
                } else {
                    this.f4159j.b((long) a3.size());
                }
                try {
                    return a3.toString("UTF-8");
                } catch (UnsupportedEncodingException e2) {
                    a("UTF-8 encoding not supported.", e2);
                } catch (Throwable th) {
                    a("String resource at " + str + " failed to load.", th);
                    return null;
                }
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        public String a(String str, List<String> list, com.applovin.impl.sdk.ad.f fVar) {
            if (!com.applovin.impl.sdk.utils.m.b(str)) {
                return str;
            }
            if (!((Boolean) this.f4130a.a(c.e.G0)).booleanValue()) {
                a("Resource caching is disabled, skipping cache...");
                return str;
            }
            StringBuilder sb = new StringBuilder(str);
            boolean shouldCancelHtmlCachingIfShown = fVar.shouldCancelHtmlCachingIfShown();
            for (String next : list) {
                int i2 = 0;
                int i3 = 0;
                while (i2 < sb.length()) {
                    if (g()) {
                        return str;
                    }
                    i2 = sb.indexOf(next, i3);
                    if (i2 == -1) {
                        continue;
                        break;
                    }
                    int length = sb.length();
                    int i4 = i2;
                    while (!this.f4158i.contains(Character.valueOf(sb.charAt(i4))) && i4 < length) {
                        i4++;
                    }
                    if (i4 <= i2 || i4 == length) {
                        d("Unable to cache resource; ad HTML is invalid.");
                        return str;
                    }
                    String substring = sb.substring(next.length() + i2, i4);
                    if (!com.applovin.impl.sdk.utils.m.b(substring)) {
                        a("Skip caching of non-resource " + substring);
                    } else if (!shouldCancelHtmlCachingIfShown || !fVar.hasShown()) {
                        String a2 = a(next, substring);
                        if (a2 != null) {
                            sb.replace(i2, i4, a2);
                            this.f4159j.e();
                        } else {
                            this.f4159j.f();
                        }
                    } else {
                        a("Cancelling HTML caching due to ad being shown already");
                        this.f4159j.a();
                        return str;
                    }
                    i3 = i4;
                }
            }
            return sb.toString();
        }

        public void a(b.C0087b bVar) {
            if (bVar.p().equalsIgnoreCase(this.f4155f.k())) {
                d("Updating flag for timeout...");
                this.f4160k = true;
            }
            this.f4130a.a().b(this);
        }

        /* access modifiers changed from: package-private */
        public void a(AppLovinAdBase appLovinAdBase) {
            com.applovin.impl.sdk.d.d.a(this.f4159j, appLovinAdBase, this.f4130a);
        }

        /* access modifiers changed from: package-private */
        public Uri b(String str, List<String> list, boolean z) {
            String str2;
            try {
                String a2 = this.f4157h.a(d(), str, this.f4155f.g(), list, z, this.f4159j);
                if (!com.applovin.impl.sdk.utils.m.b(a2)) {
                    return null;
                }
                File a3 = this.f4157h.a(a2, d());
                if (a3 != null) {
                    Uri fromFile = Uri.fromFile(a3);
                    if (fromFile != null) {
                        return fromFile;
                    }
                    str2 = "Unable to extract Uri from image file";
                } else {
                    str2 = "Unable to retrieve File from cached image filename = " + a2;
                }
                d(str2);
                return null;
            } catch (Throwable th) {
                a("Failed to cache image at url = " + str, th);
                return null;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.applovin.impl.sdk.f.j.a(java.lang.String, java.util.List<java.lang.String>, boolean):android.net.Uri
         arg types: [java.lang.String, java.util.List<java.lang.String>, int]
         candidates:
          com.applovin.impl.sdk.f.j.a(java.lang.String, java.util.List<java.lang.String>, com.applovin.impl.sdk.ad.f):java.lang.String
          com.applovin.impl.sdk.f.j.a(java.lang.String, java.util.List<java.lang.String>, boolean):android.net.Uri */
        /* access modifiers changed from: package-private */
        public Uri e(String str) {
            return a(str, this.f4155f.f(), true);
        }

        /* access modifiers changed from: package-private */
        public String f(String str) {
            if (!com.applovin.impl.sdk.utils.m.b(str)) {
                return null;
            }
            com.applovin.impl.sdk.network.b a2 = com.applovin.impl.sdk.network.b.a(this.f4130a).a(str).b(FirebasePerformance.HttpMethod.GET).a((Object) "").a(0).a();
            AtomicReference atomicReference = new AtomicReference(null);
            this.f4130a.i().a(a2, new a.C0107a(), new a(atomicReference, str));
            String str2 = (String) atomicReference.get();
            if (str2 != null) {
                this.f4159j.a((long) str2.length());
            }
            return str2;
        }

        /* access modifiers changed from: protected */
        public void f() {
            this.f4130a.a().b(this);
        }

        /* access modifiers changed from: protected */
        public boolean g() {
            return this.f4160k;
        }

        /* access modifiers changed from: package-private */
        public void h() {
            a("Caching mute images...");
            Uri a2 = a(this.f4155f.b0(), "mute");
            if (a2 != null) {
                this.f4155f.a(a2);
            }
            Uri a3 = a(this.f4155f.c0(), "unmute");
            if (a3 != null) {
                this.f4155f.b(a3);
            }
            a("Ad updated with muteImageFilename = " + this.f4155f.b0() + ", unmuteImageFilename = " + this.f4155f.c0());
        }

        /* access modifiers changed from: package-private */
        public void i() {
            if (this.f4156g != null) {
                a("Rendered new ad:" + this.f4155f);
                this.f4156g.adReceived(this.f4155f);
                this.f4156g = null;
            }
        }

        public void run() {
            if (this.f4155f.j()) {
                a("Subscribing to timeout events...");
                this.f4130a.a().a(this);
            }
        }
    }

    public class k extends j {
        private final com.applovin.impl.sdk.ad.a l;
        private boolean m;
        private boolean n;

        class a implements Runnable {
            a() {
            }

            public void run() {
                k.this.j();
            }
        }

        public k(com.applovin.impl.sdk.ad.a aVar, k kVar, AppLovinAdLoadListener appLovinAdLoadListener) {
            super("TaskCacheAppLovinAd", aVar, kVar, appLovinAdLoadListener);
            this.l = aVar;
        }

        /* access modifiers changed from: private */
        public void j() {
            boolean d0 = this.l.d0();
            boolean z = this.n;
            if (d0 || z) {
                a("Begin caching for streaming ad #" + this.l.getAdIdNumber() + "...");
                h();
                if (d0) {
                    if (this.m) {
                        i();
                    }
                    k();
                    if (!this.m) {
                        i();
                    }
                    l();
                } else {
                    i();
                    k();
                }
            } else {
                a("Begin processing for non-streaming ad #" + this.l.getAdIdNumber() + "...");
                h();
                k();
                l();
                i();
            }
            long currentTimeMillis = System.currentTimeMillis() - this.l.getCreatedAtMillis();
            com.applovin.impl.sdk.d.d.a(this.l, this.f4130a);
            com.applovin.impl.sdk.d.d.a(currentTimeMillis, this.l, this.f4130a);
            a(this.l);
            f();
        }

        private void k() {
            a("Caching HTML resources...");
            this.l.a(a(this.l.v0(), this.l.f(), this.l));
            this.l.a(true);
            a("Finish caching non-video resources for ad #" + this.l.getAdIdNumber());
            q Z = this.f4130a.Z();
            String c2 = c();
            Z.a(c2, "Ad updated with cachedHTML = " + this.l.v0());
        }

        private void l() {
            Uri e2;
            if (!g() && (e2 = e(this.l.x0())) != null) {
                this.l.w0();
                this.l.c(e2);
            }
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.f4095k;
        }

        public void a(boolean z) {
            this.m = z;
        }

        public void b(boolean z) {
            this.n = z;
        }

        public void run() {
            super.run();
            a aVar = new a();
            if (this.f4155f.i()) {
                this.f4130a.j().c().execute(aVar);
            } else {
                aVar.run();
            }
        }
    }

    public class l extends m {
        public l(List<NativeAdImpl> list, k kVar, AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
            super("TaskCacheNativeAdImages", list, kVar, appLovinNativeAdLoadListener);
        }

        public l(List<NativeAdImpl> list, k kVar, AppLovinNativeAdPrecacheListener appLovinNativeAdPrecacheListener) {
            super("TaskCacheNativeAdImages", list, kVar, appLovinNativeAdPrecacheListener);
        }

        private boolean b(NativeAdImpl nativeAdImpl) {
            c("Unable to cache image resource");
            a(nativeAdImpl, !com.applovin.impl.sdk.utils.h.a(d()) ? AppLovinErrorCodes.NO_NETWORK : AppLovinErrorCodes.UNABLE_TO_PRECACHE_IMAGE_RESOURCES);
            return false;
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.l;
        }

        /* access modifiers changed from: protected */
        public void a(NativeAdImpl nativeAdImpl) {
            AppLovinNativeAdPrecacheListener appLovinNativeAdPrecacheListener = this.f4167h;
            if (appLovinNativeAdPrecacheListener != null) {
                appLovinNativeAdPrecacheListener.onNativeAdImagesPrecached(nativeAdImpl);
            }
        }

        /* access modifiers changed from: protected */
        public void a(NativeAdImpl nativeAdImpl, int i2) {
            AppLovinNativeAdPrecacheListener appLovinNativeAdPrecacheListener = this.f4167h;
            if (appLovinNativeAdPrecacheListener != null) {
                appLovinNativeAdPrecacheListener.onNativeAdImagePrecachingFailed(nativeAdImpl, i2);
            }
        }

        /* access modifiers changed from: protected */
        public boolean a(NativeAdImpl nativeAdImpl, o oVar) {
            a("Beginning native ad image caching for #" + nativeAdImpl.getAdId());
            if (((Boolean) this.f4130a.a(c.e.G0)).booleanValue()) {
                String a2 = a(nativeAdImpl.getSourceIconUrl(), oVar, nativeAdImpl.getResourcePrefixes());
                if (a2 == null) {
                    return b(nativeAdImpl);
                }
                nativeAdImpl.setIconUrl(a2);
                String a3 = a(nativeAdImpl.getSourceImageUrl(), oVar, nativeAdImpl.getResourcePrefixes());
                if (a3 == null) {
                    return b(nativeAdImpl);
                }
                nativeAdImpl.setImageUrl(a3);
                return true;
            }
            a("Resource caching is disabled, skipping...");
            return true;
        }
    }

    abstract class m extends c {

        /* renamed from: f  reason: collision with root package name */
        private final List<NativeAdImpl> f4165f;

        /* renamed from: g  reason: collision with root package name */
        private final AppLovinNativeAdLoadListener f4166g;

        /* renamed from: h  reason: collision with root package name */
        protected final AppLovinNativeAdPrecacheListener f4167h;

        /* renamed from: i  reason: collision with root package name */
        private int f4168i;

        m(String str, List<NativeAdImpl> list, k kVar, AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
            super(str, kVar);
            this.f4165f = list;
            this.f4166g = appLovinNativeAdLoadListener;
            this.f4167h = null;
        }

        m(String str, List<NativeAdImpl> list, k kVar, AppLovinNativeAdPrecacheListener appLovinNativeAdPrecacheListener) {
            super(str, kVar);
            if (list != null) {
                this.f4165f = list;
                this.f4166g = null;
                this.f4167h = appLovinNativeAdPrecacheListener;
                return;
            }
            throw new IllegalArgumentException("Native ads cannot be null");
        }

        private void a(int i2) {
            AppLovinNativeAdLoadListener appLovinNativeAdLoadListener = this.f4166g;
            if (appLovinNativeAdLoadListener != null) {
                appLovinNativeAdLoadListener.onNativeAdsFailedToLoad(i2);
            }
        }

        private void a(List<AppLovinNativeAd> list) {
            AppLovinNativeAdLoadListener appLovinNativeAdLoadListener = this.f4166g;
            if (appLovinNativeAdLoadListener != null) {
                appLovinNativeAdLoadListener.onNativeAdsLoaded(list);
            }
        }

        /* access modifiers changed from: protected */
        public String a(String str, o oVar, List<String> list) {
            if (!com.applovin.impl.sdk.utils.m.b(str)) {
                a("Asked to cache file with null/empty URL, nothing to do.");
                return null;
            } else if (!com.applovin.impl.sdk.utils.p.a(str, list)) {
                a("Domain is not whitelisted, skipping precache for URL " + str);
                return null;
            } else {
                try {
                    String a2 = oVar.a(d(), str, null, list, true, true, null);
                    if (a2 != null) {
                        return a2;
                    }
                    c("Unable to cache icon resource " + str);
                    return null;
                } catch (Exception e2) {
                    a("Unable to cache icon resource " + str, e2);
                    return null;
                }
            }
        }

        /* access modifiers changed from: protected */
        public abstract void a(NativeAdImpl nativeAdImpl);

        /* access modifiers changed from: protected */
        public abstract boolean a(NativeAdImpl nativeAdImpl, o oVar);

        public void run() {
            List<NativeAdImpl> list;
            for (NativeAdImpl next : this.f4165f) {
                a("Beginning resource caching phase...");
                if (a(next, this.f4130a.u())) {
                    this.f4168i++;
                    a(next);
                } else {
                    d("Unable to cache resources");
                }
            }
            try {
                if (this.f4168i == this.f4165f.size()) {
                    list = this.f4165f;
                } else if (((Boolean) this.f4130a.a(c.e.m2)).booleanValue()) {
                    d("Mismatch between successful populations and requested size");
                    a(-6);
                    return;
                } else {
                    list = this.f4165f;
                }
                a(list);
            } catch (Throwable th) {
                q.c(c(), "Encountered exception while notifying publisher code", th);
            }
        }
    }

    public class n extends m {
        public n(List<NativeAdImpl> list, k kVar, AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
            super("TaskCacheNativeAdVideos", list, kVar, appLovinNativeAdLoadListener);
        }

        public n(List<NativeAdImpl> list, k kVar, AppLovinNativeAdPrecacheListener appLovinNativeAdPrecacheListener) {
            super("TaskCacheNativeAdVideos", list, kVar, appLovinNativeAdPrecacheListener);
        }

        private boolean b(NativeAdImpl nativeAdImpl) {
            c("Unable to cache video resource " + nativeAdImpl.getSourceVideoUrl());
            a(nativeAdImpl, !com.applovin.impl.sdk.utils.h.a(d()) ? AppLovinErrorCodes.NO_NETWORK : AppLovinErrorCodes.UNABLE_TO_PRECACHE_VIDEO_RESOURCES);
            return false;
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.m;
        }

        /* access modifiers changed from: protected */
        public void a(NativeAdImpl nativeAdImpl) {
            AppLovinNativeAdPrecacheListener appLovinNativeAdPrecacheListener = this.f4167h;
            if (appLovinNativeAdPrecacheListener != null) {
                appLovinNativeAdPrecacheListener.onNativeAdVideoPreceached(nativeAdImpl);
            }
        }

        /* access modifiers changed from: protected */
        public void a(NativeAdImpl nativeAdImpl, int i2) {
            AppLovinNativeAdPrecacheListener appLovinNativeAdPrecacheListener = this.f4167h;
            if (appLovinNativeAdPrecacheListener != null) {
                appLovinNativeAdPrecacheListener.onNativeAdVideoPrecachingFailed(nativeAdImpl, i2);
            }
        }

        /* access modifiers changed from: protected */
        public boolean a(NativeAdImpl nativeAdImpl, o oVar) {
            if (!com.applovin.impl.sdk.utils.m.b(nativeAdImpl.getSourceVideoUrl())) {
                return true;
            }
            a("Beginning native ad video caching" + nativeAdImpl.getAdId());
            if (((Boolean) this.f4130a.a(c.e.G0)).booleanValue()) {
                String a2 = a(nativeAdImpl.getSourceVideoUrl(), oVar, nativeAdImpl.getResourcePrefixes());
                if (a2 == null) {
                    return b(nativeAdImpl);
                }
                nativeAdImpl.setVideoUrl(a2);
            } else {
                a("Resource caching is disabled, skipping...");
            }
            return true;
        }
    }

    class o extends j {
        private final e.c.a.a.a l;

        class a implements Runnable {
            a() {
            }

            public void run() {
                o.this.j();
            }
        }

        public o(e.c.a.a.a aVar, k kVar, AppLovinAdLoadListener appLovinAdLoadListener) {
            super("TaskCacheVastAd", aVar, kVar, appLovinAdLoadListener);
            this.l = aVar;
        }

        /* access modifiers changed from: private */
        public void j() {
            if (this.l.v0()) {
                a("Begin caching for VAST streaming ad #" + this.f4155f.getAdIdNumber() + "...");
                h();
                if (this.l.B0()) {
                    i();
                }
                if (this.l.A0() == a.c.COMPANION_AD) {
                    k();
                    m();
                } else {
                    l();
                }
                if (!this.l.B0()) {
                    i();
                }
                if (this.l.A0() == a.c.COMPANION_AD) {
                    l();
                } else {
                    k();
                    m();
                }
            } else {
                a("Begin caching for VAST ad #" + this.f4155f.getAdIdNumber() + "...");
                h();
                k();
                l();
                m();
                i();
            }
            a("Finished caching VAST ad #" + this.l.getAdIdNumber());
            long currentTimeMillis = System.currentTimeMillis() - this.l.getCreatedAtMillis();
            com.applovin.impl.sdk.d.d.a(this.l, this.f4130a);
            com.applovin.impl.sdk.d.d.a(currentTimeMillis, this.l, this.f4130a);
            a(this.l);
            f();
        }

        private void k() {
            String str;
            String str2;
            String str3;
            if (!g()) {
                if (this.l.y0()) {
                    e.c.a.a.b F0 = this.l.F0();
                    if (F0 != null) {
                        e.c.a.a.e b2 = F0.b();
                        if (b2 != null) {
                            Uri b3 = b2.b();
                            String uri = b3 != null ? b3.toString() : "";
                            String c2 = b2.c();
                            if (URLUtil.isValidUrl(uri) || com.applovin.impl.sdk.utils.m.b(c2)) {
                                if (b2.a() == e.a.STATIC) {
                                    a("Caching static companion ad at " + uri + "...");
                                    Uri b4 = b(uri, Collections.emptyList(), false);
                                    if (b4 != null) {
                                        b2.a(b4);
                                    } else {
                                        str2 = "Failed to cache static companion ad";
                                    }
                                } else if (b2.a() == e.a.HTML) {
                                    if (com.applovin.impl.sdk.utils.m.b(uri)) {
                                        a("Begin caching HTML companion ad. Fetching from " + uri + "...");
                                        c2 = f(uri);
                                        if (com.applovin.impl.sdk.utils.m.b(c2)) {
                                            str3 = "HTML fetched. Caching HTML now...";
                                        } else {
                                            str2 = "Unable to load companion ad resources from " + uri;
                                        }
                                    } else {
                                        str3 = "Caching provided HTML for companion ad. No fetch required. HTML: " + c2;
                                    }
                                    a(str3);
                                    b2.a(a(c2, Collections.emptyList(), this.l));
                                } else if (b2.a() == e.a.IFRAME) {
                                    str = "Skip caching of iFrame resource...";
                                } else {
                                    return;
                                }
                                this.l.a(true);
                                return;
                            }
                            c("Companion ad does not have any resources attached. Skipping...");
                            return;
                        }
                        str2 = "Failed to retrieve non-video resources from companion ad. Skipping...";
                        d(str2);
                        return;
                    }
                    str = "No companion ad provided. Skipping...";
                } else {
                    str = "Companion ad caching disabled. Skipping...";
                }
                a(str);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.applovin.impl.sdk.f.j.a(java.lang.String, java.util.List<java.lang.String>, boolean):android.net.Uri
         arg types: [java.lang.String, java.util.List, int]
         candidates:
          com.applovin.impl.sdk.f.j.a(java.lang.String, java.util.List<java.lang.String>, com.applovin.impl.sdk.ad.f):java.lang.String
          com.applovin.impl.sdk.f.j.a(java.lang.String, java.util.List<java.lang.String>, boolean):android.net.Uri */
        private void l() {
            e.c.a.a.k E0;
            Uri b2;
            if (!g()) {
                if (!this.l.z0()) {
                    a("Video caching disabled. Skipping...");
                } else if (this.l.D0() != null && (E0 = this.l.E0()) != null && (b2 = E0.b()) != null) {
                    Uri a2 = a(b2.toString(), (List<String>) Collections.emptyList(), false);
                    if (a2 != null) {
                        a("Video file successfully cached into: " + a2);
                        E0.a(a2);
                        return;
                    }
                    d("Failed to cache video file: " + E0);
                }
            }
        }

        private void m() {
            String str;
            String str2;
            if (!g()) {
                if (this.l.x0() != null) {
                    a("Begin caching HTML template. Fetching from " + this.l.x0() + "...");
                    str = a(this.l.x0().toString(), this.l.f());
                } else {
                    str = this.l.w0();
                }
                if (com.applovin.impl.sdk.utils.m.b(str)) {
                    e.c.a.a.a aVar = this.l;
                    aVar.a(a(str, aVar.f(), this.l));
                    str2 = "Finish caching HTML template " + this.l.w0() + " for ad #" + this.l.getAdIdNumber();
                } else {
                    str2 = "Unable to load HTML template";
                }
                a(str2);
            }
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.n;
        }

        public void run() {
            super.run();
            a aVar = new a();
            if (this.f4155f.i()) {
                this.f4130a.j().c().execute(aVar);
            } else {
                aVar.run();
            }
        }
    }

    public class p extends c {

        /* renamed from: f  reason: collision with root package name */
        private final a f4170f;

        public interface a {
            void a(l.c cVar);
        }

        public p(k kVar, a aVar) {
            super("TaskCollectAdvertisingId", kVar);
            this.f4170f = aVar;
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.f4088d;
        }

        public void run() {
            this.f4170f.a(this.f4130a.n().d());
        }
    }

    public class q extends c {
        /* access modifiers changed from: private */

        /* renamed from: f  reason: collision with root package name */
        public final com.applovin.impl.sdk.network.g f4171f;
        /* access modifiers changed from: private */

        /* renamed from: g  reason: collision with root package name */
        public final AppLovinPostbackListener f4172g;

        /* renamed from: h  reason: collision with root package name */
        private final y.b f4173h;

        class a extends e0<Object> {
            final /* synthetic */ String l;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            a(com.applovin.impl.sdk.network.b bVar, k kVar, String str) {
                super(bVar, kVar);
                this.l = str;
            }

            public void a(int i2) {
                d("Failed to dispatch postback. Error code: " + i2 + " URL: " + this.l);
                if (q.this.f4172g != null) {
                    q.this.f4172g.onPostbackFailure(this.l, i2);
                }
            }

            public void a(Object obj, int i2) {
                a("Successfully dispatched postback to URL: " + this.l);
                if (((Boolean) this.f4130a.a(c.e.R3)).booleanValue()) {
                    if (obj != null && (obj instanceof JSONObject)) {
                        JSONObject jSONObject = (JSONObject) obj;
                        Iterator<String> it = this.f4130a.b(c.e.Q).iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            if (q.this.f4171f.a().startsWith(it.next())) {
                                a("Updating settings from: " + q.this.f4171f.a());
                                com.applovin.impl.sdk.utils.h.b(jSONObject, this.f4130a);
                                com.applovin.impl.sdk.utils.h.a(jSONObject, this.f4130a);
                                break;
                            }
                        }
                    }
                } else if (obj != null && (obj instanceof String)) {
                    for (String startsWith : this.f4130a.b(c.e.Q)) {
                        if (q.this.f4171f.a().startsWith(startsWith)) {
                            String str = (String) obj;
                            if (!TextUtils.isEmpty(str)) {
                                try {
                                    a("Updating settings from: " + q.this.f4171f.a());
                                    JSONObject jSONObject2 = new JSONObject(str);
                                    com.applovin.impl.sdk.utils.h.b(jSONObject2, this.f4130a);
                                    com.applovin.impl.sdk.utils.h.a(jSONObject2, this.f4130a);
                                    break;
                                } catch (JSONException unused) {
                                    continue;
                                }
                            } else {
                                continue;
                            }
                        }
                    }
                }
                if (q.this.f4172g != null) {
                    q.this.f4172g.onPostbackSuccess(this.l);
                }
            }
        }

        public q(com.applovin.impl.sdk.network.g gVar, y.b bVar, k kVar, AppLovinPostbackListener appLovinPostbackListener) {
            super("TaskDispatchPostback", kVar);
            if (gVar != null) {
                this.f4171f = gVar;
                this.f4172g = appLovinPostbackListener;
                this.f4173h = bVar;
                return;
            }
            throw new IllegalArgumentException("No request specified");
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.f4089e;
        }

        public void run() {
            String a2 = this.f4171f.a();
            if (!com.applovin.impl.sdk.utils.m.b(a2)) {
                b("Requested URL is not valid; nothing to do...");
                AppLovinPostbackListener appLovinPostbackListener = this.f4172g;
                if (appLovinPostbackListener != null) {
                    appLovinPostbackListener.onPostbackFailure(a2, AppLovinErrorCodes.INVALID_URL);
                    return;
                }
                return;
            }
            a aVar = new a(this.f4171f, b(), a2);
            aVar.a(this.f4173h);
            b().j().a(aVar);
        }
    }

    public class r extends c {

        /* renamed from: g  reason: collision with root package name */
        private static int f4174g;
        /* access modifiers changed from: private */

        /* renamed from: f  reason: collision with root package name */
        public final AtomicBoolean f4175f = new AtomicBoolean();

        class a extends e0<JSONObject> {
            a(com.applovin.impl.sdk.network.b bVar, k kVar, boolean z) {
                super(bVar, kVar, z);
            }

            public void a(int i2) {
                d("Unable to fetch basic SDK settings: server returned " + i2);
                r.this.a(new JSONObject());
            }

            public void a(JSONObject jSONObject, int i2) {
                r.this.a(jSONObject);
            }
        }

        private class b extends c {
            public b(k kVar) {
                super("TaskTimeoutFetchBasicSettings", kVar, true);
            }

            public com.applovin.impl.sdk.d.i a() {
                return com.applovin.impl.sdk.d.i.f4093i;
            }

            public void run() {
                if (!r.this.f4175f.get()) {
                    d("Timing out fetch basic settings...");
                    r.this.a(new JSONObject());
                }
            }
        }

        public r(k kVar) {
            super("TaskFetchBasicSettings", kVar, true);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Boolean, com.applovin.impl.sdk.k):java.lang.Boolean
         arg types: [org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.k]
         candidates:
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, float, com.applovin.impl.sdk.k):float
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, long, com.applovin.impl.sdk.k):long
          com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, java.lang.Object, com.applovin.impl.sdk.k):java.lang.Object
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Object, com.applovin.impl.sdk.k):java.lang.Object
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.util.List, com.applovin.impl.sdk.k):java.util.List
          com.applovin.impl.sdk.utils.i.a(org.json.JSONArray, int, org.json.JSONObject, com.applovin.impl.sdk.k):org.json.JSONObject
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, int, com.applovin.impl.sdk.k):void
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.String, com.applovin.impl.sdk.k):void
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONArray, com.applovin.impl.sdk.k):void
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, org.json.JSONObject, com.applovin.impl.sdk.k):void
          com.applovin.impl.sdk.utils.i.a(org.json.JSONObject, java.lang.String, java.lang.Boolean, com.applovin.impl.sdk.k):java.lang.Boolean */
        /* access modifiers changed from: private */
        public void a(JSONObject jSONObject) {
            boolean z = true;
            if (this.f4175f.compareAndSet(false, true)) {
                com.applovin.impl.sdk.utils.h.b(jSONObject, this.f4130a);
                com.applovin.impl.sdk.utils.h.a(jSONObject, this.f4130a);
                if (jSONObject.length() != 0) {
                    z = false;
                }
                com.applovin.impl.sdk.utils.h.a(jSONObject, z, this.f4130a);
                com.applovin.impl.mediation.d.b.e(jSONObject, this.f4130a);
                com.applovin.impl.mediation.d.b.f(jSONObject, this.f4130a);
                b("Executing initialize SDK...");
                this.f4130a.d0().a(com.applovin.impl.sdk.utils.i.a(jSONObject, "smd", (Boolean) false, this.f4130a).booleanValue());
                com.applovin.impl.sdk.utils.h.d(jSONObject, this.f4130a);
                this.f4130a.j().a(new x(this.f4130a));
                com.applovin.impl.sdk.utils.h.c(jSONObject, this.f4130a);
                b("Finished executing initialize SDK");
            }
        }

        private String h() {
            return com.applovin.impl.sdk.utils.h.a((String) this.f4130a.a(c.e.R), "5.0/i", b());
        }

        private String i() {
            return com.applovin.impl.sdk.utils.h.a((String) this.f4130a.a(c.e.S), "5.0/i", b());
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.f4090f;
        }

        /* access modifiers changed from: protected */
        public Map<String, String> f() {
            HashMap hashMap = new HashMap();
            hashMap.put("rid", UUID.randomUUID().toString());
            if (!((Boolean) this.f4130a.a(c.e.G3)).booleanValue()) {
                hashMap.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.f4130a.X());
            }
            Boolean a2 = h.a(d());
            if (a2 != null) {
                hashMap.put("huc", a2.toString());
            }
            Boolean b2 = h.b(d());
            if (b2 != null) {
                hashMap.put("aru", b2.toString());
            }
            return hashMap;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
          ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
          ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
          ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
          ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
        /* access modifiers changed from: protected */
        public JSONObject g() {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("sdk_version", AppLovinSdk.VERSION);
                jSONObject.put("build", String.valueOf(131));
                int i2 = f4174g + 1;
                f4174g = i2;
                jSONObject.put("init_count", String.valueOf(i2));
                jSONObject.put("server_installed_at", com.applovin.impl.sdk.utils.m.d((String) this.f4130a.a(c.e.f4006k)));
                if (this.f4130a.g()) {
                    jSONObject.put("first_install", true);
                }
                if (!this.f4130a.h()) {
                    jSONObject.put("first_install_v2", true);
                }
                String str = (String) this.f4130a.a(c.e.V2);
                if (com.applovin.impl.sdk.utils.m.b(str)) {
                    jSONObject.put("plugin_version", com.applovin.impl.sdk.utils.m.d(str));
                }
                String R = this.f4130a.R();
                if (com.applovin.impl.sdk.utils.m.b(R)) {
                    jSONObject.put("mediation_provider", com.applovin.impl.sdk.utils.m.d(R));
                }
                c.b a2 = com.applovin.impl.mediation.d.c.a(this.f4130a);
                jSONObject.put("installed_mediation_adapters", a2.a());
                jSONObject.put("uninstalled_mediation_adapter_classnames", a2.b());
                l.d c2 = this.f4130a.n().c();
                jSONObject.put(InMobiNetworkValues.PACKAGE_NAME, com.applovin.impl.sdk.utils.m.d(c2.f4282c));
                jSONObject.put(TapjoyConstants.TJC_APP_VERSION_NAME, com.applovin.impl.sdk.utils.m.d(c2.f4281b));
                jSONObject.put(TapjoyConstants.TJC_DEBUG, com.applovin.impl.sdk.utils.m.d(c2.f4285f));
                jSONObject.put(TapjoyConstants.TJC_PLATFORM, "android");
                jSONObject.put("os", com.applovin.impl.sdk.utils.m.d(Build.VERSION.RELEASE));
                jSONObject.put("tg", this.f4130a.a(c.g.f4021i));
                if (((Boolean) this.f4130a.a(c.e.Q2)).booleanValue()) {
                    jSONObject.put("compass_random_token", this.f4130a.N());
                }
                if (((Boolean) this.f4130a.a(c.e.S2)).booleanValue()) {
                    jSONObject.put("applovin_random_token", this.f4130a.O());
                }
            } catch (JSONException e2) {
                a("Failed to construct JSON body", e2);
            }
            return jSONObject;
        }

        public void run() {
            Map<String, String> f2 = f();
            b.a b2 = com.applovin.impl.sdk.network.b.a(this.f4130a).a(h()).c(i()).a(f2).a(g()).b(FirebasePerformance.HttpMethod.POST).a((Object) new JSONObject()).a(((Integer) this.f4130a.a(c.e.A2)).intValue()).c(((Integer) this.f4130a.a(c.e.D2)).intValue()).b(((Integer) this.f4130a.a(c.e.z2)).intValue());
            b2.b(true);
            com.applovin.impl.sdk.network.b a2 = b2.a();
            this.f4130a.j().a(new b(this.f4130a), y.b.TIMEOUT, ((long) ((Integer) this.f4130a.a(c.e.z2)).intValue()) + 250);
            a aVar = new a(a2, this.f4130a, e());
            aVar.a(c.e.T);
            aVar.b(c.e.U);
            this.f4130a.j().a(aVar);
        }
    }

    public class s extends t {

        /* renamed from: i  reason: collision with root package name */
        private final List<String> f4177i;

        public s(List<String> list, AppLovinAdLoadListener appLovinAdLoadListener, k kVar) {
            super(com.applovin.impl.sdk.ad.d.a(a(list), kVar), appLovinAdLoadListener, "TaskFetchMultizoneAd", kVar);
            this.f4177i = Collections.unmodifiableList(list);
        }

        private static String a(List<String> list) {
            if (list != null && !list.isEmpty()) {
                return list.get(0);
            }
            throw new IllegalArgumentException("No zone identifiers specified");
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.o;
        }

        /* access modifiers changed from: package-private */
        public Map<String, String> f() {
            HashMap hashMap = new HashMap(1);
            List<String> list = this.f4177i;
            hashMap.put("zone_ids", com.applovin.impl.sdk.utils.m.d(com.applovin.impl.sdk.utils.e.a(list, list.size())));
            return hashMap;
        }

        /* access modifiers changed from: protected */
        public com.applovin.impl.sdk.ad.b g() {
            return com.applovin.impl.sdk.ad.b.APPLOVIN_MULTIZONE;
        }
    }

    public class t extends c {

        /* renamed from: f  reason: collision with root package name */
        private final com.applovin.impl.sdk.ad.d f4178f;

        /* renamed from: g  reason: collision with root package name */
        private final AppLovinAdLoadListener f4179g;

        /* renamed from: h  reason: collision with root package name */
        private boolean f4180h;

        class a extends e0<JSONObject> {
            a(com.applovin.impl.sdk.network.b bVar, k kVar) {
                super(bVar, kVar);
            }

            public void a(int i2) {
                t.this.b(i2);
            }

            public void a(JSONObject jSONObject, int i2) {
                if (i2 == 200) {
                    com.applovin.impl.sdk.utils.i.b(jSONObject, "ad_fetch_latency_millis", this.f4146k.a(), this.f4130a);
                    com.applovin.impl.sdk.utils.i.b(jSONObject, "ad_fetch_response_size", this.f4146k.b(), this.f4130a);
                    t.this.b(jSONObject);
                    return;
                }
                t.this.b(i2);
            }
        }

        public t(com.applovin.impl.sdk.ad.d dVar, AppLovinAdLoadListener appLovinAdLoadListener, k kVar) {
            this(dVar, appLovinAdLoadListener, "TaskFetchNextAd", kVar);
        }

        t(com.applovin.impl.sdk.ad.d dVar, AppLovinAdLoadListener appLovinAdLoadListener, String str, k kVar) {
            super(str, kVar);
            this.f4180h = false;
            this.f4178f = dVar;
            this.f4179g = appLovinAdLoadListener;
        }

        private void a(com.applovin.impl.sdk.d.h hVar) {
            long b2 = hVar.b(com.applovin.impl.sdk.d.g.f4077f);
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - b2 > TimeUnit.MINUTES.toMillis((long) ((Integer) this.f4130a.a(c.e.H2)).intValue())) {
                hVar.b(com.applovin.impl.sdk.d.g.f4077f, currentTimeMillis);
                hVar.c(com.applovin.impl.sdk.d.g.f4078g);
            }
        }

        /* access modifiers changed from: private */
        public void b(int i2) {
            boolean z = i2 != 204;
            q Z = b().Z();
            String c2 = c();
            Boolean valueOf = Boolean.valueOf(z);
            Z.a(c2, valueOf, "Unable to fetch " + this.f4178f + " ad: server returned " + i2);
            if (i2 == -800) {
                this.f4130a.k().a(com.applovin.impl.sdk.d.g.f4082k);
            }
            try {
                a(i2);
            } catch (Throwable th) {
                q.c(c(), "Unable process a failure to recieve an ad", th);
            }
        }

        /* access modifiers changed from: private */
        public void b(JSONObject jSONObject) {
            com.applovin.impl.sdk.utils.h.b(jSONObject, this.f4130a);
            com.applovin.impl.sdk.utils.h.a(jSONObject, this.f4130a);
            com.applovin.impl.sdk.utils.h.c(jSONObject, this.f4130a);
            c a2 = a(jSONObject);
            if (((Boolean) this.f4130a.a(c.e.K3)).booleanValue()) {
                this.f4130a.j().a(a2);
            } else {
                this.f4130a.j().a(a2, y.b.MAIN);
            }
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.p;
        }

        /* access modifiers changed from: protected */
        public c a(JSONObject jSONObject) {
            return new z(jSONObject, this.f4178f, g(), this.f4179g, this.f4130a);
        }

        /* access modifiers changed from: protected */
        public void a(int i2) {
            AppLovinAdLoadListener appLovinAdLoadListener = this.f4179g;
            if (appLovinAdLoadListener == null) {
                return;
            }
            if (appLovinAdLoadListener instanceof n) {
                ((n) appLovinAdLoadListener).a(this.f4178f, i2);
            } else {
                appLovinAdLoadListener.failedToReceiveAd(i2);
            }
        }

        public void a(boolean z) {
            this.f4180h = z;
        }

        /* access modifiers changed from: package-private */
        public Map<String, String> f() {
            HashMap hashMap = new HashMap(4);
            hashMap.put("zone_id", com.applovin.impl.sdk.utils.m.d(this.f4178f.a()));
            if (this.f4178f.b() != null) {
                hashMap.put("size", this.f4178f.b().getLabel());
            }
            if (this.f4178f.c() != null) {
                hashMap.put("require", this.f4178f.c().getLabel());
            }
            if (((Boolean) this.f4130a.a(c.e.l)).booleanValue()) {
                hashMap.put("n", String.valueOf(this.f4130a.B().a(this.f4178f.a())));
            }
            return hashMap;
        }

        /* access modifiers changed from: protected */
        public com.applovin.impl.sdk.ad.b g() {
            return this.f4178f.j() ? com.applovin.impl.sdk.ad.b.APPLOVIN_PRIMARY_ZONE : com.applovin.impl.sdk.ad.b.APPLOVIN_CUSTOM_ZONE;
        }

        /* access modifiers changed from: protected */
        public String h() {
            return com.applovin.impl.sdk.utils.h.c(this.f4130a);
        }

        /* access modifiers changed from: protected */
        public String i() {
            return com.applovin.impl.sdk.utils.h.d(this.f4130a);
        }

        public void run() {
            String str;
            StringBuilder sb;
            if (this.f4180h) {
                sb = new StringBuilder();
                str = "Preloading next ad of zone: ";
            } else {
                sb = new StringBuilder();
                str = "Fetching next ad of zone: ";
            }
            sb.append(str);
            sb.append(this.f4178f);
            a(sb.toString());
            if (((Boolean) this.f4130a.a(c.e.c3)).booleanValue() && com.applovin.impl.sdk.utils.p.d()) {
                a("User is connected to a VPN");
            }
            com.applovin.impl.sdk.d.h k2 = this.f4130a.k();
            k2.a(com.applovin.impl.sdk.d.g.f4075d);
            if (k2.b(com.applovin.impl.sdk.d.g.f4077f) == 0) {
                k2.b(com.applovin.impl.sdk.d.g.f4077f, System.currentTimeMillis());
            }
            try {
                Map<String, String> a2 = this.f4130a.n().a(f(), this.f4180h, false);
                a(k2);
                b.a b2 = com.applovin.impl.sdk.network.b.a(this.f4130a).a(h()).a(a2).c(i()).b(FirebasePerformance.HttpMethod.GET).a((Object) new JSONObject()).a(((Integer) this.f4130a.a(c.e.w2)).intValue()).b(((Integer) this.f4130a.a(c.e.v2)).intValue());
                b2.b(true);
                a aVar = new a(b2.a(), this.f4130a);
                aVar.a(c.e.T);
                aVar.b(c.e.U);
                this.f4130a.j().a(aVar);
            } catch (Throwable th) {
                a("Unable to fetch ad " + this.f4178f, th);
                b(0);
                this.f4130a.l().a(a());
            }
        }
    }

    public class u extends t {

        /* renamed from: i  reason: collision with root package name */
        private final int f4181i;

        /* renamed from: j  reason: collision with root package name */
        private final AppLovinNativeAdLoadListener f4182j;

        public u(String str, int i2, k kVar, AppLovinNativeAdLoadListener appLovinNativeAdLoadListener) {
            super(com.applovin.impl.sdk.ad.d.b(str, kVar), null, "TaskFetchNextNativeAd", kVar);
            this.f4181i = i2;
            this.f4182j = appLovinNativeAdLoadListener;
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.q;
        }

        /* access modifiers changed from: protected */
        public c a(JSONObject jSONObject) {
            return new c0(jSONObject, this.f4130a, this.f4182j);
        }

        /* access modifiers changed from: protected */
        public void a(int i2) {
            AppLovinNativeAdLoadListener appLovinNativeAdLoadListener = this.f4182j;
            if (appLovinNativeAdLoadListener != null) {
                appLovinNativeAdLoadListener.onNativeAdsFailedToLoad(i2);
            }
        }

        /* access modifiers changed from: package-private */
        public Map<String, String> f() {
            Map<String, String> f2 = super.f();
            f2.put("slot_count", Integer.toString(this.f4181i));
            return f2;
        }

        /* access modifiers changed from: protected */
        public String h() {
            return ((String) this.f4130a.a(c.e.T)) + "4.0/nad";
        }

        /* access modifiers changed from: protected */
        public String i() {
            return ((String) this.f4130a.a(c.e.U)) + "4.0/nad";
        }
    }

    public class v extends t {

        /* renamed from: i  reason: collision with root package name */
        private final com.applovin.impl.sdk.ad.c f4183i;

        public v(com.applovin.impl.sdk.ad.c cVar, AppLovinAdLoadListener appLovinAdLoadListener, k kVar) {
            super(com.applovin.impl.sdk.ad.d.a("adtoken_zone", kVar), appLovinAdLoadListener, "TaskFetchTokenAd", kVar);
            this.f4183i = cVar;
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.r;
        }

        /* access modifiers changed from: package-private */
        public Map<String, String> f() {
            HashMap hashMap = new HashMap(2);
            hashMap.put("adtoken", com.applovin.impl.sdk.utils.m.d(this.f4183i.a()));
            hashMap.put("adtoken_prefix", com.applovin.impl.sdk.utils.m.d(this.f4183i.c()));
            return hashMap;
        }

        /* access modifiers changed from: protected */
        public com.applovin.impl.sdk.ad.b g() {
            return com.applovin.impl.sdk.ad.b.REGULAR_AD_TOKEN;
        }
    }

    public class w extends c {
        /* access modifiers changed from: private */

        /* renamed from: f  reason: collision with root package name */
        public final b f4184f;

        class a extends e0<JSONObject> {
            a(com.applovin.impl.sdk.network.b bVar, k kVar) {
                super(bVar, kVar);
            }

            public void a(int i2) {
                d("Unable to fetch variables: server returned " + i2);
                q.i("AppLovinVariableService", "Failed to load variables.");
                w.this.f4184f.a();
            }

            public void a(JSONObject jSONObject, int i2) {
                com.applovin.impl.sdk.utils.h.b(jSONObject, this.f4130a);
                com.applovin.impl.sdk.utils.h.a(jSONObject, this.f4130a);
                com.applovin.impl.sdk.utils.h.d(jSONObject, this.f4130a);
                w.this.f4184f.a();
            }
        }

        public interface b {
            void a();
        }

        public w(k kVar, b bVar) {
            super("TaskFetchVariables", kVar);
            this.f4184f = bVar;
        }

        private void a(Map<String, String> map) {
            try {
                l.c d2 = this.f4130a.n().d();
                String str = d2.f4279b;
                if (com.applovin.impl.sdk.utils.m.b(str)) {
                    map.put("idfa", str);
                }
                map.put("dnt", Boolean.toString(d2.f4278a));
            } catch (Throwable th) {
                a("Failed to populate advertising info", th);
            }
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.s;
        }

        /* access modifiers changed from: protected */
        public Map<String, String> f() {
            l n = this.f4130a.n();
            l.f b2 = n.b();
            l.d c2 = n.c();
            HashMap hashMap = new HashMap();
            hashMap.put(TapjoyConstants.TJC_PLATFORM, com.applovin.impl.sdk.utils.m.d(b2.f4291c));
            hashMap.put("model", com.applovin.impl.sdk.utils.m.d(b2.f4289a));
            hashMap.put(InMobiNetworkValues.PACKAGE_NAME, com.applovin.impl.sdk.utils.m.d(c2.f4282c));
            hashMap.put("installer_name", com.applovin.impl.sdk.utils.m.d(c2.f4283d));
            hashMap.put("ia", Long.toString(c2.f4286g));
            hashMap.put("api_did", this.f4130a.a(c.e.f4001f));
            hashMap.put("brand", com.applovin.impl.sdk.utils.m.d(b2.f4292d));
            hashMap.put("brand_name", com.applovin.impl.sdk.utils.m.d(b2.f4293e));
            hashMap.put("hardware", com.applovin.impl.sdk.utils.m.d(b2.f4294f));
            hashMap.put("revision", com.applovin.impl.sdk.utils.m.d(b2.f4295g));
            hashMap.put("sdk_version", AppLovinSdk.VERSION);
            hashMap.put("os", com.applovin.impl.sdk.utils.m.d(b2.f4290b));
            hashMap.put("orientation_lock", b2.l);
            hashMap.put(TapjoyConstants.TJC_APP_VERSION_NAME, com.applovin.impl.sdk.utils.m.d(c2.f4281b));
            hashMap.put(TapjoyConstants.TJC_DEVICE_COUNTRY_CODE, com.applovin.impl.sdk.utils.m.d(b2.f4297i));
            hashMap.put("carrier", com.applovin.impl.sdk.utils.m.d(b2.f4298j));
            hashMap.put("tz_offset", String.valueOf(b2.r));
            hashMap.put("aida", String.valueOf(b2.I));
            String str = "1";
            hashMap.put("adr", b2.t ? str : AppEventsConstants.EVENT_PARAM_VALUE_NO);
            hashMap.put("volume", String.valueOf(b2.v));
            if (!b2.x) {
                str = AppEventsConstants.EVENT_PARAM_VALUE_NO;
            }
            hashMap.put("sim", str);
            hashMap.put("gy", String.valueOf(b2.y));
            hashMap.put("is_tablet", String.valueOf(b2.z));
            hashMap.put("tv", String.valueOf(b2.A));
            hashMap.put("vs", String.valueOf(b2.B));
            hashMap.put("lpm", String.valueOf(b2.C));
            hashMap.put("tg", c2.f4284e);
            hashMap.put("fs", String.valueOf(b2.E));
            hashMap.put("fm", String.valueOf(b2.F.f4301b));
            hashMap.put("tm", String.valueOf(b2.F.f4300a));
            hashMap.put("lmt", String.valueOf(b2.F.f4302c));
            hashMap.put("lm", String.valueOf(b2.F.f4303d));
            hashMap.put("adns", String.valueOf(b2.m));
            hashMap.put("adnsd", String.valueOf(b2.n));
            hashMap.put("xdpi", String.valueOf(b2.o));
            hashMap.put("ydpi", String.valueOf(b2.p));
            hashMap.put("screen_size_in", String.valueOf(b2.q));
            hashMap.put(TapjoyConstants.TJC_DEBUG, Boolean.toString(com.applovin.impl.sdk.utils.p.b(this.f4130a)));
            if (!((Boolean) this.f4130a.a(c.e.G3)).booleanValue()) {
                hashMap.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.f4130a.X());
            }
            a(hashMap);
            if (((Boolean) this.f4130a.a(c.e.O2)).booleanValue()) {
                com.applovin.impl.sdk.utils.p.a("cuid", this.f4130a.M(), hashMap);
            }
            if (((Boolean) this.f4130a.a(c.e.R2)).booleanValue()) {
                hashMap.put("compass_random_token", this.f4130a.N());
            }
            if (((Boolean) this.f4130a.a(c.e.T2)).booleanValue()) {
                hashMap.put("applovin_random_token", this.f4130a.O());
            }
            Boolean bool = b2.G;
            if (bool != null) {
                hashMap.put("huc", bool.toString());
            }
            Boolean bool2 = b2.H;
            if (bool2 != null) {
                hashMap.put("aru", bool2.toString());
            }
            l.e eVar = b2.u;
            if (eVar != null) {
                hashMap.put("act", String.valueOf(eVar.f4287a));
                hashMap.put("acm", String.valueOf(eVar.f4288b));
            }
            String str2 = b2.w;
            if (com.applovin.impl.sdk.utils.m.b(str2)) {
                hashMap.put("ua", com.applovin.impl.sdk.utils.m.d(str2));
            }
            String str3 = b2.D;
            if (!TextUtils.isEmpty(str3)) {
                hashMap.put("so", com.applovin.impl.sdk.utils.m.d(str3));
            }
            hashMap.put("sc", com.applovin.impl.sdk.utils.m.d((String) this.f4130a.a(c.e.f4004i)));
            hashMap.put("sc2", com.applovin.impl.sdk.utils.m.d((String) this.f4130a.a(c.e.f4005j)));
            hashMap.put("server_installed_at", com.applovin.impl.sdk.utils.m.d((String) this.f4130a.a(c.e.f4006k)));
            com.applovin.impl.sdk.utils.p.a("persisted_data", com.applovin.impl.sdk.utils.m.d((String) this.f4130a.a(c.g.z)), hashMap);
            return hashMap;
        }

        public void run() {
            a aVar = new a(com.applovin.impl.sdk.network.b.a(this.f4130a).a(com.applovin.impl.sdk.utils.h.e(this.f4130a)).c(com.applovin.impl.sdk.utils.h.f(this.f4130a)).a(f()).b(FirebasePerformance.HttpMethod.GET).a((Object) new JSONObject()).b(((Integer) this.f4130a.a(c.e.E2)).intValue()).a(), this.f4130a);
            aVar.a(c.e.a0);
            aVar.b(c.e.b0);
            this.f4130a.j().a(aVar);
        }
    }

    public class x extends c {
        /* access modifiers changed from: private */

        /* renamed from: f  reason: collision with root package name */
        public final k f4185f;

        class a implements Runnable {
            a() {
            }

            public void run() {
                x.this.f4185f.b0().a(x.this.f4185f.A().a());
            }
        }

        public x(k kVar) {
            super("TaskInitializeSdk", kVar);
            this.f4185f = kVar;
        }

        private void a(c.e<Boolean> eVar) {
            if (((Boolean) this.f4185f.a(eVar)).booleanValue()) {
                this.f4185f.s().f(com.applovin.impl.sdk.ad.d.a(AppLovinAdSize.INTERSTITIAL, AppLovinAdType.INCENTIVIZED, this.f4185f));
            }
        }

        private void f() {
            if (!this.f4185f.b0().a()) {
                Activity E = this.f4185f.E();
                if (E != null) {
                    this.f4185f.b0().a(E);
                } else {
                    this.f4185f.j().a(new C0104f(this.f4185f, true, new a()), y.b.MAIN, TimeUnit.SECONDS.toMillis(1));
                }
            }
        }

        private void g() {
            this.f4185f.j().a(new i(this.f4185f), y.b.MAIN);
        }

        private void h() {
            this.f4185f.s().a();
            this.f4185f.t().a();
        }

        private void i() {
            j();
            k();
            l();
        }

        private void j() {
            LinkedHashSet<com.applovin.impl.sdk.ad.d> a2 = this.f4185f.v().a();
            if (!a2.isEmpty()) {
                a("Scheduling preload(s) for " + a2.size() + " zone(s)");
                Iterator<com.applovin.impl.sdk.ad.d> it = a2.iterator();
                while (it.hasNext()) {
                    com.applovin.impl.sdk.ad.d next = it.next();
                    if (next.d()) {
                        this.f4185f.T().preloadAds(next);
                    } else {
                        this.f4185f.S().preloadAds(next);
                    }
                }
            }
        }

        private void k() {
            c.e<Boolean> eVar = c.e.o0;
            String str = (String) this.f4185f.a(c.e.n0);
            boolean z = false;
            if (str.length() > 0) {
                for (String fromString : com.applovin.impl.sdk.utils.e.a(str)) {
                    AppLovinAdSize fromString2 = AppLovinAdSize.fromString(fromString);
                    if (fromString2 != null) {
                        this.f4185f.s().f(com.applovin.impl.sdk.ad.d.a(fromString2, AppLovinAdType.REGULAR, this.f4185f));
                        if (AppLovinAdSize.INTERSTITIAL.getLabel().equals(fromString2.getLabel())) {
                            a(eVar);
                            z = true;
                        }
                    }
                }
            }
            if (!z) {
                a(eVar);
            }
        }

        private void l() {
            if (((Boolean) this.f4185f.a(c.e.p0)).booleanValue()) {
                this.f4185f.t().f(com.applovin.impl.sdk.ad.d.h(this.f4185f));
            }
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.f4087c;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:18:0x013d, code lost:
            if (r12.f4185f.H() == false) goto L_0x0140;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0140, code lost:
            r2 = "failed";
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0141, code lost:
            r8.append(r2);
            r8.append(" in ");
            r8.append(java.lang.System.currentTimeMillis() - r6);
            r8.append("ms");
            a(r8.toString());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0159, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x00e8, code lost:
            if (r12.f4185f.H() != false) goto L_0x0141;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r12 = this;
                java.lang.String r0 = "ms"
                java.lang.String r1 = " in "
                java.lang.String r2 = "succeeded"
                java.lang.String r3 = "failed"
                java.lang.String r4 = " initialization "
                java.lang.String r5 = "AppLovin SDK "
                long r6 = java.lang.System.currentTimeMillis()
                java.lang.StringBuilder r8 = new java.lang.StringBuilder
                r8.<init>()
                java.lang.String r9 = "Initializing AppLovin SDK "
                r8.append(r9)
                java.lang.String r9 = com.applovin.sdk.AppLovinSdk.VERSION
                r8.append(r9)
                java.lang.String r9 = "..."
                r8.append(r9)
                java.lang.String r8 = r8.toString()
                r12.a(r8)
                com.applovin.impl.sdk.k r8 = r12.f4185f     // Catch:{ all -> 0x00eb }
                com.applovin.impl.sdk.d.h r8 = r8.k()     // Catch:{ all -> 0x00eb }
                r8.d()     // Catch:{ all -> 0x00eb }
                com.applovin.impl.sdk.k r8 = r12.f4185f     // Catch:{ all -> 0x00eb }
                com.applovin.impl.sdk.d.h r8 = r8.k()     // Catch:{ all -> 0x00eb }
                com.applovin.impl.sdk.d.g r9 = com.applovin.impl.sdk.d.g.f4076e     // Catch:{ all -> 0x00eb }
                r8.c(r9)     // Catch:{ all -> 0x00eb }
                com.applovin.impl.sdk.k r8 = r12.f4185f     // Catch:{ all -> 0x00eb }
                com.applovin.impl.sdk.o r8 = r8.u()     // Catch:{ all -> 0x00eb }
                android.content.Context r9 = r12.d()     // Catch:{ all -> 0x00eb }
                r8.a(r9)     // Catch:{ all -> 0x00eb }
                com.applovin.impl.sdk.k r8 = r12.f4185f     // Catch:{ all -> 0x00eb }
                com.applovin.impl.sdk.o r8 = r8.u()     // Catch:{ all -> 0x00eb }
                android.content.Context r9 = r12.d()     // Catch:{ all -> 0x00eb }
                r8.b(r9)     // Catch:{ all -> 0x00eb }
                r12.h()     // Catch:{ all -> 0x00eb }
                r12.i()     // Catch:{ all -> 0x00eb }
                r12.f()     // Catch:{ all -> 0x00eb }
                com.applovin.impl.sdk.k r8 = r12.f4185f     // Catch:{ all -> 0x00eb }
                com.applovin.impl.sdk.d.c r8 = r8.w()     // Catch:{ all -> 0x00eb }
                r8.a()     // Catch:{ all -> 0x00eb }
                r12.g()     // Catch:{ all -> 0x00eb }
                com.applovin.impl.sdk.k r8 = r12.f4185f     // Catch:{ all -> 0x00eb }
                com.applovin.impl.sdk.l r8 = r8.n()     // Catch:{ all -> 0x00eb }
                r8.e()     // Catch:{ all -> 0x00eb }
                com.applovin.impl.sdk.k r8 = r12.f4185f     // Catch:{ all -> 0x00eb }
                r9 = 1
                r8.a(r9)     // Catch:{ all -> 0x00eb }
                com.applovin.impl.sdk.k r8 = r12.f4185f     // Catch:{ all -> 0x00eb }
                com.applovin.impl.sdk.network.e r8 = r8.m()     // Catch:{ all -> 0x00eb }
                r8.a()     // Catch:{ all -> 0x00eb }
                com.applovin.impl.sdk.k r8 = r12.f4185f     // Catch:{ all -> 0x00eb }
                com.applovin.sdk.AppLovinEventService r8 = r8.U()     // Catch:{ all -> 0x00eb }
                com.applovin.impl.sdk.EventServiceImpl r8 = (com.applovin.impl.sdk.EventServiceImpl) r8     // Catch:{ all -> 0x00eb }
                r8.maybeTrackAppOpenEvent()     // Catch:{ all -> 0x00eb }
                com.applovin.impl.sdk.k r8 = r12.f4185f     // Catch:{ all -> 0x00eb }
                com.applovin.impl.mediation.k r8 = r8.b()     // Catch:{ all -> 0x00eb }
                r8.a()     // Catch:{ all -> 0x00eb }
                com.applovin.impl.sdk.k r8 = r12.f4185f     // Catch:{ all -> 0x00eb }
                com.applovin.impl.mediation.a$b r8 = r8.d0()     // Catch:{ all -> 0x00eb }
                boolean r8 = r8.a()     // Catch:{ all -> 0x00eb }
                if (r8 == 0) goto L_0x00af
                com.applovin.impl.sdk.k r8 = r12.f4185f     // Catch:{ all -> 0x00eb }
                com.applovin.impl.mediation.a$b r8 = r8.d0()     // Catch:{ all -> 0x00eb }
                r8.b()     // Catch:{ all -> 0x00eb }
            L_0x00af:
                com.applovin.impl.sdk.k r8 = r12.f4185f
                com.applovin.impl.sdk.c$e<java.lang.Boolean> r9 = com.applovin.impl.sdk.c.e.D
                java.lang.Object r8 = r8.a(r9)
                java.lang.Boolean r8 = (java.lang.Boolean) r8
                boolean r8 = r8.booleanValue()
                if (r8 == 0) goto L_0x00d2
                com.applovin.impl.sdk.k r8 = r12.f4185f
                com.applovin.impl.sdk.c$e<java.lang.Long> r9 = com.applovin.impl.sdk.c.e.E
                java.lang.Object r8 = r8.a(r9)
                java.lang.Long r8 = (java.lang.Long) r8
                long r8 = r8.longValue()
                com.applovin.impl.sdk.k r10 = r12.f4185f
                r10.a(r8)
            L_0x00d2:
                java.lang.StringBuilder r8 = new java.lang.StringBuilder
                r8.<init>()
                r8.append(r5)
                java.lang.String r5 = com.applovin.sdk.AppLovinSdk.VERSION
                r8.append(r5)
                r8.append(r4)
                com.applovin.impl.sdk.k r4 = r12.f4185f
                boolean r4 = r4.H()
                if (r4 == 0) goto L_0x0140
                goto L_0x0141
            L_0x00eb:
                r8 = move-exception
                java.lang.String r9 = "Unable to initialize SDK."
                r12.a(r9, r8)     // Catch:{ all -> 0x015a }
                com.applovin.impl.sdk.k r8 = r12.f4185f     // Catch:{ all -> 0x015a }
                r9 = 0
                r8.a(r9)     // Catch:{ all -> 0x015a }
                com.applovin.impl.sdk.k r8 = r12.f4185f     // Catch:{ all -> 0x015a }
                com.applovin.impl.sdk.d.j r8 = r8.l()     // Catch:{ all -> 0x015a }
                com.applovin.impl.sdk.d.i r9 = r12.a()     // Catch:{ all -> 0x015a }
                r8.a(r9)     // Catch:{ all -> 0x015a }
                com.applovin.impl.sdk.k r8 = r12.f4185f
                com.applovin.impl.sdk.c$e<java.lang.Boolean> r9 = com.applovin.impl.sdk.c.e.D
                java.lang.Object r8 = r8.a(r9)
                java.lang.Boolean r8 = (java.lang.Boolean) r8
                boolean r8 = r8.booleanValue()
                if (r8 == 0) goto L_0x0127
                com.applovin.impl.sdk.k r8 = r12.f4185f
                com.applovin.impl.sdk.c$e<java.lang.Long> r9 = com.applovin.impl.sdk.c.e.E
                java.lang.Object r8 = r8.a(r9)
                java.lang.Long r8 = (java.lang.Long) r8
                long r8 = r8.longValue()
                com.applovin.impl.sdk.k r10 = r12.f4185f
                r10.a(r8)
            L_0x0127:
                java.lang.StringBuilder r8 = new java.lang.StringBuilder
                r8.<init>()
                r8.append(r5)
                java.lang.String r5 = com.applovin.sdk.AppLovinSdk.VERSION
                r8.append(r5)
                r8.append(r4)
                com.applovin.impl.sdk.k r4 = r12.f4185f
                boolean r4 = r4.H()
                if (r4 == 0) goto L_0x0140
                goto L_0x0141
            L_0x0140:
                r2 = r3
            L_0x0141:
                r8.append(r2)
                r8.append(r1)
                long r1 = java.lang.System.currentTimeMillis()
                long r1 = r1 - r6
                r8.append(r1)
                r8.append(r0)
                java.lang.String r0 = r8.toString()
                r12.a(r0)
                return
            L_0x015a:
                r8 = move-exception
                com.applovin.impl.sdk.k r9 = r12.f4185f
                com.applovin.impl.sdk.c$e<java.lang.Boolean> r10 = com.applovin.impl.sdk.c.e.D
                java.lang.Object r9 = r9.a(r10)
                java.lang.Boolean r9 = (java.lang.Boolean) r9
                boolean r9 = r9.booleanValue()
                if (r9 == 0) goto L_0x017e
                com.applovin.impl.sdk.k r9 = r12.f4185f
                com.applovin.impl.sdk.c$e<java.lang.Long> r10 = com.applovin.impl.sdk.c.e.E
                java.lang.Object r9 = r9.a(r10)
                java.lang.Long r9 = (java.lang.Long) r9
                long r9 = r9.longValue()
                com.applovin.impl.sdk.k r11 = r12.f4185f
                r11.a(r9)
            L_0x017e:
                java.lang.StringBuilder r9 = new java.lang.StringBuilder
                r9.<init>()
                r9.append(r5)
                java.lang.String r5 = com.applovin.sdk.AppLovinSdk.VERSION
                r9.append(r5)
                r9.append(r4)
                com.applovin.impl.sdk.k r4 = r12.f4185f
                boolean r4 = r4.H()
                if (r4 == 0) goto L_0x0197
                goto L_0x0198
            L_0x0197:
                r2 = r3
            L_0x0198:
                r9.append(r2)
                r9.append(r1)
                long r1 = java.lang.System.currentTimeMillis()
                long r1 = r1 - r6
                r9.append(r1)
                r9.append(r0)
                java.lang.String r0 = r9.toString()
                r12.a(r0)
                throw r8
            */
            throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.f.x.run():void");
        }
    }

    public class y {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final k f4187a;
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public final q f4188b;

        /* renamed from: c  reason: collision with root package name */
        private final ScheduledThreadPoolExecutor f4189c;

        /* renamed from: d  reason: collision with root package name */
        private final ScheduledThreadPoolExecutor f4190d;

        /* renamed from: e  reason: collision with root package name */
        private final ScheduledThreadPoolExecutor f4191e;

        /* renamed from: f  reason: collision with root package name */
        private final ScheduledThreadPoolExecutor f4192f;

        /* renamed from: g  reason: collision with root package name */
        private final ScheduledThreadPoolExecutor f4193g;

        /* renamed from: h  reason: collision with root package name */
        private final ScheduledThreadPoolExecutor f4194h;

        /* renamed from: i  reason: collision with root package name */
        private final ScheduledThreadPoolExecutor f4195i;

        /* renamed from: j  reason: collision with root package name */
        private final ScheduledThreadPoolExecutor f4196j;

        /* renamed from: k  reason: collision with root package name */
        private final ScheduledThreadPoolExecutor f4197k;
        private final ScheduledThreadPoolExecutor l;
        private final ScheduledThreadPoolExecutor m;
        private final ScheduledThreadPoolExecutor n;
        private final ScheduledThreadPoolExecutor o;
        private final ScheduledThreadPoolExecutor p;
        private final ScheduledThreadPoolExecutor q;
        private final ScheduledThreadPoolExecutor r;
        private final ScheduledThreadPoolExecutor s;
        private final ScheduledThreadPoolExecutor t;
        private final ScheduledThreadPoolExecutor u;
        private final ScheduledThreadPoolExecutor v;
        private final List<d> w = new ArrayList(5);
        private final Object x = new Object();
        private boolean y;

        class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ ScheduledExecutorService f4198a;

            /* renamed from: b  reason: collision with root package name */
            final /* synthetic */ Runnable f4199b;

            a(y yVar, ScheduledExecutorService scheduledExecutorService, Runnable runnable) {
                this.f4198a = scheduledExecutorService;
                this.f4199b = runnable;
            }

            public void run() {
                this.f4198a.execute(this.f4199b);
            }
        }

        public enum b {
            MAIN,
            TIMEOUT,
            BACKGROUND,
            ADVERTISING_INFO_COLLECTION,
            POSTBACKS,
            CACHING_INTERSTITIAL,
            CACHING_INCENTIVIZED,
            CACHING_OTHER,
            REWARD,
            MEDIATION_MAIN,
            MEDIATION_TIMEOUT,
            MEDIATION_BACKGROUND,
            MEDIATION_POSTBACKS,
            MEDIATION_BANNER,
            MEDIATION_INTERSTITIAL,
            MEDIATION_INCENTIVIZED,
            MEDIATION_REWARD
        }

        private class c implements ThreadFactory {

            /* renamed from: a  reason: collision with root package name */
            private final String f4211a;

            class a implements Thread.UncaughtExceptionHandler {
                a() {
                }

                public void uncaughtException(Thread thread, Throwable th) {
                    y.this.f4188b.b("TaskManager", "Caught unhandled exception", th);
                }
            }

            c(String str) {
                this.f4211a = str;
            }

            public Thread newThread(Runnable runnable) {
                Thread thread = new Thread(runnable, "AppLovinSdk:" + this.f4211a + ":" + com.applovin.impl.sdk.utils.p.a(y.this.f4187a.X()));
                thread.setDaemon(true);
                thread.setPriority(10);
                thread.setUncaughtExceptionHandler(new a());
                return thread;
            }
        }

        private class d implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            private final String f4214a;
            /* access modifiers changed from: private */

            /* renamed from: b  reason: collision with root package name */
            public final c f4215b;
            /* access modifiers changed from: private */

            /* renamed from: c  reason: collision with root package name */
            public final b f4216c;

            d(c cVar, b bVar) {
                this.f4214a = cVar.c();
                this.f4215b = cVar;
                this.f4216c = bVar;
            }

            public void run() {
                long a2;
                q b2;
                StringBuilder sb;
                long currentTimeMillis = System.currentTimeMillis();
                try {
                    com.applovin.impl.sdk.utils.g.a();
                    if (y.this.f4187a.G()) {
                        if (!this.f4215b.e()) {
                            y.this.f4188b.c(this.f4214a, "Task re-scheduled...");
                            y.this.a(this.f4215b, this.f4216c, 2000);
                            a2 = y.this.a(this.f4216c) - 1;
                            b2 = y.this.f4188b;
                            sb = new StringBuilder();
                            sb.append(this.f4216c);
                            sb.append(" queue finished task ");
                            sb.append(this.f4215b.c());
                            sb.append(" with queue size ");
                            sb.append(a2);
                            b2.c("TaskManager", sb.toString());
                        }
                    }
                    y.this.f4188b.c(this.f4214a, "Task started execution...");
                    this.f4215b.run();
                    long currentTimeMillis2 = System.currentTimeMillis() - currentTimeMillis;
                    y.this.f4187a.l().a(this.f4215b.a(), currentTimeMillis2);
                    q b3 = y.this.f4188b;
                    String str = this.f4214a;
                    b3.c(str, "Task executed successfully in " + currentTimeMillis2 + "ms.");
                    a2 = y.this.a(this.f4216c) - 1;
                    b2 = y.this.f4188b;
                    sb = new StringBuilder();
                } catch (Throwable th) {
                    q b4 = y.this.f4188b;
                    b4.c("TaskManager", this.f4216c + " queue finished task " + this.f4215b.c() + " with queue size " + (y.this.a(this.f4216c) - 1));
                    throw th;
                }
                sb.append(this.f4216c);
                sb.append(" queue finished task ");
                sb.append(this.f4215b.c());
                sb.append(" with queue size ");
                sb.append(a2);
                b2.c("TaskManager", sb.toString());
            }
        }

        public y(k kVar) {
            this.f4187a = kVar;
            this.f4188b = kVar.Z();
            this.f4189c = a("main");
            this.f4190d = a("timeout");
            this.f4191e = a("back");
            this.f4192f = a("advertising_info_collection");
            this.f4193g = a("postbacks");
            this.f4194h = a("caching_interstitial");
            this.f4195i = a("caching_incentivized");
            this.f4196j = a("caching_other");
            this.f4197k = a("reward");
            this.l = a("mediation_main");
            this.m = a("mediation_timeout");
            this.n = a("mediation_background");
            this.o = a("mediation_postbacks");
            this.p = a("mediation_banner");
            this.q = a("mediation_interstitial");
            this.r = a("mediation_incentivized");
            this.s = a("mediation_reward");
            this.t = a("auxiliary_operations", ((Integer) kVar.a(c.e.v1)).intValue());
            this.u = a("caching_operations", ((Integer) kVar.a(c.e.w1)).intValue());
            this.v = a("shared_thread_pool", ((Integer) kVar.a(c.e.u)).intValue());
        }

        /* access modifiers changed from: private */
        public long a(b bVar) {
            long taskCount;
            ScheduledThreadPoolExecutor scheduledThreadPoolExecutor;
            if (bVar == b.MAIN) {
                taskCount = this.f4189c.getTaskCount();
                scheduledThreadPoolExecutor = this.f4189c;
            } else if (bVar == b.TIMEOUT) {
                taskCount = this.f4190d.getTaskCount();
                scheduledThreadPoolExecutor = this.f4190d;
            } else if (bVar == b.BACKGROUND) {
                taskCount = this.f4191e.getTaskCount();
                scheduledThreadPoolExecutor = this.f4191e;
            } else if (bVar == b.ADVERTISING_INFO_COLLECTION) {
                taskCount = this.f4192f.getTaskCount();
                scheduledThreadPoolExecutor = this.f4192f;
            } else if (bVar == b.POSTBACKS) {
                taskCount = this.f4193g.getTaskCount();
                scheduledThreadPoolExecutor = this.f4193g;
            } else if (bVar == b.CACHING_INTERSTITIAL) {
                taskCount = this.f4194h.getTaskCount();
                scheduledThreadPoolExecutor = this.f4194h;
            } else if (bVar == b.CACHING_INCENTIVIZED) {
                taskCount = this.f4195i.getTaskCount();
                scheduledThreadPoolExecutor = this.f4195i;
            } else if (bVar == b.CACHING_OTHER) {
                taskCount = this.f4196j.getTaskCount();
                scheduledThreadPoolExecutor = this.f4196j;
            } else if (bVar == b.REWARD) {
                taskCount = this.f4197k.getTaskCount();
                scheduledThreadPoolExecutor = this.f4197k;
            } else if (bVar == b.MEDIATION_MAIN) {
                taskCount = this.l.getTaskCount();
                scheduledThreadPoolExecutor = this.l;
            } else if (bVar == b.MEDIATION_TIMEOUT) {
                taskCount = this.m.getTaskCount();
                scheduledThreadPoolExecutor = this.m;
            } else if (bVar == b.MEDIATION_BACKGROUND) {
                taskCount = this.n.getTaskCount();
                scheduledThreadPoolExecutor = this.n;
            } else if (bVar == b.MEDIATION_POSTBACKS) {
                taskCount = this.o.getTaskCount();
                scheduledThreadPoolExecutor = this.o;
            } else if (bVar == b.MEDIATION_BANNER) {
                taskCount = this.p.getTaskCount();
                scheduledThreadPoolExecutor = this.p;
            } else if (bVar == b.MEDIATION_INTERSTITIAL) {
                taskCount = this.q.getTaskCount();
                scheduledThreadPoolExecutor = this.q;
            } else if (bVar == b.MEDIATION_INCENTIVIZED) {
                taskCount = this.r.getTaskCount();
                scheduledThreadPoolExecutor = this.r;
            } else if (bVar != b.MEDIATION_REWARD) {
                return 0;
            } else {
                taskCount = this.s.getTaskCount();
                scheduledThreadPoolExecutor = this.s;
            }
            return taskCount - scheduledThreadPoolExecutor.getCompletedTaskCount();
        }

        private ScheduledThreadPoolExecutor a(String str) {
            return a(str, 1);
        }

        private ScheduledThreadPoolExecutor a(String str, int i2) {
            return new ScheduledThreadPoolExecutor(i2, new c(str));
        }

        private void a(Runnable runnable, long j2, ScheduledExecutorService scheduledExecutorService, boolean z) {
            if (j2 <= 0) {
                scheduledExecutorService.submit(runnable);
            } else if (z) {
                com.applovin.impl.sdk.utils.d.a(j2, this.f4187a, new a(this, scheduledExecutorService, runnable));
            } else {
                scheduledExecutorService.schedule(runnable, j2, TimeUnit.MILLISECONDS);
            }
        }

        private boolean a(d dVar) {
            if (dVar.f4215b.e()) {
                return false;
            }
            synchronized (this.x) {
                if (this.y) {
                    return false;
                }
                this.w.add(dVar);
                return true;
            }
        }

        public void a(c cVar) {
            if (cVar != null) {
                long currentTimeMillis = System.currentTimeMillis();
                try {
                    q qVar = this.f4188b;
                    qVar.c("TaskManager", "Executing " + cVar.c() + " immediately...");
                    cVar.run();
                    this.f4187a.l().a(cVar.a(), System.currentTimeMillis() - currentTimeMillis);
                    q qVar2 = this.f4188b;
                    qVar2.c("TaskManager", cVar.c() + " finished executing...");
                } catch (Throwable th) {
                    this.f4188b.b(cVar.c(), "Task failed execution", th);
                    this.f4187a.l().a(cVar.a(), true, System.currentTimeMillis() - currentTimeMillis);
                }
            } else {
                this.f4188b.e("TaskManager", "Attempted to execute null task immediately");
            }
        }

        public void a(c cVar, b bVar) {
            a(cVar, bVar, 0);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.applovin.impl.sdk.f.y.a(com.applovin.impl.sdk.f$c, com.applovin.impl.sdk.f$y$b, long, boolean):void
         arg types: [com.applovin.impl.sdk.f$c, com.applovin.impl.sdk.f$y$b, long, int]
         candidates:
          com.applovin.impl.sdk.f.y.a(java.lang.Runnable, long, java.util.concurrent.ScheduledExecutorService, boolean):void
          com.applovin.impl.sdk.f.y.a(com.applovin.impl.sdk.f$c, com.applovin.impl.sdk.f$y$b, long, boolean):void */
        public void a(c cVar, b bVar, long j2) {
            a(cVar, bVar, j2, false);
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v2, resolved type: com.applovin.impl.sdk.f$y$d} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v29, resolved type: com.applovin.impl.sdk.f$c} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v30, resolved type: com.applovin.impl.sdk.f$c} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v31, resolved type: com.applovin.impl.sdk.f$c} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a(com.applovin.impl.sdk.f.c r12, com.applovin.impl.sdk.f.y.b r13, long r14, boolean r16) {
            /*
                r11 = this;
                r6 = r11
                r1 = r12
                r0 = r13
                r2 = r14
                if (r1 == 0) goto L_0x0124
                r4 = 0
                int r7 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                if (r7 < 0) goto L_0x010d
                com.applovin.impl.sdk.f$y$d r4 = new com.applovin.impl.sdk.f$y$d
                r4.<init>(r12, r13)
                boolean r5 = r11.a(r4)
                if (r5 != 0) goto L_0x00e9
                com.applovin.impl.sdk.k r5 = r6.f4187a
                com.applovin.impl.sdk.c$e<java.lang.Boolean> r7 = com.applovin.impl.sdk.c.e.v
                java.lang.Object r5 = r5.a(r7)
                java.lang.Boolean r5 = (java.lang.Boolean) r5
                boolean r5 = r5.booleanValue()
                if (r5 == 0) goto L_0x0033
                java.util.concurrent.ScheduledThreadPoolExecutor r4 = r6.v
                r0 = r11
                r1 = r12
                r2 = r14
            L_0x002c:
                r5 = r16
                r0.a(r1, r2, r4, r5)
                goto L_0x010c
            L_0x0033:
                long r7 = r11.a(r13)
                r9 = 1
                long r7 = r7 + r9
                com.applovin.impl.sdk.q r5 = r6.f4188b
                java.lang.StringBuilder r9 = new java.lang.StringBuilder
                r9.<init>()
                java.lang.String r10 = "Scheduling "
                r9.append(r10)
                java.lang.String r1 = r12.c()
                r9.append(r1)
                java.lang.String r1 = " on "
                r9.append(r1)
                r9.append(r13)
                java.lang.String r1 = " queue in "
                r9.append(r1)
                r9.append(r14)
                java.lang.String r1 = "ms with new queue size "
                r9.append(r1)
                r9.append(r7)
                java.lang.String r1 = r9.toString()
                java.lang.String r7 = "TaskManager"
                r5.b(r7, r1)
                com.applovin.impl.sdk.f$y$b r1 = com.applovin.impl.sdk.f.y.b.MAIN
                if (r0 != r1) goto L_0x0079
                java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.f4189c
            L_0x0074:
                r0 = r11
                r1 = r4
                r2 = r14
                r4 = r5
                goto L_0x002c
            L_0x0079:
                com.applovin.impl.sdk.f$y$b r1 = com.applovin.impl.sdk.f.y.b.TIMEOUT
                if (r0 != r1) goto L_0x0080
                java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.f4190d
                goto L_0x0074
            L_0x0080:
                com.applovin.impl.sdk.f$y$b r1 = com.applovin.impl.sdk.f.y.b.BACKGROUND
                if (r0 != r1) goto L_0x0087
                java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.f4191e
                goto L_0x0074
            L_0x0087:
                com.applovin.impl.sdk.f$y$b r1 = com.applovin.impl.sdk.f.y.b.ADVERTISING_INFO_COLLECTION
                if (r0 != r1) goto L_0x008e
                java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.f4192f
                goto L_0x0074
            L_0x008e:
                com.applovin.impl.sdk.f$y$b r1 = com.applovin.impl.sdk.f.y.b.POSTBACKS
                if (r0 != r1) goto L_0x0095
                java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.f4193g
                goto L_0x0074
            L_0x0095:
                com.applovin.impl.sdk.f$y$b r1 = com.applovin.impl.sdk.f.y.b.CACHING_INTERSTITIAL
                if (r0 != r1) goto L_0x009c
                java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.f4194h
                goto L_0x0074
            L_0x009c:
                com.applovin.impl.sdk.f$y$b r1 = com.applovin.impl.sdk.f.y.b.CACHING_INCENTIVIZED
                if (r0 != r1) goto L_0x00a3
                java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.f4195i
                goto L_0x0074
            L_0x00a3:
                com.applovin.impl.sdk.f$y$b r1 = com.applovin.impl.sdk.f.y.b.CACHING_OTHER
                if (r0 != r1) goto L_0x00aa
                java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.f4196j
                goto L_0x0074
            L_0x00aa:
                com.applovin.impl.sdk.f$y$b r1 = com.applovin.impl.sdk.f.y.b.REWARD
                if (r0 != r1) goto L_0x00b1
                java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.f4197k
                goto L_0x0074
            L_0x00b1:
                com.applovin.impl.sdk.f$y$b r1 = com.applovin.impl.sdk.f.y.b.MEDIATION_MAIN
                if (r0 != r1) goto L_0x00b8
                java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.l
                goto L_0x0074
            L_0x00b8:
                com.applovin.impl.sdk.f$y$b r1 = com.applovin.impl.sdk.f.y.b.MEDIATION_TIMEOUT
                if (r0 != r1) goto L_0x00bf
                java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.m
                goto L_0x0074
            L_0x00bf:
                com.applovin.impl.sdk.f$y$b r1 = com.applovin.impl.sdk.f.y.b.MEDIATION_BACKGROUND
                if (r0 != r1) goto L_0x00c6
                java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.n
                goto L_0x0074
            L_0x00c6:
                com.applovin.impl.sdk.f$y$b r1 = com.applovin.impl.sdk.f.y.b.MEDIATION_POSTBACKS
                if (r0 != r1) goto L_0x00cd
                java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.o
                goto L_0x0074
            L_0x00cd:
                com.applovin.impl.sdk.f$y$b r1 = com.applovin.impl.sdk.f.y.b.MEDIATION_BANNER
                if (r0 != r1) goto L_0x00d4
                java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.p
                goto L_0x0074
            L_0x00d4:
                com.applovin.impl.sdk.f$y$b r1 = com.applovin.impl.sdk.f.y.b.MEDIATION_INTERSTITIAL
                if (r0 != r1) goto L_0x00db
                java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.q
                goto L_0x0074
            L_0x00db:
                com.applovin.impl.sdk.f$y$b r1 = com.applovin.impl.sdk.f.y.b.MEDIATION_INCENTIVIZED
                if (r0 != r1) goto L_0x00e2
                java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.r
                goto L_0x0074
            L_0x00e2:
                com.applovin.impl.sdk.f$y$b r1 = com.applovin.impl.sdk.f.y.b.MEDIATION_REWARD
                if (r0 != r1) goto L_0x010c
                java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.s
                goto L_0x0074
            L_0x00e9:
                com.applovin.impl.sdk.q r0 = r6.f4188b
                java.lang.String r2 = r12.c()
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = "Task "
                r3.append(r4)
                java.lang.String r1 = r12.c()
                r3.append(r1)
                java.lang.String r1 = " execution delayed until after init"
                r3.append(r1)
                java.lang.String r1 = r3.toString()
                r0.c(r2, r1)
            L_0x010c:
                return
            L_0x010d:
                java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r4 = "Invalid delay specified: "
                r1.append(r4)
                r1.append(r14)
                java.lang.String r1 = r1.toString()
                r0.<init>(r1)
                throw r0
            L_0x0124:
                java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
                java.lang.String r1 = "No task specified"
                r0.<init>(r1)
                goto L_0x012d
            L_0x012c:
                throw r0
            L_0x012d:
                goto L_0x012c
            */
            throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.f.y.a(com.applovin.impl.sdk.f$c, com.applovin.impl.sdk.f$y$b, long, boolean):void");
        }

        public boolean a() {
            return this.y;
        }

        public ScheduledExecutorService b() {
            return this.t;
        }

        public ScheduledExecutorService c() {
            return this.u;
        }

        public void d() {
            synchronized (this.x) {
                this.y = false;
            }
        }

        public void e() {
            synchronized (this.x) {
                this.y = true;
                for (d next : this.w) {
                    a(next.f4215b, next.f4216c);
                }
                this.w.clear();
            }
        }
    }

    public class z extends c implements AppLovinAdLoadListener {

        /* renamed from: f  reason: collision with root package name */
        private final JSONObject f4218f;

        /* renamed from: g  reason: collision with root package name */
        private final com.applovin.impl.sdk.ad.d f4219g;

        /* renamed from: h  reason: collision with root package name */
        private final com.applovin.impl.sdk.ad.b f4220h;

        /* renamed from: i  reason: collision with root package name */
        private final AppLovinAdLoadListener f4221i;

        public z(JSONObject jSONObject, com.applovin.impl.sdk.ad.d dVar, com.applovin.impl.sdk.ad.b bVar, AppLovinAdLoadListener appLovinAdLoadListener, k kVar) {
            super("TaskProcessAdResponse", kVar);
            if (jSONObject == null) {
                throw new IllegalArgumentException("No response specified");
            } else if (dVar != null) {
                this.f4218f = jSONObject;
                this.f4219g = dVar;
                this.f4220h = bVar;
                this.f4221i = appLovinAdLoadListener;
            } else {
                throw new IllegalArgumentException("No zone specified");
            }
        }

        private void a(int i2) {
            com.applovin.impl.sdk.utils.p.a(this.f4221i, this.f4219g, i2, this.f4130a);
        }

        private void a(AppLovinAd appLovinAd) {
            try {
                if (this.f4221i != null) {
                    this.f4221i.adReceived(appLovinAd);
                }
            } catch (Throwable th) {
                a("Unable process a ad received notification", th);
            }
        }

        private void a(JSONObject jSONObject) {
            String b2 = com.applovin.impl.sdk.utils.i.b(jSONObject, "type", "undefined", this.f4130a);
            if ("applovin".equalsIgnoreCase(b2)) {
                a("Starting task for AppLovin ad...");
                this.f4130a.j().a(new b0(jSONObject, this.f4218f, this.f4220h, this, this.f4130a));
            } else if ("vast".equalsIgnoreCase(b2)) {
                a("Starting task for VAST ad...");
                this.f4130a.j().a(a0.a(jSONObject, this.f4218f, this.f4220h, this, this.f4130a));
            } else {
                c("Unable to process ad of unknown type: " + b2);
                failedToReceiveAd(-800);
            }
        }

        public com.applovin.impl.sdk.d.i a() {
            return com.applovin.impl.sdk.d.i.t;
        }

        public void adReceived(AppLovinAd appLovinAd) {
            a(appLovinAd);
        }

        public void failedToReceiveAd(int i2) {
            a(i2);
        }

        public void run() {
            JSONArray b2 = com.applovin.impl.sdk.utils.i.b(this.f4218f, "ads", new JSONArray(), this.f4130a);
            if (b2.length() > 0) {
                a("Processing ad...");
                a(com.applovin.impl.sdk.utils.i.a(b2, 0, new JSONObject(), this.f4130a));
                return;
            }
            c("No ads were returned from the server");
            com.applovin.impl.sdk.utils.p.a(this.f4219g.a(), this.f4218f, this.f4130a);
            a(204);
        }
    }

    public f(k kVar, b bVar) {
        this.f4119d = bVar;
        this.f4118c = kVar;
    }

    /* access modifiers changed from: private */
    public void j() {
        synchronized (this.f4117b) {
            this.f4116a = null;
            if (!((Boolean) this.f4118c.a(c.d.q4)).booleanValue()) {
                this.f4118c.D().unregisterReceiver(this);
                this.f4118c.y().b(this);
            }
        }
    }

    public void a(long j2) {
        synchronized (this.f4117b) {
            c();
            this.f4120e = j2;
            this.f4116a = com.applovin.impl.sdk.utils.n.a(j2, this.f4118c, new a());
            if (!((Boolean) this.f4118c.a(c.d.q4)).booleanValue()) {
                this.f4118c.D().registerReceiver(this, new IntentFilter("com.applovin.application_paused"));
                this.f4118c.D().registerReceiver(this, new IntentFilter("com.applovin.application_resumed"));
                this.f4118c.y().a(this);
            }
            if (((Boolean) this.f4118c.a(c.d.p4)).booleanValue() && (this.f4118c.y().b() || this.f4118c.x().a())) {
                this.f4116a.b();
            }
        }
    }

    public boolean a() {
        boolean z2;
        synchronized (this.f4117b) {
            z2 = this.f4116a != null;
        }
        return z2;
    }

    public long b() {
        long a2;
        synchronized (this.f4117b) {
            a2 = this.f4116a != null ? this.f4116a.a() : -1;
        }
        return a2;
    }

    public void c() {
        synchronized (this.f4117b) {
            if (this.f4116a != null) {
                this.f4116a.d();
                j();
            }
        }
    }

    public void d() {
        synchronized (this.f4117b) {
            if (this.f4116a != null) {
                this.f4116a.b();
            }
        }
    }

    public void e() {
        synchronized (this.f4117b) {
            if (this.f4116a != null) {
                this.f4116a.c();
            }
        }
    }

    public void f() {
        if (((Boolean) this.f4118c.a(c.d.o4)).booleanValue()) {
            d();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x005e, code lost:
        if (r2 == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0060, code lost:
        r9.f4119d.onAdRefresh();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void g() {
        /*
            r9 = this;
            com.applovin.impl.sdk.k r0 = r9.f4118c
            com.applovin.impl.sdk.c$e<java.lang.Boolean> r1 = com.applovin.impl.sdk.c.d.o4
            java.lang.Object r0 = r0.a(r1)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0069
            java.lang.Object r0 = r9.f4117b
            monitor-enter(r0)
            com.applovin.impl.sdk.k r1 = r9.f4118c     // Catch:{ all -> 0x0066 }
            com.applovin.impl.sdk.p r1 = r1.y()     // Catch:{ all -> 0x0066 }
            boolean r1 = r1.b()     // Catch:{ all -> 0x0066 }
            if (r1 == 0) goto L_0x002e
            com.applovin.impl.sdk.k r1 = r9.f4118c     // Catch:{ all -> 0x0066 }
            com.applovin.impl.sdk.q r1 = r1.Z()     // Catch:{ all -> 0x0066 }
            java.lang.String r2 = "AdRefreshManager"
            java.lang.String r3 = "Waiting for the full screen ad to be dismissed to resume the timer."
            r1.b(r2, r3)     // Catch:{ all -> 0x0066 }
            monitor-exit(r0)     // Catch:{ all -> 0x0066 }
            return
        L_0x002e:
            com.applovin.impl.sdk.utils.n r1 = r9.f4116a     // Catch:{ all -> 0x0066 }
            r2 = 0
            if (r1 == 0) goto L_0x005d
            long r3 = r9.f4120e     // Catch:{ all -> 0x0066 }
            long r5 = r9.b()     // Catch:{ all -> 0x0066 }
            long r3 = r3 - r5
            com.applovin.impl.sdk.k r1 = r9.f4118c     // Catch:{ all -> 0x0066 }
            com.applovin.impl.sdk.c$e<java.lang.Long> r5 = com.applovin.impl.sdk.c.d.n4     // Catch:{ all -> 0x0066 }
            java.lang.Object r1 = r1.a(r5)     // Catch:{ all -> 0x0066 }
            java.lang.Long r1 = (java.lang.Long) r1     // Catch:{ all -> 0x0066 }
            long r5 = r1.longValue()     // Catch:{ all -> 0x0066 }
            r7 = 0
            int r1 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r1 < 0) goto L_0x0058
            int r1 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r1 <= 0) goto L_0x0058
            r9.c()     // Catch:{ all -> 0x0066 }
            r1 = 1
            r2 = 1
            goto L_0x005d
        L_0x0058:
            com.applovin.impl.sdk.utils.n r1 = r9.f4116a     // Catch:{ all -> 0x0066 }
            r1.c()     // Catch:{ all -> 0x0066 }
        L_0x005d:
            monitor-exit(r0)     // Catch:{ all -> 0x0066 }
            if (r2 == 0) goto L_0x0069
            com.applovin.impl.sdk.f$b r0 = r9.f4119d
            r0.onAdRefresh()
            goto L_0x0069
        L_0x0066:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0066 }
            throw r1
        L_0x0069:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.f.g():void");
    }

    public void h() {
        if (((Boolean) this.f4118c.a(c.d.p4)).booleanValue()) {
            d();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void i() {
        /*
            r4 = this;
            com.applovin.impl.sdk.k r0 = r4.f4118c
            com.applovin.impl.sdk.c$e<java.lang.Boolean> r1 = com.applovin.impl.sdk.c.d.p4
            java.lang.Object r0 = r0.a(r1)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x003c
            java.lang.Object r0 = r4.f4117b
            monitor-enter(r0)
            com.applovin.impl.sdk.k r1 = r4.f4118c     // Catch:{ all -> 0x0039 }
            com.applovin.impl.sdk.v r1 = r1.x()     // Catch:{ all -> 0x0039 }
            boolean r1 = r1.a()     // Catch:{ all -> 0x0039 }
            if (r1 == 0) goto L_0x002e
            com.applovin.impl.sdk.k r1 = r4.f4118c     // Catch:{ all -> 0x0039 }
            com.applovin.impl.sdk.q r1 = r1.Z()     // Catch:{ all -> 0x0039 }
            java.lang.String r2 = "AdRefreshManager"
            java.lang.String r3 = "Waiting for the application to enter foreground to resume the timer."
            r1.b(r2, r3)     // Catch:{ all -> 0x0039 }
            monitor-exit(r0)     // Catch:{ all -> 0x0039 }
            return
        L_0x002e:
            com.applovin.impl.sdk.utils.n r1 = r4.f4116a     // Catch:{ all -> 0x0039 }
            if (r1 == 0) goto L_0x0037
            com.applovin.impl.sdk.utils.n r1 = r4.f4116a     // Catch:{ all -> 0x0039 }
            r1.c()     // Catch:{ all -> 0x0039 }
        L_0x0037:
            monitor-exit(r0)     // Catch:{ all -> 0x0039 }
            goto L_0x003c
        L_0x0039:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0039 }
            throw r1
        L_0x003c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.f.i():void");
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if ("com.applovin.application_paused".equals(action)) {
            f();
        } else if ("com.applovin.application_resumed".equals(action)) {
            g();
        }
    }
}
