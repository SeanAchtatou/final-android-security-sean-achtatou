package com.applovin.impl.sdk;

import com.applovin.impl.sdk.ad.NativeAdImpl;
import com.applovin.impl.sdk.ad.d;
import com.applovin.impl.sdk.ad.j;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.f;
import com.applovin.impl.sdk.utils.m;
import com.applovin.nativeAds.AppLovinNativeAd;
import com.applovin.nativeAds.AppLovinNativeAdLoadListener;
import com.applovin.nativeAds.AppLovinNativeAdPrecacheListener;
import com.applovin.sdk.AppLovinAd;
import java.util.Arrays;
import java.util.List;

public class r extends s {

    class a implements AppLovinNativeAdPrecacheListener {
        a() {
        }

        public void onNativeAdImagePrecachingFailed(AppLovinNativeAd appLovinNativeAd, int i2) {
            r rVar = r.this;
            rVar.c(d.h(rVar.f4382a), i2);
        }

        public void onNativeAdImagesPrecached(AppLovinNativeAd appLovinNativeAd) {
            if (!m.b(appLovinNativeAd.getVideoUrl())) {
                r.this.b((j) appLovinNativeAd);
            }
        }

        public void onNativeAdVideoPrecachingFailed(AppLovinNativeAd appLovinNativeAd, int i2) {
            q qVar = r.this.f4383b;
            qVar.d("NativeAdPreloadManager", "Video failed to cache during native ad preload. " + i2);
            r.this.b((j) appLovinNativeAd);
        }

        public void onNativeAdVideoPreceached(AppLovinNativeAd appLovinNativeAd) {
            r.this.b((j) appLovinNativeAd);
        }
    }

    r(k kVar) {
        super(kVar);
    }

    /* access modifiers changed from: package-private */
    public d a(j jVar) {
        return ((NativeAdImpl) jVar).getAdZone();
    }

    /* access modifiers changed from: package-private */
    public f.c a(d dVar) {
        return new f.u(null, 1, this.f4382a, this);
    }

    public void a() {
        h(d.h(this.f4382a));
    }

    public void a(d dVar, int i2) {
    }

    /* access modifiers changed from: package-private */
    public void a(Object obj, d dVar, int i2) {
        ((AppLovinNativeAdLoadListener) obj).onNativeAdsFailedToLoad(i2);
    }

    /* access modifiers changed from: package-private */
    public void a(Object obj, j jVar) {
        AppLovinNativeAdLoadListener appLovinNativeAdLoadListener = (AppLovinNativeAdLoadListener) obj;
        appLovinNativeAdLoadListener.onNativeAdsLoaded(Arrays.asList((AppLovinNativeAd) jVar));
    }

    public void adReceived(AppLovinAd appLovinAd) {
    }

    public void failedToReceiveAd(int i2) {
    }

    public void onNativeAdsFailedToLoad(int i2) {
        c(d.h(this.f4382a), i2);
    }

    public void onNativeAdsLoaded(List<AppLovinNativeAd> list) {
        AppLovinNativeAd appLovinNativeAd = list.get(0);
        if (((Boolean) this.f4382a.a(c.e.F0)).booleanValue()) {
            this.f4382a.T().precacheResources(appLovinNativeAd, new a());
        } else {
            b((j) appLovinNativeAd);
        }
    }
}
