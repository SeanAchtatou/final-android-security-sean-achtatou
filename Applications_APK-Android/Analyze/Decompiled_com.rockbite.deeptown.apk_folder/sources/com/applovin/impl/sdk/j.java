package com.applovin.impl.sdk;

import android.app.Activity;
import android.content.Intent;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.m;
import com.applovin.sdk.AppLovinPrivacySettings;
import com.applovin.sdk.AppLovinSdkUtils;
import com.applovin.sdk.AppLovinUserService;
import com.applovin.sdk.AppLovinWebViewActivity;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicBoolean;

public class j implements i.b, AppLovinWebViewActivity.EventListener {
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public static final AtomicBoolean f4234g = new AtomicBoolean();
    /* access modifiers changed from: private */

    /* renamed from: h  reason: collision with root package name */
    public static WeakReference<AppLovinWebViewActivity> f4235h;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final k f4236a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final q f4237b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public AppLovinUserService.OnConsentDialogDismissListener f4238c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public i f4239d;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public WeakReference<Activity> f4240e = new WeakReference<>(null);
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public com.applovin.impl.sdk.utils.a f4241f;

    class a extends com.applovin.impl.sdk.utils.a {
        a() {
        }

        public void onActivityStarted(Activity activity) {
            WeakReference unused = j.this.f4240e = new WeakReference(activity);
        }
    }

    class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AppLovinUserService.OnConsentDialogDismissListener f4243a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ Activity f4244b;

        class a extends com.applovin.impl.sdk.utils.a {
            a() {
            }

            public void onActivityStarted(Activity activity) {
                if (activity instanceof AppLovinWebViewActivity) {
                    if (!j.this.c() || j.f4235h.get() != activity) {
                        AppLovinWebViewActivity appLovinWebViewActivity = (AppLovinWebViewActivity) activity;
                        WeakReference unused = j.f4235h = new WeakReference(appLovinWebViewActivity);
                        appLovinWebViewActivity.loadUrl((String) j.this.f4236a.a(c.e.x), j.this);
                    }
                    j.f4234g.set(false);
                }
            }
        }

        b(AppLovinUserService.OnConsentDialogDismissListener onConsentDialogDismissListener, Activity activity) {
            this.f4243a = onConsentDialogDismissListener;
            this.f4244b = activity;
        }

        public void run() {
            j jVar = j.this;
            if (!jVar.a(jVar.f4236a) || j.f4234g.getAndSet(true)) {
                AppLovinUserService.OnConsentDialogDismissListener onConsentDialogDismissListener = this.f4243a;
                if (onConsentDialogDismissListener != null) {
                    onConsentDialogDismissListener.onDismiss();
                    return;
                }
                return;
            }
            WeakReference unused = j.this.f4240e = new WeakReference(this.f4244b);
            AppLovinUserService.OnConsentDialogDismissListener unused2 = j.this.f4238c = this.f4243a;
            com.applovin.impl.sdk.utils.a unused3 = j.this.f4241f = new a();
            j.this.f4236a.A().a(j.this.f4241f);
            Intent intent = new Intent(this.f4244b, AppLovinWebViewActivity.class);
            intent.putExtra(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, j.this.f4236a.X());
            intent.putExtra(AppLovinWebViewActivity.INTENT_EXTRA_KEY_IMMERSIVE_MODE_ON, (Serializable) j.this.f4236a.a(c.e.y));
            this.f4244b.startActivity(intent);
        }
    }

    class c implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ long f4247a;

        c(long j2) {
            this.f4247a = j2;
        }

        public void run() {
            j.this.f4237b.b("ConsentDialogManager", "Scheduling repeating consent alert");
            j.this.f4239d.a(this.f4247a, j.this.f4236a, j.this);
        }
    }

    class d implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Activity f4249a;

        d(Activity activity) {
            this.f4249a = activity;
        }

        public void run() {
            j.this.a(this.f4249a, (AppLovinUserService.OnConsentDialogDismissListener) null);
        }
    }

    j(k kVar) {
        this.f4236a = kVar;
        this.f4237b = kVar.Z();
        if (kVar.e() != null) {
            this.f4240e = new WeakReference<>(kVar.e());
        }
        kVar.A().a(new a());
        this.f4239d = new i(this, kVar);
    }

    private void a(boolean z, long j2) {
        f();
        if (z) {
            a(j2);
        }
    }

    /* access modifiers changed from: private */
    public boolean a(k kVar) {
        if (c()) {
            q.i("AppLovinSdk", "Consent dialog already showing");
            return false;
        } else if (!h.a(kVar.d())) {
            q.i("AppLovinSdk", "No internet available, skip showing of consent dialog");
            return false;
        } else if (!((Boolean) kVar.a(c.e.w)).booleanValue()) {
            this.f4237b.e("ConsentDialogManager", "Blocked publisher from showing consent dialog");
            return false;
        } else if (m.b((String) kVar.a(c.e.x))) {
            return true;
        } else {
            this.f4237b.e("ConsentDialogManager", "AdServer returned empty consent dialog URL");
            return false;
        }
    }

    private void f() {
        this.f4236a.A().b(this.f4241f);
        if (c()) {
            AppLovinWebViewActivity appLovinWebViewActivity = f4235h.get();
            f4235h = null;
            if (appLovinWebViewActivity != null) {
                appLovinWebViewActivity.finish();
                AppLovinUserService.OnConsentDialogDismissListener onConsentDialogDismissListener = this.f4238c;
                if (onConsentDialogDismissListener != null) {
                    onConsentDialogDismissListener.onDismiss();
                    this.f4238c = null;
                }
            }
        }
    }

    public void a() {
        if (this.f4240e.get() != null) {
            AppLovinSdkUtils.runOnUiThreadDelayed(new d(this.f4240e.get()), ((Long) this.f4236a.a(c.e.z)).longValue());
        }
    }

    public void a(long j2) {
        AppLovinSdkUtils.runOnUiThread(new c(j2));
    }

    public void a(Activity activity, AppLovinUserService.OnConsentDialogDismissListener onConsentDialogDismissListener) {
        activity.runOnUiThread(new b(onConsentDialogDismissListener, activity));
    }

    public void b() {
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        WeakReference<AppLovinWebViewActivity> weakReference = f4235h;
        return (weakReference == null || weakReference.get() == null) ? false : true;
    }

    public void onReceivedEvent(String str) {
        boolean booleanValue;
        k kVar;
        c.e<Long> eVar;
        if ("accepted".equalsIgnoreCase(str)) {
            AppLovinPrivacySettings.setHasUserConsent(true, this.f4236a.d());
            f();
            return;
        }
        if ("rejected".equalsIgnoreCase(str)) {
            AppLovinPrivacySettings.setHasUserConsent(false, this.f4236a.d());
            booleanValue = ((Boolean) this.f4236a.a(c.e.A)).booleanValue();
            kVar = this.f4236a;
            eVar = c.e.F;
        } else if ("closed".equalsIgnoreCase(str)) {
            booleanValue = ((Boolean) this.f4236a.a(c.e.B)).booleanValue();
            kVar = this.f4236a;
            eVar = c.e.G;
        } else if (AppLovinWebViewActivity.EVENT_DISMISSED_VIA_BACK_BUTTON.equalsIgnoreCase(str)) {
            booleanValue = ((Boolean) this.f4236a.a(c.e.C)).booleanValue();
            kVar = this.f4236a;
            eVar = c.e.H;
        } else {
            return;
        }
        a(booleanValue, ((Long) kVar.a(eVar)).longValue());
    }
}
