package com.applovin.impl.sdk.ad;

import android.text.TextUtils;
import android.util.Base64;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.q;
import com.applovin.impl.sdk.utils.m;
import org.json.JSONException;
import org.json.JSONObject;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private final k f3953a;

    /* renamed from: b  reason: collision with root package name */
    private final String f3954b;

    public enum a {
        UNSPECIFIED("UNSPECIFIED"),
        REGULAR("REGULAR"),
        AD_RESPONSE_JSON("AD_RESPONSE_JSON");
        

        /* renamed from: a  reason: collision with root package name */
        private final String f3959a;

        private a(String str) {
            this.f3959a = str;
        }

        public String toString() {
            return this.f3959a;
        }
    }

    public c(String str, k kVar) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("Identifier is empty");
        } else if (kVar != null) {
            this.f3954b = str;
            this.f3953a = kVar;
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    private String a(c.e<String> eVar) {
        for (String next : this.f3953a.b(eVar)) {
            if (this.f3954b.startsWith(next)) {
                return next;
            }
        }
        return null;
    }

    public String a() {
        return this.f3954b;
    }

    public a b() {
        return a(c.e.d0) != null ? a.REGULAR : a(c.e.e0) != null ? a.AD_RESPONSE_JSON : a.UNSPECIFIED;
    }

    public String c() {
        String a2 = a(c.e.d0);
        if (!TextUtils.isEmpty(a2)) {
            return a2;
        }
        String a3 = a(c.e.e0);
        if (!TextUtils.isEmpty(a3)) {
            return a3;
        }
        return null;
    }

    public JSONObject d() {
        if (b() != a.AD_RESPONSE_JSON) {
            return null;
        }
        try {
            JSONObject jSONObject = new JSONObject(new String(Base64.decode(this.f3954b.substring(c().length()), 0), "UTF-8"));
            q Z = this.f3953a.Z();
            Z.b("AdToken", "Decoded token into ad response: " + jSONObject);
            return jSONObject;
        } catch (JSONException e2) {
            q Z2 = this.f3953a.Z();
            Z2.b("AdToken", "Unable to decode token '" + this.f3954b + "' into JSON", e2);
            return null;
        } catch (Throwable th) {
            q Z3 = this.f3953a.Z();
            Z3.b("AdToken", "Unable to process ad response from token '" + this.f3954b + "'", th);
            return null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof c)) {
            return false;
        }
        String str = this.f3954b;
        String str2 = ((c) obj).f3954b;
        return str != null ? str.equals(str2) : str2 == null;
    }

    public int hashCode() {
        String str = this.f3954b;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    public String toString() {
        String a2 = m.a(32, this.f3954b);
        return "AdToken{id=" + a2 + ", type=" + b() + '}';
    }
}
