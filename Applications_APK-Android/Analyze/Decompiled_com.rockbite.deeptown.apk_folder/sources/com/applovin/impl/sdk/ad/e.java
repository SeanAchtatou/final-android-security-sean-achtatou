package com.applovin.impl.sdk.ad;

import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.q;
import com.applovin.impl.sdk.utils.i;
import java.util.LinkedHashSet;
import org.json.JSONArray;
import org.json.JSONObject;

public class e {

    /* renamed from: a  reason: collision with root package name */
    private final k f3969a;

    /* renamed from: b  reason: collision with root package name */
    private final q f3970b;

    /* renamed from: c  reason: collision with root package name */
    private LinkedHashSet<d> f3971c;

    /* renamed from: d  reason: collision with root package name */
    private final Object f3972d = new Object();

    /* renamed from: e  reason: collision with root package name */
    private volatile boolean f3973e;

    public e(k kVar) {
        this.f3969a = kVar;
        this.f3970b = kVar.Z();
        this.f3971c = b();
    }

    /*  JADX ERROR: StackOverflow in pass: MarkFinallyVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    private java.util.LinkedHashSet<com.applovin.impl.sdk.ad.d> b() {
        /*
            r7 = this;
            java.lang.String r0 = "Retrieved persisted zones: "
            java.lang.String r1 = "AdZoneManager"
            java.util.LinkedHashSet r2 = new java.util.LinkedHashSet
            r2.<init>()
            com.applovin.impl.sdk.k r3 = r7.f3969a     // Catch:{ all -> 0x006f }
            com.applovin.impl.sdk.c$g<java.lang.String> r4 = com.applovin.impl.sdk.c.g.t     // Catch:{ all -> 0x006f }
            java.lang.Object r3 = r3.a(r4)     // Catch:{ all -> 0x006f }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ all -> 0x006f }
            boolean r4 = com.applovin.impl.sdk.utils.m.b(r3)     // Catch:{ all -> 0x006f }
            if (r4 == 0) goto L_0x003f
            org.json.JSONArray r4 = new org.json.JSONArray     // Catch:{ all -> 0x006f }
            r4.<init>(r3)     // Catch:{ all -> 0x006f }
            int r5 = r4.length()     // Catch:{ all -> 0x006f }
            if (r5 <= 0) goto L_0x0029
            java.util.LinkedHashSet r2 = r7.b(r4)     // Catch:{ all -> 0x006f }
            goto L_0x003f
        L_0x0029:
            com.applovin.impl.sdk.q r4 = r7.f3970b     // Catch:{ all -> 0x006f }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x006f }
            r5.<init>()     // Catch:{ all -> 0x006f }
            java.lang.String r6 = "Unable to inflate json string: "
            r5.append(r6)     // Catch:{ all -> 0x006f }
            r5.append(r3)     // Catch:{ all -> 0x006f }
            java.lang.String r3 = r5.toString()     // Catch:{ all -> 0x006f }
            r4.b(r1, r3)     // Catch:{ all -> 0x006f }
        L_0x003f:
            boolean r3 = r2.isEmpty()
            if (r3 != 0) goto L_0x00a7
            com.applovin.impl.sdk.q r3 = r7.f3970b
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r0)
            r4.append(r2)
            java.lang.String r0 = r4.toString()
            r3.b(r1, r0)
            java.util.Iterator r0 = r2.iterator()
        L_0x005d:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x00a7
            java.lang.Object r1 = r0.next()
            com.applovin.impl.sdk.ad.d r1 = (com.applovin.impl.sdk.ad.d) r1
            com.applovin.impl.sdk.k r3 = r7.f3969a
            r1.a(r3)
            goto L_0x005d
        L_0x006f:
            r3 = move-exception
            com.applovin.impl.sdk.q r4 = r7.f3970b     // Catch:{ all -> 0x00a8 }
            java.lang.String r5 = "Encountered error retrieving persisted zones"
            r4.b(r1, r5, r3)     // Catch:{ all -> 0x00a8 }
            boolean r3 = r2.isEmpty()
            if (r3 != 0) goto L_0x00a7
            com.applovin.impl.sdk.q r3 = r7.f3970b
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r0)
            r4.append(r2)
            java.lang.String r0 = r4.toString()
            r3.b(r1, r0)
            java.util.Iterator r0 = r2.iterator()
        L_0x0095:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x00a7
            java.lang.Object r1 = r0.next()
            com.applovin.impl.sdk.ad.d r1 = (com.applovin.impl.sdk.ad.d) r1
            com.applovin.impl.sdk.k r3 = r7.f3969a
            r1.a(r3)
            goto L_0x0095
        L_0x00a7:
            return r2
        L_0x00a8:
            r3 = move-exception
            boolean r4 = r2.isEmpty()
            if (r4 != 0) goto L_0x00d9
            com.applovin.impl.sdk.q r4 = r7.f3970b
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            r5.append(r0)
            r5.append(r2)
            java.lang.String r0 = r5.toString()
            r4.b(r1, r0)
            java.util.Iterator r0 = r2.iterator()
        L_0x00c7:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x00d9
            java.lang.Object r1 = r0.next()
            com.applovin.impl.sdk.ad.d r1 = (com.applovin.impl.sdk.ad.d) r1
            com.applovin.impl.sdk.k r2 = r7.f3969a
            r1.a(r2)
            goto L_0x00c7
        L_0x00d9:
            goto L_0x00db
        L_0x00da:
            throw r3
        L_0x00db:
            goto L_0x00da
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.ad.e.b():java.util.LinkedHashSet");
    }

    private LinkedHashSet<d> b(JSONArray jSONArray) {
        LinkedHashSet<d> linkedHashSet = new LinkedHashSet<>(jSONArray.length());
        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
            JSONObject a2 = i.a(jSONArray, i2, (JSONObject) null, this.f3969a);
            q qVar = this.f3970b;
            qVar.b("AdZoneManager", "Loading zone: " + i.d(a2) + "...");
            linkedHashSet.add(d.a(i.b(a2, "id", (String) null, this.f3969a), a2, this.f3969a));
        }
        return linkedHashSet;
    }

    private void c(JSONArray jSONArray) {
        if (((Boolean) this.f3969a.a(c.e.G2)).booleanValue()) {
            this.f3970b.b("AdZoneManager", "Persisting zones...");
            this.f3969a.a(c.g.t, jSONArray.toString());
        }
    }

    public LinkedHashSet<d> a() {
        LinkedHashSet<d> linkedHashSet;
        synchronized (this.f3972d) {
            linkedHashSet = this.f3971c;
        }
        return linkedHashSet;
    }

    public LinkedHashSet<d> a(JSONArray jSONArray) {
        if (jSONArray == null) {
            return new LinkedHashSet<>();
        }
        LinkedHashSet<d> linkedHashSet = new LinkedHashSet<>(jSONArray.length());
        LinkedHashSet<d> linkedHashSet2 = null;
        synchronized (this.f3972d) {
            if (!this.f3973e) {
                q qVar = this.f3970b;
                qVar.b("AdZoneManager", "Found " + jSONArray.length() + " zone(s)...");
                linkedHashSet2 = b(jSONArray);
                linkedHashSet = new LinkedHashSet<>(linkedHashSet2);
                linkedHashSet.removeAll(this.f3971c);
                this.f3971c = linkedHashSet2;
                this.f3973e = true;
            }
        }
        if (linkedHashSet2 != null) {
            c(jSONArray);
            this.f3970b.b("AdZoneManager", "Finished loading zones");
        }
        return linkedHashSet;
    }

    public boolean a(d dVar) {
        boolean contains;
        synchronized (this.f3972d) {
            contains = this.f3971c.contains(dVar);
        }
        return contains;
    }
}
