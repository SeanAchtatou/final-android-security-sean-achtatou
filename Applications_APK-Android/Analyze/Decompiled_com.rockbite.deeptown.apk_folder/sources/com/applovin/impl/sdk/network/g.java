package com.applovin.impl.sdk.network;

import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.network.b;
import java.util.Map;
import org.json.JSONObject;

public class g<T> extends b {

    public static class a<T> extends b.a<T> {
        public a(k kVar) {
            super(kVar);
            this.f4336h = false;
            this.f4337i = ((Integer) kVar.a(c.e.p2)).intValue();
            this.f4338j = ((Integer) kVar.a(c.e.o2)).intValue();
            this.f4339k = ((Integer) kVar.a(c.e.r2)).intValue();
        }

        public /* synthetic */ b.a a(int i2) {
            d(i2);
            return this;
        }

        public /* synthetic */ b.a a(Object obj) {
            b(obj);
            return this;
        }

        public /* synthetic */ b.a a(String str) {
            d(str);
            return this;
        }

        public /* synthetic */ b.a a(Map map) {
            b((Map<String, String>) map);
            return this;
        }

        public /* synthetic */ b.a a(JSONObject jSONObject) {
            b(jSONObject);
            return this;
        }

        public /* synthetic */ b.a a(boolean z) {
            c(z);
            return this;
        }

        public /* synthetic */ b.a b(int i2) {
            e(i2);
            return this;
        }

        public /* synthetic */ b.a b(String str) {
            f(str);
            return this;
        }

        public a b(Object obj) {
            this.f4335g = obj;
            return this;
        }

        public a b(Map<String, String> map) {
            this.f4332d = map;
            return this;
        }

        public a b(JSONObject jSONObject) {
            this.f4334f = jSONObject;
            return this;
        }

        /* renamed from: b */
        public g<T> a() {
            return new g<>(this);
        }

        public /* synthetic */ b.a c(int i2) {
            f(i2);
            return this;
        }

        public /* synthetic */ b.a c(String str) {
            e(str);
            return this;
        }

        public a c(Map<String, String> map) {
            this.f4333e = map;
            return this;
        }

        public a c(boolean z) {
            this.l = z;
            return this;
        }

        public a d(int i2) {
            this.f4337i = i2;
            return this;
        }

        public a d(String str) {
            this.f4330b = str;
            return this;
        }

        public a e(int i2) {
            this.f4338j = i2;
            return this;
        }

        public a e(String str) {
            this.f4331c = str;
            return this;
        }

        public a f(int i2) {
            this.f4339k = i2;
            return this;
        }

        public a f(String str) {
            this.f4329a = str;
            return this;
        }
    }

    protected g(a aVar) {
        super(aVar);
    }

    public static a b(k kVar) {
        return new a(kVar);
    }
}
