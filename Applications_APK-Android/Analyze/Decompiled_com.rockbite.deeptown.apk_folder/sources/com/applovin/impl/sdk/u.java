package com.applovin.impl.sdk;

import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.utils.i;
import org.json.JSONObject;

public class u {

    /* renamed from: a  reason: collision with root package name */
    private final k f4395a;

    /* renamed from: b  reason: collision with root package name */
    private final JSONObject f4396b;

    /* renamed from: c  reason: collision with root package name */
    private final Object f4397c = new Object();

    public u(k kVar) {
        this.f4395a = kVar;
        this.f4396b = i.a((String) kVar.b(c.g.s, "{}"), new JSONObject(), kVar);
    }

    public Integer a(String str) {
        Integer valueOf;
        synchronized (this.f4397c) {
            if (this.f4396b.has(str)) {
                i.a(this.f4396b, str, i.b(this.f4396b, str, 0, this.f4395a) + 1, this.f4395a);
            } else {
                i.a(this.f4396b, str, 1, this.f4395a);
            }
            this.f4395a.a(c.g.s, this.f4396b.toString());
            valueOf = Integer.valueOf(i.b(this.f4396b, str, 0, this.f4395a));
        }
        return valueOf;
    }
}
