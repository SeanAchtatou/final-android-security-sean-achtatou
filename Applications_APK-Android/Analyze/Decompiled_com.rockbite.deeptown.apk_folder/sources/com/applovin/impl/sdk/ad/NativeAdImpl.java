package com.applovin.impl.sdk.ad;

import android.content.Context;
import android.net.Uri;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.network.e;
import com.applovin.impl.sdk.network.f;
import com.applovin.impl.sdk.q;
import com.applovin.impl.sdk.utils.p;
import com.applovin.nativeAds.AppLovinNativeAd;
import com.applovin.sdk.AppLovinErrorCodes;
import com.applovin.sdk.AppLovinPostbackListener;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class NativeAdImpl implements j, AppLovinNativeAd {
    public static final String QUERY_PARAM_IS_FIRST_PLAY = "fp";
    public static final String QUERY_PARAM_VIDEO_PERCENT_VIEWED = "pv";

    /* renamed from: a  reason: collision with root package name */
    private final k f3924a;

    /* renamed from: b  reason: collision with root package name */
    private final d f3925b;

    /* renamed from: c  reason: collision with root package name */
    private final String f3926c;

    /* renamed from: d  reason: collision with root package name */
    private final String f3927d;

    /* renamed from: e  reason: collision with root package name */
    private final String f3928e;

    /* renamed from: f  reason: collision with root package name */
    private final String f3929f;

    /* renamed from: g  reason: collision with root package name */
    private final String f3930g;

    /* renamed from: h  reason: collision with root package name */
    private final String f3931h;

    /* renamed from: i  reason: collision with root package name */
    private final String f3932i;

    /* renamed from: j  reason: collision with root package name */
    private final String f3933j;

    /* renamed from: k  reason: collision with root package name */
    private final String f3934k;
    private final String l;
    private final String m;
    private final String n;
    private final String o;
    private final List<com.applovin.impl.sdk.d.a> p;
    private final List<com.applovin.impl.sdk.d.a> q;
    private final String r;
    private final long s;
    private final List<String> t;
    private String u;
    private String v;
    private float w;
    private String x;
    private AtomicBoolean y;

    public static class b {

        /* renamed from: a  reason: collision with root package name */
        private d f3935a;

        /* renamed from: b  reason: collision with root package name */
        private String f3936b;

        /* renamed from: c  reason: collision with root package name */
        private String f3937c;

        /* renamed from: d  reason: collision with root package name */
        private String f3938d;

        /* renamed from: e  reason: collision with root package name */
        private String f3939e;

        /* renamed from: f  reason: collision with root package name */
        private String f3940f;

        /* renamed from: g  reason: collision with root package name */
        private String f3941g;

        /* renamed from: h  reason: collision with root package name */
        private String f3942h;

        /* renamed from: i  reason: collision with root package name */
        private String f3943i;

        /* renamed from: j  reason: collision with root package name */
        private String f3944j;

        /* renamed from: k  reason: collision with root package name */
        private String f3945k;
        private float l;
        private String m;
        private String n;
        private String o;
        private String p;
        private String q;
        private List<com.applovin.impl.sdk.d.a> r;
        private List<com.applovin.impl.sdk.d.a> s;
        private String t;
        private String u;
        private long v;
        private List<String> w;
        private k x;

        public b a(float f2) {
            this.l = f2;
            return this;
        }

        public b a(long j2) {
            this.v = j2;
            return this;
        }

        public b a(d dVar) {
            this.f3935a = dVar;
            return this;
        }

        public b a(k kVar) {
            this.x = kVar;
            return this;
        }

        public b a(String str) {
            this.f3937c = str;
            return this;
        }

        public b a(List<com.applovin.impl.sdk.d.a> list) {
            this.r = list;
            return this;
        }

        public NativeAdImpl a() {
            return new NativeAdImpl(this.f3935a, this.f3936b, this.f3937c, this.f3938d, this.f3939e, this.f3940f, this.f3941g, this.f3942h, this.f3943i, this.f3944j, this.f3945k, this.l, this.m, this.n, this.o, this.p, this.q, this.r, this.s, this.t, this.u, this.v, this.w, this.x);
        }

        public b b(String str) {
            this.f3938d = str;
            return this;
        }

        public b b(List<com.applovin.impl.sdk.d.a> list) {
            this.s = list;
            return this;
        }

        public b c(String str) {
            this.f3939e = str;
            return this;
        }

        public b c(List<String> list) {
            this.w = list;
            return this;
        }

        public b d(String str) {
            this.f3940f = str;
            return this;
        }

        public b e(String str) {
            this.f3936b = str;
            return this;
        }

        public b f(String str) {
            this.f3941g = str;
            return this;
        }

        public b g(String str) {
            this.f3942h = str;
            return this;
        }

        public b h(String str) {
            this.f3943i = str;
            return this;
        }

        public b i(String str) {
            this.f3944j = str;
            return this;
        }

        public b j(String str) {
            this.f3945k = str;
            return this;
        }

        public b k(String str) {
            this.m = str;
            return this;
        }

        public b l(String str) {
            this.n = str;
            return this;
        }

        public b m(String str) {
            this.o = str;
            return this;
        }

        public b n(String str) {
            this.p = str;
            return this;
        }

        public b o(String str) {
            this.q = str;
            return this;
        }

        public b p(String str) {
            this.t = str;
            return this;
        }

        public b q(String str) {
            this.u = str;
            return this;
        }
    }

    private NativeAdImpl(d dVar, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, float f2, String str11, String str12, String str13, String str14, String str15, List<com.applovin.impl.sdk.d.a> list, List<com.applovin.impl.sdk.d.a> list2, String str16, String str17, long j2, List<String> list3, k kVar) {
        this.y = new AtomicBoolean();
        this.f3925b = dVar;
        this.f3926c = str;
        this.f3927d = str2;
        this.f3928e = str3;
        this.f3929f = str4;
        this.f3930g = str5;
        this.f3931h = str6;
        this.f3932i = str7;
        this.f3933j = str8;
        this.u = str9;
        this.v = str10;
        this.w = f2;
        this.x = str11;
        this.l = str12;
        this.m = str13;
        this.n = str14;
        this.o = str15;
        this.p = list;
        this.q = list2;
        this.r = str16;
        this.f3934k = str17;
        this.s = j2;
        this.t = list3;
        this.f3924a = kVar;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || NativeAdImpl.class != obj.getClass()) {
            return false;
        }
        NativeAdImpl nativeAdImpl = (NativeAdImpl) obj;
        d dVar = this.f3925b;
        if (dVar == null ? nativeAdImpl.f3925b != null : !dVar.equals(nativeAdImpl.f3925b)) {
            return false;
        }
        String str = this.f3933j;
        if (str == null ? nativeAdImpl.f3933j != null : !str.equals(nativeAdImpl.f3933j)) {
            return false;
        }
        String str2 = this.r;
        if (str2 == null ? nativeAdImpl.r != null : !str2.equals(nativeAdImpl.r)) {
            return false;
        }
        String str3 = this.l;
        if (str3 == null ? nativeAdImpl.l != null : !str3.equals(nativeAdImpl.l)) {
            return false;
        }
        String str4 = this.f3934k;
        if (str4 == null ? nativeAdImpl.f3934k != null : !str4.equals(nativeAdImpl.f3934k)) {
            return false;
        }
        String str5 = this.f3932i;
        if (str5 == null ? nativeAdImpl.f3932i != null : !str5.equals(nativeAdImpl.f3932i)) {
            return false;
        }
        String str6 = this.m;
        if (str6 == null ? nativeAdImpl.m != null : !str6.equals(nativeAdImpl.m)) {
            return false;
        }
        String str7 = this.f3927d;
        if (str7 == null ? nativeAdImpl.f3927d != null : !str7.equals(nativeAdImpl.f3927d)) {
            return false;
        }
        String str8 = this.f3928e;
        if (str8 == null ? nativeAdImpl.f3928e != null : !str8.equals(nativeAdImpl.f3928e)) {
            return false;
        }
        String str9 = this.f3929f;
        if (str9 == null ? nativeAdImpl.f3929f != null : !str9.equals(nativeAdImpl.f3929f)) {
            return false;
        }
        String str10 = this.f3930g;
        if (str10 == null ? nativeAdImpl.f3930g != null : !str10.equals(nativeAdImpl.f3930g)) {
            return false;
        }
        String str11 = this.f3931h;
        if (str11 == null ? nativeAdImpl.f3931h != null : !str11.equals(nativeAdImpl.f3931h)) {
            return false;
        }
        String str12 = this.o;
        if (str12 == null ? nativeAdImpl.o != null : !str12.equals(nativeAdImpl.o)) {
            return false;
        }
        String str13 = this.n;
        if (str13 == null ? nativeAdImpl.n != null : !str13.equals(nativeAdImpl.n)) {
            return false;
        }
        List<com.applovin.impl.sdk.d.a> list = this.p;
        if (list == null ? nativeAdImpl.p != null : !list.equals(nativeAdImpl.p)) {
            return false;
        }
        List<com.applovin.impl.sdk.d.a> list2 = this.q;
        if (list2 == null ? nativeAdImpl.q != null : !list2.equals(nativeAdImpl.q)) {
            return false;
        }
        List<String> list3 = this.t;
        List<String> list4 = nativeAdImpl.t;
        return list3 == null ? list4 == null : list3.equals(list4);
    }

    public long getAdId() {
        return this.s;
    }

    public d getAdZone() {
        return this.f3925b;
    }

    public String getCaptionText() {
        return this.f3933j;
    }

    public String getClCode() {
        return this.r;
    }

    public String getClickUrl() {
        return this.l;
    }

    public String getCtaText() {
        return this.f3934k;
    }

    public String getDescriptionText() {
        return this.f3932i;
    }

    public String getIconUrl() {
        return this.u;
    }

    public String getImageUrl() {
        return this.v;
    }

    public String getImpressionTrackingUrl() {
        return this.m;
    }

    public List<String> getResourcePrefixes() {
        return this.t;
    }

    public String getSourceIconUrl() {
        return this.f3927d;
    }

    public String getSourceImageUrl() {
        return this.f3928e;
    }

    public String getSourceStarRatingImageUrl() {
        return this.f3929f;
    }

    public String getSourceVideoUrl() {
        return this.f3930g;
    }

    public float getStarRating() {
        return this.w;
    }

    public String getTitle() {
        return this.f3931h;
    }

    public String getVideoEndTrackingUrl(int i2, boolean z) {
        Uri build;
        if (this.o == null) {
            build = Uri.EMPTY;
        } else {
            if (i2 < 0 || i2 > 100) {
                q.c("AppLovinNativeAd", "Invalid percent viewed supplied.", new IllegalArgumentException("Percent viewed must be an integer between 0 and 100."));
            }
            build = Uri.parse(this.o).buildUpon().appendQueryParameter(QUERY_PARAM_VIDEO_PERCENT_VIEWED, Integer.toString(i2)).appendQueryParameter(QUERY_PARAM_IS_FIRST_PLAY, Boolean.toString(z)).build();
        }
        return build.toString();
    }

    public String getVideoStartTrackingUrl() {
        return this.n;
    }

    public String getVideoUrl() {
        return this.x;
    }

    public String getZoneId() {
        return this.f3926c;
    }

    public int hashCode() {
        String str = this.f3927d;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.f3928e;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.f3929f;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.f3930g;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.f3931h;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.f3932i;
        int hashCode6 = (hashCode5 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.f3933j;
        int hashCode7 = (hashCode6 + (str7 != null ? str7.hashCode() : 0)) * 31;
        String str8 = this.f3934k;
        int hashCode8 = (hashCode7 + (str8 != null ? str8.hashCode() : 0)) * 31;
        String str9 = this.l;
        int hashCode9 = (hashCode8 + (str9 != null ? str9.hashCode() : 0)) * 31;
        String str10 = this.m;
        int hashCode10 = (hashCode9 + (str10 != null ? str10.hashCode() : 0)) * 31;
        String str11 = this.n;
        int hashCode11 = (hashCode10 + (str11 != null ? str11.hashCode() : 0)) * 31;
        String str12 = this.o;
        int hashCode12 = (hashCode11 + (str12 != null ? str12.hashCode() : 0)) * 31;
        List<com.applovin.impl.sdk.d.a> list = this.p;
        int hashCode13 = (hashCode12 + (list != null ? list.hashCode() : 0)) * 31;
        List<com.applovin.impl.sdk.d.a> list2 = this.q;
        int hashCode14 = (hashCode13 + (list2 != null ? list2.hashCode() : 0)) * 31;
        String str13 = this.r;
        int hashCode15 = (hashCode14 + (str13 != null ? str13.hashCode() : 0)) * 31;
        d dVar = this.f3925b;
        int hashCode16 = (hashCode15 + (dVar != null ? dVar.hashCode() : 0)) * 31;
        List<String> list3 = this.t;
        if (list3 != null) {
            i2 = list3.hashCode();
        }
        return hashCode16 + i2;
    }

    public boolean isImagePrecached() {
        String str = this.u;
        boolean z = str != null && !str.equals(this.f3927d);
        String str2 = this.v;
        return z && (str2 != null && !str2.equals(this.f3928e));
    }

    public boolean isVideoPrecached() {
        String str = this.x;
        return str != null && !str.equals(this.f3930g);
    }

    public void launchClickTarget(Context context) {
        for (com.applovin.impl.sdk.d.a next : this.q) {
            e m2 = this.f3924a.m();
            f.b k2 = f.k();
            k2.a(next.a());
            k2.b(next.b());
            k2.a(false);
            m2.a(k2.a());
        }
        p.a(context, Uri.parse(this.l), this.f3924a);
    }

    public void setIconUrl(String str) {
        this.u = str;
    }

    public void setImageUrl(String str) {
        this.v = str;
    }

    public void setStarRating(float f2) {
        this.w = f2;
    }

    public void setVideoUrl(String str) {
        this.x = str;
    }

    public String toString() {
        return "AppLovinNativeAd{clCode='" + this.r + '\'' + ", adZone='" + this.f3925b + '\'' + ", sourceIconUrl='" + this.f3927d + '\'' + ", sourceImageUrl='" + this.f3928e + '\'' + ", sourceStarRatingImageUrl='" + this.f3929f + '\'' + ", sourceVideoUrl='" + this.f3930g + '\'' + ", title='" + this.f3931h + '\'' + ", descriptionText='" + this.f3932i + '\'' + ", captionText='" + this.f3933j + '\'' + ", ctaText='" + this.f3934k + '\'' + ", iconUrl='" + this.u + '\'' + ", imageUrl='" + this.v + '\'' + ", starRating='" + this.w + '\'' + ", videoUrl='" + this.x + '\'' + ", clickUrl='" + this.l + '\'' + ", impressionTrackingUrl='" + this.m + '\'' + ", videoStartTrackingUrl='" + this.n + '\'' + ", videoEndTrackingUrl='" + this.o + '\'' + ", impressionPostbacks=" + this.p + '\'' + ", clickTrackingPostbacks=" + this.q + '\'' + ", resourcePrefixes=" + this.t + '}';
    }

    public void trackImpression() {
        trackImpression(null);
    }

    public void trackImpression(AppLovinPostbackListener appLovinPostbackListener) {
        if (!this.y.getAndSet(true)) {
            this.f3924a.Z().b("AppLovinNativeAd", "Tracking impression...");
            for (com.applovin.impl.sdk.d.a next : this.p) {
                e m2 = this.f3924a.m();
                f.b k2 = f.k();
                k2.a(next.a());
                k2.b(next.b());
                k2.a(false);
                m2.a(k2.a(), true, appLovinPostbackListener);
            }
        } else if (appLovinPostbackListener != null) {
            appLovinPostbackListener.onPostbackFailure(this.m, AppLovinErrorCodes.NATIVE_AD_IMPRESSION_ALREADY_TRACKED);
        }
    }
}
