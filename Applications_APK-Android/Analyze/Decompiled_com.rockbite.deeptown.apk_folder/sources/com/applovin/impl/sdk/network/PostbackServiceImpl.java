package com.applovin.impl.sdk.network;

import com.applovin.impl.sdk.f;
import com.applovin.impl.sdk.k;
import com.applovin.impl.sdk.network.g;
import com.applovin.sdk.AppLovinPostbackListener;
import com.applovin.sdk.AppLovinPostbackService;

public class PostbackServiceImpl implements AppLovinPostbackService {

    /* renamed from: a  reason: collision with root package name */
    private final k f4308a;

    public PostbackServiceImpl(k kVar) {
        this.f4308a = kVar;
    }

    public void dispatchPostbackAsync(String str, AppLovinPostbackListener appLovinPostbackListener) {
        g.a b2 = g.b(this.f4308a);
        b2.d(str);
        b2.c(false);
        dispatchPostbackRequest(b2.a(), appLovinPostbackListener);
    }

    public void dispatchPostbackRequest(g gVar, f.y.b bVar, AppLovinPostbackListener appLovinPostbackListener) {
        this.f4308a.j().a(new f.q(gVar, bVar, this.f4308a, appLovinPostbackListener), bVar);
    }

    public void dispatchPostbackRequest(g gVar, AppLovinPostbackListener appLovinPostbackListener) {
        dispatchPostbackRequest(gVar, f.y.b.POSTBACKS, appLovinPostbackListener);
    }

    public String toString() {
        return "PostbackService{}";
    }
}
