package com.applovin.impl.sdk.d;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private long f4060a;

    /* renamed from: b  reason: collision with root package name */
    private long f4061b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f4062c;

    /* renamed from: d  reason: collision with root package name */
    private long f4063d;

    /* renamed from: e  reason: collision with root package name */
    private long f4064e;

    public void a() {
        this.f4062c = true;
    }

    public void a(long j2) {
        this.f4060a += j2;
    }

    public void b(long j2) {
        this.f4061b += j2;
    }

    public boolean b() {
        return this.f4062c;
    }

    public long c() {
        return this.f4060a;
    }

    public long d() {
        return this.f4061b;
    }

    public void e() {
        this.f4063d++;
    }

    public void f() {
        this.f4064e++;
    }

    public long g() {
        return this.f4063d;
    }

    public long h() {
        return this.f4064e;
    }

    public String toString() {
        return "CacheStatsTracker{totalDownloadedBytes=" + this.f4060a + ", totalCachedBytes=" + this.f4061b + ", isHTMLCachingCancelled=" + this.f4062c + ", htmlResourceCacheSuccessCount=" + this.f4063d + ", htmlResourceCacheFailureCount=" + this.f4064e + '}';
    }
}
