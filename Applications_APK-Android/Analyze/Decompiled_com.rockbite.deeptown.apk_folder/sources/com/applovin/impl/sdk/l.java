package com.applovin.impl.sdk;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.hardware.SensorManager;
import android.media.AudioDeviceInfo;
import android.media.AudioManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.webkit.WebView;
import com.applovin.impl.sdk.c;
import com.applovin.impl.sdk.f;
import com.applovin.impl.sdk.utils.k;
import com.applovin.impl.sdk.utils.m;
import com.applovin.impl.sdk.utils.p;
import com.applovin.sdk.AppLovinSdkUtils;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.tapjoy.TJAdUnitConstants;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

public class l {

    /* renamed from: f  reason: collision with root package name */
    private static String f4266f;

    /* renamed from: g  reason: collision with root package name */
    private static String f4267g;

    /* renamed from: h  reason: collision with root package name */
    private static int f4268h;

    /* renamed from: a  reason: collision with root package name */
    private final k f4269a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final q f4270b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public final Context f4271c;

    /* renamed from: d  reason: collision with root package name */
    private final Map<Class, Object> f4272d;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with root package name */
    public final AtomicReference<c> f4273e = new AtomicReference<>();

    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AtomicReference f4274a;

        /* renamed from: b  reason: collision with root package name */
        final /* synthetic */ CountDownLatch f4275b;

        a(AtomicReference atomicReference, CountDownLatch countDownLatch) {
            this.f4274a = atomicReference;
            this.f4275b = countDownLatch;
        }

        public void run() {
            try {
                this.f4274a.set(new WebView(l.this.f4271c).getSettings().getUserAgentString());
            } catch (Throwable th) {
                this.f4275b.countDown();
                throw th;
            }
            this.f4275b.countDown();
        }
    }

    class b implements f.p.a {
        b() {
        }

        public void a(c cVar) {
            l.this.f4273e.set(cVar);
        }
    }

    public static class c {

        /* renamed from: a  reason: collision with root package name */
        public boolean f4278a;

        /* renamed from: b  reason: collision with root package name */
        public String f4279b = "";
    }

    public static class d {

        /* renamed from: a  reason: collision with root package name */
        public String f4280a;

        /* renamed from: b  reason: collision with root package name */
        public String f4281b;

        /* renamed from: c  reason: collision with root package name */
        public String f4282c;

        /* renamed from: d  reason: collision with root package name */
        public String f4283d;

        /* renamed from: e  reason: collision with root package name */
        public String f4284e;

        /* renamed from: f  reason: collision with root package name */
        public String f4285f;

        /* renamed from: g  reason: collision with root package name */
        public long f4286g;
    }

    public static class e {

        /* renamed from: a  reason: collision with root package name */
        public int f4287a = -1;

        /* renamed from: b  reason: collision with root package name */
        public int f4288b = -1;
    }

    public static class f {
        public boolean A;
        public boolean B;
        public int C = -1;
        public String D;
        public long E;
        public g F = new g();
        public Boolean G;
        public Boolean H;
        public boolean I;

        /* renamed from: a  reason: collision with root package name */
        public String f4289a;

        /* renamed from: b  reason: collision with root package name */
        public String f4290b;

        /* renamed from: c  reason: collision with root package name */
        public String f4291c;

        /* renamed from: d  reason: collision with root package name */
        public String f4292d;

        /* renamed from: e  reason: collision with root package name */
        public String f4293e;

        /* renamed from: f  reason: collision with root package name */
        public String f4294f;

        /* renamed from: g  reason: collision with root package name */
        public String f4295g;

        /* renamed from: h  reason: collision with root package name */
        public int f4296h;

        /* renamed from: i  reason: collision with root package name */
        public String f4297i;

        /* renamed from: j  reason: collision with root package name */
        public String f4298j;

        /* renamed from: k  reason: collision with root package name */
        public Locale f4299k;
        public String l;
        public float m;
        public int n;
        public float o;
        public float p;
        public double q;
        public double r;
        public int s;
        public boolean t;
        public e u;
        public int v;
        public String w;
        public boolean x;
        public boolean y;
        public boolean z;
    }

    public static class g {

        /* renamed from: a  reason: collision with root package name */
        public long f4300a = -1;

        /* renamed from: b  reason: collision with root package name */
        public long f4301b = -1;

        /* renamed from: c  reason: collision with root package name */
        public long f4302c = -1;

        /* renamed from: d  reason: collision with root package name */
        public boolean f4303d = false;
    }

    protected l(k kVar) {
        if (kVar != null) {
            this.f4269a = kVar;
            this.f4270b = kVar.Z();
            this.f4271c = kVar.d();
            this.f4272d = Collections.synchronizedMap(new HashMap());
            return;
        }
        throw new IllegalArgumentException("No sdk specified");
    }

    private f a(f fVar) {
        if (fVar == null) {
            fVar = new f();
        }
        fVar.G = h.a(this.f4271c);
        fVar.H = h.b(this.f4271c);
        fVar.u = ((Boolean) this.f4269a.a(c.e.W2)).booleanValue() ? j() : null;
        if (((Boolean) this.f4269a.a(c.e.h3)).booleanValue()) {
            fVar.t = n();
        }
        try {
            AudioManager audioManager = (AudioManager) this.f4271c.getSystemService("audio");
            if (audioManager != null) {
                fVar.v = (int) (((float) audioManager.getStreamVolume(3)) * ((Float) this.f4269a.a(c.e.i3)).floatValue());
            }
        } catch (Throwable th) {
            this.f4270b.b("DataCollector", "Unable to collect volume", th);
        }
        if (((Boolean) this.f4269a.a(c.e.j3)).booleanValue()) {
            if (f4266f == null) {
                String r = r();
                if (!m.b(r)) {
                    r = "";
                }
                f4266f = r;
            }
            if (m.b(f4266f)) {
                fVar.w = f4266f;
            }
        }
        if (((Boolean) this.f4269a.a(c.e.a3)).booleanValue()) {
            try {
                fVar.E = Environment.getDataDirectory().getFreeSpace();
            } catch (Throwable th2) {
                fVar.E = -1;
                this.f4270b.b("DataCollector", "Unable to collect free space.", th2);
            }
        }
        if (((Boolean) this.f4269a.a(c.e.b3)).booleanValue()) {
            try {
                ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
                ((ActivityManager) this.f4271c.getSystemService("activity")).getMemoryInfo(memoryInfo);
                fVar.F.f4301b = memoryInfo.availMem;
                fVar.F.f4303d = memoryInfo.lowMemory;
                fVar.F.f4302c = memoryInfo.threshold;
                fVar.F.f4300a = memoryInfo.totalMem;
            } catch (Throwable th3) {
                this.f4270b.b("DataCollector", "Unable to collect memory info.", th3);
            }
        }
        String str = (String) this.f4269a.c().a(c.e.l3);
        int i2 = 0;
        if (!str.equalsIgnoreCase(f4267g)) {
            try {
                f4267g = str;
                PackageInfo packageInfo = this.f4271c.getPackageManager().getPackageInfo(str, 0);
                fVar.s = packageInfo.versionCode;
                f4268h = packageInfo.versionCode;
            } catch (Throwable unused) {
                f4268h = 0;
            }
        } else {
            fVar.s = f4268h;
        }
        if (((Boolean) this.f4269a.a(c.e.X2)).booleanValue()) {
            fVar.z = AppLovinSdkUtils.isTablet(this.f4271c);
        }
        if (((Boolean) this.f4269a.a(c.e.Y2)).booleanValue()) {
            fVar.A = m();
        }
        if (((Boolean) this.f4269a.a(c.e.Z2)).booleanValue()) {
            String k2 = k();
            if (!TextUtils.isEmpty(k2)) {
                fVar.D = k2;
            }
        }
        fVar.l = g();
        if (((Boolean) this.f4269a.a(c.e.c3)).booleanValue()) {
            fVar.B = p.d();
        }
        if (com.applovin.impl.sdk.utils.g.f()) {
            try {
                if (((PowerManager) this.f4271c.getSystemService("power")).isPowerSaveMode()) {
                    i2 = 1;
                }
                fVar.C = i2;
            } catch (Throwable th4) {
                this.f4270b.b("DataCollector", "Unable to collect power saving mode", th4);
            }
        }
        return fVar;
    }

    private String a(int i2) {
        if (i2 == 1) {
            return "receiver";
        }
        if (i2 == 2) {
            return "speaker";
        }
        if (i2 == 4 || i2 == 3) {
            return "headphones";
        }
        if (i2 == 8) {
            return "bluetootha2dpoutput";
        }
        if (i2 == 13 || i2 == 19 || i2 == 5 || i2 == 6 || i2 == 12 || i2 == 11) {
            return "lineout";
        }
        if (i2 == 9 || i2 == 10) {
            return "hdmioutput";
        }
        return null;
    }

    private boolean a(String str) {
        if (str != null) {
            Context context = this.f4271c;
            if (context != null) {
                return k.a(str, context.getPackageName(), this.f4271c.getPackageManager()) == 0;
            }
            throw new IllegalArgumentException("No context specified");
        }
        throw new IllegalArgumentException("No permission name specified");
    }

    private boolean a(String str, c.e<String> eVar) {
        for (String startsWith : com.applovin.impl.sdk.utils.e.a((String) this.f4269a.a(eVar))) {
            if (str.startsWith(startsWith)) {
                return true;
            }
        }
        return false;
    }

    private String b(String str) {
        int length = str.length();
        int[] iArr = {11, 12, 10, 3, 2, 1, 15, 10, 15, 14};
        int length2 = iArr.length;
        char[] cArr = new char[length];
        for (int i2 = 0; i2 < length; i2++) {
            cArr[i2] = str.charAt(i2);
            for (int i3 = length2 - 1; i3 >= 0; i3--) {
                cArr[i2] = (char) (cArr[i2] ^ iArr[i3]);
            }
        }
        return new String(cArr);
    }

    private Map<String, String> f() {
        return a(null, false, true);
    }

    private String g() {
        try {
            int d2 = p.d(this.f4271c);
            return d2 == 1 ? TJAdUnitConstants.String.PORTRAIT : d2 == 2 ? TJAdUnitConstants.String.LANDSCAPE : "none";
        } catch (Throwable th) {
            this.f4269a.Z().b("DataCollector", "Encountered error while attempting to collect application orientation", th);
            return "none";
        }
    }

    private c h() {
        if (i()) {
            try {
                c cVar = new c();
                AdvertisingIdClient.Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(this.f4271c);
                cVar.f4278a = advertisingIdInfo.isLimitAdTrackingEnabled();
                cVar.f4279b = advertisingIdInfo.getId();
                return cVar;
            } catch (Throwable th) {
                this.f4270b.b("DataCollector", "Could not collect Google Advertising ID - this will negatively impact your eCPMs! Please integrate the Google Play Services SDK into your application. More info can be found online at http://developer.android.com/google/play-services/setup.html. If you're sure you've integrated the SDK and are still seeing this message, you may need to add a ProGuard exception: -keep public class com.google.android.gms.** { public protected *; }", th);
            }
        } else {
            q.i("DataCollector", "Could not collect Google Advertising ID - this will negatively impact your eCPMs! Please integrate the Google Play Services SDK into your application. More info can be found online at http://developer.android.com/google/play-services/setup.html. If you're sure you've integrated the SDK and are still seeing this message, you may need to add a ProGuard exception: -keep public class com.google.android.gms.** { public protected *; }");
            return new c();
        }
    }

    private boolean i() {
        return p.e("com.google.android.gms.ads.identifier.AdvertisingIdClient");
    }

    private e j() {
        try {
            e eVar = new e();
            Intent registerReceiver = this.f4271c.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            int i2 = -1;
            int intExtra = registerReceiver != null ? registerReceiver.getIntExtra("level", -1) : -1;
            int intExtra2 = registerReceiver != null ? registerReceiver.getIntExtra("scale", -1) : -1;
            if (intExtra <= 0 || intExtra2 <= 0) {
                eVar.f4288b = -1;
            } else {
                eVar.f4288b = (int) ((((float) intExtra) / ((float) intExtra2)) * 100.0f);
            }
            if (registerReceiver != null) {
                i2 = registerReceiver.getIntExtra("status", -1);
            }
            eVar.f4287a = i2;
            return eVar;
        } catch (Throwable th) {
            this.f4270b.b("DataCollector", "Unable to collect battery info", th);
            return null;
        }
    }

    private String k() {
        try {
            AudioManager audioManager = (AudioManager) this.f4271c.getSystemService("audio");
            StringBuilder sb = new StringBuilder();
            if (com.applovin.impl.sdk.utils.g.g()) {
                for (AudioDeviceInfo type : audioManager.getDevices(2)) {
                    String a2 = a(type.getType());
                    if (!TextUtils.isEmpty(a2)) {
                        sb.append(a2);
                        sb.append(",");
                    }
                }
            } else {
                if (audioManager.isWiredHeadsetOn()) {
                    sb.append("headphones");
                    sb.append(",");
                }
                if (audioManager.isBluetoothA2dpOn()) {
                    sb.append("bluetootha2dpoutput");
                }
            }
            if (sb.length() > 0 && sb.charAt(sb.length() - 1) == ',') {
                sb.deleteCharAt(sb.length() - 1);
            }
            String sb2 = sb.toString();
            if (TextUtils.isEmpty(sb2)) {
                this.f4270b.b("DataCollector", "No sound outputs detected");
            }
            return sb2;
        } catch (Throwable th) {
            this.f4270b.b("DataCollector", "Unable to collect sound outputs", th);
            return null;
        }
    }

    private double l() {
        double offset = (double) TimeZone.getDefault().getOffset(new Date().getTime());
        Double.isNaN(offset);
        double round = (double) Math.round((offset * 10.0d) / 3600000.0d);
        Double.isNaN(round);
        return round / 10.0d;
    }

    private boolean m() {
        try {
            PackageManager packageManager = this.f4271c.getPackageManager();
            return com.applovin.impl.sdk.utils.g.f() ? packageManager.hasSystemFeature("android.software.leanback") : com.applovin.impl.sdk.utils.g.c() ? packageManager.hasSystemFeature("android.hardware.type.television") : !packageManager.hasSystemFeature("android.hardware.touchscreen");
        } catch (Throwable th) {
            this.f4270b.b("DataCollector", "Failed to determine if device is TV.", th);
            return false;
        }
    }

    private boolean n() {
        try {
            return o() || p();
        } catch (Throwable unused) {
            return false;
        }
    }

    private boolean o() {
        String str = Build.TAGS;
        return str != null && str.contains(b("lz}$blpz"));
    }

    private boolean p() {
        for (String b2 : new String[]{"&zpz}ld&hyy&Z|yl{|zl{'hyb", "&zk`g&z|", "&zpz}ld&k`g&z|", "&zpz}ld&qk`g&z|", "&mh}h&efjhe&qk`g&z|", "&mh}h&efjhe&k`g&z|", "&zpz}ld&zm&qk`g&z|", "&zpz}ld&k`g&oh`ezhol&z|", "&mh}h&efjhe&z|"}) {
            if (new File(b(b2)).exists()) {
                return true;
            }
        }
        return false;
    }

    private boolean q() {
        return a(Build.DEVICE, c.e.e3) || a(Build.HARDWARE, c.e.d3) || a(Build.MANUFACTURER, c.e.f3) || a(Build.MODEL, c.e.g3);
    }

    private String r() {
        AtomicReference atomicReference = new AtomicReference();
        CountDownLatch countDownLatch = new CountDownLatch(1);
        new Handler(this.f4271c.getMainLooper()).post(new a(atomicReference, countDownLatch));
        try {
            countDownLatch.await(((Long) this.f4269a.a(c.e.k3)).longValue(), TimeUnit.MILLISECONDS);
        } catch (Throwable unused) {
        }
        return (String) atomicReference.get();
    }

    /* access modifiers changed from: package-private */
    public String a() {
        String encodeToString = Base64.encodeToString(new JSONObject(f()).toString().getBytes(Charset.defaultCharset()), 2);
        if (!((Boolean) this.f4269a.a(c.e.H3)).booleanValue()) {
            return encodeToString;
        }
        return com.applovin.impl.sdk.utils.l.a(encodeToString, this.f4269a.X(), p.a(this.f4269a));
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x03df  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x03ff  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x041a  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0435  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0442  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x045e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.Map<java.lang.String, java.lang.String> a(java.util.Map<java.lang.String, java.lang.String> r6, boolean r7, boolean r8) {
        /*
            r5 = this;
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            com.applovin.impl.sdk.l$f r1 = r5.b()
            java.lang.String r2 = r1.f4292d
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.d(r2)
            java.lang.String r3 = "brand"
            r0.put(r3, r2)
            java.lang.String r2 = r1.f4293e
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.d(r2)
            java.lang.String r3 = "brand_name"
            r0.put(r3, r2)
            java.lang.String r2 = r1.f4294f
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.d(r2)
            java.lang.String r3 = "hardware"
            r0.put(r3, r2)
            int r2 = r1.f4296h
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "api_level"
            r0.put(r3, r2)
            java.lang.String r2 = r1.f4298j
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.d(r2)
            java.lang.String r3 = "carrier"
            r0.put(r3, r2)
            java.lang.String r2 = r1.f4297i
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.d(r2)
            java.lang.String r3 = "country_code"
            r0.put(r3, r2)
            java.util.Locale r2 = r1.f4299k
            java.lang.String r2 = r2.toString()
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.d(r2)
            java.lang.String r3 = "locale"
            r0.put(r3, r2)
            java.lang.String r2 = r1.f4289a
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.d(r2)
            java.lang.String r3 = "model"
            r0.put(r3, r2)
            java.lang.String r2 = r1.f4290b
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.d(r2)
            java.lang.String r3 = "os"
            r0.put(r3, r2)
            java.lang.String r2 = r1.f4291c
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.d(r2)
            java.lang.String r3 = "platform"
            r0.put(r3, r2)
            java.lang.String r2 = r1.f4295g
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.d(r2)
            java.lang.String r3 = "revision"
            r0.put(r3, r2)
            java.lang.String r2 = r1.l
            java.lang.String r3 = "orientation_lock"
            r0.put(r3, r2)
            double r2 = r1.r
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "tz_offset"
            r0.put(r3, r2)
            boolean r2 = r1.I
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "aida"
            r0.put(r3, r2)
            int r2 = r1.s
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "wvvc"
            r0.put(r3, r2)
            float r2 = r1.m
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "adns"
            r0.put(r3, r2)
            int r2 = r1.n
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "adnsd"
            r0.put(r3, r2)
            float r2 = r1.o
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "xdpi"
            r0.put(r3, r2)
            float r2 = r1.p
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "ydpi"
            r0.put(r3, r2)
            double r2 = r1.q
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "screen_size_in"
            r0.put(r3, r2)
            boolean r2 = r1.x
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.a(r2)
            java.lang.String r3 = "sim"
            r0.put(r3, r2)
            boolean r2 = r1.y
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.a(r2)
            java.lang.String r3 = "gy"
            r0.put(r3, r2)
            boolean r2 = r1.z
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.a(r2)
            java.lang.String r3 = "is_tablet"
            r0.put(r3, r2)
            boolean r2 = r1.A
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.a(r2)
            java.lang.String r3 = "tv"
            r0.put(r3, r2)
            boolean r2 = r1.B
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.a(r2)
            java.lang.String r3 = "vs"
            r0.put(r3, r2)
            int r2 = r1.C
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "lpm"
            r0.put(r3, r2)
            long r2 = r1.E
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "fs"
            r0.put(r3, r2)
            com.applovin.impl.sdk.l$g r2 = r1.F
            long r2 = r2.f4301b
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "fm"
            r0.put(r3, r2)
            com.applovin.impl.sdk.l$g r2 = r1.F
            long r2 = r2.f4300a
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "tm"
            r0.put(r3, r2)
            com.applovin.impl.sdk.l$g r2 = r1.F
            long r2 = r2.f4302c
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "lmt"
            r0.put(r3, r2)
            com.applovin.impl.sdk.l$g r2 = r1.F
            boolean r2 = r2.f4303d
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "lm"
            r0.put(r3, r2)
            boolean r2 = r1.t
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.a(r2)
            java.lang.String r3 = "adr"
            r0.put(r3, r2)
            int r2 = r1.v
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "volume"
            r0.put(r3, r2)
            java.lang.String r2 = r1.w
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.d(r2)
            java.lang.String r3 = "ua"
            com.applovin.impl.sdk.utils.p.a(r3, r2, r0)
            java.lang.String r2 = r1.D
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.d(r2)
            java.lang.String r3 = "so"
            com.applovin.impl.sdk.utils.p.a(r3, r2, r0)
            com.applovin.impl.sdk.l$e r2 = r1.u
            if (r2 == 0) goto L_0x01ac
            int r3 = r2.f4287a
            java.lang.String r3 = java.lang.String.valueOf(r3)
            java.lang.String r4 = "act"
            r0.put(r4, r3)
            int r2 = r2.f4288b
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "acm"
            r0.put(r3, r2)
        L_0x01ac:
            java.lang.Boolean r2 = r1.G
            if (r2 == 0) goto L_0x01b9
            java.lang.String r2 = r2.toString()
            java.lang.String r3 = "huc"
            r0.put(r3, r2)
        L_0x01b9:
            java.lang.Boolean r1 = r1.H
            if (r1 == 0) goto L_0x01c6
            java.lang.String r1 = r1.toString()
            java.lang.String r2 = "aru"
            r0.put(r2, r1)
        L_0x01c6:
            android.content.Context r1 = r5.f4271c
            android.graphics.Point r1 = com.applovin.impl.sdk.utils.g.a(r1)
            int r2 = r1.x
            java.lang.String r2 = java.lang.Integer.toString(r2)
            java.lang.String r3 = "dx"
            r0.put(r3, r2)
            int r1 = r1.y
            java.lang.String r1 = java.lang.Integer.toString(r1)
            java.lang.String r2 = "dy"
            r0.put(r2, r1)
            java.lang.String r1 = "accept"
            java.lang.String r2 = "custom_size,launch_app,video"
            r0.put(r1, r2)
            com.applovin.impl.sdk.k r1 = r5.f4269a
            com.applovin.impl.sdk.c$e<java.lang.String> r2 = com.applovin.impl.sdk.c.e.f4001f
            java.lang.Object r1 = r1.a(r2)
            java.lang.String r2 = "api_did"
            r0.put(r2, r1)
            java.lang.String r1 = com.applovin.sdk.AppLovinSdk.VERSION
            java.lang.String r2 = "sdk_version"
            r0.put(r2, r1)
            r1 = 131(0x83, float:1.84E-43)
            java.lang.String r1 = java.lang.Integer.toString(r1)
            java.lang.String r2 = "build"
            r0.put(r2, r1)
            java.lang.String r1 = "format"
            java.lang.String r2 = "json"
            r0.put(r1, r2)
            com.applovin.impl.sdk.l$d r1 = r5.c()
            java.lang.String r2 = r1.f4281b
            java.lang.String r2 = com.applovin.impl.sdk.utils.m.d(r2)
            java.lang.String r3 = "app_version"
            r0.put(r3, r2)
            long r2 = r1.f4286g
            java.lang.String r2 = java.lang.Long.toString(r2)
            java.lang.String r3 = "ia"
            r0.put(r3, r2)
            java.lang.String r2 = r1.f4284e
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "tg"
            r0.put(r3, r2)
            java.lang.String r2 = r1.f4283d
            java.lang.String r3 = "installer_name"
            r0.put(r3, r2)
            java.lang.String r1 = r1.f4285f
            java.lang.String r2 = "debug"
            r0.put(r2, r1)
            com.applovin.impl.sdk.k r1 = r5.f4269a
            java.lang.String r1 = r1.R()
            java.lang.String r1 = com.applovin.impl.sdk.utils.m.d(r1)
            java.lang.String r2 = "mediation_provider"
            com.applovin.impl.sdk.utils.p.a(r2, r1, r0)
            com.applovin.impl.sdk.k r1 = r5.f4269a
            java.lang.String r1 = com.applovin.impl.sdk.utils.h.b(r1)
            java.lang.String r2 = "network"
            r0.put(r2, r1)
            com.applovin.impl.sdk.k r1 = r5.f4269a
            com.applovin.impl.sdk.c$e<java.lang.String> r2 = com.applovin.impl.sdk.c.e.V2
            java.lang.Object r1 = r1.a(r2)
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r2 = "plugin_version"
            com.applovin.impl.sdk.utils.p.a(r2, r1, r0)
            java.lang.String r7 = java.lang.String.valueOf(r7)
            java.lang.String r1 = "preloading"
            r0.put(r1, r7)
            com.applovin.impl.sdk.k r7 = r5.f4269a
            com.applovin.sdk.AppLovinSdkSettings r7 = r7.P()
            boolean r7 = r7.isTestAdsEnabled()
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r7)
            java.lang.String r1 = "test_ads"
            com.applovin.impl.sdk.utils.p.a(r1, r7, r0)
            com.applovin.impl.sdk.k r7 = r5.f4269a
            boolean r7 = r7.g()
            java.lang.String r7 = java.lang.String.valueOf(r7)
            java.lang.String r1 = "first_install"
            r0.put(r1, r7)
            com.applovin.impl.sdk.k r7 = r5.f4269a
            boolean r7 = r7.h()
            r1 = 1
            r7 = r7 ^ r1
            java.lang.String r7 = java.lang.String.valueOf(r7)
            java.lang.String r2 = "first_install_v2"
            r0.put(r2, r7)
            com.applovin.impl.sdk.k r7 = r5.f4269a
            com.applovin.impl.sdk.c$e<java.lang.Boolean> r2 = com.applovin.impl.sdk.c.e.G3
            java.lang.Object r7 = r7.a(r2)
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r7 = r7.booleanValue()
            if (r7 != 0) goto L_0x02c2
            com.applovin.impl.sdk.k r7 = r5.f4269a
            java.lang.String r7 = r7.X()
            java.lang.String r2 = "sdk_key"
            r0.put(r2, r7)
        L_0x02c2:
            com.applovin.impl.sdk.k r7 = r5.f4269a
            com.applovin.impl.sdk.c$e<java.lang.String> r2 = com.applovin.impl.sdk.c.e.f4004i
            java.lang.Object r7 = r7.a(r2)
            java.lang.String r2 = "sc"
            r0.put(r2, r7)
            com.applovin.impl.sdk.k r7 = r5.f4269a
            com.applovin.impl.sdk.c$e<java.lang.String> r2 = com.applovin.impl.sdk.c.e.f4005j
            java.lang.Object r7 = r7.a(r2)
            java.lang.String r2 = "sc2"
            r0.put(r2, r7)
            com.applovin.impl.sdk.k r7 = r5.f4269a
            com.applovin.impl.sdk.c$e<java.lang.String> r2 = com.applovin.impl.sdk.c.e.f4006k
            java.lang.Object r7 = r7.a(r2)
            java.lang.String r7 = (java.lang.String) r7
            java.lang.String r7 = com.applovin.impl.sdk.utils.m.d(r7)
            java.lang.String r2 = "server_installed_at"
            r0.put(r2, r7)
            com.applovin.impl.sdk.k r7 = r5.f4269a
            com.applovin.impl.sdk.c$g<java.lang.String> r2 = com.applovin.impl.sdk.c.g.z
            java.lang.Object r7 = r7.a(r2)
            java.lang.String r7 = (java.lang.String) r7
            java.lang.String r7 = com.applovin.impl.sdk.utils.m.d(r7)
            java.lang.String r2 = "persisted_data"
            com.applovin.impl.sdk.utils.p.a(r2, r7, r0)
            android.content.Context r7 = r5.f4271c
            java.lang.String r2 = "android.permission.WRITE_EXTERNAL_STORAGE"
            boolean r7 = com.applovin.impl.sdk.utils.g.a(r2, r7)
            java.lang.String r7 = java.lang.Boolean.toString(r7)
            java.lang.String r2 = "v1"
            r0.put(r2, r7)
            java.lang.String r7 = "true"
            java.lang.String r2 = "v2"
            r0.put(r2, r7)
            java.lang.String r2 = "v3"
            r0.put(r2, r7)
            java.lang.String r2 = "v4"
            r0.put(r2, r7)
            java.lang.String r2 = "v5"
            r0.put(r2, r7)
            com.applovin.impl.sdk.k r7 = r5.f4269a
            com.applovin.impl.sdk.c$e<java.lang.Boolean> r2 = com.applovin.impl.sdk.c.e.m3
            java.lang.Object r7 = r7.a(r2)
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r7 = r7.booleanValue()
            if (r7 == 0) goto L_0x038a
            com.applovin.impl.sdk.k r7 = r5.f4269a
            com.applovin.impl.sdk.d.h r7 = r7.k()
            com.applovin.impl.sdk.d.g r2 = com.applovin.impl.sdk.d.g.f4076e
            long r2 = r7.b(r2)
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "li"
            r0.put(r3, r2)
            com.applovin.impl.sdk.d.g r2 = com.applovin.impl.sdk.d.g.f4078g
            long r2 = r7.b(r2)
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "si"
            r0.put(r3, r2)
            com.applovin.impl.sdk.d.g r2 = com.applovin.impl.sdk.d.g.f4082k
            long r2 = r7.b(r2)
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "pf"
            r0.put(r3, r2)
            com.applovin.impl.sdk.d.g r2 = com.applovin.impl.sdk.d.g.q
            long r2 = r7.b(r2)
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = "mpf"
            r0.put(r3, r2)
            com.applovin.impl.sdk.d.g r2 = com.applovin.impl.sdk.d.g.l
            long r2 = r7.b(r2)
            java.lang.String r7 = java.lang.String.valueOf(r2)
            java.lang.String r2 = "gpf"
            r0.put(r2, r7)
        L_0x038a:
            android.content.Context r7 = r5.f4271c
            java.lang.String r7 = r7.getPackageName()
            java.lang.String r7 = com.applovin.impl.sdk.utils.m.e(r7)
            java.lang.String r2 = "vz"
            r0.put(r2, r7)
            com.applovin.impl.sdk.k r7 = r5.f4269a
            boolean r7 = r7.L()
            java.lang.String r7 = java.lang.Boolean.toString(r7)
            java.lang.String r2 = "pnr"
            r0.put(r2, r7)
            if (r8 == 0) goto L_0x03cd
            java.util.concurrent.atomic.AtomicReference<com.applovin.impl.sdk.l$c> r7 = r5.f4273e
            java.lang.Object r7 = r7.get()
            com.applovin.impl.sdk.l$c r7 = (com.applovin.impl.sdk.l.c) r7
            if (r7 == 0) goto L_0x03b8
            r5.e()
            goto L_0x03d7
        L_0x03b8:
            boolean r7 = com.applovin.impl.sdk.utils.p.b()
            if (r7 == 0) goto L_0x03cd
            com.applovin.impl.sdk.l$c r7 = new com.applovin.impl.sdk.l$c
            r7.<init>()
            java.lang.String r8 = java.lang.Boolean.toString(r1)
            java.lang.String r1 = "inc"
            r0.put(r1, r8)
            goto L_0x03d7
        L_0x03cd:
            com.applovin.impl.sdk.k r7 = r5.f4269a
            com.applovin.impl.sdk.l r7 = r7.n()
            com.applovin.impl.sdk.l$c r7 = r7.d()
        L_0x03d7:
            java.lang.String r8 = r7.f4279b
            boolean r1 = com.applovin.impl.sdk.utils.m.b(r8)
            if (r1 == 0) goto L_0x03e4
            java.lang.String r1 = "idfa"
            r0.put(r1, r8)
        L_0x03e4:
            boolean r7 = r7.f4278a
            java.lang.String r7 = java.lang.Boolean.toString(r7)
            java.lang.String r8 = "dnt"
            r0.put(r8, r7)
            com.applovin.impl.sdk.k r7 = r5.f4269a
            com.applovin.impl.sdk.c$e<java.lang.Boolean> r8 = com.applovin.impl.sdk.c.e.O2
            java.lang.Object r7 = r7.a(r8)
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r7 = r7.booleanValue()
            if (r7 == 0) goto L_0x040a
            com.applovin.impl.sdk.k r7 = r5.f4269a
            java.lang.String r7 = r7.M()
            java.lang.String r8 = "cuid"
            com.applovin.impl.sdk.utils.p.a(r8, r7, r0)
        L_0x040a:
            com.applovin.impl.sdk.k r7 = r5.f4269a
            com.applovin.impl.sdk.c$e<java.lang.Boolean> r8 = com.applovin.impl.sdk.c.e.R2
            java.lang.Object r7 = r7.a(r8)
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r7 = r7.booleanValue()
            if (r7 == 0) goto L_0x0425
            com.applovin.impl.sdk.k r7 = r5.f4269a
            java.lang.String r7 = r7.N()
            java.lang.String r8 = "compass_random_token"
            r0.put(r8, r7)
        L_0x0425:
            com.applovin.impl.sdk.k r7 = r5.f4269a
            com.applovin.impl.sdk.c$e<java.lang.Boolean> r8 = com.applovin.impl.sdk.c.e.T2
            java.lang.Object r7 = r7.a(r8)
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r7 = r7.booleanValue()
            if (r7 == 0) goto L_0x0440
            com.applovin.impl.sdk.k r7 = r5.f4269a
            java.lang.String r7 = r7.O()
            java.lang.String r8 = "applovin_random_token"
            r0.put(r8, r7)
        L_0x0440:
            if (r6 == 0) goto L_0x0445
            r0.putAll(r6)
        L_0x0445:
            java.util.UUID r6 = java.util.UUID.randomUUID()
            java.lang.String r6 = r6.toString()
            java.lang.String r7 = "rid"
            r0.put(r7, r6)
            com.applovin.impl.sdk.k r6 = r5.f4269a
            com.applovin.impl.sdk.network.a r6 = r6.i()
            com.applovin.impl.sdk.network.a$b r6 = r6.a()
            if (r6 == 0) goto L_0x048e
            long r7 = r6.a()
            java.lang.String r7 = java.lang.String.valueOf(r7)
            java.lang.String r8 = "lrm_ts_ms"
            r0.put(r8, r7)
            java.lang.String r7 = r6.b()
            java.lang.String r8 = "lrm_url"
            r0.put(r8, r7)
            long r7 = r6.d()
            java.lang.String r7 = java.lang.String.valueOf(r7)
            java.lang.String r8 = "lrm_ct_ms"
            r0.put(r8, r7)
            long r6 = r6.c()
            java.lang.String r6 = java.lang.String.valueOf(r6)
            java.lang.String r7 = "lrm_rs"
            r0.put(r7, r6)
        L_0x048e:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.l.a(java.util.Map, boolean, boolean):java.util.Map");
    }

    public f b() {
        f fVar;
        TelephonyManager telephonyManager;
        Class<f> cls = f.class;
        Object obj = this.f4272d.get(cls);
        if (obj != null) {
            fVar = (f) obj;
        } else {
            fVar = new f();
            fVar.f4299k = Locale.getDefault();
            fVar.f4289a = Build.MODEL;
            fVar.f4290b = Build.VERSION.RELEASE;
            fVar.f4291c = "android";
            fVar.f4292d = Build.MANUFACTURER;
            fVar.f4293e = Build.BRAND;
            fVar.f4294f = Build.HARDWARE;
            fVar.f4296h = Build.VERSION.SDK_INT;
            fVar.f4295g = Build.DEVICE;
            fVar.r = l();
            fVar.x = q();
            fVar.I = i();
            try {
                fVar.y = ((SensorManager) this.f4271c.getSystemService("sensor")).getDefaultSensor(4) != null;
            } catch (Throwable th) {
                this.f4270b.b("DataCollector", "Unable to retrieve gyroscope availability", th);
            }
            if (a("android.permission.READ_PHONE_STATE") && (telephonyManager = (TelephonyManager) this.f4271c.getSystemService("phone")) != null) {
                fVar.f4297i = telephonyManager.getSimCountryIso().toUpperCase(Locale.ENGLISH);
                String networkOperatorName = telephonyManager.getNetworkOperatorName();
                try {
                    fVar.f4298j = URLEncoder.encode(networkOperatorName, "UTF-8");
                } catch (UnsupportedEncodingException unused) {
                    fVar.f4298j = networkOperatorName;
                }
            }
            try {
                DisplayMetrics displayMetrics = this.f4271c.getResources().getDisplayMetrics();
                fVar.m = displayMetrics.density;
                fVar.n = displayMetrics.densityDpi;
                fVar.o = displayMetrics.xdpi;
                fVar.p = displayMetrics.ydpi;
                Point a2 = com.applovin.impl.sdk.utils.g.a(this.f4271c);
                double sqrt = Math.sqrt(Math.pow((double) a2.x, 2.0d) + Math.pow((double) a2.y, 2.0d));
                double d2 = (double) fVar.o;
                Double.isNaN(d2);
                fVar.q = sqrt / d2;
            } catch (Throwable unused2) {
            }
            this.f4272d.put(cls, fVar);
        }
        return a(fVar);
    }

    public d c() {
        PackageInfo packageInfo;
        Class<d> cls = d.class;
        Object obj = this.f4272d.get(cls);
        if (obj != null) {
            return (d) obj;
        }
        ApplicationInfo applicationInfo = this.f4271c.getApplicationInfo();
        long lastModified = new File(applicationInfo.sourceDir).lastModified();
        PackageManager packageManager = this.f4271c.getPackageManager();
        String str = null;
        try {
            packageInfo = packageManager.getPackageInfo(this.f4271c.getPackageName(), 0);
            try {
                str = packageManager.getInstallerPackageName(applicationInfo.packageName);
            } catch (Throwable unused) {
            }
        } catch (Throwable unused2) {
            packageInfo = null;
        }
        d dVar = new d();
        dVar.f4282c = applicationInfo.packageName;
        String str2 = "";
        if (str == null) {
            str = str2;
        }
        dVar.f4283d = str;
        dVar.f4286g = lastModified;
        dVar.f4280a = String.valueOf(packageManager.getApplicationLabel(applicationInfo));
        if (packageInfo != null) {
            str2 = packageInfo.versionName;
        }
        dVar.f4281b = str2;
        dVar.f4284e = (String) this.f4269a.a(c.g.f4021i);
        dVar.f4285f = Boolean.toString(p.b(this.f4269a));
        this.f4272d.put(cls, dVar);
        return dVar;
    }

    public c d() {
        c h2 = h();
        if (!((Boolean) this.f4269a.a(c.e.N2)).booleanValue()) {
            return new c();
        }
        if (!h2.f4278a || ((Boolean) this.f4269a.a(c.e.M2)).booleanValue()) {
            return h2;
        }
        h2.f4279b = "";
        return h2;
    }

    public void e() {
        this.f4269a.j().a(new f.p(this.f4269a, new b()), f.y.b.ADVERTISING_INFO_COLLECTION);
    }
}
