package com.applovin.mediation.ads;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.applovin.adview.AppLovinAdView;
import com.applovin.impl.mediation.ads.MaxAdViewImpl;
import com.applovin.impl.sdk.utils.p;
import com.applovin.impl.sdk.utils.q;
import com.applovin.mediation.MaxAdViewAdListener;
import com.applovin.sdk.AppLovinSdk;

public class MaxAdView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private MaxAdViewImpl f4559a;

    /* renamed from: b  reason: collision with root package name */
    private View f4560b;

    /* renamed from: c  reason: collision with root package name */
    private int f4561c;

    public MaxAdView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public MaxAdView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        String attributeValue = attributeSet.getAttributeValue(AppLovinAdView.NAMESPACE, "adUnitId");
        int attributeIntValue = attributeSet.getAttributeIntValue("http://schemas.android.com/apk/res/android", "gravity", 49);
        if (attributeValue == null) {
            throw new IllegalArgumentException("No ad unit ID specified");
        } else if (TextUtils.isEmpty(attributeValue)) {
            throw new IllegalArgumentException("Empty ad unit ID specified");
        } else if (context instanceof Activity) {
            Activity activity = (Activity) context;
            a(attributeValue, attributeIntValue, AppLovinSdk.getInstance(activity), activity);
        } else {
            throw new IllegalArgumentException("Max ad view context is not an activity");
        }
    }

    public MaxAdView(String str, Activity activity) {
        this(str, AppLovinSdk.getInstance(activity), activity);
    }

    public MaxAdView(String str, AppLovinSdk appLovinSdk, Activity activity) {
        super(activity);
        a(str, 49, appLovinSdk, activity);
    }

    private void a(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        TextView textView = new TextView(context);
        textView.setBackgroundColor(Color.rgb(220, 220, 220));
        textView.setTextColor(-16777216);
        textView.setText("AppLovin MAX Ad");
        textView.setGravity(17);
        addView(textView, displayMetrics.widthPixels, (int) TypedValue.applyDimension(1, 50.0f, displayMetrics));
    }

    private void a(String str, int i2, AppLovinSdk appLovinSdk, Activity activity) {
        if (!isInEditMode()) {
            this.f4560b = new View(activity);
            this.f4560b.setBackgroundColor(0);
            addView(this.f4560b);
            this.f4560b.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            this.f4561c = getVisibility();
            this.f4559a = new MaxAdViewImpl(str, this, this.f4560b, p.a(appLovinSdk), activity);
            setGravity(i2);
            if (getBackground() instanceof ColorDrawable) {
                setBackgroundColor(((ColorDrawable) getBackground()).getColor());
            }
            super.setBackgroundColor(0);
            return;
        }
        a(activity);
    }

    public void destroy() {
        this.f4559a.destroy();
    }

    public String getPlacement() {
        return this.f4559a.getPlacement();
    }

    public void loadAd() {
        this.f4559a.loadAd();
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        super.onWindowVisibilityChanged(i2);
        if (this.f4559a != null && q.a(this.f4561c, i2)) {
            this.f4559a.onWindowVisibilityChanged(i2);
        }
        this.f4561c = i2;
    }

    public void setAlpha(float f2) {
        View view = this.f4560b;
        if (view != null) {
            view.setAlpha(f2);
        }
    }

    public void setBackgroundColor(int i2) {
        MaxAdViewImpl maxAdViewImpl = this.f4559a;
        if (maxAdViewImpl != null) {
            maxAdViewImpl.setPublisherBackgroundColor(i2);
        }
        View view = this.f4560b;
        if (view != null) {
            view.setBackgroundColor(i2);
        }
    }

    public void setExtraParameter(String str, String str2) {
        this.f4559a.setExtraParameter(str, str2);
    }

    public void setListener(MaxAdViewAdListener maxAdViewAdListener) {
        this.f4559a.setListener(maxAdViewAdListener);
    }

    public void setPlacement(String str) {
        this.f4559a.setPlacement(str);
    }

    public void startAutoRefresh() {
        this.f4559a.startAutoRefresh();
    }

    public void stopAutoRefresh() {
        this.f4559a.stopAutoRefresh();
    }

    public String toString() {
        MaxAdViewImpl maxAdViewImpl = this.f4559a;
        return maxAdViewImpl != null ? maxAdViewImpl.toString() : "MaxAdView";
    }
}
