package com.applovin.mediation.ads;

import android.app.Activity;
import android.text.TextUtils;
import com.applovin.impl.mediation.ads.MaxFullscreenAdImpl;
import com.applovin.impl.sdk.utils.p;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.sdk.AppLovinSdk;
import java.lang.ref.WeakReference;

public class MaxInterstitialAd implements MaxFullscreenAdImpl.d {

    /* renamed from: b  reason: collision with root package name */
    private static WeakReference<Activity> f4562b = new WeakReference<>(null);

    /* renamed from: a  reason: collision with root package name */
    private final MaxFullscreenAdImpl f4563a;

    public MaxInterstitialAd(String str, Activity activity) {
        this(str, AppLovinSdk.getInstance(activity), activity);
    }

    public MaxInterstitialAd(String str, AppLovinSdk appLovinSdk, Activity activity) {
        if (str == null) {
            throw new IllegalArgumentException("No ad unit ID specified");
        } else if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("Empty ad unit ID specified");
        } else if (activity == null) {
            throw new IllegalArgumentException("No activity specified");
        } else if (appLovinSdk != null) {
            f4562b = new WeakReference<>(activity);
            this.f4563a = new MaxFullscreenAdImpl(str, MaxAdFormat.INTERSTITIAL, this, "MaxInterstitialAd", p.a(appLovinSdk));
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    public void destroy() {
        this.f4563a.destroy();
    }

    public Activity getActivity() {
        return f4562b.get();
    }

    public boolean isReady() {
        return this.f4563a.isReady();
    }

    public void loadAd() {
        this.f4563a.loadAd(getActivity());
    }

    public void setExtraParameter(String str, String str2) {
        this.f4563a.setExtraParameter(str, str2);
    }

    public void setListener(MaxAdListener maxAdListener) {
        this.f4563a.setListener(maxAdListener);
    }

    public void showAd() {
        showAd(null);
    }

    public void showAd(String str) {
        this.f4563a.showAd(str, getActivity());
    }

    public String toString() {
        return "" + this.f4563a;
    }
}
