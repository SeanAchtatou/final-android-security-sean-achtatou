package com.applovin.mediation.ads;

import android.app.Activity;
import android.text.TextUtils;
import com.applovin.impl.mediation.ads.MaxFullscreenAdImpl;
import com.applovin.impl.sdk.utils.p;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxRewardedAdListener;
import com.applovin.sdk.AppLovinSdk;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

public class MaxRewardedAd implements MaxFullscreenAdImpl.d {

    /* renamed from: b  reason: collision with root package name */
    private static final Map<String, MaxRewardedAd> f4564b = new HashMap();

    /* renamed from: c  reason: collision with root package name */
    private static final Object f4565c = new Object();

    /* renamed from: d  reason: collision with root package name */
    private static WeakReference<Activity> f4566d = new WeakReference<>(null);

    /* renamed from: a  reason: collision with root package name */
    private final MaxFullscreenAdImpl f4567a;

    private MaxRewardedAd(String str, AppLovinSdk appLovinSdk) {
        this.f4567a = new MaxFullscreenAdImpl(str, MaxAdFormat.REWARDED, this, "MaxRewardedAd", p.a(appLovinSdk));
    }

    public static MaxRewardedAd getInstance(String str, Activity activity) {
        return getInstance(str, AppLovinSdk.getInstance(activity), activity);
    }

    public static MaxRewardedAd getInstance(String str, AppLovinSdk appLovinSdk, Activity activity) {
        if (str == null) {
            throw new IllegalArgumentException("No ad unit ID specified");
        } else if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("Empty ad unit ID specified");
        } else if (activity == null) {
            throw new IllegalArgumentException("No activity specified");
        } else if (appLovinSdk != null) {
            updateActivity(activity);
            synchronized (f4565c) {
                MaxRewardedAd maxRewardedAd = f4564b.get(str);
                if (maxRewardedAd != null) {
                    return maxRewardedAd;
                }
                MaxRewardedAd maxRewardedAd2 = new MaxRewardedAd(str, appLovinSdk);
                f4564b.put(str, maxRewardedAd2);
                return maxRewardedAd2;
            }
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    public static void updateActivity(Activity activity) {
        if (activity != null) {
            f4566d = new WeakReference<>(activity);
        }
    }

    public void destroy() {
        synchronized (f4565c) {
            f4564b.remove(this.f4567a.getAdUnitId());
        }
        this.f4567a.destroy();
    }

    public Activity getActivity() {
        return f4566d.get();
    }

    public boolean isReady() {
        return this.f4567a.isReady();
    }

    public void loadAd() {
        this.f4567a.loadAd(getActivity());
    }

    public void setExtrasParameter(String str, String str2) {
        this.f4567a.setExtraParameter(str, str2);
    }

    public void setListener(MaxRewardedAdListener maxRewardedAdListener) {
        this.f4567a.setListener(maxRewardedAdListener);
    }

    public void showAd() {
        showAd(null);
    }

    public void showAd(String str) {
        this.f4567a.showAd(str, getActivity());
    }

    public String toString() {
        return "" + this.f4567a;
    }
}
