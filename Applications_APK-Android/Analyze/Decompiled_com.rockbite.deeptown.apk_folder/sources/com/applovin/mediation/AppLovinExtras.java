package com.applovin.mediation;

import android.os.Bundle;

public class AppLovinExtras {

    public static class Builder {

        /* renamed from: a  reason: collision with root package name */
        private boolean f4527a;

        public Bundle build() {
            Bundle bundle = new Bundle(1);
            bundle.putBoolean("mute_audio", this.f4527a);
            return bundle;
        }

        public Builder setMuteAudio(boolean z) {
            this.f4527a = z;
            return this;
        }
    }
}
