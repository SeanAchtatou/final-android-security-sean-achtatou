package com.applovin.mediation.e;

import android.util.Log;
import android.view.View;
import com.applovin.adview.AppLovinAdView;
import com.applovin.adview.AppLovinAdViewDisplayErrorCode;
import com.applovin.adview.AppLovinAdViewEventListener;
import com.applovin.mediation.AppLovinUtils;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinSdk;
import com.google.android.gms.ads.mediation.MediationAdLoadCallback;
import com.google.android.gms.ads.mediation.MediationBannerAd;
import com.google.android.gms.ads.mediation.MediationBannerAdCallback;
import com.google.android.gms.ads.mediation.MediationBannerAdConfiguration;

/* compiled from: AppLovinRtbBannerRenderer */
public final class a implements MediationBannerAd, AppLovinAdLoadListener, AppLovinAdDisplayListener, AppLovinAdClickListener, AppLovinAdViewEventListener {

    /* renamed from: g  reason: collision with root package name */
    private static final String f4581g = "a";

    /* renamed from: a  reason: collision with root package name */
    private final MediationBannerAdConfiguration f4582a;

    /* renamed from: b  reason: collision with root package name */
    private final MediationAdLoadCallback<MediationBannerAd, MediationBannerAdCallback> f4583b;

    /* renamed from: c  reason: collision with root package name */
    private MediationBannerAdCallback f4584c;

    /* renamed from: d  reason: collision with root package name */
    private final AppLovinSdk f4585d;

    /* renamed from: e  reason: collision with root package name */
    private final AppLovinAdSize f4586e;

    /* renamed from: f  reason: collision with root package name */
    private AppLovinAdView f4587f;

    public a(MediationBannerAdConfiguration mediationBannerAdConfiguration, MediationAdLoadCallback<MediationBannerAd, MediationBannerAdCallback> mediationAdLoadCallback) {
        this.f4582a = mediationBannerAdConfiguration;
        this.f4583b = mediationAdLoadCallback;
        this.f4586e = AppLovinUtils.appLovinAdSizeFromAdMobAdSize(mediationBannerAdConfiguration.getContext(), mediationBannerAdConfiguration.getAdSize());
        this.f4585d = AppLovinUtils.retrieveSdk(mediationBannerAdConfiguration.getServerParameters(), mediationBannerAdConfiguration.getContext());
    }

    public void a() {
        AppLovinAdSize appLovinAdSize = this.f4586e;
        if (appLovinAdSize != null) {
            this.f4587f = new AppLovinAdView(this.f4585d, appLovinAdSize, this.f4582a.getContext());
            this.f4587f.setAdDisplayListener(this);
            this.f4587f.setAdClickListener(this);
            this.f4587f.setAdViewEventListener(this);
            this.f4585d.getAdService().loadNextAdForAdToken(this.f4582a.getBidResponse(), this);
            return;
        }
        this.f4583b.onFailure("Failed to request banner with unsupported size");
    }

    public void adClicked(AppLovinAd appLovinAd) {
        Log.d(f4581g, "Banner clicked");
        this.f4584c.reportAdClicked();
    }

    public void adClosedFullscreen(AppLovinAd appLovinAd, AppLovinAdView appLovinAdView) {
        Log.d(f4581g, "Banner closed fullscreen");
        this.f4584c.onAdClosed();
    }

    public void adDisplayed(AppLovinAd appLovinAd) {
        Log.d(f4581g, "Banner displayed");
        this.f4584c.reportAdImpression();
        this.f4584c.onAdOpened();
    }

    public void adFailedToDisplay(AppLovinAd appLovinAd, AppLovinAdView appLovinAdView, AppLovinAdViewDisplayErrorCode appLovinAdViewDisplayErrorCode) {
        String str = f4581g;
        Log.e(str, "Banner failed to display: " + appLovinAdViewDisplayErrorCode);
    }

    public void adHidden(AppLovinAd appLovinAd) {
        Log.d(f4581g, "Banner hidden");
    }

    public void adLeftApplication(AppLovinAd appLovinAd, AppLovinAdView appLovinAdView) {
        Log.d(f4581g, "Banner left application");
        this.f4584c.onAdLeftApplication();
    }

    public void adOpenedFullscreen(AppLovinAd appLovinAd, AppLovinAdView appLovinAdView) {
        Log.d(f4581g, "Banner opened fullscreen");
        this.f4584c.onAdOpened();
    }

    public void adReceived(AppLovinAd appLovinAd) {
        String str = f4581g;
        Log.d(str, "Banner did load ad: " + appLovinAd.getAdIdNumber());
        this.f4584c = this.f4583b.onSuccess(this);
        this.f4587f.renderAd(appLovinAd);
    }

    public void failedToReceiveAd(int i2) {
        String str = f4581g;
        Log.e(str, "Failed to load banner ad with error: " + i2);
        this.f4583b.onFailure(Integer.toString(AppLovinUtils.toAdMobErrorCode(i2)));
    }

    public View getView() {
        return this.f4587f;
    }
}
