package com.applovin.mediation.e;

import android.content.Context;
import android.util.Log;
import com.applovin.adview.AppLovinInterstitialAd;
import com.applovin.adview.AppLovinInterstitialAdDialog;
import com.applovin.mediation.AppLovinUtils;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.applovin.sdk.AppLovinSdk;
import com.google.android.gms.ads.mediation.MediationAdLoadCallback;
import com.google.android.gms.ads.mediation.MediationInterstitialAd;
import com.google.android.gms.ads.mediation.MediationInterstitialAdCallback;
import com.google.android.gms.ads.mediation.MediationInterstitialAdConfiguration;

/* compiled from: AppLovinRtbInterstitialRenderer */
public final class b implements MediationInterstitialAd, AppLovinAdLoadListener, AppLovinAdDisplayListener, AppLovinAdClickListener, AppLovinAdVideoPlaybackListener {

    /* renamed from: g  reason: collision with root package name */
    private static final String f4588g = "b";

    /* renamed from: a  reason: collision with root package name */
    private final MediationInterstitialAdConfiguration f4589a;

    /* renamed from: b  reason: collision with root package name */
    private final MediationAdLoadCallback<MediationInterstitialAd, MediationInterstitialAdCallback> f4590b;

    /* renamed from: c  reason: collision with root package name */
    private MediationInterstitialAdCallback f4591c;

    /* renamed from: d  reason: collision with root package name */
    private final AppLovinSdk f4592d;

    /* renamed from: e  reason: collision with root package name */
    private AppLovinInterstitialAdDialog f4593e;

    /* renamed from: f  reason: collision with root package name */
    private AppLovinAd f4594f;

    public b(MediationInterstitialAdConfiguration mediationInterstitialAdConfiguration, MediationAdLoadCallback<MediationInterstitialAd, MediationInterstitialAdCallback> mediationAdLoadCallback) {
        this.f4589a = mediationInterstitialAdConfiguration;
        this.f4590b = mediationAdLoadCallback;
        this.f4592d = AppLovinUtils.retrieveSdk(mediationInterstitialAdConfiguration.getServerParameters(), mediationInterstitialAdConfiguration.getContext());
    }

    public void a() {
        this.f4593e = AppLovinInterstitialAd.create(this.f4592d, this.f4589a.getContext());
        this.f4593e.setAdDisplayListener(this);
        this.f4593e.setAdClickListener(this);
        this.f4593e.setAdVideoPlaybackListener(this);
        this.f4592d.getAdService().loadNextAdForAdToken(this.f4589a.getBidResponse(), this);
    }

    public void adClicked(AppLovinAd appLovinAd) {
        Log.d(f4588g, "Interstitial clicked");
        this.f4591c.reportAdClicked();
        this.f4591c.onAdLeftApplication();
    }

    public void adDisplayed(AppLovinAd appLovinAd) {
        Log.d(f4588g, "Interstitial displayed");
        this.f4591c.reportAdImpression();
        this.f4591c.onAdOpened();
    }

    public void adHidden(AppLovinAd appLovinAd) {
        Log.d(f4588g, "Interstitial hidden");
        this.f4591c.onAdClosed();
    }

    public void adReceived(AppLovinAd appLovinAd) {
        String str = f4588g;
        Log.d(str, "Interstitial did load ad: " + appLovinAd.getAdIdNumber());
        this.f4594f = appLovinAd;
        this.f4591c = this.f4590b.onSuccess(this);
    }

    public void failedToReceiveAd(int i2) {
        String str = f4588g;
        Log.e(str, "Failed to load interstitial ad with error: " + i2);
        this.f4590b.onFailure(Integer.toString(AppLovinUtils.toAdMobErrorCode(i2)));
    }

    public void showAd(Context context) {
        this.f4592d.getSettings().setMuted(AppLovinUtils.shouldMuteAudio(this.f4589a.getMediationExtras()));
        this.f4593e.showAndRender(this.f4594f);
    }

    public void videoPlaybackBegan(AppLovinAd appLovinAd) {
        Log.d(f4588g, "Interstitial video playback began");
    }

    public void videoPlaybackEnded(AppLovinAd appLovinAd, double d2, boolean z) {
        String str = f4588g;
        Log.d(str, "Interstitial video playback ended at playback percent: " + d2 + "%");
    }
}
