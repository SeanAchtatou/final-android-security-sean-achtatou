package com.applovin.mediation;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.applovin.nativeAds.AppLovinNativeAd;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.mediation.NativeAppInstallAdMapper;
import java.util.ArrayList;

/* compiled from: AppLovinNativeAdMapper */
class d extends NativeAppInstallAdMapper {

    /* renamed from: a  reason: collision with root package name */
    private AppLovinNativeAd f4578a;

    /* compiled from: AppLovinNativeAdMapper */
    private static class a extends NativeAd.Image {

        /* renamed from: a  reason: collision with root package name */
        private final Drawable f4579a;

        /* renamed from: b  reason: collision with root package name */
        private final Uri f4580b;

        a(Uri uri, Drawable drawable) {
            this.f4579a = drawable;
            this.f4580b = uri;
        }

        public Drawable getDrawable() {
            return this.f4579a;
        }

        public double getScale() {
            return 1.0d;
        }

        public Uri getUri() {
            return this.f4580b;
        }
    }

    d(AppLovinNativeAd appLovinNativeAd, Context context) {
        this.f4578a = appLovinNativeAd;
        setHeadline(appLovinNativeAd.getTitle());
        setBody(appLovinNativeAd.getDescriptionText());
        setCallToAction(appLovinNativeAd.getCtaText());
        ImageView imageView = new ImageView(context);
        imageView.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        ArrayList arrayList = new ArrayList(1);
        Uri parse = Uri.parse(appLovinNativeAd.getImageUrl());
        Drawable createFromPath = Drawable.createFromPath(parse.getPath());
        Uri parse2 = Uri.parse(appLovinNativeAd.getIconUrl());
        Drawable createFromPath2 = Drawable.createFromPath(parse2.getPath());
        a aVar = new a(parse, createFromPath);
        a aVar2 = new a(parse2, createFromPath2);
        arrayList.add(aVar);
        setImages(arrayList);
        setIcon(aVar2);
        imageView.setImageDrawable(createFromPath);
        setMediaView(imageView);
        setStarRating((double) appLovinNativeAd.getStarRating());
        Bundle bundle = new Bundle();
        bundle.putLong(AppLovinNativeAdapter.KEY_EXTRA_AD_ID, appLovinNativeAd.getAdId());
        bundle.putString(AppLovinNativeAdapter.KEY_EXTRA_CAPTION_TEXT, appLovinNativeAd.getCaptionText());
        setExtras(bundle);
        setOverrideClickHandling(false);
        setOverrideImpressionRecording(false);
    }

    public void handleClick(View view) {
        this.f4578a.launchClickTarget(view.getContext());
    }

    public void recordImpression() {
        this.f4578a.trackImpression();
    }
}
