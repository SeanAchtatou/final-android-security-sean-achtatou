package com.applovin.mediation;

import com.applovin.adview.AppLovinAdView;
import com.applovin.adview.AppLovinAdViewDisplayErrorCode;
import com.applovin.adview.AppLovinAdViewEventListener;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinSdkUtils;
import com.google.android.gms.ads.mediation.MediationBannerListener;

/* compiled from: AppLovinBannerAdListener */
class a implements AppLovinAdLoadListener, AppLovinAdDisplayListener, AppLovinAdClickListener, AppLovinAdViewEventListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final ApplovinAdapter f4552a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final MediationBannerListener f4553b;

    /* renamed from: c  reason: collision with root package name */
    private final AppLovinAdView f4554c;

    /* renamed from: d  reason: collision with root package name */
    private final String f4555d;

    /* renamed from: com.applovin.mediation.a$a  reason: collision with other inner class name */
    /* compiled from: AppLovinBannerAdListener */
    class C0110a implements Runnable {
        C0110a() {
        }

        public void run() {
            a.this.f4553b.onAdLoaded(a.this.f4552a);
        }
    }

    /* compiled from: AppLovinBannerAdListener */
    class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ int f4557a;

        b(int i2) {
            this.f4557a = i2;
        }

        public void run() {
            a.this.f4553b.onAdFailedToLoad(a.this.f4552a, AppLovinUtils.toAdMobErrorCode(this.f4557a));
        }
    }

    a(String str, AppLovinAdView appLovinAdView, ApplovinAdapter applovinAdapter, MediationBannerListener mediationBannerListener) {
        this.f4552a = applovinAdapter;
        this.f4553b = mediationBannerListener;
        this.f4554c = appLovinAdView;
        this.f4555d = str;
    }

    public void adClicked(AppLovinAd appLovinAd) {
        ApplovinAdapter.log(3, "Banner clicked");
        this.f4553b.onAdClicked(this.f4552a);
    }

    public void adClosedFullscreen(AppLovinAd appLovinAd, AppLovinAdView appLovinAdView) {
        ApplovinAdapter.log(3, "Banner closed fullscreen");
        this.f4553b.onAdClosed(this.f4552a);
    }

    public void adDisplayed(AppLovinAd appLovinAd) {
        ApplovinAdapter.log(3, "Banner displayed");
    }

    public void adFailedToDisplay(AppLovinAd appLovinAd, AppLovinAdView appLovinAdView, AppLovinAdViewDisplayErrorCode appLovinAdViewDisplayErrorCode) {
        ApplovinAdapter.log(6, "Banner failed to display: " + appLovinAdViewDisplayErrorCode);
    }

    public void adHidden(AppLovinAd appLovinAd) {
        ApplovinAdapter.log(3, "Banner dismissed");
    }

    public void adLeftApplication(AppLovinAd appLovinAd, AppLovinAdView appLovinAdView) {
        ApplovinAdapter.log(3, "Banner left application");
        this.f4553b.onAdLeftApplication(this.f4552a);
    }

    public void adOpenedFullscreen(AppLovinAd appLovinAd, AppLovinAdView appLovinAdView) {
        ApplovinAdapter.log(3, "Banner opened fullscreen");
        this.f4553b.onAdOpened(this.f4552a);
    }

    public void adReceived(AppLovinAd appLovinAd) {
        ApplovinAdapter.log(3, "Banner did load ad: " + appLovinAd.getAdIdNumber() + " for zone: " + this.f4555d);
        this.f4554c.renderAd(appLovinAd);
        AppLovinSdkUtils.runOnUiThread(new C0110a());
    }

    public void failedToReceiveAd(int i2) {
        ApplovinAdapter.log(6, "Failed to load banner ad with error: " + i2);
        AppLovinSdkUtils.runOnUiThread(new b(i2));
    }
}
