package com.applovin.mediation;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinMediationProvider;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkSettings;
import com.google.android.gms.ads.AdSize;
import java.util.ArrayList;
import java.util.Iterator;

public class AppLovinUtils {
    private static Bundle a(Context context) {
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }

    public static boolean androidManifestHasValidSdkKey(Context context) {
        Bundle a2 = a(context);
        if (a2 != null) {
            return !TextUtils.isEmpty(a2.getString("applovin.sdk.key"));
        }
        return false;
    }

    public static AppLovinAdSize appLovinAdSizeFromAdMobAdSize(Context context, AdSize adSize) {
        ArrayList arrayList = new ArrayList(3);
        arrayList.add(0, AdSize.BANNER);
        arrayList.add(1, AdSize.LEADERBOARD);
        arrayList.add(2, AdSize.MEDIUM_RECTANGLE);
        AdSize findClosestSize = findClosestSize(context, adSize, arrayList);
        if (findClosestSize == null) {
            return null;
        }
        if (AdSize.BANNER.equals(findClosestSize)) {
            return AppLovinAdSize.BANNER;
        }
        if (AdSize.MEDIUM_RECTANGLE.equals(findClosestSize)) {
            return AppLovinAdSize.MREC;
        }
        if (AdSize.LEADERBOARD.equals(findClosestSize)) {
            return AppLovinAdSize.LEADER;
        }
        return null;
    }

    private static boolean b(AdSize adSize, AdSize adSize2) {
        if (adSize2 == null) {
            return false;
        }
        int width = adSize.getWidth();
        int width2 = adSize2.getWidth();
        int height = adSize.getHeight();
        int height2 = adSize2.getHeight();
        double d2 = (double) width;
        Double.isNaN(d2);
        if (d2 * 0.5d <= ((double) width2) && width >= width2) {
            double d3 = (double) height;
            Double.isNaN(d3);
            if (d3 * 0.7d > ((double) height2) || height < height2) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static AdSize findClosestSize(Context context, AdSize adSize, ArrayList<AdSize> arrayList) {
        AdSize adSize2 = null;
        if (!(arrayList == null || adSize == null)) {
            float f2 = context.getResources().getDisplayMetrics().density;
            AdSize adSize3 = new AdSize(Math.round(((float) adSize.getWidthInPixels(context)) / f2), Math.round(((float) adSize.getHeightInPixels(context)) / f2));
            Iterator<AdSize> it = arrayList.iterator();
            while (it.hasNext()) {
                AdSize next = it.next();
                if (b(adSize3, next)) {
                    if (adSize2 != null) {
                        next = a(adSize2, next);
                    }
                    adSize2 = next;
                }
            }
        }
        return adSize2;
    }

    public static String retrievePlacement(Bundle bundle) {
        if (bundle.containsKey("placement")) {
            return bundle.getString("placement");
        }
        return null;
    }

    public static AppLovinSdk retrieveSdk(Bundle bundle, Context context) {
        AppLovinSdk appLovinSdk;
        String string = bundle != null ? bundle.getString("sdkKey") : null;
        if (!TextUtils.isEmpty(string)) {
            appLovinSdk = AppLovinSdk.getInstance(string, new AppLovinSdkSettings(), context);
        } else {
            appLovinSdk = AppLovinSdk.getInstance(context);
        }
        appLovinSdk.setPluginVersion(BuildConfig.VERSION_NAME);
        appLovinSdk.setMediationProvider(AppLovinMediationProvider.ADMOB);
        return appLovinSdk;
    }

    public static String retrieveZoneId(Bundle bundle) {
        return bundle.containsKey("zone_id") ? bundle.getString("zone_id") : "";
    }

    public static boolean shouldMuteAudio(Bundle bundle) {
        return bundle != null && bundle.getBoolean("mute_audio");
    }

    public static int toAdMobErrorCode(int i2) {
        if (i2 == 204) {
            return 3;
        }
        return i2 == -102 ? 2 : 0;
    }

    private static AdSize a(AdSize adSize, AdSize adSize2) {
        return adSize.getWidth() * adSize.getHeight() > adSize2.getWidth() * adSize2.getHeight() ? adSize : adSize2;
    }
}
