package com.applovin.mediation;

import android.content.Context;
import android.util.Log;
import com.applovin.nativeAds.AppLovinNativeAd;
import com.applovin.nativeAds.AppLovinNativeAdLoadListener;
import com.applovin.nativeAds.AppLovinNativeAdPrecacheListener;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkUtils;
import com.google.android.gms.ads.mediation.MediationNativeListener;
import java.lang.ref.WeakReference;
import java.util.List;

/* compiled from: AppLovinNativeAdListener */
class c implements AppLovinNativeAdLoadListener, AppLovinNativeAdPrecacheListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final AppLovinNativeAdapter f4570a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final MediationNativeListener f4571b;

    /* renamed from: c  reason: collision with root package name */
    private final AppLovinSdk f4572c;

    /* renamed from: d  reason: collision with root package name */
    private final WeakReference<Context> f4573d;

    /* compiled from: AppLovinNativeAdListener */
    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ d f4574a;

        a(d dVar) {
            this.f4574a = dVar;
        }

        public void run() {
            c.this.f4571b.onAdLoaded(c.this.f4570a, this.f4574a);
        }
    }

    /* compiled from: AppLovinNativeAdListener */
    class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ int f4576a;

        b(int i2) {
            this.f4576a = i2;
        }

        public void run() {
            c.this.f4571b.onAdFailedToLoad(c.this.f4570a, this.f4576a);
        }
    }

    c(AppLovinNativeAdapter appLovinNativeAdapter, MediationNativeListener mediationNativeListener, AppLovinSdk appLovinSdk, Context context) {
        this.f4570a = appLovinNativeAdapter;
        this.f4571b = mediationNativeListener;
        this.f4572c = appLovinSdk;
        this.f4573d = new WeakReference<>(context);
    }

    public void onNativeAdImagePrecachingFailed(AppLovinNativeAd appLovinNativeAd, int i2) {
        String str = AppLovinNativeAdapter.f4532a;
        Log.e(str, "Native ad failed to pre cache images " + i2);
        a(AppLovinUtils.toAdMobErrorCode(i2));
    }

    public void onNativeAdImagesPrecached(AppLovinNativeAd appLovinNativeAd) {
        Context context = this.f4573d.get();
        if (context == null) {
            Log.e(AppLovinNativeAdapter.f4532a, "Failed to create mapper. Context is null.");
            a(0);
            return;
        }
        d dVar = new d(appLovinNativeAd, context);
        Log.d(AppLovinNativeAdapter.f4532a, "Native ad loaded.");
        AppLovinSdkUtils.runOnUiThread(new a(dVar));
    }

    public void onNativeAdVideoPrecachingFailed(AppLovinNativeAd appLovinNativeAd, int i2) {
    }

    public void onNativeAdVideoPreceached(AppLovinNativeAd appLovinNativeAd) {
    }

    public void onNativeAdsFailedToLoad(int i2) {
        String str = AppLovinNativeAdapter.f4532a;
        Log.e(str, "Native ad failed to load " + i2);
        a(AppLovinUtils.toAdMobErrorCode(i2));
    }

    public void onNativeAdsLoaded(List<AppLovinNativeAd> list) {
        if (list.size() <= 0 || !a(list.get(0))) {
            Log.e(AppLovinNativeAdapter.f4532a, "Ad from AppLovin doesn't have all assets required for the app install ad format");
            a(3);
            return;
        }
        this.f4572c.getNativeAdService().precacheResources(list.get(0), this);
    }

    private void a(int i2) {
        AppLovinSdkUtils.runOnUiThread(new b(i2));
    }

    private static boolean a(AppLovinNativeAd appLovinNativeAd) {
        return (appLovinNativeAd.getImageUrl() == null || appLovinNativeAd.getIconUrl() == null || appLovinNativeAd.getTitle() == null || appLovinNativeAd.getDescriptionText() == null || appLovinNativeAd.getCtaText() == null) ? false : true;
    }
}
