package com.applovin.mediation;

import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdRewardListener;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.google.ads.mediation.applovin.AppLovinMediationAdapter;
import com.google.ads.mediation.applovin.AppLovinRewardItem;
import com.google.android.gms.ads.mediation.MediationRewardedAdCallback;
import com.google.android.gms.ads.mediation.MediationRewardedAdConfiguration;
import com.underwater.demolisher.data.vo.RemoteConfigConst;
import java.util.Map;

public class AppLovinIncentivizedAdListener implements AppLovinAdRewardListener, AppLovinAdDisplayListener, AppLovinAdClickListener, AppLovinAdVideoPlaybackListener {

    /* renamed from: a  reason: collision with root package name */
    private MediationRewardedAdCallback f4528a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f4529b;

    /* renamed from: c  reason: collision with root package name */
    private AppLovinRewardItem f4530c;

    /* renamed from: d  reason: collision with root package name */
    private String f4531d;

    public AppLovinIncentivizedAdListener(MediationRewardedAdConfiguration mediationRewardedAdConfiguration, MediationRewardedAdCallback mediationRewardedAdCallback) {
        this.f4531d = AppLovinUtils.retrieveZoneId(mediationRewardedAdConfiguration.getServerParameters());
        this.f4528a = mediationRewardedAdCallback;
    }

    public void adClicked(AppLovinAd appLovinAd) {
        ApplovinAdapter.log(3, "Rewarded video clicked");
        this.f4528a.reportAdClicked();
    }

    public void adDisplayed(AppLovinAd appLovinAd) {
        ApplovinAdapter.log(3, "Rewarded video displayed");
        this.f4528a.onAdOpened();
        this.f4528a.reportAdImpression();
    }

    public void adHidden(AppLovinAd appLovinAd) {
        ApplovinAdapter.log(3, "Rewarded video dismissed");
        AppLovinMediationAdapter.INCENTIVIZED_ADS.remove(this.f4531d);
        if (this.f4529b) {
            this.f4528a.onUserEarnedReward(this.f4530c);
        }
        this.f4528a.onAdClosed();
    }

    public void userDeclinedToViewAd(AppLovinAd appLovinAd) {
        ApplovinAdapter.log(3, "User declined to view rewarded video");
    }

    public void userOverQuota(AppLovinAd appLovinAd, Map<String, String> map) {
        ApplovinAdapter.log(6, "Rewarded video validation request for ad did exceed quota with response: " + map);
    }

    public void userRewardRejected(AppLovinAd appLovinAd, Map<String, String> map) {
        ApplovinAdapter.log(6, "Rewarded video validation request was rejected with response: " + map);
    }

    public void userRewardVerified(AppLovinAd appLovinAd, Map<String, String> map) {
        String str = map.get("currency");
        int parseDouble = (int) Double.parseDouble(map.get("amount"));
        ApplovinAdapter.log(3, "Rewarded " + parseDouble + RemoteConfigConst.SHOP_SPECIAL_EVENT_OFFER + str);
        this.f4530c = new AppLovinRewardItem(parseDouble, str);
    }

    public void validationRequestFailed(AppLovinAd appLovinAd, int i2) {
        ApplovinAdapter.log(6, "Rewarded video validation request for ad failed with error code: " + i2);
    }

    public void videoPlaybackBegan(AppLovinAd appLovinAd) {
        ApplovinAdapter.log(3, "Rewarded video playback began");
        this.f4528a.onVideoStart();
    }

    public void videoPlaybackEnded(AppLovinAd appLovinAd, double d2, boolean z) {
        ApplovinAdapter.log(3, "Rewarded video playback ended at playback percent: " + d2 + "%");
        this.f4529b = z;
        if (z) {
            this.f4528a.onVideoComplete();
        }
    }
}
