package com.applovin.mediation;

import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.google.android.gms.ads.mediation.MediationInterstitialListener;

/* compiled from: AppLovinInterstitialAdListener */
class b implements AppLovinAdDisplayListener, AppLovinAdClickListener, AppLovinAdVideoPlaybackListener {

    /* renamed from: a  reason: collision with root package name */
    private final ApplovinAdapter f4568a;

    /* renamed from: b  reason: collision with root package name */
    private final MediationInterstitialListener f4569b;

    b(ApplovinAdapter applovinAdapter, MediationInterstitialListener mediationInterstitialListener) {
        this.f4568a = applovinAdapter;
        this.f4569b = mediationInterstitialListener;
    }

    public void adClicked(AppLovinAd appLovinAd) {
        ApplovinAdapter.log(3, "Interstitial clicked");
        this.f4569b.onAdClicked(this.f4568a);
        this.f4569b.onAdLeftApplication(this.f4568a);
    }

    public void adDisplayed(AppLovinAd appLovinAd) {
        ApplovinAdapter.log(3, "Interstitial displayed");
        this.f4569b.onAdOpened(this.f4568a);
    }

    public void adHidden(AppLovinAd appLovinAd) {
        ApplovinAdapter.log(3, "Interstitial dismissed");
        this.f4569b.onAdClosed(this.f4568a);
    }

    public void videoPlaybackBegan(AppLovinAd appLovinAd) {
        ApplovinAdapter.log(3, "Interstitial video playback began");
    }

    public void videoPlaybackEnded(AppLovinAd appLovinAd, double d2, boolean z) {
        ApplovinAdapter.log(3, "Interstitial video playback ended at playback percent: " + d2 + "%");
    }
}
