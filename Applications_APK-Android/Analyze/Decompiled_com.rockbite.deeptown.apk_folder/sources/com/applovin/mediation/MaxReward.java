package com.applovin.mediation;

public class MaxReward {
    public static final int DEFAULT_AMOUNT = 0;
    public static final String DEFAULT_LABEL = "";

    /* renamed from: a  reason: collision with root package name */
    private final String f4550a;

    /* renamed from: b  reason: collision with root package name */
    private final int f4551b;

    private MaxReward(int i2, String str) {
        if (i2 >= 0) {
            this.f4550a = str;
            this.f4551b = i2;
            return;
        }
        throw new IllegalArgumentException("Reward amount must be greater than or equal to 0");
    }

    public static MaxReward create(int i2, String str) {
        return new MaxReward(i2, str);
    }

    public static MaxReward createDefault() {
        return create(0, "");
    }

    public final int getAmount() {
        return this.f4551b;
    }

    public final String getLabel() {
        return this.f4550a;
    }

    public String toString() {
        return "MaxReward{amount=" + this.f4551b + ", label='" + this.f4550a + '\'' + '}';
    }
}
