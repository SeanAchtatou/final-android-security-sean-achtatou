package com.applovin.mediation;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import com.applovin.adview.AppLovinAdView;
import com.applovin.adview.AppLovinInterstitialAd;
import com.applovin.adview.AppLovinInterstitialAdDialog;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkUtils;
import com.google.ads.mediation.applovin.AppLovinMediationAdapter;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.mediation.MediationAdRequest;
import com.google.android.gms.ads.mediation.MediationBannerAdapter;
import com.google.android.gms.ads.mediation.MediationBannerListener;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.mediation.MediationInterstitialListener;
import com.google.android.gms.ads.mediation.MediationRewardedAd;
import com.google.android.gms.ads.mediation.OnContextChangedListener;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

public class ApplovinAdapter extends AppLovinMediationAdapter implements MediationBannerAdapter, MediationInterstitialAdapter, OnContextChangedListener, MediationRewardedAd {
    /* access modifiers changed from: private */

    /* renamed from: h  reason: collision with root package name */
    public static final HashMap<String, Queue<AppLovinAd>> f4533h = new HashMap<>();
    /* access modifiers changed from: private */

    /* renamed from: i  reason: collision with root package name */
    public static final Object f4534i = new Object();

    /* renamed from: a  reason: collision with root package name */
    private AppLovinSdk f4535a;

    /* renamed from: b  reason: collision with root package name */
    private Context f4536b;

    /* renamed from: c  reason: collision with root package name */
    private Bundle f4537c;
    /* access modifiers changed from: private */

    /* renamed from: d  reason: collision with root package name */
    public MediationInterstitialListener f4538d;

    /* renamed from: e  reason: collision with root package name */
    private AppLovinAdView f4539e;
    /* access modifiers changed from: private */

    /* renamed from: f  reason: collision with root package name */
    public String f4540f;
    /* access modifiers changed from: private */

    /* renamed from: g  reason: collision with root package name */
    public String f4541g;

    class a implements AppLovinAdLoadListener {

        /* renamed from: com.applovin.mediation.ApplovinAdapter$a$a  reason: collision with other inner class name */
        class C0109a implements Runnable {
            C0109a() {
            }

            public void run() {
                ApplovinAdapter.this.f4538d.onAdLoaded(ApplovinAdapter.this);
            }
        }

        class b implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ int f4544a;

            b(int i2) {
                this.f4544a = i2;
            }

            public void run() {
                ApplovinAdapter.this.f4538d.onAdFailedToLoad(ApplovinAdapter.this, AppLovinUtils.toAdMobErrorCode(this.f4544a));
            }
        }

        a() {
        }

        public void adReceived(AppLovinAd appLovinAd) {
            ApplovinAdapter.log(3, "Interstitial did load ad: " + appLovinAd.getAdIdNumber() + " for zone: " + ApplovinAdapter.this.f4541g + " and placement: " + ApplovinAdapter.this.f4540f);
            synchronized (ApplovinAdapter.f4534i) {
                Object obj = (Queue) ApplovinAdapter.f4533h.get(ApplovinAdapter.this.f4541g);
                if (obj == null) {
                    obj = new LinkedList();
                    ApplovinAdapter.f4533h.put(ApplovinAdapter.this.f4541g, obj);
                }
                obj.offer(appLovinAd);
                AppLovinSdkUtils.runOnUiThread(new C0109a());
            }
        }

        public void failedToReceiveAd(int i2) {
            ApplovinAdapter.log(6, "Interstitial failed to load with error: " + i2);
            AppLovinSdkUtils.runOnUiThread(new b(i2));
        }
    }

    class b implements Runnable {
        b() {
        }

        public void run() {
            ApplovinAdapter.this.f4538d.onAdLoaded(ApplovinAdapter.this);
        }
    }

    class c implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ MediationBannerListener f4547a;

        c(MediationBannerListener mediationBannerListener) {
            this.f4547a = mediationBannerListener;
        }

        public void run() {
            this.f4547a.onAdFailedToLoad(ApplovinAdapter.this, 1);
        }
    }

    public static void log(int i2, String str) {
        Log.println(i2, "AppLovinAdapter", str);
    }

    public View getBannerView() {
        return this.f4539e;
    }

    public void onContextChanged(Context context) {
        if (context != null) {
            log(3, "Context changed: " + context);
            this.f4536b = context;
        }
    }

    public void onDestroy() {
    }

    public void onPause() {
    }

    public void onResume() {
    }

    public void requestBannerAd(Context context, MediationBannerListener mediationBannerListener, Bundle bundle, AdSize adSize, MediationAdRequest mediationAdRequest, Bundle bundle2) {
        this.f4535a = AppLovinUtils.retrieveSdk(bundle, context);
        this.f4540f = AppLovinUtils.retrievePlacement(bundle);
        this.f4541g = AppLovinUtils.retrieveZoneId(bundle);
        log(3, "Requesting banner of size " + adSize + " for zone: " + this.f4541g + " and placement: " + this.f4540f);
        AppLovinAdSize appLovinAdSizeFromAdMobAdSize = AppLovinUtils.appLovinAdSizeFromAdMobAdSize(context, adSize);
        if (appLovinAdSizeFromAdMobAdSize != null) {
            this.f4539e = new AppLovinAdView(this.f4535a, appLovinAdSizeFromAdMobAdSize, context);
            a aVar = new a(this.f4541g, this.f4539e, this, mediationBannerListener);
            this.f4539e.setAdDisplayListener(aVar);
            this.f4539e.setAdClickListener(aVar);
            this.f4539e.setAdViewEventListener(aVar);
            if (!TextUtils.isEmpty(this.f4541g)) {
                this.f4535a.getAdService().loadNextAdForZoneId(this.f4541g, aVar);
            } else {
                this.f4535a.getAdService().loadNextAd(appLovinAdSizeFromAdMobAdSize, aVar);
            }
        } else {
            log(6, "Failed to request banner with unsupported size");
            if (mediationBannerListener != null) {
                AppLovinSdkUtils.runOnUiThread(new c(mediationBannerListener));
            }
        }
    }

    public void requestInterstitialAd(Context context, MediationInterstitialListener mediationInterstitialListener, Bundle bundle, MediationAdRequest mediationAdRequest, Bundle bundle2) {
        this.f4535a = AppLovinUtils.retrieveSdk(bundle, context);
        this.f4536b = context;
        this.f4537c = bundle2;
        this.f4538d = mediationInterstitialListener;
        this.f4540f = AppLovinUtils.retrievePlacement(bundle);
        this.f4541g = AppLovinUtils.retrieveZoneId(bundle);
        log(3, "Requesting interstitial for zone: " + this.f4541g + " and placement: " + this.f4540f);
        a aVar = new a();
        synchronized (f4534i) {
            Queue queue = f4533h.get(this.f4541g);
            if (queue != null) {
                if (queue == null || !queue.isEmpty()) {
                    log(3, "Enqueued interstitial found. Finishing load...");
                    AppLovinSdkUtils.runOnUiThread(new b());
                }
            }
            if (!TextUtils.isEmpty(this.f4541g)) {
                this.f4535a.getAdService().loadNextAdForZoneId(this.f4541g, aVar);
            } else {
                this.f4535a.getAdService().loadNextAd(AppLovinAdSize.INTERSTITIAL, aVar);
            }
        }
    }

    public void showInterstitial() {
        synchronized (f4534i) {
            this.f4535a.getSettings().setMuted(AppLovinUtils.shouldMuteAudio(this.f4537c));
            Queue queue = f4533h.get(this.f4541g);
            AppLovinAd appLovinAd = queue != null ? (AppLovinAd) queue.poll() : null;
            AppLovinInterstitialAdDialog create = AppLovinInterstitialAd.create(this.f4535a, this.f4536b);
            b bVar = new b(this, this.f4538d);
            create.setAdDisplayListener(bVar);
            create.setAdClickListener(bVar);
            create.setAdVideoPlaybackListener(bVar);
            if (appLovinAd != null) {
                log(3, "Showing interstitial for zone: " + this.f4541g + " placement: " + this.f4540f);
                create.showAndRender(appLovinAd, this.f4540f);
            } else {
                log(3, "Attempting to show interstitial before one was loaded");
                if (!TextUtils.isEmpty(this.f4541g) || !create.isAdReadyToDisplay()) {
                    this.f4538d.onAdOpened(this);
                    this.f4538d.onAdClosed(this);
                } else {
                    log(3, "Showing interstitial preloaded by SDK");
                    create.show(this.f4540f);
                }
            }
        }
    }
}
