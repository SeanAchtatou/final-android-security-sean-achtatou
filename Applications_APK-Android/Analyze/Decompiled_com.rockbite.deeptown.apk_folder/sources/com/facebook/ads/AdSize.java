package com.facebook.ads;

import androidx.annotation.Keep;
import com.facebook.ads.internal.api.AdSizeApi;
import com.facebook.ads.internal.dynamicloading.DynamicLoaderFactory;
import java.io.Serializable;

@Keep
public class AdSize implements Serializable {
    @Deprecated
    public static final AdSize BANNER_320_50 = new AdSize(4);
    public static final AdSize BANNER_HEIGHT_50 = new AdSize(5);
    public static final AdSize BANNER_HEIGHT_90 = new AdSize(6);
    public static final AdSize INTERSTITIAL = new AdSize(100);
    public static final AdSize RECTANGLE_HEIGHT_250 = new AdSize(7);
    private final int UNDEFINED;
    private AdSizeApi mAdSizeApi;
    private final int mInitHeight;
    private final int mInitSizeType;
    private final int mInitWidth;

    public AdSize(int i2, int i3) {
        this.UNDEFINED = -1;
        this.mInitSizeType = -1;
        this.mInitWidth = i2;
        this.mInitHeight = i3;
    }

    public static AdSize fromWidthAndHeight(int i2, int i3) {
        if (INTERSTITIAL.getHeight() == i3 && INTERSTITIAL.getWidth() == i2) {
            return INTERSTITIAL;
        }
        if (BANNER_320_50.getHeight() == i3 && BANNER_320_50.getWidth() == i2) {
            return BANNER_320_50;
        }
        if (BANNER_HEIGHT_50.getHeight() == i3 && BANNER_HEIGHT_50.getWidth() == i2) {
            return BANNER_HEIGHT_50;
        }
        if (BANNER_HEIGHT_90.getHeight() == i3 && BANNER_HEIGHT_90.getWidth() == i2) {
            return BANNER_HEIGHT_90;
        }
        if (RECTANGLE_HEIGHT_250.getHeight() == i3 && RECTANGLE_HEIGHT_250.getWidth() == i2) {
            return RECTANGLE_HEIGHT_250;
        }
        return null;
    }

    private AdSizeApi getAdSizeApi(int i2) {
        if (this.mAdSizeApi == null) {
            this.mAdSizeApi = DynamicLoaderFactory.makeLoaderUnsafe().createAdSizeApi(i2);
        }
        return this.mAdSizeApi;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || AdSize.class != obj.getClass()) {
            return false;
        }
        AdSize adSize = (AdSize) obj;
        if (getWidth() == adSize.getWidth() && getHeight() == adSize.getHeight()) {
            return true;
        }
        return false;
    }

    public int getHeight() {
        int i2 = this.mInitSizeType;
        if (i2 != -1) {
            return getAdSizeApi(i2).getHeight();
        }
        return this.mInitHeight;
    }

    public int getWidth() {
        int i2 = this.mInitSizeType;
        if (i2 != -1) {
            return getAdSizeApi(i2).getWidth();
        }
        return this.mInitWidth;
    }

    public int hashCode() {
        return (getWidth() * 31) + getHeight();
    }

    private AdSize(int i2) {
        this.UNDEFINED = -1;
        this.mInitSizeType = i2;
        this.mInitWidth = -1;
        this.mInitHeight = -1;
    }
}
