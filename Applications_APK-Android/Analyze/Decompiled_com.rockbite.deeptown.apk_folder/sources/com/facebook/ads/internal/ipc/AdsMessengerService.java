package com.facebook.ads.internal.ipc;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import androidx.annotation.Keep;
import com.facebook.ads.internal.api.AdsMessengerServiceApi;
import com.facebook.ads.internal.dynamicloading.DynamicLoaderFactory;

@Keep
public class AdsMessengerService extends Service {
    private AdsMessengerServiceApi mAdsMessengerServiceApi;

    public IBinder onBind(Intent intent) {
        return this.mAdsMessengerServiceApi.onBind(intent);
    }

    public void onCreate() {
        this.mAdsMessengerServiceApi = DynamicLoaderFactory.makeLoader(this, false).createAdsMessengerServiceApi(this);
        this.mAdsMessengerServiceApi.onCreate();
    }

    public void onDestroy() {
        this.mAdsMessengerServiceApi.onDestroy();
    }
}
