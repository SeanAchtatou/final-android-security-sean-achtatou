package com.facebook.internal;

import com.facebook.FacebookSdk;
import com.facebook.internal.FetchedAppGateKeepersManager;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.drive.MetadataChangeSet;

public final class FeatureManager {

    /* renamed from: com.facebook.internal.FeatureManager$2  reason: invalid class name */
    static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] $SwitchMap$com$facebook$internal$FeatureManager$Feature = new int[Feature.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(20:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|(3:19|20|22)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(22:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|22) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0062 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x006e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.facebook.internal.FeatureManager$Feature[] r0 = com.facebook.internal.FeatureManager.Feature.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.facebook.internal.FeatureManager.AnonymousClass2.$SwitchMap$com$facebook$internal$FeatureManager$Feature = r0
                int[] r0 = com.facebook.internal.FeatureManager.AnonymousClass2.$SwitchMap$com$facebook$internal$FeatureManager$Feature     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.facebook.internal.FeatureManager$Feature r1 = com.facebook.internal.FeatureManager.Feature.RestrictiveDataFiltering     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.facebook.internal.FeatureManager.AnonymousClass2.$SwitchMap$com$facebook$internal$FeatureManager$Feature     // Catch:{ NoSuchFieldError -> 0x001f }
                com.facebook.internal.FeatureManager$Feature r1 = com.facebook.internal.FeatureManager.Feature.Instrument     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.facebook.internal.FeatureManager.AnonymousClass2.$SwitchMap$com$facebook$internal$FeatureManager$Feature     // Catch:{ NoSuchFieldError -> 0x002a }
                com.facebook.internal.FeatureManager$Feature r1 = com.facebook.internal.FeatureManager.Feature.CrashReport     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.facebook.internal.FeatureManager.AnonymousClass2.$SwitchMap$com$facebook$internal$FeatureManager$Feature     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.facebook.internal.FeatureManager$Feature r1 = com.facebook.internal.FeatureManager.Feature.ErrorReport     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = com.facebook.internal.FeatureManager.AnonymousClass2.$SwitchMap$com$facebook$internal$FeatureManager$Feature     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.facebook.internal.FeatureManager$Feature r1 = com.facebook.internal.FeatureManager.Feature.Core     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r0 = com.facebook.internal.FeatureManager.AnonymousClass2.$SwitchMap$com$facebook$internal$FeatureManager$Feature     // Catch:{ NoSuchFieldError -> 0x004b }
                com.facebook.internal.FeatureManager$Feature r1 = com.facebook.internal.FeatureManager.Feature.AppEvents     // Catch:{ NoSuchFieldError -> 0x004b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                int[] r0 = com.facebook.internal.FeatureManager.AnonymousClass2.$SwitchMap$com$facebook$internal$FeatureManager$Feature     // Catch:{ NoSuchFieldError -> 0x0056 }
                com.facebook.internal.FeatureManager$Feature r1 = com.facebook.internal.FeatureManager.Feature.CodelessEvents     // Catch:{ NoSuchFieldError -> 0x0056 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0056 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0056 }
            L_0x0056:
                int[] r0 = com.facebook.internal.FeatureManager.AnonymousClass2.$SwitchMap$com$facebook$internal$FeatureManager$Feature     // Catch:{ NoSuchFieldError -> 0x0062 }
                com.facebook.internal.FeatureManager$Feature r1 = com.facebook.internal.FeatureManager.Feature.Login     // Catch:{ NoSuchFieldError -> 0x0062 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0062 }
                r2 = 8
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0062 }
            L_0x0062:
                int[] r0 = com.facebook.internal.FeatureManager.AnonymousClass2.$SwitchMap$com$facebook$internal$FeatureManager$Feature     // Catch:{ NoSuchFieldError -> 0x006e }
                com.facebook.internal.FeatureManager$Feature r1 = com.facebook.internal.FeatureManager.Feature.Share     // Catch:{ NoSuchFieldError -> 0x006e }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x006e }
                r2 = 9
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x006e }
            L_0x006e:
                int[] r0 = com.facebook.internal.FeatureManager.AnonymousClass2.$SwitchMap$com$facebook$internal$FeatureManager$Feature     // Catch:{ NoSuchFieldError -> 0x007a }
                com.facebook.internal.FeatureManager$Feature r1 = com.facebook.internal.FeatureManager.Feature.Places     // Catch:{ NoSuchFieldError -> 0x007a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x007a }
                r2 = 10
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x007a }
            L_0x007a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.internal.FeatureManager.AnonymousClass2.<clinit>():void");
        }
    }

    public interface Callback {
        void onCompleted(boolean z);
    }

    public enum Feature {
        Unknown(-1),
        Core(0),
        AppEvents(256),
        CodelessEvents(257),
        RestrictiveDataFiltering(258),
        Instrument(AdRequest.MAX_CONTENT_URL_LENGTH),
        CrashReport(513),
        ErrorReport(514),
        Login(65536),
        Share(MetadataChangeSet.INDEXABLE_TEXT_SIZE_LIMIT_BYTES),
        Places(196608);
        
        private final int code;

        private Feature(int i2) {
            this.code = i2;
        }

        static Feature fromInt(int i2) {
            for (Feature feature : values()) {
                if (feature.code == i2) {
                    return feature;
                }
            }
            return Unknown;
        }

        public Feature getParent() {
            int i2 = this.code;
            if ((i2 & 255) > 0) {
                return fromInt(i2 & 16776960);
            }
            if ((65280 & i2) > 0) {
                return fromInt(i2 & 16711680);
            }
            return fromInt(0);
        }

        public String toString() {
            switch (AnonymousClass2.$SwitchMap$com$facebook$internal$FeatureManager$Feature[ordinal()]) {
                case 1:
                    return "RestrictiveDataFiltering";
                case 2:
                    return "Instrument";
                case 3:
                    return "CrashReport";
                case 4:
                    return "ErrorReport";
                case 5:
                    return "CoreKit";
                case 6:
                    return "AppEvents";
                case 7:
                    return "CodelessEvents";
                case 8:
                    return "LoginKit";
                case 9:
                    return "ShareKit";
                case 10:
                    return "PlacesKit";
                default:
                    return "unknown";
            }
        }
    }

    public static void checkFeature(final Feature feature, final Callback callback) {
        FetchedAppGateKeepersManager.loadAppGateKeepersAsync(new FetchedAppGateKeepersManager.Callback() {
            public void onCompleted() {
                callback.onCompleted(FeatureManager.isEnabled(feature));
            }
        });
    }

    private static boolean defaultStatus(Feature feature) {
        int i2 = AnonymousClass2.$SwitchMap$com$facebook$internal$FeatureManager$Feature[feature.ordinal()];
        return (i2 == 1 || i2 == 2 || i2 == 3 || i2 == 4) ? false : true;
    }

    private static boolean getGKStatus(Feature feature) {
        return FetchedAppGateKeepersManager.getGateKeeperForKey("FBSDKFeature" + feature.toString(), FacebookSdk.getApplicationId(), defaultStatus(feature));
    }

    public static boolean isEnabled(Feature feature) {
        if (Feature.Unknown == feature) {
            return false;
        }
        if (Feature.Core == feature) {
            return true;
        }
        Feature parent = feature.getParent();
        if (parent == feature) {
            return getGKStatus(feature);
        }
        if (!isEnabled(parent) || !getGKStatus(feature)) {
            return false;
        }
        return true;
    }
}
