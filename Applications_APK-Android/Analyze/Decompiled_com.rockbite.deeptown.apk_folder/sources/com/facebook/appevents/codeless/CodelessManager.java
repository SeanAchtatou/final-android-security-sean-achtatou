package com.facebook.appevents.codeless;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.codeless.ViewIndexingTrigger;
import com.facebook.appevents.codeless.internal.Constants;
import com.facebook.appevents.internal.AppEventUtility;
import com.facebook.internal.AttributionIdentifiers;
import com.facebook.internal.FeatureManager;
import com.facebook.internal.FetchedAppSettings;
import com.facebook.internal.FetchedAppSettingsManager;
import com.facebook.internal.Utility;
import java.util.Locale;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONObject;

public final class CodelessManager {
    /* access modifiers changed from: private */
    public static String deviceSessionID = null;
    /* access modifiers changed from: private */
    public static Boolean isAppIndexingEnabled = false;
    /* access modifiers changed from: private */
    public static volatile Boolean isCheckingSession = false;
    private static CodelessMatcher matcher;
    /* access modifiers changed from: private */
    public static SensorManager sensorManager;
    /* access modifiers changed from: private */
    public static ViewIndexer viewIndexer;
    /* access modifiers changed from: private */
    public static final ViewIndexingTrigger viewIndexingTrigger = new ViewIndexingTrigger();

    public static void checkCodelessSession(final String str) {
        if (!isCheckingSession.booleanValue()) {
            isCheckingSession = true;
            FacebookSdk.getExecutor().execute(new Runnable() {
                public void run() {
                    boolean z = true;
                    GraphRequest newPostRequest = GraphRequest.newPostRequest(null, String.format(Locale.US, "%s/app_indexing_session", str), null, null);
                    Bundle parameters = newPostRequest.getParameters();
                    if (parameters == null) {
                        parameters = new Bundle();
                    }
                    AttributionIdentifiers attributionIdentifiers = AttributionIdentifiers.getAttributionIdentifiers(FacebookSdk.getApplicationContext());
                    JSONArray jSONArray = new JSONArray();
                    String str = Build.MODEL;
                    if (str == null) {
                        str = "";
                    }
                    jSONArray.put(str);
                    if (attributionIdentifiers == null || attributionIdentifiers.getAndroidAdvertiserId() == null) {
                        jSONArray.put("");
                    } else {
                        jSONArray.put(attributionIdentifiers.getAndroidAdvertiserId());
                    }
                    String str2 = AppEventsConstants.EVENT_PARAM_VALUE_NO;
                    jSONArray.put(str2);
                    if (AppEventUtility.isEmulator()) {
                        str2 = "1";
                    }
                    jSONArray.put(str2);
                    Locale currentLocale = Utility.getCurrentLocale();
                    jSONArray.put(currentLocale.getLanguage() + "_" + currentLocale.getCountry());
                    String jSONArray2 = jSONArray.toString();
                    parameters.putString(Constants.DEVICE_SESSION_ID, CodelessManager.getCurrentDeviceSessionID());
                    parameters.putString(Constants.EXTINFO, jSONArray2);
                    newPostRequest.setParameters(parameters);
                    if (newPostRequest != null) {
                        JSONObject jSONObject = newPostRequest.executeAndWait().getJSONObject();
                        if (jSONObject == null || !jSONObject.optBoolean(Constants.APP_INDEXING_ENABLED, false)) {
                            z = false;
                        }
                        Boolean unused = CodelessManager.isAppIndexingEnabled = Boolean.valueOf(z);
                        if (!CodelessManager.isAppIndexingEnabled.booleanValue()) {
                            String unused2 = CodelessManager.deviceSessionID = null;
                        } else {
                            CodelessManager.viewIndexer.schedule();
                        }
                    }
                    Boolean unused3 = CodelessManager.isCheckingSession = false;
                }
            });
        }
    }

    public static String getCurrentDeviceSessionID() {
        if (deviceSessionID == null) {
            deviceSessionID = UUID.randomUUID().toString();
        }
        return deviceSessionID;
    }

    public static boolean getIsAppIndexingEnabled() {
        return isAppIndexingEnabled.booleanValue();
    }

    /* access modifiers changed from: private */
    public static synchronized CodelessMatcher getMatcher() {
        CodelessMatcher codelessMatcher;
        synchronized (CodelessManager.class) {
            if (matcher == null) {
                matcher = new CodelessMatcher();
            }
            codelessMatcher = matcher;
        }
        return codelessMatcher;
    }

    public static void onActivityPaused(final Activity activity) {
        FeatureManager.checkFeature(FeatureManager.Feature.CodelessEvents, new FeatureManager.Callback() {
            public void onCompleted(boolean z) {
                if (z) {
                    CodelessManager.getMatcher().remove(activity);
                    if (CodelessManager.viewIndexer != null) {
                        CodelessManager.viewIndexer.unschedule();
                    }
                    if (CodelessManager.sensorManager != null) {
                        CodelessManager.sensorManager.unregisterListener(CodelessManager.viewIndexingTrigger);
                    }
                }
            }
        });
    }

    public static void onActivityResumed(final Activity activity) {
        FeatureManager.checkFeature(FeatureManager.Feature.CodelessEvents, new FeatureManager.Callback() {
            public void onCompleted(boolean z) {
                if (z) {
                    CodelessManager.getMatcher().add(activity);
                    Context applicationContext = activity.getApplicationContext();
                    final String applicationId = FacebookSdk.getApplicationId();
                    final FetchedAppSettings appSettingsWithoutQuery = FetchedAppSettingsManager.getAppSettingsWithoutQuery(applicationId);
                    if (appSettingsWithoutQuery != null && appSettingsWithoutQuery.getCodelessEventsEnabled()) {
                        SensorManager unused = CodelessManager.sensorManager = (SensorManager) applicationContext.getSystemService("sensor");
                        if (CodelessManager.sensorManager != null) {
                            Sensor defaultSensor = CodelessManager.sensorManager.getDefaultSensor(1);
                            ViewIndexer unused2 = CodelessManager.viewIndexer = new ViewIndexer(activity);
                            CodelessManager.viewIndexingTrigger.setOnShakeListener(new ViewIndexingTrigger.OnShakeListener() {
                                public void onShake() {
                                    FetchedAppSettings fetchedAppSettings = appSettingsWithoutQuery;
                                    boolean z = true;
                                    boolean z2 = fetchedAppSettings != null && fetchedAppSettings.getCodelessEventsEnabled();
                                    if (!FacebookSdk.getCodelessSetupEnabled()) {
                                        z = false;
                                    }
                                    if (z2 && z) {
                                        CodelessManager.checkCodelessSession(applicationId);
                                    }
                                }
                            });
                            CodelessManager.sensorManager.registerListener(CodelessManager.viewIndexingTrigger, defaultSensor, 2);
                            if (appSettingsWithoutQuery != null && appSettingsWithoutQuery.getCodelessEventsEnabled()) {
                                CodelessManager.viewIndexer.schedule();
                            }
                        }
                    }
                }
            }
        });
    }

    public static void updateAppIndexing(Boolean bool) {
        isAppIndexingEnabled = bool;
    }
}
