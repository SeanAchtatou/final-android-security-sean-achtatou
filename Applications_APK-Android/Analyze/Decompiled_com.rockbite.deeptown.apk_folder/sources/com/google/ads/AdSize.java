package com.google.ads;

import android.content.Context;
import com.esotericsoftware.spine.Animation;

@Deprecated
/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class AdSize {
    public static final int AUTO_HEIGHT = -2;
    public static final AdSize BANNER = new AdSize(320, 50, "mb");
    public static final int FULL_WIDTH = -1;
    public static final AdSize IAB_BANNER = new AdSize(468, 60, "as");
    public static final AdSize IAB_LEADERBOARD = new AdSize(728, 90, "as");
    public static final AdSize IAB_MRECT = new AdSize(300, 250, "as");
    public static final AdSize IAB_WIDE_SKYSCRAPER = new AdSize(160, 600, "as");
    public static final int LANDSCAPE_AD_HEIGHT = 32;
    public static final int LARGE_AD_HEIGHT = 90;
    public static final int PORTRAIT_AD_HEIGHT = 50;
    public static final AdSize SMART_BANNER = new AdSize(-1, -2, "mb");
    private final com.google.android.gms.ads.AdSize zzdi;

    public AdSize(com.google.android.gms.ads.AdSize adSize) {
        this.zzdi = adSize;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof AdSize)) {
            return false;
        }
        return this.zzdi.equals(((AdSize) obj).zzdi);
    }

    public final AdSize findBestSize(AdSize... adSizeArr) {
        AdSize adSize = null;
        if (adSizeArr == null) {
            return null;
        }
        float f2 = Animation.CurveTimeline.LINEAR;
        int width = getWidth();
        int height = getHeight();
        for (AdSize adSize2 : adSizeArr) {
            int width2 = adSize2.getWidth();
            int height2 = adSize2.getHeight();
            if (isSizeAppropriate(width2, height2)) {
                float f3 = ((float) (width2 * height2)) / ((float) (width * height));
                if (f3 > 1.0f) {
                    f3 = 1.0f / f3;
                }
                if (f3 > f2) {
                    adSize = adSize2;
                    f2 = f3;
                }
            }
        }
        return adSize;
    }

    public final int getHeight() {
        return this.zzdi.getHeight();
    }

    public final int getHeightInPixels(Context context) {
        return this.zzdi.getHeightInPixels(context);
    }

    public final int getWidth() {
        return this.zzdi.getWidth();
    }

    public final int getWidthInPixels(Context context) {
        return this.zzdi.getWidthInPixels(context);
    }

    public final int hashCode() {
        return this.zzdi.hashCode();
    }

    public final boolean isAutoHeight() {
        return this.zzdi.isAutoHeight();
    }

    public final boolean isCustomAdSize() {
        return false;
    }

    public final boolean isFullWidth() {
        return this.zzdi.isFullWidth();
    }

    public final boolean isSizeAppropriate(int i2, int i3) {
        int width = getWidth();
        int height = getHeight();
        float f2 = (float) i2;
        float f3 = (float) width;
        if (f2 > f3 * 1.25f || f2 < f3 * 0.8f) {
            return false;
        }
        float f4 = (float) i3;
        float f5 = (float) height;
        return f4 <= 1.25f * f5 && f4 >= f5 * 0.8f;
    }

    public final String toString() {
        return this.zzdi.toString();
    }

    public AdSize(int i2, int i3) {
        this(new com.google.android.gms.ads.AdSize(i2, i3));
    }

    private AdSize(int i2, int i3, String str) {
        this(new com.google.android.gms.ads.AdSize(i2, i3));
    }
}
