package com.google.ads.mediation;

import android.location.Location;
import com.google.ads.AdRequest;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

@Deprecated
/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public class MediationAdRequest {
    private final Date zzme;
    private final AdRequest.Gender zzmf;
    private final Set<String> zzmg;
    private final boolean zzmh;
    private final Location zzmi;

    public MediationAdRequest(Date date, AdRequest.Gender gender, Set<String> set, boolean z, Location location) {
        this.zzme = date;
        this.zzmf = gender;
        this.zzmg = set;
        this.zzmh = z;
        this.zzmi = location;
    }

    public Integer getAgeInYears() {
        if (this.zzme == null) {
            return null;
        }
        Calendar instance = Calendar.getInstance();
        Calendar instance2 = Calendar.getInstance();
        instance.setTime(this.zzme);
        Integer valueOf = Integer.valueOf(instance2.get(1) - instance.get(1));
        return (instance2.get(2) < instance.get(2) || (instance2.get(2) == instance.get(2) && instance2.get(5) < instance.get(5))) ? Integer.valueOf(valueOf.intValue() - 1) : valueOf;
    }

    public Date getBirthday() {
        return this.zzme;
    }

    public AdRequest.Gender getGender() {
        return this.zzmf;
    }

    public Set<String> getKeywords() {
        return this.zzmg;
    }

    public Location getLocation() {
        return this.zzmi;
    }

    public boolean isTesting() {
        return this.zzmh;
    }
}
