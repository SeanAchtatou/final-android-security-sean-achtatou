package com.google.ads.mediation.tapjoy;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import com.applovin.sdk.AppLovinMediationProvider;
import com.google.ads.mediation.tapjoy.TapjoyInitializer;
import com.google.android.gms.ads.mediation.MediationAdLoadCallback;
import com.google.android.gms.ads.mediation.MediationRewardedAd;
import com.google.android.gms.ads.mediation.MediationRewardedAdCallback;
import com.google.android.gms.ads.mediation.MediationRewardedAdConfiguration;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.tapjoy.TJActionRequest;
import com.tapjoy.TJError;
import com.tapjoy.TJPlacement;
import com.tapjoy.TJPlacementListener;
import com.tapjoy.TJPlacementVideoListener;
import com.tapjoy.Tapjoy;
import com.tapjoy.TapjoyAuctionFlags;
import com.tapjoy.TapjoyConnectFlag;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Hashtable;
import org.json.JSONException;
import org.json.JSONObject;

public class TapjoyRewardedRenderer implements MediationRewardedAd, TJPlacementVideoListener {
    private static final String TAPJOY_DEBUG_FLAG_KEY = "enable_debug";
    private static boolean isRtbAd = false;
    /* access modifiers changed from: private */
    public static HashMap<String, WeakReference<TapjoyRewardedRenderer>> mPlacementsInUse = new HashMap<>();
    private MediationRewardedAdConfiguration adConfiguration;
    /* access modifiers changed from: private */
    public MediationAdLoadCallback<MediationRewardedAd, MediationRewardedAdCallback> mAdLoadCallback;
    /* access modifiers changed from: private */
    public MediationRewardedAdCallback mMediationRewardedAdCallback;
    /* access modifiers changed from: private */
    public final Handler mainHandler = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public TJPlacement videoPlacement;

    public class TapjoyReward implements RewardItem {
        public TapjoyReward() {
        }

        public int getAmount() {
            return 1;
        }

        public String getType() {
            return "";
        }
    }

    public TapjoyRewardedRenderer(MediationRewardedAdConfiguration mediationRewardedAdConfiguration, MediationAdLoadCallback<MediationRewardedAd, MediationRewardedAdCallback> mediationAdLoadCallback) {
        this.adConfiguration = mediationRewardedAdConfiguration;
        this.mAdLoadCallback = mediationAdLoadCallback;
    }

    /* access modifiers changed from: private */
    public void createVideoPlacementAndRequestContent(final String str) {
        Log.i(TapjoyMediationAdapter.TAG, "Creating video placement for AdMob adapter");
        this.videoPlacement = Tapjoy.getPlacement(str, new TJPlacementListener() {
            public void onClick(TJPlacement tJPlacement) {
                TapjoyRewardedRenderer.this.mainHandler.post(new Runnable() {
                    public void run() {
                        Log.d(TapjoyMediationAdapter.TAG, "Tapjoy Rewarded Ad has been clicked.");
                        if (TapjoyRewardedRenderer.this.mMediationRewardedAdCallback != null) {
                            TapjoyRewardedRenderer.this.mMediationRewardedAdCallback.reportAdClicked();
                        }
                    }
                });
            }

            public void onContentDismiss(TJPlacement tJPlacement) {
                TapjoyRewardedRenderer.this.mainHandler.post(new Runnable() {
                    public void run() {
                        Log.d(TapjoyMediationAdapter.TAG, "Tapjoy Rewarded Ad has been closed.");
                        if (TapjoyRewardedRenderer.this.mMediationRewardedAdCallback != null) {
                            TapjoyRewardedRenderer.this.mMediationRewardedAdCallback.onAdClosed();
                        }
                        TapjoyRewardedRenderer.mPlacementsInUse.remove(str);
                    }
                });
            }

            public void onContentReady(TJPlacement tJPlacement) {
                TapjoyRewardedRenderer.this.mainHandler.post(new Runnable() {
                    public void run() {
                        Log.d(TapjoyMediationAdapter.TAG, "Tapjoy Rewarded Ad is available.");
                        if (TapjoyRewardedRenderer.this.mAdLoadCallback != null) {
                            TapjoyRewardedRenderer tapjoyRewardedRenderer = TapjoyRewardedRenderer.this;
                            MediationRewardedAdCallback unused = tapjoyRewardedRenderer.mMediationRewardedAdCallback = (MediationRewardedAdCallback) tapjoyRewardedRenderer.mAdLoadCallback.onSuccess(TapjoyRewardedRenderer.this);
                        }
                    }
                });
            }

            public void onContentShow(TJPlacement tJPlacement) {
                TapjoyRewardedRenderer.this.mainHandler.post(new Runnable() {
                    public void run() {
                        Log.d(TapjoyMediationAdapter.TAG, "Tapjoy Rewarded Ad has been opened.");
                        if (TapjoyRewardedRenderer.this.mMediationRewardedAdCallback != null) {
                            TapjoyRewardedRenderer.this.mMediationRewardedAdCallback.onAdOpened();
                        }
                    }
                });
            }

            public void onPurchaseRequest(TJPlacement tJPlacement, TJActionRequest tJActionRequest, String str) {
            }

            public void onRequestFailure(TJPlacement tJPlacement, final TJError tJError) {
                TapjoyRewardedRenderer.this.mainHandler.post(new Runnable() {
                    public void run() {
                        TapjoyRewardedRenderer.mPlacementsInUse.remove(str);
                        String str = "Failed to request rewarded ad from Tapjoy: " + tJError.message;
                        Log.w(TapjoyMediationAdapter.TAG, str);
                        if (TapjoyRewardedRenderer.this.mAdLoadCallback != null) {
                            TapjoyRewardedRenderer.this.mAdLoadCallback.onFailure(str);
                        }
                    }
                });
            }

            public void onRequestSuccess(TJPlacement tJPlacement) {
                TapjoyRewardedRenderer.this.mainHandler.post(new Runnable() {
                    public void run() {
                        if (!TapjoyRewardedRenderer.this.videoPlacement.isContentAvailable()) {
                            TapjoyRewardedRenderer.mPlacementsInUse.remove(str);
                            Log.w(TapjoyMediationAdapter.TAG, "Failed to request rewarded ad from Tapjoy: No Fill.");
                            if (TapjoyRewardedRenderer.this.mAdLoadCallback != null) {
                                TapjoyRewardedRenderer.this.mAdLoadCallback.onFailure("Failed to request rewarded ad from Tapjoy: No Fill.");
                            }
                        }
                    }
                });
            }

            public void onRewardRequest(TJPlacement tJPlacement, TJActionRequest tJActionRequest, String str, int i2) {
            }
        });
        this.videoPlacement.setMediationName(AppLovinMediationProvider.ADMOB);
        this.videoPlacement.setAdapterVersion("1.0.0");
        if (isRtbAd) {
            HashMap hashMap = new HashMap();
            try {
                JSONObject jSONObject = new JSONObject(this.adConfiguration.getBidResponse());
                String string = jSONObject.getString("id");
                String string2 = jSONObject.getString(TapjoyAuctionFlags.AUCTION_DATA);
                hashMap.put("id", string);
                hashMap.put(TapjoyAuctionFlags.AUCTION_DATA, string2);
            } catch (JSONException e2) {
                String str2 = TapjoyMediationAdapter.TAG;
                Log.e(str2, "Bid Response JSON Error: " + e2.getMessage());
            }
            this.videoPlacement.setAuctionData(hashMap);
        }
        this.videoPlacement.setVideoListener(this);
        this.videoPlacement.requestContent();
    }

    public void onVideoComplete(TJPlacement tJPlacement) {
        this.mainHandler.post(new Runnable() {
            public void run() {
                Log.d(TapjoyMediationAdapter.TAG, "Tapjoy Rewarded Ad has finished playing.");
                if (TapjoyRewardedRenderer.this.mMediationRewardedAdCallback != null) {
                    TapjoyRewardedRenderer.this.mMediationRewardedAdCallback.onVideoComplete();
                    TapjoyRewardedRenderer.this.mMediationRewardedAdCallback.onUserEarnedReward(new TapjoyReward());
                }
            }
        });
    }

    public void onVideoError(final TJPlacement tJPlacement, final String str) {
        this.mainHandler.post(new Runnable() {
            public void run() {
                TapjoyRewardedRenderer.mPlacementsInUse.remove(tJPlacement.getName());
                String str = TapjoyMediationAdapter.TAG;
                Log.w(str, "Tapjoy Rewarded Ad has failed to play: " + str);
                if (TapjoyRewardedRenderer.this.mMediationRewardedAdCallback != null) {
                    TapjoyRewardedRenderer.this.mMediationRewardedAdCallback.onAdFailedToShow(str);
                }
            }
        });
    }

    public void onVideoStart(TJPlacement tJPlacement) {
        this.mainHandler.post(new Runnable() {
            public void run() {
                Log.d(TapjoyMediationAdapter.TAG, "Tapjoy Rewarded Ad has started playing.");
                if (TapjoyRewardedRenderer.this.mMediationRewardedAdCallback != null) {
                    TapjoyRewardedRenderer.this.mMediationRewardedAdCallback.onVideoStart();
                    TapjoyRewardedRenderer.this.mMediationRewardedAdCallback.reportAdImpression();
                }
            }
        });
    }

    public void render() {
        if (!this.adConfiguration.getBidResponse().equals("")) {
            isRtbAd = true;
        }
        Bundle serverParameters = this.adConfiguration.getServerParameters();
        final String string = serverParameters.getString("placementName");
        if (TextUtils.isEmpty(string)) {
            Log.w(TapjoyMediationAdapter.TAG, "No placement name given for Tapjoy-AdMob adapter");
            this.mAdLoadCallback.onFailure("No placement name given for Tapjoy-AdMob adapter");
            return;
        }
        Log.i(TapjoyMediationAdapter.TAG, "Loading ad for Tapjoy-AdMob adapter");
        Bundle mediationExtras = this.adConfiguration.getMediationExtras();
        Context context = this.adConfiguration.getContext();
        if (!(context instanceof Activity)) {
            Log.e(TapjoyMediationAdapter.TAG, "Tapjoy SDK requires an Activity context to request ads");
            this.mAdLoadCallback.onFailure("Tapjoy SDK requires an Activity context to request ads");
            return;
        }
        Activity activity = (Activity) context;
        String string2 = serverParameters.getString("sdkKey");
        if (TextUtils.isEmpty(string2)) {
            Log.w(TapjoyMediationAdapter.TAG, "Failed to request ad from Tapjoy: Missing or Invalid SDK Key.");
            this.mAdLoadCallback.onFailure("Failed to request ad from Tapjoy: Missing or Invalid SDK Key.");
            return;
        }
        Tapjoy.setActivity(activity);
        Hashtable hashtable = new Hashtable();
        if (mediationExtras != null && mediationExtras.containsKey(TAPJOY_DEBUG_FLAG_KEY)) {
            hashtable.put(TapjoyConnectFlag.ENABLE_LOGGING, Boolean.valueOf(mediationExtras.getBoolean(TAPJOY_DEBUG_FLAG_KEY, false)));
        }
        TapjoyInitializer.getInstance().initialize(activity, string2, hashtable, new TapjoyInitializer.Listener() {
            public void onInitializeFailed(String str) {
                String str2 = "Failed to request ad from Tapjoy: " + str;
                Log.w(TapjoyMediationAdapter.TAG, str2);
                TapjoyRewardedRenderer.this.mAdLoadCallback.onFailure(str2);
            }

            public void onInitializeSucceeded() {
                if (!TapjoyRewardedRenderer.mPlacementsInUse.containsKey(string) || ((WeakReference) TapjoyRewardedRenderer.mPlacementsInUse.get(string)).get() == null) {
                    TapjoyRewardedRenderer.mPlacementsInUse.put(string, new WeakReference(TapjoyRewardedRenderer.this));
                    TapjoyRewardedRenderer.this.createVideoPlacementAndRequestContent(string);
                    return;
                }
                String str = "An ad has already been requested for placement: " + string;
                Log.w(TapjoyMediationAdapter.TAG, str);
                TapjoyRewardedRenderer.this.mAdLoadCallback.onFailure(str);
            }
        });
    }

    public void showAd(Context context) {
        Log.i(TapjoyMediationAdapter.TAG, "Show video content for Tapjoy-AdMob adapter");
        TJPlacement tJPlacement = this.videoPlacement;
        if (tJPlacement == null || !tJPlacement.isContentAvailable()) {
            MediationRewardedAdCallback mediationRewardedAdCallback = this.mMediationRewardedAdCallback;
            if (mediationRewardedAdCallback != null) {
                mediationRewardedAdCallback.onAdFailedToShow("Tapjoy Rewarded Ad is not ready.");
                return;
            }
            return;
        }
        this.videoPlacement.showContent();
    }
}
