package com.google.ads.mediation.customevent;

@Deprecated
/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface CustomEventInterstitialListener extends CustomEventListener {
    void onReceivedAd();
}
