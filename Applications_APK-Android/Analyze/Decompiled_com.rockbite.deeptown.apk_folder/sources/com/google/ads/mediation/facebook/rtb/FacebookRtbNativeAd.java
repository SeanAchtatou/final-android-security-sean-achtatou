package com.google.ads.mediation.facebook.rtb;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdOptionsView;
import com.facebook.ads.ExtraHints;
import com.facebook.ads.MediaView;
import com.facebook.ads.MediaViewListener;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdLayout;
import com.facebook.ads.NativeAdListener;
import com.google.ads.mediation.facebook.FacebookAdapter;
import com.google.ads.mediation.facebook.FacebookMediationAdapter;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdAssetNames;
import com.google.android.gms.ads.mediation.MediationAdLoadCallback;
import com.google.android.gms.ads.mediation.MediationNativeAdCallback;
import com.google.android.gms.ads.mediation.MediationNativeAdConfiguration;
import com.google.android.gms.ads.mediation.UnifiedNativeAdMapper;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Map;

public class FacebookRtbNativeAd extends UnifiedNativeAdMapper {
    private MediationNativeAdConfiguration adConfiguration;
    /* access modifiers changed from: private */
    public MediationAdLoadCallback<UnifiedNativeAdMapper, MediationNativeAdCallback> callback;
    private MediaView mMediaView;
    private NativeAd mNativeAd;
    /* access modifiers changed from: private */
    public MediationNativeAdCallback mNativeAdCallback;

    private class FacebookAdapterNativeAdImage extends NativeAd.Image {
        private Drawable mDrawable;
        private Uri mUri;

        public FacebookAdapterNativeAdImage(Uri uri) {
            this.mUri = uri;
        }

        public Drawable getDrawable() {
            return this.mDrawable;
        }

        public double getScale() {
            return 1.0d;
        }

        public Uri getUri() {
            return this.mUri;
        }

        /* access modifiers changed from: protected */
        public void setDrawable(Drawable drawable) {
            this.mDrawable = drawable;
        }
    }

    private interface NativeAdMapperListener {
        void onMappingFailed();

        void onMappingSuccess();
    }

    private class NativeListener implements AdListener, NativeAdListener {
        private WeakReference<Context> mContext;
        private com.facebook.ads.NativeAd mNativeAd;

        NativeListener(Context context, com.facebook.ads.NativeAd nativeAd) {
            this.mNativeAd = nativeAd;
            this.mContext = new WeakReference<>(context);
        }

        public void onAdClicked(Ad ad) {
            FacebookRtbNativeAd.this.mNativeAdCallback.onAdOpened();
            FacebookRtbNativeAd.this.mNativeAdCallback.onAdLeftApplication();
        }

        public void onAdLoaded(Ad ad) {
            if (ad != this.mNativeAd) {
                Log.w(FacebookMediationAdapter.TAG, "Ad loaded is not a native ad.");
                FacebookRtbNativeAd.this.callback.onFailure("Ad Loaded is not a Native Ad");
                return;
            }
            Context context = this.mContext.get();
            if (context == null) {
                Log.w(FacebookMediationAdapter.TAG, "Failed to create ad options view, Context is null.");
                FacebookRtbNativeAd.this.callback.onFailure("Context is null");
                return;
            }
            FacebookRtbNativeAd.this.mapNativeAd(context, new NativeAdMapperListener() {
                public void onMappingFailed() {
                    FacebookRtbNativeAd.this.callback.onFailure("Ad Failed to Load");
                }

                public void onMappingSuccess() {
                    FacebookRtbNativeAd facebookRtbNativeAd = FacebookRtbNativeAd.this;
                    MediationNativeAdCallback unused = facebookRtbNativeAd.mNativeAdCallback = (MediationNativeAdCallback) facebookRtbNativeAd.callback.onSuccess(FacebookRtbNativeAd.this);
                }
            });
        }

        public void onError(Ad ad, AdError adError) {
            String errorMessage = adError.getErrorMessage();
            if (!TextUtils.isEmpty(errorMessage)) {
                Log.w(FacebookMediationAdapter.TAG, errorMessage);
            }
            FacebookRtbNativeAd.this.callback.onFailure(adError.getErrorMessage());
        }

        public void onLoggingImpression(Ad ad) {
        }

        public void onMediaDownloaded(Ad ad) {
            Log.d(FacebookMediationAdapter.TAG, "onMediaDownloaded");
        }
    }

    public FacebookRtbNativeAd(MediationNativeAdConfiguration mediationNativeAdConfiguration, MediationAdLoadCallback<UnifiedNativeAdMapper, MediationNativeAdCallback> mediationAdLoadCallback) {
        this.callback = mediationAdLoadCallback;
        this.adConfiguration = mediationNativeAdConfiguration;
    }

    private boolean containsRequiredFieldsForUnifiedNativeAd(com.facebook.ads.NativeAd nativeAd) {
        return (nativeAd.getAdHeadline() == null || nativeAd.getAdCoverImage() == null || nativeAd.getAdBodyText() == null || nativeAd.getAdIcon() == null || nativeAd.getAdCallToAction() == null || this.mMediaView == null) ? false : true;
    }

    public void mapNativeAd(Context context, NativeAdMapperListener nativeAdMapperListener) {
        if (!containsRequiredFieldsForUnifiedNativeAd(this.mNativeAd)) {
            Log.w(FacebookMediationAdapter.TAG, "Ad from Facebook doesn't have all assets required for the app install format.");
            nativeAdMapperListener.onMappingFailed();
            return;
        }
        setHeadline(this.mNativeAd.getAdHeadline());
        ArrayList arrayList = new ArrayList();
        arrayList.add(new FacebookAdapterNativeAdImage(null));
        setImages(arrayList);
        setBody(this.mNativeAd.getAdBodyText());
        setIcon(new FacebookAdapterNativeAdImage(null));
        setCallToAction(this.mNativeAd.getAdCallToAction());
        setAdvertiser(this.mNativeAd.getAdvertiserName());
        this.mMediaView.setListener(new MediaViewListener() {
            public void onComplete(MediaView mediaView) {
                if (FacebookRtbNativeAd.this.mNativeAdCallback != null) {
                    FacebookRtbNativeAd.this.mNativeAdCallback.onVideoComplete();
                }
            }

            public void onEnterFullscreen(MediaView mediaView) {
            }

            public void onExitFullscreen(MediaView mediaView) {
            }

            public void onFullscreenBackground(MediaView mediaView) {
            }

            public void onFullscreenForeground(MediaView mediaView) {
            }

            public void onPause(MediaView mediaView) {
            }

            public void onPlay(MediaView mediaView) {
            }

            public void onVolumeChange(MediaView mediaView, float f2) {
            }
        });
        setHasVideoContent(true);
        setMediaView(this.mMediaView);
        setStarRating(null);
        Bundle bundle = new Bundle();
        bundle.putCharSequence("id", this.mNativeAd.getId());
        bundle.putCharSequence(FacebookAdapter.KEY_SOCIAL_CONTEXT_ASSET, this.mNativeAd.getAdSocialContext());
        setExtras(bundle);
        setAdChoicesContent(new AdOptionsView(context, this.mNativeAd, new NativeAdLayout(context)));
        nativeAdMapperListener.onMappingSuccess();
    }

    public void render() {
        String placementID = FacebookMediationAdapter.getPlacementID(this.adConfiguration.getServerParameters());
        if (TextUtils.isEmpty(placementID)) {
            Log.e(FacebookMediationAdapter.TAG, "Failed to request ad, placementID is null or empty.");
            this.callback.onFailure("Failed to request ad, placementID is null or empty.");
            return;
        }
        this.mMediaView = new MediaView(this.adConfiguration.getContext());
        this.mNativeAd = new com.facebook.ads.NativeAd(this.adConfiguration.getContext(), placementID);
        if (!TextUtils.isEmpty(this.adConfiguration.getWatermark())) {
            this.mNativeAd.setExtraHints(new ExtraHints.Builder().mediationData(this.adConfiguration.getWatermark()).build());
        }
        com.facebook.ads.NativeAd nativeAd = this.mNativeAd;
        nativeAd.loadAd(nativeAd.buildLoadAdConfig().withAdListener(new NativeListener(this.adConfiguration.getContext(), this.mNativeAd)).withBid(this.adConfiguration.getBidResponse()).build());
    }

    public void trackViews(View view, Map<String, View> map, Map<String, View> map2) {
        setOverrideClickHandling(true);
        ArrayList arrayList = new ArrayList();
        ImageView imageView = null;
        for (Map.Entry next : map.entrySet()) {
            arrayList.add(next.getValue());
            if (((String) next.getKey()).equals(UnifiedNativeAdAssetNames.ASSET_ICON)) {
                imageView = (ImageView) next.getValue();
            }
        }
        this.mNativeAd.registerViewForInteraction(view, this.mMediaView, imageView, arrayList);
    }

    public void untrackView(View view) {
        super.untrackView(view);
    }
}
