package com.google.ads.mediation.tapjoy.rtb;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.google.android.gms.ads.mediation.MediationAdConfiguration;
import com.google.android.gms.ads.mediation.MediationAdLoadCallback;
import com.google.android.gms.ads.mediation.MediationInterstitialAd;
import com.google.android.gms.ads.mediation.MediationInterstitialAdCallback;
import com.tapjoy.TJActionRequest;
import com.tapjoy.TJError;
import com.tapjoy.TJPlacement;
import com.tapjoy.TJPlacementListener;
import com.tapjoy.Tapjoy;
import com.tapjoy.TapjoyAuctionFlags;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

public class TapjoyRtbInterstitialRenderer implements MediationInterstitialAd {
    private static final String MEDIATION_AGENT = "admob";
    private static final String PLACEMENT_NAME_SERVER_PARAMETER_KEY = "placementName";
    private static final String TAPJOY_INTERNAL_ADAPTER_VERSION = "2.0.0";
    /* access modifiers changed from: private */
    public static HashMap<String, WeakReference<TapjoyRtbInterstitialRenderer>> placementsInUse = new HashMap<>();
    private final String TAG = "TapjoyRTB Interstitial";
    private MediationAdConfiguration adConfiguration;
    /* access modifiers changed from: private */
    public final MediationAdLoadCallback<MediationInterstitialAd, MediationInterstitialAdCallback> callback;
    /* access modifiers changed from: private */
    public TJPlacement interstitialPlacement;
    /* access modifiers changed from: private */
    public String interstitialPlacementName = null;
    /* access modifiers changed from: private */
    public MediationInterstitialAdCallback listener;
    /* access modifiers changed from: private */
    public final Handler mainHandler = new Handler(Looper.getMainLooper());

    public TapjoyRtbInterstitialRenderer(MediationAdConfiguration mediationAdConfiguration, MediationAdLoadCallback<MediationInterstitialAd, MediationInterstitialAdCallback> mediationAdLoadCallback) {
        this.adConfiguration = mediationAdConfiguration;
        this.callback = mediationAdLoadCallback;
    }

    private boolean checkParams() {
        if (this.adConfiguration.getServerParameters().getString(PLACEMENT_NAME_SERVER_PARAMETER_KEY) == null) {
            return false;
        }
        this.interstitialPlacementName = this.adConfiguration.getServerParameters().getString(PLACEMENT_NAME_SERVER_PARAMETER_KEY);
        return true;
    }

    private void createInterstitialPlacementAndRequestContent() {
        this.interstitialPlacement = Tapjoy.getPlacement(this.interstitialPlacementName, new TJPlacementListener() {
            public void onClick(TJPlacement tJPlacement) {
                TapjoyRtbInterstitialRenderer.this.mainHandler.post(new Runnable() {
                    public void run() {
                        if (TapjoyRtbInterstitialRenderer.this.listener != null) {
                            TapjoyRtbInterstitialRenderer.this.listener.reportAdClicked();
                            TapjoyRtbInterstitialRenderer.this.listener.onAdLeftApplication();
                        }
                    }
                });
            }

            public void onContentDismiss(TJPlacement tJPlacement) {
                TapjoyRtbInterstitialRenderer.this.mainHandler.post(new Runnable() {
                    public void run() {
                        if (TapjoyRtbInterstitialRenderer.this.listener != null) {
                            TapjoyRtbInterstitialRenderer.this.listener.onAdClosed();
                        }
                        TapjoyRtbInterstitialRenderer.placementsInUse.remove(TapjoyRtbInterstitialRenderer.this.interstitialPlacementName);
                    }
                });
            }

            public void onContentReady(TJPlacement tJPlacement) {
                TapjoyRtbInterstitialRenderer.this.mainHandler.post(new Runnable() {
                    public void run() {
                        TapjoyRtbInterstitialRenderer tapjoyRtbInterstitialRenderer = TapjoyRtbInterstitialRenderer.this;
                        MediationInterstitialAdCallback unused = tapjoyRtbInterstitialRenderer.listener = (MediationInterstitialAdCallback) tapjoyRtbInterstitialRenderer.callback.onSuccess(TapjoyRtbInterstitialRenderer.this);
                        Log.d("TapjoyRTB Interstitial", "Interstitial onContentReady");
                    }
                });
            }

            public void onContentShow(TJPlacement tJPlacement) {
                TapjoyRtbInterstitialRenderer.this.mainHandler.post(new Runnable() {
                    public void run() {
                        if (TapjoyRtbInterstitialRenderer.this.listener != null) {
                            TapjoyRtbInterstitialRenderer.this.listener.onAdOpened();
                            TapjoyRtbInterstitialRenderer.this.listener.reportAdImpression();
                        }
                    }
                });
            }

            public void onPurchaseRequest(TJPlacement tJPlacement, TJActionRequest tJActionRequest, String str) {
            }

            public void onRequestFailure(TJPlacement tJPlacement, final TJError tJError) {
                TapjoyRtbInterstitialRenderer.this.mainHandler.post(new Runnable() {
                    public void run() {
                        TapjoyRtbInterstitialRenderer.placementsInUse.remove(TapjoyRtbInterstitialRenderer.this.interstitialPlacementName);
                        TapjoyRtbInterstitialRenderer.this.callback.onFailure(tJError.message);
                    }
                });
            }

            public void onRequestSuccess(TJPlacement tJPlacement) {
                TapjoyRtbInterstitialRenderer.this.mainHandler.post(new Runnable() {
                    public void run() {
                        if (!TapjoyRtbInterstitialRenderer.this.interstitialPlacement.isContentAvailable()) {
                            TapjoyRtbInterstitialRenderer.placementsInUse.remove(TapjoyRtbInterstitialRenderer.this.interstitialPlacementName);
                            TapjoyRtbInterstitialRenderer.this.callback.onFailure("NO_FILL");
                            Log.d("TapjoyRTB Interstitial", "Interstitial Content isn't available");
                        }
                    }
                });
            }

            public void onRewardRequest(TJPlacement tJPlacement, TJActionRequest tJActionRequest, String str, int i2) {
            }
        });
        this.interstitialPlacement.setMediationName("admob");
        this.interstitialPlacement.setAdapterVersion(TAPJOY_INTERNAL_ADAPTER_VERSION);
        HashMap hashMap = new HashMap();
        try {
            JSONObject jSONObject = new JSONObject(this.adConfiguration.getBidResponse());
            String string = jSONObject.getString("id");
            String string2 = jSONObject.getString(TapjoyAuctionFlags.AUCTION_DATA);
            hashMap.put("id", string);
            hashMap.put(TapjoyAuctionFlags.AUCTION_DATA, string2);
        } catch (JSONException e2) {
            Log.e("TapjoyRTB Interstitial", "Bid Response JSON Error: " + e2.getMessage());
        }
        this.interstitialPlacement.setAuctionData(hashMap);
        this.interstitialPlacement.requestContent();
    }

    public void render() {
        Log.i("TapjoyRTB Interstitial", "Rendering interstitial placement for AdMob adapter");
        if (!checkParams()) {
            this.callback.onFailure("Invalid server parameters specified in the UI");
        } else if (!placementsInUse.containsKey(this.interstitialPlacementName) || placementsInUse.get(this.interstitialPlacementName).get() == null) {
            placementsInUse.put(this.interstitialPlacementName, new WeakReference(this));
            createInterstitialPlacementAndRequestContent();
        } else {
            String str = "An ad has already been requested for placement: " + this.interstitialPlacementName;
            Log.w("TapjoyRTB Interstitial", str);
            this.callback.onFailure(str);
        }
    }

    public void showAd(Context context) {
        Log.i("TapjoyRTB Interstitial", "Show interstitial content for Tapjoy-AdMob adapter");
        TJPlacement tJPlacement = this.interstitialPlacement;
        if (tJPlacement != null && tJPlacement.isContentAvailable()) {
            this.interstitialPlacement.showContent();
        }
    }
}
