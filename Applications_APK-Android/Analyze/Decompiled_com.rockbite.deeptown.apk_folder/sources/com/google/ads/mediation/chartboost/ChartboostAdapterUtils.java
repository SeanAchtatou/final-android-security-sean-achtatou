package com.google.ads.mediation.chartboost;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import com.chartboost.sdk.a;
import com.chartboost.sdk.d.a;

class ChartboostAdapterUtils {
    static final String KEY_AD_LOCATION = "adLocation";
    static final String KEY_APP_ID = "appId";
    static final String KEY_APP_SIGNATURE = "appSignature";

    /* renamed from: com.google.ads.mediation.chartboost.ChartboostAdapterUtils$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError = new int[a.c.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(64:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|59|60|61|62|(3:63|64|66)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(66:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|59|60|61|62|63|64|66) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0062 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x006e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x007a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0086 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0092 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x009e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x00aa */
        /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x00b6 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x00c2 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:35:0x00ce */
        /* JADX WARNING: Missing exception handler attribute for start block: B:37:0x00da */
        /* JADX WARNING: Missing exception handler attribute for start block: B:39:0x00e6 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:41:0x00f2 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:43:0x00fe */
        /* JADX WARNING: Missing exception handler attribute for start block: B:45:0x010a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:47:0x0116 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:49:0x0122 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:51:0x012e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:53:0x013a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:55:0x0146 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:57:0x0152 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:59:0x015e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:61:0x016a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:63:0x0176 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.chartboost.sdk.d.a$c[] r0 = com.chartboost.sdk.d.a.c.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError = r0
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.INTERNAL     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x001f }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.INVALID_RESPONSE     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x002a }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.NO_HOST_ACTIVITY     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.USER_CANCELLATION     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.WRONG_ORIENTATION     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x004b }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.ERROR_PLAYING_VIDEO     // Catch:{ NoSuchFieldError -> 0x004b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x0056 }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.ERROR_CREATING_VIEW     // Catch:{ NoSuchFieldError -> 0x0056 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0056 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0056 }
            L_0x0056:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x0062 }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.SESSION_NOT_STARTED     // Catch:{ NoSuchFieldError -> 0x0062 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0062 }
                r2 = 8
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0062 }
            L_0x0062:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x006e }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.ERROR_DISPLAYING_VIEW     // Catch:{ NoSuchFieldError -> 0x006e }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x006e }
                r2 = 9
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x006e }
            L_0x006e:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x007a }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.ERROR_LOADING_WEB_VIEW     // Catch:{ NoSuchFieldError -> 0x007a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x007a }
                r2 = 10
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x007a }
            L_0x007a:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x0086 }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.INCOMPATIBLE_API_VERSION     // Catch:{ NoSuchFieldError -> 0x0086 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0086 }
                r2 = 11
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0086 }
            L_0x0086:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x0092 }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.ASSET_PREFETCH_IN_PROGRESS     // Catch:{ NoSuchFieldError -> 0x0092 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0092 }
                r2 = 12
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0092 }
            L_0x0092:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x009e }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.IMPRESSION_ALREADY_VISIBLE     // Catch:{ NoSuchFieldError -> 0x009e }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x009e }
                r2 = 13
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x009e }
            L_0x009e:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x00aa }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.ACTIVITY_MISSING_IN_MANIFEST     // Catch:{ NoSuchFieldError -> 0x00aa }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00aa }
                r2 = 14
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00aa }
            L_0x00aa:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x00b6 }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.WEB_VIEW_CLIENT_RECEIVED_ERROR     // Catch:{ NoSuchFieldError -> 0x00b6 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00b6 }
                r2 = 15
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00b6 }
            L_0x00b6:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x00c2 }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.NETWORK_FAILURE     // Catch:{ NoSuchFieldError -> 0x00c2 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00c2 }
                r2 = 16
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00c2 }
            L_0x00c2:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x00ce }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.END_POINT_DISABLED     // Catch:{ NoSuchFieldError -> 0x00ce }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00ce }
                r2 = 17
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00ce }
            L_0x00ce:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x00da }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.INTERNET_UNAVAILABLE     // Catch:{ NoSuchFieldError -> 0x00da }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00da }
                r2 = 18
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00da }
            L_0x00da:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x00e6 }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.TOO_MANY_CONNECTIONS     // Catch:{ NoSuchFieldError -> 0x00e6 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00e6 }
                r2 = 19
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00e6 }
            L_0x00e6:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x00f2 }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.ASSETS_DOWNLOAD_FAILURE     // Catch:{ NoSuchFieldError -> 0x00f2 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00f2 }
                r2 = 20
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00f2 }
            L_0x00f2:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x00fe }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.WEB_VIEW_PAGE_LOAD_TIMEOUT     // Catch:{ NoSuchFieldError -> 0x00fe }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00fe }
                r2 = 21
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00fe }
            L_0x00fe:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x010a }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.INVALID_LOCATION     // Catch:{ NoSuchFieldError -> 0x010a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x010a }
                r2 = 22
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x010a }
            L_0x010a:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x0116 }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.VIDEO_ID_MISSING     // Catch:{ NoSuchFieldError -> 0x0116 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0116 }
                r2 = 23
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0116 }
            L_0x0116:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x0122 }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.HARDWARE_ACCELERATION_DISABLED     // Catch:{ NoSuchFieldError -> 0x0122 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0122 }
                r2 = 24
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0122 }
            L_0x0122:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x012e }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.FIRST_SESSION_INTERSTITIALS_DISABLED     // Catch:{ NoSuchFieldError -> 0x012e }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x012e }
                r2 = 25
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x012e }
            L_0x012e:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x013a }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.INTERNET_UNAVAILABLE_AT_SHOW     // Catch:{ NoSuchFieldError -> 0x013a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x013a }
                r2 = 26
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x013a }
            L_0x013a:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x0146 }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.NO_AD_FOUND     // Catch:{ NoSuchFieldError -> 0x0146 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0146 }
                r2 = 27
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0146 }
            L_0x0146:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x0152 }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.ASSET_MISSING     // Catch:{ NoSuchFieldError -> 0x0152 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0152 }
                r2 = 28
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0152 }
            L_0x0152:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x015e }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.VIDEO_UNAVAILABLE     // Catch:{ NoSuchFieldError -> 0x015e }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x015e }
                r2 = 29
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x015e }
            L_0x015e:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x016a }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.EMPTY_LOCAL_VIDEO_LIST     // Catch:{ NoSuchFieldError -> 0x016a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x016a }
                r2 = 30
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x016a }
            L_0x016a:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x0176 }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.PENDING_IMPRESSION_ERROR     // Catch:{ NoSuchFieldError -> 0x0176 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0176 }
                r2 = 31
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0176 }
            L_0x0176:
                int[] r0 = com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError     // Catch:{ NoSuchFieldError -> 0x0182 }
                com.chartboost.sdk.d.a$c r1 = com.chartboost.sdk.d.a.c.VIDEO_UNAVAILABLE_FOR_CURRENT_ORIENTATION     // Catch:{ NoSuchFieldError -> 0x0182 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0182 }
                r2 = 32
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0182 }
            L_0x0182:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.ads.mediation.chartboost.ChartboostAdapterUtils.AnonymousClass1.<clinit>():void");
        }
    }

    ChartboostAdapterUtils() {
    }

    static ChartboostParams createChartboostParams(Bundle bundle, Bundle bundle2) {
        ChartboostParams chartboostParams = new ChartboostParams();
        String string = bundle.getString("appId");
        String string2 = bundle.getString(KEY_APP_SIGNATURE);
        if (!(string == null || string2 == null)) {
            chartboostParams.setAppId(string.trim());
            chartboostParams.setAppSignature(string2.trim());
        }
        String string3 = bundle.getString(KEY_AD_LOCATION);
        if (!isValidParam(string3)) {
            Log.w(ChartboostMediationAdapter.TAG, String.format("Chartboost ad location is empty, defaulting to %s. Please set the Ad Location parameter in the AdMob UI.", "Default"));
            string3 = "Default";
        }
        chartboostParams.setLocation(string3.trim());
        if (bundle2 != null && bundle2.containsKey("framework") && bundle2.containsKey("framework_version")) {
            chartboostParams.setFramework((a.C0125a) bundle2.getSerializable("framework"));
            chartboostParams.setFrameworkVersion(bundle2.getString("framework_version"));
        }
        return chartboostParams;
    }

    static int getAdRequestErrorType(a.c cVar) {
        switch (AnonymousClass1.$SwitchMap$com$chartboost$sdk$Model$CBError$CBImpressionError[cVar.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
                return 0;
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
                return 2;
            case 22:
            case 23:
            case 24:
            case 25:
                return 1;
            default:
                return 3;
        }
    }

    static boolean isValidChartboostParams(ChartboostParams chartboostParams) {
        String appId = chartboostParams.getAppId();
        String appSignature = chartboostParams.getAppSignature();
        if (isValidParam(appId) && isValidParam(appSignature)) {
            return true;
        }
        String str = !isValidParam(appId) ? !isValidParam(appSignature) ? "App ID and App Signature" : "App ID" : "App Signature";
        String str2 = ChartboostMediationAdapter.TAG;
        Log.w(str2, str + " cannot be empty.");
        return false;
    }

    static boolean isValidContext(Context context) {
        if (context == null) {
            Log.w(ChartboostAdapter.TAG, "Context cannot be null");
            return false;
        } else if (context instanceof Activity) {
            return true;
        } else {
            Log.w(ChartboostAdapter.TAG, "Context is not an Activity. Chartboost requires an Activity context to load ads.");
            return false;
        }
    }

    static boolean isValidParam(String str) {
        return (str == null || str.trim().length() == 0) ? false : true;
    }
}
