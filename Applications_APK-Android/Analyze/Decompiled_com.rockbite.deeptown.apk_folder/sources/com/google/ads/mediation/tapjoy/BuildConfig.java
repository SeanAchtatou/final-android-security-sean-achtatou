package com.google.ads.mediation.tapjoy;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.google.ads.mediation.tapjoy";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "";
    public static final int VERSION_CODE = 12030101;
    public static final String VERSION_NAME = "12.3.1.1";
}
