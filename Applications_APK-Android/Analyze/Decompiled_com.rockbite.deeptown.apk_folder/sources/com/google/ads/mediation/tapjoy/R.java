package com.google.ads.mediation.tapjoy;

public final class R {

    public static final class attr {
        public static final int adSize = 2130837538;
        public static final int adSizes = 2130837539;
        public static final int adUnitId = 2130837540;
        public static final int buttonSize = 2130837572;
        public static final int circleCrop = 2130837579;
        public static final int colorScheme = 2130837594;
        public static final int font = 2130837639;
        public static final int fontProviderAuthority = 2130837641;
        public static final int fontProviderCerts = 2130837642;
        public static final int fontProviderFetchStrategy = 2130837643;
        public static final int fontProviderFetchTimeout = 2130837644;
        public static final int fontProviderPackage = 2130837645;
        public static final int fontProviderQuery = 2130837646;
        public static final int fontStyle = 2130837647;
        public static final int fontWeight = 2130837649;
        public static final int imageAspectRatio = 2130837660;
        public static final int imageAspectRatioAdjust = 2130837661;
        public static final int scopeUris = 2130837725;

        private attr() {
        }
    }

    public static final class bool {
        public static final int abc_action_bar_embed_tabs = 2130903040;

        private bool() {
        }
    }

    public static final class color {
        public static final int common_google_signin_btn_text_dark = 2130968626;
        public static final int common_google_signin_btn_text_dark_default = 2130968627;
        public static final int common_google_signin_btn_text_dark_disabled = 2130968628;
        public static final int common_google_signin_btn_text_dark_focused = 2130968629;
        public static final int common_google_signin_btn_text_dark_pressed = 2130968630;
        public static final int common_google_signin_btn_text_light = 2130968631;
        public static final int common_google_signin_btn_text_light_default = 2130968632;
        public static final int common_google_signin_btn_text_light_disabled = 2130968633;
        public static final int common_google_signin_btn_text_light_focused = 2130968634;
        public static final int common_google_signin_btn_text_light_pressed = 2130968635;
        public static final int notification_action_color_filter = 2130968660;
        public static final int notification_icon_bg_color = 2130968661;
        public static final int primary_text_default_material_dark = 2130968666;
        public static final int ripple_material_light = 2130968671;
        public static final int secondary_text_default_material_dark = 2130968672;
        public static final int secondary_text_default_material_light = 2130968673;

        private color() {
        }
    }

    public static final class dimen {
        public static final int compat_button_inset_horizontal_material = 2131034196;
        public static final int compat_button_inset_vertical_material = 2131034197;
        public static final int compat_button_padding_horizontal_material = 2131034198;
        public static final int compat_button_padding_vertical_material = 2131034199;
        public static final int compat_control_corner_material = 2131034200;
        public static final int notification_action_icon_size = 2131034218;
        public static final int notification_action_text_size = 2131034219;
        public static final int notification_big_circle_margin = 2131034220;
        public static final int notification_content_margin_start = 2131034221;
        public static final int notification_large_icon_height = 2131034222;
        public static final int notification_large_icon_width = 2131034223;
        public static final int notification_main_column_padding_top = 2131034224;
        public static final int notification_media_narrow_margin = 2131034225;
        public static final int notification_right_icon_size = 2131034226;
        public static final int notification_right_side_padding_top = 2131034227;
        public static final int notification_small_icon_background_padding = 2131034228;
        public static final int notification_small_icon_size_as_large = 2131034229;
        public static final int notification_subtext_size = 2131034230;
        public static final int notification_top_pad = 2131034231;
        public static final int notification_top_pad_large_text = 2131034232;

        private dimen() {
        }
    }

    public static final class drawable {
        public static final int common_full_open_on_phone = 2131099745;
        public static final int common_google_signin_btn_icon_dark = 2131099746;
        public static final int common_google_signin_btn_icon_dark_focused = 2131099747;
        public static final int common_google_signin_btn_icon_dark_normal = 2131099748;
        public static final int common_google_signin_btn_icon_light = 2131099751;
        public static final int common_google_signin_btn_icon_light_focused = 2131099752;
        public static final int common_google_signin_btn_icon_light_normal = 2131099753;
        public static final int common_google_signin_btn_text_dark = 2131099755;
        public static final int common_google_signin_btn_text_dark_focused = 2131099756;
        public static final int common_google_signin_btn_text_dark_normal = 2131099757;
        public static final int common_google_signin_btn_text_light = 2131099760;
        public static final int common_google_signin_btn_text_light_focused = 2131099761;
        public static final int common_google_signin_btn_text_light_normal = 2131099762;
        public static final int notification_action_background = 2131099768;
        public static final int notification_bg = 2131099769;
        public static final int notification_bg_low = 2131099770;
        public static final int notification_bg_low_normal = 2131099771;
        public static final int notification_bg_low_pressed = 2131099772;
        public static final int notification_bg_normal = 2131099773;
        public static final int notification_bg_normal_pressed = 2131099774;
        public static final int notification_icon_background = 2131099775;
        public static final int notification_template_icon_bg = 2131099776;
        public static final int notification_template_icon_low_bg = 2131099777;
        public static final int notification_tile_bg = 2131099778;
        public static final int notify_panel_notification_icon_bg = 2131099779;

        private drawable() {
        }
    }

    public static final class id {
        public static final int action_container = 2131165230;
        public static final int action_divider = 2131165232;
        public static final int action_image = 2131165233;
        public static final int action_text = 2131165239;
        public static final int actions = 2131165240;
        public static final int adjust_height = 2131165243;
        public static final int adjust_width = 2131165244;
        public static final int async = 2131165248;
        public static final int auto = 2131165249;
        public static final int blocking = 2131165251;
        public static final int chronometer = 2131165266;
        public static final int dark = 2131165274;
        public static final int forever = 2131165287;
        public static final int icon = 2131165291;
        public static final int icon_group = 2131165292;
        public static final int icon_only = 2131165293;
        public static final int info = 2131165297;
        public static final int italic = 2131165298;
        public static final int light = 2131165301;
        public static final int line1 = 2131165302;
        public static final int line3 = 2131165303;
        public static final int none = 2131165312;
        public static final int normal = 2131165313;
        public static final int notification_background = 2131165314;
        public static final int notification_main_column = 2131165315;
        public static final int notification_main_column_container = 2131165316;
        public static final int right_icon = 2131165324;
        public static final int right_side = 2131165325;
        public static final int standard = 2131165350;
        public static final int text = 2131165363;
        public static final int text2 = 2131165364;
        public static final int time = 2131165367;
        public static final int title = 2131165368;
        public static final int wide = 2131165377;
        public static final int wrap_content = 2131165379;

        private id() {
        }
    }

    public static final class integer {
        public static final int cancel_button_image_alpha = 2131230722;
        public static final int google_play_services_version = 2131230724;
        public static final int status_bar_notification_info_maxnum = 2131230725;

        private integer() {
        }
    }

    public static final class layout {
        public static final int notification_action = 2131361830;
        public static final int notification_action_tombstone = 2131361831;
        public static final int notification_template_custom_big = 2131361832;
        public static final int notification_template_icon_group = 2131361833;
        public static final int notification_template_part_chronometer = 2131361834;
        public static final int notification_template_part_time = 2131361835;

        private layout() {
        }
    }

    public static final class string {
        public static final int cancel = 2131427359;
        public static final int common_google_play_services_enable_button = 2131427361;
        public static final int common_google_play_services_enable_text = 2131427362;
        public static final int common_google_play_services_enable_title = 2131427363;
        public static final int common_google_play_services_install_button = 2131427364;
        public static final int common_google_play_services_install_title = 2131427366;
        public static final int common_google_play_services_notification_ticker = 2131427368;
        public static final int common_google_play_services_unknown_issue = 2131427369;
        public static final int common_google_play_services_unsupported_text = 2131427370;
        public static final int common_google_play_services_update_button = 2131427371;
        public static final int common_google_play_services_update_text = 2131427372;
        public static final int common_google_play_services_update_title = 2131427373;
        public static final int common_google_play_services_updating_text = 2131427374;
        public static final int common_open_on_phone = 2131427376;
        public static final int common_signin_button_text = 2131427377;
        public static final int common_signin_button_text_long = 2131427378;
        public static final int date_format_month_day = 2131427379;
        public static final int date_format_year_month_day = 2131427380;
        public static final int empty = 2131427382;
        public static final int failed_to_get_more = 2131427384;
        public static final int failed_to_load = 2131427385;
        public static final int failed_to_refresh = 2131427386;
        public static final int getting_more = 2131427391;
        public static final int just_before = 2131427396;
        public static final int loading = 2131427397;
        public static final int no = 2131427398;
        public static final int ok = 2131427399;
        public static final int please_wait = 2131427400;
        public static final int pull_down_to_load = 2131427402;
        public static final int pull_down_to_refresh = 2131427403;
        public static final int pull_up_to_get_more = 2131427404;
        public static final int refresh = 2131427405;
        public static final int release_to_get_more = 2131427406;
        public static final int release_to_load = 2131427407;
        public static final int release_to_refresh = 2131427408;
        public static final int s1 = 2131427409;
        public static final int s2 = 2131427410;
        public static final int s3 = 2131427411;
        public static final int s4 = 2131427412;
        public static final int s5 = 2131427413;
        public static final int s6 = 2131427414;
        public static final int s7 = 2131427415;
        public static final int search_hint = 2131427416;
        public static final int settings = 2131427418;
        public static final int sign_in = 2131427419;
        public static final int sign_out = 2131427420;
        public static final int sign_up = 2131427421;
        public static final int status_bar_notification_info_overflow = 2131427422;
        public static final int today = 2131427423;
        public static final int updating = 2131427424;
        public static final int yes = 2131427425;
        public static final int yesterday = 2131427426;

        private string() {
        }
    }

    public static final class style {
        public static final int TextAppearance_Compat_Notification = 2131493101;
        public static final int TextAppearance_Compat_Notification_Info = 2131493102;
        public static final int TextAppearance_Compat_Notification_Line2 = 2131493103;
        public static final int TextAppearance_Compat_Notification_Time = 2131493104;
        public static final int TextAppearance_Compat_Notification_Title = 2131493105;
        public static final int Theme_IAPTheme = 2131493130;
        public static final int Widget_Compat_NotificationActionContainer = 2131493213;
        public static final int Widget_Compat_NotificationActionText = 2131493214;

        private style() {
        }
    }

    public static final class styleable {
        public static final int[] AdsAttrs = {com.rockbite.deeptown.R.attr.adSize, com.rockbite.deeptown.R.attr.adSizes, com.rockbite.deeptown.R.attr.adUnitId};
        public static final int AdsAttrs_adSize = 0;
        public static final int AdsAttrs_adSizes = 1;
        public static final int AdsAttrs_adUnitId = 2;
        public static final int[] FontFamily = {com.rockbite.deeptown.R.attr.fontProviderAuthority, com.rockbite.deeptown.R.attr.fontProviderCerts, com.rockbite.deeptown.R.attr.fontProviderFetchStrategy, com.rockbite.deeptown.R.attr.fontProviderFetchTimeout, com.rockbite.deeptown.R.attr.fontProviderPackage, com.rockbite.deeptown.R.attr.fontProviderQuery};
        public static final int[] FontFamilyFont = {16844082, 16844083, 16844095, 16844143, 16844144, com.rockbite.deeptown.R.attr.font, com.rockbite.deeptown.R.attr.fontStyle, com.rockbite.deeptown.R.attr.fontVariationSettings, com.rockbite.deeptown.R.attr.fontWeight, com.rockbite.deeptown.R.attr.ttcIndex};
        public static final int FontFamilyFont_android_font = 0;
        public static final int FontFamilyFont_android_fontStyle = 2;
        public static final int FontFamilyFont_android_fontVariationSettings = 4;
        public static final int FontFamilyFont_android_fontWeight = 1;
        public static final int FontFamilyFont_android_ttcIndex = 3;
        public static final int FontFamilyFont_font = 5;
        public static final int FontFamilyFont_fontStyle = 6;
        public static final int FontFamilyFont_fontVariationSettings = 7;
        public static final int FontFamilyFont_fontWeight = 8;
        public static final int FontFamilyFont_ttcIndex = 9;
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderCerts = 1;
        public static final int FontFamily_fontProviderFetchStrategy = 2;
        public static final int FontFamily_fontProviderFetchTimeout = 3;
        public static final int FontFamily_fontProviderPackage = 4;
        public static final int FontFamily_fontProviderQuery = 5;
        public static final int[] LoadingImageView = {com.rockbite.deeptown.R.attr.circleCrop, com.rockbite.deeptown.R.attr.imageAspectRatio, com.rockbite.deeptown.R.attr.imageAspectRatioAdjust};
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] SignInButton = {com.rockbite.deeptown.R.attr.buttonSize, com.rockbite.deeptown.R.attr.colorScheme, com.rockbite.deeptown.R.attr.scopeUris};
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;

        private styleable() {
        }
    }

    private R() {
    }
}
