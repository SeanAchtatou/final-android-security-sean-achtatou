package com.google.ads.mediation.tapjoy;

import android.app.Activity;
import android.util.Log;
import com.tapjoy.TJConnectListener;
import com.tapjoy.Tapjoy;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

public class TapjoyInitializer implements TJConnectListener {
    private static TapjoyInitializer instance;
    private ArrayList<Listener> initListeners = new ArrayList<>();
    private InitStatus status = InitStatus.UNINITIALIZED;

    private enum InitStatus {
        UNINITIALIZED,
        INITIALIZING,
        INITIALIZED
    }

    interface Listener {
        void onInitializeFailed(String str);

        void onInitializeSucceeded();
    }

    private TapjoyInitializer() {
    }

    static TapjoyInitializer getInstance() {
        if (instance == null) {
            instance = new TapjoyInitializer();
        }
        return instance;
    }

    /* access modifiers changed from: package-private */
    public void initialize(Activity activity, String str, Hashtable<String, Object> hashtable, Listener listener) {
        if (this.status.equals(InitStatus.INITIALIZED) || Tapjoy.isConnected()) {
            listener.onInitializeSucceeded();
            return;
        }
        this.initListeners.add(listener);
        if (!this.status.equals(InitStatus.INITIALIZING)) {
            this.status = InitStatus.INITIALIZING;
            Log.i(TapjoyMediationAdapter.TAG, "Connecting to Tapjoy for Tapjoy-AdMob adapter");
            Tapjoy.connect(activity, str, hashtable, this);
        }
    }

    public void onConnectFailure() {
        this.status = InitStatus.UNINITIALIZED;
        Iterator<Listener> it = this.initListeners.iterator();
        while (it.hasNext()) {
            it.next().onInitializeFailed("Tapjoy failed to connect.");
        }
        this.initListeners.clear();
    }

    public void onConnectSuccess() {
        this.status = InitStatus.INITIALIZED;
        Iterator<Listener> it = this.initListeners.iterator();
        while (it.hasNext()) {
            it.next().onInitializeSucceeded();
        }
        this.initListeners.clear();
    }
}
