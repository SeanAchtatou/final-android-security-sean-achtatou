package com.google.ads.mediation.tapjoy;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.applovin.sdk.AppLovinMediationProvider;
import com.google.ads.mediation.tapjoy.TapjoyInitializer;
import com.google.android.gms.ads.mediation.MediationAdRequest;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.mediation.MediationInterstitialListener;
import com.tapjoy.TJActionRequest;
import com.tapjoy.TJError;
import com.tapjoy.TJPlacement;
import com.tapjoy.TJPlacementListener;
import com.tapjoy.Tapjoy;
import com.tapjoy.TapjoyConnectFlag;
import java.util.Hashtable;

public class TapjoyAdapter extends TapjoyMediationAdapter implements MediationInterstitialAdapter {
    /* access modifiers changed from: private */
    public TJPlacement interstitialPlacement;
    private String interstitialPlacementName = null;
    /* access modifiers changed from: private */
    public final Handler mainHandler = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public MediationInterstitialListener mediationInterstitialListener;
    private String sdkKey = null;

    public static final class TapjoyExtrasBundleBuilder {
        private static final String DEBUG = "enable_debug";
        private boolean debugEnabled = false;

        public Bundle build() {
            Bundle bundle = new Bundle();
            bundle.putBoolean(DEBUG, this.debugEnabled);
            return bundle;
        }

        public TapjoyExtrasBundleBuilder setDebug(boolean z) {
            this.debugEnabled = z;
            return this;
        }
    }

    private boolean checkParams(Context context, Bundle bundle) {
        String str;
        if (bundle != null) {
            this.sdkKey = bundle.getString("sdkKey");
            str = bundle.getString("placementName");
        } else {
            str = null;
        }
        if (this.sdkKey == null || str == null) {
            Log.w(TapjoyMediationAdapter.TAG, "Did not receive valid server parameters from AdMob");
            return false;
        }
        this.interstitialPlacementName = str;
        if (context instanceof Activity) {
            Tapjoy.setActivity((Activity) context);
            return true;
        }
        Log.w(TapjoyMediationAdapter.TAG, "Tapjoy requires an Activity context to initialize");
        return false;
    }

    /* access modifiers changed from: private */
    public void createInterstitialPlacementAndRequestContent() {
        Log.i(TapjoyMediationAdapter.TAG, "Creating interstitial placement for AdMob adapter");
        this.interstitialPlacement = Tapjoy.getPlacement(this.interstitialPlacementName, new TJPlacementListener() {
            public void onClick(TJPlacement tJPlacement) {
                TapjoyAdapter.this.mainHandler.post(new Runnable() {
                    public void run() {
                        TapjoyAdapter.this.mediationInterstitialListener.onAdClicked(TapjoyAdapter.this);
                        TapjoyAdapter.this.mediationInterstitialListener.onAdLeftApplication(TapjoyAdapter.this);
                    }
                });
            }

            public void onContentDismiss(TJPlacement tJPlacement) {
                TapjoyAdapter.this.mainHandler.post(new Runnable() {
                    public void run() {
                        TapjoyAdapter.this.mediationInterstitialListener.onAdClosed(TapjoyAdapter.this);
                    }
                });
            }

            public void onContentReady(TJPlacement tJPlacement) {
                TapjoyAdapter.this.mainHandler.post(new Runnable() {
                    public void run() {
                        TapjoyAdapter.this.mediationInterstitialListener.onAdLoaded(TapjoyAdapter.this);
                    }
                });
            }

            public void onContentShow(TJPlacement tJPlacement) {
                TapjoyAdapter.this.mainHandler.post(new Runnable() {
                    public void run() {
                        TapjoyAdapter.this.mediationInterstitialListener.onAdOpened(TapjoyAdapter.this);
                    }
                });
            }

            public void onPurchaseRequest(TJPlacement tJPlacement, TJActionRequest tJActionRequest, String str) {
            }

            public void onRequestFailure(TJPlacement tJPlacement, final TJError tJError) {
                TapjoyAdapter.this.mainHandler.post(new Runnable() {
                    public void run() {
                        String str = TapjoyMediationAdapter.TAG;
                        Log.w(str, "Failed to request ad from Tapjoy: " + tJError.message);
                        TapjoyAdapter.this.mediationInterstitialListener.onAdFailedToLoad(TapjoyAdapter.this, 0);
                    }
                });
            }

            public void onRequestSuccess(TJPlacement tJPlacement) {
                TapjoyAdapter.this.mainHandler.post(new Runnable() {
                    public void run() {
                        if (!TapjoyAdapter.this.interstitialPlacement.isContentAvailable()) {
                            TapjoyAdapter.this.mediationInterstitialListener.onAdFailedToLoad(TapjoyAdapter.this, 3);
                        }
                    }
                });
            }

            public void onRewardRequest(TJPlacement tJPlacement, TJActionRequest tJActionRequest, String str, int i2) {
            }
        });
        this.interstitialPlacement.setMediationName(AppLovinMediationProvider.ADMOB);
        this.interstitialPlacement.setAdapterVersion("1.0.0");
        requestInterstitialPlacementContent();
    }

    private void requestInterstitialPlacementContent() {
        this.interstitialPlacement.requestContent();
    }

    public void onDestroy() {
    }

    public void onPause() {
    }

    public void onResume() {
    }

    public void requestInterstitialAd(Context context, MediationInterstitialListener mediationInterstitialListener2, Bundle bundle, MediationAdRequest mediationAdRequest, Bundle bundle2) {
        this.mediationInterstitialListener = mediationInterstitialListener2;
        if (!checkParams(context, bundle)) {
            this.mediationInterstitialListener.onAdFailedToLoad(this, 1);
            return;
        }
        Hashtable hashtable = new Hashtable();
        if (bundle2 != null && bundle2.containsKey("enable_debug")) {
            hashtable.put(TapjoyConnectFlag.ENABLE_LOGGING, Boolean.valueOf(bundle2.getBoolean("enable_debug", false)));
        }
        TapjoyInitializer.getInstance().initialize((Activity) context, this.sdkKey, hashtable, new TapjoyInitializer.Listener() {
            public void onInitializeFailed(String str) {
                String str2 = TapjoyMediationAdapter.TAG;
                Log.w(str2, "Failed to load ad from Tapjoy: " + str);
                TapjoyAdapter.this.mediationInterstitialListener.onAdFailedToLoad(TapjoyAdapter.this, 0);
            }

            public void onInitializeSucceeded() {
                if (TapjoyAdapter.this.interstitialPlacement == null || !TapjoyAdapter.this.interstitialPlacement.isContentAvailable()) {
                    TapjoyAdapter.this.createInterstitialPlacementAndRequestContent();
                } else {
                    TapjoyAdapter.this.mediationInterstitialListener.onAdLoaded(TapjoyAdapter.this);
                }
            }
        });
    }

    public void showInterstitial() {
        Log.i(TapjoyMediationAdapter.TAG, "Show interstitial content for Tapjoy-AdMob adapter");
        TJPlacement tJPlacement = this.interstitialPlacement;
        if (tJPlacement != null && tJPlacement.isContentAvailable()) {
            this.interstitialPlacement.showContent();
        }
    }
}
