package com.google.ads.mediation.tapjoy;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.google.ads.mediation.tapjoy.TapjoyInitializer;
import com.google.ads.mediation.tapjoy.rtb.TapjoyRtbInterstitialRenderer;
import com.google.android.gms.ads.mediation.InitializationCompleteCallback;
import com.google.android.gms.ads.mediation.MediationAdLoadCallback;
import com.google.android.gms.ads.mediation.MediationConfiguration;
import com.google.android.gms.ads.mediation.MediationInterstitialAd;
import com.google.android.gms.ads.mediation.MediationInterstitialAdCallback;
import com.google.android.gms.ads.mediation.MediationInterstitialAdConfiguration;
import com.google.android.gms.ads.mediation.MediationRewardedAd;
import com.google.android.gms.ads.mediation.MediationRewardedAdCallback;
import com.google.android.gms.ads.mediation.MediationRewardedAdConfiguration;
import com.google.android.gms.ads.mediation.VersionInfo;
import com.google.android.gms.ads.mediation.rtb.RtbAdapter;
import com.google.android.gms.ads.mediation.rtb.RtbSignalData;
import com.google.android.gms.ads.mediation.rtb.SignalCallbacks;
import com.tapjoy.Tapjoy;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;

public class TapjoyMediationAdapter extends RtbAdapter {
    static final String MEDIATION_AGENT = "admob";
    static final String PLACEMENT_NAME_SERVER_PARAMETER_KEY = "placementName";
    static final String SDK_KEY_SERVER_PARAMETER_KEY = "sdkKey";
    static final String TAG = "TapjoyMediationAdapter";
    static final String TAPJOY_INTERNAL_ADAPTER_VERSION = "1.0.0";

    public void collectSignals(RtbSignalData rtbSignalData, SignalCallbacks signalCallbacks) {
        signalCallbacks.onSuccess(Tapjoy.getUserToken());
    }

    public VersionInfo getSDKVersionInfo() {
        String[] split = Tapjoy.getVersion().split("\\.");
        return new VersionInfo(Integer.parseInt(split[0]), Integer.parseInt(split[1]), Integer.parseInt(split[2]));
    }

    public VersionInfo getVersionInfo() {
        String[] split = BuildConfig.VERSION_NAME.split("\\.");
        return new VersionInfo(Integer.parseInt(split[0]), Integer.parseInt(split[1]), (Integer.parseInt(split[2]) * 100) + Integer.parseInt(split[3]));
    }

    public void initialize(Context context, final InitializationCompleteCallback initializationCompleteCallback, List<MediationConfiguration> list) {
        if (!(context instanceof Activity)) {
            initializationCompleteCallback.onInitializationFailed("Initialization Failed: Tapjoy SDK requires an Activity context to initialize");
            return;
        }
        HashSet hashSet = new HashSet();
        for (MediationConfiguration serverParameters : list) {
            String string = serverParameters.getServerParameters().getString(SDK_KEY_SERVER_PARAMETER_KEY);
            if (!TextUtils.isEmpty(string)) {
                hashSet.add(string);
            }
        }
        int size = hashSet.size();
        if (size > 0) {
            String str = (String) hashSet.iterator().next();
            if (size > 1) {
                Log.w(TAG, String.format("Multiple '%s' entries found: %s. Using '%s' to initialize the IronSource SDK.", SDK_KEY_SERVER_PARAMETER_KEY, hashSet.toString(), str));
            }
            Activity activity = (Activity) context;
            Tapjoy.setActivity(activity);
            TapjoyInitializer.getInstance().initialize(activity, str, new Hashtable(), new TapjoyInitializer.Listener() {
                public void onInitializeFailed(String str) {
                    InitializationCompleteCallback initializationCompleteCallback = initializationCompleteCallback;
                    initializationCompleteCallback.onInitializationFailed("Initialization failed: " + str);
                }

                public void onInitializeSucceeded() {
                    initializationCompleteCallback.onInitializationSucceeded();
                }
            });
            return;
        }
        initializationCompleteCallback.onInitializationFailed("Initialization failed: Missing or Invalid SDK key.");
    }

    public void loadInterstitialAd(MediationInterstitialAdConfiguration mediationInterstitialAdConfiguration, MediationAdLoadCallback<MediationInterstitialAd, MediationInterstitialAdCallback> mediationAdLoadCallback) {
        new TapjoyRtbInterstitialRenderer(mediationInterstitialAdConfiguration, mediationAdLoadCallback).render();
    }

    public void loadRewardedAd(MediationRewardedAdConfiguration mediationRewardedAdConfiguration, MediationAdLoadCallback<MediationRewardedAd, MediationRewardedAdCallback> mediationAdLoadCallback) {
        new TapjoyRewardedRenderer(mediationRewardedAdConfiguration, mediationAdLoadCallback).render();
    }
}
