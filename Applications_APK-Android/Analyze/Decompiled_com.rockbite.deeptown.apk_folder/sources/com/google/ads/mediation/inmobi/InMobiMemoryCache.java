package com.google.ads.mediation.inmobi;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

class InMobiMemoryCache {
    private static final String TAG = "MemoryCache";
    private final Map<String, Drawable> mCache = Collections.synchronizedMap(new LinkedHashMap(10, 1.5f, true));
    private long mLimit = 1000000;
    private long mSize = 0;

    InMobiMemoryCache() {
        setLimit(Runtime.getRuntime().maxMemory() / 4);
    }

    private void checkSize() {
        Log.i(TAG, "cache size=" + this.mSize + " length=" + this.mCache.size());
        if (this.mSize > this.mLimit) {
            Iterator<Map.Entry<String, Drawable>> it = this.mCache.entrySet().iterator();
            while (it.hasNext()) {
                this.mSize -= getSizeInBytes(((BitmapDrawable) it.next().getValue()).getBitmap());
                it.remove();
                if (this.mSize <= this.mLimit) {
                    break;
                }
            }
            Log.i(TAG, "Clean cache. New size " + this.mCache.size());
        }
    }

    private long getSizeInBytes(Bitmap bitmap) {
        if (bitmap == null) {
            return 0;
        }
        return (long) (bitmap.getRowBytes() * bitmap.getHeight());
    }

    private void setLimit(long j2) {
        this.mLimit = j2;
        StringBuilder sb = new StringBuilder();
        sb.append("MemoryCache will use up to ");
        double d2 = (double) this.mLimit;
        Double.isNaN(d2);
        sb.append((d2 / 1024.0d) / 1024.0d);
        sb.append("MB");
        Log.i(TAG, sb.toString());
    }

    public void clear() {
        try {
            this.mCache.clear();
            this.mSize = 0;
        } catch (NullPointerException e2) {
            e2.printStackTrace();
        }
    }

    public Drawable get(String str) {
        try {
            if (!this.mCache.containsKey(str)) {
                return null;
            }
            return this.mCache.get(str);
        } catch (NullPointerException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public void put(String str, Drawable drawable) {
        try {
            if (this.mCache.containsKey(str)) {
                this.mSize -= getSizeInBytes(((BitmapDrawable) this.mCache.get(str)).getBitmap());
            }
            this.mCache.put(str, drawable);
            this.mSize += getSizeInBytes(((BitmapDrawable) drawable).getBitmap());
            checkSize();
            Log.d(TAG, "Drawable used from cache");
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }
}
