package com.google.ads.mediation.chartboost;

import com.chartboost.sdk.a;

public class ChartboostParams {
    private String appId;
    private String appSignature;
    private a.C0125a cbFramework;
    private String cbFrameworkVersion;
    private String cbLocation = "Default";

    public String getAppId() {
        return this.appId;
    }

    public String getAppSignature() {
        return this.appSignature;
    }

    public a.C0125a getFramework() {
        return this.cbFramework;
    }

    public String getFrameworkVersion() {
        return this.cbFrameworkVersion;
    }

    public String getLocation() {
        return this.cbLocation;
    }

    public void setAppId(String str) {
        this.appId = str;
    }

    public void setAppSignature(String str) {
        this.appSignature = str;
    }

    public void setFramework(a.C0125a aVar) {
        this.cbFramework = aVar;
    }

    public void setFrameworkVersion(String str) {
        this.cbFrameworkVersion = str;
    }

    public void setLocation(String str) {
        this.cbLocation = str;
    }
}
