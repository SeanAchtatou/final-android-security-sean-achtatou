package com.google.ads.mediation.chartboost;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import androidx.annotation.Keep;
import com.chartboost.sdk.a;
import com.chartboost.sdk.d.a;
import com.google.android.gms.ads.mediation.MediationAdRequest;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.mediation.MediationInterstitialListener;

@Keep
public class ChartboostAdapter extends ChartboostMediationAdapter implements MediationInterstitialAdapter {
    protected static final String TAG = "ChartboostAdapter";
    /* access modifiers changed from: private */
    public AbstractChartboostAdapterDelegate mChartboostInterstitialDelegate = new AbstractChartboostAdapterDelegate() {
        public void didCacheInterstitial(String str) {
            super.didCacheInterstitial(str);
            if (ChartboostAdapter.this.mMediationInterstitialListener != null && ChartboostAdapter.this.mIsLoading && str.equals(ChartboostAdapter.this.mChartboostParams.getLocation())) {
                boolean unused = ChartboostAdapter.this.mIsLoading = false;
                ChartboostAdapter.this.mMediationInterstitialListener.onAdLoaded(ChartboostAdapter.this);
            }
        }

        public void didClickInterstitial(String str) {
            super.didClickInterstitial(str);
            if (ChartboostAdapter.this.mMediationInterstitialListener != null) {
                ChartboostAdapter.this.mMediationInterstitialListener.onAdClicked(ChartboostAdapter.this);
                ChartboostAdapter.this.mMediationInterstitialListener.onAdLeftApplication(ChartboostAdapter.this);
            }
        }

        public void didDismissInterstitial(String str) {
            super.didDismissInterstitial(str);
            if (ChartboostAdapter.this.mMediationInterstitialListener != null) {
                ChartboostAdapter.this.mMediationInterstitialListener.onAdClosed(ChartboostAdapter.this);
            }
        }

        public void didDisplayInterstitial(String str) {
            super.didDisplayInterstitial(str);
            if (ChartboostAdapter.this.mMediationInterstitialListener != null) {
                ChartboostAdapter.this.mMediationInterstitialListener.onAdOpened(ChartboostAdapter.this);
            }
        }

        public void didFailToLoadInterstitial(String str, a.c cVar) {
            super.didFailToLoadInterstitial(str, cVar);
            if (ChartboostAdapter.this.mMediationInterstitialListener != null && str.equals(ChartboostAdapter.this.mChartboostParams.getLocation())) {
                if (ChartboostAdapter.this.mIsLoading) {
                    boolean unused = ChartboostAdapter.this.mIsLoading = false;
                    ChartboostAdapter.this.mMediationInterstitialListener.onAdFailedToLoad(ChartboostAdapter.this, ChartboostAdapterUtils.getAdRequestErrorType(cVar));
                } else if (cVar == a.c.INTERNET_UNAVAILABLE_AT_SHOW) {
                    ChartboostAdapter.this.mMediationInterstitialListener.onAdOpened(ChartboostAdapter.this);
                    ChartboostAdapter.this.mMediationInterstitialListener.onAdClosed(ChartboostAdapter.this);
                }
            }
        }

        public void didInitialize() {
            super.didInitialize();
            boolean unused = ChartboostAdapter.this.mIsLoading = true;
            ChartboostSingleton.loadInterstitialAd(ChartboostAdapter.this.mChartboostInterstitialDelegate);
        }

        public ChartboostParams getChartboostParams() {
            return ChartboostAdapter.this.mChartboostParams;
        }
    };
    /* access modifiers changed from: private */
    public ChartboostParams mChartboostParams = new ChartboostParams();
    /* access modifiers changed from: private */
    public boolean mIsLoading;
    /* access modifiers changed from: private */
    public MediationInterstitialListener mMediationInterstitialListener;

    public static final class ChartboostExtrasBundleBuilder {
        static final String KEY_FRAMEWORK = "framework";
        static final String KEY_FRAMEWORK_VERSION = "framework_version";
        private a.C0125a cbFramework;
        private String cbFrameworkVersion;

        public Bundle build() {
            Bundle bundle = new Bundle();
            bundle.putSerializable(KEY_FRAMEWORK, this.cbFramework);
            bundle.putString(KEY_FRAMEWORK_VERSION, this.cbFrameworkVersion);
            return bundle;
        }

        public ChartboostExtrasBundleBuilder setFramework(a.C0125a aVar, String str) {
            this.cbFramework = aVar;
            this.cbFrameworkVersion = str;
            return this;
        }
    }

    public void onDestroy() {
    }

    public void onPause() {
    }

    public void onResume() {
    }

    public void requestInterstitialAd(Context context, MediationInterstitialListener mediationInterstitialListener, Bundle bundle, MediationAdRequest mediationAdRequest, Bundle bundle2) {
        this.mMediationInterstitialListener = mediationInterstitialListener;
        this.mChartboostParams = ChartboostAdapterUtils.createChartboostParams(bundle, bundle2);
        if (!ChartboostAdapterUtils.isValidChartboostParams(this.mChartboostParams)) {
            MediationInterstitialListener mediationInterstitialListener2 = this.mMediationInterstitialListener;
            if (mediationInterstitialListener2 != null) {
                mediationInterstitialListener2.onAdFailedToLoad(this, 1);
            }
        } else if (!ChartboostAdapterUtils.isValidContext(context)) {
            Log.w(TAG, "Failed to request ad from Chartboost: Internal Error.");
            this.mMediationInterstitialListener.onAdFailedToLoad(this, 1);
        } else {
            ChartboostSingleton.startChartboostInterstitial(context, this.mChartboostInterstitialDelegate);
        }
    }

    public void showInterstitial() {
        ChartboostSingleton.showInterstitialAd(this.mChartboostInterstitialDelegate);
    }
}
