package com.google.ads.mediation.chartboost;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import com.chartboost.sdk.d.a;
import com.google.android.gms.ads.mediation.Adapter;
import com.google.android.gms.ads.mediation.InitializationCompleteCallback;
import com.google.android.gms.ads.mediation.MediationAdLoadCallback;
import com.google.android.gms.ads.mediation.MediationConfiguration;
import com.google.android.gms.ads.mediation.MediationRewardedAd;
import com.google.android.gms.ads.mediation.MediationRewardedAdCallback;
import com.google.android.gms.ads.mediation.MediationRewardedAdConfiguration;
import com.google.android.gms.ads.mediation.VersionInfo;
import com.google.firebase.remoteconfig.RemoteConfigConstants;
import java.util.HashMap;
import java.util.List;

public class ChartboostMediationAdapter extends Adapter implements MediationRewardedAd {
    static final String TAG = "ChartboostMediationAdapter";
    /* access modifiers changed from: private */
    public MediationAdLoadCallback<MediationRewardedAd, MediationRewardedAdCallback> mAdLoadCallback;
    /* access modifiers changed from: private */
    public ChartboostParams mChartboostParams = new ChartboostParams();
    /* access modifiers changed from: private */
    public AbstractChartboostAdapterDelegate mChartboostRewardedVideoDelegate = new AbstractChartboostAdapterDelegate() {
        public void didCacheRewardedVideo(String str) {
            super.didCacheRewardedVideo(str);
            if (ChartboostMediationAdapter.this.mAdLoadCallback != null && ChartboostMediationAdapter.this.mIsLoading && str.equals(ChartboostMediationAdapter.this.mChartboostParams.getLocation())) {
                boolean unused = ChartboostMediationAdapter.this.mIsLoading = false;
                ChartboostMediationAdapter chartboostMediationAdapter = ChartboostMediationAdapter.this;
                MediationRewardedAdCallback unused2 = chartboostMediationAdapter.mRewardedAdCallback = (MediationRewardedAdCallback) chartboostMediationAdapter.mAdLoadCallback.onSuccess(ChartboostMediationAdapter.this);
            }
        }

        public void didClickRewardedVideo(String str) {
            super.didClickRewardedVideo(str);
            if (ChartboostMediationAdapter.this.mRewardedAdCallback != null) {
                ChartboostMediationAdapter.this.mRewardedAdCallback.reportAdClicked();
            }
        }

        public void didCompleteRewardedVideo(String str, int i2) {
            super.didCompleteRewardedVideo(str, i2);
            if (ChartboostMediationAdapter.this.mRewardedAdCallback != null) {
                ChartboostMediationAdapter.this.mRewardedAdCallback.onVideoComplete();
                ChartboostMediationAdapter.this.mRewardedAdCallback.onUserEarnedReward(new ChartboostReward(i2));
            }
        }

        public void didDismissRewardedVideo(String str) {
            super.didDismissRewardedVideo(str);
            if (ChartboostMediationAdapter.this.mRewardedAdCallback != null) {
                ChartboostMediationAdapter.this.mRewardedAdCallback.onAdClosed();
            }
        }

        public void didDisplayRewardedVideo(String str) {
            super.didDisplayRewardedVideo(str);
            if (ChartboostMediationAdapter.this.mRewardedAdCallback != null) {
                ChartboostMediationAdapter.this.mRewardedAdCallback.onAdOpened();
                ChartboostMediationAdapter.this.mRewardedAdCallback.onVideoStart();
                ChartboostMediationAdapter.this.mRewardedAdCallback.reportAdImpression();
            }
        }

        public void didFailToLoadRewardedVideo(String str, a.c cVar) {
            super.didFailToLoadRewardedVideo(str, cVar);
            if (ChartboostMediationAdapter.this.mAdLoadCallback != null && str.equals(ChartboostMediationAdapter.this.mChartboostParams.getLocation())) {
                if (ChartboostMediationAdapter.this.mIsLoading) {
                    String str2 = "Failed to load ad from Chartboost: " + cVar.toString();
                    Log.w(ChartboostMediationAdapter.TAG, str2);
                    ChartboostMediationAdapter.this.mAdLoadCallback.onFailure(str2);
                    boolean unused = ChartboostMediationAdapter.this.mIsLoading = false;
                } else if (cVar == a.c.INTERNET_UNAVAILABLE_AT_SHOW && ChartboostMediationAdapter.this.mRewardedAdCallback != null) {
                    ChartboostMediationAdapter.this.mRewardedAdCallback.onAdFailedToShow("No network connection is available.");
                }
            }
        }

        public void didInitialize() {
            super.didInitialize();
            if (ChartboostMediationAdapter.this.mInitializationCallback != null) {
                ChartboostMediationAdapter.this.mInitializationCallback.onInitializationSucceeded();
            }
            if (ChartboostMediationAdapter.this.mAdLoadCallback != null) {
                boolean unused = ChartboostMediationAdapter.this.mIsLoading = true;
                ChartboostSingleton.loadRewardedVideoAd(ChartboostMediationAdapter.this.mChartboostRewardedVideoDelegate);
            }
        }

        public ChartboostParams getChartboostParams() {
            return ChartboostMediationAdapter.this.mChartboostParams;
        }
    };
    /* access modifiers changed from: private */
    public InitializationCompleteCallback mInitializationCallback;
    /* access modifiers changed from: private */
    public boolean mIsLoading;
    /* access modifiers changed from: private */
    public MediationRewardedAdCallback mRewardedAdCallback;

    public VersionInfo getSDKVersionInfo() {
        String[] split = com.chartboost.sdk.a.a().split("\\.");
        return new VersionInfo(Integer.parseInt(split[0]), Integer.parseInt(split[1]), Integer.parseInt(split[2]));
    }

    public VersionInfo getVersionInfo() {
        String[] split = BuildConfig.VERSION_NAME.split("\\.");
        return new VersionInfo(Integer.parseInt(split[0]), Integer.parseInt(split[1]), (Integer.parseInt(split[2]) * 100) + Integer.parseInt(split[3]));
    }

    public void initialize(Context context, InitializationCompleteCallback initializationCompleteCallback, List<MediationConfiguration> list) {
        if (!(context instanceof Activity)) {
            initializationCompleteCallback.onInitializationFailed("Chartboost SDK requires an Activity context to initialize");
            return;
        }
        HashMap hashMap = new HashMap();
        for (MediationConfiguration serverParameters : list) {
            Bundle serverParameters2 = serverParameters.getServerParameters();
            String string = serverParameters2.getString(RemoteConfigConstants.RequestFieldKey.APP_ID);
            if (!TextUtils.isEmpty(string)) {
                hashMap.put(string, serverParameters2);
            }
        }
        int size = hashMap.size();
        if (size > 0) {
            String str = (String) hashMap.keySet().iterator().next();
            Bundle bundle = (Bundle) hashMap.get(str);
            if (size > 1) {
                Log.w(TAG, String.format("Multiple '%s' entries found: %s. Using '%s' to initialize the Chartboost SDK", RemoteConfigConstants.RequestFieldKey.APP_ID, hashMap.keySet(), str));
            }
            this.mInitializationCallback = initializationCompleteCallback;
            this.mChartboostParams = ChartboostAdapterUtils.createChartboostParams(bundle, null);
            if (!ChartboostAdapterUtils.isValidChartboostParams(this.mChartboostParams)) {
                initializationCompleteCallback.onInitializationFailed("Initialization Failed: Invalid server parameters.");
            } else {
                ChartboostSingleton.startChartboostRewardedVideo(context, this.mChartboostRewardedVideoDelegate);
            }
        } else {
            initializationCompleteCallback.onInitializationFailed("Initialization failed:Missing or invalid App ID.");
        }
    }

    public void loadRewardedAd(MediationRewardedAdConfiguration mediationRewardedAdConfiguration, MediationAdLoadCallback<MediationRewardedAd, MediationRewardedAdCallback> mediationAdLoadCallback) {
        this.mAdLoadCallback = mediationAdLoadCallback;
        Bundle serverParameters = mediationRewardedAdConfiguration.getServerParameters();
        Bundle mediationExtras = mediationRewardedAdConfiguration.getMediationExtras();
        Context context = mediationRewardedAdConfiguration.getContext();
        if (!(context instanceof Activity)) {
            Log.w(TAG, "Failed to request ad from Chartboost: Chartboost SDK requires an Activity context to initialize.");
            mediationAdLoadCallback.onFailure("Failed to request ad from Chartboost: Chartboost SDK requires an Activity context to initialize.");
            return;
        }
        this.mChartboostParams = ChartboostAdapterUtils.createChartboostParams(serverParameters, mediationExtras);
        if (!ChartboostAdapterUtils.isValidChartboostParams(this.mChartboostParams)) {
            Log.w(TAG, "Failed to request ad from Chartboost: Invalid server parameters.");
            mediationAdLoadCallback.onFailure("Failed to request ad from Chartboost: Invalid server parameters.");
        } else if (!ChartboostAdapterUtils.isValidContext(context)) {
            Log.w(TAG, "Failed to request ad from Chartboost: Internal Error.");
            mediationAdLoadCallback.onFailure("Failed to request ad from Chartboost: Internal Error.");
        } else {
            ChartboostSingleton.startChartboostRewardedVideo(context, this.mChartboostRewardedVideoDelegate);
        }
    }

    public void showAd(Context context) {
        ChartboostSingleton.showRewardedVideoAd(this.mChartboostRewardedVideoDelegate);
    }
}
