package com.google.ads.mediation.chartboost;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.google.ads.mediation.chartboost";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "";
    public static final int VERSION_CODE = 7050000;
    public static final String VERSION_NAME = "7.5.0.0";
}
