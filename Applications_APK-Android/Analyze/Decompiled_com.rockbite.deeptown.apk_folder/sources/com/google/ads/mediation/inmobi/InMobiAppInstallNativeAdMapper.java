package com.google.ads.mediation.inmobi;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;
import com.google.ads.mediation.inmobi.ImageDownloaderAsyncTask;
import com.google.android.gms.ads.mediation.MediationNativeListener;
import com.google.android.gms.ads.mediation.NativeAppInstallAdMapper;
import com.inmobi.ads.InMobiNative;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

class InMobiAppInstallNativeAdMapper extends NativeAppInstallAdMapper {
    /* access modifiers changed from: private */
    public static final String LOG_TAG = "InMobiAppInstallNativeAdMapper";
    /* access modifiers changed from: private */
    public final InMobiAdapter mInMobiAdapter;
    /* access modifiers changed from: private */
    public final InMobiNative mInMobiNative;
    private final Boolean mIsOnlyURL;
    private final HashMap<String, String> mLandingUrlMap = new HashMap<>();
    /* access modifiers changed from: private */
    public final MediationNativeListener mMediationNativeListener;

    InMobiAppInstallNativeAdMapper(InMobiAdapter inMobiAdapter, InMobiNative inMobiNative, Boolean bool, MediationNativeListener mediationNativeListener) {
        this.mInMobiAdapter = inMobiAdapter;
        this.mInMobiNative = inMobiNative;
        this.mIsOnlyURL = bool;
        this.mMediationNativeListener = mediationNativeListener;
    }

    public void handleClick(View view) {
        this.mInMobiNative.reportAdClickAndOpenLandingPage();
    }

    /* access modifiers changed from: package-private */
    public void mapAppInstallAd(final Context context) {
        try {
            if (this.mInMobiNative.getCustomAdContent() != null) {
                JSONObject customAdContent = this.mInMobiNative.getCustomAdContent();
                setHeadline((String) InMobiAdapterUtils.mandatoryChecking(this.mInMobiNative.getAdTitle(), "title"));
                setBody((String) InMobiAdapterUtils.mandatoryChecking(this.mInMobiNative.getAdDescription(), InMobiNetworkValues.DESCRIPTION));
                setCallToAction((String) InMobiAdapterUtils.mandatoryChecking(this.mInMobiNative.getAdCtaText(), InMobiNetworkValues.CTA));
                String str = (String) InMobiAdapterUtils.mandatoryChecking(this.mInMobiNative.getAdLandingPageUrl(), InMobiNetworkValues.LANDING_URL);
                Bundle bundle = new Bundle();
                bundle.putString(InMobiNetworkValues.LANDING_URL, str);
                setExtras(bundle);
                this.mLandingUrlMap.put(InMobiNetworkValues.LANDING_URL, str);
                HashMap hashMap = new HashMap();
                URL url = new URL(this.mInMobiNative.getAdIconUrl());
                final Uri parse = Uri.parse(url.toURI().toString());
                final Double valueOf = Double.valueOf(1.0d);
                if (!this.mIsOnlyURL.booleanValue()) {
                    hashMap.put("icon_key", url);
                } else {
                    setIcon(new InMobiNativeMappedImage(null, parse, valueOf.doubleValue()));
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(new InMobiNativeMappedImage(new ColorDrawable(0), null, 1.0d));
                    setImages(arrayList);
                }
                try {
                    if (customAdContent.has(InMobiNetworkValues.RATING)) {
                        setStarRating(Double.parseDouble(customAdContent.getString(InMobiNetworkValues.RATING)));
                    }
                    if (customAdContent.has(InMobiNetworkValues.PACKAGE_NAME)) {
                        setStore("Google Play");
                    } else {
                        setStore("Others");
                    }
                    if (customAdContent.has("price")) {
                        setPrice(customAdContent.getString("price"));
                    }
                } catch (JSONException e2) {
                    e2.printStackTrace();
                }
                final RelativeLayout relativeLayout = new RelativeLayout(context);
                relativeLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
                ViewTreeObserver viewTreeObserver = relativeLayout.getViewTreeObserver();
                if (viewTreeObserver.isAlive()) {
                    viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                        public void onGlobalLayout() {
                            if (Build.VERSION.SDK_INT >= 16) {
                                relativeLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                            } else {
                                relativeLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                            }
                            int width = ((View) relativeLayout.getParent()).getWidth();
                            String access$000 = InMobiAppInstallNativeAdMapper.LOG_TAG;
                            Log.d(access$000, "parent layout width is " + width);
                            View primaryViewOfWidth = InMobiAppInstallNativeAdMapper.this.mInMobiNative.getPrimaryViewOfWidth(context, null, relativeLayout, width);
                            if (primaryViewOfWidth != null) {
                                relativeLayout.addView(primaryViewOfWidth);
                            }
                        }
                    });
                }
                setMediaView(relativeLayout);
                setHasVideoContent(this.mInMobiNative.isVideo() == null ? false : this.mInMobiNative.isVideo().booleanValue());
                setOverrideClickHandling(false);
                if (!this.mIsOnlyURL.booleanValue()) {
                    new ImageDownloaderAsyncTask(new ImageDownloaderAsyncTask.DrawableDownloadListener() {
                        public void onDownloadFailure() {
                            InMobiAppInstallNativeAdMapper.this.mMediationNativeListener.onAdFailedToLoad(InMobiAppInstallNativeAdMapper.this.mInMobiAdapter, 3);
                        }

                        public void onDownloadSuccess(HashMap<String, Drawable> hashMap) {
                            Drawable drawable = hashMap.get("icon_key");
                            InMobiAppInstallNativeAdMapper.this.setIcon(new InMobiNativeMappedImage(drawable, parse, valueOf.doubleValue()));
                            ArrayList arrayList = new ArrayList();
                            arrayList.add(new InMobiNativeMappedImage(new ColorDrawable(0), null, 1.0d));
                            InMobiAppInstallNativeAdMapper.this.setImages(arrayList);
                            if (drawable != null) {
                                InMobiAppInstallNativeAdMapper.this.mMediationNativeListener.onAdLoaded(InMobiAppInstallNativeAdMapper.this.mInMobiAdapter, InMobiAppInstallNativeAdMapper.this);
                            } else {
                                InMobiAppInstallNativeAdMapper.this.mMediationNativeListener.onAdFailedToLoad(InMobiAppInstallNativeAdMapper.this.mInMobiAdapter, 2);
                            }
                        }
                    }).execute(hashMap);
                } else {
                    this.mMediationNativeListener.onAdLoaded(this.mInMobiAdapter, this);
                }
            } else {
                this.mMediationNativeListener.onAdFailedToLoad(this.mInMobiAdapter, 3);
            }
        } catch (MandatoryParamException | MalformedURLException | URISyntaxException e3) {
            e3.printStackTrace();
            this.mMediationNativeListener.onAdFailedToLoad(this.mInMobiAdapter, 3);
        }
    }

    public void recordImpression() {
    }

    public void trackView(View view) {
    }

    public void untrackView(View view) {
        this.mInMobiNative.destroy();
    }
}
