package com.google.ads.mediation.inmobi;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import com.google.android.gms.ads.formats.NativeAd;

class InMobiNativeMappedImage extends NativeAd.Image {
    private final Drawable mInMobiDrawable;
    private final double mInMobiScale;
    private final Uri mInmobiImageUri;

    public InMobiNativeMappedImage(Drawable drawable, Uri uri, double d2) {
        this.mInMobiDrawable = drawable;
        this.mInmobiImageUri = uri;
        this.mInMobiScale = d2;
    }

    public Drawable getDrawable() {
        return this.mInMobiDrawable;
    }

    public double getScale() {
        return this.mInMobiScale;
    }

    public Uri getUri() {
        return this.mInmobiImageUri;
    }
}
