package com.google.ads.mediation.facebook;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.annotation.Keep;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdOptionsView;
import com.facebook.ads.AdSettings;
import com.facebook.ads.AdView;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdExtendedListener;
import com.facebook.ads.MediaView;
import com.facebook.ads.MediaViewListener;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdBase;
import com.facebook.ads.NativeAdLayout;
import com.facebook.ads.NativeAdListener;
import com.facebook.ads.NativeBannerAd;
import com.google.ads.mediation.facebook.FacebookInitializer;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdAssetNames;
import com.google.android.gms.ads.mediation.MediationAdRequest;
import com.google.android.gms.ads.mediation.MediationBannerAdapter;
import com.google.android.gms.ads.mediation.MediationBannerListener;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.mediation.MediationInterstitialListener;
import com.google.android.gms.ads.mediation.MediationNativeAdapter;
import com.google.android.gms.ads.mediation.MediationNativeListener;
import com.google.android.gms.ads.mediation.NativeAppInstallAdMapper;
import com.google.android.gms.ads.mediation.NativeMediationAdRequest;
import com.google.android.gms.ads.mediation.UnifiedNativeAdMapper;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

@Keep
public final class FacebookAdapter extends FacebookMediationAdapter implements MediationBannerAdapter, MediationInterstitialAdapter, MediationNativeAdapter {
    public static final String KEY_ID = "id";
    public static final String KEY_SOCIAL_CONTEXT_ASSET = "social_context";
    private static final int MAX_STAR_RATING = 5;
    /* access modifiers changed from: private */
    public AtomicBoolean didInterstitialAdClose = new AtomicBoolean();
    /* access modifiers changed from: private */
    public boolean isNativeBanner;
    private AdView mAdView;
    /* access modifiers changed from: private */
    public MediationBannerListener mBannerListener;
    private InterstitialAd mInterstitialAd;
    /* access modifiers changed from: private */
    public MediationInterstitialListener mInterstitialListener;
    /* access modifiers changed from: private */
    public boolean mIsImpressionRecorded;
    /* access modifiers changed from: private */
    public MediaView mMediaView;
    private NativeAd mNativeAd;
    private NativeBannerAd mNativeBannerAd;
    /* access modifiers changed from: private */
    public MediationNativeListener mNativeListener;
    private RelativeLayout mWrappedAdView;

    private class BannerListener implements AdListener {
        public void onAdClicked(Ad ad) {
            FacebookAdapter.this.mBannerListener.onAdClicked(FacebookAdapter.this);
            FacebookAdapter.this.mBannerListener.onAdOpened(FacebookAdapter.this);
            FacebookAdapter.this.mBannerListener.onAdLeftApplication(FacebookAdapter.this);
        }

        public void onAdLoaded(Ad ad) {
            FacebookAdapter.this.mBannerListener.onAdLoaded(FacebookAdapter.this);
        }

        public void onError(Ad ad, AdError adError) {
            String errorMessage = adError.getErrorMessage();
            if (!TextUtils.isEmpty(errorMessage)) {
                Log.w(FacebookMediationAdapter.TAG, errorMessage);
            }
            MediationBannerListener access$100 = FacebookAdapter.this.mBannerListener;
            FacebookAdapter facebookAdapter = FacebookAdapter.this;
            access$100.onAdFailedToLoad(facebookAdapter, facebookAdapter.convertErrorCode(adError));
        }

        public void onLoggingImpression(Ad ad) {
        }

        private BannerListener() {
        }
    }

    private class FacebookAdapterNativeAdImage extends NativeAd.Image {
        private Drawable mDrawable;
        private Uri mUri;

        public FacebookAdapterNativeAdImage(Uri uri) {
            this.mUri = uri;
        }

        public Drawable getDrawable() {
            return this.mDrawable;
        }

        public double getScale() {
            return 1.0d;
        }

        public Uri getUri() {
            return this.mUri;
        }

        /* access modifiers changed from: protected */
        public void setDrawable(Drawable drawable) {
            this.mDrawable = drawable;
        }
    }

    private class InterstitialListener implements InterstitialAdExtendedListener {
        public void onAdClicked(Ad ad) {
            FacebookAdapter.this.mInterstitialListener.onAdClicked(FacebookAdapter.this);
            FacebookAdapter.this.mInterstitialListener.onAdLeftApplication(FacebookAdapter.this);
        }

        public void onAdLoaded(Ad ad) {
            FacebookAdapter.this.mInterstitialListener.onAdLoaded(FacebookAdapter.this);
        }

        public void onError(Ad ad, AdError adError) {
            String errorMessage = adError.getErrorMessage();
            if (!TextUtils.isEmpty(errorMessage)) {
                Log.w(FacebookMediationAdapter.TAG, errorMessage);
            }
            MediationInterstitialListener access$300 = FacebookAdapter.this.mInterstitialListener;
            FacebookAdapter facebookAdapter = FacebookAdapter.this;
            access$300.onAdFailedToLoad(facebookAdapter, facebookAdapter.convertErrorCode(adError));
        }

        public void onInterstitialActivityDestroyed() {
            if (!FacebookAdapter.this.didInterstitialAdClose.getAndSet(true)) {
                FacebookAdapter.this.mInterstitialListener.onAdClosed(FacebookAdapter.this);
            }
        }

        public void onInterstitialDismissed(Ad ad) {
            if (!FacebookAdapter.this.didInterstitialAdClose.getAndSet(true)) {
                FacebookAdapter.this.mInterstitialListener.onAdClosed(FacebookAdapter.this);
            }
        }

        public void onInterstitialDisplayed(Ad ad) {
            FacebookAdapter.this.mInterstitialListener.onAdOpened(FacebookAdapter.this);
        }

        public void onLoggingImpression(Ad ad) {
        }

        public void onRewardedAdCompleted() {
        }

        public void onRewardedAdServerFailed() {
        }

        public void onRewardedAdServerSucceeded() {
        }

        private InterstitialListener() {
        }
    }

    private interface NativeAdMapperListener {
        void onMappingFailed();

        void onMappingSuccess();
    }

    private class NativeBannerListener implements AdListener, NativeAdListener {
        private WeakReference<Context> mContext;
        private NativeMediationAdRequest mMediationAdRequest;
        private NativeBannerAd mNativeBannerAd;

        public void onAdClicked(Ad ad) {
            FacebookAdapter.this.mNativeListener.onAdClicked(FacebookAdapter.this);
            FacebookAdapter.this.mNativeListener.onAdOpened(FacebookAdapter.this);
            FacebookAdapter.this.mNativeListener.onAdLeftApplication(FacebookAdapter.this);
        }

        public void onAdLoaded(Ad ad) {
            if (ad != this.mNativeBannerAd) {
                Log.w(FacebookMediationAdapter.TAG, "Ad loaded is not a native banner ad.");
                FacebookAdapter.this.mNativeListener.onAdFailedToLoad(FacebookAdapter.this, 0);
                return;
            }
            Context context = this.mContext.get();
            if (context == null) {
                Log.w(FacebookMediationAdapter.TAG, "Failed to create ad options view, Context is null.");
                FacebookAdapter.this.mNativeListener.onAdFailedToLoad(FacebookAdapter.this, 1);
                return;
            }
            NativeAdOptions nativeAdOptions = this.mMediationAdRequest.getNativeAdOptions();
            if (this.mMediationAdRequest.isUnifiedNativeAdRequested()) {
                final UnifiedAdMapper unifiedAdMapper = new UnifiedAdMapper(this.mNativeBannerAd, nativeAdOptions);
                unifiedAdMapper.mapUnifiedNativeAd(context, new NativeAdMapperListener() {
                    public void onMappingFailed() {
                        FacebookAdapter.this.mNativeListener.onAdFailedToLoad(FacebookAdapter.this, 3);
                    }

                    public void onMappingSuccess() {
                        FacebookAdapter.this.mNativeListener.onAdLoaded(FacebookAdapter.this, unifiedAdMapper);
                    }
                });
            } else if (this.mMediationAdRequest.isAppInstallAdRequested()) {
                final AppInstallMapper appInstallMapper = new AppInstallMapper(this.mNativeBannerAd, nativeAdOptions);
                appInstallMapper.mapNativeAd(context, new NativeAdMapperListener() {
                    public void onMappingFailed() {
                        FacebookAdapter.this.mNativeListener.onAdFailedToLoad(FacebookAdapter.this, 3);
                    }

                    public void onMappingSuccess() {
                        FacebookAdapter.this.mNativeListener.onAdLoaded(FacebookAdapter.this, appInstallMapper);
                    }
                });
            } else {
                Log.e(FacebookMediationAdapter.TAG, "Content Ads are not supported.");
                FacebookAdapter.this.mNativeListener.onAdFailedToLoad(FacebookAdapter.this, 1);
            }
        }

        public void onError(Ad ad, AdError adError) {
            String errorMessage = adError.getErrorMessage();
            if (!TextUtils.isEmpty(errorMessage)) {
                Log.w(FacebookMediationAdapter.TAG, errorMessage);
            }
            MediationNativeListener access$500 = FacebookAdapter.this.mNativeListener;
            FacebookAdapter facebookAdapter = FacebookAdapter.this;
            access$500.onAdFailedToLoad(facebookAdapter, facebookAdapter.convertErrorCode(adError));
        }

        public void onLoggingImpression(Ad ad) {
            if (FacebookAdapter.this.mIsImpressionRecorded) {
                Log.d(FacebookMediationAdapter.TAG, "Received onLoggingImpression callback for a native whose impression is already recorded. Ignoring the duplicate callback.");
                return;
            }
            FacebookAdapter.this.mNativeListener.onAdImpression(FacebookAdapter.this);
            boolean unused = FacebookAdapter.this.mIsImpressionRecorded = true;
        }

        public void onMediaDownloaded(Ad ad) {
            Log.d(FacebookMediationAdapter.TAG, "onMediaDownloaded");
        }

        private NativeBannerListener(Context context, NativeBannerAd nativeBannerAd, NativeMediationAdRequest nativeMediationAdRequest) {
            this.mContext = new WeakReference<>(context);
            this.mNativeBannerAd = nativeBannerAd;
            this.mMediationAdRequest = nativeMediationAdRequest;
        }
    }

    private class NativeListener implements AdListener, NativeAdListener {
        private WeakReference<Context> mContext;
        private NativeMediationAdRequest mMediationAdRequest;
        private com.facebook.ads.NativeAd mNativeAd;

        public void onAdClicked(Ad ad) {
            FacebookAdapter.this.mNativeListener.onAdClicked(FacebookAdapter.this);
            FacebookAdapter.this.mNativeListener.onAdOpened(FacebookAdapter.this);
            FacebookAdapter.this.mNativeListener.onAdLeftApplication(FacebookAdapter.this);
        }

        public void onAdLoaded(Ad ad) {
            if (ad != this.mNativeAd) {
                Log.w(FacebookMediationAdapter.TAG, "Ad loaded is not a native ad.");
                FacebookAdapter.this.mNativeListener.onAdFailedToLoad(FacebookAdapter.this, 0);
                return;
            }
            Context context = this.mContext.get();
            if (context == null) {
                Log.w(FacebookMediationAdapter.TAG, "Failed to create ad options view, Context is null.");
                FacebookAdapter.this.mNativeListener.onAdFailedToLoad(FacebookAdapter.this, 1);
                return;
            }
            NativeAdOptions nativeAdOptions = this.mMediationAdRequest.getNativeAdOptions();
            if (this.mMediationAdRequest.isUnifiedNativeAdRequested()) {
                final UnifiedAdMapper unifiedAdMapper = new UnifiedAdMapper(this.mNativeAd, nativeAdOptions);
                unifiedAdMapper.mapUnifiedNativeAd(context, new NativeAdMapperListener() {
                    public void onMappingFailed() {
                        FacebookAdapter.this.mNativeListener.onAdFailedToLoad(FacebookAdapter.this, 3);
                    }

                    public void onMappingSuccess() {
                        FacebookAdapter.this.mNativeListener.onAdLoaded(FacebookAdapter.this, unifiedAdMapper);
                    }
                });
            } else if (this.mMediationAdRequest.isAppInstallAdRequested()) {
                final AppInstallMapper appInstallMapper = new AppInstallMapper(this.mNativeAd, nativeAdOptions);
                appInstallMapper.mapNativeAd(context, new NativeAdMapperListener() {
                    public void onMappingFailed() {
                        FacebookAdapter.this.mNativeListener.onAdFailedToLoad(FacebookAdapter.this, 3);
                    }

                    public void onMappingSuccess() {
                        FacebookAdapter.this.mNativeListener.onAdLoaded(FacebookAdapter.this, appInstallMapper);
                    }
                });
            } else {
                Log.e(FacebookMediationAdapter.TAG, "Content Ads are not supported.");
                FacebookAdapter.this.mNativeListener.onAdFailedToLoad(FacebookAdapter.this, 1);
            }
        }

        public void onError(Ad ad, AdError adError) {
            String errorMessage = adError.getErrorMessage();
            if (!TextUtils.isEmpty(errorMessage)) {
                Log.w(FacebookMediationAdapter.TAG, errorMessage);
            }
            MediationNativeListener access$500 = FacebookAdapter.this.mNativeListener;
            FacebookAdapter facebookAdapter = FacebookAdapter.this;
            access$500.onAdFailedToLoad(facebookAdapter, facebookAdapter.convertErrorCode(adError));
        }

        public void onLoggingImpression(Ad ad) {
            if (FacebookAdapter.this.mIsImpressionRecorded) {
                Log.d(FacebookMediationAdapter.TAG, "Received onLoggingImpression callback for a native whose impression is already recorded. Ignoring the duplicate callback.");
                return;
            }
            FacebookAdapter.this.mNativeListener.onAdImpression(FacebookAdapter.this);
            boolean unused = FacebookAdapter.this.mIsImpressionRecorded = true;
        }

        public void onMediaDownloaded(Ad ad) {
            Log.d(FacebookMediationAdapter.TAG, "onMediaDownloaded");
        }

        private NativeListener(Context context, com.facebook.ads.NativeAd nativeAd, NativeMediationAdRequest nativeMediationAdRequest) {
            this.mContext = new WeakReference<>(context);
            this.mNativeAd = nativeAd;
            this.mMediationAdRequest = nativeMediationAdRequest;
        }
    }

    private void buildAdRequest(MediationAdRequest mediationAdRequest) {
        if (mediationAdRequest != null) {
            boolean z = true;
            if (mediationAdRequest.taggedForChildDirectedTreatment() != 1) {
                z = false;
            }
            AdSettings.setIsChildDirected(z);
        }
    }

    /* access modifiers changed from: private */
    public int convertErrorCode(AdError adError) {
        if (adError == null) {
            return 0;
        }
        int errorCode = adError.getErrorCode();
        if (errorCode == 2000) {
            return 2;
        }
        switch (errorCode) {
            case 1000:
                return 2;
            case 1001:
                return 3;
            case 1002:
                return 1;
            default:
                return 0;
        }
    }

    /* access modifiers changed from: private */
    public void createAndLoadBannerAd(Context context, String str, AdSize adSize, MediationAdRequest mediationAdRequest) {
        this.mAdView = new AdView(context, str, getAdSize(context, adSize));
        buildAdRequest(mediationAdRequest);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(adSize.getWidthInPixels(context), adSize.getHeightInPixels(context));
        this.mWrappedAdView = new RelativeLayout(context);
        this.mAdView.setLayoutParams(layoutParams);
        this.mWrappedAdView.addView(this.mAdView);
        AdView adView = this.mAdView;
        adView.loadAd(adView.buildLoadAdConfig().withAdListener(new BannerListener()).build());
    }

    /* access modifiers changed from: private */
    public void createAndLoadInterstitial(Context context, String str, MediationAdRequest mediationAdRequest) {
        this.mInterstitialAd = new InterstitialAd(context, str);
        buildAdRequest(mediationAdRequest);
        InterstitialAd interstitialAd = this.mInterstitialAd;
        interstitialAd.loadAd(interstitialAd.buildLoadAdConfig().withAdListener(new InterstitialListener()).build());
    }

    /* access modifiers changed from: private */
    public void createAndLoadNativeAd(Context context, String str, NativeMediationAdRequest nativeMediationAdRequest, Bundle bundle) {
        if (bundle != null) {
            this.isNativeBanner = bundle.getBoolean(FacebookExtras.NATIVE_BANNER);
        }
        if (this.isNativeBanner) {
            this.mNativeBannerAd = new NativeBannerAd(context, str);
            buildAdRequest(nativeMediationAdRequest);
            NativeBannerAd nativeBannerAd = this.mNativeBannerAd;
            nativeBannerAd.loadAd(nativeBannerAd.buildLoadAdConfig().withAdListener(new NativeBannerListener(context, this.mNativeBannerAd, nativeMediationAdRequest)).build());
            return;
        }
        this.mMediaView = new MediaView(context);
        this.mNativeAd = new com.facebook.ads.NativeAd(context, str);
        buildAdRequest(nativeMediationAdRequest);
        com.facebook.ads.NativeAd nativeAd = this.mNativeAd;
        nativeAd.loadAd(nativeAd.buildLoadAdConfig().withAdListener(new NativeListener(context, this.mNativeAd, nativeMediationAdRequest)).build());
    }

    public static AdSize findClosestSize(Context context, AdSize adSize, ArrayList<AdSize> arrayList) {
        AdSize adSize2 = null;
        if (!(arrayList == null || adSize == null)) {
            float f2 = context.getResources().getDisplayMetrics().density;
            AdSize adSize3 = new AdSize(Math.round(((float) adSize.getWidthInPixels(context)) / f2), Math.round(((float) adSize.getHeightInPixels(context)) / f2));
            Iterator<AdSize> it = arrayList.iterator();
            while (it.hasNext()) {
                AdSize next = it.next();
                if (isSizeInRange(adSize3, next)) {
                    if (adSize2 != null) {
                        next = getLargerByArea(adSize2, next);
                    }
                    adSize2 = next;
                }
            }
        }
        return adSize2;
    }

    private com.facebook.ads.AdSize getAdSize(Context context, AdSize adSize) {
        int width = adSize.getWidth();
        if (width < 0) {
            width = Math.round(((float) adSize.getWidthInPixels(context)) / context.getResources().getDisplayMetrics().density);
        }
        ArrayList arrayList = new ArrayList(3);
        arrayList.add(0, new AdSize(width, 50));
        arrayList.add(1, new AdSize(width, 90));
        arrayList.add(2, new AdSize(width, 250));
        String str = FacebookMediationAdapter.TAG;
        Log.i(str, "Potential ad sizes: " + arrayList.toString());
        AdSize findClosestSize = findClosestSize(context, adSize, arrayList);
        if (findClosestSize == null) {
            return null;
        }
        String str2 = FacebookMediationAdapter.TAG;
        Log.i(str2, "Found closest ad size: " + findClosestSize.toString());
        int height = findClosestSize.getHeight();
        if (height == com.facebook.ads.AdSize.BANNER_HEIGHT_50.getHeight()) {
            return com.facebook.ads.AdSize.BANNER_HEIGHT_50;
        }
        if (height == com.facebook.ads.AdSize.BANNER_HEIGHT_90.getHeight()) {
            return com.facebook.ads.AdSize.BANNER_HEIGHT_90;
        }
        if (height == com.facebook.ads.AdSize.RECTANGLE_HEIGHT_250.getHeight()) {
            return com.facebook.ads.AdSize.RECTANGLE_HEIGHT_250;
        }
        return null;
    }

    private static AdSize getLargerByArea(AdSize adSize, AdSize adSize2) {
        return adSize.getWidth() * adSize.getHeight() > adSize2.getWidth() * adSize2.getHeight() ? adSize : adSize2;
    }

    private static boolean isSizeInRange(AdSize adSize, AdSize adSize2) {
        if (adSize2 == null) {
            return false;
        }
        int width = adSize.getWidth();
        int width2 = adSize2.getWidth();
        int height = adSize.getHeight();
        int height2 = adSize2.getHeight();
        double d2 = (double) width;
        Double.isNaN(d2);
        if (d2 * 0.5d <= ((double) width2) && width >= width2) {
            double d3 = (double) height;
            Double.isNaN(d3);
            if (d3 * 0.7d > ((double) height2) || height < height2) {
                return false;
            }
            return true;
        }
        return false;
    }

    public View getBannerView() {
        return this.mWrappedAdView;
    }

    public void onDestroy() {
        AdView adView = this.mAdView;
        if (adView != null) {
            adView.destroy();
        }
        InterstitialAd interstitialAd = this.mInterstitialAd;
        if (interstitialAd != null) {
            interstitialAd.destroy();
        }
        com.facebook.ads.NativeAd nativeAd = this.mNativeAd;
        if (nativeAd != null) {
            nativeAd.unregisterView();
            this.mNativeAd.destroy();
        }
        MediaView mediaView = this.mMediaView;
        if (mediaView != null) {
            mediaView.destroy();
        }
        NativeBannerAd nativeBannerAd = this.mNativeBannerAd;
        if (nativeBannerAd != null) {
            nativeBannerAd.unregisterView();
            this.mNativeBannerAd.destroy();
        }
    }

    public void onPause() {
    }

    public void onResume() {
    }

    public void requestBannerAd(Context context, MediationBannerListener mediationBannerListener, Bundle bundle, AdSize adSize, MediationAdRequest mediationAdRequest, Bundle bundle2) {
        this.mBannerListener = mediationBannerListener;
        String placementID = FacebookMediationAdapter.getPlacementID(bundle);
        if (TextUtils.isEmpty(placementID)) {
            Log.e(FacebookMediationAdapter.TAG, "Failed to request ad, placementID is null or empty");
            this.mBannerListener.onAdFailedToLoad(this, 1);
        } else if (adSize == null) {
            Log.w(FacebookMediationAdapter.TAG, "Fail to request banner ad, adSize is null");
            this.mBannerListener.onAdFailedToLoad(this, 1);
        } else if (getAdSize(context, adSize) == null) {
            String str = FacebookMediationAdapter.TAG;
            Log.w(str, "The input ad size " + adSize.toString() + " is not supported at this moment.");
            this.mBannerListener.onAdFailedToLoad(this, 3);
        } else {
            final Context context2 = context;
            final String str2 = placementID;
            final AdSize adSize2 = adSize;
            final MediationAdRequest mediationAdRequest2 = mediationAdRequest;
            FacebookInitializer.getInstance().initialize(context, placementID, new FacebookInitializer.Listener() {
                public void onInitializeError(String str) {
                    String str2 = FacebookMediationAdapter.TAG;
                    Log.w(str2, "Failed to load ad from Facebook: " + str);
                    if (FacebookAdapter.this.mBannerListener != null) {
                        FacebookAdapter.this.mBannerListener.onAdFailedToLoad(FacebookAdapter.this, 0);
                    }
                }

                public void onInitializeSuccess() {
                    FacebookAdapter.this.createAndLoadBannerAd(context2, str2, adSize2, mediationAdRequest2);
                }
            });
        }
    }

    public void requestInterstitialAd(final Context context, MediationInterstitialListener mediationInterstitialListener, Bundle bundle, final MediationAdRequest mediationAdRequest, Bundle bundle2) {
        this.mInterstitialListener = mediationInterstitialListener;
        final String placementID = FacebookMediationAdapter.getPlacementID(bundle);
        if (TextUtils.isEmpty(placementID)) {
            Log.e(FacebookMediationAdapter.TAG, "Failed to request ad, placementID is null or empty");
            this.mInterstitialListener.onAdFailedToLoad(this, 1);
            return;
        }
        FacebookInitializer.getInstance().initialize(context, placementID, new FacebookInitializer.Listener() {
            public void onInitializeError(String str) {
                String str2 = FacebookMediationAdapter.TAG;
                Log.w(str2, "Failed to load ad from Facebook: " + str);
                if (FacebookAdapter.this.mInterstitialListener != null) {
                    FacebookAdapter.this.mInterstitialListener.onAdFailedToLoad(FacebookAdapter.this, 0);
                }
            }

            public void onInitializeSuccess() {
                FacebookAdapter.this.createAndLoadInterstitial(context, placementID, mediationAdRequest);
            }
        });
    }

    public void requestNativeAd(Context context, MediationNativeListener mediationNativeListener, Bundle bundle, NativeMediationAdRequest nativeMediationAdRequest, Bundle bundle2) {
        this.mNativeListener = mediationNativeListener;
        String placementID = FacebookMediationAdapter.getPlacementID(bundle);
        if (TextUtils.isEmpty(placementID)) {
            Log.e(FacebookMediationAdapter.TAG, "Failed to request ad, placementID is null or empty.");
            this.mNativeListener.onAdFailedToLoad(this, 1);
            return;
        }
        boolean z = nativeMediationAdRequest.isAppInstallAdRequested() && nativeMediationAdRequest.isContentAdRequested();
        if (nativeMediationAdRequest.isUnifiedNativeAdRequested() || z) {
            final Context context2 = context;
            final String str = placementID;
            final NativeMediationAdRequest nativeMediationAdRequest2 = nativeMediationAdRequest;
            final Bundle bundle3 = bundle2;
            FacebookInitializer.getInstance().initialize(context, placementID, new FacebookInitializer.Listener() {
                public void onInitializeError(String str) {
                    String str2 = FacebookMediationAdapter.TAG;
                    Log.w(str2, "Failed to load ad from Facebook: " + str);
                    if (FacebookAdapter.this.mNativeListener != null) {
                        FacebookAdapter.this.mNativeListener.onAdFailedToLoad(FacebookAdapter.this, 0);
                    }
                }

                public void onInitializeSuccess() {
                    FacebookAdapter.this.createAndLoadNativeAd(context2, str, nativeMediationAdRequest2, bundle3);
                }
            });
            return;
        }
        Log.w(FacebookMediationAdapter.TAG, "Either unified native ads or both app install and content ads must be requested.");
        this.mNativeListener.onAdFailedToLoad(this, 1);
    }

    public void showInterstitial() {
        if (this.mInterstitialAd.isAdLoaded()) {
            this.mInterstitialAd.show();
        }
    }

    class AppInstallMapper extends NativeAppInstallAdMapper {
        private com.facebook.ads.NativeAd mNativeAd;
        private NativeAdOptions mNativeAdOptions;
        private NativeBannerAd mNativeBannerAd;

        public AppInstallMapper(com.facebook.ads.NativeAd nativeAd, NativeAdOptions nativeAdOptions) {
            this.mNativeAd = nativeAd;
            this.mNativeAdOptions = nativeAdOptions;
        }

        private boolean containsRequiredFieldsForNativeAppInstallAd(com.facebook.ads.NativeAd nativeAd) {
            return (nativeAd.getAdHeadline() == null || nativeAd.getAdCoverImage() == null || nativeAd.getAdBodyText() == null || nativeAd.getAdIcon() == null || nativeAd.getAdCallToAction() == null || FacebookAdapter.this.mMediaView == null) ? false : true;
        }

        private boolean containsRequiredFieldsForNativeBannerAd(NativeBannerAd nativeBannerAd) {
            return (nativeBannerAd.getAdHeadline() == null || nativeBannerAd.getAdBodyText() == null || nativeBannerAd.getAdIcon() == null || nativeBannerAd.getAdCallToAction() == null) ? false : true;
        }

        private Double getRating(NativeAdBase.Rating rating) {
            if (rating == null) {
                return null;
            }
            return Double.valueOf((rating.getValue() * 5.0d) / rating.getScale());
        }

        public void mapNativeAd(Context context, NativeAdMapperListener nativeAdMapperListener) {
            AdOptionsView adOptionsView;
            if (FacebookAdapter.this.isNativeBanner) {
                if (!containsRequiredFieldsForNativeBannerAd(this.mNativeBannerAd)) {
                    Log.w(FacebookMediationAdapter.TAG, "Ad from Facebook doesn't have all assets required for the Native Banner Ad format.");
                    nativeAdMapperListener.onMappingFailed();
                    return;
                }
                setHeadline(this.mNativeBannerAd.getAdHeadline());
                setBody(this.mNativeBannerAd.getAdBodyText());
                setIcon(new FacebookAdapterNativeAdImage(Uri.parse(this.mNativeBannerAd.getAdIcon().toString())));
                setCallToAction(this.mNativeBannerAd.getAdCallToAction());
                Bundle bundle = new Bundle();
                bundle.putCharSequence("id", this.mNativeBannerAd.getId());
                bundle.putCharSequence(FacebookAdapter.KEY_SOCIAL_CONTEXT_ASSET, this.mNativeBannerAd.getAdSocialContext());
                setExtras(bundle);
            } else if (!containsRequiredFieldsForNativeAppInstallAd(this.mNativeAd)) {
                Log.w(FacebookMediationAdapter.TAG, "Ad from Facebook doesn't have all assets required for the app install format.");
                nativeAdMapperListener.onMappingFailed();
                return;
            } else {
                setHeadline(this.mNativeAd.getAdHeadline());
                ArrayList arrayList = new ArrayList();
                arrayList.add(new FacebookAdapterNativeAdImage(Uri.parse(this.mNativeAd.getAdCoverImage().toString())));
                setImages(arrayList);
                setBody(this.mNativeAd.getAdBodyText());
                setIcon(new FacebookAdapterNativeAdImage(Uri.parse(this.mNativeAd.getAdIcon().toString())));
                setCallToAction(this.mNativeAd.getAdCallToAction());
                FacebookAdapter.this.mMediaView.setListener(new MediaViewListener() {
                    public void onComplete(MediaView mediaView) {
                        if (FacebookAdapter.this.mNativeListener != null) {
                            FacebookAdapter.this.mNativeListener.onVideoEnd(FacebookAdapter.this);
                        }
                    }

                    public void onEnterFullscreen(MediaView mediaView) {
                    }

                    public void onExitFullscreen(MediaView mediaView) {
                    }

                    public void onFullscreenBackground(MediaView mediaView) {
                    }

                    public void onFullscreenForeground(MediaView mediaView) {
                    }

                    public void onPause(MediaView mediaView) {
                    }

                    public void onPlay(MediaView mediaView) {
                    }

                    public void onVolumeChange(MediaView mediaView, float f2) {
                    }
                });
                setMediaView(FacebookAdapter.this.mMediaView);
                setHasVideoContent(true);
                Double rating = getRating(this.mNativeAd.getAdStarRating());
                if (rating != null) {
                    setStarRating(rating.doubleValue());
                }
                Bundle bundle2 = new Bundle();
                bundle2.putCharSequence("id", this.mNativeAd.getId());
                bundle2.putCharSequence(FacebookAdapter.KEY_SOCIAL_CONTEXT_ASSET, this.mNativeAd.getAdSocialContext());
                setExtras(bundle2);
            }
            NativeAdLayout nativeAdLayout = new NativeAdLayout(context);
            if (FacebookAdapter.this.isNativeBanner) {
                adOptionsView = new AdOptionsView(context, this.mNativeBannerAd, nativeAdLayout);
            } else {
                adOptionsView = new AdOptionsView(context, this.mNativeAd, nativeAdLayout);
            }
            setAdChoicesContent(adOptionsView);
            nativeAdMapperListener.onMappingSuccess();
        }

        public void trackViews(View view, Map<String, View> map, Map<String, View> map2) {
            setOverrideImpressionRecording(true);
            setOverrideClickHandling(true);
            ArrayList arrayList = new ArrayList();
            ImageView imageView = null;
            for (Map.Entry next : map.entrySet()) {
                arrayList.add(next.getValue());
                if (((String) next.getKey()).equals(NativeAppInstallAd.ASSET_ICON) || ((String) next.getKey()).equals(UnifiedNativeAdAssetNames.ASSET_ICON)) {
                    imageView = (ImageView) next.getValue();
                }
            }
            if (FacebookAdapter.this.isNativeBanner) {
                this.mNativeBannerAd.registerViewForInteraction(view, imageView);
            } else {
                this.mNativeAd.registerViewForInteraction(view, FacebookAdapter.this.mMediaView, imageView, arrayList);
            }
        }

        public void untrackView(View view) {
            super.untrackView(view);
        }

        public AppInstallMapper(NativeBannerAd nativeBannerAd, NativeAdOptions nativeAdOptions) {
            this.mNativeBannerAd = nativeBannerAd;
            this.mNativeAdOptions = nativeAdOptions;
        }
    }

    class UnifiedAdMapper extends UnifiedNativeAdMapper {
        private com.facebook.ads.NativeAd mNativeAd;
        private NativeAdOptions mNativeAdOptions;
        private NativeBannerAd mNativeBannerAd;

        public UnifiedAdMapper(com.facebook.ads.NativeAd nativeAd, NativeAdOptions nativeAdOptions) {
            this.mNativeAd = nativeAd;
            this.mNativeAdOptions = nativeAdOptions;
        }

        private boolean containsRequiredFieldsForNativeBannerAd(NativeBannerAd nativeBannerAd) {
            return (nativeBannerAd.getAdHeadline() == null || nativeBannerAd.getAdBodyText() == null || nativeBannerAd.getAdIcon() == null || nativeBannerAd.getAdCallToAction() == null) ? false : true;
        }

        private boolean containsRequiredFieldsForUnifiedNativeAd(com.facebook.ads.NativeAd nativeAd) {
            return (nativeAd.getAdHeadline() == null || nativeAd.getAdCoverImage() == null || nativeAd.getAdBodyText() == null || nativeAd.getAdIcon() == null || nativeAd.getAdCallToAction() == null || FacebookAdapter.this.mMediaView == null) ? false : true;
        }

        private Double getRating(NativeAdBase.Rating rating) {
            if (rating == null) {
                return null;
            }
            return Double.valueOf((rating.getValue() * 5.0d) / rating.getScale());
        }

        public void mapUnifiedNativeAd(Context context, NativeAdMapperListener nativeAdMapperListener) {
            AdOptionsView adOptionsView;
            if (FacebookAdapter.this.isNativeBanner) {
                if (!containsRequiredFieldsForNativeBannerAd(this.mNativeBannerAd)) {
                    Log.w(FacebookMediationAdapter.TAG, "Ad from Facebook doesn't have all assets required for the Native Banner Ad format.");
                    nativeAdMapperListener.onMappingFailed();
                    return;
                }
                setHeadline(this.mNativeBannerAd.getAdHeadline());
                setBody(this.mNativeBannerAd.getAdBodyText());
                setIcon(new FacebookAdapterNativeAdImage(Uri.parse(this.mNativeBannerAd.getAdIcon().toString())));
                setCallToAction(this.mNativeBannerAd.getAdCallToAction());
                setAdvertiser(this.mNativeBannerAd.getAdvertiserName());
                Bundle bundle = new Bundle();
                bundle.putCharSequence("id", this.mNativeBannerAd.getId());
                bundle.putCharSequence(FacebookAdapter.KEY_SOCIAL_CONTEXT_ASSET, this.mNativeBannerAd.getAdSocialContext());
                setExtras(bundle);
            } else if (!containsRequiredFieldsForUnifiedNativeAd(this.mNativeAd)) {
                Log.w(FacebookMediationAdapter.TAG, "Ad from Facebook doesn't have all assets required for the Native Ad format.");
                nativeAdMapperListener.onMappingFailed();
                return;
            } else {
                setHeadline(this.mNativeAd.getAdHeadline());
                ArrayList arrayList = new ArrayList();
                arrayList.add(new FacebookAdapterNativeAdImage(Uri.parse(this.mNativeAd.getAdCoverImage().toString())));
                setImages(arrayList);
                setBody(this.mNativeAd.getAdBodyText());
                setIcon(new FacebookAdapterNativeAdImage(Uri.parse(this.mNativeAd.getAdIcon().toString())));
                setCallToAction(this.mNativeAd.getAdCallToAction());
                setAdvertiser(this.mNativeAd.getAdvertiserName());
                FacebookAdapter.this.mMediaView.setListener(new MediaViewListener() {
                    public void onComplete(MediaView mediaView) {
                        if (FacebookAdapter.this.mNativeListener != null) {
                            FacebookAdapter.this.mNativeListener.onVideoEnd(FacebookAdapter.this);
                        }
                    }

                    public void onEnterFullscreen(MediaView mediaView) {
                    }

                    public void onExitFullscreen(MediaView mediaView) {
                    }

                    public void onFullscreenBackground(MediaView mediaView) {
                    }

                    public void onFullscreenForeground(MediaView mediaView) {
                    }

                    public void onPause(MediaView mediaView) {
                    }

                    public void onPlay(MediaView mediaView) {
                    }

                    public void onVolumeChange(MediaView mediaView, float f2) {
                    }
                });
                setMediaView(FacebookAdapter.this.mMediaView);
                setHasVideoContent(true);
                Double rating = getRating(this.mNativeAd.getAdStarRating());
                if (rating != null) {
                    setStarRating(rating);
                }
                Bundle bundle2 = new Bundle();
                bundle2.putCharSequence("id", this.mNativeAd.getId());
                bundle2.putCharSequence(FacebookAdapter.KEY_SOCIAL_CONTEXT_ASSET, this.mNativeAd.getAdSocialContext());
                setExtras(bundle2);
            }
            NativeAdLayout nativeAdLayout = new NativeAdLayout(context);
            if (FacebookAdapter.this.isNativeBanner) {
                adOptionsView = new AdOptionsView(context, this.mNativeBannerAd, nativeAdLayout);
            } else {
                adOptionsView = new AdOptionsView(context, this.mNativeAd, nativeAdLayout);
            }
            setAdChoicesContent(adOptionsView);
            nativeAdMapperListener.onMappingSuccess();
        }

        public void trackViews(View view, Map<String, View> map, Map<String, View> map2) {
            setOverrideImpressionRecording(true);
            setOverrideClickHandling(true);
            ArrayList arrayList = new ArrayList();
            ImageView imageView = null;
            for (Map.Entry next : map.entrySet()) {
                arrayList.add(next.getValue());
                if (((String) next.getKey()).equals(NativeAppInstallAd.ASSET_ICON) || ((String) next.getKey()).equals(UnifiedNativeAdAssetNames.ASSET_ICON)) {
                    imageView = (ImageView) next.getValue();
                }
            }
            if (FacebookAdapter.this.isNativeBanner) {
                this.mNativeBannerAd.registerViewForInteraction(view, imageView);
            } else {
                this.mNativeAd.registerViewForInteraction(view, FacebookAdapter.this.mMediaView, imageView, arrayList);
            }
        }

        public void untrackView(View view) {
            super.untrackView(view);
        }

        public UnifiedAdMapper(NativeBannerAd nativeBannerAd, NativeAdOptions nativeAdOptions) {
            this.mNativeBannerAd = nativeBannerAd;
            this.mNativeAdOptions = nativeAdOptions;
        }
    }
}
