package com.google.ads.mediation.chartboost;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.chartboost.sdk.a;
import com.chartboost.sdk.b;
import com.chartboost.sdk.c.a;
import com.chartboost.sdk.d.a;
import java.lang.ref.WeakReference;
import java.util.HashMap;

public final class ChartboostSingleton {
    private static ChartboostSingletonDelegate chartboostSingletonDelegate;
    /* access modifiers changed from: private */
    public static HashMap<String, WeakReference<AbstractChartboostAdapterDelegate>> mInterstitialDelegates = new HashMap<>();
    /* access modifiers changed from: private */
    public static boolean mIsChartboostInitialized;
    /* access modifiers changed from: private */
    public static boolean mIsChartboostInitializing;
    /* access modifiers changed from: private */
    public static HashMap<String, WeakReference<AbstractChartboostAdapterDelegate>> mRewardedDelegates = new HashMap<>();

    private static final class ChartboostSingletonDelegate extends b {
        private ChartboostSingletonDelegate() {
        }

        public void didCacheInterstitial(String str) {
            super.didCacheInterstitial(str);
            WeakReference access$500 = ChartboostSingleton.getInterstitialDelegate(str);
            if (access$500 != null && access$500.get() != null) {
                ((AbstractChartboostAdapterDelegate) access$500.get()).didCacheInterstitial(str);
            }
        }

        public void didCacheRewardedVideo(String str) {
            super.didCacheRewardedVideo(str);
            WeakReference access$600 = ChartboostSingleton.getRewardedDelegate(str);
            if (access$600 != null && access$600.get() != null) {
                ((AbstractChartboostAdapterDelegate) access$600.get()).didCacheRewardedVideo(str);
            }
        }

        public void didClickInterstitial(String str) {
            super.didClickInterstitial(str);
            WeakReference access$500 = ChartboostSingleton.getInterstitialDelegate(str);
            if (access$500 != null && access$500.get() != null) {
                ((AbstractChartboostAdapterDelegate) access$500.get()).didClickInterstitial(str);
            }
        }

        public void didClickRewardedVideo(String str) {
            super.didClickRewardedVideo(str);
            WeakReference access$600 = ChartboostSingleton.getRewardedDelegate(str);
            if (access$600 != null && access$600.get() != null) {
                ((AbstractChartboostAdapterDelegate) access$600.get()).didClickRewardedVideo(str);
            }
        }

        public void didCompleteInterstitial(String str) {
            super.didCompleteInterstitial(str);
        }

        public void didCompleteRewardedVideo(String str, int i2) {
            super.didCompleteRewardedVideo(str, i2);
            WeakReference access$600 = ChartboostSingleton.getRewardedDelegate(str);
            if (access$600 != null && access$600.get() != null) {
                ((AbstractChartboostAdapterDelegate) access$600.get()).didCompleteRewardedVideo(str, i2);
            }
        }

        public void didDismissInterstitial(String str) {
            super.didDismissInterstitial(str);
            WeakReference access$500 = ChartboostSingleton.getInterstitialDelegate(str);
            if (!(access$500 == null || access$500.get() == null)) {
                ((AbstractChartboostAdapterDelegate) access$500.get()).didDismissInterstitial(str);
            }
            ChartboostSingleton.mInterstitialDelegates.remove(str);
        }

        public void didDismissRewardedVideo(String str) {
            super.didDismissRewardedVideo(str);
            WeakReference access$600 = ChartboostSingleton.getRewardedDelegate(str);
            if (!(access$600 == null || access$600.get() == null)) {
                ((AbstractChartboostAdapterDelegate) access$600.get()).didDismissRewardedVideo(str);
            }
            ChartboostSingleton.mRewardedDelegates.remove(str);
        }

        public void didDisplayInterstitial(String str) {
            super.didDisplayInterstitial(str);
            WeakReference access$500 = ChartboostSingleton.getInterstitialDelegate(str);
            if (access$500 != null && access$500.get() != null) {
                ((AbstractChartboostAdapterDelegate) access$500.get()).didDisplayInterstitial(str);
            }
        }

        public void didDisplayRewardedVideo(String str) {
            super.didDisplayRewardedVideo(str);
            WeakReference access$600 = ChartboostSingleton.getRewardedDelegate(str);
            if (access$600 != null && access$600.get() != null) {
                ((AbstractChartboostAdapterDelegate) access$600.get()).didDisplayRewardedVideo(str);
            }
        }

        public void didFailToLoadInterstitial(String str, a.c cVar) {
            super.didFailToLoadInterstitial(str, cVar);
            WeakReference access$500 = ChartboostSingleton.getInterstitialDelegate(str);
            if (!(access$500 == null || access$500.get() == null)) {
                ((AbstractChartboostAdapterDelegate) access$500.get()).didFailToLoadInterstitial(str, cVar);
            }
            ChartboostSingleton.mInterstitialDelegates.remove(str);
        }

        public void didFailToLoadRewardedVideo(String str, a.c cVar) {
            super.didFailToLoadRewardedVideo(str, cVar);
            WeakReference access$600 = ChartboostSingleton.getRewardedDelegate(str);
            if (!(access$600 == null || access$600.get() == null)) {
                ((AbstractChartboostAdapterDelegate) access$600.get()).didFailToLoadRewardedVideo(str, cVar);
            }
            ChartboostSingleton.mRewardedDelegates.remove(str);
        }

        public void didInitialize() {
            super.didInitialize();
            boolean unused = ChartboostSingleton.mIsChartboostInitializing = false;
            boolean unused2 = ChartboostSingleton.mIsChartboostInitialized = true;
            for (WeakReference weakReference : ChartboostSingleton.mInterstitialDelegates.values()) {
                if (weakReference.get() != null) {
                    ((AbstractChartboostAdapterDelegate) weakReference.get()).didInitialize();
                }
            }
            for (WeakReference weakReference2 : ChartboostSingleton.mRewardedDelegates.values()) {
                if (weakReference2.get() != null) {
                    ((AbstractChartboostAdapterDelegate) weakReference2.get()).didInitialize();
                }
            }
        }
    }

    private static void addInterstitialDelegate(String str, AbstractChartboostAdapterDelegate abstractChartboostAdapterDelegate) {
        if (!TextUtils.isEmpty(str) && abstractChartboostAdapterDelegate != null) {
            mInterstitialDelegates.put(str, new WeakReference(abstractChartboostAdapterDelegate));
        }
    }

    private static void addRewardedDelegate(String str, AbstractChartboostAdapterDelegate abstractChartboostAdapterDelegate) {
        if (!TextUtils.isEmpty(str) && abstractChartboostAdapterDelegate != null) {
            mRewardedDelegates.put(str, new WeakReference(abstractChartboostAdapterDelegate));
        }
    }

    private static ChartboostSingletonDelegate getInstance() {
        if (chartboostSingletonDelegate == null) {
            chartboostSingletonDelegate = new ChartboostSingletonDelegate();
        }
        return chartboostSingletonDelegate;
    }

    /* access modifiers changed from: private */
    public static WeakReference<AbstractChartboostAdapterDelegate> getInterstitialDelegate(String str) {
        if (TextUtils.isEmpty(str) || !mInterstitialDelegates.containsKey(str)) {
            return null;
        }
        return mInterstitialDelegates.get(str);
    }

    /* access modifiers changed from: private */
    public static WeakReference<AbstractChartboostAdapterDelegate> getRewardedDelegate(String str) {
        if (TextUtils.isEmpty(str) || !mRewardedDelegates.containsKey(str)) {
            return null;
        }
        return mRewardedDelegates.get(str);
    }

    protected static void loadInterstitialAd(AbstractChartboostAdapterDelegate abstractChartboostAdapterDelegate) {
        String location = abstractChartboostAdapterDelegate.getChartboostParams().getLocation();
        if (com.chartboost.sdk.a.c(location)) {
            abstractChartboostAdapterDelegate.didCacheInterstitial(location);
        } else {
            com.chartboost.sdk.a.a(location);
        }
    }

    protected static void loadRewardedVideoAd(AbstractChartboostAdapterDelegate abstractChartboostAdapterDelegate) {
        String location = abstractChartboostAdapterDelegate.getChartboostParams().getLocation();
        if (com.chartboost.sdk.a.d(location)) {
            abstractChartboostAdapterDelegate.didCacheRewardedVideo(location);
        } else {
            com.chartboost.sdk.a.b(location);
        }
    }

    protected static void showInterstitialAd(AbstractChartboostAdapterDelegate abstractChartboostAdapterDelegate) {
        com.chartboost.sdk.a.e(abstractChartboostAdapterDelegate.getChartboostParams().getLocation());
    }

    protected static void showRewardedVideoAd(AbstractChartboostAdapterDelegate abstractChartboostAdapterDelegate) {
        com.chartboost.sdk.a.f(abstractChartboostAdapterDelegate.getChartboostParams().getLocation());
    }

    private static void startChartboost(Activity activity, ChartboostParams chartboostParams, AbstractChartboostAdapterDelegate abstractChartboostAdapterDelegate) {
        if (!mIsChartboostInitializing) {
            if (chartboostParams.getFramework() != null && !TextUtils.isEmpty(chartboostParams.getFrameworkVersion())) {
                com.chartboost.sdk.a.a(chartboostParams.getFramework(), chartboostParams.getFrameworkVersion());
            }
            if (!mIsChartboostInitialized) {
                mIsChartboostInitializing = true;
                com.chartboost.sdk.a.a(activity, chartboostParams.getAppId(), chartboostParams.getAppSignature());
                com.chartboost.sdk.a.a(a.b.CBMediationAdMob, BuildConfig.VERSION_NAME);
                com.chartboost.sdk.a.a(a.C0126a.INTEGRATION);
                com.chartboost.sdk.a.a(getInstance());
                com.chartboost.sdk.a.a(true);
                com.chartboost.sdk.a.a(activity);
                com.chartboost.sdk.a.e(activity);
                com.chartboost.sdk.a.d(activity);
                return;
            }
            abstractChartboostAdapterDelegate.didInitialize();
        }
    }

    public static void startChartboostInterstitial(Context context, AbstractChartboostAdapterDelegate abstractChartboostAdapterDelegate) {
        String location = abstractChartboostAdapterDelegate.getChartboostParams().getLocation();
        WeakReference<AbstractChartboostAdapterDelegate> interstitialDelegate = getInterstitialDelegate(location);
        if (interstitialDelegate == null || interstitialDelegate.get() == null) {
            addInterstitialDelegate(location, abstractChartboostAdapterDelegate);
            startChartboost((Activity) context, abstractChartboostAdapterDelegate.getChartboostParams(), abstractChartboostAdapterDelegate);
            return;
        }
        String str = ChartboostMediationAdapter.TAG;
        Log.w(str, "An ad has already been requested for the location: " + location);
        abstractChartboostAdapterDelegate.didFailToLoadInterstitial(location, a.c.NO_AD_FOUND);
    }

    public static void startChartboostRewardedVideo(Context context, AbstractChartboostAdapterDelegate abstractChartboostAdapterDelegate) {
        String location = abstractChartboostAdapterDelegate.getChartboostParams().getLocation();
        WeakReference<AbstractChartboostAdapterDelegate> rewardedDelegate = getRewardedDelegate(location);
        if (rewardedDelegate == null || rewardedDelegate.get() == null) {
            addRewardedDelegate(location, abstractChartboostAdapterDelegate);
            startChartboost((Activity) context, abstractChartboostAdapterDelegate.getChartboostParams(), abstractChartboostAdapterDelegate);
            return;
        }
        String str = ChartboostMediationAdapter.TAG;
        Log.w(str, "An ad has already been requested for the location: " + location);
        abstractChartboostAdapterDelegate.didFailToLoadRewardedVideo(location, a.c.NO_AD_FOUND);
    }
}
