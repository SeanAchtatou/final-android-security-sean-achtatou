package com.google.ads.mediation.inmobi;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.ads.mediation.MediationAdLoadCallback;
import com.google.android.gms.ads.mediation.MediationRewardedAd;
import com.google.android.gms.ads.mediation.MediationRewardedAdCallback;
import com.google.android.gms.ads.mediation.MediationRewardedAdConfiguration;
import com.google.android.gms.ads.mediation.rtb.SignalCallbacks;
import com.inmobi.ads.InMobiAdRequestStatus;
import com.inmobi.ads.InMobiInterstitial;
import com.inmobi.ads.listeners.InterstitialAdEventListener;
import com.inmobi.sdk.InMobiSdk;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

public class InMobiRewardedAd implements MediationRewardedAd {
    public static final String TAG = InMobiRewardedAd.class.getName();
    /* access modifiers changed from: private */
    public static HashMap<Long, WeakReference<InMobiRewardedAd>> mPlacementsInUse = new HashMap<>();
    private InMobiInterstitial mInMobiRewardedAd;
    /* access modifiers changed from: private */
    public MediationAdLoadCallback<MediationRewardedAd, MediationRewardedAdCallback> mMediationAdLoadCallback;
    /* access modifiers changed from: private */
    public MediationRewardedAdCallback mRewardedAdCallback;
    /* access modifiers changed from: private */
    public SignalCallbacks mSignalCallbacks;

    public InMobiRewardedAd(Context context, final long j2) {
        this.mInMobiRewardedAd = new InMobiInterstitial(context, j2, new InterstitialAdEventListener() {
            public void onAdClicked(InMobiInterstitial inMobiInterstitial, Map<Object, Object> map) {
                Log.d(InMobiRewardedAd.TAG, "onAdClicked");
                if (InMobiRewardedAd.this.mRewardedAdCallback != null) {
                    InMobiRewardedAd.this.mRewardedAdCallback.reportAdClicked();
                }
            }

            public void onAdDismissed(InMobiInterstitial inMobiInterstitial) {
                Log.d(InMobiRewardedAd.TAG, "onAdDismissed");
                if (InMobiRewardedAd.this.mRewardedAdCallback != null) {
                    InMobiRewardedAd.this.mRewardedAdCallback.onAdClosed();
                }
                InMobiRewardedAd.mPlacementsInUse.remove(Long.valueOf(j2));
            }

            public void onAdDisplayFailed(InMobiInterstitial inMobiInterstitial) {
                Log.d(InMobiRewardedAd.TAG, "onAdDisplayFailed");
                if (InMobiRewardedAd.this.mRewardedAdCallback != null) {
                    InMobiRewardedAd.this.mRewardedAdCallback.onAdFailedToShow("Internal Error.");
                }
                InMobiRewardedAd.mPlacementsInUse.remove(Long.valueOf(j2));
            }

            public void onAdDisplayed(InMobiInterstitial inMobiInterstitial) {
                Log.d(InMobiRewardedAd.TAG, "onAdDisplayed");
                if (InMobiRewardedAd.this.mRewardedAdCallback != null) {
                    InMobiRewardedAd.this.mRewardedAdCallback.onAdOpened();
                    InMobiRewardedAd.this.mRewardedAdCallback.onVideoStart();
                    InMobiRewardedAd.this.mRewardedAdCallback.reportAdImpression();
                }
            }

            public void onAdLoadFailed(InMobiInterstitial inMobiInterstitial, InMobiAdRequestStatus inMobiAdRequestStatus) {
                String str = "Failed to load ad from InMobi: " + inMobiAdRequestStatus.getMessage();
                Log.w(InMobiRewardedAd.TAG, str);
                InMobiRewardedAd.this.mMediationAdLoadCallback.onFailure(str);
                InMobiRewardedAd.mPlacementsInUse.remove(Long.valueOf(j2));
            }

            public void onAdLoadSucceeded(InMobiInterstitial inMobiInterstitial) {
                Log.d(InMobiRewardedAd.TAG, "onAdLoadSucceeded");
                InMobiRewardedAd inMobiRewardedAd = InMobiRewardedAd.this;
                MediationRewardedAdCallback unused = inMobiRewardedAd.mRewardedAdCallback = (MediationRewardedAdCallback) inMobiRewardedAd.mMediationAdLoadCallback.onSuccess(InMobiRewardedAd.this);
            }

            public void onAdReceived(InMobiInterstitial inMobiInterstitial) {
                Log.d(InMobiRewardedAd.TAG, "InMobi Ad server responded with an Ad.");
            }

            public void onAdWillDisplay(InMobiInterstitial inMobiInterstitial) {
                Log.d(InMobiRewardedAd.TAG, "onAdWillDisplay");
            }

            public void onRequestPayloadCreated(byte[] bArr) {
                String str = new String(bArr);
                String str2 = InMobiRewardedAd.TAG;
                Log.d(str2, "onRequestPayloadCreated: " + str);
                if (InMobiRewardedAd.this.mSignalCallbacks != null) {
                    InMobiRewardedAd.this.mSignalCallbacks.onSuccess(str);
                }
            }

            public void onRequestPayloadCreationFailed(InMobiAdRequestStatus inMobiAdRequestStatus) {
                String message = inMobiAdRequestStatus.getMessage();
                String str = InMobiRewardedAd.TAG;
                Log.d(str, "onRequestPayloadCreationFailed: " + message);
                if (InMobiRewardedAd.this.mSignalCallbacks != null) {
                    InMobiRewardedAd.this.mSignalCallbacks.onFailure(message);
                }
            }

            public void onRewardsUnlocked(InMobiInterstitial inMobiInterstitial, Map<Object, Object> map) {
                String str;
                int i2;
                Log.d(InMobiRewardedAd.TAG, "InMobi RewardedVideo onRewardsUnlocked.");
                String str2 = "";
                if (map != null) {
                    str = str2;
                    for (Object obj : map.keySet()) {
                        str2 = obj.toString();
                        str = map.get(str2).toString();
                        if (!TextUtils.isEmpty(str2) && !TextUtils.isEmpty(str)) {
                            break;
                        }
                    }
                } else {
                    str = str2;
                }
                if (!TextUtils.isEmpty(str)) {
                    try {
                        i2 = Integer.parseInt(str);
                    } catch (NumberFormatException unused) {
                        String str3 = InMobiRewardedAd.TAG;
                        Log.w(str3, "Expected an integer reward value. Got " + str + " instead. Using reward value of 1.");
                        i2 = 1;
                    }
                } else {
                    i2 = 0;
                }
                if (InMobiRewardedAd.this.mRewardedAdCallback != null) {
                    InMobiRewardedAd.this.mRewardedAdCallback.onVideoComplete();
                    InMobiRewardedAd.this.mRewardedAdCallback.onUserEarnedReward(new InMobiReward(str2, i2));
                }
            }

            public void onUserLeftApplication(InMobiInterstitial inMobiInterstitial) {
                Log.d(InMobiRewardedAd.TAG, "onUserLeftApplication");
            }
        });
    }

    public void collectSignals(SignalCallbacks signalCallbacks) {
        this.mSignalCallbacks = signalCallbacks;
        this.mInMobiRewardedAd.getSignals();
    }

    public void load(MediationRewardedAdConfiguration mediationRewardedAdConfiguration, MediationAdLoadCallback<MediationRewardedAd, MediationRewardedAdCallback> mediationAdLoadCallback) {
        Context context = mediationRewardedAdConfiguration.getContext();
        this.mMediationAdLoadCallback = mediationAdLoadCallback;
        if (!(context instanceof Activity)) {
            Log.w(TAG, "Failed to load ad from InMobi: InMobi SDK requires an Activity context to load ads.");
            mediationAdLoadCallback.onFailure("Failed to load ad from InMobi: InMobi SDK requires an Activity context to load ads.");
            return;
        }
        Bundle serverParameters = mediationRewardedAdConfiguration.getServerParameters();
        Bundle mediationExtras = mediationRewardedAdConfiguration.getMediationExtras();
        if (!InMobiMediationAdapter.isSdkInitialized.get()) {
            String string = serverParameters.getString("accountid");
            if (TextUtils.isEmpty(string)) {
                Log.w(TAG, "Failed to load ad from InMobi: Missing or Invalid Account ID.");
                this.mMediationAdLoadCallback.onFailure("Failed to load ad from InMobi: Missing or Invalid Account ID.");
                return;
            }
            InMobiSdk.init(context, string, InMobiConsent.getConsentObj());
            InMobiMediationAdapter.isSdkInitialized.set(true);
        }
        String string2 = serverParameters.getString("placementid");
        if (TextUtils.isEmpty(string2)) {
            Log.e(TAG, "Failed to load ad from InMobi: Missing or Invalid Placement ID.");
            this.mMediationAdLoadCallback.onFailure("Failed to load ad from InMobi: Missing or Invalid Placement ID.");
            return;
        }
        try {
            long parseLong = Long.parseLong(string2.trim());
            if (!mPlacementsInUse.containsKey(Long.valueOf(parseLong)) || mPlacementsInUse.get(Long.valueOf(parseLong)).get() == null) {
                this.mInMobiRewardedAd.setExtras(InMobiAdapterUtils.createInMobiParameterMap(mediationRewardedAdConfiguration));
                InMobiAdapterUtils.setGlobalTargeting(mediationRewardedAdConfiguration, mediationExtras);
                String bidResponse = mediationRewardedAdConfiguration.getBidResponse();
                if (TextUtils.isEmpty(bidResponse)) {
                    this.mInMobiRewardedAd.load();
                } else {
                    this.mInMobiRewardedAd.load(bidResponse.getBytes());
                }
            } else {
                String str = "Failed to load ad from InMobi: An ad has already been requested for placement ID: " + parseLong;
                Log.w(TAG, str);
                this.mMediationAdLoadCallback.onFailure(str);
            }
        } catch (NumberFormatException e2) {
            Log.w(TAG, "Failed to load ad from InMobi: Invalid Placement ID.", e2);
            this.mMediationAdLoadCallback.onFailure("Failed to load ad from InMobi: Invalid Placement ID.");
        }
    }

    public void showAd(Context context) {
        if (this.mInMobiRewardedAd.isReady()) {
            this.mInMobiRewardedAd.show();
        }
    }
}
