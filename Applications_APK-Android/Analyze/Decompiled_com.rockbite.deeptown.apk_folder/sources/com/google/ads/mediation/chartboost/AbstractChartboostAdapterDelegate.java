package com.google.ads.mediation.chartboost;

import com.chartboost.sdk.b;

public abstract class AbstractChartboostAdapterDelegate extends b {
    public abstract ChartboostParams getChartboostParams();
}
