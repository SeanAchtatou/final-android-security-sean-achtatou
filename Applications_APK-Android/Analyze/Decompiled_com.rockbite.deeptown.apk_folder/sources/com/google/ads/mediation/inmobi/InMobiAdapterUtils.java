package com.google.ads.mediation.inmobi;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import com.facebook.appevents.AppEventsConstants;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.mediation.MediationAdConfiguration;
import com.google.android.gms.ads.mediation.MediationAdRequest;
import com.google.android.gms.ads.mediation.MediationRewardedAdConfiguration;
import com.inmobi.sdk.InMobiSdk;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Objects;

class InMobiAdapterUtils {
    static final String KEY_ACCOUNT_ID = "accountid";
    static final String KEY_PLACEMENT_ID = "placementid";

    InMobiAdapterUtils() {
    }

    private static void configureGlobalTargeting(Bundle bundle) {
        InMobiSdk.Education education;
        InMobiSdk.AgeGroup ageGroup;
        if (bundle == null) {
            Log.d("InMobiAdapter", "Bundle extras are null");
            bundle = new Bundle();
        }
        String str = "";
        String str2 = str;
        String str3 = str2;
        for (String next : bundle.keySet()) {
            String string = bundle.getString(next);
            if (next.equals(InMobiNetworkKeys.AREA_CODE)) {
                if (!"".equals(string)) {
                    InMobiSdk.setAreaCode(string);
                }
            } else if (next.equals(InMobiNetworkKeys.AGE)) {
                try {
                    if (!"".equals(string)) {
                        InMobiSdk.setAge(Integer.parseInt(string));
                    }
                } catch (NumberFormatException e2) {
                    Log.d("Please Set age properly", e2.getMessage());
                }
            } else if (next.equals(InMobiNetworkKeys.POSTAL_CODE)) {
                if (!"".equals(string)) {
                    InMobiSdk.setPostalCode(string);
                }
            } else if (next.equals(InMobiNetworkKeys.LANGUAGE)) {
                if (!"".equals(string)) {
                    InMobiSdk.setLanguage(string);
                }
            } else if (next.equals(InMobiNetworkKeys.CITY)) {
                str = string;
            } else if (next.equals(InMobiNetworkKeys.STATE)) {
                str2 = string;
            } else if (next.equals(InMobiNetworkKeys.COUNTRY)) {
                str3 = string;
            } else if (next.equals(InMobiNetworkKeys.AGE_GROUP)) {
                if (!(string == null || (ageGroup = getAgeGroup(string)) == null)) {
                    InMobiSdk.setAgeGroup(ageGroup);
                }
            } else if (next.equals(InMobiNetworkKeys.EDUCATION)) {
                if (!(string == null || (education = getEducation(string)) == null)) {
                    InMobiSdk.setEducation(education);
                }
            } else if (next.equals(InMobiNetworkKeys.LOGLEVEL)) {
                if (string != null) {
                    InMobiSdk.setLogLevel(getLogLevel(string));
                } else {
                    InMobiSdk.setLogLevel(InMobiSdk.LogLevel.NONE);
                }
            } else if (next.equals(InMobiNetworkKeys.INTERESTS)) {
                InMobiSdk.setInterests(string);
            }
        }
        if (Build.VERSION.SDK_INT >= 19 && !Objects.equals(str, "") && !Objects.equals(str2, "") && !Objects.equals(str3, "")) {
            InMobiSdk.setLocationWithCityStateCountry(str, str2, str3);
        }
    }

    static HashMap<String, String> createInMobiParameterMap(MediationAdRequest mediationAdRequest) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("tp", "c_admob");
        if (mediationAdRequest.taggedForChildDirectedTreatment() == 1) {
            hashMap.put("coppa", "1");
        } else {
            hashMap.put("coppa", AppEventsConstants.EVENT_PARAM_VALUE_NO);
        }
        return hashMap;
    }

    public static AdSize findClosestSize(Context context, AdSize adSize, ArrayList<AdSize> arrayList) {
        AdSize adSize2 = null;
        if (!(arrayList == null || adSize == null)) {
            float f2 = context.getResources().getDisplayMetrics().density;
            AdSize adSize3 = new AdSize(Math.round(((float) adSize.getWidthInPixels(context)) / f2), Math.round(((float) adSize.getHeightInPixels(context)) / f2));
            Iterator<AdSize> it = arrayList.iterator();
            while (it.hasNext()) {
                AdSize next = it.next();
                if (isSizeInRange(adSize3, next)) {
                    if (adSize2 != null) {
                        next = getLargerByArea(adSize2, next);
                    }
                    adSize2 = next;
                }
            }
        }
        return adSize2;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private static InMobiSdk.AgeGroup getAgeGroup(String str) {
        char c2;
        switch (str.hashCode()) {
            case -2144603857:
                if (str.equals(InMobiNetworkValues.BETWEEN_55_AND_65)) {
                    c2 = 7;
                    break;
                }
                c2 = 65535;
                break;
            case -1892470079:
                if (str.equals(InMobiNetworkValues.ABOVE_65)) {
                    c2 = 0;
                    break;
                }
                c2 = 65535;
                break;
            case -1873932011:
                if (str.equals(InMobiNetworkValues.BELOW_18)) {
                    c2 = 1;
                    break;
                }
                c2 = 65535;
                break;
            case -1017207884:
                if (str.equals(InMobiNetworkValues.BETWEEN_25_AND_29)) {
                    c2 = 3;
                    break;
                }
                c2 = 65535;
                break;
            case -337149426:
                if (str.equals(InMobiNetworkValues.BETWEEN_45_AND_54)) {
                    c2 = 6;
                    break;
                }
                c2 = 65535;
                break;
            case 1346187892:
                if (str.equals(InMobiNetworkValues.BETWEEN_30_AND_34)) {
                    c2 = 4;
                    break;
                }
                c2 = 65535;
                break;
            case 1470305006:
                if (str.equals(InMobiNetworkValues.BETWEEN_35_AND_44)) {
                    c2 = 5;
                    break;
                }
                c2 = 65535;
                break;
            case 1723710283:
                if (str.equals(InMobiNetworkValues.BETWEEN_18_AND_24)) {
                    c2 = 2;
                    break;
                }
                c2 = 65535;
                break;
            default:
                c2 = 65535;
                break;
        }
        switch (c2) {
            case 0:
                return InMobiSdk.AgeGroup.ABOVE_65;
            case 1:
                return InMobiSdk.AgeGroup.BELOW_18;
            case 2:
                return InMobiSdk.AgeGroup.BETWEEN_18_AND_24;
            case 3:
                return InMobiSdk.AgeGroup.BETWEEN_25_AND_29;
            case 4:
                return InMobiSdk.AgeGroup.BETWEEN_30_AND_34;
            case 5:
                return InMobiSdk.AgeGroup.BETWEEN_35_AND_44;
            case 6:
                return InMobiSdk.AgeGroup.BETWEEN_45_AND_54;
            case 7:
                return InMobiSdk.AgeGroup.BETWEEN_55_AND_65;
            default:
                return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0043  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.inmobi.sdk.InMobiSdk.Education getEducation(java.lang.String r4) {
        /*
            int r0 = r4.hashCode()
            r1 = -2023680018(0xffffffff876117ee, float:-1.6934151E-34)
            r2 = 2
            r3 = 1
            if (r0 == r1) goto L_0x002a
            r1 = 1302797304(0x4da71bf8, float:3.50453504E8)
            if (r0 == r1) goto L_0x0020
            r1 = 1522352361(0x5abd40e9, float:2.663507E16)
            if (r0 == r1) goto L_0x0016
            goto L_0x0034
        L_0x0016:
            java.lang.String r0 = "EDUCATION_HIGHSCHOOLORLESS"
            boolean r4 = r4.equals(r0)
            if (r4 == 0) goto L_0x0034
            r4 = 1
            goto L_0x0035
        L_0x0020:
            java.lang.String r0 = "EDUCATION_POSTGRADUATEORABOVE"
            boolean r4 = r4.equals(r0)
            if (r4 == 0) goto L_0x0034
            r4 = 2
            goto L_0x0035
        L_0x002a:
            java.lang.String r0 = "EDUCATION_COLLEGEORGRADUATE"
            boolean r4 = r4.equals(r0)
            if (r4 == 0) goto L_0x0034
            r4 = 0
            goto L_0x0035
        L_0x0034:
            r4 = -1
        L_0x0035:
            if (r4 == 0) goto L_0x0043
            if (r4 == r3) goto L_0x0040
            if (r4 == r2) goto L_0x003d
            r4 = 0
            return r4
        L_0x003d:
            com.inmobi.sdk.InMobiSdk$Education r4 = com.inmobi.sdk.InMobiSdk.Education.POST_GRADUATE_OR_ABOVE
            return r4
        L_0x0040:
            com.inmobi.sdk.InMobiSdk$Education r4 = com.inmobi.sdk.InMobiSdk.Education.HIGH_SCHOOL_OR_LESS
            return r4
        L_0x0043:
            com.inmobi.sdk.InMobiSdk$Education r4 = com.inmobi.sdk.InMobiSdk.Education.COLLEGE_OR_GRADUATE
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.mediation.inmobi.InMobiAdapterUtils.getEducation(java.lang.String):com.inmobi.sdk.InMobiSdk$Education");
    }

    private static AdSize getLargerByArea(AdSize adSize, AdSize adSize2) {
        return adSize.getWidth() * adSize.getHeight() > adSize2.getWidth() * adSize2.getHeight() ? adSize : adSize2;
    }

    private static InMobiSdk.LogLevel getLogLevel(String str) {
        if (str.equals(InMobiNetworkValues.LOGLEVEL_DEBUG)) {
            return InMobiSdk.LogLevel.DEBUG;
        }
        if (str.equals(InMobiNetworkValues.LOGLEVEL_ERROR)) {
            return InMobiSdk.LogLevel.ERROR;
        }
        if (str.equals(InMobiNetworkValues.LOGLEVEL_NONE)) {
            return InMobiSdk.LogLevel.NONE;
        }
        return InMobiSdk.LogLevel.NONE;
    }

    private static boolean isSizeInRange(AdSize adSize, AdSize adSize2) {
        if (adSize2 == null) {
            return false;
        }
        int width = adSize.getWidth();
        int width2 = adSize2.getWidth();
        int height = adSize.getHeight();
        int height2 = adSize2.getHeight();
        double d2 = (double) width;
        Double.isNaN(d2);
        if (d2 * 0.5d <= ((double) width2) && width >= width2) {
            double d3 = (double) height;
            Double.isNaN(d3);
            if (d3 * 0.7d > ((double) height2) || height < height2) {
                return false;
            }
            return true;
        }
        return false;
    }

    static <T> T mandatoryChecking(T t, String str) throws MandatoryParamException {
        if (t != null && !t.toString().isEmpty()) {
            return t;
        }
        throw new MandatoryParamException("Mandatory param " + str + " not present");
    }

    static void setGlobalTargeting(MediationAdRequest mediationAdRequest, Bundle bundle) {
        configureGlobalTargeting(bundle);
        if (mediationAdRequest.getLocation() != null) {
            InMobiSdk.setLocation(mediationAdRequest.getLocation());
        }
        if (mediationAdRequest.getBirthday() != null) {
            Calendar instance = Calendar.getInstance();
            instance.setTime(mediationAdRequest.getBirthday());
            InMobiSdk.setYearOfBirth(instance.get(1));
        }
        if (mediationAdRequest.getGender() != -1) {
            int gender = mediationAdRequest.getGender();
            if (gender == 1) {
                InMobiSdk.setGender(InMobiSdk.Gender.MALE);
            } else if (gender == 2) {
                InMobiSdk.setGender(InMobiSdk.Gender.FEMALE);
            }
        }
    }

    static HashMap<String, String> createInMobiParameterMap(MediationAdConfiguration mediationAdConfiguration) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("tp", "c_admob");
        if (mediationAdConfiguration.taggedForChildDirectedTreatment() == 1) {
            hashMap.put("coppa", "1");
        } else {
            hashMap.put("coppa", AppEventsConstants.EVENT_PARAM_VALUE_NO);
        }
        return hashMap;
    }

    static void setGlobalTargeting(MediationRewardedAdConfiguration mediationRewardedAdConfiguration, Bundle bundle) {
        configureGlobalTargeting(bundle);
        if (mediationRewardedAdConfiguration.getLocation() != null) {
            InMobiSdk.setLocation(mediationRewardedAdConfiguration.getLocation());
        }
    }
}
