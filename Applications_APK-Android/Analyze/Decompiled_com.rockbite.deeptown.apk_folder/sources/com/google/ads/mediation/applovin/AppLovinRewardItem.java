package com.google.ads.mediation.applovin;

import com.google.android.gms.ads.rewarded.RewardItem;

public final class AppLovinRewardItem implements RewardItem {
    private final int mAmount;
    private final String mType;

    public AppLovinRewardItem(int i2, String str) {
        this.mAmount = i2;
        this.mType = str;
    }

    public int getAmount() {
        return this.mAmount;
    }

    public String getType() {
        return this.mType;
    }
}
