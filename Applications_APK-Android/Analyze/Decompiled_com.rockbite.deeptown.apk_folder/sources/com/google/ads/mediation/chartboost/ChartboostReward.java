package com.google.ads.mediation.chartboost;

import com.google.android.gms.ads.rewarded.RewardItem;

public class ChartboostReward implements RewardItem {
    private final int mAmount;

    public ChartboostReward(int i2) {
        this.mAmount = i2;
    }

    public int getAmount() {
        return this.mAmount;
    }

    public String getType() {
        return "";
    }
}
