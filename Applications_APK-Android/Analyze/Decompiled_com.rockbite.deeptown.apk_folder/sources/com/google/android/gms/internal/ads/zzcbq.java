package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzi;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzcbq implements zzi {
    private final /* synthetic */ zzcbn zzfri;

    zzcbq(zzcbn zzcbn) {
        this.zzfri = zzcbn;
    }

    public final void zzjv() {
        this.zzfri.zzfre.onPause();
    }

    public final void zzjw() {
        this.zzfri.zzfre.onResume();
    }
}
