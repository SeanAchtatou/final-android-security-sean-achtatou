package com.google.android.gms.internal.ads;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcku implements zzckr<zzbwk> {
    private final zzdhd zzfov;
    private final zzbvm zzfzt;
    private final zzbyq zzfzu;

    public zzcku(zzbvm zzbvm, zzdhd zzdhd, zzbyq zzbyq) {
        this.zzfzt = zzbvm;
        this.zzfov = zzdhd;
        this.zzfzu = zzbyq;
    }

    public final boolean zza(zzczt zzczt, zzczl zzczl) {
        zzczp zzczp = zzczl.zzglo;
        return (zzczp == null || zzczp.zzfka == null) ? false : true;
    }

    public final zzdhe<List<zzdhe<zzbwk>>> zzb(zzczt zzczt, zzczl zzczl) {
        zzdhe<zzcaj> zzaou = this.zzfzt.zzade().zzaou();
        this.zzfzt.zzade().zzb(zzaou);
        return zzdgs.zzb(zzdgs.zzb(zzaou, new zzckt(this, zzczl), this.zzfov), new zzckw(this, zzczt, zzczl), this.zzfov);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzbwk zza(zzdhe zzdhe, zzdhe zzdhe2, zzczt zzczt, zzczl zzczl, JSONObject jSONObject) throws Exception {
        zzbws zzbws = (zzbws) zzdhe.get();
        zzcaj zzcaj = (zzcaj) zzdhe2.get();
        zzbwu zza = this.zzfzt.zza(new zzbmt(zzczt, zzczl, null), new zzbxe(zzbws), new zzbvy(jSONObject, zzcaj));
        zza.zzado().zzakr();
        zza.zzadp().zzb(zzcaj);
        zza.zzadq().zzl(zzbws.zzajf());
        return zza.zzadn();
    }

    private final zzdhe<zzbwk> zzb(zzczt zzczt, zzczl zzczl, JSONObject jSONObject) {
        zzdhe<zzcaj> zzaou = this.zzfzt.zzade().zzaou();
        zzdhe<zzbws> zza = this.zzfzu.zza(zzczt, zzczl, jSONObject);
        return zzdgs.zzb(zzaou, zza).zza(new zzckx(this, zza, zzaou, zzczt, zzczl, jSONObject), this.zzfov);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzdhe zza(zzczt zzczt, zzczl zzczl, JSONArray jSONArray) throws Exception {
        if (jSONArray.length() == 0) {
            return zzdgs.zzk(new zzcfb(3));
        }
        if (zzczt.zzgmh.zzfgl.zzgdu <= 1) {
            return zzdgs.zzb(zzb(zzczt, zzczl, jSONArray.getJSONObject(0)), zzcky.zzdoq, this.zzfov);
        }
        int length = jSONArray.length();
        this.zzfzt.zzade().zzdm(Math.min(length, zzczt.zzgmh.zzfgl.zzgdu));
        ArrayList arrayList = new ArrayList(zzczt.zzgmh.zzfgl.zzgdu);
        for (int i2 = 0; i2 < zzczt.zzgmh.zzfgl.zzgdu; i2++) {
            if (i2 < length) {
                arrayList.add(zzb(zzczt, zzczl, jSONArray.getJSONObject(i2)));
            } else {
                arrayList.add(zzdgs.zzk(new zzcfb(3)));
            }
        }
        return zzdgs.zzaj(arrayList);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzaxs.zza(java.lang.String, java.lang.Object):org.json.JSONObject
     arg types: [java.lang.String, boolean]
     candidates:
      com.google.android.gms.internal.ads.zzaxs.zza(org.json.JSONArray, java.util.List<java.lang.String>):java.util.List<java.lang.String>
      com.google.android.gms.internal.ads.zzaxs.zza(org.json.JSONObject, java.lang.String[]):org.json.JSONObject
      com.google.android.gms.internal.ads.zzaxs.zza(android.util.JsonWriter, java.lang.Object):void
      com.google.android.gms.internal.ads.zzaxs.zza(android.util.JsonWriter, org.json.JSONArray):void
      com.google.android.gms.internal.ads.zzaxs.zza(android.util.JsonWriter, org.json.JSONObject):void
      com.google.android.gms.internal.ads.zzaxs.zza(java.lang.String, java.lang.Object):org.json.JSONObject */
    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzdhe zza(zzczl zzczl, zzcaj zzcaj) throws Exception {
        JSONObject zza = zzaxs.zza("isNonagon", (Object) true);
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("response", zzczl.zzglo.zzfka);
        jSONObject.put("sdk_params", zza);
        return zzdgs.zzb(zzcaj.zzc("google.afma.nativeAds.preProcessJson", jSONObject), zzckv.zzbkw, this.zzfov);
    }
}
