package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzavh implements Callable {
    private final zzave zzdqx;

    zzavh(zzave zzave) {
        this.zzdqx = zzave;
    }

    public final Object call() {
        return this.zzdqx.zzvi();
    }
}
