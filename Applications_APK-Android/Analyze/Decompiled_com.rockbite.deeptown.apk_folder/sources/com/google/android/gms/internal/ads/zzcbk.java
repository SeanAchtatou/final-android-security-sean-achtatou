package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcbk implements zzdxg<zzcio<zzcbb>> {
    private final zzdxp<zzcna<zzcbb, zzdac, zzcjx>> zzfcx;
    private final zzdxp<zzczu> zzfep;
    private final zzdxp<zzcna<zzcbb, zzdac, zzcjy>> zzfrb;

    public zzcbk(zzdxp<zzcna<zzcbb, zzdac, zzcjx>> zzdxp, zzdxp<zzcna<zzcbb, zzdac, zzcjy>> zzdxp2, zzdxp<zzczu> zzdxp3) {
        this.zzfcx = zzdxp;
        this.zzfrb = zzdxp2;
        this.zzfep = zzdxp3;
    }

    public final /* synthetic */ Object get() {
        zzcio zzcio;
        zzdxp<zzcna<zzcbb, zzdac, zzcjx>> zzdxp = this.zzfcx;
        zzdxp<zzcna<zzcbb, zzdac, zzcjy>> zzdxp2 = this.zzfrb;
        if (this.zzfep.get().zzgms.contains("new_rewarded")) {
            zzcio = zzdxp2.get();
        } else {
            zzcio = zzdxp.get();
        }
        return (zzcio) zzdxm.zza(zzcio, "Cannot return null from a non-@Nullable @Provides method");
    }
}
