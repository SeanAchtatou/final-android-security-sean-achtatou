package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcgv implements Callable {
    private final zzdhe zzfpa;
    private final zzdhe zzfpn;

    zzcgv(zzdhe zzdhe, zzdhe zzdhe2) {
        this.zzfpn = zzdhe;
        this.zzfpa = zzdhe2;
    }

    public final Object call() {
        return new zzcho((JSONObject) this.zzfpn.get(), (zzaqq) this.zzfpa.get());
    }
}
