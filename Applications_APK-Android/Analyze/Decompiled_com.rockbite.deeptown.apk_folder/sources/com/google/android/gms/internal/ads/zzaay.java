package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzaay {
    public static zzaan<Boolean> zzctf = zzaan.zzf("gad:force_dynamite_loading_enabled", false);
    public static zzaan<Boolean> zzctg = zzaan.zzf("gads:uri_query_to_map_rewrite:enabled", false);
    public static zzaan<Boolean> zzcth = zzaan.zzf("gads:sdk_csi_write_to_file", false);
}
