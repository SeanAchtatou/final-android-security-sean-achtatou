package com.google.android.gms.internal.ads;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzas {
    public static zzd zzb(zzo zzo) {
        boolean z;
        long j2;
        long j3;
        long j4;
        long j5;
        zzo zzo2 = zzo;
        long currentTimeMillis = System.currentTimeMillis();
        Map<String, String> map = zzo2.zzab;
        String str = map.get("Date");
        long zzf = str != null ? zzf(str) : 0;
        String str2 = map.get("Cache-Control");
        int i2 = 0;
        if (str2 != null) {
            String[] split = str2.split(",", 0);
            j3 = 0;
            int i3 = 0;
            j2 = 0;
            while (i2 < split.length) {
                String trim = split[i2].trim();
                if (trim.equals("no-cache") || trim.equals("no-store")) {
                    return null;
                }
                if (trim.startsWith("max-age=")) {
                    try {
                        j3 = Long.parseLong(trim.substring(8));
                    } catch (Exception unused) {
                    }
                } else if (trim.startsWith("stale-while-revalidate=")) {
                    j2 = Long.parseLong(trim.substring(23));
                } else if (trim.equals("must-revalidate") || trim.equals("proxy-revalidate")) {
                    i3 = 1;
                }
                i2++;
            }
            i2 = i3;
            z = true;
        } else {
            j3 = 0;
            j2 = 0;
            z = false;
        }
        String str3 = map.get("Expires");
        long zzf2 = str3 != null ? zzf(str3) : 0;
        String str4 = map.get("Last-Modified");
        long zzf3 = str4 != null ? zzf(str4) : 0;
        String str5 = map.get("ETag");
        if (z) {
            j5 = currentTimeMillis + (j3 * 1000);
            if (i2 == 0) {
                Long.signum(j2);
                j4 = (j2 * 1000) + j5;
                zzd zzd = new zzd();
                zzd.data = zzo2.data;
                zzd.zzg = str5;
                zzd.zzk = j5;
                zzd.zzj = j4;
                zzd.zzh = zzf;
                zzd.zzi = zzf3;
                zzd.zzl = map;
                zzd.zzm = zzo2.allHeaders;
                return zzd;
            }
        } else if (zzf <= 0 || zzf2 < zzf) {
            j5 = 0;
        } else {
            j4 = (zzf2 - zzf) + currentTimeMillis;
            j5 = j4;
            zzd zzd2 = new zzd();
            zzd2.data = zzo2.data;
            zzd2.zzg = str5;
            zzd2.zzk = j5;
            zzd2.zzj = j4;
            zzd2.zzh = zzf;
            zzd2.zzi = zzf3;
            zzd2.zzl = map;
            zzd2.zzm = zzo2.allHeaders;
            return zzd2;
        }
        j4 = j5;
        zzd zzd22 = new zzd();
        zzd22.data = zzo2.data;
        zzd22.zzg = str5;
        zzd22.zzk = j5;
        zzd22.zzj = j4;
        zzd22.zzh = zzf;
        zzd22.zzi = zzf3;
        zzd22.zzl = map;
        zzd22.zzm = zzo2.allHeaders;
        return zzd22;
    }

    private static long zzf(String str) {
        try {
            return zzq().parse(str).getTime();
        } catch (ParseException e2) {
            zzag.zza(e2, "Unable to parse dateStr: %s, falling back to 0", str);
            return 0;
        }
    }

    private static SimpleDateFormat zzq() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return simpleDateFormat;
    }

    static String zzb(long j2) {
        return zzq().format(new Date(j2));
    }
}
