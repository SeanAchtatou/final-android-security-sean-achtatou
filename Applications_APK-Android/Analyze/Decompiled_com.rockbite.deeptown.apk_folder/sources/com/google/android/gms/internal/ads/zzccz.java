package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzccz implements zzdxg<zzccw> {
    private final zzdxp<zzcxw> zzemf;
    private final zzdxp<zzsm> zzfsb;

    private zzccz(zzdxp<zzsm> zzdxp, zzdxp<zzcxw> zzdxp2) {
        this.zzfsb = zzdxp;
        this.zzemf = zzdxp2;
    }

    public static zzccz zzt(zzdxp<zzsm> zzdxp, zzdxp<zzcxw> zzdxp2) {
        return new zzccz(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return new zzccw(this.zzfsb.get(), this.zzemf.get());
    }
}
