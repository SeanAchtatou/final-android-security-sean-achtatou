package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzaua implements zzaui {
    static final zzaui zzdpr = new zzaua();

    private zzaua() {
    }

    public final Object zzb(zzbfq zzbfq) {
        String currentScreenName = zzbfq.getCurrentScreenName();
        if (currentScreenName != null) {
            return currentScreenName;
        }
        String currentScreenClass = zzbfq.getCurrentScreenClass();
        return currentScreenClass != null ? currentScreenClass : "";
    }
}
