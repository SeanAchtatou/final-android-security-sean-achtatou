package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import com.google.android.gms.common.util.Clock;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzclp {
    /* access modifiers changed from: private */
    public final Clock zzbmq;
    private final List<String> zzgad = Collections.synchronizedList(new ArrayList());

    public zzclp(Clock clock) {
        this.zzbmq = clock;
    }

    public final <T> zzdhe<T> zza(zzczl zzczl, zzdhe<T> zzdhe) {
        long elapsedRealtime = this.zzbmq.elapsedRealtime();
        String str = zzczl.zzdcm;
        if (str != null) {
            zzdgs.zza(zzdhe, new zzcls(this, str, elapsedRealtime), zzazd.zzdwj);
        }
        return zzdhe;
    }

    public final String zzamh() {
        return TextUtils.join("_", this.zzgad);
    }

    /* access modifiers changed from: private */
    public final void zza(String str, int i2, long j2) {
        List<String> list = this.zzgad;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 33);
        sb.append(str);
        sb.append(".");
        sb.append(i2);
        sb.append(".");
        sb.append(j2);
        list.add(sb.toString());
    }
}
