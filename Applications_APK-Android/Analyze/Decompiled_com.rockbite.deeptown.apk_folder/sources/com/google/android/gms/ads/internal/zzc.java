package com.google.android.gms.ads.internal;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.internal.ads.zzapz;
import com.google.android.gms.internal.ads.zzato;
import com.google.android.gms.internal.ads.zzawb;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzc {
    private boolean zzbks;
    private zzato zzbkt;
    private zzapz zzbku = null;
    private final Context zzup;

    public zzc(Context context, zzato zzato, zzapz zzapz) {
        this.zzup = context;
        this.zzbkt = zzato;
        if (this.zzbku == null) {
            this.zzbku = new zzapz();
        }
    }

    private final boolean zzjp() {
        zzato zzato = this.zzbkt;
        return (zzato != null && zzato.zzuk().zzdox) || this.zzbku.zzdln;
    }

    public final void recordClick() {
        this.zzbks = true;
    }

    public final void zzbq(String str) {
        List<String> list;
        if (zzjp()) {
            if (str == null) {
                str = "";
            }
            zzato zzato = this.zzbkt;
            if (zzato != null) {
                zzato.zza(str, null, 3);
                return;
            }
            zzapz zzapz = this.zzbku;
            if (zzapz.zzdln && (list = zzapz.zzdlo) != null) {
                for (String next : list) {
                    if (!TextUtils.isEmpty(next)) {
                        String replace = next.replace("{NAVIGATION_URL}", Uri.encode(str));
                        zzq.zzkq();
                        zzawb.zzb(this.zzup, "", replace);
                    }
                }
            }
        }
    }

    public final boolean zzjq() {
        return !zzjp() || this.zzbks;
    }
}
