package com.google.android.gms.internal.ads;

import com.google.android.gms.common.util.VisibleForTesting;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbzf {
    public final int type = 2;
    public final String zzcc;
    public final String zzfps;
    public final zzabu zzfpt;

    @VisibleForTesting
    public zzbzf(String str, String str2) {
        this.zzcc = str;
        this.zzfps = str2;
        this.zzfpt = null;
    }

    @VisibleForTesting
    public zzbzf(String str, zzabu zzabu) {
        this.zzcc = str;
        this.zzfps = null;
        this.zzfpt = zzabu;
    }
}
