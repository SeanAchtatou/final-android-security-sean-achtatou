package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import b.c.b.c;
import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.ads.internal.overlay.zzd;
import com.google.android.gms.common.util.PlatformVersion;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcjn implements zzcio<zzbtu> {
    private final zzczj zzfbj;
    private final Executor zzfci;
    private final zzbup zzfyt;
    private final Context zzup;

    public zzcjn(Context context, Executor executor, zzbup zzbup, zzczj zzczj) {
        this.zzup = context;
        this.zzfyt = zzbup;
        this.zzfci = executor;
        this.zzfbj = zzczj;
    }

    private static String zzc(zzczl zzczl) {
        try {
            return zzczl.zzglr.getString("tab_url");
        } catch (Exception unused) {
            return null;
        }
    }

    public final boolean zza(zzczt zzczt, zzczl zzczl) {
        return (this.zzup instanceof Activity) && PlatformVersion.isAtLeastIceCreamSandwichMR1() && zzaao.zzk(this.zzup) && !TextUtils.isEmpty(zzc(zzczl));
    }

    public final zzdhe<zzbtu> zzb(zzczt zzczt, zzczl zzczl) {
        String zzc = zzc(zzczl);
        return zzdgs.zzb(zzdgs.zzaj(null), new zzcjq(this, zzc != null ? Uri.parse(zzc) : null, zzczt, zzczl), this.zzfci);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzdhe zza(Uri uri, zzczt zzczt, zzczl zzczl, Object obj) throws Exception {
        try {
            c a2 = new c.a().a();
            a2.f2723a.setData(uri);
            zzd zzd = new zzd(a2.f2723a);
            zzazl zzazl = new zzazl();
            zzbtw zza = this.zzfyt.zza(new zzbmt(zzczt, zzczl, null), new zzbtv(new zzcjp(zzazl)));
            zzazl.set(new AdOverlayInfoParcel(zzd, null, zza.zzaen(), null, new zzazb(0, 0, false)));
            this.zzfbj.zzvb();
            return zzdgs.zzaj(zza.zzaem());
        } catch (Throwable th) {
            zzayu.zzc("Error in CustomTabsAdRenderer", th);
            throw th;
        }
    }
}
