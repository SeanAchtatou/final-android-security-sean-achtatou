package com.google.android.gms.games;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.PendingResultUtil;
import com.google.android.gms.games.event.EventBuffer;
import com.google.android.gms.games.event.Events;

/* compiled from: com.google.android.gms:play-services-games@@19.0.0 */
final class zzb implements PendingResultUtil.ResultConverter<Events.LoadEventsResult, EventBuffer> {
    zzb() {
    }

    public final /* synthetic */ Object convert(Result result) {
        Events.LoadEventsResult loadEventsResult = (Events.LoadEventsResult) result;
        if (loadEventsResult == null) {
            return null;
        }
        return loadEventsResult.getEvents();
    }
}
