package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.ads.reward.AdMetadataListener;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzczf extends zzark {
    private final zzczs zzfhh;
    private boolean zzgdg = false;
    private final zzcyt zzgkw;
    private final zzcxz zzgkx;
    /* access modifiers changed from: private */
    public zzcbb zzgky;

    public zzczf(zzcyt zzcyt, zzcxz zzcxz, zzczs zzczs) {
        this.zzgkw = zzcyt;
        this.zzgkx = zzcxz;
        this.zzfhh = zzczs;
    }

    private final synchronized boolean zzamp() {
        return this.zzgky != null && !this.zzgky.isClosed();
    }

    public final void destroy() throws RemoteException {
        zzl(null);
    }

    public final Bundle getAdMetadata() {
        Preconditions.checkMainThread("getAdMetadata can only be called from the UI thread.");
        zzcbb zzcbb = this.zzgky;
        return zzcbb != null ? zzcbb.getAdMetadata() : new Bundle();
    }

    public final synchronized String getMediationAdapterClassName() throws RemoteException {
        if (this.zzgky == null || this.zzgky.zzags() == null) {
            return null;
        }
        return this.zzgky.zzags().getMediationAdapterClassName();
    }

    public final boolean isLoaded() throws RemoteException {
        Preconditions.checkMainThread("isLoaded must be called on the main UI thread.");
        return zzamp();
    }

    public final void pause() {
        zzj(null);
    }

    public final void resume() {
        zzk(null);
    }

    public final void setAppPackageName(String str) throws RemoteException {
    }

    public final synchronized void setCustomData(String str) throws RemoteException {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcja)).booleanValue()) {
            Preconditions.checkMainThread("#008 Must be called on the main UI thread.: setCustomData");
            this.zzfhh.zzdnw = str;
        }
    }

    public final synchronized void setImmersiveMode(boolean z) {
        Preconditions.checkMainThread("setImmersiveMode must be called on the main UI thread.");
        this.zzgdg = z;
    }

    public final synchronized void setUserId(String str) throws RemoteException {
        Preconditions.checkMainThread("setUserId must be called on the main UI thread.");
        this.zzfhh.zzdnv = str;
    }

    public final synchronized void show() throws RemoteException {
        zzi(null);
    }

    public final synchronized void zza(zzaru zzaru) throws RemoteException {
        Preconditions.checkMainThread("loadAd must be called on the main UI thread.");
        if (!zzzp.zzcp(zzaru.zzbqz)) {
            if (zzamp()) {
                if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcor)).booleanValue()) {
                    return;
                }
            }
            zzcyq zzcyq = new zzcyq(null);
            this.zzgky = null;
            this.zzgkw.zza(zzaru.zzdio, zzaru.zzbqz, zzcyq, new zzcze(this));
        }
    }

    public final synchronized void zzi(IObjectWrapper iObjectWrapper) throws RemoteException {
        Activity activity;
        Preconditions.checkMainThread("showAd must be called on the main UI thread.");
        if (this.zzgky != null) {
            if (iObjectWrapper != null) {
                Object unwrap = ObjectWrapper.unwrap(iObjectWrapper);
                if (unwrap instanceof Activity) {
                    activity = (Activity) unwrap;
                    this.zzgky.zzb(this.zzgdg, activity);
                }
            }
            activity = null;
            this.zzgky.zzb(this.zzgdg, activity);
        }
    }

    public final synchronized void zzj(IObjectWrapper iObjectWrapper) {
        Context context;
        Preconditions.checkMainThread("pause must be called on the main UI thread.");
        if (this.zzgky != null) {
            if (iObjectWrapper == null) {
                context = null;
            } else {
                context = (Context) ObjectWrapper.unwrap(iObjectWrapper);
            }
            this.zzgky.zzagr().zzbv(context);
        }
    }

    public final synchronized void zzk(IObjectWrapper iObjectWrapper) {
        Context context;
        Preconditions.checkMainThread("resume must be called on the main UI thread.");
        if (this.zzgky != null) {
            if (iObjectWrapper == null) {
                context = null;
            } else {
                context = (Context) ObjectWrapper.unwrap(iObjectWrapper);
            }
            this.zzgky.zzagr().zzbw(context);
        }
    }

    public final synchronized zzxa zzkb() throws RemoteException {
        if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcrf)).booleanValue()) {
            return null;
        }
        if (this.zzgky == null) {
            return null;
        }
        return this.zzgky.zzags();
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v4, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: android.content.Context} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void zzl(com.google.android.gms.dynamic.IObjectWrapper r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            java.lang.String r0 = "destroy must be called on the main UI thread."
            com.google.android.gms.common.internal.Preconditions.checkMainThread(r0)     // Catch:{ all -> 0x0025 }
            com.google.android.gms.internal.ads.zzcxz r0 = r2.zzgkx     // Catch:{ all -> 0x0025 }
            r1 = 0
            r0.zza(r1)     // Catch:{ all -> 0x0025 }
            com.google.android.gms.internal.ads.zzcbb r0 = r2.zzgky     // Catch:{ all -> 0x0025 }
            if (r0 == 0) goto L_0x0023
            if (r3 != 0) goto L_0x0013
            goto L_0x001a
        L_0x0013:
            java.lang.Object r3 = com.google.android.gms.dynamic.ObjectWrapper.unwrap(r3)     // Catch:{ all -> 0x0025 }
            r1 = r3
            android.content.Context r1 = (android.content.Context) r1     // Catch:{ all -> 0x0025 }
        L_0x001a:
            com.google.android.gms.internal.ads.zzcbb r3 = r2.zzgky     // Catch:{ all -> 0x0025 }
            com.google.android.gms.internal.ads.zzbpg r3 = r3.zzagr()     // Catch:{ all -> 0x0025 }
            r3.zzbx(r1)     // Catch:{ all -> 0x0025 }
        L_0x0023:
            monitor-exit(r2)
            return
        L_0x0025:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzczf.zzl(com.google.android.gms.dynamic.IObjectWrapper):void");
    }

    public final boolean zzqa() {
        zzcbb zzcbb = this.zzgky;
        return zzcbb != null && zzcbb.zzqa();
    }

    public final void zza(zzaro zzaro) throws RemoteException {
        Preconditions.checkMainThread("setRewardedVideoAdListener can only be called from the UI thread.");
        this.zzgkx.zzb(zzaro);
    }

    public final void zza(zzarj zzarj) {
        Preconditions.checkMainThread("#008 Must be called on the main UI thread.: setRewardedAdSkuListener");
        this.zzgkx.zzb(zzarj);
    }

    public final void zza(zzvx zzvx) {
        Preconditions.checkMainThread("setAdMetadataListener can only be called from the UI thread.");
        if (zzvx == null) {
            this.zzgkx.zza((AdMetadataListener) null);
        } else {
            this.zzgkx.zza(new zzczh(this, zzvx));
        }
    }
}
