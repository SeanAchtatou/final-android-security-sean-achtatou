package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbit implements zzbow {
    private final zzczn zzfbk;
    private final zzczt zzfbl;
    private final zzdda zzfbm;

    public zzbit(zzczt zzczt, zzdda zzdda) {
        this.zzfbl = zzczt;
        this.zzfbm = zzdda;
        this.zzfbk = zzczt.zzgmi.zzgmf;
    }

    public final void onAdFailedToLoad(int i2) {
        this.zzfbm.zza(this.zzfbl, null, this.zzfbk.zzdbt);
    }
}
