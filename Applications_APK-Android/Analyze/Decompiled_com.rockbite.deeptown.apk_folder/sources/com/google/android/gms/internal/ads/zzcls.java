package com.google.android.gms.internal.ads;

import java.util.concurrent.CancellationException;
import java.util.concurrent.TimeoutException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzcls implements zzdgt<T> {
    private final /* synthetic */ String zzgae;
    private final /* synthetic */ long zzgaf;
    private final /* synthetic */ zzclp zzgag;

    zzcls(zzclp zzclp, String str, long j2) {
        this.zzgag = zzclp;
        this.zzgae = str;
        this.zzgaf = j2;
    }

    public final void onSuccess(T t) {
        this.zzgag.zza(this.zzgae, 0, this.zzgag.zzbmq.elapsedRealtime() - this.zzgaf);
    }

    public final void zzb(Throwable th) {
        long elapsedRealtime = this.zzgag.zzbmq.elapsedRealtime();
        int i2 = 3;
        if (th instanceof TimeoutException) {
            i2 = 2;
        } else if (!(th instanceof zzclf)) {
            i2 = th instanceof CancellationException ? 4 : (!(th instanceof zzcfb) || ((zzcfb) th).getErrorCode() != 3) ? 6 : 1;
        }
        this.zzgag.zza(this.zzgae, i2, elapsedRealtime - this.zzgaf);
    }
}
