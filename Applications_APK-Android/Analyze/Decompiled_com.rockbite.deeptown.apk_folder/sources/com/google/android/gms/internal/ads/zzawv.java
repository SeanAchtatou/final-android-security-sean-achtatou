package com.google.android.gms.internal.ads;

import android.content.DialogInterface;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzawv implements DialogInterface.OnClickListener {
    private final zzawt zzdta;
    private final int zzdtf;
    private final int zzdtg;
    private final int zzdth;

    zzawv(zzawt zzawt, int i2, int i3, int i4) {
        this.zzdta = zzawt;
        this.zzdtf = i2;
        this.zzdtg = i3;
        this.zzdth = i4;
    }

    public final void onClick(DialogInterface dialogInterface, int i2) {
        this.zzdta.zza(this.zzdtf, this.zzdtg, this.zzdth, dialogInterface, i2);
    }
}
