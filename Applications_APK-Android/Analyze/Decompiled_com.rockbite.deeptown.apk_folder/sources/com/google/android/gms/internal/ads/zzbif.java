package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbif implements zzdxg<zzaks> {
    private static final zzbif zzfbd = new zzbif();

    public static zzbif zzafe() {
        return zzfbd;
    }

    public static zzaks zzaff() {
        return (zzaks) zzdxm.zza(new zzaks(), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        return zzaff();
    }
}
