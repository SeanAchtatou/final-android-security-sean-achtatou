package com.google.android.gms.internal.ads;

import android.view.View;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzcnq<AdT> {
    AdT zza(zzczt zzczt, zzczl zzczl, View view, zzcnt zzcnt);
}
