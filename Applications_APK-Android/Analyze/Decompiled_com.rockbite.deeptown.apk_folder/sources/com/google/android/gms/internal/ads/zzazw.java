package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzazw implements Runnable {
    private final int zzdtf;
    private final zzazx zzdww;

    zzazw(zzazx zzazx, int i2) {
        this.zzdww = zzazx;
        this.zzdtf = i2;
    }

    public final void run() {
        this.zzdww.zzcu(this.zzdtf);
    }
}
