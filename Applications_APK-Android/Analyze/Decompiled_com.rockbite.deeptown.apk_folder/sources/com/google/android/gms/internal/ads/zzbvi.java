package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public class zzbvi {
    private final zzbwz zzfea;
    private final zzvh zzfjw;

    public zzbvi(zzbwz zzbwz, zzvh zzvh) {
        this.zzfea = zzbwz;
        this.zzfjw = zzvh;
    }

    public final zzbwz zzaij() {
        return this.zzfea;
    }

    public final zzvh zzaik() {
        return this.zzfjw;
    }
}
