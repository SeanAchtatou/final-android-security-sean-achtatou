package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcsh implements zzdxg<zzcsf> {
    private final zzdxp<zzdhd> zzfcv;

    private zzcsh(zzdxp<zzdhd> zzdxp) {
        this.zzfcv = zzdxp;
    }

    public static zzcsh zzak(zzdxp<zzdhd> zzdxp) {
        return new zzcsh(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzcsf(this.zzfcv.get());
    }
}
