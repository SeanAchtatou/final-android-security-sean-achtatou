package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzagm implements Runnable {
    private final /* synthetic */ zzagh zzcyb;

    zzagm(zzagh zzagh) {
        this.zzcyb = zzagh;
    }

    public final void run() {
        this.zzcyb.disconnect();
    }
}
