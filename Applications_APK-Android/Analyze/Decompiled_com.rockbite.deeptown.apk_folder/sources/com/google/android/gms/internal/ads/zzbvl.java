package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzbvl extends zzboe<zzbvm> {
    zzbvl zza(zzbod zzbod);

    zzbvl zza(zzbrm zzbrm);

    zzbvl zza(zzbvi zzbvi);

    zzbvm zzadf();
}
