package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbpc implements zzbrn {
    static final zzbrn zzfhp = new zzbpc();

    private zzbpc() {
    }

    public final void zzp(Object obj) {
        ((zzbpe) obj).onAdImpression();
    }
}
