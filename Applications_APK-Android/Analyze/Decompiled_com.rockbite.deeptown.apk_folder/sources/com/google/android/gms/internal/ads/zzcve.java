package com.google.android.gms.internal.ads;

import android.content.Context;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzcve implements zzcub<zzcty<JSONObject>> {
    private final JSONObject zzghw;

    zzcve(Context context) {
        this.zzghw = zzaqw.zzy(context);
    }

    public final zzdhe<zzcty<JSONObject>> zzanc() {
        return zzdgs.zzaj(new zzcvh(this));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzo(JSONObject jSONObject) {
        try {
            jSONObject.put("gms_sdk_env", this.zzghw);
        } catch (JSONException unused) {
            zzavs.zzed("Failed putting version constants.");
        }
    }
}
