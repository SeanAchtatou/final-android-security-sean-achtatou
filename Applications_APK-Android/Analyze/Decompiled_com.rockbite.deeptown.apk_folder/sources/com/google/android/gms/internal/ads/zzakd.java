package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzakd<I, O> implements zzaju<I, O> {
    private final zzais zzdav;
    /* access modifiers changed from: private */
    public final zzajw<O> zzdaw;
    private final zzajv<I> zzdax;
    private final String zzday;

    zzakd(zzais zzais, String str, zzajv<I> zzajv, zzajw<O> zzajw) {
        this.zzdav = zzais;
        this.zzday = str;
        this.zzdax = zzajv;
        this.zzdaw = zzajw;
    }

    /* access modifiers changed from: private */
    public final void zza(zzajf zzajf, zzajq zzajq, I i2, zzazl<O> zzazl) {
        try {
            zzq.zzkq();
            String zzwk = zzawb.zzwk();
            zzafa.zzcxi.zza(zzwk, new zzaki(this, zzajf, zzazl));
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("id", zzwk);
            jSONObject.put("args", this.zzdax.zzj(i2));
            zzajq.zza(this.zzday, jSONObject);
        } catch (Exception e2) {
            zzazl.setException(e2);
            zzayu.zzc("Unable to invokeJavascript", e2);
            zzajf.release();
        } catch (Throwable th) {
            zzajf.release();
            throw th;
        }
    }

    public final zzdhe<O> zzf(I i2) throws Exception {
        return zzi(i2);
    }

    public final zzdhe<O> zzi(I i2) {
        zzazl zzazl = new zzazl();
        zzajf zzb = this.zzdav.zzb((zzdq) null);
        zzb.zza(new zzakg(this, zzb, i2, zzazl), new zzakf(this, zzazl, zzb));
        return zzazl;
    }
}
