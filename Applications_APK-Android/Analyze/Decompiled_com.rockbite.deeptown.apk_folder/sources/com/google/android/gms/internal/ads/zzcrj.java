package com.google.android.gms.internal.ads;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcrj implements zzcub<zzcrg> {
    private final zzdhd zzfov;
    private final Context zzup;

    public zzcrj(zzdhd zzdhd, Context context) {
        this.zzfov = zzdhd;
        this.zzup = context;
    }

    public final zzdhe<zzcrg> zzanc() {
        return this.zzfov.zzd(new zzcri(this));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzcrg zzang() throws Exception {
        double d2;
        Intent registerReceiver = this.zzup.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        boolean z = false;
        if (registerReceiver != null) {
            int intExtra = registerReceiver.getIntExtra("status", -1);
            double intExtra2 = (double) registerReceiver.getIntExtra("level", -1);
            double intExtra3 = (double) registerReceiver.getIntExtra("scale", -1);
            Double.isNaN(intExtra2);
            Double.isNaN(intExtra3);
            d2 = intExtra2 / intExtra3;
            if (intExtra == 2 || intExtra == 5) {
                z = true;
            }
        } else {
            d2 = -1.0d;
        }
        return new zzcrg(d2, z);
    }
}
