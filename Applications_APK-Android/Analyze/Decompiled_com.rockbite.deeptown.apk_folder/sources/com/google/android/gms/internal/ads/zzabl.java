package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzabl {
    public static zzaan<Boolean> zzcuu = zzaan.zzf("gads:webview:permission:disabled", false);
    public static zzaan<Boolean> zzcuv = zzaan.zzf("gads:corewebview:adwebview_factory:enable", false);
    public static zzaan<Boolean> zzcuw = zzaan.zzf("gads:corewebview:javascript_engine", false);
}
