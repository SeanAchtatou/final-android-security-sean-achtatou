package com.google.android.gms.gass;

import android.content.Context;
import com.google.android.gms.common.internal.ShowFirstParty;
import com.google.android.gms.gass.internal.zzo;

@ShowFirstParty
/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
public final class zzd {
    public static zzo zza(Context context, int i2, String str, String str2, String str3, AdShield2Logger adShield2Logger) {
        return new zzg(context, 1, str, str2, str3, adShield2Logger).zzdo(50000);
    }
}
