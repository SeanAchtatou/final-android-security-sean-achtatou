package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.WeakHashMap;
import java.util.concurrent.Future;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzaqv {
    /* access modifiers changed from: private */
    public WeakHashMap<Context, zzaqx> zzdni = new WeakHashMap<>();

    public final Future<zzaqt> zzx(Context context) {
        return zzazd.zzdwe.zzd(new zzaqu(this, context));
    }
}
