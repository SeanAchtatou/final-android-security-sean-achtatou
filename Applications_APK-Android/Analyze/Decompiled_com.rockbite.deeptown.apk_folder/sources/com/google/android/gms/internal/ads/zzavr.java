package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzavr implements Runnable {
    private final /* synthetic */ zzavo zzdru;

    zzavr(zzavo zzavo) {
        this.zzdru = zzavo;
    }

    public final void run() {
        Thread unused = this.zzdru.thread = Thread.currentThread();
        this.zzdru.zztu();
    }
}
