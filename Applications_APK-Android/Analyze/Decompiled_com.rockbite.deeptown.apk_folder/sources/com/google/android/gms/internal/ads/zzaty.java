package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzaty implements zzaui {
    static final zzaui zzdpr = new zzaty();

    private zzaty() {
    }

    public final Object zzb(zzbfq zzbfq) {
        return zzbfq.getAppInstanceId();
    }
}
