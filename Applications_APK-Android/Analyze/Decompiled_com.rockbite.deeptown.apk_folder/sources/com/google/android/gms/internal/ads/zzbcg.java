package com.google.android.gms.internal.ads;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.MotionEvent;
import com.facebook.internal.NativeProtocol;
import com.tapjoy.TJAdUnitConstants;
import com.underwater.demolisher.data.vo.RemoteConfigConst;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbcg implements zzafn<zzbaz> {
    private boolean zzecw;

    private static int zza(Context context, Map<String, String> map, String str, int i2) {
        String str2 = map.get(str);
        if (str2 == null) {
            return i2;
        }
        try {
            zzve.zzou();
            return zzayk.zza(context, Integer.parseInt(str2));
        } catch (NumberFormatException unused) {
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 34 + String.valueOf(str2).length());
            sb.append("Could not parse ");
            sb.append(str);
            sb.append(" in a video GMSG: ");
            sb.append(str2);
            zzayu.zzez(sb.toString());
            return i2;
        }
    }

    private static void zza(zzbai zzbai, Map<String, String> map) {
        String str = map.get("minBufferMs");
        String str2 = map.get("maxBufferMs");
        String str3 = map.get("bufferForPlaybackMs");
        String str4 = map.get("bufferForPlaybackAfterRebufferMs");
        String str5 = map.get("socketReceiveBufferSize");
        if (str != null) {
            try {
                zzbai.zzcv(Integer.parseInt(str));
            } catch (NumberFormatException unused) {
                zzayu.zzez(String.format("Could not parse buffer parameters in loadControl video GMSG: (%s, %s)", str, str2));
                return;
            }
        }
        if (str2 != null) {
            zzbai.zzcw(Integer.parseInt(str2));
        }
        if (str3 != null) {
            zzbai.zzcx(Integer.parseInt(str3));
        }
        if (str4 != null) {
            zzbai.zzcy(Integer.parseInt(str4));
        }
        if (str5 != null) {
            zzbai.zzcz(Integer.parseInt(str5));
        }
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        int i2;
        zzbaz zzbaz = (zzbaz) obj;
        String str = (String) map.get(NativeProtocol.WEB_DIALOG_ACTION);
        if (str == null) {
            zzayu.zzez("Action missing from video GMSG.");
            return;
        }
        if (zzayu.isLoggable(3)) {
            JSONObject jSONObject = new JSONObject(map);
            jSONObject.remove("google.afma.Notify_dt");
            String jSONObject2 = jSONObject.toString();
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 13 + String.valueOf(jSONObject2).length());
            sb.append("Video GMSG: ");
            sb.append(str);
            sb.append(RemoteConfigConst.SHOP_SPECIAL_EVENT_OFFER);
            sb.append(jSONObject2);
            zzayu.zzea(sb.toString());
        }
        if ("background".equals(str)) {
            String str2 = (String) map.get("color");
            if (TextUtils.isEmpty(str2)) {
                zzayu.zzez("Color parameter missing from color video GMSG.");
                return;
            }
            try {
                zzbaz.setBackgroundColor(Color.parseColor(str2));
            } catch (IllegalArgumentException unused) {
                zzayu.zzez("Invalid color parameter in video GMSG.");
            }
        } else {
            if ("decoderProps".equals(str)) {
                String str3 = (String) map.get("mimeTypes");
                if (str3 == null) {
                    zzayu.zzez("No MIME types specified for decoder properties inspection.");
                    zzbai.zza(zzbaz, "missingMimeTypes");
                } else if (Build.VERSION.SDK_INT < 16) {
                    zzayu.zzez("Video decoder properties available on API versions >= 16.");
                    zzbai.zza(zzbaz, "deficientApiVersion");
                } else {
                    HashMap hashMap = new HashMap();
                    for (String str4 : str3.split(",")) {
                        hashMap.put(str4, zzayi.zzer(str4.trim()));
                    }
                    zzbai.zza(zzbaz, hashMap);
                }
            } else {
                zzbao zzyk = zzbaz.zzyk();
                if (zzyk == null) {
                    zzayu.zzez("Could not get underlay container for a video GMSG.");
                    return;
                }
                boolean equals = "new".equals(str);
                boolean equals2 = "position".equals(str);
                if (equals || equals2) {
                    Context context = zzbaz.getContext();
                    int zza = zza(context, map, "x", 0);
                    int zza2 = zza(context, map, "y", 0);
                    int zza3 = zza(context, map, "w", -1);
                    int zza4 = zza(context, map, "h", -1);
                    int min = Math.min(zza3, zzbaz.zzyt() - zza);
                    int min2 = Math.min(zza4, zzbaz.zzys() - zza2);
                    try {
                        i2 = Integer.parseInt((String) map.get("player"));
                    } catch (NumberFormatException unused2) {
                        i2 = 0;
                    }
                    boolean parseBoolean = Boolean.parseBoolean((String) map.get("spherical"));
                    if (!equals || zzyk.zzye() != null) {
                        zzyk.zze(zza, zza2, min, min2);
                        return;
                    }
                    zzyk.zza(zza, zza2, min, min2, i2, parseBoolean, new zzbaw((String) map.get("flags")));
                    zzbai zzye = zzyk.zzye();
                    if (zzye != null) {
                        zza(zzye, (Map<String, String>) map);
                        return;
                    }
                    return;
                }
                zzbed zzyl = zzbaz.zzyl();
                if (zzyl != null) {
                    if ("timeupdate".equals(str)) {
                        String str5 = (String) map.get(TJAdUnitConstants.String.VIDEO_CURRENT_TIME);
                        if (str5 == null) {
                            zzayu.zzez("currentTime parameter missing from timeupdate video GMSG.");
                            return;
                        }
                        try {
                            zzyl.zze(Float.parseFloat(str5));
                            return;
                        } catch (NumberFormatException unused3) {
                            String valueOf = String.valueOf(str5);
                            zzayu.zzez(valueOf.length() != 0 ? "Could not parse currentTime parameter from timeupdate video GMSG: ".concat(valueOf) : new String("Could not parse currentTime parameter from timeupdate video GMSG: "));
                            return;
                        }
                    } else if ("skip".equals(str)) {
                        zzyl.zzabo();
                        return;
                    }
                }
                zzbai zzye2 = zzyk.zzye();
                if (zzye2 == null) {
                    zzbai.zza(zzbaz);
                } else if (TJAdUnitConstants.String.CLICK.equals(str)) {
                    Context context2 = zzbaz.getContext();
                    int zza5 = zza(context2, map, "x", 0);
                    int zza6 = zza(context2, map, "y", 0);
                    long uptimeMillis = SystemClock.uptimeMillis();
                    MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 0, (float) zza5, (float) zza6, 0);
                    zzye2.zze(obtain);
                    obtain.recycle();
                } else if (TJAdUnitConstants.String.VIDEO_CURRENT_TIME.equals(str)) {
                    String str6 = (String) map.get("time");
                    if (str6 == null) {
                        zzayu.zzez("Time parameter missing from currentTime video GMSG.");
                        return;
                    }
                    try {
                        zzye2.seekTo((int) (Float.parseFloat(str6) * 1000.0f));
                    } catch (NumberFormatException unused4) {
                        String valueOf2 = String.valueOf(str6);
                        zzayu.zzez(valueOf2.length() != 0 ? "Could not parse time parameter from currentTime video GMSG: ".concat(valueOf2) : new String("Could not parse time parameter from currentTime video GMSG: "));
                    }
                } else if ("hide".equals(str)) {
                    zzye2.setVisibility(4);
                } else if ("load".equals(str)) {
                    zzye2.zzhq();
                } else if ("loadControl".equals(str)) {
                    zza(zzye2, (Map<String, String>) map);
                } else if ("muted".equals(str)) {
                    if (Boolean.parseBoolean((String) map.get("muted"))) {
                        zzye2.zzxy();
                    } else {
                        zzye2.zzxz();
                    }
                } else if ("pause".equals(str)) {
                    zzye2.pause();
                } else if ("play".equals(str)) {
                    zzye2.play();
                } else if ("show".equals(str)) {
                    zzye2.setVisibility(0);
                } else if ("src".equals(str)) {
                    String str7 = (String) map.get("src");
                    String[] strArr = {str7};
                    String str8 = (String) map.get("demuxed");
                    if (str8 != null) {
                        try {
                            JSONArray jSONArray = new JSONArray(str8);
                            String[] strArr2 = new String[jSONArray.length()];
                            for (int i3 = 0; i3 < jSONArray.length(); i3++) {
                                strArr2[i3] = jSONArray.getString(i3);
                            }
                            strArr = strArr2;
                        } catch (JSONException unused5) {
                            String valueOf3 = String.valueOf(str8);
                            zzayu.zzez(valueOf3.length() != 0 ? "Malformed demuxed URL list for playback: ".concat(valueOf3) : new String("Malformed demuxed URL list for playback: "));
                            strArr = new String[]{str7};
                        }
                    }
                    zzye2.zzc(str7, strArr);
                } else if ("touchMove".equals(str)) {
                    Context context3 = zzbaz.getContext();
                    zzye2.zza((float) zza(context3, map, "dx", 0), (float) zza(context3, map, "dy", 0));
                    if (!this.zzecw) {
                        zzbaz.zzts();
                        this.zzecw = true;
                    }
                } else if ("volume".equals(str)) {
                    String str9 = (String) map.get("volume");
                    if (str9 == null) {
                        zzayu.zzez("Level parameter missing from volume video GMSG.");
                        return;
                    }
                    try {
                        zzye2.setVolume(Float.parseFloat(str9));
                    } catch (NumberFormatException unused6) {
                        String valueOf4 = String.valueOf(str9);
                        zzayu.zzez(valueOf4.length() != 0 ? "Could not parse volume parameter from volume video GMSG: ".concat(valueOf4) : new String("Could not parse volume parameter from volume video GMSG: "));
                    }
                } else if ("watermark".equals(str)) {
                    zzye2.zzya();
                } else {
                    String valueOf5 = String.valueOf(str);
                    zzayu.zzez(valueOf5.length() != 0 ? "Unknown video action: ".concat(valueOf5) : new String("Unknown video action: "));
                }
            }
        }
    }
}
