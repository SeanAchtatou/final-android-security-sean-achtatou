package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzaqb extends IInterface {
    zzapx zza(zzapv zzapv) throws RemoteException;

    void zza(zzapv zzapv, zzaqc zzaqc) throws RemoteException;

    void zza(zzaqk zzaqk, zzaqe zzaqe) throws RemoteException;

    void zza(String str, zzaqe zzaqe) throws RemoteException;

    void zzb(zzaqk zzaqk, zzaqe zzaqe) throws RemoteException;

    void zzc(zzaqk zzaqk, zzaqe zzaqe) throws RemoteException;
}
