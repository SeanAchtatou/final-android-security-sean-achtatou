package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbpu;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzcis<AdapterT, ListenerT extends zzbpu> {
    zzcip<AdapterT, ListenerT> zzd(String str, JSONObject jSONObject) throws zzdab;
}
