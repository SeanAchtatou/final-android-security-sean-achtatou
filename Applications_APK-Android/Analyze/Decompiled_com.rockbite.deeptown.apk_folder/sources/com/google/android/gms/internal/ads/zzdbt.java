package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdbt implements zzdxg<zzdhd> {
    private static final zzdbt zzgpr = new zzdbt();

    public static zzdbt zzapx() {
        return zzgpr;
    }

    public final /* synthetic */ Object get() {
        return (zzdhd) zzdxm.zza(zzdhg.zza(zzddq.zzaqs().zzdu(zzddv.zzgtv)), "Cannot return null from a non-@Nullable @Provides method");
    }
}
