package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzaar {
    public static zzaan<String> zzcsn = zzaan.zzi("gads:afs:csa_webview_custom_domain_param_key", "csa_customDomain");
    public static zzaan<String> zzcso = zzaan.zzi("gads:afs:csa_webview_static_file_path", "/afs/ads/i/webview.html");
}
