package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcgy implements Callable {
    private final zzdhe zzfgb;
    private final zzdhe zzfpa;
    private final zzdhe zzfpn;

    zzcgy(zzdhe zzdhe, zzdhe zzdhe2, zzdhe zzdhe3) {
        this.zzfpn = zzdhe;
        this.zzfpa = zzdhe2;
        this.zzfgb = zzdhe3;
    }

    public final Object call() {
        return new zzchk((zzchn) this.zzfpn.get(), (JSONObject) this.zzfpa.get(), (zzaqq) this.zzfgb.get());
    }
}
