package com.google.android.gms.internal.ads;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbyw implements zzded {
    private final String zzcyz;
    private final int zzdtg;
    private final int zzdth;
    private final double zzfpm;

    zzbyw(String str, double d2, int i2, int i3) {
        this.zzcyz = str;
        this.zzfpm = d2;
        this.zzdtg = i2;
        this.zzdth = i3;
    }

    public final Object apply(Object obj) {
        String str = this.zzcyz;
        return new zzabu(new BitmapDrawable(Resources.getSystem(), (Bitmap) obj), Uri.parse(str), this.zzfpm, this.zzdtg, this.zzdth);
    }
}
