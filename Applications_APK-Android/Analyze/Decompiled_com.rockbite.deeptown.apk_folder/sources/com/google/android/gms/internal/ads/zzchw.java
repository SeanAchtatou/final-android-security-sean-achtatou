package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzchw implements Callable {
    private final zzchr zzfwx;

    private zzchw(zzchr zzchr) {
        this.zzfwx = zzchr;
    }

    static Callable zza(zzchr zzchr) {
        return new zzchw(zzchr);
    }

    public final Object call() {
        return this.zzfwx.getWritableDatabase();
    }
}
