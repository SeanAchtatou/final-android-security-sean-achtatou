package com.google.android.gms.internal.ads;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzba {
    public static zzu zza(Context context) {
        zzu zzu = new zzu(new zzal(new zzaz(context.getApplicationContext())), new zzak(new zzat()));
        zzu.start();
        return zzu;
    }
}
