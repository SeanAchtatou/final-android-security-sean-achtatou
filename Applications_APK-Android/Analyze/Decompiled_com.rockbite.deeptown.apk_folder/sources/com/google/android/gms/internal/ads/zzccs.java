package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzso;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzccs implements zzdxg<zzcdh> {
    private static final zzccs zzfrw = new zzccs();

    public static zzccs zzakz() {
        return zzfrw;
    }

    public final /* synthetic */ Object get() {
        return (zzcdh) zzdxm.zza(new zzcdh(zzso.zza.C0151zza.REQUEST_WILL_UPDATE_SIGNALS, zzso.zza.C0151zza.REQUEST_DID_UPDATE_SIGNALS, zzso.zza.C0151zza.REQUEST_FAILED_TO_UPDATE_SIGNALS), "Cannot return null from a non-@Nullable @Provides method");
    }
}
