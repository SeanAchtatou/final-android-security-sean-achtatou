package com.google.android.gms.internal.ads;

import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcvh implements zzcty {
    private final zzcve zzghx;

    zzcvh(zzcve zzcve) {
        this.zzghx = zzcve;
    }

    public final void zzr(Object obj) {
        this.zzghx.zzo((JSONObject) obj);
    }
}
