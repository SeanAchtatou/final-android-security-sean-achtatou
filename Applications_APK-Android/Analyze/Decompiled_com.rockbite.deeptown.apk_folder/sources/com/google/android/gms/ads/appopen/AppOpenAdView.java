package com.google.android.gms.ads.appopen;

import android.content.Context;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.ads.zzayu;
import com.google.android.gms.internal.ads.zzrc;
import com.google.android.gms.internal.ads.zzuj;
import com.google.android.gms.internal.ads.zzvu;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class AppOpenAdView extends ViewGroup {
    private AppOpenAd zzaby;
    private AppOpenAdPresentationCallback zzabz;

    public AppOpenAdView(Context context) {
        super(context);
        Preconditions.checkNotNull(context, "Context cannot be null");
    }

    private final AdSize getAdSize() {
        zzvu zzdm = this.zzaby.zzdm();
        if (zzdm == null) {
            return null;
        }
        try {
            zzuj zzjz = zzdm.zzjz();
            if (zzjz != null) {
                return zzjz.zzoo();
            }
            return null;
        } catch (RemoteException e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
            return null;
        }
    }

    private final void zzdo() {
        AppOpenAdPresentationCallback appOpenAdPresentationCallback;
        AppOpenAd appOpenAd = this.zzaby;
        if (appOpenAd != null && (appOpenAdPresentationCallback = this.zzabz) != null) {
            appOpenAd.zza(new zzrc(appOpenAdPresentationCallback));
        }
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        View childAt = getChildAt(0);
        if (childAt != null && childAt.getVisibility() != 8) {
            int measuredWidth = childAt.getMeasuredWidth();
            int measuredHeight = childAt.getMeasuredHeight();
            int i6 = ((i4 - i2) - measuredWidth) / 2;
            int i7 = ((i5 - i3) - measuredHeight) / 2;
            childAt.layout(i6, i7, measuredWidth + i6, measuredHeight + i7);
        }
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i2, int i3) {
        int i4;
        int i5 = 0;
        View childAt = getChildAt(0);
        if (childAt == null || childAt.getVisibility() == 8) {
            AdSize adSize = null;
            try {
                adSize = getAdSize();
            } catch (NullPointerException e2) {
                zzayu.zzc("Unable to retrieve ad size.", e2);
            }
            if (adSize != null) {
                Context context = getContext();
                int widthInPixels = adSize.getWidthInPixels(context);
                i4 = adSize.getHeightInPixels(context);
                i5 = widthInPixels;
            } else {
                i4 = 0;
            }
        } else {
            measureChild(childAt, i2, i3);
            i5 = childAt.getMeasuredWidth();
            i4 = childAt.getMeasuredHeight();
        }
        setMeasuredDimension(View.resolveSize(Math.max(i5, getSuggestedMinimumWidth()), i2), View.resolveSize(Math.max(i4, getSuggestedMinimumHeight()), i3));
    }

    public final void setAppOpenAd(AppOpenAd appOpenAd) {
        IObjectWrapper zzjx;
        try {
            zzvu zzdm = appOpenAd.zzdm();
            if (zzdm != null && (zzjx = zzdm.zzjx()) != null) {
                View view = (View) ObjectWrapper.unwrap(zzjx);
                if (view.getParent() == null) {
                    removeAllViews();
                    addView(view);
                    this.zzaby = appOpenAd;
                    zzdo();
                    return;
                }
                zzayu.zzex("Trying to set AppOpenAd which is already in use.");
            }
        } catch (RemoteException e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
        }
    }

    public final void setAppOpenAdPresentationCallback(AppOpenAdPresentationCallback appOpenAdPresentationCallback) {
        this.zzabz = appOpenAdPresentationCallback;
        zzdo();
    }

    public AppOpenAdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public AppOpenAdView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
    }
}
