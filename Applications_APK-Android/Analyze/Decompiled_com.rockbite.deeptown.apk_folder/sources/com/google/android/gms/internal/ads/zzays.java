package com.google.android.gms.internal.ads;

import android.util.JsonWriter;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final /* synthetic */ class zzays implements zzayv {
    private final String zzcyz;

    zzays(String str) {
        this.zzcyz = str;
    }

    public final void zzb(JsonWriter jsonWriter) {
        zzayo.zza(this.zzcyz, jsonWriter);
    }
}
