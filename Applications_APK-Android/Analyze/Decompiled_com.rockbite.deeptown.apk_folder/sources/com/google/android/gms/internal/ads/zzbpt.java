package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbpt implements zzbrn {
    private final String zzcyr;
    private final String zzdbl;
    private final zzare zzfhs;

    zzbpt(zzare zzare, String str, String str2) {
        this.zzfhs = zzare;
        this.zzcyr = str;
        this.zzdbl = str2;
    }

    public final void zzp(Object obj) {
        ((zzbov) obj).zzb(this.zzfhs, this.zzcyr, this.zzdbl);
    }
}
