package com.google.android.gms.internal.ads;

import org.json.JSONException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbom extends zzwz {
    private final String zzfhj;
    private final String zzfhk;

    public zzbom(zzczl zzczl, String str) {
        String str2;
        if (zzczl == null) {
            str2 = null;
        } else {
            str2 = zzczl.zzfhk;
        }
        this.zzfhk = str2;
        String zzb = "com.google.android.gms.ads.mediation.customevent.CustomEventAdapter".equals(str) || "com.google.ads.mediation.customevent.CustomEventAdapter".equals(str) ? zzb(zzczl) : null;
        this.zzfhj = zzb == null ? str : zzb;
    }

    private static String zzb(zzczl zzczl) {
        try {
            return zzczl.zzglr.getString("class_name");
        } catch (JSONException unused) {
            return null;
        }
    }

    public final String getMediationAdapterClassName() {
        return this.zzfhj;
    }

    public final String zzpj() {
        return this.zzfhk;
    }
}
