package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
final class zzcf implements zzdsa {
    static final zzdsa zzew = new zzcf();

    private zzcf() {
    }

    public final boolean zzf(int i2) {
        return zzcd.zzj(i2) != null;
    }
}
