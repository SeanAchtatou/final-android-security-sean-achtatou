package com.google.android.gms.internal.ads;

import com.facebook.appevents.AppEventsConstants;
import com.tapjoy.TapjoyConstants;
import java.util.HashMap;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzbcp implements Runnable {
    private final /* synthetic */ String zzdug;
    private final /* synthetic */ String zzedb;
    private final /* synthetic */ boolean zzede;
    private final /* synthetic */ zzbcn zzedf;
    private final /* synthetic */ long zzedh;
    private final /* synthetic */ long zzedi;
    private final /* synthetic */ int zzedj;
    private final /* synthetic */ int zzedk;

    zzbcp(zzbcn zzbcn, String str, String str2, long j2, long j3, boolean z, int i2, int i3) {
        this.zzedf = zzbcn;
        this.zzdug = str;
        this.zzedb = str2;
        this.zzedh = j2;
        this.zzedi = j3;
        this.zzede = z;
        this.zzedj = i2;
        this.zzedk = i3;
    }

    public final void run() {
        HashMap hashMap = new HashMap();
        hashMap.put(TapjoyConstants.TJC_SDK_TYPE_DEFAULT, "precacheProgress");
        hashMap.put("src", this.zzdug);
        hashMap.put("cachedSrc", this.zzedb);
        hashMap.put("bufferedDuration", Long.toString(this.zzedh));
        hashMap.put("totalDuration", Long.toString(this.zzedi));
        hashMap.put("cacheReady", this.zzede ? "1" : AppEventsConstants.EVENT_PARAM_VALUE_NO);
        hashMap.put("playerCount", Integer.toString(this.zzedj));
        hashMap.put("playerPreparedCount", Integer.toString(this.zzedk));
        this.zzedf.zza("onPrecacheEvent", hashMap);
    }
}
