package com.google.android.gms.internal.ads;

import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.ScheduledExecutorService;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public abstract class zzdcd<E> {
    /* access modifiers changed from: private */
    public static final zzdhe<?> zzgqa = zzdgs.zzaj(null);
    /* access modifiers changed from: private */
    public final ScheduledExecutorService zzfdi;
    /* access modifiers changed from: private */
    public final zzdhd zzfov;
    /* access modifiers changed from: private */
    public final zzdcp<E> zzgqb;

    public zzdcd(zzdhd zzdhd, ScheduledExecutorService scheduledExecutorService, zzdcp<E> zzdcp) {
        this.zzfov = zzdhd;
        this.zzfdi = scheduledExecutorService;
        this.zzgqb = zzdcp;
    }

    public final <I> zzdcj<I> zza(Object obj, zzdhe zzdhe) {
        return new zzdcj(this, obj, zzdhe, Collections.singletonList(zzdhe), zzdhe);
    }

    public final zzdch zzu(E e2) {
        return new zzdch(this, e2);
    }

    /* access modifiers changed from: protected */
    public abstract String zzv(E e2);

    public final zzdcf zza(Object obj, zzdhe<?>... zzdheArr) {
        return new zzdcf(this, obj, Arrays.asList(zzdheArr));
    }
}
