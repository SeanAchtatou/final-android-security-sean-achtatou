package com.google.android.gms.ads;

import com.google.android.gms.common.util.VisibleForTesting;

@VisibleForTesting
/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzb {
    public static AdSize zza(int i2, int i3, String str) {
        return new AdSize(i2, i3, str);
    }

    public static boolean zzb(AdSize adSize) {
        return adSize.zzdi();
    }

    public static int zzc(AdSize adSize) {
        return adSize.zzdj();
    }

    public static AdSize zza(int i2, int i3) {
        AdSize adSize = new AdSize(i2, 0);
        adSize.zzc(true);
        adSize.zzl(i3);
        return adSize;
    }

    public static boolean zza(AdSize adSize) {
        return adSize.zzdh();
    }
}
