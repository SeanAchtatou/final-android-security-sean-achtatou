package com.google.android.gms.internal.ads;

import android.content.DialogInterface;
import android.webkit.JsPromptResult;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzbdh implements DialogInterface.OnClickListener {
    private final /* synthetic */ JsPromptResult zzeeb;

    zzbdh(JsPromptResult jsPromptResult) {
        this.zzeeb = jsPromptResult;
    }

    public final void onClick(DialogInterface dialogInterface, int i2) {
        this.zzeeb.cancel();
    }
}
