package com.google.android.gms.internal.ads;

import android.content.pm.PackageInfo;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcuv implements zzcub<zzcus> {
    private final Executor executor;
    private final PackageInfo zzdip;
    private final zzava zzghl;
    private final String zzgho;

    public zzcuv(zzava zzava, Executor executor2, String str, PackageInfo packageInfo) {
        this.zzghl = zzava;
        this.executor = executor2;
        this.zzgho = str;
        this.zzdip = packageInfo;
    }

    public final zzdhe<zzcus> zzanc() {
        return zzdgs.zzb(zzdgs.zzb(this.zzghl.zza(this.zzgho, this.zzdip), zzcuu.zzdoq, this.executor), Throwable.class, new zzcux(this), this.executor);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzdhe zzf(Throwable th) throws Exception {
        return zzdgs.zzaj(new zzcus(this.zzgho));
    }
}
