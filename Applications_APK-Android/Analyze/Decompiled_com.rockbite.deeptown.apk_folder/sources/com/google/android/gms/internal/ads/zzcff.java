package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcff implements zzdgf {
    private final zzcfe zzfuh;
    private final zzaqk zzfui;

    zzcff(zzcfe zzcfe, zzaqk zzaqk) {
        this.zzfuh = zzcfe;
        this.zzfui = zzaqk;
    }

    public final zzdhe zzf(Object obj) {
        return this.zzfuh.zza(this.zzfui, (zzcgr) obj);
    }
}
