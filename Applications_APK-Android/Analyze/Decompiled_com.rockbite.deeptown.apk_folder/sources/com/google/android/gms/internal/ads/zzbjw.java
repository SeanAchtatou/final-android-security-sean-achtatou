package com.google.android.gms.internal.ads;

import android.view.View;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbjw {
    private final View view;
    private final zzbdi zzcza;
    private final zzczk zzfdo;
    private final int zzfdp;

    public zzbjw(View view2, zzbdi zzbdi, zzczk zzczk, int i2) {
        this.view = view2;
        this.zzcza = zzbdi;
        this.zzfdo = zzczk;
        this.zzfdp = i2;
    }

    public final zzbdi zzaft() {
        return this.zzcza;
    }

    public final View zzafu() {
        return this.view;
    }

    public final zzczk zzafv() {
        return this.zzfdo;
    }

    public final int zzafw() {
        return this.zzfdp;
    }
}
