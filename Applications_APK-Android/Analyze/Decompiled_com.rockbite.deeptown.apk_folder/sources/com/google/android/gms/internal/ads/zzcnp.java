package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcnp<AdT> implements zzdxg<zzcnl<AdT>> {
    private final zzdxp<zzdhd> zzfei;
    private final zzdxp<zzdcr> zzfet;
    private final zzdxp<zzaak> zzgbt;
    private final zzdxp<zzcnq<AdT>> zzgby;

    public zzcnp(zzdxp<zzdcr> zzdxp, zzdxp<zzdhd> zzdxp2, zzdxp<zzaak> zzdxp3, zzdxp<zzcnq<AdT>> zzdxp4) {
        this.zzfet = zzdxp;
        this.zzfei = zzdxp2;
        this.zzgbt = zzdxp3;
        this.zzgby = zzdxp4;
    }

    public final /* synthetic */ Object get() {
        return new zzcnl(this.zzfet.get(), this.zzfei.get(), this.zzgbt.get(), this.zzgby.get());
    }
}
