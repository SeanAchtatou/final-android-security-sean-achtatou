package com.google.android.gms.internal.ads;

import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzafk implements zzafn<zzbdi> {
    zzafk() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        ((zzbdi) obj).zzay(!Boolean.parseBoolean((String) map.get("disabled")));
    }
}
