package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zzq;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzawc implements zzayp {
    private final /* synthetic */ Context val$context;
    private final /* synthetic */ String zzdsv;

    zzawc(zzawb zzawb, Context context, String str) {
        this.val$context = context;
        this.zzdsv = str;
    }

    public final void zzen(String str) {
        zzq.zzkq();
        zzawb.zzb(this.val$context, this.zzdsv, str);
    }
}
