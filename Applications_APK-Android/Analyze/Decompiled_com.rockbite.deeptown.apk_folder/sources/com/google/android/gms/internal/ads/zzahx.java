package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzahx extends zzbfk {
    private final /* synthetic */ zzahr zzcys;

    private zzahx(zzahr zzahr) {
        this.zzcys = zzahr;
    }

    public final void zza(zzbfn zzbfn) {
        if (this.zzcys.zzcyp != null) {
            this.zzcys.zzcyp.zzsa();
        }
    }

    public final void zzb(zzbfn zzbfn) {
        this.zzcys.zzg(zzbfn.uri);
    }

    public final boolean zzc(zzbfn zzbfn) {
        return this.zzcys.zzg(zzbfn.uri);
    }
}
