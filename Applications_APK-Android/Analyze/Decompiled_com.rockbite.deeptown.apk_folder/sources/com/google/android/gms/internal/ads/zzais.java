package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zzq;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzais {
    /* access modifiers changed from: private */
    public final Object lock;
    /* access modifiers changed from: private */
    public int status;
    private final zzazb zzbll;
    private final String zzczl;
    /* access modifiers changed from: private */
    public zzaxh<zzaif> zzczm;
    private zzaxh<zzaif> zzczn;
    /* access modifiers changed from: private */
    public zzajj zzczo;
    private final Context zzup;

    public zzais(Context context, zzazb zzazb, String str) {
        this.lock = new Object();
        this.status = 1;
        this.zzczl = str;
        this.zzup = context.getApplicationContext();
        this.zzbll = zzazb;
        this.zzczm = new zzajg();
        this.zzczn = new zzajg();
    }

    /* access modifiers changed from: protected */
    public final zzajj zza(zzdq zzdq) {
        zzajj zzajj = new zzajj(this.zzczn);
        zzazd.zzdwi.execute(new zzair(this, zzdq, zzajj));
        zzajj.zza(new zzajb(this, zzajj), new zzaje(this, zzajj));
        return zzajj;
    }

    public final zzajf zzb(zzdq zzdq) {
        synchronized (this.lock) {
            synchronized (this.lock) {
                if (this.zzczo != null && this.status == 0) {
                    this.zzczo.zza(new zzaiu(this), zzait.zzczp);
                }
            }
            if (this.zzczo != null) {
                if (this.zzczo.getStatus() != -1) {
                    if (this.status == 0) {
                        zzajf zzsc = this.zzczo.zzsc();
                        return zzsc;
                    } else if (this.status == 1) {
                        this.status = 2;
                        zza((zzdq) null);
                        zzajf zzsc2 = this.zzczo.zzsc();
                        return zzsc2;
                    } else if (this.status == 2) {
                        zzajf zzsc3 = this.zzczo.zzsc();
                        return zzsc3;
                    } else {
                        zzajf zzsc4 = this.zzczo.zzsc();
                        return zzsc4;
                    }
                }
            }
            this.status = 2;
            this.zzczo = zza((zzdq) null);
            zzajf zzsc5 = this.zzczo.zzsc();
            return zzsc5;
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zza(zzaif zzaif) {
        if (zzaif.isDestroyed()) {
            this.status = 1;
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zza(zzdq zzdq, zzajj zzajj) {
        zzaif zzaif;
        try {
            Context context = this.zzup;
            zzazb zzazb = this.zzbll;
            if (zzabl.zzcuw.get().booleanValue()) {
                zzaif = new zzahr(context, zzazb);
            } else {
                zzaif = new zzaih(context, zzazb, zzdq, null);
            }
            zzaif.zza(new zzaiw(this, zzajj, zzaif));
            zzaif.zza("/jsLoaded", new zzaix(this, zzajj, zzaif));
            zzayd zzayd = new zzayd();
            zzaja zzaja = new zzaja(this, zzdq, zzaif, zzayd);
            zzayd.set(zzaja);
            zzaif.zza("/requestReload", zzaja);
            if (this.zzczl.endsWith(".js")) {
                zzaif.zzcv(this.zzczl);
            } else if (this.zzczl.startsWith("<html>")) {
                zzaif.zzcw(this.zzczl);
            } else {
                zzaif.zzcx(this.zzczl);
            }
            zzawb.zzdsr.postDelayed(new zzaiz(this, zzajj, zzaif), (long) zzajd.zzczz);
        } catch (Throwable th) {
            zzayu.zzc("Error creating webview.", th);
            zzq.zzku().zza(th, "SdkJavascriptFactory.loadJavascriptEngine");
            zzajj.reject();
        }
    }

    public zzais(Context context, zzazb zzazb, String str, zzaxh<zzaif> zzaxh, zzaxh<zzaif> zzaxh2) {
        this(context, zzazb, str);
        this.zzczm = zzaxh;
        this.zzczn = zzaxh2;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0029, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ void zza(com.google.android.gms.internal.ads.zzajj r4, com.google.android.gms.internal.ads.zzaif r5) {
        /*
            r3 = this;
            java.lang.Object r0 = r3.lock
            monitor-enter(r0)
            int r1 = r4.getStatus()     // Catch:{ all -> 0x002a }
            r2 = -1
            if (r1 == r2) goto L_0x0028
            int r1 = r4.getStatus()     // Catch:{ all -> 0x002a }
            r2 = 1
            if (r1 != r2) goto L_0x0012
            goto L_0x0028
        L_0x0012:
            r4.reject()     // Catch:{ all -> 0x002a }
            com.google.android.gms.internal.ads.zzdhd r4 = com.google.android.gms.internal.ads.zzazd.zzdwi     // Catch:{ all -> 0x002a }
            r5.getClass()     // Catch:{ all -> 0x002a }
            java.lang.Runnable r5 = com.google.android.gms.internal.ads.zzaiy.zzb(r5)     // Catch:{ all -> 0x002a }
            r4.execute(r5)     // Catch:{ all -> 0x002a }
            java.lang.String r4 = "Could not receive loaded message in a timely manner. Rejecting."
            com.google.android.gms.internal.ads.zzavs.zzed(r4)     // Catch:{ all -> 0x002a }
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            return
        L_0x0028:
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            return
        L_0x002a:
            r4 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzais.zza(com.google.android.gms.internal.ads.zzajj, com.google.android.gms.internal.ads.zzaif):void");
    }
}
