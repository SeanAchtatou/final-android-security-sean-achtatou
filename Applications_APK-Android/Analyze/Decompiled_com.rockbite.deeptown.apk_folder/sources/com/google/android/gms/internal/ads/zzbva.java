package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbva implements zzbov {
    private final zzbpd zzfjm;
    private final zzczl zzfjn;

    public zzbva(zzbpd zzbpd, zzczl zzczl) {
        this.zzfjm = zzbpd;
        this.zzfjn = zzczl;
    }

    public final void onAdClosed() {
    }

    public final void onAdLeftApplication() {
    }

    public final void onAdOpened() {
        int i2 = this.zzfjn.zzglz;
        if (i2 == 0 || i2 == 1) {
            this.zzfjm.onAdImpression();
        }
    }

    public final void onRewardedVideoCompleted() {
    }

    public final void onRewardedVideoStarted() {
    }

    public final void zzb(zzare zzare, String str, String str2) {
    }
}
