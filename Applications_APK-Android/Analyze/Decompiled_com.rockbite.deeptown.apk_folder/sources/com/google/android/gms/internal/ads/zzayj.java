package com.google.android.gms.internal.ads;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzayj extends BroadcastReceiver {
    private final /* synthetic */ zzayg zzdvg;

    zzayj(zzayg zzayg) {
        this.zzdvg = zzayg;
    }

    public final void onReceive(Context context, Intent intent) {
        this.zzdvg.zzc(context, intent);
    }
}
