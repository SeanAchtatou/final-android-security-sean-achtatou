package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.net.Uri;
import android.view.Surface;
import android.view.TextureView;
import com.appsflyer.share.Constants;
import com.google.android.gms.ads.internal.zzq;
import java.nio.ByteBuffer;
import java.util.Arrays;

@TargetApi(16)
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbbc extends zzbag implements TextureView.SurfaceTextureListener, zzbca {
    private Surface zzbht;
    private final zzbay zzdwy;
    private final boolean zzdwz;
    private int zzdxe;
    private int zzdxf;
    private int zzdxh;
    private int zzdxi;
    private zzbax zzdxj;
    private final boolean zzdxk;
    private zzbah zzdxm;
    private final zzbaz zzdxu;
    private String[] zzdyh;
    private final zzbaw zzebe;
    private zzbbs zzebf;
    private String zzebg;
    private boolean zzebh;
    private int zzebi = 1;
    private boolean zzebj;
    private boolean zzebk;
    private float zzebl;

    public zzbbc(Context context, zzbay zzbay, zzbaz zzbaz, boolean z, boolean z2, zzbaw zzbaw) {
        super(context);
        this.zzdwz = z2;
        this.zzdxu = zzbaz;
        this.zzdwy = zzbay;
        this.zzdxk = z;
        this.zzebe = zzbaw;
        setSurfaceTextureListener(this);
        this.zzdwy.zzb(this);
    }

    private final void zza(Surface surface, boolean z) {
        zzbbs zzbbs = this.zzebf;
        if (zzbbs != null) {
            zzbbs.zza(surface, z);
        } else {
            zzayu.zzez("Trying to set surface before player is initalized.");
        }
    }

    private final void zzo(int i2, int i3) {
        float f2 = i3 > 0 ? ((float) i2) / ((float) i3) : 1.0f;
        if (this.zzebl != f2) {
            this.zzebl = f2;
            requestLayout();
        }
    }

    private final zzbbs zzyx() {
        return new zzbbs(this.zzdxu.getContext(), this.zzebe);
    }

    private final String zzyy() {
        return zzq.zzkq().zzr(this.zzdxu.getContext(), this.zzdxu.zzyr().zzbma);
    }

    private final boolean zzyz() {
        return this.zzebf != null && !this.zzebh;
    }

    private final boolean zzza() {
        return zzyz() && this.zzebi != 1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzbbc.zza(android.view.Surface, boolean):void
     arg types: [android.view.Surface, int]
     candidates:
      com.google.android.gms.internal.ads.zzbbc.zza(float, boolean):void
      com.google.android.gms.internal.ads.zzbbc.zza(float, float):void
      com.google.android.gms.internal.ads.zzbbc.zza(java.lang.String, java.lang.Exception):void
      com.google.android.gms.internal.ads.zzbag.zza(float, float):void
      com.google.android.gms.internal.ads.zzbca.zza(java.lang.String, java.lang.Exception):void
      com.google.android.gms.internal.ads.zzbbc.zza(android.view.Surface, boolean):void */
    private final void zzzb() {
        String str;
        if (this.zzebf == null && (str = this.zzebg) != null && this.zzbht != null) {
            if (str.startsWith("cache:")) {
                zzbcn zzfe = this.zzdxu.zzfe(this.zzebg);
                if (zzfe instanceof zzbcy) {
                    this.zzebf = ((zzbcy) zzfe).zzzr();
                } else if (zzfe instanceof zzbcz) {
                    zzbcz zzbcz = (zzbcz) zzfe;
                    String zzyy = zzyy();
                    ByteBuffer byteBuffer = zzbcz.getByteBuffer();
                    boolean zzzs = zzbcz.zzzs();
                    String url = zzbcz.getUrl();
                    if (url == null) {
                        zzayu.zzez("Stream cache URL is null.");
                        return;
                    } else {
                        this.zzebf = zzyx();
                        this.zzebf.zza(new Uri[]{Uri.parse(url)}, zzyy, byteBuffer, zzzs);
                    }
                } else {
                    String valueOf = String.valueOf(this.zzebg);
                    zzayu.zzez(valueOf.length() != 0 ? "Stream cache miss: ".concat(valueOf) : new String("Stream cache miss: "));
                    return;
                }
            } else {
                this.zzebf = zzyx();
                String zzyy2 = zzyy();
                Uri[] uriArr = new Uri[this.zzdyh.length];
                int i2 = 0;
                while (true) {
                    String[] strArr = this.zzdyh;
                    if (i2 >= strArr.length) {
                        break;
                    }
                    uriArr[i2] = Uri.parse(strArr[i2]);
                    i2++;
                }
                this.zzebf.zza(uriArr, zzyy2);
            }
            this.zzebf.zza(this);
            zza(this.zzbht, false);
            this.zzebi = this.zzebf.zzzm().getPlaybackState();
            if (this.zzebi == 3) {
                zzzc();
            }
        }
    }

    private final void zzzc() {
        if (!this.zzebj) {
            this.zzebj = true;
            zzawb.zzdsr.post(new zzbbf(this));
            zzxs();
            this.zzdwy.zzer();
            if (this.zzebk) {
                play();
            }
        }
    }

    private final void zzzd() {
        zzo(this.zzdxe, this.zzdxf);
    }

    private final void zzze() {
        zzbbs zzbbs = this.zzebf;
        if (zzbbs != null) {
            zzbbs.zzaw(true);
        }
    }

    private final void zzzf() {
        zzbbs zzbbs = this.zzebf;
        if (zzbbs != null) {
            zzbbs.zzaw(false);
        }
    }

    public final int getCurrentPosition() {
        if (zzza()) {
            return (int) this.zzebf.zzzm().zzec();
        }
        return 0;
    }

    public final int getDuration() {
        if (zzza()) {
            return (int) this.zzebf.zzzm().getDuration();
        }
        return 0;
    }

    public final int getVideoHeight() {
        return this.zzdxf;
    }

    public final int getVideoWidth() {
        return this.zzdxe;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzbbc.zza(float, boolean):void
     arg types: [?, int]
     candidates:
      com.google.android.gms.internal.ads.zzbbc.zza(android.view.Surface, boolean):void
      com.google.android.gms.internal.ads.zzbbc.zza(float, float):void
      com.google.android.gms.internal.ads.zzbbc.zza(java.lang.String, java.lang.Exception):void
      com.google.android.gms.internal.ads.zzbag.zza(float, float):void
      com.google.android.gms.internal.ads.zzbca.zza(java.lang.String, java.lang.Exception):void
      com.google.android.gms.internal.ads.zzbbc.zza(float, boolean):void */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0080  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onMeasure(int r11, int r12) {
        /*
            r10 = this;
            super.onMeasure(r11, r12)
            int r11 = r10.getMeasuredWidth()
            int r12 = r10.getMeasuredHeight()
            float r0 = r10.zzebl
            r1 = 0
            int r2 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r2 == 0) goto L_0x002a
            com.google.android.gms.internal.ads.zzbax r2 = r10.zzdxj
            if (r2 != 0) goto L_0x002a
            float r2 = (float) r11
            float r3 = (float) r12
            float r3 = r2 / r3
            int r4 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r4 <= 0) goto L_0x0020
            float r2 = r2 / r0
            int r12 = (int) r2
        L_0x0020:
            float r0 = r10.zzebl
            int r2 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r2 >= 0) goto L_0x002a
            float r11 = (float) r12
            float r11 = r11 * r0
            int r11 = (int) r11
        L_0x002a:
            r10.setMeasuredDimension(r11, r12)
            com.google.android.gms.internal.ads.zzbax r0 = r10.zzdxj
            if (r0 == 0) goto L_0x0034
            r0.zzm(r11, r12)
        L_0x0034:
            int r0 = android.os.Build.VERSION.SDK_INT
            r2 = 16
            if (r0 != r2) goto L_0x00a2
            int r0 = r10.zzdxh
            if (r0 <= 0) goto L_0x0040
            if (r0 != r11) goto L_0x0046
        L_0x0040:
            int r0 = r10.zzdxi
            if (r0 <= 0) goto L_0x009e
            if (r0 == r12) goto L_0x009e
        L_0x0046:
            boolean r0 = r10.zzdwz
            if (r0 == 0) goto L_0x009e
            boolean r0 = r10.zzyz()
            if (r0 == 0) goto L_0x009e
            com.google.android.gms.internal.ads.zzbbs r0 = r10.zzebf
            com.google.android.gms.internal.ads.zzgk r0 = r0.zzzm()
            long r2 = r0.zzec()
            r4 = 0
            int r6 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r6 <= 0) goto L_0x009e
            boolean r2 = r0.zzea()
            if (r2 == 0) goto L_0x0067
            goto L_0x009e
        L_0x0067:
            r2 = 1
            r10.zza(r1, r2)
            r0.zzf(r2)
            long r1 = r0.zzec()
            com.google.android.gms.common.util.Clock r3 = com.google.android.gms.ads.internal.zzq.zzkx()
            long r3 = r3.currentTimeMillis()
        L_0x007a:
            boolean r5 = r10.zzyz()
            if (r5 == 0) goto L_0x0097
            long r5 = r0.zzec()
            int r7 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r7 != 0) goto L_0x0097
            com.google.android.gms.common.util.Clock r5 = com.google.android.gms.ads.internal.zzq.zzkx()
            long r5 = r5.currentTimeMillis()
            long r5 = r5 - r3
            r7 = 250(0xfa, double:1.235E-321)
            int r9 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r9 <= 0) goto L_0x007a
        L_0x0097:
            r1 = 0
            r0.zzf(r1)
            r10.zzxs()
        L_0x009e:
            r10.zzdxh = r11
            r10.zzdxi = r12
        L_0x00a2:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzbbc.onMeasure(int, int):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzbbc.zza(android.view.Surface, boolean):void
     arg types: [android.view.Surface, int]
     candidates:
      com.google.android.gms.internal.ads.zzbbc.zza(float, boolean):void
      com.google.android.gms.internal.ads.zzbbc.zza(float, float):void
      com.google.android.gms.internal.ads.zzbbc.zza(java.lang.String, java.lang.Exception):void
      com.google.android.gms.internal.ads.zzbag.zza(float, float):void
      com.google.android.gms.internal.ads.zzbca.zza(java.lang.String, java.lang.Exception):void
      com.google.android.gms.internal.ads.zzbbc.zza(android.view.Surface, boolean):void */
    public final void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i2, int i3) {
        if (this.zzdxk) {
            this.zzdxj = new zzbax(getContext());
            this.zzdxj.zza(surfaceTexture, i2, i3);
            this.zzdxj.start();
            SurfaceTexture zzyg = this.zzdxj.zzyg();
            if (zzyg != null) {
                surfaceTexture = zzyg;
            } else {
                this.zzdxj.zzyf();
                this.zzdxj = null;
            }
        }
        this.zzbht = new Surface(surfaceTexture);
        if (this.zzebf == null) {
            zzzb();
        } else {
            zza(this.zzbht, true);
            if (!this.zzebe.zzdze) {
                zzze();
            }
        }
        if (this.zzdxe == 0 || this.zzdxf == 0) {
            zzo(i2, i3);
        } else {
            zzzd();
        }
        zzawb.zzdsr.post(new zzbbi(this));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzbbc.zza(android.view.Surface, boolean):void
     arg types: [?[OBJECT, ARRAY], int]
     candidates:
      com.google.android.gms.internal.ads.zzbbc.zza(float, boolean):void
      com.google.android.gms.internal.ads.zzbbc.zza(float, float):void
      com.google.android.gms.internal.ads.zzbbc.zza(java.lang.String, java.lang.Exception):void
      com.google.android.gms.internal.ads.zzbag.zza(float, float):void
      com.google.android.gms.internal.ads.zzbca.zza(java.lang.String, java.lang.Exception):void
      com.google.android.gms.internal.ads.zzbbc.zza(android.view.Surface, boolean):void */
    public final boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        pause();
        zzbax zzbax = this.zzdxj;
        if (zzbax != null) {
            zzbax.zzyf();
            this.zzdxj = null;
        }
        if (this.zzebf != null) {
            zzzf();
            Surface surface = this.zzbht;
            if (surface != null) {
                surface.release();
            }
            this.zzbht = null;
            zza((Surface) null, true);
        }
        zzawb.zzdsr.post(new zzbbk(this));
        return true;
    }

    public final void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i2, int i3) {
        zzbax zzbax = this.zzdxj;
        if (zzbax != null) {
            zzbax.zzm(i2, i3);
        }
        zzawb.zzdsr.post(new zzbbl(this, i2, i3));
    }

    public final void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        this.zzdwy.zzc(this);
        this.zzdxs.zza(surfaceTexture, this.zzdxm);
    }

    /* access modifiers changed from: protected */
    public final void onWindowVisibilityChanged(int i2) {
        StringBuilder sb = new StringBuilder(57);
        sb.append("AdExoPlayerView3 window visibility changed to ");
        sb.append(i2);
        zzavs.zzed(sb.toString());
        zzawb.zzdsr.post(new zzbbn(this, i2));
        super.onWindowVisibilityChanged(i2);
    }

    public final void pause() {
        if (zzza()) {
            if (this.zzebe.zzdze) {
                zzzf();
            }
            this.zzebf.zzzm().zzf(false);
            this.zzdwy.zzyj();
            this.zzdxt.zzyj();
            zzawb.zzdsr.post(new zzbbj(this));
        }
    }

    public final void play() {
        if (zzza()) {
            if (this.zzebe.zzdze) {
                zzze();
            }
            this.zzebf.zzzm().zzf(true);
            this.zzdwy.zzyi();
            this.zzdxt.zzyi();
            this.zzdxs.zzxu();
            zzawb.zzdsr.post(new zzbbg(this));
            return;
        }
        this.zzebk = true;
    }

    public final void seekTo(int i2) {
        if (zzza()) {
            this.zzebf.zzzm().seekTo((long) i2);
        }
    }

    public final void setVideoPath(String str) {
        if (str != null) {
            this.zzebg = str;
            this.zzdyh = new String[]{str};
            zzzb();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzbbc.zza(android.view.Surface, boolean):void
     arg types: [?[OBJECT, ARRAY], int]
     candidates:
      com.google.android.gms.internal.ads.zzbbc.zza(float, boolean):void
      com.google.android.gms.internal.ads.zzbbc.zza(float, float):void
      com.google.android.gms.internal.ads.zzbbc.zza(java.lang.String, java.lang.Exception):void
      com.google.android.gms.internal.ads.zzbag.zza(float, float):void
      com.google.android.gms.internal.ads.zzbca.zza(java.lang.String, java.lang.Exception):void
      com.google.android.gms.internal.ads.zzbbc.zza(android.view.Surface, boolean):void */
    public final void stop() {
        if (zzyz()) {
            this.zzebf.zzzm().stop();
            if (this.zzebf != null) {
                zza((Surface) null, true);
                zzbbs zzbbs = this.zzebf;
                if (zzbbs != null) {
                    zzbbs.zza((zzbca) null);
                    this.zzebf.release();
                    this.zzebf = null;
                }
                this.zzebi = 1;
                this.zzebh = false;
                this.zzebj = false;
                this.zzebk = false;
            }
        }
        this.zzdwy.zzyj();
        this.zzdxt.zzyj();
        this.zzdwy.onStop();
    }

    public final void zzb(String str, String[] strArr) {
        if (str != null) {
            if (strArr == null) {
                setVideoPath(str);
            }
            this.zzebg = str;
            this.zzdyh = (String[]) Arrays.copyOf(strArr, strArr.length);
            zzzb();
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzc(boolean z, long j2) {
        this.zzdxu.zza(z, j2);
    }

    public final void zzcv(int i2) {
        zzbbs zzbbs = this.zzebf;
        if (zzbbs != null) {
            zzbbs.zzzp().zzdc(i2);
        }
    }

    public final void zzcw(int i2) {
        zzbbs zzbbs = this.zzebf;
        if (zzbbs != null) {
            zzbbs.zzzp().zzdd(i2);
        }
    }

    public final void zzcx(int i2) {
        zzbbs zzbbs = this.zzebf;
        if (zzbbs != null) {
            zzbbs.zzzp().zzcx(i2);
        }
    }

    public final void zzcy(int i2) {
        zzbbs zzbbs = this.zzebf;
        if (zzbbs != null) {
            zzbbs.zzzp().zzcy(i2);
        }
    }

    public final void zzcz(int i2) {
        zzbbs zzbbs = this.zzebf;
        if (zzbbs != null) {
            zzbbs.zzcz(i2);
        }
    }

    public final void zzda(int i2) {
        if (this.zzebi != i2) {
            this.zzebi = i2;
            if (i2 == 3) {
                zzzc();
            } else if (i2 == 4) {
                if (this.zzebe.zzdze) {
                    zzzf();
                }
                this.zzdwy.zzyj();
                this.zzdxt.zzyj();
                zzawb.zzdsr.post(new zzbbe(this));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzdb(int i2) {
        zzbah zzbah = this.zzdxm;
        if (zzbah != null) {
            zzbah.onWindowVisibilityChanged(i2);
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzff(String str) {
        zzbah zzbah = this.zzdxm;
        if (zzbah != null) {
            zzbah.zzm("ExoPlayerAdapter error", str);
        }
    }

    public final void zzn(int i2, int i3) {
        this.zzdxe = i2;
        this.zzdxf = i3;
        zzzd();
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzp(int i2, int i3) {
        zzbah zzbah = this.zzdxm;
        if (zzbah != null) {
            zzbah.zzk(i2, i3);
        }
    }

    public final String zzxo() {
        String str = this.zzdxk ? " spherical" : "";
        return str.length() != 0 ? "ExoPlayer/3".concat(str) : new String("ExoPlayer/3");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzbbc.zza(float, boolean):void
     arg types: [float, int]
     candidates:
      com.google.android.gms.internal.ads.zzbbc.zza(android.view.Surface, boolean):void
      com.google.android.gms.internal.ads.zzbbc.zza(float, float):void
      com.google.android.gms.internal.ads.zzbbc.zza(java.lang.String, java.lang.Exception):void
      com.google.android.gms.internal.ads.zzbag.zza(float, float):void
      com.google.android.gms.internal.ads.zzbca.zza(java.lang.String, java.lang.Exception):void
      com.google.android.gms.internal.ads.zzbbc.zza(float, boolean):void */
    public final void zzxs() {
        zza(this.zzdxt.getVolume(), false);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzzg() {
        zzbah zzbah = this.zzdxm;
        if (zzbah != null) {
            zzbah.zzxw();
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzzh() {
        zzbah zzbah = this.zzdxm;
        if (zzbah != null) {
            zzbah.zzxt();
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzzi() {
        zzbah zzbah = this.zzdxm;
        if (zzbah != null) {
            zzbah.onPaused();
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzzj() {
        zzbah zzbah = this.zzdxm;
        if (zzbah != null) {
            zzbah.zzxu();
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzzk() {
        zzbah zzbah = this.zzdxm;
        if (zzbah != null) {
            zzbah.zzxv();
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzzl() {
        zzbah zzbah = this.zzdxm;
        if (zzbah != null) {
            zzbah.zzer();
        }
    }

    private final void zza(float f2, boolean z) {
        zzbbs zzbbs = this.zzebf;
        if (zzbbs != null) {
            zzbbs.zzb(f2, z);
        } else {
            zzayu.zzez("Trying to set volume before player is initalized.");
        }
    }

    public final void zzb(boolean z, long j2) {
        if (this.zzdxu != null) {
            zzazd.zzdwi.execute(new zzbbm(this, z, j2));
        }
    }

    public final void zza(zzbah zzbah) {
        this.zzdxm = zzbah;
    }

    public final void zza(float f2, float f3) {
        zzbax zzbax = this.zzdxj;
        if (zzbax != null) {
            zzbax.zzb(f2, f3);
        }
    }

    public final void zza(String str, Exception exc) {
        String canonicalName = exc.getClass().getCanonicalName();
        String message = exc.getMessage();
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 2 + String.valueOf(canonicalName).length() + String.valueOf(message).length());
        sb.append(str);
        sb.append(Constants.URL_PATH_DELIMITER);
        sb.append(canonicalName);
        sb.append(":");
        sb.append(message);
        String sb2 = sb.toString();
        String valueOf = String.valueOf(sb2);
        zzayu.zzez(valueOf.length() != 0 ? "ExoPlayerAdapter error: ".concat(valueOf) : new String("ExoPlayerAdapter error: "));
        this.zzebh = true;
        if (this.zzebe.zzdze) {
            zzzf();
        }
        zzawb.zzdsr.post(new zzbbh(this, sb2));
    }
}
