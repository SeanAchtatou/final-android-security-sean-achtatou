package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbpu;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcna<AdT, AdapterT, ListenerT extends zzbpu> implements zzcio<AdT> {
    private final zzcis<AdapterT, ListenerT> zzfaq;
    private final zzdcr zzfgm;
    private final zzcir<AdT, AdapterT, ListenerT> zzgbg;
    private final zzdhd zzgbh;

    public zzcna(zzdcr zzdcr, zzdhd zzdhd, zzcis<AdapterT, ListenerT> zzcis, zzcir<AdT, AdapterT, ListenerT> zzcir) {
        this.zzfgm = zzdcr;
        this.zzgbh = zzdhd;
        this.zzgbg = zzcir;
        this.zzfaq = zzcis;
    }

    public final boolean zza(zzczt zzczt, zzczl zzczl) {
        return !zzczl.zzglp.isEmpty();
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ListenerT
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final com.google.android.gms.internal.ads.zzdhe<AdT> zzb(com.google.android.gms.internal.ads.zzczt r6, com.google.android.gms.internal.ads.zzczl r7) {
        /*
            r5 = this;
            java.lang.Class<com.google.ads.mediation.admob.AdMobAdapter> r0 = com.google.ads.mediation.admob.AdMobAdapter.class
            java.util.List<java.lang.String> r1 = r7.zzglp
            java.util.Iterator r1 = r1.iterator()
        L_0x0008:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x001f
            java.lang.Object r2 = r1.next()
            java.lang.String r2 = (java.lang.String) r2
            com.google.android.gms.internal.ads.zzcis<AdapterT, ListenerT> r3 = r5.zzfaq     // Catch:{ zzdab -> 0x001d }
            org.json.JSONObject r4 = r7.zzglr     // Catch:{ zzdab -> 0x001d }
            com.google.android.gms.internal.ads.zzcip r1 = r3.zzd(r2, r4)     // Catch:{ zzdab -> 0x001d }
            goto L_0x0020
        L_0x001d:
            goto L_0x0008
        L_0x001f:
            r1 = 0
        L_0x0020:
            if (r1 != 0) goto L_0x002e
            com.google.android.gms.internal.ads.zzclf r6 = new com.google.android.gms.internal.ads.zzclf
            java.lang.String r7 = "unable to instantiate mediation adapter class"
            r6.<init>(r7)
            com.google.android.gms.internal.ads.zzdhe r6 = com.google.android.gms.internal.ads.zzdgs.zzk(r6)
            return r6
        L_0x002e:
            com.google.android.gms.internal.ads.zzazl r2 = new com.google.android.gms.internal.ads.zzazl
            r2.<init>()
            com.google.android.gms.internal.ads.zzcnb r3 = new com.google.android.gms.internal.ads.zzcnb
            r3.<init>(r5, r2, r1)
            ListenerT r4 = r1.zzfyf
            r4.zza(r3)
            boolean r3 = r7.zzdmf
            if (r3 == 0) goto L_0x0065
            com.google.android.gms.internal.ads.zzczo r3 = r6.zzgmh
            com.google.android.gms.internal.ads.zzczu r3 = r3.zzfgl
            com.google.android.gms.internal.ads.zzug r3 = r3.zzgml
            android.os.Bundle r3 = r3.zzccf
            java.lang.String r4 = r0.getName()
            android.os.Bundle r4 = r3.getBundle(r4)
            if (r4 != 0) goto L_0x005f
            android.os.Bundle r4 = new android.os.Bundle
            r4.<init>()
            java.lang.String r0 = r0.getName()
            r3.putBundle(r0, r4)
        L_0x005f:
            r0 = 1
            java.lang.String r3 = "render_test_ad_label"
            r4.putBoolean(r3, r0)
        L_0x0065:
            com.google.android.gms.internal.ads.zzdcr r0 = r5.zzfgm
            com.google.android.gms.internal.ads.zzdco r3 = com.google.android.gms.internal.ads.zzdco.ADAPTER_LOAD_AD_SYN
            com.google.android.gms.internal.ads.zzdch r0 = r0.zzu(r3)
            com.google.android.gms.internal.ads.zzcmz r3 = new com.google.android.gms.internal.ads.zzcmz
            r3.<init>(r5, r6, r7, r1)
            com.google.android.gms.internal.ads.zzdhd r4 = r5.zzgbh
            com.google.android.gms.internal.ads.zzdcj r0 = r0.zza(r3, r4)
            com.google.android.gms.internal.ads.zzdco r3 = com.google.android.gms.internal.ads.zzdco.ADAPTER_LOAD_AD_ACK
            com.google.android.gms.internal.ads.zzdcj r0 = r0.zzw(r3)
            com.google.android.gms.internal.ads.zzdcj r0 = r0.zzc(r2)
            com.google.android.gms.internal.ads.zzdco r2 = com.google.android.gms.internal.ads.zzdco.ADAPTER_WRAP_ADAPTER
            com.google.android.gms.internal.ads.zzdcj r0 = r0.zzw(r2)
            com.google.android.gms.internal.ads.zzcnc r2 = new com.google.android.gms.internal.ads.zzcnc
            r2.<init>(r5, r6, r7, r1)
            com.google.android.gms.internal.ads.zzdcj r6 = r0.zzb(r2)
            com.google.android.gms.internal.ads.zzdca r6 = r6.zzaqg()
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzcna.zzb(com.google.android.gms.internal.ads.zzczt, com.google.android.gms.internal.ads.zzczl):com.google.android.gms.internal.ads.zzdhe");
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzd(zzczt zzczt, zzczl zzczl, zzcip zzcip) throws Exception {
        this.zzgbg.zza(zzczt, zzczl, zzcip);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Object zza(zzczt zzczt, zzczl zzczl, zzcip zzcip, Void voidR) throws Exception {
        return this.zzgbg.zzb(zzczt, zzczl, zzcip);
    }
}
