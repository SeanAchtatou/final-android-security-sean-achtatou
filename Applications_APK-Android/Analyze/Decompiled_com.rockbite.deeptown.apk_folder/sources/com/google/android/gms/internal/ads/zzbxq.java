package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import android.view.View;
import com.google.android.gms.common.util.Clock;
import java.lang.ref.WeakReference;
import java.util.HashMap;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbxq implements View.OnClickListener {
    private final Clock zzbmq;
    private final zzcaj zzfnx;
    private zzaeb zzfny;
    private zzafn<Object> zzfnz;
    String zzfoa;
    Long zzfob;
    WeakReference<View> zzfoc;

    public zzbxq(zzcaj zzcaj, Clock clock) {
        this.zzfnx = zzcaj;
        this.zzbmq = clock;
    }

    private final void zzaki() {
        View view;
        this.zzfoa = null;
        this.zzfob = null;
        WeakReference<View> weakReference = this.zzfoc;
        if (weakReference != null && (view = weakReference.get()) != null) {
            view.setClickable(false);
            view.setOnClickListener(null);
            this.zzfoc = null;
        }
    }

    public final void cancelUnconfirmedClick() {
        if (this.zzfny != null && this.zzfob != null) {
            zzaki();
            try {
                this.zzfny.onUnconfirmedClickCancelled();
            } catch (RemoteException e2) {
                zzayu.zze("#007 Could not call remote method.", e2);
            }
        }
    }

    public final void onClick(View view) {
        WeakReference<View> weakReference = this.zzfoc;
        if (weakReference != null && weakReference.get() == view) {
            if (!(this.zzfoa == null || this.zzfob == null)) {
                HashMap hashMap = new HashMap();
                hashMap.put("id", this.zzfoa);
                hashMap.put("time_interval", String.valueOf(this.zzbmq.currentTimeMillis() - this.zzfob.longValue()));
                hashMap.put("messageType", "onePointFiveClick");
                this.zzfnx.zza("sendMessageToNativeJs", hashMap);
            }
            zzaki();
        }
    }

    public final void zza(zzaeb zzaeb) {
        this.zzfny = zzaeb;
        zzafn<Object> zzafn = this.zzfnz;
        if (zzafn != null) {
            this.zzfnx.zzb("/unconfirmedClick", zzafn);
        }
        this.zzfnz = new zzbxp(this, zzaeb);
        this.zzfnx.zza("/unconfirmedClick", this.zzfnz);
    }

    public final zzaeb zzakh() {
        return this.zzfny;
    }
}
