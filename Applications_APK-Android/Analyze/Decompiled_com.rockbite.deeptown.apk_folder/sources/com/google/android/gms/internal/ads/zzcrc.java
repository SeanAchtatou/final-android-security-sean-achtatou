package com.google.android.gms.internal.ads;

import android.os.Bundle;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcrc implements zzcty<Bundle> {
    private final float zzdje;
    private final int zzdmh;
    private final boolean zzdmi;
    private final boolean zzdmj;
    private final int zzdmk;
    private final int zzdml;
    private final int zzdmm;
    private final boolean zzgfj;

    public zzcrc(int i2, boolean z, boolean z2, int i3, int i4, int i5, float f2, boolean z3) {
        this.zzdmh = i2;
        this.zzdmi = z;
        this.zzdmj = z2;
        this.zzdmk = i3;
        this.zzdml = i4;
        this.zzdmm = i5;
        this.zzdje = f2;
        this.zzgfj = z3;
    }

    public final /* synthetic */ void zzr(Object obj) {
        Bundle bundle = (Bundle) obj;
        bundle.putInt("am", this.zzdmh);
        bundle.putBoolean("ma", this.zzdmi);
        bundle.putBoolean("sp", this.zzdmj);
        bundle.putInt("muv", this.zzdmk);
        bundle.putInt("rm", this.zzdml);
        bundle.putInt("riv", this.zzdmm);
        bundle.putFloat("android_app_volume", this.zzdje);
        bundle.putBoolean("android_app_muted", this.zzgfj);
    }
}
