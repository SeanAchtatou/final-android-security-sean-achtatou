package com.google.android.gms.internal.ads;

import com.google.android.gms.common.internal.Preconditions;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzajj extends zzazo<zzaif> {
    private final Object lock = new Object();
    /* access modifiers changed from: private */
    public zzaxh<zzaif> zzczn;
    private boolean zzdae;
    private int zzdaf;

    public zzajj(zzaxh<zzaif> zzaxh) {
        this.zzczn = zzaxh;
        this.zzdae = false;
        this.zzdaf = 0;
    }

    private final void zzsf() {
        synchronized (this.lock) {
            Preconditions.checkState(this.zzdaf >= 0);
            if (!this.zzdae || this.zzdaf != 0) {
                zzavs.zzed("There are still references to the engine. Not destroying.");
            } else {
                zzavs.zzed("No reference is left (including root). Cleaning up engine.");
                zza(new zzajo(this), new zzazm());
            }
        }
    }

    public final zzajf zzsc() {
        zzajf zzajf = new zzajf(this);
        synchronized (this.lock) {
            zza(new zzajm(this, zzajf), new zzajl(this, zzajf));
            Preconditions.checkState(this.zzdaf >= 0);
            this.zzdaf++;
        }
        return zzajf;
    }

    /* access modifiers changed from: protected */
    public final void zzsd() {
        synchronized (this.lock) {
            Preconditions.checkState(this.zzdaf > 0);
            zzavs.zzed("Releasing 1 reference for JS Engine");
            this.zzdaf--;
            zzsf();
        }
    }

    public final void zzse() {
        synchronized (this.lock) {
            Preconditions.checkState(this.zzdaf >= 0);
            zzavs.zzed("Releasing root reference. JS Engine will be destroyed once other references are released.");
            this.zzdae = true;
            zzsf();
        }
    }
}
