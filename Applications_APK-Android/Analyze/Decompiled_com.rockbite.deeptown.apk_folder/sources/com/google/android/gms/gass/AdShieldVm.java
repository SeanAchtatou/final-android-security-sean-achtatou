package com.google.android.gms.gass;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Base64;
import android.view.MotionEvent;
import android.view.View;
import com.google.android.gms.gass.internal.Program;
import com.tapjoy.TJAdUnitConstants;
import dalvik.system.DexClassLoader;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
public class AdShieldVm implements AdShieldHandle {
    private Program zzgrz;
    private Object zzgsa;
    private final Context zzup;
    private final AdShield2Logger zzus;

    public AdShieldVm(Context context, AdShield2Logger adShield2Logger) {
        this.zzup = context;
        this.zzus = adShield2Logger;
    }

    private final Object zza(Class<?> cls, Program program) {
        long currentTimeMillis = System.currentTimeMillis();
        try {
            return cls.getDeclaredConstructor(Context.class, String.class, byte[].class, Object.class, Bundle.class, Integer.TYPE).newInstance(this.zzup, "msa-r", program.getBytecodeFileContents(), null, new Bundle(), 2);
        } catch (IllegalAccessException e2) {
            zza(2004, currentTimeMillis, e2);
            return null;
        } catch (InstantiationException e3) {
            zza(2004, currentTimeMillis, e3);
            return null;
        } catch (NoSuchMethodException e4) {
            zza(2004, currentTimeMillis, e4);
            return null;
        } catch (InvocationTargetException e5) {
            zza(2004, currentTimeMillis, e5);
            return null;
        }
    }

    private final byte[] zzb(Map<String, String> map, Map<String, Object> map2) {
        long currentTimeMillis = System.currentTimeMillis();
        Object obj = this.zzgsa;
        if (obj == null) {
            return null;
        }
        try {
            return (byte[]) obj.getClass().getDeclaredMethod("xss", Map.class, Map.class).invoke(this.zzgsa, null, map2);
        } catch (IllegalAccessException e2) {
            zza((int) AdShield2Logger.EVENTID_VM_SNAP_EXCEPTION, currentTimeMillis, e2);
            return null;
        } catch (NoSuchMethodException e3) {
            zza((int) AdShield2Logger.EVENTID_VM_SNAP_EXCEPTION, currentTimeMillis, e3);
            return null;
        } catch (InvocationTargetException e4) {
            zza((int) AdShield2Logger.EVENTID_VM_SNAP_EXCEPTION, currentTimeMillis, e4);
            return null;
        }
    }

    private static String zzj(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        return Base64.encodeToString(bArr, 11);
    }

    private final synchronized void zzx(Object obj) {
        if (this.zzgsa != null) {
            close();
        }
        this.zzgsa = obj;
    }

    private final synchronized int zzy(Object obj) {
        long currentTimeMillis = System.currentTimeMillis();
        try {
        } catch (Exception e2) {
            zza(2006, currentTimeMillis, e2);
            return 0;
        }
        return ((Integer) obj.getClass().getDeclaredMethod("lcs", new Class[0]).invoke(obj, new Object[0])).intValue();
    }

    public synchronized void close() {
        long currentTimeMillis = System.currentTimeMillis();
        try {
            this.zzgsa.getClass().getDeclaredMethod(TJAdUnitConstants.String.CLOSE, new Class[0]).invoke(this.zzgsa, new Object[0]);
            zza(3001, (Exception) null, currentTimeMillis);
        } catch (Exception e2) {
            zza(2003, currentTimeMillis, e2);
        }
    }

    public synchronized String getClickSignals(Context context, String str, String str2, View view, Activity activity) {
        HashMap hashMap;
        hashMap = new HashMap();
        hashMap.put("ctx", context);
        hashMap.put("clickString", str2);
        hashMap.put("aid", str);
        hashMap.put("view", view);
        hashMap.put("act", activity);
        return zzj(zzb(null, hashMap));
    }

    public synchronized String getImpressionSignals(Context context, String str, View view, Activity activity) {
        HashMap hashMap;
        hashMap = new HashMap();
        hashMap.put("ctx", context);
        hashMap.put("aid", str);
        hashMap.put("view", view);
        hashMap.put("act", activity);
        return zzj(zzb(null, hashMap));
    }

    public int getLastCrashInformation() {
        Object obj = this.zzgsa;
        if (obj != null) {
            return zzy(obj);
        }
        return 0;
    }

    public Program getProgram() {
        return this.zzgrz;
    }

    public synchronized String getQuerySignals(Context context, String str) {
        HashMap hashMap;
        hashMap = new HashMap();
        hashMap.put("ctx", context);
        hashMap.put("aid", str);
        return zzj(zzb(null, hashMap));
    }

    public AdShieldError initializedVmAndProgram(Program program) {
        long currentTimeMillis = System.currentTimeMillis();
        Class<?> loadVmClass = loadVmClass(program);
        if (loadVmClass == null) {
            return new AdShieldError(2, "lc");
        }
        Object zza = zza(loadVmClass, program);
        if (zza == null) {
            return new AdShieldError(3, "it");
        }
        if (runVmInit(zza)) {
            int zzy = zzy(zza);
            if (zzy != 0) {
                this.zzus.logSignals(4001, System.currentTimeMillis() - currentTimeMillis, Integer.toString(zzy), null);
                StringBuilder sb = new StringBuilder(13);
                sb.append("ci");
                sb.append(zzy);
                return new AdShieldError(5, sb.toString());
            }
            zzx(zza);
            this.zzgrz = program;
            zza(3000, (Exception) null, currentTimeMillis);
            return null;
        }
        zza(4000, (Exception) null, currentTimeMillis);
        return new AdShieldError(4, "ri");
    }

    public Class<?> loadVmClass(Program program) {
        File optFile = program.getOptFile();
        File vmFile = program.getVmFile();
        long currentTimeMillis = System.currentTimeMillis();
        try {
            return new DexClassLoader(vmFile.getAbsolutePath(), optFile.getAbsolutePath(), null, this.zzup.getClassLoader()).loadClass("com.google.ccc.abuse.droidguard.DroidGuard");
        } catch (ClassNotFoundException e2) {
            zza(2008, currentTimeMillis, e2);
            return null;
        }
    }

    public synchronized void reportTouchEvent(String str, MotionEvent motionEvent) {
        long currentTimeMillis = System.currentTimeMillis();
        try {
            HashMap hashMap = new HashMap();
            hashMap.put("aid", str);
            hashMap.put("evt", motionEvent);
            this.zzgsa.getClass().getDeclaredMethod("he", Map.class).invoke(this.zzgsa, hashMap);
            zza(3003, (Exception) null, currentTimeMillis);
        } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e2) {
            zza((int) AdShield2Logger.EVENTID_VM_TOUCH_EXCEPTION, currentTimeMillis, e2);
        }
    }

    public boolean runVmInit(Object obj) {
        long currentTimeMillis = System.currentTimeMillis();
        try {
            return ((Boolean) obj.getClass().getDeclaredMethod("init", new Class[0]).invoke(obj, new Object[0])).booleanValue();
        } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e2) {
            zza(2001, currentTimeMillis, e2);
            return false;
        }
    }

    private final void zza(int i2, long j2, Exception exc) {
        zza(i2, exc, j2);
    }

    private final void zza(int i2, Exception exc, long j2) {
        this.zzus.logException(i2, System.currentTimeMillis() - j2, exc);
    }
}
