package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.res.Configuration;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.google.android.gms.ads.internal.zzq;

@TargetApi(24)
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public class zzawm extends zzawn {
    private static boolean zze(int i2, int i3, int i4) {
        return Math.abs(i2 - i3) <= i4;
    }

    public final boolean zza(Activity activity, Configuration configuration) {
        if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcns)).booleanValue()) {
            return false;
        }
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcnu)).booleanValue()) {
            return activity.isInMultiWindowMode();
        }
        zzve.zzou();
        int zza = zzayk.zza(activity, configuration.screenHeightDp);
        int zza2 = zzayk.zza(activity, configuration.screenWidthDp);
        zzq.zzkq();
        DisplayMetrics zza3 = zzawb.zza((WindowManager) activity.getApplicationContext().getSystemService("window"));
        int i2 = zza3.heightPixels;
        int i3 = zza3.widthPixels;
        int identifier = activity.getResources().getIdentifier("status_bar_height", "dimen", "android");
        int dimensionPixelSize = identifier > 0 ? activity.getResources().getDimensionPixelSize(identifier) : 0;
        double d2 = (double) activity.getResources().getDisplayMetrics().density;
        Double.isNaN(d2);
        int round = ((int) Math.round(d2 + 0.5d)) * ((Integer) zzve.zzoy().zzd(zzzn.zzcnr)).intValue();
        if (!(zze(i2, zza + dimensionPixelSize, round) && zze(i3, zza2, round))) {
            return true;
        }
        return false;
    }
}
