package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbbl implements Runnable {
    private final int zzdtf;
    private final int zzdtg;
    private final zzbbc zzebm;

    zzbbl(zzbbc zzbbc, int i2, int i3) {
        this.zzebm = zzbbc;
        this.zzdtf = i2;
        this.zzdtg = i3;
    }

    public final void run() {
        this.zzebm.zzp(this.zzdtf, this.zzdtg);
    }
}
