package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.overlay.zzo;
import com.google.android.gms.common.util.Clock;
import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbjd implements zzo, zzbpe, zzbph, zzps {
    private final Clock zzbmq;
    private final zzbiy zzfce;
    private final zzbjb zzfcf;
    private final Set<zzbdi> zzfcg = new HashSet();
    private final zzako<JSONObject, JSONObject> zzfch;
    private final Executor zzfci;
    private final AtomicBoolean zzfcj = new AtomicBoolean(false);
    private final zzbjf zzfck = new zzbjf();
    private boolean zzfcl = false;
    private WeakReference<?> zzfcm = new WeakReference<>(this);

    public zzbjd(zzakh zzakh, zzbjb zzbjb, Executor executor, zzbiy zzbiy, Clock clock) {
        this.zzfce = zzbiy;
        zzajy<JSONObject> zzajy = zzajx.zzdaq;
        this.zzfch = zzakh.zzb("google.afma.activeView.handleUpdate", zzajy, zzajy);
        this.zzfcf = zzbjb;
        this.zzfci = executor;
        this.zzbmq = clock;
    }

    private final void zzafo() {
        for (zzbdi zze : this.zzfcg) {
            this.zzfce.zze(zze);
        }
        this.zzfce.zzafm();
    }

    public final synchronized void onAdImpression() {
        if (this.zzfcj.compareAndSet(false, true)) {
            this.zzfce.zza(this);
            zzafn();
        }
    }

    public final synchronized void onPause() {
        this.zzfck.zzfco = true;
        zzafn();
    }

    public final synchronized void onResume() {
        this.zzfck.zzfco = false;
        zzafn();
    }

    public final synchronized void zza(zzpt zzpt) {
        this.zzfck.zzbnq = zzpt.zzbnq;
        this.zzfck.zzfcr = zzpt;
        zzafn();
    }

    public final synchronized void zzafn() {
        if (!(this.zzfcm.get() != null)) {
            zzafp();
        } else if (!this.zzfcl && this.zzfcj.get()) {
            try {
                this.zzfck.timestamp = this.zzbmq.elapsedRealtime();
                JSONObject zza = this.zzfcf.zzj(this.zzfck);
                for (zzbdi zzbjg : this.zzfcg) {
                    this.zzfci.execute(new zzbjg(zzbjg, zza));
                }
                zzazh.zzb(this.zzfch.zzf(zza), "ActiveViewListener.callActiveViewJs");
            } catch (Exception e2) {
                zzavs.zza("Failed to call ActiveViewJS", e2);
            }
        }
    }

    public final synchronized void zzafp() {
        zzafo();
        this.zzfcl = true;
    }

    public final synchronized void zzbv(Context context) {
        this.zzfck.zzfco = true;
        zzafn();
    }

    public final synchronized void zzbw(Context context) {
        this.zzfck.zzfco = false;
        zzafn();
    }

    public final synchronized void zzbx(Context context) {
        this.zzfck.zzfcq = "u";
        zzafn();
        zzafo();
        this.zzfcl = true;
    }

    public final synchronized void zzf(zzbdi zzbdi) {
        this.zzfcg.add(zzbdi);
        this.zzfce.zzd(zzbdi);
    }

    public final void zzo(Object obj) {
        this.zzfcm = new WeakReference<>(obj);
    }

    public final void zzte() {
    }

    public final void zztf() {
    }
}
