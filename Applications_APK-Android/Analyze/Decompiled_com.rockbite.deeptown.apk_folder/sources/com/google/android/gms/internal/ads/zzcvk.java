package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcvk implements zzdxg<zzcvi> {
    private final zzdxp<zzauw> zzeqj;
    private final zzdxp<zzdhd> zzfcv;
    private final zzdxp<String> zzfrr;

    public zzcvk(zzdxp<zzauw> zzdxp, zzdxp<zzdhd> zzdxp2, zzdxp<String> zzdxp3) {
        this.zzeqj = zzdxp;
        this.zzfcv = zzdxp2;
        this.zzfrr = zzdxp3;
    }

    public final /* synthetic */ Object get() {
        return new zzcvi(this.zzeqj.get(), this.zzfcv.get(), this.zzfrr.get());
    }
}
