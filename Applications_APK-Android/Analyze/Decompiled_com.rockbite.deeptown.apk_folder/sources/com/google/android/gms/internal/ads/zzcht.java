package com.google.android.gms.internal.ads;

import android.database.sqlite.SQLiteDatabase;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcht {
    private final zzchr zzfwu;
    private final zzdhd zzfwv;

    public zzcht(zzchr zzchr, zzdhd zzdhd) {
        this.zzfwu = zzchr;
        this.zzfwv = zzdhd;
    }

    public final void zza(zzdby<SQLiteDatabase, Void> zzdby) {
        zzdhd zzdhd = this.zzfwv;
        zzchr zzchr = this.zzfwu;
        zzchr.getClass();
        zzdgs.zza(zzdhd.zzd(zzchw.zza(zzchr)), new zzchv(this, zzdby), this.zzfwv);
    }
}
