package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.overlay.zzo;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbql implements zzbrn {
    static final zzbrn zzfhp = new zzbql();

    private zzbql() {
    }

    public final void zzp(Object obj) {
        ((zzo) obj).onPause();
    }
}
