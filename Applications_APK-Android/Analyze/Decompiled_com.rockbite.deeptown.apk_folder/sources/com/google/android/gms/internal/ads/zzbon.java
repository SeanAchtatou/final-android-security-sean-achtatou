package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbod;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbon implements zzdxg<zzbod.zza> {
    private final zzbod zzfhi;

    private zzbon(zzbod zzbod) {
        this.zzfhi = zzbod;
    }

    public static zzbon zzl(zzbod zzbod) {
        return new zzbon(zzbod);
    }

    public final /* synthetic */ Object get() {
        return (zzbod.zza) zzdxm.zza(this.zzfhi.zzahc(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
