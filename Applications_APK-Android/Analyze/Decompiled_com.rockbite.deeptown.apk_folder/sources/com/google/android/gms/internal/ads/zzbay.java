package com.google.android.gms.internal.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzq;
import java.util.concurrent.TimeUnit;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbay {
    private final zzazb zzdij;
    private final String zzdiy;
    private final zzaae zzdxw;
    private boolean zzdya;
    private final zzaac zzeao;
    private final zzaxg zzeap = new zzaxl().zza("min_1", Double.MIN_VALUE, 1.0d).zza("1_5", 1.0d, 5.0d).zza("5_10", 5.0d, 10.0d).zza("10_20", 10.0d, 20.0d).zza("20_30", 20.0d, 30.0d).zza("30_max", 30.0d, Double.MAX_VALUE).zzxa();
    private final long[] zzeaq;
    private final String[] zzear;
    private boolean zzeas = false;
    private boolean zzeat = false;
    private boolean zzeau = false;
    private boolean zzeav = false;
    private zzbag zzeaw;
    private boolean zzeax;
    private boolean zzeay;
    private long zzeaz = -1;
    private final Context zzup;

    public zzbay(Context context, zzazb zzazb, String str, zzaae zzaae, zzaac zzaac) {
        this.zzup = context;
        this.zzdij = zzazb;
        this.zzdiy = str;
        this.zzdxw = zzaae;
        this.zzeao = zzaac;
        String str2 = (String) zzve.zzoy().zzd(zzzn.zzcha);
        if (str2 == null) {
            this.zzear = new String[0];
            this.zzeaq = new long[0];
            return;
        }
        String[] split = TextUtils.split(str2, ",");
        this.zzear = new String[split.length];
        this.zzeaq = new long[split.length];
        for (int i2 = 0; i2 < split.length; i2++) {
            try {
                this.zzeaq[i2] = Long.parseLong(split[i2]);
            } catch (NumberFormatException e2) {
                zzayu.zzd("Unable to parse frame hash target time number.", e2);
                this.zzeaq[i2] = -1;
            }
        }
    }

    public final void onStop() {
        if (zzabm.zzcux.get().booleanValue() && !this.zzeax) {
            Bundle bundle = new Bundle();
            bundle.putString("type", "native-player-metrics");
            bundle.putString("request", this.zzdiy);
            bundle.putString("player", this.zzeaw.zzxo());
            for (zzaxi next : this.zzeap.zzwz()) {
                String valueOf = String.valueOf(next.name);
                bundle.putString(valueOf.length() != 0 ? "fps_c_".concat(valueOf) : new String("fps_c_"), Integer.toString(next.count));
                String valueOf2 = String.valueOf(next.name);
                bundle.putString(valueOf2.length() != 0 ? "fps_p_".concat(valueOf2) : new String("fps_p_"), Double.toString(next.zzdtz));
            }
            int i2 = 0;
            while (true) {
                long[] jArr = this.zzeaq;
                if (i2 < jArr.length) {
                    String str = this.zzear[i2];
                    if (str != null) {
                        String valueOf3 = String.valueOf(Long.valueOf(jArr[i2]));
                        StringBuilder sb = new StringBuilder(String.valueOf(valueOf3).length() + 3);
                        sb.append("fh_");
                        sb.append(valueOf3);
                        bundle.putString(sb.toString(), str);
                    }
                    i2++;
                } else {
                    zzq.zzkq().zza(this.zzup, this.zzdij.zzbma, "gmob-apps", bundle, true);
                    this.zzeax = true;
                    return;
                }
            }
        }
    }

    public final void zzb(zzbag zzbag) {
        zzzv.zza(this.zzdxw, this.zzeao, "vpc2");
        this.zzeas = true;
        zzaae zzaae = this.zzdxw;
        if (zzaae != null) {
            zzaae.zzh("vpn", zzbag.zzxo());
        }
        this.zzeaw = zzbag;
    }

    public final void zzc(zzbag zzbag) {
        if (this.zzeau && !this.zzeav) {
            if (zzavs.zzvs() && !this.zzeav) {
                zzavs.zzed("VideoMetricsMixin first frame");
            }
            zzzv.zza(this.zzdxw, this.zzeao, "vff2");
            this.zzeav = true;
        }
        long nanoTime = zzq.zzkx().nanoTime();
        if (this.zzdya && this.zzeay && this.zzeaz != -1) {
            double nanos = (double) TimeUnit.SECONDS.toNanos(1);
            double d2 = (double) (nanoTime - this.zzeaz);
            Double.isNaN(nanos);
            Double.isNaN(d2);
            this.zzeap.zza(nanos / d2);
        }
        this.zzeay = this.zzdya;
        this.zzeaz = nanoTime;
        long longValue = ((Long) zzve.zzoy().zzd(zzzn.zzchb)).longValue();
        long currentPosition = (long) zzbag.getCurrentPosition();
        int i2 = 0;
        while (true) {
            String[] strArr = this.zzear;
            if (i2 >= strArr.length) {
                return;
            }
            if (strArr[i2] != null || longValue <= Math.abs(currentPosition - this.zzeaq[i2])) {
                i2++;
            } else {
                String[] strArr2 = this.zzear;
                int i3 = 8;
                Bitmap bitmap = zzbag.getBitmap(8, 8);
                long j2 = 63;
                int i4 = 0;
                long j3 = 0;
                while (i4 < i3) {
                    long j4 = j2;
                    int i5 = 0;
                    while (i5 < i3) {
                        int pixel = bitmap.getPixel(i5, i4);
                        j3 |= ((Color.blue(pixel) + Color.red(pixel)) + Color.green(pixel) > 128 ? 1 : 0) << ((int) j4);
                        i5++;
                        j4--;
                        i3 = 8;
                    }
                    i4++;
                    j2 = j4;
                    i3 = 8;
                }
                strArr2[i2] = String.format("%016X", Long.valueOf(j3));
                return;
            }
        }
    }

    public final void zzer() {
        if (this.zzeas && !this.zzeat) {
            zzzv.zza(this.zzdxw, this.zzeao, "vfr2");
            this.zzeat = true;
        }
    }

    public final void zzyi() {
        this.zzdya = true;
        if (this.zzeat && !this.zzeau) {
            zzzv.zza(this.zzdxw, this.zzeao, "vfp2");
            this.zzeau = true;
        }
    }

    public final void zzyj() {
        this.zzdya = false;
    }
}
