package com.google.android.gms.internal.ads;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbar extends zzbaj {
    public final zzbag zza(Context context, zzbaz zzbaz, int i2, boolean z, zzaae zzaae, zzbaw zzbaw) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        boolean z2 = false;
        if (!(applicationInfo == null || applicationInfo.targetSdkVersion >= 11)) {
            return null;
        }
        zzbay zzbay = new zzbay(context, zzbaz.zzyr(), zzbaz.zzyp(), zzaae, zzbaz.zzym());
        if (Build.VERSION.SDK_INT >= 16 && i2 == 2) {
            z2 = true;
        }
        if (z2) {
            return new zzbbc(context, zzbay, zzbaz, z, zzbaj.zzb(zzbaz), zzbaw);
        }
        return new zzazx(context, z, zzbaj.zzb(zzbaz), zzbaw, new zzbay(context, zzbaz.zzyr(), zzbaz.zzyp(), zzaae, zzbaz.zzym()));
    }
}
