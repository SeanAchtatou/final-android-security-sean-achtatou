package com.google.android.gms.ads;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.RemoteException;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.ads.zzaot;
import com.google.android.gms.internal.ads.zzayu;
import com.google.android.gms.internal.ads.zzve;

@KeepForSdk
/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class AdActivity extends Activity {
    @KeepForSdk
    public static final String CLASS_NAME = "com.google.android.gms.ads.AdActivity";
    @KeepForSdk
    public static final String SIMPLE_CLASS_NAME = "AdActivity";
    private zzaot zzaaz;

    private final void zzdf() {
        zzaot zzaot = this.zzaaz;
        if (zzaot != null) {
            try {
                zzaot.zzdf();
            } catch (RemoteException e2) {
                zzayu.zze("#007 Could not call remote method.", e2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void onActivityResult(int i2, int i3, Intent intent) {
        try {
            this.zzaaz.onActivityResult(i2, i3, intent);
        } catch (Exception e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
        }
        super.onActivityResult(i2, i3, intent);
    }

    public final void onBackPressed() {
        boolean z = true;
        try {
            if (this.zzaaz != null) {
                z = this.zzaaz.zztm();
            }
        } catch (RemoteException e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
        }
        if (z) {
            super.onBackPressed();
        }
    }

    public final void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        try {
            this.zzaaz.zzad(ObjectWrapper.wrap(configuration));
        } catch (RemoteException e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
        }
    }

    /* access modifiers changed from: protected */
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.zzaaz = zzve.zzov().zzb(this);
        zzaot zzaot = this.zzaaz;
        if (zzaot == null) {
            zzayu.zze("#007 Could not call remote method.", null);
            finish();
            return;
        }
        try {
            zzaot.onCreate(bundle);
        } catch (RemoteException e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public final void onDestroy() {
        try {
            if (this.zzaaz != null) {
                this.zzaaz.onDestroy();
            }
        } catch (RemoteException e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public final void onPause() {
        try {
            if (this.zzaaz != null) {
                this.zzaaz.onPause();
            }
        } catch (RemoteException e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
            finish();
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public final void onRestart() {
        super.onRestart();
        try {
            if (this.zzaaz != null) {
                this.zzaaz.onRestart();
            }
        } catch (RemoteException e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public final void onResume() {
        super.onResume();
        try {
            if (this.zzaaz != null) {
                this.zzaaz.onResume();
            }
        } catch (RemoteException e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public final void onSaveInstanceState(Bundle bundle) {
        try {
            if (this.zzaaz != null) {
                this.zzaaz.onSaveInstanceState(bundle);
            }
        } catch (RemoteException e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
            finish();
        }
        super.onSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public final void onStart() {
        super.onStart();
        try {
            if (this.zzaaz != null) {
                this.zzaaz.onStart();
            }
        } catch (RemoteException e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public final void onStop() {
        try {
            if (this.zzaaz != null) {
                this.zzaaz.onStop();
            }
        } catch (RemoteException e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
            finish();
        }
        super.onStop();
    }

    public final void setContentView(int i2) {
        super.setContentView(i2);
        zzdf();
    }

    public final void setContentView(View view) {
        super.setContentView(view);
        zzdf();
    }

    public final void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        super.setContentView(view, layoutParams);
        zzdf();
    }
}
