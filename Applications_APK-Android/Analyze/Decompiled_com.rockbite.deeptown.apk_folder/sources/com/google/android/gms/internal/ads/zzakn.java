package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzakn implements zzdgf {
    private final zzako zzdbg;
    private final Object zzdbh;

    zzakn(zzako zzako, Object obj) {
        this.zzdbg = zzako;
        this.zzdbh = obj;
    }

    public final zzdhe zzf(Object obj) {
        return this.zzdbg.zza(this.zzdbh, (zzajq) obj);
    }
}
