package com.google.android.gms.internal.ads;

import java.lang.ref.WeakReference;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzbqq implements Runnable {
    private final WeakReference<zzbqp> zzfhv;

    private zzbqq(zzbqp zzbqp) {
        this.zzfhv = new WeakReference<>(zzbqp);
    }

    public final void run() {
        zzbqp zzbqp = this.zzfhv.get();
        if (zzbqp != null) {
            zzbqp.zzahk();
        }
    }
}
