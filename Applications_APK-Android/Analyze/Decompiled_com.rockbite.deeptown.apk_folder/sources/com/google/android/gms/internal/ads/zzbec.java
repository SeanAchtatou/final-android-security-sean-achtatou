package com.google.android.gms.internal.ads;

import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbec implements Runnable {
    private final Map zzdvw;
    private final zzbed zzehe;

    zzbec(zzbed zzbed, Map map) {
        this.zzehe = zzbed;
        this.zzdvw = map;
    }

    public final void run() {
        this.zzehe.zzj(this.zzdvw);
    }
}
