package com.google.android.gms.internal.ads;

import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzcph implements zzdgt<zzcps> {
    private final /* synthetic */ zzaun zzgdz;

    zzcph(zzcpi zzcpi, zzaun zzaun) {
        this.zzgdz = zzaun;
    }

    public final /* synthetic */ void onSuccess(Object obj) {
        zzcps zzcps = (zzcps) obj;
        try {
            this.zzgdz.zzk(zzcps.zzgeg, zzcps.zzgeh);
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
        }
    }

    public final void zzb(Throwable th) {
        try {
            this.zzgdz.onError("Internal error.");
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
        }
    }
}
