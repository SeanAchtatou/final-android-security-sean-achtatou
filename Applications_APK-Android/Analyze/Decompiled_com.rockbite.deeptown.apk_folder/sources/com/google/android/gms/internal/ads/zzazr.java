package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzazr implements zzdgt<T> {
    private final /* synthetic */ zzazo zzdwt;

    zzazr(zzazo zzazo) {
        this.zzdwt = zzazo;
    }

    public final void onSuccess(T t) {
        this.zzdwt.zzdwq.set(1);
    }

    public final void zzb(Throwable th) {
        this.zzdwt.zzdwq.set(-1);
    }
}
