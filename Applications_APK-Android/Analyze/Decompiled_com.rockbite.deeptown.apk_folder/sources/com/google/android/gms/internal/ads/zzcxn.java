package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzcxn<R> {
    public final Executor executor;
    public final String zzbqz;
    public final zzug zzdio;
    public final zzuo zzgey;
    public final zzcxv<R> zzgkh;
    public final zzcxs zzgki;

    public zzcxn(zzcxv<R> zzcxv, zzcxs zzcxs, zzug zzug, String str, Executor executor2, zzuo zzuo) {
        this.zzgkh = zzcxv;
        this.zzgki = zzcxs;
        this.zzdio = zzug;
        this.zzbqz = str;
        this.executor = executor2;
        this.zzgey = zzuo;
    }
}
