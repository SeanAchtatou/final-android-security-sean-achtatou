package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzso;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcdc implements zzdxg<zzcdh> {
    private static final zzcdc zzfsd = new zzcdc();

    public static zzcdc zzalc() {
        return zzfsd;
    }

    public final /* synthetic */ Object get() {
        return (zzcdh) zzdxm.zza(new zzcdh(zzso.zza.C0151zza.REQUEST_WILL_BUILD_URL, zzso.zza.C0151zza.REQUEST_DID_BUILD_URL, zzso.zza.C0151zza.REQUEST_FAILED_TO_BUILD_URL), "Cannot return null from a non-@Nullable @Provides method");
    }
}
