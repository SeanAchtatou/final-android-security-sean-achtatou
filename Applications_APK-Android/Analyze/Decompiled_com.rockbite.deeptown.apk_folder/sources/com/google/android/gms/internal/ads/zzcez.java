package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzcez extends zzags {
    private final /* synthetic */ zzceq zzftz;
    private final /* synthetic */ Object zzfua;
    private final /* synthetic */ String zzfub;
    private final /* synthetic */ long zzfuc;
    private final /* synthetic */ zzazl zzfud;

    zzcez(zzceq zzceq, Object obj, String str, long j2, zzazl zzazl) {
        this.zzftz = zzceq;
        this.zzfua = obj;
        this.zzfub = str;
        this.zzfuc = j2;
        this.zzfud = zzazl;
    }

    public final void onInitializationFailed(String str) {
        synchronized (this.zzfua) {
            this.zzftz.zza(this.zzfub, false, str, (int) (zzq.zzkx().elapsedRealtime() - this.zzfuc));
            this.zzftz.zzftr.zzq(this.zzfub, "error");
            this.zzfud.set(false);
        }
    }

    public final void onInitializationSucceeded() {
        synchronized (this.zzfua) {
            this.zzftz.zza(this.zzfub, true, "", (int) (zzq.zzkx().elapsedRealtime() - this.zzfuc));
            this.zzftz.zzftr.zzge(this.zzfub);
            this.zzfud.set(true);
        }
    }
}
