package com.google.android.gms.internal.ads;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdFormat;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.google.android.gms.ads.mediation.Adapter;
import com.google.android.gms.ads.mediation.MediationAdapter;
import com.google.android.gms.ads.mediation.MediationBannerAdapter;
import com.google.android.gms.ads.mediation.MediationConfiguration;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.mediation.MediationNativeAdapter;
import com.google.android.gms.ads.mediation.MediationRewardedAd;
import com.google.android.gms.ads.mediation.MediationRewardedAdConfiguration;
import com.google.android.gms.ads.mediation.NativeAdMapper;
import com.google.android.gms.ads.mediation.NativeAppInstallAdMapper;
import com.google.android.gms.ads.mediation.NativeContentAdMapper;
import com.google.android.gms.ads.mediation.OnContextChangedListener;
import com.google.android.gms.ads.mediation.OnImmersiveModeUpdatedListener;
import com.google.android.gms.ads.mediation.UnifiedNativeAdMapper;
import com.google.android.gms.ads.mediation.zza;
import com.google.android.gms.ads.reward.mediation.InitializableMediationRewardedVideoAdAdapter;
import com.google.android.gms.ads.reward.mediation.MediationRewardedVideoAdAdapter;
import com.google.android.gms.ads.zzb;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzaly extends zzalg {
    private final Object zzddn;
    private zzalz zzddo;
    private zzarz zzddp;
    private IObjectWrapper zzddq;
    /* access modifiers changed from: private */
    public MediationRewardedAd zzddr;

    public zzaly(MediationAdapter mediationAdapter) {
        this.zzddn = mediationAdapter;
    }

    private static boolean zzc(zzug zzug) {
        if (zzug.zzccb) {
            return true;
        }
        zzve.zzou();
        return zzayk.zzxd();
    }

    public final void destroy() throws RemoteException {
        Object obj = this.zzddn;
        if (obj instanceof MediationAdapter) {
            try {
                ((MediationAdapter) obj).onDestroy();
            } catch (Throwable th) {
                zzayu.zzc("", th);
                throw new RemoteException();
            }
        }
    }

    public final Bundle getInterstitialAdapterInfo() {
        Object obj = this.zzddn;
        if (obj instanceof zzbfy) {
            return ((zzbfy) obj).getInterstitialAdapterInfo();
        }
        String canonicalName = zzbfy.class.getCanonicalName();
        String canonicalName2 = this.zzddn.getClass().getCanonicalName();
        StringBuilder sb = new StringBuilder(String.valueOf(canonicalName).length() + 22 + String.valueOf(canonicalName2).length());
        sb.append(canonicalName);
        sb.append(" #009 Class mismatch: ");
        sb.append(canonicalName2);
        zzayu.zzez(sb.toString());
        return new Bundle();
    }

    public final zzxb getVideoController() {
        Object obj = this.zzddn;
        if (!(obj instanceof zza)) {
            return null;
        }
        try {
            return ((zza) obj).getVideoController();
        } catch (Throwable th) {
            zzayu.zzc("", th);
            return null;
        }
    }

    public final boolean isInitialized() throws RemoteException {
        Object obj = this.zzddn;
        if (obj instanceof MediationRewardedVideoAdAdapter) {
            zzayu.zzea("Check if adapter is initialized.");
            try {
                return ((MediationRewardedVideoAdAdapter) this.zzddn).isInitialized();
            } catch (Throwable th) {
                zzayu.zzc("", th);
                throw new RemoteException();
            }
        } else if (obj instanceof Adapter) {
            return this.zzddp != null;
        } else {
            String canonicalName = MediationRewardedVideoAdAdapter.class.getCanonicalName();
            String canonicalName2 = Adapter.class.getCanonicalName();
            String canonicalName3 = this.zzddn.getClass().getCanonicalName();
            StringBuilder sb = new StringBuilder(String.valueOf(canonicalName).length() + 26 + String.valueOf(canonicalName2).length() + String.valueOf(canonicalName3).length());
            sb.append(canonicalName);
            sb.append(" or ");
            sb.append(canonicalName2);
            sb.append(" #009 Class mismatch: ");
            sb.append(canonicalName3);
            zzayu.zzez(sb.toString());
            throw new RemoteException();
        }
    }

    public final void pause() throws RemoteException {
        Object obj = this.zzddn;
        if (obj instanceof MediationAdapter) {
            try {
                ((MediationAdapter) obj).onPause();
            } catch (Throwable th) {
                zzayu.zzc("", th);
                throw new RemoteException();
            }
        }
    }

    public final void resume() throws RemoteException {
        Object obj = this.zzddn;
        if (obj instanceof MediationAdapter) {
            try {
                ((MediationAdapter) obj).onResume();
            } catch (Throwable th) {
                zzayu.zzc("", th);
                throw new RemoteException();
            }
        }
    }

    public final void setImmersiveMode(boolean z) throws RemoteException {
        Object obj = this.zzddn;
        if (!(obj instanceof OnImmersiveModeUpdatedListener)) {
            String canonicalName = OnImmersiveModeUpdatedListener.class.getCanonicalName();
            String canonicalName2 = this.zzddn.getClass().getCanonicalName();
            StringBuilder sb = new StringBuilder(String.valueOf(canonicalName).length() + 22 + String.valueOf(canonicalName2).length());
            sb.append(canonicalName);
            sb.append(" #009 Class mismatch: ");
            sb.append(canonicalName2);
            zzayu.zzez(sb.toString());
            return;
        }
        try {
            ((OnImmersiveModeUpdatedListener) obj).onImmersiveModeUpdated(z);
        } catch (Throwable th) {
            zzayu.zzc("", th);
        }
    }

    public final void showInterstitial() throws RemoteException {
        if (this.zzddn instanceof MediationInterstitialAdapter) {
            zzayu.zzea("Showing interstitial from adapter.");
            try {
                ((MediationInterstitialAdapter) this.zzddn).showInterstitial();
            } catch (Throwable th) {
                zzayu.zzc("", th);
                throw new RemoteException();
            }
        } else {
            String canonicalName = MediationInterstitialAdapter.class.getCanonicalName();
            String canonicalName2 = this.zzddn.getClass().getCanonicalName();
            StringBuilder sb = new StringBuilder(String.valueOf(canonicalName).length() + 22 + String.valueOf(canonicalName2).length());
            sb.append(canonicalName);
            sb.append(" #009 Class mismatch: ");
            sb.append(canonicalName2);
            zzayu.zzez(sb.toString());
            throw new RemoteException();
        }
    }

    public final void showVideo() throws RemoteException {
        Object obj = this.zzddn;
        if (obj instanceof MediationRewardedVideoAdAdapter) {
            zzayu.zzea("Show rewarded video ad from adapter.");
            try {
                ((MediationRewardedVideoAdAdapter) this.zzddn).showVideo();
            } catch (Throwable th) {
                zzayu.zzc("", th);
                throw new RemoteException();
            }
        } else if (obj instanceof Adapter) {
            MediationRewardedAd mediationRewardedAd = this.zzddr;
            if (mediationRewardedAd != null) {
                mediationRewardedAd.showAd((Context) ObjectWrapper.unwrap(this.zzddq));
            } else {
                zzayu.zzex("Can not show null mediated rewarded ad.");
                throw new RemoteException();
            }
        } else {
            String canonicalName = MediationRewardedVideoAdAdapter.class.getCanonicalName();
            String canonicalName2 = Adapter.class.getCanonicalName();
            String canonicalName3 = this.zzddn.getClass().getCanonicalName();
            StringBuilder sb = new StringBuilder(String.valueOf(canonicalName).length() + 26 + String.valueOf(canonicalName2).length() + String.valueOf(canonicalName3).length());
            sb.append(canonicalName);
            sb.append(" or ");
            sb.append(canonicalName2);
            sb.append(" #009 Class mismatch: ");
            sb.append(canonicalName3);
            zzayu.zzez(sb.toString());
            throw new RemoteException();
        }
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzuj zzuj, zzug zzug, String str, zzali zzali) throws RemoteException {
        zza(iObjectWrapper, zzuj, zzug, str, null, zzali);
    }

    public final void zzb(IObjectWrapper iObjectWrapper, zzug zzug, String str, zzali zzali) throws RemoteException {
        Bundle bundle;
        zzug zzug2 = zzug;
        String str2 = str;
        if (this.zzddn instanceof Adapter) {
            zzayu.zzea("Requesting rewarded ad from adapter.");
            try {
                Adapter adapter = (Adapter) this.zzddn;
                zzalx zzalx = new zzalx(this, zzali, adapter);
                Context context = (Context) ObjectWrapper.unwrap(iObjectWrapper);
                Bundle zza = zza(str2, zzug2, (String) null);
                if (zzug2.zzccf == null || (bundle = zzug2.zzccf.getBundle(this.zzddn.getClass().getName())) == null) {
                    bundle = new Bundle();
                }
                boolean zzc = zzc(zzug);
                Location location = zzug2.zzmi;
                int i2 = zzug2.zzabo;
                int i3 = zzug2.zzabp;
                String zza2 = zza(str2, zzug2);
                MediationRewardedAdConfiguration mediationRewardedAdConfiguration = r5;
                MediationRewardedAdConfiguration mediationRewardedAdConfiguration2 = new MediationRewardedAdConfiguration(context, "", zza, bundle, zzc, location, i2, i3, zza2, "");
                adapter.loadRewardedAd(mediationRewardedAdConfiguration, zzalx);
            } catch (Exception e2) {
                zzayu.zzc("", e2);
                throw new RemoteException();
            }
        } else {
            String canonicalName = Adapter.class.getCanonicalName();
            String canonicalName2 = this.zzddn.getClass().getCanonicalName();
            StringBuilder sb = new StringBuilder(String.valueOf(canonicalName).length() + 22 + String.valueOf(canonicalName2).length());
            sb.append(canonicalName);
            sb.append(" #009 Class mismatch: ");
            sb.append(canonicalName2);
            zzayu.zzez(sb.toString());
            throw new RemoteException();
        }
    }

    public final void zzs(IObjectWrapper iObjectWrapper) throws RemoteException {
        Context context = (Context) ObjectWrapper.unwrap(iObjectWrapper);
        Object obj = this.zzddn;
        if (obj instanceof OnContextChangedListener) {
            ((OnContextChangedListener) obj).onContextChanged(context);
        }
    }

    public final IObjectWrapper zzsk() throws RemoteException {
        Object obj = this.zzddn;
        if (obj instanceof MediationBannerAdapter) {
            try {
                return ObjectWrapper.wrap(((MediationBannerAdapter) obj).getBannerView());
            } catch (Throwable th) {
                zzayu.zzc("", th);
                throw new RemoteException();
            }
        } else {
            String canonicalName = MediationBannerAdapter.class.getCanonicalName();
            String canonicalName2 = this.zzddn.getClass().getCanonicalName();
            StringBuilder sb = new StringBuilder(String.valueOf(canonicalName).length() + 22 + String.valueOf(canonicalName2).length());
            sb.append(canonicalName);
            sb.append(" #009 Class mismatch: ");
            sb.append(canonicalName2);
            zzayu.zzez(sb.toString());
            throw new RemoteException();
        }
    }

    public final zzall zzsl() {
        NativeAdMapper zzsw = this.zzddo.zzsw();
        if (zzsw instanceof NativeAppInstallAdMapper) {
            return new zzamb((NativeAppInstallAdMapper) zzsw);
        }
        return null;
    }

    public final zzalq zzsm() {
        NativeAdMapper zzsw = this.zzddo.zzsw();
        if (zzsw instanceof NativeContentAdMapper) {
            return new zzame((NativeContentAdMapper) zzsw);
        }
        return null;
    }

    public final Bundle zzsn() {
        Object obj = this.zzddn;
        if (obj instanceof zzbfw) {
            return ((zzbfw) obj).zzsn();
        }
        String canonicalName = zzbfw.class.getCanonicalName();
        String canonicalName2 = this.zzddn.getClass().getCanonicalName();
        StringBuilder sb = new StringBuilder(String.valueOf(canonicalName).length() + 22 + String.valueOf(canonicalName2).length());
        sb.append(canonicalName);
        sb.append(" #009 Class mismatch: ");
        sb.append(canonicalName2);
        zzayu.zzez(sb.toString());
        return new Bundle();
    }

    public final Bundle zzso() {
        return new Bundle();
    }

    public final boolean zzsp() {
        return this.zzddn instanceof InitializableMediationRewardedVideoAdAdapter;
    }

    public final zzade zzsq() {
        NativeCustomTemplateAd zzsy = this.zzddo.zzsy();
        if (zzsy instanceof zzadf) {
            return ((zzadf) zzsy).zzro();
        }
        return null;
    }

    public final zzalr zzsr() {
        UnifiedNativeAdMapper zzsx = this.zzddo.zzsx();
        if (zzsx != null) {
            return new zzamt(zzsx);
        }
        return null;
    }

    public final void zzt(IObjectWrapper iObjectWrapper) throws RemoteException {
        if (this.zzddn instanceof Adapter) {
            zzayu.zzea("Show rewarded ad from adapter.");
            MediationRewardedAd mediationRewardedAd = this.zzddr;
            if (mediationRewardedAd != null) {
                mediationRewardedAd.showAd((Context) ObjectWrapper.unwrap(iObjectWrapper));
            } else {
                zzayu.zzex("Can not show null mediation rewarded ad.");
                throw new RemoteException();
            }
        } else {
            String canonicalName = Adapter.class.getCanonicalName();
            String canonicalName2 = this.zzddn.getClass().getCanonicalName();
            StringBuilder sb = new StringBuilder(String.valueOf(canonicalName).length() + 22 + String.valueOf(canonicalName2).length());
            sb.append(canonicalName);
            sb.append(" #009 Class mismatch: ");
            sb.append(canonicalName2);
            zzayu.zzez(sb.toString());
            throw new RemoteException();
        }
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzuj zzuj, zzug zzug, String str, String str2, zzali zzali) throws RemoteException {
        Date date;
        AdSize zza;
        zzuj zzuj2 = zzuj;
        zzug zzug2 = zzug;
        String str3 = str;
        if (this.zzddn instanceof MediationBannerAdapter) {
            zzayu.zzea("Requesting banner ad from adapter.");
            try {
                MediationBannerAdapter mediationBannerAdapter = (MediationBannerAdapter) this.zzddn;
                Bundle bundle = null;
                HashSet hashSet = zzug2.zzcca != null ? new HashSet(zzug2.zzcca) : null;
                if (zzug2.zzcby == -1) {
                    date = null;
                } else {
                    date = new Date(zzug2.zzcby);
                }
                zzalv zzalv = new zzalv(date, zzug2.zzcbz, hashSet, zzug2.zzmi, zzc(zzug), zzug2.zzabo, zzug2.zzcck, zzug2.zzabp, zza(str3, zzug2));
                if (zzug2.zzccf != null) {
                    bundle = zzug2.zzccf.getBundle(mediationBannerAdapter.getClass().getName());
                }
                Bundle bundle2 = bundle;
                if (zzuj2.zzccv) {
                    zza = zzb.zza(zzuj2.width, zzuj2.height);
                } else {
                    zza = zzb.zza(zzuj2.width, zzuj2.height, zzuj2.zzabg);
                }
                mediationBannerAdapter.requestBannerAd((Context) ObjectWrapper.unwrap(iObjectWrapper), new zzalz(zzali), zza(str3, zzug2, str2), zza, zzalv, bundle2);
            } catch (Throwable th) {
                zzayu.zzc("", th);
                throw new RemoteException();
            }
        } else {
            String canonicalName = MediationBannerAdapter.class.getCanonicalName();
            String canonicalName2 = this.zzddn.getClass().getCanonicalName();
            StringBuilder sb = new StringBuilder(String.valueOf(canonicalName).length() + 22 + String.valueOf(canonicalName2).length());
            sb.append(canonicalName);
            sb.append(" #009 Class mismatch: ");
            sb.append(canonicalName2);
            zzayu.zzez(sb.toString());
            throw new RemoteException();
        }
    }

    public zzaly(Adapter adapter) {
        this.zzddn = adapter;
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzug zzug, String str, zzali zzali) throws RemoteException {
        zza(iObjectWrapper, zzug, str, (String) null, zzali);
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzug zzug, String str, String str2, zzali zzali) throws RemoteException {
        Date date;
        zzug zzug2 = zzug;
        String str3 = str;
        if (this.zzddn instanceof MediationInterstitialAdapter) {
            zzayu.zzea("Requesting interstitial ad from adapter.");
            try {
                MediationInterstitialAdapter mediationInterstitialAdapter = (MediationInterstitialAdapter) this.zzddn;
                Bundle bundle = null;
                HashSet hashSet = zzug2.zzcca != null ? new HashSet(zzug2.zzcca) : null;
                if (zzug2.zzcby == -1) {
                    date = null;
                } else {
                    date = new Date(zzug2.zzcby);
                }
                zzalv zzalv = new zzalv(date, zzug2.zzcbz, hashSet, zzug2.zzmi, zzc(zzug), zzug2.zzabo, zzug2.zzcck, zzug2.zzabp, zza(str3, zzug2));
                if (zzug2.zzccf != null) {
                    bundle = zzug2.zzccf.getBundle(mediationInterstitialAdapter.getClass().getName());
                }
                mediationInterstitialAdapter.requestInterstitialAd((Context) ObjectWrapper.unwrap(iObjectWrapper), new zzalz(zzali), zza(str3, zzug2, str2), zzalv, bundle);
            } catch (Throwable th) {
                zzayu.zzc("", th);
                throw new RemoteException();
            }
        } else {
            String canonicalName = MediationInterstitialAdapter.class.getCanonicalName();
            String canonicalName2 = this.zzddn.getClass().getCanonicalName();
            StringBuilder sb = new StringBuilder(String.valueOf(canonicalName).length() + 22 + String.valueOf(canonicalName2).length());
            sb.append(canonicalName);
            sb.append(" #009 Class mismatch: ");
            sb.append(canonicalName2);
            zzayu.zzez(sb.toString());
            throw new RemoteException();
        }
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzug zzug, String str, String str2, zzali zzali, zzaby zzaby, List<String> list) throws RemoteException {
        Date date;
        zzug zzug2 = zzug;
        String str3 = str;
        Object obj = this.zzddn;
        if (obj instanceof MediationNativeAdapter) {
            try {
                MediationNativeAdapter mediationNativeAdapter = (MediationNativeAdapter) obj;
                Bundle bundle = null;
                HashSet hashSet = zzug2.zzcca != null ? new HashSet(zzug2.zzcca) : null;
                if (zzug2.zzcby == -1) {
                    date = null;
                } else {
                    date = new Date(zzug2.zzcby);
                }
                zzamd zzamd = new zzamd(date, zzug2.zzcbz, hashSet, zzug2.zzmi, zzc(zzug), zzug2.zzabo, zzaby, list, zzug2.zzcck, zzug2.zzabp, zza(str3, zzug2));
                if (zzug2.zzccf != null) {
                    bundle = zzug2.zzccf.getBundle(mediationNativeAdapter.getClass().getName());
                }
                this.zzddo = new zzalz(zzali);
                mediationNativeAdapter.requestNativeAd((Context) ObjectWrapper.unwrap(iObjectWrapper), this.zzddo, zza(str3, zzug2, str2), zzamd, bundle);
            } catch (Throwable th) {
                zzayu.zzc("", th);
                throw new RemoteException();
            }
        } else {
            String canonicalName = MediationNativeAdapter.class.getCanonicalName();
            String canonicalName2 = this.zzddn.getClass().getCanonicalName();
            StringBuilder sb = new StringBuilder(String.valueOf(canonicalName).length() + 22 + String.valueOf(canonicalName2).length());
            sb.append(canonicalName);
            sb.append(" #009 Class mismatch: ");
            sb.append(canonicalName2);
            zzayu.zzez(sb.toString());
            throw new RemoteException();
        }
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzug zzug, String str, zzarz zzarz, String str2) throws RemoteException {
        Bundle bundle;
        zzalv zzalv;
        Date date;
        zzug zzug2 = zzug;
        zzarz zzarz2 = zzarz;
        String str3 = str2;
        Object obj = this.zzddn;
        if (obj instanceof MediationRewardedVideoAdAdapter) {
            zzayu.zzea("Initialize rewarded video adapter.");
            try {
                MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter = (MediationRewardedVideoAdAdapter) this.zzddn;
                Bundle zza = zza(str3, zzug2, (String) null);
                if (zzug2 != null) {
                    HashSet hashSet = zzug2.zzcca != null ? new HashSet(zzug2.zzcca) : null;
                    if (zzug2.zzcby == -1) {
                        date = null;
                    } else {
                        date = new Date(zzug2.zzcby);
                    }
                    zzalv zzalv2 = new zzalv(date, zzug2.zzcbz, hashSet, zzug2.zzmi, zzc(zzug), zzug2.zzabo, zzug2.zzcck, zzug2.zzabp, zza(str3, zzug2));
                    bundle = zzug2.zzccf != null ? zzug2.zzccf.getBundle(mediationRewardedVideoAdAdapter.getClass().getName()) : null;
                    zzalv = zzalv2;
                } else {
                    zzalv = null;
                    bundle = null;
                }
                mediationRewardedVideoAdAdapter.initialize((Context) ObjectWrapper.unwrap(iObjectWrapper), zzalv, str, new zzasa(zzarz2), zza, bundle);
            } catch (Throwable th) {
                zzayu.zzc("", th);
                throw new RemoteException();
            }
        } else if (obj instanceof Adapter) {
            this.zzddq = iObjectWrapper;
            this.zzddp = zzarz2;
            zzarz2.zzaf(ObjectWrapper.wrap(obj));
        } else {
            String canonicalName = MediationRewardedVideoAdAdapter.class.getCanonicalName();
            String canonicalName2 = Adapter.class.getCanonicalName();
            String canonicalName3 = this.zzddn.getClass().getCanonicalName();
            StringBuilder sb = new StringBuilder(String.valueOf(canonicalName).length() + 26 + String.valueOf(canonicalName2).length() + String.valueOf(canonicalName3).length());
            sb.append(canonicalName);
            sb.append(" or ");
            sb.append(canonicalName2);
            sb.append(" #009 Class mismatch: ");
            sb.append(canonicalName3);
            zzayu.zzez(sb.toString());
            throw new RemoteException();
        }
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzarz zzarz, List<String> list) throws RemoteException {
        if (this.zzddn instanceof InitializableMediationRewardedVideoAdAdapter) {
            zzayu.zzea("Initialize rewarded video adapter.");
            try {
                InitializableMediationRewardedVideoAdAdapter initializableMediationRewardedVideoAdAdapter = (InitializableMediationRewardedVideoAdAdapter) this.zzddn;
                ArrayList arrayList = new ArrayList();
                for (String zza : list) {
                    arrayList.add(zza(zza, (zzug) null, (String) null));
                }
                initializableMediationRewardedVideoAdAdapter.initialize((Context) ObjectWrapper.unwrap(iObjectWrapper), new zzasa(zzarz), arrayList);
            } catch (Throwable th) {
                zzayu.zzd("Could not initialize rewarded video adapter.", th);
                throw new RemoteException();
            }
        } else {
            String canonicalName = InitializableMediationRewardedVideoAdAdapter.class.getCanonicalName();
            String canonicalName2 = this.zzddn.getClass().getCanonicalName();
            StringBuilder sb = new StringBuilder(String.valueOf(canonicalName).length() + 22 + String.valueOf(canonicalName2).length());
            sb.append(canonicalName);
            sb.append(" #009 Class mismatch: ");
            sb.append(canonicalName2);
            zzayu.zzez(sb.toString());
            throw new RemoteException();
        }
    }

    public final void zza(zzug zzug, String str) throws RemoteException {
        zza(zzug, str, (String) null);
    }

    public final void zza(zzug zzug, String str, String str2) throws RemoteException {
        Date date;
        zzug zzug2 = zzug;
        String str3 = str;
        Object obj = this.zzddn;
        if (obj instanceof MediationRewardedVideoAdAdapter) {
            zzayu.zzea("Requesting rewarded video ad from adapter.");
            try {
                MediationRewardedVideoAdAdapter mediationRewardedVideoAdAdapter = (MediationRewardedVideoAdAdapter) this.zzddn;
                Bundle bundle = null;
                HashSet hashSet = zzug2.zzcca != null ? new HashSet(zzug2.zzcca) : null;
                if (zzug2.zzcby == -1) {
                    date = null;
                } else {
                    date = new Date(zzug2.zzcby);
                }
                zzalv zzalv = new zzalv(date, zzug2.zzcbz, hashSet, zzug2.zzmi, zzc(zzug), zzug2.zzabo, zzug2.zzcck, zzug2.zzabp, zza(str3, zzug2));
                if (zzug2.zzccf != null) {
                    bundle = zzug2.zzccf.getBundle(mediationRewardedVideoAdAdapter.getClass().getName());
                }
                mediationRewardedVideoAdAdapter.loadAd(zzalv, zza(str3, zzug2, str2), bundle);
            } catch (Throwable th) {
                zzayu.zzc("", th);
                throw new RemoteException();
            }
        } else if (obj instanceof Adapter) {
            zzb(this.zzddq, zzug2, str3, new zzamc((Adapter) obj, this.zzddp));
        } else {
            String canonicalName = MediationRewardedVideoAdAdapter.class.getCanonicalName();
            String canonicalName2 = Adapter.class.getCanonicalName();
            String canonicalName3 = this.zzddn.getClass().getCanonicalName();
            StringBuilder sb = new StringBuilder(String.valueOf(canonicalName).length() + 26 + String.valueOf(canonicalName2).length() + String.valueOf(canonicalName3).length());
            sb.append(canonicalName);
            sb.append(" or ");
            sb.append(canonicalName2);
            sb.append(" #009 Class mismatch: ");
            sb.append(canonicalName3);
            zzayu.zzez(sb.toString());
            throw new RemoteException();
        }
    }

    public final void zza(IObjectWrapper iObjectWrapper, zzagp zzagp, List<zzagx> list) throws RemoteException {
        AdFormat adFormat;
        if (this.zzddn instanceof Adapter) {
            zzama zzama = new zzama(this, zzagp);
            ArrayList arrayList = new ArrayList();
            for (zzagx next : list) {
                String str = next.zzcyg;
                char c2 = 65535;
                switch (str.hashCode()) {
                    case -1396342996:
                        if (str.equals("banner")) {
                            c2 = 0;
                            break;
                        }
                        break;
                    case -1052618729:
                        if (str.equals("native")) {
                            c2 = 3;
                            break;
                        }
                        break;
                    case -239580146:
                        if (str.equals("rewarded")) {
                            c2 = 2;
                            break;
                        }
                        break;
                    case 604727084:
                        if (str.equals("interstitial")) {
                            c2 = 1;
                            break;
                        }
                        break;
                }
                if (c2 == 0) {
                    adFormat = AdFormat.BANNER;
                } else if (c2 == 1) {
                    adFormat = AdFormat.INTERSTITIAL;
                } else if (c2 == 2) {
                    adFormat = AdFormat.REWARDED;
                } else if (c2 == 3) {
                    adFormat = AdFormat.NATIVE;
                } else {
                    throw new RemoteException();
                }
                arrayList.add(new MediationConfiguration(adFormat, next.extras));
            }
            ((Adapter) this.zzddn).initialize((Context) ObjectWrapper.unwrap(iObjectWrapper), zzama, arrayList);
            return;
        }
        throw new RemoteException();
    }

    private final Bundle zza(String str, zzug zzug, String str2) throws RemoteException {
        Bundle bundle;
        String valueOf = String.valueOf(str);
        zzayu.zzea(valueOf.length() != 0 ? "Server parameters: ".concat(valueOf) : new String("Server parameters: "));
        try {
            Bundle bundle2 = new Bundle();
            if (str != null) {
                JSONObject jSONObject = new JSONObject(str);
                bundle = new Bundle();
                Iterator<String> keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    bundle.putString(next, jSONObject.getString(next));
                }
            } else {
                bundle = bundle2;
            }
            if (this.zzddn instanceof AdMobAdapter) {
                bundle.putString("adJson", str2);
                if (zzug != null) {
                    bundle.putInt("tagForChildDirectedTreatment", zzug.zzabo);
                }
            }
            bundle.remove("max_ad_content_rating");
            return bundle;
        } catch (Throwable th) {
            zzayu.zzc("", th);
            throw new RemoteException();
        }
    }

    private static String zza(String str, zzug zzug) {
        try {
            return new JSONObject(str).getString("max_ad_content_rating");
        } catch (JSONException unused) {
            return zzug.zzabq;
        }
    }
}
