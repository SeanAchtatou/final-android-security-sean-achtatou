package com.google.android.gms.internal.ads;

import android.util.JsonWriter;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final /* synthetic */ class zzayt implements zzayv {
    private final byte[] zzdvy;

    zzayt(byte[] bArr) {
        this.zzdvy = bArr;
    }

    public final void zzb(JsonWriter jsonWriter) {
        zzayo.zza(this.zzdvy, jsonWriter);
    }
}
