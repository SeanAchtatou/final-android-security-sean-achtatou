package com.google.android.gms.internal.ads;

import b.d.a;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbyc implements zzbpe {
    private final zzbws zzfkc;
    private final zzbww zzfli;

    public zzbyc(zzbws zzbws, zzbww zzbww) {
        this.zzfkc = zzbws;
        this.zzfli = zzbww;
    }

    public final void onAdImpression() {
        if (this.zzfkc.zzajh() != null) {
            zzbdi zzajg = this.zzfkc.zzajg();
            zzbdi zzajf = this.zzfkc.zzajf();
            if (zzajg == null) {
                zzajg = zzajf != null ? zzajf : null;
            }
            if (this.zzfli.zzaiw() && zzajg != null) {
                zzajg.zza("onSdkImpression", new a());
            }
        }
    }
}
