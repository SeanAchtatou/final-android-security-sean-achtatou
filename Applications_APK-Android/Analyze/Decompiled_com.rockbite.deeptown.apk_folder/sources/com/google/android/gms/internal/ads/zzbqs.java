package com.google.android.gms.internal.ads;

import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbqs extends zzbrl<zzbqx> implements zzbqx {
    public zzbqs(Set<zzbsu<zzbqx>> set) {
        super(set);
    }

    public final void zzb(zzaqk zzaqk) {
        zza(new zzbqv(zzaqk));
    }

    public final void zzb(zzczt zzczt) {
        zza(new zzbqu(zzczt));
    }
}
