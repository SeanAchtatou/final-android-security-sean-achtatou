package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzaub implements zzaul {
    private final String zzcyz;

    zzaub(String str) {
        this.zzcyz = str;
    }

    public final void zza(zzbfq zzbfq) {
        zzbfq.endAdUnitExposure(this.zzcyz);
    }
}
