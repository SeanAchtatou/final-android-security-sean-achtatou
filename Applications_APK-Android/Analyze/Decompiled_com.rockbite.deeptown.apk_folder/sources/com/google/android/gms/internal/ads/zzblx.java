package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzblx implements zzdxg<zzbly> {
    private final zzdxp<zzczl> zzfbp;
    private final zzdxp<zzbpd> zzfff;
    private final zzdxp<zzbqf> zzffg;

    private zzblx(zzdxp<zzczl> zzdxp, zzdxp<zzbpd> zzdxp2, zzdxp<zzbqf> zzdxp3) {
        this.zzfbp = zzdxp;
        this.zzfff = zzdxp2;
        this.zzffg = zzdxp3;
    }

    public static zzblx zzf(zzdxp<zzczl> zzdxp, zzdxp<zzbpd> zzdxp2, zzdxp<zzbqf> zzdxp3) {
        return new zzblx(zzdxp, zzdxp2, zzdxp3);
    }

    public final /* synthetic */ Object get() {
        return new zzbly(this.zzfbp.get(), this.zzfff.get(), this.zzffg.get());
    }
}
