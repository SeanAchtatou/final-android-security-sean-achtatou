package com.google.android.gms.internal.ads;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import com.esotericsoftware.spine.Animation;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcqh implements zzcub<zzcqe> {
    private final zzave zzbmm;
    private final zzczu zzfgl;
    private final zzcub<zzcue> zzget;
    private final Context zzup;

    public zzcqh(zzcrk<zzcue> zzcrk, zzczu zzczu, Context context, zzave zzave) {
        this.zzget = zzcrk;
        this.zzfgl = zzczu;
        this.zzup = context;
        this.zzbmm = zzave;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzcqe zza(zzcue zzcue) {
        boolean z;
        String str;
        int i2;
        int i3;
        float f2;
        String str2;
        int i4;
        int i5;
        int i6;
        DisplayMetrics displayMetrics;
        zzuj zzuj = this.zzfgl.zzblm;
        zzuj[] zzujArr = zzuj.zzccp;
        if (zzujArr != null) {
            str = null;
            boolean z2 = false;
            boolean z3 = false;
            z = false;
            for (zzuj zzuj2 : zzujArr) {
                if (!zzuj2.zzccq && !z2) {
                    str = zzuj2.zzabg;
                    z2 = true;
                }
                if (zzuj2.zzccq && !z3) {
                    z3 = true;
                    z = true;
                }
                if (z2 && z3) {
                    break;
                }
            }
        } else {
            str = zzuj.zzabg;
            z = zzuj.zzccq;
        }
        Resources resources = this.zzup.getResources();
        if (resources == null || (displayMetrics = resources.getDisplayMetrics()) == null) {
            str2 = null;
            f2 = Animation.CurveTimeline.LINEAR;
            i3 = 0;
            i2 = 0;
        } else {
            float f3 = displayMetrics.density;
            int i7 = displayMetrics.widthPixels;
            i2 = displayMetrics.heightPixels;
            str2 = this.zzbmm.zzvf().zzwg();
            i3 = i7;
            f2 = f3;
        }
        StringBuilder sb = new StringBuilder();
        zzuj[] zzujArr2 = zzuj.zzccp;
        if (zzujArr2 != null) {
            boolean z4 = false;
            for (zzuj zzuj3 : zzujArr2) {
                if (zzuj3.zzccq) {
                    z4 = true;
                } else {
                    if (sb.length() != 0) {
                        sb.append("|");
                    }
                    if (zzuj3.width != -1 || f2 == Animation.CurveTimeline.LINEAR) {
                        i5 = zzuj3.width;
                    } else {
                        i5 = (int) (((float) zzuj3.widthPixels) / f2);
                    }
                    sb.append(i5);
                    sb.append("x");
                    if (zzuj3.height == -2) {
                        if (f2 != Animation.CurveTimeline.LINEAR) {
                            i6 = (int) (((float) zzuj3.heightPixels) / f2);
                            sb.append(i6);
                        }
                    }
                    i6 = zzuj3.height;
                    sb.append(i6);
                }
            }
            if (z4) {
                if (sb.length() != 0) {
                    i4 = 0;
                    sb.insert(0, "|");
                } else {
                    i4 = 0;
                }
                sb.insert(i4, "320x50");
            }
        }
        return new zzcqe(zzuj, str, z, sb.toString(), f2, i3, i2, str2);
    }

    public final zzdhe<zzcqe> zzanc() {
        return zzdgs.zzb(this.zzget.zzanc(), new zzcqg(this), zzazd.zzdwj);
    }
}
