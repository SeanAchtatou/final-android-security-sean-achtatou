package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public abstract class zzbvm {
    public abstract zzbwt zza(zzbmt zzbmt, zzbxe zzbxe, zzbyg zzbyg);

    public abstract zzbwu zza(zzbmt zzbmt, zzbxe zzbxe, zzbvy zzbvy);

    public abstract zzbmz<zzbmj> zzadc();

    public abstract zzbou zzadd();

    public abstract zzdaf<zzcaj> zzade();
}
