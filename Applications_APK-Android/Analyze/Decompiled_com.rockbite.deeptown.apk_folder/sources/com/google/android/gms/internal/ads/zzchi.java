package com.google.android.gms.internal.ads;

import android.os.ParcelFileDescriptor;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzchi implements zzdgt<ParcelFileDescriptor> {
    private final /* synthetic */ zzaqe zzfwk;

    zzchi(zzcgw zzcgw, zzaqe zzaqe) {
        this.zzfwk = zzaqe;
    }

    public final /* synthetic */ void onSuccess(Object obj) {
        try {
            this.zzfwk.zzb((ParcelFileDescriptor) obj);
        } catch (RemoteException e2) {
            zzavs.zza("Service can't call client", e2);
        }
    }

    public final void zzb(Throwable th) {
        try {
            this.zzfwk.zza(zzaxc.zza(th, zzcfb.zzd(th)));
        } catch (RemoteException e2) {
            zzavs.zza("Service can't call client", e2);
        }
    }
}
