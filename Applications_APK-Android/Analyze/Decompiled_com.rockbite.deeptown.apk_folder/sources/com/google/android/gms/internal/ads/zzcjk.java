package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import android.view.View;
import com.google.android.gms.dynamic.ObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcjk implements zzcir<zzbkk, zzani, zzcjy> {
    /* access modifiers changed from: private */
    public View view;
    private final zzblg zzfyj;
    private final Context zzup;

    public zzcjk(Context context, zzblg zzblg) {
        this.zzup = context;
        this.zzfyj = zzblg;
    }

    public final void zza(zzczt zzczt, zzczl zzczl, zzcip<zzani, zzcjy> zzcip) throws zzdab {
        try {
            ((zzani) zzcip.zzddn).zzdm(zzczl.zzdem);
            ((zzani) zzcip.zzddn).zza(zzczl.zzeif, zzczl.zzglr.toString(), zzczt.zzgmh.zzfgl.zzgml, ObjectWrapper.wrap(this.zzup), new zzcjl(this, zzcip), (zzali) zzcip.zzfyf, zzczt.zzgmh.zzfgl.zzblm);
        } catch (RemoteException e2) {
            throw new zzdab(e2);
        }
    }

    public final /* synthetic */ Object zzb(zzczt zzczt, zzczl zzczl, zzcip zzcip) throws zzdab, zzclr {
        zzbkj zza = this.zzfyj.zza(new zzbmt(zzczt, zzczl, zzcip.zzfge), new zzbkn(this.view, null, new zzcjj(zzcip), zzczl.zzglq.get(0)));
        zza.zzaei().zzq(this.view);
        ((zzcjy) zzcip.zzfyf).zza(zza.zzadm());
        return zza.zzaeh();
    }

    static final /* synthetic */ zzxb zza(zzcip zzcip) throws zzdab {
        try {
            return ((zzani) zzcip.zzddn).getVideoController();
        } catch (RemoteException e2) {
            throw new zzdab(e2);
        }
    }
}
