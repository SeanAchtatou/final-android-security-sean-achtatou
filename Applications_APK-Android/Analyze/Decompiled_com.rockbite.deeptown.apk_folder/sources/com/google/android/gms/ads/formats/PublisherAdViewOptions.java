package com.google.android.gms.ads.formats;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.ads.doubleclick.AppEventListener;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.ads.zzadz;
import com.google.android.gms.internal.ads.zzaea;
import com.google.android.gms.internal.ads.zzul;
import com.google.android.gms.internal.ads.zzwb;
import com.google.android.gms.internal.ads.zzwc;
import com.google.android.gms.internal.ads.zzyu;

@SafeParcelable.Class(creator = "PublisherAdViewOptionsCreator")
/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class PublisherAdViewOptions extends AbstractSafeParcelable {
    public static final Parcelable.Creator<PublisherAdViewOptions> CREATOR = new zzc();
    @SafeParcelable.Field(getter = "getManualImpressionsEnabled", id = 1)
    private final boolean zzbkh;
    @SafeParcelable.Field(getter = "getAppEventListenerBinder", id = 2, type = "android.os.IBinder")
    private final zzwc zzbki;
    private AppEventListener zzbkj;
    @SafeParcelable.Field(getter = "getDelayedBannerAdListenerBinder", id = 3)
    private final IBinder zzbkk;

    /* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
    public static final class Builder {
        /* access modifiers changed from: private */
        public boolean zzbkh = false;
        /* access modifiers changed from: private */
        public AppEventListener zzbkj;
        /* access modifiers changed from: private */
        public ShouldDelayBannerRenderingListener zzbkl;

        public final PublisherAdViewOptions build() {
            return new PublisherAdViewOptions(this);
        }

        public final Builder setAppEventListener(AppEventListener appEventListener) {
            this.zzbkj = appEventListener;
            return this;
        }

        public final Builder setManualImpressionsEnabled(boolean z) {
            this.zzbkh = z;
            return this;
        }

        @KeepForSdk
        public final Builder setShouldDelayBannerRenderingListener(ShouldDelayBannerRenderingListener shouldDelayBannerRenderingListener) {
            this.zzbkl = shouldDelayBannerRenderingListener;
            return this;
        }
    }

    private PublisherAdViewOptions(Builder builder) {
        this.zzbkh = builder.zzbkh;
        this.zzbkj = builder.zzbkj;
        AppEventListener appEventListener = this.zzbkj;
        zzyu zzyu = null;
        this.zzbki = appEventListener != null ? new zzul(appEventListener) : null;
        this.zzbkk = builder.zzbkl != null ? new zzyu(builder.zzbkl) : zzyu;
    }

    public final AppEventListener getAppEventListener() {
        return this.zzbkj;
    }

    public final boolean getManualImpressionsEnabled() {
        return this.zzbkh;
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeBoolean(parcel, 1, getManualImpressionsEnabled());
        zzwc zzwc = this.zzbki;
        SafeParcelWriter.writeIBinder(parcel, 2, zzwc == null ? null : zzwc.asBinder(), false);
        SafeParcelWriter.writeIBinder(parcel, 3, this.zzbkk, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }

    public final zzwc zzjm() {
        return this.zzbki;
    }

    public final zzaea zzjn() {
        return zzadz.zzw(this.zzbkk);
    }

    @SafeParcelable.Constructor
    PublisherAdViewOptions(@SafeParcelable.Param(id = 1) boolean z, @SafeParcelable.Param(id = 2) IBinder iBinder, @SafeParcelable.Param(id = 3) IBinder iBinder2) {
        this.zzbkh = z;
        this.zzbki = iBinder != null ? zzwb.zze(iBinder) : null;
        this.zzbkk = iBinder2;
    }
}
