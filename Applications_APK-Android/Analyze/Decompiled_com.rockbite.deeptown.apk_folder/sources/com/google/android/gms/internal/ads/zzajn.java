package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzajn implements Runnable {
    private final zzajo zzdah;
    private final zzaif zzdai;

    zzajn(zzajo zzajo, zzaif zzaif) {
        this.zzdah = zzajo;
        this.zzdai = zzaif;
    }

    public final void run() {
        zzajo zzajo = this.zzdah;
        zzaif zzaif = this.zzdai;
        zzajo.zzdaj.zzczn.zzh(zzaif);
        zzaif.destroy();
    }
}
