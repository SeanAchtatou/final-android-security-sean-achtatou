package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcie implements zzdxg<zzcib> {
    private final zzdxp<zzchz> zzfxr;

    private zzcie(zzdxp<zzchz> zzdxp) {
        this.zzfxr = zzdxp;
    }

    public static zzcie zzae(zzdxp<zzchz> zzdxp) {
        return new zzcie(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzcib(this.zzfxr.get());
    }
}
