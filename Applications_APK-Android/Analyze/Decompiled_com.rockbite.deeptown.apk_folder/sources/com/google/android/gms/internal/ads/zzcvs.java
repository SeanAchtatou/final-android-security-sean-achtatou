package com.google.android.gms.internal.ads;

import android.os.Bundle;
import com.google.android.gms.ads.internal.zzq;
import com.tapjoy.TapjoyConstants;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcvs implements zzcty<JSONObject> {
    private Bundle zzgie;

    public zzcvs(Bundle bundle) {
        this.zzgie = bundle;
    }

    public final /* synthetic */ void zzr(Object obj) {
        JSONObject jSONObject = (JSONObject) obj;
        if (this.zzgie != null) {
            try {
                zzaxs.zzb(zzaxs.zzb(jSONObject, TapjoyConstants.TJC_NOTIFICATION_DEVICE_PREFIX), "play_store").put("parental_controls", zzq.zzkq().zzd(this.zzgie));
            } catch (JSONException unused) {
                zzavs.zzed("Failed putting parental controls bundle.");
            }
        }
    }
}
