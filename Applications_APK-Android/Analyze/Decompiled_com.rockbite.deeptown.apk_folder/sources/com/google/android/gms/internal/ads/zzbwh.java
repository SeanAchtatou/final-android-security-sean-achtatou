package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbwh implements zzdxg<zzbwi> {
    private final zzdxp<zzbws> zzfkx;

    private zzbwh(zzdxp<zzbws> zzdxp) {
        this.zzfkx = zzdxp;
    }

    public static zzbwh zzw(zzdxp<zzbws> zzdxp) {
        return new zzbwh(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzbwi(this.zzfkx.get());
    }
}
