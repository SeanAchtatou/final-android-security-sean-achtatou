package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzaac {
    private final long time;
    private final String zzcrw;
    private final zzaac zzcrx;

    public zzaac(long j2, String str, zzaac zzaac) {
        this.time = j2;
        this.zzcrw = str;
        this.zzcrx = zzaac;
    }

    public final long getTime() {
        return this.time;
    }

    public final String zzqq() {
        return this.zzcrw;
    }

    public final zzaac zzqr() {
        return this.zzcrx;
    }
}
