package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcnr implements zzdxg<zzaak> {
    private final zzcns zzgbz;

    private zzcnr(zzcns zzcns) {
        this.zzgbz = zzcns;
    }

    public static zzcnr zzc(zzcns zzcns) {
        return new zzcnr(zzcns);
    }

    public final /* synthetic */ Object get() {
        return this.zzgbz.zzami();
    }
}
