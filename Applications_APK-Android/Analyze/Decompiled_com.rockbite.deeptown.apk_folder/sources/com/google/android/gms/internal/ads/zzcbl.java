package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcbl implements zzdxg<String> {
    private static final zzcbl zzfrc = new zzcbl();

    public static zzcbl zzakt() {
        return zzfrc;
    }

    public final /* synthetic */ Object get() {
        return (String) zzdxm.zza("rewarded", "Cannot return null from a non-@Nullable @Provides method");
    }
}
