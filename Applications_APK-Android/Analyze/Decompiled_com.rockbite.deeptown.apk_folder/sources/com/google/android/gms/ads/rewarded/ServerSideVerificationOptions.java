package com.google.android.gms.ads.rewarded;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public class ServerSideVerificationOptions {
    private final String zzdnv;
    private final String zzdnw;

    /* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
    public static final class Builder {
        /* access modifiers changed from: private */
        public String zzdnv = "";
        /* access modifiers changed from: private */
        public String zzdnw = "";

        public final ServerSideVerificationOptions build() {
            return new ServerSideVerificationOptions(this);
        }

        public final Builder setCustomData(String str) {
            this.zzdnw = str;
            return this;
        }

        public final Builder setUserId(String str) {
            this.zzdnv = str;
            return this;
        }
    }

    private ServerSideVerificationOptions(Builder builder) {
        this.zzdnv = builder.zzdnv;
        this.zzdnw = builder.zzdnw;
    }

    public String getCustomData() {
        return this.zzdnw;
    }

    public String getUserId() {
        return this.zzdnv;
    }
}
