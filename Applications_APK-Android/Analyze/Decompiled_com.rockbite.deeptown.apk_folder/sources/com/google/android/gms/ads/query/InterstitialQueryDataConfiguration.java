package com.google.android.gms.ads.query;

import android.content.Context;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public class InterstitialQueryDataConfiguration extends QueryDataConfiguration {
    @KeepForSdk
    public InterstitialQueryDataConfiguration(Context context, String str) {
        super(context, str);
    }
}
