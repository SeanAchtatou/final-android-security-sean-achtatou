package com.google.android.gms.ads;

import android.content.Context;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.ads.zzayk;
import com.google.android.gms.internal.ads.zzuj;
import com.google.android.gms.internal.ads.zzve;

@VisibleForTesting
/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class AdSize {
    public static final int AUTO_HEIGHT = -2;
    public static final AdSize BANNER = new AdSize(320, 50, "320x50_mb");
    public static final AdSize FLUID = new AdSize(-3, -4, "fluid");
    public static final AdSize FULL_BANNER = new AdSize(468, 60, "468x60_as");
    public static final int FULL_WIDTH = -1;
    public static final AdSize INVALID = new AdSize(0, 0, "invalid");
    public static final AdSize LARGE_BANNER = new AdSize(320, 100, "320x100_as");
    public static final AdSize LEADERBOARD = new AdSize(728, 90, "728x90_as");
    public static final AdSize MEDIUM_RECTANGLE = new AdSize(300, 250, "300x250_as");
    public static final AdSize SEARCH = new AdSize(-3, 0, "search_v2");
    public static final AdSize SMART_BANNER = new AdSize(-1, -2, "smart_banner");
    public static final AdSize WIDE_SKYSCRAPER = new AdSize(160, 600, "160x600_as");
    public static final AdSize zzabf = new AdSize(50, 50, "50x50_mb");
    private final int height;
    private final int width;
    private final String zzabg;
    private boolean zzabh;
    private boolean zzabi;
    private int zzabj;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AdSize(int r5, int r6) {
        /*
            r4 = this;
            r0 = -1
            if (r5 != r0) goto L_0x0006
            java.lang.String r0 = "FULL"
            goto L_0x000a
        L_0x0006:
            java.lang.String r0 = java.lang.String.valueOf(r5)
        L_0x000a:
            r1 = -2
            if (r6 != r1) goto L_0x0010
            java.lang.String r1 = "AUTO"
            goto L_0x0014
        L_0x0010:
            java.lang.String r1 = java.lang.String.valueOf(r6)
        L_0x0014:
            java.lang.String r2 = java.lang.String.valueOf(r0)
            int r2 = r2.length()
            int r2 = r2 + 4
            java.lang.String r3 = java.lang.String.valueOf(r1)
            int r3 = r3.length()
            int r2 = r2 + r3
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r2)
            r3.append(r0)
            java.lang.String r0 = "x"
            r3.append(r0)
            r3.append(r1)
            java.lang.String r0 = "_as"
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r4.<init>(r5, r6, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.AdSize.<init>(int, int):void");
    }

    public static AdSize getCurrentOrientationAnchoredAdaptiveBannerAdSize(Context context, int i2) {
        AdSize zza = zzayk.zza(context, i2, 50, 0);
        zza.zzabh = true;
        return zza;
    }

    @Deprecated
    public static AdSize getCurrentOrientationBannerAdSizeWithWidth(Context context, int i2) {
        return getCurrentOrientationAnchoredAdaptiveBannerAdSize(context, i2);
    }

    public static AdSize getLandscapeAnchoredAdaptiveBannerAdSize(Context context, int i2) {
        AdSize zza = zzayk.zza(context, i2, 50, 2);
        zza.zzabh = true;
        return zza;
    }

    @Deprecated
    public static AdSize getLandscapeBannerAdSizeWithWidth(Context context, int i2) {
        return getLandscapeAnchoredAdaptiveBannerAdSize(context, i2);
    }

    public static AdSize getPortraitAnchoredAdaptiveBannerAdSize(Context context, int i2) {
        AdSize zza = zzayk.zza(context, i2, 50, 1);
        zza.zzabh = true;
        return zza;
    }

    @Deprecated
    public static AdSize getPortraitBannerAdSizeWithWidth(Context context, int i2) {
        return getPortraitAnchoredAdaptiveBannerAdSize(context, i2);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof AdSize)) {
            return false;
        }
        AdSize adSize = (AdSize) obj;
        return this.width == adSize.width && this.height == adSize.height && this.zzabg.equals(adSize.zzabg);
    }

    public final int getHeight() {
        return this.height;
    }

    public final int getHeightInPixels(Context context) {
        int i2 = this.height;
        if (i2 == -4 || i2 == -3) {
            return -1;
        }
        if (i2 == -2) {
            return zzuj.zzc(context.getResources().getDisplayMetrics());
        }
        zzve.zzou();
        return zzayk.zza(context, this.height);
    }

    public final int getWidth() {
        return this.width;
    }

    public final int getWidthInPixels(Context context) {
        int i2 = this.width;
        if (i2 == -4 || i2 == -3) {
            return -1;
        }
        if (i2 == -1) {
            return zzuj.zzb(context.getResources().getDisplayMetrics());
        }
        zzve.zzou();
        return zzayk.zza(context, this.width);
    }

    public final int hashCode() {
        return this.zzabg.hashCode();
    }

    public final boolean isAutoHeight() {
        return this.height == -2;
    }

    public final boolean isFluid() {
        return this.width == -3 && this.height == -4;
    }

    public final boolean isFullWidth() {
        return this.width == -1;
    }

    public final String toString() {
        return this.zzabg;
    }

    /* access modifiers changed from: package-private */
    public final void zzc(boolean z) {
        this.zzabi = true;
    }

    /* access modifiers changed from: package-private */
    public final boolean zzdh() {
        return this.zzabh;
    }

    /* access modifiers changed from: package-private */
    public final boolean zzdi() {
        return this.zzabi;
    }

    /* access modifiers changed from: package-private */
    public final int zzdj() {
        return this.zzabj;
    }

    /* access modifiers changed from: package-private */
    public final void zzl(int i2) {
        this.zzabj = i2;
    }

    AdSize(int i2, int i3, String str) {
        if (i2 < 0 && i2 != -1 && i2 != -3) {
            StringBuilder sb = new StringBuilder(37);
            sb.append("Invalid width for AdSize: ");
            sb.append(i2);
            throw new IllegalArgumentException(sb.toString());
        } else if (i3 >= 0 || i3 == -2 || i3 == -4) {
            this.width = i2;
            this.height = i3;
            this.zzabg = str;
        } else {
            StringBuilder sb2 = new StringBuilder(38);
            sb2.append("Invalid height for AdSize: ");
            sb2.append(i3);
            throw new IllegalArgumentException(sb2.toString());
        }
    }
}
