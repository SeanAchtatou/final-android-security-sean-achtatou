package com.google.android.gms.internal.ads;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbor implements zzdxg<zzato> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzczl> zzfda;
    private final zzboo zzfhm;
    private final zzdxp<zzazb> zzfhn;
    private final zzdxp<zzatq> zzfho;

    private zzbor(zzboo zzboo, zzdxp<Context> zzdxp, zzdxp<zzazb> zzdxp2, zzdxp<zzczl> zzdxp3, zzdxp<zzatq> zzdxp4) {
        this.zzfhm = zzboo;
        this.zzejv = zzdxp;
        this.zzfhn = zzdxp2;
        this.zzfda = zzdxp3;
        this.zzfho = zzdxp4;
    }

    public static zzbor zza(zzboo zzboo, zzdxp<Context> zzdxp, zzdxp<zzazb> zzdxp2, zzdxp<zzczl> zzdxp3, zzdxp<zzatq> zzdxp4) {
        return new zzbor(zzboo, zzdxp, zzdxp2, zzdxp3, zzdxp4);
    }

    public final /* synthetic */ Object get() {
        Context context = this.zzejv.get();
        zzazb zzazb = this.zzfhn.get();
        zzczl zzczl = this.zzfda.get();
        zzatq zzatq = this.zzfho.get();
        zzatn zzatn = zzczl.zzgls;
        if (zzatn != null) {
            return new zzatf(context, zzazb, zzatn, zzczl.zzglo.zzdhr, zzatq);
        }
        return null;
    }
}
