package com.google.android.gms.internal.ads;

import android.view.View;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzato {
    void zza(String str, Map<String, String> map, int i2);

    String[] zza(String[] strArr);

    void zzdv(String str);

    void zzj(View view);

    zzatn zzuk();

    boolean zzul();

    void zzum();

    void zzun();
}
