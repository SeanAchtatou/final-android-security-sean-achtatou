package com.google.android.gms.internal.ads;

import android.content.Context;
import java.lang.ref.WeakReference;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcfc implements zzdxg<zzceq> {
    private final zzdxp<zzcka> zzejs;
    private final zzdxp<zzcea> zzeko;
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzazb> zzfdb;
    private final zzdxp<Executor> zzfei;
    private final zzdxp<ScheduledExecutorService> zzfpr;
    private final zzdxp<Context> zzfuf;
    private final zzdxp<WeakReference<Context>> zzfug;

    public zzcfc(zzdxp<Executor> zzdxp, zzdxp<Context> zzdxp2, zzdxp<WeakReference<Context>> zzdxp3, zzdxp<Executor> zzdxp4, zzdxp<zzcka> zzdxp5, zzdxp<ScheduledExecutorService> zzdxp6, zzdxp<zzcea> zzdxp7, zzdxp<zzazb> zzdxp8) {
        this.zzfei = zzdxp;
        this.zzfuf = zzdxp2;
        this.zzfug = zzdxp3;
        this.zzfcv = zzdxp4;
        this.zzejs = zzdxp5;
        this.zzfpr = zzdxp6;
        this.zzeko = zzdxp7;
        this.zzfdb = zzdxp8;
    }

    public final /* synthetic */ Object get() {
        return new zzceq(this.zzfei.get(), this.zzfuf.get(), this.zzfug.get(), this.zzfcv.get(), this.zzejs.get(), this.zzfpr.get(), this.zzeko.get(), this.zzfdb.get());
    }
}
