package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Process;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import com.google.android.gms.ads.internal.zzq;
import java.io.InputStream;
import java.util.Map;

@TargetApi(16)
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public class zzawh {
    private zzawh() {
    }

    public static boolean zzb(zzbdi zzbdi) {
        if (zzbdi == null) {
            return false;
        }
        zzbdi.onResume();
        return true;
    }

    public static zzawh zzcr(int i2) {
        if (i2 >= 28) {
            return new zzawo();
        }
        if (i2 >= 26) {
            return new zzawp();
        }
        if (i2 >= 24) {
            return new zzawm();
        }
        if (i2 >= 21) {
            return new zzawn();
        }
        if (i2 >= 19) {
            return new zzawk();
        }
        if (i2 >= 18) {
            return new zzawl();
        }
        if (i2 >= 17) {
            return new zzawi();
        }
        return new zzawh();
    }

    public static boolean zzwq() {
        int myUid = Process.myUid();
        return myUid == 0 || myUid == 1000;
    }

    public String getDefaultUserAgent(Context context) {
        return "";
    }

    public boolean isAttachedToWindow(View view) {
        return (view.getWindowToken() == null && view.getWindowVisibility() == 8) ? false : true;
    }

    public boolean zza(Activity activity, Configuration configuration) {
        return false;
    }

    public boolean zza(Context context, WebSettings webSettings) {
        zzayc.zza(context, new zzawg(context, webSettings));
        webSettings.setAllowFileAccessFromFileURLs(false);
        webSettings.setAllowUniversalAccessFromFileURLs(false);
        return true;
    }

    public void zzbc(Context context) {
    }

    public CookieManager zzbd(Context context) {
        if (zzwq()) {
            return null;
        }
        try {
            CookieSyncManager.createInstance(context);
            return CookieManager.getInstance();
        } catch (Throwable th) {
            zzayu.zzc("Failed to obtain CookieManager.", th);
            zzq.zzku().zza(th, "ApiLevelUtil.getCookieManager");
            return null;
        }
    }

    public void zzg(Activity activity) {
    }

    public int zzwo() {
        return 5;
    }

    public ViewGroup.LayoutParams zzwp() {
        return new ViewGroup.LayoutParams(-2, -2);
    }

    public int zzwr() {
        return 1;
    }

    public long zzws() {
        return -1;
    }

    public int zzb(ContentResolver contentResolver) {
        return Settings.System.getInt(contentResolver, "airplane_mode_on", 0);
    }

    public static boolean zza(zzbdi zzbdi) {
        if (zzbdi == null) {
            return false;
        }
        zzbdi.onPause();
        return true;
    }

    public zzbdl zza(zzbdi zzbdi, zzsm zzsm, boolean z) {
        return new zzbek(zzbdi, zzsm, z);
    }

    public Drawable zza(Context context, Bitmap bitmap, boolean z, float f2) {
        return new BitmapDrawable(context.getResources(), bitmap);
    }

    public zzte zza(Context context, TelephonyManager telephonyManager) {
        return zzte.ENUM_UNKNOWN;
    }

    public int zza(ContentResolver contentResolver) {
        return Settings.System.getInt(contentResolver, "wifi_on", 0);
    }

    public WebResourceResponse zza(String str, String str2, int i2, String str3, Map<String, String> map, InputStream inputStream) {
        return new WebResourceResponse(str, str2, inputStream);
    }
}
