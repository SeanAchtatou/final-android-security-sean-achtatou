package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzair implements Runnable {
    private final zzais zzczi;
    private final zzdq zzczj;
    private final zzajj zzczk;

    zzair(zzais zzais, zzdq zzdq, zzajj zzajj) {
        this.zzczi = zzais;
        this.zzczj = zzdq;
        this.zzczk = zzajj;
    }

    public final void run() {
        this.zzczi.zza(this.zzczj, this.zzczk);
    }
}
