package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbmd;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdbi<AdT extends zzbmd> {
    public zzczt zzelt;
    public zzbmz<AdT> zzgpc;
    public AdT zzgpd;
    public long zzgpe;
}
