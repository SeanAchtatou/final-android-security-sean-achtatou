package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcyv implements zzcxv {
    private final zzcyt zzgkr;

    zzcyv(zzcyt zzcyt) {
        this.zzgkr = zzcyt;
    }

    public final zzboe zzc(zzcxs zzcxs) {
        return this.zzgkr.zzd(zzcxs);
    }
}
