package com.google.android.gms.internal.ads;

import android.os.Bundle;
import com.tapjoy.TapjoyConstants;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcrg implements zzcty<Bundle> {
    private final double zzdmt;
    private final boolean zzdmu;

    public zzcrg(double d2, boolean z) {
        this.zzdmt = d2;
        this.zzdmu = z;
    }

    public final /* synthetic */ void zzr(Object obj) {
        Bundle bundle = (Bundle) obj;
        Bundle zza = zzdaa.zza(bundle, TapjoyConstants.TJC_NOTIFICATION_DEVICE_PREFIX);
        bundle.putBundle(TapjoyConstants.TJC_NOTIFICATION_DEVICE_PREFIX, zza);
        Bundle zza2 = zzdaa.zza(zza, "battery");
        zza.putBundle("battery", zza2);
        zza2.putBoolean("is_charging", this.zzdmu);
        zza2.putDouble("battery_level", this.zzdmt);
    }
}
