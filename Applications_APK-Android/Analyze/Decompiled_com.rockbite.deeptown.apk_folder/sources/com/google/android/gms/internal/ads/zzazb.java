package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

@SafeParcelable.Class(creator = "VersionInfoParcelCreator")
@SafeParcelable.Reserved({1})
/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzazb extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzazb> CREATOR = new zzaza();
    @SafeParcelable.Field(id = 2)
    public String zzbma;
    @SafeParcelable.Field(id = 3)
    public int zzdvz;
    @SafeParcelable.Field(id = 4)
    public int zzdwa;
    @SafeParcelable.Field(id = 5)
    public boolean zzdwb;
    @SafeParcelable.Field(id = 6)
    private boolean zzdwc;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzazb.<init>(int, int, boolean, boolean, boolean):void
     arg types: [int, int, boolean, int, int]
     candidates:
      com.google.android.gms.internal.ads.zzazb.<init>(java.lang.String, int, int, boolean, boolean):void
      com.google.android.gms.internal.ads.zzazb.<init>(int, int, boolean, boolean, boolean):void */
    public zzazb(int i2, int i3, boolean z) {
        this(i2, i3, z, false, false);
    }

    public static zzazb zzxm() {
        return new zzazb(12451009, 12451009, true);
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 2, this.zzbma, false);
        SafeParcelWriter.writeInt(parcel, 3, this.zzdvz);
        SafeParcelWriter.writeInt(parcel, 4, this.zzdwa);
        SafeParcelWriter.writeBoolean(parcel, 5, this.zzdwb);
        SafeParcelWriter.writeBoolean(parcel, 6, this.zzdwc);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzazb.<init>(int, int, boolean, boolean, boolean):void
     arg types: [int, int, int, int, int]
     candidates:
      com.google.android.gms.internal.ads.zzazb.<init>(java.lang.String, int, int, boolean, boolean):void
      com.google.android.gms.internal.ads.zzazb.<init>(int, int, boolean, boolean, boolean):void */
    public zzazb(int i2, int i3, boolean z, boolean z2) {
        this(i2, i3, true, false, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzazb.<init>(java.lang.String, int, int, boolean, boolean):void
     arg types: [java.lang.String, int, int, boolean, int]
     candidates:
      com.google.android.gms.internal.ads.zzazb.<init>(int, int, boolean, boolean, boolean):void
      com.google.android.gms.internal.ads.zzazb.<init>(java.lang.String, int, int, boolean, boolean):void */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private zzazb(int r8, int r9, boolean r10, boolean r11, boolean r12) {
        /*
            r7 = this;
            if (r10 == 0) goto L_0x0005
            java.lang.String r11 = "0"
            goto L_0x0007
        L_0x0005:
            java.lang.String r11 = "1"
        L_0x0007:
            int r12 = r11.length()
            int r12 = r12 + 36
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r12)
            java.lang.String r12 = "afma-sdk-a-v"
            r0.append(r12)
            r0.append(r8)
            java.lang.String r12 = "."
            r0.append(r12)
            r0.append(r9)
            r0.append(r12)
            r0.append(r11)
            java.lang.String r2 = r0.toString()
            r6 = 0
            r1 = r7
            r3 = r8
            r4 = r9
            r5 = r10
            r1.<init>(r2, r3, r4, r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzazb.<init>(int, int, boolean, boolean, boolean):void");
    }

    @SafeParcelable.Constructor
    zzazb(@SafeParcelable.Param(id = 2) String str, @SafeParcelable.Param(id = 3) int i2, @SafeParcelable.Param(id = 4) int i3, @SafeParcelable.Param(id = 5) boolean z, @SafeParcelable.Param(id = 6) boolean z2) {
        this.zzbma = str;
        this.zzdvz = i2;
        this.zzdwa = i3;
        this.zzdwb = z;
        this.zzdwc = z2;
    }
}
