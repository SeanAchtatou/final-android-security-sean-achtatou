package com.google.android.gms.internal.ads;

import android.view.View;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbua implements zzdxg<View> {
    private final zzbtv zzfje;

    private zzbua(zzbtv zzbtv) {
        this.zzfje = zzbtv;
    }

    public static zzbua zza(zzbtv zzbtv) {
        return new zzbua(zzbtv);
    }

    public final /* synthetic */ Object get() {
        return this.zzfje.zzaig();
    }
}
