package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbcb implements zzbbq {
    private final zzbbs zzeco;

    zzbcb(zzbbs zzbbs) {
        this.zzeco = zzbbs;
    }

    public final void zzb(boolean z, long j2) {
        this.zzeco.zzd(z, j2);
    }
}
