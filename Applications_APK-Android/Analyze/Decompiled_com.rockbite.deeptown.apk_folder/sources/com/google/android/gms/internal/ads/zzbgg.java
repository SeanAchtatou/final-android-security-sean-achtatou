package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbgg implements zzdxg<zzbij> {
    private final zzbga zzejr;
    private final zzdxp<zzbfx> zzejt;

    public zzbgg(zzbga zzbga, zzdxp<zzbfx> zzdxp) {
        this.zzejr = zzbga;
        this.zzejt = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return (zzbij) zzdxm.zza(this.zzejt.get(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
