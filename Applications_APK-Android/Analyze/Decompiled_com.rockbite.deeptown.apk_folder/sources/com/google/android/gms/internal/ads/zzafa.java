package com.google.android.gms.internal.ads;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.text.TextUtils;
import com.appsflyer.share.Constants;
import com.tapjoy.TJAdUnitConstants;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzafa {
    public static final zzafn<zzbdi> zzcwu = zzaez.zzcwt;
    public static final zzafn<zzbdi> zzcwv = zzafc.zzcwt;
    public static final zzafn<zzbdi> zzcww = zzafb.zzcwt;
    public static final zzafn<zzbdi> zzcwx = new zzafg();
    public static final zzafn<zzbdi> zzcwy = new zzaff();
    public static final zzafn<zzbdi> zzcwz = zzafe.zzcwt;
    public static final zzafn<Object> zzcxa = new zzafi();
    public static final zzafn<zzbdi> zzcxb = new zzafh();
    public static final zzafn<zzbdi> zzcxc = zzafd.zzcwt;
    public static final zzafn<zzbdi> zzcxd = new zzafk();
    public static final zzafn<zzbdi> zzcxe = new zzafj();
    public static final zzafn<zzbaz> zzcxf = new zzbcg();
    public static final zzafn<zzbaz> zzcxg = new zzbcj();
    public static final zzafn<zzbdi> zzcxh = new zzaex();
    public static final zzaft zzcxi = new zzaft();
    public static final zzafn<zzbdi> zzcxj = new zzafm();
    public static final zzafn<zzbdi> zzcxk = new zzafl();
    public static final zzafn<zzbdi> zzcxl = new zzafo();

    static final /* synthetic */ void zza(zzbeq zzbeq, Map map) {
        String str = (String) map.get("tx");
        String str2 = (String) map.get("ty");
        String str3 = (String) map.get("td");
        try {
            int parseInt = Integer.parseInt(str);
            int parseInt2 = Integer.parseInt(str2);
            int parseInt3 = Integer.parseInt(str3);
            zzdq zzaad = zzbeq.zzaad();
            if (zzaad != null) {
                zzaad.zzbw().zza(parseInt, parseInt2, parseInt3);
            }
        } catch (NumberFormatException unused) {
            zzayu.zzez("Could not parse touch parameters from gmsg.");
        }
    }

    static final /* synthetic */ void zzb(zzbei zzbei, Map map) {
        PackageManager packageManager = zzbei.getContext().getPackageManager();
        try {
            try {
                JSONArray jSONArray = new JSONObject((String) map.get(TJAdUnitConstants.String.DATA)).getJSONArray("intents");
                JSONObject jSONObject = new JSONObject();
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    try {
                        JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                        String optString = jSONObject2.optString("id");
                        String optString2 = jSONObject2.optString("u");
                        String optString3 = jSONObject2.optString("i");
                        String optString4 = jSONObject2.optString("m");
                        String optString5 = jSONObject2.optString("p");
                        String optString6 = jSONObject2.optString(Constants.URL_CAMPAIGN);
                        jSONObject2.optString("f");
                        jSONObject2.optString("e");
                        String optString7 = jSONObject2.optString("intent_url");
                        Intent intent = null;
                        if (!TextUtils.isEmpty(optString7)) {
                            try {
                                intent = Intent.parseUri(optString7, 0);
                            } catch (URISyntaxException e2) {
                                URISyntaxException uRISyntaxException = e2;
                                String valueOf = String.valueOf(optString7);
                                zzayu.zzc(valueOf.length() != 0 ? "Error parsing the url: ".concat(valueOf) : new String("Error parsing the url: "), uRISyntaxException);
                            }
                        }
                        boolean z = true;
                        if (intent == null) {
                            intent = new Intent();
                            if (!TextUtils.isEmpty(optString2)) {
                                intent.setData(Uri.parse(optString2));
                            }
                            if (!TextUtils.isEmpty(optString3)) {
                                intent.setAction(optString3);
                            }
                            if (!TextUtils.isEmpty(optString4)) {
                                intent.setType(optString4);
                            }
                            if (!TextUtils.isEmpty(optString5)) {
                                intent.setPackage(optString5);
                            }
                            if (!TextUtils.isEmpty(optString6)) {
                                String[] split = optString6.split(Constants.URL_PATH_DELIMITER, 2);
                                if (split.length == 2) {
                                    intent.setComponent(new ComponentName(split[0], split[1]));
                                }
                            }
                        }
                        if (packageManager.resolveActivity(intent, 65536) == null) {
                            z = false;
                        }
                        try {
                            jSONObject.put(optString, z);
                        } catch (JSONException e3) {
                            zzayu.zzc("Error constructing openable urls response.", e3);
                        }
                    } catch (JSONException e4) {
                        zzayu.zzc("Error parsing the intent data.", e4);
                    }
                }
                ((zzahs) zzbei).zzb("openableIntents", jSONObject);
            } catch (JSONException unused) {
                ((zzahs) zzbei).zzb("openableIntents", new JSONObject());
            }
        } catch (JSONException unused2) {
            ((zzahs) zzbei).zzb("openableIntents", new JSONObject());
        }
    }

    static final /* synthetic */ void zzc(zzbei zzbei, Map map) {
        String str = (String) map.get("urls");
        if (TextUtils.isEmpty(str)) {
            zzayu.zzez("URLs missing in canOpenURLs GMSG.");
            return;
        }
        String[] split = str.split(",");
        HashMap hashMap = new HashMap();
        PackageManager packageManager = zzbei.getContext().getPackageManager();
        for (String str2 : split) {
            String[] split2 = str2.split(";", 2);
            boolean z = true;
            if (packageManager.resolveActivity(new Intent(split2.length > 1 ? split2[1].trim() : "android.intent.action.VIEW", Uri.parse(split2[0].trim())), 65536) == null) {
                z = false;
            }
            hashMap.put(str2, Boolean.valueOf(z));
        }
        ((zzahs) zzbei).zza("openableURLs", hashMap);
    }

    static final /* synthetic */ void zza(zzbei zzbei, Map map) {
        String str = (String) map.get("u");
        if (str == null) {
            zzayu.zzez("URL missing from httpTrack GMSG.");
        } else {
            new zzayb(zzbei.getContext(), ((zzbet) zzbei).zzyr().zzbma, str).zzvr();
        }
    }

    static final /* synthetic */ void zza(zzahs zzahs, Map map) {
        String str = (String) map.get("u");
        if (str == null) {
            zzayu.zzez("URL missing from click GMSG.");
            return;
        }
        Uri parse = Uri.parse(str);
        try {
            zzdq zzaad = ((zzbeq) zzahs).zzaad();
            if (zzaad != null && zzaad.zzb(parse)) {
                parse = zzaad.zza(parse, ((zzbei) zzahs).getContext(), ((zzbes) zzahs).getView(), ((zzbei) zzahs).zzyn());
            }
        } catch (zzdt unused) {
            String valueOf = String.valueOf(str);
            zzayu.zzez(valueOf.length() != 0 ? "Unable to append parameter to URL: ".concat(valueOf) : new String("Unable to append parameter to URL: "));
        }
        zzbei zzbei = (zzbei) zzahs;
        new zzayb(zzbei.getContext(), ((zzbet) zzahs).zzyr().zzbma, zzauk.zzb(parse, zzbei.getContext())).zzvr();
    }
}
