package com.google.android.gms.internal.ads;

import android.view.View;
import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public class zzbkn {
    private final View view;
    private final zzbdi zzcza;
    private final zzczk zzfdo;
    private final zzbme zzfdz;

    public zzbkn(View view2, zzbdi zzbdi, zzbme zzbme, zzczk zzczk) {
        this.view = view2;
        this.zzcza = zzbdi;
        this.zzfdz = zzbme;
        this.zzfdo = zzczk;
    }

    public zzbpw zza(Set<zzbsu<zzbqb>> set) {
        return new zzbpw(set);
    }

    public final zzbdi zzaft() {
        return this.zzcza;
    }

    public final View zzaga() {
        return this.view;
    }

    public final zzbme zzagh() {
        return this.zzfdz;
    }

    public final zzczk zzagi() {
        return this.zzfdo;
    }
}
