package com.google.android.gms.internal.ads;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.internal.ads.zzso;
import com.google.android.gms.internal.ads.zzsy;
import java.util.ArrayList;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcik {
    private zzazb zzdij;
    private zzcht zzfxo;
    private zzsm zzfya;
    private Context zzup;

    public zzcik(Context context, zzazb zzazb, zzsm zzsm, zzcht zzcht) {
        this.zzup = context;
        this.zzdij = zzazb;
        this.zzfya = zzsm;
        this.zzfxo = zzcht;
    }

    public final void zzamc() {
        try {
            this.zzfxo.zza(new zzcij(this));
        } catch (Exception e2) {
            String valueOf = String.valueOf(e2.getMessage());
            zzayu.zzex(valueOf.length() != 0 ? "Error in offline signals database startup: ".concat(valueOf) : new String("Error in offline signals database startup: "));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* access modifiers changed from: package-private */
    public final /* synthetic */ Void zzb(SQLiteDatabase sQLiteDatabase) throws Exception {
        SQLiteDatabase sQLiteDatabase2 = sQLiteDatabase;
        ArrayList<zzsy.zzj.zza> zza = zzcih.zza(sQLiteDatabase);
        zzsy.zzj zzj = (zzsy.zzj) zzsy.zzj.zzno().zzca(this.zzup.getPackageName()).zzcb(Build.MODEL).zzcd(zzcih.zza(sQLiteDatabase2, 0)).zzc(zza).zzce(zzcih.zza(sQLiteDatabase2, 1)).zzes(zzq.zzkx().currentTimeMillis()).zzet(zzcih.zzb(sQLiteDatabase2, 2)).zzbaf();
        int size = zza.size();
        long j2 = 0;
        int i2 = 0;
        while (i2 < size) {
            zzsy.zzj.zza zza2 = zza.get(i2);
            i2++;
            zzsy.zzj.zza zza3 = zza2;
            if (zza3.zznq() == zzte.ENUM_TRUE && zza3.getTimestamp() > j2) {
                j2 = zza3.getTimestamp();
            }
        }
        if (j2 != 0) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("value", Long.valueOf(j2));
            sQLiteDatabase2.update("offline_signal_statistics", contentValues, "statistic_name = 'last_successful_request_time'", null);
        }
        this.zzfya.zza(new zzcim(zzj));
        zztt zztt = new zztt();
        zztt.zzcam = Integer.valueOf(this.zzdij.zzdvz);
        zztt.zzcan = Integer.valueOf(this.zzdij.zzdwa);
        zztt.zzcao = Integer.valueOf(this.zzdij.zzdwb ? 0 : 2);
        this.zzfya.zza(new zzcil(zztt));
        this.zzfya.zza(zzso.zza.C0151zza.OFFLINE_UPLOAD);
        sQLiteDatabase2.delete("offline_signal_contents", null, null);
        ContentValues contentValues2 = new ContentValues();
        contentValues2.put("value", (Integer) 0);
        sQLiteDatabase2.update("offline_signal_statistics", contentValues2, "statistic_name = ?", new String[]{"failed_requests"});
        ContentValues contentValues3 = new ContentValues();
        contentValues3.put("value", (Integer) 0);
        sQLiteDatabase2.update("offline_signal_statistics", contentValues3, "statistic_name = ?", new String[]{"total_requests"});
        return null;
    }
}
