package com.google.android.gms.drive;

import android.os.Parcel;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public abstract class zzu extends AbstractSafeParcelable {
    private volatile transient boolean zzbt = false;

    public void writeToParcel(Parcel parcel, int i2) {
        Preconditions.checkState(!this.zzbt);
        this.zzbt = true;
        zza(parcel, i2);
    }

    /* access modifiers changed from: protected */
    public abstract void zza(Parcel parcel, int i2);
}
