package com.google.android.gms.internal.ads;

import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzafn<ContextT> {
    void zza(ContextT contextt, Map<String, String> map);
}
