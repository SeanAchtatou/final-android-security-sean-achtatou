package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zza {
    void initialize();

    zzd zza(String str);

    void zza(String str, zzd zzd);
}
