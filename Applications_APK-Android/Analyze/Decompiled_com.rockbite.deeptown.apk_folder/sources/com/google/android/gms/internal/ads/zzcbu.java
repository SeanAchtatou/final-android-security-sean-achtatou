package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.overlay.zzt;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcbu implements zzt {
    private final zzcbp zzfrj;

    zzcbu(zzcbp zzcbp) {
        this.zzfrj = zzcbp;
    }

    public final void zztv() {
        this.zzfrj.zzakv();
    }
}
