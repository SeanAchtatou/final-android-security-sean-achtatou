package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import com.facebook.internal.NativeProtocol;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzafy implements zzafn<Object> {
    private final zzafx zzcxu;

    private zzafy(zzafx zzafx) {
        this.zzcxu = zzafx;
    }

    public static void zza(zzbdi zzbdi, zzafx zzafx) {
        zzbdi.zza("/reward", new zzafy(zzafx));
    }

    public final void zza(Object obj, Map<String, String> map) {
        String str = map.get(NativeProtocol.WEB_DIALOG_ACTION);
        if ("grant".equals(str)) {
            zzasd zzasd = null;
            try {
                int parseInt = Integer.parseInt(map.get("amount"));
                String str2 = map.get("type");
                if (!TextUtils.isEmpty(str2)) {
                    zzasd = new zzasd(str2, parseInt);
                }
            } catch (NumberFormatException e2) {
                zzayu.zzd("Unable to parse reward amount.", e2);
            }
            this.zzcxu.zza(zzasd);
        } else if ("video_start".equals(str)) {
            this.zzcxu.zzrs();
        } else if ("video_complete".equals(str)) {
            this.zzcxu.zzrt();
        }
    }
}
