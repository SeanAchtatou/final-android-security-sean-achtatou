package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzblz implements zzdxg<zzbqp> {
    private final zzbma zzffl;

    public zzblz(zzbma zzbma) {
        this.zzffl = zzbma;
    }

    public final /* synthetic */ Object get() {
        return (zzbqp) zzdxm.zza(this.zzffl.zzagq(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
