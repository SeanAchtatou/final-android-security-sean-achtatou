package com.google.android.gms.internal.ads;

import java.util.List;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbyv implements zzded {
    private final JSONObject zzfcs;
    private final zzbyu zzfpl;

    zzbyv(zzbyu zzbyu, JSONObject jSONObject) {
        this.zzfpl = zzbyu;
        this.zzfcs = jSONObject;
    }

    public final Object apply(Object obj) {
        return this.zzfpl.zza(this.zzfcs, (List) obj);
    }
}
