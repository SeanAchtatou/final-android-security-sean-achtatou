package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcja implements zzdgf {
    private final zzczl zzfel;
    private final zzczt zzfot;
    private final zzcix zzfym;

    zzcja(zzcix zzcix, zzczt zzczt, zzczl zzczl) {
        this.zzfym = zzcix;
        this.zzfot = zzczt;
        this.zzfel = zzczl;
    }

    public final zzdhe zzf(Object obj) {
        return this.zzfym.zzb(this.zzfot, this.zzfel, obj);
    }
}
