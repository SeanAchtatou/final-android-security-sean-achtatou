package com.google.android.gms.internal.ads;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzbha implements zzcwx {
    private final /* synthetic */ zzbgr zzerr;
    private zzdxp<Context> zzewd;
    private zzdxp<String> zzewe;
    private zzdxp<zzcxt<zzbka, zzbke>> zzewf;
    private zzdxp<zzcwz> zzewg;
    private zzdxp<zzcwl> zzewh;
    private zzdxp<zzcwr> zzewi;

    private zzbha(zzbgr zzbgr, Context context, String str) {
        this.zzerr = zzbgr;
        this.zzewd = zzdxf.zzbe(context);
        this.zzewe = zzdxf.zzbe(str);
        this.zzewf = new zzcxu(this.zzewd, this.zzerr.zzelm, this.zzerr.zzeln);
        this.zzewg = zzdxd.zzan(new zzcxc(this.zzerr.zzelm));
        this.zzewh = zzdxd.zzan(new zzcwo(this.zzewd, this.zzerr.zzejz, this.zzerr.zzejt, this.zzewf, this.zzewg, zzczz.zzaot()));
        this.zzewi = zzdxd.zzan(new zzcwu(this.zzerr.zzejt, this.zzewd, this.zzewe, this.zzewh, this.zzewg, this.zzerr.zzekg));
    }

    public final zzcwr zzaea() {
        return this.zzewi.get();
    }
}
