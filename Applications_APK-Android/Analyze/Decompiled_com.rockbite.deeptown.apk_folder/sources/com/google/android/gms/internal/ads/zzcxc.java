package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcxc implements zzdxg<zzcwz> {
    private final zzdxp<zzdax> zzgjl;

    public zzcxc(zzdxp<zzdax> zzdxp) {
        this.zzgjl = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return new zzcwz(this.zzgjl.get());
    }
}
