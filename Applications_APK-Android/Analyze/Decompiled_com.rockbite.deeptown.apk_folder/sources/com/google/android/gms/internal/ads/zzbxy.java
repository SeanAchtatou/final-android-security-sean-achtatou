package com.google.android.gms.internal.ads;

import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbxy implements zzafn {
    private final zzbxr zzfof;

    zzbxy(zzbxr zzbxr) {
        this.zzfof = zzbxr;
    }

    public final void zza(Object obj, Map map) {
        this.zzfof.zza((zzbdi) obj, map);
    }
}
