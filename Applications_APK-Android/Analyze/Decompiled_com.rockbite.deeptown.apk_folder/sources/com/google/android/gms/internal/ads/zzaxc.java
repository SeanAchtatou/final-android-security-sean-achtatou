package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

@SafeParcelable.Class(creator = "ExceptionParcelCreator")
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzaxc extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzaxc> CREATOR = new zzaxe();
    @SafeParcelable.Field(id = 2)
    public final int errorCode;
    @SafeParcelable.Field(id = 1)
    public final String zzdtr;

    @SafeParcelable.Constructor
    zzaxc(@SafeParcelable.Param(id = 1) String str, @SafeParcelable.Param(id = 2) int i2) {
        this.zzdtr = str == null ? "" : str;
        this.errorCode = i2;
    }

    public static zzaxc zza(Throwable th, int i2) {
        return new zzaxc(th.getMessage(), i2);
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.zzdtr, false);
        SafeParcelWriter.writeInt(parcel, 2, this.errorCode);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
