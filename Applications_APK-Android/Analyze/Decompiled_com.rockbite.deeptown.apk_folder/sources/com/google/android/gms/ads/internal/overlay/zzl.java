package com.google.android.gms.ads.internal.overlay;

import android.app.Activity;
import android.graphics.Bitmap;
import com.google.android.gms.ads.internal.zzg;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.internal.ads.zzavo;
import com.google.android.gms.internal.ads.zzawb;
import com.google.android.gms.internal.ads.zzawh;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzl extends zzavo {
    final /* synthetic */ zzc zzdho;

    private zzl(zzc zzc) {
        this.zzdho = zzc;
    }

    public final void zztu() {
        Bitmap zza = zzq.zzlj().zza(Integer.valueOf(this.zzdho.zzdgn.zzdhx.zzblc));
        if (zza != null) {
            zzawh zzks = zzq.zzks();
            zzc zzc = this.zzdho;
            Activity activity = zzc.zzzk;
            zzg zzg = zzc.zzdgn.zzdhx;
            zzawb.zzdsr.post(new zzk(this, zzks.zza(activity, zza, zzg.zzbla, zzg.zzblb)));
        }
    }
}
