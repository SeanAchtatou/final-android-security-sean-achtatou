package com.google.android.gms.internal.ads;

import android.widget.RelativeLayout;
import com.google.android.gms.ads.internal.zzq;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzcwv implements zzcoz<zzbke> {
    private final /* synthetic */ zzcwr zzgjd;

    zzcwv(zzcwr zzcwr) {
        this.zzgjd = zzcwr;
    }

    public final /* synthetic */ void onSuccess(Object obj) {
        zzbke zzbke = (zzbke) obj;
        zzbke zzbke2 = this.zzgjd.zzgiz;
        if (zzbke2 != null) {
            zzbke2.destroy();
        }
        zzcwr zzcwr = this.zzgjd;
        zzcwr.zzgiz = zzbke;
        zzcwr.zzfdu.removeAllViews();
        this.zzgjd.zzfdu.addView(zzbke.zzaga(), zzq.zzks().zzwp());
        if (this.zzgjd.zzbli.zzdwa < ((Integer) zzve.zzoy().zzd(zzzn.zzcoh)).intValue()) {
            com.google.android.gms.ads.internal.overlay.zzq zza = this.zzgjd.zza(zzbke);
            RelativeLayout.LayoutParams zzb = zzcwr.zzb(zzbke);
            zza.zzal(zzbke.zzagb());
            this.zzgjd.zzfdu.addView(zza, zzb);
        }
        this.zzgjd.zzc(zzbke);
        this.zzgjd.zzfdu.setMinimumHeight(this.zzgjd.zzaod().heightPixels);
        this.zzgjd.zzfdu.setMinimumWidth(this.zzgjd.zzaod().widthPixels);
        this.zzgjd.zzgix.zzb(new zzbkg(zzbke, this.zzgjd));
        zzbke.zzagf();
    }

    public final void zzamx() {
        this.zzgjd.zzgiz = null;
    }
}
