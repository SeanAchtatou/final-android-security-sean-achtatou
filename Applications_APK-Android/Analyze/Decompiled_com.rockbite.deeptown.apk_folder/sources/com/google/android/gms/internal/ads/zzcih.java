package com.google.android.gms.internal.ads;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.google.android.gms.internal.ads.zzsy;
import java.util.ArrayList;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcih {
    public static ArrayList<zzsy.zzj.zza> zza(SQLiteDatabase sQLiteDatabase) {
        ArrayList<zzsy.zzj.zza> arrayList = new ArrayList<>();
        Cursor query = sQLiteDatabase.query("offline_signal_contents", new String[]{"serialized_proto_data"}, null, null, null, null, null);
        while (query.moveToNext()) {
            try {
                arrayList.add(zzsy.zzj.zza.zzg(query.getBlob(query.getColumnIndexOrThrow("serialized_proto_data"))));
            } catch (zzdse e2) {
                zzayu.zzex("Unable to deserialize proto from offline signals database:");
                zzayu.zzex(e2.getMessage());
            }
        }
        query.close();
        return arrayList;
    }

    public static long zzb(SQLiteDatabase sQLiteDatabase, int i2) {
        Cursor zzc = zzc(sQLiteDatabase, 2);
        long j2 = 0;
        if (zzc.getCount() > 0) {
            zzc.moveToNext();
            j2 = 0 + zzc.getLong(zzc.getColumnIndexOrThrow("value"));
        }
        zzc.close();
        return j2;
    }

    private static Cursor zzc(SQLiteDatabase sQLiteDatabase, int i2) {
        String[] strArr = {"value"};
        String[] strArr2 = new String[1];
        if (i2 == 0) {
            strArr2[0] = "failed_requests";
        } else if (i2 == 1) {
            strArr2[0] = "total_requests";
        } else if (i2 == 2) {
            strArr2[0] = "last_successful_request_time";
        }
        return sQLiteDatabase.query("offline_signal_statistics", strArr, "statistic_name = ?", strArr2, null, null, null);
    }

    public static int zza(SQLiteDatabase sQLiteDatabase, int i2) {
        int i3 = 0;
        if (i2 == 2) {
            return 0;
        }
        Cursor zzc = zzc(sQLiteDatabase, i2);
        if (zzc.getCount() > 0) {
            zzc.moveToNext();
            i3 = 0 + zzc.getInt(zzc.getColumnIndexOrThrow("value"));
        }
        zzc.close();
        return i3;
    }
}
