package com.google.android.gms.internal.ads;

import android.content.Context;
import com.esotericsoftware.spine.Animation;
import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.ads.internal.overlay.zzn;
import com.google.android.gms.ads.internal.overlay.zzt;
import com.google.android.gms.ads.internal.zzg;
import com.google.android.gms.ads.internal.zzq;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzcmo implements zzbuv {
    private final zzazb zzbli;
    private final zzczl zzffc;
    private final zzczu zzfgl;
    private final zzcbn zzfod;
    private final zzdhe<zzcbd> zzfzp;
    private final zzbdi zzgas;
    private final Context zzup;

    private zzcmo(Context context, zzcbn zzcbn, zzczu zzczu, zzazb zzazb, zzczl zzczl, zzdhe<zzcbd> zzdhe, zzbdi zzbdi) {
        this.zzup = context;
        this.zzfod = zzcbn;
        this.zzfgl = zzczu;
        this.zzbli = zzazb;
        this.zzffc = zzczl;
        this.zzfzp = zzdhe;
        this.zzgas = zzbdi;
    }

    public final void zza(boolean z, Context context) {
        zzbdi zzbdi;
        zzbdi zzbdi2;
        zzcbd zzcbd = (zzcbd) zzdgs.zzc(this.zzfzp);
        try {
            zzczl zzczl = this.zzffc;
            if (!this.zzgas.zzaap()) {
                zzbdi2 = this.zzgas;
            } else {
                if (!((Boolean) zzve.zzoy().zzd(zzzn.zzciu)).booleanValue()) {
                    zzbdi2 = this.zzgas;
                } else {
                    zzbdi zzc = this.zzfod.zzc(this.zzfgl.zzblm);
                    zzafy.zza(zzc, zzcbd.zzaev());
                    zzccd zzccd = new zzccd();
                    zzccd.zza(this.zzup, zzc.getView());
                    zzcbd.zzadx().zzb(zzc, true);
                    zzc.zzaaa().zza(new zzcmn(zzccd, zzc));
                    zzbev zzaaa = zzc.zzaaa();
                    zzc.getClass();
                    zzaaa.zza(zzcmq.zzq(zzc));
                    zzc.zzb(zzczl.zzglo.zzdhr, zzczl.zzglo.zzdht, null);
                    zzbdi = zzc;
                    zzbdi.zzax(true);
                    zzq.zzkq();
                    boolean zzbb = zzawb.zzbb(this.zzup);
                    zzczl zzczl2 = this.zzffc;
                    zzg zzg = new zzg(false, zzbb, false, Animation.CurveTimeline.LINEAR, -1, z, zzczl2.zzglv, zzczl2.zzblf);
                    zzq.zzkp();
                    zzbun zzaeo = zzcbd.zzaeo();
                    zzczl zzczl3 = this.zzffc;
                    int i2 = zzczl3.zzglw;
                    zzazb zzazb = this.zzbli;
                    String str = zzczl3.zzdkp;
                    zzczp zzczp = zzczl3.zzglo;
                    String str2 = zzczp.zzdhr;
                    zzn.zza(context, new AdOverlayInfoParcel((zzty) null, zzaeo, (zzt) null, zzbdi, i2, zzazb, str, zzg, str2, zzczp.zzdht), true);
                }
            }
            zzbdi = zzbdi2;
            zzbdi.zzax(true);
            zzq.zzkq();
            boolean zzbb2 = zzawb.zzbb(this.zzup);
            zzczl zzczl22 = this.zzffc;
            zzg zzg2 = new zzg(false, zzbb2, false, Animation.CurveTimeline.LINEAR, -1, z, zzczl22.zzglv, zzczl22.zzblf);
            zzq.zzkp();
            zzbun zzaeo2 = zzcbd.zzaeo();
            zzczl zzczl32 = this.zzffc;
            int i22 = zzczl32.zzglw;
            zzazb zzazb2 = this.zzbli;
            String str3 = zzczl32.zzdkp;
            zzczp zzczp2 = zzczl32.zzglo;
            String str22 = zzczp2.zzdhr;
            zzn.zza(context, new AdOverlayInfoParcel((zzty) null, zzaeo2, (zzt) null, zzbdi, i22, zzazb2, str3, zzg2, str22, zzczp2.zzdht), true);
        } catch (zzbdv e2) {
            zzayu.zzc("", e2);
        }
    }
}
