package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.internal.BaseImplementation;

/* compiled from: com.google.android.gms:play-services-base@@17.1.0 */
public final class zaaf implements zabb {
    /* access modifiers changed from: private */
    public final zabe zafv;
    private boolean zafw = false;

    public zaaf(zabe zabe) {
        this.zafv = zabe;
    }

    public final void begin() {
    }

    public final void connect() {
        if (this.zafw) {
            this.zafw = false;
            this.zafv.zaa(new zaah(this, this));
        }
    }

    public final boolean disconnect() {
        if (this.zafw) {
            return false;
        }
        if (this.zafv.zaeh.zaav()) {
            this.zafw = true;
            for (zack zabt : this.zafv.zaeh.zahi) {
                zabt.zabt();
            }
            return false;
        }
        this.zafv.zaf(null);
        return true;
    }

    public final <A extends Api.AnyClient, R extends Result, T extends BaseImplementation.ApiMethodImpl<R, A>> T enqueue(T t) {
        return execute(t);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final <A extends com.google.android.gms.common.api.Api.AnyClient, T extends com.google.android.gms.common.api.internal.BaseImplementation.ApiMethodImpl<? extends com.google.android.gms.common.api.Result, A>> T execute(T r4) {
        /*
            r3 = this;
            com.google.android.gms.common.api.internal.zabe r0 = r3.zafv     // Catch:{ DeadObjectException -> 0x004b }
            com.google.android.gms.common.api.internal.zaaw r0 = r0.zaeh     // Catch:{ DeadObjectException -> 0x004b }
            com.google.android.gms.common.api.internal.zacp r0 = r0.zahj     // Catch:{ DeadObjectException -> 0x004b }
            r0.zac(r4)     // Catch:{ DeadObjectException -> 0x004b }
            com.google.android.gms.common.api.internal.zabe r0 = r3.zafv     // Catch:{ DeadObjectException -> 0x004b }
            com.google.android.gms.common.api.internal.zaaw r0 = r0.zaeh     // Catch:{ DeadObjectException -> 0x004b }
            com.google.android.gms.common.api.Api$AnyClientKey r1 = r4.getClientKey()     // Catch:{ DeadObjectException -> 0x004b }
            java.util.Map<com.google.android.gms.common.api.Api$AnyClientKey<?>, com.google.android.gms.common.api.Api$Client> r0 = r0.zahd     // Catch:{ DeadObjectException -> 0x004b }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ DeadObjectException -> 0x004b }
            com.google.android.gms.common.api.Api$Client r0 = (com.google.android.gms.common.api.Api.Client) r0     // Catch:{ DeadObjectException -> 0x004b }
            java.lang.String r1 = "Appropriate Api was not requested."
            com.google.android.gms.common.internal.Preconditions.checkNotNull(r0, r1)     // Catch:{ DeadObjectException -> 0x004b }
            boolean r1 = r0.isConnected()     // Catch:{ DeadObjectException -> 0x004b }
            if (r1 != 0) goto L_0x003d
            com.google.android.gms.common.api.internal.zabe r1 = r3.zafv     // Catch:{ DeadObjectException -> 0x004b }
            java.util.Map<com.google.android.gms.common.api.Api$AnyClientKey<?>, com.google.android.gms.common.ConnectionResult> r1 = r1.zaht     // Catch:{ DeadObjectException -> 0x004b }
            com.google.android.gms.common.api.Api$AnyClientKey r2 = r4.getClientKey()     // Catch:{ DeadObjectException -> 0x004b }
            boolean r1 = r1.containsKey(r2)     // Catch:{ DeadObjectException -> 0x004b }
            if (r1 == 0) goto L_0x003d
            com.google.android.gms.common.api.Status r0 = new com.google.android.gms.common.api.Status     // Catch:{ DeadObjectException -> 0x004b }
            r1 = 17
            r0.<init>(r1)     // Catch:{ DeadObjectException -> 0x004b }
            r4.setFailedResult(r0)     // Catch:{ DeadObjectException -> 0x004b }
            goto L_0x0055
        L_0x003d:
            boolean r1 = r0 instanceof com.google.android.gms.common.internal.SimpleClientAdapter     // Catch:{ DeadObjectException -> 0x004b }
            if (r1 == 0) goto L_0x0047
            com.google.android.gms.common.internal.SimpleClientAdapter r0 = (com.google.android.gms.common.internal.SimpleClientAdapter) r0     // Catch:{ DeadObjectException -> 0x004b }
            com.google.android.gms.common.api.Api$SimpleClient r0 = r0.getClient()     // Catch:{ DeadObjectException -> 0x004b }
        L_0x0047:
            r4.run(r0)     // Catch:{ DeadObjectException -> 0x004b }
            goto L_0x0055
        L_0x004b:
            com.google.android.gms.common.api.internal.zabe r0 = r3.zafv
            com.google.android.gms.common.api.internal.zaai r1 = new com.google.android.gms.common.api.internal.zaai
            r1.<init>(r3, r3)
            r0.zaa(r1)
        L_0x0055:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.zaaf.execute(com.google.android.gms.common.api.internal.BaseImplementation$ApiMethodImpl):com.google.android.gms.common.api.internal.BaseImplementation$ApiMethodImpl");
    }

    public final void onConnected(Bundle bundle) {
    }

    public final void onConnectionSuspended(int i2) {
        this.zafv.zaf(null);
        this.zafv.zahx.zab(i2, this.zafw);
    }

    public final void zaa(ConnectionResult connectionResult, Api<?> api, boolean z) {
    }

    /* access modifiers changed from: package-private */
    public final void zaak() {
        if (this.zafw) {
            this.zafw = false;
            this.zafv.zaeh.zahj.release();
            disconnect();
        }
    }
}
