package com.google.android.gms.internal.ads;

import android.os.Bundle;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzctb implements zzcty<Bundle> {
    private final Bundle zzfhf;

    public zzctb(Bundle bundle) {
        this.zzfhf = bundle;
    }

    public final /* synthetic */ void zzr(Object obj) {
        Bundle bundle = (Bundle) obj;
        Bundle bundle2 = this.zzfhf;
        if (bundle2 != null) {
            bundle.putAll(bundle2);
        }
    }
}
