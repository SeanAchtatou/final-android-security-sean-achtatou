package com.google.android.gms.internal.ads;

import android.view.MotionEvent;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzcag implements zzabw {
    private final /* synthetic */ zzcad zzfqe;

    zzcag(zzcad zzcad) {
        this.zzfqe = zzcad;
    }

    public final void zzc(MotionEvent motionEvent) {
    }

    public final void zzrd() {
        this.zzfqe.zzfnf.zzfu(NativeCustomTemplateAd.ASSET_NAME_VIDEO);
    }
}
