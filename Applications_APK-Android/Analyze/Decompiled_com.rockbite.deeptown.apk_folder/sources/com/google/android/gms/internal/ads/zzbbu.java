package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbbu implements zzno {
    private final String zzcyr;
    private final zzbbs zzeco;

    zzbbu(zzbbs zzbbs, String str) {
        this.zzeco = zzbbs;
        this.zzcyr = str;
    }

    public final zznl zzih() {
        return this.zzeco.zzfh(this.zzcyr);
    }
}
