package com.google.android.gms.common.api.internal;

import android.app.Activity;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-base@@17.1.0 */
public final class zaa extends ActivityLifecycleObserver {
    private final WeakReference<C0135zaa> zaco;

    public zaa(Activity activity) {
        this(C0135zaa.zaa(activity));
    }

    public final ActivityLifecycleObserver onStopCallOnce(Runnable runnable) {
        C0135zaa zaa = this.zaco.get();
        if (zaa != null) {
            zaa.zaa(runnable);
            return this;
        }
        throw new IllegalStateException("The target activity has already been GC'd");
    }

    private zaa(C0135zaa zaa) {
        this.zaco = new WeakReference<>(zaa);
    }

    /* renamed from: com.google.android.gms.common.api.internal.zaa$zaa  reason: collision with other inner class name */
    /* compiled from: com.google.android.gms:play-services-base@@17.1.0 */
    static class C0135zaa extends LifecycleCallback {
        private List<Runnable> zacn = new ArrayList();

        private C0135zaa(LifecycleFragment lifecycleFragment) {
            super(lifecycleFragment);
            this.mLifecycleFragment.addCallback("LifecycleObserverOnStop", this);
        }

        /* access modifiers changed from: private */
        public static C0135zaa zaa(Activity activity) {
            C0135zaa zaa;
            synchronized (activity) {
                LifecycleFragment fragment = LifecycleCallback.getFragment(activity);
                zaa = (C0135zaa) fragment.getCallbackOrNull("LifecycleObserverOnStop", C0135zaa.class);
                if (zaa == null) {
                    zaa = new C0135zaa(fragment);
                }
            }
            return zaa;
        }

        public void onStop() {
            List<Runnable> list;
            synchronized (this) {
                list = this.zacn;
                this.zacn = new ArrayList();
            }
            for (Runnable run : list) {
                run.run();
            }
        }

        /* access modifiers changed from: private */
        public final synchronized void zaa(Runnable runnable) {
            this.zacn.add(runnable);
        }
    }
}
