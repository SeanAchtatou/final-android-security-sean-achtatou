package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzbad implements Runnable {
    private final /* synthetic */ zzazx zzdxn;
    private final /* synthetic */ int zzdxq;
    private final /* synthetic */ int zzdxr;

    zzbad(zzazx zzazx, int i2, int i3) {
        this.zzdxn = zzazx;
        this.zzdxq = i2;
        this.zzdxr = i3;
    }

    public final void run() {
        if (this.zzdxn.zzdxm != null) {
            this.zzdxn.zzdxm.zzk(this.zzdxq, this.zzdxr);
        }
    }
}
