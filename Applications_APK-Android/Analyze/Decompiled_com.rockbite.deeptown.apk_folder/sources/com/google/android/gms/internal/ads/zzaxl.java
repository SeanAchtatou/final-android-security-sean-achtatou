package com.google.android.gms.internal.ads;

import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzaxl {
    /* access modifiers changed from: private */
    public final List<String> zzdud = new ArrayList();
    /* access modifiers changed from: private */
    public final List<Double> zzdue = new ArrayList();
    /* access modifiers changed from: private */
    public final List<Double> zzduf = new ArrayList();

    public final zzaxl zza(String str, double d2, double d3) {
        int i2 = 0;
        while (i2 < this.zzdud.size()) {
            double doubleValue = this.zzduf.get(i2).doubleValue();
            double doubleValue2 = this.zzdue.get(i2).doubleValue();
            if (d2 < doubleValue || (doubleValue == d2 && d3 < doubleValue2)) {
                break;
            }
            i2++;
        }
        this.zzdud.add(i2, str);
        this.zzduf.add(i2, Double.valueOf(d2));
        this.zzdue.add(i2, Double.valueOf(d3));
        return this;
    }

    public final zzaxg zzxa() {
        return new zzaxg(this);
    }
}
