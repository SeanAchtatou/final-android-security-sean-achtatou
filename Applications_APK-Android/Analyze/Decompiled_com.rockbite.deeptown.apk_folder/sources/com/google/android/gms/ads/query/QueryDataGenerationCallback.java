package com.google.android.gms.ads.query;

import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface QueryDataGenerationCallback {
    @KeepForSdk
    void onFailure(String str);

    @KeepForSdk
    void onSuccess(QueryData queryData);
}
