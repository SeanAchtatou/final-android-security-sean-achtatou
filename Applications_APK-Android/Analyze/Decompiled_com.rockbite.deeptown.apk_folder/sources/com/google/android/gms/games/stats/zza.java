package com.google.android.gms.games.stats;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.esotericsoftware.spine.Animation;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

/* compiled from: com.google.android.gms:play-services-games@@19.0.0 */
public final class zza implements Parcelable.Creator<PlayerStatsEntity> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int validateObjectHeader = SafeParcelReader.validateObjectHeader(parcel);
        Bundle bundle = null;
        float f2 = Animation.CurveTimeline.LINEAR;
        float f3 = Animation.CurveTimeline.LINEAR;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        float f4 = Animation.CurveTimeline.LINEAR;
        float f5 = Animation.CurveTimeline.LINEAR;
        float f6 = Animation.CurveTimeline.LINEAR;
        float f7 = Animation.CurveTimeline.LINEAR;
        float f8 = Animation.CurveTimeline.LINEAR;
        while (parcel.dataPosition() < validateObjectHeader) {
            int readHeader = SafeParcelReader.readHeader(parcel);
            switch (SafeParcelReader.getFieldId(readHeader)) {
                case 1:
                    f2 = SafeParcelReader.readFloat(parcel2, readHeader);
                    break;
                case 2:
                    f3 = SafeParcelReader.readFloat(parcel2, readHeader);
                    break;
                case 3:
                    i2 = SafeParcelReader.readInt(parcel2, readHeader);
                    break;
                case 4:
                    i3 = SafeParcelReader.readInt(parcel2, readHeader);
                    break;
                case 5:
                    i4 = SafeParcelReader.readInt(parcel2, readHeader);
                    break;
                case 6:
                    f4 = SafeParcelReader.readFloat(parcel2, readHeader);
                    break;
                case 7:
                    f5 = SafeParcelReader.readFloat(parcel2, readHeader);
                    break;
                case 8:
                    bundle = SafeParcelReader.createBundle(parcel2, readHeader);
                    break;
                case 9:
                    f6 = SafeParcelReader.readFloat(parcel2, readHeader);
                    break;
                case 10:
                    f7 = SafeParcelReader.readFloat(parcel2, readHeader);
                    break;
                case 11:
                    f8 = SafeParcelReader.readFloat(parcel2, readHeader);
                    break;
                default:
                    SafeParcelReader.skipUnknownField(parcel2, readHeader);
                    break;
            }
        }
        SafeParcelReader.ensureAtEnd(parcel2, validateObjectHeader);
        return new PlayerStatsEntity(f2, f3, i2, i3, i4, f4, f5, bundle, f6, f7, f8);
    }

    public final /* synthetic */ Object[] newArray(int i2) {
        return new PlayerStatsEntity[i2];
    }
}
