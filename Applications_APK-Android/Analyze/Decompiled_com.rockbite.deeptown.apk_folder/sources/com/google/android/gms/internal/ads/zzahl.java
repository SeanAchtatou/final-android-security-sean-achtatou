package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzahl {
    private int zzbjx = 2;
    private String zzcyj = "";

    public zzahl(int i2) {
        this.zzbjx = i2;
    }

    public final int getMediaAspectRatio() {
        return this.zzbjx;
    }

    public final String zzrw() {
        return this.zzcyj;
    }

    public zzahl(String str) {
        this.zzcyj = str;
    }
}
