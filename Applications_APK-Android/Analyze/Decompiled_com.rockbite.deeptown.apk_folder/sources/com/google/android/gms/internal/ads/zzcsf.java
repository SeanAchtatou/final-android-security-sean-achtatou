package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcsf implements zzcub<zzcsc> {
    private final zzdhd zzfov;

    public zzcsf(zzdhd zzdhd) {
        this.zzfov = zzdhd;
    }

    public final zzdhe<zzcsc> zzanc() {
        return this.zzfov.zzd(zzcse.zzgfx);
    }
}
