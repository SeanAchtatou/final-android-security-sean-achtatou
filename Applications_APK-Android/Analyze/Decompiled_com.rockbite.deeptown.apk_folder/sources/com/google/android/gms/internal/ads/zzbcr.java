package com.google.android.gms.internal.ads;

import com.tapjoy.TapjoyConstants;
import java.util.HashMap;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzbcr implements Runnable {
    private final /* synthetic */ String zzdug;
    private final /* synthetic */ String zzedb;
    private final /* synthetic */ int zzedd;
    private final /* synthetic */ zzbcn zzedf;

    zzbcr(zzbcn zzbcn, String str, String str2, int i2) {
        this.zzedf = zzbcn;
        this.zzdug = str;
        this.zzedb = str2;
        this.zzedd = i2;
    }

    public final void run() {
        HashMap hashMap = new HashMap();
        hashMap.put(TapjoyConstants.TJC_SDK_TYPE_DEFAULT, "precacheComplete");
        hashMap.put("src", this.zzdug);
        hashMap.put("cachedSrc", this.zzedb);
        hashMap.put("totalBytes", Integer.toString(this.zzedd));
        this.zzedf.zza("onPrecacheEvent", hashMap);
    }
}
