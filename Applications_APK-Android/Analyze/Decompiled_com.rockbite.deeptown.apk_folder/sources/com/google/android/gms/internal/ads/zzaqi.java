package com.google.android.gms.internal.ads;

import java.io.OutputStream;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzaqi implements Runnable {
    private final OutputStream zzdlp;
    private final byte[] zzdlq;

    zzaqi(OutputStream outputStream, byte[] bArr) {
        this.zzdlp = outputStream;
        this.zzdlq = bArr;
    }

    public final void run() {
        zzaqj.zza(this.zzdlp, this.zzdlq);
    }
}
