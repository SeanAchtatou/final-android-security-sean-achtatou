package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzchq implements zzdxg<zzbsu<zzbow>> {
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzchx> zzfdd;

    private zzchq(zzdxp<zzchx> zzdxp, zzdxp<Executor> zzdxp2) {
        this.zzfdd = zzdxp;
        this.zzfcv = zzdxp2;
    }

    public static zzchq zzah(zzdxp<zzchx> zzdxp, zzdxp<Executor> zzdxp2) {
        return new zzchq(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(new zzbsu(this.zzfdd.get(), this.zzfcv.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
