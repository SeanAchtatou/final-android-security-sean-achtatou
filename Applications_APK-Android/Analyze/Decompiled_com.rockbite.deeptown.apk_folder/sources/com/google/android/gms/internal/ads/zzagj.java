package com.google.android.gms.internal.ads;

import android.os.ParcelFileDescriptor;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzagj extends zzagg {
    private final /* synthetic */ zzazl zzbrs;

    zzagj(zzagk zzagk, zzazl zzazl) {
        this.zzbrs = zzazl;
    }

    public final void zza(ParcelFileDescriptor parcelFileDescriptor) throws RemoteException {
        this.zzbrs.set(parcelFileDescriptor);
    }
}
