package com.google.android.gms.gass;

import android.content.Context;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.HandlerThread;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.BaseGmsClient;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.gass.internal.zzd;
import com.google.android.gms.gass.internal.zzm;
import com.google.android.gms.gass.internal.zzo;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

@VisibleForTesting
/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
final class zzg implements BaseGmsClient.BaseConnectionCallbacks, BaseGmsClient.BaseOnConnectionFailedListener {
    private final long startTime;
    private final HandlerThread zzdup;
    @VisibleForTesting
    private zzd zzgsb;
    private final LinkedBlockingQueue<zzo> zzgsd;
    private final String zzgsf;
    private final String zzgsg;
    private final int zzgsh = 1;
    private final AdShield2Logger zzus;

    public zzg(Context context, int i2, String str, String str2, String str3, AdShield2Logger adShield2Logger) {
        this.zzgsf = str;
        this.zzgsg = str2;
        this.zzus = adShield2Logger;
        this.zzdup = new HandlerThread("GassDGClient");
        this.zzdup.start();
        this.startTime = System.currentTimeMillis();
        this.zzgsb = new zzd(context, this.zzdup.getLooper(), this, this);
        this.zzgsd = new LinkedBlockingQueue<>();
        this.zzgsb.checkAvailabilityAndConnect();
    }

    private final void zza(int i2, long j2, Exception exc) {
        AdShield2Logger adShield2Logger = this.zzus;
        if (adShield2Logger != null) {
            adShield2Logger.logException(i2, System.currentTimeMillis() - j2, exc);
        }
    }

    private final void zzalu() {
        zzd zzd = this.zzgsb;
        if (zzd == null) {
            return;
        }
        if (zzd.isConnected() || this.zzgsb.isConnecting()) {
            this.zzgsb.disconnect();
        }
    }

    private final com.google.android.gms.gass.internal.zzg zzaqh() {
        try {
            return this.zzgsb.zzaqp();
        } catch (DeadObjectException | IllegalStateException unused) {
            return null;
        }
    }

    @VisibleForTesting
    private static zzo zzaqj() {
        return new zzo(null);
    }

    public final void onConnected(Bundle bundle) {
        com.google.android.gms.gass.internal.zzg zzaqh = zzaqh();
        if (zzaqh != null) {
            try {
                this.zzgsd.put(zzaqh.zza(new zzm(this.zzgsh, this.zzgsf, this.zzgsg)));
            } catch (Throwable th) {
                zza(AdShield2Logger.EVENTID_GASSDGCLIENT_CONNECTED_EXCEPTION, this.startTime, new Exception(th));
            } finally {
                zzalu();
                this.zzdup.quit();
            }
        }
    }

    public final void onConnectionFailed(ConnectionResult connectionResult) {
        try {
            this.zzgsd.put(zzaqj());
        } catch (InterruptedException unused) {
        }
    }

    public final void onConnectionSuspended(int i2) {
        try {
            this.zzgsd.put(zzaqj());
        } catch (InterruptedException unused) {
        }
    }

    public final zzo zzdo(int i2) {
        zzo zzo;
        try {
            zzo = this.zzgsd.poll(50000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e2) {
            zza(2009, this.startTime, e2);
            zzo = null;
        }
        zza(3004, this.startTime, null);
        return zzo == null ? zzaqj() : zzo;
    }
}
