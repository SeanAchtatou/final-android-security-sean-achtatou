package com.google.android.gms.ads.formats;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.ads.zzacm;
import com.google.android.gms.internal.ads.zzayu;
import com.google.android.gms.internal.ads.zzve;
import com.google.android.gms.internal.ads.zzzn;

@Deprecated
/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public class NativeAdView extends FrameLayout {
    private final FrameLayout zzbkf;
    private final zzacm zzbkg = zzjl();

    public NativeAdView(Context context) {
        super(context);
        this.zzbkf = zzd(context);
    }

    private final FrameLayout zzd(Context context) {
        FrameLayout frameLayout = new FrameLayout(context);
        frameLayout.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        addView(frameLayout);
        return frameLayout;
    }

    private final zzacm zzjl() {
        Preconditions.checkNotNull(this.zzbkf, "createDelegate must be called after mOverlayFrame has been created");
        if (isInEditMode()) {
            return null;
        }
        return zzve.zzov().zza(this.zzbkf.getContext(), this, this.zzbkf);
    }

    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        super.addView(view, i2, layoutParams);
        super.bringChildToFront(this.zzbkf);
    }

    public void bringChildToFront(View view) {
        super.bringChildToFront(view);
        FrameLayout frameLayout = this.zzbkf;
        if (frameLayout != view) {
            super.bringChildToFront(frameLayout);
        }
    }

    public void destroy() {
        try {
            this.zzbkg.destroy();
        } catch (RemoteException e2) {
            zzayu.zzc("Unable to destroy native ad view", e2);
        }
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        zzacm zzacm;
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzclq)).booleanValue() && (zzacm = this.zzbkg) != null) {
            try {
                zzacm.zzf(ObjectWrapper.wrap(motionEvent));
            } catch (RemoteException e2) {
                zzayu.zzc("Unable to call handleTouchEvent on delegate", e2);
            }
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public AdChoicesView getAdChoicesView() {
        View zzbp = zzbp(NativeAd.ASSET_ADCHOICES_CONTAINER_VIEW);
        if (zzbp instanceof AdChoicesView) {
            return (AdChoicesView) zzbp;
        }
        return null;
    }

    public void onVisibilityChanged(View view, int i2) {
        super.onVisibilityChanged(view, i2);
        zzacm zzacm = this.zzbkg;
        if (zzacm != null) {
            try {
                zzacm.zzc(ObjectWrapper.wrap(view), i2);
            } catch (RemoteException e2) {
                zzayu.zzc("Unable to call onVisibilityChanged on delegate", e2);
            }
        }
    }

    public void removeAllViews() {
        super.removeAllViews();
        super.addView(this.zzbkf);
    }

    public void removeView(View view) {
        if (this.zzbkf != view) {
            super.removeView(view);
        }
    }

    public void setAdChoicesView(AdChoicesView adChoicesView) {
        zza(NativeAd.ASSET_ADCHOICES_CONTAINER_VIEW, adChoicesView);
    }

    public void setNativeAd(NativeAd nativeAd) {
        try {
            this.zzbkg.zza((IObjectWrapper) nativeAd.zzjj());
        } catch (RemoteException e2) {
            zzayu.zzc("Unable to call setNativeAd on delegate", e2);
        }
    }

    /* access modifiers changed from: protected */
    public final void zza(String str, View view) {
        try {
            this.zzbkg.zzb(str, ObjectWrapper.wrap(view));
        } catch (RemoteException e2) {
            zzayu.zzc("Unable to call setAssetView on delegate", e2);
        }
    }

    /* access modifiers changed from: protected */
    public final View zzbp(String str) {
        try {
            IObjectWrapper zzco = this.zzbkg.zzco(str);
            if (zzco != null) {
                return (View) ObjectWrapper.unwrap(zzco);
            }
            return null;
        } catch (RemoteException e2) {
            zzayu.zzc("Unable to call getAssetView on delegate", e2);
            return null;
        }
    }

    public NativeAdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.zzbkf = zzd(context);
    }

    public NativeAdView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.zzbkf = zzd(context);
    }

    @TargetApi(21)
    public NativeAdView(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2, i3);
        this.zzbkf = zzd(context);
    }
}
