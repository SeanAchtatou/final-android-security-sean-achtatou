package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcrb implements zzded {
    private final zzcqz zzgfi;

    zzcrb(zzcqz zzcqz) {
        this.zzgfi = zzcqz;
    }

    public final Object apply(Object obj) {
        return new zzcqw(false, false, ((Boolean) obj).booleanValue());
    }
}
