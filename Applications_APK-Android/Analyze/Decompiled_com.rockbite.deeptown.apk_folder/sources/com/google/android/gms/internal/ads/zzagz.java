package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

@SafeParcelable.Class(creator = "InstreamAdConfigurationParcelCreator")
/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzagz extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzagz> CREATOR = new zzahc();
    @SafeParcelable.VersionField(id = 1000)
    public final int versionCode;
    @SafeParcelable.Field(id = 3)
    public final int zzbjx;
    @SafeParcelable.Field(id = 1)
    public final int zzcyi;
    @SafeParcelable.Field(id = 2)
    public final String zzcyj;

    public zzagz(zzahl zzahl) {
        this(2, 1, zzahl.zzrw(), zzahl.getMediaAspectRatio());
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zzcyi);
        SafeParcelWriter.writeString(parcel, 2, this.zzcyj, false);
        SafeParcelWriter.writeInt(parcel, 3, this.zzbjx);
        SafeParcelWriter.writeInt(parcel, 1000, this.versionCode);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }

    @SafeParcelable.Constructor
    public zzagz(@SafeParcelable.Param(id = 1000) int i2, @SafeParcelable.Param(id = 1) int i3, @SafeParcelable.Param(id = 2) String str, @SafeParcelable.Param(id = 3) int i4) {
        this.versionCode = i2;
        this.zzcyi = i3;
        this.zzcyj = str;
        this.zzbjx = i4;
    }
}
