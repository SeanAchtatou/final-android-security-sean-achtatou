package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbbv implements zzno {
    private final byte[] zzdvy;

    zzbbv(byte[] bArr) {
        this.zzdvy = bArr;
    }

    public final zznl zzih() {
        return new zznm(this.zzdvy);
    }
}
