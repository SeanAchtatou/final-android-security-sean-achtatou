package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzajl implements zzazn {
    private final /* synthetic */ zzajf zzdag;

    zzajl(zzajj zzajj, zzajf zzajf) {
        this.zzdag = zzajf;
    }

    public final void run() {
        zzavs.zzed("Rejecting reference for JS Engine.");
        this.zzdag.reject();
    }
}
