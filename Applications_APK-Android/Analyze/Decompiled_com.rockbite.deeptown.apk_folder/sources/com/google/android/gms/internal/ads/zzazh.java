package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzazh {
    public static void zza(zzdhe<?> zzdhe, String str) {
        zzdgs.zza(zzdhe, new zzazg(str), zzazd.zzdwj);
    }

    public static void zzb(zzdhe<?> zzdhe, String str) {
        zzdgs.zza(zzdhe, new zzazj(str), zzazd.zzdwj);
    }
}
