package com.google.android.gms.internal.ads;

import android.content.DialogInterface;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzaob implements DialogInterface.OnClickListener {
    private final /* synthetic */ zzanz zzdff;

    zzaob(zzanz zzanz) {
        this.zzdff = zzanz;
    }

    public final void onClick(DialogInterface dialogInterface, int i2) {
        this.zzdff.zzds("Operation denied by user.");
    }
}
