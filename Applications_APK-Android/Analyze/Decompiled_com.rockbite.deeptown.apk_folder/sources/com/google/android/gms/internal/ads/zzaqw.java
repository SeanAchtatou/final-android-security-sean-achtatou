package com.google.android.gms.internal.ads;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;
import com.google.firebase.perf.FirebasePerformance;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzaqw extends zzaqy {
    private final Object lock = new Object();
    private SharedPreferences zzdnj;
    private final zzaju<JSONObject, JSONObject> zzdnk;
    private final Context zzyv;

    public zzaqw(Context context, zzaju<JSONObject, JSONObject> zzaju) {
        this.zzyv = context.getApplicationContext();
        this.zzdnk = zzaju;
    }

    public static JSONObject zzy(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("js", zzazb.zzxm().zzbma);
            jSONObject.put("mf", zzaaz.zzcti.get());
            jSONObject.put("cl", "278033407");
            jSONObject.put("rapid_rc", "dev");
            jSONObject.put("rapid_rollup", FirebasePerformance.HttpMethod.HEAD);
            jSONObject.put("admob_module_version", 20360);
            jSONObject.put("dynamite_local_version", (int) ModuleDescriptor.MODULE_VERSION);
            jSONObject.put("dynamite_version", DynamiteModule.getRemoteVersion(context, ModuleDescriptor.MODULE_ID));
            jSONObject.put("container_version", 12451009);
        } catch (JSONException unused) {
        }
        return jSONObject;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Void zzf(JSONObject jSONObject) {
        zzzn.zza(this.zzyv, 1, jSONObject);
        this.zzdnj.edit().putLong("js_last_update", zzq.zzkx().currentTimeMillis()).apply();
        return null;
    }

    public final zzdhe<Void> zzuh() {
        synchronized (this.lock) {
            if (this.zzdnj == null) {
                this.zzdnj = this.zzyv.getSharedPreferences("google_ads_flags_meta", 0);
            }
        }
        if (zzq.zzkx().currentTimeMillis() - this.zzdnj.getLong("js_last_update", 0) < zzaaz.zzctj.get().longValue()) {
            return zzdgs.zzaj(null);
        }
        return zzdgs.zzb(this.zzdnk.zzi(zzy(this.zzyv)), new zzaqz(this), zzazd.zzdwj);
    }
}
