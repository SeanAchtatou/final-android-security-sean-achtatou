package com.google.android.gms.internal.ads;

import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbqy implements zzdxg<zzbqw> {
    private final zzdxp<Set<zzbsu<zzbrb>>> zzfeo;

    private zzbqy(zzdxp<Set<zzbsu<zzbrb>>> zzdxp) {
        this.zzfeo = zzdxp;
    }

    public static zzbqy zzo(zzdxp<Set<zzbsu<zzbrb>>> zzdxp) {
        return new zzbqy(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzbqw(this.zzfeo.get());
    }
}
