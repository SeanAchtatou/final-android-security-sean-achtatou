package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import com.facebook.internal.NativeProtocol;
import com.google.android.gms.ads.internal.zzq;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzaex implements zzafn<zzbdi> {
    public final /* synthetic */ void zza(Object obj, Map map) {
        zzbdi zzbdi = (zzbdi) obj;
        String str = (String) map.get(NativeProtocol.WEB_DIALOG_ACTION);
        if ("tick".equals(str)) {
            String str2 = (String) map.get("label");
            String str3 = (String) map.get("start_label");
            String str4 = (String) map.get("timestamp");
            if (TextUtils.isEmpty(str2)) {
                zzayu.zzez("No label given for CSI tick.");
            } else if (TextUtils.isEmpty(str4)) {
                zzayu.zzez("No timestamp given for CSI tick.");
            } else {
                try {
                    long elapsedRealtime = zzq.zzkx().elapsedRealtime() + (Long.parseLong(str4) - zzq.zzkx().currentTimeMillis());
                    if (TextUtils.isEmpty(str3)) {
                        str3 = "native:view_load";
                    }
                    zzbdi.zzyq().zza(str2, str3, elapsedRealtime);
                } catch (NumberFormatException e2) {
                    zzayu.zzd("Malformed timestamp for CSI tick.", e2);
                }
            }
        } else if ("experiment".equals(str)) {
            String str5 = (String) map.get("value");
            if (TextUtils.isEmpty(str5)) {
                zzayu.zzez("No value given for CSI experiment.");
                return;
            }
            zzaae zzqp = zzbdi.zzyq().zzqp();
            if (zzqp == null) {
                zzayu.zzez("No ticker for WebView, dropping experiment ID.");
            } else {
                zzqp.zzh("e", str5);
            }
        } else if ("extra".equals(str)) {
            String str6 = (String) map.get("name");
            String str7 = (String) map.get("value");
            if (TextUtils.isEmpty(str7)) {
                zzayu.zzez("No value given for CSI extra.");
            } else if (TextUtils.isEmpty(str6)) {
                zzayu.zzez("No name given for CSI extra.");
            } else {
                zzaae zzqp2 = zzbdi.zzyq().zzqp();
                if (zzqp2 == null) {
                    zzayu.zzez("No ticker for WebView, dropping extra parameter.");
                } else {
                    zzqp2.zzh(str6, str7);
                }
            }
        }
    }
}
