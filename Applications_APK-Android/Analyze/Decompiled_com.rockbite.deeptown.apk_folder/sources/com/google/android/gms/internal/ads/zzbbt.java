package com.google.android.gms.internal.ads;

import com.google.android.gms.common.util.VisibleForTesting;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbbt implements zzha {
    private int zzbep;
    private final zznp zzeci;
    private long zzecj;
    private long zzeck;
    private long zzecl;
    private long zzecm;
    private boolean zzecn;

    zzbbt() {
        this(15000, 30000, 2500, 5000);
    }

    @VisibleForTesting
    private final void zzk(boolean z) {
        this.zzbep = 0;
        this.zzecn = false;
        if (z) {
            this.zzeci.reset();
        }
    }

    public final void onStopped() {
        zzk(true);
    }

    public final void zza(zzhf[] zzhfArr, zzmr zzmr, zzng zzng) {
        this.zzbep = 0;
        for (int i2 = 0; i2 < zzhfArr.length; i2++) {
            if (zzng.zzay(i2) != null) {
                this.zzbep += zzoq.zzbl(zzhfArr[i2].getTrackType());
            }
        }
        this.zzeci.zzba(this.zzbep);
    }

    public final synchronized boolean zzc(long j2, boolean z) {
        boolean z2;
        long j3 = z ? this.zzecm : this.zzecl;
        if (j3 <= 0 || j2 >= j3) {
            z2 = true;
        } else {
            z2 = false;
        }
        return z2;
    }

    public final synchronized void zzcx(int i2) {
        this.zzecl = ((long) i2) * 1000;
    }

    public final synchronized void zzcy(int i2) {
        this.zzecm = ((long) i2) * 1000;
    }

    public final synchronized void zzdc(int i2) {
        this.zzecj = ((long) i2) * 1000;
    }

    public final synchronized void zzdd(int i2) {
        this.zzeck = ((long) i2) * 1000;
    }

    public final synchronized boolean zzdt(long j2) {
        char c2;
        boolean z = false;
        if (j2 > this.zzeck) {
            c2 = 0;
        } else {
            c2 = j2 < this.zzecj ? (char) 2 : 1;
        }
        boolean z2 = this.zzeci.zzii() >= this.zzbep;
        if (c2 == 2 || (c2 == 1 && this.zzecn && !z2)) {
            z = true;
        }
        this.zzecn = z;
        return this.zzecn;
    }

    public final void zzer() {
        zzk(false);
    }

    public final void zzes() {
        zzk(true);
    }

    public final zznj zzet() {
        return this.zzeci;
    }

    private zzbbt(int i2, int i3, long j2, long j3) {
        this.zzeci = new zznp(true, 65536);
        this.zzecj = 15000000;
        this.zzeck = 30000000;
        this.zzecl = 2500000;
        this.zzecm = 5000000;
    }
}
