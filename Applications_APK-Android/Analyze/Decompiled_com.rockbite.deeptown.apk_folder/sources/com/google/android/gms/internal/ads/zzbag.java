package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.content.Context;
import android.view.TextureView;

@TargetApi(14)
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public abstract class zzbag extends TextureView implements zzbbd {
    protected final zzbaq zzdxs = new zzbaq();
    protected final zzbba zzdxt;

    public zzbag(Context context) {
        super(context);
        this.zzdxt = new zzbba(context, this);
    }

    public abstract int getCurrentPosition();

    public abstract int getDuration();

    public abstract int getVideoHeight();

    public abstract int getVideoWidth();

    public abstract void pause();

    public abstract void play();

    public abstract void seekTo(int i2);

    public abstract void setVideoPath(String str);

    public abstract void stop();

    public abstract void zza(float f2, float f3);

    public abstract void zza(zzbah zzbah);

    public void zzb(String str, String[] strArr) {
        setVideoPath(str);
    }

    public void zzcv(int i2) {
    }

    public void zzcw(int i2) {
    }

    public void zzcx(int i2) {
    }

    public void zzcy(int i2) {
    }

    public void zzcz(int i2) {
    }

    public abstract String zzxo();

    public abstract void zzxs();
}
