package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.rewarded.RewardedAdCallback;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzasx extends zzask {
    private final RewardedAdCallback zzdoa;

    public zzasx(RewardedAdCallback rewardedAdCallback) {
        this.zzdoa = rewardedAdCallback;
    }

    public final void onRewardedAdClosed() {
        RewardedAdCallback rewardedAdCallback = this.zzdoa;
        if (rewardedAdCallback != null) {
            rewardedAdCallback.onRewardedAdClosed();
        }
    }

    public final void onRewardedAdFailedToShow(int i2) {
        RewardedAdCallback rewardedAdCallback = this.zzdoa;
        if (rewardedAdCallback != null) {
            rewardedAdCallback.onRewardedAdFailedToShow(i2);
        }
    }

    public final void onRewardedAdOpened() {
        RewardedAdCallback rewardedAdCallback = this.zzdoa;
        if (rewardedAdCallback != null) {
            rewardedAdCallback.onRewardedAdOpened();
        }
    }

    public final void zza(zzasf zzasf) {
        RewardedAdCallback rewardedAdCallback = this.zzdoa;
        if (rewardedAdCallback != null) {
            rewardedAdCallback.onUserEarnedReward(new zzasu(zzasf));
        }
    }
}
