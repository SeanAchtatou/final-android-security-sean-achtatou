package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.ads.AdRequest;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationBannerListener;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationInterstitialListener;
import com.google.ads.mediation.MediationServerParameters;
import com.google.ads.mediation.NetworkExtras;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzamf<NETWORK_EXTRAS extends NetworkExtras, SERVER_PARAMETERS extends MediationServerParameters> implements MediationBannerListener, MediationInterstitialListener {
    /* access modifiers changed from: private */
    public final zzali zzdds;

    public zzamf(zzali zzali) {
        this.zzdds = zzali;
    }

    public final void onClick(MediationBannerAdapter<?, ?> mediationBannerAdapter) {
        zzayu.zzea("Adapter called onClick.");
        zzve.zzou();
        if (!zzayk.zzxe()) {
            zzayu.zze("#008 Must be called on the main UI thread.", null);
            zzayk.zzyu.post(new zzami(this));
            return;
        }
        try {
            this.zzdds.onAdClicked();
        } catch (RemoteException e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
        }
    }

    public final void onDismissScreen(MediationBannerAdapter<?, ?> mediationBannerAdapter) {
        zzayu.zzea("Adapter called onDismissScreen.");
        zzve.zzou();
        if (!zzayk.zzxe()) {
            zzayu.zzez("#008 Must be called on the main UI thread.");
            zzayk.zzyu.post(new zzamj(this));
            return;
        }
        try {
            this.zzdds.onAdClosed();
        } catch (RemoteException e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
        }
    }

    public final void onFailedToReceiveAd(MediationBannerAdapter<?, ?> mediationBannerAdapter, AdRequest.ErrorCode errorCode) {
        String valueOf = String.valueOf(errorCode);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 47);
        sb.append("Adapter called onFailedToReceiveAd with error. ");
        sb.append(valueOf);
        zzayu.zzea(sb.toString());
        zzve.zzou();
        if (!zzayk.zzxe()) {
            zzayu.zze("#008 Must be called on the main UI thread.", null);
            zzayk.zzyu.post(new zzamm(this, errorCode));
            return;
        }
        try {
            this.zzdds.onAdFailedToLoad(zzamr.zza(errorCode));
        } catch (RemoteException e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
        }
    }

    public final void onLeaveApplication(MediationBannerAdapter<?, ?> mediationBannerAdapter) {
        zzayu.zzea("Adapter called onLeaveApplication.");
        zzve.zzou();
        if (!zzayk.zzxe()) {
            zzayu.zze("#008 Must be called on the main UI thread.", null);
            zzayk.zzyu.post(new zzaml(this));
            return;
        }
        try {
            this.zzdds.onAdLeftApplication();
        } catch (RemoteException e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
        }
    }

    public final void onPresentScreen(MediationBannerAdapter<?, ?> mediationBannerAdapter) {
        zzayu.zzea("Adapter called onPresentScreen.");
        zzve.zzou();
        if (!zzayk.zzxe()) {
            zzayu.zze("#008 Must be called on the main UI thread.", null);
            zzayk.zzyu.post(new zzamo(this));
            return;
        }
        try {
            this.zzdds.onAdOpened();
        } catch (RemoteException e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
        }
    }

    public final void onReceivedAd(MediationBannerAdapter<?, ?> mediationBannerAdapter) {
        zzayu.zzea("Adapter called onReceivedAd.");
        zzve.zzou();
        if (!zzayk.zzxe()) {
            zzayu.zze("#008 Must be called on the main UI thread.", null);
            zzayk.zzyu.post(new zzamn(this));
            return;
        }
        try {
            this.zzdds.onAdLoaded();
        } catch (RemoteException e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
        }
    }

    public final void onDismissScreen(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter) {
        zzayu.zzea("Adapter called onDismissScreen.");
        zzve.zzou();
        if (!zzayk.zzxe()) {
            zzayu.zze("#008 Must be called on the main UI thread.", null);
            zzayk.zzyu.post(new zzamq(this));
            return;
        }
        try {
            this.zzdds.onAdClosed();
        } catch (RemoteException e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
        }
    }

    public final void onFailedToReceiveAd(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter, AdRequest.ErrorCode errorCode) {
        String valueOf = String.valueOf(errorCode);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 47);
        sb.append("Adapter called onFailedToReceiveAd with error ");
        sb.append(valueOf);
        sb.append(".");
        zzayu.zzea(sb.toString());
        zzve.zzou();
        if (!zzayk.zzxe()) {
            zzayu.zze("#008 Must be called on the main UI thread.", null);
            zzayk.zzyu.post(new zzamp(this, errorCode));
            return;
        }
        try {
            this.zzdds.onAdFailedToLoad(zzamr.zza(errorCode));
        } catch (RemoteException e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
        }
    }

    public final void onLeaveApplication(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter) {
        zzayu.zzea("Adapter called onLeaveApplication.");
        zzve.zzou();
        if (!zzayk.zzxe()) {
            zzayu.zze("#008 Must be called on the main UI thread.", null);
            zzayk.zzyu.post(new zzams(this));
            return;
        }
        try {
            this.zzdds.onAdLeftApplication();
        } catch (RemoteException e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
        }
    }

    public final void onPresentScreen(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter) {
        zzayu.zzea("Adapter called onPresentScreen.");
        zzve.zzou();
        if (!zzayk.zzxe()) {
            zzayu.zze("#008 Must be called on the main UI thread.", null);
            zzayk.zzyu.post(new zzamh(this));
            return;
        }
        try {
            this.zzdds.onAdOpened();
        } catch (RemoteException e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
        }
    }

    public final void onReceivedAd(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter) {
        zzayu.zzea("Adapter called onReceivedAd.");
        zzve.zzou();
        if (!zzayk.zzxe()) {
            zzayu.zze("#008 Must be called on the main UI thread.", null);
            zzayk.zzyu.post(new zzamk(this));
            return;
        }
        try {
            this.zzdds.onAdLoaded();
        } catch (RemoteException e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
        }
    }
}
