package com.google.android.gms.internal.ads;

import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzcmb extends zzang {
    private zzcip<zzani, zzcjy> zzfyr;

    private zzcmb(zzcma zzcma, zzcip<zzani, zzcjy> zzcip) {
        this.zzfyr = zzcip;
    }

    public final void zzdl(String str) throws RemoteException {
        ((zzcjy) this.zzfyr.zzfyf).onAdFailedToLoad(0);
    }

    public final void zztb() throws RemoteException {
        ((zzcjy) this.zzfyr.zzfyf).onAdLoaded();
    }
}
