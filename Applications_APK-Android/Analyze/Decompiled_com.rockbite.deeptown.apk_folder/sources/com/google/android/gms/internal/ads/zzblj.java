package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzblj implements zzdxg<String> {
    private static final zzblj zzfer = new zzblj();

    public static zzblj zzagk() {
        return zzfer;
    }

    public final /* synthetic */ Object get() {
        return (String) zzdxm.zza("banner", "Cannot return null from a non-@Nullable @Provides method");
    }
}
