package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbox implements zzbrn {
    private final int zzdvv;

    zzbox(int i2) {
        this.zzdvv = i2;
    }

    public final void zzp(Object obj) {
        ((zzbow) obj).onAdFailedToLoad(this.zzdvv);
    }
}
