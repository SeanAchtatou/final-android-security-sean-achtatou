package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbmb implements zzdxg<zzbmc> {
    private final zzdxp<zzats> zzffn;

    public zzbmb(zzdxp<zzats> zzdxp) {
        this.zzffn = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return new zzbmc(this.zzffn.get());
    }
}
