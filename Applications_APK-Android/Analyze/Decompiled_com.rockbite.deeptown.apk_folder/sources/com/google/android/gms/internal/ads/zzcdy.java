package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcdy implements Runnable {
    private final String zzcyr;
    private final zzcdv zzfsz;

    zzcdy(zzcdv zzcdv, String str) {
        this.zzfsz = zzcdv;
        this.zzcyr = str;
    }

    public final void run() {
        this.zzfsz.zzgc(this.zzcyr);
    }
}
