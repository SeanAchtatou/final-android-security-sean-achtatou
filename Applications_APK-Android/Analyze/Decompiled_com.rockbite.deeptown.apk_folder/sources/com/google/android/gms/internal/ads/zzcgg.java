package com.google.android.gms.internal.ads;

import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcgg implements zzdgf {
    private final zzaju zzfvn;

    zzcgg(zzaju zzaju) {
        this.zzfvn = zzaju;
    }

    public final zzdhe zzf(Object obj) {
        return this.zzfvn.zzi((JSONObject) obj);
    }
}
