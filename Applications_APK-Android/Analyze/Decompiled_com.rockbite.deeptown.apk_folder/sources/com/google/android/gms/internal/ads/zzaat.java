package com.google.android.gms.internal.ads;

import com.tapjoy.TapjoyConstants;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzaat {
    private static zzaan<Boolean> zzcsp = zzaas.zzf("gads:consent:gmscore:dsid:enabled", true);
    private static zzaan<Boolean> zzcsq = zzaas.zzf("gads:consent:gmscore:lat:enabled", true);
    private static zzaan<String> zzcsr = new zzaas("gads:consent:gmscore:backend_url", "https://adservice.google.com/getconfig/pubvendors", zzaap.zzcsk);
    private static zzaan<Long> zzcss = new zzaas("gads:consent:gmscore:time_out", Long.valueOf((long) TapjoyConstants.TIMER_INCREMENT), zzaap.zzcsi);
    private static zzaan<Boolean> zzcst = zzaas.zzf("gads:consent:gmscore:enabled", true);

    public static final void zza(zzabq zzabq) {
    }
}
