package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbs;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzck {
    private static boolean zzmq = false;
    /* access modifiers changed from: private */
    public static MessageDigest zzmr;
    private static final Object zzms = new Object();
    private static final Object zzmt = new Object();
    static CountDownLatch zzmu = new CountDownLatch(1);

    private static Vector<byte[]> zza(byte[] bArr, int i2) {
        if (bArr == null || bArr.length <= 0) {
            return null;
        }
        int length = ((bArr.length + 255) - 1) / 255;
        Vector<byte[]> vector = new Vector<>();
        int i3 = 0;
        while (i3 < length) {
            int i4 = i3 * 255;
            try {
                vector.add(Arrays.copyOfRange(bArr, i4, bArr.length - i4 > 255 ? i4 + 255 : bArr.length));
                i3++;
            } catch (IndexOutOfBoundsException unused) {
                return null;
            }
        }
        return vector;
    }

    public static byte[] zzb(byte[] bArr) throws NoSuchAlgorithmException {
        byte[] digest;
        synchronized (zzms) {
            MessageDigest zzbm = zzbm();
            if (zzbm != null) {
                zzbm.reset();
                zzbm.update(bArr);
                digest = zzmr.digest();
            } else {
                throw new NoSuchAlgorithmException("Cannot compute hash");
            }
        }
        return digest;
    }

    static void zzbl() {
        synchronized (zzmt) {
            if (!zzmq) {
                zzmq = true;
                new Thread(new zzcm()).start();
            }
        }
    }

    private static MessageDigest zzbm() {
        boolean z;
        MessageDigest messageDigest;
        zzbl();
        try {
            z = zzmu.await(2, TimeUnit.SECONDS);
        } catch (InterruptedException unused) {
            z = false;
        }
        if (z && (messageDigest = zzmr) != null) {
            return messageDigest;
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzci.zza(byte[], boolean):java.lang.String
     arg types: [byte[], int]
     candidates:
      com.google.android.gms.internal.ads.zzci.zza(java.lang.String, boolean):byte[]
      com.google.android.gms.internal.ads.zzci.zza(byte[], boolean):java.lang.String */
    static String zzj(zzbs.zza zza, String str) throws GeneralSecurityException, UnsupportedEncodingException {
        byte[] bArr;
        byte[] byteArray = zza.toByteArray();
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzclj)).booleanValue()) {
            Vector<byte[]> zza2 = zza(byteArray, 255);
            if (zza2 == null || zza2.size() == 0) {
                bArr = zza(zza(zzbs.zza.zzd.PSN_ENCODE_SIZE_FAIL).toByteArray(), str, true);
            } else {
                zzbs.zzf.zza zzbi = zzbs.zzf.zzbi();
                Iterator<byte[]> it = zza2.iterator();
                while (it.hasNext()) {
                    zzbi.zzi(zzdqk.zzu(zza(it.next(), str, false)));
                }
                zzbi.zzj(zzdqk.zzu(zzb(byteArray)));
                bArr = ((zzbs.zzf) zzbi.zzbaf()).toByteArray();
            }
        } else if (zzeo.zzyh != null) {
            bArr = ((zzbs.zzf) zzbs.zzf.zzbi().zzi(zzdqk.zzu(zzeo.zzyh.zzc(byteArray, str != null ? str.getBytes() : new byte[0]))).zza(zzbz.TINK_HYBRID).zzbaf()).toByteArray();
        } else {
            throw new GeneralSecurityException();
        }
        return zzci.zza(bArr, true);
    }

    private static zzbs.zza zza(zzbs.zza.zzd zzd) {
        zzbs.zza.zzb zzan = zzbs.zza.zzan();
        zzan.zzau((long) zzd.zzae());
        return (zzbs.zza) zzan.zzbaf();
    }

    private static byte[] zza(byte[] bArr, String str, boolean z) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        byte[] bArr2;
        int i2 = z ? 239 : 255;
        if (bArr.length > i2) {
            bArr = zza(zzbs.zza.zzd.PSN_ENCODE_SIZE_FAIL).toByteArray();
        }
        if (bArr.length < i2) {
            byte[] bArr3 = new byte[(i2 - bArr.length)];
            new SecureRandom().nextBytes(bArr3);
            bArr2 = ByteBuffer.allocate(i2 + 1).put((byte) bArr.length).put(bArr).put(bArr3).array();
        } else {
            bArr2 = ByteBuffer.allocate(i2 + 1).put((byte) bArr.length).put(bArr).array();
        }
        if (z) {
            bArr2 = ByteBuffer.allocate(256).put(zzb(bArr2)).put(bArr2).array();
        }
        byte[] bArr4 = new byte[256];
        for (zzcr zza : new zzcp().zzun) {
            zza.zza(bArr2, bArr4);
        }
        if (str != null && str.length() > 0) {
            if (str.length() > 32) {
                str = str.substring(0, 32);
            }
            new zzdpx(str.getBytes("UTF-8")).zzt(bArr4);
        }
        return bArr4;
    }
}
