package com.google.android.gms.internal.ads;

import android.content.DialogInterface;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzawu implements DialogInterface.OnClickListener {
    private final String zzcyr;
    private final zzawt zzdta;

    zzawu(zzawt zzawt, String str) {
        this.zzdta = zzawt;
        this.zzcyr = str;
    }

    public final void onClick(DialogInterface dialogInterface, int i2) {
        this.zzdta.zza(this.zzcyr, dialogInterface, i2);
    }
}
