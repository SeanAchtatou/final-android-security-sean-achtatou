package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdbg {
    public static final int zzgos = 1;
    public static final int zzgot = 2;
    public static final int zzgou = 3;
    private static final /* synthetic */ int[] zzgov = {zzgos, zzgot, zzgou};
    public static final int zzgow = 1;
    private static final /* synthetic */ int[] zzgox = {zzgow};

    public static int[] zzapn() {
        return (int[]) zzgov.clone();
    }

    public static int[] zzapo() {
        return (int[]) zzgox.clone();
    }
}
