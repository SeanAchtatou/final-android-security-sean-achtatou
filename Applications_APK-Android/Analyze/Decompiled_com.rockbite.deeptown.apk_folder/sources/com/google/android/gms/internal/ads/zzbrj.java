package com.google.android.gms.internal.ads;

import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbrj implements zzdxg<zzbrf> {
    private final zzdxp<Set<zzbsu<zzbri>>> zzfeo;

    private zzbrj(zzdxp<Set<zzbsu<zzbri>>> zzdxp) {
        this.zzfeo = zzdxp;
    }

    public static zzbrj zzq(zzdxp<Set<zzbsu<zzbri>>> zzdxp) {
        return new zzbrj(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzbrf(this.zzfeo.get());
    }
}
