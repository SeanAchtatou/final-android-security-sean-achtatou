package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.appevents.AppEventsConstants;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.internal.Preconditions;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TapjoyConstants;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbai extends FrameLayout implements zzbah {
    private final zzbaz zzdxu;
    private final FrameLayout zzdxv;
    private final zzaae zzdxw;
    private final zzbbb zzdxx;
    private final long zzdxy;
    private zzbag zzdxz;
    private boolean zzdya;
    private boolean zzdyb;
    private boolean zzdyc;
    private boolean zzdyd;
    private long zzdye;
    private long zzdyf;
    private String zzdyg;
    private String[] zzdyh;
    private Bitmap zzdyi;
    private ImageView zzdyj;
    private boolean zzdyk;

    public zzbai(Context context, zzbaz zzbaz, int i2, boolean z, zzaae zzaae, zzbaw zzbaw) {
        super(context);
        zzbaz zzbaz2 = zzbaz;
        this.zzdxu = zzbaz2;
        zzaae zzaae2 = zzaae;
        this.zzdxw = zzaae2;
        this.zzdxv = new FrameLayout(context);
        addView(this.zzdxv, new FrameLayout.LayoutParams(-1, -1));
        Preconditions.checkNotNull(zzbaz.zzyo());
        this.zzdxz = zzbaz.zzyo().zzbkp.zza(context, zzbaz2, i2, z, zzaae2, zzbaw);
        zzbag zzbag = this.zzdxz;
        if (zzbag != null) {
            this.zzdxv.addView(zzbag, new FrameLayout.LayoutParams(-1, -1, 17));
            if (((Boolean) zzve.zzoy().zzd(zzzn.zzchc)).booleanValue()) {
                zzya();
            }
        }
        this.zzdyj = new ImageView(context);
        this.zzdxy = ((Long) zzve.zzoy().zzd(zzzn.zzchg)).longValue();
        this.zzdyd = ((Boolean) zzve.zzoy().zzd(zzzn.zzche)).booleanValue();
        zzaae zzaae3 = this.zzdxw;
        if (zzaae3 != null) {
            zzaae3.zzh("spinner_used", this.zzdyd ? "1" : AppEventsConstants.EVENT_PARAM_VALUE_NO);
        }
        this.zzdxx = new zzbbb(this);
        zzbag zzbag2 = this.zzdxz;
        if (zzbag2 != null) {
            zzbag2.zza(this);
        }
        if (this.zzdxz == null) {
            zzm("AdVideoUnderlay Error", "Allocating player failed.");
        }
    }

    public static void zza(zzbaz zzbaz) {
        HashMap hashMap = new HashMap();
        hashMap.put(TapjoyConstants.TJC_SDK_TYPE_DEFAULT, "no_video_view");
        zzbaz.zza("onVideoEvent", hashMap);
    }

    private final boolean zzyc() {
        return this.zzdyj.getParent() != null;
    }

    private final void zzyd() {
        if (this.zzdxu.zzyn() != null && this.zzdyb && !this.zzdyc) {
            this.zzdxu.zzyn().getWindow().clearFlags(128);
            this.zzdyb = false;
        }
    }

    public final void destroy() {
        this.zzdxx.pause();
        zzbag zzbag = this.zzdxz;
        if (zzbag != null) {
            zzbag.stop();
        }
        zzyd();
    }

    public final void finalize() throws Throwable {
        try {
            this.zzdxx.pause();
            if (this.zzdxz != null) {
                zzbag zzbag = this.zzdxz;
                zzdhd zzdhd = zzazd.zzdwi;
                zzbag.getClass();
                zzdhd.execute(zzbal.zza(zzbag));
            }
        } finally {
            super.finalize();
        }
    }

    public final void onPaused() {
        zzd("pause", new String[0]);
        zzyd();
        this.zzdya = false;
    }

    public final void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z) {
            this.zzdxx.resume();
        } else {
            this.zzdxx.pause();
            this.zzdyf = this.zzdye;
        }
        zzawb.zzdsr.post(new zzbak(this, z));
    }

    public final void onWindowVisibilityChanged(int i2) {
        boolean z;
        super.onWindowVisibilityChanged(i2);
        if (i2 == 0) {
            this.zzdxx.resume();
            z = true;
        } else {
            this.zzdxx.pause();
            this.zzdyf = this.zzdye;
            z = false;
        }
        zzawb.zzdsr.post(new zzbap(this, z));
    }

    public final void pause() {
        zzbag zzbag = this.zzdxz;
        if (zzbag != null) {
            zzbag.pause();
        }
    }

    public final void play() {
        zzbag zzbag = this.zzdxz;
        if (zzbag != null) {
            zzbag.play();
        }
    }

    public final void seekTo(int i2) {
        zzbag zzbag = this.zzdxz;
        if (zzbag != null) {
            zzbag.seekTo(i2);
        }
    }

    public final void setVolume(float f2) {
        zzbag zzbag = this.zzdxz;
        if (zzbag != null) {
            zzbag.zzdxt.setVolume(f2);
            zzbag.zzxs();
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzau(boolean z) {
        zzd("windowFocusChanged", "hasWindowFocus", String.valueOf(z));
    }

    public final void zzc(String str, String[] strArr) {
        this.zzdyg = str;
        this.zzdyh = strArr;
    }

    public final void zzcv(int i2) {
        this.zzdxz.zzcv(i2);
    }

    public final void zzcw(int i2) {
        this.zzdxz.zzcw(i2);
    }

    public final void zzcx(int i2) {
        this.zzdxz.zzcx(i2);
    }

    public final void zzcy(int i2) {
        this.zzdxz.zzcy(i2);
    }

    public final void zzcz(int i2) {
        this.zzdxz.zzcz(i2);
    }

    public final void zzd(int i2, int i3, int i4, int i5) {
        if (i4 != 0 && i5 != 0) {
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(i4, i5);
            layoutParams.setMargins(i2, i3, 0, 0);
            this.zzdxv.setLayoutParams(layoutParams);
            requestLayout();
        }
    }

    @TargetApi(14)
    public final void zze(MotionEvent motionEvent) {
        zzbag zzbag = this.zzdxz;
        if (zzbag != null) {
            zzbag.dispatchTouchEvent(motionEvent);
        }
    }

    public final void zzer() {
        zzbag zzbag = this.zzdxz;
        if (zzbag != null && this.zzdyf == 0) {
            zzd("canplaythrough", "duration", String.valueOf(((float) zzbag.getDuration()) / 1000.0f), TJAdUnitConstants.String.VIDEO_WIDTH, String.valueOf(this.zzdxz.getVideoWidth()), TJAdUnitConstants.String.VIDEO_HEIGHT, String.valueOf(this.zzdxz.getVideoHeight()));
        }
    }

    public final void zzhq() {
        if (this.zzdxz != null) {
            if (!TextUtils.isEmpty(this.zzdyg)) {
                this.zzdxz.zzb(this.zzdyg, this.zzdyh);
            } else {
                zzd("no_src", new String[0]);
            }
        }
    }

    public final void zzk(int i2, int i3) {
        if (this.zzdyd) {
            int max = Math.max(i2 / ((Integer) zzve.zzoy().zzd(zzzn.zzchf)).intValue(), 1);
            int max2 = Math.max(i3 / ((Integer) zzve.zzoy().zzd(zzzn.zzchf)).intValue(), 1);
            Bitmap bitmap = this.zzdyi;
            if (bitmap == null || bitmap.getWidth() != max || this.zzdyi.getHeight() != max2) {
                this.zzdyi = Bitmap.createBitmap(max, max2, Bitmap.Config.ARGB_8888);
                this.zzdyk = false;
            }
        }
    }

    public final void zzm(String str, String str2) {
        zzd("error", "what", str, "extra", str2);
    }

    public final void zzxt() {
        this.zzdxx.resume();
        zzawb.zzdsr.post(new zzban(this));
    }

    public final void zzxu() {
        if (this.zzdxu.zzyn() != null && !this.zzdyb) {
            this.zzdyc = (this.zzdxu.zzyn().getWindow().getAttributes().flags & 128) != 0;
            if (!this.zzdyc) {
                this.zzdxu.zzyn().getWindow().addFlags(128);
                this.zzdyb = true;
            }
        }
        this.zzdya = true;
    }

    public final void zzxv() {
        zzd("ended", new String[0]);
        zzyd();
    }

    public final void zzxw() {
        if (this.zzdyk && this.zzdyi != null && !zzyc()) {
            this.zzdyj.setImageBitmap(this.zzdyi);
            this.zzdyj.invalidate();
            this.zzdxv.addView(this.zzdyj, new FrameLayout.LayoutParams(-1, -1));
            this.zzdxv.bringChildToFront(this.zzdyj);
        }
        this.zzdxx.pause();
        this.zzdyf = this.zzdye;
        zzawb.zzdsr.post(new zzbam(this));
    }

    public final void zzxx() {
        if (this.zzdya && zzyc()) {
            this.zzdxv.removeView(this.zzdyj);
        }
        if (this.zzdyi != null) {
            long elapsedRealtime = zzq.zzkx().elapsedRealtime();
            if (this.zzdxz.getBitmap(this.zzdyi) != null) {
                this.zzdyk = true;
            }
            long elapsedRealtime2 = zzq.zzkx().elapsedRealtime() - elapsedRealtime;
            if (zzavs.zzvs()) {
                StringBuilder sb = new StringBuilder(46);
                sb.append("Spinner frame grab took ");
                sb.append(elapsedRealtime2);
                sb.append("ms");
                zzavs.zzed(sb.toString());
            }
            if (elapsedRealtime2 > this.zzdxy) {
                zzayu.zzez("Spinner frame grab crossed jank threshold! Suspending spinner.");
                this.zzdyd = false;
                this.zzdyi = null;
                zzaae zzaae = this.zzdxw;
                if (zzaae != null) {
                    zzaae.zzh("spinner_jank", Long.toString(elapsedRealtime2));
                }
            }
        }
    }

    public final void zzxy() {
        zzbag zzbag = this.zzdxz;
        if (zzbag != null) {
            zzbag.zzdxt.setMuted(true);
            zzbag.zzxs();
        }
    }

    public final void zzxz() {
        zzbag zzbag = this.zzdxz;
        if (zzbag != null) {
            zzbag.zzdxt.setMuted(false);
            zzbag.zzxs();
        }
    }

    @TargetApi(14)
    public final void zzya() {
        zzbag zzbag = this.zzdxz;
        if (zzbag != null) {
            TextView textView = new TextView(zzbag.getContext());
            String valueOf = String.valueOf(this.zzdxz.zzxo());
            textView.setText(valueOf.length() != 0 ? "AdMob - ".concat(valueOf) : new String("AdMob - "));
            textView.setTextColor(-65536);
            textView.setBackgroundColor(-256);
            this.zzdxv.addView(textView, new FrameLayout.LayoutParams(-2, -2, 17));
            this.zzdxv.bringChildToFront(textView);
        }
    }

    /* access modifiers changed from: package-private */
    public final void zzyb() {
        zzbag zzbag = this.zzdxz;
        if (zzbag != null) {
            long currentPosition = (long) zzbag.getCurrentPosition();
            if (this.zzdye != currentPosition && currentPosition > 0) {
                zzd("timeupdate", "time", String.valueOf(((float) currentPosition) / 1000.0f));
                this.zzdye = currentPosition;
            }
        }
    }

    public static void zza(zzbaz zzbaz, Map<String, List<Map<String, Object>>> map) {
        HashMap hashMap = new HashMap();
        hashMap.put(TapjoyConstants.TJC_SDK_TYPE_DEFAULT, "decoderProps");
        hashMap.put("mimeTypes", map);
        zzbaz.zza("onVideoEvent", hashMap);
    }

    /* access modifiers changed from: private */
    public final void zzd(String str, String... strArr) {
        HashMap hashMap = new HashMap();
        hashMap.put(TapjoyConstants.TJC_SDK_TYPE_DEFAULT, str);
        String str2 = null;
        for (String str3 : strArr) {
            if (str2 == null) {
                str2 = str3;
            } else {
                hashMap.put(str2, str3);
                str2 = null;
            }
        }
        this.zzdxu.zza("onVideoEvent", hashMap);
    }

    public static void zza(zzbaz zzbaz, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put(TapjoyConstants.TJC_SDK_TYPE_DEFAULT, "decoderProps");
        hashMap.put("error", str);
        zzbaz.zza("onVideoEvent", hashMap);
    }

    public final void zza(float f2, float f3) {
        zzbag zzbag = this.zzdxz;
        if (zzbag != null) {
            zzbag.zza(f2, f3);
        }
    }
}
