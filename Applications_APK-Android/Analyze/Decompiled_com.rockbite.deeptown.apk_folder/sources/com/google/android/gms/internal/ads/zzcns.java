package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public class zzcns {
    private final zzaak zzgca;

    public zzcns(zzaak zzaak) {
        this.zzgca = zzaak;
    }

    /* access modifiers changed from: package-private */
    public final zzaak zzami() {
        return this.zzgca;
    }
}
