package com.google.android.gms.internal.ads;

import com.google.ads.AdRequest;
import com.google.ads.mediation.MediationAdRequest;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzamr {
    public static int zza(AdRequest.ErrorCode errorCode) {
        int i2 = zzamu.zzdej[errorCode.ordinal()];
        if (i2 == 2) {
            return 1;
        }
        if (i2 != 3) {
            return i2 != 4 ? 0 : 3;
        }
        return 2;
    }

    public static MediationAdRequest zza(zzug zzug, boolean z) {
        AdRequest.Gender gender;
        List<String> list = zzug.zzcca;
        HashSet hashSet = list != null ? new HashSet(list) : null;
        Date date = new Date(zzug.zzcby);
        int i2 = zzug.zzcbz;
        if (i2 == 1) {
            gender = AdRequest.Gender.MALE;
        } else if (i2 != 2) {
            gender = AdRequest.Gender.UNKNOWN;
        } else {
            gender = AdRequest.Gender.FEMALE;
        }
        return new MediationAdRequest(date, gender, hashSet, z, zzug.zzmi);
    }
}
