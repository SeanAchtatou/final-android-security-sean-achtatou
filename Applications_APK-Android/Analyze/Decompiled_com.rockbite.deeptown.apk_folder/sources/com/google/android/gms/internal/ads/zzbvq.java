package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbvq implements zzdxg<String> {
    private static final zzbvq zzfjz = new zzbvq();

    public static zzbvq zzaim() {
        return zzfjz;
    }

    public final /* synthetic */ Object get() {
        return (String) zzdxm.zza("native", "Cannot return null from a non-@Nullable @Provides method");
    }
}
