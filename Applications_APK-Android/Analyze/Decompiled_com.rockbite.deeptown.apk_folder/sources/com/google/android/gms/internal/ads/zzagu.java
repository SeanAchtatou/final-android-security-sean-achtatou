package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface zzagu extends IInterface {
    void zzc(List<zzagn> list) throws RemoteException;
}
