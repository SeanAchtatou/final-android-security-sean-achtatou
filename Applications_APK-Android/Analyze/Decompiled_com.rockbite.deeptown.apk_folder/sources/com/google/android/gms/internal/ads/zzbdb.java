package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbdb implements Runnable {
    private final boolean zzdym;
    private final long zzebn;
    private final zzbaz zzedz;

    zzbdb(zzbaz zzbaz, boolean z, long j2) {
        this.zzedz = zzbaz;
        this.zzdym = z;
        this.zzebn = j2;
    }

    public final void run() {
        this.zzedz.zza(this.zzdym, this.zzebn);
    }
}
