package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbyf implements zzdxg<zzbxa> {
    private final zzdxp<zzbye> zzetg;
    private final zzbyg zzfon;

    public zzbyf(zzbyg zzbyg, zzdxp<zzbye> zzdxp) {
        this.zzfon = zzbyg;
        this.zzetg = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return (zzbxa) zzdxm.zza(this.zzetg.get(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
