package com.google.android.gms.internal.ads;

import com.google.android.gms.common.internal.Objects;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzaxi {
    public final int count;
    public final String name;
    private final double zzdtx;
    private final double zzdty;
    public final double zzdtz;

    public zzaxi(String str, double d2, double d3, double d4, int i2) {
        this.name = str;
        this.zzdty = d2;
        this.zzdtx = d3;
        this.zzdtz = d4;
        this.count = i2;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof zzaxi)) {
            return false;
        }
        zzaxi zzaxi = (zzaxi) obj;
        if (Objects.equal(this.name, zzaxi.name) && this.zzdtx == zzaxi.zzdtx && this.zzdty == zzaxi.zzdty && this.count == zzaxi.count && Double.compare(this.zzdtz, zzaxi.zzdtz) == 0) {
            return true;
        }
        return false;
    }

    public final int hashCode() {
        return Objects.hashCode(this.name, Double.valueOf(this.zzdtx), Double.valueOf(this.zzdty), Double.valueOf(this.zzdtz), Integer.valueOf(this.count));
    }

    public final String toString() {
        return Objects.toStringHelper(this).add("name", this.name).add("minBound", Double.valueOf(this.zzdty)).add("maxBound", Double.valueOf(this.zzdtx)).add("percent", Double.valueOf(this.zzdtz)).add("count", Integer.valueOf(this.count)).toString();
    }
}
