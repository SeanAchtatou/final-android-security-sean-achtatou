package com.google.android.gms.internal.ads;

import android.content.Context;
import android.view.ViewGroup;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbao {
    private final Context zzcgd;
    private final zzbaz zzdxu;
    private final ViewGroup zzdyp;
    private zzbai zzdyq;

    public zzbao(Context context, ViewGroup viewGroup, zzbdi zzbdi) {
        this(context, viewGroup, zzbdi, null);
    }

    public final void onDestroy() {
        Preconditions.checkMainThread("onDestroy must be called from the UI thread.");
        zzbai zzbai = this.zzdyq;
        if (zzbai != null) {
            zzbai.destroy();
            this.zzdyp.removeView(this.zzdyq);
            this.zzdyq = null;
        }
    }

    public final void onPause() {
        Preconditions.checkMainThread("onPause must be called from the UI thread.");
        zzbai zzbai = this.zzdyq;
        if (zzbai != null) {
            zzbai.pause();
        }
    }

    public final void zza(int i2, int i3, int i4, int i5, int i6, boolean z, zzbaw zzbaw) {
        if (this.zzdyq == null) {
            zzzv.zza(this.zzdxu.zzyq().zzqp(), this.zzdxu.zzym(), "vpr2");
            Context context = this.zzcgd;
            zzbaz zzbaz = this.zzdxu;
            this.zzdyq = new zzbai(context, zzbaz, i6, z, zzbaz.zzyq().zzqp(), zzbaw);
            this.zzdyp.addView(this.zzdyq, 0, new ViewGroup.LayoutParams(-1, -1));
            this.zzdyq.zzd(i2, i3, i4, i5);
            this.zzdxu.zzav(false);
        }
    }

    public final void zze(int i2, int i3, int i4, int i5) {
        Preconditions.checkMainThread("The underlay may only be modified from the UI thread.");
        zzbai zzbai = this.zzdyq;
        if (zzbai != null) {
            zzbai.zzd(i2, i3, i4, i5);
        }
    }

    public final zzbai zzye() {
        Preconditions.checkMainThread("getAdVideoUnderlay must be called from the UI thread.");
        return this.zzdyq;
    }

    @VisibleForTesting
    private zzbao(Context context, ViewGroup viewGroup, zzbaz zzbaz, zzbai zzbai) {
        this.zzcgd = context.getApplicationContext() != null ? context.getApplicationContext() : context;
        this.zzdyp = viewGroup;
        this.zzdxu = zzbaz;
        this.zzdyq = null;
    }
}
