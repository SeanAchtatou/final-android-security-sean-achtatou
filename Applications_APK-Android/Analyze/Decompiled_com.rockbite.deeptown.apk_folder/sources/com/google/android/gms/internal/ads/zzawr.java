package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.view.View;
import android.view.WindowInsets;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzawr implements View.OnApplyWindowInsetsListener {
    private final zzawo zzdsy;
    private final Activity zzdsz;

    zzawr(zzawo zzawo, Activity activity) {
        this.zzdsy = zzawo;
        this.zzdsz = activity;
    }

    public final WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
        return zzawo.zza(this.zzdsz, view, windowInsets);
    }
}
