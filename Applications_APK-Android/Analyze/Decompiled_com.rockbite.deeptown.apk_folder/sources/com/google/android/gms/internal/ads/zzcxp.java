package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcxp {
    public static <T> void zza(AtomicReference<T> atomicReference, zzcxo<T> zzcxo) {
        T t = atomicReference.get();
        if (t != null) {
            try {
                zzcxo.zzt(t);
            } catch (RemoteException e2) {
                zzayu.zze("#007 Could not call remote method.", e2);
            }
        }
    }
}
