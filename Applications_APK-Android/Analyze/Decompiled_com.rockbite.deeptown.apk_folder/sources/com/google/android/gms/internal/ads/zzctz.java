package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzctz implements zzdxg<zzctx> {
    private final zzdxp<zzcxw> zzfjl;

    private zzctz(zzdxp<zzcxw> zzdxp) {
        this.zzfjl = zzdxp;
    }

    public static zzctz zzal(zzdxp<zzcxw> zzdxp) {
        return new zzctz(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzctx(this.zzfjl.get());
    }
}
