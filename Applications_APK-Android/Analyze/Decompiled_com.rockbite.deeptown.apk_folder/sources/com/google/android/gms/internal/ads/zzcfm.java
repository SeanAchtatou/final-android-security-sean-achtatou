package com.google.android.gms.internal.ads;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import java.util.List;
import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcfm implements Callable {
    private final zzazb zzfek;
    private final zzdhe zzfpn;
    private final String zzfru;
    private final ApplicationInfo zzfuu;
    private final List zzfuv;
    private final PackageInfo zzfuw;
    private final zzdhe zzfux;
    private final zzavu zzfuy;
    private final String zzfuz;

    zzcfm(zzdhe zzdhe, zzazb zzazb, ApplicationInfo applicationInfo, String str, List list, PackageInfo packageInfo, zzdhe zzdhe2, zzavu zzavu, String str2) {
        this.zzfpn = zzdhe;
        this.zzfek = zzazb;
        this.zzfuu = applicationInfo;
        this.zzfru = str;
        this.zzfuv = list;
        this.zzfuw = packageInfo;
        this.zzfux = zzdhe2;
        this.zzfuy = zzavu;
        this.zzfuz = str2;
    }

    public final Object call() {
        zzdhe zzdhe = this.zzfpn;
        zzazb zzazb = this.zzfek;
        ApplicationInfo applicationInfo = this.zzfuu;
        String str = this.zzfru;
        List list = this.zzfuv;
        PackageInfo packageInfo = this.zzfuw;
        zzdhe zzdhe2 = this.zzfux;
        zzavu zzavu = this.zzfuy;
        String str2 = this.zzfuz;
        boolean zzvy = zzavu.zzvy();
        return new zzaqk((Bundle) zzdhe.get(), zzazb, applicationInfo, str, list, packageInfo, (String) zzdhe2.get(), zzvy, str2, null, null);
    }
}
