package com.google.android.gms.internal.ads;

import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcvj implements zzcty<JSONObject> {
    private String zzghz;
    private String zzgia;

    public zzcvj(String str, String str2) {
        this.zzghz = str;
        this.zzgia = str2;
    }

    public final /* synthetic */ void zzr(Object obj) {
        try {
            JSONObject zzb = zzaxs.zzb((JSONObject) obj, "pii");
            zzb.put("doritos", this.zzghz);
            zzb.put("doritos_v2", this.zzgia);
        } catch (JSONException unused) {
            zzavs.zzed("Failed putting doritos string.");
        }
    }
}
