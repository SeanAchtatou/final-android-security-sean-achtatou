package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbxf implements zzdxg<zzbws> {
    private final zzbxe zzfmy;

    private zzbxf(zzbxe zzbxe) {
        this.zzfmy = zzbxe;
    }

    public static zzbxf zza(zzbxe zzbxe) {
        return new zzbxf(zzbxe);
    }

    public static zzbws zzb(zzbxe zzbxe) {
        return (zzbws) zzdxm.zza(zzbxe.zzajx(), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        return zzb(this.zzfmy);
    }
}
