package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.dynamic.ObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzaud implements zzaul {
    private final Context zzcri;
    private final String zzcyr;

    zzaud(Context context, String str) {
        this.zzcri = context;
        this.zzcyr = str;
    }

    public final void zza(zzbfq zzbfq) {
        Context context = this.zzcri;
        zzbfq.zzb(ObjectWrapper.wrap(context), this.zzcyr, context.getPackageName());
    }
}
