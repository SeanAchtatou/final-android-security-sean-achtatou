package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcdx implements zzdxg<zzcdv> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzayy> zzepi;
    private final zzdxp<Executor> zzfcv;

    public zzcdx(zzdxp<Executor> zzdxp, zzdxp<zzayy> zzdxp2, zzdxp<Context> zzdxp3) {
        this.zzfcv = zzdxp;
        this.zzepi = zzdxp2;
        this.zzejv = zzdxp3;
    }

    public final /* synthetic */ Object get() {
        return new zzcdv(this.zzfcv.get(), this.zzepi.get(), this.zzejv.get());
    }
}
