package com.google.android.gms.internal.ads;

import android.os.Build;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbce implements zzbcv {
    public final zzbcn zza(zzbaz zzbaz, int i2, String str, zzbaw zzbaw) {
        if (Build.VERSION.SDK_INT < 16 || i2 <= 0) {
            return new zzbcu(zzbaz);
        }
        int zzzo = zzbbs.zzzo();
        if (zzzo < zzbaw.zzdzl) {
            return new zzbcy(zzbaz, zzbaw);
        }
        if (zzzo < zzbaw.zzdzf) {
            return new zzbcz(zzbaz, zzbaw);
        }
        return new zzbcx(zzbaz);
    }
}
