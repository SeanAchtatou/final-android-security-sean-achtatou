package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.doubleclick.AppEventListener;
import com.google.android.gms.ads.internal.zzq;
import java.util.Collections;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzceo implements AppEventListener, zzbov, zzbow, zzbpe, zzbph, zzbqb, zzbqx, zzdcx, zzty {
    private long startTime;
    private final List<Object> zzdvu;
    private final zzcec zzfti;

    public zzceo(zzcec zzcec, zzbfx zzbfx) {
        this.zzfti = zzcec;
        this.zzdvu = Collections.singletonList(zzbfx);
    }

    public final void onAdClicked() {
        zza(zzty.class, "onAdClicked", new Object[0]);
    }

    public final void onAdClosed() {
        zza(zzbov.class, "onAdClosed", new Object[0]);
    }

    public final void onAdFailedToLoad(int i2) {
        zza(zzbow.class, "onAdFailedToLoad", Integer.valueOf(i2));
    }

    public final void onAdImpression() {
        zza(zzbpe.class, "onAdImpression", new Object[0]);
    }

    public final void onAdLeftApplication() {
        zza(zzbov.class, "onAdLeftApplication", new Object[0]);
    }

    public final void onAdLoaded() {
        long elapsedRealtime = zzq.zzkx().elapsedRealtime() - this.startTime;
        StringBuilder sb = new StringBuilder(41);
        sb.append("Ad Request Latency : ");
        sb.append(elapsedRealtime);
        zzavs.zzed(sb.toString());
        zza(zzbqb.class, "onAdLoaded", new Object[0]);
    }

    public final void onAdOpened() {
        zza(zzbov.class, "onAdOpened", new Object[0]);
    }

    public final void onAppEvent(String str, String str2) {
        zza(AppEventListener.class, "onAppEvent", str, str2);
    }

    public final void onRewardedVideoCompleted() {
        zza(zzbov.class, "onRewardedVideoCompleted", new Object[0]);
    }

    public final void onRewardedVideoStarted() {
        zza(zzbov.class, "onRewardedVideoStarted", new Object[0]);
    }

    public final void zza(zzdco zzdco, String str) {
        zza(zzdcp.class, "onTaskCreated", str);
    }

    public final void zzb(zzare zzare, String str, String str2) {
        zza(zzbov.class, "onRewarded", zzare, str, str2);
    }

    public final void zzb(zzczt zzczt) {
    }

    public final void zzbv(Context context) {
        zza(zzbph.class, "onPause", context);
    }

    public final void zzbw(Context context) {
        zza(zzbph.class, "onResume", context);
    }

    public final void zzbx(Context context) {
        zza(zzbph.class, "onDestroy", context);
    }

    public final void zzc(zzdco zzdco, String str) {
        zza(zzdcp.class, "onTaskSucceeded", str);
    }

    public final void zza(zzdco zzdco, String str, Throwable th) {
        zza(zzdcp.class, "onTaskFailed", str, th.getClass().getSimpleName());
    }

    public final void zzb(zzdco zzdco, String str) {
        zza(zzdcp.class, "onTaskStarted", str);
    }

    private final void zza(Class<?> cls, String str, Object... objArr) {
        zzcec zzcec = this.zzfti;
        List<Object> list = this.zzdvu;
        String valueOf = String.valueOf(cls.getSimpleName());
        zzcec.zza(list, valueOf.length() != 0 ? "Event-".concat(valueOf) : new String("Event-"), str, objArr);
    }

    public final void zzb(zzaqk zzaqk) {
        this.startTime = zzq.zzkx().elapsedRealtime();
        zza(zzbqx.class, "onAdRequest", new Object[0]);
    }
}
