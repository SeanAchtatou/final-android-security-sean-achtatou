package com.google.android.gms.internal.ads;

import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzaqz implements zzded {
    private final zzaqw zzdnn;

    zzaqz(zzaqw zzaqw) {
        this.zzdnn = zzaqw;
    }

    public final Object apply(Object obj) {
        return this.zzdnn.zzf((JSONObject) obj);
    }
}
