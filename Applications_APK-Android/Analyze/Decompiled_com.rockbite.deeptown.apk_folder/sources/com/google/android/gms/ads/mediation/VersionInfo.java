package com.google.android.gms.ads.mediation;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class VersionInfo {
    private final int zzejc;
    private final int zzejd;
    private final int zzeje;

    public VersionInfo(int i2, int i3, int i4) {
        this.zzejc = i2;
        this.zzejd = i3;
        this.zzeje = i4;
    }

    public final int getMajorVersion() {
        return this.zzejc;
    }

    public final int getMicroVersion() {
        return this.zzeje;
    }

    public final int getMinorVersion() {
        return this.zzejd;
    }
}
