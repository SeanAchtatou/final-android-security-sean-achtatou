package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.webkit.DownloadListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.esotericsoftware.spine.Animation;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.internal.ServerProtocol;
import com.google.android.gms.ads.internal.overlay.zzc;
import com.google.android.gms.ads.internal.overlay.zzd;
import com.google.android.gms.ads.internal.zza;
import com.google.android.gms.ads.internal.zzi;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.util.PlatformVersion;
import com.google.android.gms.common.util.Predicate;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.ads.zzso;
import com.google.android.gms.internal.ads.zzsy;
import com.underwater.demolisher.data.vo.RemoteConfigConst;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

@VisibleForTesting
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzbfg extends zzbfm implements ViewTreeObserver.OnGlobalLayoutListener, DownloadListener, zzaia, zzbdi {
    private int maxHeight = -1;
    private int maxWidth = -1;
    private String zzabg;
    private final zzazb zzbll;
    private final WindowManager zzbnl;
    private int zzdgf = -1;
    private int zzdgg = -1;
    private boolean zzdhs;
    private String zzdiy = "";
    private zzaac zzeao;
    private final zzsm zzeeg;
    private final zzbez zzefu;
    private final zzdq zzefv;
    private final zzi zzefw;
    private final zza zzefx;
    private final zzro zzefz;
    private final boolean zzega;
    private zzc zzege;
    private zzbey zzegg;
    private boolean zzegi;
    private boolean zzegj;
    private int zzegk;
    private boolean zzegl = true;
    private boolean zzegm = false;
    private zzbed zzegn;
    private boolean zzego;
    private boolean zzegp;
    private zzabw zzegq;
    private zzabr zzegr;
    private zzra zzegs;
    private int zzegt;
    /* access modifiers changed from: private */
    public int zzegu;
    private zzaac zzegv;
    private zzaac zzegw;
    private zzaab zzegx;
    private WeakReference<View.OnClickListener> zzegy;
    private zzc zzegz;
    private zzayl zzehb;
    private Map<String, zzbcn> zzehc;
    private final zzbfb zzehw;
    private final AtomicReference<IObjectWrapper> zzehx = new AtomicReference<>();
    private final DisplayMetrics zzwe;

    @VisibleForTesting
    protected zzbfg(zzbez zzbez, zzbfb zzbfb, zzbey zzbey, String str, boolean z, boolean z2, zzdq zzdq, zzazb zzazb, zzaae zzaae, zzi zzi, zza zza, zzsm zzsm, zzro zzro, boolean z3) {
        super(zzbez, zzbfb);
        this.zzefu = zzbez;
        this.zzehw = zzbfb;
        this.zzegg = zzbey;
        this.zzabg = str;
        this.zzegi = z;
        this.zzegk = -1;
        this.zzefv = zzdq;
        this.zzbll = zzazb;
        this.zzefw = zzi;
        this.zzefx = zza;
        this.zzbnl = (WindowManager) getContext().getSystemService("window");
        zzq.zzkq();
        this.zzwe = zzawb.zza(this.zzbnl);
        this.zzeeg = zzsm;
        this.zzefz = zzro;
        this.zzega = z3;
        this.zzehb = new zzayl(this.zzefu.zzyn(), this, this, null);
        zzq.zzkq().zza(zzbez, zzazb.zzbma, getSettings());
        setDownloadListener(this);
        zzabj();
        if (PlatformVersion.isAtLeastJellyBeanMR1()) {
            addJavascriptInterface(zzbee.zzc(this), "googleAdsJsInterface");
        }
        zzabn();
        this.zzegx = new zzaab(new zzaae(true, "make_wv", this.zzabg));
        this.zzegx.zzqp().zzc(zzaae);
        this.zzeao = zzzv.zzb(this.zzegx.zzqp());
        this.zzegx.zza("native:view_create", this.zzeao);
        this.zzegw = null;
        this.zzegv = null;
        zzq.zzks().zzbc(zzbez);
    }

    private final boolean zzabg() {
        int i2;
        int i3;
        boolean z = false;
        if (!this.zzehw.zzaat() && !this.zzehw.zzaau()) {
            return false;
        }
        zzve.zzou();
        DisplayMetrics displayMetrics = this.zzwe;
        int zzb = zzayk.zzb(displayMetrics, displayMetrics.widthPixels);
        zzve.zzou();
        DisplayMetrics displayMetrics2 = this.zzwe;
        int zzb2 = zzayk.zzb(displayMetrics2, displayMetrics2.heightPixels);
        Activity zzyn = this.zzefu.zzyn();
        if (zzyn == null || zzyn.getWindow() == null) {
            i3 = zzb;
            i2 = zzb2;
        } else {
            zzq.zzkq();
            int[] zzd = zzawb.zzd(zzyn);
            zzve.zzou();
            int zzb3 = zzayk.zzb(this.zzwe, zzd[0]);
            zzve.zzou();
            i2 = zzayk.zzb(this.zzwe, zzd[1]);
            i3 = zzb3;
        }
        if (this.zzdgf == zzb && this.zzdgg == zzb2 && this.maxWidth == i3 && this.maxHeight == i2) {
            return false;
        }
        if (!(this.zzdgf == zzb && this.zzdgg == zzb2)) {
            z = true;
        }
        this.zzdgf = zzb;
        this.zzdgg = zzb2;
        this.maxWidth = i3;
        this.maxHeight = i2;
        new zzaoo(this).zza(zzb, zzb2, i3, i2, this.zzwe.density, this.zzbnl.getDefaultDisplay().getRotation());
        return z;
    }

    private final void zzabi() {
        zzzv.zza(this.zzegx.zzqp(), this.zzeao, "aeh2");
    }

    private final synchronized void zzabj() {
        if (!this.zzegi) {
            if (!this.zzegg.zzabt()) {
                if (Build.VERSION.SDK_INT < 18) {
                    zzayu.zzea("Disabling hardware acceleration on an AdView.");
                    zzabk();
                    return;
                }
                zzayu.zzea("Enabling hardware acceleration on an AdView.");
                zzabl();
                return;
            }
        }
        zzayu.zzea("Enabling hardware acceleration on an overlay.");
        zzabl();
    }

    private final synchronized void zzabk() {
        if (!this.zzegj) {
            zzq.zzks();
            setLayerType(1, null);
        }
        this.zzegj = true;
    }

    private final synchronized void zzabl() {
        if (this.zzegj) {
            zzq.zzks();
            setLayerType(0, null);
        }
        this.zzegj = false;
    }

    private final synchronized void zzabm() {
        if (this.zzehc != null) {
            for (zzbcn release : this.zzehc.values()) {
                release.release();
            }
        }
        this.zzehc = null;
    }

    private final void zzabn() {
        zzaae zzqp;
        zzaab zzaab = this.zzegx;
        if (zzaab != null && (zzqp = zzaab.zzqp()) != null && zzq.zzku().zzuz() != null) {
            zzq.zzku().zzuz().zza(zzqp);
        }
    }

    private final void zzbd(boolean z) {
        HashMap hashMap = new HashMap();
        hashMap.put("isVisible", z ? "1" : AppEventsConstants.EVENT_PARAM_VALUE_NO);
        zzahz.zza(this, "onAdVisibilityChanged", hashMap);
    }

    public final View getView() {
        return this;
    }

    public final WebView getWebView() {
        return this;
    }

    /* access modifiers changed from: protected */
    public final synchronized void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isDestroyed()) {
            this.zzehb.onAttachedToWindow();
        }
        boolean z = this.zzego;
        if (this.zzehw != null && this.zzehw.zzaau()) {
            if (!this.zzegp) {
                this.zzehw.zzaaw();
                this.zzehw.zzaax();
                this.zzegp = true;
            }
            zzabg();
            z = true;
        }
        zzbd(z);
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        synchronized (this) {
            if (!isDestroyed()) {
                this.zzehb.onDetachedFromWindow();
            }
            super.onDetachedFromWindow();
            if (this.zzegp && this.zzehw != null && this.zzehw.zzaau() && getViewTreeObserver() != null && getViewTreeObserver().isAlive()) {
                this.zzehw.zzaaw();
                this.zzehw.zzaax();
                this.zzegp = false;
            }
        }
        zzbd(false);
    }

    public final void onDownloadStart(String str, String str2, String str3, String str4, long j2) {
        try {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.parse(str), str4);
            zzq.zzkq();
            zzawb.zza(getContext(), intent);
        } catch (ActivityNotFoundException unused) {
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 51 + String.valueOf(str4).length());
            sb.append("Couldn't find an Activity to view url/mimetype: ");
            sb.append(str);
            sb.append(" / ");
            sb.append(str4);
            zzayu.zzea(sb.toString());
        }
    }

    /* access modifiers changed from: protected */
    @TargetApi(21)
    public final void onDraw(Canvas canvas) {
        if (Build.VERSION.SDK_INT != 21 || !canvas.isHardwareAccelerated() || isAttachedToWindow()) {
            super.onDraw(canvas);
        }
    }

    public final boolean onGenericMotionEvent(MotionEvent motionEvent) {
        float axisValue = motionEvent.getAxisValue(9);
        float axisValue2 = motionEvent.getAxisValue(10);
        if (motionEvent.getActionMasked() == 8) {
            if (axisValue > Animation.CurveTimeline.LINEAR && !canScrollVertically(-1)) {
                return false;
            }
            if (axisValue < Animation.CurveTimeline.LINEAR && !canScrollVertically(1)) {
                return false;
            }
            if (axisValue2 > Animation.CurveTimeline.LINEAR && !canScrollHorizontally(-1)) {
                return false;
            }
            if (axisValue2 < Animation.CurveTimeline.LINEAR && !canScrollHorizontally(1)) {
                return false;
            }
        }
        return super.onGenericMotionEvent(motionEvent);
    }

    public final void onGlobalLayout() {
        boolean zzabg2 = zzabg();
        zzc zzzw = zzzw();
        if (zzzw != null && zzabg2) {
            zzzw.zztq();
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x0172  */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x01d8 A[SYNTHETIC, Splitter:B:109:0x01d8] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0125  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x0141  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:64:0x00de=Splitter:B:64:0x00de, B:116:0x01ee=Splitter:B:116:0x01ee} */
    @android.annotation.SuppressLint({"DrawAllocation"})
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void onMeasure(int r8, int r9) {
        /*
            r7 = this;
            monitor-enter(r7)
            boolean r0 = r7.isDestroyed()     // Catch:{ all -> 0x01f3 }
            r1 = 0
            if (r0 == 0) goto L_0x000d
            r7.setMeasuredDimension(r1, r1)     // Catch:{ all -> 0x01f3 }
            monitor-exit(r7)
            return
        L_0x000d:
            boolean r0 = r7.isInEditMode()     // Catch:{ all -> 0x01f3 }
            if (r0 != 0) goto L_0x01ee
            boolean r0 = r7.zzegi     // Catch:{ all -> 0x01f3 }
            if (r0 != 0) goto L_0x01ee
            com.google.android.gms.internal.ads.zzbey r0 = r7.zzegg     // Catch:{ all -> 0x01f3 }
            boolean r0 = r0.zzabu()     // Catch:{ all -> 0x01f3 }
            if (r0 == 0) goto L_0x0021
            goto L_0x01ee
        L_0x0021:
            com.google.android.gms.internal.ads.zzbey r0 = r7.zzegg     // Catch:{ all -> 0x01f3 }
            boolean r0 = r0.zzabw()     // Catch:{ all -> 0x01f3 }
            if (r0 == 0) goto L_0x002e
            super.onMeasure(r8, r9)     // Catch:{ all -> 0x01f3 }
            monitor-exit(r7)
            return
        L_0x002e:
            com.google.android.gms.internal.ads.zzbey r0 = r7.zzegg     // Catch:{ all -> 0x01f3 }
            boolean r0 = r0.zzabv()     // Catch:{ all -> 0x01f3 }
            if (r0 == 0) goto L_0x0091
            com.google.android.gms.internal.ads.zzzc<java.lang.Boolean> r0 = com.google.android.gms.internal.ads.zzzn.zzcmh     // Catch:{ all -> 0x01f3 }
            com.google.android.gms.internal.ads.zzzj r1 = com.google.android.gms.internal.ads.zzve.zzoy()     // Catch:{ all -> 0x01f3 }
            java.lang.Object r0 = r1.zzd(r0)     // Catch:{ all -> 0x01f3 }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ all -> 0x01f3 }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x01f3 }
            if (r0 == 0) goto L_0x004d
            super.onMeasure(r8, r9)     // Catch:{ all -> 0x01f3 }
            monitor-exit(r7)
            return
        L_0x004d:
            com.google.android.gms.internal.ads.zzbed r0 = r7.zzyl()     // Catch:{ all -> 0x01f3 }
            r1 = 0
            if (r0 == 0) goto L_0x0059
            float r0 = r0.getAspectRatio()     // Catch:{ all -> 0x01f3 }
            goto L_0x005a
        L_0x0059:
            r0 = 0
        L_0x005a:
            int r1 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r1 != 0) goto L_0x0063
            super.onMeasure(r8, r9)     // Catch:{ all -> 0x01f3 }
            monitor-exit(r7)
            return
        L_0x0063:
            int r8 = android.view.View.MeasureSpec.getSize(r8)     // Catch:{ all -> 0x01f3 }
            int r9 = android.view.View.MeasureSpec.getSize(r9)     // Catch:{ all -> 0x01f3 }
            float r1 = (float) r9     // Catch:{ all -> 0x01f3 }
            float r1 = r1 * r0
            int r1 = (int) r1     // Catch:{ all -> 0x01f3 }
            float r2 = (float) r8     // Catch:{ all -> 0x01f3 }
            float r2 = r2 / r0
            int r2 = (int) r2     // Catch:{ all -> 0x01f3 }
            if (r9 != 0) goto L_0x007c
            if (r2 == 0) goto L_0x007c
            float r9 = (float) r2     // Catch:{ all -> 0x01f3 }
            float r9 = r9 * r0
            int r1 = (int) r9     // Catch:{ all -> 0x01f3 }
            r9 = r2
            goto L_0x0084
        L_0x007c:
            if (r8 != 0) goto L_0x0084
            if (r1 == 0) goto L_0x0084
            float r8 = (float) r1     // Catch:{ all -> 0x01f3 }
            float r8 = r8 / r0
            int r2 = (int) r8     // Catch:{ all -> 0x01f3 }
            r8 = r1
        L_0x0084:
            int r8 = java.lang.Math.min(r1, r8)     // Catch:{ all -> 0x01f3 }
            int r9 = java.lang.Math.min(r2, r9)     // Catch:{ all -> 0x01f3 }
            r7.setMeasuredDimension(r8, r9)     // Catch:{ all -> 0x01f3 }
            monitor-exit(r7)
            return
        L_0x0091:
            com.google.android.gms.internal.ads.zzbey r0 = r7.zzegg     // Catch:{ all -> 0x01f3 }
            boolean r0 = r0.isFluid()     // Catch:{ all -> 0x01f3 }
            if (r0 == 0) goto L_0x00e3
            com.google.android.gms.internal.ads.zzzc<java.lang.Boolean> r0 = com.google.android.gms.internal.ads.zzzn.zzcmk     // Catch:{ all -> 0x01f3 }
            com.google.android.gms.internal.ads.zzzj r1 = com.google.android.gms.internal.ads.zzve.zzoy()     // Catch:{ all -> 0x01f3 }
            java.lang.Object r0 = r1.zzd(r0)     // Catch:{ all -> 0x01f3 }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ all -> 0x01f3 }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x01f3 }
            if (r0 != 0) goto L_0x00de
            boolean r0 = com.google.android.gms.common.util.PlatformVersion.isAtLeastJellyBeanMR1()     // Catch:{ all -> 0x01f3 }
            if (r0 != 0) goto L_0x00b2
            goto L_0x00de
        L_0x00b2:
            java.lang.String r0 = "/contentHeight"
            com.google.android.gms.internal.ads.zzbfi r1 = new com.google.android.gms.internal.ads.zzbfi     // Catch:{ all -> 0x01f3 }
            r1.<init>(r7)     // Catch:{ all -> 0x01f3 }
            r7.zza(r0, r1)     // Catch:{ all -> 0x01f3 }
            java.lang.String r0 = "(function() {  var height = -1;  if (document.body) {    height = document.body.offsetHeight;  } else if (document.documentElement) {    height = document.documentElement.offsetHeight;  }  var url = 'gmsg://mobileads.google.com/contentHeight?';  url += 'height=' + height;  try {    window.googleAdsJsInterface.notify(url);  } catch (e) {    var frame = document.getElementById('afma-notify-fluid');    if (!frame) {      frame = document.createElement('IFRAME');      frame.id = 'afma-notify-fluid';      frame.style.display = 'none';      var body = document.body || document.documentElement;      body.appendChild(frame);    }    frame.src = url;  }})();"
            r7.zzcy(r0)     // Catch:{ all -> 0x01f3 }
            int r8 = android.view.View.MeasureSpec.getSize(r8)     // Catch:{ all -> 0x01f3 }
            int r0 = r7.zzegu     // Catch:{ all -> 0x01f3 }
            r1 = -1
            if (r0 == r1) goto L_0x00d5
            int r9 = r7.zzegu     // Catch:{ all -> 0x01f3 }
            float r9 = (float) r9     // Catch:{ all -> 0x01f3 }
            android.util.DisplayMetrics r0 = r7.zzwe     // Catch:{ all -> 0x01f3 }
            float r0 = r0.density     // Catch:{ all -> 0x01f3 }
            float r9 = r9 * r0
            int r9 = (int) r9     // Catch:{ all -> 0x01f3 }
            goto L_0x00d9
        L_0x00d5:
            int r9 = android.view.View.MeasureSpec.getSize(r9)     // Catch:{ all -> 0x01f3 }
        L_0x00d9:
            r7.setMeasuredDimension(r8, r9)     // Catch:{ all -> 0x01f3 }
            monitor-exit(r7)
            return
        L_0x00de:
            super.onMeasure(r8, r9)     // Catch:{ all -> 0x01f3 }
            monitor-exit(r7)
            return
        L_0x00e3:
            com.google.android.gms.internal.ads.zzbey r0 = r7.zzegg     // Catch:{ all -> 0x01f3 }
            boolean r0 = r0.zzabt()     // Catch:{ all -> 0x01f3 }
            if (r0 == 0) goto L_0x00f8
            android.util.DisplayMetrics r8 = r7.zzwe     // Catch:{ all -> 0x01f3 }
            int r8 = r8.widthPixels     // Catch:{ all -> 0x01f3 }
            android.util.DisplayMetrics r9 = r7.zzwe     // Catch:{ all -> 0x01f3 }
            int r9 = r9.heightPixels     // Catch:{ all -> 0x01f3 }
            r7.setMeasuredDimension(r8, r9)     // Catch:{ all -> 0x01f3 }
            monitor-exit(r7)
            return
        L_0x00f8:
            int r0 = android.view.View.MeasureSpec.getMode(r8)     // Catch:{ all -> 0x01f3 }
            int r8 = android.view.View.MeasureSpec.getSize(r8)     // Catch:{ all -> 0x01f3 }
            int r2 = android.view.View.MeasureSpec.getMode(r9)     // Catch:{ all -> 0x01f3 }
            int r9 = android.view.View.MeasureSpec.getSize(r9)     // Catch:{ all -> 0x01f3 }
            r3 = 1073741824(0x40000000, float:2.0)
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = 2147483647(0x7fffffff, float:NaN)
            if (r0 == r4) goto L_0x0118
            if (r0 != r3) goto L_0x0114
            goto L_0x0118
        L_0x0114:
            r0 = 2147483647(0x7fffffff, float:NaN)
            goto L_0x0119
        L_0x0118:
            r0 = r8
        L_0x0119:
            if (r2 == r4) goto L_0x011d
            if (r2 != r3) goto L_0x011e
        L_0x011d:
            r5 = r9
        L_0x011e:
            com.google.android.gms.internal.ads.zzbey r2 = r7.zzegg     // Catch:{ all -> 0x01f3 }
            int r2 = r2.widthPixels     // Catch:{ all -> 0x01f3 }
            r3 = 1
            if (r2 > r0) goto L_0x012e
            com.google.android.gms.internal.ads.zzbey r2 = r7.zzegg     // Catch:{ all -> 0x01f3 }
            int r2 = r2.heightPixels     // Catch:{ all -> 0x01f3 }
            if (r2 <= r5) goto L_0x012c
            goto L_0x012e
        L_0x012c:
            r2 = 0
            goto L_0x012f
        L_0x012e:
            r2 = 1
        L_0x012f:
            com.google.android.gms.internal.ads.zzzc<java.lang.Boolean> r4 = com.google.android.gms.internal.ads.zzzn.zzcoq     // Catch:{ all -> 0x01f3 }
            com.google.android.gms.internal.ads.zzzj r6 = com.google.android.gms.internal.ads.zzve.zzoy()     // Catch:{ all -> 0x01f3 }
            java.lang.Object r4 = r6.zzd(r4)     // Catch:{ all -> 0x01f3 }
            java.lang.Boolean r4 = (java.lang.Boolean) r4     // Catch:{ all -> 0x01f3 }
            boolean r4 = r4.booleanValue()     // Catch:{ all -> 0x01f3 }
            if (r4 == 0) goto L_0x016e
            com.google.android.gms.internal.ads.zzbey r4 = r7.zzegg     // Catch:{ all -> 0x01f3 }
            int r4 = r4.widthPixels     // Catch:{ all -> 0x01f3 }
            float r4 = (float) r4     // Catch:{ all -> 0x01f3 }
            android.util.DisplayMetrics r6 = r7.zzwe     // Catch:{ all -> 0x01f3 }
            float r6 = r6.density     // Catch:{ all -> 0x01f3 }
            float r4 = r4 / r6
            float r0 = (float) r0     // Catch:{ all -> 0x01f3 }
            android.util.DisplayMetrics r6 = r7.zzwe     // Catch:{ all -> 0x01f3 }
            float r6 = r6.density     // Catch:{ all -> 0x01f3 }
            float r0 = r0 / r6
            int r0 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            if (r0 > 0) goto L_0x016a
            com.google.android.gms.internal.ads.zzbey r0 = r7.zzegg     // Catch:{ all -> 0x01f3 }
            int r0 = r0.heightPixels     // Catch:{ all -> 0x01f3 }
            float r0 = (float) r0     // Catch:{ all -> 0x01f3 }
            android.util.DisplayMetrics r4 = r7.zzwe     // Catch:{ all -> 0x01f3 }
            float r4 = r4.density     // Catch:{ all -> 0x01f3 }
            float r0 = r0 / r4
            float r4 = (float) r5     // Catch:{ all -> 0x01f3 }
            android.util.DisplayMetrics r5 = r7.zzwe     // Catch:{ all -> 0x01f3 }
            float r5 = r5.density     // Catch:{ all -> 0x01f3 }
            float r4 = r4 / r5
            int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r0 > 0) goto L_0x016a
            goto L_0x016b
        L_0x016a:
            r3 = 0
        L_0x016b:
            if (r2 == 0) goto L_0x016e
            r2 = r3
        L_0x016e:
            r0 = 8
            if (r2 == 0) goto L_0x01d8
            com.google.android.gms.internal.ads.zzbey r2 = r7.zzegg     // Catch:{ all -> 0x01f3 }
            int r2 = r2.widthPixels     // Catch:{ all -> 0x01f3 }
            float r2 = (float) r2     // Catch:{ all -> 0x01f3 }
            android.util.DisplayMetrics r3 = r7.zzwe     // Catch:{ all -> 0x01f3 }
            float r3 = r3.density     // Catch:{ all -> 0x01f3 }
            float r2 = r2 / r3
            int r2 = (int) r2     // Catch:{ all -> 0x01f3 }
            com.google.android.gms.internal.ads.zzbey r3 = r7.zzegg     // Catch:{ all -> 0x01f3 }
            int r3 = r3.heightPixels     // Catch:{ all -> 0x01f3 }
            float r3 = (float) r3     // Catch:{ all -> 0x01f3 }
            android.util.DisplayMetrics r4 = r7.zzwe     // Catch:{ all -> 0x01f3 }
            float r4 = r4.density     // Catch:{ all -> 0x01f3 }
            float r3 = r3 / r4
            int r3 = (int) r3     // Catch:{ all -> 0x01f3 }
            float r8 = (float) r8     // Catch:{ all -> 0x01f3 }
            android.util.DisplayMetrics r4 = r7.zzwe     // Catch:{ all -> 0x01f3 }
            float r4 = r4.density     // Catch:{ all -> 0x01f3 }
            float r8 = r8 / r4
            int r8 = (int) r8     // Catch:{ all -> 0x01f3 }
            float r9 = (float) r9     // Catch:{ all -> 0x01f3 }
            android.util.DisplayMetrics r4 = r7.zzwe     // Catch:{ all -> 0x01f3 }
            float r4 = r4.density     // Catch:{ all -> 0x01f3 }
            float r9 = r9 / r4
            int r9 = (int) r9     // Catch:{ all -> 0x01f3 }
            r4 = 103(0x67, float:1.44E-43)
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x01f3 }
            r5.<init>(r4)     // Catch:{ all -> 0x01f3 }
            java.lang.String r4 = "Not enough space to show ad. Needs "
            r5.append(r4)     // Catch:{ all -> 0x01f3 }
            r5.append(r2)     // Catch:{ all -> 0x01f3 }
            java.lang.String r2 = "x"
            r5.append(r2)     // Catch:{ all -> 0x01f3 }
            r5.append(r3)     // Catch:{ all -> 0x01f3 }
            java.lang.String r2 = " dp, but only has "
            r5.append(r2)     // Catch:{ all -> 0x01f3 }
            r5.append(r8)     // Catch:{ all -> 0x01f3 }
            java.lang.String r8 = "x"
            r5.append(r8)     // Catch:{ all -> 0x01f3 }
            r5.append(r9)     // Catch:{ all -> 0x01f3 }
            java.lang.String r8 = " dp."
            r5.append(r8)     // Catch:{ all -> 0x01f3 }
            java.lang.String r8 = r5.toString()     // Catch:{ all -> 0x01f3 }
            com.google.android.gms.internal.ads.zzayu.zzez(r8)     // Catch:{ all -> 0x01f3 }
            int r8 = r7.getVisibility()     // Catch:{ all -> 0x01f3 }
            if (r8 == r0) goto L_0x01d3
            r8 = 4
            r7.setVisibility(r8)     // Catch:{ all -> 0x01f3 }
        L_0x01d3:
            r7.setMeasuredDimension(r1, r1)     // Catch:{ all -> 0x01f3 }
            monitor-exit(r7)
            return
        L_0x01d8:
            int r8 = r7.getVisibility()     // Catch:{ all -> 0x01f3 }
            if (r8 == r0) goto L_0x01e1
            r7.setVisibility(r1)     // Catch:{ all -> 0x01f3 }
        L_0x01e1:
            com.google.android.gms.internal.ads.zzbey r8 = r7.zzegg     // Catch:{ all -> 0x01f3 }
            int r8 = r8.widthPixels     // Catch:{ all -> 0x01f3 }
            com.google.android.gms.internal.ads.zzbey r9 = r7.zzegg     // Catch:{ all -> 0x01f3 }
            int r9 = r9.heightPixels     // Catch:{ all -> 0x01f3 }
            r7.setMeasuredDimension(r8, r9)     // Catch:{ all -> 0x01f3 }
            monitor-exit(r7)
            return
        L_0x01ee:
            super.onMeasure(r8, r9)     // Catch:{ all -> 0x01f3 }
            monitor-exit(r7)
            return
        L_0x01f3:
            r8 = move-exception
            monitor-exit(r7)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzbfg.onMeasure(int, int):void");
    }

    public final void onPause() {
        try {
            super.onPause();
        } catch (Exception e2) {
            zzayu.zzc("Could not pause webview.", e2);
        }
    }

    public final void onResume() {
        try {
            super.onResume();
        } catch (Exception e2) {
            zzayu.zzc("Could not resume webview.", e2);
        }
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.zzehw.zzaau() || this.zzehw.zzaav()) {
            zzdq zzdq = this.zzefv;
            if (zzdq != null) {
                zzdq.zza(motionEvent);
            }
        } else {
            synchronized (this) {
                if (this.zzegq != null) {
                    this.zzegq.zzc(motionEvent);
                }
            }
        }
        return super.onTouchEvent(motionEvent);
    }

    public final void setOnClickListener(View.OnClickListener onClickListener) {
        this.zzegy = new WeakReference<>(onClickListener);
        super.setOnClickListener(onClickListener);
    }

    public final synchronized void setRequestedOrientation(int i2) {
        this.zzegk = i2;
        if (this.zzege != null) {
            this.zzege.setRequestedOrientation(this.zzegk);
        }
    }

    public final void stopLoading() {
        try {
            super.stopLoading();
        } catch (Exception e2) {
            zzayu.zzc("Could not stop loading webview.", e2);
        }
    }

    public final void zza(String str, Map map) {
        zzahz.zza(this, str, map);
    }

    public final void zza(String str, JSONObject jSONObject) {
        zzahz.zza(this, str, jSONObject);
    }

    public final void zza(boolean z, long j2) {
        HashMap hashMap = new HashMap(2);
        hashMap.put("success", z ? "1" : AppEventsConstants.EVENT_PARAM_VALUE_NO);
        hashMap.put("duration", Long.toString(j2));
        zzahz.zza(this, "onCacheAccessComplete", hashMap);
    }

    public final /* synthetic */ zzbev zzaaa() {
        return this.zzehw;
    }

    public final WebViewClient zzaab() {
        return this.zzehw;
    }

    public final synchronized boolean zzaac() {
        return this.zzdhs;
    }

    public final zzdq zzaad() {
        return this.zzefv;
    }

    public final IObjectWrapper zzaae() {
        return this.zzehx.get();
    }

    public final synchronized boolean zzaaf() {
        return this.zzegi;
    }

    public final void zzaag() {
    }

    public final synchronized boolean zzaah() {
        return this.zzegl;
    }

    public final synchronized boolean zzaai() {
        return this.zzegt > 0;
    }

    public final void zzaaj() {
        this.zzehb.zzxg();
    }

    public final void zzaak() {
        if (this.zzegw == null) {
            this.zzegw = zzzv.zzb(this.zzegx.zzqp());
            this.zzegx.zza("native:view_load", this.zzegw);
        }
    }

    public final synchronized zzabw zzaal() {
        return this.zzegq;
    }

    public final void zzaam() {
        setBackgroundColor(0);
    }

    public final void zzaan() {
        zzavs.zzed("Cannot add text view to inner AdWebView");
    }

    public final synchronized zzra zzaao() {
        return this.zzegs;
    }

    public final boolean zzaap() {
        return false;
    }

    public final zzro zzaaq() {
        return this.zzefz;
    }

    public final boolean zzaar() {
        return ((Boolean) zzve.zzoy().zzd(zzzn.zzcqj)).booleanValue() && this.zzefz != null && this.zzega;
    }

    public final synchronized void zzal(boolean z) {
        if (this.zzege != null) {
            this.zzege.zza(this.zzehw.zzaat(), z);
        } else {
            this.zzdhs = z;
        }
    }

    public final void zzan(IObjectWrapper iObjectWrapper) {
        this.zzehx.set(iObjectWrapper);
    }

    public final void zzav(boolean z) {
        this.zzehw.zzav(z);
    }

    public final synchronized void zzax(boolean z) {
        boolean z2 = z != this.zzegi;
        this.zzegi = z;
        zzabj();
        if (z2) {
            if (!((Boolean) zzve.zzoy().zzd(zzzn.zzchp)).booleanValue() || !this.zzegg.zzabt()) {
                new zzaoo(this).zzdu(z ? "expanded" : RemoteConfigConst.PREDICTION_METHOD_DEFAULT);
            }
        }
    }

    public final synchronized void zzay(boolean z) {
        this.zzegl = z;
    }

    public final synchronized void zzaz(boolean z) {
        this.zzegt += z ? 1 : -1;
        if (this.zzegt <= 0 && this.zzege != null) {
            this.zzege.zztt();
        }
    }

    public final synchronized void zzb(String str, String str2, String str3) {
        String str4 = str;
        super.loadDataWithBaseURL(str4, zzbeo.zzf(str2, zzbeo.zzabp()), "text/html", "UTF-8", str3);
    }

    public final void zzb(String str, JSONObject jSONObject) {
        zzahz.zzb(this, str, jSONObject);
    }

    public final void zzba(boolean z) {
        this.zzehw.zzba(z);
    }

    /* access modifiers changed from: protected */
    public final synchronized void zzbe(boolean z) {
        if (!z) {
            zzabn();
            this.zzehb.zzxh();
            if (this.zzege != null) {
                this.zzege.close();
                this.zzege.onDestroy();
                this.zzege = null;
            }
        }
        this.zzehx.set(null);
        this.zzehw.destroy();
        zzq.zzlm();
        zzbck.zzc(this);
        zzabm();
    }

    public final void zzbr(Context context) {
        this.zzefu.setBaseContext(context);
        this.zzehb.zzh(this.zzefu.zzyn());
    }

    public final void zzc(boolean z, int i2) {
        this.zzehw.zzc(z, i2);
    }

    public final synchronized void zzcy(String str) {
        if (!isDestroyed()) {
            super.zzcy(str);
        } else {
            zzayu.zzez("The webview is destroyed. Ignoring action.");
        }
    }

    public final void zzde(int i2) {
        if (i2 == 0) {
            zzzv.zza(this.zzegx.zzqp(), this.zzeao, "aebb2");
        }
        zzabi();
        if (this.zzegx.zzqp() != null) {
            this.zzegx.zzqp().zzh("close_type", String.valueOf(i2));
        }
        HashMap hashMap = new HashMap(2);
        hashMap.put("closetype", String.valueOf(i2));
        hashMap.put(ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION, this.zzbll.zzbma);
        zzahz.zza(this, "onhide", hashMap);
    }

    public final synchronized zzbcn zzfe(String str) {
        if (this.zzehc == null) {
            return null;
        }
        return this.zzehc.get(str);
    }

    public final void zzj(String str, String str2) {
        zzahz.zza(this, str, str2);
    }

    public final synchronized void zzjv() {
        this.zzegm = true;
        if (this.zzefw != null) {
            this.zzefw.zzjv();
        }
    }

    public final synchronized void zzjw() {
        this.zzegm = false;
        if (this.zzefw != null) {
            this.zzefw.zzjw();
        }
    }

    public final void zztr() {
        if (this.zzegv == null) {
            zzzv.zza(this.zzegx.zzqp(), this.zzeao, "aes2");
            this.zzegv = zzzv.zzb(this.zzegx.zzqp());
            this.zzegx.zza("native:view_show", this.zzegv);
        }
        HashMap hashMap = new HashMap(1);
        hashMap.put(ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION, this.zzbll.zzbma);
        zzahz.zza(this, "onshow", hashMap);
    }

    public final void zzts() {
        zzc zzzw = zzzw();
        if (zzzw != null) {
            zzzw.zzts();
        }
    }

    public final zzbao zzyk() {
        return null;
    }

    public final synchronized zzbed zzyl() {
        return this.zzegn;
    }

    public final zzaac zzym() {
        return this.zzeao;
    }

    public final Activity zzyn() {
        return this.zzefu.zzyn();
    }

    public final zza zzyo() {
        return this.zzefx;
    }

    public final synchronized String zzyp() {
        return this.zzdiy;
    }

    public final zzaab zzyq() {
        return this.zzegx;
    }

    public final zzazb zzyr() {
        return this.zzbll;
    }

    public final int zzys() {
        return getMeasuredHeight();
    }

    public final int zzyt() {
        return getMeasuredWidth();
    }

    public final synchronized void zzyu() {
        if (this.zzegr != null) {
            this.zzegr.zzrb();
        }
    }

    public final void zzzt() {
        zzabi();
        HashMap hashMap = new HashMap(1);
        hashMap.put(ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION, this.zzbll.zzbma);
        zzahz.zza(this, "onhide", hashMap);
    }

    public final void zzzu() {
        HashMap hashMap = new HashMap(3);
        hashMap.put("app_muted", String.valueOf(zzq.zzkv().zzpf()));
        hashMap.put("app_volume", String.valueOf(zzq.zzkv().zzpe()));
        hashMap.put("device_volume", String.valueOf(zzawq.zzbe(getContext())));
        zzahz.zza(this, "volume", hashMap);
    }

    public final Context zzzv() {
        return this.zzefu.zzzv();
    }

    public final synchronized zzc zzzw() {
        return this.zzege;
    }

    public final synchronized zzc zzzx() {
        return this.zzegz;
    }

    public final synchronized zzbey zzzy() {
        return this.zzegg;
    }

    public final synchronized String zzzz() {
        return this.zzabg;
    }

    public final synchronized void zzb(zzc zzc) {
        this.zzegz = zzc;
    }

    public final synchronized void zza(zzc zzc) {
        this.zzege = zzc;
    }

    public final void zzb(String str, zzafn<? super zzbdi> zzafn) {
        zzbfb zzbfb = this.zzehw;
        if (zzbfb != null) {
            zzbfb.zzb(str, zzafn);
        }
    }

    public final synchronized void zza(zzbey zzbey) {
        this.zzegg = zzbey;
        requestLayout();
    }

    public final boolean zzb(boolean z, int i2) {
        destroy();
        this.zzeeg.zza(new zzbfj(z, i2));
        this.zzeeg.zza(zzso.zza.C0151zza.ANDROID_WEBVIEW_CRASH);
        return true;
    }

    public final synchronized void zza(String str, zzbcn zzbcn) {
        if (this.zzehc == null) {
            this.zzehc = new HashMap();
        }
        this.zzehc.put(str, zzbcn);
    }

    public final synchronized void zza(zzabw zzabw) {
        this.zzegq = zzabw;
    }

    public final synchronized void zza(zzabr zzabr) {
        this.zzegr = zzabr;
    }

    public final synchronized void zza(zzra zzra) {
        this.zzegs = zzra;
    }

    public final synchronized void zza(zzbed zzbed) {
        if (this.zzegn != null) {
            zzayu.zzex("Attempt to create multiple AdWebViewVideoControllers.");
        } else {
            this.zzegn = zzbed;
        }
    }

    public final void zza(zzd zzd) {
        this.zzehw.zza(zzd);
    }

    public final void zza(boolean z, int i2, String str) {
        this.zzehw.zza(z, i2, str);
    }

    public final void zza(boolean z, int i2, String str, String str2) {
        this.zzehw.zza(z, i2, str, str2);
    }

    public final void zza(zzpt zzpt) {
        synchronized (this) {
            this.zzego = zzpt.zzbnq;
        }
        zzbd(zzpt.zzbnq);
    }

    public final void zza(String str, zzafn<? super zzbdi> zzafn) {
        zzbfb zzbfb = this.zzehw;
        if (zzbfb != null) {
            zzbfb.zza(str, zzafn);
        }
    }

    public final void zza(String str, Predicate<zzafn<? super zzbdi>> predicate) {
        zzbfb zzbfb = this.zzehw;
        if (zzbfb != null) {
            zzbfb.zza(str, predicate);
        }
    }

    public final void zza(ViewGroup viewGroup, Activity activity, String str, String str2) {
        if (!zzaar()) {
            zzavs.zzed("AR ad is not enabled or the ad from the server is not an AR ad.");
            return;
        }
        if (getParent() instanceof ViewGroup) {
            ((ViewGroup) getParent()).removeView(this);
        }
        zzavs.zzed("Initializing ArWebView object.");
        this.zzefz.zza(activity, this);
        this.zzefz.zzc(str, str2);
        if (viewGroup != null) {
            viewGroup.addView(this.zzefz.getView());
        } else {
            zzayu.zzex("The FrameLayout object cannot be null.");
        }
    }

    static final /* synthetic */ void zza(boolean z, int i2, zztu zztu) {
        zzsy.zzw.zza zzog = zzsy.zzw.zzog();
        if (zzog.zzof() != z) {
            zzog.zzw(z);
        }
        zztu.zzcba = (zzsy.zzw) zzog.zzci(i2).zzbaf();
    }
}
