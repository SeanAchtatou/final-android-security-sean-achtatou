package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.measurement.api.AppMeasurementSdk;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzaku {
    private static zzaku zzdbm;
    private AtomicBoolean zzdbn = new AtomicBoolean(false);

    zzaku() {
    }

    private static void zza(Context context, AppMeasurementSdk appMeasurementSdk) {
        try {
            ((zzbfs) zzayx.zza(context, "com.google.android.gms.ads.measurement.DynamiteMeasurementManager", zzakv.zzbtz)).zza(ObjectWrapper.wrap(context), new zzakr(appMeasurementSdk));
        } catch (RemoteException | zzayz | NullPointerException e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
        }
    }

    static /* synthetic */ void zzd(Context context, String str) {
        boolean z;
        zzzn.initialize(context);
        if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcih)).booleanValue()) {
            if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcig)).booleanValue()) {
                z = false;
                Bundle bundle = new Bundle();
                bundle.putBoolean("measurementEnabled", z);
                zza(context, AppMeasurementSdk.getInstance(context, "FA-Ads", "am", str, bundle));
            }
        }
        z = true;
        Bundle bundle2 = new Bundle();
        bundle2.putBoolean("measurementEnabled", z);
        zza(context, AppMeasurementSdk.getInstance(context, "FA-Ads", "am", str, bundle2));
    }

    private static boolean zzn(Context context) {
        try {
            context.getClassLoader().loadClass("com.google.firebase.analytics.FirebaseAnalytics");
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        }
    }

    static /* synthetic */ void zzo(Context context) {
        zzzn.initialize(context);
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcil)).booleanValue() && zzn(context)) {
            zza(context, AppMeasurementSdk.getInstance(context));
        }
    }

    public static zzaku zzsj() {
        if (zzdbm == null) {
            zzdbm = new zzaku();
        }
        return zzdbm;
    }

    public final Thread zzc(Context context, String str) {
        if (!this.zzdbn.compareAndSet(false, true)) {
            return null;
        }
        Thread thread = new Thread(new zzakt(this, context, str));
        thread.start();
        return thread;
    }

    public final Thread zzm(Context context) {
        if (!this.zzdbn.compareAndSet(false, true)) {
            return null;
        }
        Thread thread = new Thread(new zzakw(this, context));
        thread.start();
        return thread;
    }
}
