package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcgx implements Callable {
    private final zzdhe zzfpa;
    private final zzdhe zzfpn;

    zzcgx(zzdhe zzdhe, zzdhe zzdhe2) {
        this.zzfpn = zzdhe;
        this.zzfpa = zzdhe2;
    }

    public final Object call() {
        zzdhe zzdhe = this.zzfpn;
        zzdhe zzdhe2 = this.zzfpa;
        return new zzchk((zzchn) zzdhe.get(), ((zzchh) zzdhe2.get()).zzfwj, ((zzchh) zzdhe2.get()).zzfwi);
    }
}
