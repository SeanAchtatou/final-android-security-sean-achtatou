package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcla implements zzdxg<zzcku> {
    private final zzdxp<zzbyq> zzeqb;
    private final zzdxp<zzdhd> zzfcv;
    private final zzdxp<zzbvm> zzfyl;

    public zzcla(zzdxp<zzbvm> zzdxp, zzdxp<zzdhd> zzdxp2, zzdxp<zzbyq> zzdxp3) {
        this.zzfyl = zzdxp;
        this.zzfcv = zzdxp2;
        this.zzeqb = zzdxp3;
    }

    public final /* synthetic */ Object get() {
        return new zzcku(this.zzfyl.get(), this.zzfcv.get(), this.zzeqb.get());
    }
}
