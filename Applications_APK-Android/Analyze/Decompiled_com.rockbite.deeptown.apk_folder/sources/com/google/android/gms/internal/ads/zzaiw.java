package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzaiw implements zzaii {
    private final zzais zzczi;
    private final zzajj zzczq;
    private final zzaif zzczr;

    zzaiw(zzais zzais, zzajj zzajj, zzaif zzaif) {
        this.zzczi = zzais;
        this.zzczq = zzajj;
        this.zzczr = zzaif;
    }

    public final void zzsa() {
        zzawb.zzdsr.postDelayed(new zzaiv(this.zzczi, this.zzczq, this.zzczr), (long) zzajd.zzdaa);
    }
}
