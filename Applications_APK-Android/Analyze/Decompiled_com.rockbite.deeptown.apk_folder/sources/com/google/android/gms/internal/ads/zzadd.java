package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public abstract class zzadd extends zzgb implements zzade {
    public zzadd() {
        super("com.google.android.gms.ads.internal.formats.client.INativeCustomTemplateAd");
    }

    public static zzade zzp(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.formats.client.INativeCustomTemplateAd");
        if (queryLocalInterface instanceof zzade) {
            return (zzade) queryLocalInterface;
        }
        return new zzadg(iBinder);
    }

    /* access modifiers changed from: protected */
    public final boolean zza(int i2, Parcel parcel, Parcel parcel2, int i3) throws RemoteException {
        switch (i2) {
            case 1:
                String zzct = zzct(parcel.readString());
                parcel2.writeNoException();
                parcel2.writeString(zzct);
                return true;
            case 2:
                zzaci zzcu = zzcu(parcel.readString());
                parcel2.writeNoException();
                zzge.zza(parcel2, zzcu);
                return true;
            case 3:
                List<String> availableAssetNames = getAvailableAssetNames();
                parcel2.writeNoException();
                parcel2.writeStringList(availableAssetNames);
                return true;
            case 4:
                String customTemplateId = getCustomTemplateId();
                parcel2.writeNoException();
                parcel2.writeString(customTemplateId);
                return true;
            case 5:
                performClick(parcel.readString());
                parcel2.writeNoException();
                return true;
            case 6:
                recordImpression();
                parcel2.writeNoException();
                return true;
            case 7:
                zzxb videoController = getVideoController();
                parcel2.writeNoException();
                zzge.zza(parcel2, videoController);
                return true;
            case 8:
                destroy();
                parcel2.writeNoException();
                return true;
            case 9:
                IObjectWrapper zzrk = zzrk();
                parcel2.writeNoException();
                zzge.zza(parcel2, zzrk);
                return true;
            case 10:
                boolean zzp = zzp(IObjectWrapper.Stub.asInterface(parcel.readStrongBinder()));
                parcel2.writeNoException();
                zzge.writeBoolean(parcel2, zzp);
                return true;
            case 11:
                IObjectWrapper zzrf = zzrf();
                parcel2.writeNoException();
                zzge.zza(parcel2, zzrf);
                return true;
            case 12:
                boolean zzrl = zzrl();
                parcel2.writeNoException();
                zzge.writeBoolean(parcel2, zzrl);
                return true;
            case 13:
                boolean zzrm = zzrm();
                parcel2.writeNoException();
                zzge.writeBoolean(parcel2, zzrm);
                return true;
            case 14:
                zzq(IObjectWrapper.Stub.asInterface(parcel.readStrongBinder()));
                parcel2.writeNoException();
                return true;
            case 15:
                zzrn();
                parcel2.writeNoException();
                return true;
            default:
                return false;
        }
    }
}
