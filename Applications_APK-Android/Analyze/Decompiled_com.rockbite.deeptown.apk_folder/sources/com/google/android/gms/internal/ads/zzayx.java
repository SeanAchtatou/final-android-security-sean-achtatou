package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.IBinder;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzayx {
    public static <T> T zza(Context context, String str, zzayw<IBinder, T> zzayw) throws zzayz {
        try {
            return zzayw.apply(zzbq(context).instantiate(str));
        } catch (Exception e2) {
            throw new zzayz(e2);
        }
    }

    public static Context zzbp(Context context) throws zzayz {
        return zzbq(context).getModuleContext();
    }

    private static DynamiteModule zzbq(Context context) throws zzayz {
        try {
            return DynamiteModule.load(context, DynamiteModule.PREFER_REMOTE, ModuleDescriptor.MODULE_ID);
        } catch (Exception e2) {
            throw new zzayz(e2);
        }
    }
}
