package com.google.android.gms.internal.ads;

import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbte extends zzbrl<zzafx> implements zzafx {
    public zzbte(Set<zzbsu<zzafx>> set) {
        super(set);
    }

    public final void zza(zzasd zzasd) {
        zza(new zzbtg(zzasd));
    }

    public final synchronized void zzrs() {
        zza(zzbtd.zzfhp);
    }

    public final void zzrt() {
        zza(zzbtf.zzfhp);
    }
}
