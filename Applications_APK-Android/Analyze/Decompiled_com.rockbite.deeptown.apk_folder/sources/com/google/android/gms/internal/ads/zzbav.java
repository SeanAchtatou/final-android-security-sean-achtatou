package com.google.android.gms.internal.ads;

import android.os.Looper;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzbav implements Runnable {
    zzbav(zzbas zzbas) {
    }

    public final void run() {
        Looper.myLooper().quit();
    }
}
