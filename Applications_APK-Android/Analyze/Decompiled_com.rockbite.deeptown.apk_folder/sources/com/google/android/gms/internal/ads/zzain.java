package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzain implements Runnable {
    private final /* synthetic */ zzaih zzcze;
    private final /* synthetic */ String zzczg;

    zzain(zzaih zzaih, String str) {
        this.zzcze = zzaih;
        this.zzczg = str;
    }

    public final void run() {
        this.zzcze.zzcza.loadUrl(this.zzczg);
    }
}
