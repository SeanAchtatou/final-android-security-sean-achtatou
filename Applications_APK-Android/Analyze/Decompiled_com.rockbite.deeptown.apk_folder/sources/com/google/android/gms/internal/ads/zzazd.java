package com.google.android.gms.internal.ads;

import com.facebook.appevents.AppEventsConstants;
import com.google.android.gms.common.api.Api;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzazd {
    public static final zzdhd zzdwe = zza(new ThreadPoolExecutor(2, (int) Api.BaseClientBuilder.API_PRIORITY_OTHER, 10, TimeUnit.SECONDS, new SynchronousQueue(), zzfc("Default")));
    public static final zzdhd zzdwf;
    public static final zzdhd zzdwg;
    public static final ScheduledExecutorService zzdwh = new ScheduledThreadPoolExecutor(3, zzfc(AppEventsConstants.EVENT_NAME_SCHEDULE));
    public static final zzdhd zzdwi = zza(new zzazf());
    public static final zzdhd zzdwj = zza(zzdhg.zzarw());

    static {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(5, 5, 10, TimeUnit.SECONDS, new LinkedBlockingQueue(), zzfc("Loader"));
        threadPoolExecutor.allowCoreThreadTimeOut(true);
        zzdwf = zza(threadPoolExecutor);
        ThreadPoolExecutor threadPoolExecutor2 = new ThreadPoolExecutor(1, 1, 10, TimeUnit.SECONDS, new LinkedBlockingQueue(), zzfc("Activeview"));
        threadPoolExecutor2.allowCoreThreadTimeOut(true);
        zzdwg = zza(threadPoolExecutor2);
    }

    private static zzdhd zza(Executor executor) {
        return new zzaze(executor, null);
    }

    private static ThreadFactory zzfc(String str) {
        return new zzazc(str);
    }
}
