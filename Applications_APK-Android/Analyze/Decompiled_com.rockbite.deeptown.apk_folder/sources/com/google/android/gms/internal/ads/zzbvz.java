package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbvz implements zzdxg<zzbww> {
    private final zzdxp<zzbwv> zzetz;
    private final zzbvy zzfla;

    public zzbvz(zzbvy zzbvy, zzdxp<zzbwv> zzdxp) {
        this.zzfla = zzbvy;
        this.zzetz = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return (zzbww) zzdxm.zza(this.zzetz.get(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
