package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzq;
import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcrw implements Callable {
    private final zzcrx zzgfw;

    zzcrw(zzcrx zzcrx) {
        this.zzgfw = zzcrx;
    }

    public final Object call() {
        String str;
        String str2;
        String str3;
        zzq.zzkq();
        zzqi zzvt = zzq.zzku().zzvf().zzvt();
        Bundle bundle = null;
        if (!(zzvt == null || zzvt == null || (zzq.zzku().zzvf().zzvu() && zzq.zzku().zzvf().zzvw()))) {
            if (zzvt.zzmf()) {
                zzvt.wakeup();
            }
            zzqc zzmd = zzvt.zzmd();
            if (zzmd != null) {
                str2 = zzmd.zzls();
                str = zzmd.zzlt();
                str3 = zzmd.zzlu();
                if (str2 != null) {
                    zzq.zzku().zzvf().zzee(str2);
                }
                if (str3 != null) {
                    zzq.zzku().zzvf().zzef(str3);
                }
            } else {
                str2 = zzq.zzku().zzvf().zzvv();
                str3 = zzq.zzku().zzvf().zzvx();
                str = null;
            }
            Bundle bundle2 = new Bundle(1);
            if (!zzq.zzku().zzvf().zzvw()) {
                if (str3 == null || TextUtils.isEmpty(str3)) {
                    bundle2.putString("v_fp_vertical", "no_hash");
                } else {
                    bundle2.putString("v_fp_vertical", str3);
                }
            }
            if (str2 != null && !zzq.zzku().zzvf().zzvu()) {
                bundle2.putString("fingerprint", str2);
                if (!str2.equals(str)) {
                    bundle2.putString("v_fp", str);
                }
            }
            if (!bundle2.isEmpty()) {
                bundle = bundle2;
            }
        }
        return new zzcru(bundle);
    }
}
