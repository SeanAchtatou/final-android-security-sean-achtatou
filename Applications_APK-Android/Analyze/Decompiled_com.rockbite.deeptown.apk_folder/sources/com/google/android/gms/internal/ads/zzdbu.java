package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdbu implements zzdxg<zzdhd> {
    private static final zzdbu zzgps = new zzdbu();

    public static zzdbu zzapy() {
        return zzgps;
    }

    public final /* synthetic */ Object get() {
        return (zzdhd) zzdxm.zza(zzazd.zzdwi, "Cannot return null from a non-@Nullable @Provides method");
    }
}
