package com.google.android.gms.internal.ads;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcgs implements zzdxg<zzcgp> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzatv> zzfay;

    private zzcgs(zzdxp<Context> zzdxp, zzdxp<zzatv> zzdxp2) {
        this.zzejv = zzdxp;
        this.zzfay = zzdxp2;
    }

    public static zzcgs zzaf(zzdxp<Context> zzdxp, zzdxp<zzatv> zzdxp2) {
        return new zzcgs(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return new zzcgp(this.zzejv.get(), this.zzfay.get());
    }
}
