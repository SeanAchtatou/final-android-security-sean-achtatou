package com.google.android.gms.internal.ads;

import android.graphics.Bitmap;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzbyo implements zzded<zzo, Bitmap> {
    private final /* synthetic */ double zzfop;
    private final /* synthetic */ boolean zzfoq;
    private final /* synthetic */ zzbyl zzfor;

    zzbyo(zzbyl zzbyl, double d2, boolean z) {
        this.zzfor = zzbyl;
        this.zzfop = d2;
        this.zzfoq = z;
    }

    public final /* synthetic */ Object apply(Object obj) {
        return this.zzfor.zza(((zzo) obj).data, this.zzfop, this.zzfoq);
    }
}
