package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcaz implements zzafx {
    private final zzbpm zzfgg;
    private final zzasd zzfqx;
    private final String zzfqy;
    private final String zzfqz;

    public zzcaz(zzbpm zzbpm, zzczl zzczl) {
        this.zzfgg = zzbpm;
        this.zzfqx = zzczl.zzdky;
        this.zzfqy = zzczl.zzdcx;
        this.zzfqz = zzczl.zzdcy;
    }

    public final void zza(zzasd zzasd) {
        int i2;
        String str;
        zzasd zzasd2 = this.zzfqx;
        if (zzasd2 != null) {
            zzasd = zzasd2;
        }
        if (zzasd != null) {
            str = zzasd.type;
            i2 = zzasd.zzdno;
        } else {
            str = "";
            i2 = 1;
        }
        this.zzfgg.zzb(new zzarc(str, i2), this.zzfqy, this.zzfqz);
    }

    public final void zzrs() {
        this.zzfgg.onRewardedVideoStarted();
    }

    public final void zzrt() {
        this.zzfgg.onRewardedVideoCompleted();
    }
}
