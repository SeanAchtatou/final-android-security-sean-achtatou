package com.google.android.gms.internal.ads;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzay extends ByteArrayOutputStream {
    private final zzaj zzbw;

    public zzay(zzaj zzaj, int i2) {
        this.zzbw = zzaj;
        this.buf = this.zzbw.zzc(Math.max(i2, 256));
    }

    private final void zzd(int i2) {
        int i3 = this.count;
        if (i3 + i2 > this.buf.length) {
            byte[] zzc = this.zzbw.zzc((i3 + i2) << 1);
            System.arraycopy(this.buf, 0, zzc, 0, this.count);
            this.zzbw.zza(this.buf);
            this.buf = zzc;
        }
    }

    public final void close() throws IOException {
        this.zzbw.zza(this.buf);
        this.buf = null;
        super.close();
    }

    public final void finalize() {
        this.zzbw.zza(this.buf);
    }

    public final synchronized void write(byte[] bArr, int i2, int i3) {
        zzd(i3);
        super.write(bArr, i2, i3);
    }

    public final synchronized void write(int i2) {
        zzd(1);
        super.write(i2);
    }
}
