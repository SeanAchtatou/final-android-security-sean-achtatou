package com.google.android.gms.internal.ads;

import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbpw extends zzbrl<zzbqb> implements zzbqb {
    public zzbpw(Set<zzbsu<zzbqb>> set) {
        super(set);
    }

    public final void onAdLoaded() {
        zza(zzbpz.zzfhp);
    }
}
