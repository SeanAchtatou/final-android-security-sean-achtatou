package com.google.android.gms.internal.ads;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbgd implements zzdxg<Context> {
    private final zzbga zzejr;

    public zzbgd(zzbga zzbga) {
        this.zzejr = zzbga;
    }

    public static Context zza(zzbga zzbga) {
        return (Context) zzdxm.zza(zzbga.zzacp(), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        return zza(this.zzejr);
    }
}
