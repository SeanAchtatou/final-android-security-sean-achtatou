package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbgh implements zzdxg<zzcis<zzdac, zzcjx>> {
    private final zzbga zzejr;
    private final zzdxp<zzcka> zzejs;

    public zzbgh(zzbga zzbga, zzdxp<zzcka> zzdxp) {
        this.zzejr = zzbga;
        this.zzejs = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return (zzcis) zzdxm.zza(new zzcmf(this.zzejs.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
