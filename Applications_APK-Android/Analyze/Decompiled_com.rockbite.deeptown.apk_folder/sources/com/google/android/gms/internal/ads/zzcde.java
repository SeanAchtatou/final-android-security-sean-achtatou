package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzso;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcde implements zzdxg<zzcdh> {
    private static final zzcde zzfsf = new zzcde();

    public static zzcde zzale() {
        return zzfsf;
    }

    public final /* synthetic */ Object get() {
        return (zzcdh) zzdxm.zza(new zzcdh(zzso.zza.C0151zza.REQUEST_WILL_MAKE_NETWORK_REQUEST, zzso.zza.C0151zza.REQUEST_DID_RECEIVE_NETWORK_RESPONSE, zzso.zza.C0151zza.REQUEST_FAILED_TO_MAKE_NETWORK_REQUEST), "Cannot return null from a non-@Nullable @Provides method");
    }
}
