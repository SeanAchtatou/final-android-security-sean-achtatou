package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.ads.rewarded.RewardItem;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzatc extends zzase {
    private final String type;
    private final int zzdno;

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public zzatc(zzasd zzasd) {
        this(zzasd != null ? zzasd.type : "", zzasd != null ? zzasd.zzdno : 1);
    }

    public final int getAmount() throws RemoteException {
        return this.zzdno;
    }

    public final String getType() throws RemoteException {
        return this.type;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public zzatc(RewardItem rewardItem) {
        this(rewardItem != null ? rewardItem.getType() : "", rewardItem != null ? rewardItem.getAmount() : 1);
    }

    public zzatc(String str, int i2) {
        this.type = str;
        this.zzdno = i2;
    }
}
