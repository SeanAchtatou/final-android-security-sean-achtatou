package com.google.android.gms.internal.ads;

import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcoy implements zzbow {
    private final zzcop zzgdq;
    private final zzahh zzgdr;

    zzcoy(zzcop zzcop, zzahh zzahh) {
        this.zzgdq = zzcop;
        this.zzgdr = zzahh;
    }

    public final void onAdFailedToLoad(int i2) {
        zzcop zzcop = this.zzgdq;
        zzahh zzahh = this.zzgdr;
        zzcop.onAdFailedToLoad(i2);
        if (zzahh != null) {
            try {
                zzahh.onInstreamAdFailedToLoad(i2);
            } catch (RemoteException e2) {
                zzayu.zze("#007 Could not call remote method.", e2);
            }
        }
    }
}
