package com.google.android.gms.internal.ads;

import android.content.pm.ApplicationInfo;
import android.location.Location;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzakp {
    zzdhe<Location> zza(ApplicationInfo applicationInfo);
}
