package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import com.google.android.gms.ads.internal.zzq;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcpv implements zzcty<Bundle> {
    private final zzuj zzblm;
    private final List<Parcelable> zzgel;
    private final Context zzup;

    public zzcpv(Context context, zzuj zzuj, List<Parcelable> list) {
        this.zzup = context;
        this.zzblm = zzuj;
        this.zzgel = list;
    }

    public final /* synthetic */ void zzr(Object obj) {
        Bundle bundle = (Bundle) obj;
        if (zzabb.zzctv.get().booleanValue()) {
            Bundle bundle2 = new Bundle();
            zzq.zzkq();
            bundle2.putString("activity", zzawb.zzat(this.zzup));
            Bundle bundle3 = new Bundle();
            bundle3.putInt("width", this.zzblm.width);
            bundle3.putInt("height", this.zzblm.height);
            bundle2.putBundle("size", bundle3);
            if (this.zzgel.size() > 0) {
                List<Parcelable> list = this.zzgel;
                bundle2.putParcelableArray("parents", (Parcelable[]) list.toArray(new Parcelable[list.size()]));
            }
            bundle.putBundle("view_hierarchy", bundle2);
        }
    }
}
