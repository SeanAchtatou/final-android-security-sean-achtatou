package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbyy implements zzdgf {
    private final String zzcyr;
    private final zzbyu zzfpl;

    zzbyy(zzbyu zzbyu, String str) {
        this.zzfpl = zzbyu;
        this.zzcyr = str;
    }

    public final zzdhe zzf(Object obj) {
        return this.zzfpl.zzb(this.zzcyr, obj);
    }
}
