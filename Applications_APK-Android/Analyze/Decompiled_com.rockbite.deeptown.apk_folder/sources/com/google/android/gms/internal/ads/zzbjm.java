package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbjm implements zzdxg<zzakh> {
    private final zzdxp<zzakc> zzfdf;

    private zzbjm(zzdxp<zzakc> zzdxp) {
        this.zzfdf = zzdxp;
    }

    public static zzbjm zzb(zzdxp<zzakc> zzdxp) {
        return new zzbjm(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return (zzakh) zzdxm.zza(this.zzfdf.get().zzsh(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
