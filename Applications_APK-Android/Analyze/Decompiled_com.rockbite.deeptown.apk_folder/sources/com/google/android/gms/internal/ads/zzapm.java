package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final /* synthetic */ class zzapm implements Runnable {
    private final String zzcyr;
    private final zzayy zzdie;

    zzapm(zzayy zzayy, String str) {
        this.zzdie = zzayy;
        this.zzcyr = str;
    }

    public final void run() {
        this.zzdie.zzen(this.zzcyr);
    }
}
