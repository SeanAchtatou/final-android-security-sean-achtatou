package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.os.Environment;
import android.os.StatFs;
import android.view.View;

@TargetApi(18)
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public class zzawl extends zzawi {
    public boolean isAttachedToWindow(View view) {
        return super.isAttachedToWindow(view) || view.getWindowId() != null;
    }

    public final int zzwo() {
        return 14;
    }

    public final long zzws() {
        if (!((Boolean) zzve.zzoy().zzd(zzzn.zzclt)).booleanValue()) {
            return -1;
        }
        return new StatFs(Environment.getDataDirectory().getAbsolutePath()).getAvailableBytes() / 1024;
    }
}
