package com.google.android.gms.internal.ads;

import com.google.android.gms.common.util.Clock;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzczj {
    private final Object lock = new Object();
    private final Clock zzbmq;
    private volatile long zzdrd = 0;
    private volatile int zzglg = zzczi.zzglc;

    public zzczj(Clock clock) {
        this.zzbmq = clock;
    }

    private final void zzaol() {
        long currentTimeMillis = this.zzbmq.currentTimeMillis();
        synchronized (this.lock) {
            if (this.zzglg == zzczi.zzgle) {
                if (this.zzdrd + ((Long) zzve.zzoy().zzd(zzzn.zzcpq)).longValue() <= currentTimeMillis) {
                    this.zzglg = zzczi.zzglc;
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001d, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void zzr(int r5, int r6) {
        /*
            r4 = this;
            r4.zzaol()
            com.google.android.gms.common.util.Clock r0 = r4.zzbmq
            long r0 = r0.currentTimeMillis()
            java.lang.Object r2 = r4.lock
            monitor-enter(r2)
            int r3 = r4.zzglg     // Catch:{ all -> 0x001e }
            if (r3 == r5) goto L_0x0012
            monitor-exit(r2)     // Catch:{ all -> 0x001e }
            return
        L_0x0012:
            r4.zzglg = r6     // Catch:{ all -> 0x001e }
            int r5 = r4.zzglg     // Catch:{ all -> 0x001e }
            int r6 = com.google.android.gms.internal.ads.zzczi.zzgle     // Catch:{ all -> 0x001e }
            if (r5 != r6) goto L_0x001c
            r4.zzdrd = r0     // Catch:{ all -> 0x001e }
        L_0x001c:
            monitor-exit(r2)     // Catch:{ all -> 0x001e }
            return
        L_0x001e:
            r5 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x001e }
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzczj.zzr(int, int):void");
    }

    public final boolean zzaom() {
        boolean z;
        synchronized (this.lock) {
            zzaol();
            z = this.zzglg == zzczi.zzgld;
        }
        return z;
    }

    public final boolean zzaon() {
        boolean z;
        synchronized (this.lock) {
            zzaol();
            z = this.zzglg == zzczi.zzgle;
        }
        return z;
    }

    public final void zzbl(boolean z) {
        if (z) {
            zzr(zzczi.zzglc, zzczi.zzgld);
        } else {
            zzr(zzczi.zzgld, zzczi.zzglc);
        }
    }

    public final void zzvb() {
        zzr(zzczi.zzgld, zzczi.zzgle);
    }
}
