package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzckz implements zzcir<zzbwk, zzdac, zzcjy> {
    private final Executor zzfci;
    private final zzbvm zzfzt;
    private final Context zzup;

    public zzckz(Context context, zzbvm zzbvm, Executor executor) {
        this.zzup = context;
        this.zzfzt = zzbvm;
        this.zzfci = executor;
    }

    public final void zza(zzczt zzczt, zzczl zzczl, zzcip<zzdac, zzcjy> zzcip) throws zzdab {
        zzczu zzczu = zzczt.zzgmh.zzfgl;
        ((zzdac) zzcip.zzddn).zza(this.zzup, zzczt.zzgmh.zzfgl.zzgml, zzczl.zzglr.toString(), zzaxs.zza(zzczl.zzglo), (zzali) zzcip.zzfyf, zzczu.zzddz, zzczu.zzgmn);
    }

    public final /* synthetic */ Object zzb(zzczt zzczt, zzczl zzczl, zzcip zzcip) throws zzdab, zzclr {
        zzbws zzbws;
        zzall zzsl = ((zzdac) zzcip.zzddn).zzsl();
        zzalq zzsm = ((zzdac) zzcip.zzddn).zzsm();
        zzalr zzsr = ((zzdac) zzcip.zzddn).zzsr();
        if (zzsr != null && zza(zzczt, 6)) {
            zzbws = zzbws.zzb(zzsr);
        } else if (zzsl != null && zza(zzczt, 6)) {
            zzbws = zzbws.zzb(zzsl);
        } else if (zzsl != null && zza(zzczt, 2)) {
            zzbws = zzbws.zza(zzsl);
        } else if (zzsm != null && zza(zzczt, 6)) {
            zzbws = zzbws.zzb(zzsm);
        } else if (zzsm == null || !zza(zzczt, 1)) {
            throw new zzclr("No native ad mappers", 0);
        } else {
            zzbws = zzbws.zza(zzsm);
        }
        if (zzczt.zzgmh.zzfgl.zzgmn.contains(Integer.toString(zzbws.zzaja()))) {
            zzbwt zza = this.zzfzt.zza(new zzbmt(zzczt, zzczl, zzcip.zzfge), new zzbxe(zzbws), new zzbyg(zzsm, zzsl, zzsr));
            ((zzcjy) zzcip.zzfyf).zza(zza.zzadm());
            zza.zzadh().zza(new zzbiu((zzdac) zzcip.zzddn), this.zzfci);
            return zza.zzadn();
        }
        throw new zzclr("No corresponding native ad listener", 0);
    }

    private static boolean zza(zzczt zzczt, int i2) {
        return zzczt.zzgmh.zzfgl.zzgmn.contains(Integer.toString(i2));
    }
}
