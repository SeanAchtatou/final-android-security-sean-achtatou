package com.google.android.gms.internal.ads;

import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbrq implements zzdxg<zzbou> {
    private final zzbrm zzfim;
    private final zzdxp<Set<zzbsu<zzbow>>> zzfin;

    private zzbrq(zzbrm zzbrm, zzdxp<Set<zzbsu<zzbow>>> zzdxp) {
        this.zzfim = zzbrm;
        this.zzfin = zzdxp;
    }

    public static zzbrq zza(zzbrm zzbrm, zzdxp<Set<zzbsu<zzbow>>> zzdxp) {
        return new zzbrq(zzbrm, zzdxp);
    }

    public final /* synthetic */ Object get() {
        return (zzbou) zzdxm.zza(this.zzfim.zzc(this.zzfin.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
