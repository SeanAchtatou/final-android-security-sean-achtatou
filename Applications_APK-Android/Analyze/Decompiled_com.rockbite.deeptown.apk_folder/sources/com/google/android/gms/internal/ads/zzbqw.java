package com.google.android.gms.internal.ads;

import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbqw extends zzbrl<zzbrb> implements zzbrb {
    public zzbqw(Set<zzbsu<zzbrb>> set) {
        super(set);
    }

    public final void zzagj() {
        zza(zzbqz.zzfhp);
    }
}
