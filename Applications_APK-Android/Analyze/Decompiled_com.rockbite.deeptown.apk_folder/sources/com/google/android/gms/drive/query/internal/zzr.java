package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.drive.query.Filter;
import java.util.ArrayList;
import java.util.List;

@SafeParcelable.Class(creator = "LogicalFilterCreator")
@SafeParcelable.Reserved({1000})
public final class zzr extends zza {
    public static final Parcelable.Creator<zzr> CREATOR = new zzs();
    private List<Filter> zzls;
    @SafeParcelable.Field(id = 1)
    private final zzx zzlz;
    @SafeParcelable.Field(id = 2)
    private final List<FilterHolder> zzmo;

    @SafeParcelable.Constructor
    zzr(@SafeParcelable.Param(id = 1) zzx zzx, @SafeParcelable.Param(id = 2) List<FilterHolder> list) {
        this.zzlz = zzx;
        this.zzmo = list;
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeParcelable(parcel, 1, this.zzlz, i2, false);
        SafeParcelWriter.writeTypedList(parcel, 2, this.zzmo, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }

    public final <T> T zza(zzj<T> zzj) {
        ArrayList arrayList = new ArrayList();
        for (FilterHolder filter : this.zzmo) {
            arrayList.add(filter.getFilter().zza(zzj));
        }
        return zzj.zza(this.zzlz, arrayList);
    }

    public zzr(zzx zzx, Iterable<Filter> iterable) {
        this.zzlz = zzx;
        this.zzls = new ArrayList();
        this.zzmo = new ArrayList();
        for (Filter next : iterable) {
            this.zzls.add(next);
            this.zzmo.add(new FilterHolder(next));
        }
    }

    public zzr(zzx zzx, Filter filter, Filter... filterArr) {
        this.zzlz = zzx;
        this.zzmo = new ArrayList(filterArr.length + 1);
        this.zzmo.add(new FilterHolder(filter));
        this.zzls = new ArrayList(filterArr.length + 1);
        this.zzls.add(filter);
        for (Filter filter2 : filterArr) {
            this.zzmo.add(new FilterHolder(filter2));
            this.zzls.add(filter2);
        }
    }
}
