package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zzq;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzafp implements zzafn<Object> {
    private final Context zzup;

    public zzafp(Context context) {
        this.zzup = context;
    }

    public final void zza(Object obj, Map<String, String> map) {
        if (zzq.zzlo().zzab(this.zzup)) {
            String str = map.get("eventName");
            String str2 = map.get("eventId");
            char c2 = 65535;
            int hashCode = str.hashCode();
            if (hashCode != 94399) {
                if (hashCode != 94401) {
                    if (hashCode == 94407 && str.equals("_ai")) {
                        c2 = 1;
                    }
                } else if (str.equals("_ac")) {
                    c2 = 0;
                }
            } else if (str.equals("_aa")) {
                c2 = 2;
            }
            if (c2 == 0) {
                zzq.zzlo().zzh(this.zzup, str2);
            } else if (c2 == 1) {
                zzq.zzlo().zzi(this.zzup, str2);
            } else if (c2 != 2) {
                zzayu.zzex("logScionEvent gmsg contained unsupported eventName");
            } else {
                zzq.zzlo().zzk(this.zzup, str2);
            }
        }
    }
}
