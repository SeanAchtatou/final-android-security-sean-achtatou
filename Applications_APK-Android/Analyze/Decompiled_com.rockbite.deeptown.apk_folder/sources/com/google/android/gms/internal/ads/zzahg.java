package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface zzahg extends IInterface {
    void zzcn(int i2) throws RemoteException;

    void zzrv() throws RemoteException;
}
