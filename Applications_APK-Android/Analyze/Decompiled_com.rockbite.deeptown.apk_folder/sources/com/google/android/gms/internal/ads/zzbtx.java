package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbtx implements zzbrb {
    private final zzbdi zzehp;

    zzbtx(zzbdi zzbdi) {
        this.zzehp = zzbdi;
    }

    public final void zzagj() {
        zzbdi zzbdi = this.zzehp;
        if (zzbdi.zzzw() != null) {
            zzbdi.zzzw().close();
        }
    }
}
