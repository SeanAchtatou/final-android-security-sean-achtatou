package com.google.android.gms.internal.ads;

import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbsq extends zzbrl<zzbsn> {
    public zzbsq(Set<zzbsu<zzbsn>> set) {
        super(set);
    }

    public final void onHide() {
        zza(zzbss.zzfhp);
    }

    public final void zzahy() {
        zza(zzbsp.zzfhp);
    }
}
