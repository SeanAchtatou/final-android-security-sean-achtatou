package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzciw implements zzbpe {
    private final zzbdi zzehp;

    zzciw(zzbdi zzbdi) {
        this.zzehp = zzbdi;
    }

    public final void onAdImpression() {
        zzbdi zzbdi = this.zzehp;
        if (zzbdi.zzaaa() != null) {
            zzbdi.zzaaa().zzaaz();
        }
    }
}
