package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcji implements zzbme {
    private final zzdac zzfyp;

    private zzcji(zzdac zzdac) {
        this.zzfyp = zzdac;
    }

    static zzbme zza(zzdac zzdac) {
        return new zzcji(zzdac);
    }

    public final zzxb getVideoController() {
        return this.zzfyp.getVideoController();
    }
}
