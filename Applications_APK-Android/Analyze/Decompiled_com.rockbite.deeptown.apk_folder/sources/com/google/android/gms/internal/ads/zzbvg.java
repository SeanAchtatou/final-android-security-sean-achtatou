package com.google.android.gms.internal.ads;

import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzbvg implements zzbmi<zzbmj> {
    private final zzbwz zzfea;
    private final Map<String, zzcio<zzbmj>> zzfft;
    private final zzdxp<zzbmi<zzbkk>> zzfjs;
    private final Map<String, zzcio<zzbwk>> zzfjt;
    private final Map<String, zzckr<zzbwk>> zzfju;

    zzbvg(Map<String, zzcio<zzbmj>> map, Map<String, zzcio<zzbwk>> map2, Map<String, zzckr<zzbwk>> map3, zzdxp<zzbmi<zzbkk>> zzdxp, zzbwz zzbwz) {
        this.zzfft = map;
        this.zzfjt = map2;
        this.zzfju = map3;
        this.zzfjs = zzdxp;
        this.zzfea = zzbwz;
    }

    public final zzcio<zzbmj> zzd(int i2, String str) {
        zzcio zzd;
        zzcio<zzbmj> zzcio = this.zzfft.get(str);
        if (zzcio != null) {
            return zzcio;
        }
        if (i2 != 1) {
            if (i2 != 4) {
                return null;
            }
            zzckr zzckr = this.zzfju.get(str);
            if (zzckr != null) {
                return zzbmj.zza((zzckr<? extends zzbmd>) zzckr);
            }
            zzcio zzcio2 = this.zzfjt.get(str);
            if (zzcio2 != null) {
                return zzbmj.zza(zzcio2);
            }
            return null;
        } else if (this.zzfea.zzajs() == null || (zzd = this.zzfjs.get().zzd(i2, str)) == null) {
            return null;
        } else {
            return zzbmj.zza(zzd);
        }
    }
}
