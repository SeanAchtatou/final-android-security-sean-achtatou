package com.google.android.gms.internal.ads;

import java.util.concurrent.ThreadFactory;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdbz implements zzdxg<ThreadFactory> {
    private static final zzdbz zzgpw = new zzdbz();

    public static zzdbz zzaqc() {
        return zzgpw;
    }

    public final /* synthetic */ Object get() {
        return (ThreadFactory) zzdxm.zza(new zzdbr(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
