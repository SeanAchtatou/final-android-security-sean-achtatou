package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.TimeUnit;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcfq implements zzdxg<zzdhe<String>> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzdcr> zzfet;

    private zzcfq(zzdxp<zzdcr> zzdxp, zzdxp<Context> zzdxp2) {
        this.zzfet = zzdxp;
        this.zzejv = zzdxp2;
    }

    public static zzcfq zzad(zzdxp<zzdcr> zzdxp, zzdxp<Context> zzdxp2) {
        return new zzcfq(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return (zzdhe) zzdxm.zza(this.zzfet.get().zzu(zzdco.WEBVIEW_COOKIE).zzc(new zzcfh(this.zzejv.get())).zza(1, TimeUnit.SECONDS).zza(Exception.class, zzcfk.zzfun).zzaqg(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
