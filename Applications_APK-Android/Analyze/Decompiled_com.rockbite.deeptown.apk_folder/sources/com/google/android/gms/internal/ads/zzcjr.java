package com.google.android.gms.internal.ads;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcjr implements zzcio<zzbkk> {
    private final ScheduledExecutorService zzffx;
    /* access modifiers changed from: private */
    public final zzbou zzfik;
    private final zzdhd zzfov;
    private final zzblg zzfyy;
    private final zzcix zzfyz;

    public zzcjr(zzblg zzblg, zzcix zzcix, zzbou zzbou, ScheduledExecutorService scheduledExecutorService, zzdhd zzdhd) {
        this.zzfyy = zzblg;
        this.zzfyz = zzcix;
        this.zzfik = zzbou;
        this.zzffx = scheduledExecutorService;
        this.zzfov = zzdhd;
    }

    public final boolean zza(zzczt zzczt, zzczl zzczl) {
        return zzczt.zzgmh.zzfgl.zzaoo() != null && this.zzfyz.zza(zzczt, zzczl);
    }

    public final zzdhe<zzbkk> zzb(zzczt zzczt, zzczl zzczl) {
        return this.zzfov.zzd(new zzcju(this, zzczt, zzczl));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzbkk zzc(zzczt zzczt, zzczl zzczl) throws Exception {
        return this.zzfyy.zza(new zzbmt(zzczt, zzczl, null), new zzbls(zzczt.zzgmh.zzfgl.zzaoo(), new zzcjt(this, zzczt, zzczl))).zzaef();
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzd(zzczt zzczt, zzczl zzczl) {
        zzdgs.zza(zzdgs.zza(this.zzfyz.zzb(zzczt, zzczl), (long) zzczl.zzglx, TimeUnit.SECONDS, this.zzffx), new zzcjw(this), this.zzfov);
    }
}
