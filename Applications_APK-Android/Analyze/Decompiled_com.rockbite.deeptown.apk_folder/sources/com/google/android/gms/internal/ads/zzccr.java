package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzso;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzccr implements zzdxg<zzcdh> {
    private static final zzccr zzfrv = new zzccr();

    public static zzccr zzaky() {
        return zzfrv;
    }

    public final /* synthetic */ Object get() {
        return (zzcdh) zzdxm.zza(new zzcdh(zzso.zza.C0151zza.REQUEST_WILL_RENDER, zzso.zza.C0151zza.REQUEST_DID_RENDER, zzso.zza.C0151zza.REQUEST_FAILED_TO_RENDER), "Cannot return null from a non-@Nullable @Provides method");
    }
}
