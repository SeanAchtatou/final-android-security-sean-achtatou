package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.ads.zzbod;
import com.google.android.gms.internal.ads.zzbrm;
import java.util.Collections;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcoo extends zzvt implements zzbqt {
    /* access modifiers changed from: private */
    public final ViewGroup zzfdu;
    private final zzbfx zzfzz;
    /* access modifiers changed from: private */
    public zzbkk zzgcp;
    private final Context zzgcr;
    private final zzczw zzgcs = new zzczw();
    private final zzcop zzgcw = new zzcop();
    private final zzcoq zzgcx = new zzcoq();
    private final zzcos zzgcy = new zzcos();
    /* access modifiers changed from: private */
    public final zzbqp zzgcz;
    private zzaak zzgda;
    /* access modifiers changed from: private */
    public zzdhe<zzbkk> zzgdb;

    public zzcoo(zzbfx zzbfx, Context context, zzuj zzuj, String str) {
        this.zzfdu = new FrameLayout(context);
        this.zzfzz = zzbfx;
        this.zzgcr = context;
        this.zzgcs.zzd(zzuj).zzgk(str);
        this.zzgcz = zzbfx.zzace();
        this.zzgcz.zza(this, this.zzfzz.zzaca());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbow, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
     arg types: [com.google.android.gms.internal.ads.zzcop, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.ads.doubleclick.AppEventListener, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.ads.reward.AdMetadataListener, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbov, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbpa, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbpe, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbqb, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbqg, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzty, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzwc, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbow, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbqb, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
     arg types: [com.google.android.gms.internal.ads.zzcop, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.ads.doubleclick.AppEventListener, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.ads.reward.AdMetadataListener, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbov, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbow, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbpa, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbpe, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbqg, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzty, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzwc, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbqb, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbov, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
     arg types: [com.google.android.gms.internal.ads.zzcop, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.ads.doubleclick.AppEventListener, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.ads.reward.AdMetadataListener, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbow, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbpa, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbpe, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbqb, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbqg, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzty, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzwc, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbov, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzty, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
     arg types: [com.google.android.gms.internal.ads.zzcop, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.ads.doubleclick.AppEventListener, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.ads.reward.AdMetadataListener, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbov, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbow, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbpa, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbpe, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbqb, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzbqg, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzwc, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza
      com.google.android.gms.internal.ads.zzbrm.zza.zza(com.google.android.gms.internal.ads.zzty, java.util.concurrent.Executor):com.google.android.gms.internal.ads.zzbrm$zza */
    private final synchronized zzblg zzb(zzczu zzczu) {
        return this.zzfzz.zzach().zzc(new zzbod.zza().zzbz(this.zzgcr).zza(zzczu).zzahh()).zzc(new zzbrm.zza().zza((zzty) this.zzgcw, this.zzfzz.zzaca()).zza(this.zzgcx, this.zzfzz.zzaca()).zza((zzbov) this.zzgcw, this.zzfzz.zzaca()).zza((zzbqb) this.zzgcw, this.zzfzz.zzaca()).zza((zzbow) this.zzgcw, this.zzfzz.zzaca()).zza(this.zzgcy, this.zzfzz.zzaca()).zzahw()).zza(new zzcns(this.zzgda)).zzb(new zzbvi(zzbwz.zzfmx, null)).zza(new zzbma(this.zzgcz)).zzb(new zzbkf(this.zzfdu)).zzaee();
    }

    public final synchronized void destroy() {
        Preconditions.checkMainThread("destroy must be called on the main UI thread.");
        if (this.zzgcp != null) {
            this.zzgcp.destroy();
        }
    }

    public final Bundle getAdMetadata() {
        Preconditions.checkMainThread("getAdMetadata must be called on the main UI thread.");
        return new Bundle();
    }

    public final synchronized String getAdUnitId() {
        return this.zzgcs.zzaor();
    }

    public final synchronized String getMediationAdapterClassName() {
        if (this.zzgcp == null || this.zzgcp.zzags() == null) {
            return null;
        }
        return this.zzgcp.zzags().getMediationAdapterClassName();
    }

    public final synchronized zzxb getVideoController() {
        Preconditions.checkMainThread("getVideoController must be called from the main thread.");
        if (this.zzgcp == null) {
            return null;
        }
        return this.zzgcp.getVideoController();
    }

    public final synchronized boolean isLoading() {
        return this.zzgdb != null && !this.zzgdb.isDone();
    }

    public final boolean isReady() {
        return false;
    }

    public final synchronized void pause() {
        Preconditions.checkMainThread("pause must be called on the main UI thread.");
        if (this.zzgcp != null) {
            this.zzgcp.zzagr().zzbv(null);
        }
    }

    public final synchronized void resume() {
        Preconditions.checkMainThread("resume must be called on the main UI thread.");
        if (this.zzgcp != null) {
            this.zzgcp.zzagr().zzbw(null);
        }
    }

    public final void setImmersiveMode(boolean z) {
    }

    public final synchronized void setManualImpressionsEnabled(boolean z) {
        Preconditions.checkMainThread("setManualImpressionsEnabled must be called from the main thread.");
        this.zzgcs.zzbm(z);
    }

    public final void setUserId(String str) {
    }

    public final void showInterstitial() {
    }

    public final void stopLoading() {
    }

    public final void zza(zzaoy zzaoy) {
    }

    public final void zza(zzape zzape, String str) {
    }

    public final void zza(zzaro zzaro) {
    }

    public final void zza(zzrg zzrg) {
    }

    public final void zza(zzuo zzuo) {
    }

    public final void zza(zzxh zzxh) {
    }

    public final synchronized boolean zza(zzug zzug) {
        Preconditions.checkMainThread("loadAd must be called on the main UI thread.");
        if (this.zzgdb != null) {
            return false;
        }
        zzdad.zze(this.zzgcr, zzug.zzccb);
        zzczu zzaos = this.zzgcs.zzg(zzug).zzaos();
        if (!zzabe.zzcub.get().booleanValue() || !this.zzgcs.zzjz().zzccs || this.zzgcw == null) {
            zzblg zzb = zzb(zzaos);
            this.zzgdb = zzb.zzadc().zzaha();
            zzdgs.zza(this.zzgdb, new zzcon(this, zzb), this.zzfzz.zzaca());
            return true;
        }
        this.zzgcw.onAdFailedToLoad(1);
        return false;
    }

    public final synchronized void zzahl() {
        boolean z;
        ViewParent parent = this.zzfdu.getParent();
        if (!(parent instanceof View)) {
            z = false;
        } else {
            View view = (View) parent;
            z = zzq.zzkq().zza(view, view.getContext());
        }
        if (z) {
            zza(this.zzgcs.zzaoq());
        } else {
            this.zzgcz.zzdg(60);
        }
    }

    public final void zzbr(String str) {
    }

    public final IObjectWrapper zzjx() {
        Preconditions.checkMainThread("destroy must be called on the main UI thread.");
        return ObjectWrapper.wrap(this.zzfdu);
    }

    public final synchronized void zzjy() {
        Preconditions.checkMainThread("recordManualImpression must be called on the main UI thread.");
        if (this.zzgcp != null) {
            this.zzgcp.zzjy();
        }
    }

    public final synchronized zzuj zzjz() {
        Preconditions.checkMainThread("getAdSize must be called on the main UI thread.");
        if (this.zzgcp != null) {
            return zzczy.zza(this.zzgcr, Collections.singletonList(this.zzgcp.zzafz()));
        }
        return this.zzgcs.zzjz();
    }

    public final synchronized String zzka() {
        if (this.zzgcp == null || this.zzgcp.zzags() == null) {
            return null;
        }
        return this.zzgcp.zzags().getMediationAdapterClassName();
    }

    public final synchronized zzxa zzkb() {
        if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcrf)).booleanValue()) {
            return null;
        }
        if (this.zzgcp == null) {
            return null;
        }
        return this.zzgcp.zzags();
    }

    public final zzwc zzkc() {
        return this.zzgcy.zzamq();
    }

    public final zzvh zzkd() {
        return this.zzgcw.zzamo();
    }

    public final void zza(zzvh zzvh) {
        Preconditions.checkMainThread("setAdListener must be called on the main UI thread.");
        this.zzgcw.zzc(zzvh);
    }

    public final void zza(zzwc zzwc) {
        Preconditions.checkMainThread("setAppEventListener must be called on the main UI thread.");
        this.zzgcy.zzb(zzwc);
    }

    public final synchronized void zza(zzuj zzuj) {
        Preconditions.checkMainThread("setAdSize must be called on the main UI thread.");
        this.zzgcs.zzd(zzuj);
        if (this.zzgcp != null) {
            this.zzgcp.zza(this.zzfdu, zzuj);
        }
    }

    public final synchronized void zza(zzwi zzwi) {
        Preconditions.checkMainThread("setCorrelationIdProvider must be called on the main UI thread");
        this.zzgcs.zzc(zzwi);
    }

    public final synchronized void zza(zzyw zzyw) {
        Preconditions.checkMainThread("setVideoOptions must be called on the main UI thread.");
        this.zzgcs.zzc(zzyw);
    }

    public final void zza(zzvg zzvg) {
        Preconditions.checkMainThread("setAdListener must be called on the main UI thread.");
        this.zzgcx.zzb(zzvg);
    }

    public final synchronized void zza(zzaak zzaak) {
        Preconditions.checkMainThread("setOnCustomRenderedAdLoadedListener must be called on the main UI thread.");
        this.zzgda = zzaak;
    }

    public final void zza(zzvx zzvx) {
        Preconditions.checkMainThread("setAdMetadataListener must be called on the main UI thread.");
    }
}
