package com.google.android.gms.internal.ads;

import com.moat.analytics.mobile.cha.BuildConfig;
import com.tapjoy.TapjoyConstants;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzabi {
    public static zzaan<Long> zzcuj = zzaan.zzb("gads:dynamite_load:fail:sample_rate", TapjoyConstants.TIMER_INCREMENT);
    public static zzaan<Boolean> zzcuk = zzaan.zzf("gads:report_dynamite_crash_in_background_thread", false);
    public static zzaan<String> zzcul = zzaan.zzi("gads:public_beta:traffic_multiplier", BuildConfig.VERSION_NAME);
    public static zzaan<String> zzcum = zzaan.zzi("gads:sdk_crash_report_class_prefix", "com.google.");
    public static zzaan<Boolean> zzcun = zzaan.zzf("gads:sdk_crash_report_enabled", false);
    public static zzaan<Boolean> zzcuo = zzaan.zzf("gads:sdk_crash_report_full_stacktrace", false);
    public static zzaan<Double> zzcup = new zzaan<>("gads:trapped_exception_sample_rate", Double.valueOf(0.01d), zzaap.zzcsj);
}
