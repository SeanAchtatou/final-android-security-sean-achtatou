package com.google.android.gms.internal.ads;

import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public abstract class zzcut {
    public abstract zzcua<JSONObject> zzadt();

    public abstract zzcua<JSONObject> zzadu();

    public abstract zzdcr zzadv();
}
