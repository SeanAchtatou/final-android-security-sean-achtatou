package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface zzast extends IInterface {
    void zza(zzasf zzasf, String str, String str2) throws RemoteException;
}
