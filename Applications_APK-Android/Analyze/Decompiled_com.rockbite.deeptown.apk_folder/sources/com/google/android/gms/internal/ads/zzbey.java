package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbey {
    public final int heightPixels;
    private final int type;
    public final int widthPixels;

    private zzbey(int i2, int i3, int i4) {
        this.type = i2;
        this.widthPixels = i3;
        this.heightPixels = i4;
    }

    public static zzbey zzabq() {
        return new zzbey(0, 0, 0);
    }

    public static zzbey zzabr() {
        return new zzbey(4, 0, 0);
    }

    public static zzbey zzabs() {
        return new zzbey(5, 0, 0);
    }

    public static zzbey zzb(zzuj zzuj) {
        if (zzuj.zzcco) {
            return new zzbey(3, 0, 0);
        }
        if (zzuj.zzccq) {
            return new zzbey(2, 0, 0);
        }
        if (zzuj.zzbmc) {
            return zzabq();
        }
        return zzq(zzuj.widthPixels, zzuj.heightPixels);
    }

    public static zzbey zzq(int i2, int i3) {
        return new zzbey(1, i2, i3);
    }

    public final boolean isFluid() {
        return this.type == 2;
    }

    public final boolean zzabt() {
        return this.type == 3;
    }

    public final boolean zzabu() {
        return this.type == 0;
    }

    public final boolean zzabv() {
        return this.type == 4;
    }

    public final boolean zzabw() {
        return this.type == 5;
    }
}
