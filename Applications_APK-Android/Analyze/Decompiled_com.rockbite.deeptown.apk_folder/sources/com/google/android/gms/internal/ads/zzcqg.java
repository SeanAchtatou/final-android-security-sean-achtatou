package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcqg implements zzded {
    private final zzcqh zzges;

    zzcqg(zzcqh zzcqh) {
        this.zzges = zzcqh;
    }

    public final Object apply(Object obj) {
        return this.zzges.zza((zzcue) obj);
    }
}
