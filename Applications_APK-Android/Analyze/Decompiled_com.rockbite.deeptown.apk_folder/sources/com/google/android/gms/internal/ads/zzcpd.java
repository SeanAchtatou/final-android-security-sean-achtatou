package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzcpd implements zzdgt<zzbmd> {
    private final /* synthetic */ zzcoz zzgdv;
    private final /* synthetic */ zzbvm zzgdw;
    private final /* synthetic */ zzcpb zzgdx;

    zzcpd(zzcpb zzcpb, zzcoz zzcoz, zzbvm zzbvm) {
        this.zzgdx = zzcpb;
        this.zzgdv = zzcoz;
        this.zzgdw = zzbvm;
    }

    public final /* synthetic */ void onSuccess(Object obj) {
        zzbmd zzbmd = (zzbmd) obj;
        synchronized (this.zzgdx) {
            this.zzgdv.onSuccess(zzbmd);
        }
    }

    public final void zzb(Throwable th) {
        this.zzgdw.zzadd().onAdFailedToLoad(zzcfb.zzd(th));
        zzdad.zzc(th, "NativeAdLoader.onFailure");
        this.zzgdv.zzamx();
    }
}
