package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.content.Context;
import android.media.AudioManager;
import com.esotericsoftware.spine.Animation;

@TargetApi(14)
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbba implements AudioManager.OnAudioFocusChangeListener {
    private float zzcy = 1.0f;
    private boolean zzdya;
    private final AudioManager zzeba;
    private final zzbbd zzebb;
    private boolean zzebc;
    private boolean zzebd;

    public zzbba(Context context, zzbbd zzbbd) {
        this.zzeba = (AudioManager) context.getSystemService("audio");
        this.zzebb = zzbbd;
    }

    private final void zzyv() {
        boolean z;
        boolean z2;
        boolean z3 = false;
        boolean z4 = this.zzdya && !this.zzebd && this.zzcy > Animation.CurveTimeline.LINEAR;
        if (z4 && !(z2 = this.zzebc)) {
            AudioManager audioManager = this.zzeba;
            if (audioManager != null && !z2) {
                if (audioManager.requestAudioFocus(this, 3, 2) == 1) {
                    z3 = true;
                }
                this.zzebc = z3;
            }
            this.zzebb.zzxs();
        } else if (!z4 && (z = this.zzebc)) {
            AudioManager audioManager2 = this.zzeba;
            if (audioManager2 != null && z) {
                if (audioManager2.abandonAudioFocus(this) == 0) {
                    z3 = true;
                }
                this.zzebc = z3;
            }
            this.zzebb.zzxs();
        }
    }

    public final float getVolume() {
        float f2 = this.zzebd ? Animation.CurveTimeline.LINEAR : this.zzcy;
        if (this.zzebc) {
            return f2;
        }
        return Animation.CurveTimeline.LINEAR;
    }

    public final void onAudioFocusChange(int i2) {
        this.zzebc = i2 > 0;
        this.zzebb.zzxs();
    }

    public final void setMuted(boolean z) {
        this.zzebd = z;
        zzyv();
    }

    public final void setVolume(float f2) {
        this.zzcy = f2;
        zzyv();
    }

    public final void zzyi() {
        this.zzdya = true;
        zzyv();
    }

    public final void zzyj() {
        this.zzdya = false;
        zzyv();
    }
}
