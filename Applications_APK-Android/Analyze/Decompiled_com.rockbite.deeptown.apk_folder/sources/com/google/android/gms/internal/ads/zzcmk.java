package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcmk implements Runnable {
    private final zzccd zzfzn;

    private zzcmk(zzccd zzccd) {
        this.zzfzn = zzccd;
    }

    static Runnable zza(zzccd zzccd) {
        return new zzcmk(zzccd);
    }

    public final void run() {
        this.zzfzn.zzakx();
    }
}
