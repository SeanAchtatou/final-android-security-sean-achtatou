package com.google.android.gms.internal.ads;

import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcva implements zzcty<JSONObject> {
    private JSONObject zzght;

    public zzcva(JSONObject jSONObject) {
        this.zzght = jSONObject;
    }

    public final /* synthetic */ void zzr(Object obj) {
        try {
            ((JSONObject) obj).put("cache_state", this.zzght);
        } catch (JSONException unused) {
            zzavs.zzed("Unable to get cache_state");
        }
    }
}
