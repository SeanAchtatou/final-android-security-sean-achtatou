package com.google.android.gms.internal.ads;

import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzavu {
    void zza(String str, String str2, boolean z);

    void zzao(boolean z);

    void zzap(boolean z);

    void zzaq(boolean z);

    void zzb(Runnable runnable);

    void zzcp(int i2);

    void zzcq(int i2);

    void zzee(String str);

    void zzef(String str);

    void zzeg(String str);

    void zzeh(String str);

    void zzez(long j2);

    void zzfa(long j2);

    zzqi zzvt();

    boolean zzvu();

    String zzvv();

    boolean zzvw();

    String zzvx();

    boolean zzvy();

    int zzvz();

    zzavf zzwa();

    long zzwb();

    int zzwc();

    long zzwd();

    JSONObject zzwe();

    void zzwf();

    String zzwg();
}
