package com.google.android.gms.games.leaderboard;

import android.util.SparseArray;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.games.zzee;

/* compiled from: com.google.android.gms:play-services-games@@19.0.0 */
public final class ScoreSubmissionData {
    private static final String[] zzox = {"leaderboardId", "playerId", "timeSpan", "hasResult", "rawScore", "formattedScore", "newBest", "scoreTag"};
    private int statusCode;
    private String zzbw;
    private String zznt;
    private SparseArray<Result> zzoy = new SparseArray<>();

    /* compiled from: com.google.android.gms:play-services-games@@19.0.0 */
    public static final class Result {
        public final String formattedScore;
        public final boolean newBest;
        public final long rawScore;
        public final String scoreTag;

        public Result(long j2, String str, String str2, boolean z) {
            this.rawScore = j2;
            this.formattedScore = str;
            this.scoreTag = str2;
            this.newBest = z;
        }

        public final String toString() {
            return Objects.toStringHelper(this).add("RawScore", Long.valueOf(this.rawScore)).add("FormattedScore", this.formattedScore).add("ScoreTag", this.scoreTag).add("NewBest", Boolean.valueOf(this.newBest)).toString();
        }
    }

    public ScoreSubmissionData(DataHolder dataHolder) {
        this.statusCode = dataHolder.getStatusCode();
        int count = dataHolder.getCount();
        Preconditions.checkArgument(count == 3);
        for (int i2 = 0; i2 < count; i2++) {
            int windowIndex = dataHolder.getWindowIndex(i2);
            if (i2 == 0) {
                this.zznt = dataHolder.getString("leaderboardId", i2, windowIndex);
                this.zzbw = dataHolder.getString("playerId", i2, windowIndex);
            }
            if (dataHolder.getBoolean("hasResult", i2, windowIndex)) {
                this.zzoy.put(dataHolder.getInteger("timeSpan", i2, windowIndex), new Result(dataHolder.getLong("rawScore", i2, windowIndex), dataHolder.getString("formattedScore", i2, windowIndex), dataHolder.getString("scoreTag", i2, windowIndex), dataHolder.getBoolean("newBest", i2, windowIndex)));
            }
        }
    }

    public final String getLeaderboardId() {
        return this.zznt;
    }

    public final String getPlayerId() {
        return this.zzbw;
    }

    public final Result getScoreResult(int i2) {
        return this.zzoy.get(i2);
    }

    public final String toString() {
        String str;
        Objects.ToStringHelper add = Objects.toStringHelper(this).add("PlayerId", this.zzbw).add("StatusCode", Integer.valueOf(this.statusCode));
        for (int i2 = 0; i2 < 3; i2++) {
            Result result = this.zzoy.get(i2);
            add.add("TimesSpan", zzee.zzp(i2));
            if (result == null) {
                str = "null";
            } else {
                str = result.toString();
            }
            add.add("Result", str);
        }
        return add.toString();
    }
}
