package com.google.android.gms.internal.ads;

import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzaja implements zzafn<zzajq> {
    private final /* synthetic */ zzaif zzczt;
    private final /* synthetic */ zzais zzczu;
    private final /* synthetic */ zzdq zzczw;
    private final /* synthetic */ zzayd zzczx;

    zzaja(zzais zzais, zzdq zzdq, zzaif zzaif, zzayd zzayd) {
        this.zzczu = zzais;
        this.zzczw = zzdq;
        this.zzczt = zzaif;
        this.zzczx = zzayd;
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzajq zzajq = (zzajq) obj;
        synchronized (this.zzczu.lock) {
            zzayu.zzey("JS Engine is requesting an update");
            if (this.zzczu.status == 0) {
                zzayu.zzey("Starting reload.");
                int unused = this.zzczu.status = 2;
                this.zzczu.zza(this.zzczw);
            }
            this.zzczt.zzb("/requestReload", (zzafn) this.zzczx.get());
        }
    }
}
