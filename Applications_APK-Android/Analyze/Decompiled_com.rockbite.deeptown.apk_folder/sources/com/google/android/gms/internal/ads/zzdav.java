package com.google.android.gms.internal.ads;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdav implements zzdxg<Context> {
    private final zzdaq zzgne;
    private final zzdxp<zzdao> zzgng;

    private zzdav(zzdaq zzdaq, zzdxp<zzdao> zzdxp) {
        this.zzgne = zzdaq;
        this.zzgng = zzdxp;
    }

    public static Context zzb(zzdaq zzdaq, zzdao zzdao) {
        return (Context) zzdxm.zza(zzdao.zzyv, "Cannot return null from a non-@Nullable @Provides method");
    }

    public static zzdav zzc(zzdaq zzdaq, zzdxp<zzdao> zzdxp) {
        return new zzdav(zzdaq, zzdxp);
    }

    public final /* synthetic */ Object get() {
        return zzb(this.zzgne, this.zzgng.get());
    }
}
