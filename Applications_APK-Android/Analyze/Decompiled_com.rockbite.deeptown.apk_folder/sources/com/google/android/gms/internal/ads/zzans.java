package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.ads.mediation.MediationAdLoadCallback;
import com.google.android.gms.ads.mediation.MediationRewardedAd;
import com.google.android.gms.ads.mediation.MediationRewardedAdCallback;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzans implements MediationAdLoadCallback<MediationRewardedAd, MediationRewardedAdCallback> {
    private final /* synthetic */ zzali zzdeo;
    private final /* synthetic */ zzann zzdep;
    private final /* synthetic */ zzand zzdes;

    zzans(zzann zzann, zzand zzand, zzali zzali) {
        this.zzdep = zzann;
        this.zzdes = zzand;
        this.zzdeo = zzali;
    }

    /* access modifiers changed from: private */
    /* renamed from: zza */
    public final MediationRewardedAdCallback onSuccess(MediationRewardedAd mediationRewardedAd) {
        if (mediationRewardedAd == null) {
            zzayu.zzez("Adapter incorrectly returned a null ad. The onFailure() callback should be called if an adapter fails to load an ad.");
            try {
                this.zzdes.zzdl("Adapter returned null.");
                return null;
            } catch (RemoteException e2) {
                zzayu.zzc("", e2);
                return null;
            }
        } else {
            try {
                MediationRewardedAd unused = this.zzdep.zzddr = mediationRewardedAd;
                this.zzdes.zztb();
            } catch (RemoteException e3) {
                zzayu.zzc("", e3);
            }
            return new zzant(this.zzdeo);
        }
    }

    public final void onFailure(String str) {
        try {
            this.zzdes.zzdl(str);
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
        }
    }
}
