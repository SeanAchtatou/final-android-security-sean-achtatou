package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzasy extends zzasr {
    private final RewardedAdLoadCallback zzdob;

    public zzasy(RewardedAdLoadCallback rewardedAdLoadCallback) {
        this.zzdob = rewardedAdLoadCallback;
    }

    public final void onRewardedAdFailedToLoad(int i2) {
        RewardedAdLoadCallback rewardedAdLoadCallback = this.zzdob;
        if (rewardedAdLoadCallback != null) {
            rewardedAdLoadCallback.onRewardedAdFailedToLoad(i2);
        }
    }

    public final void onRewardedAdLoaded() {
        RewardedAdLoadCallback rewardedAdLoadCallback = this.zzdob;
        if (rewardedAdLoadCallback != null) {
            rewardedAdLoadCallback.onRewardedAdLoaded();
        }
    }
}
