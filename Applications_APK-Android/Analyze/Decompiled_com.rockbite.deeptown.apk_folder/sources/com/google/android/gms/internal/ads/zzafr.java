package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.View;
import com.appsflyer.share.Constants;
import com.google.android.gms.ads.internal.overlay.zzd;
import com.google.android.gms.ads.internal.zzc;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.ads.zzbei;
import com.google.android.gms.internal.ads.zzbel;
import com.google.android.gms.internal.ads.zzbep;
import com.google.android.gms.internal.ads.zzbeq;
import com.google.android.gms.internal.ads.zzbes;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TapjoyConstants;
import java.net.URISyntaxException;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzafr<T extends zzbei & zzbel & zzbep & zzbeq & zzbes> implements zzafn<T> {
    private final zzc zzcxm;
    private final zzaoe zzcxn;

    public zzafr(zzc zzc, zzaoe zzaoe) {
        this.zzcxm = zzc;
        this.zzcxn = zzaoe;
    }

    @VisibleForTesting
    static Uri zza(Context context, zzdq zzdq, Uri uri, View view, Activity activity) {
        if (zzdq == null) {
            return uri;
        }
        try {
            if (zzdq.zzc(uri)) {
                return zzdq.zza(uri, context, view, activity);
            }
            return uri;
        } catch (zzdt unused) {
            return uri;
        } catch (Exception e2) {
            zzq.zzku().zza(e2, "OpenGmsgHandler.maybeAddClickSignalsToUri");
            return uri;
        }
    }

    private final void zzab(boolean z) {
        zzaoe zzaoe = this.zzcxn;
        if (zzaoe != null) {
            zzaoe.zzac(z);
        }
    }

    private static boolean zzc(Map<String, String> map) {
        return "1".equals(map.get("custom_close"));
    }

    private static int zzd(Map<String, String> map) {
        String str = map.get("o");
        if (str == null) {
            return -1;
        }
        if ("p".equalsIgnoreCase(str)) {
            zzq.zzks();
            return 7;
        } else if ("l".equalsIgnoreCase(str)) {
            zzq.zzks();
            return 6;
        } else if (Constants.URL_CAMPAIGN.equalsIgnoreCase(str)) {
            return zzq.zzks().zzwo();
        } else {
            return -1;
        }
    }

    @VisibleForTesting
    static Uri zze(Uri uri) {
        try {
            if (uri.getQueryParameter("aclk_ms") != null) {
                return uri.buildUpon().appendQueryParameter("aclk_upms", String.valueOf(SystemClock.uptimeMillis())).build();
            }
        } catch (UnsupportedOperationException e2) {
            String valueOf = String.valueOf(uri.toString());
            zzayu.zzc(valueOf.length() != 0 ? "Error adding click uptime parameter to url: ".concat(valueOf) : new String("Error adding click uptime parameter to url: "), e2);
        }
        return uri;
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzbei zzbei = (zzbei) obj;
        String zzb = zzauk.zzb((String) map.get("u"), zzbei.getContext(), true);
        String str = (String) map.get("a");
        if (str == null) {
            zzayu.zzez("Action missing from an open GMSG.");
            return;
        }
        zzc zzc = this.zzcxm;
        if (zzc != null && !zzc.zzjq()) {
            this.zzcxm.zzbq(zzb);
        } else if ("expand".equalsIgnoreCase(str)) {
            if (((zzbel) zzbei).zzaaf()) {
                zzayu.zzez("Cannot expand WebView that is already expanded.");
                return;
            }
            zzab(false);
            ((zzbep) zzbei).zzc(zzc(map), zzd(map));
        } else if ("webapp".equalsIgnoreCase(str)) {
            zzab(false);
            if (zzb != null) {
                ((zzbep) zzbei).zza(zzc(map), zzd(map), zzb);
            } else {
                ((zzbep) zzbei).zza(zzc(map), zzd(map), (String) map.get(TJAdUnitConstants.String.HTML), (String) map.get("baseurl"));
            }
        } else if (!TapjoyConstants.TJC_APP_PLACEMENT.equalsIgnoreCase(str) || !"true".equalsIgnoreCase((String) map.get("system_browser"))) {
            zzab(true);
            String str2 = (String) map.get("intent_url");
            Intent intent = null;
            if (!TextUtils.isEmpty(str2)) {
                try {
                    intent = Intent.parseUri(str2, 0);
                } catch (URISyntaxException e2) {
                    String valueOf = String.valueOf(str2);
                    zzayu.zzc(valueOf.length() != 0 ? "Error parsing the url: ".concat(valueOf) : new String("Error parsing the url: "), e2);
                }
            }
            if (!(intent == null || intent.getData() == null)) {
                Uri data = intent.getData();
                if (!Uri.EMPTY.equals(data)) {
                    intent.setData(zze(zza(zzbei.getContext(), ((zzbeq) zzbei).zzaad(), data, ((zzbes) zzbei).getView(), zzbei.zzyn())));
                }
            }
            if (intent != null) {
                ((zzbep) zzbei).zza(new zzd(intent));
                return;
            }
            if (!TextUtils.isEmpty(zzb)) {
                zzb = zze(zza(zzbei.getContext(), ((zzbeq) zzbei).zzaad(), Uri.parse(zzb), ((zzbes) zzbei).getView(), zzbei.zzyn())).toString();
            }
            ((zzbep) zzbei).zza(new zzd((String) map.get("i"), zzb, (String) map.get("m"), (String) map.get("p"), (String) map.get(Constants.URL_CAMPAIGN), (String) map.get("f"), (String) map.get("e")));
        } else {
            zzab(true);
            if (TextUtils.isEmpty(zzb)) {
                zzayu.zzez("Destination url cannot be empty.");
                return;
            }
            try {
                ((zzbep) zzbei).zza(new zzd(new zzafu(zzbei.getContext(), ((zzbeq) zzbei).zzaad(), ((zzbes) zzbei).getView()).zze(map)));
            } catch (ActivityNotFoundException e3) {
                zzayu.zzez(e3.getMessage());
            }
        }
    }
}
