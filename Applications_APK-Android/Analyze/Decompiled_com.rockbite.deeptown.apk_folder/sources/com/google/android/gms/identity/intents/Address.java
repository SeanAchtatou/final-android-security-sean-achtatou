package com.google.android.gms.identity.intents;

import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation;
import com.google.android.gms.internal.identity.zze;

public final class Address {
    public static final Api<AddressOptions> API = new Api<>("Address.API", CLIENT_BUILDER, CLIENT_KEY);
    private static final Api.AbstractClientBuilder<zze, AddressOptions> CLIENT_BUILDER = new zza();
    private static final Api.ClientKey<zze> CLIENT_KEY = new Api.ClientKey<>();

    public static final class AddressOptions implements Api.ApiOptions.HasOptions {
        public final int theme;

        public AddressOptions() {
            this.theme = 0;
        }

        public AddressOptions(int i2) {
            this.theme = i2;
        }
    }

    private static abstract class zza extends BaseImplementation.ApiMethodImpl<Status, zze> {
        public zza(GoogleApiClient googleApiClient) {
            super(Address.API, googleApiClient);
        }

        public /* synthetic */ Result createFailedResult(Status status) {
            return status;
        }
    }

    public static void requestUserAddress(GoogleApiClient googleApiClient, UserAddressRequest userAddressRequest, int i2) {
        googleApiClient.enqueue(new zzb(googleApiClient, userAddressRequest, i2));
    }
}
