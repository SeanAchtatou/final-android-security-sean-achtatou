package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbin implements zzdxg<zzbio> {
    private final zzdxp<zzavu> zzemi;

    private zzbin(zzdxp<zzavu> zzdxp) {
        this.zzemi = zzdxp;
    }

    public static zzbin zza(zzdxp<zzavu> zzdxp) {
        return new zzbin(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzbio(this.zzemi.get());
    }
}
