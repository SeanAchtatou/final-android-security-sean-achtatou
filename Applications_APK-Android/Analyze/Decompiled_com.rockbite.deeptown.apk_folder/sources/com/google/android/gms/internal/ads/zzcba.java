package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcba implements zzdxg<zzcax> {
    private final zzdxp<zzbws> zzfkx;

    public zzcba(zzdxp<zzbws> zzdxp) {
        this.zzfkx = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return new zzcax(this.zzfkx.get());
    }
}
