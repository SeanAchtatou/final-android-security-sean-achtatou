package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbs;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
final class zzbu implements zzdsa {
    static final zzdsa zzew = new zzbu();

    private zzbu() {
    }

    public final boolean zzf(int i2) {
        return zzbs.zza.C0140zza.zzg(i2) != null;
    }
}
