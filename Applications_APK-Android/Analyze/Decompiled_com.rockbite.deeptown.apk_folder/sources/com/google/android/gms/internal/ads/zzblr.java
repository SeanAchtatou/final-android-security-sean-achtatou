package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzblr implements zzdxg<Runnable> {
    private final zzbls zzffa;

    public zzblr(zzbls zzbls) {
        this.zzffa = zzbls;
    }

    public final /* synthetic */ Object get() {
        return (Runnable) zzdxm.zza(this.zzffa.zzagn(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
