package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcuo implements zzded {
    static final zzded zzdoq = new zzcuo();

    private zzcuo() {
    }

    public final Object apply(Object obj) {
        AdvertisingIdClient.Info info = (AdvertisingIdClient.Info) obj;
        info.getClass();
        return new zzcum(info, null);
    }
}
