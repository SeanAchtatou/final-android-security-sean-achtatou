package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface zzaca extends IInterface {
    String getText() throws RemoteException;

    List<zzaci> zzqx() throws RemoteException;
}
