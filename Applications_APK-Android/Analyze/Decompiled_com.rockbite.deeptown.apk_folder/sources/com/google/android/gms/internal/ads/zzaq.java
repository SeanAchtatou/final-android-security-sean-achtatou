package com.google.android.gms.internal.ads;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzaq extends FilterInputStream {
    private final long zzce;
    private long zzcf;

    zzaq(InputStream inputStream, long j2) {
        super(inputStream);
        this.zzce = j2;
    }

    public final int read() throws IOException {
        int read = super.read();
        if (read != -1) {
            this.zzcf++;
        }
        return read;
    }

    /* access modifiers changed from: package-private */
    public final long zzo() {
        return this.zzce - this.zzcf;
    }

    public final int read(byte[] bArr, int i2, int i3) throws IOException {
        int read = super.read(bArr, i2, i3);
        if (read != -1) {
            this.zzcf += (long) read;
        }
        return read;
    }
}
