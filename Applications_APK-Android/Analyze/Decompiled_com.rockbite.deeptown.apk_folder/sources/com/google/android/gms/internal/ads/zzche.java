package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzche implements Callable {
    private final zzdhe zzfgb;
    private final zzdhe zzfpa;
    private final zzcgw zzfwg;

    zzche(zzcgw zzcgw, zzdhe zzdhe, zzdhe zzdhe2) {
        this.zzfwg = zzcgw;
        this.zzfpa = zzdhe;
        this.zzfgb = zzdhe2;
    }

    public final Object call() {
        return this.zzfwg.zza(this.zzfpa, this.zzfgb);
    }
}
