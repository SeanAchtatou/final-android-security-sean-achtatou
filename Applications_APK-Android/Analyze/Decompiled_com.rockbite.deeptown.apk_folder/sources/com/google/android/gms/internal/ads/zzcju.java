package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcju implements Callable {
    private final zzczl zzfel;
    private final zzczt zzfot;
    private final zzcjr zzfzb;

    zzcju(zzcjr zzcjr, zzczt zzczt, zzczl zzczl) {
        this.zzfzb = zzcjr;
        this.zzfot = zzczt;
        this.zzfel = zzczl;
    }

    public final Object call() {
        return this.zzfzb.zzc(this.zzfot, this.zzfel);
    }
}
