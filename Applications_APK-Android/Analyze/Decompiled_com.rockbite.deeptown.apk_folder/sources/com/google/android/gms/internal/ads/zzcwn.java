package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcwn implements zzcxv {
    private final zzcwl zzgil;

    zzcwn(zzcwl zzcwl) {
        this.zzgil = zzcwl;
    }

    public final zzboe zzc(zzcxs zzcxs) {
        return this.zzgil.zzb(zzcxs);
    }
}
