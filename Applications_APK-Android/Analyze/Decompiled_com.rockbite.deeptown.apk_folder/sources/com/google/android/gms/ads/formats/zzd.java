package com.google.android.gms.ads.formats;

import android.widget.ImageView;
import com.google.android.gms.internal.ads.zzabv;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final /* synthetic */ class zzd implements zzabv {
    private final UnifiedNativeAdView zzbkm;

    zzd(UnifiedNativeAdView unifiedNativeAdView) {
        this.zzbkm = unifiedNativeAdView;
    }

    public final void setImageScaleType(ImageView.ScaleType scaleType) {
        this.zzbkm.zza(scaleType);
    }
}
