package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcgu {
    private final zzdhd zzfov;
    private final zzdxa<zzcgw> zzful;
    private final zzcgn zzfwa;

    public zzcgu(zzdhd zzdhd, zzcgn zzcgn, zzdxa<zzcgw> zzdxa) {
        this.zzfov = zzdhd;
        this.zzfwa = zzcgn;
        this.zzful = zzdxa;
    }
}
