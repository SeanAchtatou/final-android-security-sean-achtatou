package com.google.android.gms.internal.ads;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.RemoteException;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzacj extends NativeAd.Image {
    private final int height;
    private final Uri uri;
    private final int width;
    private final double zzcvn;
    private final zzaci zzcvs;
    private final Drawable zzcvt;

    public zzacj(zzaci zzaci) {
        Drawable drawable;
        int i2;
        this.zzcvs = zzaci;
        Uri uri2 = null;
        try {
            IObjectWrapper zzrc = this.zzcvs.zzrc();
            if (zzrc != null) {
                drawable = (Drawable) ObjectWrapper.unwrap(zzrc);
                this.zzcvt = drawable;
                uri2 = this.zzcvs.getUri();
                this.uri = uri2;
                double d2 = 1.0d;
                d2 = this.zzcvs.getScale();
                this.zzcvn = d2;
                int i3 = -1;
                i2 = this.zzcvs.getWidth();
                this.width = i2;
                i3 = this.zzcvs.getHeight();
                this.height = i3;
            }
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
        }
        drawable = null;
        this.zzcvt = drawable;
        try {
            uri2 = this.zzcvs.getUri();
        } catch (RemoteException e3) {
            zzayu.zzc("", e3);
        }
        this.uri = uri2;
        double d22 = 1.0d;
        try {
            d22 = this.zzcvs.getScale();
        } catch (RemoteException e4) {
            zzayu.zzc("", e4);
        }
        this.zzcvn = d22;
        int i32 = -1;
        try {
            i2 = this.zzcvs.getWidth();
        } catch (RemoteException e5) {
            zzayu.zzc("", e5);
            i2 = -1;
        }
        this.width = i2;
        try {
            i32 = this.zzcvs.getHeight();
        } catch (RemoteException e6) {
            zzayu.zzc("", e6);
        }
        this.height = i32;
    }

    public final Drawable getDrawable() {
        return this.zzcvt;
    }

    public final int getHeight() {
        return this.height;
    }

    public final double getScale() {
        return this.zzcvn;
    }

    public final Uri getUri() {
        return this.uri;
    }

    public final int getWidth() {
        return this.width;
    }
}
