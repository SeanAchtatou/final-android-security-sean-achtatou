package com.google.android.gms.internal.ads;

import com.facebook.internal.NativeProtocol;
import com.tapjoy.TJAdUnitConstants;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public class zzaoo {
    private final zzbdi zzcza;
    private final String zzdgj;

    public zzaoo(zzbdi zzbdi) {
        this(zzbdi, "");
    }

    public final void zza(int i2, int i3, int i4, int i5, float f2, int i6) {
        try {
            this.zzcza.zzb("onScreenInfoChanged", new JSONObject().put("width", i2).put("height", i3).put("maxSizeWidth", i4).put("maxSizeHeight", i5).put("density", (double) f2).put("rotation", i6));
        } catch (JSONException e2) {
            zzayu.zzc("Error occurred while obtaining screen information.", e2);
        }
    }

    public final void zzb(int i2, int i3, int i4, int i5) {
        try {
            this.zzcza.zzb("onSizeChanged", new JSONObject().put("x", i2).put("y", i3).put("width", i4).put("height", i5));
        } catch (JSONException e2) {
            zzayu.zzc("Error occurred while dispatching size change.", e2);
        }
    }

    public final void zzc(int i2, int i3, int i4, int i5) {
        try {
            this.zzcza.zzb("onDefaultPositionReceived", new JSONObject().put("x", i2).put("y", i3).put("width", i4).put("height", i5));
        } catch (JSONException e2) {
            zzayu.zzc("Error occurred while dispatching default position.", e2);
        }
    }

    public final void zzds(String str) {
        try {
            JSONObject put = new JSONObject().put(TJAdUnitConstants.String.MESSAGE, str).put(NativeProtocol.WEB_DIALOG_ACTION, this.zzdgj);
            if (this.zzcza != null) {
                this.zzcza.zzb("onError", put);
            }
        } catch (JSONException e2) {
            zzayu.zzc("Error occurred while dispatching error event.", e2);
        }
    }

    public final void zzdt(String str) {
        try {
            this.zzcza.zzb("onReadyEventReceived", new JSONObject().put("js", str));
        } catch (JSONException e2) {
            zzayu.zzc("Error occurred while dispatching ready Event.", e2);
        }
    }

    public final void zzdu(String str) {
        try {
            this.zzcza.zzb("onStateChanged", new JSONObject().put("state", str));
        } catch (JSONException e2) {
            zzayu.zzc("Error occurred while dispatching state change.", e2);
        }
    }

    public zzaoo(zzbdi zzbdi, String str) {
        this.zzcza = zzbdi;
        this.zzdgj = str;
    }
}
