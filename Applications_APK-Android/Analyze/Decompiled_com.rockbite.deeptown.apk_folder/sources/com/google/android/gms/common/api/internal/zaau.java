package com.google.android.gms.common.api.internal;

/* compiled from: com.google.android.gms:play-services-base@@17.1.0 */
abstract class zaau implements Runnable {
    private final /* synthetic */ zaak zafz;

    private zaau(zaak zaak) {
        this.zafz = zaak;
    }

    public void run() {
        this.zafz.zaer.lock();
        try {
            if (!Thread.interrupted()) {
                zaal();
                this.zafz.zaer.unlock();
            }
        } catch (RuntimeException e2) {
            this.zafz.zafv.zab(e2);
        } finally {
            this.zafz.zaer.unlock();
        }
    }

    /* access modifiers changed from: protected */
    public abstract void zaal();

    /* synthetic */ zaau(zaak zaak, zaaj zaaj) {
        this(zaak);
    }
}
