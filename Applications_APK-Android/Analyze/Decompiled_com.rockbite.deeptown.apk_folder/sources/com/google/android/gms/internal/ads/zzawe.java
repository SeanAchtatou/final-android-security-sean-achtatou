package com.google.android.gms.internal.ads;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzawe extends BroadcastReceiver {
    private final /* synthetic */ zzawb zzdsw;

    private zzawe(zzawb zzawb) {
        this.zzdsw = zzawb;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzawb.zza(com.google.android.gms.internal.ads.zzawb, boolean):boolean
     arg types: [com.google.android.gms.internal.ads.zzawb, int]
     candidates:
      com.google.android.gms.internal.ads.zzawb.zza(com.google.android.gms.internal.ads.zzawb, java.lang.String):java.lang.String
      com.google.android.gms.internal.ads.zzawb.zza(android.content.Context, android.content.Intent):void
      com.google.android.gms.internal.ads.zzawb.zza(android.content.Context, android.net.Uri):void
      com.google.android.gms.internal.ads.zzawb.zza(android.content.Context, java.lang.Throwable):void
      com.google.android.gms.internal.ads.zzawb.zza(org.json.JSONArray, java.lang.Object):void
      com.google.android.gms.internal.ads.zzawb.zza(android.os.Bundle, org.json.JSONObject):org.json.JSONObject
      com.google.android.gms.internal.ads.zzawb.zza(android.view.View, android.content.Context):boolean
      com.google.android.gms.internal.ads.zzawb.zza(com.google.android.gms.internal.ads.zzawb, boolean):boolean */
    public final void onReceive(Context context, Intent intent) {
        if ("android.intent.action.USER_PRESENT".equals(intent.getAction())) {
            boolean unused = this.zzdsw.zzxy = true;
        } else if ("android.intent.action.SCREEN_OFF".equals(intent.getAction())) {
            boolean unused2 = this.zzdsw.zzxy = false;
        }
    }

    /* synthetic */ zzawe(zzawb zzawb, zzawa zzawa) {
        this(zzawb);
    }
}
