package com.google.android.gms.internal.ads;

import java.io.InputStream;
import java.util.Collections;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzar {
    private final int zzcg;
    private final List<zzk> zzch;
    private final int zzci;
    private final InputStream zzcj;

    public zzar(int i2, List<zzk> list) {
        this(i2, list, -1, null);
    }

    public final InputStream getContent() {
        return this.zzcj;
    }

    public final int getContentLength() {
        return this.zzci;
    }

    public final int getStatusCode() {
        return this.zzcg;
    }

    public final List<zzk> zzp() {
        return Collections.unmodifiableList(this.zzch);
    }

    public zzar(int i2, List<zzk> list, int i3, InputStream inputStream) {
        this.zzcg = i2;
        this.zzch = list;
        this.zzci = i3;
        this.zzcj = inputStream;
    }
}
