package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbpu;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcne<AdT, AdapterT, ListenerT extends zzbpu> implements zzdxg<zzcna<AdT, AdapterT, ListenerT>> {
    private final zzdxp<zzcis<AdapterT, ListenerT>> zzfaw;
    private final zzdxp<zzdhd> zzfei;
    private final zzdxp<zzdcr> zzfet;
    private final zzdxp<zzcir<AdT, AdapterT, ListenerT>> zzgbk;

    private zzcne(zzdxp<zzdcr> zzdxp, zzdxp<zzdhd> zzdxp2, zzdxp<zzcis<AdapterT, ListenerT>> zzdxp3, zzdxp<zzcir<AdT, AdapterT, ListenerT>> zzdxp4) {
        this.zzfet = zzdxp;
        this.zzfei = zzdxp2;
        this.zzfaw = zzdxp3;
        this.zzgbk = zzdxp4;
    }

    public static <AdT, AdapterT, ListenerT extends zzbpu> zzcne<AdT, AdapterT, ListenerT> zzd(zzdxp<zzdcr> zzdxp, zzdxp<zzdhd> zzdxp2, zzdxp<zzcis<AdapterT, ListenerT>> zzdxp3, zzdxp<zzcir<AdT, AdapterT, ListenerT>> zzdxp4) {
        return new zzcne<>(zzdxp, zzdxp2, zzdxp3, zzdxp4);
    }

    public final /* synthetic */ Object get() {
        return new zzcna(this.zzfet.get(), this.zzfei.get(), this.zzfaw.get(), this.zzgbk.get());
    }
}
