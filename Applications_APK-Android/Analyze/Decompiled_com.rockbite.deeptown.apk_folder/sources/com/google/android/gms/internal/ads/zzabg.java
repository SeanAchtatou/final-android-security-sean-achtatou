package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzabg {
    public static zzaan<Boolean> zzcue = zzaan.zzf("gads:rewarded_sku:enabled", true);
    private static zzaan<Boolean> zzcuf = zzaan.zzf("gads:rewarded_sku:override_test:enabled", false);
}
