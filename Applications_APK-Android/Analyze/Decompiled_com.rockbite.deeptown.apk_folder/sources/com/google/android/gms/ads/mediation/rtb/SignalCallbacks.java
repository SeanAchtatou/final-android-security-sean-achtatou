package com.google.android.gms.ads.mediation.rtb;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface SignalCallbacks {
    void onFailure(String str);

    void onSuccess(String str);
}
