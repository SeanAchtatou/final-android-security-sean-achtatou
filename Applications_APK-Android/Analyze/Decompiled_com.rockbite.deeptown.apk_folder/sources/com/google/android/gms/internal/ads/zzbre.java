package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzsy;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbre implements zzbrn {
    private final zzsy.zza zzfhy;

    zzbre(zzsy.zza zza) {
        this.zzfhy = zza;
    }

    public final void zzp(Object obj) {
        ((zzbri) obj).zza(this.zzfhy);
    }
}
