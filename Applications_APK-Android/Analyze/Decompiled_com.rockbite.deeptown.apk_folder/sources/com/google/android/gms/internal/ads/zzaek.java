package com.google.android.gms.internal.ads;

import android.graphics.drawable.Drawable;
import android.os.RemoteException;
import com.esotericsoftware.spine.Animation;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzaek implements UnifiedNativeAd.MediaContent {
    private final zzacd zzcwg;

    public zzaek(zzacd zzacd) {
        this.zzcwg = zzacd;
    }

    public final float getAspectRatio() {
        try {
            return this.zzcwg.getAspectRatio();
        } catch (RemoteException unused) {
            return Animation.CurveTimeline.LINEAR;
        }
    }

    public final Drawable getMainImage() {
        try {
            IObjectWrapper zzre = this.zzcwg.zzre();
            if (zzre != null) {
                return (Drawable) ObjectWrapper.unwrap(zzre);
            }
            return null;
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
            return null;
        }
    }

    public final void setMainImage(Drawable drawable) {
        try {
            this.zzcwg.zzo(ObjectWrapper.wrap(drawable));
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
        }
    }

    public final zzacd zzrr() {
        return this.zzcwg;
    }
}
