package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;
import com.tapjoy.TJAdUnitConstants;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzaog {
    private final zzbdi zzcza;
    private final boolean zzdfu;
    private final String zzdfv;

    public zzaog(zzbdi zzbdi, Map<String, String> map) {
        this.zzcza = zzbdi;
        this.zzdfv = map.get("forceOrientation");
        if (map.containsKey("allowOrientationChange")) {
            this.zzdfu = Boolean.parseBoolean(map.get("allowOrientationChange"));
        } else {
            this.zzdfu = true;
        }
    }

    public final void execute() {
        int i2;
        if (this.zzcza == null) {
            zzayu.zzez("AdWebView is null");
            return;
        }
        if (TJAdUnitConstants.String.PORTRAIT.equalsIgnoreCase(this.zzdfv)) {
            zzq.zzks();
            i2 = 7;
        } else if (TJAdUnitConstants.String.LANDSCAPE.equalsIgnoreCase(this.zzdfv)) {
            zzq.zzks();
            i2 = 6;
        } else if (this.zzdfu) {
            i2 = -1;
        } else {
            i2 = zzq.zzks().zzwo();
        }
        this.zzcza.setRequestedOrientation(i2);
    }
}
