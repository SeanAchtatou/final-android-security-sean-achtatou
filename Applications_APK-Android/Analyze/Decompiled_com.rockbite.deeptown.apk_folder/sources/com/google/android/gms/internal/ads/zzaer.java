package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.formats.UnifiedNativeAd;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzaer extends zzady {
    private final UnifiedNativeAd.OnUnifiedNativeAdLoadedListener zzcwm;

    public zzaer(UnifiedNativeAd.OnUnifiedNativeAdLoadedListener onUnifiedNativeAdLoadedListener) {
        this.zzcwm = onUnifiedNativeAdLoadedListener;
    }

    public final void zza(zzaeg zzaeg) {
        this.zzcwm.onUnifiedNativeAdLoaded(new zzaeh(zzaeg));
    }
}
