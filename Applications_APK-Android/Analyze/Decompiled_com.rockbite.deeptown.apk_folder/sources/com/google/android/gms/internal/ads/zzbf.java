package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.nio.ByteBuffer;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzbf {
    String getType();

    void zza(zzbi zzbi);

    void zza(zzdws zzdws, ByteBuffer byteBuffer, long j2, zzbe zzbe) throws IOException;
}
