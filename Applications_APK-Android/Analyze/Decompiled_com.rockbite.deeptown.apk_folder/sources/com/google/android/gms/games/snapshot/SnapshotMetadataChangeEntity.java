package com.google.android.gms.games.snapshot;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.games.internal.zzc;

@SafeParcelable.Class(creator = "SnapshotMetadataChangeCreator")
@SafeParcelable.Reserved({1000})
/* compiled from: com.google.android.gms:play-services-games@@19.0.0 */
public final class SnapshotMetadataChangeEntity extends zzc implements SnapshotMetadataChange {
    public static final Parcelable.Creator<SnapshotMetadataChangeEntity> CREATOR = new zzc();
    @SafeParcelable.Field(getter = "getDescription", id = 1)
    private final String description;
    @SafeParcelable.Field(getter = "getProgressValue", id = 6)
    private final Long zzrc;
    @SafeParcelable.Field(getter = "getCoverImageUri", id = 4)
    private final Uri zzre;
    @SafeParcelable.Field(getter = "getPlayedTimeMillis", id = 2)
    private final Long zzrf;
    @SafeParcelable.Field(getter = "getCoverImageTeleporter", id = 5)
    private BitmapTeleporter zzrg;

    SnapshotMetadataChangeEntity() {
        this(null, null, null, null, null);
    }

    public final Bitmap getCoverImage() {
        BitmapTeleporter bitmapTeleporter = this.zzrg;
        if (bitmapTeleporter == null) {
            return null;
        }
        return bitmapTeleporter.get();
    }

    public final String getDescription() {
        return this.description;
    }

    public final Long getPlayedTimeMillis() {
        return this.zzrf;
    }

    public final Long getProgressValue() {
        return this.zzrc;
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, getDescription(), false);
        SafeParcelWriter.writeLongObject(parcel, 2, getPlayedTimeMillis(), false);
        SafeParcelWriter.writeParcelable(parcel, 4, this.zzre, i2, false);
        SafeParcelWriter.writeParcelable(parcel, 5, this.zzrg, i2, false);
        SafeParcelWriter.writeLongObject(parcel, 6, getProgressValue(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }

    public final BitmapTeleporter zzds() {
        return this.zzrg;
    }

    @SafeParcelable.Constructor
    SnapshotMetadataChangeEntity(@SafeParcelable.Param(id = 1) String str, @SafeParcelable.Param(id = 2) Long l, @SafeParcelable.Param(id = 5) BitmapTeleporter bitmapTeleporter, @SafeParcelable.Param(id = 4) Uri uri, @SafeParcelable.Param(id = 6) Long l2) {
        this.description = str;
        this.zzrf = l;
        this.zzrg = bitmapTeleporter;
        this.zzre = uri;
        this.zzrc = l2;
        Preconditions.checkState(this.zzrg == null || uri == null, "Cannot set both a URI and an image");
    }
}
