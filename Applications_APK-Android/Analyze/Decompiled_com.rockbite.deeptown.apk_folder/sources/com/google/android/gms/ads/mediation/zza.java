package com.google.android.gms.ads.mediation;

import com.google.android.gms.internal.ads.zzxb;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface zza {
    zzxb getVideoController();
}
