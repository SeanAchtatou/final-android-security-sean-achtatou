package com.google.android.gms.internal.ads;

import android.os.IBinder;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final /* synthetic */ class zzaop implements zzayw {
    static final zzayw zzbtz = new zzaop();

    private zzaop() {
    }

    public final Object apply(Object obj) {
        return zzdde.zzap((IBinder) obj);
    }
}
