package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
final class zzcg implements zzdsa {
    static final zzdsa zzew = new zzcg();

    private zzcg() {
    }

    public final boolean zzf(int i2) {
        return zzce.zzk(i2) != null;
    }
}
