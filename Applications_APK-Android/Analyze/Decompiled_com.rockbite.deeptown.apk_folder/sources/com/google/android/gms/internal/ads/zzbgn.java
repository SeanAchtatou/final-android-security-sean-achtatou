package com.google.android.gms.internal.ads;

import java.util.HashMap;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbgn implements zzdxg<HashMap<String, zzchh>> {
    private static final zzbgn zzejw = new zzbgn();

    public static zzbgn zzacv() {
        return zzejw;
    }

    public final /* synthetic */ Object get() {
        return (HashMap) zzdxm.zza(new HashMap(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
