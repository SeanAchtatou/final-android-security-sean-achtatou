package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzabj {
    private static zzaan<Long> zzcuq = zzaan.zzb("gads:ad_loader:timeout_ms", 60000);
    public static zzaan<Long> zzcur = zzaan.zzb("gads:rendering:timeout_ms", 60000);
    private static zzaan<Long> zzcus = zzaan.zzb("gads:resolve_future:default_timeout_ms", 30000);
}
