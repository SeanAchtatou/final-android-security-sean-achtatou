package com.google.android.gms.drive.metadata;

import com.google.android.gms.common.data.DataHolder;
import java.util.Collection;

public abstract class zzb<T> extends zza<Collection<T>> {
    protected zzb(String str, Collection<String> collection, Collection<String> collection2, int i2) {
        super(str, collection, collection2, i2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: zzd */
    public Collection<T> zzc(DataHolder dataHolder, int i2, int i3) {
        throw new UnsupportedOperationException("Cannot read collections from a dataHolder.");
    }
}
