package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcwb implements zzdxg<Boolean> {
    private final zzcvw zzgih;

    public zzcwb(zzcvw zzcvw) {
        this.zzgih = zzcvw;
    }

    public final /* synthetic */ Object get() {
        return Boolean.valueOf(this.zzgih.zzanw());
    }
}
