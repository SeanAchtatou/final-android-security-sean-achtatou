package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbwb implements zzdxg<zzcaj> {
    private final zzbvy zzfla;

    public zzbwb(zzbvy zzbvy) {
        this.zzfla = zzbvy;
    }

    public static zzcaj zza(zzbvy zzbvy) {
        return (zzcaj) zzdxm.zza(zzbvy.zzait(), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        return zza(this.zzfla);
    }
}
