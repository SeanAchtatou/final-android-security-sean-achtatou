package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbmc implements zzps {
    private final zzats zzffo;

    public zzbmc(zzats zzats) {
        this.zzffo = zzats;
    }

    public final void zza(zzpt zzpt) {
        this.zzffo.zza(zzpt);
    }
}
