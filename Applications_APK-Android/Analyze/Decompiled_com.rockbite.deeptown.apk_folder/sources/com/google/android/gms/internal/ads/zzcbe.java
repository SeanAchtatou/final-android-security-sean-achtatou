package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcbe implements Runnable {
    private final zzbdi zzehp;

    private zzcbe(zzbdi zzbdi) {
        this.zzehp = zzbdi;
    }

    static Runnable zzh(zzbdi zzbdi) {
        return new zzcbe(zzbdi);
    }

    public final void run() {
        this.zzehp.destroy();
    }
}
