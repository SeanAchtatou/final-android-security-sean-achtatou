package com.google.android.gms.internal.ads;

import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.DefaultClock;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdaj implements zzdxg<Clock> {
    private final zzdag zzgna;

    public zzdaj(zzdag zzdag) {
        this.zzgna = zzdag;
    }

    public final /* synthetic */ Object get() {
        return (Clock) zzdxm.zza(DefaultClock.getInstance(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
