package com.google.android.gms.internal.ads;

import java.util.List;
import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdcf {
    private final E zzgpx;
    private final List<zzdhe<?>> zzgqc;
    private final /* synthetic */ zzdcd zzgqd;

    private zzdcf(zzdcd zzdcd, E e2, List<zzdhe<?>> list) {
        this.zzgqd = zzdcd;
        this.zzgpx = e2;
        this.zzgqc = list;
    }

    public final <O> zzdcj<O> zzb(Callable<O> callable) {
        zzdgx zzi = zzdgs.zzi(this.zzgqc);
        zzdhe zza = zzi.zza(zzdce.zzgfx, zzazd.zzdwj);
        zzdcd zzdcd = this.zzgqd;
        return new zzdcj(zzdcd, this.zzgpx, zza, this.zzgqc, zzi.zza(callable, zzdcd.zzfov));
    }
}
