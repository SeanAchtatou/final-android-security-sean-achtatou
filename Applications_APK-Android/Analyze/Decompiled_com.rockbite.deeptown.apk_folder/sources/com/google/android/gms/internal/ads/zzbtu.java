package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.gass.zzf;
import java.lang.ref.WeakReference;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbtu extends zzbmd {
    private final WeakReference<zzbdi> zzfix;
    private final zzbsk zzfiy;
    private final zzbuv zzfiz;
    private final zzbmx zzfja;
    private final zzf zzfjb;
    private boolean zzfjc = false;
    private final Context zzup;

    zzbtu(zzbmg zzbmg, Context context, zzbdi zzbdi, zzbsk zzbsk, zzbuv zzbuv, zzbmx zzbmx, zzf zzf) {
        super(zzbmg);
        this.zzup = context;
        this.zzfix = new WeakReference<>(zzbdi);
        this.zzfiy = zzbsk;
        this.zzfiz = zzbuv;
        this.zzfja = zzbmx;
        this.zzfjb = zzf;
    }

    public final void finalize() throws Throwable {
        try {
            zzbdi zzbdi = this.zzfix.get();
            if (((Boolean) zzve.zzoy().zzd(zzzn.zzcrd)).booleanValue()) {
                if (!this.zzfjc && zzbdi != null) {
                    zzdhd zzdhd = zzazd.zzdwi;
                    zzbdi.getClass();
                    zzdhd.execute(zzbtt.zzh(zzbdi));
                }
            } else if (zzbdi != null) {
                zzbdi.destroy();
            }
        } finally {
            super.finalize();
        }
    }

    public final boolean isClosed() {
        return this.zzfja.isClosed();
    }

    public final boolean zzaid() {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcin)).booleanValue()) {
            zzq.zzkq();
            if (zzawb.zzau(this.zzup)) {
                zzayu.zzez("Interstitials that show when your app is in the background are a violation of AdMob policies and may lead to blocked ad serving. To learn more, visit  https://googlemobileadssdk.page.link/admob-interstitial-policies");
                if (((Boolean) zzve.zzoy().zzd(zzzn.zzcio)).booleanValue()) {
                    this.zzfjb.zzgq(this.zzfbl.zzgmi.zzgmf.zzbzo);
                }
                return false;
            }
        }
        if (!this.zzfjc) {
            return true;
        }
        return false;
    }

    public final void zzbg(boolean z) {
        this.zzfiy.zzahx();
        this.zzfiz.zza(z, this.zzup);
        this.zzfjc = true;
    }
}
