package com.google.android.gms.internal.ads;

import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcgf implements zzdgf {
    private final zzcge zzfvk;

    zzcgf(zzcge zzcge) {
        this.zzfvk = zzcge;
    }

    public final zzdhe zzf(Object obj) {
        return this.zzfvk.zzn((JSONObject) obj);
    }
}
