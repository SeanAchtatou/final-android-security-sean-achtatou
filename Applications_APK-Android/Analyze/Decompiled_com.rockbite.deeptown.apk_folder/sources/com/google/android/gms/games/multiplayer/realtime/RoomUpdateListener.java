package com.google.android.gms.games.multiplayer.realtime;

@Deprecated
/* compiled from: com.google.android.gms:play-services-games@@19.0.0 */
public interface RoomUpdateListener {
    void onJoinedRoom(int i2, Room room);

    void onLeftRoom(int i2, String str);

    void onRoomConnected(int i2, Room room);

    void onRoomCreated(int i2, Room room);
}
