package com.google.android.gms.ads.doubleclick;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.Correlator;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.ads.zzayu;
import com.google.android.gms.internal.ads.zzvu;
import com.google.android.gms.internal.ads.zzxl;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class PublisherAdView extends ViewGroup {
    private final zzxl zzaca;

    public PublisherAdView(Context context) {
        super(context);
        this.zzaca = new zzxl(this);
    }

    public final void destroy() {
        this.zzaca.destroy();
    }

    public final AdListener getAdListener() {
        return this.zzaca.getAdListener();
    }

    public final AdSize getAdSize() {
        return this.zzaca.getAdSize();
    }

    public final AdSize[] getAdSizes() {
        return this.zzaca.getAdSizes();
    }

    public final String getAdUnitId() {
        return this.zzaca.getAdUnitId();
    }

    public final AppEventListener getAppEventListener() {
        return this.zzaca.getAppEventListener();
    }

    public final String getMediationAdapterClassName() {
        return this.zzaca.getMediationAdapterClassName();
    }

    public final OnCustomRenderedAdLoadedListener getOnCustomRenderedAdLoadedListener() {
        return this.zzaca.getOnCustomRenderedAdLoadedListener();
    }

    public final VideoController getVideoController() {
        return this.zzaca.getVideoController();
    }

    public final VideoOptions getVideoOptions() {
        return this.zzaca.getVideoOptions();
    }

    public final boolean isLoading() {
        return this.zzaca.isLoading();
    }

    public final void loadAd(PublisherAdRequest publisherAdRequest) {
        this.zzaca.zza(publisherAdRequest.zzdg());
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        View childAt = getChildAt(0);
        if (childAt != null && childAt.getVisibility() != 8) {
            int measuredWidth = childAt.getMeasuredWidth();
            int measuredHeight = childAt.getMeasuredHeight();
            int i6 = ((i4 - i2) - measuredWidth) / 2;
            int i7 = ((i5 - i3) - measuredHeight) / 2;
            childAt.layout(i6, i7, measuredWidth + i6, measuredHeight + i7);
        }
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i2, int i3) {
        int i4;
        int i5 = 0;
        View childAt = getChildAt(0);
        if (childAt == null || childAt.getVisibility() == 8) {
            AdSize adSize = null;
            try {
                adSize = getAdSize();
            } catch (NullPointerException e2) {
                zzayu.zzc("Unable to retrieve ad size.", e2);
            }
            if (adSize != null) {
                Context context = getContext();
                int widthInPixels = adSize.getWidthInPixels(context);
                i4 = adSize.getHeightInPixels(context);
                i5 = widthInPixels;
            } else {
                i4 = 0;
            }
        } else {
            measureChild(childAt, i2, i3);
            i5 = childAt.getMeasuredWidth();
            i4 = childAt.getMeasuredHeight();
        }
        setMeasuredDimension(View.resolveSize(Math.max(i5, getSuggestedMinimumWidth()), i2), View.resolveSize(Math.max(i4, getSuggestedMinimumHeight()), i3));
    }

    public final void pause() {
        this.zzaca.pause();
    }

    public final void recordManualImpression() {
        this.zzaca.recordManualImpression();
    }

    public final void resume() {
        this.zzaca.resume();
    }

    public final void setAdListener(AdListener adListener) {
        this.zzaca.setAdListener(adListener);
    }

    public final void setAdSizes(AdSize... adSizeArr) {
        if (adSizeArr == null || adSizeArr.length <= 0) {
            throw new IllegalArgumentException("The supported ad sizes must contain at least one valid ad size.");
        }
        this.zzaca.zza(adSizeArr);
    }

    public final void setAdUnitId(String str) {
        this.zzaca.setAdUnitId(str);
    }

    public final void setAppEventListener(AppEventListener appEventListener) {
        this.zzaca.setAppEventListener(appEventListener);
    }

    @KeepForSdk
    @Deprecated
    public final void setCorrelator(Correlator correlator) {
    }

    public final void setManualImpressionsEnabled(boolean z) {
        this.zzaca.setManualImpressionsEnabled(z);
    }

    public final void setOnCustomRenderedAdLoadedListener(OnCustomRenderedAdLoadedListener onCustomRenderedAdLoadedListener) {
        this.zzaca.setOnCustomRenderedAdLoadedListener(onCustomRenderedAdLoadedListener);
    }

    public final void setVideoOptions(VideoOptions videoOptions) {
        this.zzaca.setVideoOptions(videoOptions);
    }

    public final boolean zza(zzvu zzvu) {
        return this.zzaca.zza(zzvu);
    }

    public PublisherAdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.zzaca = new zzxl(this, attributeSet, true);
        Preconditions.checkNotNull(context, "Context cannot be null");
    }

    public PublisherAdView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.zzaca = new zzxl(this, attributeSet, true);
    }
}
