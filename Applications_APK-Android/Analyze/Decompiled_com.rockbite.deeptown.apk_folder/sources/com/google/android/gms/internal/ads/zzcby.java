package com.google.android.gms.internal.ads;

import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcby implements zzafn {
    private final zzbdi zzfpv;
    private final zzcbp zzfrj;

    zzcby(zzcbp zzcbp, zzbdi zzbdi) {
        this.zzfrj = zzcbp;
        this.zzfpv = zzbdi;
    }

    public final void zza(Object obj, Map map) {
        this.zzfrj.zza(this.zzfpv, (zzbdi) obj, map);
    }
}
