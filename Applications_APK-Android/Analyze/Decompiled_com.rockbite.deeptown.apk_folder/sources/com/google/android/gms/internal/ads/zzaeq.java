package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.formats.NativeCustomTemplateAd;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzaeq extends zzads {
    private final NativeCustomTemplateAd.OnCustomTemplateAdLoadedListener zzcwl;

    public zzaeq(NativeCustomTemplateAd.OnCustomTemplateAdLoadedListener onCustomTemplateAdLoadedListener) {
        this.zzcwl = onCustomTemplateAdLoadedListener;
    }

    public final void zzb(zzade zzade) {
        this.zzcwl.onCustomTemplateAdLoaded(zzadf.zza(zzade));
    }
}
