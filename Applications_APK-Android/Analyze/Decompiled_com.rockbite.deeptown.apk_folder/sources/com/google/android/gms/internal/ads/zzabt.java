package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.formats.UnifiedNativeAd;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface zzabt {
    void setMediaContent(UnifiedNativeAd.MediaContent mediaContent);
}
