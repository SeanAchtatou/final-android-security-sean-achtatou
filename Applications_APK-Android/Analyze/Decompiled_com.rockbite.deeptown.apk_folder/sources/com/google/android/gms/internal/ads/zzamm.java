package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.ads.AdRequest;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzamm implements Runnable {
    private final /* synthetic */ zzamf zzdef;
    private final /* synthetic */ AdRequest.ErrorCode zzdeg;

    zzamm(zzamf zzamf, AdRequest.ErrorCode errorCode) {
        this.zzdef = zzamf;
        this.zzdeg = errorCode;
    }

    public final void run() {
        try {
            this.zzdef.zzdds.onAdFailedToLoad(zzamr.zza(this.zzdeg));
        } catch (RemoteException e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
        }
    }
}
