package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Message;
import android.view.View;
import android.view.WindowManager;
import android.webkit.ConsoleMessage;
import android.webkit.GeolocationPermissions;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.PermissionRequest;
import android.webkit.WebChromeClient;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.android.gms.ads.internal.zzc;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.util.PlatformVersion;

@TargetApi(11)
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbda extends WebChromeClient {
    private final zzbdi zzcza;

    public zzbda(zzbdi zzbdi) {
        this.zzcza = zzbdi;
    }

    private final boolean zza(Context context, String str, String str2, String str3, String str4, JsResult jsResult, JsPromptResult jsPromptResult, boolean z) {
        zzc zzaas;
        try {
            if (this.zzcza == null || this.zzcza.zzaaa() == null || this.zzcza.zzaaa().zzaas() == null || (zzaas = this.zzcza.zzaaa().zzaas()) == null || zzaas.zzjq()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(str2);
                if (z) {
                    LinearLayout linearLayout = new LinearLayout(context);
                    linearLayout.setOrientation(1);
                    TextView textView = new TextView(context);
                    textView.setText(str3);
                    EditText editText = new EditText(context);
                    editText.setText(str4);
                    linearLayout.addView(textView);
                    linearLayout.addView(editText);
                    builder.setView(linearLayout).setPositiveButton(17039370, new zzbdg(jsPromptResult, editText)).setNegativeButton(17039360, new zzbdh(jsPromptResult)).setOnCancelListener(new zzbde(jsPromptResult)).create().show();
                } else {
                    builder.setMessage(str3).setPositiveButton(17039370, new zzbdf(jsResult)).setNegativeButton(17039360, new zzbdc(jsResult)).setOnCancelListener(new zzbdd(jsResult)).create().show();
                }
                return true;
            }
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 11 + String.valueOf(str3).length());
            sb.append("window.");
            sb.append(str);
            sb.append("('");
            sb.append(str3);
            sb.append("')");
            zzaas.zzbq(sb.toString());
            return false;
        } catch (WindowManager.BadTokenException e2) {
            zzayu.zzd("Fail to display Dialog.", e2);
        }
    }

    public final void onCloseWindow(WebView webView) {
        if (!(webView instanceof zzbdi)) {
            zzayu.zzez("Tried to close a WebView that wasn't an AdWebView.");
            return;
        }
        com.google.android.gms.ads.internal.overlay.zzc zzzw = ((zzbdi) webView).zzzw();
        if (zzzw == null) {
            zzayu.zzez("Tried to close an AdWebView not associated with an overlay.");
        } else {
            zzzw.close();
        }
    }

    public final boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        String message = consoleMessage.message();
        String sourceId = consoleMessage.sourceId();
        int lineNumber = consoleMessage.lineNumber();
        StringBuilder sb = new StringBuilder(String.valueOf(message).length() + 19 + String.valueOf(sourceId).length());
        sb.append("JS: ");
        sb.append(message);
        sb.append(" (");
        sb.append(sourceId);
        sb.append(":");
        sb.append(lineNumber);
        sb.append(")");
        String sb2 = sb.toString();
        if (sb2.contains("Application Cache")) {
            return super.onConsoleMessage(consoleMessage);
        }
        int i2 = zzbdj.zzeed[consoleMessage.messageLevel().ordinal()];
        if (i2 == 1) {
            zzayu.zzex(sb2);
        } else if (i2 == 2) {
            zzayu.zzez(sb2);
        } else if (i2 == 3 || i2 == 4) {
            zzayu.zzey(sb2);
        } else if (i2 != 5) {
            zzayu.zzey(sb2);
        } else {
            zzayu.zzea(sb2);
        }
        return super.onConsoleMessage(consoleMessage);
    }

    public final boolean onCreateWindow(WebView webView, boolean z, boolean z2, Message message) {
        WebView.WebViewTransport webViewTransport = (WebView.WebViewTransport) message.obj;
        WebView webView2 = new WebView(webView.getContext());
        if (this.zzcza.zzaab() != null) {
            webView2.setWebViewClient(this.zzcza.zzaab());
        }
        webViewTransport.setWebView(webView2);
        message.sendToTarget();
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public final void onExceededDatabaseQuota(String str, String str2, long j2, long j3, long j4, WebStorage.QuotaUpdater quotaUpdater) {
        long j5 = 5242880 - j4;
        if (j5 <= 0) {
            quotaUpdater.updateQuota(j2);
            return;
        }
        if (j2 != 0) {
            if (j3 == 0) {
                j2 = Math.min(j2 + Math.min(131072L, j5), 1048576L);
            } else if (j3 <= Math.min(1048576 - j2, j5)) {
                j2 += j3;
            }
            j3 = j2;
        } else if (j3 > j5 || j3 > 1048576) {
            j3 = 0;
        }
        quotaUpdater.updateQuota(j3);
    }

    public final void onGeolocationPermissionsShowPrompt(String str, GeolocationPermissions.Callback callback) {
        boolean z;
        if (callback != null) {
            zzq.zzkq();
            if (!zzawb.zzq(this.zzcza.getContext(), "android.permission.ACCESS_FINE_LOCATION")) {
                zzq.zzkq();
                if (!zzawb.zzq(this.zzcza.getContext(), "android.permission.ACCESS_COARSE_LOCATION")) {
                    z = false;
                    callback.invoke(str, z, true);
                }
            }
            z = true;
            callback.invoke(str, z, true);
        }
    }

    public final void onHideCustomView() {
        com.google.android.gms.ads.internal.overlay.zzc zzzw = this.zzcza.zzzw();
        if (zzzw == null) {
            zzayu.zzez("Could not get ad overlay when hiding custom view.");
        } else {
            zzzw.zztk();
        }
    }

    public final boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
        return zza(zza(webView), "alert", str, str2, null, jsResult, null, false);
    }

    public final boolean onJsBeforeUnload(WebView webView, String str, String str2, JsResult jsResult) {
        return zza(zza(webView), "onBeforeUnload", str, str2, null, jsResult, null, false);
    }

    public final boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
        return zza(zza(webView), "confirm", str, str2, null, jsResult, null, false);
    }

    public final boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
        return zza(zza(webView), "prompt", str, str2, str3, null, jsPromptResult, true);
    }

    @TargetApi(21)
    public final void onPermissionRequest(PermissionRequest permissionRequest) {
        if (!PlatformVersion.isAtLeastLollipop() || zzabl.zzcuu.get().booleanValue()) {
            super.onPermissionRequest(permissionRequest);
            return;
        }
        zzbdi zzbdi = this.zzcza;
        if (zzbdi == null || zzbdi.zzaaa() == null || this.zzcza.zzaaa().zzabf() == null) {
            super.onPermissionRequest(permissionRequest);
            return;
        }
        String[] zza = this.zzcza.zzaaa().zzabf().zza(permissionRequest.getResources());
        if (zza.length > 0) {
            permissionRequest.grant(zza);
        } else {
            permissionRequest.deny();
        }
    }

    public final void onReachedMaxAppCacheSize(long j2, long j3, WebStorage.QuotaUpdater quotaUpdater) {
        long j4 = j2 + 131072;
        if (5242880 - j3 < j4) {
            quotaUpdater.updateQuota(0);
        } else {
            quotaUpdater.updateQuota(j4);
        }
    }

    public final void onShowCustomView(View view, WebChromeClient.CustomViewCallback customViewCallback) {
        onShowCustomView(view, -1, customViewCallback);
    }

    @Deprecated
    public final void onShowCustomView(View view, int i2, WebChromeClient.CustomViewCallback customViewCallback) {
        com.google.android.gms.ads.internal.overlay.zzc zzzw = this.zzcza.zzzw();
        if (zzzw == null) {
            zzayu.zzez("Could not get ad overlay when showing custom view.");
            customViewCallback.onCustomViewHidden();
            return;
        }
        zzzw.zza(view, customViewCallback);
        zzzw.setRequestedOrientation(i2);
    }

    private static Context zza(WebView webView) {
        if (!(webView instanceof zzbdi)) {
            return webView.getContext();
        }
        zzbdi zzbdi = (zzbdi) webView;
        Activity zzyn = zzbdi.zzyn();
        if (zzyn != null) {
            return zzyn;
        }
        return zzbdi.getContext();
    }
}
