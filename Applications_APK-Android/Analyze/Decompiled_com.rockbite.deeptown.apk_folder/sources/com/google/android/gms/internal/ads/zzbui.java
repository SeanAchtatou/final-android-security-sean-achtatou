package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbui implements zzdxg<zzbsu<zzbsm>> {
    public static zzbsu<zzbsm> zza(zzbtv zzbtv, zzbve zzbve) {
        return (zzbsu) zzdxm.zza(new zzbsu(zzbve, zzazd.zzdwj), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        throw new NoSuchMethodError();
    }
}
