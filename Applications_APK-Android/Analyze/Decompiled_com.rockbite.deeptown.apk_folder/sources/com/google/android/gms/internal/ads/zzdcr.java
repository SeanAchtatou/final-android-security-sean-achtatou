package com.google.android.gms.internal.ads;

import java.util.concurrent.ScheduledExecutorService;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdcr extends zzdcd<zzdco> {
    zzdcr(zzdhd zzdhd, ScheduledExecutorService scheduledExecutorService, zzdcq zzdcq) {
        super(zzdhd, scheduledExecutorService, zzdcq);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ String zzv(Object obj) {
        return ((zzdco) obj).zzaqe();
    }
}
