package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcpt implements zzdxg<zzcpr> {
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzcgm> zzgei;

    public zzcpt(zzdxp<Executor> zzdxp, zzdxp<zzcgm> zzdxp2) {
        this.zzfcv = zzdxp;
        this.zzgei = zzdxp2;
    }

    public final /* synthetic */ Object get() {
        return new zzcpr(this.zzfcv.get(), this.zzgei.get());
    }
}
