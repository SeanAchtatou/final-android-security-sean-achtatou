package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzciy implements zzdxg<zzciu> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<Executor> zzfei;
    private final zzdxp<zzczu> zzfep;
    private final zzdxp<zzcbn> zzfog;
    private final zzdxp<zzbka> zzfyl;

    public zzciy(zzdxp<zzbka> zzdxp, zzdxp<Context> zzdxp2, zzdxp<Executor> zzdxp3, zzdxp<zzcbn> zzdxp4, zzdxp<zzczu> zzdxp5) {
        this.zzfyl = zzdxp;
        this.zzejv = zzdxp2;
        this.zzfei = zzdxp3;
        this.zzfog = zzdxp4;
        this.zzfep = zzdxp5;
    }

    public final /* synthetic */ Object get() {
        return new zzciu(this.zzfyl.get(), this.zzejv.get(), this.zzfei.get(), this.zzfog.get(), this.zzfep.get());
    }
}
