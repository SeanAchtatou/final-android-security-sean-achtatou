package com.google.android.gms.internal.ads;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcvd implements zzcub<zzcva> {
    private zzdhd zzfov;
    zzsa zzghv;
    Context zzup;

    public zzcvd(zzsa zzsa, zzdhd zzdhd, Context context) {
        this.zzghv = zzsa;
        this.zzfov = zzdhd;
        this.zzup = context;
    }

    public final zzdhe<zzcva> zzanc() {
        return this.zzfov.zzd(new zzcvc(this));
    }
}
