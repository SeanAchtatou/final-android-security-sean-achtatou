package com.google.android.gms.internal.ads;

import android.os.Bundle;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcqn implements zzcty<Bundle> {
    private final zzuo zzgey;

    public zzcqn(zzuo zzuo) {
        this.zzgey = zzuo;
    }

    public final /* synthetic */ void zzr(Object obj) {
        Bundle bundle = (Bundle) obj;
        zzuo zzuo = this.zzgey;
        if (zzuo != null) {
            int i2 = zzuo.orientation;
            if (i2 == 1) {
                bundle.putString("avo", "p");
            } else if (i2 == 2) {
                bundle.putString("avo", "l");
            }
        }
    }
}
