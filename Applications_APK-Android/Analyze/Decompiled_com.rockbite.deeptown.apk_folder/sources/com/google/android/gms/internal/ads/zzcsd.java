package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcsd implements zzdxg<zzcsb> {
    private final zzdxp<zzdhd> zzfcv;

    private zzcsd(zzdxp<zzdhd> zzdxp) {
        this.zzfcv = zzdxp;
    }

    public static zzcsd zzaj(zzdxp<zzdhd> zzdxp) {
        return new zzcsd(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzcsb(this.zzfcv.get());
    }
}
