package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.dynamic.IObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbdx implements Runnable {
    private final IObjectWrapper zzefs;

    zzbdx(IObjectWrapper iObjectWrapper) {
        this.zzefs = iObjectWrapper;
    }

    public final void run() {
        zzq.zzlf().zzac(this.zzefs);
    }
}
