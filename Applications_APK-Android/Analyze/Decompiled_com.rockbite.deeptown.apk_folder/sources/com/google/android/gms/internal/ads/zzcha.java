package com.google.android.gms.internal.ads;

import android.os.Bundle;
import com.google.android.gms.ads.internal.zzq;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcha implements zzdgf {
    private final zzcut zzfwf;

    zzcha(zzcut zzcut) {
        this.zzfwf = zzcut;
    }

    public final zzdhe zzf(Object obj) {
        return this.zzfwf.zzadt().zzs(zzq.zzkq().zzd((Bundle) obj));
    }
}
