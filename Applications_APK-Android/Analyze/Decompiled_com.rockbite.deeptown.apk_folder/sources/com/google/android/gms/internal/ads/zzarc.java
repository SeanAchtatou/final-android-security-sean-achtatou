package com.google.android.gms.internal.ads;

import com.google.android.gms.common.internal.Objects;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzarc extends zzarh {
    private final String type;
    private final int zzdno;

    public zzarc(String str, int i2) {
        this.type = str;
        this.zzdno = i2;
    }

    public final boolean equals(Object obj) {
        if (obj != null && (obj instanceof zzarc)) {
            zzarc zzarc = (zzarc) obj;
            if (!Objects.equal(this.type, zzarc.type) || !Objects.equal(Integer.valueOf(this.zzdno), Integer.valueOf(zzarc.zzdno))) {
                return false;
            }
            return true;
        }
        return false;
    }

    public final int getAmount() {
        return this.zzdno;
    }

    public final String getType() {
        return this.type;
    }
}
