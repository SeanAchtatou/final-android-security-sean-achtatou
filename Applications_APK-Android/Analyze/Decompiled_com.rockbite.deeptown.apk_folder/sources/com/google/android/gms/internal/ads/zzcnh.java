package com.google.android.gms.internal.ads;

import android.view.View;
import java.util.Collections;
import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzcnh extends zzbkn {
    zzcnh(zzcng zzcng, View view, zzbdi zzbdi, zzbme zzbme, zzczk zzczk) {
        super(view, null, zzbme, zzczk);
    }

    public final zzbpw zza(Set<zzbsu<zzbqb>> set) {
        return new zzbpw(Collections.emptySet());
    }
}
