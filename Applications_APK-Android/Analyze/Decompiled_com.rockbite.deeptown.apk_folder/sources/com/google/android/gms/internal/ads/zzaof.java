package com.google.android.gms.internal.ads;

import android.app.AlertDialog;
import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.text.TextUtils;
import android.webkit.URLUtil;
import com.google.android.gms.ads.impl.R;
import com.google.android.gms.ads.internal.zzq;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzaof extends zzaoo {
    private final Map<String, String> zzcsd;
    /* access modifiers changed from: private */
    public final Context zzup;

    public zzaof(zzbdi zzbdi, Map<String, String> map) {
        super(zzbdi, "storePicture");
        this.zzcsd = map;
        this.zzup = zzbdi.zzyn();
    }

    public final void execute() {
        if (this.zzup == null) {
            zzds("Activity context is not available");
            return;
        }
        zzq.zzkq();
        if (!zzawb.zzas(this.zzup).zzqd()) {
            zzds("Feature is not supported by the device.");
            return;
        }
        String str = this.zzcsd.get("iurl");
        if (TextUtils.isEmpty(str)) {
            zzds("Image url cannot be empty.");
        } else if (!URLUtil.isValidUrl(str)) {
            String valueOf = String.valueOf(str);
            zzds(valueOf.length() != 0 ? "Invalid image url: ".concat(valueOf) : new String("Invalid image url: "));
        } else {
            String lastPathSegment = Uri.parse(str).getLastPathSegment();
            zzq.zzkq();
            if (!zzawb.zzej(lastPathSegment)) {
                String valueOf2 = String.valueOf(lastPathSegment);
                zzds(valueOf2.length() != 0 ? "Image type not recognized: ".concat(valueOf2) : new String("Image type not recognized: "));
                return;
            }
            Resources resources = zzq.zzku().getResources();
            zzq.zzkq();
            AlertDialog.Builder zzar = zzawb.zzar(this.zzup);
            zzar.setTitle(resources != null ? resources.getString(R.string.s1) : "Save image");
            zzar.setMessage(resources != null ? resources.getString(R.string.s2) : "Allow Ad to store image in Picture gallery?");
            zzar.setPositiveButton(resources != null ? resources.getString(R.string.s3) : "Accept", new zzaoi(this, str, lastPathSegment));
            zzar.setNegativeButton(resources != null ? resources.getString(R.string.s4) : "Decline", new zzaoh(this));
            zzar.create().show();
        }
    }
}
