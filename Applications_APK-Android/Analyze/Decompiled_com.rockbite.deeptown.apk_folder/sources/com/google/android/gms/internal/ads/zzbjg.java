package com.google.android.gms.internal.ads;

import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbjg implements Runnable {
    private final zzbdi zzehp;
    private final JSONObject zzfcs;

    zzbjg(zzbdi zzbdi, JSONObject jSONObject) {
        this.zzehp = zzbdi;
        this.zzfcs = jSONObject;
    }

    public final void run() {
        this.zzehp.zza("AFMA_updateActiveView", this.zzfcs);
    }
}
