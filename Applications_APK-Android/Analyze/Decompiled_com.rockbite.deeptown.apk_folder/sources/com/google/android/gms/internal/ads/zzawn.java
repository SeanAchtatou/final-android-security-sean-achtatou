package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.content.Context;
import android.webkit.CookieManager;
import android.webkit.WebResourceResponse;
import com.google.android.gms.ads.internal.zzq;
import java.io.InputStream;
import java.util.Map;

@TargetApi(21)
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public class zzawn extends zzawk {
    public final zzbdl zza(zzbdi zzbdi, zzsm zzsm, boolean z) {
        return new zzbem(zzbdi, zzsm, z);
    }

    public final CookieManager zzbd(Context context) {
        if (zzawh.zzwq()) {
            return null;
        }
        try {
            return CookieManager.getInstance();
        } catch (Throwable th) {
            zzayu.zzc("Failed to obtain CookieManager.", th);
            zzq.zzku().zza(th, "ApiLevelUtil.getCookieManager");
            return null;
        }
    }

    public final int zzwr() {
        return 16974374;
    }

    public final WebResourceResponse zza(String str, String str2, int i2, String str3, Map<String, String> map, InputStream inputStream) {
        return new WebResourceResponse(str, str2, i2, str3, map, inputStream);
    }
}
