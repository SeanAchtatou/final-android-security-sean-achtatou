package com.google.android.gms.games.snapshot;

import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.util.IOUtils;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.games.internal.zzaz;
import com.google.android.gms.games.internal.zzc;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

@SafeParcelable.Class(creator = "SnapshotContentsEntityCreator")
@SafeParcelable.Reserved({1000})
/* compiled from: com.google.android.gms:play-services-games@@19.0.0 */
public final class SnapshotContentsEntity extends zzc implements SnapshotContents {
    public static final Parcelable.Creator<SnapshotContentsEntity> CREATOR = new zza();
    private static final Object zzqx = new Object();
    @SafeParcelable.Field(getter = "getContents", id = 1)
    private Contents zzqy;

    @SafeParcelable.Constructor
    public SnapshotContentsEntity(@SafeParcelable.Param(id = 1) Contents contents) {
        this.zzqy = contents;
    }

    private final boolean zza(int i2, byte[] bArr, int i3, int i4, boolean z) {
        Preconditions.checkState(!isClosed(), "Must provide a previously opened SnapshotContents");
        synchronized (zzqx) {
            FileOutputStream fileOutputStream = new FileOutputStream(this.zzqy.getParcelFileDescriptor().getFileDescriptor());
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
            try {
                FileChannel channel = fileOutputStream.getChannel();
                channel.position((long) i2);
                bufferedOutputStream.write(bArr, i3, i4);
                if (z) {
                    channel.truncate((long) bArr.length);
                }
                bufferedOutputStream.flush();
            } catch (IOException e2) {
                zzaz.i("SnapshotContentsEntity", "Failed to write snapshot data", e2);
                return false;
            }
        }
        return true;
    }

    public final void close() {
        this.zzqy = null;
    }

    public final ParcelFileDescriptor getParcelFileDescriptor() {
        Preconditions.checkState(!isClosed(), "Cannot mutate closed contents!");
        return this.zzqy.getParcelFileDescriptor();
    }

    public final boolean isClosed() {
        return this.zzqy == null;
    }

    public final boolean modifyBytes(int i2, byte[] bArr, int i3, int i4) {
        return zza(i2, bArr, i3, bArr.length, false);
    }

    public final byte[] readFully() throws IOException {
        byte[] readInputStreamFully;
        Preconditions.checkState(!isClosed(), "Must provide a previously opened Snapshot");
        synchronized (zzqx) {
            FileInputStream fileInputStream = new FileInputStream(this.zzqy.getParcelFileDescriptor().getFileDescriptor());
            BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
            try {
                fileInputStream.getChannel().position(0L);
                readInputStreamFully = IOUtils.readInputStreamFully(bufferedInputStream, false);
                fileInputStream.getChannel().position(0L);
            } catch (IOException e2) {
                zzaz.w("SnapshotContentsEntity", "Failed to read snapshot data", e2);
                throw e2;
            }
        }
        return readInputStreamFully;
    }

    public final boolean writeBytes(byte[] bArr) {
        return zza(0, bArr, 0, bArr.length, true);
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeParcelable(parcel, 1, this.zzqy, i2, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }

    public final Contents zzdr() {
        return this.zzqy;
    }
}
