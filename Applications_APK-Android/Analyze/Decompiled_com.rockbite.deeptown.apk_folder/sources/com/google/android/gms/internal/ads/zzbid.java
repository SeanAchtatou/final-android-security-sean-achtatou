package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbid implements zzdxg<zzsf> {
    private static final zzbid zzfbb = new zzbid();

    public static zzbid zzafb() {
        return zzfbb;
    }

    public static zzsf zzafc() {
        return (zzsf) zzdxm.zza(new zzsf(), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        return zzafc();
    }
}
