package com.google.android.gms.internal.ads;

import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcvm implements zzcub<zzcvn> {
    List<String> zzdiz;
    private zzdhd zzfov;
    zzyz zzgib;

    public zzcvm(zzyz zzyz, zzdhd zzdhd, List<String> list) {
        this.zzgib = zzyz;
        this.zzfov = zzdhd;
        this.zzdiz = list;
    }

    public final zzdhe<zzcvn> zzanc() {
        return this.zzfov.zzd(new zzcvp(this));
    }
}
