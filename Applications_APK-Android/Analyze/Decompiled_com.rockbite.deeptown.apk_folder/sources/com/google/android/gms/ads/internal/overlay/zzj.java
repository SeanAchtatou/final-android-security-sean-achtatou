package com.google.android.gms.ads.internal.overlay;

import android.content.Context;
import android.view.MotionEvent;
import android.widget.RelativeLayout;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.ads.zzawt;

@VisibleForTesting
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzj extends RelativeLayout {
    @VisibleForTesting
    private zzawt zzdhk;
    @VisibleForTesting
    boolean zzdhl;

    public zzj(Context context, String str, String str2) {
        super(context);
        this.zzdhk = new zzawt(context, str);
        this.zzdhk.zzx(str2);
    }

    public final boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (this.zzdhl) {
            return false;
        }
        this.zzdhk.zzd(motionEvent);
        return false;
    }
}
