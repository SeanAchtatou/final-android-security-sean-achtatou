package com.google.android.gms.internal.ads;

import android.content.Context;
import android.view.View;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdac {
    private final zzald zzgmv;

    public zzdac(zzald zzald) {
        this.zzgmv = zzald;
    }

    public final void destroy() throws zzdab {
        try {
            this.zzgmv.destroy();
        } catch (Throwable th) {
            throw new zzdab(th);
        }
    }

    public final zzxb getVideoController() throws zzdab {
        try {
            return this.zzgmv.getVideoController();
        } catch (Throwable th) {
            throw new zzdab(th);
        }
    }

    public final View getView() throws zzdab {
        try {
            return (View) ObjectWrapper.unwrap(this.zzgmv.zzsk());
        } catch (Throwable th) {
            throw new zzdab(th);
        }
    }

    public final boolean isInitialized() throws zzdab {
        try {
            return this.zzgmv.isInitialized();
        } catch (Throwable th) {
            throw new zzdab(th);
        }
    }

    public final void onContextChanged(Context context) throws zzdab {
        try {
            this.zzgmv.zzs(ObjectWrapper.wrap(context));
        } catch (Throwable th) {
            throw new zzdab(th);
        }
    }

    public final void pause() throws zzdab {
        try {
            this.zzgmv.pause();
        } catch (Throwable th) {
            throw new zzdab(th);
        }
    }

    public final void resume() throws zzdab {
        try {
            this.zzgmv.resume();
        } catch (Throwable th) {
            throw new zzdab(th);
        }
    }

    public final void setImmersiveMode(boolean z) throws zzdab {
        try {
            this.zzgmv.setImmersiveMode(z);
        } catch (Throwable th) {
            throw new zzdab(th);
        }
    }

    public final void showInterstitial() throws zzdab {
        try {
            this.zzgmv.showInterstitial();
        } catch (Throwable th) {
            throw new zzdab(th);
        }
    }

    public final void showVideo() throws zzdab {
        try {
            this.zzgmv.showVideo();
        } catch (Throwable th) {
            throw new zzdab(th);
        }
    }

    public final void zza(Context context, zzuj zzuj, zzug zzug, String str, zzali zzali) throws zzdab {
        try {
            this.zzgmv.zza(ObjectWrapper.wrap(context), zzuj, zzug, str, zzali);
        } catch (Throwable th) {
            throw new zzdab(th);
        }
    }

    public final void zzb(Context context, zzug zzug, String str, zzali zzali) throws zzdab {
        try {
            this.zzgmv.zzb(ObjectWrapper.wrap(context), zzug, str, zzali);
        } catch (Throwable th) {
            throw new zzdab(th);
        }
    }

    public final void zzcb(Context context) throws zzdab {
        try {
            this.zzgmv.zzt(ObjectWrapper.wrap(context));
        } catch (Throwable th) {
            throw new zzdab(th);
        }
    }

    public final zzall zzsl() throws zzdab {
        try {
            return this.zzgmv.zzsl();
        } catch (Throwable th) {
            throw new zzdab(th);
        }
    }

    public final zzalq zzsm() throws zzdab {
        try {
            return this.zzgmv.zzsm();
        } catch (Throwable th) {
            throw new zzdab(th);
        }
    }

    public final boolean zzsp() throws zzdab {
        try {
            return this.zzgmv.zzsp();
        } catch (Throwable th) {
            throw new zzdab(th);
        }
    }

    public final zzalr zzsr() throws zzdab {
        try {
            return this.zzgmv.zzsr();
        } catch (Throwable th) {
            throw new zzdab(th);
        }
    }

    public final void zza(Context context, zzug zzug, String str, zzali zzali) throws zzdab {
        try {
            this.zzgmv.zza(ObjectWrapper.wrap(context), zzug, str, zzali);
        } catch (Throwable th) {
            throw new zzdab(th);
        }
    }

    public final void zza(Context context, zzuj zzuj, zzug zzug, String str, String str2, zzali zzali) throws zzdab {
        try {
            this.zzgmv.zza(ObjectWrapper.wrap(context), zzuj, zzug, str, str2, zzali);
        } catch (Throwable th) {
            throw new zzdab(th);
        }
    }

    public final void zza(Context context, zzug zzug, String str, String str2, zzali zzali) throws zzdab {
        try {
            this.zzgmv.zza(ObjectWrapper.wrap(context), zzug, str, str2, zzali);
        } catch (Throwable th) {
            throw new zzdab(th);
        }
    }

    public final void zza(Context context, zzug zzug, String str, zzarz zzarz, String str2) throws zzdab {
        try {
            this.zzgmv.zza(ObjectWrapper.wrap(context), zzug, (String) null, zzarz, str2);
        } catch (Throwable th) {
            throw new zzdab(th);
        }
    }

    public final void zza(zzug zzug, String str) throws zzdab {
        try {
            this.zzgmv.zza(zzug, str);
        } catch (Throwable th) {
            throw new zzdab(th);
        }
    }

    public final void zza(Context context, zzug zzug, String str, String str2, zzali zzali, zzaby zzaby, List<String> list) throws zzdab {
        try {
            this.zzgmv.zza(ObjectWrapper.wrap(context), zzug, str, str2, zzali, zzaby, list);
        } catch (Throwable th) {
            throw new zzdab(th);
        }
    }

    public final void zza(Context context, zzarz zzarz, List<String> list) throws zzdab {
        try {
            this.zzgmv.zza(ObjectWrapper.wrap(context), zzarz, list);
        } catch (Throwable th) {
            throw new zzdab(th);
        }
    }

    public final void zza(Context context, zzagp zzagp, List<zzagx> list) throws zzdab {
        try {
            this.zzgmv.zza(ObjectWrapper.wrap(context), zzagp, list);
        } catch (Throwable th) {
            throw new zzdab(th);
        }
    }
}
