package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.measurement.api.AppMeasurementSdk;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzakr extends zzbft {
    private final AppMeasurementSdk zzdbj;

    zzakr(AppMeasurementSdk appMeasurementSdk) {
        this.zzdbj = appMeasurementSdk;
    }

    public final void beginAdUnitExposure(String str) throws RemoteException {
        this.zzdbj.beginAdUnitExposure(str);
    }

    public final void clearConditionalUserProperty(String str, String str2, Bundle bundle) throws RemoteException {
        this.zzdbj.clearConditionalUserProperty(str, str2, bundle);
    }

    public final void endAdUnitExposure(String str) throws RemoteException {
        this.zzdbj.endAdUnitExposure(str);
    }

    public final long generateEventId() throws RemoteException {
        return this.zzdbj.generateEventId();
    }

    public final String getAppIdOrigin() throws RemoteException {
        return this.zzdbj.getAppIdOrigin();
    }

    public final String getAppInstanceId() throws RemoteException {
        return this.zzdbj.getAppInstanceId();
    }

    public final List getConditionalUserProperties(String str, String str2) throws RemoteException {
        return this.zzdbj.getConditionalUserProperties(str, str2);
    }

    public final String getCurrentScreenClass() throws RemoteException {
        return this.zzdbj.getCurrentScreenClass();
    }

    public final String getCurrentScreenName() throws RemoteException {
        return this.zzdbj.getCurrentScreenName();
    }

    public final String getGmpAppId() throws RemoteException {
        return this.zzdbj.getGmpAppId();
    }

    public final int getMaxUserProperties(String str) throws RemoteException {
        return this.zzdbj.getMaxUserProperties(str);
    }

    public final Map getUserProperties(String str, String str2, boolean z) throws RemoteException {
        return this.zzdbj.getUserProperties(str, str2, z);
    }

    public final void logEvent(String str, String str2, Bundle bundle) throws RemoteException {
        this.zzdbj.logEvent(str, str2, bundle);
    }

    public final void performAction(Bundle bundle) throws RemoteException {
        this.zzdbj.performAction(bundle);
    }

    public final Bundle performActionWithResponse(Bundle bundle) throws RemoteException {
        return this.zzdbj.performActionWithResponse(bundle);
    }

    public final void setConditionalUserProperty(Bundle bundle) throws RemoteException {
        this.zzdbj.setConditionalUserProperty(bundle);
    }

    public final void zza(String str, String str2, IObjectWrapper iObjectWrapper) throws RemoteException {
        this.zzdbj.setUserProperty(str, str2, iObjectWrapper != null ? ObjectWrapper.unwrap(iObjectWrapper) : null);
    }

    public final void zzb(IObjectWrapper iObjectWrapper, String str, String str2) throws RemoteException {
        this.zzdbj.setCurrentScreen(iObjectWrapper != null ? (Activity) ObjectWrapper.unwrap(iObjectWrapper) : null, str, str2);
    }
}
