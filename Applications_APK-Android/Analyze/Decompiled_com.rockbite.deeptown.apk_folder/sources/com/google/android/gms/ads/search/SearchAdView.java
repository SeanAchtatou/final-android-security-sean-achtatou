package com.google.android.gms.ads.search;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.internal.ads.zzayu;
import com.google.android.gms.internal.ads.zzxl;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class SearchAdView extends ViewGroup {
    private final zzxl zzaca;

    public SearchAdView(Context context) {
        super(context);
        this.zzaca = new zzxl(this);
    }

    public final void destroy() {
        this.zzaca.destroy();
    }

    public final AdListener getAdListener() {
        return this.zzaca.getAdListener();
    }

    public final AdSize getAdSize() {
        return this.zzaca.getAdSize();
    }

    public final String getAdUnitId() {
        return this.zzaca.getAdUnitId();
    }

    public final void loadAd(SearchAdRequest searchAdRequest) {
        this.zzaca.zza(searchAdRequest.zzdg());
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        View childAt = getChildAt(0);
        if (childAt != null && childAt.getVisibility() != 8) {
            int measuredWidth = childAt.getMeasuredWidth();
            int measuredHeight = childAt.getMeasuredHeight();
            int i6 = ((i4 - i2) - measuredWidth) / 2;
            int i7 = ((i5 - i3) - measuredHeight) / 2;
            childAt.layout(i6, i7, measuredWidth + i6, measuredHeight + i7);
        }
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i2, int i3) {
        int i4;
        int i5 = 0;
        View childAt = getChildAt(0);
        if (childAt == null || childAt.getVisibility() == 8) {
            AdSize adSize = null;
            try {
                adSize = getAdSize();
            } catch (NullPointerException e2) {
                zzayu.zzc("Unable to retrieve ad size.", e2);
            }
            if (adSize != null) {
                Context context = getContext();
                int widthInPixels = adSize.getWidthInPixels(context);
                i4 = adSize.getHeightInPixels(context);
                i5 = widthInPixels;
            } else {
                i4 = 0;
            }
        } else {
            measureChild(childAt, i2, i3);
            i5 = childAt.getMeasuredWidth();
            i4 = childAt.getMeasuredHeight();
        }
        setMeasuredDimension(View.resolveSize(Math.max(i5, getSuggestedMinimumWidth()), i2), View.resolveSize(Math.max(i4, getSuggestedMinimumHeight()), i3));
    }

    public final void pause() {
        this.zzaca.pause();
    }

    public final void resume() {
        this.zzaca.resume();
    }

    public final void setAdListener(AdListener adListener) {
        this.zzaca.setAdListener(adListener);
    }

    public final void setAdSize(AdSize adSize) {
        this.zzaca.setAdSizes(adSize);
    }

    public final void setAdUnitId(String str) {
        this.zzaca.setAdUnitId(str);
    }

    public final void loadAd(DynamicHeightSearchAdRequest dynamicHeightSearchAdRequest) {
        if (AdSize.SEARCH.equals(getAdSize())) {
            this.zzaca.zza(dynamicHeightSearchAdRequest.zzdg());
            return;
        }
        throw new IllegalStateException("You must use AdSize.SEARCH for a DynamicHeightSearchAdRequest");
    }

    public SearchAdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.zzaca = new zzxl(this, attributeSet, false);
    }

    public SearchAdView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.zzaca = new zzxl(this, attributeSet, false);
    }
}
