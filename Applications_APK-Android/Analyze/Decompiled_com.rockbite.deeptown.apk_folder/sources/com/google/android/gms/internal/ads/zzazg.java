package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzazg implements zzdgt<Object> {
    private final /* synthetic */ String zzdwm;

    zzazg(String str) {
        this.zzdwm = str;
    }

    public final void onSuccess(Object obj) {
    }

    public final void zzb(Throwable th) {
        zzq.zzku().zza(th, this.zzdwm);
    }
}
