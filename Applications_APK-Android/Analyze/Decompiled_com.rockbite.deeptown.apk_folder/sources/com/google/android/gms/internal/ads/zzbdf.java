package com.google.android.gms.internal.ads;

import android.content.DialogInterface;
import android.webkit.JsResult;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzbdf implements DialogInterface.OnClickListener {
    private final /* synthetic */ JsResult zzeea;

    zzbdf(JsResult jsResult) {
        this.zzeea = jsResult;
    }

    public final void onClick(DialogInterface dialogInterface, int i2) {
        this.zzeea.confirm();
    }
}
