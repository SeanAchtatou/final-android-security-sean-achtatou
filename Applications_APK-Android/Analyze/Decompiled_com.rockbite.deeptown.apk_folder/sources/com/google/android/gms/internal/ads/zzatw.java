package com.google.android.gms.internal.ads;

import android.os.Bundle;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzatw implements zzaul {
    private final String zzcyz;
    private final Bundle zzdpq;

    zzatw(String str, Bundle bundle) {
        this.zzcyz = str;
        this.zzdpq = bundle;
    }

    public final void zza(zzbfq zzbfq) {
        zzbfq.logEvent("am", this.zzcyz, this.zzdpq);
    }
}
