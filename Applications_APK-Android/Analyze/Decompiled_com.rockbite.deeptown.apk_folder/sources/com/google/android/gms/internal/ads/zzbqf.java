package com.google.android.gms.internal.ads;

import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbqf extends zzbrl<zzbqg> {
    public zzbqf(Set<zzbsu<zzbqg>> set) {
        super(set);
    }

    public final synchronized void zzahi() {
        zza(zzbqe.zzfhp);
    }
}
