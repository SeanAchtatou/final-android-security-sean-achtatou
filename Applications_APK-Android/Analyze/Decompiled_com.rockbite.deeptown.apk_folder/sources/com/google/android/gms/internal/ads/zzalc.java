package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface zzalc extends IInterface {
    zzald zzde(String str) throws RemoteException;

    boolean zzdf(String str) throws RemoteException;

    zzani zzdi(String str) throws RemoteException;
}
