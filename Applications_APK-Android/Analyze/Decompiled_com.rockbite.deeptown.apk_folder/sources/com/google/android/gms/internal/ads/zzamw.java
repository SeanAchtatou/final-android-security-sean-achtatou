package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface zzamw extends IInterface {
    void zzdl(String str) throws RemoteException;

    void zzx(IObjectWrapper iObjectWrapper) throws RemoteException;
}
