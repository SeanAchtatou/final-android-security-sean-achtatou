package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcmd extends zzcnd {
    private zzbte zzgam;
    private zzboz zzgan;

    public zzcmd(zzboq zzboq, zzbpd zzbpd, zzbpm zzbpm, zzbpw zzbpw, zzboz zzboz, zzbra zzbra, zzbtj zzbtj, zzbqj zzbqj, zzbte zzbte) {
        super(zzboq, zzbpd, zzbpm, zzbpw, zzbra, zzbqj, zzbtj);
        this.zzgam = zzbte;
        this.zzgan = zzboz;
    }

    public final void onVideoEnd() {
        this.zzgam.zzrt();
    }

    public final void zza(zzasf zzasf) throws RemoteException {
        this.zzgam.zza(new zzasd(zzasf.getType(), zzasf.getAmount()));
    }

    public final void zzb(Bundle bundle) throws RemoteException {
    }

    public final void zzb(zzasd zzasd) {
        this.zzgam.zza(zzasd);
    }

    public final void zzco(int i2) throws RemoteException {
        this.zzgan.zzco(i2);
    }

    public final void zzss() {
        this.zzgam.zzrs();
    }

    public final void zzst() throws RemoteException {
        this.zzgam.zzrt();
    }
}
