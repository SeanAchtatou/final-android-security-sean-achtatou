package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import com.appsflyer.share.Constants;
import java.util.Arrays;
import java.util.List;

@TargetApi(16)
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbcy extends zzbcn implements zzbca {
    private String zzdyg;
    private boolean zzedo;
    private zzbbs zzedr;
    private Exception zzeds;
    private boolean zzedt;

    public zzbcy(zzbaz zzbaz, zzbaw zzbaw) {
        super(zzbaz);
        this.zzedr = new zzbbs(zzbaz.getContext(), zzbaw);
        this.zzedr.zza(this);
    }

    private final void zzfl(String str) {
        synchronized (this) {
            this.zzedo = true;
            notify();
            release();
        }
        String str2 = this.zzdyg;
        if (str2 != null) {
            String zzfj = zzfj(str2);
            Exception exc = this.zzeds;
            if (exc != null) {
                zza(this.zzdyg, zzfj, "badUrl", zzb(str, exc));
            } else {
                zza(this.zzdyg, zzfj, "externalAbort", "Programmatic precache abort.");
            }
        }
    }

    public final void abort() {
        zzfl(null);
    }

    public final void release() {
        zzbbs zzbbs = this.zzedr;
        if (zzbbs != null) {
            zzbbs.zza((zzbca) null);
            this.zzedr.release();
        }
        super.release();
    }

    public final void zza(String str, Exception exc) {
        String str2 = (String) zzve.zzoy().zzd(zzzn.zzcgs);
        if (str2 != null) {
            List asList = Arrays.asList(str2.split(","));
            if (asList.contains("all") || asList.contains(exc.getClass().getCanonicalName())) {
                return;
            }
        }
        this.zzeds = exc;
        zzayu.zzd("Precache error", exc);
        zzfl(str);
    }

    public final void zzb(boolean z, long j2) {
        zzbaz zzbaz = this.zzedg.get();
        if (zzbaz != null) {
            zzazd.zzdwi.execute(new zzbdb(zzbaz, z, j2));
        }
    }

    public final void zzcv(int i2) {
        this.zzedr.zzzp().zzdc(i2);
    }

    public final void zzcw(int i2) {
        this.zzedr.zzzp().zzdd(i2);
    }

    public final void zzcx(int i2) {
        this.zzedr.zzzp().zzcx(i2);
    }

    public final void zzcy(int i2) {
        this.zzedr.zzzp().zzcy(i2);
    }

    public final void zzda(int i2) {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0087, code lost:
        if (r11.zzeds == null) goto L_0x008e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x008d, code lost:
        throw r11.zzeds;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x008e, code lost:
        r1 = "externalAbort";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0097, code lost:
        throw new java.io.IOException("Abort requested before buffering finished. ");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean zze(java.lang.String r34, java.lang.String[] r35) {
        /*
            r33 = this;
            r11 = r33
            r12 = r34
            r0 = r35
            r11.zzdyg = r12
            java.lang.String r13 = r33.zzfj(r34)
            java.lang.String r14 = "error"
            int r1 = r0.length     // Catch:{ Exception -> 0x0165 }
            android.net.Uri[] r1 = new android.net.Uri[r1]     // Catch:{ Exception -> 0x0165 }
            r2 = 0
        L_0x0012:
            int r3 = r0.length     // Catch:{ Exception -> 0x0165 }
            if (r2 >= r3) goto L_0x0023
            r3 = r0[r2]     // Catch:{ Exception -> 0x0020 }
            android.net.Uri r3 = android.net.Uri.parse(r3)     // Catch:{ Exception -> 0x0020 }
            r1[r2] = r3     // Catch:{ Exception -> 0x0020 }
            int r2 = r2 + 1
            goto L_0x0012
        L_0x0020:
            r0 = move-exception
            goto L_0x0168
        L_0x0023:
            com.google.android.gms.internal.ads.zzbbs r0 = r11.zzedr     // Catch:{ Exception -> 0x0165 }
            java.lang.String r2 = r11.zzduy     // Catch:{ Exception -> 0x0165 }
            r0.zza(r1, r2)     // Catch:{ Exception -> 0x0165 }
            java.lang.ref.WeakReference<com.google.android.gms.internal.ads.zzbaz> r0 = r11.zzedg     // Catch:{ Exception -> 0x0165 }
            java.lang.Object r0 = r0.get()     // Catch:{ Exception -> 0x0165 }
            com.google.android.gms.internal.ads.zzbaz r0 = (com.google.android.gms.internal.ads.zzbaz) r0     // Catch:{ Exception -> 0x0165 }
            if (r0 == 0) goto L_0x0037
            r0.zza(r13, r11)     // Catch:{ Exception -> 0x0020 }
        L_0x0037:
            com.google.android.gms.common.util.Clock r0 = com.google.android.gms.ads.internal.zzq.zzkx()     // Catch:{ Exception -> 0x0165 }
            long r16 = r0.currentTimeMillis()     // Catch:{ Exception -> 0x0165 }
            com.google.android.gms.internal.ads.zzzc<java.lang.Long> r1 = com.google.android.gms.internal.ads.zzzn.zzcgy     // Catch:{ Exception -> 0x0165 }
            com.google.android.gms.internal.ads.zzzj r2 = com.google.android.gms.internal.ads.zzve.zzoy()     // Catch:{ Exception -> 0x0165 }
            java.lang.Object r1 = r2.zzd(r1)     // Catch:{ Exception -> 0x0165 }
            java.lang.Long r1 = (java.lang.Long) r1     // Catch:{ Exception -> 0x0165 }
            long r9 = r1.longValue()     // Catch:{ Exception -> 0x0165 }
            com.google.android.gms.internal.ads.zzzc<java.lang.Long> r1 = com.google.android.gms.internal.ads.zzzn.zzcgx     // Catch:{ Exception -> 0x0165 }
            com.google.android.gms.internal.ads.zzzj r2 = com.google.android.gms.internal.ads.zzve.zzoy()     // Catch:{ Exception -> 0x0165 }
            java.lang.Object r1 = r2.zzd(r1)     // Catch:{ Exception -> 0x0165 }
            java.lang.Long r1 = (java.lang.Long) r1     // Catch:{ Exception -> 0x0165 }
            long r1 = r1.longValue()     // Catch:{ Exception -> 0x0165 }
            r3 = 1000(0x3e8, double:4.94E-321)
            long r6 = r1 * r3
            com.google.android.gms.internal.ads.zzzc<java.lang.Integer> r1 = com.google.android.gms.internal.ads.zzzn.zzcgw     // Catch:{ Exception -> 0x0165 }
            com.google.android.gms.internal.ads.zzzj r2 = com.google.android.gms.internal.ads.zzve.zzoy()     // Catch:{ Exception -> 0x0165 }
            java.lang.Object r1 = r2.zzd(r1)     // Catch:{ Exception -> 0x0165 }
            java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ Exception -> 0x0165 }
            int r1 = r1.intValue()     // Catch:{ Exception -> 0x0165 }
            long r4 = (long) r1     // Catch:{ Exception -> 0x0165 }
            r1 = -1
        L_0x0076:
            monitor-enter(r33)     // Catch:{ Exception -> 0x0165 }
            long r18 = r0.currentTimeMillis()     // Catch:{ all -> 0x015e }
            long r18 = r18 - r16
            int r3 = (r18 > r6 ? 1 : (r18 == r6 ? 0 : -1))
            if (r3 > 0) goto L_0x0131
            boolean r3 = r11.zzedo     // Catch:{ all -> 0x015e }
            if (r3 == 0) goto L_0x0098
            java.lang.Exception r0 = r11.zzeds     // Catch:{ all -> 0x0163 }
            if (r0 == 0) goto L_0x008e
            java.lang.String r1 = "badUrl"
            java.lang.Exception r0 = r11.zzeds     // Catch:{ all -> 0x0157 }
            throw r0     // Catch:{ all -> 0x0157 }
        L_0x008e:
            java.lang.String r1 = "externalAbort"
            java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x0157 }
            java.lang.String r2 = "Abort requested before buffering finished. "
            r0.<init>(r2)     // Catch:{ all -> 0x0157 }
            throw r0     // Catch:{ all -> 0x0157 }
        L_0x0098:
            boolean r3 = r11.zzedt     // Catch:{ all -> 0x015e }
            r18 = 1
            if (r3 == 0) goto L_0x00a1
            monitor-exit(r33)     // Catch:{ all -> 0x0163 }
            goto L_0x0102
        L_0x00a1:
            com.google.android.gms.internal.ads.zzbbs r3 = r11.zzedr     // Catch:{ all -> 0x015e }
            com.google.android.gms.internal.ads.zzgk r3 = r3.zzzm()     // Catch:{ all -> 0x015e }
            if (r3 == 0) goto L_0x0125
            r20 = r14
            long r14 = r3.getDuration()     // Catch:{ all -> 0x015a }
            r21 = 0
            int r8 = (r14 > r21 ? 1 : (r14 == r21 ? 0 : -1))
            if (r8 <= 0) goto L_0x0107
            long r23 = r3.getBufferedPosition()     // Catch:{ all -> 0x015a }
            int r3 = (r23 > r1 ? 1 : (r23 == r1 ? 0 : -1))
            if (r3 == 0) goto L_0x00e4
            int r1 = (r23 > r21 ? 1 : (r23 == r21 ? 0 : -1))
            if (r1 <= 0) goto L_0x00c3
            r8 = 1
            goto L_0x00c4
        L_0x00c3:
            r8 = 0
        L_0x00c4:
            int r25 = com.google.android.gms.internal.ads.zzbbs.zzzn()     // Catch:{ all -> 0x015a }
            int r26 = com.google.android.gms.internal.ads.zzbbs.zzzo()     // Catch:{ all -> 0x015a }
            r1 = r33
            r2 = r34
            r3 = r13
            r27 = r4
            r4 = r23
            r29 = r6
            r6 = r14
            r31 = r9
            r9 = r25
            r10 = r26
            r1.zza(r2, r3, r4, r6, r8, r9, r10)     // Catch:{ all -> 0x015a }
            r1 = r23
            goto L_0x00ea
        L_0x00e4:
            r27 = r4
            r29 = r6
            r31 = r9
        L_0x00ea:
            int r3 = (r23 > r14 ? 1 : (r23 == r14 ? 0 : -1))
            if (r3 < 0) goto L_0x00f3
            r11.zzb(r12, r13, r14)     // Catch:{ all -> 0x015a }
            monitor-exit(r33)     // Catch:{ all -> 0x015a }
            goto L_0x0102
        L_0x00f3:
            com.google.android.gms.internal.ads.zzbbs r3 = r11.zzedr     // Catch:{ all -> 0x015a }
            long r3 = r3.getBytesTransferred()     // Catch:{ all -> 0x015a }
            int r5 = (r3 > r27 ? 1 : (r3 == r27 ? 0 : -1))
            if (r5 < 0) goto L_0x0103
            int r3 = (r23 > r21 ? 1 : (r23 == r21 ? 0 : -1))
            if (r3 <= 0) goto L_0x0103
            monitor-exit(r33)     // Catch:{ all -> 0x015a }
        L_0x0102:
            return r18
        L_0x0103:
            r3 = r1
            r1 = r31
            goto L_0x010d
        L_0x0107:
            r27 = r4
            r29 = r6
            r3 = r1
            r1 = r9
        L_0x010d:
            r11.wait(r1)     // Catch:{ InterruptedException -> 0x011b }
            monitor-exit(r33)     // Catch:{ all -> 0x015a }
            r9 = r1
            r1 = r3
            r14 = r20
            r4 = r27
            r6 = r29
            goto L_0x0076
        L_0x011b:
            java.lang.String r1 = "interrupted"
            java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x0157 }
            java.lang.String r2 = "Wait interrupted."
            r0.<init>(r2)     // Catch:{ all -> 0x0157 }
            throw r0     // Catch:{ all -> 0x0157 }
        L_0x0125:
            r20 = r14
            java.lang.String r1 = "exoPlayerReleased"
            java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x0157 }
            java.lang.String r2 = "ExoPlayer was released during preloading."
            r0.<init>(r2)     // Catch:{ all -> 0x0157 }
            throw r0     // Catch:{ all -> 0x0157 }
        L_0x0131:
            r29 = r6
            r20 = r14
            java.lang.String r1 = "downloadTimeout"
            java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x0157 }
            r2 = 47
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0157 }
            r3.<init>(r2)     // Catch:{ all -> 0x0157 }
            java.lang.String r2 = "Timeout reached. Limit: "
            r3.append(r2)     // Catch:{ all -> 0x0157 }
            r4 = r29
            r3.append(r4)     // Catch:{ all -> 0x0157 }
            java.lang.String r2 = " ms"
            r3.append(r2)     // Catch:{ all -> 0x0157 }
            java.lang.String r2 = r3.toString()     // Catch:{ all -> 0x0157 }
            r0.<init>(r2)     // Catch:{ all -> 0x0157 }
            throw r0     // Catch:{ all -> 0x0157 }
        L_0x0157:
            r0 = move-exception
            r14 = r1
            goto L_0x0161
        L_0x015a:
            r0 = move-exception
            r14 = r20
            goto L_0x0161
        L_0x015e:
            r0 = move-exception
            r20 = r14
        L_0x0161:
            monitor-exit(r33)     // Catch:{ all -> 0x0163 }
            throw r0     // Catch:{ Exception -> 0x0020 }
        L_0x0163:
            r0 = move-exception
            goto L_0x0161
        L_0x0165:
            r0 = move-exception
            r20 = r14
        L_0x0168:
            java.lang.String r1 = r0.getMessage()
            java.lang.String r2 = java.lang.String.valueOf(r34)
            int r2 = r2.length()
            int r2 = r2 + 34
            java.lang.String r3 = java.lang.String.valueOf(r1)
            int r3 = r3.length()
            int r2 = r2 + r3
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r2)
            java.lang.String r2 = "Failed to preload url "
            r3.append(r2)
            r3.append(r12)
            java.lang.String r2 = " Exception: "
            r3.append(r2)
            r3.append(r1)
            java.lang.String r1 = r3.toString()
            com.google.android.gms.internal.ads.zzayu.zzez(r1)
            r33.release()
            java.lang.String r0 = zzb(r14, r0)
            r11.zza(r12, r13, r14, r0)
            r1 = 0
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzbcy.zze(java.lang.String, java.lang.String[]):boolean");
    }

    public final boolean zzfi(String str) {
        return zze(str, new String[]{str});
    }

    /* access modifiers changed from: protected */
    public final String zzfj(String str) {
        String valueOf = String.valueOf(super.zzfj(str));
        return valueOf.length() != 0 ? "cache:".concat(valueOf) : new String("cache:");
    }

    public final void zzn(int i2, int i3) {
    }

    public final zzbbs zzzr() {
        synchronized (this) {
            this.zzedt = true;
            notify();
        }
        this.zzedr.zza((zzbca) null);
        zzbbs zzbbs = this.zzedr;
        this.zzedr = null;
        return zzbbs;
    }

    private static String zzb(String str, Exception exc) {
        String canonicalName = exc.getClass().getCanonicalName();
        String message = exc.getMessage();
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 2 + String.valueOf(canonicalName).length() + String.valueOf(message).length());
        sb.append(str);
        sb.append(Constants.URL_PATH_DELIMITER);
        sb.append(canonicalName);
        sb.append(":");
        sb.append(message);
        return sb.toString();
    }
}
