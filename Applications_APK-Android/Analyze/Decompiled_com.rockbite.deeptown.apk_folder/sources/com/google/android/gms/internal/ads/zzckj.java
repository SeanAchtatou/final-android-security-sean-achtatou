package com.google.android.gms.internal.ads;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzckj implements zzbuv {
    private final zzcip zzfyq;

    zzckj(zzcip zzcip) {
        this.zzfyq = zzcip;
    }

    public final void zza(boolean z, Context context) {
        zzcip zzcip = this.zzfyq;
        try {
            ((zzdac) zzcip.zzddn).setImmersiveMode(z);
            ((zzdac) zzcip.zzddn).showInterstitial();
        } catch (zzdab unused) {
            zzayu.zzey("Cannot show interstitial.");
        }
    }
}
