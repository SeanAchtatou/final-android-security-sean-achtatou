package com.google.android.gms.internal.ads;

import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzaki implements zzafv {
    private final /* synthetic */ zzakd zzdbb;
    private final zzajf zzdbe;
    private final zzazl<O> zzdbf;

    public zzaki(zzakd zzakd, zzajf zzajf, zzazl<O> zzazl) {
        this.zzdbb = zzakd;
        this.zzdbe = zzajf;
        this.zzdbf = zzazl;
    }

    public final void onFailure(String str) {
        if (str == null) {
            try {
                this.zzdbf.setException(new zzajr());
            } catch (IllegalStateException unused) {
                this.zzdbe.release();
                return;
            } catch (Throwable th) {
                this.zzdbe.release();
                throw th;
            }
        } else {
            this.zzdbf.setException(new zzajr(str));
        }
        this.zzdbe.release();
    }

    public final void zzc(JSONObject jSONObject) {
        try {
            this.zzdbf.set(this.zzdbb.zzdaw.zzd(jSONObject));
        } catch (IllegalStateException unused) {
        } catch (JSONException e2) {
            this.zzdbf.setException(e2);
        } finally {
            this.zzdbe.release();
        }
    }
}
