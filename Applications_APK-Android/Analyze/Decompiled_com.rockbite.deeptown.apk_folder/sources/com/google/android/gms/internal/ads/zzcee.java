package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.doubleclick.AppEventListener;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcee {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzcee.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbsu<T>>
     arg types: [com.google.android.gms.internal.ads.zzceo, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzcee.zzc(com.google.android.gms.internal.ads.zzceo, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbsu<com.google.android.gms.internal.ads.zzbqb>>
      com.google.android.gms.internal.ads.zzcee.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbsu<T>> */
    public static Set<zzbsu<zzbph>> zza(zzceo zzceo, Executor executor) {
        return zzc((Object) zzceo, executor);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzcee.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbsu<T>>
     arg types: [com.google.android.gms.internal.ads.zzceo, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzcee.zzc(com.google.android.gms.internal.ads.zzceo, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbsu<com.google.android.gms.internal.ads.zzbqb>>
      com.google.android.gms.internal.ads.zzcee.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbsu<T>> */
    public static Set<zzbsu<AppEventListener>> zzb(zzceo zzceo, Executor executor) {
        return zzc((Object) zzceo, executor);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzcee.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbsu<T>>
     arg types: [com.google.android.gms.internal.ads.zzceo, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzcee.zzc(com.google.android.gms.internal.ads.zzceo, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbsu<com.google.android.gms.internal.ads.zzbqb>>
      com.google.android.gms.internal.ads.zzcee.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbsu<T>> */
    public static Set<zzbsu<zzbqb>> zzc(zzceo zzceo, Executor executor) {
        return zzc((Object) zzceo, executor);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzcee.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbsu<T>>
     arg types: [com.google.android.gms.internal.ads.zzceo, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzcee.zzc(com.google.android.gms.internal.ads.zzceo, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbsu<com.google.android.gms.internal.ads.zzbqb>>
      com.google.android.gms.internal.ads.zzcee.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbsu<T>> */
    public static Set<zzbsu<zzbow>> zzd(zzceo zzceo, Executor executor) {
        return zzc((Object) zzceo, executor);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzcee.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbsu<T>>
     arg types: [com.google.android.gms.internal.ads.zzceo, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzcee.zzc(com.google.android.gms.internal.ads.zzceo, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbsu<com.google.android.gms.internal.ads.zzbqb>>
      com.google.android.gms.internal.ads.zzcee.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbsu<T>> */
    public static Set<zzbsu<zzbov>> zze(zzceo zzceo, Executor executor) {
        return zzc((Object) zzceo, executor);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzcee.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbsu<T>>
     arg types: [com.google.android.gms.internal.ads.zzceo, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzcee.zzc(com.google.android.gms.internal.ads.zzceo, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbsu<com.google.android.gms.internal.ads.zzbqb>>
      com.google.android.gms.internal.ads.zzcee.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbsu<T>> */
    public static Set<zzbsu<zzbpe>> zzf(zzceo zzceo, Executor executor) {
        return zzc((Object) zzceo, executor);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzcee.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbsu<T>>
     arg types: [com.google.android.gms.internal.ads.zzceo, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzcee.zzc(com.google.android.gms.internal.ads.zzceo, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbsu<com.google.android.gms.internal.ads.zzbqb>>
      com.google.android.gms.internal.ads.zzcee.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbsu<T>> */
    public static Set<zzbsu<zzty>> zzg(zzceo zzceo, Executor executor) {
        return zzc((Object) zzceo, executor);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzcee.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbsu<T>>
     arg types: [com.google.android.gms.internal.ads.zzceo, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzcee.zzc(com.google.android.gms.internal.ads.zzceo, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbsu<com.google.android.gms.internal.ads.zzbqb>>
      com.google.android.gms.internal.ads.zzcee.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbsu<T>> */
    public static Set<zzbsu<zzdcx>> zzh(zzceo zzceo, Executor executor) {
        return zzc((Object) zzceo, executor);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzcee.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbsu<T>>
     arg types: [com.google.android.gms.internal.ads.zzceo, java.util.concurrent.Executor]
     candidates:
      com.google.android.gms.internal.ads.zzcee.zzc(com.google.android.gms.internal.ads.zzceo, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbsu<com.google.android.gms.internal.ads.zzbqb>>
      com.google.android.gms.internal.ads.zzcee.zzc(java.lang.Object, java.util.concurrent.Executor):java.util.Set<com.google.android.gms.internal.ads.zzbsu<T>> */
    public static Set<zzbsu<zzbqx>> zzi(zzceo zzceo, Executor executor) {
        return zzc((Object) zzceo, executor);
    }

    private static <T> Set<zzbsu<T>> zzc(T t, Executor executor) {
        if (zzabc.zzctx.get().booleanValue()) {
            return Collections.singleton(new zzbsu(t, executor));
        }
        return Collections.emptySet();
    }
}
