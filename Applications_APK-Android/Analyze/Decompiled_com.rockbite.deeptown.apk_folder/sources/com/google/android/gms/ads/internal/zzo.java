package com.google.android.gms.ads.internal;

import android.os.Bundle;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.internal.ads.zzaar;
import com.google.android.gms.internal.ads.zzazb;
import com.google.android.gms.internal.ads.zzug;
import java.util.Map;
import java.util.TreeMap;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzo {
    private final String zzblt;
    private final Map<String, String> zzblu = new TreeMap();
    private String zzblv;
    private String zzblw;

    public zzo(String str) {
        this.zzblt = str;
    }

    public final String getQuery() {
        return this.zzblv;
    }

    public final void zza(zzug zzug, zzazb zzazb) {
        this.zzblv = zzug.zzccd.zzblv;
        Bundle bundle = zzug.zzccf;
        Bundle bundle2 = bundle != null ? bundle.getBundle(AdMobAdapter.class.getName()) : null;
        if (bundle2 != null) {
            String str = zzaar.zzcsn.get();
            for (String next : bundle2.keySet()) {
                if (str.equals(next)) {
                    this.zzblw = bundle2.getString(next);
                } else if (next.startsWith("csa_")) {
                    this.zzblu.put(next.substring(4), bundle2.getString(next));
                }
            }
            this.zzblu.put("SDKVersion", zzazb.zzbma);
        }
    }

    public final String zzkg() {
        return this.zzblw;
    }

    public final String zzkh() {
        return this.zzblt;
    }

    public final Map<String, String> zzki() {
        return this.zzblu;
    }
}
