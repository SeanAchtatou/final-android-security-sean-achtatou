package com.google.android.gms.ads.internal.overlay;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import com.google.android.gms.ads.internal.zzg;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.ads.zzaew;
import com.google.android.gms.internal.ads.zzaey;
import com.google.android.gms.internal.ads.zzaoo;
import com.google.android.gms.internal.ads.zzaos;
import com.google.android.gms.internal.ads.zzawb;
import com.google.android.gms.internal.ads.zzawh;
import com.google.android.gms.internal.ads.zzayu;
import com.google.android.gms.internal.ads.zzbdi;
import com.google.android.gms.internal.ads.zzbdr;
import com.google.android.gms.internal.ads.zzbev;
import com.google.android.gms.internal.ads.zzsm;
import com.google.android.gms.internal.ads.zzve;
import com.google.android.gms.internal.ads.zzzn;
import java.util.Collections;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public class zzc extends zzaos implements zzy {
    @VisibleForTesting
    private static final int zzdgm = Color.argb(0, 0, 0, 0);
    @VisibleForTesting
    private boolean zzbkx = false;
    @VisibleForTesting
    zzbdi zzcza;
    @VisibleForTesting
    AdOverlayInfoParcel zzdgn;
    @VisibleForTesting
    private zzi zzdgo;
    @VisibleForTesting
    private zzq zzdgp;
    @VisibleForTesting
    private boolean zzdgq = false;
    @VisibleForTesting
    private FrameLayout zzdgr;
    @VisibleForTesting
    private WebChromeClient.CustomViewCallback zzdgs;
    @VisibleForTesting
    private boolean zzdgt = false;
    @VisibleForTesting
    private zzj zzdgu;
    @VisibleForTesting
    private boolean zzdgv = false;
    @VisibleForTesting
    int zzdgw = 0;
    private final Object zzdgx = new Object();
    private Runnable zzdgy;
    private boolean zzdgz;
    private boolean zzdha;
    private boolean zzdhb = false;
    private boolean zzdhc = false;
    private boolean zzdhd = true;
    protected final Activity zzzk;

    public zzc(Activity activity) {
        this.zzzk = activity;
    }

    private final void zzai(boolean z) {
        int intValue = ((Integer) zzve.zzoy().zzd(zzzn.zzcnv)).intValue();
        zzp zzp = new zzp();
        zzp.size = 50;
        zzp.paddingLeft = z ? intValue : 0;
        zzp.paddingRight = z ? 0 : intValue;
        zzp.paddingTop = 0;
        zzp.paddingBottom = intValue;
        this.zzdgp = new zzq(this.zzzk, zzp, this);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(z ? 11 : 9);
        zza(z, this.zzdgn.zzdhs);
        this.zzdgu.addView(this.zzdgp, layoutParams);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.ads.internal.overlay.zzc.zza(boolean, boolean):void
     arg types: [boolean, int]
     candidates:
      com.google.android.gms.ads.internal.overlay.zzc.zza(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      com.google.android.gms.ads.internal.overlay.zzc.zza(boolean, boolean):void */
    private final void zzaj(boolean z) throws zzg {
        if (!this.zzdha) {
            this.zzzk.requestWindowFeature(1);
        }
        Window window = this.zzzk.getWindow();
        if (window != null) {
            zzbdi zzbdi = this.zzdgn.zzcza;
            zzbev zzaaa = zzbdi != null ? zzbdi.zzaaa() : null;
            boolean z2 = false;
            boolean z3 = zzaaa != null && zzaaa.zzaat();
            this.zzdgv = false;
            if (z3) {
                int i2 = this.zzdgn.orientation;
                zzq.zzks();
                if (i2 == 6) {
                    if (this.zzzk.getResources().getConfiguration().orientation == 1) {
                        z2 = true;
                    }
                    this.zzdgv = z2;
                } else {
                    int i3 = this.zzdgn.orientation;
                    zzq.zzks();
                    if (i3 == 7) {
                        if (this.zzzk.getResources().getConfiguration().orientation == 2) {
                            z2 = true;
                        }
                        this.zzdgv = z2;
                    }
                }
            }
            boolean z4 = this.zzdgv;
            StringBuilder sb = new StringBuilder(46);
            sb.append("Delay onShow to next orientation change: ");
            sb.append(z4);
            zzayu.zzea(sb.toString());
            setRequestedOrientation(this.zzdgn.orientation);
            zzq.zzks();
            window.setFlags(16777216, 16777216);
            zzayu.zzea("Hardware acceleration on the AdActivity window enabled.");
            if (!this.zzbkx) {
                this.zzdgu.setBackgroundColor(-16777216);
            } else {
                this.zzdgu.setBackgroundColor(zzdgm);
            }
            this.zzzk.setContentView(this.zzdgu);
            this.zzdha = true;
            if (z) {
                try {
                    zzq.zzkr();
                    this.zzcza = zzbdr.zza(this.zzzk, this.zzdgn.zzcza != null ? this.zzdgn.zzcza.zzzy() : null, this.zzdgn.zzcza != null ? this.zzdgn.zzcza.zzzz() : null, true, z3, null, this.zzdgn.zzbll, null, null, this.zzdgn.zzcza != null ? this.zzdgn.zzcza.zzyo() : null, zzsm.zzmt(), null, false);
                    zzbev zzaaa2 = this.zzcza.zzaaa();
                    AdOverlayInfoParcel adOverlayInfoParcel = this.zzdgn;
                    zzaew zzaew = adOverlayInfoParcel.zzcwq;
                    zzaey zzaey = adOverlayInfoParcel.zzcws;
                    zzt zzt = adOverlayInfoParcel.zzdhu;
                    zzbdi zzbdi2 = adOverlayInfoParcel.zzcza;
                    zzaaa2.zza(null, zzaew, null, zzaey, zzt, true, null, zzbdi2 != null ? zzbdi2.zzaaa().zzaas() : null, null, null);
                    this.zzcza.zzaaa().zza(new zzf(this));
                    AdOverlayInfoParcel adOverlayInfoParcel2 = this.zzdgn;
                    String str = adOverlayInfoParcel2.url;
                    if (str != null) {
                        this.zzcza.loadUrl(str);
                    } else {
                        String str2 = adOverlayInfoParcel2.zzdht;
                        if (str2 != null) {
                            this.zzcza.loadDataWithBaseURL(adOverlayInfoParcel2.zzdhr, str2, "text/html", "UTF-8", null);
                        } else {
                            throw new zzg("No URL or HTML to display in ad overlay.");
                        }
                    }
                    zzbdi zzbdi3 = this.zzdgn.zzcza;
                    if (zzbdi3 != null) {
                        zzbdi3.zzb(this);
                    }
                } catch (Exception e2) {
                    zzayu.zzc("Error obtaining webview.", e2);
                    throw new zzg("Could not obtain webview for the overlay.");
                }
            } else {
                this.zzcza = this.zzdgn.zzcza;
                this.zzcza.zzbr(this.zzzk);
            }
            this.zzcza.zza(this);
            zzbdi zzbdi4 = this.zzdgn.zzcza;
            if (zzbdi4 != null) {
                zzc(zzbdi4.zzaae(), this.zzdgu);
            }
            ViewParent parent = this.zzcza.getParent();
            if (parent != null && (parent instanceof ViewGroup)) {
                ((ViewGroup) parent).removeView(this.zzcza.getView());
            }
            if (this.zzbkx) {
                this.zzcza.zzaam();
            }
            zzbdi zzbdi5 = this.zzcza;
            Activity activity = this.zzzk;
            AdOverlayInfoParcel adOverlayInfoParcel3 = this.zzdgn;
            zzbdi5.zza((ViewGroup) null, activity, adOverlayInfoParcel3.zzdhr, adOverlayInfoParcel3.zzdht);
            this.zzdgu.addView(this.zzcza.getView(), -1, -1);
            if (!z && !this.zzdgv) {
                zztr();
            }
            zzai(z3);
            if (this.zzcza.zzaac()) {
                zza(z3, true);
                return;
            }
            return;
        }
        throw new zzg("Invalid activity, no window available.");
    }

    private static void zzc(IObjectWrapper iObjectWrapper, View view) {
        if (iObjectWrapper != null && view != null) {
            zzq.zzlf().zza(iObjectWrapper, view);
        }
    }

    private final void zzto() {
        if (this.zzzk.isFinishing() && !this.zzdhb) {
            this.zzdhb = true;
            zzbdi zzbdi = this.zzcza;
            if (zzbdi != null) {
                zzbdi.zzde(this.zzdgw);
                synchronized (this.zzdgx) {
                    if (!this.zzdgz && this.zzcza.zzaai()) {
                        this.zzdgy = new zze(this);
                        zzawb.zzdsr.postDelayed(this.zzdgy, ((Long) zzve.zzoy().zzd(zzzn.zzcjg)).longValue());
                        return;
                    }
                }
            }
            zztp();
        }
    }

    private final void zztr() {
        this.zzcza.zztr();
    }

    public final void close() {
        this.zzdgw = 2;
        this.zzzk.finish();
    }

    public final void onActivityResult(int i2, int i3, Intent intent) {
    }

    public final void onBackPressed() {
        this.zzdgw = 0;
    }

    public void onCreate(Bundle bundle) {
        this.zzzk.requestWindowFeature(1);
        this.zzdgt = bundle != null && bundle.getBoolean("com.google.android.gms.ads.internal.overlay.hasResumed", false);
        try {
            this.zzdgn = AdOverlayInfoParcel.zzc(this.zzzk.getIntent());
            if (this.zzdgn != null) {
                if (this.zzdgn.zzbll.zzdwa > 7500000) {
                    this.zzdgw = 3;
                }
                if (this.zzzk.getIntent() != null) {
                    this.zzdhd = this.zzzk.getIntent().getBooleanExtra("shouldCallOnOverlayOpened", true);
                }
                if (this.zzdgn.zzdhx != null) {
                    this.zzbkx = this.zzdgn.zzdhx.zzbkx;
                } else {
                    this.zzbkx = false;
                }
                if (this.zzbkx && this.zzdgn.zzdhx.zzblc != -1) {
                    new zzl(this).zzvr();
                }
                if (bundle == null) {
                    if (this.zzdgn.zzdhq != null && this.zzdhd) {
                        this.zzdgn.zzdhq.zztf();
                    }
                    if (!(this.zzdgn.zzdhv == 1 || this.zzdgn.zzcbt == null)) {
                        this.zzdgn.zzcbt.onAdClicked();
                    }
                }
                this.zzdgu = new zzj(this.zzzk, this.zzdgn.zzdhw, this.zzdgn.zzbll.zzbma);
                this.zzdgu.setId(1000);
                zzq.zzks().zzg(this.zzzk);
                int i2 = this.zzdgn.zzdhv;
                if (i2 == 1) {
                    zzaj(false);
                } else if (i2 == 2) {
                    this.zzdgo = new zzi(this.zzdgn.zzcza);
                    zzaj(false);
                } else if (i2 == 3) {
                    zzaj(true);
                } else {
                    throw new zzg("Could not determine ad overlay type.");
                }
            } else {
                throw new zzg("Could not get info for ad overlay.");
            }
        } catch (zzg e2) {
            zzayu.zzez(e2.getMessage());
            this.zzdgw = 3;
            this.zzzk.finish();
        }
    }

    public final void onDestroy() {
        zzbdi zzbdi = this.zzcza;
        if (zzbdi != null) {
            try {
                this.zzdgu.removeView(zzbdi.getView());
            } catch (NullPointerException unused) {
            }
        }
        zzto();
    }

    public final void onPause() {
        zztk();
        zzo zzo = this.zzdgn.zzdhq;
        if (zzo != null) {
            zzo.onPause();
        }
        if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcnt)).booleanValue() && this.zzcza != null && (!this.zzzk.isFinishing() || this.zzdgo == null)) {
            zzq.zzks();
            zzawh.zza(this.zzcza);
        }
        zzto();
    }

    public final void onRestart() {
    }

    public final void onResume() {
        zzo zzo = this.zzdgn.zzdhq;
        if (zzo != null) {
            zzo.onResume();
        }
        zza(this.zzzk.getResources().getConfiguration());
        if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcnt)).booleanValue()) {
            zzbdi zzbdi = this.zzcza;
            if (zzbdi == null || zzbdi.isDestroyed()) {
                zzayu.zzez("The webview does not exist. Ignoring action.");
                return;
            }
            zzq.zzks();
            zzawh.zzb(this.zzcza);
        }
    }

    public final void onSaveInstanceState(Bundle bundle) {
        bundle.putBoolean("com.google.android.gms.ads.internal.overlay.hasResumed", this.zzdgt);
    }

    public final void onStart() {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcnt)).booleanValue()) {
            zzbdi zzbdi = this.zzcza;
            if (zzbdi == null || zzbdi.isDestroyed()) {
                zzayu.zzez("The webview does not exist. Ignoring action.");
                return;
            }
            zzq.zzks();
            zzawh.zzb(this.zzcza);
        }
    }

    public final void onStop() {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcnt)).booleanValue() && this.zzcza != null && (!this.zzzk.isFinishing() || this.zzdgo == null)) {
            zzq.zzks();
            zzawh.zza(this.zzcza);
        }
        zzto();
    }

    public final void setRequestedOrientation(int i2) {
        if (this.zzzk.getApplicationInfo().targetSdkVersion >= ((Integer) zzve.zzoy().zzd(zzzn.zzcpr)).intValue()) {
            if (this.zzzk.getApplicationInfo().targetSdkVersion <= ((Integer) zzve.zzoy().zzd(zzzn.zzcps)).intValue()) {
                if (Build.VERSION.SDK_INT >= ((Integer) zzve.zzoy().zzd(zzzn.zzcpt)).intValue()) {
                    if (Build.VERSION.SDK_INT <= ((Integer) zzve.zzoy().zzd(zzzn.zzcpu)).intValue()) {
                        return;
                    }
                }
            }
        }
        try {
            this.zzzk.setRequestedOrientation(i2);
        } catch (Throwable th) {
            zzq.zzku().zzb(th, "AdOverlay.setRequestedOrientation");
        }
    }

    public final void zza(boolean z, boolean z2) {
        AdOverlayInfoParcel adOverlayInfoParcel;
        zzg zzg;
        AdOverlayInfoParcel adOverlayInfoParcel2;
        zzg zzg2;
        boolean z3 = true;
        boolean z4 = ((Boolean) zzve.zzoy().zzd(zzzn.zzcjh)).booleanValue() && (adOverlayInfoParcel2 = this.zzdgn) != null && (zzg2 = adOverlayInfoParcel2.zzdhx) != null && zzg2.zzble;
        boolean z5 = ((Boolean) zzve.zzoy().zzd(zzzn.zzcji)).booleanValue() && (adOverlayInfoParcel = this.zzdgn) != null && (zzg = adOverlayInfoParcel.zzdhx) != null && zzg.zzblf;
        if (z && z2 && z4 && !z5) {
            new zzaoo(this.zzcza, "useCustomClose").zzds("Custom close has been disabled for interstitial ads in this ad slot.");
        }
        zzq zzq = this.zzdgp;
        if (zzq != null) {
            if (!z5 && (!z2 || z4)) {
                z3 = false;
            }
            zzq.zzal(z3);
        }
    }

    public final void zzad(IObjectWrapper iObjectWrapper) {
        zza((Configuration) ObjectWrapper.unwrap(iObjectWrapper));
    }

    public final void zzdf() {
        this.zzdha = true;
    }

    public final void zztk() {
        AdOverlayInfoParcel adOverlayInfoParcel = this.zzdgn;
        if (adOverlayInfoParcel != null && this.zzdgq) {
            setRequestedOrientation(adOverlayInfoParcel.orientation);
        }
        if (this.zzdgr != null) {
            this.zzzk.setContentView(this.zzdgu);
            this.zzdha = true;
            this.zzdgr.removeAllViews();
            this.zzdgr = null;
        }
        WebChromeClient.CustomViewCallback customViewCallback = this.zzdgs;
        if (customViewCallback != null) {
            customViewCallback.onCustomViewHidden();
            this.zzdgs = null;
        }
        this.zzdgq = false;
    }

    public final void zztl() {
        this.zzdgw = 1;
        this.zzzk.finish();
    }

    public final boolean zztm() {
        this.zzdgw = 0;
        zzbdi zzbdi = this.zzcza;
        if (zzbdi == null) {
            return true;
        }
        boolean zzaah = zzbdi.zzaah();
        if (!zzaah) {
            this.zzcza.zza("onbackblocked", Collections.emptyMap());
        }
        return zzaah;
    }

    public final void zztn() {
        this.zzdgu.removeView(this.zzdgp);
        zzai(true);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    public final void zztp() {
        zzbdi zzbdi;
        zzo zzo;
        if (!this.zzdhc) {
            this.zzdhc = true;
            zzbdi zzbdi2 = this.zzcza;
            if (zzbdi2 != null) {
                this.zzdgu.removeView(zzbdi2.getView());
                zzi zzi = this.zzdgo;
                if (zzi != null) {
                    this.zzcza.zzbr(zzi.zzup);
                    this.zzcza.zzax(false);
                    ViewGroup viewGroup = this.zzdgo.parent;
                    View view = this.zzcza.getView();
                    zzi zzi2 = this.zzdgo;
                    viewGroup.addView(view, zzi2.index, zzi2.zzdhj);
                    this.zzdgo = null;
                } else if (this.zzzk.getApplicationContext() != null) {
                    this.zzcza.zzbr(this.zzzk.getApplicationContext());
                }
                this.zzcza = null;
            }
            AdOverlayInfoParcel adOverlayInfoParcel = this.zzdgn;
            if (!(adOverlayInfoParcel == null || (zzo = adOverlayInfoParcel.zzdhq) == null)) {
                zzo.zzte();
            }
            AdOverlayInfoParcel adOverlayInfoParcel2 = this.zzdgn;
            if (adOverlayInfoParcel2 != null && (zzbdi = adOverlayInfoParcel2.zzcza) != null) {
                zzc(zzbdi.zzaae(), this.zzdgn.zzcza.getView());
            }
        }
    }

    public final void zztq() {
        if (this.zzdgv) {
            this.zzdgv = false;
            zztr();
        }
    }

    public final void zzts() {
        this.zzdgu.zzdhl = true;
    }

    public final void zztt() {
        synchronized (this.zzdgx) {
            this.zzdgz = true;
            if (this.zzdgy != null) {
                zzawb.zzdsr.removeCallbacks(this.zzdgy);
                zzawb.zzdsr.post(this.zzdgy);
            }
        }
    }

    public final void zza(View view, WebChromeClient.CustomViewCallback customViewCallback) {
        this.zzdgr = new FrameLayout(this.zzzk);
        this.zzdgr.setBackgroundColor(-16777216);
        this.zzdgr.addView(view, -1, -1);
        this.zzzk.setContentView(this.zzdgr);
        this.zzdha = true;
        this.zzdgs = customViewCallback;
        this.zzdgq = true;
    }

    private final void zza(Configuration configuration) {
        AdOverlayInfoParcel adOverlayInfoParcel;
        zzg zzg;
        zzg zzg2;
        AdOverlayInfoParcel adOverlayInfoParcel2 = this.zzdgn;
        boolean z = true;
        boolean z2 = false;
        boolean z3 = (adOverlayInfoParcel2 == null || (zzg2 = adOverlayInfoParcel2.zzdhx) == null || !zzg2.zzbky) ? false : true;
        boolean zza = zzq.zzks().zza(this.zzzk, configuration);
        if ((this.zzbkx && !z3) || zza) {
            z = false;
        } else if (Build.VERSION.SDK_INT >= 19 && (adOverlayInfoParcel = this.zzdgn) != null && (zzg = adOverlayInfoParcel.zzdhx) != null && zzg.zzbld) {
            z2 = true;
        }
        Window window = this.zzzk.getWindow();
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcjj)).booleanValue() && Build.VERSION.SDK_INT >= 19) {
            View decorView = window.getDecorView();
            int i2 = 256;
            if (z) {
                i2 = 5380;
                if (z2) {
                    i2 = 5894;
                }
            }
            decorView.setSystemUiVisibility(i2);
        } else if (z) {
            window.addFlags(1024);
            window.clearFlags(2048);
            if (Build.VERSION.SDK_INT >= 19 && z2) {
                window.getDecorView().setSystemUiVisibility(4098);
            }
        } else {
            window.addFlags(2048);
            window.clearFlags(1024);
        }
    }
}
