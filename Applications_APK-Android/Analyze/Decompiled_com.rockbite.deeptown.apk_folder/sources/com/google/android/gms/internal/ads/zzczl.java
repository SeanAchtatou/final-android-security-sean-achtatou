package com.google.android.gms.internal.ads;

import android.util.JsonReader;
import com.facebook.appevents.AppEventsConstants;
import com.google.android.gms.ads.internal.zzq;
import com.tapjoy.TJAdUnitConstants;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzczl {
    public final String zzaez;
    public final boolean zzblf;
    public final List<String> zzdbq;
    public final List<String> zzdbr;
    public final String zzdbw;
    public final boolean zzdcd;
    public final boolean zzdce;
    public final boolean zzdcf;
    public final String zzdcm;
    public final String zzdcx;
    public final String zzdcy;
    public final String zzdem;
    public final List<String> zzdkm;
    public final String zzdkp;
    public final String zzdks;
    public final zzasd zzdky;
    public final List<String> zzdkz;
    public final List<String> zzdla;
    public final boolean zzdli;
    public final boolean zzdll;
    public final boolean zzdlm;
    public final boolean zzdmf;
    public final boolean zzega;
    public final String zzeif;
    public final int zzfdp;
    public final String zzfhk;
    public final int zzfjj;
    public final List<String> zzgli;
    public final int zzglj;
    public final List<String> zzglk;
    public final List<String> zzgll;
    public final List<String> zzglm;
    public final List<zzczk> zzgln;
    public final zzczp zzglo;
    public final List<String> zzglp;
    public final List<zzczk> zzglq;
    public final JSONObject zzglr;
    public final zzatn zzgls;
    public final JSONObject zzglt;
    public final JSONObject zzglu;
    public final boolean zzglv;
    public final int zzglw;
    public final int zzglx;
    public final JSONObject zzgly;
    public final int zzglz;
    public final boolean zzgma;

    zzczl(JsonReader jsonReader) throws IllegalStateException, IOException, JSONException, NumberFormatException {
        String str;
        List<String> list;
        char c2;
        List<String> emptyList = Collections.emptyList();
        List<String> emptyList2 = Collections.emptyList();
        List<String> emptyList3 = Collections.emptyList();
        List<String> emptyList4 = Collections.emptyList();
        List<String> emptyList5 = Collections.emptyList();
        Collections.emptyList();
        List<String> emptyList6 = Collections.emptyList();
        List<String> emptyList7 = Collections.emptyList();
        List<String> emptyList8 = Collections.emptyList();
        List<String> emptyList9 = Collections.emptyList();
        List<zzczk> emptyList10 = Collections.emptyList();
        List<String> emptyList11 = Collections.emptyList();
        List<zzczk> emptyList12 = Collections.emptyList();
        JSONObject jSONObject = new JSONObject();
        JSONObject jSONObject2 = new JSONObject();
        JSONObject jSONObject3 = new JSONObject();
        JSONObject jSONObject4 = new JSONObject();
        jsonReader.beginObject();
        List<String> list2 = emptyList11;
        List<zzczk> list3 = emptyList12;
        JSONObject jSONObject5 = jSONObject;
        JSONObject jSONObject6 = jSONObject2;
        JSONObject jSONObject7 = jSONObject3;
        JSONObject jSONObject8 = jSONObject4;
        zzasd zzasd = null;
        zzczp zzczp = null;
        zzatn zzatn = null;
        String str2 = "";
        String str3 = str2;
        String str4 = str3;
        String str5 = str4;
        String str6 = str5;
        String str7 = str6;
        String str8 = str7;
        String str9 = str8;
        String str10 = str9;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        boolean z4 = false;
        boolean z5 = false;
        boolean z6 = false;
        int i2 = -1;
        int i3 = 0;
        boolean z7 = false;
        boolean z8 = false;
        boolean z9 = false;
        int i4 = 0;
        boolean z10 = false;
        int i5 = -1;
        boolean z11 = false;
        List<String> list4 = emptyList8;
        List<String> list5 = emptyList9;
        List<zzczk> list6 = emptyList10;
        String str11 = str10;
        List<String> list7 = emptyList6;
        List<String> list8 = emptyList7;
        int i6 = 0;
        List<String> list9 = emptyList5;
        List<String> list10 = emptyList4;
        List<String> list11 = emptyList3;
        List<String> list12 = emptyList2;
        int i7 = 0;
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            if (nextName == null) {
                str = "";
            } else {
                str = nextName;
            }
            List<zzczk> list13 = list6;
            switch (str.hashCode()) {
                case -1980587809:
                    list = list5;
                    if (str.equals("debug_signals")) {
                        c2 = 26;
                        break;
                    }
                    c2 = 65535;
                    break;
                case -1965512151:
                    list = list5;
                    if (str.equals("omid_settings")) {
                        c2 = '&';
                        break;
                    }
                    c2 = 65535;
                    break;
                case -1620470467:
                    list = list5;
                    if (str.equals("backend_query_id")) {
                        c2 = '-';
                        break;
                    }
                    c2 = 65535;
                    break;
                case -1440104884:
                    list = list5;
                    if (str.equals("is_custom_close_blocked")) {
                        c2 = ' ';
                        break;
                    }
                    c2 = 65535;
                    break;
                case -1439500848:
                    list = list5;
                    if (str.equals(TJAdUnitConstants.String.ORIENTATION)) {
                        c2 = '\"';
                        break;
                    }
                    c2 = 65535;
                    break;
                case -1428969291:
                    list = list5;
                    if (str.equals("enable_omid")) {
                        c2 = '$';
                        break;
                    }
                    c2 = 65535;
                    break;
                case -1403779768:
                    list = list5;
                    if (str.equals("showable_impression_type")) {
                        c2 = ')';
                        break;
                    }
                    c2 = 65535;
                    break;
                case -1360811658:
                    list = list5;
                    if (str.equals("ad_sizes")) {
                        c2 = 17;
                        break;
                    }
                    c2 = 65535;
                    break;
                case -1306015996:
                    list = list5;
                    if (str.equals("adapters")) {
                        c2 = 18;
                        break;
                    }
                    c2 = 65535;
                    break;
                case -1289032093:
                    list = list5;
                    if (str.equals("extras")) {
                        c2 = 27;
                        break;
                    }
                    c2 = 65535;
                    break;
                case -1234181075:
                    list = list5;
                    if (str.equals("allow_pub_rendered_attribution")) {
                        c2 = 28;
                        break;
                    }
                    c2 = 65535;
                    break;
                case -1181000426:
                    list = list5;
                    if (str.equals("is_augmented_reality_ad")) {
                        c2 = '*';
                        break;
                    }
                    c2 = 65535;
                    break;
                case -1152230954:
                    list = list5;
                    if (str.equals(AppEventsConstants.EVENT_PARAM_AD_TYPE)) {
                        c2 = 1;
                        break;
                    }
                    c2 = 65535;
                    break;
                case -1146534047:
                    list = list5;
                    if (str.equals("is_scroll_aware")) {
                        c2 = '(';
                        break;
                    }
                    c2 = 65535;
                    break;
                case -1115838944:
                    list = list5;
                    if (str.equals("fill_urls")) {
                        c2 = 13;
                        break;
                    }
                    c2 = 65535;
                    break;
                case -1081936678:
                    list = list5;
                    if (str.equals("allocation_id")) {
                        c2 = 19;
                        break;
                    }
                    c2 = 65535;
                    break;
                case -1078050970:
                    list = list5;
                    if (str.equals("video_complete_urls")) {
                        c2 = 9;
                        break;
                    }
                    c2 = 65535;
                    break;
                case -1051269058:
                    list = list5;
                    if (str.equals("active_view")) {
                        c2 = 23;
                        break;
                    }
                    c2 = 65535;
                    break;
                case -982608540:
                    list = list5;
                    if (str.equals("valid_from_timestamp")) {
                        c2 = 11;
                        break;
                    }
                    c2 = 65535;
                    break;
                case -776859333:
                    list = list5;
                    if (str.equals("click_urls")) {
                        c2 = 2;
                        break;
                    }
                    c2 = 65535;
                    break;
                case -544216775:
                    list = list5;
                    if (str.equals("safe_browsing")) {
                        c2 = 24;
                        break;
                    }
                    c2 = 65535;
                    break;
                case -437057161:
                    list = list5;
                    if (str.equals("imp_urls")) {
                        c2 = 3;
                        break;
                    }
                    c2 = 65535;
                    break;
                case -404326515:
                    list = list5;
                    if (str.equals("render_timeout_ms")) {
                        c2 = '#';
                        break;
                    }
                    c2 = 65535;
                    break;
                case -397704715:
                    list = list5;
                    if (str.equals("ad_close_time_ms")) {
                        c2 = '+';
                        break;
                    }
                    c2 = 65535;
                    break;
                case -213424028:
                    list = list5;
                    if (str.equals("watermark")) {
                        c2 = ',';
                        break;
                    }
                    c2 = 65535;
                    break;
                case -29338502:
                    list = list5;
                    if (str.equals("allow_custom_click_gesture")) {
                        c2 = 30;
                        break;
                    }
                    c2 = 65535;
                    break;
                case 3107:
                    list = list5;
                    if (str.equals("ad")) {
                        c2 = 16;
                        break;
                    }
                    c2 = 65535;
                    break;
                case 3355:
                    list = list5;
                    if (str.equals("id")) {
                        c2 = 21;
                        break;
                    }
                    c2 = 65535;
                    break;
                case 3076010:
                    list = list5;
                    if (str.equals(TJAdUnitConstants.String.DATA)) {
                        c2 = 20;
                        break;
                    }
                    c2 = 65535;
                    break;
                case 63195984:
                    list = list5;
                    if (str.equals("render_test_label")) {
                        c2 = 31;
                        break;
                    }
                    c2 = 65535;
                    break;
                case 107433883:
                    list = list5;
                    if (str.equals("qdata")) {
                        c2 = 22;
                        break;
                    }
                    c2 = 65535;
                    break;
                case 230323073:
                    list = list5;
                    if (str.equals("ad_load_urls")) {
                        c2 = 4;
                        break;
                    }
                    c2 = 65535;
                    break;
                case 418392395:
                    list = list5;
                    if (str.equals("is_closable_area_disabled")) {
                        c2 = '!';
                        break;
                    }
                    c2 = 65535;
                    break;
                case 597473788:
                    list = list5;
                    if (str.equals("debug_dialog_string")) {
                        c2 = 25;
                        break;
                    }
                    c2 = 65535;
                    break;
                case 673261304:
                    list = list5;
                    if (str.equals("reward_granted_urls")) {
                        c2 = 7;
                        break;
                    }
                    c2 = 65535;
                    break;
                case 754887508:
                    list = list5;
                    if (str.equals("container_sizes")) {
                        c2 = 15;
                        break;
                    }
                    c2 = 65535;
                    break;
                case 791122864:
                    list = list5;
                    if (str.equals("impression_type")) {
                        c2 = 5;
                        break;
                    }
                    c2 = 65535;
                    break;
                case 1010584092:
                    list = list5;
                    if (str.equals("transaction_id")) {
                        c2 = 10;
                        break;
                    }
                    c2 = 65535;
                    break;
                case 1100650276:
                    list = list5;
                    if (str.equals("rewards")) {
                        c2 = 12;
                        break;
                    }
                    c2 = 65535;
                    break;
                case 1321720943:
                    list = list5;
                    if (str.equals("allow_pub_owned_ad_view")) {
                        c2 = 29;
                        break;
                    }
                    c2 = 65535;
                    break;
                case 1637553475:
                    list = list5;
                    if (str.equals("bid_response")) {
                        c2 = '%';
                        break;
                    }
                    c2 = 65535;
                    break;
                case 1638957285:
                    list = list5;
                    if (str.equals("video_start_urls")) {
                        c2 = 6;
                        break;
                    }
                    c2 = 65535;
                    break;
                case 1688341040:
                    list = list5;
                    if (str.equals("video_reward_urls")) {
                        c2 = 8;
                        break;
                    }
                    c2 = 65535;
                    break;
                case 1799285870:
                    list = list5;
                    if (str.equals("use_third_party_container_height")) {
                        c2 = '.';
                        break;
                    }
                    c2 = 65535;
                    break;
                case 1839650832:
                    list = list5;
                    if (str.equals("renderers")) {
                        c2 = 0;
                        break;
                    }
                    c2 = 65535;
                    break;
                case 1875425491:
                    list = list5;
                    if (str.equals("is_analytics_logging_enabled")) {
                        c2 = '\'';
                        break;
                    }
                    c2 = 65535;
                    break;
                case 2072888499:
                    list = list5;
                    if (str.equals("manual_tracking_urls")) {
                        c2 = 14;
                        break;
                    }
                    c2 = 65535;
                    break;
                default:
                    list = list5;
                    c2 = 65535;
                    break;
            }
            switch (c2) {
                case 0:
                    emptyList = zzaxs.zza(jsonReader);
                    list6 = list13;
                    break;
                case 1:
                    String nextString = jsonReader.nextString();
                    if ("banner".equals(nextString)) {
                        i7 = 1;
                    } else if ("interstitial".equals(nextString)) {
                        i7 = 2;
                    } else if ("native_express".equals(nextString)) {
                        i7 = 3;
                    } else if ("native".equals(nextString)) {
                        i7 = 4;
                    } else {
                        i7 = "rewarded".equals(nextString) ? 5 : 0;
                    }
                    list6 = list13;
                    break;
                case 2:
                    list12 = zzaxs.zza(jsonReader);
                    list6 = list13;
                    break;
                case 3:
                    list11 = zzaxs.zza(jsonReader);
                    list6 = list13;
                    break;
                case 4:
                    list10 = zzaxs.zza(jsonReader);
                    list6 = list13;
                    break;
                case 5:
                    i6 = jsonReader.nextInt();
                    if (!(i6 == 0 || i6 == 1)) {
                        i6 = 0;
                    }
                    list6 = list13;
                    break;
                case 6:
                    list9 = zzaxs.zza(jsonReader);
                    list6 = list13;
                    break;
                case 7:
                    zzaxs.zza(jsonReader);
                    list6 = list13;
                    break;
                case 8:
                    list7 = zzaxs.zza(jsonReader);
                    list6 = list13;
                    break;
                case 9:
                    list8 = zzaxs.zza(jsonReader);
                    list6 = list13;
                    break;
                case 10:
                    str11 = jsonReader.nextString();
                    list6 = list13;
                    break;
                case 11:
                    str2 = jsonReader.nextString();
                    list6 = list13;
                    break;
                case 12:
                    zzasd = zzasd.zza(zzaxs.zzd(jsonReader));
                    list6 = list13;
                    break;
                case 13:
                    list4 = zzaxs.zza(jsonReader);
                    list6 = list13;
                    break;
                case 14:
                    list5 = zzaxs.zza(jsonReader);
                    list6 = list13;
                    continue;
                case 15:
                    list6 = zzczk.zze(jsonReader);
                    break;
                case 16:
                    zzczp = new zzczp(jsonReader);
                    list6 = list13;
                    break;
                case 17:
                    list3 = zzczk.zze(jsonReader);
                    list6 = list13;
                    break;
                case 18:
                    list2 = zzaxs.zza(jsonReader);
                    list6 = list13;
                    break;
                case 19:
                    str3 = jsonReader.nextString();
                    list6 = list13;
                    break;
                case 20:
                    jSONObject5 = zzaxs.zzc(jsonReader);
                    list6 = list13;
                    break;
                case 21:
                    str4 = jsonReader.nextString();
                    list6 = list13;
                    break;
                case 22:
                    str5 = jsonReader.nextString();
                    list6 = list13;
                    break;
                case 23:
                    str6 = zzaxs.zzc(jsonReader).toString();
                    list6 = list13;
                    break;
                case 24:
                    zzatn = zzatn.zzg(zzaxs.zzc(jsonReader));
                    list6 = list13;
                    break;
                case 25:
                    str7 = jsonReader.nextString();
                    list6 = list13;
                    break;
                case 26:
                    jSONObject6 = zzaxs.zzc(jsonReader);
                    list6 = list13;
                    break;
                case 27:
                    jSONObject7 = zzaxs.zzc(jsonReader);
                    list6 = list13;
                    break;
                case 28:
                    z = jsonReader.nextBoolean();
                    list6 = list13;
                    break;
                case 29:
                    z2 = jsonReader.nextBoolean();
                    list6 = list13;
                    break;
                case 30:
                    z3 = jsonReader.nextBoolean();
                    list6 = list13;
                    break;
                case 31:
                    z4 = jsonReader.nextBoolean();
                    list6 = list13;
                    break;
                case ' ':
                    z5 = jsonReader.nextBoolean();
                    list6 = list13;
                    break;
                case '!':
                    z6 = jsonReader.nextBoolean();
                    list6 = list13;
                    break;
                case '\"':
                    String nextString2 = jsonReader.nextString();
                    if (TJAdUnitConstants.String.LANDSCAPE.equalsIgnoreCase(nextString2)) {
                        zzq.zzks();
                        i2 = 6;
                    } else if (TJAdUnitConstants.String.PORTRAIT.equalsIgnoreCase(nextString2)) {
                        zzq.zzks();
                        i2 = 7;
                    } else {
                        i2 = -1;
                    }
                    list6 = list13;
                    break;
                case '#':
                    i3 = jsonReader.nextInt();
                    list6 = list13;
                    break;
                case '$':
                    z7 = jsonReader.nextBoolean();
                    list6 = list13;
                    break;
                case '%':
                    str8 = jsonReader.nextString();
                    list6 = list13;
                    break;
                case '&':
                    jSONObject8 = zzaxs.zzc(jsonReader);
                    list6 = list13;
                    break;
                case '\'':
                    z8 = jsonReader.nextBoolean();
                    list6 = list13;
                    break;
                case '(':
                    z9 = jsonReader.nextBoolean();
                    list6 = list13;
                    break;
                case ')':
                    i4 = jsonReader.nextInt();
                    list6 = list13;
                    break;
                case '*':
                    z10 = jsonReader.nextBoolean();
                    list6 = list13;
                    break;
                case '+':
                    i5 = jsonReader.nextInt();
                    list6 = list13;
                    break;
                case ',':
                    str9 = jsonReader.nextString();
                    list6 = list13;
                    break;
                case '-':
                    str10 = jsonReader.nextString();
                    list6 = list13;
                    break;
                case '.':
                    z11 = jsonReader.nextBoolean();
                    list6 = list13;
                    break;
                default:
                    jsonReader.skipValue();
                    list6 = list13;
                    break;
            }
            list5 = list;
        }
        jsonReader.endObject();
        this.zzgli = emptyList;
        this.zzfjj = i7;
        this.zzdbq = list12;
        this.zzdbr = list11;
        this.zzglk = list10;
        this.zzglj = i6;
        this.zzdkz = list9;
        this.zzdla = list7;
        this.zzgll = list8;
        this.zzdcx = str11;
        this.zzdcy = str2;
        this.zzdky = zzasd;
        this.zzglm = list4;
        this.zzdkm = list5;
        this.zzgln = list6;
        this.zzglo = zzczp;
        this.zzglp = list2;
        this.zzglq = list3;
        this.zzdcm = str3;
        this.zzglr = jSONObject5;
        this.zzaez = str4;
        this.zzdbw = str5;
        this.zzdks = str6;
        this.zzgls = zzatn;
        this.zzdkp = str7;
        this.zzglt = jSONObject6;
        this.zzglu = jSONObject7;
        this.zzdcd = z;
        this.zzdce = z2;
        this.zzdcf = z3;
        this.zzdmf = z4;
        this.zzglv = z5;
        this.zzblf = z6;
        this.zzglw = i2;
        this.zzglx = i3;
        this.zzdli = z7;
        this.zzeif = str8;
        this.zzgly = jSONObject8;
        this.zzdll = z8;
        this.zzdlm = z9;
        this.zzglz = i4;
        this.zzega = z10;
        this.zzdem = str9;
        this.zzfdp = i5;
        this.zzfhk = str10;
        this.zzgma = z11;
    }
}
