package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.overlay.zzd;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzbep {
    void zza(zzd zzd);

    void zza(boolean z, int i2, String str);

    void zza(boolean z, int i2, String str, String str2);

    void zzc(boolean z, int i2);
}
