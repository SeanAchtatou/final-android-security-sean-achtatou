package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzsy;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzccy implements zzsp {
    private final zzsy.zza zzfhy;

    zzccy(zzsy.zza zza) {
        this.zzfhy = zza;
    }

    public final void zza(zztu zztu) {
        zztu.zzcbb = this.zzfhy;
    }
}
