package com.google.android.gms.common.util;

import java.util.regex.Pattern;

public final class zzb {
    private static Pattern zzgx;

    public static int zzc(int i2) {
        if (i2 == -1) {
            return -1;
        }
        return i2 / 1000;
    }
}
