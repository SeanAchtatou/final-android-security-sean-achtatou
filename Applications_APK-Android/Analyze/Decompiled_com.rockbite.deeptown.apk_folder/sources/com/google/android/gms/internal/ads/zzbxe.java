package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbxe {
    private final zzbws zzfkc;

    public zzbxe(zzbws zzbws) {
        this.zzfkc = zzbws;
    }

    public final zzbws zzajx() {
        return this.zzfkc;
    }
}
