package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbob;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcxr<RequestComponentT extends zzbob<AdT>, AdT> implements zzcxt<RequestComponentT, AdT> {
    private RequestComponentT zzgjx;

    public final zzdhe<AdT> zza(zzcxs zzcxs, zzcxv<RequestComponentT> zzcxv) {
        this.zzgjx = (zzbob) zzcxv.zzc(zzcxs).zzadg();
        return this.zzgjx.zzadc().zzaha();
    }

    public final /* synthetic */ Object zzaog() {
        return this.zzgjx;
    }
}
