package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcwh implements zzcty<JSONObject> {
    private final Map<String, Object> zzgik;

    public zzcwh(Map<String, Object> map) {
        this.zzgik = map;
    }

    public final /* synthetic */ void zzr(Object obj) {
        try {
            ((JSONObject) obj).put("video_decoders", zzq.zzkq().zzi(this.zzgik));
        } catch (JSONException e2) {
            String valueOf = String.valueOf(e2.getMessage());
            zzavs.zzed(valueOf.length() != 0 ? "Could not encode video decoder properties: ".concat(valueOf) : new String("Could not encode video decoder properties: "));
        }
    }
}
