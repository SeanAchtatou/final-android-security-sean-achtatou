package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcsi implements Callable {
    private final zzcsj zzgfy;

    zzcsi(zzcsj zzcsj) {
        this.zzgfy = zzcsj;
    }

    public final Object call() {
        return this.zzgfy.zzani();
    }
}
