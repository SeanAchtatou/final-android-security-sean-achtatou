package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzso;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcdd implements zzdxg<zzcdh> {
    private static final zzcdd zzfse = new zzcdd();

    public static zzcdd zzald() {
        return zzfse;
    }

    public final /* synthetic */ Object get() {
        return (zzcdh) zzdxm.zza(new zzcdh(zzso.zza.C0151zza.REQUEST_WILL_PROCESS_RESPONSE, zzso.zza.C0151zza.REQUEST_DID_PROCESS_RESPONSE, zzso.zza.C0151zza.REQUEST_FAILED_TO_PROCESS_RESPONSE), "Cannot return null from a non-@Nullable @Provides method");
    }
}
