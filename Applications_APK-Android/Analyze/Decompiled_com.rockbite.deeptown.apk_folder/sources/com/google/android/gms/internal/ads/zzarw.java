package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.reward.AdMetadataListener;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.dynamic.ObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzarw implements RewardedVideoAd {
    private final Object lock = new Object();
    private final zzarl zzdnt;
    private final zzarv zzdnu = new zzarv(null);
    private String zzdnv;
    private String zzdnw;
    private final Context zzup;

    public zzarw(Context context, zzarl zzarl) {
        this.zzdnt = zzarl == null ? new zzyl() : zzarl;
        this.zzup = context.getApplicationContext();
    }

    private final void zza(String str, zzxj zzxj) {
        synchronized (this.lock) {
            if (this.zzdnt != null) {
                try {
                    this.zzdnt.zza(zzuh.zza(this.zzup, zzxj, str));
                } catch (RemoteException e2) {
                    zzayu.zze("#007 Could not call remote method.", e2);
                }
            }
        }
    }

    public final void destroy() {
        destroy(null);
    }

    public final Bundle getAdMetadata() {
        synchronized (this.lock) {
            if (this.zzdnt != null) {
                try {
                    Bundle adMetadata = this.zzdnt.getAdMetadata();
                    return adMetadata;
                } catch (RemoteException e2) {
                    zzayu.zze("#007 Could not call remote method.", e2);
                }
            }
            Bundle bundle = new Bundle();
            return bundle;
        }
    }

    public final String getCustomData() {
        String str;
        synchronized (this.lock) {
            str = this.zzdnw;
        }
        return str;
    }

    public final String getMediationAdapterClassName() {
        try {
            if (this.zzdnt != null) {
                return this.zzdnt.getMediationAdapterClassName();
            }
            return null;
        } catch (RemoteException e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
            return null;
        }
    }

    public final RewardedVideoAdListener getRewardedVideoAdListener() {
        RewardedVideoAdListener rewardedVideoAdListener;
        synchronized (this.lock) {
            rewardedVideoAdListener = this.zzdnu.getRewardedVideoAdListener();
        }
        return rewardedVideoAdListener;
    }

    public final String getUserId() {
        String str;
        synchronized (this.lock) {
            str = this.zzdnv;
        }
        return str;
    }

    public final boolean isLoaded() {
        synchronized (this.lock) {
            if (this.zzdnt == null) {
                return false;
            }
            try {
                boolean isLoaded = this.zzdnt.isLoaded();
                return isLoaded;
            } catch (RemoteException e2) {
                zzayu.zze("#007 Could not call remote method.", e2);
                return false;
            }
        }
    }

    public final void loadAd(String str, AdRequest adRequest) {
        zza(str, adRequest.zzdg());
    }

    public final void pause() {
        pause(null);
    }

    public final void resume() {
        resume(null);
    }

    public final void setAdMetadataListener(AdMetadataListener adMetadataListener) {
        synchronized (this.lock) {
            if (this.zzdnt != null) {
                try {
                    this.zzdnt.zza(new zzud(adMetadataListener));
                } catch (RemoteException e2) {
                    zzayu.zze("#007 Could not call remote method.", e2);
                }
            }
        }
    }

    public final void setCustomData(String str) {
        synchronized (this.lock) {
            if (this.zzdnt != null) {
                try {
                    this.zzdnt.setCustomData(str);
                    this.zzdnw = str;
                } catch (RemoteException e2) {
                    zzayu.zze("#007 Could not call remote method.", e2);
                }
            }
        }
    }

    public final void setImmersiveMode(boolean z) {
        synchronized (this.lock) {
            if (this.zzdnt != null) {
                try {
                    this.zzdnt.setImmersiveMode(z);
                } catch (RemoteException e2) {
                    zzayu.zze("#007 Could not call remote method.", e2);
                }
            }
        }
    }

    public final void setRewardedVideoAdListener(RewardedVideoAdListener rewardedVideoAdListener) {
        synchronized (this.lock) {
            this.zzdnu.setRewardedVideoAdListener(rewardedVideoAdListener);
            if (this.zzdnt != null) {
                try {
                    this.zzdnt.zza(this.zzdnu);
                } catch (RemoteException e2) {
                    zzayu.zze("#007 Could not call remote method.", e2);
                }
            }
        }
    }

    public final void setUserId(String str) {
        synchronized (this.lock) {
            this.zzdnv = str;
            if (this.zzdnt != null) {
                try {
                    this.zzdnt.setUserId(str);
                } catch (RemoteException e2) {
                    zzayu.zze("#007 Could not call remote method.", e2);
                }
            }
        }
    }

    public final void show() {
        synchronized (this.lock) {
            if (this.zzdnt != null) {
                try {
                    this.zzdnt.show();
                } catch (RemoteException e2) {
                    zzayu.zze("#007 Could not call remote method.", e2);
                }
            }
        }
    }

    public final void destroy(Context context) {
        synchronized (this.lock) {
            this.zzdnu.setRewardedVideoAdListener(null);
            if (this.zzdnt != null) {
                try {
                    this.zzdnt.zzl(ObjectWrapper.wrap(context));
                } catch (RemoteException e2) {
                    zzayu.zze("#007 Could not call remote method.", e2);
                }
            }
        }
    }

    public final void loadAd(String str, PublisherAdRequest publisherAdRequest) {
        zza(str, publisherAdRequest.zzdg());
    }

    public final void pause(Context context) {
        synchronized (this.lock) {
            if (this.zzdnt != null) {
                try {
                    this.zzdnt.zzj(ObjectWrapper.wrap(context));
                } catch (RemoteException e2) {
                    zzayu.zze("#007 Could not call remote method.", e2);
                }
            }
        }
    }

    public final void resume(Context context) {
        synchronized (this.lock) {
            if (this.zzdnt != null) {
                try {
                    this.zzdnt.zzk(ObjectWrapper.wrap(context));
                } catch (RemoteException e2) {
                    zzayu.zze("#007 Could not call remote method.", e2);
                }
            }
        }
    }
}
