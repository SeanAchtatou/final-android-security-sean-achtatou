package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.VisibleForTesting;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbju implements zzqj {
    private final Clock zzbmq;
    private Runnable zzdro = null;
    private final ScheduledExecutorService zzfdi;
    private ScheduledFuture<?> zzfdj;
    private long zzfdk = -1;
    private long zzfdl = -1;
    private boolean zzfdm = false;

    public zzbju(ScheduledExecutorService scheduledExecutorService, Clock clock) {
        this.zzfdi = scheduledExecutorService;
        this.zzbmq = clock;
        zzq.zzkt().zza(this);
    }

    @VisibleForTesting
    private final synchronized void zzafr() {
        if (!this.zzfdm) {
            if (this.zzfdj == null || this.zzfdj.isDone()) {
                this.zzfdl = -1;
            } else {
                this.zzfdj.cancel(true);
                this.zzfdl = this.zzfdk - this.zzbmq.elapsedRealtime();
            }
            this.zzfdm = true;
        }
    }

    @VisibleForTesting
    private final synchronized void zzafs() {
        if (this.zzfdm) {
            if (this.zzfdl > 0 && this.zzfdj != null && this.zzfdj.isCancelled()) {
                this.zzfdj = this.zzfdi.schedule(this.zzdro, this.zzfdl, TimeUnit.MILLISECONDS);
            }
            this.zzfdm = false;
        }
    }

    public final synchronized void zza(int i2, Runnable runnable) {
        this.zzdro = runnable;
        long j2 = (long) i2;
        this.zzfdk = this.zzbmq.elapsedRealtime() + j2;
        this.zzfdj = this.zzfdi.schedule(runnable, j2, TimeUnit.MILLISECONDS);
    }

    public final void zzp(boolean z) {
        if (z) {
            zzafs();
        } else {
            zzafr();
        }
    }
}
