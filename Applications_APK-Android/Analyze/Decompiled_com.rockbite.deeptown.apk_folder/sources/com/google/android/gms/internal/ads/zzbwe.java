package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.HashSet;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbwe implements zzdxg<zzbst> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzczl> zzffb;

    public zzbwe(zzdxp<Context> zzdxp, zzdxp<zzczl> zzdxp2) {
        this.zzejv = zzdxp;
        this.zzffb = zzdxp2;
    }

    public final /* synthetic */ Object get() {
        return (zzbst) zzdxm.zza(new zzbst(this.zzejv.get(), new HashSet(), this.zzffb.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
