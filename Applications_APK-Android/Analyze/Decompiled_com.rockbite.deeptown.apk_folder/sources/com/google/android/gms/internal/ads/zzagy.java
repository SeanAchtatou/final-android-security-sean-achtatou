package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.initialization.AdapterStatus;
import com.google.android.gms.ads.initialization.InitializationStatus;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzagy implements InitializationStatus {
    private final Map<String, AdapterStatus> zzcyh;

    public zzagy(Map<String, AdapterStatus> map) {
        this.zzcyh = map;
    }

    public final Map<String, AdapterStatus> getAdapterStatusMap() {
        return this.zzcyh;
    }
}
