package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.ads.mediation.Adapter;
import com.google.android.gms.dynamic.ObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzamc extends zzalh {
    private final zzarz zzddp;
    private final Adapter zzddy;

    zzamc(Adapter adapter, zzarz zzarz) {
        this.zzddy = adapter;
        this.zzddp = zzarz;
    }

    public final void onAdClicked() throws RemoteException {
        zzarz zzarz = this.zzddp;
        if (zzarz != null) {
            zzarz.zzak(ObjectWrapper.wrap(this.zzddy));
        }
    }

    public final void onAdClosed() throws RemoteException {
        zzarz zzarz = this.zzddp;
        if (zzarz != null) {
            zzarz.zzaj(ObjectWrapper.wrap(this.zzddy));
        }
    }

    public final void onAdFailedToLoad(int i2) throws RemoteException {
        zzarz zzarz = this.zzddp;
        if (zzarz != null) {
            zzarz.zze(ObjectWrapper.wrap(this.zzddy), i2);
        }
    }

    public final void onAdImpression() throws RemoteException {
    }

    public final void onAdLeftApplication() throws RemoteException {
    }

    public final void onAdLoaded() throws RemoteException {
        zzarz zzarz = this.zzddp;
        if (zzarz != null) {
            zzarz.zzag(ObjectWrapper.wrap(this.zzddy));
        }
    }

    public final void onAdOpened() throws RemoteException {
        zzarz zzarz = this.zzddp;
        if (zzarz != null) {
            zzarz.zzah(ObjectWrapper.wrap(this.zzddy));
        }
    }

    public final void onAppEvent(String str, String str2) throws RemoteException {
    }

    public final void onVideoEnd() throws RemoteException {
    }

    public final void onVideoPause() throws RemoteException {
    }

    public final void onVideoPlay() throws RemoteException {
    }

    public final void zza(zzade zzade, String str) throws RemoteException {
    }

    public final void zza(zzalj zzalj) throws RemoteException {
    }

    public final void zza(zzasf zzasf) throws RemoteException {
        zzarz zzarz = this.zzddp;
        if (zzarz != null) {
            zzarz.zza(ObjectWrapper.wrap(this.zzddy), new zzasd(zzasf.getType(), zzasf.getAmount()));
        }
    }

    public final void zzb(Bundle bundle) throws RemoteException {
    }

    public final void zzb(zzasd zzasd) throws RemoteException {
    }

    public final void zzco(int i2) throws RemoteException {
    }

    public final void zzdj(String str) throws RemoteException {
    }

    public final void zzss() throws RemoteException {
        zzarz zzarz = this.zzddp;
        if (zzarz != null) {
            zzarz.zzai(ObjectWrapper.wrap(this.zzddy));
        }
    }

    public final void zzst() throws RemoteException {
        zzarz zzarz = this.zzddp;
        if (zzarz != null) {
            zzarz.zzam(ObjectWrapper.wrap(this.zzddy));
        }
    }
}
