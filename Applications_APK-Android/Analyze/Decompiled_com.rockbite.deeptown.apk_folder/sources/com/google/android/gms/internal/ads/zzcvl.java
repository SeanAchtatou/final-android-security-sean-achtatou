package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcvl implements Callable {
    private final zzdhe zzfpa;
    private final zzdhe zzfpn;

    zzcvl(zzdhe zzdhe, zzdhe zzdhe2) {
        this.zzfpn = zzdhe;
        this.zzfpa = zzdhe2;
    }

    public final Object call() {
        return new zzcvj((String) this.zzfpn.get(), (String) this.zzfpa.get());
    }
}
