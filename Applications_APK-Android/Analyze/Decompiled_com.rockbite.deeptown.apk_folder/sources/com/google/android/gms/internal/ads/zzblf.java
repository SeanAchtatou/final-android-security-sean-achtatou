package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzblf extends zzboe<zzblg> {
    zzblf zza(zzbma zzbma);

    zzblf zza(zzcns zzcns);

    zzblg zzaee();

    zzblf zzb(zzbkf zzbkf);

    zzblf zzb(zzbvi zzbvi);

    zzblf zzc(zzbod zzbod);

    zzblf zzc(zzbrm zzbrm);
}
