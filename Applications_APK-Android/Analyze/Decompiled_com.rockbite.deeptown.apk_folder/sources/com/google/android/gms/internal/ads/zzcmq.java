package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcmq implements zzbex {
    private final zzbdi zzehp;

    private zzcmq(zzbdi zzbdi) {
        this.zzehp = zzbdi;
    }

    static zzbex zzq(zzbdi zzbdi) {
        return new zzcmq(zzbdi);
    }

    public final void zzsb() {
        this.zzehp.zztr();
    }
}
