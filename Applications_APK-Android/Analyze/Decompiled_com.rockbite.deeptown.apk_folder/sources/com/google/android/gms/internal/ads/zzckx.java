package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzckx implements Callable {
    private final zzdhe zzfgb;
    private final zzdhe zzfpa;
    private final zzczt zzfzm;
    private final zzcku zzfzs;
    private final zzczl zzfzv;
    private final JSONObject zzfzw;

    zzckx(zzcku zzcku, zzdhe zzdhe, zzdhe zzdhe2, zzczt zzczt, zzczl zzczl, JSONObject jSONObject) {
        this.zzfzs = zzcku;
        this.zzfpa = zzdhe;
        this.zzfgb = zzdhe2;
        this.zzfzm = zzczt;
        this.zzfzv = zzczl;
        this.zzfzw = jSONObject;
    }

    public final Object call() {
        return this.zzfzs.zza(this.zzfpa, this.zzfgb, this.zzfzm, this.zzfzv, this.zzfzw);
    }
}
