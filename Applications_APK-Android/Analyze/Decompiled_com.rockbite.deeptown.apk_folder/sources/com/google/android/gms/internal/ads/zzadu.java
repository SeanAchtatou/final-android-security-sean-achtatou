package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface zzadu extends IInterface {
    void zza(zzvu zzvu, IObjectWrapper iObjectWrapper) throws RemoteException;
}
