package com.google.android.gms.drive.widget;

import android.content.Context;
import android.database.CursorIndexOutOfBoundsException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.google.android.gms.common.data.DataBuffer;
import com.google.android.gms.common.internal.GmsLogger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DataBufferAdapter<T> extends BaseAdapter {
    private static final GmsLogger zzbz = new GmsLogger("DataBufferAdapter", "");
    private final int fieldId;
    private final int resource;
    private final Context zzgw;
    private int zzmz;
    private final List<DataBuffer<T>> zzna;
    private final LayoutInflater zznb;
    private boolean zznc;

    public DataBufferAdapter(Context context, int i2, int i3, List<DataBuffer<T>> list) {
        this.zznc = true;
        this.zzgw = context;
        this.zzmz = i2;
        this.resource = i2;
        this.fieldId = i3;
        this.zzna = list;
        this.zznb = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private final View zza(int i2, View view, ViewGroup viewGroup, int i3) {
        TextView textView;
        if (view == null) {
            view = this.zznb.inflate(i3, viewGroup, false);
        }
        try {
            if (this.fieldId == 0) {
                textView = (TextView) view;
            } else {
                textView = (TextView) view.findViewById(this.fieldId);
            }
            Object item = getItem(i2);
            if (item instanceof CharSequence) {
                textView.setText((CharSequence) item);
            } else {
                textView.setText(item.toString());
            }
            return view;
        } catch (ClassCastException e2) {
            zzbz.e("DataBufferAdapter", "You must supply a resource ID for a TextView", e2);
            throw new IllegalStateException("DataBufferAdapter requires the resource ID to be a TextView", e2);
        }
    }

    public void append(DataBuffer<T> dataBuffer) {
        this.zzna.add(dataBuffer);
        if (this.zznc) {
            notifyDataSetChanged();
        }
    }

    public void clear() {
        for (DataBuffer<T> release : this.zzna) {
            release.release();
        }
        this.zzna.clear();
        if (this.zznc) {
            notifyDataSetChanged();
        }
    }

    public Context getContext() {
        return this.zzgw;
    }

    public int getCount() {
        int i2 = 0;
        for (DataBuffer<T> count : this.zzna) {
            i2 += count.getCount();
        }
        return i2;
    }

    public View getDropDownView(int i2, View view, ViewGroup viewGroup) {
        return zza(i2, view, viewGroup, this.zzmz);
    }

    public T getItem(int i2) throws CursorIndexOutOfBoundsException {
        int i3 = i2;
        for (DataBuffer next : this.zzna) {
            int count = next.getCount();
            if (count <= i3) {
                i3 -= count;
            } else {
                try {
                    return next.get(i3);
                } catch (CursorIndexOutOfBoundsException unused) {
                    throw new CursorIndexOutOfBoundsException(i2, getCount());
                }
            }
        }
        throw new CursorIndexOutOfBoundsException(i2, getCount());
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    public View getView(int i2, View view, ViewGroup viewGroup) {
        return zza(i2, view, viewGroup, this.resource);
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        this.zznc = true;
    }

    public void setDropDownViewResource(int i2) {
        this.zzmz = i2;
    }

    public void setNotifyOnChange(boolean z) {
        this.zznc = z;
    }

    public DataBufferAdapter(Context context, int i2, int i3) {
        this(context, i2, i3, new ArrayList());
    }

    public DataBufferAdapter(Context context, int i2, List<DataBuffer<T>> list) {
        this(context, i2, 0, list);
    }

    public DataBufferAdapter(Context context, int i2) {
        this(context, i2, 0, new ArrayList());
    }

    public DataBufferAdapter(Context context, int i2, int i3, DataBuffer<T>... dataBufferArr) {
        this(context, i2, i3, Arrays.asList(dataBufferArr));
    }

    public DataBufferAdapter(Context context, int i2, DataBuffer<T>... dataBufferArr) {
        this(context, i2, 0, Arrays.asList(dataBufferArr));
    }
}
