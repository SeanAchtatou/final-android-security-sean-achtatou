package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.ads.rewarded.RewardItem;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzasu implements RewardItem {
    private final zzasf zzdny;

    public zzasu(zzasf zzasf) {
        this.zzdny = zzasf;
    }

    public final int getAmount() {
        zzasf zzasf = this.zzdny;
        if (zzasf == null) {
            return 0;
        }
        try {
            return zzasf.getAmount();
        } catch (RemoteException e2) {
            zzayu.zzd("Could not forward getAmount to RewardItem", e2);
            return 0;
        }
    }

    public final String getType() {
        zzasf zzasf = this.zzdny;
        if (zzasf == null) {
            return null;
        }
        try {
            return zzasf.getType();
        } catch (RemoteException e2) {
            zzayu.zzd("Could not forward getType to RewardItem", e2);
            return null;
        }
    }
}
