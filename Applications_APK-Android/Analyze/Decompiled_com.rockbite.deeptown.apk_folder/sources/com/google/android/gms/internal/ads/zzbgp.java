package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbgp implements zzdxg<zzarb> {
    private static final zzbgp zzejx = new zzbgp();

    public static zzbgp zzada() {
        return zzejx;
    }

    public final /* synthetic */ Object get() {
        return (zzarb) zzdxm.zza(new zzara(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
