package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcol extends zzvl {
    private final zzcot zzgcq;

    public zzcol(Context context, zzbfx zzbfx, zzczw zzczw, zzbwz zzbwz, zzvh zzvh) {
        zzcov zzcov = new zzcov(zzbwz);
        zzcov.zzc(zzvh);
        this.zzgcq = new zzcot(new zzcpb(zzbfx, context, zzcov, zzczw), zzczw.zzaor());
    }

    public final synchronized String getMediationAdapterClassName() {
        return this.zzgcq.getMediationAdapterClassName();
    }

    public final synchronized boolean isLoading() throws RemoteException {
        return this.zzgcq.isLoading();
    }

    public final synchronized void zza(zzug zzug, int i2) throws RemoteException {
        this.zzgcq.zza(zzug, i2);
    }

    public final void zzb(zzug zzug) throws RemoteException {
        this.zzgcq.zza(zzug, 1);
    }

    public final synchronized String zzka() {
        return this.zzgcq.zzka();
    }
}
