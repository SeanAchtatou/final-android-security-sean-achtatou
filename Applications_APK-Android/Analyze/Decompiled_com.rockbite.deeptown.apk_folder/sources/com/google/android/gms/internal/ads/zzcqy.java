package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcqy implements zzddw {
    private final zzazl zzbru;

    zzcqy(zzazl zzazl) {
        this.zzbru = zzazl;
    }

    public final void zzbk(boolean z) {
        this.zzbru.set(false);
    }
}
