package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcnl<AdT> implements zzcio<AdT> {
    private final zzdcr zzfgm;
    private final zzdhd zzgbh;
    private final zzaak zzgbn;
    /* access modifiers changed from: private */
    public final zzcnq<AdT> zzgbr;

    public zzcnl(zzdcr zzdcr, zzdhd zzdhd, zzaak zzaak, zzcnq<AdT> zzcnq) {
        this.zzfgm = zzdcr;
        this.zzgbh = zzdhd;
        this.zzgbn = zzaak;
        this.zzgbr = zzcnq;
    }

    public final boolean zza(zzczt zzczt, zzczl zzczl) {
        zzczp zzczp;
        return (this.zzgbn == null || (zzczp = zzczl.zzglo) == null || zzczp.zzdht == null) ? false : true;
    }

    public final zzdhe<AdT> zzb(zzczt zzczt, zzczl zzczl) {
        zzazl zzazl = new zzazl();
        zzcnt zzcnt = new zzcnt();
        zzcnt.zza(new zzcnn(this, zzazl, zzczt, zzczl, zzcnt));
        zzczp zzczp = zzczl.zzglo;
        return this.zzfgm.zzu(zzdco.CUSTOM_RENDER_SYN).zza(new zzcno(this, new zzaad(zzcnt, zzczp.zzdhr, zzczp.zzdht)), this.zzgbh).zzw(zzdco.CUSTOM_RENDER_ACK).zzc(zzazl).zzaqg();
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzb(zzaad zzaad) throws Exception {
        this.zzgbn.zza(zzaad);
    }
}
