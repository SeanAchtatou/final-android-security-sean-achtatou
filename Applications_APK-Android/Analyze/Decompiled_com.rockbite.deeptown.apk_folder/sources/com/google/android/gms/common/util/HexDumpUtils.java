package com.google.android.gms.common.util;

import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public final class HexDumpUtils {
    @KeepForSdk
    public static String dump(byte[] bArr, int i2, int i3, boolean z) {
        if (bArr == null || bArr.length == 0 || i2 < 0 || i3 <= 0 || i2 + i3 > bArr.length) {
            return null;
        }
        int i4 = 57;
        if (z) {
            i4 = 75;
        }
        StringBuilder sb = new StringBuilder(i4 * (((i3 + 16) - 1) / 16));
        int i5 = i2;
        int i6 = i3;
        int i7 = 0;
        int i8 = 0;
        while (i6 > 0) {
            if (i7 == 0) {
                if (i3 < 65536) {
                    sb.append(String.format("%04X:", Integer.valueOf(i5)));
                } else {
                    sb.append(String.format("%08X:", Integer.valueOf(i5)));
                }
                i8 = i5;
            } else if (i7 == 8) {
                sb.append(" -");
            }
            sb.append(String.format(" %02X", Integer.valueOf(bArr[i5] & 255)));
            i6--;
            i7++;
            if (z && (i7 == 16 || i6 == 0)) {
                int i9 = 16 - i7;
                if (i9 > 0) {
                    for (int i10 = 0; i10 < i9; i10++) {
                        sb.append("   ");
                    }
                }
                if (i9 >= 8) {
                    sb.append("  ");
                }
                sb.append("  ");
                for (int i11 = 0; i11 < i7; i11++) {
                    char c2 = (char) bArr[i8 + i11];
                    if (c2 < ' ' || c2 > '~') {
                        c2 = '.';
                    }
                    sb.append(c2);
                }
            }
            if (i7 == 16 || i6 == 0) {
                sb.append(10);
                i7 = 0;
            }
            i5++;
        }
        return sb.toString();
    }
}
