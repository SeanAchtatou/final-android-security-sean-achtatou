package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzbhn implements zzcbh {
    private zzbod zzelr;
    private zzczt zzelt;
    private final /* synthetic */ zzbgr zzerr;
    private zzbrm zzers;
    private zzcxw zzert;

    private zzbhn(zzbgr zzbgr) {
        this.zzerr = zzbgr;
    }

    public final /* synthetic */ zzboe zza(zzcxw zzcxw) {
        this.zzert = zzcxw;
        return this;
    }

    /* renamed from: zzaes */
    public final zzcbi zzadg() {
        zzdxm.zza(this.zzers, zzbrm.class);
        zzdxm.zza(this.zzelr, zzbod.class);
        return new zzbhm(this.zzerr, new zzbnb(), new zzdai(), new zzbny(), new zzcee(), this.zzers, this.zzelr, new zzdaq(), this.zzelt, this.zzert);
    }

    public final /* synthetic */ zzcbh zze(zzbod zzbod) {
        this.zzelr = (zzbod) zzdxm.checkNotNull(zzbod);
        return this;
    }

    public final /* synthetic */ zzboe zza(zzczt zzczt) {
        this.zzelt = zzczt;
        return this;
    }

    public final /* synthetic */ zzcbh zze(zzbrm zzbrm) {
        this.zzers = (zzbrm) zzdxm.checkNotNull(zzbrm);
        return this;
    }
}
