package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcdm implements zzdxg<zzcdj> {
    private final zzdxp<zzcdv> zzekn;
    private final zzdxp<zzcds> zzeml;

    private zzcdm(zzdxp<zzcds> zzdxp, zzdxp<zzcdv> zzdxp2) {
        this.zzeml = zzdxp;
        this.zzekn = zzdxp2;
    }

    public static zzcdm zzw(zzdxp<zzcds> zzdxp, zzdxp<zzcdv> zzdxp2) {
        return new zzcdm(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return new zzcdj(this.zzeml.get(), this.zzekn.get());
    }
}
