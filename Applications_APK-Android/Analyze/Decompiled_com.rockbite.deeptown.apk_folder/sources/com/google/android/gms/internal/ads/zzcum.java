package com.google.android.gms.internal.ads;

import android.text.TextUtils;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcum implements zzcty<JSONObject> {
    private final AdvertisingIdClient.Info zzghj;
    private final String zzghk;

    public zzcum(AdvertisingIdClient.Info info, String str) {
        this.zzghj = info;
        this.zzghk = str;
    }

    public final /* synthetic */ void zzr(Object obj) {
        try {
            JSONObject zzb = zzaxs.zzb((JSONObject) obj, "pii");
            if (this.zzghj == null || TextUtils.isEmpty(this.zzghj.getId())) {
                zzb.put("pdid", this.zzghk);
                zzb.put("pdidtype", "ssaid");
                return;
            }
            zzb.put("rdid", this.zzghj.getId());
            zzb.put("is_lat", this.zzghj.isLimitAdTrackingEnabled());
            zzb.put("idtype", "adid");
        } catch (JSONException e2) {
            zzavs.zza("Failed putting Ad ID.", e2);
        }
    }
}
