package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcpk implements zzdxg<zzcpi> {
    private final zzdxp<zzbfx> zzgec;

    public zzcpk(zzdxp<zzbfx> zzdxp) {
        this.zzgec = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return new zzcpi(this.zzgec.get());
    }
}
