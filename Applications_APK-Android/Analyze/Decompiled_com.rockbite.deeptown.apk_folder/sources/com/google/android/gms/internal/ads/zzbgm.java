package com.google.android.gms.internal.ads;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbgm implements zzdxg<zzaqy> {
    private final zzdxp<Context> zzejv;

    public zzbgm(zzdxp<Context> zzdxp) {
        this.zzejv = zzdxp;
    }

    public final /* synthetic */ Object get() {
        Context context = this.zzejv.get();
        return (zzaqy) zzdxm.zza(new zzaqw(context, new zzard(context).zztz()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
