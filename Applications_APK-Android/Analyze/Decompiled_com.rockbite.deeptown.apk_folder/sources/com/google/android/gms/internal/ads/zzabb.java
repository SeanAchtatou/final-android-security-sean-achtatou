package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzabb {
    public static zzaan<Boolean> zzctv = zzaan.zzf("gads:ad_key_enabled", false);
    public static zzaan<Boolean> zzctw = zzaan.zzf("gads:adshield:enable_adshield_instrumentation", false);
}
