package com.google.android.gms.internal.ads;

import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbpf implements zzdxg<zzbpd> {
    private final zzdxp<Set<zzbsu<zzbpe>>> zzfeo;

    private zzbpf(zzdxp<Set<zzbsu<zzbpe>>> zzdxp) {
        this.zzfeo = zzdxp;
    }

    public static zzbpf zzh(zzdxp<Set<zzbsu<zzbpe>>> zzdxp) {
        return new zzbpf(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzbpd(this.zzfeo.get());
    }
}
