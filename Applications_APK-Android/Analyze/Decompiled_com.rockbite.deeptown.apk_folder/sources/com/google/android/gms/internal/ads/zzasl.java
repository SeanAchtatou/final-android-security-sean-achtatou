package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface zzasl extends IInterface {
    void onRewardedAdClosed() throws RemoteException;

    void onRewardedAdFailedToShow(int i2) throws RemoteException;

    void onRewardedAdOpened() throws RemoteException;

    void zza(zzasf zzasf) throws RemoteException;
}
