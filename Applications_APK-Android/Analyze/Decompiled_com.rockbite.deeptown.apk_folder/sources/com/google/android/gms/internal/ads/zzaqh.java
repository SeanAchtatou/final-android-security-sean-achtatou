package com.google.android.gms.internal.ads;

import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public abstract class zzaqh extends zzgb implements zzaqe {
    public zzaqh() {
        super("com.google.android.gms.ads.internal.request.INonagonStreamingResponseListener");
    }

    /* access modifiers changed from: protected */
    public final boolean zza(int i2, Parcel parcel, Parcel parcel2, int i3) throws RemoteException {
        if (i2 == 1) {
            zzb((ParcelFileDescriptor) zzge.zza(parcel, ParcelFileDescriptor.CREATOR));
        } else if (i2 != 2) {
            return false;
        } else {
            zza((zzaxc) zzge.zza(parcel, zzaxc.CREATOR));
        }
        parcel2.writeNoException();
        return true;
    }
}
