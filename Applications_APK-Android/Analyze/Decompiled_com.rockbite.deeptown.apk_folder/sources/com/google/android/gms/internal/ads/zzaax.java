package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzaax {
    public static zzaan<Boolean> zzctd = zzaan.zzf("gads:debug_logging_feature:enable", false);
    public static zzaan<Boolean> zzcte = zzaan.zzf("gads:debug_logging_feature:intercept_web_view", false);
}
