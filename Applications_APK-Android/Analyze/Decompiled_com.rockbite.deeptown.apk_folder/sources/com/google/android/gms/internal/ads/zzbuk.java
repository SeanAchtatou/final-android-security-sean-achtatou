package com.google.android.gms.internal.ads;

import java.util.Collections;
import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbuk implements zzdxg<Set<zzbsu<zzbsn>>> {
    private final zzdxp<zzbvc> zzfdq;

    private zzbuk(zzdxp<zzbvc> zzdxp) {
        this.zzfdq = zzdxp;
    }

    public static zzbuk zzu(zzdxp<zzbvc> zzdxp) {
        return new zzbuk(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return (Set) zzdxm.zza(Collections.singleton(zzbsu.zzb(this.zzfdq.get(), zzazd.zzdwj)), "Cannot return null from a non-@Nullable @Provides method");
    }
}
