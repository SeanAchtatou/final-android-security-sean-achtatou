package com.google.android.gms.ads.query;

import android.content.Context;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public class NativeQueryDataConfiguration extends QueryDataConfiguration {
    @KeepForSdk
    public NativeQueryDataConfiguration(Context context, String str) {
        super(context, str);
    }
}
