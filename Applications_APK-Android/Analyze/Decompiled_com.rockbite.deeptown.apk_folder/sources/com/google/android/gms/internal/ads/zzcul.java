package com.google.android.gms.internal.ads;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import com.google.android.gms.ads.internal.zzq;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcul implements zzcub<zzcui> {
    private final zzdhd zzfov;
    private final Context zzup;

    public zzcul(zzdhd zzdhd, Context context) {
        this.zzfov = zzdhd;
        this.zzup = context;
    }

    public final zzdhe<zzcui> zzanc() {
        return this.zzfov.zzd(new zzcuk(this));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzcui zzanr() throws Exception {
        int i2;
        boolean z;
        int i3;
        int i4;
        TelephonyManager telephonyManager = (TelephonyManager) this.zzup.getSystemService("phone");
        String networkOperator = telephonyManager.getNetworkOperator();
        int networkType = telephonyManager.getNetworkType();
        int phoneType = telephonyManager.getPhoneType();
        zzq.zzkq();
        int i5 = -1;
        if (zzawb.zzq(this.zzup, "android.permission.ACCESS_NETWORK_STATE")) {
            ConnectivityManager connectivityManager = (ConnectivityManager) this.zzup.getSystemService("connectivity");
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo != null) {
                int type = activeNetworkInfo.getType();
                int ordinal = activeNetworkInfo.getDetailedState().ordinal();
                i4 = type;
                i5 = ordinal;
            } else {
                i4 = -1;
            }
            if (Build.VERSION.SDK_INT >= 16) {
                i2 = i5;
                i3 = i4;
                z = connectivityManager.isActiveNetworkMetered();
            } else {
                i2 = i5;
                i3 = i4;
                z = false;
            }
        } else {
            i3 = -2;
            z = false;
            i2 = -1;
        }
        return new zzcui(networkOperator, i3, networkType, phoneType, z, i2);
    }
}
