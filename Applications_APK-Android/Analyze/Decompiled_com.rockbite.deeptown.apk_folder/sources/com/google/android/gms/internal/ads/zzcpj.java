package com.google.android.gms.internal.ads;

import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public class zzcpj {
    private String zzgeb;

    /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
    public static class zza {
        /* access modifiers changed from: private */
        public String zzgeb;

        public final zza zzgj(String str) {
            this.zzgeb = str;
            return this;
        }
    }

    private zzcpj(zza zza2) {
        this.zzgeb = zza2.zzgeb;
    }

    public final Set<String> zzamz() {
        HashSet hashSet = new HashSet();
        hashSet.add(this.zzgeb.toLowerCase(Locale.ROOT));
        return hashSet;
    }

    public final String zzana() {
        return this.zzgeb.toLowerCase(Locale.ROOT);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final int zzanb() {
        char c2;
        String str = this.zzgeb;
        switch (str.hashCode()) {
            case -1999289321:
                if (str.equals("NATIVE")) {
                    c2 = 2;
                    break;
                }
                c2 = 65535;
                break;
            case -1372958932:
                if (str.equals("INTERSTITIAL")) {
                    c2 = 1;
                    break;
                }
                c2 = 65535;
                break;
            case 543046670:
                if (str.equals("REWARDED")) {
                    c2 = 3;
                    break;
                }
                c2 = 65535;
                break;
            case 1951953708:
                if (str.equals("BANNER")) {
                    c2 = 0;
                    break;
                }
                c2 = 65535;
                break;
            default:
                c2 = 65535;
                break;
        }
        if (c2 == 0) {
            return 1;
        }
        if (c2 == 1) {
            return 3;
        }
        if (c2 != 2) {
            return c2 != 3 ? 0 : 7;
        }
        return 6;
    }
}
