package com.google.android.gms.internal.ads;

import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbvx implements zzdxg<JSONObject> {
    private final zzdxp<zzbww> zzeti;
    private final zzbvy zzfla;

    public zzbvx(zzbvy zzbvy, zzdxp<zzbww> zzdxp) {
        this.zzfla = zzbvy;
        this.zzeti = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return this.zzeti.get().zzajl();
    }
}
