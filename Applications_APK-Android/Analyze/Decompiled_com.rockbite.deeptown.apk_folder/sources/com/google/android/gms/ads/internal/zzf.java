package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.ads.zzdgf;
import com.google.android.gms.internal.ads.zzdgs;
import com.google.android.gms.internal.ads.zzdhe;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzf implements zzdgf {
    static final zzdgf zzbkw = new zzf();

    private zzf() {
    }

    public final zzdhe zzf(Object obj) {
        JSONObject jSONObject = (JSONObject) obj;
        if (jSONObject.optBoolean("isSuccessful", false)) {
            zzq.zzku().zzvf().zzeg(jSONObject.getString("appSettingsJson"));
        }
        return zzdgs.zzaj(null);
    }
}
