package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzaxt implements zzy {
    private final /* synthetic */ zzazl zzduo;

    zzaxt(zzazl zzazl) {
        this.zzduo = zzazl;
    }

    public final void zzc(zzae zzae) {
        this.zzduo.setException(zzae);
    }
}
