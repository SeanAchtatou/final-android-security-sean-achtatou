package com.google.android.gms.ads.formats;

import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.internal.ads.zzabt;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final /* synthetic */ class zze implements zzabt {
    private final UnifiedNativeAdView zzbkm;

    zze(UnifiedNativeAdView unifiedNativeAdView) {
        this.zzbkm = unifiedNativeAdView;
    }

    public final void setMediaContent(UnifiedNativeAd.MediaContent mediaContent) {
        this.zzbkm.zza(mediaContent);
    }
}
