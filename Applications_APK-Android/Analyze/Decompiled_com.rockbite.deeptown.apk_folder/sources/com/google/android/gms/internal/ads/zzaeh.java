package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.ads.MuteThisAdListener;
import com.google.android.gms.ads.MuteThisAdReason;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzaeh extends UnifiedNativeAd {
    private final VideoController zzcel = new VideoController();
    private final List<NativeAd.Image> zzcvv = new ArrayList();
    private final zzacj zzcvw;
    private final NativeAd.AdChoicesInfo zzcvx;
    private final zzaeg zzcwe;
    private final List<MuteThisAdReason> zzcwf = new ArrayList();

    /* JADX WARN: Type inference failed for: r3v5, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00b2 A[Catch:{ RemoteException -> 0x00bf }] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public zzaeh(com.google.android.gms.internal.ads.zzaeg r6) {
        /*
            r5 = this;
            java.lang.String r0 = ""
            r5.<init>()
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r5.zzcvv = r1
            com.google.android.gms.ads.VideoController r1 = new com.google.android.gms.ads.VideoController
            r1.<init>()
            r5.zzcel = r1
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r5.zzcwf = r1
            r5.zzcwe = r6
            r6 = 0
            com.google.android.gms.internal.ads.zzaeg r1 = r5.zzcwe     // Catch:{ RemoteException -> 0x005e }
            java.util.List r1 = r1.getImages()     // Catch:{ RemoteException -> 0x005e }
            if (r1 == 0) goto L_0x0062
            java.util.Iterator r1 = r1.iterator()     // Catch:{ RemoteException -> 0x005e }
        L_0x0029:
            boolean r2 = r1.hasNext()     // Catch:{ RemoteException -> 0x005e }
            if (r2 == 0) goto L_0x0062
            java.lang.Object r2 = r1.next()     // Catch:{ RemoteException -> 0x005e }
            boolean r3 = r2 instanceof android.os.IBinder     // Catch:{ RemoteException -> 0x005e }
            if (r3 == 0) goto L_0x0050
            android.os.IBinder r2 = (android.os.IBinder) r2     // Catch:{ RemoteException -> 0x005e }
            if (r2 == 0) goto L_0x0050
            java.lang.String r3 = "com.google.android.gms.ads.internal.formats.client.INativeAdImage"
            android.os.IInterface r3 = r2.queryLocalInterface(r3)     // Catch:{ RemoteException -> 0x005e }
            boolean r4 = r3 instanceof com.google.android.gms.internal.ads.zzaci     // Catch:{ RemoteException -> 0x005e }
            if (r4 == 0) goto L_0x0049
            r2 = r3
            com.google.android.gms.internal.ads.zzaci r2 = (com.google.android.gms.internal.ads.zzaci) r2     // Catch:{ RemoteException -> 0x005e }
            goto L_0x0051
        L_0x0049:
            com.google.android.gms.internal.ads.zzack r3 = new com.google.android.gms.internal.ads.zzack     // Catch:{ RemoteException -> 0x005e }
            r3.<init>(r2)     // Catch:{ RemoteException -> 0x005e }
            r2 = r3
            goto L_0x0051
        L_0x0050:
            r2 = r6
        L_0x0051:
            if (r2 == 0) goto L_0x0029
            java.util.List<com.google.android.gms.ads.formats.NativeAd$Image> r3 = r5.zzcvv     // Catch:{ RemoteException -> 0x005e }
            com.google.android.gms.internal.ads.zzacj r4 = new com.google.android.gms.internal.ads.zzacj     // Catch:{ RemoteException -> 0x005e }
            r4.<init>(r2)     // Catch:{ RemoteException -> 0x005e }
            r3.add(r4)     // Catch:{ RemoteException -> 0x005e }
            goto L_0x0029
        L_0x005e:
            r1 = move-exception
            com.google.android.gms.internal.ads.zzayu.zzc(r0, r1)
        L_0x0062:
            com.google.android.gms.internal.ads.zzaeg r1 = r5.zzcwe     // Catch:{ RemoteException -> 0x0091 }
            java.util.List r1 = r1.getMuteThisAdReasons()     // Catch:{ RemoteException -> 0x0091 }
            if (r1 == 0) goto L_0x0095
            java.util.Iterator r1 = r1.iterator()     // Catch:{ RemoteException -> 0x0091 }
        L_0x006e:
            boolean r2 = r1.hasNext()     // Catch:{ RemoteException -> 0x0091 }
            if (r2 == 0) goto L_0x0095
            java.lang.Object r2 = r1.next()     // Catch:{ RemoteException -> 0x0091 }
            boolean r3 = r2 instanceof android.os.IBinder     // Catch:{ RemoteException -> 0x0091 }
            if (r3 == 0) goto L_0x0083
            android.os.IBinder r2 = (android.os.IBinder) r2     // Catch:{ RemoteException -> 0x0091 }
            com.google.android.gms.internal.ads.zzwr r2 = com.google.android.gms.internal.ads.zzwu.zzg(r2)     // Catch:{ RemoteException -> 0x0091 }
            goto L_0x0084
        L_0x0083:
            r2 = r6
        L_0x0084:
            if (r2 == 0) goto L_0x006e
            java.util.List<com.google.android.gms.ads.MuteThisAdReason> r3 = r5.zzcwf     // Catch:{ RemoteException -> 0x0091 }
            com.google.android.gms.internal.ads.zzww r4 = new com.google.android.gms.internal.ads.zzww     // Catch:{ RemoteException -> 0x0091 }
            r4.<init>(r2)     // Catch:{ RemoteException -> 0x0091 }
            r3.add(r4)     // Catch:{ RemoteException -> 0x0091 }
            goto L_0x006e
        L_0x0091:
            r1 = move-exception
            com.google.android.gms.internal.ads.zzayu.zzc(r0, r1)
        L_0x0095:
            com.google.android.gms.internal.ads.zzaeg r1 = r5.zzcwe     // Catch:{ RemoteException -> 0x00a3 }
            com.google.android.gms.internal.ads.zzaci r1 = r1.zzrg()     // Catch:{ RemoteException -> 0x00a3 }
            if (r1 == 0) goto L_0x00a7
            com.google.android.gms.internal.ads.zzacj r2 = new com.google.android.gms.internal.ads.zzacj     // Catch:{ RemoteException -> 0x00a3 }
            r2.<init>(r1)     // Catch:{ RemoteException -> 0x00a3 }
            goto L_0x00a8
        L_0x00a3:
            r1 = move-exception
            com.google.android.gms.internal.ads.zzayu.zzc(r0, r1)
        L_0x00a7:
            r2 = r6
        L_0x00a8:
            r5.zzcvw = r2
            com.google.android.gms.internal.ads.zzaeg r1 = r5.zzcwe     // Catch:{ RemoteException -> 0x00bf }
            com.google.android.gms.internal.ads.zzaca r1 = r1.zzrh()     // Catch:{ RemoteException -> 0x00bf }
            if (r1 == 0) goto L_0x00c3
            com.google.android.gms.internal.ads.zzacb r1 = new com.google.android.gms.internal.ads.zzacb     // Catch:{ RemoteException -> 0x00bf }
            com.google.android.gms.internal.ads.zzaeg r2 = r5.zzcwe     // Catch:{ RemoteException -> 0x00bf }
            com.google.android.gms.internal.ads.zzaca r2 = r2.zzrh()     // Catch:{ RemoteException -> 0x00bf }
            r1.<init>(r2)     // Catch:{ RemoteException -> 0x00bf }
            r6 = r1
            goto L_0x00c3
        L_0x00bf:
            r1 = move-exception
            com.google.android.gms.internal.ads.zzayu.zzc(r0, r1)
        L_0x00c3:
            r5.zzcvx = r6
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzaeh.<init>(com.google.android.gms.internal.ads.zzaeg):void");
    }

    /* access modifiers changed from: private */
    /* renamed from: zzrf */
    public final IObjectWrapper zzjj() {
        try {
            return this.zzcwe.zzrf();
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
            return null;
        }
    }

    public final void cancelUnconfirmedClick() {
        try {
            this.zzcwe.cancelUnconfirmedClick();
        } catch (RemoteException e2) {
            zzayu.zzc("Failed to cancelUnconfirmedClick", e2);
        }
    }

    public final void destroy() {
        try {
            this.zzcwe.destroy();
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
        }
    }

    public final void enableCustomClickGesture() {
        try {
            this.zzcwe.zzrp();
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
        }
    }

    public final NativeAd.AdChoicesInfo getAdChoicesInfo() {
        return this.zzcvx;
    }

    public final String getAdvertiser() {
        try {
            return this.zzcwe.getAdvertiser();
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
            return null;
        }
    }

    public final String getBody() {
        try {
            return this.zzcwe.getBody();
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
            return null;
        }
    }

    public final String getCallToAction() {
        try {
            return this.zzcwe.getCallToAction();
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
            return null;
        }
    }

    public final Bundle getExtras() {
        try {
            Bundle extras = this.zzcwe.getExtras();
            if (extras != null) {
                return extras;
            }
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
        }
        return new Bundle();
    }

    public final String getHeadline() {
        try {
            return this.zzcwe.getHeadline();
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
            return null;
        }
    }

    public final NativeAd.Image getIcon() {
        return this.zzcvw;
    }

    public final List<NativeAd.Image> getImages() {
        return this.zzcvv;
    }

    public final UnifiedNativeAd.MediaContent getMediaContent() {
        try {
            if (this.zzcwe.zzrq() != null) {
                return new zzaek(this.zzcwe.zzrq());
            }
            return null;
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
            return null;
        }
    }

    public final String getMediationAdapterClassName() {
        try {
            return this.zzcwe.getMediationAdapterClassName();
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
            return null;
        }
    }

    public final List<MuteThisAdReason> getMuteThisAdReasons() {
        return this.zzcwf;
    }

    public final String getPrice() {
        try {
            return this.zzcwe.getPrice();
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
            return null;
        }
    }

    public final Double getStarRating() {
        try {
            double starRating = this.zzcwe.getStarRating();
            if (starRating == -1.0d) {
                return null;
            }
            return Double.valueOf(starRating);
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
            return null;
        }
    }

    public final String getStore() {
        try {
            return this.zzcwe.getStore();
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
            return null;
        }
    }

    public final VideoController getVideoController() {
        try {
            if (this.zzcwe.getVideoController() != null) {
                this.zzcel.zza(this.zzcwe.getVideoController());
            }
        } catch (RemoteException e2) {
            zzayu.zzc("Exception occurred while getting video controller", e2);
        }
        return this.zzcel;
    }

    public final boolean isCustomClickGestureEnabled() {
        try {
            return this.zzcwe.isCustomClickGestureEnabled();
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
            return false;
        }
    }

    public final boolean isCustomMuteThisAdEnabled() {
        try {
            return this.zzcwe.isCustomMuteThisAdEnabled();
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
            return false;
        }
    }

    public final void muteThisAd(MuteThisAdReason muteThisAdReason) {
        try {
            if (!isCustomMuteThisAdEnabled()) {
                zzayu.zzex("Ad is not custom mute enabled");
            } else if (muteThisAdReason == null) {
                this.zzcwe.zza((zzwr) null);
            } else if (muteThisAdReason instanceof zzww) {
                this.zzcwe.zza(((zzww) muteThisAdReason).zzpi());
            } else {
                zzayu.zzex("Use mute reason from UnifiedNativeAd.getMuteThisAdReasons() or null");
            }
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
        }
    }

    public final void performClick(Bundle bundle) {
        try {
            this.zzcwe.performClick(bundle);
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
        }
    }

    public final void recordCustomClickGesture() {
        try {
            this.zzcwe.recordCustomClickGesture();
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
        }
    }

    public final boolean recordImpression(Bundle bundle) {
        try {
            return this.zzcwe.recordImpression(bundle);
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
            return false;
        }
    }

    public final void reportTouchEvent(Bundle bundle) {
        try {
            this.zzcwe.reportTouchEvent(bundle);
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
        }
    }

    public final void setMuteThisAdListener(MuteThisAdListener muteThisAdListener) {
        try {
            this.zzcwe.zza(new zzws(muteThisAdListener));
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
        }
    }

    public final void setUnconfirmedClickListener(UnifiedNativeAd.UnconfirmedClickListener unconfirmedClickListener) {
        try {
            this.zzcwe.zza(new zzaeu(unconfirmedClickListener));
        } catch (RemoteException e2) {
            zzayu.zzc("Failed to setUnconfirmedClickListener", e2);
        }
    }

    public final Object zzjo() {
        try {
            IObjectWrapper zzri = this.zzcwe.zzri();
            if (zzri != null) {
                return ObjectWrapper.unwrap(zzri);
            }
            return null;
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
            return null;
        }
    }
}
