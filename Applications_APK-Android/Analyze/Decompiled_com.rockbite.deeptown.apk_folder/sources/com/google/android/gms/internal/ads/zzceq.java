package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzq;
import com.tapjoy.TJAdUnitConstants;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzceq {
    /* access modifiers changed from: private */
    public final Executor executor;
    private final zzazb zzbli;
    private final Context zzcgd;
    private final Executor zzfci;
    private final ScheduledExecutorService zzfdi;
    private boolean zzftl = false;
    /* access modifiers changed from: private */
    public boolean zzftm = false;
    /* access modifiers changed from: private */
    public final long zzftn;
    /* access modifiers changed from: private */
    public final zzazl<Boolean> zzfto = new zzazl<>();
    private final WeakReference<Context> zzftp;
    private final zzcka zzftq;
    /* access modifiers changed from: private */
    public final zzcea zzftr;
    private Map<String, zzagn> zzfts = new ConcurrentHashMap();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzceq.zza(java.lang.String, boolean, java.lang.String, int):void
     arg types: [java.lang.String, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzceq.zza(com.google.android.gms.internal.ads.zzdac, com.google.android.gms.internal.ads.zzagp, java.util.List, java.lang.String):void
      com.google.android.gms.internal.ads.zzceq.zza(java.lang.Object, com.google.android.gms.internal.ads.zzazl, java.lang.String, long):void
      com.google.android.gms.internal.ads.zzceq.zza(java.lang.String, boolean, java.lang.String, int):void */
    public zzceq(Executor executor2, Context context, WeakReference<Context> weakReference, Executor executor3, zzcka zzcka, ScheduledExecutorService scheduledExecutorService, zzcea zzcea, zzazb zzazb) {
        this.zzftq = zzcka;
        this.zzcgd = context;
        this.zzftp = weakReference;
        this.executor = executor3;
        this.zzfdi = scheduledExecutorService;
        this.zzfci = executor2;
        this.zzftr = zzcea;
        this.zzbli = zzazb;
        this.zzftn = zzq.zzkx().elapsedRealtime();
        zza("com.google.android.gms.ads.MobileAds", false, "", 0);
    }

    /* access modifiers changed from: private */
    public final void zza(String str, boolean z, String str2, int i2) {
        this.zzfts.put(str, new zzagn(str, z, i2, str2));
    }

    private final synchronized zzdhe<String> zzaln() {
        String zzvl = zzq.zzku().zzvf().zzwa().zzvl();
        if (!TextUtils.isEmpty(zzvl)) {
            return zzdgs.zzaj(zzvl);
        }
        zzazl zzazl = new zzazl();
        zzq.zzku().zzvf().zzb(new zzcer(this, zzazl));
        return zzazl;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzceq.zza(java.lang.String, boolean, java.lang.String, int):void
     arg types: [java.lang.String, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzceq.zza(com.google.android.gms.internal.ads.zzdac, com.google.android.gms.internal.ads.zzagp, java.util.List, java.lang.String):void
      com.google.android.gms.internal.ads.zzceq.zza(java.lang.Object, com.google.android.gms.internal.ads.zzazl, java.lang.String, long):void
      com.google.android.gms.internal.ads.zzceq.zza(java.lang.String, boolean, java.lang.String, int):void */
    /* access modifiers changed from: private */
    public final void zzgf(String str) {
        try {
            ArrayList arrayList = new ArrayList();
            JSONObject jSONObject = new JSONObject(str).getJSONObject("initializer_settings").getJSONObject("config");
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                Object obj = new Object();
                zzazl zzazl = new zzazl();
                zzdhe zza = zzdgs.zza(zzazl, ((Long) zzve.zzoy().zzd(zzzn.zzcks)).longValue(), TimeUnit.SECONDS, this.zzfdi);
                this.zzftr.zzgd(next);
                long elapsedRealtime = zzq.zzkx().elapsedRealtime();
                Iterator<String> it = keys;
                zzcet zzcet = r1;
                zzcet zzcet2 = new zzcet(this, obj, zzazl, next, elapsedRealtime);
                zza.addListener(zzcet, this.executor);
                arrayList.add(zza);
                zzcez zzcez = new zzcez(this, obj, next, elapsedRealtime, zzazl);
                JSONObject optJSONObject = jSONObject.optJSONObject(next);
                ArrayList arrayList2 = new ArrayList();
                if (optJSONObject != null) {
                    try {
                        JSONArray jSONArray = optJSONObject.getJSONArray(TJAdUnitConstants.String.DATA);
                        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                            JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                            String optString = jSONObject2.optString("format", "");
                            JSONObject optJSONObject2 = jSONObject2.optJSONObject(TJAdUnitConstants.String.DATA);
                            Bundle bundle = new Bundle();
                            if (optJSONObject2 != null) {
                                Iterator<String> keys2 = optJSONObject2.keys();
                                while (keys2.hasNext()) {
                                    String next2 = keys2.next();
                                    bundle.putString(next2, optJSONObject2.optString(next2, ""));
                                }
                            }
                            arrayList2.add(new zzagx(optString, bundle));
                        }
                    } catch (JSONException unused) {
                    }
                }
                zza(next, false, "", 0);
                try {
                    this.zzfci.execute(new zzcev(this, this.zzftq.zze(next, new JSONObject()), zzcez, arrayList2, next));
                } catch (zzdab unused2) {
                    try {
                        zzcez.onInitializationFailed("Failed to create Adapter.");
                    } catch (RemoteException e2) {
                        zzayu.zzc("", e2);
                    }
                }
                keys = it;
            }
            zzdgs.zzh(arrayList).zza(new zzcew(this), this.executor);
        } catch (JSONException e3) {
            zzavs.zza("Malformed CLD response", e3);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzceq.zza(java.lang.String, boolean, java.lang.String, int):void
     arg types: [java.lang.String, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzceq.zza(com.google.android.gms.internal.ads.zzdac, com.google.android.gms.internal.ads.zzagp, java.util.List, java.lang.String):void
      com.google.android.gms.internal.ads.zzceq.zza(java.lang.Object, com.google.android.gms.internal.ads.zzazl, java.lang.String, long):void
      com.google.android.gms.internal.ads.zzceq.zza(java.lang.String, boolean, java.lang.String, int):void */
    public final void zzall() {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzckq)).booleanValue() && !zzabe.zzctz.get().booleanValue()) {
            if (this.zzbli.zzdwa >= ((Integer) zzve.zzoy().zzd(zzzn.zzckr)).intValue()) {
                if (!this.zzftl) {
                    synchronized (this) {
                        if (!this.zzftl) {
                            this.zzftr.zzali();
                            this.zzfto.addListener(new zzces(this), this.executor);
                            this.zzftl = true;
                            zzdhe<String> zzaln = zzaln();
                            this.zzfdi.schedule(new zzceu(this), ((Long) zzve.zzoy().zzd(zzzn.zzckt)).longValue(), TimeUnit.SECONDS);
                            zzdgs.zza(zzaln, new zzcex(this), this.executor);
                            return;
                        }
                        return;
                    }
                }
                return;
            }
        }
        zza("com.google.android.gms.ads.MobileAds", true, "", 0);
        this.zzfto.set(false);
    }

    public final List<zzagn> zzalm() {
        ArrayList arrayList = new ArrayList();
        for (String next : this.zzfts.keySet()) {
            zzagn zzagn = this.zzfts.get(next);
            arrayList.add(new zzagn(next, zzagn.zzcyd, zzagn.zzcye, zzagn.description));
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Object zzalo() throws Exception {
        this.zzfto.set(true);
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzceq.zza(java.lang.String, boolean, java.lang.String, int):void
     arg types: [java.lang.String, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzceq.zza(com.google.android.gms.internal.ads.zzdac, com.google.android.gms.internal.ads.zzagp, java.util.List, java.lang.String):void
      com.google.android.gms.internal.ads.zzceq.zza(java.lang.Object, com.google.android.gms.internal.ads.zzazl, java.lang.String, long):void
      com.google.android.gms.internal.ads.zzceq.zza(java.lang.String, boolean, java.lang.String, int):void */
    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzalp() {
        synchronized (this) {
            if (!this.zzftm) {
                zza("com.google.android.gms.ads.MobileAds", false, "Timeout.", (int) (zzq.zzkx().elapsedRealtime() - this.zzftn));
                this.zzfto.setException(new Exception());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzalq() {
        this.zzftr.zzalj();
    }

    public final void zzb(zzagu zzagu) {
        this.zzfto.addListener(new zzcep(this, zzagu), this.zzfci);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zzc(zzagu zzagu) {
        try {
            zzagu.zzc(zzalm());
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0035, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0036, code lost:
        com.google.android.gms.internal.ads.zzayu.zzc("", r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003b, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:?, code lost:
        r4 = new java.lang.StringBuilder(java.lang.String.valueOf(r5).length() + 74);
        r4.append("Failed to initialize adapter. ");
        r4.append(r5);
        r4.append(" does not implement the initialize() method.");
        r3.onInitializationFailed(r4.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0034, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0011 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ void zza(com.google.android.gms.internal.ads.zzdac r2, com.google.android.gms.internal.ads.zzagp r3, java.util.List r4, java.lang.String r5) {
        /*
            r1 = this;
            java.lang.ref.WeakReference<android.content.Context> r0 = r1.zzftp     // Catch:{ zzdab -> 0x0011 }
            java.lang.Object r0 = r0.get()     // Catch:{ zzdab -> 0x0011 }
            android.content.Context r0 = (android.content.Context) r0     // Catch:{ zzdab -> 0x0011 }
            if (r0 == 0) goto L_0x000b
            goto L_0x000d
        L_0x000b:
            android.content.Context r0 = r1.zzcgd     // Catch:{ zzdab -> 0x0011 }
        L_0x000d:
            r2.zza(r0, r3, r4)     // Catch:{ zzdab -> 0x0011 }
            return
        L_0x0011:
            java.lang.String r2 = java.lang.String.valueOf(r5)     // Catch:{ RemoteException -> 0x0035 }
            int r2 = r2.length()     // Catch:{ RemoteException -> 0x0035 }
            int r2 = r2 + 74
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ RemoteException -> 0x0035 }
            r4.<init>(r2)     // Catch:{ RemoteException -> 0x0035 }
            java.lang.String r2 = "Failed to initialize adapter. "
            r4.append(r2)     // Catch:{ RemoteException -> 0x0035 }
            r4.append(r5)     // Catch:{ RemoteException -> 0x0035 }
            java.lang.String r2 = " does not implement the initialize() method."
            r4.append(r2)     // Catch:{ RemoteException -> 0x0035 }
            java.lang.String r2 = r4.toString()     // Catch:{ RemoteException -> 0x0035 }
            r3.onInitializationFailed(r2)     // Catch:{ RemoteException -> 0x0035 }
            return
        L_0x0035:
            r2 = move-exception
            java.lang.String r3 = ""
            com.google.android.gms.internal.ads.zzayu.zzc(r3, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzceq.zza(com.google.android.gms.internal.ads.zzdac, com.google.android.gms.internal.ads.zzagp, java.util.List, java.lang.String):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzceq.zza(java.lang.String, boolean, java.lang.String, int):void
     arg types: [java.lang.String, int, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.ads.zzceq.zza(com.google.android.gms.internal.ads.zzdac, com.google.android.gms.internal.ads.zzagp, java.util.List, java.lang.String):void
      com.google.android.gms.internal.ads.zzceq.zza(java.lang.Object, com.google.android.gms.internal.ads.zzazl, java.lang.String, long):void
      com.google.android.gms.internal.ads.zzceq.zza(java.lang.String, boolean, java.lang.String, int):void */
    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zza(Object obj, zzazl zzazl, String str, long j2) {
        synchronized (obj) {
            if (!zzazl.isDone()) {
                zza(str, false, "Timeout.", (int) (zzq.zzkx().elapsedRealtime() - j2));
                this.zzftr.zzq(str, "timeout");
                zzazl.set(false);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void zza(zzazl zzazl) {
        this.executor.execute(new zzcey(this, zzazl));
    }
}
