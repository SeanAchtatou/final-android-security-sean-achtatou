package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.internal.ads.zzsy;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzdba implements zzdbb {
    private final ConcurrentHashMap<zzdbl, zzday> zzgnt;
    private zzdbe zzgnu;
    private zzdbc zzgnv = new zzdbc();

    public zzdba(zzdbe zzdbe) {
        this.zzgnt = new ConcurrentHashMap<>(zzdbe.zzgoj);
        this.zzgnu = zzdbe;
    }

    private final void dumpToLog() {
        if (zzdbe.zzapl()) {
            StringBuilder sb = new StringBuilder();
            sb.append(this.zzgnu.zzgoh);
            sb.append(" PoolCollection");
            sb.append(this.zzgnv.zzapk());
            int i2 = 0;
            for (Map.Entry next : this.zzgnt.entrySet()) {
                i2++;
                sb.append(i2);
                sb.append(". ");
                sb.append(next.getValue());
                sb.append("#");
                sb.append(((zzdbl) next.getKey()).hashCode());
                sb.append("    ");
                for (int i3 = 0; i3 < ((zzday) next.getValue()).size(); i3++) {
                    sb.append("[O]");
                }
                for (int size = ((zzday) next.getValue()).size(); size < this.zzgnu.zzgoj; size++) {
                    sb.append("[ ]");
                }
                sb.append("\n");
                sb.append(((zzday) next.getValue()).zzapa());
                sb.append("\n");
            }
            while (i2 < this.zzgnu.zzgoi) {
                i2++;
                sb.append(i2);
                sb.append(".\n");
            }
            zzayu.zzea(sb.toString());
        }
    }

    public final synchronized zzdbi<?> zza(zzdbl zzdbl) {
        zzdbi<?> zzdbi;
        zzday zzday = this.zzgnt.get(zzdbl);
        zzdbi = null;
        if (zzday != null) {
            zzdbi = zzday.zzaox();
            if (zzdbi == null) {
                this.zzgnv.zzapf();
            }
            zza(zzdbi, zzday.zzapb());
        } else {
            this.zzgnv.zzape();
            zza((zzdbi<?>) null, (zzdbo) null);
        }
        return zzdbi;
    }

    public final synchronized boolean zzb(zzdbl zzdbl) {
        zzday zzday = this.zzgnt.get(zzdbl);
        if (zzday == null) {
            return true;
        }
        if (zzday.size() < this.zzgnu.zzgoj) {
            return true;
        }
        return false;
    }

    public final synchronized boolean zza(zzdbl zzdbl, zzdbi<?> zzdbi) {
        boolean zzb;
        zzday zzday = this.zzgnt.get(zzdbl);
        zzdbi.zzgpe = zzq.zzkx().currentTimeMillis();
        if (zzday == null) {
            zzday = new zzday(this.zzgnu.zzgoj, this.zzgnu.zzgok * 1000);
            if (this.zzgnt.size() == this.zzgnu.zzgoi) {
                int i2 = zzdbd.zzgoc[this.zzgnu.zzgon - 1];
                long j2 = Long.MAX_VALUE;
                zzdbl zzdbl2 = null;
                if (i2 == 1) {
                    for (Map.Entry next : this.zzgnt.entrySet()) {
                        if (((zzday) next.getValue()).getCreationTimeMillis() < j2) {
                            j2 = ((zzday) next.getValue()).getCreationTimeMillis();
                            zzdbl2 = (zzdbl) next.getKey();
                        }
                    }
                    if (zzdbl2 != null) {
                        this.zzgnt.remove(zzdbl2);
                    }
                } else if (i2 == 2) {
                    for (Map.Entry next2 : this.zzgnt.entrySet()) {
                        if (((zzday) next2.getValue()).zzaoy() < j2) {
                            j2 = ((zzday) next2.getValue()).zzaoy();
                            zzdbl2 = (zzdbl) next2.getKey();
                        }
                    }
                    if (zzdbl2 != null) {
                        this.zzgnt.remove(zzdbl2);
                    }
                } else if (i2 == 3) {
                    int i3 = Api.BaseClientBuilder.API_PRIORITY_OTHER;
                    for (Map.Entry next3 : this.zzgnt.entrySet()) {
                        if (((zzday) next3.getValue()).zzaoz() < i3) {
                            i3 = ((zzday) next3.getValue()).zzaoz();
                            zzdbl2 = (zzdbl) next3.getKey();
                        }
                    }
                    if (zzdbl2 != null) {
                        this.zzgnt.remove(zzdbl2);
                    }
                }
                this.zzgnv.zzaph();
            }
            this.zzgnt.put(zzdbl, zzday);
            this.zzgnv.zzapg();
        }
        zzb = zzday.zzb(zzdbi);
        this.zzgnv.zzapi();
        zzdbf zzapj = this.zzgnv.zzapj();
        zzdbo zzapb = zzday.zzapb();
        if (zzdbi != null) {
            zzdbi.zzgpc.zzahb().zzb((zzsy.zza) ((zzdrt) zzsy.zza.zzmz().zza(zzsy.zza.C0152zza.zzmx().zzb(zzsy.zza.zzc.IN_MEMORY).zza(zzsy.zza.zze.zznd().zzu(zzapj.zzgoq).zzv(zzapj.zzgor).zzbv(zzapb.zzgpj))).zzbaf()));
        }
        dumpToLog();
        return zzb;
    }

    public final zzdbl zza(zzug zzug, String str, zzuo zzuo) {
        return new zzdbk(zzug, str, new zzaqs(this.zzgnu.zzup).zzug().zzdmo, this.zzgnu.zzgol, zzuo);
    }

    private final void zza(zzdbi<?> zzdbi, zzdbo zzdbo) {
        if (zzdbi != null) {
            zzdbi.zzgpc.zzahb().zza((zzsy.zza) zzsy.zza.zzmz().zza(zzsy.zza.C0152zza.zzmx().zzb(zzsy.zza.zzc.IN_MEMORY).zza(zzsy.zza.zzd.zznb().zzt(zzdbo.zzgpi).zzbu(zzdbo.zzgpj))).zzbaf());
        }
        dumpToLog();
    }
}
