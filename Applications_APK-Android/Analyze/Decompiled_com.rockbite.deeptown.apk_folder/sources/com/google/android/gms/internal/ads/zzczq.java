package com.google.android.gms.internal.ads;

import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzczq {
    public String name;
    public Map<String, String> zzgmd;

    zzczq(String str, Map<String, String> map) {
        this.name = str;
        this.zzgmd = map;
    }
}
