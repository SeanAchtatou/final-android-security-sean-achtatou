package com.google.android.gms.internal.ads;

import android.view.View;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzbfd implements Runnable {
    private final /* synthetic */ View val$view;
    private final /* synthetic */ zzato zzeev;
    private final /* synthetic */ int zzeew;
    private final /* synthetic */ zzbfb zzehv;

    zzbfd(zzbfb zzbfb, View view, zzato zzato, int i2) {
        this.zzehv = zzbfb;
        this.val$view = view;
        this.zzeev = zzato;
        this.zzeew = i2;
    }

    public final void run() {
        this.zzehv.zza(this.val$view, this.zzeev, this.zzeew - 1);
    }
}
