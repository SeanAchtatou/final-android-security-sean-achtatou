package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzbus extends zzboe<zzbup> {
    zzbup zzaek();

    zzbus zzb(zzcns zzcns);

    zzbus zzd(zzbod zzbod);

    zzbus zzd(zzbrm zzbrm);
}
