package com.google.android.gms.internal.ads;

import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzaxg {
    private final String[] zzdts;
    private final double[] zzdtt;
    private final double[] zzdtu;
    private final int[] zzdtv;
    private int zzdtw;

    private zzaxg(zzaxl zzaxl) {
        int size = zzaxl.zzdue.size();
        this.zzdts = (String[]) zzaxl.zzdud.toArray(new String[size]);
        this.zzdtt = zze(zzaxl.zzdue);
        this.zzdtu = zze(zzaxl.zzduf);
        this.zzdtv = new int[size];
        this.zzdtw = 0;
    }

    private static double[] zze(List<Double> list) {
        double[] dArr = new double[list.size()];
        for (int i2 = 0; i2 < dArr.length; i2++) {
            dArr[i2] = list.get(i2).doubleValue();
        }
        return dArr;
    }

    public final void zza(double d2) {
        this.zzdtw++;
        int i2 = 0;
        while (true) {
            double[] dArr = this.zzdtu;
            if (i2 < dArr.length) {
                if (dArr[i2] <= d2 && d2 < this.zzdtt[i2]) {
                    int[] iArr = this.zzdtv;
                    iArr[i2] = iArr[i2] + 1;
                }
                if (d2 >= this.zzdtu[i2]) {
                    i2++;
                } else {
                    return;
                }
            } else {
                return;
            }
        }
    }

    public final List<zzaxi> zzwz() {
        ArrayList arrayList = new ArrayList(this.zzdts.length);
        int i2 = 0;
        while (true) {
            String[] strArr = this.zzdts;
            if (i2 >= strArr.length) {
                return arrayList;
            }
            String str = strArr[i2];
            double d2 = this.zzdtu[i2];
            double d3 = this.zzdtt[i2];
            int[] iArr = this.zzdtv;
            double d4 = (double) iArr[i2];
            double d5 = (double) this.zzdtw;
            Double.isNaN(d4);
            Double.isNaN(d5);
            arrayList.add(new zzaxi(str, d2, d3, d4 / d5, iArr[i2]));
            i2++;
        }
    }
}
