package com.google.android.gms.ads.internal.overlay;

import com.google.android.gms.internal.ads.zzbdi;
import com.google.android.gms.internal.ads.zzbeu;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzf implements zzbeu {
    private final zzc zzdhi;

    zzf(zzc zzc) {
        this.zzdhi = zzc;
    }

    public final void zzak(boolean z) {
        zzbdi zzbdi = this.zzdhi.zzcza;
        if (zzbdi != null) {
            zzbdi.zztr();
        }
    }
}
