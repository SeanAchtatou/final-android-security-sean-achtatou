package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.util.VisibleForTesting;
import com.tapjoy.TapjoyConstants;
import java.lang.ref.WeakReference;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public abstract class zzbcn implements Releasable {
    protected Context mContext;
    protected String zzduy;
    protected WeakReference<zzbaz> zzedg;

    public zzbcn(zzbaz zzbaz) {
        this.mContext = zzbaz.getContext();
        this.zzduy = zzq.zzkq().zzr(this.mContext, zzbaz.zzyr().zzbma);
        this.zzedg = new WeakReference<>(zzbaz);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: private */
    public static String zzfk(String str) {
        char c2;
        switch (str.hashCode()) {
            case -1947652542:
                if (str.equals("interrupted")) {
                    c2 = 3;
                    break;
                }
                c2 = 65535;
                break;
            case -1396664534:
                if (str.equals("badUrl")) {
                    c2 = 8;
                    break;
                }
                c2 = 65535;
                break;
            case -1347010958:
                if (str.equals("inProgress")) {
                    c2 = 2;
                    break;
                }
                c2 = 65535;
                break;
            case -918817863:
                if (str.equals("downloadTimeout")) {
                    c2 = 9;
                    break;
                }
                c2 = 65535;
                break;
            case -659376217:
                if (str.equals("contentLengthMissing")) {
                    c2 = 0;
                    break;
                }
                c2 = 65535;
                break;
            case -642208130:
                if (str.equals("playerFailed")) {
                    c2 = 5;
                    break;
                }
                c2 = 65535;
                break;
            case -354048396:
                if (str.equals("sizeExceeded")) {
                    c2 = 11;
                    break;
                }
                c2 = 65535;
                break;
            case -32082395:
                if (str.equals("externalAbort")) {
                    c2 = 10;
                    break;
                }
                c2 = 65535;
                break;
            case 3387234:
                if (str.equals("noop")) {
                    c2 = 4;
                    break;
                }
                c2 = 65535;
                break;
            case 96784904:
                if (str.equals("error")) {
                    c2 = 1;
                    break;
                }
                c2 = 65535;
                break;
            case 580119100:
                if (str.equals("expireFailed")) {
                    c2 = 6;
                    break;
                }
                c2 = 65535;
                break;
            case 725497484:
                if (str.equals("noCacheDir")) {
                    c2 = 7;
                    break;
                }
                c2 = 65535;
                break;
            default:
                c2 = 65535;
                break;
        }
        switch (c2) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            default:
                return TapjoyConstants.LOG_LEVEL_INTERNAL;
            case 6:
            case 7:
                return "io";
            case 8:
            case 9:
                return "network";
            case 10:
            case 11:
                return "policy";
        }
    }

    public abstract void abort();

    public void release() {
    }

    @VisibleForTesting
    public final void zza(String str, String str2, long j2, long j3, boolean z, int i2, int i3) {
        zzayk.zzyu.post(new zzbcp(this, str, str2, j2, j3, z, i2, i3));
    }

    @VisibleForTesting
    public final void zzb(String str, String str2, long j2) {
        zzayk.zzyu.post(new zzbcq(this, str, str2, j2));
    }

    /* access modifiers changed from: protected */
    public void zzcv(int i2) {
    }

    /* access modifiers changed from: protected */
    public void zzcw(int i2) {
    }

    /* access modifiers changed from: protected */
    public void zzcx(int i2) {
    }

    /* access modifiers changed from: protected */
    public void zzcy(int i2) {
    }

    public boolean zze(String str, String[] strArr) {
        return zzfi(str);
    }

    public abstract boolean zzfi(String str);

    /* access modifiers changed from: protected */
    public String zzfj(String str) {
        zzve.zzou();
        return zzayk.zzes(str);
    }

    @VisibleForTesting
    public final void zza(String str, String str2, int i2, int i3, long j2, long j3, boolean z, int i4, int i5) {
        zzayk.zzyu.post(new zzbco(this, str, str2, i2, i3, j2, j3, z, i4, i5));
    }

    /* access modifiers changed from: protected */
    public final void zza(String str, String str2, int i2) {
        zzayk.zzyu.post(new zzbcr(this, str, str2, i2));
    }

    @VisibleForTesting
    public final void zza(String str, String str2, String str3, String str4) {
        zzayk.zzyu.post(new zzbct(this, str, str2, str3, str4));
    }

    /* access modifiers changed from: private */
    public final void zza(String str, Map<String, String> map) {
        zzbaz zzbaz = this.zzedg.get();
        if (zzbaz != null) {
            zzbaz.zza(str, map);
        }
    }
}
