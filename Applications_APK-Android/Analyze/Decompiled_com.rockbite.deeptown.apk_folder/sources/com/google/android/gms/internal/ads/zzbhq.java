package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public class zzbhq {
    private zza zzfah;

    /* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
    public static abstract class zza {
        public abstract zzbcv zzacw();

        public abstract zzbaj zzacx();

        public abstract zzsr zzacy();

        public abstract zzatq zzacz();
    }

    public zzbhq(zza zza2) {
        this.zzfah = zza2;
    }

    public final zzatq zzacz() {
        return this.zzfah.zzacz();
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [com.google.android.gms.internal.ads.zzatr, com.google.android.gms.internal.ads.zzatl] */
    public final com.google.android.gms.ads.internal.zza zzaex() {
        zza zza2 = this.zzfah;
        return new com.google.android.gms.ads.internal.zza(zza2.zzacw(), zza2.zzacx(), new zzatl(zza2.zzacz()), zza2.zzacy());
    }
}
