package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzaqx {
    public final long zzdnl = zzq.zzkx().currentTimeMillis();
    public final zzaqt zzdnm;

    public zzaqx(zzaqv zzaqv, zzaqt zzaqt) {
        this.zzdnm = zzaqt;
    }
}
