package com.google.android.gms.common.server.response;

import com.esotericsoftware.spine.Animation;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.ShowFirstParty;
import com.google.android.gms.common.server.response.FastJsonResponse;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Stack;

@ShowFirstParty
@KeepForSdk
/* compiled from: com.google.android.gms:play-services-base@@17.1.0 */
public class FastParser<T extends FastJsonResponse> {
    private static final char[] zaqu = {'u', 'l', 'l'};
    private static final char[] zaqv = {'r', 'u', 'e'};
    private static final char[] zaqw = {'r', 'u', 'e', '\"'};
    private static final char[] zaqx = {'a', 'l', 's', 'e'};
    private static final char[] zaqy = {'a', 'l', 's', 'e', '\"'};
    private static final char[] zaqz = {10};
    private static final zaa<Integer> zarb = new zab();
    private static final zaa<Long> zarc = new zaa();
    private static final zaa<Float> zard = new zad();
    private static final zaa<Double> zare = new zac();
    private static final zaa<Boolean> zarf = new zaf();
    private static final zaa<String> zarg = new zae();
    private static final zaa<BigInteger> zarh = new zah();
    private static final zaa<BigDecimal> zari = new zag();
    private final char[] zaqp = new char[1];
    private final char[] zaqq = new char[32];
    private final char[] zaqr = new char[1024];
    private final StringBuilder zaqs = new StringBuilder(32);
    private final StringBuilder zaqt = new StringBuilder(1024);
    private final Stack<Integer> zara = new Stack<>();

    @ShowFirstParty
    @KeepForSdk
    /* compiled from: com.google.android.gms:play-services-base@@17.1.0 */
    public static class ParseException extends Exception {
        public ParseException(String str) {
            super(str);
        }

        public ParseException(String str, Throwable th) {
            super(str, th);
        }

        public ParseException(Throwable th) {
            super(th);
        }
    }

    /* compiled from: com.google.android.gms:play-services-base@@17.1.0 */
    private interface zaa<O> {
        O zah(FastParser fastParser, BufferedReader bufferedReader) throws ParseException, IOException;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.server.response.FastParser.zaa(java.io.BufferedReader, boolean):boolean
     arg types: [java.io.BufferedReader, int]
     candidates:
      com.google.android.gms.common.server.response.FastParser.zaa(com.google.android.gms.common.server.response.FastParser, java.io.BufferedReader):int
      com.google.android.gms.common.server.response.FastParser.zaa(java.io.BufferedReader, char[]):int
      com.google.android.gms.common.server.response.FastParser.zaa(java.io.BufferedReader, com.google.android.gms.common.server.response.FastJsonResponse$Field<?, ?>):java.util.ArrayList<T>
      com.google.android.gms.common.server.response.FastParser.zaa(java.io.BufferedReader, com.google.android.gms.common.server.response.FastParser$zaa):java.util.ArrayList<O>
      com.google.android.gms.common.server.response.FastParser.zaa(java.io.BufferedReader, com.google.android.gms.common.server.response.FastJsonResponse):boolean
      com.google.android.gms.common.server.response.FastParser.zaa(java.io.BufferedReader, boolean):boolean */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x027b  */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x0299  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final boolean zaa(java.io.BufferedReader r17, com.google.android.gms.common.server.response.FastJsonResponse r18) throws com.google.android.gms.common.server.response.FastParser.ParseException, java.io.IOException {
        /*
            r16 = this;
            r1 = r16
            r0 = r17
            r2 = r18
            java.lang.String r3 = "Error instantiating inner object"
            java.util.Map r4 = r18.getFieldMappings()
            java.lang.String r5 = r16.zaa(r17)
            r6 = 0
            r7 = 1
            java.lang.Integer r8 = java.lang.Integer.valueOf(r7)
            if (r5 != 0) goto L_0x001c
            r1.zak(r7)
            return r6
        L_0x001c:
            r9 = 0
        L_0x001d:
            if (r5 == 0) goto L_0x029f
            java.lang.Object r5 = r4.get(r5)
            com.google.android.gms.common.server.response.FastJsonResponse$Field r5 = (com.google.android.gms.common.server.response.FastJsonResponse.Field) r5
            if (r5 != 0) goto L_0x002c
            java.lang.String r5 = r16.zab(r17)
            goto L_0x001d
        L_0x002c:
            java.util.Stack<java.lang.Integer> r10 = r1.zara
            r11 = 4
            java.lang.Integer r12 = java.lang.Integer.valueOf(r11)
            r10.push(r12)
            int r10 = r5.zaqf
            r12 = 123(0x7b, float:1.72E-43)
            r13 = 44
            r14 = 125(0x7d, float:1.75E-43)
            r15 = 110(0x6e, float:1.54E-43)
            switch(r10) {
                case 0: goto L_0x0258;
                case 1: goto L_0x0242;
                case 2: goto L_0x022c;
                case 3: goto L_0x0216;
                case 4: goto L_0x0200;
                case 5: goto L_0x01e8;
                case 6: goto L_0x01d0;
                case 7: goto L_0x01ba;
                case 8: goto L_0x01a5;
                case 9: goto L_0x0193;
                case 10: goto L_0x00d0;
                case 11: goto L_0x005c;
                default: goto L_0x0043;
            }
        L_0x0043:
            com.google.android.gms.common.server.response.FastParser$ParseException r0 = new com.google.android.gms.common.server.response.FastParser$ParseException
            r2 = 30
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r2)
            java.lang.String r2 = "Invalid field type "
            r3.append(r2)
            r3.append(r10)
            java.lang.String r2 = r3.toString()
            r0.<init>(r2)
            throw r0
        L_0x005c:
            boolean r10 = r5.zaqg
            if (r10 == 0) goto L_0x0093
            char r10 = r16.zaj(r17)
            if (r10 != r15) goto L_0x0072
            char[] r10 = com.google.android.gms.common.server.response.FastParser.zaqu
            r1.zab(r0, r10)
            java.lang.String r10 = r5.zaqj
            r2.addConcreteTypeArrayInternal(r5, r10, r9)
            goto L_0x026d
        L_0x0072:
            java.util.Stack<java.lang.Integer> r12 = r1.zara
            r15 = 5
            java.lang.Integer r15 = java.lang.Integer.valueOf(r15)
            r12.push(r15)
            r12 = 91
            if (r10 != r12) goto L_0x008b
            java.lang.String r10 = r5.zaqj
            java.util.ArrayList r12 = r1.zaa(r0, r5)
            r2.addConcreteTypeArrayInternal(r5, r10, r12)
            goto L_0x026d
        L_0x008b:
            com.google.android.gms.common.server.response.FastParser$ParseException r0 = new com.google.android.gms.common.server.response.FastParser$ParseException
            java.lang.String r2 = "Expected array start"
            r0.<init>(r2)
            throw r0
        L_0x0093:
            char r10 = r16.zaj(r17)
            if (r10 != r15) goto L_0x00a5
            char[] r10 = com.google.android.gms.common.server.response.FastParser.zaqu
            r1.zab(r0, r10)
            java.lang.String r10 = r5.zaqj
            r2.addConcreteTypeInternal(r5, r10, r9)
            goto L_0x026d
        L_0x00a5:
            java.util.Stack<java.lang.Integer> r15 = r1.zara
            r15.push(r8)
            if (r10 != r12) goto L_0x00c8
            com.google.android.gms.common.server.response.FastJsonResponse r10 = r5.zacn()     // Catch:{ InstantiationException -> 0x00c1, IllegalAccessException -> 0x00ba }
            r1.zaa(r0, r10)     // Catch:{ InstantiationException -> 0x00c1, IllegalAccessException -> 0x00ba }
            java.lang.String r12 = r5.zaqj     // Catch:{ InstantiationException -> 0x00c1, IllegalAccessException -> 0x00ba }
            r2.addConcreteTypeInternal(r5, r12, r10)     // Catch:{ InstantiationException -> 0x00c1, IllegalAccessException -> 0x00ba }
            goto L_0x026d
        L_0x00ba:
            r0 = move-exception
            com.google.android.gms.common.server.response.FastParser$ParseException r2 = new com.google.android.gms.common.server.response.FastParser$ParseException
            r2.<init>(r3, r0)
            throw r2
        L_0x00c1:
            r0 = move-exception
            com.google.android.gms.common.server.response.FastParser$ParseException r2 = new com.google.android.gms.common.server.response.FastParser$ParseException
            r2.<init>(r3, r0)
            throw r2
        L_0x00c8:
            com.google.android.gms.common.server.response.FastParser$ParseException r0 = new com.google.android.gms.common.server.response.FastParser$ParseException
            java.lang.String r2 = "Expected start of object"
            r0.<init>(r2)
            throw r0
        L_0x00d0:
            char r10 = r16.zaj(r17)
            if (r10 != r15) goto L_0x00de
            char[] r10 = com.google.android.gms.common.server.response.FastParser.zaqu
            r1.zab(r0, r10)
            r10 = r9
            goto L_0x0162
        L_0x00de:
            if (r10 != r12) goto L_0x018b
            java.util.Stack<java.lang.Integer> r10 = r1.zara
            r10.push(r8)
            java.util.HashMap r10 = new java.util.HashMap
            r10.<init>()
        L_0x00ea:
            char r12 = r16.zaj(r17)
            if (r12 == 0) goto L_0x0183
            r15 = 34
            if (r12 == r15) goto L_0x00fc
            if (r12 == r14) goto L_0x00f8
            goto L_0x017f
        L_0x00f8:
            r1.zak(r7)
            goto L_0x0162
        L_0x00fc:
            char[] r12 = r1.zaqq
            java.lang.StringBuilder r11 = r1.zaqs
            java.lang.String r11 = zab(r0, r12, r11, r9)
            char r12 = r16.zaj(r17)
            r6 = 58
            if (r12 == r6) goto L_0x0129
            com.google.android.gms.common.server.response.FastParser$ParseException r0 = new com.google.android.gms.common.server.response.FastParser$ParseException
            java.lang.String r2 = "No map value found for key "
            java.lang.String r3 = java.lang.String.valueOf(r11)
            int r4 = r3.length()
            if (r4 == 0) goto L_0x011f
            java.lang.String r2 = r2.concat(r3)
            goto L_0x0125
        L_0x011f:
            java.lang.String r3 = new java.lang.String
            r3.<init>(r2)
            r2 = r3
        L_0x0125:
            r0.<init>(r2)
            throw r0
        L_0x0129:
            char r6 = r16.zaj(r17)
            if (r6 == r15) goto L_0x014c
            com.google.android.gms.common.server.response.FastParser$ParseException r0 = new com.google.android.gms.common.server.response.FastParser$ParseException
            java.lang.String r2 = "Expected String value for key "
            java.lang.String r3 = java.lang.String.valueOf(r11)
            int r4 = r3.length()
            if (r4 == 0) goto L_0x0142
            java.lang.String r2 = r2.concat(r3)
            goto L_0x0148
        L_0x0142:
            java.lang.String r3 = new java.lang.String
            r3.<init>(r2)
            r2 = r3
        L_0x0148:
            r0.<init>(r2)
            throw r0
        L_0x014c:
            char[] r6 = r1.zaqq
            java.lang.StringBuilder r12 = r1.zaqs
            java.lang.String r6 = zab(r0, r6, r12, r9)
            r10.put(r11, r6)
            char r6 = r16.zaj(r17)
            if (r6 == r13) goto L_0x017f
            if (r6 != r14) goto L_0x0166
            r1.zak(r7)
        L_0x0162:
            r2.zaa(r5, r10)
            goto L_0x01b6
        L_0x0166:
            com.google.android.gms.common.server.response.FastParser$ParseException r0 = new com.google.android.gms.common.server.response.FastParser$ParseException
            r2 = 48
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r2)
            java.lang.String r2 = "Unexpected character while parsing string map: "
            r3.append(r2)
            r3.append(r6)
            java.lang.String r2 = r3.toString()
            r0.<init>(r2)
            throw r0
        L_0x017f:
            r6 = 0
            r11 = 4
            goto L_0x00ea
        L_0x0183:
            com.google.android.gms.common.server.response.FastParser$ParseException r0 = new com.google.android.gms.common.server.response.FastParser$ParseException
            java.lang.String r2 = "Unexpected EOF"
            r0.<init>(r2)
            throw r0
        L_0x018b:
            com.google.android.gms.common.server.response.FastParser$ParseException r0 = new com.google.android.gms.common.server.response.FastParser$ParseException
            java.lang.String r2 = "Expected start of a map object"
            r0.<init>(r2)
            throw r0
        L_0x0193:
            char[] r6 = r1.zaqr
            java.lang.StringBuilder r10 = r1.zaqt
            char[] r11 = com.google.android.gms.common.server.response.FastParser.zaqz
            java.lang.String r6 = r1.zaa(r0, r6, r10, r11)
            byte[] r6 = com.google.android.gms.common.util.Base64Utils.decodeUrlSafe(r6)
            r2.zaa(r5, r6)
            goto L_0x01b6
        L_0x01a5:
            char[] r6 = r1.zaqr
            java.lang.StringBuilder r10 = r1.zaqt
            char[] r11 = com.google.android.gms.common.server.response.FastParser.zaqz
            java.lang.String r6 = r1.zaa(r0, r6, r10, r11)
            byte[] r6 = com.google.android.gms.common.util.Base64Utils.decode(r6)
            r2.zaa(r5, r6)
        L_0x01b6:
            r5 = 4
            r6 = 0
            goto L_0x026e
        L_0x01ba:
            boolean r6 = r5.zaqg
            if (r6 == 0) goto L_0x01c8
            com.google.android.gms.common.server.response.FastParser$zaa<java.lang.String> r6 = com.google.android.gms.common.server.response.FastParser.zarg
            java.util.ArrayList r6 = r1.zaa(r0, r6)
            r2.zah(r5, r6)
            goto L_0x01b6
        L_0x01c8:
            java.lang.String r6 = r16.zac(r17)
            r2.zaa(r5, r6)
            goto L_0x01b6
        L_0x01d0:
            boolean r6 = r5.zaqg
            if (r6 == 0) goto L_0x01de
            com.google.android.gms.common.server.response.FastParser$zaa<java.lang.Boolean> r6 = com.google.android.gms.common.server.response.FastParser.zarf
            java.util.ArrayList r6 = r1.zaa(r0, r6)
            r2.zag(r5, r6)
            goto L_0x01b6
        L_0x01de:
            r6 = 0
            boolean r10 = r1.zaa(r0, r6)
            r2.zaa(r5, r10)
            goto L_0x026d
        L_0x01e8:
            boolean r10 = r5.zaqg
            if (r10 == 0) goto L_0x01f7
            com.google.android.gms.common.server.response.FastParser$zaa<java.math.BigDecimal> r10 = com.google.android.gms.common.server.response.FastParser.zari
            java.util.ArrayList r10 = r1.zaa(r0, r10)
            r2.zaf(r5, r10)
            goto L_0x026d
        L_0x01f7:
            java.math.BigDecimal r10 = r16.zai(r17)
            r2.zaa(r5, r10)
            goto L_0x026d
        L_0x0200:
            boolean r10 = r5.zaqg
            if (r10 == 0) goto L_0x020e
            com.google.android.gms.common.server.response.FastParser$zaa<java.lang.Double> r10 = com.google.android.gms.common.server.response.FastParser.zare
            java.util.ArrayList r10 = r1.zaa(r0, r10)
            r2.zae(r5, r10)
            goto L_0x026d
        L_0x020e:
            double r10 = r16.zah(r17)
            r2.zaa(r5, r10)
            goto L_0x026d
        L_0x0216:
            boolean r10 = r5.zaqg
            if (r10 == 0) goto L_0x0224
            com.google.android.gms.common.server.response.FastParser$zaa<java.lang.Float> r10 = com.google.android.gms.common.server.response.FastParser.zard
            java.util.ArrayList r10 = r1.zaa(r0, r10)
            r2.zad(r5, r10)
            goto L_0x026d
        L_0x0224:
            float r10 = r16.zag(r17)
            r2.zaa(r5, r10)
            goto L_0x026d
        L_0x022c:
            boolean r10 = r5.zaqg
            if (r10 == 0) goto L_0x023a
            com.google.android.gms.common.server.response.FastParser$zaa<java.lang.Long> r10 = com.google.android.gms.common.server.response.FastParser.zarc
            java.util.ArrayList r10 = r1.zaa(r0, r10)
            r2.zac(r5, r10)
            goto L_0x026d
        L_0x023a:
            long r10 = r16.zae(r17)
            r2.zaa(r5, r10)
            goto L_0x026d
        L_0x0242:
            boolean r10 = r5.zaqg
            if (r10 == 0) goto L_0x0250
            com.google.android.gms.common.server.response.FastParser$zaa<java.math.BigInteger> r10 = com.google.android.gms.common.server.response.FastParser.zarh
            java.util.ArrayList r10 = r1.zaa(r0, r10)
            r2.zab(r5, r10)
            goto L_0x026d
        L_0x0250:
            java.math.BigInteger r10 = r16.zaf(r17)
            r2.zaa(r5, r10)
            goto L_0x026d
        L_0x0258:
            boolean r10 = r5.zaqg
            if (r10 == 0) goto L_0x0266
            com.google.android.gms.common.server.response.FastParser$zaa<java.lang.Integer> r10 = com.google.android.gms.common.server.response.FastParser.zarb
            java.util.ArrayList r10 = r1.zaa(r0, r10)
            r2.zaa(r5, r10)
            goto L_0x026d
        L_0x0266:
            int r10 = r16.zad(r17)
            r2.zaa(r5, r10)
        L_0x026d:
            r5 = 4
        L_0x026e:
            r1.zak(r5)
            r5 = 2
            r1.zak(r5)
            char r5 = r16.zaj(r17)
            if (r5 == r13) goto L_0x0299
            if (r5 != r14) goto L_0x0280
            r5 = r9
            goto L_0x001d
        L_0x0280:
            com.google.android.gms.common.server.response.FastParser$ParseException r0 = new com.google.android.gms.common.server.response.FastParser$ParseException
            r2 = 55
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r2)
            java.lang.String r2 = "Expected end of object or field separator, but found: "
            r3.append(r2)
            r3.append(r5)
            java.lang.String r2 = r3.toString()
            r0.<init>(r2)
            throw r0
        L_0x0299:
            java.lang.String r5 = r16.zaa(r17)
            goto L_0x001d
        L_0x029f:
            r1.zak(r7)
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.server.response.FastParser.zaa(java.io.BufferedReader, com.google.android.gms.common.server.response.FastJsonResponse):boolean");
    }

    private final String zab(BufferedReader bufferedReader) throws ParseException, IOException {
        BufferedReader bufferedReader2 = bufferedReader;
        bufferedReader2.mark(1024);
        char zaj = zaj(bufferedReader);
        if (zaj != '\"') {
            if (zaj != ',') {
                int i2 = 1;
                if (zaj == '[') {
                    this.zara.push(5);
                    bufferedReader2.mark(32);
                    if (zaj(bufferedReader) == ']') {
                        zak(5);
                    } else {
                        bufferedReader.reset();
                        boolean z = false;
                        boolean z2 = false;
                        while (i2 > 0) {
                            char zaj2 = zaj(bufferedReader);
                            if (zaj2 == 0) {
                                throw new ParseException("Unexpected EOF while parsing array");
                            } else if (!Character.isISOControl(zaj2)) {
                                if (zaj2 == '\"' && !z) {
                                    z2 = !z2;
                                }
                                if (zaj2 == '[' && !z2) {
                                    i2++;
                                }
                                if (zaj2 == ']' && !z2) {
                                    i2--;
                                }
                                z = (zaj2 != '\\' || !z2) ? false : !z;
                            } else {
                                throw new ParseException("Unexpected control character while reading array");
                            }
                        }
                        zak(5);
                    }
                } else if (zaj != '{') {
                    bufferedReader.reset();
                    zaa(bufferedReader2, this.zaqr);
                } else {
                    this.zara.push(1);
                    bufferedReader2.mark(32);
                    char zaj3 = zaj(bufferedReader);
                    if (zaj3 == '}') {
                        zak(1);
                    } else if (zaj3 == '\"') {
                        bufferedReader.reset();
                        zaa(bufferedReader);
                        do {
                        } while (zab(bufferedReader) != null);
                        zak(1);
                    } else {
                        StringBuilder sb = new StringBuilder(18);
                        sb.append("Unexpected token ");
                        sb.append(zaj3);
                        throw new ParseException(sb.toString());
                    }
                }
            } else {
                throw new ParseException("Missing value");
            }
        } else if (bufferedReader2.read(this.zaqp) != -1) {
            char c2 = this.zaqp[0];
            boolean z3 = false;
            do {
                if (c2 != '\"' || z3) {
                    z3 = c2 == '\\' ? !z3 : false;
                    if (bufferedReader2.read(this.zaqp) != -1) {
                        c2 = this.zaqp[0];
                    } else {
                        throw new ParseException("Unexpected EOF while parsing string");
                    }
                }
            } while (!Character.isISOControl(c2));
            throw new ParseException("Unexpected control character while reading string");
        } else {
            throw new ParseException("Unexpected EOF while parsing string");
        }
        char zaj4 = zaj(bufferedReader);
        if (zaj4 == ',') {
            zak(2);
            return zaa(bufferedReader);
        } else if (zaj4 == '}') {
            zak(2);
            return null;
        } else {
            StringBuilder sb2 = new StringBuilder(18);
            sb2.append("Unexpected token ");
            sb2.append(zaj4);
            throw new ParseException(sb2.toString());
        }
    }

    /* access modifiers changed from: private */
    public final String zac(BufferedReader bufferedReader) throws ParseException, IOException {
        return zaa(bufferedReader, this.zaqq, this.zaqs, null);
    }

    /* access modifiers changed from: private */
    public final int zad(BufferedReader bufferedReader) throws ParseException, IOException {
        int i2;
        boolean z;
        int i3;
        int i4;
        int i5;
        int zaa2 = zaa(bufferedReader, this.zaqr);
        if (zaa2 == 0) {
            return 0;
        }
        char[] cArr = this.zaqr;
        if (zaa2 > 0) {
            if (cArr[0] == '-') {
                i3 = 1;
                z = true;
                i2 = Integer.MIN_VALUE;
            } else {
                i3 = 0;
                z = false;
                i2 = -2147483647;
            }
            if (i3 < zaa2) {
                i5 = i3 + 1;
                int digit = Character.digit(cArr[i3], 10);
                if (digit >= 0) {
                    i4 = -digit;
                } else {
                    throw new ParseException("Unexpected non-digit character");
                }
            } else {
                i5 = i3;
                i4 = 0;
            }
            while (i5 < zaa2) {
                int i6 = i5 + 1;
                int digit2 = Character.digit(cArr[i5], 10);
                if (digit2 < 0) {
                    throw new ParseException("Unexpected non-digit character");
                } else if (i4 >= -214748364) {
                    int i7 = i4 * 10;
                    if (i7 >= i2 + digit2) {
                        i4 = i7 - digit2;
                        i5 = i6;
                    } else {
                        throw new ParseException("Number too large");
                    }
                } else {
                    throw new ParseException("Number too large");
                }
            }
            if (!z) {
                return -i4;
            }
            if (i5 > 1) {
                return i4;
            }
            throw new ParseException("No digits to parse");
        }
        throw new ParseException("No number to parse");
    }

    /* access modifiers changed from: private */
    public final long zae(BufferedReader bufferedReader) throws ParseException, IOException {
        boolean z;
        long j2;
        long j3;
        int i2;
        int zaa2 = zaa(bufferedReader, this.zaqr);
        if (zaa2 == 0) {
            return 0;
        }
        char[] cArr = this.zaqr;
        if (zaa2 > 0) {
            int i3 = 0;
            if (cArr[0] == '-') {
                j2 = Long.MIN_VALUE;
                i3 = 1;
                z = true;
            } else {
                j2 = -9223372036854775807L;
                z = false;
            }
            if (i3 < zaa2) {
                i2 = i3 + 1;
                int digit = Character.digit(cArr[i3], 10);
                if (digit >= 0) {
                    j3 = (long) (-digit);
                } else {
                    throw new ParseException("Unexpected non-digit character");
                }
            } else {
                j3 = 0;
                i2 = i3;
            }
            while (i2 < zaa2) {
                int i4 = i2 + 1;
                int digit2 = Character.digit(cArr[i2], 10);
                if (digit2 < 0) {
                    throw new ParseException("Unexpected non-digit character");
                } else if (j3 >= -922337203685477580L) {
                    long j4 = j3 * 10;
                    long j5 = (long) digit2;
                    if (j4 >= j2 + j5) {
                        j3 = j4 - j5;
                        i2 = i4;
                    } else {
                        throw new ParseException("Number too large");
                    }
                } else {
                    throw new ParseException("Number too large");
                }
            }
            if (!z) {
                return -j3;
            }
            if (i2 > 1) {
                return j3;
            }
            throw new ParseException("No digits to parse");
        }
        throw new ParseException("No number to parse");
    }

    /* access modifiers changed from: private */
    public final BigInteger zaf(BufferedReader bufferedReader) throws ParseException, IOException {
        int zaa2 = zaa(bufferedReader, this.zaqr);
        if (zaa2 == 0) {
            return null;
        }
        return new BigInteger(new String(this.zaqr, 0, zaa2));
    }

    /* access modifiers changed from: private */
    public final float zag(BufferedReader bufferedReader) throws ParseException, IOException {
        int zaa2 = zaa(bufferedReader, this.zaqr);
        if (zaa2 == 0) {
            return Animation.CurveTimeline.LINEAR;
        }
        return Float.parseFloat(new String(this.zaqr, 0, zaa2));
    }

    /* access modifiers changed from: private */
    public final double zah(BufferedReader bufferedReader) throws ParseException, IOException {
        int zaa2 = zaa(bufferedReader, this.zaqr);
        if (zaa2 == 0) {
            return FirebaseRemoteConfig.DEFAULT_VALUE_FOR_DOUBLE;
        }
        return Double.parseDouble(new String(this.zaqr, 0, zaa2));
    }

    /* access modifiers changed from: private */
    public final BigDecimal zai(BufferedReader bufferedReader) throws ParseException, IOException {
        int zaa2 = zaa(bufferedReader, this.zaqr);
        if (zaa2 == 0) {
            return null;
        }
        return new BigDecimal(new String(this.zaqr, 0, zaa2));
    }

    private final char zaj(BufferedReader bufferedReader) throws ParseException, IOException {
        if (bufferedReader.read(this.zaqp) == -1) {
            return 0;
        }
        while (Character.isWhitespace(this.zaqp[0])) {
            if (bufferedReader.read(this.zaqp) == -1) {
                return 0;
            }
        }
        return this.zaqp[0];
    }

    private final void zak(int i2) throws ParseException {
        if (!this.zara.isEmpty()) {
            int intValue = this.zara.pop().intValue();
            if (intValue != i2) {
                StringBuilder sb = new StringBuilder(46);
                sb.append("Expected state ");
                sb.append(i2);
                sb.append(" but had ");
                sb.append(intValue);
                throw new ParseException(sb.toString());
            }
            return;
        }
        StringBuilder sb2 = new StringBuilder(46);
        sb2.append("Expected state ");
        sb2.append(i2);
        sb2.append(" but had empty stack");
        throw new ParseException(sb2.toString());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.server.response.FastParser.zaa(java.io.BufferedReader, com.google.android.gms.common.server.response.FastJsonResponse):boolean
     arg types: [java.io.BufferedReader, T]
     candidates:
      com.google.android.gms.common.server.response.FastParser.zaa(com.google.android.gms.common.server.response.FastParser, java.io.BufferedReader):int
      com.google.android.gms.common.server.response.FastParser.zaa(java.io.BufferedReader, char[]):int
      com.google.android.gms.common.server.response.FastParser.zaa(java.io.BufferedReader, com.google.android.gms.common.server.response.FastJsonResponse$Field<?, ?>):java.util.ArrayList<T>
      com.google.android.gms.common.server.response.FastParser.zaa(java.io.BufferedReader, com.google.android.gms.common.server.response.FastParser$zaa):java.util.ArrayList<O>
      com.google.android.gms.common.server.response.FastParser.zaa(java.io.BufferedReader, boolean):boolean
      com.google.android.gms.common.server.response.FastParser.zaa(java.io.BufferedReader, com.google.android.gms.common.server.response.FastJsonResponse):boolean */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    @com.google.android.gms.common.annotation.KeepForSdk
    public void parse(java.io.InputStream r7, T r8) throws com.google.android.gms.common.server.response.FastParser.ParseException {
        /*
            r6 = this;
            java.lang.String r0 = "Failed to close reader while parsing."
            java.lang.String r1 = "FastParser"
            java.io.BufferedReader r2 = new java.io.BufferedReader
            java.io.InputStreamReader r3 = new java.io.InputStreamReader
            r3.<init>(r7)
            r7 = 1024(0x400, float:1.435E-42)
            r2.<init>(r3, r7)
            java.util.Stack<java.lang.Integer> r7 = r6.zara     // Catch:{ IOException -> 0x009d }
            r3 = 0
            java.lang.Integer r4 = java.lang.Integer.valueOf(r3)     // Catch:{ IOException -> 0x009d }
            r7.push(r4)     // Catch:{ IOException -> 0x009d }
            char r7 = r6.zaj(r2)     // Catch:{ IOException -> 0x009d }
            if (r7 == 0) goto L_0x0093
            r4 = 91
            r5 = 1
            if (r7 == r4) goto L_0x004f
            r4 = 123(0x7b, float:1.72E-43)
            if (r7 != r4) goto L_0x0036
            java.util.Stack<java.lang.Integer> r7 = r6.zara     // Catch:{ IOException -> 0x009d }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r5)     // Catch:{ IOException -> 0x009d }
            r7.push(r4)     // Catch:{ IOException -> 0x009d }
            r6.zaa(r2, r8)     // Catch:{ IOException -> 0x009d }
            goto L_0x0080
        L_0x0036:
            com.google.android.gms.common.server.response.FastParser$ParseException r8 = new com.google.android.gms.common.server.response.FastParser$ParseException     // Catch:{ IOException -> 0x009d }
            r3 = 19
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x009d }
            r4.<init>(r3)     // Catch:{ IOException -> 0x009d }
            java.lang.String r3 = "Unexpected token: "
            r4.append(r3)     // Catch:{ IOException -> 0x009d }
            r4.append(r7)     // Catch:{ IOException -> 0x009d }
            java.lang.String r7 = r4.toString()     // Catch:{ IOException -> 0x009d }
            r8.<init>(r7)     // Catch:{ IOException -> 0x009d }
            throw r8     // Catch:{ IOException -> 0x009d }
        L_0x004f:
            java.util.Stack<java.lang.Integer> r7 = r6.zara     // Catch:{ IOException -> 0x009d }
            r4 = 5
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ IOException -> 0x009d }
            r7.push(r4)     // Catch:{ IOException -> 0x009d }
            java.util.Map r7 = r8.getFieldMappings()     // Catch:{ IOException -> 0x009d }
            int r4 = r7.size()     // Catch:{ IOException -> 0x009d }
            if (r4 != r5) goto L_0x008b
            java.util.Set r7 = r7.entrySet()     // Catch:{ IOException -> 0x009d }
            java.util.Iterator r7 = r7.iterator()     // Catch:{ IOException -> 0x009d }
            java.lang.Object r7 = r7.next()     // Catch:{ IOException -> 0x009d }
            java.util.Map$Entry r7 = (java.util.Map.Entry) r7     // Catch:{ IOException -> 0x009d }
            java.lang.Object r7 = r7.getValue()     // Catch:{ IOException -> 0x009d }
            com.google.android.gms.common.server.response.FastJsonResponse$Field r7 = (com.google.android.gms.common.server.response.FastJsonResponse.Field) r7     // Catch:{ IOException -> 0x009d }
            java.util.ArrayList r4 = r6.zaa(r2, r7)     // Catch:{ IOException -> 0x009d }
            java.lang.String r5 = r7.zaqj     // Catch:{ IOException -> 0x009d }
            r8.addConcreteTypeArrayInternal(r7, r5, r4)     // Catch:{ IOException -> 0x009d }
        L_0x0080:
            r6.zak(r3)     // Catch:{ IOException -> 0x009d }
            r2.close()     // Catch:{ IOException -> 0x0087 }
            return
        L_0x0087:
            android.util.Log.w(r1, r0)
            return
        L_0x008b:
            com.google.android.gms.common.server.response.FastParser$ParseException r7 = new com.google.android.gms.common.server.response.FastParser$ParseException     // Catch:{ IOException -> 0x009d }
            java.lang.String r8 = "Object array response class must have a single Field"
            r7.<init>(r8)     // Catch:{ IOException -> 0x009d }
            throw r7     // Catch:{ IOException -> 0x009d }
        L_0x0093:
            com.google.android.gms.common.server.response.FastParser$ParseException r7 = new com.google.android.gms.common.server.response.FastParser$ParseException     // Catch:{ IOException -> 0x009d }
            java.lang.String r8 = "No data to parse"
            r7.<init>(r8)     // Catch:{ IOException -> 0x009d }
            throw r7     // Catch:{ IOException -> 0x009d }
        L_0x009b:
            r7 = move-exception
            goto L_0x00a4
        L_0x009d:
            r7 = move-exception
            com.google.android.gms.common.server.response.FastParser$ParseException r8 = new com.google.android.gms.common.server.response.FastParser$ParseException     // Catch:{ all -> 0x009b }
            r8.<init>(r7)     // Catch:{ all -> 0x009b }
            throw r8     // Catch:{ all -> 0x009b }
        L_0x00a4:
            r2.close()     // Catch:{ IOException -> 0x00a8 }
            goto L_0x00ab
        L_0x00a8:
            android.util.Log.w(r1, r0)
        L_0x00ab:
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.server.response.FastParser.parse(java.io.InputStream, com.google.android.gms.common.server.response.FastJsonResponse):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x0032 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String zab(java.io.BufferedReader r9, char[] r10, java.lang.StringBuilder r11, char[] r12) throws com.google.android.gms.common.server.response.FastParser.ParseException, java.io.IOException {
        /*
            r0 = 0
            r11.setLength(r0)
            int r1 = r10.length
            r9.mark(r1)
            r1 = 0
            r2 = 0
        L_0x000a:
            int r3 = r9.read(r10)
            r4 = -1
            if (r3 == r4) goto L_0x0071
            r4 = r2
            r2 = r1
            r1 = 0
        L_0x0014:
            if (r1 >= r3) goto L_0x0067
            char r5 = r10[r1]
            boolean r6 = java.lang.Character.isISOControl(r5)
            r7 = 1
            if (r6 == 0) goto L_0x003a
            if (r12 == 0) goto L_0x002e
            r6 = 0
        L_0x0022:
            int r8 = r12.length
            if (r6 >= r8) goto L_0x002e
            char r8 = r12[r6]
            if (r8 != r5) goto L_0x002b
            r6 = 1
            goto L_0x002f
        L_0x002b:
            int r6 = r6 + 1
            goto L_0x0022
        L_0x002e:
            r6 = 0
        L_0x002f:
            if (r6 == 0) goto L_0x0032
            goto L_0x003a
        L_0x0032:
            com.google.android.gms.common.server.response.FastParser$ParseException r9 = new com.google.android.gms.common.server.response.FastParser$ParseException
            java.lang.String r10 = "Unexpected control character while reading string"
            r9.<init>(r10)
            throw r9
        L_0x003a:
            r6 = 34
            if (r5 != r6) goto L_0x005b
            if (r2 != 0) goto L_0x005b
            r11.append(r10, r0, r1)
            r9.reset()
            int r1 = r1 + r7
            long r0 = (long) r1
            r9.skip(r0)
            if (r4 == 0) goto L_0x0056
            java.lang.String r9 = r11.toString()
            java.lang.String r9 = com.google.android.gms.common.util.JsonUtils.unescapeString(r9)
            return r9
        L_0x0056:
            java.lang.String r9 = r11.toString()
            return r9
        L_0x005b:
            r6 = 92
            if (r5 != r6) goto L_0x0063
            r2 = r2 ^ 1
            r4 = 1
            goto L_0x0064
        L_0x0063:
            r2 = 0
        L_0x0064:
            int r1 = r1 + 1
            goto L_0x0014
        L_0x0067:
            r11.append(r10, r0, r3)
            int r1 = r10.length
            r9.mark(r1)
            r1 = r2
            r2 = r4
            goto L_0x000a
        L_0x0071:
            com.google.android.gms.common.server.response.FastParser$ParseException r9 = new com.google.android.gms.common.server.response.FastParser$ParseException
            java.lang.String r10 = "Unexpected EOF while parsing string"
            r9.<init>(r10)
            goto L_0x007a
        L_0x0079:
            throw r9
        L_0x007a:
            goto L_0x0079
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.server.response.FastParser.zab(java.io.BufferedReader, char[], java.lang.StringBuilder, char[]):java.lang.String");
    }

    private final void zab(BufferedReader bufferedReader, char[] cArr) throws ParseException, IOException {
        int i2 = 0;
        while (i2 < cArr.length) {
            int read = bufferedReader.read(this.zaqq, 0, cArr.length - i2);
            if (read != -1) {
                int i3 = 0;
                while (i3 < read) {
                    if (cArr[i3 + i2] == this.zaqq[i3]) {
                        i3++;
                    } else {
                        throw new ParseException("Unexpected character");
                    }
                }
                i2 += read;
            } else {
                throw new ParseException("Unexpected EOF");
            }
        }
    }

    private final String zaa(BufferedReader bufferedReader) throws ParseException, IOException {
        this.zara.push(2);
        char zaj = zaj(bufferedReader);
        if (zaj == '\"') {
            this.zara.push(3);
            String zab = zab(bufferedReader, this.zaqq, this.zaqs, null);
            zak(3);
            if (zaj(bufferedReader) == ':') {
                return zab;
            }
            throw new ParseException("Expected key/value separator");
        } else if (zaj == ']') {
            zak(2);
            zak(1);
            zak(5);
            return null;
        } else if (zaj == '}') {
            zak(2);
            return null;
        } else {
            StringBuilder sb = new StringBuilder(19);
            sb.append("Unexpected token: ");
            sb.append(zaj);
            throw new ParseException(sb.toString());
        }
    }

    private final <O> ArrayList<O> zaa(BufferedReader bufferedReader, zaa<O> zaa2) throws ParseException, IOException {
        char zaj = zaj(bufferedReader);
        if (zaj == 'n') {
            zab(bufferedReader, zaqu);
            return null;
        } else if (zaj == '[') {
            this.zara.push(5);
            ArrayList<O> arrayList = new ArrayList<>();
            while (true) {
                bufferedReader.mark(1024);
                char zaj2 = zaj(bufferedReader);
                if (zaj2 == 0) {
                    throw new ParseException("Unexpected EOF");
                } else if (zaj2 != ',') {
                    if (zaj2 != ']') {
                        bufferedReader.reset();
                        arrayList.add(zaa2.zah(this, bufferedReader));
                    } else {
                        zak(5);
                        return arrayList;
                    }
                }
            }
        } else {
            throw new ParseException("Expected start of array");
        }
    }

    private final String zaa(BufferedReader bufferedReader, char[] cArr, StringBuilder sb, char[] cArr2) throws ParseException, IOException {
        char zaj = zaj(bufferedReader);
        if (zaj == '\"') {
            return zab(bufferedReader, cArr, sb, cArr2);
        }
        if (zaj == 'n') {
            zab(bufferedReader, zaqu);
            return null;
        }
        throw new ParseException("Expected string");
    }

    /* access modifiers changed from: private */
    public final boolean zaa(BufferedReader bufferedReader, boolean z) throws ParseException, IOException {
        while (true) {
            char zaj = zaj(bufferedReader);
            if (zaj != '\"') {
                if (zaj == 'f') {
                    zab(bufferedReader, z ? zaqy : zaqx);
                    return false;
                } else if (zaj == 'n') {
                    zab(bufferedReader, zaqu);
                    return false;
                } else if (zaj == 't') {
                    zab(bufferedReader, z ? zaqw : zaqv);
                    return true;
                } else {
                    StringBuilder sb = new StringBuilder(19);
                    sb.append("Unexpected token: ");
                    sb.append(zaj);
                    throw new ParseException(sb.toString());
                }
            } else if (!z) {
                z = true;
            } else {
                throw new ParseException("No boolean value found in string");
            }
        }
    }

    private final <T extends FastJsonResponse> ArrayList<T> zaa(BufferedReader bufferedReader, FastJsonResponse.Field<?, ?> field) throws ParseException, IOException {
        ArrayList<T> arrayList = new ArrayList<>();
        char zaj = zaj(bufferedReader);
        if (zaj == ']') {
            zak(5);
            return arrayList;
        } else if (zaj == 'n') {
            zab(bufferedReader, zaqu);
            zak(5);
            return null;
        } else if (zaj == '{') {
            this.zara.push(1);
            while (true) {
                try {
                    FastJsonResponse zacn = field.zacn();
                    if (!zaa(bufferedReader, zacn)) {
                        return arrayList;
                    }
                    arrayList.add(zacn);
                    char zaj2 = zaj(bufferedReader);
                    if (zaj2 != ',') {
                        if (zaj2 == ']') {
                            zak(5);
                            return arrayList;
                        }
                        StringBuilder sb = new StringBuilder(19);
                        sb.append("Unexpected token: ");
                        sb.append(zaj2);
                        throw new ParseException(sb.toString());
                    } else if (zaj(bufferedReader) == '{') {
                        this.zara.push(1);
                    } else {
                        throw new ParseException("Expected start of next object in array");
                    }
                } catch (InstantiationException e2) {
                    throw new ParseException("Error instantiating inner object", e2);
                } catch (IllegalAccessException e3) {
                    throw new ParseException("Error instantiating inner object", e3);
                }
            }
        } else {
            StringBuilder sb2 = new StringBuilder(19);
            sb2.append("Unexpected token: ");
            sb2.append(zaj);
            throw new ParseException(sb2.toString());
        }
    }

    private final int zaa(BufferedReader bufferedReader, char[] cArr) throws ParseException, IOException {
        int i2;
        char zaj = zaj(bufferedReader);
        if (zaj == 0) {
            throw new ParseException("Unexpected EOF");
        } else if (zaj == ',') {
            throw new ParseException("Missing value");
        } else if (zaj == 'n') {
            zab(bufferedReader, zaqu);
            return 0;
        } else {
            bufferedReader.mark(1024);
            if (zaj == '\"') {
                i2 = 0;
                boolean z = false;
                while (i2 < cArr.length && bufferedReader.read(cArr, i2, 1) != -1) {
                    char c2 = cArr[i2];
                    if (Character.isISOControl(c2)) {
                        throw new ParseException("Unexpected control character while reading string");
                    } else if (c2 != '\"' || z) {
                        z = c2 == '\\' ? !z : false;
                        i2++;
                    } else {
                        bufferedReader.reset();
                        bufferedReader.skip((long) (i2 + 1));
                        return i2;
                    }
                }
            } else {
                cArr[0] = zaj;
                int i3 = 1;
                while (i2 < cArr.length && bufferedReader.read(cArr, i2, 1) != -1) {
                    if (cArr[i2] == '}' || cArr[i2] == ',' || Character.isWhitespace(cArr[i2]) || cArr[i2] == ']') {
                        bufferedReader.reset();
                        bufferedReader.skip((long) (i2 - 1));
                        cArr[i2] = 0;
                        return i2;
                    }
                    i3 = i2 + 1;
                }
            }
            if (i2 == cArr.length) {
                throw new ParseException("Absurdly long value");
            }
            throw new ParseException("Unexpected EOF");
        }
    }
}
