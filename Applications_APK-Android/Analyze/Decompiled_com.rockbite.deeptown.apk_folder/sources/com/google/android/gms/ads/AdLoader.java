package com.google.android.gms.ads;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.google.android.gms.ads.formats.OnPublisherAdViewLoadedListener;
import com.google.android.gms.ads.formats.PublisherAdViewOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.ads.zzaby;
import com.google.android.gms.internal.ads.zzael;
import com.google.android.gms.internal.ads.zzaen;
import com.google.android.gms.internal.ads.zzaeo;
import com.google.android.gms.internal.ads.zzaep;
import com.google.android.gms.internal.ads.zzaeq;
import com.google.android.gms.internal.ads.zzaer;
import com.google.android.gms.internal.ads.zzakz;
import com.google.android.gms.internal.ads.zzayu;
import com.google.android.gms.internal.ads.zzuc;
import com.google.android.gms.internal.ads.zzuh;
import com.google.android.gms.internal.ads.zzuj;
import com.google.android.gms.internal.ads.zzve;
import com.google.android.gms.internal.ads.zzvm;
import com.google.android.gms.internal.ads.zzvn;
import com.google.android.gms.internal.ads.zzxj;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public class AdLoader {
    private final zzuh zzaba;
    private final zzvm zzabb;
    private final Context zzup;

    AdLoader(Context context, zzvm zzvm) {
        this(context, zzvm, zzuh.zzccn);
    }

    private final void zza(zzxj zzxj) {
        try {
            this.zzabb.zzb(zzuh.zza(this.zzup, zzxj));
        } catch (RemoteException e2) {
            zzayu.zzc("Failed to load ad.", e2);
        }
    }

    @Deprecated
    public String getMediationAdapterClassName() {
        try {
            return this.zzabb.zzka();
        } catch (RemoteException e2) {
            zzayu.zzd("Failed to get the mediation adapter class name.", e2);
            return null;
        }
    }

    public boolean isLoading() {
        try {
            return this.zzabb.isLoading();
        } catch (RemoteException e2) {
            zzayu.zzd("Failed to check if ad is loading.", e2);
            return false;
        }
    }

    public void loadAd(AdRequest adRequest) {
        zza(adRequest.zzdg());
    }

    public void loadAds(AdRequest adRequest, int i2) {
        try {
            this.zzabb.zza(zzuh.zza(this.zzup, adRequest.zzdg()), i2);
        } catch (RemoteException e2) {
            zzayu.zzc("Failed to load ads.", e2);
        }
    }

    public void loadAd(PublisherAdRequest publisherAdRequest) {
        zza(publisherAdRequest.zzdg());
    }

    private AdLoader(Context context, zzvm zzvm, zzuh zzuh) {
        this.zzup = context;
        this.zzabb = zzvm;
        this.zzaba = zzuh;
    }

    /* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
    public static class Builder {
        private final zzvn zzabd;
        private final Context zzup;

        public Builder(Context context, String str) {
            this((Context) Preconditions.checkNotNull(context, "context cannot be null"), zzve.zzov().zzb(context, str, new zzakz()));
        }

        public AdLoader build() {
            try {
                return new AdLoader(this.zzup, this.zzabd.zzpd());
            } catch (RemoteException e2) {
                zzayu.zzc("Failed to build AdLoader.", e2);
                return null;
            }
        }

        @Deprecated
        public Builder forAppInstallAd(NativeAppInstallAd.OnAppInstallAdLoadedListener onAppInstallAdLoadedListener) {
            try {
                this.zzabd.zza(new zzael(onAppInstallAdLoadedListener));
            } catch (RemoteException e2) {
                zzayu.zzd("Failed to add app install ad listener", e2);
            }
            return this;
        }

        @Deprecated
        public Builder forContentAd(NativeContentAd.OnContentAdLoadedListener onContentAdLoadedListener) {
            try {
                this.zzabd.zza(new zzaeo(onContentAdLoadedListener));
            } catch (RemoteException e2) {
                zzayu.zzd("Failed to add content ad listener", e2);
            }
            return this;
        }

        public Builder forCustomTemplateAd(String str, NativeCustomTemplateAd.OnCustomTemplateAdLoadedListener onCustomTemplateAdLoadedListener, NativeCustomTemplateAd.OnCustomClickListener onCustomClickListener) {
            zzaen zzaen;
            try {
                zzvn zzvn = this.zzabd;
                zzaeq zzaeq = new zzaeq(onCustomTemplateAdLoadedListener);
                if (onCustomClickListener == null) {
                    zzaen = null;
                } else {
                    zzaen = new zzaen(onCustomClickListener);
                }
                zzvn.zza(str, zzaeq, zzaen);
            } catch (RemoteException e2) {
                zzayu.zzd("Failed to add custom template ad listener", e2);
            }
            return this;
        }

        public Builder forPublisherAdView(OnPublisherAdViewLoadedListener onPublisherAdViewLoadedListener, AdSize... adSizeArr) {
            if (adSizeArr == null || adSizeArr.length <= 0) {
                throw new IllegalArgumentException("The supported ad sizes must contain at least one valid ad size.");
            }
            try {
                this.zzabd.zza(new zzaep(onPublisherAdViewLoadedListener), new zzuj(this.zzup, adSizeArr));
            } catch (RemoteException e2) {
                zzayu.zzd("Failed to add publisher banner ad listener", e2);
            }
            return this;
        }

        public Builder forUnifiedNativeAd(UnifiedNativeAd.OnUnifiedNativeAdLoadedListener onUnifiedNativeAdLoadedListener) {
            try {
                this.zzabd.zza(new zzaer(onUnifiedNativeAdLoadedListener));
            } catch (RemoteException e2) {
                zzayu.zzd("Failed to add google native ad listener", e2);
            }
            return this;
        }

        public Builder withAdListener(AdListener adListener) {
            try {
                this.zzabd.zzb(new zzuc(adListener));
            } catch (RemoteException e2) {
                zzayu.zzd("Failed to set AdListener.", e2);
            }
            return this;
        }

        @KeepForSdk
        @Deprecated
        public Builder withCorrelator(Correlator correlator) {
            return this;
        }

        public Builder withNativeAdOptions(NativeAdOptions nativeAdOptions) {
            try {
                this.zzabd.zza(new zzaby(nativeAdOptions));
            } catch (RemoteException e2) {
                zzayu.zzd("Failed to specify native ad options", e2);
            }
            return this;
        }

        public Builder withPublisherAdViewOptions(PublisherAdViewOptions publisherAdViewOptions) {
            try {
                this.zzabd.zza(publisherAdViewOptions);
            } catch (RemoteException e2) {
                zzayu.zzd("Failed to specify DFP banner ad options", e2);
            }
            return this;
        }

        private Builder(Context context, zzvn zzvn) {
            this.zzup = context;
            this.zzabd = zzvn;
        }
    }
}
