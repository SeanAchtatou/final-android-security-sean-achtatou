package com.google.android.gms.internal.ads;

import android.net.Uri;
import com.google.android.gms.ads.internal.overlay.zzo;
import com.google.android.gms.ads.internal.overlay.zzt;
import com.google.android.gms.ads.internal.zzc;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzbev {
    void zza(int i2, int i3, boolean z);

    void zza(zzbeu zzbeu);

    void zza(zzbex zzbex);

    void zza(zzty zzty, zzaew zzaew, zzo zzo, zzaey zzaey, zzt zzt, boolean z, zzafq zzafq, zzc zzc, zzaon zzaon, zzato zzato);

    zzc zzaas();

    boolean zzaat();

    void zzaaz();

    void zzaba();

    void zzabb();

    void zzabc();

    zzato zzabf();

    void zzbb(boolean z);

    void zzbc(boolean z);

    void zzh(Uri uri);

    void zzi(int i2, int i3);

    void zztn();
}
