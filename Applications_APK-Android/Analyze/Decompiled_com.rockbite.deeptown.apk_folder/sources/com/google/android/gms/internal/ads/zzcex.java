package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzcex implements zzdgt<String> {
    final /* synthetic */ zzceq zzftz;

    zzcex(zzceq zzceq) {
        this.zzftz = zzceq;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzceq.zza(com.google.android.gms.internal.ads.zzceq, boolean):boolean
     arg types: [com.google.android.gms.internal.ads.zzceq, int]
     candidates:
      com.google.android.gms.internal.ads.zzceq.zza(com.google.android.gms.internal.ads.zzceq, java.lang.String):void
      com.google.android.gms.internal.ads.zzceq.zza(com.google.android.gms.internal.ads.zzceq, boolean):boolean */
    public final /* synthetic */ void onSuccess(Object obj) {
        String str = (String) obj;
        synchronized (this) {
            boolean unused = this.zzftz.zzftm = true;
            this.zzftz.zza("com.google.android.gms.ads.MobileAds", true, "", (int) (zzq.zzkx().elapsedRealtime() - this.zzftz.zzftn));
            this.zzftz.executor.execute(new zzcfa(this, str));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzceq.zza(com.google.android.gms.internal.ads.zzceq, boolean):boolean
     arg types: [com.google.android.gms.internal.ads.zzceq, int]
     candidates:
      com.google.android.gms.internal.ads.zzceq.zza(com.google.android.gms.internal.ads.zzceq, java.lang.String):void
      com.google.android.gms.internal.ads.zzceq.zza(com.google.android.gms.internal.ads.zzceq, boolean):boolean */
    public final void zzb(Throwable th) {
        synchronized (this) {
            boolean unused = this.zzftz.zzftm = true;
            this.zzftz.zza("com.google.android.gms.ads.MobileAds", false, "Internal Error.", (int) (zzq.zzkx().elapsedRealtime() - this.zzftz.zzftn));
            this.zzftz.zzfto.setException(new Exception());
        }
    }
}
