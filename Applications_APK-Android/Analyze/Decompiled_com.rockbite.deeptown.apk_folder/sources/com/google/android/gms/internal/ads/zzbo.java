package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzdrt;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
public final class zzbo {

    /* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
    public static final class zza extends zzdrt<zza, C0139zza> implements zzdtg {
        private static volatile zzdtn<zza> zzdz;
        /* access modifiers changed from: private */
        public static final zza zzef;
        private int zzdl;
        private zzb zzed;
        private zzc zzee;

        /* renamed from: com.google.android.gms.internal.ads.zzbo$zza$zza  reason: collision with other inner class name */
        /* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
        public static final class C0139zza extends zzdrt.zzb<zza, C0139zza> implements zzdtg {
            private C0139zza() {
                super(zza.zzef);
            }

            /* synthetic */ C0139zza(zzbn zzbn) {
                this();
            }
        }

        static {
            zza zza = new zza();
            zzef = zza;
            zzdrt.zza(zza.class, zza);
        }

        private zza() {
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i2, Object obj, Object obj2) {
            switch (zzbn.zzdk[i2 - 1]) {
                case 1:
                    return new zza();
                case 2:
                    return new C0139zza(null);
                case 3:
                    return zzdrt.zza(zzef, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\t\u0000\u0002\t\u0001", new Object[]{"zzdl", "zzed", "zzee"});
                case 4:
                    return zzef;
                case 5:
                    zzdtn<zza> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zza.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzef);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        public final boolean zzw() {
            return (this.zzdl & 1) != 0;
        }

        public final zzb zzx() {
            zzb zzb = this.zzed;
            return zzb == null ? zzb.zzac() : zzb;
        }

        public final boolean zzy() {
            return (this.zzdl & 2) != 0;
        }

        public final zzc zzz() {
            zzc zzc = this.zzee;
            return zzc == null ? zzc.zzah() : zzc;
        }
    }

    /* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
    public static final class zzb extends zzdrt<zzb, zza> implements zzdtg {
        private static volatile zzdtn<zzb> zzdz;
        /* access modifiers changed from: private */
        public static final zzb zzeh;
        private int zzdl;
        private int zzeg = 2;

        /* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
        public static final class zza extends zzdrt.zzb<zzb, zza> implements zzdtg {
            private zza() {
                super(zzb.zzeh);
            }

            /* synthetic */ zza(zzbn zzbn) {
                this();
            }
        }

        static {
            zzb zzb = new zzb();
            zzeh = zzb;
            zzdrt.zza(zzb.class, zzb);
        }

        private zzb() {
        }

        public static zzb zzac() {
            return zzeh;
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i2, Object obj, Object obj2) {
            switch (zzbn.zzdk[i2 - 1]) {
                case 1:
                    return new zzb();
                case 2:
                    return new zza(null);
                case 3:
                    return zzdrt.zza(zzeh, "\u0001\u0001\u0000\u0001\u001b\u001b\u0001\u0000\u0000\u0000\u001b\f\u0000", new Object[]{"zzdl", "zzeg", zzbq.zzaf()});
                case 4:
                    return zzeh;
                case 5:
                    zzdtn<zzb> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zzb.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzeh);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        public final zzbq zzab() {
            zzbq zze = zzbq.zze(this.zzeg);
            return zze == null ? zzbq.ENUM_SIGNAL_SOURCE_ADSHIELD : zze;
        }
    }

    /* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
    public static final class zzc extends zzdrt<zzc, zza> implements zzdtg {
        private static volatile zzdtn<zzc> zzdz;
        /* access modifiers changed from: private */
        public static final zzc zzev;
        private int zzdl;
        private String zzep = "";
        private String zzeq = "";
        private String zzer = "";
        private String zzes = "";
        private String zzet = "";
        private String zzeu = "";

        /* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
        public static final class zza extends zzdrt.zzb<zzc, zza> implements zzdtg {
            private zza() {
                super(zzc.zzev);
            }

            /* synthetic */ zza(zzbn zzbn) {
                this();
            }
        }

        static {
            zzc zzc = new zzc();
            zzev = zzc;
            zzdrt.zza(zzc.class, zzc);
        }

        private zzc() {
        }

        public static zzc zzah() {
            return zzev;
        }

        /* access modifiers changed from: protected */
        public final Object zza(int i2, Object obj, Object obj2) {
            switch (zzbn.zzdk[i2 - 1]) {
                case 1:
                    return new zzc();
                case 2:
                    return new zza(null);
                case 3:
                    return zzdrt.zza(zzev, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001\b\u0000\u0002\b\u0001\u0003\b\u0002\u0004\b\u0003\u0005\b\u0004\u0006\b\u0005", new Object[]{"zzdl", "zzep", "zzeq", "zzer", "zzes", "zzet", "zzeu"});
                case 4:
                    return zzev;
                case 5:
                    zzdtn<zzc> zzdtn = zzdz;
                    if (zzdtn == null) {
                        synchronized (zzc.class) {
                            zzdtn = zzdz;
                            if (zzdtn == null) {
                                zzdtn = new zzdrt.zza<>(zzev);
                                zzdz = zzdtn;
                            }
                        }
                    }
                    return zzdtn;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        public final String zzag() {
            return this.zzep;
        }
    }
}
