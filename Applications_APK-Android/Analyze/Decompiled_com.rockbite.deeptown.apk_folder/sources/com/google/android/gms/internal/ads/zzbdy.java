package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbdy implements zzsp {
    private final int zzdtf;
    private final boolean zzeft;

    zzbdy(boolean z, int i2) {
        this.zzeft = z;
        this.zzdtf = i2;
    }

    public final void zza(zztu zztu) {
        zzbdz.zza(this.zzeft, this.zzdtf, zztu);
    }
}
