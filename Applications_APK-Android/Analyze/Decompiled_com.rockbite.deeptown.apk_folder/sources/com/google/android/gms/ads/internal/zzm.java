package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.ads.zzdq;
import com.google.android.gms.internal.ads.zzdr;
import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzm implements Callable<zzdq> {
    private final /* synthetic */ zzl zzblk;

    zzm(zzl zzl) {
        this.zzblk = zzl;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzdr.zza(java.lang.String, android.content.Context, boolean):com.google.android.gms.internal.ads.zzdr
     arg types: [java.lang.String, android.content.Context, int]
     candidates:
      com.google.android.gms.internal.ads.zzdi.zza(com.google.android.gms.internal.ads.zzei, android.view.MotionEvent, android.util.DisplayMetrics):com.google.android.gms.internal.ads.zzeq
      com.google.android.gms.internal.ads.zzdi.zza(android.content.Context, android.view.View, android.app.Activity):java.lang.String
      com.google.android.gms.internal.ads.zzdi.zza(int, int, int):void
      com.google.android.gms.internal.ads.zzdj.zza(android.content.Context, android.view.View, android.app.Activity):java.lang.String
      com.google.android.gms.internal.ads.zzdj.zza(android.content.Context, java.lang.String, android.view.View):java.lang.String
      com.google.android.gms.internal.ads.zzdj.zza(int, int, int):void
      com.google.android.gms.internal.ads.zzdg.zza(android.content.Context, android.view.View, android.app.Activity):java.lang.String
      com.google.android.gms.internal.ads.zzdg.zza(android.content.Context, java.lang.String, android.view.View):java.lang.String
      com.google.android.gms.internal.ads.zzdg.zza(int, int, int):void
      com.google.android.gms.internal.ads.zzdr.zza(java.lang.String, android.content.Context, boolean):com.google.android.gms.internal.ads.zzdr */
    public final /* synthetic */ Object call() throws Exception {
        return new zzdq(zzdr.zza(this.zzblk.zzbll.zzbma, this.zzblk.zzup, false));
    }
}
