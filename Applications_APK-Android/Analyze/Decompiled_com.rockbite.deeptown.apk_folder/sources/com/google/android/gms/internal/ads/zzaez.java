package com.google.android.gms.internal.ads;

import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzaez implements zzafn {
    static final zzafn zzcwt = new zzaez();

    private zzaez() {
    }

    public final void zza(Object obj, Map map) {
        zzafa.zzc((zzbei) obj, map);
    }
}
