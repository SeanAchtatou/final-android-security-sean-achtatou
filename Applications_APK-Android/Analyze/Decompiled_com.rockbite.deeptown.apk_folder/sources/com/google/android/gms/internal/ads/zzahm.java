package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.esotericsoftware.spine.Animation;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.instream.InstreamAd;
import com.google.android.gms.ads.instream.InstreamAdView;
import com.google.android.gms.dynamic.ObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzahm extends InstreamAd {
    private final VideoController zzcel = zzrx();
    private final zzahb zzcyk;

    public zzahm(zzahb zzahb) {
        this.zzcyk = zzahb;
    }

    private final VideoController zzrx() {
        VideoController videoController = new VideoController();
        try {
            videoController.zza(this.zzcyk.getVideoController());
        } catch (RemoteException e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
        }
        return videoController;
    }

    public final void destroy() {
        try {
            this.zzcyk.destroy();
        } catch (RemoteException e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
        }
    }

    public final float getAspectRatio() {
        VideoController videoController = this.zzcel;
        if (videoController == null) {
            return Animation.CurveTimeline.LINEAR;
        }
        return videoController.getAspectRatio();
    }

    public final VideoController getVideoController() {
        return this.zzcel;
    }

    public final float getVideoCurrentTime() {
        VideoController videoController = this.zzcel;
        if (videoController == null) {
            return Animation.CurveTimeline.LINEAR;
        }
        return videoController.getVideoCurrentTime();
    }

    public final float getVideoDuration() {
        VideoController videoController = this.zzcel;
        if (videoController == null) {
            return Animation.CurveTimeline.LINEAR;
        }
        return videoController.getVideoDuration();
    }

    public final void zza(InstreamAdView instreamAdView) {
        if (instreamAdView == null) {
            zzayu.zzex("showInView: parameter view must not be null.");
            return;
        }
        try {
            this.zzcyk.zzr(ObjectWrapper.wrap(instreamAdView));
        } catch (RemoteException e2) {
            zzayu.zze("#007 Could not call remote method.", e2);
        }
    }
}
