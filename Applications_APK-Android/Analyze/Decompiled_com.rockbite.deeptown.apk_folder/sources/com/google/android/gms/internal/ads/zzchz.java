package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzchz {
    private int responseCode = 0;
    private long zzfxc = 0;
    private long zzfxd = 0;
    private long zzfxe = 0;
    private final Object zzfxf = new Object();
    private final Object zzfxg = new Object();
    private final Object zzfxh = new Object();
    private final Object zzfxi = new Object();

    public final int getResponseCode() {
        int i2;
        synchronized (this.zzfxf) {
            i2 = this.responseCode;
        }
        return i2;
    }

    public final long zzaly() {
        long j2;
        synchronized (this.zzfxg) {
            j2 = this.zzfxc;
        }
        return j2;
    }

    public final synchronized long zzalz() {
        long j2;
        synchronized (this.zzfxh) {
            j2 = this.zzfxd;
        }
        return j2;
    }

    public final synchronized long zzama() {
        long j2;
        synchronized (this.zzfxi) {
            j2 = this.zzfxe;
        }
        return j2;
    }

    public final void zzdk(int i2) {
        synchronized (this.zzfxf) {
            this.responseCode = i2;
        }
    }

    public final void zzeq(long j2) {
        synchronized (this.zzfxg) {
            this.zzfxc = j2;
        }
    }

    public final synchronized void zzer(long j2) {
        synchronized (this.zzfxi) {
            this.zzfxe = j2;
        }
    }

    public final synchronized void zzfe(long j2) {
        synchronized (this.zzfxh) {
            this.zzfxd = j2;
        }
    }
}
