package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbwo implements Runnable {
    private final boolean zzdym;
    private final zzbwk zzflf;

    zzbwo(zzbwk zzbwk, boolean z) {
        this.zzflf = zzbwk;
        this.zzdym = z;
    }

    public final void run() {
        this.zzflf.zzbh(this.zzdym);
    }
}
