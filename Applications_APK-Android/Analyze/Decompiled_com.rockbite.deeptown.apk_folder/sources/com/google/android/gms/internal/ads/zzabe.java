package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzabe {
    public static zzaan<Boolean> zzcgj = zzaan.zzf("gads:sdk_use_dynamic_module", true);
    public static zzaan<Boolean> zzctz = zzaan.zzf("gads:adapter_initialization:red_button", false);
    private static zzaan<Boolean> zzcua = zzaan.zzf("gads:ad_serving:enabled", true);
    public static zzaan<Boolean> zzcub = zzaan.zzf("gads:adaptive_banner:fail_invalid_ad_size", true);
}
