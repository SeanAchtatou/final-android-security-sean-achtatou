package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzajo implements zzazp<zzaif> {
    final /* synthetic */ zzajj zzdaj;

    zzajo(zzajj zzajj) {
        this.zzdaj = zzajj;
    }

    public final /* synthetic */ void zzh(Object obj) {
        zzazd.zzdwi.execute(new zzajn(this, (zzaif) obj));
    }
}
