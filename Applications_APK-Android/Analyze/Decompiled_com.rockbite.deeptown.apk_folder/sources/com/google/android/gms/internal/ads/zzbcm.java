package com.google.android.gms.internal.ads;

import com.facebook.appevents.AppEventsConstants;
import com.tapjoy.TapjoyConstants;
import java.util.HashMap;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzbcm implements Runnable {
    private final /* synthetic */ String zzdug;
    private final /* synthetic */ String zzedb;
    private final /* synthetic */ int zzedc;
    private final /* synthetic */ int zzedd;
    private final /* synthetic */ boolean zzede = false;
    private final /* synthetic */ zzbcn zzedf;

    zzbcm(zzbcn zzbcn, String str, String str2, int i2, int i3, boolean z) {
        this.zzedf = zzbcn;
        this.zzdug = str;
        this.zzedb = str2;
        this.zzedc = i2;
        this.zzedd = i3;
    }

    public final void run() {
        HashMap hashMap = new HashMap();
        hashMap.put(TapjoyConstants.TJC_SDK_TYPE_DEFAULT, "precacheProgress");
        hashMap.put("src", this.zzdug);
        hashMap.put("cachedSrc", this.zzedb);
        hashMap.put("bytesLoaded", Integer.toString(this.zzedc));
        hashMap.put("totalBytes", Integer.toString(this.zzedd));
        hashMap.put("cacheReady", this.zzede ? "1" : AppEventsConstants.EVENT_PARAM_VALUE_NO);
        this.zzedf.zza("onPrecacheEvent", hashMap);
    }
}
