package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbbn implements Runnable {
    private final int zzdtf;
    private final zzbbc zzebm;

    zzbbn(zzbbc zzbbc, int i2) {
        this.zzebm = zzbbc;
        this.zzdtf = i2;
    }

    public final void run() {
        this.zzebm.zzdb(this.zzdtf);
    }
}
