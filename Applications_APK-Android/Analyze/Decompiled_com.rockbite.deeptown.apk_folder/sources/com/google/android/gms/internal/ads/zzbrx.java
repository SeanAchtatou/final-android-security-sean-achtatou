package com.google.android.gms.internal.ads;

import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbrx implements zzdxg<Set<zzbsu<zzbov>>> {
    private final zzbrm zzfim;

    private zzbrx(zzbrm zzbrm) {
        this.zzfim = zzbrm;
    }

    public static zzbrx zzn(zzbrm zzbrm) {
        return new zzbrx(zzbrm);
    }

    public final /* synthetic */ Object get() {
        return (Set) zzdxm.zza(this.zzfim.zzahm(), "Cannot return null from a non-@Nullable @Provides method");
    }
}
