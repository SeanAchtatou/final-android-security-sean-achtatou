package com.google.android.gms.internal.ads;

import android.app.Activity;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.ads.reward.AdMetadataListener;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcyz extends zzasj {
    private final String zzbqz;
    private final zzczs zzfhh;
    private final zzcyt zzgkw;
    private final zzcxz zzgkx;
    /* access modifiers changed from: private */
    public zzcbb zzgky;

    public zzcyz(String str, zzcyt zzcyt, zzcxz zzcxz, zzczs zzczs) {
        this.zzbqz = str;
        this.zzgkw = zzcyt;
        this.zzgkx = zzcxz;
        this.zzfhh = zzczs;
    }

    public final Bundle getAdMetadata() {
        Preconditions.checkMainThread("#008 Must be called on the main UI thread.");
        zzcbb zzcbb = this.zzgky;
        return zzcbb != null ? zzcbb.getAdMetadata() : new Bundle();
    }

    public final synchronized String getMediationAdapterClassName() throws RemoteException {
        if (this.zzgky == null || this.zzgky.zzags() == null) {
            return null;
        }
        return this.zzgky.zzags().getMediationAdapterClassName();
    }

    public final boolean isLoaded() {
        Preconditions.checkMainThread("#008 Must be called on the main UI thread.");
        zzcbb zzcbb = this.zzgky;
        return zzcbb != null && !zzcbb.zzaks();
    }

    public final synchronized void zza(zzug zzug, zzaso zzaso) throws RemoteException {
        Preconditions.checkMainThread("#008 Must be called on the main UI thread.");
        this.zzgkx.zza(zzaso);
        if (this.zzgky == null) {
            zzcyq zzcyq = new zzcyq(null);
            this.zzgkw.zzaoj();
            this.zzgkw.zza(zzug, this.zzbqz, zzcyq, new zzcyy(this));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzcyz.zza(com.google.android.gms.dynamic.IObjectWrapper, boolean):void
     arg types: [com.google.android.gms.dynamic.IObjectWrapper, int]
     candidates:
      com.google.android.gms.internal.ads.zzcyz.zza(com.google.android.gms.internal.ads.zzcyz, com.google.android.gms.internal.ads.zzcbb):com.google.android.gms.internal.ads.zzcbb
      com.google.android.gms.internal.ads.zzcyz.zza(com.google.android.gms.internal.ads.zzug, com.google.android.gms.internal.ads.zzaso):void
      com.google.android.gms.internal.ads.zzasg.zza(com.google.android.gms.internal.ads.zzug, com.google.android.gms.internal.ads.zzaso):void
      com.google.android.gms.internal.ads.zzcyz.zza(com.google.android.gms.dynamic.IObjectWrapper, boolean):void */
    public final synchronized void zzh(IObjectWrapper iObjectWrapper) throws RemoteException {
        zza(iObjectWrapper, false);
    }

    public final zzxa zzkb() {
        zzcbb zzcbb;
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcrf)).booleanValue() && (zzcbb = this.zzgky) != null) {
            return zzcbb.zzags();
        }
        return null;
    }

    public final zzasf zzpz() {
        Preconditions.checkMainThread("#008 Must be called on the main UI thread.");
        zzcbb zzcbb = this.zzgky;
        if (zzcbb != null) {
            return zzcbb.zzpz();
        }
        return null;
    }

    public final synchronized void zza(IObjectWrapper iObjectWrapper, boolean z) throws RemoteException {
        Preconditions.checkMainThread("#008 Must be called on the main UI thread.");
        if (this.zzgky == null) {
            zzayu.zzez("Rewarded can not be shown before loaded");
            this.zzgkx.zzco(2);
            return;
        }
        this.zzgky.zzb(z, (Activity) ObjectWrapper.unwrap(iObjectWrapper));
    }

    public final void zza(zzasl zzasl) {
        Preconditions.checkMainThread("#008 Must be called on the main UI thread.");
        this.zzgkx.zzb(zzasl);
    }

    public final void zza(zzast zzast) {
        Preconditions.checkMainThread("#008 Must be called on the main UI thread.");
        this.zzgkx.zzb(zzast);
    }

    public final void zza(zzwv zzwv) {
        if (zzwv == null) {
            this.zzgkx.zza((AdMetadataListener) null);
        } else {
            this.zzgkx.zza(new zzczb(this, zzwv));
        }
    }

    public final synchronized void zza(zzatb zzatb) {
        Preconditions.checkMainThread("#008 Must be called on the main UI thread.");
        zzczs zzczs = this.zzfhh;
        zzczs.zzdnv = zzatb.zzdnv;
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcja)).booleanValue()) {
            zzczs.zzdnw = zzatb.zzdnw;
        }
    }
}
