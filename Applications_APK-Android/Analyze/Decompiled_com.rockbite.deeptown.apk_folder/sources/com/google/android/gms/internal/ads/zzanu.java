package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.ads.mediation.rtb.SignalCallbacks;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzanu implements SignalCallbacks {
    private final /* synthetic */ zzanj zzdeu;

    zzanu(zzann zzann, zzanj zzanj) {
        this.zzdeu = zzanj;
    }

    public final void onFailure(String str) {
        try {
            this.zzdeu.onFailure(str);
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
        }
    }

    public final void onSuccess(String str) {
        try {
            this.zzdeu.zzdn(str);
        } catch (RemoteException e2) {
            zzayu.zzc("", e2);
        }
    }
}
