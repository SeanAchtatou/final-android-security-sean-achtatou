package com.google.android.gms.internal.ads;

import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcgd implements zzdgf {
    private final zzcge zzfvk;
    private final zzua zzfvl;

    zzcgd(zzcge zzcge, zzua zzua) {
        this.zzfvk = zzcge;
        this.zzfvl = zzua;
    }

    public final zzdhe zzf(Object obj) {
        zzua zzua = this.zzfvl;
        String str = (String) obj;
        String str2 = zzua.zzcbu;
        String str3 = zzua.zzcbv;
        JSONObject jSONObject = new JSONObject();
        JSONObject jSONObject2 = new JSONObject();
        JSONObject jSONObject3 = new JSONObject();
        jSONObject3.put("headers", new JSONObject());
        jSONObject3.put("body", str2);
        jSONObject2.put("base_url", "");
        jSONObject2.put("signals", new JSONObject(str3));
        jSONObject.put("request", jSONObject2);
        jSONObject.put("response", jSONObject3);
        jSONObject.put("flags", new JSONObject());
        return zzdgs.zzaj(jSONObject);
    }
}
