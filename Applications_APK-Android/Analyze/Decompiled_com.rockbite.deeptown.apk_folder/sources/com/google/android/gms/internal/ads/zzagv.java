package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.initialization.AdapterStatus;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzagv implements AdapterStatus {
    private final String description;
    private final int zzcye;
    private final AdapterStatus.State zzcyf;

    public zzagv(AdapterStatus.State state, String str, int i2) {
        this.zzcyf = state;
        this.description = str;
        this.zzcye = i2;
    }

    public final String getDescription() {
        return this.description;
    }

    public final AdapterStatus.State getInitializationState() {
        return this.zzcyf;
    }

    public final int getLatency() {
        return this.zzcye;
    }
}
