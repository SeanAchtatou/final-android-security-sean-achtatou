package com.google.android.gms.ads.query;

import android.content.Context;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public class QueryDataConfiguration {
    private final String zzbqz;
    private final Context zzup;

    @KeepForSdk
    QueryDataConfiguration(Context context, String str) {
        this.zzup = context;
        this.zzbqz = str;
    }

    @KeepForSdk
    public String getAdUnitId() {
        return this.zzbqz;
    }

    @KeepForSdk
    public Context getContext() {
        return this.zzup;
    }
}
