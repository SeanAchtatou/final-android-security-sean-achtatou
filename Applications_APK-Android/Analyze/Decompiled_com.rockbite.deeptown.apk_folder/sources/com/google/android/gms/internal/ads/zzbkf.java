package com.google.android.gms.internal.ads;

import android.view.ViewGroup;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public class zzbkf {
    private final ViewGroup zzfdu;

    public zzbkf(ViewGroup viewGroup) {
        this.zzfdu = viewGroup;
    }

    public final ViewGroup zzagd() {
        return this.zzfdu;
    }
}
