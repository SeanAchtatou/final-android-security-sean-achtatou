package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbwm implements Runnable {
    private final zzbxa zzflr;

    private zzbwm(zzbxa zzbxa) {
        this.zzflr = zzbxa;
    }

    static Runnable zza(zzbxa zzbxa) {
        return new zzbwm(zzbxa);
    }

    public final void run() {
        this.zzflr.zzaiq();
    }
}
