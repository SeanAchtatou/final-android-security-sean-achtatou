package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzbef implements Runnable {
    private final int zzdtf;
    private final int zzdtg;
    private final boolean zzefe;
    private final boolean zzeff;
    private final zzbed zzehe;

    zzbef(zzbed zzbed, int i2, int i3, boolean z, boolean z2) {
        this.zzehe = zzbed;
        this.zzdtf = i2;
        this.zzdtg = i3;
        this.zzefe = z;
        this.zzeff = z2;
    }

    public final void run() {
        this.zzehe.zzb(this.zzdtf, this.zzdtg, this.zzefe, this.zzeff);
    }
}
