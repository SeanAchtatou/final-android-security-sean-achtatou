package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzcwm implements zzdgt<zzbke> {
    private final /* synthetic */ zzcoz zzgdv;
    private final /* synthetic */ zzcwl zzgip;

    zzcwm(zzcwl zzcwl, zzcoz zzcoz) {
        this.zzgip = zzcwl;
        this.zzgdv = zzcoz;
    }

    public final /* synthetic */ void onSuccess(Object obj) {
        zzbke zzbke = (zzbke) obj;
        synchronized (this.zzgip) {
            zzdhe unused = this.zzgip.zzgdb = null;
            this.zzgdv.onSuccess(zzbke);
        }
    }

    public final void zzb(Throwable th) {
        synchronized (this.zzgip) {
            zzdhe unused = this.zzgip.zzgdb = null;
            ((zzbka) this.zzgip.zzgio.zzaog()).zzadd().onAdFailedToLoad(zzcfb.zzd(th));
            zzdad.zzc(th, "AppOpenAdLoader.onFailure");
            this.zzgdv.zzamx();
        }
    }
}
