package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.dynamic.ObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzclb implements zzcir<zzbwk, zzani, zzcjy> {
    private final zzbvm zzfzt;
    /* access modifiers changed from: private */
    public zzalr zzfzx;
    private final Context zzup;

    public zzclb(Context context, zzbvm zzbvm) {
        this.zzup = context;
        this.zzfzt = zzbvm;
    }

    public final void zza(zzczt zzczt, zzczl zzczl, zzcip<zzani, zzcjy> zzcip) throws zzdab {
        try {
            ((zzani) zzcip.zzddn).zzdm(zzczl.zzdem);
            ((zzani) zzcip.zzddn).zza(zzczl.zzeif, zzczl.zzglr.toString(), zzczt.zzgmh.zzfgl.zzgml, ObjectWrapper.wrap(this.zzup), new zzcld(this, zzcip), (zzali) zzcip.zzfyf);
        } catch (RemoteException e2) {
            throw new zzdab(e2);
        }
    }

    public final /* synthetic */ Object zzb(zzczt zzczt, zzczl zzczl, zzcip zzcip) throws zzdab, zzclr {
        if (zzczt.zzgmh.zzfgl.zzgmn.contains(Integer.toString(6))) {
            zzbws zzb = zzbws.zzb(this.zzfzx);
            if (zzczt.zzgmh.zzfgl.zzgmn.contains(Integer.toString(zzb.zzaja()))) {
                zzbwt zza = this.zzfzt.zza(new zzbmt(zzczt, zzczl, zzcip.zzfge), new zzbxe(zzb), new zzbyg(null, null, this.zzfzx));
                ((zzcjy) zzcip.zzfyf).zza(zza.zzadm());
                return zza.zzadn();
            }
            throw new zzclr("No corresponding native ad listener", 0);
        }
        throw new zzclr("Unified must be used for RTB.", 1);
    }
}
