package com.google.android.gms.internal.ads;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public abstract class zzalb extends zzgb implements zzalc {
    public zzalb() {
        super("com.google.android.gms.ads.internal.mediation.client.IAdapterCreator");
    }

    public static zzalc zzaa(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.internal.mediation.client.IAdapterCreator");
        if (queryLocalInterface instanceof zzalc) {
            return (zzalc) queryLocalInterface;
        }
        return new zzale(iBinder);
    }

    /* access modifiers changed from: protected */
    public final boolean zza(int i2, Parcel parcel, Parcel parcel2, int i3) throws RemoteException {
        if (i2 == 1) {
            zzald zzde = zzde(parcel.readString());
            parcel2.writeNoException();
            zzge.zza(parcel2, zzde);
        } else if (i2 == 2) {
            boolean zzdf = zzdf(parcel.readString());
            parcel2.writeNoException();
            zzge.writeBoolean(parcel2, zzdf);
        } else if (i2 != 3) {
            return false;
        } else {
            zzani zzdi = zzdi(parcel.readString());
            parcel2.writeNoException();
            zzge.zza(parcel2, zzdi);
        }
        return true;
    }
}
