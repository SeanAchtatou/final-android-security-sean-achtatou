package com.google.android.gms.internal.ads;

import com.tapjoy.TJAdUnitConstants;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzafm implements zzafn<zzbdi> {
    zzafm() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzbdi zzbdi = (zzbdi) obj;
        if (map.keySet().contains(TJAdUnitConstants.String.VIDEO_START)) {
            zzbdi.zzaaa().zzaba();
        } else if (map.keySet().contains("stop")) {
            zzbdi.zzaaa().zzabb();
        } else if (map.keySet().contains("cancel")) {
            zzbdi.zzaaa().zzabc();
        }
    }
}
