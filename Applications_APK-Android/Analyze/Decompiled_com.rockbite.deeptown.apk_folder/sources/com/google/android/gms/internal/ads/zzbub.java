package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbub implements zzdxg<zzbuv> {
    public static zzbuv zzb(zzbtv zzbtv) {
        return (zzbuv) zzdxm.zza(zzbtv.zzaie(), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        throw new NoSuchMethodError();
    }
}
