package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzcty;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcsx<S extends zzcty<?>> implements zzcub<S> {
    private final ScheduledExecutorService zzfdi;
    private final zzcub<S> zzgfn;
    private final long zzgge;

    public zzcsx(zzcub<S> zzcub, long j2, ScheduledExecutorService scheduledExecutorService) {
        this.zzgfn = zzcub;
        this.zzgge = j2;
        this.zzfdi = scheduledExecutorService;
    }

    public final zzdhe<S> zzanc() {
        zzdhe<S> zzanc = this.zzgfn.zzanc();
        long j2 = this.zzgge;
        if (j2 > 0) {
            zzanc = zzdgs.zza(zzanc, j2, TimeUnit.MILLISECONDS, this.zzfdi);
        }
        return zzdgs.zzb(zzanc, Throwable.class, zzcsw.zzbkw, zzazd.zzdwj);
    }
}
