package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcyr implements zzdxg<zzcxz> {
    private final zzdxp<zzdax> zzgjl;

    public zzcyr(zzdxp<zzdax> zzdxp) {
        this.zzgjl = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return new zzcxz(this.zzgjl.get());
    }
}
