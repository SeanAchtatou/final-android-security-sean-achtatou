package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.view.View;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.mediation.NativeContentAdMapper;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzame extends zzalp {
    private final NativeContentAdMapper zzdec;

    public zzame(NativeContentAdMapper nativeContentAdMapper) {
        this.zzdec = nativeContentAdMapper;
    }

    public final String getAdvertiser() {
        return this.zzdec.getAdvertiser();
    }

    public final String getBody() {
        return this.zzdec.getBody();
    }

    public final String getCallToAction() {
        return this.zzdec.getCallToAction();
    }

    public final Bundle getExtras() {
        return this.zzdec.getExtras();
    }

    public final String getHeadline() {
        return this.zzdec.getHeadline();
    }

    public final List getImages() {
        List<NativeAd.Image> images = this.zzdec.getImages();
        if (images == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (NativeAd.Image next : images) {
            arrayList.add(new zzabu(next.getDrawable(), next.getUri(), next.getScale(), next.getWidth(), next.getHeight()));
        }
        return arrayList;
    }

    public final boolean getOverrideClickHandling() {
        return this.zzdec.getOverrideClickHandling();
    }

    public final boolean getOverrideImpressionRecording() {
        return this.zzdec.getOverrideImpressionRecording();
    }

    public final zzxb getVideoController() {
        if (this.zzdec.getVideoController() != null) {
            return this.zzdec.getVideoController().zzdl();
        }
        return null;
    }

    public final void recordImpression() {
        this.zzdec.recordImpression();
    }

    public final void zzc(IObjectWrapper iObjectWrapper, IObjectWrapper iObjectWrapper2, IObjectWrapper iObjectWrapper3) {
        this.zzdec.trackViews((View) ObjectWrapper.unwrap(iObjectWrapper), (HashMap) ObjectWrapper.unwrap(iObjectWrapper2), (HashMap) ObjectWrapper.unwrap(iObjectWrapper3));
    }

    public final zzaca zzrh() {
        return null;
    }

    public final IObjectWrapper zzri() {
        return null;
    }

    public final zzaci zzrj() {
        NativeAd.Image logo = this.zzdec.getLogo();
        if (logo != null) {
            return new zzabu(logo.getDrawable(), logo.getUri(), logo.getScale(), logo.getWidth(), logo.getHeight());
        }
        return null;
    }

    public final IObjectWrapper zzsu() {
        View adChoicesContent = this.zzdec.getAdChoicesContent();
        if (adChoicesContent == null) {
            return null;
        }
        return ObjectWrapper.wrap(adChoicesContent);
    }

    public final IObjectWrapper zzsv() {
        View zzabz = this.zzdec.zzabz();
        if (zzabz == null) {
            return null;
        }
        return ObjectWrapper.wrap(zzabz);
    }

    public final void zzu(IObjectWrapper iObjectWrapper) {
        this.zzdec.handleClick((View) ObjectWrapper.unwrap(iObjectWrapper));
    }

    public final void zzv(IObjectWrapper iObjectWrapper) {
        this.zzdec.trackView((View) ObjectWrapper.unwrap(iObjectWrapper));
    }

    public final void zzw(IObjectWrapper iObjectWrapper) {
        this.zzdec.untrackView((View) ObjectWrapper.unwrap(iObjectWrapper));
    }
}
