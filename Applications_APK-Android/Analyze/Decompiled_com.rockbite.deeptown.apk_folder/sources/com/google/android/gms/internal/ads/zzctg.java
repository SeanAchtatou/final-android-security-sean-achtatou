package com.google.android.gms.internal.ads;

import android.os.Bundle;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzctg implements zzcty<Bundle> {
    private final String zzggi;

    public zzctg(String str) {
        this.zzggi = str;
    }

    public final /* synthetic */ void zzr(Object obj) {
        ((Bundle) obj).putString("rtb", this.zzggi);
    }
}
