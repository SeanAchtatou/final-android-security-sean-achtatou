package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcck implements zzsp {
    private final String zzcyr;
    private final int zzdvv;
    private final zztt zzfrt;
    private final String zzfru;

    zzcck(int i2, String str, zztt zztt, String str2) {
        this.zzdvv = i2;
        this.zzcyr = str;
        this.zzfrt = zztt;
        this.zzfru = str2;
    }

    public final void zza(zztu zztu) {
        int i2 = this.zzdvv;
        String str = this.zzcyr;
        zztt zztt = this.zzfrt;
        String str2 = this.zzfru;
        zztu.zzcay.zzbzt = Integer.valueOf(i2);
        zzts zzts = zztu.zzcav;
        zzts.zzcae = str;
        zzts.zzcah = zztt;
        zztu.zzcaq = str2;
    }
}
