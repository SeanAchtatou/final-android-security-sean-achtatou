package com.google.android.gms.internal.ads;

import android.content.Context;
import android.media.AudioManager;
import com.esotericsoftware.spine.Animation;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzawq {
    private float zzdje = 1.0f;
    private boolean zzdjk = false;

    public static float zzbe(Context context) {
        AudioManager audioManager = (AudioManager) context.getSystemService("audio");
        if (audioManager == null) {
            return Animation.CurveTimeline.LINEAR;
        }
        int streamMaxVolume = audioManager.getStreamMaxVolume(3);
        int streamVolume = audioManager.getStreamVolume(3);
        if (streamMaxVolume == 0) {
            return Animation.CurveTimeline.LINEAR;
        }
        return ((float) streamVolume) / ((float) streamMaxVolume);
    }

    private final synchronized boolean zzwt() {
        return this.zzdje >= Animation.CurveTimeline.LINEAR;
    }

    public final synchronized void setAppMuted(boolean z) {
        this.zzdjk = z;
    }

    public final synchronized void setAppVolume(float f2) {
        this.zzdje = f2;
    }

    public final synchronized float zzpe() {
        if (!zzwt()) {
            return 1.0f;
        }
        return this.zzdje;
    }

    public final synchronized boolean zzpf() {
        return this.zzdjk;
    }
}
