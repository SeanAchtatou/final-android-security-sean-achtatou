package com.google.android.gms.internal.ads;

import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcug implements Callable {
    private final zzcuh zzghh;

    zzcug(zzcuh zzcuh) {
        this.zzghh = zzcuh;
    }

    public final Object call() {
        return this.zzghh.zzanq();
    }
}
