package com.google.android.gms.internal.ads;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.view.Display;
import android.view.WindowManager;
import com.esotericsoftware.spine.Animation;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzbas implements SensorEventListener {
    private final SensorManager zzdyv;
    private final Object zzdyw = new Object();
    private final Display zzdyx;
    private final float[] zzdyy = new float[9];
    private final float[] zzdyz = new float[9];
    private float[] zzdza;
    private Handler zzdzb;
    private zzbau zzdzc;

    zzbas(Context context) {
        this.zzdyv = (SensorManager) context.getSystemService("sensor");
        this.zzdyx = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
    }

    private final void zzl(int i2, int i3) {
        float[] fArr = this.zzdyz;
        float f2 = fArr[i2];
        fArr[i2] = fArr[i3];
        fArr[i3] = f2;
    }

    public final void onAccuracyChanged(Sensor sensor, int i2) {
    }

    public final void onSensorChanged(SensorEvent sensorEvent) {
        float[] fArr = sensorEvent.values;
        if (fArr[0] != Animation.CurveTimeline.LINEAR || fArr[1] != Animation.CurveTimeline.LINEAR || fArr[2] != Animation.CurveTimeline.LINEAR) {
            synchronized (this.zzdyw) {
                if (this.zzdza == null) {
                    this.zzdza = new float[9];
                }
            }
            SensorManager.getRotationMatrixFromVector(this.zzdyy, fArr);
            int rotation = this.zzdyx.getRotation();
            if (rotation == 1) {
                SensorManager.remapCoordinateSystem(this.zzdyy, 2, 129, this.zzdyz);
            } else if (rotation == 2) {
                SensorManager.remapCoordinateSystem(this.zzdyy, 129, 130, this.zzdyz);
            } else if (rotation != 3) {
                System.arraycopy(this.zzdyy, 0, this.zzdyz, 0, 9);
            } else {
                SensorManager.remapCoordinateSystem(this.zzdyy, 130, 1, this.zzdyz);
            }
            zzl(1, 3);
            zzl(2, 6);
            zzl(5, 7);
            synchronized (this.zzdyw) {
                System.arraycopy(this.zzdyz, 0, this.zzdza, 0, 9);
            }
            zzbau zzbau = this.zzdzc;
            if (zzbau != null) {
                zzbau.zztq();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void start() {
        if (this.zzdzb == null) {
            Sensor defaultSensor = this.zzdyv.getDefaultSensor(11);
            if (defaultSensor == null) {
                zzayu.zzex("No Sensor of TYPE_ROTATION_VECTOR");
                return;
            }
            HandlerThread handlerThread = new HandlerThread("OrientationMonitor");
            handlerThread.start();
            this.zzdzb = new zzddu(handlerThread.getLooper());
            if (!this.zzdyv.registerListener(this, defaultSensor, 0, this.zzdzb)) {
                zzayu.zzex("SensorManager.registerListener failed.");
                stop();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void stop() {
        if (this.zzdzb != null) {
            this.zzdyv.unregisterListener(this);
            this.zzdzb.post(new zzbav(this));
            this.zzdzb = null;
        }
    }

    /* access modifiers changed from: package-private */
    public final void zza(zzbau zzbau) {
        this.zzdzc = zzbau;
    }

    /* access modifiers changed from: package-private */
    public final boolean zza(float[] fArr) {
        synchronized (this.zzdyw) {
            if (this.zzdza == null) {
                return false;
            }
            System.arraycopy(this.zzdza, 0, fArr, 0, this.zzdza.length);
            return true;
        }
    }
}
