package com.google.android.gms.internal.ads;

import java.lang.ref.WeakReference;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzbvt implements zzafn<Object> {
    private WeakReference<zzbvr> zzfkv;

    private zzbvt(zzbvr zzbvr) {
        this.zzfkv = new WeakReference<>(zzbvr);
    }

    public final void zza(Object obj, Map<String, String> map) {
        zzbvr zzbvr = this.zzfkv.get();
        if (zzbvr != null && "_ac".equals(map.get("eventName"))) {
            zzbvr.zzfke.onAdClicked();
        }
    }
}
