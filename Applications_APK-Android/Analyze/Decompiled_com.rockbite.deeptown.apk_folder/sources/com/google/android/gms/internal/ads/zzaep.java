package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.ads.doubleclick.AppEventListener;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.google.android.gms.ads.formats.OnPublisherAdViewLoadedListener;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class zzaep extends zzadt {
    /* access modifiers changed from: private */
    public final OnPublisherAdViewLoadedListener zzcwk;

    public zzaep(OnPublisherAdViewLoadedListener onPublisherAdViewLoadedListener) {
        this.zzcwk = onPublisherAdViewLoadedListener;
    }

    public final void zza(zzvu zzvu, IObjectWrapper iObjectWrapper) {
        if (zzvu != null && iObjectWrapper != null) {
            PublisherAdView publisherAdView = new PublisherAdView((Context) ObjectWrapper.unwrap(iObjectWrapper));
            AppEventListener appEventListener = null;
            try {
                if (zzvu.zzkd() instanceof zzuc) {
                    zzuc zzuc = (zzuc) zzvu.zzkd();
                    publisherAdView.setAdListener(zzuc != null ? zzuc.getAdListener() : null);
                }
            } catch (RemoteException e2) {
                zzayu.zzc("", e2);
            }
            try {
                if (zzvu.zzkc() instanceof zzul) {
                    zzul zzul = (zzul) zzvu.zzkc();
                    if (zzul != null) {
                        appEventListener = zzul.getAppEventListener();
                    }
                    publisherAdView.setAppEventListener(appEventListener);
                }
            } catch (RemoteException e3) {
                zzayu.zzc("", e3);
            }
            zzayk.zzyu.post(new zzaes(this, publisherAdView, zzvu));
        }
    }
}
