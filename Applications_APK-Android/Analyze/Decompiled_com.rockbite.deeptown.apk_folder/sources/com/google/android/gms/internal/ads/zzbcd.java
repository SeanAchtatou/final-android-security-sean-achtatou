package com.google.android.gms.internal.ads;

import android.net.Uri;
import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzbcd implements zznl {
    private Uri uri;
    private final zznl zzecs;
    private final long zzect;
    private final zznl zzecu;
    private long zzecv;

    zzbcd(zznl zznl, int i2, zznl zznl2) {
        this.zzecs = zznl;
        this.zzect = (long) i2;
        this.zzecu = zznl2;
    }

    public final void close() throws IOException {
        this.zzecs.close();
        this.zzecu.close();
    }

    public final Uri getUri() {
        return this.uri;
    }

    public final int read(byte[] bArr, int i2, int i3) throws IOException {
        int i4;
        long j2 = this.zzecv;
        long j3 = this.zzect;
        if (j2 < j3) {
            i4 = this.zzecs.read(bArr, i2, (int) Math.min((long) i3, j3 - j2));
            this.zzecv += (long) i4;
        } else {
            i4 = 0;
        }
        if (this.zzecv < this.zzect) {
            return i4;
        }
        int read = this.zzecu.read(bArr, i2 + i4, i3 - i4);
        int i5 = i4 + read;
        this.zzecv += (long) read;
        return i5;
    }

    public final long zza(zznq zznq) throws IOException {
        zznq zznq2;
        zznq zznq3;
        zznq zznq4 = zznq;
        this.uri = zznq4.uri;
        long j2 = zznq4.zzamw;
        long j3 = this.zzect;
        if (j2 >= j3) {
            zznq2 = null;
        } else {
            long j4 = zznq4.zzce;
            zznq2 = new zznq(zznq4.uri, j2, j4 != -1 ? Math.min(j4, j3 - j2) : j3 - j2, null);
        }
        long j5 = zznq4.zzce;
        if (j5 == -1 || zznq4.zzamw + j5 > this.zzect) {
            long max = Math.max(this.zzect, zznq4.zzamw);
            long j6 = zznq4.zzce;
            zznq3 = new zznq(zznq4.uri, max, j6 != -1 ? Math.min(j6, (zznq4.zzamw + j6) - this.zzect) : -1, null);
        } else {
            zznq3 = null;
        }
        long j7 = 0;
        long zza = zznq2 != null ? this.zzecs.zza(zznq2) : 0;
        if (zznq3 != null) {
            j7 = this.zzecu.zza(zznq3);
        }
        this.zzecv = zznq4.zzamw;
        if (zza == -1 || j7 == -1) {
            return -1;
        }
        return zza + j7;
    }
}
