package com.google.android.gms.internal.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.ads.internal.overlay.zzn;
import com.google.android.gms.ads.internal.zzq;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcjp implements zzbuv {
    private final zzazl zzbru;

    zzcjp(zzazl zzazl) {
        this.zzbru = zzazl;
    }

    public final void zza(boolean z, Context context) {
        zzazl zzazl = this.zzbru;
        try {
            zzq.zzkp();
            zzn.zza(context, (AdOverlayInfoParcel) zzazl.get(), true);
        } catch (Exception unused) {
        }
    }
}
