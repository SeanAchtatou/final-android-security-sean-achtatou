package com.google.android.gms.ads.internal;

import android.content.Context;
import android.text.TextUtils;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.ads.zzaju;
import com.google.android.gms.internal.ads.zzajx;
import com.google.android.gms.internal.ads.zzajy;
import com.google.android.gms.internal.ads.zzakc;
import com.google.android.gms.internal.ads.zzavf;
import com.google.android.gms.internal.ads.zzayu;
import com.google.android.gms.internal.ads.zzazb;
import com.google.android.gms.internal.ads.zzazd;
import com.google.android.gms.internal.ads.zzazh;
import com.google.android.gms.internal.ads.zzdgs;
import com.google.android.gms.internal.ads.zzdhe;
import com.google.android.gms.internal.ads.zzve;
import com.google.android.gms.internal.ads.zzzn;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzd {
    private long zzbkv = 0;
    private Context zzup;

    public final void zza(Context context, zzazb zzazb, String str, Runnable runnable) {
        zza(context, zzazb, true, null, str, null, runnable);
    }

    public final void zza(Context context, zzazb zzazb, String str, zzavf zzavf) {
        zza(context, zzazb, false, zzavf, zzavf != null ? zzavf.zzvm() : null, str, null);
    }

    @VisibleForTesting
    private final void zza(Context context, zzazb zzazb, boolean z, zzavf zzavf, String str, String str2, Runnable runnable) {
        if (zzq.zzkx().elapsedRealtime() - this.zzbkv < 5000) {
            zzayu.zzez("Not retrying to fetch app settings");
            return;
        }
        this.zzbkv = zzq.zzkx().elapsedRealtime();
        boolean z2 = true;
        if (zzavf != null) {
            if (!(zzq.zzkx().currentTimeMillis() - zzavf.zzvj() > ((Long) zzve.zzoy().zzd(zzzn.zzcmt)).longValue()) && zzavf.zzvk()) {
                z2 = false;
            }
        }
        if (z2) {
            if (context == null) {
                zzayu.zzez("Context not provided to fetch application settings");
            } else if (!TextUtils.isEmpty(str) || !TextUtils.isEmpty(str2)) {
                Context applicationContext = context.getApplicationContext();
                if (applicationContext == null) {
                    applicationContext = context;
                }
                this.zzup = applicationContext;
                zzakc zzb = zzq.zzld().zzb(this.zzup, zzazb);
                zzajy<JSONObject> zzajy = zzajx.zzdaq;
                zzaju zza = zzb.zza("google.afma.config.fetchAppSettings", zzajy, zzajy);
                try {
                    JSONObject jSONObject = new JSONObject();
                    if (!TextUtils.isEmpty(str)) {
                        jSONObject.put("app_id", str);
                    } else if (!TextUtils.isEmpty(str2)) {
                        jSONObject.put("ad_unit_id", str2);
                    }
                    jSONObject.put("is_init", z);
                    jSONObject.put("pn", context.getPackageName());
                    zzdhe zzi = zza.zzi(jSONObject);
                    zzdhe zzb2 = zzdgs.zzb(zzi, zzf.zzbkw, zzazd.zzdwj);
                    if (runnable != null) {
                        zzi.addListener(runnable, zzazd.zzdwj);
                    }
                    zzazh.zza(zzb2, "ConfigLoader.maybeFetchNewAppSettings");
                } catch (Exception e2) {
                    zzayu.zzc("Error requesting application settings", e2);
                }
            } else {
                zzayu.zzez("App settings could not be fetched. Required parameters missing");
            }
        }
    }
}
