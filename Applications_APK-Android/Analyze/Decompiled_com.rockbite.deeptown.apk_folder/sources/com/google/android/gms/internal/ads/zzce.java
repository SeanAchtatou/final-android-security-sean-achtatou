package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
public enum zzce implements zzdry {
    UNKNOWN_PROTO(0),
    AFMA_SIGNALS(1),
    UNITY_SIGNALS(2),
    PARTNER_SIGNALS(3);
    
    private static final zzdrx<zzce> zzen = new zzch();
    private final int value;

    private zzce(int i2) {
        this.value = i2;
    }

    public static zzdsa zzaf() {
        return zzcg.zzew;
    }

    public static zzce zzk(int i2) {
        if (i2 == 0) {
            return UNKNOWN_PROTO;
        }
        if (i2 == 1) {
            return AFMA_SIGNALS;
        }
        if (i2 == 2) {
            return UNITY_SIGNALS;
        }
        if (i2 != 3) {
            return null;
        }
        return PARTNER_SIGNALS;
    }

    public final String toString() {
        return "<" + zzce.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.value + " name=" + name() + '>';
    }

    public final int zzae() {
        return this.value;
    }
}
