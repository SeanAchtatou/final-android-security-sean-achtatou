package com.google.android.gms.ads;

import android.os.RemoteException;
import com.esotericsoftware.spine.Animation;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.ads.zzayu;
import com.google.android.gms.internal.ads.zzxb;
import com.google.android.gms.internal.ads.zzyt;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public final class VideoController {
    @KeepForSdk
    public static final int PLAYBACK_STATE_ENDED = 3;
    @KeepForSdk
    public static final int PLAYBACK_STATE_PAUSED = 2;
    @KeepForSdk
    public static final int PLAYBACK_STATE_PLAYING = 1;
    @KeepForSdk
    public static final int PLAYBACK_STATE_READY = 5;
    @KeepForSdk
    public static final int PLAYBACK_STATE_UNKNOWN = 0;
    private final Object lock = new Object();
    private zzxb zzabt;
    private VideoLifecycleCallbacks zzabu;

    /* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
    public static abstract class VideoLifecycleCallbacks {
        public void onVideoEnd() {
        }

        public void onVideoMute(boolean z) {
        }

        public void onVideoPause() {
        }

        public void onVideoPlay() {
        }

        public void onVideoStart() {
        }
    }

    public final float getAspectRatio() {
        synchronized (this.lock) {
            if (this.zzabt == null) {
                return Animation.CurveTimeline.LINEAR;
            }
            try {
                float aspectRatio = this.zzabt.getAspectRatio();
                return aspectRatio;
            } catch (RemoteException e2) {
                zzayu.zzc("Unable to call getAspectRatio on video controller.", e2);
                return Animation.CurveTimeline.LINEAR;
            }
        }
    }

    @KeepForSdk
    public final int getPlaybackState() {
        synchronized (this.lock) {
            if (this.zzabt == null) {
                return 0;
            }
            try {
                int playbackState = this.zzabt.getPlaybackState();
                return playbackState;
            } catch (RemoteException e2) {
                zzayu.zzc("Unable to call getPlaybackState on video controller.", e2);
                return 0;
            }
        }
    }

    public final float getVideoCurrentTime() {
        synchronized (this.lock) {
            if (this.zzabt == null) {
                return Animation.CurveTimeline.LINEAR;
            }
            try {
                float zzpl = this.zzabt.zzpl();
                return zzpl;
            } catch (RemoteException e2) {
                zzayu.zzc("Unable to call getCurrentTime on video controller.", e2);
                return Animation.CurveTimeline.LINEAR;
            }
        }
    }

    public final float getVideoDuration() {
        synchronized (this.lock) {
            if (this.zzabt == null) {
                return Animation.CurveTimeline.LINEAR;
            }
            try {
                float zzpk = this.zzabt.zzpk();
                return zzpk;
            } catch (RemoteException e2) {
                zzayu.zzc("Unable to call getDuration on video controller.", e2);
                return Animation.CurveTimeline.LINEAR;
            }
        }
    }

    public final VideoLifecycleCallbacks getVideoLifecycleCallbacks() {
        VideoLifecycleCallbacks videoLifecycleCallbacks;
        synchronized (this.lock) {
            videoLifecycleCallbacks = this.zzabu;
        }
        return videoLifecycleCallbacks;
    }

    public final boolean hasVideoContent() {
        boolean z;
        synchronized (this.lock) {
            z = this.zzabt != null;
        }
        return z;
    }

    public final boolean isClickToExpandEnabled() {
        synchronized (this.lock) {
            if (this.zzabt == null) {
                return false;
            }
            try {
                boolean isClickToExpandEnabled = this.zzabt.isClickToExpandEnabled();
                return isClickToExpandEnabled;
            } catch (RemoteException e2) {
                zzayu.zzc("Unable to call isClickToExpandEnabled.", e2);
                return false;
            }
        }
    }

    public final boolean isCustomControlsEnabled() {
        synchronized (this.lock) {
            if (this.zzabt == null) {
                return false;
            }
            try {
                boolean isCustomControlsEnabled = this.zzabt.isCustomControlsEnabled();
                return isCustomControlsEnabled;
            } catch (RemoteException e2) {
                zzayu.zzc("Unable to call isUsingCustomPlayerControls.", e2);
                return false;
            }
        }
    }

    public final boolean isMuted() {
        synchronized (this.lock) {
            if (this.zzabt == null) {
                return true;
            }
            try {
                boolean isMuted = this.zzabt.isMuted();
                return isMuted;
            } catch (RemoteException e2) {
                zzayu.zzc("Unable to call isMuted on video controller.", e2);
                return true;
            }
        }
    }

    public final void mute(boolean z) {
        synchronized (this.lock) {
            if (this.zzabt != null) {
                try {
                    this.zzabt.mute(z);
                } catch (RemoteException e2) {
                    zzayu.zzc("Unable to call mute on video controller.", e2);
                }
            }
        }
    }

    public final void pause() {
        synchronized (this.lock) {
            if (this.zzabt != null) {
                try {
                    this.zzabt.pause();
                } catch (RemoteException e2) {
                    zzayu.zzc("Unable to call pause on video controller.", e2);
                }
            }
        }
    }

    public final void play() {
        synchronized (this.lock) {
            if (this.zzabt != null) {
                try {
                    this.zzabt.play();
                } catch (RemoteException e2) {
                    zzayu.zzc("Unable to call play on video controller.", e2);
                }
            }
        }
    }

    public final void setVideoLifecycleCallbacks(VideoLifecycleCallbacks videoLifecycleCallbacks) {
        Preconditions.checkNotNull(videoLifecycleCallbacks, "VideoLifecycleCallbacks may not be null.");
        synchronized (this.lock) {
            this.zzabu = videoLifecycleCallbacks;
            if (this.zzabt != null) {
                try {
                    this.zzabt.zza(new zzyt(videoLifecycleCallbacks));
                } catch (RemoteException e2) {
                    zzayu.zzc("Unable to call setVideoLifecycleCallbacks on video controller.", e2);
                }
            }
        }
    }

    public final void stop() {
        synchronized (this.lock) {
            if (this.zzabt != null) {
                try {
                    this.zzabt.stop();
                } catch (RemoteException e2) {
                    zzayu.zzc("Unable to call stop on video controller.", e2);
                }
            }
        }
    }

    public final void zza(zzxb zzxb) {
        synchronized (this.lock) {
            this.zzabt = zzxb;
            if (this.zzabu != null) {
                setVideoLifecycleCallbacks(this.zzabu);
            }
        }
    }

    public final zzxb zzdl() {
        zzxb zzxb;
        synchronized (this.lock) {
            zzxb = this.zzabt;
        }
        return zzxb;
    }
}
