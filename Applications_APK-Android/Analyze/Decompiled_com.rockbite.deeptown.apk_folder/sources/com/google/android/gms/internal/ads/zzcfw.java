package com.google.android.gms.internal.ads;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcfw implements zzdxg<zzcgi> {
    public static zzcgi zzca(Context context) {
        return (zzcgi) zzdxm.zza(new zzcgi(context), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        throw new NoSuchMethodError();
    }
}
