package com.google.android.gms.internal.ads;

import java.util.Set;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcuf<T> implements zzdxg<zzcua<T>> {
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<Set<zzcub<? extends zzcty<T>>>> zzghg;

    private zzcuf(zzdxp<Executor> zzdxp, zzdxp<Set<zzcub<? extends zzcty<T>>>> zzdxp2) {
        this.zzfcv = zzdxp;
        this.zzghg = zzdxp2;
    }

    public static <T> zzcuf<T> zzau(zzdxp<Executor> zzdxp, zzdxp<Set<zzcub<? extends zzcty<T>>>> zzdxp2) {
        return new zzcuf<>(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return new zzcua(this.zzfcv.get(), this.zzghg.get());
    }
}
