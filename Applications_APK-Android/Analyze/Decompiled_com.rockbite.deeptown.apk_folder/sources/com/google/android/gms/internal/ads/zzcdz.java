package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcdz implements zzdxg<zzcea> {
    private final zzdxp<String> zzfta;
    private final zzdxp<zzcdv> zzftb;

    public zzcdz(zzdxp<String> zzdxp, zzdxp<zzcdv> zzdxp2) {
        this.zzfta = zzdxp;
        this.zzftb = zzdxp2;
    }

    public final /* synthetic */ Object get() {
        return new zzcea(this.zzfta.get(), this.zzftb.get());
    }
}
