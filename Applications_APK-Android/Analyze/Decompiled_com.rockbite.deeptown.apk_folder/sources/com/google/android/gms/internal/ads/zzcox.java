package com.google.android.gms.internal.ads;

import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzcox<AdT> {
    boolean isLoading();

    boolean zza(zzug zzug, String str, zzcpa zzcpa, zzcoz<? super AdT> zzcoz) throws RemoteException;
}
