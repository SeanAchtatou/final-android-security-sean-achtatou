package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzblt implements zzdxg<zzblo> {
    private final zzdxp<zzbmg> zzetc;
    private final zzdxp<Runnable> zzexh;
    private final zzdxp<zzaea> zzfdd;
    private final zzdxp<Executor> zzfei;

    public zzblt(zzdxp<zzbmg> zzdxp, zzdxp<zzaea> zzdxp2, zzdxp<Runnable> zzdxp3, zzdxp<Executor> zzdxp4) {
        this.zzetc = zzdxp;
        this.zzfdd = zzdxp2;
        this.zzexh = zzdxp3;
        this.zzfei = zzdxp4;
    }

    public final /* synthetic */ Object get() {
        return new zzblo(this.zzetc.get(), this.zzfdd.get(), this.zzexh.get(), this.zzfei.get());
    }
}
