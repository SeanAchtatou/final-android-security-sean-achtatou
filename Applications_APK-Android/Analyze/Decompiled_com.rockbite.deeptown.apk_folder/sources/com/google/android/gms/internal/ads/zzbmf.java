package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public abstract class zzbmf {
    public abstract zzbpg zzadh();

    public abstract zzbpm zzadi();

    public abstract zzboq zzadj();

    public abstract zzbpd zzadk();

    public abstract zzbsy zzadl();

    public abstract zzcnd zzadm();
}
