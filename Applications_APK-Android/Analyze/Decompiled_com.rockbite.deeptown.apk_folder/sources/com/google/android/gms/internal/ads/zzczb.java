package com.google.android.gms.internal.ads;

import android.os.RemoteException;
import com.google.android.gms.ads.reward.AdMetadataListener;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzczb extends AdMetadataListener {
    private final /* synthetic */ zzcyz zzgkv;
    private final /* synthetic */ zzwv zzgkz;

    zzczb(zzcyz zzcyz, zzwv zzwv) {
        this.zzgkv = zzcyz;
        this.zzgkz = zzwv;
    }

    public final void onAdMetadataChanged() {
        if (this.zzgkv.zzgky != null) {
            try {
                this.zzgkz.onAdMetadataChanged();
            } catch (RemoteException e2) {
                zzayu.zze("#007 Could not call remote method.", e2);
            }
        }
    }
}
