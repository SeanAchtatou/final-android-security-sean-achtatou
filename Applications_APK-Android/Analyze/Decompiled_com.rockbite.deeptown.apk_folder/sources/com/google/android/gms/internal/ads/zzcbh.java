package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzcbh extends zzboe<zzcbi> {
    zzcbi zzaes();

    zzcbh zze(zzbod zzbod);

    zzcbh zze(zzbrm zzbrm);
}
