package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbjf {
    public long timestamp = 0;
    public boolean zzbnq = false;
    public boolean zzfco = false;
    private boolean zzfcp = false;
    public String zzfcq;
    public zzpt zzfcr = null;

    protected zzbjf() {
    }
}
