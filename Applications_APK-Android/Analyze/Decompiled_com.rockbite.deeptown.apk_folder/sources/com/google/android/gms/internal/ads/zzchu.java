package com.google.android.gms.internal.ads;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzchu implements zzdxg<zzchr> {
    private final zzdxp<Context> zzejv;

    private zzchu(zzdxp<Context> zzdxp) {
        this.zzejv = zzdxp;
    }

    public static zzchu zzac(zzdxp<Context> zzdxp) {
        return new zzchu(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzchr(this.zzejv.get());
    }
}
