package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcsb implements zzcub<zzcry> {
    private final zzdhd zzfov;

    public zzcsb(zzdhd zzdhd) {
        this.zzfov = zzdhd;
    }

    public final zzdhe<zzcry> zzanc() {
        return this.zzfov.zzd(zzcsa.zzgfx);
    }
}
