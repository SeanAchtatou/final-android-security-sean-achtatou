package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbih implements zzdxg<zzava> {
    private static final zzbih zzfbf = new zzbih();

    public static zzbih zzafi() {
        return zzfbf;
    }

    public static zzava zzafj() {
        return (zzava) zzdxm.zza(new zzauy(), "Cannot return null from a non-@Nullable @Provides method");
    }

    public final /* synthetic */ Object get() {
        return zzafj();
    }
}
