package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
final class zzaap {
    public static final int zzcsh = 1;
    public static final int zzcsi = 2;
    public static final int zzcsj = 3;
    public static final int zzcsk = 4;
    private static final /* synthetic */ int[] zzcsl = {zzcsh, zzcsi, zzcsj, zzcsk};

    public static int[] zzqv() {
        return (int[]) zzcsl.clone();
    }
}
