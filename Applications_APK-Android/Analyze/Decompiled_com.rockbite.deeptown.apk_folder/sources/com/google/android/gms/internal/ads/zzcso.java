package com.google.android.gms.internal.ads;

import android.os.Bundle;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcso implements zzcsk {
    static final zzcsk zzgga = new zzcso();

    private zzcso() {
    }

    public final void zzr(Object obj) {
        ((Bundle) obj).putInt("native_version", 0);
    }
}
