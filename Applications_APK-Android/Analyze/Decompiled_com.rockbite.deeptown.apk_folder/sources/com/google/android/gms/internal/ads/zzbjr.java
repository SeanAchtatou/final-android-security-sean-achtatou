package com.google.android.gms.internal.ads;

import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbjr implements zzdxg<JSONObject> {
    private final zzdxp<zzczl> zzfda;

    private zzbjr(zzdxp<zzczl> zzdxp) {
        this.zzfda = zzdxp;
    }

    public static zzbjr zzc(zzdxp<zzczl> zzdxp) {
        return new zzbjr(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return zzbjs.zza(this.zzfda.get());
    }
}
