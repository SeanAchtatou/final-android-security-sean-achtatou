package com.google.android.gms.internal.ads;

import java.util.Deque;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingDeque;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzdaf<T> {
    private final zzdhd zzfov;
    private final Deque<zzdhe<T>> zzgmw = new LinkedBlockingDeque();
    private final Callable<T> zzgmx;

    public zzdaf(Callable<T> callable, zzdhd zzdhd) {
        this.zzgmx = callable;
        this.zzfov = zzdhd;
    }

    public final synchronized zzdhe<T> zzaou() {
        zzdm(1);
        return this.zzgmw.poll();
    }

    public final synchronized void zzb(zzdhe<T> zzdhe) {
        this.zzgmw.addFirst(zzdhe);
    }

    public final synchronized void zzdm(int i2) {
        int size = i2 - this.zzgmw.size();
        for (int i3 = 0; i3 < size; i3++) {
            this.zzgmw.add(this.zzfov.zzd(this.zzgmx));
        }
    }
}
