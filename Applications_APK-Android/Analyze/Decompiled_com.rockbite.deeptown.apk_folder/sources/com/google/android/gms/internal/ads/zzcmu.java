package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzcmu implements zzbth {
    private final /* synthetic */ zzczt zzgav;
    private final /* synthetic */ zzczl zzgaw;
    private final /* synthetic */ zzcip zzgax;
    final /* synthetic */ zzcms zzgay;

    zzcmu(zzcms zzcms, zzczt zzczt, zzczl zzczl, zzcip zzcip) {
        this.zzgay = zzcms;
        this.zzgav = zzczt;
        this.zzgaw = zzczl;
        this.zzgax = zzcip;
    }

    public final void onInitializationSucceeded() {
        this.zzgay.zzfci.execute(new zzcmt(this, this.zzgav, this.zzgaw, this.zzgax));
    }

    public final void zzdh(int i2) {
        String valueOf = String.valueOf(this.zzgax.zzfge);
        zzayu.zzez(valueOf.length() != 0 ? "Fail to initialize adapter ".concat(valueOf) : new String("Fail to initialize adapter "));
    }
}
