package com.google.android.gms.internal.ads;

import android.graphics.drawable.Drawable;
import android.os.RemoteException;
import com.esotericsoftware.spine.Animation;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbwi extends zzacg {
    private final zzbws zzfkc;
    private IObjectWrapper zzfle;

    public zzbwi(zzbws zzbws) {
        this.zzfkc = zzbws;
    }

    private final float zzaiu() {
        try {
            return this.zzfkc.getVideoController().getAspectRatio();
        } catch (RemoteException e2) {
            zzayu.zzc("Remote exception getting video controller aspect ratio.", e2);
            return Animation.CurveTimeline.LINEAR;
        }
    }

    private static float zzap(IObjectWrapper iObjectWrapper) {
        Drawable drawable;
        if (iObjectWrapper == null || (drawable = (Drawable) ObjectWrapper.unwrap(iObjectWrapper)) == null || drawable.getIntrinsicWidth() == -1 || drawable.getIntrinsicHeight() == -1) {
            return Animation.CurveTimeline.LINEAR;
        }
        return ((float) drawable.getIntrinsicWidth()) / ((float) drawable.getIntrinsicHeight());
    }

    public final float getAspectRatio() throws RemoteException {
        if (!((Boolean) zzve.zzoy().zzd(zzzn.zzcqi)).booleanValue()) {
            return Animation.CurveTimeline.LINEAR;
        }
        if (this.zzfkc.getMediaContentAspectRatio() != Animation.CurveTimeline.LINEAR) {
            return this.zzfkc.getMediaContentAspectRatio();
        }
        if (this.zzfkc.getVideoController() != null) {
            return zzaiu();
        }
        IObjectWrapper iObjectWrapper = this.zzfle;
        if (iObjectWrapper != null) {
            return zzap(iObjectWrapper);
        }
        zzaci zzajc = this.zzfkc.zzajc();
        if (zzajc == null) {
            return Animation.CurveTimeline.LINEAR;
        }
        float width = (zzajc == null || zzajc.getWidth() == -1 || zzajc.getHeight() == -1) ? Animation.CurveTimeline.LINEAR : ((float) zzajc.getWidth()) / ((float) zzajc.getHeight());
        if (width != Animation.CurveTimeline.LINEAR) {
            return width;
        }
        return zzap(zzajc.zzrc());
    }

    public final void zzo(IObjectWrapper iObjectWrapper) {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcmd)).booleanValue()) {
            this.zzfle = iObjectWrapper;
        }
    }

    public final IObjectWrapper zzre() throws RemoteException {
        IObjectWrapper iObjectWrapper = this.zzfle;
        if (iObjectWrapper != null) {
            return iObjectWrapper;
        }
        zzaci zzajc = this.zzfkc.zzajc();
        if (zzajc == null) {
            return null;
        }
        return zzajc.zzrc();
    }
}
