package com.google.android.gms.internal.ads;

import android.os.Bundle;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcvu implements zzded {
    static final zzded zzdoq = new zzcvu();

    private zzcvu() {
    }

    public final Object apply(Object obj) {
        return new zzcvs((Bundle) obj);
    }
}
