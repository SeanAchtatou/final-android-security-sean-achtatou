package com.google.android.gms.internal.ads;

import com.google.android.gms.common.util.VisibleForTesting;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzaqt {
    public final float zzboh;
    public final int zzdgf;
    public final int zzdgg;
    private final int zzdmh;
    private final boolean zzdmi;
    private final boolean zzdmj;
    private final int zzdmk;
    private final int zzdml;
    private final int zzdmm;
    private final String zzdmn;
    public final int zzdmo;
    public final int zzdmp;
    private final int zzdmq;
    private final boolean zzdmr;
    private final int zzdms;
    private final double zzdmt;
    private final boolean zzdmu;
    private final String zzdmv;
    private final String zzdmw;
    public final boolean zzdmx;
    public final boolean zzdmy;
    public final String zzdmz;
    public final boolean zzdna;
    public final boolean zzdnb;
    public final boolean zzdnc;
    public final String zzdnd;
    public final String zzdne;
    public final String zzdnf;
    private final boolean zzdng;

    @VisibleForTesting
    zzaqt(int i2, boolean z, boolean z2, String str, String str2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, String str3, String str4, String str5, int i3, int i4, int i5, int i6, int i7, int i8, float f2, int i9, int i10, double d2, boolean z8, boolean z9, int i11, String str6, boolean z10, String str7) {
        this.zzdmh = i2;
        this.zzdmx = z;
        this.zzdmy = z2;
        this.zzdmn = str;
        this.zzdmz = str2;
        this.zzdna = z3;
        this.zzdnb = z4;
        this.zzdnc = z5;
        this.zzdmi = z6;
        this.zzdmj = z7;
        this.zzdnd = str3;
        this.zzdne = str4;
        this.zzdmk = i3;
        this.zzdmo = i4;
        this.zzdmp = i5;
        this.zzdmq = i6;
        this.zzdml = i7;
        this.zzdmm = i8;
        this.zzboh = f2;
        this.zzdgf = i9;
        this.zzdgg = i10;
        this.zzdmt = d2;
        this.zzdmu = z8;
        this.zzdmr = z9;
        this.zzdms = i11;
        this.zzdmv = str6;
        this.zzdng = z10;
        this.zzdnf = str5;
        this.zzdmw = str7;
    }
}
