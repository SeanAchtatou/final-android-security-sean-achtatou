package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.text.TextUtils;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcds {
    private ConcurrentHashMap<String, String> zzfsq;

    public zzcds(zzcdv zzcdv) {
        this.zzfsq = zzcdv.zzalh();
    }

    public final void zzc(zzczt zzczt) {
        if (zzczt.zzgmi.zzgme.size() > 0) {
            int i2 = zzczt.zzgmi.zzgme.get(0).zzfjj;
            if (i2 == 1) {
                this.zzfsq.put("ad_format", "banner");
            } else if (i2 == 2) {
                this.zzfsq.put("ad_format", "interstitial");
            } else if (i2 == 3) {
                this.zzfsq.put("ad_format", "native_express");
            } else if (i2 == 4) {
                this.zzfsq.put("ad_format", "native_advanced");
            } else if (i2 != 5) {
                this.zzfsq.put("ad_format", "unknown");
            } else {
                this.zzfsq.put("ad_format", "rewarded");
            }
            if (!TextUtils.isEmpty(zzczt.zzgmi.zzgmf.zzbzo)) {
                this.zzfsq.put("gqi", zzczt.zzgmi.zzgmf.zzbzo);
            }
        }
    }

    public final void zzi(Bundle bundle) {
        if (bundle.containsKey("cnt")) {
            this.zzfsq.put("network_coarse", Integer.toString(bundle.getInt("cnt")));
        }
        if (bundle.containsKey("gnt")) {
            this.zzfsq.put("network_fine", Integer.toString(bundle.getInt("gnt")));
        }
    }

    public final Map<String, String> zzqu() {
        return this.zzfsq;
    }
}
