package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.Map;
import java.util.Set;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzatq {
    zzdhe<Map<String, String>> zza(Context context, Set<String> set);
}
