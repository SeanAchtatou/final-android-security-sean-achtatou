package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.zzq;
import java.io.InputStream;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcgm extends zzcgk {
    public zzcgm(Context context) {
        this.zzfvt = new zzaps(context, zzq.zzle().zzxb(), this, this);
    }

    public final void onConnected(Bundle bundle) {
        synchronized (this.mLock) {
            if (!this.zzfvr) {
                this.zzfvr = true;
                try {
                    this.zzfvt.zztx().zzb(this.zzfvs, new zzcgj(this));
                } catch (RemoteException | IllegalArgumentException unused) {
                    this.zzdbf.setException(new zzcgr(0));
                } catch (Throwable th) {
                    zzq.zzku().zza(th, "RemoteSignalsClientTask.onConnected");
                    this.zzdbf.setException(new zzcgr(0));
                }
            }
        }
    }

    public final zzdhe<InputStream> zzg(zzaqk zzaqk) {
        synchronized (this.mLock) {
            if (this.zzfvq) {
                zzazl<InputStream> zzazl = this.zzdbf;
                return zzazl;
            }
            this.zzfvq = true;
            this.zzfvs = zzaqk;
            this.zzfvt.checkAvailabilityAndConnect();
            this.zzdbf.addListener(new zzcgl(this), zzazd.zzdwj);
            zzazl<InputStream> zzazl2 = this.zzdbf;
            return zzazl2;
        }
    }
}
