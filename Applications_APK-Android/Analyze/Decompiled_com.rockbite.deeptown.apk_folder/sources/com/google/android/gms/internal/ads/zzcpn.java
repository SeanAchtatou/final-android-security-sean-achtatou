package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcpn implements zzdxg<Integer> {
    private final zzcpj zzged;

    public zzcpn(zzcpj zzcpj) {
        this.zzged = zzcpj;
    }

    public final /* synthetic */ Object get() {
        return Integer.valueOf(this.zzged.zzanb());
    }
}
