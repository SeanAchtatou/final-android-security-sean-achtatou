package com.google.android.gms.internal.ads;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface zzahb extends IInterface {
    void destroy() throws RemoteException;

    zzxb getVideoController() throws RemoteException;

    void zza(IObjectWrapper iObjectWrapper, zzahg zzahg) throws RemoteException;

    void zzr(IObjectWrapper iObjectWrapper) throws RemoteException;
}
