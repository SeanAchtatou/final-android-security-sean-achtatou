package com.google.android.gms.internal.ads;

import android.content.Context;
import android.content.pm.PackageInfo;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzava {
    zzdhe<String> zza(String str, PackageInfo packageInfo);

    zzdhe<AdvertisingIdClient.Info> zzak(Context context);
}
