package com.google.android.gms.ads;

public final class R {

    public static final class attr {
        public static final int adSize = 2130837538;
        public static final int adSizes = 2130837539;
        public static final int adUnitId = 2130837540;

        private attr() {
        }
    }

    public static final class style {
        public static final int Theme_IAPTheme = 2131493130;

        private style() {
        }
    }

    public static final class styleable {
        public static final int[] AdsAttrs = {com.rockbite.deeptown.R.attr.adSize, com.rockbite.deeptown.R.attr.adSizes, com.rockbite.deeptown.R.attr.adUnitId};
        public static final int AdsAttrs_adSize = 0;
        public static final int AdsAttrs_adSizes = 1;
        public static final int AdsAttrs_adUnitId = 2;

        private styleable() {
        }
    }

    private R() {
    }
}
