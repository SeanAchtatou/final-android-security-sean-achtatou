package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcje implements Runnable {
    private final zzbdi zzfpv;
    private final zzcix zzfym;

    zzcje(zzcix zzcix, zzbdi zzbdi) {
        this.zzfym = zzcix;
        this.zzfpv = zzbdi;
    }

    public final void run() {
        this.zzfym.zzo(this.zzfpv);
    }
}
