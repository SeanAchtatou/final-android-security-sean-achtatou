package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcyh implements zzcxo {
    private final int zzdvv;

    zzcyh(int i2) {
        this.zzdvv = i2;
    }

    public final void zzt(Object obj) {
        ((zzasl) obj).onRewardedAdFailedToShow(this.zzdvv);
    }
}
