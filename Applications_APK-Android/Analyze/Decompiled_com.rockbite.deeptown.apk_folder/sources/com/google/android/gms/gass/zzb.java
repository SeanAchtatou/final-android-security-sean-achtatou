package com.google.android.gms.gass;

import android.content.Context;
import com.google.android.gms.internal.ads.zzsr;
import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-gass@@18.3.0 */
final /* synthetic */ class zzb implements Callable {
    private final Context zzcri;

    zzb(Context context) {
        this.zzcri = context;
    }

    public final Object call() {
        return new zzsr(this.zzcri, "GLAS", null);
    }
}
