package com.google.android.gms.internal.ads;

import java.util.Map;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzbiz implements zzafn<Object> {
    final /* synthetic */ zzbiy zzfbw;

    zzbiz(zzbiy zzbiy) {
        this.zzfbw = zzbiy;
    }

    public final void zza(Object obj, Map<String, String> map) {
        if (this.zzfbw.zzl(map)) {
            this.zzfbw.executor.execute(new zzbjc(this));
        }
    }
}
