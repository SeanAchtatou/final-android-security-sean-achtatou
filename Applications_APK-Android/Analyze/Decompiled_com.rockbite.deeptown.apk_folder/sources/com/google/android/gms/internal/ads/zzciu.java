package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzciu implements zzcio<zzbke> {
    private final Executor zzfci;
    private final zzczu zzfgl;
    private final zzcbn zzfod;
    private final zzbka zzfyh;
    private final Context zzup;

    public zzciu(zzbka zzbka, Context context, Executor executor, zzcbn zzcbn, zzczu zzczu) {
        this.zzup = context;
        this.zzfyh = zzbka;
        this.zzfci = executor;
        this.zzfod = zzcbn;
        this.zzfgl = zzczu;
    }

    public final boolean zza(zzczt zzczt, zzczl zzczl) {
        zzczp zzczp = zzczl.zzglo;
        return (zzczp == null || zzczp.zzdht == null) ? false : true;
    }

    public final zzdhe<zzbke> zzb(zzczt zzczt, zzczl zzczl) {
        return zzdgs.zzb(zzdgs.zzaj(null), new zzcit(this, zzczt, zzczl), this.zzfci);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ zzdhe zza(zzczt zzczt, zzczl zzczl, Object obj) throws Exception {
        zzuj zza = zzczy.zza(this.zzup, zzczl.zzglq);
        zzbdi zzc = this.zzfod.zzc(zza);
        zzbjt zza2 = this.zzfyh.zza(new zzbmt(zzczt, zzczl, null), new zzbjw(zzc.getView(), zzc, zzczy.zze(zza), zzczl.zzfdp));
        zza2.zzadx().zzb(zzc, false);
        zza2.zzadk().zza(new zzciw(zzc), zzazd.zzdwj);
        zza2.zzadx();
        zzczp zzczp = zzczl.zzglo;
        return zzdgs.zzb(zzcbp.zza(zzc, zzczp.zzdhr, zzczp.zzdht), new zzciv(zza2), zzazd.zzdwj);
    }
}
