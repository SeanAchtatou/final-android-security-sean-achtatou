package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public interface zzbca {
    void zza(String str, Exception exc);

    void zzb(boolean z, long j2);

    void zzda(int i2);

    void zzn(int i2, int i3);
}
