package com.google.android.gms.ads.impl;

public final class R {

    public static final class string {
        public static final int s1 = 2131427409;
        public static final int s2 = 2131427410;
        public static final int s3 = 2131427411;
        public static final int s4 = 2131427412;
        public static final int s5 = 2131427413;
        public static final int s6 = 2131427414;
        public static final int s7 = 2131427415;

        private string() {
        }
    }

    private R() {
    }
}
