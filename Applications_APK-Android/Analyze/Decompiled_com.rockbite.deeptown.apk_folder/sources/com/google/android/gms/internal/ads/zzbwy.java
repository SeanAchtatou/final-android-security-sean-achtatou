package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbwy implements zzdxg<zzbww> {
    private final zzdxp<zzczl> zzfda;

    public zzbwy(zzdxp<zzczl> zzdxp) {
        this.zzfda = zzdxp;
    }

    public final /* synthetic */ Object get() {
        return new zzbww(this.zzfda.get());
    }
}
