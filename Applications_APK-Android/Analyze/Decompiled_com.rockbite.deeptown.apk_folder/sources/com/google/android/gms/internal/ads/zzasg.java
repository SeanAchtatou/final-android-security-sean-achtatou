package com.google.android.gms.internal.ads;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

/* compiled from: com.google.android.gms:play-services-ads-lite@@18.3.0 */
public interface zzasg extends IInterface {
    Bundle getAdMetadata() throws RemoteException;

    String getMediationAdapterClassName() throws RemoteException;

    boolean isLoaded() throws RemoteException;

    void zza(IObjectWrapper iObjectWrapper, boolean z) throws RemoteException;

    void zza(zzasl zzasl) throws RemoteException;

    void zza(zzast zzast) throws RemoteException;

    void zza(zzatb zzatb) throws RemoteException;

    void zza(zzug zzug, zzaso zzaso) throws RemoteException;

    void zza(zzwv zzwv) throws RemoteException;

    void zzh(IObjectWrapper iObjectWrapper) throws RemoteException;

    zzxa zzkb() throws RemoteException;

    zzasf zzpz() throws RemoteException;
}
