package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzq;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzbcl implements Runnable {
    private final /* synthetic */ zzbci zzeda;

    zzbcl(zzbci zzbci) {
        this.zzeda = zzbci;
    }

    public final void run() {
        zzq.zzlm().zzb(this.zzeda);
    }
}
