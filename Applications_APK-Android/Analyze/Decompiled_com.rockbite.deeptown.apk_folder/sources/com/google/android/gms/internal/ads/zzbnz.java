package com.google.android.gms.internal.ads;

import android.content.Context;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbnz implements zzdxg<zzded<zzczl, zzawt>> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<zzazb> zzfav;
    private final zzdxp<zzczu> zzfep;

    public zzbnz(zzdxp<Context> zzdxp, zzdxp<zzazb> zzdxp2, zzdxp<zzczu> zzdxp3) {
        this.zzejv = zzdxp;
        this.zzfav = zzdxp2;
        this.zzfep = zzdxp3;
    }

    public final /* synthetic */ Object get() {
        return (zzded) zzdxm.zza(new zzboa(this.zzejv.get(), this.zzfav.get(), this.zzfep.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
