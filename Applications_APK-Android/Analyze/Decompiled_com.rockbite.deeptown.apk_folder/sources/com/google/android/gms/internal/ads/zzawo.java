package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.app.Activity;
import android.graphics.Rect;
import android.text.TextUtils;
import android.view.DisplayCutout;
import android.view.View;
import android.view.Window;
import android.view.WindowInsets;
import android.view.WindowManager;
import com.google.android.gms.ads.internal.zzq;
import java.util.Locale;

@TargetApi(28)
/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzawo extends zzawp {
    private static void zza(boolean z, Activity activity) {
        Window window = activity.getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        int i2 = attributes.layoutInDisplayCutoutMode;
        int i3 = z ? 1 : 2;
        if (i3 != i2) {
            attributes.layoutInDisplayCutoutMode = i3;
            window.setAttributes(attributes);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzawo.zza(boolean, android.app.Activity):void
     arg types: [int, android.app.Activity]
     candidates:
      com.google.android.gms.internal.ads.zzawp.zza(android.content.Context, android.telephony.TelephonyManager):com.google.android.gms.internal.ads.zzte
      com.google.android.gms.internal.ads.zzawm.zza(android.app.Activity, android.content.res.Configuration):boolean
      com.google.android.gms.internal.ads.zzawi.zza(android.content.Context, android.webkit.WebSettings):boolean
      com.google.android.gms.internal.ads.zzawh.zza(android.content.Context, android.telephony.TelephonyManager):com.google.android.gms.internal.ads.zzte
      com.google.android.gms.internal.ads.zzawh.zza(android.app.Activity, android.content.res.Configuration):boolean
      com.google.android.gms.internal.ads.zzawh.zza(android.content.Context, android.webkit.WebSettings):boolean
      com.google.android.gms.internal.ads.zzawo.zza(boolean, android.app.Activity):void */
    public final void zzg(Activity activity) {
        if (((Boolean) zzve.zzoy().zzd(zzzn.zzcjk)).booleanValue() && zzq.zzku().zzvf().zzwg() == null && !activity.isInMultiWindowMode()) {
            zza(true, activity);
            activity.getWindow().getDecorView().setOnApplyWindowInsetsListener(new zzawr(this, activity));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.ads.zzawo.zza(boolean, android.app.Activity):void
     arg types: [int, android.app.Activity]
     candidates:
      com.google.android.gms.internal.ads.zzawp.zza(android.content.Context, android.telephony.TelephonyManager):com.google.android.gms.internal.ads.zzte
      com.google.android.gms.internal.ads.zzawm.zza(android.app.Activity, android.content.res.Configuration):boolean
      com.google.android.gms.internal.ads.zzawi.zza(android.content.Context, android.webkit.WebSettings):boolean
      com.google.android.gms.internal.ads.zzawh.zza(android.content.Context, android.telephony.TelephonyManager):com.google.android.gms.internal.ads.zzte
      com.google.android.gms.internal.ads.zzawh.zza(android.app.Activity, android.content.res.Configuration):boolean
      com.google.android.gms.internal.ads.zzawh.zza(android.content.Context, android.webkit.WebSettings):boolean
      com.google.android.gms.internal.ads.zzawo.zza(boolean, android.app.Activity):void */
    static /* synthetic */ WindowInsets zza(Activity activity, View view, WindowInsets windowInsets) {
        if (zzq.zzku().zzvf().zzwg() == null) {
            DisplayCutout displayCutout = windowInsets.getDisplayCutout();
            String str = "";
            if (displayCutout != null) {
                zzavu zzvf = zzq.zzku().zzvf();
                for (Rect next : displayCutout.getBoundingRects()) {
                    String format = String.format(Locale.US, "%d,%d,%d,%d", Integer.valueOf(next.left), Integer.valueOf(next.top), Integer.valueOf(next.right), Integer.valueOf(next.bottom));
                    if (!TextUtils.isEmpty(str)) {
                        str = String.valueOf(str).concat("|");
                    }
                    String valueOf = String.valueOf(str);
                    String valueOf2 = String.valueOf(format);
                    str = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
                }
                zzvf.zzeh(str);
            } else {
                zzq.zzku().zzvf().zzeh(str);
            }
        }
        zza(false, activity);
        return view.onApplyWindowInsets(windowInsets);
    }
}
