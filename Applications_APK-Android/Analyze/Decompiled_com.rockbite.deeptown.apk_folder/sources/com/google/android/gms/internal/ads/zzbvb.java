package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzbvb implements zzdxg<zzbvc> {
    private final zzdxp<zzbpg> zzfjk;

    private zzbvb(zzdxp<zzbpg> zzdxp) {
        this.zzfjk = zzdxp;
    }

    public static zzbvb zzv(zzdxp<zzbpg> zzdxp) {
        return new zzbvb(zzdxp);
    }

    public final /* synthetic */ Object get() {
        return new zzbvc(this.zzfjk.get());
    }
}
