package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzcyo implements zzcxo {
    static final zzcxo zzgjk = new zzcyo();

    private zzcyo() {
    }

    public final void zzt(Object obj) {
        ((zzaro) obj).onRewardedVideoStarted();
    }
}
