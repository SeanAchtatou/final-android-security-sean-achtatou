package com.google.android.gms.internal.ads;

import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzchs implements zzdxg<zzbsu<zzdcx>> {
    private final zzdxp<Executor> zzfcv;
    private final zzdxp<zzcib> zzfdd;

    private zzchs(zzdxp<zzcib> zzdxp, zzdxp<Executor> zzdxp2) {
        this.zzfdd = zzdxp;
        this.zzfcv = zzdxp2;
    }

    public static zzchs zzai(zzdxp<zzcib> zzdxp, zzdxp<Executor> zzdxp2) {
        return new zzchs(zzdxp, zzdxp2);
    }

    public final /* synthetic */ Object get() {
        return (zzbsu) zzdxm.zza(new zzbsu(this.zzfdd.get(), this.zzfcv.get()), "Cannot return null from a non-@Nullable @Provides method");
    }
}
