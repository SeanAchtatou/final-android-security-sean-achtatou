package com.google.android.gms.internal.ads;

import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final class zzajz implements zzajv<JSONObject>, zzajw<JSONObject> {
    zzajz() {
    }

    public final /* synthetic */ Object zzd(JSONObject jSONObject) throws JSONException {
        return jSONObject;
    }

    public final /* synthetic */ JSONObject zzj(Object obj) throws JSONException {
        return (JSONObject) obj;
    }
}
