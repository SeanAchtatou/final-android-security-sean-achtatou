package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcav implements zzdxg<zzcar> {
    private final zzdxp<zzbra> zzerj;
    private final zzdxp<zzbqa> zzerq;
    private final zzdxp<zzbpm> zzesd;
    private final zzdxp<zzboq> zzesg;
    private final zzdxp<zzbqj> zzesu;

    public zzcav(zzdxp<zzboq> zzdxp, zzdxp<zzbpm> zzdxp2, zzdxp<zzbqa> zzdxp3, zzdxp<zzbqj> zzdxp4, zzdxp<zzbra> zzdxp5) {
        this.zzesg = zzdxp;
        this.zzesd = zzdxp2;
        this.zzerq = zzdxp3;
        this.zzesu = zzdxp4;
        this.zzerj = zzdxp5;
    }

    public final /* synthetic */ Object get() {
        return new zzcar(this.zzesg.get(), this.zzesd.get(), this.zzerq.get(), this.zzesu.get(), this.zzerj.get());
    }
}
