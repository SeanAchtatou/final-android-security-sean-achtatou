package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.ConnectionResult;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcgn extends zzcgk {
    private int zzfvv = zzcgq.zzfvw;

    public zzcgn(Context context) {
        this.zzfvt = new zzaps(context, zzq.zzle().zzxb(), this, this);
    }

    public final void onConnected(Bundle bundle) {
        synchronized (this.mLock) {
            if (!this.zzfvr) {
                this.zzfvr = true;
                try {
                    if (this.zzfvv == zzcgq.zzfvx) {
                        this.zzfvt.zztx().zzc(this.zzfvs, new zzcgj(this));
                    } else if (this.zzfvv == zzcgq.zzfvy) {
                        this.zzfvt.zztx().zza((String) null, new zzcgj(this));
                    } else {
                        this.zzdbf.setException(new zzcgr(0));
                    }
                } catch (RemoteException | IllegalArgumentException unused) {
                    this.zzdbf.setException(new zzcgr(0));
                } catch (Throwable th) {
                    zzq.zzku().zza(th, "RemoteUrlAndCacheKeyClientTask.onConnected");
                    this.zzdbf.setException(new zzcgr(0));
                }
            }
        }
    }

    public final void onConnectionFailed(ConnectionResult connectionResult) {
        zzayu.zzea("Cannot connect to remote service, fallback to local instance.");
        this.zzdbf.setException(new zzcgr(0));
    }
}
