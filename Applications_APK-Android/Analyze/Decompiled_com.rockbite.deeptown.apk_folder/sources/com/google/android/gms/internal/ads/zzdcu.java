package com.google.android.gms.internal.ads;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
final /* synthetic */ class zzdcu implements zzbrn {
    private final zzdca zzgrk;

    zzdcu(zzdca zzdca) {
        this.zzgrk = zzdca;
    }

    public final void zzp(Object obj) {
        zzdca zzdca = this.zzgrk;
        ((zzdcx) obj).zzc((zzdco) zzdca.zzaqd(), zzdca.zzaqe());
    }
}
