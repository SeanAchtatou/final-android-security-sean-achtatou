package com.google.android.gms.internal.ads;

import android.content.Context;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.gms:play-services-ads@@18.3.0 */
public final class zzcjs implements zzdxg<zzcjn> {
    private final zzdxp<Context> zzejv;
    private final zzdxp<Executor> zzfei;
    private final zzdxp<zzbup> zzfyl;
    private final zzdxp<zzczj> zzfza;

    public zzcjs(zzdxp<Context> zzdxp, zzdxp<Executor> zzdxp2, zzdxp<zzbup> zzdxp3, zzdxp<zzczj> zzdxp4) {
        this.zzejv = zzdxp;
        this.zzfei = zzdxp2;
        this.zzfyl = zzdxp3;
        this.zzfza = zzdxp4;
    }

    public final /* synthetic */ Object get() {
        return new zzcjn(this.zzejv.get(), this.zzfei.get(), this.zzfyl.get(), this.zzfza.get());
    }
}
