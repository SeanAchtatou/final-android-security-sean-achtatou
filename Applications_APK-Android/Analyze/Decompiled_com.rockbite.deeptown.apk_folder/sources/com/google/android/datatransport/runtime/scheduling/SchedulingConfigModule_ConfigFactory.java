package com.google.android.datatransport.runtime.scheduling;

import com.google.android.datatransport.runtime.scheduling.jobscheduling.SchedulerConfig;
import com.google.android.datatransport.runtime.time.Clock;
import f.b.b;
import f.b.d;
import javax.inject.Provider;

/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public final class SchedulingConfigModule_ConfigFactory implements b<SchedulerConfig> {
    private final Provider<Clock> clockProvider;

    public SchedulingConfigModule_ConfigFactory(Provider<Clock> provider) {
        this.clockProvider = provider;
    }

    public static SchedulerConfig config(Clock clock) {
        SchedulerConfig config = SchedulingConfigModule.config(clock);
        d.a(config, "Cannot return null from a non-@Nullable @Provides method");
        return config;
    }

    public static SchedulingConfigModule_ConfigFactory create(Provider<Clock> provider) {
        return new SchedulingConfigModule_ConfigFactory(provider);
    }

    public SchedulerConfig get() {
        return config(this.clockProvider.get());
    }
}
