package com.google.android.datatransport.runtime;

import f.b.b;
import f.b.d;
import java.util.concurrent.Executor;

/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public final class ExecutionModule_ExecutorFactory implements b<Executor> {
    private static final ExecutionModule_ExecutorFactory INSTANCE = new ExecutionModule_ExecutorFactory();

    public static ExecutionModule_ExecutorFactory create() {
        return INSTANCE;
    }

    public static Executor executor() {
        Executor executor = ExecutionModule.executor();
        d.a(executor, "Cannot return null from a non-@Nullable @Provides method");
        return executor;
    }

    public Executor get() {
        return executor();
    }
}
