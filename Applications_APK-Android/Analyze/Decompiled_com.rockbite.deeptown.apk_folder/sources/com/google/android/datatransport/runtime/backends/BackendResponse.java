package com.google.android.datatransport.runtime.backends;

import com.google.auto.value.AutoValue;

@AutoValue
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public abstract class BackendResponse {

    /* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
    public enum Status {
        OK,
        TRANSIENT_ERROR,
        FATAL_ERROR
    }

    public static BackendResponse fatalError() {
        return new AutoValue_BackendResponse(Status.FATAL_ERROR, -1);
    }

    public static BackendResponse ok(long j2) {
        return new AutoValue_BackendResponse(Status.OK, j2);
    }

    public static BackendResponse transientError() {
        return new AutoValue_BackendResponse(Status.TRANSIENT_ERROR, -1);
    }

    public abstract long getNextRequestWaitMillis();

    public abstract Status getStatus();
}
