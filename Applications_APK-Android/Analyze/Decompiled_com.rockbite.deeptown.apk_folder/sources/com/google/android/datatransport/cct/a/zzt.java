package com.google.android.datatransport.cct.a;

import com.google.android.datatransport.cct.a.zzi;
import com.google.auto.value.AutoValue;

@AutoValue
/* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
public abstract class zzt {

    @AutoValue.Builder
    /* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
    public static abstract class zza {
        public abstract zza zza(int i2);

        public abstract zza zza(long j2);

        public abstract zza zza(zzy zzy);

        /* access modifiers changed from: package-private */
        public abstract zza zza(String str);

        /* access modifiers changed from: package-private */
        public abstract zza zza(byte[] bArr);

        public abstract zzt zza();

        public abstract zza zzb(long j2);

        public abstract zza zzc(long j2);
    }

    public static zza zza(String str) {
        return new zzi.zza().zza(Integer.MIN_VALUE).zza(str);
    }

    public abstract long zza();

    public abstract long zzb();

    public abstract long zzc();

    public static zza zza(byte[] bArr) {
        return new zzi.zza().zza(Integer.MIN_VALUE).zza(bArr);
    }
}
