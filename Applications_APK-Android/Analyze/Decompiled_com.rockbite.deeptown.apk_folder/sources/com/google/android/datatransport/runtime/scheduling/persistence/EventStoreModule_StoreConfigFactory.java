package com.google.android.datatransport.runtime.scheduling.persistence;

import f.b.b;
import f.b.d;

/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public final class EventStoreModule_StoreConfigFactory implements b<EventStoreConfig> {
    private static final EventStoreModule_StoreConfigFactory INSTANCE = new EventStoreModule_StoreConfigFactory();

    public static EventStoreModule_StoreConfigFactory create() {
        return INSTANCE;
    }

    public static EventStoreConfig storeConfig() {
        EventStoreConfig storeConfig = EventStoreModule.storeConfig();
        d.a(storeConfig, "Cannot return null from a non-@Nullable @Provides method");
        return storeConfig;
    }

    public EventStoreConfig get() {
        return storeConfig();
    }
}
