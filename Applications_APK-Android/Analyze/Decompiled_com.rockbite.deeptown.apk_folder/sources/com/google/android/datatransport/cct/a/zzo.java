package com.google.android.datatransport.cct.a;

import com.google.auto.value.AutoValue;
import java.util.List;

@AutoValue
/* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
public abstract class zzo {
    public static zzo zza(List<zzv> list) {
        return new zze(list);
    }

    public abstract List<zzv> zza();
}
