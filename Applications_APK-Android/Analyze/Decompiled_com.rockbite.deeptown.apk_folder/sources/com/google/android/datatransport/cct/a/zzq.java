package com.google.android.datatransport.cct.a;

import com.google.android.datatransport.cct.a.zzg;
import com.google.auto.value.AutoValue;

@AutoValue
/* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
public abstract class zzq {

    @AutoValue.Builder
    /* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
    public static abstract class zza {
        public abstract zza zza(zza zza);

        public abstract zza zza(zzb zzb);

        public abstract zzq zza();
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    /* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
    public static final class zzb extends Enum<zzb> {
        public static final zzb zza = new zzb("UNKNOWN", 0, 0);
        public static final zzb zzb = new zzb("ANDROID", 1, 4);

        static {
            zzb[] zzbArr = {zza, zzb};
        }

        private zzb(String str, int i2, int i3) {
        }
    }

    public static zza zza() {
        return new zzg.zza();
    }
}
