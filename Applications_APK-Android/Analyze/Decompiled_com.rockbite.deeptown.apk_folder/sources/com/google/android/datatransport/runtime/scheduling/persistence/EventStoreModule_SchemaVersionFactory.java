package com.google.android.datatransport.runtime.scheduling.persistence;

import f.b.b;

/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public final class EventStoreModule_SchemaVersionFactory implements b<Integer> {
    private static final EventStoreModule_SchemaVersionFactory INSTANCE = new EventStoreModule_SchemaVersionFactory();

    public static EventStoreModule_SchemaVersionFactory create() {
        return INSTANCE;
    }

    public static int schemaVersion() {
        return EventStoreModule.schemaVersion();
    }

    public Integer get() {
        return Integer.valueOf(schemaVersion());
    }
}
