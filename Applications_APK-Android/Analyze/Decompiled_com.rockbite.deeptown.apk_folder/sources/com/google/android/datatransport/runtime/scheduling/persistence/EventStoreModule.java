package com.google.android.datatransport.runtime.scheduling.persistence;

import com.google.android.datatransport.runtime.synchronization.SynchronizationGuard;

/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public abstract class EventStoreModule {
    static int schemaVersion() {
        return SchemaManager.SCHEMA_VERSION;
    }

    static EventStoreConfig storeConfig() {
        return EventStoreConfig.DEFAULT;
    }

    /* access modifiers changed from: package-private */
    public abstract EventStore eventStore(SQLiteEventStore sQLiteEventStore);

    /* access modifiers changed from: package-private */
    public abstract SynchronizationGuard synchronizationGuard(SQLiteEventStore sQLiteEventStore);
}
