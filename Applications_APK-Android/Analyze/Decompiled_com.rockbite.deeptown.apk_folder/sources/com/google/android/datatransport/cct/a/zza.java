package com.google.android.datatransport.cct.a;

import com.google.android.datatransport.cct.a.zzd;
import com.google.auto.value.AutoValue;

@AutoValue
/* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
public abstract class zza {

    @AutoValue.Builder
    /* renamed from: com.google.android.datatransport.cct.a.zza$zza  reason: collision with other inner class name */
    /* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
    public static abstract class C0134zza {
        public abstract C0134zza zza(int i2);

        public abstract C0134zza zza(String str);

        public abstract zza zza();

        public abstract C0134zza zzb(String str);

        public abstract C0134zza zzc(String str);

        public abstract C0134zza zzd(String str);

        public abstract C0134zza zze(String str);

        public abstract C0134zza zzf(String str);

        public abstract C0134zza zzg(String str);
    }

    public static C0134zza zza() {
        return new zzd.zza().zza(Integer.MIN_VALUE);
    }
}
