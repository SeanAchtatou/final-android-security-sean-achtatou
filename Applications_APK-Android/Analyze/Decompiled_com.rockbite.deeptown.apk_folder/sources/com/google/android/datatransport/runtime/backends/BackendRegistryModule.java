package com.google.android.datatransport.runtime.backends;

/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public abstract class BackendRegistryModule {
    /* access modifiers changed from: package-private */
    public abstract BackendRegistry backendRegistry(MetadataBackendRegistry metadataBackendRegistry);
}
