package com.google.android.datatransport;

import com.google.auto.value.AutoValue;

@AutoValue
/* compiled from: com.google.android.datatransport:transport-api@@2.2.0 */
public abstract class Event<T> {
    public static <T> Event<T> ofData(int i2, T t) {
        return new AutoValue_Event(Integer.valueOf(i2), t, Priority.DEFAULT);
    }

    public static <T> Event<T> ofTelemetry(int i2, T t) {
        return new AutoValue_Event(Integer.valueOf(i2), t, Priority.VERY_LOW);
    }

    public static <T> Event<T> ofUrgent(int i2, T t) {
        return new AutoValue_Event(Integer.valueOf(i2), t, Priority.HIGHEST);
    }

    public abstract Integer getCode();

    public abstract T getPayload();

    public abstract Priority getPriority();

    public static <T> Event<T> ofData(T t) {
        return new AutoValue_Event(null, t, Priority.DEFAULT);
    }

    public static <T> Event<T> ofTelemetry(T t) {
        return new AutoValue_Event(null, t, Priority.VERY_LOW);
    }

    public static <T> Event<T> ofUrgent(T t) {
        return new AutoValue_Event(null, t, Priority.HIGHEST);
    }
}
