package com.google.android.datatransport.runtime;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import javax.inject.Singleton;

/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
abstract class ExecutionModule {
    ExecutionModule() {
    }

    @Singleton
    static Executor executor() {
        return Executors.newSingleThreadExecutor();
    }
}
