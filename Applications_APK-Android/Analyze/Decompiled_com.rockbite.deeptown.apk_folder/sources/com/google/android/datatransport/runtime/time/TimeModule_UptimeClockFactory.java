package com.google.android.datatransport.runtime.time;

import f.b.b;
import f.b.d;

/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public final class TimeModule_UptimeClockFactory implements b<Clock> {
    private static final TimeModule_UptimeClockFactory INSTANCE = new TimeModule_UptimeClockFactory();

    public static TimeModule_UptimeClockFactory create() {
        return INSTANCE;
    }

    public static Clock uptimeClock() {
        Clock uptimeClock = TimeModule.uptimeClock();
        d.a(uptimeClock, "Cannot return null from a non-@Nullable @Provides method");
        return uptimeClock;
    }

    public Clock get() {
        return uptimeClock();
    }
}
