package com.google.android.datatransport.runtime.time;

import f.b.b;
import f.b.d;

/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public final class TimeModule_EventClockFactory implements b<Clock> {
    private static final TimeModule_EventClockFactory INSTANCE = new TimeModule_EventClockFactory();

    public static TimeModule_EventClockFactory create() {
        return INSTANCE;
    }

    public static Clock eventClock() {
        Clock eventClock = TimeModule.eventClock();
        d.a(eventClock, "Cannot return null from a non-@Nullable @Provides method");
        return eventClock;
    }

    public Clock get() {
        return eventClock();
    }
}
