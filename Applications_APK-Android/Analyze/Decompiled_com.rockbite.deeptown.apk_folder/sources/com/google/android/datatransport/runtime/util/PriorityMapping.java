package com.google.android.datatransport.runtime.util;

import android.util.SparseArray;
import com.google.android.datatransport.Priority;
import java.util.EnumMap;

/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public final class PriorityMapping {
    private static EnumMap<Priority, Integer> PRIORITY_INT_MAP = new EnumMap<>(Priority.class);
    private static SparseArray<Priority> PRIORITY_MAP = new SparseArray<>();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.EnumMap.put(com.google.android.datatransport.Priority, java.lang.Integer):V}
     arg types: [com.google.android.datatransport.Priority, int]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Object, java.lang.Object):java.lang.Object}
      ClspMth{java.util.AbstractMap.put(com.google.android.datatransport.Priority, java.lang.Integer):V}
      ClspMth{java.util.Map.put(com.google.android.datatransport.Priority, java.lang.Integer):V}
      ClspMth{java.util.EnumMap.put(com.google.android.datatransport.Priority, java.lang.Integer):V} */
    static {
        PRIORITY_INT_MAP.put(Priority.DEFAULT, (Integer) 0);
        PRIORITY_INT_MAP.put(Priority.VERY_LOW, (Integer) 1);
        PRIORITY_INT_MAP.put(Priority.HIGHEST, (Integer) 2);
        for (Priority next : PRIORITY_INT_MAP.keySet()) {
            PRIORITY_MAP.append(PRIORITY_INT_MAP.get(next).intValue(), next);
        }
    }

    public static int toInt(Priority priority) {
        Integer num = PRIORITY_INT_MAP.get(priority);
        if (num != null) {
            return num.intValue();
        }
        throw new IllegalStateException("PriorityMapping is missing known Priority value " + priority);
    }

    public static Priority valueOf(int i2) {
        Priority priority = PRIORITY_MAP.get(i2);
        if (priority != null) {
            return priority;
        }
        throw new IllegalArgumentException("Unknown Priority for value " + i2);
    }
}
