package android.support.transition;

import android.support.transition.Transition;

public class TransitionListenerAdapter implements Transition.TransitionListener {
    public void onTransitionCancel(Transition transition) {
    }

    public void onTransitionEnd(Transition transition) {
    }

    public void onTransitionPause(Transition transition) {
    }

    public void onTransitionResume(Transition transition) {
    }

    public void onTransitionStart(Transition transition) {
    }
}
