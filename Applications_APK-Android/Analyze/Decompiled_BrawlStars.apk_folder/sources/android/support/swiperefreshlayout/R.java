package android.support.swiperefreshlayout;

public final class R {
    private R() {
    }

    public static final class attr {
        public static final int alpha = 2130903079;
        public static final int font = 2130903283;
        public static final int fontProviderAuthority = 2130903286;
        public static final int fontProviderCerts = 2130903287;
        public static final int fontProviderFetchStrategy = 2130903288;
        public static final int fontProviderFetchTimeout = 2130903289;
        public static final int fontProviderPackage = 2130903290;
        public static final int fontProviderQuery = 2130903291;
        public static final int fontStyle = 2130903292;
        public static final int fontVariationSettings = 2130903293;
        public static final int fontWeight = 2130903294;
        public static final int ttcIndex = 2130903660;

        private attr() {
        }
    }

    public static final class color {
        public static final int notification_action_color_filter = 2131034417;
        public static final int notification_icon_bg_color = 2131034418;
        public static final int ripple_material_light = 2131034429;
        public static final int secondary_text_default_material_light = 2131034431;

        private color() {
        }
    }

    public static final class dimen {
        public static final int compat_button_inset_horizontal_material = 2131099766;
        public static final int compat_button_inset_vertical_material = 2131099767;
        public static final int compat_button_padding_horizontal_material = 2131099768;
        public static final int compat_button_padding_vertical_material = 2131099769;
        public static final int compat_control_corner_material = 2131099770;
        public static final int compat_notification_large_icon_max_height = 2131099771;
        public static final int compat_notification_large_icon_max_width = 2131099772;
        public static final int notification_action_icon_size = 2131099961;
        public static final int notification_action_text_size = 2131099962;
        public static final int notification_big_circle_margin = 2131099963;
        public static final int notification_content_margin_start = 2131099966;
        public static final int notification_large_icon_height = 2131099967;
        public static final int notification_large_icon_width = 2131099968;
        public static final int notification_main_column_padding_top = 2131099969;
        public static final int notification_media_narrow_margin = 2131099970;
        public static final int notification_right_icon_size = 2131099971;
        public static final int notification_right_side_padding_top = 2131099972;
        public static final int notification_small_icon_background_padding = 2131099973;
        public static final int notification_small_icon_size_as_large = 2131099974;
        public static final int notification_subtext_size = 2131099975;
        public static final int notification_top_pad = 2131099976;
        public static final int notification_top_pad_large_text = 2131099977;

        private dimen() {
        }
    }

    public static final class drawable {
        public static final int notification_action_background = 2131165465;
        public static final int notification_bg = 2131165467;
        public static final int notification_bg_low = 2131165468;
        public static final int notification_bg_low_normal = 2131165469;
        public static final int notification_bg_low_pressed = 2131165470;
        public static final int notification_bg_normal = 2131165471;
        public static final int notification_bg_normal_pressed = 2131165472;
        public static final int notification_icon_background = 2131165473;
        public static final int notification_template_icon_bg = 2131165474;
        public static final int notification_template_icon_low_bg = 2131165475;
        public static final int notification_tile_bg = 2131165476;
        public static final int notify_panel_notification_icon_bg = 2131165477;

        private drawable() {
        }
    }

    public static final class id {
        public static final int action_container = 2131230746;
        public static final int action_divider = 2131230748;
        public static final int action_image = 2131230749;
        public static final int action_text = 2131230755;
        public static final int actions = 2131230756;
        public static final int async = 2131230796;
        public static final int blocking = 2131230813;
        public static final int chronometer = 2131230859;
        public static final int forever = 2131230982;
        public static final int icon = 2131231067;
        public static final int icon_group = 2131231068;
        public static final int info = 2131231079;
        public static final int italic = 2131231107;
        public static final int line1 = 2131231115;
        public static final int line3 = 2131231116;
        public static final int normal = 2131231181;
        public static final int notification_background = 2131231182;
        public static final int notification_main_column = 2131231183;
        public static final int notification_main_column_container = 2131231184;
        public static final int right_icon = 2131231289;
        public static final int right_side = 2131231290;
        public static final int tag_transition_group = 2131231403;
        public static final int tag_unhandled_key_event_manager = 2131231404;
        public static final int tag_unhandled_key_listeners = 2131231405;
        public static final int text = 2131231407;
        public static final int text2 = 2131231408;
        public static final int time = 2131231418;
        public static final int title = 2131231420;

        private id() {
        }
    }

    public static final class integer {
        public static final int status_bar_notification_info_maxnum = 2131296283;

        private integer() {
        }
    }

    public static final class layout {
        public static final int notification_action = 2131427575;
        public static final int notification_action_tombstone = 2131427576;
        public static final int notification_template_custom_big = 2131427583;
        public static final int notification_template_icon_group = 2131427584;
        public static final int notification_template_part_chronometer = 2131427588;
        public static final int notification_template_part_time = 2131427589;

        private layout() {
        }
    }

    public static final class string {
        public static final int status_bar_notification_info_overflow = 2131689737;

        private string() {
        }
    }

    public static final class style {
        public static final int TextAppearance_Compat_Notification = 2131755366;
        public static final int TextAppearance_Compat_Notification_Info = 2131755367;
        public static final int TextAppearance_Compat_Notification_Line2 = 2131755369;
        public static final int TextAppearance_Compat_Notification_Time = 2131755372;
        public static final int TextAppearance_Compat_Notification_Title = 2131755374;
        public static final int Widget_Compat_NotificationActionContainer = 2131755541;
        public static final int Widget_Compat_NotificationActionText = 2131755542;

        private style() {
        }
    }

    public static final class styleable {
        public static final int[] ColorStateListItem = {16843173, 16843551, com.supercell.brawlstars.R.attr.alpha};
        public static final int ColorStateListItem_alpha = 2;
        public static final int ColorStateListItem_android_alpha = 1;
        public static final int ColorStateListItem_android_color = 0;
        public static final int[] FontFamily = {com.supercell.brawlstars.R.attr.fontProviderAuthority, com.supercell.brawlstars.R.attr.fontProviderCerts, com.supercell.brawlstars.R.attr.fontProviderFetchStrategy, com.supercell.brawlstars.R.attr.fontProviderFetchTimeout, com.supercell.brawlstars.R.attr.fontProviderPackage, com.supercell.brawlstars.R.attr.fontProviderQuery};
        public static final int[] FontFamilyFont = {16844082, 16844083, 16844095, 16844143, 16844144, com.supercell.brawlstars.R.attr.font, com.supercell.brawlstars.R.attr.fontStyle, com.supercell.brawlstars.R.attr.fontVariationSettings, com.supercell.brawlstars.R.attr.fontWeight, com.supercell.brawlstars.R.attr.ttcIndex};
        public static final int FontFamilyFont_android_font = 0;
        public static final int FontFamilyFont_android_fontStyle = 2;
        public static final int FontFamilyFont_android_fontVariationSettings = 4;
        public static final int FontFamilyFont_android_fontWeight = 1;
        public static final int FontFamilyFont_android_ttcIndex = 3;
        public static final int FontFamilyFont_font = 5;
        public static final int FontFamilyFont_fontStyle = 6;
        public static final int FontFamilyFont_fontVariationSettings = 7;
        public static final int FontFamilyFont_fontWeight = 8;
        public static final int FontFamilyFont_ttcIndex = 9;
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderCerts = 1;
        public static final int FontFamily_fontProviderFetchStrategy = 2;
        public static final int FontFamily_fontProviderFetchTimeout = 3;
        public static final int FontFamily_fontProviderPackage = 4;
        public static final int FontFamily_fontProviderQuery = 5;
        public static final int[] GradientColor = {16843165, 16843166, 16843169, 16843170, 16843171, 16843172, 16843265, 16843275, 16844048, 16844049, 16844050, 16844051};
        public static final int[] GradientColorItem = {16843173, 16844052};
        public static final int GradientColorItem_android_color = 0;
        public static final int GradientColorItem_android_offset = 1;
        public static final int GradientColor_android_centerColor = 7;
        public static final int GradientColor_android_centerX = 3;
        public static final int GradientColor_android_centerY = 4;
        public static final int GradientColor_android_endColor = 1;
        public static final int GradientColor_android_endX = 10;
        public static final int GradientColor_android_endY = 11;
        public static final int GradientColor_android_gradientRadius = 5;
        public static final int GradientColor_android_startColor = 0;
        public static final int GradientColor_android_startX = 8;
        public static final int GradientColor_android_startY = 9;
        public static final int GradientColor_android_tileMode = 6;
        public static final int GradientColor_android_type = 2;

        private styleable() {
        }
    }
}
