package android.support.v4.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import b.b.a.j.a0;
import com.facebook.internal.ServerProtocol;
import com.facebook.share.internal.ShareConstants;
import com.supercell.id.R;
import java.util.HashMap;
import kotlin.d.b.g;
import kotlin.d.b.j;

public class RtlViewPager extends ViewPager {

    /* renamed from: a  reason: collision with root package name */
    public int f3a;

    /* renamed from: b  reason: collision with root package name */
    public int f4b;
    public final HashMap<ViewPager.OnPageChangeListener, b> c;

    public final class b implements ViewPager.OnPageChangeListener {

        /* renamed from: a  reason: collision with root package name */
        public final ViewPager.OnPageChangeListener f6a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ RtlViewPager f7b;

        public b(RtlViewPager rtlViewPager, ViewPager.OnPageChangeListener onPageChangeListener) {
            j.b(onPageChangeListener, "mListener");
            this.f7b = rtlViewPager;
            this.f6a = onPageChangeListener;
        }

        public final void onPageScrollStateChanged(int i) {
            this.f6a.onPageScrollStateChanged(i);
        }

        public final void onPageScrolled(int i, float f, int i2) {
            int width = this.f7b.getWidth();
            PagerAdapter a2 = RtlViewPager.super.getAdapter();
            if (this.f7b.a() && a2 != null) {
                int count = a2.getCount();
                float f2 = (float) width;
                int pageWidth = ((int) ((1.0f - a2.getPageWidth(i)) * f2)) + i2;
                while (i < count && pageWidth > 0) {
                    i++;
                    pageWidth -= (int) (a2.getPageWidth(i) * f2);
                }
                i = (count - i) - 1;
                i2 = -pageWidth;
                f = ((float) i2) / (a2.getPageWidth(i) * f2);
            }
            this.f6a.onPageScrolled(i, f, i2);
        }

        public final void onPageSelected(int i) {
            PagerAdapter a2 = RtlViewPager.super.getAdapter();
            if (this.f7b.a() && a2 != null) {
                i = (a2.getCount() - i) - 1;
            }
            this.f6a.onPageSelected(i);
        }
    }

    public static final class c implements a0 {
        public static final Parcelable.ClassLoaderCreator<c> CREATOR = new a();

        /* renamed from: a  reason: collision with root package name */
        public final Parcelable f8a;

        /* renamed from: b  reason: collision with root package name */
        public final int f9b;

        public final class a implements Parcelable.ClassLoaderCreator<c> {
            /* JADX WARN: Type inference failed for: r2v1, types: [android.support.v4.view.RtlViewPager$c, java.lang.Object] */
            public final c createFromParcel(Parcel parcel) {
                j.b(parcel, ShareConstants.FEED_SOURCE_PARAM);
                ClassLoader classLoader = c.class.getClassLoader();
                if (classLoader == null) {
                    j.a();
                }
                return createFromParcel(parcel, classLoader);
            }

            public final c createFromParcel(Parcel parcel, ClassLoader classLoader) {
                j.b(parcel, ShareConstants.FEED_SOURCE_PARAM);
                j.b(classLoader, "loader");
                return new c(parcel, classLoader, null);
            }

            public final c[] newArray(int i) {
                return new c[i];
            }
        }

        public /* synthetic */ c(Parcel parcel, ClassLoader classLoader, g gVar) {
            this.f8a = parcel.readParcelable(classLoader == null ? c.class.getClassLoader() : classLoader);
            this.f9b = parcel.readInt();
        }

        public c(Parcelable parcelable, int i) {
            j.b(parcelable, "viewPagerSavedState");
            this.f8a = parcelable;
            this.f9b = i;
        }

        public final int describeContents() {
            return 0;
        }

        public final void writeToParcel(Parcel parcel, int i) {
            j.b(parcel, "dest");
            parcel.writeParcelable(this.f8a, i);
            parcel.writeInt(this.f9b);
        }
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public RtlViewPager(Context context) {
        this(context, null);
        j.b(context, "context");
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RtlViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        j.b(context, "context");
        this.f3a = 400;
        this.c = new HashMap<>();
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.RtlViewPager, 0, 0);
        try {
            this.f3a = obtainStyledAttributes.getInteger(R.styleable.RtlViewPager_transitionDuration, 400);
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    public final boolean a() {
        return this.f4b == 1;
    }

    public void addOnPageChangeListener(ViewPager.OnPageChangeListener onPageChangeListener) {
        j.b(onPageChangeListener, "listener");
        b bVar = new b(this, onPageChangeListener);
        this.c.put(onPageChangeListener, bVar);
        super.addOnPageChangeListener(bVar);
    }

    public void clearOnPageChangeListeners() {
        super.clearOnPageChangeListeners();
        this.c.clear();
    }

    public PagerAdapter getAdapter() {
        PagerAdapter adapter = super.getAdapter();
        if (!(adapter instanceof a)) {
            adapter = null;
        }
        a aVar = (a) adapter;
        if (aVar != null) {
            return aVar.f1a;
        }
        return null;
    }

    public int getCurrentItem() {
        int currentItem = super.getCurrentItem();
        PagerAdapter adapter = super.getAdapter();
        return (adapter == null || !a()) ? currentItem : (adapter.getCount() - currentItem) - 1;
    }

    public final int getTransitionDuration() {
        return this.f3a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public void onMeasure(int i, int i2) {
        if (View.MeasureSpec.getMode(i2) == 0) {
            int childCount = getChildCount();
            int i3 = 0;
            for (int i4 = 0; i4 < childCount; i4++) {
                View childAt = getChildAt(i4);
                childAt.measure(i, View.MeasureSpec.makeMeasureSpec(0, 0));
                j.a((Object) childAt, "child");
                int measuredHeight = childAt.getMeasuredHeight();
                if (measuredHeight > i3) {
                    i3 = measuredHeight;
                }
            }
            i2 = View.MeasureSpec.makeMeasureSpec(i3, 1073741824);
        }
        super.onMeasure(i, i2);
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        j.b(parcelable, ServerProtocol.DIALOG_PARAM_STATE);
        if (!(parcelable instanceof c)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        c cVar = (c) parcelable;
        this.f4b = cVar.f9b;
        super.onRestoreInstanceState(cVar.f8a);
    }

    public void onRtlPropertiesChanged(int i) {
        super.onRtlPropertiesChanged(i);
        int i2 = 0;
        int i3 = 1;
        if (i != 1) {
            i3 = 0;
        }
        if (i3 != this.f4b) {
            PagerAdapter adapter = super.getAdapter();
            if (adapter != null) {
                i2 = getCurrentItem();
            }
            this.f4b = i3;
            if (adapter != null) {
                adapter.notifyDataSetChanged();
                setCurrentItem(i2);
            }
        }
    }

    public Parcelable onSaveInstanceState() {
        Parcelable onSaveInstanceState = super.onSaveInstanceState();
        if (onSaveInstanceState == null) {
            j.a();
        }
        return new c(onSaveInstanceState, this.f4b);
    }

    public void removeOnPageChangeListener(ViewPager.OnPageChangeListener onPageChangeListener) {
        j.b(onPageChangeListener, "listener");
        b remove = this.c.remove(onPageChangeListener);
        if (remove != null) {
            super.removeOnPageChangeListener(remove);
        }
    }

    public void setAdapter(PagerAdapter pagerAdapter) {
        super.setAdapter(pagerAdapter != null ? new a(this, pagerAdapter) : null);
        setCurrentItem(0);
    }

    public void setCurrentItem(int i) {
        PagerAdapter adapter = super.getAdapter();
        if (adapter != null && a()) {
            i = (adapter.getCount() - i) - 1;
        }
        super.setCurrentItem(i);
    }

    public void setCurrentItem(int i, boolean z) {
        PagerAdapter adapter = super.getAdapter();
        if (adapter != null && a()) {
            i = (adapter.getCount() - i) - 1;
        }
        super.setCurrentItem(i, z);
    }

    public void setOnPageChangeListener(ViewPager.OnPageChangeListener onPageChangeListener) {
        j.b(onPageChangeListener, "listener");
        super.setOnPageChangeListener(new b(this, onPageChangeListener));
    }

    public final void setTransitionDuration(int i) {
        this.f3a = i;
    }

    public void smoothScrollTo(int i, int i2, int i3) {
        if (Math.abs(i3) <= 0) {
            float measuredWidth = (float) (((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight()) / 2);
            i3 = kotlin.e.a.a((((((float) Math.sin(0.2356194704771042d)) * measuredWidth) + measuredWidth) * 4000.0f) / ((float) this.f3a));
        }
        super.smoothScrollTo(i, i2, i3);
    }

    public final class a extends a.a.a.a.a {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ RtlViewPager f5b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(RtlViewPager rtlViewPager, PagerAdapter pagerAdapter) {
            super(pagerAdapter);
            j.b(pagerAdapter, "adapter");
            this.f5b = rtlViewPager;
        }

        public final void destroyItem(View view, int i, Object obj) {
            j.b(view, "container");
            j.b(obj, "object");
            if (this.f5b.a()) {
                i = (this.f1a.getCount() - i) - 1;
            }
            super.destroyItem(view, i, obj);
        }

        public final int getItemPosition(Object obj) {
            j.b(obj, "object");
            int itemPosition = super.getItemPosition(obj);
            if (!this.f5b.a()) {
                return itemPosition;
            }
            if (itemPosition == -1 || itemPosition == -2) {
                return -2;
            }
            return (this.f1a.getCount() - itemPosition) - 1;
        }

        public final CharSequence getPageTitle(int i) {
            if (this.f5b.a()) {
                i = (this.f1a.getCount() - i) - 1;
            }
            return this.f1a.getPageTitle(i);
        }

        public final float getPageWidth(int i) {
            if (this.f5b.a()) {
                i = (this.f1a.getCount() - i) - 1;
            }
            return this.f1a.getPageWidth(i);
        }

        public final Object instantiateItem(View view, int i) {
            j.b(view, "container");
            if (this.f5b.a()) {
                i = (this.f1a.getCount() - i) - 1;
            }
            return super.instantiateItem(view, i);
        }

        public final void setPrimaryItem(View view, int i, Object obj) {
            j.b(view, "container");
            j.b(obj, "object");
            if (this.f5b.a()) {
                i = (this.f1a.getCount() - i) - 1;
            }
            super.setPrimaryItem(view, i, obj);
        }

        public final void destroyItem(ViewGroup viewGroup, int i, Object obj) {
            j.b(viewGroup, "container");
            j.b(obj, "object");
            if (this.f5b.a()) {
                i = (this.f1a.getCount() - i) - 1;
            }
            super.destroyItem(viewGroup, i, obj);
        }

        public final Object instantiateItem(ViewGroup viewGroup, int i) {
            j.b(viewGroup, "container");
            if (this.f5b.a()) {
                i = (this.f1a.getCount() - i) - 1;
            }
            return super.instantiateItem(viewGroup, i);
        }

        public final void setPrimaryItem(ViewGroup viewGroup, int i, Object obj) {
            j.b(viewGroup, "container");
            j.b(obj, "object");
            if (this.f5b.a()) {
                i = (this.f1a.getCount() - i) - 1;
            }
            super.setPrimaryItem(viewGroup, i, obj);
        }
    }
}
