package android.support.v4.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import com.supercell.id.R;
import kotlin.d.b.g;
import kotlin.d.b.j;

public final class FlowPager extends RtlViewPager {
    public boolean d;

    public static final class a implements ViewPager.PageTransformer {
        public final void transformPage(View view, float f) {
            j.b(view, "view");
            if (f > -1.0f && f < 1.0f) {
                view.setAlpha(1.0f);
                view.setTranslationX(((float) view.getWidth()) * (-f));
                int i = 1;
                if (ViewCompat.getLayoutDirection(view) == 1) {
                    i = -1;
                }
                view.setTranslationY(((float) i) * f * ((float) view.getHeight()));
                return;
            }
            view.setAlpha(0.0f);
        }
    }

    public FlowPager(Context context) {
        this(context, null, 0, 0, 14, null);
    }

    public FlowPager(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 0, 12, null);
    }

    public FlowPager(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0, 8, null);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlowPager(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet);
        j.b(context, "context");
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.FlowPhaseIndicator, i, i2);
        try {
            setVertical(obtainStyledAttributes.getInteger(R.styleable.FlowPager_android_orientation, 0) == 1);
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ FlowPager(Context context, AttributeSet attributeSet, int i, int i2, int i3, g gVar) {
        this(context, (i3 & 2) != 0 ? null : attributeSet, (i3 & 4) != 0 ? 0 : i, (i3 & 8) != 0 ? 0 : i2);
    }

    public final boolean canScrollHorizontally(int i) {
        if (this.d) {
            return false;
        }
        return super.canScrollHorizontally(i);
    }

    public final boolean canScrollVertically(int i) {
        return this.d ? super.canScrollHorizontally(i) : super.canScrollVertically(i);
    }

    public final boolean executeKeyEvent(KeyEvent keyEvent) {
        j.b(keyEvent, NotificationCompat.CATEGORY_EVENT);
        return false;
    }

    public final boolean getVertical() {
        return this.d;
    }

    public final boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return false;
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        return false;
    }

    public final void setVertical(boolean z) {
        if (this.d != z) {
            this.d = z;
            if (z) {
                setPageTransformer(true, new a());
                setOverScrollMode(2);
                return;
            }
            setPageTransformer(false, null);
            setOverScrollMode(0);
        }
    }
}
