package android.support.v7.cardview;

public final class R {
    private R() {
    }

    public static final class attr {
        public static final int cardBackgroundColor = 2130903133;
        public static final int cardCornerRadius = 2130903134;
        public static final int cardElevation = 2130903135;
        public static final int cardMaxElevation = 2130903136;
        public static final int cardPreventCornerOverlap = 2130903137;
        public static final int cardUseCompatPadding = 2130903138;
        public static final int cardViewStyle = 2130903139;
        public static final int contentPadding = 2130903214;
        public static final int contentPaddingBottom = 2130903215;
        public static final int contentPaddingLeft = 2130903216;
        public static final int contentPaddingRight = 2130903217;
        public static final int contentPaddingTop = 2130903218;

        private attr() {
        }
    }

    public static final class color {
        public static final int cardview_dark_background = 2131034164;
        public static final int cardview_light_background = 2131034165;
        public static final int cardview_shadow_end_color = 2131034166;
        public static final int cardview_shadow_start_color = 2131034167;

        private color() {
        }
    }

    public static final class dimen {
        public static final int cardview_compat_inset_shadow = 2131099744;
        public static final int cardview_default_elevation = 2131099745;
        public static final int cardview_default_radius = 2131099746;

        private dimen() {
        }
    }

    public static final class style {
        public static final int Base_CardView = 2131755019;
        public static final int CardView = 2131755202;
        public static final int CardView_Dark = 2131755203;
        public static final int CardView_Light = 2131755204;

        private style() {
        }
    }

    public static final class styleable {
        public static final int[] CardView = {16843071, 16843072, com.supercell.brawlstars.R.attr.cardBackgroundColor, com.supercell.brawlstars.R.attr.cardCornerRadius, com.supercell.brawlstars.R.attr.cardElevation, com.supercell.brawlstars.R.attr.cardMaxElevation, com.supercell.brawlstars.R.attr.cardPreventCornerOverlap, com.supercell.brawlstars.R.attr.cardUseCompatPadding, com.supercell.brawlstars.R.attr.contentPadding, com.supercell.brawlstars.R.attr.contentPaddingBottom, com.supercell.brawlstars.R.attr.contentPaddingLeft, com.supercell.brawlstars.R.attr.contentPaddingRight, com.supercell.brawlstars.R.attr.contentPaddingTop};
        public static final int CardView_android_minHeight = 1;
        public static final int CardView_android_minWidth = 0;
        public static final int CardView_cardBackgroundColor = 2;
        public static final int CardView_cardCornerRadius = 3;
        public static final int CardView_cardElevation = 4;
        public static final int CardView_cardMaxElevation = 5;
        public static final int CardView_cardPreventCornerOverlap = 6;
        public static final int CardView_cardUseCompatPadding = 7;
        public static final int CardView_contentPadding = 8;
        public static final int CardView_contentPaddingBottom = 9;
        public static final int CardView_contentPaddingLeft = 10;
        public static final int CardView_contentPaddingRight = 11;
        public static final int CardView_contentPaddingTop = 12;

        private styleable() {
        }
    }
}
