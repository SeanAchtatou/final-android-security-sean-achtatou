package android.support.constraint.solver.widgets;

import android.support.constraint.solver.LinearSystem;

class Chain {
    private static final boolean DEBUG = false;

    Chain() {
    }

    static void applyChainConstraints(ConstraintWidgetContainer constraintWidgetContainer, LinearSystem linearSystem, int i) {
        int i2;
        ChainHead[] chainHeadArr;
        int i3;
        if (i == 0) {
            int i4 = constraintWidgetContainer.mHorizontalChainsSize;
            chainHeadArr = constraintWidgetContainer.mHorizontalChainsArray;
            i2 = i4;
            i3 = 0;
        } else {
            i3 = 2;
            int i5 = constraintWidgetContainer.mVerticalChainsSize;
            i2 = i5;
            chainHeadArr = constraintWidgetContainer.mVerticalChainsArray;
        }
        for (int i6 = 0; i6 < i2; i6++) {
            ChainHead chainHead = chainHeadArr[i6];
            chainHead.define();
            if (!constraintWidgetContainer.optimizeFor(4)) {
                applyChainConstraints(constraintWidgetContainer, linearSystem, i, i3, chainHead);
            } else if (!Optimizer.applyChainOptimized(constraintWidgetContainer, linearSystem, i, i3, chainHead)) {
                applyChainConstraints(constraintWidgetContainer, linearSystem, i, i3, chainHead);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0031, code lost:
        if (r2.mHorizontalChainStyle == 2) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0044, code lost:
        if (r2.mVerticalChainStyle == 2) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0048, code lost:
        r5 = false;
     */
    /* JADX WARNING: Removed duplicated region for block: B:190:0x038b  */
    /* JADX WARNING: Removed duplicated region for block: B:203:0x03ad  */
    /* JADX WARNING: Removed duplicated region for block: B:248:0x0477  */
    /* JADX WARNING: Removed duplicated region for block: B:253:0x04ac  */
    /* JADX WARNING: Removed duplicated region for block: B:262:0x04d1  */
    /* JADX WARNING: Removed duplicated region for block: B:263:0x04d6  */
    /* JADX WARNING: Removed duplicated region for block: B:266:0x04dc  */
    /* JADX WARNING: Removed duplicated region for block: B:267:0x04e1  */
    /* JADX WARNING: Removed duplicated region for block: B:269:0x04e5  */
    /* JADX WARNING: Removed duplicated region for block: B:275:0x04f7  */
    /* JADX WARNING: Removed duplicated region for block: B:290:0x038c A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0157  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0192  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void applyChainConstraints(android.support.constraint.solver.widgets.ConstraintWidgetContainer r35, android.support.constraint.solver.LinearSystem r36, int r37, int r38, android.support.constraint.solver.widgets.ChainHead r39) {
        /*
            r0 = r35
            r9 = r36
            r1 = r39
            android.support.constraint.solver.widgets.ConstraintWidget r10 = r1.mFirst
            android.support.constraint.solver.widgets.ConstraintWidget r11 = r1.mLast
            android.support.constraint.solver.widgets.ConstraintWidget r12 = r1.mFirstVisibleWidget
            android.support.constraint.solver.widgets.ConstraintWidget r13 = r1.mLastVisibleWidget
            android.support.constraint.solver.widgets.ConstraintWidget r2 = r1.mHead
            float r3 = r1.mTotalWeight
            android.support.constraint.solver.widgets.ConstraintWidget$DimensionBehaviour[] r4 = r0.mListDimensionBehaviors
            r4 = r4[r37]
            android.support.constraint.solver.widgets.ConstraintWidget$DimensionBehaviour r5 = android.support.constraint.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT
            r7 = 1
            if (r4 != r5) goto L_0x001d
            r4 = 1
            goto L_0x001e
        L_0x001d:
            r4 = 0
        L_0x001e:
            r5 = 2
            if (r37 != 0) goto L_0x0034
            int r8 = r2.mHorizontalChainStyle
            if (r8 != 0) goto L_0x0027
            r8 = 1
            goto L_0x0028
        L_0x0027:
            r8 = 0
        L_0x0028:
            int r14 = r2.mHorizontalChainStyle
            if (r14 != r7) goto L_0x002e
            r14 = 1
            goto L_0x002f
        L_0x002e:
            r14 = 0
        L_0x002f:
            int r15 = r2.mHorizontalChainStyle
            if (r15 != r5) goto L_0x0048
            goto L_0x0046
        L_0x0034:
            int r8 = r2.mVerticalChainStyle
            if (r8 != 0) goto L_0x003a
            r8 = 1
            goto L_0x003b
        L_0x003a:
            r8 = 0
        L_0x003b:
            int r14 = r2.mVerticalChainStyle
            if (r14 != r7) goto L_0x0041
            r14 = 1
            goto L_0x0042
        L_0x0041:
            r14 = 0
        L_0x0042:
            int r15 = r2.mVerticalChainStyle
            if (r15 != r5) goto L_0x0048
        L_0x0046:
            r5 = 1
            goto L_0x0049
        L_0x0048:
            r5 = 0
        L_0x0049:
            r15 = r8
            r8 = r10
            r16 = r14
            r14 = r5
            r5 = 0
        L_0x004f:
            r21 = 0
            if (r5 != 0) goto L_0x012a
            android.support.constraint.solver.widgets.ConstraintAnchor[] r7 = r8.mListAnchors
            r7 = r7[r38]
            if (r4 != 0) goto L_0x005f
            if (r14 == 0) goto L_0x005c
            goto L_0x005f
        L_0x005c:
            r23 = 4
            goto L_0x0061
        L_0x005f:
            r23 = 1
        L_0x0061:
            int r24 = r7.getMargin()
            android.support.constraint.solver.widgets.ConstraintAnchor r6 = r7.mTarget
            if (r6 == 0) goto L_0x0073
            if (r8 == r10) goto L_0x0073
            android.support.constraint.solver.widgets.ConstraintAnchor r6 = r7.mTarget
            int r6 = r6.getMargin()
            int r24 = r24 + r6
        L_0x0073:
            r6 = r24
            if (r14 == 0) goto L_0x0081
            if (r8 == r10) goto L_0x0081
            if (r8 == r12) goto L_0x0081
            r24 = r3
            r23 = r5
            r3 = 6
            goto L_0x0091
        L_0x0081:
            if (r15 == 0) goto L_0x008b
            if (r4 == 0) goto L_0x008b
            r24 = r3
            r23 = r5
            r3 = 4
            goto L_0x0091
        L_0x008b:
            r24 = r3
            r3 = r23
            r23 = r5
        L_0x0091:
            android.support.constraint.solver.widgets.ConstraintAnchor r5 = r7.mTarget
            if (r5 == 0) goto L_0x00be
            if (r8 != r12) goto L_0x00a6
            android.support.constraint.solver.SolverVariable r5 = r7.mSolverVariable
            r25 = r15
            android.support.constraint.solver.widgets.ConstraintAnchor r15 = r7.mTarget
            android.support.constraint.solver.SolverVariable r15 = r15.mSolverVariable
            r26 = r2
            r2 = 5
            r9.addGreaterThan(r5, r15, r6, r2)
            goto L_0x00b4
        L_0x00a6:
            r26 = r2
            r25 = r15
            android.support.constraint.solver.SolverVariable r2 = r7.mSolverVariable
            android.support.constraint.solver.widgets.ConstraintAnchor r5 = r7.mTarget
            android.support.constraint.solver.SolverVariable r5 = r5.mSolverVariable
            r15 = 6
            r9.addGreaterThan(r2, r5, r6, r15)
        L_0x00b4:
            android.support.constraint.solver.SolverVariable r2 = r7.mSolverVariable
            android.support.constraint.solver.widgets.ConstraintAnchor r5 = r7.mTarget
            android.support.constraint.solver.SolverVariable r5 = r5.mSolverVariable
            r9.addEquality(r2, r5, r6, r3)
            goto L_0x00c2
        L_0x00be:
            r26 = r2
            r25 = r15
        L_0x00c2:
            if (r4 == 0) goto L_0x00f9
            int r2 = r8.getVisibility()
            r3 = 8
            if (r2 == r3) goto L_0x00e8
            android.support.constraint.solver.widgets.ConstraintWidget$DimensionBehaviour[] r2 = r8.mListDimensionBehaviors
            r2 = r2[r37]
            android.support.constraint.solver.widgets.ConstraintWidget$DimensionBehaviour r3 = android.support.constraint.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT
            if (r2 != r3) goto L_0x00e8
            android.support.constraint.solver.widgets.ConstraintAnchor[] r2 = r8.mListAnchors
            int r3 = r38 + 1
            r2 = r2[r3]
            android.support.constraint.solver.SolverVariable r2 = r2.mSolverVariable
            android.support.constraint.solver.widgets.ConstraintAnchor[] r3 = r8.mListAnchors
            r3 = r3[r38]
            android.support.constraint.solver.SolverVariable r3 = r3.mSolverVariable
            r5 = 5
            r6 = 0
            r9.addGreaterThan(r2, r3, r6, r5)
            goto L_0x00e9
        L_0x00e8:
            r6 = 0
        L_0x00e9:
            android.support.constraint.solver.widgets.ConstraintAnchor[] r2 = r8.mListAnchors
            r2 = r2[r38]
            android.support.constraint.solver.SolverVariable r2 = r2.mSolverVariable
            android.support.constraint.solver.widgets.ConstraintAnchor[] r3 = r0.mListAnchors
            r3 = r3[r38]
            android.support.constraint.solver.SolverVariable r3 = r3.mSolverVariable
            r5 = 6
            r9.addGreaterThan(r2, r3, r6, r5)
        L_0x00f9:
            android.support.constraint.solver.widgets.ConstraintAnchor[] r2 = r8.mListAnchors
            int r3 = r38 + 1
            r2 = r2[r3]
            android.support.constraint.solver.widgets.ConstraintAnchor r2 = r2.mTarget
            if (r2 == 0) goto L_0x011a
            android.support.constraint.solver.widgets.ConstraintWidget r2 = r2.mOwner
            android.support.constraint.solver.widgets.ConstraintAnchor[] r3 = r2.mListAnchors
            r3 = r3[r38]
            android.support.constraint.solver.widgets.ConstraintAnchor r3 = r3.mTarget
            if (r3 == 0) goto L_0x011a
            android.support.constraint.solver.widgets.ConstraintAnchor[] r3 = r2.mListAnchors
            r3 = r3[r38]
            android.support.constraint.solver.widgets.ConstraintAnchor r3 = r3.mTarget
            android.support.constraint.solver.widgets.ConstraintWidget r3 = r3.mOwner
            if (r3 == r8) goto L_0x0118
            goto L_0x011a
        L_0x0118:
            r21 = r2
        L_0x011a:
            if (r21 == 0) goto L_0x0121
            r8 = r21
            r5 = r23
            goto L_0x0122
        L_0x0121:
            r5 = 1
        L_0x0122:
            r3 = r24
            r15 = r25
            r2 = r26
            goto L_0x004f
        L_0x012a:
            r26 = r2
            r24 = r3
            r25 = r15
            if (r13 == 0) goto L_0x0154
            android.support.constraint.solver.widgets.ConstraintAnchor[] r2 = r11.mListAnchors
            int r3 = r38 + 1
            r2 = r2[r3]
            android.support.constraint.solver.widgets.ConstraintAnchor r2 = r2.mTarget
            if (r2 == 0) goto L_0x0154
            android.support.constraint.solver.widgets.ConstraintAnchor[] r2 = r13.mListAnchors
            r2 = r2[r3]
            android.support.constraint.solver.SolverVariable r5 = r2.mSolverVariable
            android.support.constraint.solver.widgets.ConstraintAnchor[] r6 = r11.mListAnchors
            r3 = r6[r3]
            android.support.constraint.solver.widgets.ConstraintAnchor r3 = r3.mTarget
            android.support.constraint.solver.SolverVariable r3 = r3.mSolverVariable
            int r2 = r2.getMargin()
            int r2 = -r2
            r6 = 5
            r9.addLowerThan(r5, r3, r2, r6)
            goto L_0x0155
        L_0x0154:
            r6 = 5
        L_0x0155:
            if (r4 == 0) goto L_0x0171
            android.support.constraint.solver.widgets.ConstraintAnchor[] r0 = r0.mListAnchors
            int r2 = r38 + 1
            r0 = r0[r2]
            android.support.constraint.solver.SolverVariable r0 = r0.mSolverVariable
            android.support.constraint.solver.widgets.ConstraintAnchor[] r3 = r11.mListAnchors
            r3 = r3[r2]
            android.support.constraint.solver.SolverVariable r3 = r3.mSolverVariable
            android.support.constraint.solver.widgets.ConstraintAnchor[] r4 = r11.mListAnchors
            r2 = r4[r2]
            int r2 = r2.getMargin()
            r4 = 6
            r9.addGreaterThan(r0, r3, r2, r4)
        L_0x0171:
            java.util.ArrayList<android.support.constraint.solver.widgets.ConstraintWidget> r0 = r1.mWeightedMatchConstraintsWidgets
            if (r0 == 0) goto L_0x0229
            int r2 = r0.size()
            r3 = 1
            if (r2 <= r3) goto L_0x0229
            boolean r4 = r1.mHasUndefinedWeights
            if (r4 == 0) goto L_0x0188
            boolean r4 = r1.mHasComplexMatchWeights
            if (r4 != 0) goto L_0x0188
            int r4 = r1.mWidgetsMatchCount
            float r4 = (float) r4
            goto L_0x018a
        L_0x0188:
            r4 = r24
        L_0x018a:
            r5 = 0
            r8 = r21
            r7 = 0
            r28 = 0
        L_0x0190:
            if (r7 >= r2) goto L_0x0229
            java.lang.Object r15 = r0.get(r7)
            android.support.constraint.solver.widgets.ConstraintWidget r15 = (android.support.constraint.solver.widgets.ConstraintWidget) r15
            float[] r3 = r15.mWeight
            r3 = r3[r37]
            int r23 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r23 >= 0) goto L_0x01be
            boolean r3 = r1.mHasComplexMatchWeights
            if (r3 == 0) goto L_0x01ba
            android.support.constraint.solver.widgets.ConstraintAnchor[] r3 = r15.mListAnchors
            int r23 = r38 + 1
            r3 = r3[r23]
            android.support.constraint.solver.SolverVariable r3 = r3.mSolverVariable
            android.support.constraint.solver.widgets.ConstraintAnchor[] r15 = r15.mListAnchors
            r15 = r15[r38]
            android.support.constraint.solver.SolverVariable r15 = r15.mSolverVariable
            r5 = 4
            r6 = 0
            r9.addEquality(r3, r15, r6, r5)
            r5 = 0
            r6 = 6
            goto L_0x01d7
        L_0x01ba:
            r5 = 4
            r3 = 1065353216(0x3f800000, float:1.0)
            goto L_0x01bf
        L_0x01be:
            r5 = 4
        L_0x01bf:
            r6 = 0
            int r20 = (r3 > r6 ? 1 : (r3 == r6 ? 0 : -1))
            if (r20 != 0) goto L_0x01dc
            android.support.constraint.solver.widgets.ConstraintAnchor[] r3 = r15.mListAnchors
            int r20 = r38 + 1
            r3 = r3[r20]
            android.support.constraint.solver.SolverVariable r3 = r3.mSolverVariable
            android.support.constraint.solver.widgets.ConstraintAnchor[] r15 = r15.mListAnchors
            r15 = r15[r38]
            android.support.constraint.solver.SolverVariable r15 = r15.mSolverVariable
            r5 = 0
            r6 = 6
            r9.addEquality(r3, r15, r5, r6)
        L_0x01d7:
            r24 = r0
            r22 = r2
            goto L_0x021e
        L_0x01dc:
            r5 = 0
            r6 = 6
            if (r8 == 0) goto L_0x0217
            android.support.constraint.solver.widgets.ConstraintAnchor[] r5 = r8.mListAnchors
            r5 = r5[r38]
            android.support.constraint.solver.SolverVariable r5 = r5.mSolverVariable
            android.support.constraint.solver.widgets.ConstraintAnchor[] r8 = r8.mListAnchors
            int r22 = r38 + 1
            r8 = r8[r22]
            android.support.constraint.solver.SolverVariable r8 = r8.mSolverVariable
            android.support.constraint.solver.widgets.ConstraintAnchor[] r6 = r15.mListAnchors
            r6 = r6[r38]
            android.support.constraint.solver.SolverVariable r6 = r6.mSolverVariable
            r24 = r0
            android.support.constraint.solver.widgets.ConstraintAnchor[] r0 = r15.mListAnchors
            r0 = r0[r22]
            android.support.constraint.solver.SolverVariable r0 = r0.mSolverVariable
            r22 = r2
            android.support.constraint.solver.ArrayRow r2 = r36.createRow()
            r27 = r2
            r29 = r4
            r30 = r3
            r31 = r5
            r32 = r8
            r33 = r6
            r34 = r0
            r27.createRowEqualMatchDimensions(r28, r29, r30, r31, r32, r33, r34)
            r9.addConstraint(r2)
            goto L_0x021b
        L_0x0217:
            r24 = r0
            r22 = r2
        L_0x021b:
            r28 = r3
            r8 = r15
        L_0x021e:
            int r7 = r7 + 1
            r2 = r22
            r0 = r24
            r3 = 1
            r5 = 0
            r6 = 5
            goto L_0x0190
        L_0x0229:
            if (r12 == 0) goto L_0x0293
            if (r12 == r13) goto L_0x022f
            if (r14 == 0) goto L_0x0293
        L_0x022f:
            android.support.constraint.solver.widgets.ConstraintAnchor[] r0 = r10.mListAnchors
            r0 = r0[r38]
            android.support.constraint.solver.widgets.ConstraintAnchor[] r1 = r11.mListAnchors
            int r2 = r38 + 1
            r1 = r1[r2]
            android.support.constraint.solver.widgets.ConstraintAnchor[] r3 = r10.mListAnchors
            r3 = r3[r38]
            android.support.constraint.solver.widgets.ConstraintAnchor r3 = r3.mTarget
            if (r3 == 0) goto L_0x024a
            android.support.constraint.solver.widgets.ConstraintAnchor[] r3 = r10.mListAnchors
            r3 = r3[r38]
            android.support.constraint.solver.widgets.ConstraintAnchor r3 = r3.mTarget
            android.support.constraint.solver.SolverVariable r3 = r3.mSolverVariable
            goto L_0x024c
        L_0x024a:
            r3 = r21
        L_0x024c:
            android.support.constraint.solver.widgets.ConstraintAnchor[] r4 = r11.mListAnchors
            r4 = r4[r2]
            android.support.constraint.solver.widgets.ConstraintAnchor r4 = r4.mTarget
            if (r4 == 0) goto L_0x025e
            android.support.constraint.solver.widgets.ConstraintAnchor[] r4 = r11.mListAnchors
            r4 = r4[r2]
            android.support.constraint.solver.widgets.ConstraintAnchor r4 = r4.mTarget
            android.support.constraint.solver.SolverVariable r4 = r4.mSolverVariable
            r5 = r4
            goto L_0x0260
        L_0x025e:
            r5 = r21
        L_0x0260:
            if (r12 != r13) goto L_0x026a
            android.support.constraint.solver.widgets.ConstraintAnchor[] r0 = r12.mListAnchors
            r0 = r0[r38]
            android.support.constraint.solver.widgets.ConstraintAnchor[] r1 = r12.mListAnchors
            r1 = r1[r2]
        L_0x026a:
            if (r3 == 0) goto L_0x04bd
            if (r5 == 0) goto L_0x04bd
            if (r37 != 0) goto L_0x0275
            r2 = r26
            float r2 = r2.mHorizontalBiasPercent
            goto L_0x0279
        L_0x0275:
            r2 = r26
            float r2 = r2.mVerticalBiasPercent
        L_0x0279:
            r4 = r2
            int r6 = r0.getMargin()
            int r7 = r1.getMargin()
            android.support.constraint.solver.SolverVariable r2 = r0.mSolverVariable
            android.support.constraint.solver.SolverVariable r8 = r1.mSolverVariable
            r10 = 5
            r0 = r36
            r1 = r2
            r2 = r3
            r3 = r6
            r6 = r8
            r8 = r10
            r0.addCentering(r1, r2, r3, r4, r5, r6, r7, r8)
            goto L_0x04bd
        L_0x0293:
            if (r25 == 0) goto L_0x0390
            if (r12 == 0) goto L_0x0390
            int r0 = r1.mWidgetsMatchCount
            if (r0 <= 0) goto L_0x02a4
            int r0 = r1.mWidgetsCount
            int r1 = r1.mWidgetsMatchCount
            if (r0 != r1) goto L_0x02a4
            r17 = 1
            goto L_0x02a6
        L_0x02a4:
            r17 = 0
        L_0x02a6:
            r14 = r12
            r15 = r14
        L_0x02a8:
            if (r14 == 0) goto L_0x04bd
            android.support.constraint.solver.widgets.ConstraintWidget[] r0 = r14.mNextChainWidget
            r0 = r0[r37]
            r8 = r0
        L_0x02af:
            if (r8 == 0) goto L_0x02be
            int r0 = r8.getVisibility()
            r7 = 8
            if (r0 != r7) goto L_0x02c0
            android.support.constraint.solver.widgets.ConstraintWidget[] r0 = r8.mNextChainWidget
            r8 = r0[r37]
            goto L_0x02af
        L_0x02be:
            r7 = 8
        L_0x02c0:
            if (r8 != 0) goto L_0x02cd
            if (r14 != r13) goto L_0x02c5
            goto L_0x02cd
        L_0x02c5:
            r18 = r8
            r19 = 4
            r20 = 6
            goto L_0x0383
        L_0x02cd:
            android.support.constraint.solver.widgets.ConstraintAnchor[] r0 = r14.mListAnchors
            r0 = r0[r38]
            android.support.constraint.solver.SolverVariable r1 = r0.mSolverVariable
            android.support.constraint.solver.widgets.ConstraintAnchor r2 = r0.mTarget
            if (r2 == 0) goto L_0x02dc
            android.support.constraint.solver.widgets.ConstraintAnchor r2 = r0.mTarget
            android.support.constraint.solver.SolverVariable r2 = r2.mSolverVariable
            goto L_0x02de
        L_0x02dc:
            r2 = r21
        L_0x02de:
            if (r15 == r14) goto L_0x02e9
            android.support.constraint.solver.widgets.ConstraintAnchor[] r2 = r15.mListAnchors
            int r3 = r38 + 1
            r2 = r2[r3]
            android.support.constraint.solver.SolverVariable r2 = r2.mSolverVariable
            goto L_0x0300
        L_0x02e9:
            if (r14 != r12) goto L_0x0300
            if (r15 != r14) goto L_0x0300
            android.support.constraint.solver.widgets.ConstraintAnchor[] r2 = r10.mListAnchors
            r2 = r2[r38]
            android.support.constraint.solver.widgets.ConstraintAnchor r2 = r2.mTarget
            if (r2 == 0) goto L_0x02fe
            android.support.constraint.solver.widgets.ConstraintAnchor[] r2 = r10.mListAnchors
            r2 = r2[r38]
            android.support.constraint.solver.widgets.ConstraintAnchor r2 = r2.mTarget
            android.support.constraint.solver.SolverVariable r2 = r2.mSolverVariable
            goto L_0x0300
        L_0x02fe:
            r2 = r21
        L_0x0300:
            int r0 = r0.getMargin()
            android.support.constraint.solver.widgets.ConstraintAnchor[] r3 = r14.mListAnchors
            int r4 = r38 + 1
            r3 = r3[r4]
            int r3 = r3.getMargin()
            if (r8 == 0) goto L_0x031d
            android.support.constraint.solver.widgets.ConstraintAnchor[] r5 = r8.mListAnchors
            r5 = r5[r38]
            android.support.constraint.solver.SolverVariable r6 = r5.mSolverVariable
            android.support.constraint.solver.widgets.ConstraintAnchor[] r7 = r14.mListAnchors
            r7 = r7[r4]
            android.support.constraint.solver.SolverVariable r7 = r7.mSolverVariable
            goto L_0x0330
        L_0x031d:
            android.support.constraint.solver.widgets.ConstraintAnchor[] r5 = r11.mListAnchors
            r5 = r5[r4]
            android.support.constraint.solver.widgets.ConstraintAnchor r5 = r5.mTarget
            if (r5 == 0) goto L_0x0328
            android.support.constraint.solver.SolverVariable r6 = r5.mSolverVariable
            goto L_0x032a
        L_0x0328:
            r6 = r21
        L_0x032a:
            android.support.constraint.solver.widgets.ConstraintAnchor[] r7 = r14.mListAnchors
            r7 = r7[r4]
            android.support.constraint.solver.SolverVariable r7 = r7.mSolverVariable
        L_0x0330:
            if (r5 == 0) goto L_0x0337
            int r5 = r5.getMargin()
            int r3 = r3 + r5
        L_0x0337:
            if (r15 == 0) goto L_0x0342
            android.support.constraint.solver.widgets.ConstraintAnchor[] r5 = r15.mListAnchors
            r5 = r5[r4]
            int r5 = r5.getMargin()
            int r0 = r0 + r5
        L_0x0342:
            if (r1 == 0) goto L_0x02c5
            if (r2 == 0) goto L_0x02c5
            if (r6 == 0) goto L_0x02c5
            if (r7 == 0) goto L_0x02c5
            if (r14 != r12) goto L_0x0354
            android.support.constraint.solver.widgets.ConstraintAnchor[] r0 = r12.mListAnchors
            r0 = r0[r38]
            int r0 = r0.getMargin()
        L_0x0354:
            r5 = r0
            if (r14 != r13) goto L_0x0362
            android.support.constraint.solver.widgets.ConstraintAnchor[] r0 = r13.mListAnchors
            r0 = r0[r4]
            int r0 = r0.getMargin()
            r18 = r0
            goto L_0x0364
        L_0x0362:
            r18 = r3
        L_0x0364:
            if (r17 == 0) goto L_0x0369
            r22 = 6
            goto L_0x036b
        L_0x0369:
            r22 = 4
        L_0x036b:
            r4 = 1056964608(0x3f000000, float:0.5)
            r0 = r36
            r3 = r5
            r20 = 4
            r5 = r6
            r23 = 6
            r6 = r7
            r19 = 4
            r20 = 6
            r7 = r18
            r18 = r8
            r8 = r22
            r0.addCentering(r1, r2, r3, r4, r5, r6, r7, r8)
        L_0x0383:
            int r0 = r14.getVisibility()
            r8 = 8
            if (r0 == r8) goto L_0x038c
            r15 = r14
        L_0x038c:
            r14 = r18
            goto L_0x02a8
        L_0x0390:
            r8 = 8
            r19 = 4
            r20 = 6
            if (r16 == 0) goto L_0x04bd
            if (r12 == 0) goto L_0x04bd
            int r0 = r1.mWidgetsMatchCount
            if (r0 <= 0) goto L_0x03a7
            int r0 = r1.mWidgetsCount
            int r1 = r1.mWidgetsMatchCount
            if (r0 != r1) goto L_0x03a7
            r17 = 1
            goto L_0x03a9
        L_0x03a7:
            r17 = 0
        L_0x03a9:
            r14 = r12
            r15 = r14
        L_0x03ab:
            if (r14 == 0) goto L_0x045f
            android.support.constraint.solver.widgets.ConstraintWidget[] r0 = r14.mNextChainWidget
            r0 = r0[r37]
        L_0x03b1:
            if (r0 == 0) goto L_0x03be
            int r1 = r0.getVisibility()
            if (r1 != r8) goto L_0x03be
            android.support.constraint.solver.widgets.ConstraintWidget[] r0 = r0.mNextChainWidget
            r0 = r0[r37]
            goto L_0x03b1
        L_0x03be:
            if (r14 == r12) goto L_0x044c
            if (r14 == r13) goto L_0x044c
            if (r0 == 0) goto L_0x044c
            if (r0 != r13) goto L_0x03c9
            r7 = r21
            goto L_0x03ca
        L_0x03c9:
            r7 = r0
        L_0x03ca:
            android.support.constraint.solver.widgets.ConstraintAnchor[] r0 = r14.mListAnchors
            r0 = r0[r38]
            android.support.constraint.solver.SolverVariable r1 = r0.mSolverVariable
            android.support.constraint.solver.widgets.ConstraintAnchor[] r2 = r15.mListAnchors
            int r3 = r38 + 1
            r2 = r2[r3]
            android.support.constraint.solver.SolverVariable r2 = r2.mSolverVariable
            int r0 = r0.getMargin()
            android.support.constraint.solver.widgets.ConstraintAnchor[] r4 = r14.mListAnchors
            r4 = r4[r3]
            int r4 = r4.getMargin()
            if (r7 == 0) goto L_0x03f8
            android.support.constraint.solver.widgets.ConstraintAnchor[] r5 = r7.mListAnchors
            r5 = r5[r38]
            android.support.constraint.solver.SolverVariable r6 = r5.mSolverVariable
            android.support.constraint.solver.widgets.ConstraintAnchor r8 = r5.mTarget
            if (r8 == 0) goto L_0x03f5
            android.support.constraint.solver.widgets.ConstraintAnchor r8 = r5.mTarget
            android.support.constraint.solver.SolverVariable r8 = r8.mSolverVariable
            goto L_0x040b
        L_0x03f5:
            r8 = r21
            goto L_0x040b
        L_0x03f8:
            android.support.constraint.solver.widgets.ConstraintAnchor[] r5 = r14.mListAnchors
            r5 = r5[r3]
            android.support.constraint.solver.widgets.ConstraintAnchor r5 = r5.mTarget
            if (r5 == 0) goto L_0x0403
            android.support.constraint.solver.SolverVariable r6 = r5.mSolverVariable
            goto L_0x0405
        L_0x0403:
            r6 = r21
        L_0x0405:
            android.support.constraint.solver.widgets.ConstraintAnchor[] r8 = r14.mListAnchors
            r8 = r8[r3]
            android.support.constraint.solver.SolverVariable r8 = r8.mSolverVariable
        L_0x040b:
            if (r5 == 0) goto L_0x0412
            int r5 = r5.getMargin()
            int r4 = r4 + r5
        L_0x0412:
            r18 = r4
            if (r15 == 0) goto L_0x041f
            android.support.constraint.solver.widgets.ConstraintAnchor[] r4 = r15.mListAnchors
            r3 = r4[r3]
            int r3 = r3.getMargin()
            int r0 = r0 + r3
        L_0x041f:
            r3 = r0
            if (r17 == 0) goto L_0x0425
            r22 = 6
            goto L_0x0427
        L_0x0425:
            r22 = 4
        L_0x0427:
            if (r1 == 0) goto L_0x0443
            if (r2 == 0) goto L_0x0443
            if (r6 == 0) goto L_0x0443
            if (r8 == 0) goto L_0x0443
            r4 = 1056964608(0x3f000000, float:0.5)
            r0 = r36
            r5 = r6
            r6 = r8
            r23 = r7
            r7 = r18
            r18 = r15
            r15 = 8
            r8 = r22
            r0.addCentering(r1, r2, r3, r4, r5, r6, r7, r8)
            goto L_0x0449
        L_0x0443:
            r23 = r7
            r18 = r15
            r15 = 8
        L_0x0449:
            r0 = r23
            goto L_0x0450
        L_0x044c:
            r18 = r15
            r15 = 8
        L_0x0450:
            int r1 = r14.getVisibility()
            if (r1 == r15) goto L_0x0457
            goto L_0x0459
        L_0x0457:
            r14 = r18
        L_0x0459:
            r15 = r14
            r8 = 8
            r14 = r0
            goto L_0x03ab
        L_0x045f:
            android.support.constraint.solver.widgets.ConstraintAnchor[] r0 = r12.mListAnchors
            r0 = r0[r38]
            android.support.constraint.solver.widgets.ConstraintAnchor[] r1 = r10.mListAnchors
            r1 = r1[r38]
            android.support.constraint.solver.widgets.ConstraintAnchor r1 = r1.mTarget
            android.support.constraint.solver.widgets.ConstraintAnchor[] r2 = r13.mListAnchors
            int r3 = r38 + 1
            r10 = r2[r3]
            android.support.constraint.solver.widgets.ConstraintAnchor[] r2 = r11.mListAnchors
            r2 = r2[r3]
            android.support.constraint.solver.widgets.ConstraintAnchor r14 = r2.mTarget
            if (r1 == 0) goto L_0x04ac
            if (r12 == r13) goto L_0x0486
            android.support.constraint.solver.SolverVariable r2 = r0.mSolverVariable
            android.support.constraint.solver.SolverVariable r1 = r1.mSolverVariable
            int r0 = r0.getMargin()
            r15 = 5
            r9.addEquality(r2, r1, r0, r15)
            goto L_0x04ad
        L_0x0486:
            r15 = 5
            if (r14 == 0) goto L_0x04ad
            android.support.constraint.solver.SolverVariable r2 = r0.mSolverVariable
            android.support.constraint.solver.SolverVariable r3 = r1.mSolverVariable
            int r4 = r0.getMargin()
            r5 = 1056964608(0x3f000000, float:0.5)
            android.support.constraint.solver.SolverVariable r6 = r10.mSolverVariable
            android.support.constraint.solver.SolverVariable r7 = r14.mSolverVariable
            int r8 = r10.getMargin()
            r17 = 5
            r0 = r36
            r1 = r2
            r2 = r3
            r3 = r4
            r4 = r5
            r5 = r6
            r6 = r7
            r7 = r8
            r8 = r17
            r0.addCentering(r1, r2, r3, r4, r5, r6, r7, r8)
            goto L_0x04ad
        L_0x04ac:
            r15 = 5
        L_0x04ad:
            if (r14 == 0) goto L_0x04bd
            if (r12 == r13) goto L_0x04bd
            android.support.constraint.solver.SolverVariable r0 = r10.mSolverVariable
            android.support.constraint.solver.SolverVariable r1 = r14.mSolverVariable
            int r2 = r10.getMargin()
            int r2 = -r2
            r9.addEquality(r0, r1, r2, r15)
        L_0x04bd:
            if (r25 != 0) goto L_0x04c1
            if (r16 == 0) goto L_0x0525
        L_0x04c1:
            if (r12 == 0) goto L_0x0525
            android.support.constraint.solver.widgets.ConstraintAnchor[] r0 = r12.mListAnchors
            r0 = r0[r38]
            android.support.constraint.solver.widgets.ConstraintAnchor[] r1 = r13.mListAnchors
            int r2 = r38 + 1
            r1 = r1[r2]
            android.support.constraint.solver.widgets.ConstraintAnchor r3 = r0.mTarget
            if (r3 == 0) goto L_0x04d6
            android.support.constraint.solver.widgets.ConstraintAnchor r3 = r0.mTarget
            android.support.constraint.solver.SolverVariable r3 = r3.mSolverVariable
            goto L_0x04d8
        L_0x04d6:
            r3 = r21
        L_0x04d8:
            android.support.constraint.solver.widgets.ConstraintAnchor r4 = r1.mTarget
            if (r4 == 0) goto L_0x04e1
            android.support.constraint.solver.widgets.ConstraintAnchor r4 = r1.mTarget
            android.support.constraint.solver.SolverVariable r4 = r4.mSolverVariable
            goto L_0x04e3
        L_0x04e1:
            r4 = r21
        L_0x04e3:
            if (r11 == r13) goto L_0x04f4
            android.support.constraint.solver.widgets.ConstraintAnchor[] r4 = r11.mListAnchors
            r4 = r4[r2]
            android.support.constraint.solver.widgets.ConstraintAnchor r5 = r4.mTarget
            if (r5 == 0) goto L_0x04f2
            android.support.constraint.solver.widgets.ConstraintAnchor r4 = r4.mTarget
            android.support.constraint.solver.SolverVariable r4 = r4.mSolverVariable
            goto L_0x04f4
        L_0x04f2:
            r4 = r21
        L_0x04f4:
            r5 = r4
            if (r12 != r13) goto L_0x04ff
            android.support.constraint.solver.widgets.ConstraintAnchor[] r0 = r12.mListAnchors
            r0 = r0[r38]
            android.support.constraint.solver.widgets.ConstraintAnchor[] r1 = r12.mListAnchors
            r1 = r1[r2]
        L_0x04ff:
            if (r3 == 0) goto L_0x0525
            if (r5 == 0) goto L_0x0525
            int r4 = r0.getMargin()
            if (r13 != 0) goto L_0x050a
            goto L_0x050b
        L_0x050a:
            r11 = r13
        L_0x050b:
            android.support.constraint.solver.widgets.ConstraintAnchor[] r6 = r11.mListAnchors
            r2 = r6[r2]
            int r7 = r2.getMargin()
            android.support.constraint.solver.SolverVariable r2 = r0.mSolverVariable
            r6 = 1056964608(0x3f000000, float:0.5)
            android.support.constraint.solver.SolverVariable r8 = r1.mSolverVariable
            r10 = 5
            r0 = r36
            r1 = r2
            r2 = r3
            r3 = r4
            r4 = r6
            r6 = r8
            r8 = r10
            r0.addCentering(r1, r2, r3, r4, r5, r6, r7, r8)
        L_0x0525:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.constraint.solver.widgets.Chain.applyChainConstraints(android.support.constraint.solver.widgets.ConstraintWidgetContainer, android.support.constraint.solver.LinearSystem, int, int, android.support.constraint.solver.widgets.ChainHead):void");
    }
}
