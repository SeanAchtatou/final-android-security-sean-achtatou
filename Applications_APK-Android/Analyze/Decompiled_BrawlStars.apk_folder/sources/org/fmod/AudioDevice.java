package org.fmod;

import android.media.AudioTrack;

public class AudioDevice {
    private AudioTrack mTrack = null;

    private int fetchChannelConfigFromCount(int i) {
        if (i == 1) {
            return 2;
        }
        if (i == 2) {
            return 3;
        }
        if (i == 6) {
            return 252;
        }
        return i == 8 ? 6396 : 0;
    }

    public boolean init(int i, int i2, int i3, int i4) {
        int fetchChannelConfigFromCount = fetchChannelConfigFromCount(i);
        int minBufferSize = AudioTrack.getMinBufferSize(i2, fetchChannelConfigFromCount, 2);
        if (minBufferSize >= 0) {
            "AudioDevice::init : Min buffer size: " + minBufferSize + " bytes";
        }
        int i5 = i3 * i4 * i * 2;
        int i6 = i5 > minBufferSize ? i5 : minBufferSize;
        "AudioDevice::init : Actual buffer size: " + i6 + " bytes";
        try {
            this.mTrack = new AudioTrack(3, i2, fetchChannelConfigFromCount, 2, i6, 1);
            try {
                this.mTrack.play();
                return true;
            } catch (IllegalStateException unused) {
                this.mTrack.release();
                this.mTrack = null;
                return false;
            }
        } catch (IllegalArgumentException unused2) {
            return false;
        }
    }

    public void close() {
        try {
            this.mTrack.stop();
        } catch (IllegalStateException unused) {
        }
        this.mTrack.release();
        this.mTrack = null;
    }

    public void write(byte[] bArr, int i) {
        this.mTrack.write(bArr, 0, i);
    }
}
