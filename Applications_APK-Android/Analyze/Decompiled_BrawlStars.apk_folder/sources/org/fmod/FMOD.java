package org.fmod;

import android.content.Context;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.os.Build;

public class FMOD {
    private static Context gContext;

    public static void init(Context context) {
        gContext = context;
    }

    public static void close() {
        gContext = null;
    }

    public static boolean checkInit() {
        return gContext != null;
    }

    public static AssetManager getAssetManager() {
        Context context = gContext;
        if (context != null) {
            return context.getAssets();
        }
        return null;
    }

    public static boolean supportsLowLatency() {
        if (gContext == null || Build.VERSION.SDK_INT < 5) {
            return false;
        }
        int outputBlockSize = getOutputBlockSize();
        boolean hasSystemFeature = gContext.getPackageManager().hasSystemFeature("android.hardware.audio.low_latency");
        boolean hasSystemFeature2 = gContext.getPackageManager().hasSystemFeature("android.hardware.audio.pro");
        boolean z = outputBlockSize > 0 && outputBlockSize <= 1024;
        boolean isBluetoothOn = isBluetoothOn();
        "FMOD::supportsLowLatency                 : Low latency = " + hasSystemFeature + ", Pro Audio = " + hasSystemFeature2 + ", Bluetooth On = " + isBluetoothOn + ", Acceptable Block Size = " + z + " (" + outputBlockSize + ")";
        if (!z || !hasSystemFeature || isBluetoothOn) {
            return false;
        }
        return true;
    }

    public static boolean supportsAAudio() {
        return Build.VERSION.SDK_INT >= 26;
    }

    public static int getOutputSampleRate() {
        String property;
        if (gContext == null || Build.VERSION.SDK_INT < 17 || (property = ((AudioManager) gContext.getSystemService("audio")).getProperty("android.media.property.OUTPUT_SAMPLE_RATE")) == null) {
            return 0;
        }
        return Integer.parseInt(property);
    }

    public static int getOutputBlockSize() {
        String property;
        if (gContext == null || Build.VERSION.SDK_INT < 17 || (property = ((AudioManager) gContext.getSystemService("audio")).getProperty("android.media.property.OUTPUT_FRAMES_PER_BUFFER")) == null) {
            return 0;
        }
        return Integer.parseInt(property);
    }

    public static boolean isBluetoothOn() {
        Context context = gContext;
        if (context == null) {
            return false;
        }
        AudioManager audioManager = (AudioManager) context.getSystemService("audio");
        if (audioManager.isBluetoothA2dpOn() || audioManager.isBluetoothScoOn()) {
            return true;
        }
        return false;
    }
}
