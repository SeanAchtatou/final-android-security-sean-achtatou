package org.OpenUDID;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.provider.Settings;
import com.supercell.titan.GameApp;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

public class OpenUDID_manager implements ServiceConnection {
    private static String f = null;
    private static boolean g = false;

    /* renamed from: a  reason: collision with root package name */
    private final Context f5476a;

    /* renamed from: b  reason: collision with root package name */
    private List<ResolveInfo> f5477b;
    /* access modifiers changed from: private */
    public Map<String, Integer> c = new HashMap();
    private final SharedPreferences d;
    private final Random e = new Random();

    public void onServiceDisconnected(ComponentName componentName) {
    }

    private OpenUDID_manager(Context context) {
        this.d = context.getSharedPreferences("openudid_prefs", 0);
        this.f5476a = context;
    }

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0052 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onServiceConnected(android.content.ComponentName r5, android.os.IBinder r6) {
        /*
            r4 = this;
            android.os.Parcel r5 = android.os.Parcel.obtain()     // Catch:{ RemoteException -> 0x0052 }
            java.util.Random r0 = r4.e     // Catch:{ RemoteException -> 0x0052 }
            int r0 = r0.nextInt()     // Catch:{ RemoteException -> 0x0052 }
            r5.writeInt(r0)     // Catch:{ RemoteException -> 0x0052 }
            android.os.Parcel r0 = android.os.Parcel.obtain()     // Catch:{ RemoteException -> 0x0052 }
            android.os.Parcel r1 = android.os.Parcel.obtain()     // Catch:{ RemoteException -> 0x0052 }
            r2 = 0
            r3 = 1
            r6.transact(r3, r1, r0, r2)     // Catch:{ RemoteException -> 0x0052 }
            int r5 = r5.readInt()     // Catch:{ RemoteException -> 0x0052 }
            int r6 = r0.readInt()     // Catch:{ RemoteException -> 0x0052 }
            if (r5 != r6) goto L_0x0052
            java.lang.String r5 = r0.readString()     // Catch:{ RemoteException -> 0x0052 }
            if (r5 == 0) goto L_0x0052
            java.util.Map<java.lang.String, java.lang.Integer> r6 = r4.c     // Catch:{ RemoteException -> 0x0052 }
            boolean r6 = r6.containsKey(r5)     // Catch:{ RemoteException -> 0x0052 }
            if (r6 == 0) goto L_0x0049
            java.util.Map<java.lang.String, java.lang.Integer> r6 = r4.c     // Catch:{ RemoteException -> 0x0052 }
            java.util.Map<java.lang.String, java.lang.Integer> r0 = r4.c     // Catch:{ RemoteException -> 0x0052 }
            java.lang.Object r0 = r0.get(r5)     // Catch:{ RemoteException -> 0x0052 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ RemoteException -> 0x0052 }
            int r0 = r0.intValue()     // Catch:{ RemoteException -> 0x0052 }
            int r0 = r0 + r3
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ RemoteException -> 0x0052 }
            r6.put(r5, r0)     // Catch:{ RemoteException -> 0x0052 }
            goto L_0x0052
        L_0x0049:
            java.util.Map<java.lang.String, java.lang.Integer> r6 = r4.c     // Catch:{ RemoteException -> 0x0052 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r3)     // Catch:{ RemoteException -> 0x0052 }
            r6.put(r5, r0)     // Catch:{ RemoteException -> 0x0052 }
        L_0x0052:
            android.content.Context r5 = r4.f5476a     // Catch:{ Exception -> 0x0058 }
            r5.unbindService(r4)     // Catch:{ Exception -> 0x0058 }
            goto L_0x005c
        L_0x0058:
            r5 = move-exception
            com.supercell.titan.GameApp.debuggerException(r5)
        L_0x005c:
            r4.e()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.OpenUDID.OpenUDID_manager.onServiceConnected(android.content.ComponentName, android.os.IBinder):void");
    }

    private void c() {
        SharedPreferences.Editor edit = this.d.edit();
        edit.putString("openudid", f);
        edit.commit();
    }

    private void d() {
        f = Settings.Secure.getString(this.f5476a.getContentResolver(), "android_id");
        String str = f;
        if (str == null || str.equals("9774d56d682e549c") || f.length() < 15) {
            f = new BigInteger(64, new SecureRandom()).toString(16);
        }
    }

    private void e() {
        if (this.f5477b.size() > 0) {
            ServiceInfo serviceInfo = this.f5477b.get(0).serviceInfo;
            Intent intent = new Intent();
            intent.setComponent(new ComponentName(serviceInfo.applicationInfo.packageName, serviceInfo.name));
            try {
                this.f5476a.bindService(intent, this, 1);
            } catch (Exception e2) {
                GameApp.debuggerException(e2);
            }
            this.f5477b.remove(0);
            return;
        }
        f();
        if (f == null) {
            d();
        }
        c();
        g = true;
    }

    private void f() {
        if (!this.c.isEmpty()) {
            TreeMap treeMap = new TreeMap(new a(this, (byte) 0));
            treeMap.putAll(this.c);
            f = (String) treeMap.firstKey();
        }
    }

    public static String a() {
        return f;
    }

    public static boolean b() {
        return g;
    }

    public static void a(Context context) {
        OpenUDID_manager openUDID_manager = new OpenUDID_manager(context);
        f = openUDID_manager.d.getString("openudid", null);
        if (f == null) {
            openUDID_manager.f5477b = context.getPackageManager().queryIntentServices(new Intent("org.OpenUDID.GETUDID"), 0);
            if (openUDID_manager.f5477b != null) {
                openUDID_manager.e();
                return;
            }
            return;
        }
        g = true;
    }

    class a implements Comparator {
        private a() {
        }

        /* synthetic */ a(OpenUDID_manager openUDID_manager, byte b2) {
            this();
        }

        public int compare(Object obj, Object obj2) {
            if (((Integer) OpenUDID_manager.this.c.get(obj)).intValue() < ((Integer) OpenUDID_manager.this.c.get(obj2)).intValue()) {
                return 1;
            }
            return OpenUDID_manager.this.c.get(obj) == OpenUDID_manager.this.c.get(obj2) ? 0 : -1;
        }
    }
}
