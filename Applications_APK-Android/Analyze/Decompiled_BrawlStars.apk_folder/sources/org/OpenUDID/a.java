package org.OpenUDID;

import android.content.SharedPreferences;
import android.os.Binder;
import android.os.Parcel;

/* compiled from: OpenUDID_service */
class a extends Binder {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ OpenUDID_service f5479a;

    a(OpenUDID_service openUDID_service) {
        this.f5479a = openUDID_service;
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        SharedPreferences sharedPreferences = this.f5479a.getSharedPreferences("openudid_prefs", 0);
        parcel2.writeInt(parcel.readInt());
        parcel2.writeString(sharedPreferences.getString("openudid", null));
        return true;
    }
}
