package kotlin.f;

import java.util.Random;

/* compiled from: PlatformRandom.kt */
public abstract class a extends d {
    public abstract Random a();

    public final int a(int i) {
        return ((-i) >> 31) & (a().nextInt() >>> (32 - i));
    }

    public final int b() {
        return a().nextInt();
    }

    public final int b(int i) {
        return a().nextInt(i);
    }

    public final float c() {
        return a().nextFloat();
    }
}
