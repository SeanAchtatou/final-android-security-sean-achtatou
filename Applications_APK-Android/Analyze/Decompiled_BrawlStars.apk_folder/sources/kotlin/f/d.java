package kotlin.f;

import kotlin.d.b.j;

/* compiled from: Random.kt */
public abstract class d {
    /* access modifiers changed from: package-private */

    /* renamed from: a  reason: collision with root package name */
    public static final d f5256a = kotlin.c.a.a();

    /* renamed from: b  reason: collision with root package name */
    public static final a f5257b = a.d;
    public static final b c = new b((byte) 0);

    public abstract int a(int i);

    public int b() {
        return a(32);
    }

    public int b(int i) {
        return a(0, i);
    }

    public int a(int i, int i2) {
        int i3;
        int b2;
        int i4;
        if (i2 > i) {
            int i5 = i2 - i;
            if (i5 > 0 || i5 == Integer.MIN_VALUE) {
                if (((-i5) & i5) == i5) {
                    i3 = a(31 - Integer.numberOfLeadingZeros(i5));
                } else {
                    do {
                        b2 = b() >>> 1;
                        i4 = b2 % i5;
                    } while ((b2 - i4) + (i5 - 1) < 0);
                    i3 = i4;
                }
                return i + i3;
            }
            while (true) {
                int b3 = b();
                if (i <= b3 && i2 > b3) {
                    return b3;
                }
            }
        } else {
            Integer valueOf = Integer.valueOf(i);
            Integer valueOf2 = Integer.valueOf(i2);
            j.b(valueOf, "from");
            j.b(valueOf2, "until");
            throw new IllegalArgumentException(("Random range is empty: [" + valueOf + ", " + valueOf2 + ").").toString());
        }
    }

    public float c() {
        return ((float) a(24)) / 1.6777216E7f;
    }

    /* compiled from: Random.kt */
    public static final class b extends d {
        private b() {
        }

        public /* synthetic */ b(byte b2) {
            this();
        }

        public final int a(int i) {
            return d.f5256a.a(i);
        }

        public final int b() {
            return d.f5256a.b();
        }

        public final int b(int i) {
            return d.f5256a.b(i);
        }

        public final int a(int i, int i2) {
            return d.f5256a.a(i, i2);
        }

        public final float c() {
            return d.f5256a.c();
        }
    }

    static {
        kotlin.c.a aVar = kotlin.c.b.f5237a;
    }

    /* compiled from: Random.kt */
    public static final class a extends d {
        public static final a d = new a();

        private a() {
        }

        public final int a(int i) {
            b bVar = d.c;
            return d.f5256a.a(i);
        }
    }
}
