package kotlin.g;

import java.util.Iterator;
import kotlin.c.c;

/* compiled from: Progressions.kt */
public class a implements Iterable<Integer>, kotlin.d.b.a.a {
    public static final C0160a d = new C0160a((byte) 0);

    /* renamed from: a  reason: collision with root package name */
    public final int f5258a;

    /* renamed from: b  reason: collision with root package name */
    public final int f5259b;
    public final int c;

    public a(int i, int i2, int i3) {
        if (i3 == 0) {
            throw new IllegalArgumentException("Step must be non-zero.");
        } else if (i3 != Integer.MIN_VALUE) {
            this.f5258a = i;
            if (i3 > 0) {
                if (i < i2) {
                    i2 -= c.a(i2, i, i3);
                }
            } else if (i3 >= 0) {
                throw new IllegalArgumentException("Step is zero.");
            } else if (i > i2) {
                i2 += c.a(i, i2, -i3);
            }
            this.f5259b = i2;
            this.c = i3;
        } else {
            throw new IllegalArgumentException("Step must be greater than Int.MIN_VALUE to avoid overflow on negation.");
        }
    }

    public boolean a() {
        if (this.c > 0) {
            if (this.f5258a > this.f5259b) {
                return true;
            }
        } else if (this.f5258a < this.f5259b) {
            return true;
        }
        return false;
    }

    public boolean equals(Object obj) {
        if (obj instanceof a) {
            if (!a() || !((a) obj).a()) {
                a aVar = (a) obj;
                if (!(this.f5258a == aVar.f5258a && this.f5259b == aVar.f5259b && this.c == aVar.c)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (a()) {
            return -1;
        }
        return (((this.f5258a * 31) + this.f5259b) * 31) + this.c;
    }

    public String toString() {
        int i;
        StringBuilder sb;
        if (this.c > 0) {
            sb = new StringBuilder();
            sb.append(this.f5258a);
            sb.append("..");
            sb.append(this.f5259b);
            sb.append(" step ");
            i = this.c;
        } else {
            sb = new StringBuilder();
            sb.append(this.f5258a);
            sb.append(" downTo ");
            sb.append(this.f5259b);
            sb.append(" step ");
            i = -this.c;
        }
        sb.append(i);
        return sb.toString();
    }

    /* renamed from: kotlin.g.a$a  reason: collision with other inner class name */
    /* compiled from: Progressions.kt */
    public static final class C0160a {
        private C0160a() {
        }

        public /* synthetic */ C0160a(byte b2) {
            this();
        }
    }

    public /* synthetic */ Iterator iterator() {
        return new b(this.f5258a, this.f5259b, this.c);
    }
}
