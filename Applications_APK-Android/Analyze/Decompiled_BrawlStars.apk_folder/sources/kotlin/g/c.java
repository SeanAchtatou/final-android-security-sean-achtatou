package kotlin.g;

/* compiled from: Ranges.kt */
public final class c extends a {
    public static final a e = new a((byte) 0);
    /* access modifiers changed from: private */
    public static final c f = new c(1, 0);

    public c(int i, int i2) {
        super(i, i2, 1);
    }

    public final boolean equals(Object obj) {
        if (obj instanceof c) {
            if (!a() || !((c) obj).a()) {
                c cVar = (c) obj;
                if (!(this.f5258a == cVar.f5258a && this.f5259b == cVar.f5259b)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public final int hashCode() {
        if (a()) {
            return -1;
        }
        return (this.f5258a * 31) + this.f5259b;
    }

    public final String toString() {
        return this.f5258a + ".." + this.f5259b;
    }

    /* compiled from: Ranges.kt */
    public static final class a {
        private a() {
        }

        public /* synthetic */ a(byte b2) {
            this();
        }
    }

    public final Integer b() {
        return Integer.valueOf(this.f5258a);
    }

    public final boolean a() {
        return this.f5258a > this.f5259b;
    }
}
