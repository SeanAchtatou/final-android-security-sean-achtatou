package kotlin.g;

import java.util.NoSuchElementException;
import kotlin.a.ah;

/* compiled from: ProgressionIterators.kt */
public final class b extends ah {

    /* renamed from: a  reason: collision with root package name */
    private final int f5260a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f5261b;
    private int c;
    private final int d;

    public b(int i, int i2, int i3) {
        this.d = i3;
        this.f5260a = i2;
        boolean z = true;
        if (this.d <= 0 ? i < i2 : i > i2) {
            z = false;
        }
        this.f5261b = z;
        this.c = !this.f5261b ? this.f5260a : i;
    }

    public final boolean hasNext() {
        return this.f5261b;
    }

    public final int a() {
        int i = this.c;
        if (i != this.f5260a) {
            this.c = this.d + i;
        } else if (this.f5261b) {
            this.f5261b = false;
        } else {
            throw new NoSuchElementException();
        }
        return i;
    }
}
