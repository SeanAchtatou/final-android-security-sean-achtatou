package kotlin.g;

import kotlin.g.a;
import kotlin.g.c;

/* compiled from: _Ranges.kt */
public class g extends f {
    public static final int c(int i, int i2) {
        return i < i2 ? i2 : i;
    }

    public static final int d(int i, int i2) {
        return i > i2 ? i2 : i;
    }

    public static final a a(int i, int i2) {
        a.C0160a aVar = a.d;
        return new a(i, i2, -1);
    }

    public static final c b(int i, int i2) {
        if (i2 > Integer.MIN_VALUE) {
            return new c(i, i2 - 1);
        }
        c.a aVar = c.e;
        return c.f;
    }
}
