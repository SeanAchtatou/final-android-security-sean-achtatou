package kotlin.e;

/* compiled from: MathJVM.kt */
public class c extends b {
    public static final int a(float f) {
        if (!Float.isNaN(f)) {
            return Math.round(f);
        }
        throw new IllegalArgumentException("Cannot round NaN value.");
    }
}
