package kotlin.h;

/* compiled from: KFunction.kt */
public interface e<R> extends b<R> {
    boolean isExternal();

    boolean isInfix();

    boolean isInline();

    boolean isOperator();

    boolean isSuspend();
}
