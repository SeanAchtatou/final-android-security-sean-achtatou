package kotlin;

import java.io.Serializable;

/* compiled from: Tuples.kt */
public final class j<A, B, C> implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public final A f5299a;

    /* renamed from: b  reason: collision with root package name */
    public final B f5300b;
    public final C c;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof j)) {
            return false;
        }
        j jVar = (j) obj;
        return kotlin.d.b.j.a(this.f5299a, jVar.f5299a) && kotlin.d.b.j.a(this.f5300b, jVar.f5300b) && kotlin.d.b.j.a(this.c, jVar.c);
    }

    public final int hashCode() {
        A a2 = this.f5299a;
        int i = 0;
        int hashCode = (a2 != null ? a2.hashCode() : 0) * 31;
        B b2 = this.f5300b;
        int hashCode2 = (hashCode + (b2 != null ? b2.hashCode() : 0)) * 31;
        C c2 = this.c;
        if (c2 != null) {
            i = c2.hashCode();
        }
        return hashCode2 + i;
    }

    public j(A a2, B b2, C c2) {
        this.f5299a = a2;
        this.f5300b = b2;
        this.c = c2;
    }

    public final String toString() {
        return '(' + ((Object) this.f5299a) + ", " + ((Object) this.f5300b) + ", " + ((Object) this.c) + ')';
    }
}
