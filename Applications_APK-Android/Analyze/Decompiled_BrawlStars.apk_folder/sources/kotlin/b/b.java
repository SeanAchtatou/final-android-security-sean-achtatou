package kotlin.b;

/* compiled from: Comparisons.kt */
public class b {
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static final <T extends java.lang.Comparable<?>> int a(T r0, T r1) {
        /*
            if (r0 != r1) goto L_0x0004
            r0 = 0
            return r0
        L_0x0004:
            if (r0 != 0) goto L_0x0008
            r0 = -1
            return r0
        L_0x0008:
            if (r1 != 0) goto L_0x000c
            r0 = 1
            return r0
        L_0x000c:
            int r0 = r0.compareTo(r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlin.b.b.a(java.lang.Comparable, java.lang.Comparable):int");
    }
}
