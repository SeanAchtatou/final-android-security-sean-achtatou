package kotlin.io;

import java.io.BufferedReader;
import java.util.Iterator;
import kotlin.d.b.j;
import kotlin.i.h;

/* compiled from: ReadWrite.kt */
public final class k implements h<String> {

    /* renamed from: a  reason: collision with root package name */
    final BufferedReader f5296a;

    public k(BufferedReader bufferedReader) {
        j.b(bufferedReader, "reader");
        this.f5296a = bufferedReader;
    }

    public final Iterator<String> a() {
        return new l(this);
    }
}
