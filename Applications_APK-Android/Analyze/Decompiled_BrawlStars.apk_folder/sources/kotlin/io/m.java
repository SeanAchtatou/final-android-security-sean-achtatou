package kotlin.io;

import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import kotlin.d.b.j;

/* compiled from: ReadWrite.kt */
public final class m {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final String a(Reader reader) {
        j.b(reader, "$this$readText");
        StringWriter stringWriter = new StringWriter();
        long unused = a(reader, stringWriter, 8192);
        String stringWriter2 = stringWriter.toString();
        j.a((Object) stringWriter2, "buffer.toString()");
        return stringWriter2;
    }

    /* access modifiers changed from: private */
    public static long a(Reader reader, Writer writer, int i) {
        j.b(reader, "$this$copyTo");
        j.b(writer, "out");
        char[] cArr = new char[i];
        int read = reader.read(cArr);
        long j = 0;
        while (read >= 0) {
            writer.write(cArr, 0, read);
            j += (long) read;
            read = reader.read(cArr);
        }
        return j;
    }
}
