package kotlin.io;

import java.io.File;
import kotlin.d.b.j;

/* compiled from: Exceptions.kt */
public final class AccessDeniedException extends FileSystemException {
    public /* synthetic */ AccessDeniedException(File file, File file2, String str, int i) {
        this(file, null, str);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private AccessDeniedException(File file, File file2, String str) {
        super(file, file2, str);
        j.b(file, "file");
    }
}
