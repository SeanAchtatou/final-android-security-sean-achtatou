package kotlin.io;

/* compiled from: FileReadWrite.kt */
public class h extends g {
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0024, code lost:
        throw r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0020, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0021, code lost:
        kotlin.io.b.a(r0, r1);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void a(java.io.File r1, byte[] r2) {
        /*
            java.lang.String r0 = "$this$writeBytes"
            kotlin.d.b.j.b(r1, r0)
            java.lang.String r0 = "array"
            kotlin.d.b.j.b(r2, r0)
            java.io.FileOutputStream r0 = new java.io.FileOutputStream
            r0.<init>(r1)
            java.io.Closeable r0 = (java.io.Closeable) r0
            r1 = r0
            java.io.FileOutputStream r1 = (java.io.FileOutputStream) r1     // Catch:{ all -> 0x001e }
            r1.write(r2)     // Catch:{ all -> 0x001e }
            kotlin.m r1 = kotlin.m.f5330a     // Catch:{ all -> 0x001e }
            r1 = 0
            kotlin.io.b.a(r0, r1)
            return
        L_0x001e:
            r1 = move-exception
            throw r1     // Catch:{ all -> 0x0020 }
        L_0x0020:
            r2 = move-exception
            kotlin.io.b.a(r0, r1)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlin.io.h.a(java.io.File, byte[]):void");
    }
}
