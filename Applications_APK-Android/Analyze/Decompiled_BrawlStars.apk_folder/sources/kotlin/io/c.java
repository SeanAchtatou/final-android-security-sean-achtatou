package kotlin.io;

import android.support.v7.widget.ActivityChooserView;
import java.io.File;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Iterator;
import kotlin.NoWhenBranchMatchedException;
import kotlin.a.ar;
import kotlin.d.b.j;
import kotlin.i.h;
import kotlin.m;
import kotlin.n;

/* compiled from: FileTreeWalk.kt */
public final class c implements h<File> {

    /* renamed from: a  reason: collision with root package name */
    final File f5287a;

    /* renamed from: b  reason: collision with root package name */
    final e f5288b;
    final kotlin.d.a.b<File, Boolean> c;
    final kotlin.d.a.b<File, m> d;
    final kotlin.d.a.c<File, IOException, m> e;
    final int f;

    /* JADX WARN: Type inference failed for: r3v0, types: [kotlin.d.a.b<? super java.io.File, java.lang.Boolean>, kotlin.d.a.b<java.io.File, java.lang.Boolean>] */
    /* JADX WARN: Type inference failed for: r4v0, types: [kotlin.d.a.b<? super java.io.File, kotlin.m>, kotlin.d.a.b<java.io.File, kotlin.m>] */
    /* JADX WARN: Type inference failed for: r5v0, types: [kotlin.d.a.c<java.io.File, java.io.IOException, kotlin.m>, kotlin.d.a.c<? super java.io.File, ? super java.io.IOException, kotlin.m>] */
    /* JADX WARNING: Unknown variable types count: 3 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private c(java.io.File r1, kotlin.io.e r2, kotlin.d.a.b<? super java.io.File, java.lang.Boolean> r3, kotlin.d.a.b<? super java.io.File, kotlin.m> r4, kotlin.d.a.c<? super java.io.File, ? super java.io.IOException, kotlin.m> r5, int r6) {
        /*
            r0 = this;
            r0.<init>()
            r0.f5287a = r1
            r0.f5288b = r2
            r0.c = r3
            r0.d = r4
            r0.e = r5
            r0.f = r6
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlin.io.c.<init>(java.io.File, kotlin.io.e, kotlin.d.a.b, kotlin.d.a.b, kotlin.d.a.c, int):void");
    }

    private /* synthetic */ c(File file, e eVar, kotlin.d.a.b bVar, kotlin.d.a.b bVar2, kotlin.d.a.c cVar, int i, int i2) {
        this(file, eVar, null, null, null, ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public c(File file, e eVar) {
        this(file, eVar, null, null, null, 0, 32);
        j.b(file, "start");
        j.b(eVar, "direction");
    }

    public final Iterator<File> a() {
        return new b();
    }

    /* renamed from: kotlin.io.c$c  reason: collision with other inner class name */
    /* compiled from: FileTreeWalk.kt */
    static abstract class C0163c {

        /* renamed from: b  reason: collision with root package name */
        final File f5292b;

        public abstract File a();

        public C0163c(File file) {
            j.b(file, "root");
            this.f5292b = file;
        }
    }

    /* compiled from: FileTreeWalk.kt */
    static abstract class a extends C0163c {
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(File file) {
            super(file);
            j.b(file, "rootDir");
            if (n.f5331a) {
                boolean isDirectory = file.isDirectory();
                if (n.f5331a && !isDirectory) {
                    throw new AssertionError("rootDir must be verified to be directory beforehand.");
                }
            }
        }
    }

    /* compiled from: FileTreeWalk.kt */
    final class b extends kotlin.a.c<File> {
        private final ArrayDeque<C0163c> d = new ArrayDeque<>();

        public b() {
            if (c.this.f5287a.isDirectory()) {
                this.d.push(a(c.this.f5287a));
            } else if (c.this.f5287a.isFile()) {
                this.d.push(new C0161b(this, c.this.f5287a));
            } else {
                this.f5221a = ar.Done;
            }
        }

        private final a a(File file) {
            int i = d.f5293a[c.this.f5288b.ordinal()];
            if (i == 1) {
                return new C0162c(this, file);
            }
            if (i == 2) {
                return new a(this, file);
            }
            throw new NoWhenBranchMatchedException();
        }

        /* compiled from: FileTreeWalk.kt */
        final class a extends a {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ b f5289a;
            private boolean c;
            private File[] d;
            private int e;
            private boolean f;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, File file) {
                super(file);
                j.b(file, "rootDir");
                this.f5289a = bVar;
            }

            public final File a() {
                if (!this.f && this.d == null) {
                    kotlin.d.a.b<File, Boolean> bVar = c.this.c;
                    if (bVar != null && !bVar.invoke(this.f5292b).booleanValue()) {
                        return null;
                    }
                    this.d = this.f5292b.listFiles();
                    if (this.d == null) {
                        kotlin.d.a.c<File, IOException, m> cVar = c.this.e;
                        if (cVar != null) {
                            cVar.invoke(this.f5292b, new AccessDeniedException(this.f5292b, null, "Cannot list files in a directory", 2));
                        }
                        this.f = true;
                    }
                }
                File[] fileArr = this.d;
                if (fileArr != null) {
                    int i = this.e;
                    if (fileArr == null) {
                        j.a();
                    }
                    if (i < fileArr.length) {
                        File[] fileArr2 = this.d;
                        if (fileArr2 == null) {
                            j.a();
                        }
                        int i2 = this.e;
                        this.e = i2 + 1;
                        return fileArr2[i2];
                    }
                }
                if (!this.c) {
                    this.c = true;
                    return this.f5292b;
                }
                kotlin.d.a.b<File, m> bVar2 = c.this.d;
                if (bVar2 != null) {
                    bVar2.invoke(this.f5292b);
                }
                return null;
            }
        }

        /* renamed from: kotlin.io.c$b$c  reason: collision with other inner class name */
        /* compiled from: FileTreeWalk.kt */
        final class C0162c extends a {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ b f5291a;
            private boolean c;
            private File[] d;
            private int e;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0162c(b bVar, File file) {
                super(file);
                j.b(file, "rootDir");
                this.f5291a = bVar;
            }

            /* JADX WARNING: Code restructure failed: missing block: B:32:0x0070, code lost:
                if (r0.length == 0) goto L_0x0072;
             */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.io.File a() {
                /*
                    r7 = this;
                    boolean r0 = r7.c
                    r1 = 0
                    if (r0 != 0) goto L_0x0022
                    kotlin.io.c$b r0 = r7.f5291a
                    kotlin.io.c r0 = kotlin.io.c.this
                    kotlin.d.a.b<java.io.File, java.lang.Boolean> r0 = r0.c
                    if (r0 == 0) goto L_0x001c
                    java.io.File r2 = r7.f5292b
                    java.lang.Object r0 = r0.invoke(r2)
                    java.lang.Boolean r0 = (java.lang.Boolean) r0
                    boolean r0 = r0.booleanValue()
                    if (r0 != 0) goto L_0x001c
                    return r1
                L_0x001c:
                    r0 = 1
                    r7.c = r0
                    java.io.File r0 = r7.f5292b
                    return r0
                L_0x0022:
                    java.io.File[] r0 = r7.d
                    if (r0 == 0) goto L_0x003f
                    int r2 = r7.e
                    if (r0 != 0) goto L_0x002d
                    kotlin.d.b.j.a()
                L_0x002d:
                    int r0 = r0.length
                    if (r2 >= r0) goto L_0x0031
                    goto L_0x003f
                L_0x0031:
                    kotlin.io.c$b r0 = r7.f5291a
                    kotlin.io.c r0 = kotlin.io.c.this
                    kotlin.d.a.b<java.io.File, kotlin.m> r0 = r0.d
                    if (r0 == 0) goto L_0x003e
                    java.io.File r2 = r7.f5292b
                    r0.invoke(r2)
                L_0x003e:
                    return r1
                L_0x003f:
                    java.io.File[] r0 = r7.d
                    if (r0 != 0) goto L_0x0080
                    java.io.File r0 = r7.f5292b
                    java.io.File[] r0 = r0.listFiles()
                    r7.d = r0
                    java.io.File[] r0 = r7.d
                    if (r0 != 0) goto L_0x0066
                    kotlin.io.c$b r0 = r7.f5291a
                    kotlin.io.c r0 = kotlin.io.c.this
                    kotlin.d.a.c<java.io.File, java.io.IOException, kotlin.m> r0 = r0.e
                    if (r0 == 0) goto L_0x0066
                    java.io.File r2 = r7.f5292b
                    kotlin.io.AccessDeniedException r3 = new kotlin.io.AccessDeniedException
                    java.io.File r4 = r7.f5292b
                    r5 = 2
                    java.lang.String r6 = "Cannot list files in a directory"
                    r3.<init>(r4, r1, r6, r5)
                    r0.invoke(r2, r3)
                L_0x0066:
                    java.io.File[] r0 = r7.d
                    if (r0 == 0) goto L_0x0072
                    if (r0 != 0) goto L_0x006f
                    kotlin.d.b.j.a()
                L_0x006f:
                    int r0 = r0.length
                    if (r0 != 0) goto L_0x0080
                L_0x0072:
                    kotlin.io.c$b r0 = r7.f5291a
                    kotlin.io.c r0 = kotlin.io.c.this
                    kotlin.d.a.b<java.io.File, kotlin.m> r0 = r0.d
                    if (r0 == 0) goto L_0x007f
                    java.io.File r2 = r7.f5292b
                    r0.invoke(r2)
                L_0x007f:
                    return r1
                L_0x0080:
                    java.io.File[] r0 = r7.d
                    if (r0 != 0) goto L_0x0087
                    kotlin.d.b.j.a()
                L_0x0087:
                    int r1 = r7.e
                    int r2 = r1 + 1
                    r7.e = r2
                    r0 = r0[r1]
                    return r0
                */
                throw new UnsupportedOperationException("Method not decompiled: kotlin.io.c.b.C0162c.a():java.io.File");
            }
        }

        /* renamed from: kotlin.io.c$b$b  reason: collision with other inner class name */
        /* compiled from: FileTreeWalk.kt */
        final class C0161b extends C0163c {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ b f5290a;
            private boolean c;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0161b(b bVar, File file) {
                super(file);
                j.b(file, "rootFile");
                this.f5290a = bVar;
                if (n.f5331a) {
                    boolean isFile = file.isFile();
                    if (n.f5331a && !isFile) {
                        throw new AssertionError("rootFile must be verified to be file beforehand.");
                    }
                }
            }

            public final File a() {
                if (this.c) {
                    return null;
                }
                this.c = true;
                return this.f5292b;
            }
        }

        public final void a() {
            T t;
            while (true) {
                C0163c peek = this.d.peek();
                if (peek == null) {
                    t = null;
                    break;
                }
                t = peek.a();
                if (t != null) {
                    if (j.a(t, peek.f5292b) || !t.isDirectory() || this.d.size() >= c.this.f) {
                        break;
                    }
                    this.d.push(a(t));
                } else {
                    this.d.pop();
                }
            }
            if (t != null) {
                this.f5222b = t;
                this.f5221a = ar.Ready;
                return;
            }
            this.f5221a = ar.Done;
        }
    }
}
