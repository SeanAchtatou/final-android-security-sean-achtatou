package kotlin.io;

import java.util.Iterator;
import java.util.NoSuchElementException;
import kotlin.d.b.a.a;
import kotlin.d.b.j;

/* compiled from: ReadWrite.kt */
public final class l implements Iterator<String>, a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ k f5297a;

    /* renamed from: b  reason: collision with root package name */
    private String f5298b;
    private boolean c;

    public final void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    l(k kVar) {
        this.f5297a = kVar;
    }

    public final boolean hasNext() {
        if (this.f5298b == null && !this.c) {
            this.f5298b = this.f5297a.f5296a.readLine();
            if (this.f5298b == null) {
                this.c = true;
            }
        }
        if (this.f5298b != null) {
            return true;
        }
        return false;
    }

    public final /* synthetic */ Object next() {
        if (hasNext()) {
            String str = this.f5298b;
            this.f5298b = null;
            if (str == null) {
                j.a();
            }
            return str;
        }
        throw new NoSuchElementException();
    }
}
