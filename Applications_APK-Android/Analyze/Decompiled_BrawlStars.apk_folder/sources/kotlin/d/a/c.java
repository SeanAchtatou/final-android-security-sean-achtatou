package kotlin.d.a;

/* compiled from: Functions.kt */
public interface c<P1, P2, R> {
    R invoke(P1 p1, P2 p2);
}
