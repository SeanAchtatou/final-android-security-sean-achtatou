package kotlin.d.a;

/* compiled from: Functions.kt */
public interface d<P1, P2, P3, R> {
    R invoke(P1 p1, P2 p2, P3 p3);
}
