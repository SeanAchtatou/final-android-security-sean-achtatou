package kotlin.d.b;

import kotlin.h.c;
import kotlin.h.d;
import kotlin.h.e;
import kotlin.h.g;
import kotlin.h.i;

/* compiled from: Reflection */
public class t {

    /* renamed from: a  reason: collision with root package name */
    private static final u f5253a;

    /* renamed from: b  reason: collision with root package name */
    private static final c[] f5254b = new c[0];

    public static e a(h hVar) {
        return hVar;
    }

    public static g a(m mVar) {
        return mVar;
    }

    public static i a(q qVar) {
        return qVar;
    }

    static {
        u uVar = null;
        try {
            uVar = (u) Class.forName("kotlin.reflect.jvm.internal.ReflectionFactoryImpl").newInstance();
        } catch (ClassCastException | ClassNotFoundException | IllegalAccessException | InstantiationException unused) {
        }
        if (uVar == null) {
            uVar = new u();
        }
        f5253a = uVar;
    }

    public static String a(k kVar) {
        u uVar = f5253a;
        String obj = kVar.getClass().getGenericInterfaces()[0].toString();
        return obj.startsWith("kotlin.jvm.functions.") ? obj.substring(21) : obj;
    }

    public static d a(Class cls, String str) {
        return new o(cls, str);
    }

    public static c a(Class cls) {
        return new e(cls);
    }
}
