package kotlin.d.b;

import kotlin.h.d;

/* compiled from: PropertyReference1Impl */
public class r extends q {

    /* renamed from: a  reason: collision with root package name */
    private final d f5249a;

    /* renamed from: b  reason: collision with root package name */
    private final String f5250b;
    private final String c;

    public r(d dVar, String str, String str2) {
        this.f5249a = dVar;
        this.f5250b = str;
        this.c = str2;
    }

    public d getOwner() {
        return this.f5249a;
    }

    public String getName() {
        return this.f5250b;
    }

    public String getSignature() {
        return this.c;
    }

    public final Object a(Object obj) {
        return a().call(obj);
    }
}
