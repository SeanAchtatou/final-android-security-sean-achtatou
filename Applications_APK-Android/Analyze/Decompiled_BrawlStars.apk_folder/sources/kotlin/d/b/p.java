package kotlin.d.b;

import kotlin.h.b;
import kotlin.h.h;

/* compiled from: PropertyReference */
public abstract class p extends c implements h {
    /* access modifiers changed from: protected */
    public final h b() {
        return (h) super.getReflected();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof p) {
            p pVar = (p) obj;
            if (!getOwner().equals(pVar.getOwner()) || !getName().equals(pVar.getName()) || !getSignature().equals(pVar.getSignature()) || !j.a(getBoundReceiver(), pVar.getBoundReceiver())) {
                return false;
            }
            return true;
        } else if (obj instanceof h) {
            return obj.equals(compute());
        } else {
            return false;
        }
    }

    public int hashCode() {
        return (((getOwner().hashCode() * 31) + getName().hashCode()) * 31) + getSignature().hashCode();
    }

    public String toString() {
        b compute = compute();
        if (compute != this) {
            return compute.toString();
        }
        return "property " + getName() + " (Kotlin reflection is not available)";
    }

    /* access modifiers changed from: protected */
    public /* bridge */ /* synthetic */ b getReflected() {
        return (h) super.getReflected();
    }
}
