package kotlin.d.b;

import java.util.Collection;
import kotlin.d.b.a.a;
import kotlin.d.b.a.b;

/* compiled from: TypeIntrinsics */
public class w {
    private static <T extends Throwable> T a(Throwable th) {
        return j.a(th, w.class.getName());
    }

    public static Collection a(Object obj) {
        if (!(obj instanceof a) || (obj instanceof b)) {
            return b(obj);
        }
        String name = obj == null ? "null" : obj.getClass().getName();
        throw ((ClassCastException) a((Throwable) new ClassCastException(name + " cannot be cast to " + "kotlin.collections.MutableCollection")));
    }

    private static Collection b(Object obj) {
        try {
            return (Collection) obj;
        } catch (ClassCastException e) {
            throw ((ClassCastException) a((Throwable) e));
        }
    }
}
