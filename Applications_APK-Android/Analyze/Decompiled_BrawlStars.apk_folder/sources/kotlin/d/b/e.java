package kotlin.d.b;

import java.lang.annotation.Annotation;
import java.util.List;
import kotlin.d.a;
import kotlin.d.b;
import kotlin.h.c;

/* compiled from: ClassReference.kt */
public final class e implements d, c<Object> {

    /* renamed from: a  reason: collision with root package name */
    private final Class<?> f5241a;

    public e(Class<?> cls) {
        j.b(cls, "jClass");
        this.f5241a = cls;
    }

    public final Class<?> a() {
        return this.f5241a;
    }

    public final boolean equals(Object obj) {
        return (obj instanceof e) && j.a(a.a(this), a.a((c) obj));
    }

    public final int hashCode() {
        return a.a(this).hashCode();
    }

    public final String toString() {
        return this.f5241a.toString() + " (Kotlin reflection is not available)";
    }

    public final List<Annotation> getAnnotations() {
        throw new b();
    }
}
