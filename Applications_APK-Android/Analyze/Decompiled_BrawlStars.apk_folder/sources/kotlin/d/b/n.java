package kotlin.d.b;

import kotlin.h.d;

/* compiled from: MutablePropertyReference1Impl */
public class n extends m {

    /* renamed from: a  reason: collision with root package name */
    private final d f5245a;

    /* renamed from: b  reason: collision with root package name */
    private final String f5246b;
    private final String c;

    public n(d dVar, String str, String str2) {
        this.f5245a = dVar;
        this.f5246b = str;
        this.c = str2;
    }

    public d getOwner() {
        return this.f5245a;
    }

    public String getName() {
        return this.f5246b;
    }

    public String getSignature() {
        return this.c;
    }

    public final Object a(Object obj) {
        return a().call(obj);
    }
}
