package kotlin.d.b;

import java.io.Serializable;

/* compiled from: Lambda.kt */
public abstract class k<R> implements Serializable {
    private final int arity;

    public k(int i) {
        this.arity = i;
    }

    public int getArity() {
        return this.arity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public String toString() {
        String a2 = t.a(this);
        j.a((Object) a2, "Reflection.renderLambdaToString(this)");
        return a2;
    }
}
