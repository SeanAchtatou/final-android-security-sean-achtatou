package kotlin.d.b;

import java.util.Iterator;
import java.util.NoSuchElementException;

/* compiled from: ArrayIterator.kt */
final class a<T> implements Iterator<T>, kotlin.d.b.a.a {

    /* renamed from: a  reason: collision with root package name */
    private int f5238a;

    /* renamed from: b  reason: collision with root package name */
    private final T[] f5239b;

    public final void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public a(T[] tArr) {
        j.b(tArr, "array");
        this.f5239b = tArr;
    }

    public final boolean hasNext() {
        return this.f5238a < this.f5239b.length;
    }

    public final T next() {
        try {
            T[] tArr = this.f5239b;
            int i = this.f5238a;
            this.f5238a = i + 1;
            return tArr[i];
        } catch (ArrayIndexOutOfBoundsException e) {
            this.f5238a--;
            throw new NoSuchElementException(e.getMessage());
        }
    }
}
