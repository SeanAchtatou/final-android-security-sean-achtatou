package kotlin.d.b;

/* compiled from: PackageReference.kt */
public final class o implements d {

    /* renamed from: a  reason: collision with root package name */
    private final Class<?> f5247a;

    /* renamed from: b  reason: collision with root package name */
    private final String f5248b;

    public o(Class<?> cls, String str) {
        j.b(cls, "jClass");
        j.b(str, "moduleName");
        this.f5247a = cls;
        this.f5248b = str;
    }

    public final Class<?> a() {
        return this.f5247a;
    }

    public final boolean equals(Object obj) {
        return (obj instanceof o) && j.a(this.f5247a, ((o) obj).f5247a);
    }

    public final String toString() {
        return this.f5247a.toString() + " (Kotlin reflection is not available)";
    }

    public final int hashCode() {
        return this.f5247a.hashCode();
    }
}
