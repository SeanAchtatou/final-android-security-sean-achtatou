package kotlin.j;

import java.nio.charset.Charset;
import kotlin.d.b.j;

/* compiled from: Charsets.kt */
public final class d {

    /* renamed from: a  reason: collision with root package name */
    public static final Charset f5307a;

    /* renamed from: b  reason: collision with root package name */
    public static final Charset f5308b;
    public static final Charset c;
    public static final Charset d;
    public static final Charset e;
    public static final Charset f;
    public static final d g = new d();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.nio.charset.Charset, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    static {
        Charset forName = Charset.forName("UTF-8");
        j.a((Object) forName, "Charset.forName(\"UTF-8\")");
        f5307a = forName;
        Charset forName2 = Charset.forName("UTF-16");
        j.a((Object) forName2, "Charset.forName(\"UTF-16\")");
        f5308b = forName2;
        Charset forName3 = Charset.forName("UTF-16BE");
        j.a((Object) forName3, "Charset.forName(\"UTF-16BE\")");
        c = forName3;
        Charset forName4 = Charset.forName("UTF-16LE");
        j.a((Object) forName4, "Charset.forName(\"UTF-16LE\")");
        d = forName4;
        Charset forName5 = Charset.forName("US-ASCII");
        j.a((Object) forName5, "Charset.forName(\"US-ASCII\")");
        e = forName5;
        Charset forName6 = Charset.forName("ISO-8859-1");
        j.a((Object) forName6, "Charset.forName(\"ISO-8859-1\")");
        f = forName6;
    }

    private d() {
    }
}
