package kotlin.j;

import kotlin.d.a.c;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.h;

/* compiled from: Strings.kt */
public final class ad extends k implements c<CharSequence, Integer, h<? extends Integer, ? extends Integer>> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ char[] f5301a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ boolean f5302b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ad(char[] cArr, boolean z) {
        super(2);
        this.f5301a = cArr;
        this.f5302b = z;
    }

    public final /* synthetic */ Object invoke(Object obj, Object obj2) {
        CharSequence charSequence = (CharSequence) obj;
        int intValue = ((Number) obj2).intValue();
        j.b(charSequence, "$receiver");
        int a2 = t.a(charSequence, this.f5301a, intValue, this.f5302b);
        if (a2 < 0) {
            return null;
        }
        return kotlin.k.a(Integer.valueOf(a2), 1);
    }
}
