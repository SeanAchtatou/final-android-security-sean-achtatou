package kotlin.j;

/* compiled from: Regex.kt */
public enum s {
    IGNORE_CASE(2, 0, 2),
    MULTILINE(8, 0, 2),
    LITERAL(16, 0, 2),
    UNIX_LINES(1, 0, 2),
    COMMENTS(4, 0, 2),
    DOT_MATCHES_ALL(32, 0, 2),
    CANON_EQ(128, 0, 2);
    
    final int h;
    private final int j;

    private s(int i2, int i3) {
        this.h = i2;
        this.j = i3;
    }
}
