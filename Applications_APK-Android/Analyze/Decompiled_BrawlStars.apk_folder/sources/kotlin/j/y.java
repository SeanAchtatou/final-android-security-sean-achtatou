package kotlin.j;

import kotlin.d.a.b;
import kotlin.d.b.j;

/* compiled from: StringBuilder.kt */
public class y extends x {
    public static final <T> void a(Appendable appendable, Object obj, b bVar) {
        j.b(appendable, "$this$appendElement");
        if (bVar != null) {
            appendable.append((CharSequence) bVar.invoke(obj));
            return;
        }
        if (obj != null ? obj instanceof CharSequence : true) {
            appendable.append((CharSequence) obj);
        } else if (obj instanceof Character) {
            appendable.append(((Character) obj).charValue());
        } else {
            appendable.append(String.valueOf(obj));
        }
    }
}
