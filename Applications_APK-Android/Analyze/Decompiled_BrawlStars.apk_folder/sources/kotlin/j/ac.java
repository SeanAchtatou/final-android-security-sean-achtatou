package kotlin.j;

import com.facebook.internal.FacebookRequestErrorClassification;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;
import kotlin.a.g;
import kotlin.a.m;
import kotlin.d.b.j;
import kotlin.g.a;
import kotlin.g.c;
import kotlin.g.d;
import kotlin.i.h;
import kotlin.i.i;
import kotlin.i.p;

/* compiled from: Strings.kt */
public class ac extends ab {
    public static final c c(CharSequence charSequence) {
        j.b(charSequence, "$this$indices");
        return new c(0, charSequence.length() - 1);
    }

    public static final int d(CharSequence charSequence) {
        j.b(charSequence, "$this$lastIndex");
        return charSequence.length() - 1;
    }

    public static final String a(CharSequence charSequence, c cVar) {
        j.b(charSequence, "$this$substring");
        j.b(cVar, "range");
        return charSequence.subSequence(cVar.b().intValue(), Integer.valueOf(cVar.f5259b).intValue() + 1).toString();
    }

    public static final boolean a(CharSequence charSequence, int i, CharSequence charSequence2, int i2, int i3, boolean z) {
        j.b(charSequence, "$this$regionMatchesImpl");
        j.b(charSequence2, FacebookRequestErrorClassification.KEY_OTHER);
        if (i2 < 0 || charSequence.length() - i3 < 0 || i2 > charSequence2.length() - i3) {
            return false;
        }
        for (int i4 = 0; i4 < i3; i4++) {
            if (!a.a(charSequence.charAt(i4 + 0), charSequence2.charAt(i2 + i4), z)) {
                return false;
            }
        }
        return true;
    }

    public static final int a(CharSequence charSequence, char[] cArr, int i, boolean z) {
        boolean z2;
        j.b(charSequence, "$this$indexOfAny");
        j.b(cArr, "chars");
        if (z || cArr.length != 1 || !(charSequence instanceof String)) {
            int c = d.c(i, 0);
            int d = t.d(charSequence);
            if (c > d) {
                return -1;
            }
            while (true) {
                char charAt = charSequence.charAt(c);
                int length = cArr.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        z2 = false;
                        break;
                    } else if (a.a(cArr[i2], charAt, z)) {
                        z2 = true;
                        break;
                    } else {
                        i2++;
                    }
                }
                if (z2) {
                    return c;
                }
                if (c == d) {
                    return -1;
                }
                c++;
            }
        } else {
            j.b(cArr, "$this$single");
            int length2 = cArr.length;
            if (length2 == 0) {
                throw new NoSuchElementException("Array is empty.");
            } else if (length2 == 1) {
                return ((String) charSequence).indexOf(cArr[0], i);
            } else {
                throw new IllegalArgumentException("Array has more than one element.");
            }
        }
    }

    /* access modifiers changed from: private */
    public static final int a(CharSequence charSequence, CharSequence charSequence2, int i, int i2, boolean z, boolean z2) {
        a aVar;
        if (!z2) {
            aVar = new c(d.c(i, 0), d.d(i2, charSequence.length()));
        } else {
            aVar = d.a(d.d(i, t.d(charSequence)), d.c(i2, 0));
        }
        if (!(charSequence instanceof String) || !(charSequence2 instanceof String)) {
            int i3 = aVar.f5258a;
            int i4 = aVar.f5259b;
            int i5 = aVar.c;
            if (i5 >= 0) {
                if (i3 > i4) {
                    return -1;
                }
            } else if (i3 < i4) {
                return -1;
            }
            while (true) {
                if (t.a(charSequence2, 0, charSequence, i3, charSequence2.length(), z)) {
                    return i3;
                }
                if (i3 == i4) {
                    return -1;
                }
                i3 += i5;
            }
        } else {
            int i6 = aVar.f5258a;
            int i7 = aVar.f5259b;
            int i8 = aVar.c;
            if (i8 >= 0) {
                if (i6 > i7) {
                    return -1;
                }
            } else if (i6 < i7) {
                return -1;
            }
            while (true) {
                if (t.a((String) charSequence2, 0, (String) charSequence, i6, charSequence2.length(), z)) {
                    return i6;
                }
                if (i6 == i7) {
                    return -1;
                }
                i6 += i8;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.j.ac.a(java.lang.CharSequence, char, int, boolean):int
     arg types: [java.lang.CharSequence, char, int, int]
     candidates:
      kotlin.j.ac.a(java.lang.CharSequence, java.lang.String, int, boolean):int
      kotlin.j.ac.a(java.lang.CharSequence, char[], int, boolean):int
      kotlin.j.ac.a(java.lang.CharSequence, java.lang.String, boolean, int):java.util.List<java.lang.String>
      kotlin.j.ac.a(java.lang.CharSequence, java.lang.String[], boolean, int):kotlin.i.h<java.lang.String>
      kotlin.j.ab.a(java.lang.String, java.lang.String, java.lang.String, boolean):java.lang.String
      kotlin.j.ac.a(java.lang.CharSequence, char, int, boolean):int */
    public static /* synthetic */ int a(CharSequence charSequence, char c, int i, boolean z, int i2) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        return t.a(charSequence, c, i, false);
    }

    public static final int a(CharSequence charSequence, char c, int i, boolean z) {
        j.b(charSequence, "$this$indexOf");
        if (!z && (charSequence instanceof String)) {
            return ((String) charSequence).indexOf(c, i);
        }
        return t.a(charSequence, new char[]{c}, i, z);
    }

    public static /* synthetic */ int a(CharSequence charSequence, String str, int i, boolean z, int i2) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return t.a(charSequence, str, i, z);
    }

    public static final int a(CharSequence charSequence, String str, int i, boolean z) {
        j.b(charSequence, "$this$indexOf");
        j.b(str, "string");
        if (!z && (charSequence instanceof String)) {
            return ((String) charSequence).indexOf(str, i);
        }
        return a(charSequence, str, i, charSequence.length(), z, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.j.ac.b(java.lang.CharSequence, java.lang.String, int, boolean):int
     arg types: [java.lang.CharSequence, java.lang.String, int, int]
     candidates:
      kotlin.j.ac.b(java.lang.CharSequence, java.lang.String[], boolean, int):java.util.List<java.lang.String>
      kotlin.j.ac.b(java.lang.CharSequence, java.lang.String, int, boolean):int */
    public static /* synthetic */ int b(CharSequence charSequence, String str, int i, boolean z, int i2) {
        if ((i2 & 2) != 0) {
            i = t.d(charSequence);
        }
        return t.b(charSequence, str, i, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.j.ac.a(java.lang.CharSequence, java.lang.CharSequence, int, int, boolean, boolean):int
     arg types: [java.lang.CharSequence, java.lang.String, int, int, boolean, int]
     candidates:
      kotlin.j.ac.a(java.lang.CharSequence, java.lang.String[], int, boolean, int, int):kotlin.i.h
      kotlin.j.ac.a(java.lang.CharSequence, int, java.lang.CharSequence, int, int, boolean):boolean
      kotlin.j.ab.a(java.lang.String, int, java.lang.String, int, int, boolean):boolean
      kotlin.j.ac.a(java.lang.CharSequence, java.lang.CharSequence, int, int, boolean, boolean):int */
    public static final int b(CharSequence charSequence, String str, int i, boolean z) {
        j.b(charSequence, "$this$lastIndexOf");
        j.b(str, "string");
        if (!z && (charSequence instanceof String)) {
            return ((String) charSequence).lastIndexOf(str, i);
        }
        return a(charSequence, (CharSequence) str, i, 0, z, true);
    }

    public static final boolean a(CharSequence charSequence, CharSequence charSequence2, boolean z) {
        j.b(charSequence, "$this$contains");
        j.b(charSequence2, FacebookRequestErrorClassification.KEY_OTHER);
        if (!(charSequence2 instanceof String)) {
            if (a(charSequence, charSequence2, 0, charSequence.length(), z, false) >= 0) {
                return true;
            }
        } else if (t.a(charSequence, (String) charSequence2, 0, z, 2) >= 0) {
            return true;
        }
        return false;
    }

    public static /* synthetic */ h a(CharSequence charSequence, String[] strArr, boolean z, int i, int i2) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return t.a(charSequence, strArr, z, i);
    }

    public static final h<String> a(CharSequence charSequence, String[] strArr, boolean z, int i) {
        j.b(charSequence, "$this$splitToSequence");
        j.b(strArr, "delimiters");
        return i.b(a(charSequence, strArr, 0, z, i, 2), new af(charSequence));
    }

    public static final List<String> b(CharSequence charSequence, String[] strArr, boolean z, int i) {
        j.b(charSequence, "$this$split");
        j.b(strArr, "delimiters");
        if (strArr.length == 1) {
            boolean z2 = false;
            String str = strArr[0];
            if (str.length() == 0) {
                z2 = true;
            }
            if (!z2) {
                return a(charSequence, str, z, i);
            }
        }
        h a2 = a(charSequence, strArr, 0, z, i, 2);
        j.b(a2, "$this$asIterable");
        Iterable<c> pVar = new p(a2);
        Collection arrayList = new ArrayList(m.a(pVar, 10));
        for (c a3 : pVar) {
            arrayList.add(t.a(charSequence, a3));
        }
        return (List) arrayList;
    }

    public static final List<String> a(CharSequence charSequence, String str, boolean z, int i) {
        int i2 = 0;
        if (i >= 0) {
            int a2 = t.a(charSequence, str, 0, z);
            if (a2 == -1 || i == 1) {
                return m.a(charSequence.toString());
            }
            boolean z2 = i > 0;
            int i3 = 10;
            if (z2) {
                i3 = d.d(i, 10);
            }
            ArrayList arrayList = new ArrayList(i3);
            do {
                arrayList.add(charSequence.subSequence(i2, a2).toString());
                i2 = str.length() + a2;
                if (z2 && arrayList.size() == i - 1) {
                    break;
                }
                a2 = t.a(charSequence, str, i2, z);
            } while (a2 != -1);
            arrayList.add(charSequence.subSequence(i2, charSequence.length()).toString());
            return arrayList;
        }
        throw new IllegalArgumentException(("Limit must be non-negative, but was " + i + '.').toString());
    }

    public static final String a(String str, char... cArr) {
        CharSequence charSequence;
        j.b(str, "$this$trimStart");
        j.b(cArr, "chars");
        CharSequence charSequence2 = str;
        int length = charSequence2.length();
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            } else if (!g.a(cArr, charSequence2.charAt(i))) {
                charSequence = charSequence2.subSequence(i, charSequence2.length());
                break;
            } else {
                i++;
            }
        }
        return charSequence.toString();
    }

    public static final String b(String str, char... cArr) {
        CharSequence charSequence;
        j.b(str, "$this$trimEnd");
        j.b(cArr, "chars");
        CharSequence charSequence2 = str;
        int length = charSequence2.length();
        while (true) {
            length--;
            if (length >= 0) {
                if (!g.a(cArr, charSequence2.charAt(length))) {
                    charSequence = charSequence2.subSequence(0, length + 1);
                    break;
                }
            } else {
                break;
            }
        }
        return charSequence.toString();
    }

    public static final CharSequence b(CharSequence charSequence) {
        j.b(charSequence, "$this$trim");
        int length = charSequence.length() - 1;
        int i = 0;
        boolean z = false;
        while (i <= length) {
            boolean a2 = a.a(charSequence.charAt(!z ? i : length));
            if (!z) {
                if (!a2) {
                    z = true;
                } else {
                    i++;
                }
            } else if (!a2) {
                break;
            } else {
                length--;
            }
        }
        return charSequence.subSequence(i, length + 1);
    }

    private static /* synthetic */ h a(CharSequence charSequence, String[] strArr, int i, boolean z, int i2, int i3) {
        if (i2 >= 0) {
            return new e(charSequence, 0, i2, new ae(g.a(strArr), z));
        }
        throw new IllegalArgumentException(("Limit must be non-negative, but was " + i2 + '.').toString());
    }
}
