package kotlin.j;

import kotlin.d.a.b;
import kotlin.d.b.h;
import kotlin.d.b.j;
import kotlin.d.b.t;
import kotlin.h.d;

/* compiled from: Regex.kt */
final /* synthetic */ class q extends h implements b<j, j> {

    /* renamed from: a  reason: collision with root package name */
    public static final q f5326a = new q();

    q() {
        super(1);
    }

    public final String getName() {
        return "next";
    }

    public final d getOwner() {
        return t.a(j.class);
    }

    public final String getSignature() {
        return "next()Lkotlin/text/MatchResult;";
    }

    public final /* synthetic */ Object invoke(Object obj) {
        j jVar = (j) obj;
        j.b(jVar, "p1");
        return jVar.c();
    }
}
