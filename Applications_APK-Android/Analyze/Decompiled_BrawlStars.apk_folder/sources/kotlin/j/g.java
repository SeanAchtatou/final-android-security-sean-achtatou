package kotlin.j;

import kotlin.d.b.j;
import kotlin.g.c;

/* compiled from: Regex.kt */
public final class g {

    /* renamed from: a  reason: collision with root package name */
    private final String f5313a;

    /* renamed from: b  reason: collision with root package name */
    private final c f5314b;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof g)) {
            return false;
        }
        g gVar = (g) obj;
        return j.a(this.f5313a, gVar.f5313a) && j.a(this.f5314b, gVar.f5314b);
    }

    public final int hashCode() {
        String str = this.f5313a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        c cVar = this.f5314b;
        if (cVar != null) {
            i = cVar.hashCode();
        }
        return hashCode + i;
    }

    public final String toString() {
        return "MatchGroup(value=" + this.f5313a + ", range=" + this.f5314b + ")";
    }

    public g(String str, c cVar) {
        j.b(str, "value");
        j.b(cVar, "range");
        this.f5313a = str;
        this.f5314b = cVar;
    }
}
