package kotlin.j;

import java.util.regex.MatchResult;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.g.c;
import kotlin.g.d;

/* compiled from: Regex.kt */
final class n extends k implements b<Integer, g> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ m f5319a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    n(m mVar) {
        super(1);
        this.f5319a = mVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final /* synthetic */ Object invoke(Object obj) {
        int intValue = ((Number) obj).intValue();
        m mVar = this.f5319a;
        MatchResult a2 = mVar.f5318a.c;
        c b2 = d.b(a2.start(intValue), a2.end(intValue));
        if (b2.b().intValue() < 0) {
            return null;
        }
        String group = mVar.f5318a.c.group(intValue);
        j.a((Object) group, "matchResult.group(index)");
        return new g(group, b2);
    }
}
