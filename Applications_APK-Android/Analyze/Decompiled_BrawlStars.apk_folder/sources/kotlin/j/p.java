package kotlin.j;

import java.util.regex.Matcher;
import kotlin.d.a.a;
import kotlin.d.b.j;
import kotlin.d.b.k;

/* compiled from: Regex.kt */
final class p extends k implements a<j> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ o f5324a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ CharSequence f5325b;
    final /* synthetic */ int c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    p(o oVar, CharSequence charSequence, int i) {
        super(0);
        this.f5324a = oVar;
        this.f5325b = charSequence;
        this.c = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.regex.Matcher, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final /* synthetic */ Object invoke() {
        o oVar = this.f5324a;
        CharSequence charSequence = this.f5325b;
        int i = this.c;
        j.b(charSequence, "input");
        Matcher matcher = oVar.f5321a.matcher(charSequence);
        j.a((Object) matcher, "nativePattern.matcher(input)");
        return r.a(matcher, i, charSequence);
    }
}
