package kotlin.j;

import java.util.List;
import kotlin.d.a.c;
import kotlin.d.b.k;
import kotlin.h;

/* compiled from: Strings.kt */
final class ae extends k implements c<CharSequence, Integer, h<? extends Integer, ? extends Integer>> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ List f5303a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ boolean f5304b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ae(List list, boolean z) {
        super(2);
        this.f5303a = list;
        this.f5304b = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.j.ac.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int
     arg types: [java.lang.CharSequence, java.lang.String, int, int, int]
     candidates:
      kotlin.j.ac.a(java.lang.CharSequence, char, int, boolean, int):int
      kotlin.j.ac.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):kotlin.i.h
      kotlin.j.ac.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x012f  */
    /* JADX WARNING: Removed duplicated region for block: B:72:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object invoke(java.lang.Object r14, java.lang.Object r15) {
        /*
            r13 = this;
            java.lang.CharSequence r14 = (java.lang.CharSequence) r14
            java.lang.Number r15 = (java.lang.Number) r15
            int r15 = r15.intValue()
            java.lang.String r0 = "$receiver"
            kotlin.d.b.j.b(r14, r0)
            java.util.List r0 = r13.f5303a
            r6 = r0
            java.util.Collection r6 = (java.util.Collection) r6
            boolean r7 = r13.f5304b
            r0 = 0
            r8 = 0
            if (r7 != 0) goto L_0x008e
            int r1 = r6.size()
            r2 = 1
            if (r1 != r2) goto L_0x008e
            java.lang.Iterable r6 = (java.lang.Iterable) r6
            java.lang.String r1 = "$this$single"
            kotlin.d.b.j.b(r6, r1)
            boolean r3 = r6 instanceof java.util.List
            if (r3 == 0) goto L_0x0050
            java.util.List r6 = (java.util.List) r6
            kotlin.d.b.j.b(r6, r1)
            int r1 = r6.size()
            if (r1 == 0) goto L_0x0046
            if (r1 != r2) goto L_0x003c
            java.lang.Object r1 = r6.get(r0)
            goto L_0x0065
        L_0x003c:
            java.lang.IllegalArgumentException r14 = new java.lang.IllegalArgumentException
            java.lang.String r15 = "List has more than one element."
            r14.<init>(r15)
            java.lang.Throwable r14 = (java.lang.Throwable) r14
            throw r14
        L_0x0046:
            java.util.NoSuchElementException r14 = new java.util.NoSuchElementException
            java.lang.String r15 = "List is empty."
            r14.<init>(r15)
            java.lang.Throwable r14 = (java.lang.Throwable) r14
            throw r14
        L_0x0050:
            java.util.Iterator r1 = r6.iterator()
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x0084
            java.lang.Object r2 = r1.next()
            boolean r1 = r1.hasNext()
            if (r1 != 0) goto L_0x007a
            r1 = r2
        L_0x0065:
            java.lang.String r1 = (java.lang.String) r1
            r2 = 4
            int r14 = kotlin.j.t.a(r14, r1, r15, r0, r2)
            if (r14 >= 0) goto L_0x0070
            goto L_0x012c
        L_0x0070:
            java.lang.Integer r14 = java.lang.Integer.valueOf(r14)
            kotlin.h r14 = kotlin.k.a(r14, r1)
            goto L_0x012d
        L_0x007a:
            java.lang.IllegalArgumentException r14 = new java.lang.IllegalArgumentException
            java.lang.String r15 = "Collection has more than one element."
            r14.<init>(r15)
            java.lang.Throwable r14 = (java.lang.Throwable) r14
            throw r14
        L_0x0084:
            java.util.NoSuchElementException r14 = new java.util.NoSuchElementException
            java.lang.String r15 = "Collection is empty."
            r14.<init>(r15)
            java.lang.Throwable r14 = (java.lang.Throwable) r14
            throw r14
        L_0x008e:
            int r15 = kotlin.g.d.c(r15, r0)
            kotlin.g.c r0 = new kotlin.g.c
            int r1 = r14.length()
            r0.<init>(r15, r1)
            kotlin.g.a r0 = (kotlin.g.a) r0
            boolean r15 = r14 instanceof java.lang.String
            if (r15 == 0) goto L_0x00e5
            int r15 = r0.f5258a
            int r9 = r0.f5259b
            int r10 = r0.c
            if (r10 < 0) goto L_0x00ac
            if (r15 > r9) goto L_0x012c
            goto L_0x00ae
        L_0x00ac:
            if (r15 < r9) goto L_0x012c
        L_0x00ae:
            r0 = r6
            java.lang.Iterable r0 = (java.lang.Iterable) r0
            java.util.Iterator r11 = r0.iterator()
        L_0x00b5:
            boolean r0 = r11.hasNext()
            if (r0 == 0) goto L_0x00d3
            java.lang.Object r12 = r11.next()
            r0 = r12
            java.lang.String r0 = (java.lang.String) r0
            r1 = 0
            r2 = r14
            java.lang.String r2 = (java.lang.String) r2
            int r4 = r0.length()
            r3 = r15
            r5 = r7
            boolean r0 = kotlin.j.t.a(r0, r1, r2, r3, r4, r5)
            if (r0 == 0) goto L_0x00b5
            goto L_0x00d4
        L_0x00d3:
            r12 = r8
        L_0x00d4:
            java.lang.String r12 = (java.lang.String) r12
            if (r12 == 0) goto L_0x00e1
            java.lang.Integer r14 = java.lang.Integer.valueOf(r15)
            kotlin.h r14 = kotlin.k.a(r14, r12)
            goto L_0x012d
        L_0x00e1:
            if (r15 == r9) goto L_0x012c
            int r15 = r15 + r10
            goto L_0x00ae
        L_0x00e5:
            int r15 = r0.f5258a
            int r9 = r0.f5259b
            int r10 = r0.c
            if (r10 < 0) goto L_0x00f0
            if (r15 > r9) goto L_0x012c
            goto L_0x00f2
        L_0x00f0:
            if (r15 < r9) goto L_0x012c
        L_0x00f2:
            r0 = r6
            java.lang.Iterable r0 = (java.lang.Iterable) r0
            java.util.Iterator r11 = r0.iterator()
        L_0x00f9:
            boolean r0 = r11.hasNext()
            if (r0 == 0) goto L_0x011a
            java.lang.Object r12 = r11.next()
            r0 = r12
            java.lang.String r0 = (java.lang.String) r0
            r1 = r0
            java.lang.CharSequence r1 = (java.lang.CharSequence) r1
            r2 = 0
            int r4 = r0.length()
            r0 = r1
            r1 = r2
            r2 = r14
            r3 = r15
            r5 = r7
            boolean r0 = kotlin.j.t.a(r0, r1, r2, r3, r4, r5)
            if (r0 == 0) goto L_0x00f9
            goto L_0x011b
        L_0x011a:
            r12 = r8
        L_0x011b:
            java.lang.String r12 = (java.lang.String) r12
            if (r12 == 0) goto L_0x0128
            java.lang.Integer r14 = java.lang.Integer.valueOf(r15)
            kotlin.h r14 = kotlin.k.a(r14, r12)
            goto L_0x012d
        L_0x0128:
            if (r15 == r9) goto L_0x012c
            int r15 = r15 + r10
            goto L_0x00f2
        L_0x012c:
            r14 = r8
        L_0x012d:
            if (r14 == 0) goto L_0x0141
            A r15 = r14.f5262a
            B r14 = r14.f5263b
            java.lang.String r14 = (java.lang.String) r14
            int r14 = r14.length()
            java.lang.Integer r14 = java.lang.Integer.valueOf(r14)
            kotlin.h r8 = kotlin.k.a(r15, r14)
        L_0x0141:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlin.j.ae.invoke(java.lang.Object, java.lang.Object):java.lang.Object");
    }
}
