package kotlin.j;

import com.facebook.internal.FacebookRequestErrorClassification;
import java.util.Iterator;
import kotlin.a.ah;
import kotlin.d.b.j;
import kotlin.i.i;

/* compiled from: StringsJVM.kt */
public class ab extends aa {
    public static final boolean a(String str, String str2, boolean z) {
        if (str == null) {
            return str2 == null;
        }
        return str.equalsIgnoreCase(str2);
    }

    public static final String a(String str, String str2, String str3, boolean z) {
        j.b(str, "$this$replace");
        j.b(str2, "oldValue");
        j.b(str3, "newValue");
        return i.a(t.a(str, new String[]{str2}, z, 0, 4), str3, "", "", -1, "...", null);
    }

    public static final boolean b(String str, String str2, boolean z) {
        j.b(str, "$this$startsWith");
        j.b(str2, "prefix");
        if (!z) {
            return str.startsWith(str2);
        }
        return t.a(str, 0, str2, 0, str2.length(), z);
    }

    public static final int c(String str, String str2, boolean z) {
        j.b(str, "$this$compareTo");
        j.b(str2, FacebookRequestErrorClassification.KEY_OTHER);
        return str.compareToIgnoreCase(str2);
    }

    public static final boolean a(CharSequence charSequence) {
        boolean z;
        j.b(charSequence, "$this$isBlank");
        if (charSequence.length() != 0) {
            Iterator it = t.c(charSequence).iterator();
            while (true) {
                if (it.hasNext()) {
                    if (!a.a(charSequence.charAt(((ah) it).a()))) {
                        z = false;
                        break;
                    }
                } else {
                    z = true;
                    break;
                }
            }
            if (z) {
                return true;
            }
            return false;
        }
        return true;
    }

    public static final boolean a(String str, int i, String str2, int i2, int i3, boolean z) {
        j.b(str, "$this$regionMatches");
        j.b(str2, FacebookRequestErrorClassification.KEY_OTHER);
        if (!z) {
            return str.regionMatches(0, str2, i2, i3);
        }
        return str.regionMatches(z, 0, str2, i2, i3);
    }
}
