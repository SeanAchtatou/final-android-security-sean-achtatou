package kotlin.j;

import kotlin.d.b.j;

/* compiled from: StringNumberConversions.kt */
public class aa extends z {
    public static final Integer a(String str) {
        j.b(str, "$this$toIntOrNull");
        return t.a(str, 10);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Character.digit(int, int):int}
     arg types: [char, int]
     candidates:
      ClspMth{java.lang.Character.digit(char, int):int}
      ClspMth{java.lang.Character.digit(int, int):int} */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0037 A[LOOP:0: B:18:0x0037->B:29:0x0052, LOOP_START, PHI: r2 r3 
      PHI: (r2v2 int) = (r2v0 int), (r2v4 int) binds: [B:17:0x0035, B:29:0x0052] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r3v2 int) = (r3v1 int), (r3v3 int) binds: [B:17:0x0035, B:29:0x0052] A[DONT_GENERATE, DONT_INLINE]] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x005c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.Integer a(java.lang.String r8, int r9) {
        /*
            java.lang.String r9 = "$this$toIntOrNull"
            kotlin.d.b.j.b(r8, r9)
            r9 = 10
            kotlin.j.a.a(r9)
            int r0 = r8.length()
            r1 = 0
            if (r0 != 0) goto L_0x0012
            return r1
        L_0x0012:
            r2 = 0
            char r3 = r8.charAt(r2)
            r4 = 48
            r5 = -2147483647(0xffffffff80000001, float:-1.4E-45)
            r6 = 1
            if (r3 >= r4) goto L_0x0032
            if (r0 != r6) goto L_0x0022
            return r1
        L_0x0022:
            r4 = 45
            if (r3 != r4) goto L_0x002b
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = 1
            r4 = 1
            goto L_0x0034
        L_0x002b:
            r4 = 43
            if (r3 != r4) goto L_0x0031
            r3 = 1
            goto L_0x0033
        L_0x0031:
            return r1
        L_0x0032:
            r3 = 0
        L_0x0033:
            r4 = 0
        L_0x0034:
            int r0 = r0 - r6
            if (r3 > r0) goto L_0x0055
        L_0x0037:
            char r6 = r8.charAt(r3)
            int r6 = java.lang.Character.digit(r6, r9)
            if (r6 >= 0) goto L_0x0042
            return r1
        L_0x0042:
            r7 = -214748364(0xfffffffff3333334, float:-1.4197688E31)
            if (r2 >= r7) goto L_0x0048
            return r1
        L_0x0048:
            int r2 = r2 * 10
            int r7 = r5 + r6
            if (r2 >= r7) goto L_0x004f
            return r1
        L_0x004f:
            int r2 = r2 - r6
            if (r3 == r0) goto L_0x0055
            int r3 = r3 + 1
            goto L_0x0037
        L_0x0055:
            if (r4 == 0) goto L_0x005c
            java.lang.Integer r8 = java.lang.Integer.valueOf(r2)
            goto L_0x0061
        L_0x005c:
            int r8 = -r2
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
        L_0x0061:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlin.j.aa.a(java.lang.String, int):java.lang.Integer");
    }
}
