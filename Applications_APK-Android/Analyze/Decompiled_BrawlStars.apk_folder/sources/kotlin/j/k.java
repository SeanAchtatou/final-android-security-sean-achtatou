package kotlin.j;

import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import kotlin.d.b.j;
import kotlin.g.c;
import kotlin.g.d;

/* compiled from: Regex.kt */
public final class k implements j {

    /* renamed from: a  reason: collision with root package name */
    private final h f5315a = new m(this);

    /* renamed from: b  reason: collision with root package name */
    private List<String> f5316b;
    /* access modifiers changed from: private */
    public final Matcher c;
    private final CharSequence d;

    public k(Matcher matcher, CharSequence charSequence) {
        j.b(matcher, "matcher");
        j.b(charSequence, "input");
        this.c = matcher;
        this.d = charSequence;
    }

    public final List<String> b() {
        if (this.f5316b == null) {
            this.f5316b = new l(this);
        }
        List<String> list = this.f5316b;
        if (list == null) {
            j.a();
        }
        return list;
    }

    public final c a() {
        MatchResult matchResult = this.c;
        return d.b(matchResult.start(), matchResult.end());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.regex.Matcher, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final j c() {
        int end = this.c.end() + (this.c.end() == this.c.start() ? 1 : 0);
        if (end > this.d.length()) {
            return null;
        }
        Matcher matcher = this.c.pattern().matcher(this.d);
        j.a((Object) matcher, "matcher.pattern().matcher(input)");
        return r.a(matcher, end, this.d);
    }
}
