package kotlin.j;

import kotlin.a.e;

/* compiled from: Regex.kt */
public final class l extends e<String> {

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ k f5317b;

    l(k kVar) {
        this.f5317b = kVar;
    }

    public final /* bridge */ boolean contains(Object obj) {
        if (obj instanceof String) {
            return super.contains((String) obj);
        }
        return false;
    }

    public final /* bridge */ int indexOf(Object obj) {
        if (obj instanceof String) {
            return super.indexOf((String) obj);
        }
        return -1;
    }

    public final /* bridge */ int lastIndexOf(Object obj) {
        if (obj instanceof String) {
            return super.lastIndexOf((String) obj);
        }
        return -1;
    }

    public final int a() {
        return this.f5317b.c.groupCount() + 1;
    }

    public final /* synthetic */ Object get(int i) {
        String group = this.f5317b.c.group(i);
        return group != null ? group : "";
    }
}
