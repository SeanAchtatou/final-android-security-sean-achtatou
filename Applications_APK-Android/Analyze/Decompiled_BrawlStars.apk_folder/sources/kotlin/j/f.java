package kotlin.j;

import java.util.Iterator;
import java.util.NoSuchElementException;
import kotlin.TypeCastException;
import kotlin.d.b.a.a;
import kotlin.g.c;

/* compiled from: Strings.kt */
public final class f implements Iterator<c>, a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ e f5311a;

    /* renamed from: b  reason: collision with root package name */
    private int f5312b = -1;
    private int c;
    private int d;
    private c e;
    private int f;

    public final void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    f(e eVar) {
        this.f5311a = eVar;
        int i = eVar.f5310b;
        int length = eVar.f5309a.length();
        if (length >= 0) {
            if (i < 0) {
                length = 0;
            } else if (i <= length) {
                length = i;
            }
            this.c = length;
            this.d = this.c;
            return;
        }
        throw new IllegalArgumentException("Cannot coerce value to an empty range: maximum " + length + " is less than minimum " + 0 + '.');
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001f, code lost:
        if (r6.f < r6.f5311a.c) goto L_0x0021;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void a() {
        /*
            r6 = this;
            int r0 = r6.d
            r1 = 0
            if (r0 >= 0) goto L_0x000c
            r6.f5312b = r1
            r0 = 0
            r6.e = r0
            goto L_0x0090
        L_0x000c:
            kotlin.j.e r0 = r6.f5311a
            int r0 = r0.c
            r2 = -1
            r3 = 1
            if (r0 <= 0) goto L_0x0021
            int r0 = r6.f
            int r0 = r0 + r3
            r6.f = r0
            int r0 = r6.f
            kotlin.j.e r4 = r6.f5311a
            int r4 = r4.c
            if (r0 >= r4) goto L_0x002d
        L_0x0021:
            int r0 = r6.d
            kotlin.j.e r4 = r6.f5311a
            java.lang.CharSequence r4 = r4.f5309a
            int r4 = r4.length()
            if (r0 <= r4) goto L_0x0041
        L_0x002d:
            int r0 = r6.c
            kotlin.g.c r1 = new kotlin.g.c
            kotlin.j.e r4 = r6.f5311a
            java.lang.CharSequence r4 = r4.f5309a
            int r4 = kotlin.j.t.d(r4)
            r1.<init>(r0, r4)
            r6.e = r1
            r6.d = r2
            goto L_0x008e
        L_0x0041:
            kotlin.j.e r0 = r6.f5311a
            kotlin.d.a.c<java.lang.CharSequence, java.lang.Integer, kotlin.h<java.lang.Integer, java.lang.Integer>> r0 = r0.d
            kotlin.j.e r4 = r6.f5311a
            java.lang.CharSequence r4 = r4.f5309a
            int r5 = r6.d
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            java.lang.Object r0 = r0.invoke(r4, r5)
            kotlin.h r0 = (kotlin.h) r0
            if (r0 != 0) goto L_0x006b
            int r0 = r6.c
            kotlin.g.c r1 = new kotlin.g.c
            kotlin.j.e r4 = r6.f5311a
            java.lang.CharSequence r4 = r4.f5309a
            int r4 = kotlin.j.t.d(r4)
            r1.<init>(r0, r4)
            r6.e = r1
            r6.d = r2
            goto L_0x008e
        L_0x006b:
            A r2 = r0.f5262a
            java.lang.Number r2 = (java.lang.Number) r2
            int r2 = r2.intValue()
            B r0 = r0.f5263b
            java.lang.Number r0 = (java.lang.Number) r0
            int r0 = r0.intValue()
            int r4 = r6.c
            kotlin.g.c r4 = kotlin.g.d.b(r4, r2)
            r6.e = r4
            int r2 = r2 + r0
            r6.c = r2
            int r2 = r6.c
            if (r0 != 0) goto L_0x008b
            r1 = 1
        L_0x008b:
            int r2 = r2 + r1
            r6.d = r2
        L_0x008e:
            r6.f5312b = r3
        L_0x0090:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlin.j.f.a():void");
    }

    public final boolean hasNext() {
        if (this.f5312b == -1) {
            a();
        }
        return this.f5312b == 1;
    }

    public final /* synthetic */ Object next() {
        if (this.f5312b == -1) {
            a();
        }
        if (this.f5312b != 0) {
            c cVar = this.e;
            if (cVar != null) {
                this.e = null;
                this.f5312b = -1;
                return cVar;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.ranges.IntRange");
        }
        throw new NoSuchElementException();
    }
}
