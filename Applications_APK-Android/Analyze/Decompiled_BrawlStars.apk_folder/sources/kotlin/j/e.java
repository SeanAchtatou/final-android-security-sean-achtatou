package kotlin.j;

import java.util.Iterator;
import kotlin.g.c;
import kotlin.i.h;

/* compiled from: Strings.kt */
public final class e implements h<c> {

    /* renamed from: a  reason: collision with root package name */
    final CharSequence f5309a;

    /* renamed from: b  reason: collision with root package name */
    final int f5310b;
    final int c;
    final kotlin.d.a.c<CharSequence, Integer, kotlin.h<Integer, Integer>> d;

    /* JADX WARN: Type inference failed for: r5v0, types: [kotlin.d.a.c<? super java.lang.CharSequence, ? super java.lang.Integer, kotlin.h<java.lang.Integer, java.lang.Integer>>, java.lang.Object, kotlin.d.a.c<java.lang.CharSequence, java.lang.Integer, kotlin.h<java.lang.Integer, java.lang.Integer>>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public e(java.lang.CharSequence r2, int r3, int r4, kotlin.d.a.c<? super java.lang.CharSequence, ? super java.lang.Integer, kotlin.h<java.lang.Integer, java.lang.Integer>> r5) {
        /*
            r1 = this;
            java.lang.String r0 = "input"
            kotlin.d.b.j.b(r2, r0)
            java.lang.String r0 = "getNextMatch"
            kotlin.d.b.j.b(r5, r0)
            r1.<init>()
            r1.f5309a = r2
            r1.f5310b = r3
            r1.c = r4
            r1.d = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlin.j.e.<init>(java.lang.CharSequence, int, int, kotlin.d.a.c):void");
    }

    public final Iterator<c> a() {
        return new f(this);
    }
}
