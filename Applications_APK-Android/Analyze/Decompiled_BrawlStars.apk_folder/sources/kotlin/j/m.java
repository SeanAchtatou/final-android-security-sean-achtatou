package kotlin.j;

import java.util.Iterator;
import kotlin.a.a;
import kotlin.d.b.j;
import kotlin.g.c;
import kotlin.i.i;

/* compiled from: Regex.kt */
public final class m extends a<g> implements i {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ k f5318a;

    public final boolean isEmpty() {
        return false;
    }

    m(k kVar) {
        this.f5318a = kVar;
    }

    public final /* bridge */ boolean contains(Object obj) {
        boolean z;
        if (obj != null) {
            z = obj instanceof g;
        } else {
            z = true;
        }
        if (z) {
            return super.contains((g) obj);
        }
        return false;
    }

    public final int a() {
        return this.f5318a.c.groupCount() + 1;
    }

    public final Iterator<g> iterator() {
        j.b(this, "$this$indices");
        return i.b(kotlin.a.m.m(new c(0, size() - 1)), new n(this)).a();
    }
}
