package kotlin.j;

import java.io.Serializable;
import java.util.regex.Pattern;
import kotlin.d.b.j;
import kotlin.i.f;
import kotlin.i.h;

/* compiled from: Regex.kt */
public final class o implements Serializable {

    /* renamed from: b  reason: collision with root package name */
    public static final a f5320b = new a((byte) 0);

    /* renamed from: a  reason: collision with root package name */
    public final Pattern f5321a;

    public o(Pattern pattern) {
        j.b(pattern, "nativePattern");
        this.f5321a = pattern;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.regex.Pattern, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public o(java.lang.String r2) {
        /*
            r1 = this;
            java.lang.String r0 = "pattern"
            kotlin.d.b.j.b(r2, r0)
            java.util.regex.Pattern r2 = java.util.regex.Pattern.compile(r2)
            java.lang.String r0 = "Pattern.compile(pattern)"
            kotlin.d.b.j.a(r2, r0)
            r1.<init>(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlin.j.o.<init>(java.lang.String):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.regex.Pattern, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public o(java.lang.String r2, kotlin.j.s r3) {
        /*
            r1 = this;
            java.lang.String r0 = "pattern"
            kotlin.d.b.j.b(r2, r0)
            java.lang.String r0 = "option"
            kotlin.d.b.j.b(r3, r0)
            kotlin.j.o$a r0 = kotlin.j.o.f5320b
            int r3 = r3.h
            r0 = r3 & 2
            if (r0 == 0) goto L_0x0014
            r3 = r3 | 64
        L_0x0014:
            java.util.regex.Pattern r2 = java.util.regex.Pattern.compile(r2, r3)
            java.lang.String r3 = "Pattern.compile(pattern,…nicodeCase(option.value))"
            kotlin.d.b.j.a(r2, r3)
            r1.<init>(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlin.j.o.<init>(java.lang.String, kotlin.j.s):void");
    }

    public static /* synthetic */ h a(o oVar, CharSequence charSequence, int i, int i2) {
        j.b(charSequence, "input");
        kotlin.d.a.a pVar = new p(oVar, charSequence, 0);
        kotlin.d.a.b bVar = q.f5326a;
        j.b(pVar, "seedFunction");
        j.b(bVar, "nextFunction");
        return new f(pVar, bVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final String a(CharSequence charSequence, String str) {
        j.b(charSequence, "input");
        j.b(str, "replacement");
        String replaceAll = this.f5321a.matcher(charSequence).replaceAll(str);
        j.a((Object) replaceAll, "nativePattern.matcher(in…).replaceAll(replacement)");
        return replaceAll;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final String toString() {
        String pattern = this.f5321a.toString();
        j.a((Object) pattern, "nativePattern.toString()");
        return pattern;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    private final Object writeReplace() {
        String pattern = this.f5321a.pattern();
        j.a((Object) pattern, "nativePattern.pattern()");
        return new b(pattern, this.f5321a.flags());
    }

    /* compiled from: Regex.kt */
    static final class b implements Serializable {

        /* renamed from: a  reason: collision with root package name */
        public static final a f5322a = new a((byte) 0);
        private static final long serialVersionUID = 0;

        /* renamed from: b  reason: collision with root package name */
        private final String f5323b;
        private final int c;

        /* compiled from: Regex.kt */
        public static final class a {
            private a() {
            }

            public /* synthetic */ a(byte b2) {
                this();
            }
        }

        public b(String str, int i) {
            j.b(str, "pattern");
            this.f5323b = str;
            this.c = i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [java.util.regex.Pattern, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        private final Object readResolve() {
            Pattern compile = Pattern.compile(this.f5323b, this.c);
            j.a((Object) compile, "Pattern.compile(pattern, flags)");
            return new o(compile);
        }
    }

    /* compiled from: Regex.kt */
    public static final class a {
        private a() {
        }

        public /* synthetic */ a(byte b2) {
            this();
        }
    }
}
