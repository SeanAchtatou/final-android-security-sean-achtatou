package kotlin.j;

import kotlin.g.c;

/* compiled from: CharJVM.kt */
public class b {
    public static final boolean a(char c) {
        return Character.isWhitespace(c) || Character.isSpaceChar(c);
    }

    public static final int a(int i) {
        if (2 <= i && 36 >= i) {
            return i;
        }
        throw new IllegalArgumentException("radix " + i + " was not in valid range " + new c(2, 36));
    }
}
