package kotlin.j;

import java.util.regex.Matcher;

/* compiled from: Regex.kt */
public final class r {
    static final j a(Matcher matcher, int i, CharSequence charSequence) {
        if (!matcher.find(i)) {
            return null;
        }
        return new k(matcher, charSequence);
    }
}
