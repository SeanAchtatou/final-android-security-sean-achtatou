package kotlin;

import java.io.Serializable;
import kotlin.d.b.j;

/* compiled from: Tuples.kt */
public final class h<A, B> implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public final A f5262a;

    /* renamed from: b  reason: collision with root package name */
    public final B f5263b;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof h)) {
            return false;
        }
        h hVar = (h) obj;
        return j.a(this.f5262a, hVar.f5262a) && j.a(this.f5263b, hVar.f5263b);
    }

    public final int hashCode() {
        A a2 = this.f5262a;
        int i = 0;
        int hashCode = (a2 != null ? a2.hashCode() : 0) * 31;
        B b2 = this.f5263b;
        if (b2 != null) {
            i = b2.hashCode();
        }
        return hashCode + i;
    }

    public h(A a2, B b2) {
        this.f5262a = a2;
        this.f5263b = b2;
    }

    public final String toString() {
        return '(' + ((Object) this.f5262a) + ", " + ((Object) this.f5263b) + ')';
    }
}
