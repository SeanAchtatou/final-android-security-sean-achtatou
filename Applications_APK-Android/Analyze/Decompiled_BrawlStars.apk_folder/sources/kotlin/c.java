package kotlin;

import java.io.Serializable;

/* compiled from: Lazy.kt */
public final class c<T> implements Serializable, d<T> {

    /* renamed from: a  reason: collision with root package name */
    private final T f5234a;

    public c(T t) {
        this.f5234a = t;
    }

    public final T a() {
        return this.f5234a;
    }

    public final String toString() {
        return String.valueOf(a());
    }
}
