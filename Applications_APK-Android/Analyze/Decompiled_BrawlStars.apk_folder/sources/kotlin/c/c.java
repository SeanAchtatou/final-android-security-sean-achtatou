package kotlin.c;

/* compiled from: progressionUtil.kt */
public final class c {
    private static final int a(int i, int i2) {
        int i3 = i % i2;
        return i3 >= 0 ? i3 : i3 + i2;
    }

    public static final int a(int i, int i2, int i3) {
        return a(a(i, i3) - a(i2, i3), i3);
    }
}
