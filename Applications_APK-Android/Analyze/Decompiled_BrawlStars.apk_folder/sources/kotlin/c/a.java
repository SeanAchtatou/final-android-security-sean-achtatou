package kotlin.c;

import java.lang.reflect.Method;
import kotlin.d.b.j;
import kotlin.f.b;
import kotlin.f.d;

/* compiled from: PlatformImplementations.kt */
public class a {

    /* renamed from: kotlin.c.a$a  reason: collision with other inner class name */
    /* compiled from: PlatformImplementations.kt */
    static final class C0159a {

        /* renamed from: a  reason: collision with root package name */
        public static final Method f5235a;

        /* renamed from: b  reason: collision with root package name */
        public static final C0159a f5236b = new C0159a();

        /* JADX INFO: additional move instructions added (1) to help type inference */
        /* JADX WARN: Type inference failed for: r7v3, types: [java.lang.Class[], java.lang.Object] */
        /* JADX WARN: Type inference failed for: r5v6 */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.reflect.Method[], java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.reflect.Method, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
         arg types: [java.lang.String, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0045, code lost:
            if (kotlin.d.b.j.a((java.lang.Class) r5, r0) != false) goto L_0x0049;
         */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Unknown variable types count: 1 */
        static {
            /*
                kotlin.c.a$a r0 = new kotlin.c.a$a
                r0.<init>()
                kotlin.c.a.C0159a.f5236b = r0
                java.lang.Class<java.lang.Throwable> r0 = java.lang.Throwable.class
                java.lang.reflect.Method[] r1 = r0.getMethods()
                java.lang.String r2 = "throwableClass.methods"
                kotlin.d.b.j.a(r1, r2)
                int r2 = r1.length
                r3 = 0
                r4 = 0
            L_0x0015:
                r5 = 0
                if (r4 >= r2) goto L_0x0050
                r6 = r1[r4]
                java.lang.String r7 = "it"
                kotlin.d.b.j.a(r6, r7)
                java.lang.String r7 = r6.getName()
                java.lang.String r8 = "addSuppressed"
                boolean r7 = kotlin.d.b.j.a(r7, r8)
                r8 = 1
                if (r7 == 0) goto L_0x0048
                java.lang.Class[] r7 = r6.getParameterTypes()
                java.lang.String r9 = "it.parameterTypes"
                kotlin.d.b.j.a(r7, r9)
                java.lang.String r9 = "$this$singleOrNull"
                kotlin.d.b.j.b(r7, r9)
                int r9 = r7.length
                if (r9 != r8) goto L_0x003f
                r5 = r7[r3]
            L_0x003f:
                java.lang.Class r5 = (java.lang.Class) r5
                boolean r5 = kotlin.d.b.j.a(r5, r0)
                if (r5 == 0) goto L_0x0048
                goto L_0x0049
            L_0x0048:
                r8 = 0
            L_0x0049:
                if (r8 == 0) goto L_0x004d
                r5 = r6
                goto L_0x0050
            L_0x004d:
                int r4 = r4 + 1
                goto L_0x0015
            L_0x0050:
                kotlin.c.a.C0159a.f5235a = r5
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: kotlin.c.a.C0159a.<clinit>():void");
        }

        private C0159a() {
        }
    }

    public void a(Throwable th, Throwable th2) {
        j.b(th, "cause");
        j.b(th2, "exception");
        Method method = C0159a.f5235a;
        if (method != null) {
            method.invoke(th, th2);
        }
    }

    public static d a() {
        return new b();
    }
}
