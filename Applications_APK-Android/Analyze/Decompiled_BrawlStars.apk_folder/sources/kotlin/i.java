package kotlin;

import java.io.Serializable;
import kotlin.d.a.a;
import kotlin.d.b.j;

/* compiled from: LazyJVM.kt */
final class i<T> implements Serializable, d<T> {

    /* renamed from: a  reason: collision with root package name */
    private a<? extends T> f5266a;

    /* renamed from: b  reason: collision with root package name */
    private volatile Object f5267b;
    private final Object c;

    private i(a<? extends T> aVar, Object obj) {
        j.b(aVar, "initializer");
        this.f5266a = aVar;
        this.f5267b = l.f5329a;
        this.c = obj == null ? this : obj;
    }

    public /* synthetic */ i(a aVar, Object obj, int i) {
        this(aVar, null);
    }

    public final T a() {
        T t;
        T t2 = this.f5267b;
        if (t2 != l.f5329a) {
            return t2;
        }
        synchronized (this.c) {
            t = this.f5267b;
            if (t == l.f5329a) {
                a aVar = this.f5266a;
                if (aVar == null) {
                    j.a();
                }
                t = aVar.invoke();
                this.f5267b = t;
                this.f5266a = null;
            }
        }
        return t;
    }

    private final Object writeReplace() {
        return new c(a());
    }

    public final String toString() {
        return this.f5267b != l.f5329a ? String.valueOf(a()) : "Lazy value not initialized yet.";
    }
}
