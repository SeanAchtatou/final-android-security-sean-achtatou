package kotlin.i;

import com.facebook.share.internal.ShareConstants;
import java.util.Collection;
import java.util.Iterator;
import kotlin.d.a.b;
import kotlin.d.b.j;

/* compiled from: _Sequences.kt */
public class o extends n {
    public static final <T> h<T> a(h hVar, b bVar) {
        j.b(hVar, "$this$filter");
        j.b(bVar, "predicate");
        return new d<>(hVar, true, bVar);
    }

    public static final <T, C extends Collection<? super T>> C a(h hVar, Collection collection) {
        j.b(hVar, "$this$toCollection");
        j.b(collection, ShareConstants.DESTINATION);
        Iterator a2 = hVar.a();
        while (a2.hasNext()) {
            collection.add(a2.next());
        }
        return collection;
    }

    public static final <T, R> h<R> b(h<? extends T> hVar, b<? super T, ? extends R> bVar) {
        j.b(hVar, "$this$map");
        j.b(bVar, "transform");
        return new r<>(hVar, bVar);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static final <T, A extends java.lang.Appendable> A a(kotlin.i.h<? extends T> r2, A r3, java.lang.CharSequence r4, java.lang.CharSequence r5, java.lang.CharSequence r6, int r7, java.lang.CharSequence r8, kotlin.d.a.b<? super T, ? extends java.lang.CharSequence> r9) {
        /*
            java.lang.String r0 = "$this$joinTo"
            kotlin.d.b.j.b(r2, r0)
            java.lang.String r0 = "buffer"
            kotlin.d.b.j.b(r3, r0)
            java.lang.String r0 = "separator"
            kotlin.d.b.j.b(r4, r0)
            java.lang.String r0 = "prefix"
            kotlin.d.b.j.b(r5, r0)
            java.lang.String r0 = "postfix"
            kotlin.d.b.j.b(r6, r0)
            java.lang.String r0 = "truncated"
            kotlin.d.b.j.b(r8, r0)
            r3.append(r5)
            java.util.Iterator r2 = r2.a()
            r5 = 0
        L_0x0026:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0040
            java.lang.Object r0 = r2.next()
            int r5 = r5 + 1
            r1 = 1
            if (r5 <= r1) goto L_0x0038
            r3.append(r4)
        L_0x0038:
            if (r7 < 0) goto L_0x003c
            if (r5 > r7) goto L_0x0040
        L_0x003c:
            kotlin.j.t.a(r3, r0, r9)
            goto L_0x0026
        L_0x0040:
            if (r7 < 0) goto L_0x0047
            if (r5 <= r7) goto L_0x0047
            r3.append(r8)
        L_0x0047:
            r3.append(r6)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlin.i.o.a(kotlin.i.h, java.lang.Appendable, java.lang.CharSequence, java.lang.CharSequence, java.lang.CharSequence, int, java.lang.CharSequence, kotlin.d.a.b):java.lang.Appendable");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final <T> String a(h<? extends T> hVar, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, b<? super T, ? extends CharSequence> bVar) {
        j.b(hVar, "$this$joinToString");
        j.b(charSequence, "separator");
        j.b(charSequence2, "prefix");
        j.b(charSequence3, "postfix");
        j.b(charSequence4, "truncated");
        String sb = ((StringBuilder) i.a(hVar, new StringBuilder(), charSequence, charSequence2, charSequence3, i, charSequence4, bVar)).toString();
        j.a((Object) sb, "joinTo(StringBuilder(), …ed, transform).toString()");
        return sb;
    }
}
