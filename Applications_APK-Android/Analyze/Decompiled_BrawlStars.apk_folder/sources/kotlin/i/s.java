package kotlin.i;

import java.util.Iterator;
import kotlin.d.b.a.a;

/* compiled from: Sequences.kt */
public final class s implements Iterator<R>, a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ r f5283a;

    /* renamed from: b  reason: collision with root package name */
    private final Iterator<T> f5284b;

    public final void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    s(r rVar) {
        this.f5283a = rVar;
        this.f5284b = rVar.f5281a.a();
    }

    public final R next() {
        return this.f5283a.f5282b.invoke(this.f5284b.next());
    }

    public final boolean hasNext() {
        return this.f5284b.hasNext();
    }
}
