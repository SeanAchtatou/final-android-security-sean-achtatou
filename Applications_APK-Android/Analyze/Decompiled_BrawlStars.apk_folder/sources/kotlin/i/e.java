package kotlin.i;

import java.util.Iterator;
import java.util.NoSuchElementException;
import kotlin.d.b.a.a;

/* compiled from: Sequences.kt */
public final class e implements Iterator<T>, a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f5272a;

    /* renamed from: b  reason: collision with root package name */
    private final Iterator<T> f5273b;
    private int c = -1;
    private T d;

    public final void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    e(d dVar) {
        this.f5272a = dVar;
        this.f5273b = dVar.f5270a.a();
    }

    private final void a() {
        while (this.f5273b.hasNext()) {
            T next = this.f5273b.next();
            if (this.f5272a.c.invoke(next).booleanValue() == this.f5272a.f5271b) {
                this.d = next;
                this.c = 1;
                return;
            }
        }
        this.c = 0;
    }

    public final T next() {
        if (this.c == -1) {
            a();
        }
        if (this.c != 0) {
            T t = this.d;
            this.d = null;
            this.c = -1;
            return t;
        }
        throw new NoSuchElementException();
    }

    public final boolean hasNext() {
        if (this.c == -1) {
            a();
        }
        return this.c == 1;
    }
}
