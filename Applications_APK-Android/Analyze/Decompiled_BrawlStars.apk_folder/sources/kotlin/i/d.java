package kotlin.i;

import java.util.Iterator;
import kotlin.d.a.b;
import kotlin.d.b.j;

/* compiled from: Sequences.kt */
public final class d<T> implements h<T> {

    /* renamed from: a  reason: collision with root package name */
    final h<T> f5270a;

    /* renamed from: b  reason: collision with root package name */
    final boolean f5271b;
    final b<T, Boolean> c;

    public d(h<? extends T> hVar, boolean z, b<? super T, Boolean> bVar) {
        j.b(hVar, "sequence");
        j.b(bVar, "predicate");
        this.f5270a = hVar;
        this.f5271b = z;
        this.c = bVar;
    }

    public final Iterator<T> a() {
        return new e(this);
    }
}
