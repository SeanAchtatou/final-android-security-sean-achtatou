package kotlin.i;

import java.util.Iterator;
import kotlin.d.b.j;

/* compiled from: Sequences.kt */
public class l extends k {
    public static final <T> h<T> a(Iterator it) {
        j.b(it, "$this$asSequence");
        return i.a(new m(it));
    }

    public static final <T> h<T> a(h hVar) {
        j.b(hVar, "$this$constrainOnce");
        return hVar instanceof a ? hVar : new a<>(hVar);
    }
}
