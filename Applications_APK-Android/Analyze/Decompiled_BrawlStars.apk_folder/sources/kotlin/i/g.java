package kotlin.i;

import java.util.Iterator;
import java.util.NoSuchElementException;
import kotlin.TypeCastException;
import kotlin.d.a.b;
import kotlin.d.b.a.a;
import kotlin.d.b.j;

/* compiled from: Sequences.kt */
public final class g implements Iterator<T>, a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ f f5276a;

    /* renamed from: b  reason: collision with root package name */
    private T f5277b;
    private int c = -2;

    public final void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    g(f fVar) {
        this.f5276a = fVar;
    }

    private final void a() {
        T t;
        if (this.c == -2) {
            t = this.f5276a.f5274a.invoke();
        } else {
            b<T, T> bVar = this.f5276a.f5275b;
            T t2 = this.f5277b;
            if (t2 == null) {
                j.a();
            }
            t = bVar.invoke(t2);
        }
        this.f5277b = t;
        this.c = this.f5277b == null ? 0 : 1;
    }

    public final T next() {
        if (this.c < 0) {
            a();
        }
        if (this.c != 0) {
            T t = this.f5277b;
            if (t != null) {
                this.c = -1;
                return t;
            }
            throw new TypeCastException("null cannot be cast to non-null type T");
        }
        throw new NoSuchElementException();
    }

    public final boolean hasNext() {
        if (this.c < 0) {
            a();
        }
        return this.c == 1;
    }
}
