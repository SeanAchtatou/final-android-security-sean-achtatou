package kotlin.i;

import java.util.Iterator;
import kotlin.d.a.a;
import kotlin.d.a.b;
import kotlin.d.b.j;

/* compiled from: Sequences.kt */
public final class f<T> implements h<T> {

    /* renamed from: a  reason: collision with root package name */
    final a<T> f5274a;

    /* renamed from: b  reason: collision with root package name */
    final b<T, T> f5275b;

    public f(a<? extends T> aVar, b<? super T, ? extends T> bVar) {
        j.b(aVar, "getInitialValue");
        j.b(bVar, "getNextValue");
        this.f5274a = aVar;
        this.f5275b = bVar;
    }

    public final Iterator<T> a() {
        return new g(this);
    }
}
