package kotlin.i;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicReference;
import kotlin.d.b.j;

/* compiled from: SequencesJVM.kt */
public final class a<T> implements h<T> {

    /* renamed from: a  reason: collision with root package name */
    private final AtomicReference<h<T>> f5268a;

    public a(h<? extends T> hVar) {
        j.b(hVar, "sequence");
        this.f5268a = new AtomicReference<>(hVar);
    }

    public final Iterator<T> a() {
        h andSet = this.f5268a.getAndSet(null);
        if (andSet != null) {
            return andSet.a();
        }
        throw new IllegalStateException("This sequence can be consumed only once.");
    }
}
