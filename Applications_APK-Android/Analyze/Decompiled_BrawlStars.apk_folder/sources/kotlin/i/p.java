package kotlin.i;

import java.util.Iterator;
import kotlin.d.b.a.a;

/* compiled from: Iterables.kt */
public final class p implements Iterable<T>, a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ h f5279a;

    public p(h hVar) {
        this.f5279a = hVar;
    }

    public final Iterator<T> iterator() {
        return this.f5279a.a();
    }
}
