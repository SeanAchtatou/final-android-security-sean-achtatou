package kotlin.i;

import java.util.Iterator;
import kotlin.d.a.b;
import kotlin.d.b.j;

/* compiled from: Sequences.kt */
public final class r<T, R> implements h<R> {

    /* renamed from: a  reason: collision with root package name */
    final h<T> f5281a;

    /* renamed from: b  reason: collision with root package name */
    final b<T, R> f5282b;

    public r(h<? extends T> hVar, b<? super T, ? extends R> bVar) {
        j.b(hVar, "sequence");
        j.b(bVar, "transformer");
        this.f5281a = hVar;
        this.f5282b = bVar;
    }

    public final Iterator<R> a() {
        return new s(this);
    }
}
