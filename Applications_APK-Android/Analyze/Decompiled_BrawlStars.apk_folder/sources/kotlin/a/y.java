package kotlin.a;

import kotlin.d.a.b;
import kotlin.d.b.k;

/* compiled from: _Collections.kt */
final class y extends k implements b {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f5232a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    y(int i) {
        super(1);
        this.f5232a = i;
    }

    public final /* synthetic */ Object invoke(Object obj) {
        ((Number) obj).intValue();
        throw new IndexOutOfBoundsException("Collection doesn't contain element at index " + this.f5232a + '.');
    }
}
