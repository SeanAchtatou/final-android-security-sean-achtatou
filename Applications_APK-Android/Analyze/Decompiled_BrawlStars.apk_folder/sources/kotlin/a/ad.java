package kotlin.a;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import kotlin.d.b.a.a;
import kotlin.d.b.f;
import kotlin.d.b.j;

/* compiled from: Sets.kt */
public final class ad implements Serializable, Set, a {

    /* renamed from: a  reason: collision with root package name */
    public static final ad f5212a = new ad();
    private static final long serialVersionUID = 3406603774387020532L;

    public final /* synthetic */ boolean add(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final boolean addAll(Collection collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final int hashCode() {
        return 0;
    }

    public final boolean isEmpty() {
        return true;
    }

    public final boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final boolean removeAll(Collection collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final boolean retainAll(Collection collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final /* bridge */ int size() {
        return 0;
    }

    public final Object[] toArray() {
        return f.a(this);
    }

    public final <T> T[] toArray(T[] tArr) {
        return f.a(this, tArr);
    }

    public final String toString() {
        return "[]";
    }

    private ad() {
    }

    public final boolean contains(Object obj) {
        if (obj instanceof Void) {
            j.b((Void) obj, "element");
        }
        return false;
    }

    public final boolean equals(Object obj) {
        return (obj instanceof Set) && ((Set) obj).isEmpty();
    }

    public final boolean containsAll(Collection collection) {
        j.b(collection, MessengerShareContentUtility.ELEMENTS);
        return collection.isEmpty();
    }

    public final Iterator iterator() {
        return aa.f5209a;
    }

    private final Object readResolve() {
        return f5212a;
    }
}
