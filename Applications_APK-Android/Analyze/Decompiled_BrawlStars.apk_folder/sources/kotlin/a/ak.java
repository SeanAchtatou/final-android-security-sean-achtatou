package kotlin.a;

import java.util.Collections;
import java.util.Map;
import kotlin.d.b.j;
import kotlin.h;

/* compiled from: MapsJVM.kt */
public class ak extends aj {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Map<K, V>, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final <K, V> Map<K, V> a(h hVar) {
        j.b(hVar, "pair");
        Map<K, V> singletonMap = Collections.singletonMap(hVar.f5262a, hVar.f5263b);
        j.a((Object) singletonMap, "java.util.Collections.si…(pair.first, pair.second)");
        return singletonMap;
    }
}
