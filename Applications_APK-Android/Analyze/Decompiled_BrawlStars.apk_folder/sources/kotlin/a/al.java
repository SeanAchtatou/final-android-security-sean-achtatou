package kotlin.a;

import android.support.v7.widget.ActivityChooserView;
import com.facebook.share.internal.ShareConstants;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.TypeCastException;
import kotlin.d.b.j;
import kotlin.h;

/* compiled from: Maps.kt */
public class al extends ak {
    public static final <K, V> Map<K, V> a() {
        ac acVar = ac.f5211a;
        if (acVar != null) {
            return acVar;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.Map<K, V>");
    }

    public static final <K, V> Map<K, V> a(h... hVarArr) {
        j.b(hVarArr, "pairs");
        return hVarArr.length > 0 ? ai.a(hVarArr, new LinkedHashMap(ai.a(hVarArr.length))) : ai.a();
    }

    public static final int a(int i) {
        if (i < 3) {
            return i + 1;
        }
        return i < 1073741824 ? i + (i / 3) : ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
    }

    public static final <K, V> void a(Map<? super K, ? super V> map, h<? extends K, ? extends V>[] hVarArr) {
        j.b(map, "$this$putAll");
        j.b(hVarArr, "pairs");
        for (h<? extends K, ? extends V> hVar : hVarArr) {
            map.put(hVar.f5262a, hVar.f5263b);
        }
    }

    public static final <K, V> void a(Map<? super K, ? super V> map, Iterable<? extends h<? extends K, ? extends V>> iterable) {
        j.b(map, "$this$putAll");
        j.b(iterable, "pairs");
        for (h hVar : iterable) {
            map.put(hVar.f5262a, hVar.f5263b);
        }
    }

    public static final <K, V> Map<K, V> a(Iterable iterable) {
        j.b(iterable, "$this$toMap");
        Collection collection = (Collection) iterable;
        int size = collection.size();
        if (size == 0) {
            return ai.a();
        }
        if (size != 1) {
            return ai.a(iterable, new LinkedHashMap(ai.a(collection.size())));
        }
        return ai.a((h) ((List) iterable).get(0));
    }

    public static final <K, V, M extends Map<? super K, ? super V>> M a(Iterable<? extends h<? extends K, ? extends V>> iterable, M m) {
        j.b(iterable, "$this$toMap");
        j.b(m, ShareConstants.DESTINATION);
        ai.a(m, iterable);
        return m;
    }

    public static final <K, V> Map<K, V> b(h<? extends K, ? extends V>[] hVarArr) {
        j.b(hVarArr, "$this$toMap");
        int length = hVarArr.length;
        if (length == 0) {
            return ai.a();
        }
        if (length != 1) {
            return ai.a(hVarArr, new LinkedHashMap(ai.a(hVarArr.length)));
        }
        return ai.a(hVarArr[0]);
    }

    public static final <K, V, M extends Map<? super K, ? super V>> M a(h<? extends K, ? extends V>[] hVarArr, M m) {
        j.b(hVarArr, "$this$toMap");
        j.b(m, ShareConstants.DESTINATION);
        ai.a(m, hVarArr);
        return m;
    }
}
