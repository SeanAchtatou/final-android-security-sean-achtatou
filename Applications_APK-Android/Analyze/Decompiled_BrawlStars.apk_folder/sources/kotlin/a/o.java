package kotlin.a;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.ArrayList;
import java.util.List;
import kotlin.d.b.j;

/* compiled from: Collections.kt */
public class o extends n {
    public static final <T> List<T> a(Object... objArr) {
        j.b(objArr, MessengerShareContentUtility.ELEMENTS);
        return objArr.length > 0 ? g.a(objArr) : ab.f5210a;
    }

    public static final <T> List<T> b(Object... objArr) {
        j.b(objArr, MessengerShareContentUtility.ELEMENTS);
        return objArr.length == 0 ? new ArrayList<>() : new ArrayList<>(new f(objArr, true));
    }

    public static final <T> ArrayList<T> c(Object... objArr) {
        j.b(objArr, MessengerShareContentUtility.ELEMENTS);
        return objArr.length == 0 ? new ArrayList<>() : new ArrayList<>(new f(objArr, true));
    }

    public static final <T> List<T> b(Object obj) {
        if (obj != null) {
            return m.a(obj);
        }
        return ab.f5210a;
    }

    public static final <T> List<T> d(Object... objArr) {
        j.b(objArr, MessengerShareContentUtility.ELEMENTS);
        return g.b(objArr);
    }

    public static final <T> int a(List list) {
        j.b(list, "$this$lastIndex");
        return list.size() - 1;
    }

    public static final <T> List<T> b(List list) {
        j.b(list, "$this$optimizeReadOnlyList");
        int size = list.size();
        if (size != 0) {
            return size != 1 ? list : m.a(list.get(0));
        }
        return ab.f5210a;
    }

    public static final void a() {
        throw new ArithmeticException("Index overflow has happened.");
    }

    public static final void b() {
        throw new ArithmeticException("Count overflow has happened.");
    }
}
