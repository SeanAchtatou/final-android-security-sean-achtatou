package kotlin.a;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.Collection;
import kotlin.d.b.j;
import kotlin.d.b.w;

/* compiled from: MutableCollections.kt */
public class t extends s {
    public static final <T> boolean a(Collection collection, Iterable iterable) {
        j.b(collection, "$this$addAll");
        j.b(iterable, MessengerShareContentUtility.ELEMENTS);
        if (iterable instanceof Collection) {
            return collection.addAll((Collection) iterable);
        }
        boolean z = false;
        for (Object add : iterable) {
            if (collection.add(add)) {
                z = true;
            }
        }
        return z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
     arg types: [java.lang.Iterable, java.util.Collection]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T> */
    public static final <T> boolean b(Collection collection, Iterable iterable) {
        j.b(collection, "$this$removeAll");
        j.b(iterable, MessengerShareContentUtility.ELEMENTS);
        return w.a(collection).removeAll(m.a(iterable, (Iterable) collection));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
     arg types: [java.lang.Iterable, java.util.Collection]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T> */
    public static final <T> boolean c(Collection collection, Iterable iterable) {
        j.b(collection, "$this$retainAll");
        j.b(iterable, MessengerShareContentUtility.ELEMENTS);
        return w.a(collection).retainAll(m.a(iterable, (Iterable) collection));
    }
}
