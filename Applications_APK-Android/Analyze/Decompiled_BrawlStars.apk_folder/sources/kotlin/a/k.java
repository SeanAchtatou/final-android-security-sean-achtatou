package kotlin.a;

import com.facebook.share.internal.ShareConstants;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import kotlin.d.b.j;

/* compiled from: _Arrays.kt */
public class k extends j {
    public static final <T> boolean b(Object[] objArr, Object obj) {
        j.b(objArr, "$this$contains");
        return g.c(objArr, obj) >= 0;
    }

    public static final boolean a(char[] cArr, char c) {
        j.b(cArr, "$this$contains");
        j.b(cArr, "$this$indexOf");
        int length = cArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                i = -1;
                break;
            } else if (c == cArr[i]) {
                break;
            } else {
                i++;
            }
        }
        if (i >= 0) {
            return true;
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [T, T]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    public static final <T> int c(T[] tArr, T t) {
        j.b(tArr, "$this$indexOf");
        int i = 0;
        if (t == null) {
            int length = tArr.length;
            while (i < length) {
                if (tArr[i] == null) {
                    return i;
                }
                i++;
            }
            return -1;
        }
        int length2 = tArr.length;
        while (i < length2) {
            if (j.a((Object) t, (Object) tArr[i])) {
                return i;
            }
            i++;
        }
        return -1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.k.a(java.lang.Object[], java.util.Collection):C
     arg types: [T[], java.util.ArrayList]
     candidates:
      kotlin.a.k.a(char[], char):boolean
      kotlin.a.j.a(java.lang.Object[], java.util.Comparator):void
      kotlin.a.j.a(java.lang.Object[], java.lang.Object):T[]
      kotlin.a.j.a(java.lang.Object[], java.lang.Object[]):T[]
      kotlin.a.k.a(java.lang.Object[], java.util.Collection):C */
    public static final <T> List<T> b(T[] tArr) {
        j.b(tArr, "$this$filterNotNull");
        return (List) g.a((Object[]) tArr, (Collection) new ArrayList());
    }

    public static final <C extends Collection<? super T>, T> C a(Object[] objArr, Collection collection) {
        j.b(objArr, "$this$filterNotNullTo");
        j.b(collection, ShareConstants.DESTINATION);
        for (Object obj : objArr) {
            if (obj != null) {
                collection.add(obj);
            }
        }
        return collection;
    }

    public static final <T, C extends Collection<? super T>> C b(Object[] objArr, Collection collection) {
        j.b(objArr, "$this$toCollection");
        j.b(collection, ShareConstants.DESTINATION);
        for (Object add : objArr) {
            collection.add(add);
        }
        return collection;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.k.b(java.lang.Object[], java.util.Collection):C
     arg types: [T[], java.util.LinkedHashSet]
     candidates:
      kotlin.a.k.b(java.lang.Object[], java.lang.Object):boolean
      kotlin.a.k.b(java.lang.Object[], java.util.Collection):C */
    public static final <T> Set<T> c(T[] tArr) {
        j.b(tArr, "$this$toSet");
        int length = tArr.length;
        if (length == 0) {
            return ad.f5212a;
        }
        if (length != 1) {
            return (Set) g.b((Object[]) tArr, (Collection) new LinkedHashSet(ai.a(tArr.length)));
        }
        return an.a((Object) tArr[0]);
    }
}
