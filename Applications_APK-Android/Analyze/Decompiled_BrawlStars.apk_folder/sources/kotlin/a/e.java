package kotlin.a;

import com.facebook.internal.FacebookRequestErrorClassification;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.RandomAccess;
import kotlin.d.b.j;

/* compiled from: AbstractList.kt */
public abstract class e<E> extends a<E> implements List<E>, kotlin.d.b.a.a {

    /* renamed from: a  reason: collision with root package name */
    public static final a f5224a = new a((byte) 0);

    public void add(int i, E e) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean addAll(int i, Collection<? extends E> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public abstract E get(int i);

    public E remove(int i) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public E set(int i, E e) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    protected e() {
    }

    public Iterator<E> iterator() {
        return new b();
    }

    public ListIterator<E> listIterator() {
        return new c(0);
    }

    public ListIterator<E> listIterator(int i) {
        return new c(i);
    }

    public List<E> subList(int i, int i2) {
        return new d<>(this, i, i2);
    }

    /* compiled from: AbstractList.kt */
    static final class d<E> extends e<E> implements RandomAccess {

        /* renamed from: b  reason: collision with root package name */
        private int f5227b;
        private final e<E> c;
        private final int d;

        public d(e<? extends E> eVar, int i, int i2) {
            j.b(eVar, "list");
            this.c = eVar;
            this.d = i;
            int i3 = this.d;
            int size = this.c.size();
            if (i3 < 0 || i2 > size) {
                throw new IndexOutOfBoundsException("fromIndex: " + i3 + ", toIndex: " + i2 + ", size: " + size);
            } else if (i3 <= i2) {
                this.f5227b = i2 - this.d;
            } else {
                throw new IllegalArgumentException("fromIndex: " + i3 + " > toIndex: " + i2);
            }
        }

        public final E get(int i) {
            int i2 = this.f5227b;
            if (i >= 0 && i < i2) {
                return this.c.get(this.d + i);
            }
            throw new IndexOutOfBoundsException("index: " + i + ", size: " + i2);
        }

        public final int a() {
            return this.f5227b;
        }
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof List)) {
            return false;
        }
        Collection<Object> collection = this;
        Collection collection2 = (Collection) obj;
        j.b(collection, "c");
        j.b(collection2, FacebookRequestErrorClassification.KEY_OTHER);
        if (collection.size() != collection2.size()) {
            return false;
        }
        Iterator it = collection2.iterator();
        for (Object a2 : collection) {
            if (!j.a(a2, it.next())) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Collection collection = this;
        j.b(collection, "c");
        Iterator it = collection.iterator();
        int i = 1;
        while (it.hasNext()) {
            Object next = it.next();
            i = (i * 31) + (next != null ? next.hashCode() : 0);
        }
        return i;
    }

    /* compiled from: AbstractList.kt */
    class b implements Iterator<E>, kotlin.d.b.a.a {

        /* renamed from: a  reason: collision with root package name */
        int f5225a;

        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        public b() {
        }

        public boolean hasNext() {
            return this.f5225a < e.this.size();
        }

        public E next() {
            if (hasNext()) {
                e eVar = e.this;
                int i = this.f5225a;
                this.f5225a = i + 1;
                return eVar.get(i);
            }
            throw new NoSuchElementException();
        }
    }

    /* compiled from: AbstractList.kt */
    class c extends e<E>.b implements ListIterator<E>, kotlin.d.b.a.a {
        public void add(E e) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        public void set(E e) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        public c(int i) {
            super();
            a aVar = e.f5224a;
            int size = e.this.size();
            if (i < 0 || i > size) {
                throw new IndexOutOfBoundsException("index: " + i + ", size: " + size);
            }
            this.f5225a = i;
        }

        public E previous() {
            if (hasPrevious()) {
                this.f5225a--;
                return e.this.get(this.f5225a);
            }
            throw new NoSuchElementException();
        }

        public boolean hasPrevious() {
            return this.f5225a > 0;
        }

        public int nextIndex() {
            return this.f5225a;
        }

        public int previousIndex() {
            return this.f5225a - 1;
        }
    }

    /* compiled from: AbstractList.kt */
    public static final class a {
        private a() {
        }

        public /* synthetic */ a(byte b2) {
            this();
        }
    }

    public int indexOf(Object obj) {
        int i = 0;
        for (Object a2 : this) {
            if (j.a(a2, obj)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    public int lastIndexOf(Object obj) {
        ListIterator listIterator = listIterator(size());
        while (listIterator.hasPrevious()) {
            if (j.a(listIterator.previous(), obj)) {
                return listIterator.nextIndex();
            }
        }
        return -1;
    }
}
