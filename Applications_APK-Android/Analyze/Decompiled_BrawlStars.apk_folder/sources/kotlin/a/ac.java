package kotlin.a;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import kotlin.d.b.a.a;
import kotlin.d.b.j;

/* compiled from: Maps.kt */
final class ac implements Serializable, Map, a {

    /* renamed from: a  reason: collision with root package name */
    public static final ac f5211a = new ac();
    private static final long serialVersionUID = 8246714829545688274L;

    public final void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final boolean containsKey(Object obj) {
        return false;
    }

    public final /* bridge */ Object get(Object obj) {
        return null;
    }

    public final int hashCode() {
        return 0;
    }

    public final boolean isEmpty() {
        return true;
    }

    public final /* synthetic */ Object put(Object obj, Object obj2) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final void putAll(Map map) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final Object remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final /* bridge */ int size() {
        return 0;
    }

    public final String toString() {
        return "{}";
    }

    private ac() {
    }

    public final boolean containsValue(Object obj) {
        if (obj instanceof Void) {
            j.b((Void) obj, "value");
        }
        return false;
    }

    public final boolean equals(Object obj) {
        return (obj instanceof Map) && ((Map) obj).isEmpty();
    }

    private final Object readResolve() {
        return f5211a;
    }

    public final /* bridge */ Set<Map.Entry> entrySet() {
        return ad.f5212a;
    }

    public final /* bridge */ Set<Object> keySet() {
        return ad.f5212a;
    }

    public final /* bridge */ Collection values() {
        return ab.f5210a;
    }
}
