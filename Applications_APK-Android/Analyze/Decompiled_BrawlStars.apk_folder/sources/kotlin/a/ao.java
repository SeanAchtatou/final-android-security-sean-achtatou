package kotlin.a;

import java.util.Collections;
import java.util.Set;
import kotlin.d.b.j;

/* compiled from: SetsJVM.kt */
public class ao {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Set<T>, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final <T> Set<T> a(Object obj) {
        Set<T> singleton = Collections.singleton(obj);
        j.a((Object) singleton, "java.util.Collections.singleton(element)");
        return singleton;
    }
}
