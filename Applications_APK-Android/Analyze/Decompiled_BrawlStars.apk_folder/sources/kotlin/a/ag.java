package kotlin.a;

import java.util.Iterator;
import kotlin.d.b.a.a;
import kotlin.d.b.j;

/* compiled from: Iterators.kt */
public final class ag<T> implements Iterator<ae<? extends T>>, a {

    /* renamed from: a  reason: collision with root package name */
    private int f5216a;

    /* renamed from: b  reason: collision with root package name */
    private final Iterator<T> f5217b;

    public final void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public ag(Iterator<? extends T> it) {
        j.b(it, "iterator");
        this.f5217b = it;
    }

    public final boolean hasNext() {
        return this.f5217b.hasNext();
    }

    public final /* synthetic */ Object next() {
        int i = this.f5216a;
        this.f5216a = i + 1;
        if (i < 0) {
            m.a();
        }
        return new ae(i, this.f5217b.next());
    }
}
