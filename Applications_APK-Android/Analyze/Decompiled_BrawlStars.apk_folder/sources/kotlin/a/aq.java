package kotlin.a;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import kotlin.d.b.j;

/* compiled from: _Sets.kt */
public class aq extends ap {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
     arg types: [java.lang.Iterable<? extends T>, java.lang.Iterable]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T> */
    public static final <T> Set<T> a(Set<? extends T> set, Iterable<? extends T> iterable) {
        j.b(set, "$this$minus");
        j.b(iterable, MessengerShareContentUtility.ELEMENTS);
        Iterable iterable2 = set;
        Collection<? extends T> a2 = m.a((Iterable) iterable, iterable2);
        if (a2.isEmpty()) {
            return m.i(iterable2);
        }
        if (a2 instanceof Set) {
            Collection linkedHashSet = new LinkedHashSet();
            for (Object next : iterable2) {
                if (!a2.contains(next)) {
                    linkedHashSet.add(next);
                }
            }
            return (Set) linkedHashSet;
        }
        LinkedHashSet linkedHashSet2 = new LinkedHashSet(set);
        linkedHashSet2.removeAll(a2);
        return linkedHashSet2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
     arg types: [java.util.LinkedHashSet, java.lang.Iterable<? extends T>]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean */
    public static final <T> Set<T> b(Set<? extends T> set, Iterable<? extends T> iterable) {
        int i;
        j.b(set, "$this$plus");
        j.b(iterable, MessengerShareContentUtility.ELEMENTS);
        Integer a2 = m.a((Iterable) iterable);
        if (a2 != null) {
            i = set.size() + a2.intValue();
        } else {
            i = set.size() * 2;
        }
        LinkedHashSet linkedHashSet = new LinkedHashSet(ai.a(i));
        linkedHashSet.addAll(set);
        m.a((Collection) linkedHashSet, (Iterable) iterable);
        return linkedHashSet;
    }
}
