package kotlin.a;

import java.util.Collections;
import java.util.List;
import kotlin.d.b.j;

/* compiled from: CollectionsJVM.kt */
public class n {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.List<T>, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final <T> List<T> a(Object obj) {
        List<T> singletonList = Collections.singletonList(obj);
        j.a((Object) singletonList, "java.util.Collections.singletonList(element)");
        return singletonList;
    }
}
