package kotlin.a;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/* compiled from: _ArraysJvm.kt */
public class j extends i {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.Object[], java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final <T> T[] a(Object[] objArr, Object obj) {
        kotlin.d.b.j.b(objArr, "$this$plus");
        Object[] copyOf = Arrays.copyOf(objArr, 5);
        copyOf[4] = obj;
        kotlin.d.b.j.a((Object) copyOf, "result");
        return copyOf;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [T[], java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final <T> T[] a(Object[] objArr, Object[] objArr2) {
        kotlin.d.b.j.b(objArr, "$this$plus");
        kotlin.d.b.j.b(objArr2, MessengerShareContentUtility.ELEMENTS);
        int length = objArr.length;
        T[] copyOf = Arrays.copyOf(objArr, length + 1);
        System.arraycopy(objArr2, 0, copyOf, length, 1);
        kotlin.d.b.j.a((Object) copyOf, "result");
        return copyOf;
    }

    public static final <T> void a(Object[] objArr, Comparator comparator) {
        kotlin.d.b.j.b(objArr, "$this$sortWith");
        kotlin.d.b.j.b(comparator, "comparator");
        if (objArr.length > 1) {
            Arrays.sort(objArr, comparator);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.List<T>, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final <T> List<T> a(T[] tArr) {
        kotlin.d.b.j.b(tArr, "$this$asList");
        List<T> asList = Arrays.asList(tArr);
        kotlin.d.b.j.a((Object) asList, "ArraysUtilJVM.asList(this)");
        return asList;
    }
}
