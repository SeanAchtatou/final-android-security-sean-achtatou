package kotlin.a;

import kotlin.d.b.j;

/* compiled from: IndexedValue.kt */
public final class ae<T> {

    /* renamed from: a  reason: collision with root package name */
    public final int f5213a;

    /* renamed from: b  reason: collision with root package name */
    public final T f5214b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [T, T]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ae) {
                ae aeVar = (ae) obj;
                if (!(this.f5213a == aeVar.f5213a) || !j.a((Object) this.f5214b, (Object) aeVar.f5214b)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        int i = this.f5213a * 31;
        T t = this.f5214b;
        return i + (t != null ? t.hashCode() : 0);
    }

    public final String toString() {
        return "IndexedValue(index=" + this.f5213a + ", value=" + ((Object) this.f5214b) + ")";
    }

    public ae(int i, T t) {
        this.f5213a = i;
        this.f5214b = t;
    }
}
