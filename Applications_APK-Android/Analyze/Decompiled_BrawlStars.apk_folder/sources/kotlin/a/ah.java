package kotlin.a;

import java.util.Iterator;
import kotlin.d.b.a.a;

/* compiled from: Iterators.kt */
public abstract class ah implements Iterator<Integer>, a {
    public abstract int a();

    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public /* synthetic */ Object next() {
        return Integer.valueOf(a());
    }
}
