package kotlin.a;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import kotlin.d.b.a.a;
import kotlin.d.b.b;
import kotlin.d.b.j;

/* compiled from: Collections.kt */
final class f<T> implements Collection<T>, a {

    /* renamed from: a  reason: collision with root package name */
    private final T[] f5228a;

    /* renamed from: b  reason: collision with root package name */
    private final boolean f5229b = true;

    public final boolean add(T t) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final boolean addAll(Collection<? extends T> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final boolean removeAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final boolean retainAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final <T> T[] toArray(T[] tArr) {
        return kotlin.d.b.f.a(this, tArr);
    }

    public f(T[] tArr, boolean z) {
        j.b(tArr, "values");
        this.f5228a = tArr;
    }

    public final boolean isEmpty() {
        return this.f5228a.length == 0;
    }

    public final boolean contains(Object obj) {
        return g.b(this.f5228a, obj);
    }

    public final boolean containsAll(Collection<? extends Object> collection) {
        j.b(collection, MessengerShareContentUtility.ELEMENTS);
        Iterable<Object> iterable = collection;
        if (((Collection) iterable).isEmpty()) {
            return true;
        }
        for (Object contains : iterable) {
            if (!contains(contains)) {
                return false;
            }
        }
        return true;
    }

    public final Iterator<T> iterator() {
        return b.a(this.f5228a);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.Object[], java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final Object[] toArray() {
        T[] tArr = this.f5228a;
        boolean z = this.f5229b;
        j.b(tArr, "$this$copyToArrayOfAny");
        if (z && j.a(tArr.getClass(), Object[].class)) {
            return tArr;
        }
        Object[] copyOf = Arrays.copyOf(tArr, tArr.length, Object[].class);
        j.a((Object) copyOf, "java.util.Arrays.copyOf(… Array<Any?>::class.java)");
        return copyOf;
    }

    public final /* bridge */ int size() {
        return this.f5228a.length;
    }
}
