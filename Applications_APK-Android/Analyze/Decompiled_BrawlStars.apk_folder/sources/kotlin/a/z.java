package kotlin.a;

import java.util.Iterator;
import kotlin.d.a.a;
import kotlin.d.b.k;

/* compiled from: _Collections.kt */
final class z extends k implements a<Iterator<? extends T>> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Iterable f5233a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    z(Iterable iterable) {
        super(0);
        this.f5233a = iterable;
    }

    public final /* synthetic */ Object invoke() {
        return this.f5233a.iterator();
    }
}
