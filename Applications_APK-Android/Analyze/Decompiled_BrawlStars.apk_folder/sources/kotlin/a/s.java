package kotlin.a;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import kotlin.d.b.j;

/* compiled from: MutableCollectionsJVM.kt */
class s extends r {
    public static final <T> void a(List list, Comparator comparator) {
        j.b(list, "$this$sortWith");
        j.b(comparator, "comparator");
        if (list.size() > 1) {
            Collections.sort(list, comparator);
        }
    }
}
