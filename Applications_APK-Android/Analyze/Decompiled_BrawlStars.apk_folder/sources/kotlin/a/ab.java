package kotlin.a;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;
import kotlin.d.b.a.a;
import kotlin.d.b.f;
import kotlin.d.b.j;

/* compiled from: Collections.kt */
public final class ab implements Serializable, List, RandomAccess, a {

    /* renamed from: a  reason: collision with root package name */
    public static final ab f5210a = new ab();
    private static final long serialVersionUID = -7390468764508069838L;

    public final /* synthetic */ void add(int i, Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final /* synthetic */ boolean add(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final boolean addAll(int i, Collection collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final boolean addAll(Collection collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final int hashCode() {
        return 1;
    }

    public final boolean isEmpty() {
        return true;
    }

    public final /* synthetic */ Object remove(int i) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final boolean removeAll(Collection collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final boolean retainAll(Collection collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final /* synthetic */ Object set(int i, Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final /* bridge */ int size() {
        return 0;
    }

    public final Object[] toArray() {
        return f.a(this);
    }

    public final <T> T[] toArray(T[] tArr) {
        return f.a(this, tArr);
    }

    public final String toString() {
        return "[]";
    }

    private ab() {
    }

    public final boolean contains(Object obj) {
        if (obj instanceof Void) {
            j.b((Void) obj, "element");
        }
        return false;
    }

    public final int indexOf(Object obj) {
        if (obj instanceof Void) {
            j.b((Void) obj, "element");
        }
        return -1;
    }

    public final int lastIndexOf(Object obj) {
        if (obj instanceof Void) {
            j.b((Void) obj, "element");
        }
        return -1;
    }

    public final boolean equals(Object obj) {
        return (obj instanceof List) && ((List) obj).isEmpty();
    }

    public final boolean containsAll(Collection collection) {
        j.b(collection, MessengerShareContentUtility.ELEMENTS);
        return collection.isEmpty();
    }

    public final Iterator iterator() {
        return aa.f5209a;
    }

    public final ListIterator listIterator() {
        return aa.f5209a;
    }

    public final ListIterator listIterator(int i) {
        if (i == 0) {
            return aa.f5209a;
        }
        throw new IndexOutOfBoundsException("Index: " + i);
    }

    public final List subList(int i, int i2) {
        if (i == 0 && i2 == 0) {
            return this;
        }
        throw new IndexOutOfBoundsException("fromIndex: " + i + ", toIndex: " + i2);
    }

    private final Object readResolve() {
        return f5210a;
    }

    public final /* synthetic */ Object get(int i) {
        throw new IndexOutOfBoundsException("Empty list doesn't contain element at index " + i + '.');
    }
}
