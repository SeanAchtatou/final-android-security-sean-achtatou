package kotlin.a;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import kotlin.d.b.j;

/* compiled from: Sets.kt */
public class ap extends ao {
    public static final <T> Set<T> a(Object... objArr) {
        j.b(objArr, MessengerShareContentUtility.ELEMENTS);
        return objArr.length > 0 ? g.c(objArr) : ad.f5212a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.k.b(java.lang.Object[], java.util.Collection):C
     arg types: [T[], java.util.LinkedHashSet]
     candidates:
      kotlin.a.k.b(java.lang.Object[], java.lang.Object):boolean
      kotlin.a.k.b(java.lang.Object[], java.util.Collection):C */
    public static final <T> Set<T> b(T... tArr) {
        j.b(tArr, MessengerShareContentUtility.ELEMENTS);
        return (Set) g.b((Object[]) tArr, (Collection) new LinkedHashSet(ai.a(tArr.length)));
    }

    public static final <T> Set<T> a(Set set) {
        j.b(set, "$this$optimizeReadOnlySet");
        int size = set.size();
        if (size != 0) {
            return size != 1 ? set : an.a(set.iterator().next());
        }
        return ad.f5212a;
    }
}
