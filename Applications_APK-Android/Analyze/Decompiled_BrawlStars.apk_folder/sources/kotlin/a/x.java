package kotlin.a;

import java.util.Iterator;
import kotlin.i.h;

/* compiled from: Sequences.kt */
public final class x implements h<T> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Iterable f5231a;

    public x(Iterable iterable) {
        this.f5231a = iterable;
    }

    public final Iterator<T> a() {
        return this.f5231a.iterator();
    }
}
