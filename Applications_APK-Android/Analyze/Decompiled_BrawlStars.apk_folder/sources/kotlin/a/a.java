package kotlin.a;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.Collection;
import kotlin.TypeCastException;
import kotlin.d.b.f;
import kotlin.d.b.j;

/* compiled from: AbstractCollection.kt */
public abstract class a<E> implements Collection<E>, kotlin.d.b.a.a {
    public abstract int a();

    public boolean add(E e) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean addAll(Collection<? extends E> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean removeAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean retainAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    protected a() {
    }

    public final int size() {
        return a();
    }

    public boolean containsAll(Collection<? extends Object> collection) {
        j.b(collection, MessengerShareContentUtility.ELEMENTS);
        Iterable<Object> iterable = collection;
        if (((Collection) iterable).isEmpty()) {
            return true;
        }
        for (Object contains : iterable) {
            if (!contains(contains)) {
                return false;
            }
        }
        return true;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public String toString() {
        return m.a(this, ", ", "[", "]", 0, (CharSequence) null, new b(this), 24);
    }

    public Object[] toArray() {
        return f.a(this);
    }

    public <T> T[] toArray(T[] tArr) {
        j.b(tArr, "array");
        T[] a2 = f.a(this, tArr);
        if (a2 != null) {
            return a2;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    public boolean contains(Object obj) {
        if (isEmpty()) {
            return false;
        }
        for (Object a2 : this) {
            if (j.a(a2, obj)) {
                return true;
            }
        }
        return false;
    }
}
