package kotlin.a;

import java.util.Iterator;
import java.util.NoSuchElementException;
import kotlin.d.b.a.a;

/* compiled from: AbstractIterator.kt */
public abstract class c<T> implements Iterator<T>, a {

    /* renamed from: a  reason: collision with root package name */
    protected ar f5221a = ar.NotReady;

    /* renamed from: b  reason: collision with root package name */
    protected T f5222b;

    /* access modifiers changed from: protected */
    public abstract void a();

    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean hasNext() {
        if (this.f5221a != ar.d) {
            int i = d.f5223a[this.f5221a.ordinal()];
            if (i == 1) {
                return false;
            }
            if (i != 2) {
                this.f5221a = ar.d;
                a();
                return this.f5221a == ar.Ready;
            }
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }

    public T next() {
        if (hasNext()) {
            this.f5221a = ar.NotReady;
            return this.f5222b;
        }
        throw new NoSuchElementException();
    }
}
