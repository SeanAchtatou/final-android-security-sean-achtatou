package kotlin.a;

import java.util.Iterator;
import kotlin.d.b.a.a;

/* compiled from: Iterables.kt */
public final class af<T> implements Iterable<ae<? extends T>>, a {

    /* renamed from: a  reason: collision with root package name */
    private final kotlin.d.a.a<Iterator<T>> f5215a;

    /* JADX WARN: Type inference failed for: r2v0, types: [kotlin.d.a.a<java.util.Iterator<T>>, java.lang.Object, kotlin.d.a.a<? extends java.util.Iterator<? extends T>>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public af(kotlin.d.a.a<? extends java.util.Iterator<? extends T>> r2) {
        /*
            r1 = this;
            java.lang.String r0 = "iteratorFactory"
            kotlin.d.b.j.b(r2, r0)
            r1.<init>()
            r1.f5215a = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlin.a.af.<init>(kotlin.d.a.a):void");
    }

    public final Iterator<ae<T>> iterator() {
        return new ag<>(this.f5215a.invoke());
    }
}
