package kotlin.a;

import com.facebook.internal.FacebookRequestErrorClassification;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.facebook.share.internal.ShareConstants;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.RandomAccess;
import java.util.Set;
import kotlin.TypeCastException;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.f.d;
import kotlin.i.h;
import kotlin.j.t;

/* compiled from: _Collections.kt */
public class w extends v {
    public static final <T> T b(Iterable iterable, int i) {
        j.b(iterable, "$this$elementAt");
        boolean z = iterable instanceof List;
        if (z) {
            return ((List) iterable).get(i);
        }
        b yVar = new y(i);
        j.b(iterable, "$this$elementAtOrElse");
        j.b(yVar, "defaultValue");
        if (z) {
            List list = (List) iterable;
            return (i < 0 || i > m.a(list)) ? yVar.invoke(Integer.valueOf(i)) : list.get(i);
        } else if (i < 0) {
            return yVar.invoke(Integer.valueOf(i));
        } else {
            int i2 = 0;
            for (T next : iterable) {
                int i3 = i2 + 1;
                if (i == i2) {
                    return next;
                }
                i2 = i3;
            }
            return yVar.invoke(Integer.valueOf(i));
        }
    }

    public static final <T> T c(Iterable iterable) {
        j.b(iterable, "$this$first");
        if (iterable instanceof List) {
            return m.c((List) iterable);
        }
        Iterator it = iterable.iterator();
        if (it.hasNext()) {
            return it.next();
        }
        throw new NoSuchElementException("Collection is empty.");
    }

    public static final <T> T c(List list) {
        j.b(list, "$this$first");
        if (!list.isEmpty()) {
            return list.get(0);
        }
        throw new NoSuchElementException("List is empty.");
    }

    public static final <T> T d(Iterable iterable) {
        j.b(iterable, "$this$firstOrNull");
        if (iterable instanceof List) {
            List list = (List) iterable;
            if (list.isEmpty()) {
                return null;
            }
            return list.get(0);
        }
        Iterator it = iterable.iterator();
        if (!it.hasNext()) {
            return null;
        }
        return it.next();
    }

    public static final <T> T d(List list) {
        j.b(list, "$this$firstOrNull");
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    public static final <T> T a(List list, int i) {
        j.b(list, "$this$getOrNull");
        if (i < 0 || i > m.a(list)) {
            return null;
        }
        return list.get(i);
    }

    public static final <T> int a(Iterable iterable, Object obj) {
        j.b(iterable, "$this$indexOf");
        if (iterable instanceof List) {
            return ((List) iterable).indexOf(obj);
        }
        int i = 0;
        for (Object next : iterable) {
            if (i < 0) {
                m.a();
            }
            if (j.a(obj, next)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    public static final <T> T e(Iterable iterable) {
        j.b(iterable, "$this$last");
        if (iterable instanceof List) {
            return m.e((List) iterable);
        }
        Iterator it = iterable.iterator();
        if (it.hasNext()) {
            T next = it.next();
            while (it.hasNext()) {
                next = it.next();
            }
            return next;
        }
        throw new NoSuchElementException("Collection is empty.");
    }

    public static final <T> T e(List list) {
        j.b(list, "$this$last");
        if (!list.isEmpty()) {
            return list.get(m.a(list));
        }
        throw new NoSuchElementException("List is empty.");
    }

    public static final <T> T f(List list) {
        j.b(list, "$this$lastOrNull");
        if (list.isEmpty()) {
            return null;
        }
        return list.get(list.size() - 1);
    }

    public static final <T> T a(Collection collection, d dVar) {
        j.b(collection, "$this$random");
        j.b(dVar, "random");
        if (!collection.isEmpty()) {
            return m.b(collection, dVar.b(collection.size()));
        }
        throw new NoSuchElementException("Collection is empty.");
    }

    public static final <T> List<T> c(Iterable iterable, int i) {
        ArrayList arrayList;
        j.b(iterable, "$this$drop");
        int i2 = 0;
        if (!(i >= 0)) {
            throw new IllegalArgumentException(("Requested element count " + i + " is less than zero.").toString());
        } else if (i == 0) {
            return m.g(iterable);
        } else {
            if (iterable instanceof Collection) {
                Collection collection = (Collection) iterable;
                int size = collection.size() - i;
                if (size <= 0) {
                    return ab.f5210a;
                }
                if (size == 1) {
                    return m.a(m.e(iterable));
                }
                arrayList = new ArrayList(size);
                if (iterable instanceof List) {
                    if (iterable instanceof RandomAccess) {
                        int size2 = collection.size();
                        while (i < size2) {
                            arrayList.add(((List) iterable).get(i));
                            i++;
                        }
                    } else {
                        Iterator listIterator = ((List) iterable).listIterator(i);
                        while (listIterator.hasNext()) {
                            arrayList.add(listIterator.next());
                        }
                    }
                    return arrayList;
                }
            } else {
                arrayList = new ArrayList();
            }
            for (Object next : iterable) {
                if (i2 >= i) {
                    arrayList.add(next);
                } else {
                    i2++;
                }
            }
            return m.b((List) arrayList);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.s.a(java.util.List, java.util.Comparator):void
     arg types: [java.util.List<T>, java.util.Comparator]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.s.a(java.util.List, java.util.Comparator):void */
    public static final <T> List<T> a(Iterable iterable, Comparator comparator) {
        j.b(iterable, "$this$sortedWith");
        j.b(comparator, "comparator");
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            if (collection.size() <= 1) {
                return m.g(iterable);
            }
            Object[] array = collection.toArray(new Object[0]);
            if (array == null) {
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            } else if (array != null) {
                g.a(array, comparator);
                return g.a(array);
            } else {
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            }
        } else {
            List<T> h = m.h(iterable);
            m.a((List) h, comparator);
            return h;
        }
    }

    public static final <T, C extends Collection<? super T>> C a(Iterable iterable, Collection collection) {
        j.b(iterable, "$this$toCollection");
        j.b(collection, ShareConstants.DESTINATION);
        for (Object add : iterable) {
            collection.add(add);
        }
        return collection;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
     arg types: [java.lang.Iterable, java.util.HashSet]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C */
    public static final <T> HashSet<T> f(Iterable iterable) {
        j.b(iterable, "$this$toHashSet");
        return (HashSet) m.a(iterable, (Collection) new HashSet(ai.a(m.a(iterable, 12))));
    }

    public static final <T> List<T> g(Iterable<? extends T> iterable) {
        j.b(iterable, "$this$toList");
        if (!(iterable instanceof Collection)) {
            return m.b((List) m.h(iterable));
        }
        Collection collection = (Collection) iterable;
        int size = collection.size();
        if (size == 0) {
            return ab.f5210a;
        }
        if (size != 1) {
            return m.a(collection);
        }
        return m.a(iterable instanceof List ? ((List) iterable).get(0) : iterable.iterator().next());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
     arg types: [java.lang.Iterable<? extends T>, java.util.ArrayList]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C */
    public static final <T> List<T> h(Iterable<? extends T> iterable) {
        j.b(iterable, "$this$toMutableList");
        if (iterable instanceof Collection) {
            return m.a((Collection) iterable);
        }
        return (List) m.a((Iterable) iterable, (Collection) new ArrayList());
    }

    public static final <T> List<T> a(Collection collection) {
        j.b(collection, "$this$toMutableList");
        return new ArrayList<>(collection);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
     arg types: [java.lang.Iterable<? extends T>, java.util.LinkedHashSet]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C */
    public static final <T> Set<T> i(Iterable<? extends T> iterable) {
        j.b(iterable, "$this$toSet");
        if (!(iterable instanceof Collection)) {
            return an.a((Set) m.a((Iterable) iterable, (Collection) new LinkedHashSet()));
        }
        Collection collection = (Collection) iterable;
        int size = collection.size();
        if (size == 0) {
            return ad.f5212a;
        }
        if (size != 1) {
            return (Set) m.a((Iterable) iterable, (Collection) new LinkedHashSet(ai.a(collection.size())));
        }
        return an.a(iterable instanceof List ? ((List) iterable).get(0) : iterable.iterator().next());
    }

    public static final <T> Iterable<ae<T>> j(Iterable<? extends T> iterable) {
        j.b(iterable, "$this$withIndex");
        return new af<>(new z(iterable));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.t.c(java.util.Collection, java.lang.Iterable):boolean
     arg types: [java.util.Set<T>, java.lang.Iterable]
     candidates:
      kotlin.a.w.c(java.lang.Iterable, int):java.util.List<T>
      kotlin.a.w.c(java.lang.Iterable, java.lang.Iterable):java.util.Set<T>
      kotlin.a.t.c(java.util.Collection, java.lang.Iterable):boolean */
    public static final <T> Set<T> b(Iterable iterable, Iterable iterable2) {
        j.b(iterable, "$this$intersect");
        j.b(iterable2, FacebookRequestErrorClassification.KEY_OTHER);
        Set<T> k = m.k(iterable);
        m.c((Collection) k, iterable2);
        return k;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.t.b(java.util.Collection, java.lang.Iterable):boolean
     arg types: [java.util.Set<T>, java.lang.Iterable]
     candidates:
      kotlin.a.w.b(java.lang.Iterable, int):T
      kotlin.a.w.b(java.lang.Iterable, java.lang.Iterable):java.util.Set<T>
      kotlin.a.t.b(java.util.Collection, java.lang.Iterable):boolean */
    public static final <T> Set<T> c(Iterable iterable, Iterable iterable2) {
        j.b(iterable, "$this$subtract");
        j.b(iterable2, FacebookRequestErrorClassification.KEY_OTHER);
        Set<T> k = m.k(iterable);
        m.b((Collection) k, iterable2);
        return k;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
     arg types: [java.lang.Iterable<? extends T>, java.util.LinkedHashSet]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C */
    public static final <T> Set<T> k(Iterable<? extends T> iterable) {
        j.b(iterable, "$this$toMutableSet");
        if (iterable instanceof Collection) {
            return new LinkedHashSet<>((Collection) iterable);
        }
        return (Set) m.a((Iterable) iterable, (Collection) new LinkedHashSet());
    }

    public static final Float l(Iterable<Float> iterable) {
        j.b(iterable, "$this$max");
        Iterator<Float> it = iterable.iterator();
        if (!it.hasNext()) {
            return null;
        }
        float floatValue = it.next().floatValue();
        if (Float.isNaN(floatValue)) {
            return Float.valueOf(floatValue);
        }
        while (it.hasNext()) {
            float floatValue2 = it.next().floatValue();
            if (Float.isNaN(floatValue2)) {
                return Float.valueOf(floatValue2);
            }
            if (floatValue < floatValue2) {
                floatValue = floatValue2;
            }
        }
        return Float.valueOf(floatValue);
    }

    public static final <T> List<T> a(Collection collection, Object obj) {
        j.b(collection, "$this$plus");
        ArrayList arrayList = new ArrayList(collection.size() + 1);
        arrayList.addAll(collection);
        arrayList.add(obj);
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
     arg types: [java.util.ArrayList, java.lang.Iterable]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean */
    public static final <T> List<T> d(Collection collection, Iterable iterable) {
        j.b(collection, "$this$plus");
        j.b(iterable, MessengerShareContentUtility.ELEMENTS);
        if (iterable instanceof Collection) {
            Collection collection2 = (Collection) iterable;
            ArrayList arrayList = new ArrayList(collection.size() + collection2.size());
            arrayList.addAll(collection);
            arrayList.addAll(collection2);
            return arrayList;
        }
        ArrayList arrayList2 = new ArrayList(collection);
        m.a((Collection) arrayList2, iterable);
        return arrayList2;
    }

    public static final <T, A extends Appendable> A a(Iterable iterable, Appendable appendable, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, b bVar) {
        j.b(iterable, "$this$joinTo");
        j.b(appendable, "buffer");
        j.b(charSequence, "separator");
        j.b(charSequence2, "prefix");
        j.b(charSequence3, "postfix");
        j.b(charSequence4, "truncated");
        appendable.append(charSequence2);
        int i2 = 0;
        for (Object next : iterable) {
            i2++;
            if (i2 > 1) {
                appendable.append(charSequence);
            }
            if (i >= 0 && i2 > i) {
                break;
            }
            t.a(appendable, next, bVar);
        }
        if (i >= 0 && i2 > i) {
            appendable.append(charSequence4);
        }
        appendable.append(charSequence3);
        return appendable;
    }

    public static /* synthetic */ String a(Iterable iterable, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, b bVar, int i2) {
        if ((i2 & 1) != 0) {
        }
        CharSequence charSequence5 = charSequence;
        if ((i2 & 2) != 0) {
            charSequence2 = "";
        }
        CharSequence charSequence6 = charSequence2;
        if ((i2 & 4) != 0) {
            charSequence3 = "";
        }
        CharSequence charSequence7 = charSequence3;
        int i3 = (i2 & 8) != 0 ? -1 : i;
        if ((i2 & 16) != 0) {
            charSequence4 = "...";
        }
        CharSequence charSequence8 = charSequence4;
        if ((i2 & 32) != 0) {
            bVar = null;
        }
        return m.a(iterable, charSequence5, charSequence6, charSequence7, i3, charSequence8, bVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final <T> String a(Iterable<? extends T> iterable, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, b<? super T, ? extends CharSequence> bVar) {
        j.b(iterable, "$this$joinToString");
        j.b(charSequence, "separator");
        j.b(charSequence2, "prefix");
        j.b(charSequence3, "postfix");
        j.b(charSequence4, "truncated");
        String sb = ((StringBuilder) m.a(iterable, new StringBuilder(), charSequence, charSequence2, charSequence3, i, charSequence4, bVar)).toString();
        j.a((Object) sb, "joinTo(StringBuilder(), …ed, transform).toString()");
        return sb;
    }

    public static final <T> h<T> m(Iterable<? extends T> iterable) {
        j.b(iterable, "$this$asSequence");
        return new x(iterable);
    }

    public static final <T> List<T> d(Iterable iterable, int i) {
        j.b(iterable, "$this$take");
        int i2 = 0;
        if (!(i >= 0)) {
            throw new IllegalArgumentException(("Requested element count " + i + " is less than zero.").toString());
        } else if (i == 0) {
            return ab.f5210a;
        } else {
            if (iterable instanceof Collection) {
                if (i >= ((Collection) iterable).size()) {
                    return m.g(iterable);
                }
                if (i == 1) {
                    return m.a(m.c(iterable));
                }
            }
            ArrayList arrayList = new ArrayList(i);
            for (Object next : iterable) {
                int i3 = i2 + 1;
                if (i2 == i) {
                    break;
                }
                arrayList.add(next);
                i2 = i3;
            }
            return m.b((List) arrayList);
        }
    }
}
