package nl.komponents.kovenant;

import kotlin.d.b.j;

/* compiled from: dispatcher-jvm.kt */
final class ck<V> implements br<V> {

    /* renamed from: a  reason: collision with root package name */
    private final bu<V> f5435a;

    /* renamed from: b  reason: collision with root package name */
    private final int f5436b;

    public ck(bu<V> buVar, int i) {
        j.b(buVar, "pollable");
        this.f5435a = buVar;
        this.f5436b = i;
    }

    public final V a() {
        int i = this.f5436b;
        if (i < 0) {
            return null;
        }
        int i2 = 0;
        while (true) {
            V a2 = this.f5435a.a(false, -1);
            if (a2 != null) {
                return a2;
            }
            Thread.yield();
            if (Thread.currentThread().isInterrupted() || i2 == i) {
                return null;
            }
            i2++;
        }
    }
}
