package nl.komponents.kovenant;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.a.m;
import kotlin.d.b.j;

/* compiled from: dispatcher-jvm.kt */
final class s<V> implements bt<V> {

    /* renamed from: a  reason: collision with root package name */
    private final List<bt<V>> f5461a;

    /* JADX WARN: Type inference failed for: r2v0, types: [java.util.List<nl.komponents.kovenant.bt<V>>, java.lang.Object, java.util.List<? extends nl.komponents.kovenant.bt<V>>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public s(java.util.List<? extends nl.komponents.kovenant.bt<V>> r2) {
        /*
            r1 = this;
            java.lang.String r0 = "factories"
            kotlin.d.b.j.b(r2, r0)
            r1.<init>()
            r1.f5461a = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: nl.komponents.kovenant.s.<init>(java.util.List):void");
    }

    public final br<V> a(bu<V> buVar) {
        j.b(buVar, "pollable");
        Iterable<bt> iterable = this.f5461a;
        Collection arrayList = new ArrayList(m.a(iterable, 10));
        for (bt a2 : iterable) {
            arrayList.add(a2.a(buVar));
        }
        return new r<>((List) arrayList);
    }
}
