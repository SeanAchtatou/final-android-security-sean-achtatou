package nl.komponents.kovenant;

import kotlin.d.a.d;
import kotlin.d.b.j;
import kotlin.d.b.k;

/* compiled from: dispatcher-jvm.kt */
final class w extends k implements d<Runnable, String, Integer, Thread> {

    /* renamed from: a  reason: collision with root package name */
    public static final w f5466a = new w();

    w() {
        super(3);
    }

    public final /* synthetic */ Object invoke(Object obj, Object obj2, Object obj3) {
        Runnable runnable = (Runnable) obj;
        String str = (String) obj2;
        int intValue = ((Number) obj3).intValue();
        j.b(runnable, "target");
        j.b(str, "dispatcherName");
        Thread thread = new Thread(runnable, str + "-" + intValue);
        thread.setDaemon(false);
        return thread;
    }
}
