package nl.komponents.kovenant;

import kotlin.d.a.a;
import kotlin.d.a.b;
import kotlin.d.b.j;

/* compiled from: promises-jvm.kt */
public final class bx {
    public static final <V> bw<V, Exception> a(ao aoVar, a aVar) {
        j.b(aoVar, "context");
        j.b(aVar, "callable");
        return new cd<>(aoVar, aVar);
    }

    public static final <V, R> bw<R, Exception> a(ao aoVar, bw<? extends V, ? extends Exception> bwVar, b<? super V, ? extends R> bVar) {
        j.b(aoVar, "context");
        j.b(bwVar, "promise");
        j.b(bVar, "callable");
        return new cf<>(aoVar, bwVar, bVar);
    }

    public static final <V, E> bw<V, E> a(ao aoVar, Object obj) {
        j.b(aoVar, "context");
        return new cc<>(aoVar, obj);
    }

    public static final <V, E> bw<V, E> b(ao aoVar, E e) {
        j.b(aoVar, "context");
        return new az<>(aoVar, e);
    }
}
