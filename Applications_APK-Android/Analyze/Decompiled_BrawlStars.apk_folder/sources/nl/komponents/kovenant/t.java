package nl.komponents.kovenant;

import kotlin.d.a.a;
import kotlin.d.a.b;
import kotlin.d.a.d;
import kotlin.d.b.j;
import kotlin.m;

/* compiled from: dispatcher-jvm.kt */
final class t implements ba {

    /* renamed from: a  reason: collision with root package name */
    private String f5462a = "kovenant-dispatcher";

    /* renamed from: b  reason: collision with root package name */
    private int f5463b = Math.max(Runtime.getRuntime().availableProcessors() - 1, 1);
    private b<? super Exception, m> c = v.f5465a;
    private b<? super Throwable, m> d = u.f5464a;
    private cj<a<m>> e = new bp();
    private final an f = new an();
    private d<? super Runnable, ? super String, ? super Integer, ? extends Thread> g = w.f5466a;

    public final void a(String str) {
        j.b(str, "<set-?>");
        this.f5462a = str;
    }

    public final void a(d<? super Runnable, ? super String, ? super Integer, ? extends Thread> dVar) {
        j.b(dVar, "<set-?>");
        this.g = dVar;
    }

    public final void a(int i) {
        this.f5463b = 1;
    }

    public final void a(b<? super bs, m> bVar) {
        j.b(bVar, "body");
        this.f.f5375a.clear();
        bVar.invoke(this.f);
    }

    public final av a() {
        br brVar;
        cj<a<m>> cjVar = this.e;
        String str = this.f5462a;
        int i = this.f5463b;
        b<? super Exception, m> bVar = this.c;
        b<? super Throwable, m> bVar2 = this.d;
        an anVar = this.f;
        bu buVar = cjVar;
        j.b(buVar, "pollable");
        if (anVar.f5375a.isEmpty()) {
            brVar = new s(kotlin.a.m.a((Object[]) new bt[]{new cl(0, 1), new cb(0, 0, 3)})).a(buVar);
        } else {
            brVar = new s(anVar.f5375a).a(buVar);
        }
        return new bo(str, i, bVar, bVar2, cjVar, brVar, this.g);
    }
}
