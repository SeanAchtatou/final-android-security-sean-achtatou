package nl.komponents.kovenant.d;

import java.lang.reflect.Field;
import kotlin.TypeCastException;
import kotlin.d.b.j;
import sun.misc.Unsafe;

/* compiled from: cas-jvm.kt */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f5440a = new Object();

    /* renamed from: b  reason: collision with root package name */
    private static volatile Object f5441b;

    public static final boolean a() {
        if (f5441b == null) {
            c();
        }
        return !j.a(f5441b, f5440a);
    }

    private static final void c() {
        try {
            Class<?> cls = Class.forName("sun.misc.Unsafe");
            try {
                Field declaredField = cls.getDeclaredField("theUnsafe");
                declaredField.setAccessible(true);
                Object obj = declaredField.get(null);
                if (obj == null) {
                    obj = f5440a;
                }
                f5441b = obj;
            } catch (Exception unused) {
                Field declaredField2 = cls.getDeclaredField("THE_ONE");
                declaredField2.setAccessible(true);
                Object obj2 = declaredField2.get(null);
                if (obj2 == null) {
                    obj2 = f5440a;
                }
                f5441b = obj2;
            }
        } catch (Exception unused2) {
            f5441b = f5440a;
        }
    }

    /* access modifiers changed from: private */
    public static final Unsafe d() {
        if (f5441b == null) {
            c();
        }
        if (!j.a(f5441b, f5440a)) {
            Object obj = f5441b;
            if (obj != null) {
                return (Unsafe) obj;
            }
            throw new TypeCastException("null cannot be cast to non-null type sun.misc.Unsafe");
        }
        throw new RuntimeException("unsafe doesn't exist or is not accessible");
    }
}
