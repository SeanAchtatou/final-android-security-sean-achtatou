package nl.komponents.kovenant.d;

import java.lang.reflect.Field;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.TypeCastException;
import kotlin.d.b.d;
import kotlin.d.b.j;
import kotlin.h.c;
import sun.misc.Unsafe;

/* compiled from: cas-jvm.kt */
public final class b<C, V> extends AtomicReferenceFieldUpdater<C, V> {

    /* renamed from: a  reason: collision with root package name */
    public static final a f5442a = new a((byte) 0);
    private static final Unsafe c = a.d();

    /* renamed from: b  reason: collision with root package name */
    private final long f5443b;

    /* compiled from: cas-jvm.kt */
    public static final class a {
        private a() {
        }

        public /* synthetic */ a(byte b2) {
            this();
        }
    }

    public b(c<C> cVar, String str) {
        j.b(cVar, "targetClass");
        j.b(str, "fieldName");
        j.b(cVar, "$this$java");
        Class<?> a2 = ((d) cVar).a();
        if (a2 != null) {
            Field declaredField = a2.getDeclaredField(str);
            a aVar = f5442a;
            this.f5443b = c.objectFieldOffset(declaredField);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.Class<T>");
    }

    public final void lazySet(C c2, V v) {
        j.b(c2, "target");
        a aVar = f5442a;
        c.putOrderedObject(c2, this.f5443b, v);
    }

    public final boolean compareAndSet(C c2, V v, V v2) {
        j.b(c2, "target");
        a aVar = f5442a;
        return c.compareAndSwapObject(c2, this.f5443b, v, v2);
    }

    public final void set(C c2, V v) {
        j.b(c2, "target");
        a aVar = f5442a;
        c.putObjectVolatile(c2, this.f5443b, v);
    }

    public final V get(C c2) {
        j.b(c2, "target");
        a aVar = f5442a;
        V objectVolatile = c.getObjectVolatile(c2, this.f5443b);
        if (objectVolatile != null) {
            return (Object) objectVolatile;
        }
        throw new TypeCastException("null cannot be cast to non-null type V");
    }

    public final boolean weakCompareAndSet(C c2, V v, V v2) {
        j.b(c2, "target");
        return compareAndSet(c2, v, v2);
    }
}
