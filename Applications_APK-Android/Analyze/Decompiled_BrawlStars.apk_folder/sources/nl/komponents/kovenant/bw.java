package nl.komponents.kovenant;

import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.m;

/* compiled from: promises-api.kt */
public interface bw<V, E> {
    public static final a d = new a((byte) 0);

    V a() throws Exception;

    bw<V, E> a(b<? super V, m> bVar);

    bw<V, E> a(ax axVar, kotlin.d.a.a<m> aVar);

    bw<V, E> a(ax axVar, b<? super V, m> bVar);

    E b() throws FailedException;

    bw<V, E> b(b<? super E, m> bVar);

    bw<V, E> b(ax axVar, b<? super E, m> bVar);

    boolean d();

    boolean e();

    boolean f();

    ao h();

    /* compiled from: promises-api.kt */
    public static final class a {
        private a() {
        }

        public /* synthetic */ a(byte b2) {
            this();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: nl.komponents.kovenant.bx.a(nl.komponents.kovenant.ao, java.lang.Object):nl.komponents.kovenant.bw<V, E>
         arg types: [nl.komponents.kovenant.ao, V]
         candidates:
          nl.komponents.kovenant.bx.a(nl.komponents.kovenant.ao, kotlin.d.a.a):nl.komponents.kovenant.bw<V, java.lang.Exception>
          nl.komponents.kovenant.bx.a(nl.komponents.kovenant.ao, java.lang.Object):nl.komponents.kovenant.bw<V, E> */
        public static <V, E> bw<V, E> a(V v, ao aoVar) {
            j.b(aoVar, "context");
            return bx.a(aoVar, (Object) v);
        }

        public static <V, E> bw<V, E> b(E e, ao aoVar) {
            j.b(aoVar, "context");
            return bx.b(aoVar, e);
        }
    }
}
