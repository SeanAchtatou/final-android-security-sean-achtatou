package nl.komponents.kovenant;

import com.facebook.appevents.UserDataStore;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.m;
import nl.komponents.kovenant.ax;

/* compiled from: context-api.kt */
public interface bn extends ax {

    /* compiled from: context-api.kt */
    public static final class a {
        public static void a(bn bnVar, kotlin.d.a.a<m> aVar) {
            j.b(aVar, UserDataStore.FIRST_NAME);
            ax.b.a(bnVar, aVar);
        }
    }

    void a(b<? super Exception, m> bVar);

    void a(av avVar);
}
