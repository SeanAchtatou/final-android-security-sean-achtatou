package nl.komponents.kovenant;

/* compiled from: exceptions-api.kt */
public class KovenantException extends Exception {
    public KovenantException() {
        this(null, null, 3);
    }

    public KovenantException(String str, Exception exc) {
        super(str, exc);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ KovenantException(String str, Exception exc, int i) {
        this((i & 1) != 0 ? null : str, null);
    }
}
