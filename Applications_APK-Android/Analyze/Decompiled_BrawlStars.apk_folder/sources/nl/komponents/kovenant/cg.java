package nl.komponents.kovenant;

import kotlin.d.a.a;
import kotlin.d.a.b;
import kotlin.d.b.k;
import kotlin.m;

/* compiled from: promises-jvm.kt */
final class cg extends k implements a<m> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ cf f5430a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ b f5431b;
    final /* synthetic */ Object c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    cg(cf cfVar, b bVar, Object obj) {
        super(0);
        this.f5430a = cfVar;
        this.f5431b = bVar;
        this.c = obj;
    }

    public final /* synthetic */ Object invoke() {
        try {
            this.f5430a.e(this.f5431b.invoke(this.c));
        } catch (Exception e) {
            this.f5430a.f(e);
        } catch (Throwable th) {
            this.f5430a.c = null;
            throw th;
        }
        this.f5430a.c = null;
        return m.f5330a;
    }
}
