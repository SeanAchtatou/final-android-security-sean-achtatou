package nl.komponents.kovenant;

import java.io.PrintStream;
import kotlin.TypeCastException;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

/* compiled from: dispatcher-jvm.kt */
final class v extends k implements b<Exception, m> {

    /* renamed from: a  reason: collision with root package name */
    public static final v f5465a = new v();

    v() {
        super(1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.io.PrintStream, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final /* synthetic */ Object invoke(Object obj) {
        Exception exc = (Exception) obj;
        j.b(exc, "e");
        Throwable th = exc;
        PrintStream printStream = System.err;
        j.a((Object) printStream, "System.err");
        if (th != null) {
            th.printStackTrace(printStream);
            return m.f5330a;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.Throwable");
    }
}
