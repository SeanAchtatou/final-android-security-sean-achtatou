package nl.komponents.kovenant;

import kotlin.d.a.a;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.m;

/* compiled from: promises-jvm.kt */
final class cf<V, R> extends bz<R, Exception> implements q<R, Exception> {
    /* access modifiers changed from: private */
    public volatile a<m> c;

    public final /* synthetic */ boolean g(Object obj) {
        Exception exc = (Exception) obj;
        j.b(exc, "error");
        a<m> aVar = this.c;
        if (aVar != null) {
            this.c = null;
            this.f5335a.e().a().b(aVar);
            if (d(exc)) {
                b(exc);
                return true;
            }
        }
        return false;
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cf(ao aoVar, bw<? extends V, ? extends Exception> bwVar, b<? super V, ? extends R> bVar) {
        super(aoVar);
        j.b(aoVar, "context");
        j.b(bwVar, "promise");
        j.b(bVar, "callable");
        if (!bwVar.d()) {
            bwVar.a(at.f5380a, new ch(this, aoVar, bVar));
            bwVar.b(at.f5380a, new ci(this));
        } else if (bwVar.f()) {
            a(aoVar, bwVar.a(), bVar);
        } else {
            f(bwVar.b());
        }
    }

    /* access modifiers changed from: private */
    public final void a(ao aoVar, V v, b<? super V, ? extends R> bVar) {
        a<m> cgVar = new cg(this, bVar, v);
        this.c = cgVar;
        aoVar.e().a(cgVar);
    }
}
