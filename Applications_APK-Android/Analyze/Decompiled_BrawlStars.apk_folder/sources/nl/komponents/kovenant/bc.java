package nl.komponents.kovenant;

import kotlin.d.a.a;
import kotlin.d.a.b;
import kotlin.d.b.j;
import nl.komponents.kovenant.bw;

/* compiled from: promises-api.kt */
public final class bc {
    public static final <V, E> ap<V, E> a(ao aoVar) {
        j.b(aoVar, "context");
        bb bbVar = bb.f5389a;
        return bb.a(aoVar);
    }

    public static final <V> bw<V, Exception> a(ao aoVar, a aVar) {
        j.b(aoVar, "context");
        j.b(aVar, "body");
        return bx.a(aoVar, aVar);
    }

    public static final <V, R> bw<R, Exception> a(bw bwVar, b bVar) {
        j.b(bwVar, "$receiver");
        j.b(bVar, "bind");
        return bx.a(bwVar.h(), bwVar, bVar);
    }

    /* access modifiers changed from: private */
    public static <V, E> bw<V, E> a(bw bwVar, ao aoVar) {
        j.b(bwVar, "$receiver");
        j.b(aoVar, "context");
        if (bwVar.d()) {
            if (bwVar.f()) {
                bw<V, E> bwVar2 = (bw) bwVar.a();
                j.b(bwVar2, "$receiver");
                j.b(aoVar, "context");
                if (j.a(bwVar2.h(), aoVar)) {
                    return bwVar2;
                }
                if (bwVar2.d()) {
                    if (bwVar2.f()) {
                        bw.a aVar = bw.d;
                        return bw.a.a(bwVar2.a(), aoVar);
                    } else if (bwVar2.e()) {
                        bw.a aVar2 = bw.d;
                        return bw.a.b(bwVar2.b(), aoVar);
                    }
                }
                ap a2 = a(aoVar);
                bwVar2.a(new bj(a2));
                bwVar2.b(new bk(a2));
                return a2.i();
            } else if (bwVar.e()) {
                bw.a aVar3 = bw.d;
                return bw.a.b(bwVar.b(), aoVar);
            }
        }
        ap a3 = a(aoVar);
        bwVar.a(new bd(a3)).b(new bg(a3));
        return a3.i();
    }
}
