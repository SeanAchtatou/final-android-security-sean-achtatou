package nl.komponents.kovenant;

/* compiled from: exceptions-api.kt */
public class UnsupportedException extends KovenantException {
    public UnsupportedException() {
        this(null, null, 3);
    }

    private UnsupportedException(String str, Exception exc) {
        super(str, exc);
    }

    private /* synthetic */ UnsupportedException(String str, Exception exc, int i) {
        this(null, null);
    }
}
