package nl.komponents.kovenant;

import com.facebook.appevents.UserDataStore;
import kotlin.d.a.a;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.m;
import nl.komponents.kovenant.as;
import nl.komponents.kovenant.ax;

/* compiled from: context-api.kt */
public final class at implements ax {

    /* renamed from: a  reason: collision with root package name */
    public static final at f5380a = null;
    private static final b<Exception, m> c = null;
    private static final av d = null;

    static {
        new at();
    }

    private at() {
        f5380a = this;
        c = au.f5381a;
        as.a aVar = as.f5378a;
        d = as.f5379b;
    }

    public final void a(a<m> aVar) {
        j.b(aVar, UserDataStore.FIRST_NAME);
        ax.b.a(this, aVar);
    }

    public final av a() {
        return d;
    }

    public final b<Exception, m> b() {
        return c;
    }
}
