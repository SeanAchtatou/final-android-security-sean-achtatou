package nl.komponents.kovenant;

/* compiled from: exceptions-api.kt */
public class FailedException extends KovenantException {

    /* renamed from: a  reason: collision with root package name */
    private final Object f5333a;

    public FailedException(Object obj) {
        super(String.valueOf(obj), null, 2);
        this.f5333a = obj;
    }
}
