package nl.komponents.kovenant;

import kotlin.d.a.a;
import kotlin.d.b.j;
import kotlin.m;

/* compiled from: promises-jvm.kt */
final class cd<V> extends bz<V, Exception> implements q<V, Exception> {
    /* access modifiers changed from: private */
    public volatile a<m> c;

    public final /* synthetic */ boolean g(Object obj) {
        Exception exc = (Exception) obj;
        j.b(exc, "error");
        a<m> aVar = this.c;
        if (aVar != null) {
            this.c = null;
            this.f5335a.e().a().b(aVar);
            if (d(exc)) {
                b(exc);
                return true;
            }
        }
        return false;
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cd(ao aoVar, a<? extends V> aVar) {
        super(aoVar);
        j.b(aoVar, "context");
        j.b(aVar, "callable");
        a<m> ceVar = new ce(this, aVar);
        this.c = ceVar;
        aoVar.e().a(ceVar);
    }
}
