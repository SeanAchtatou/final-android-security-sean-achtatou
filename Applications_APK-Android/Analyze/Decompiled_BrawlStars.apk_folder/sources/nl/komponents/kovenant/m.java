package nl.komponents.kovenant;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReferenceArray;
import kotlin.d.a.b;
import kotlin.d.b.k;
import kotlin.i.h;

/* compiled from: bulk-jvm.kt */
final class m extends k implements b<V, kotlin.m> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f5454a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ AtomicReferenceArray f5455b;
    final /* synthetic */ AtomicInteger c;
    final /* synthetic */ ap d;
    final /* synthetic */ AtomicInteger e;
    final /* synthetic */ boolean f;
    final /* synthetic */ h g;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    m(int i, AtomicReferenceArray atomicReferenceArray, AtomicInteger atomicInteger, ap apVar, AtomicInteger atomicInteger2, boolean z, h hVar) {
        super(1);
        this.f5454a = i;
        this.f5455b = atomicReferenceArray;
        this.c = atomicInteger;
        this.d = apVar;
        this.e = atomicInteger2;
        this.f = z;
        this.g = hVar;
    }

    public final /* synthetic */ Object invoke(Object obj) {
        this.f5455b.set(this.f5454a, obj);
        if (this.c.decrementAndGet() == 0) {
            ap apVar = this.d;
            AtomicReferenceArray atomicReferenceArray = this.f5455b;
            ArrayList arrayList = new ArrayList();
            int i = 0;
            int length = atomicReferenceArray.length() - 1;
            if (length >= 0) {
                while (true) {
                    arrayList.add(atomicReferenceArray.get(i));
                    if (i == length) {
                        break;
                    }
                    i++;
                }
            }
            apVar.e(arrayList);
        }
        return kotlin.m.f5330a;
    }
}
