package nl.komponents.kovenant;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReferenceArray;
import kotlin.d.b.j;
import kotlin.i.h;

/* compiled from: bulk-jvm.kt */
public final class l {
    public static final <V> bw<List<V>, Exception> a(h<? extends bw<? extends V, ? extends Exception>> hVar, int i, ao aoVar, boolean z) {
        int i2 = i;
        j.b(hVar, "promises");
        j.b(aoVar, "context");
        if (i2 != 0) {
            ap a2 = bc.a(aoVar);
            AtomicReferenceArray atomicReferenceArray = new AtomicReferenceArray(i2);
            AtomicInteger atomicInteger = new AtomicInteger(i2);
            AtomicInteger atomicInteger2 = new AtomicInteger(0);
            Iterator<? extends bw<? extends V, ? extends Exception>> a3 = hVar.a();
            int i3 = 0;
            while (a3.hasNext()) {
                bw bwVar = (bw) a3.next();
                AtomicReferenceArray atomicReferenceArray2 = atomicReferenceArray;
                AtomicInteger atomicInteger3 = atomicInteger;
                ap apVar = a2;
                AtomicInteger atomicInteger4 = atomicInteger2;
                boolean z2 = z;
                h<? extends bw<? extends V, ? extends Exception>> hVar2 = hVar;
                bwVar.a(new m(i3, atomicReferenceArray2, atomicInteger3, apVar, atomicInteger4, z2, hVar2));
                bwVar.b(new n(bwVar, atomicReferenceArray2, atomicInteger3, apVar, atomicInteger4, z2, hVar2));
                i3++;
            }
            return a2.i();
        }
        throw new IllegalArgumentException("no promises provided");
    }
}
