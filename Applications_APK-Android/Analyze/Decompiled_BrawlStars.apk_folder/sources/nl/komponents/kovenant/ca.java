package nl.komponents.kovenant;

import kotlin.d.b.j;

/* compiled from: dispatcher-jvm.kt */
final class ca<V> implements br<V> {

    /* renamed from: a  reason: collision with root package name */
    private final bu<V> f5424a;

    /* renamed from: b  reason: collision with root package name */
    private final int f5425b;
    private final long c;

    public ca(bu<V> buVar, int i, long j) {
        j.b(buVar, "pollable");
        this.f5424a = buVar;
        this.f5425b = i;
        this.c = j;
    }

    public final V a() {
        int i = this.f5425b;
        if (i < 0) {
            return null;
        }
        int i2 = 0;
        while (true) {
            V a2 = this.f5424a.a(false, -1);
            if (a2 != null) {
                return a2;
            }
            Thread.sleep(this.c);
            if (i2 == i) {
                return null;
            }
            i2++;
        }
    }
}
