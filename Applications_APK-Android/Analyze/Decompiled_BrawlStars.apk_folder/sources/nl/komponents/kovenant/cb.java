package nl.komponents.kovenant;

import kotlin.d.b.j;

/* compiled from: dispatcher-jvm.kt */
final class cb<V> implements bt<V> {

    /* renamed from: a  reason: collision with root package name */
    private final int f5426a;

    /* renamed from: b  reason: collision with root package name */
    private final long f5427b;

    private cb(int i, long j) {
        this.f5426a = i;
        this.f5427b = j;
    }

    public /* synthetic */ cb(int i, long j, int i2) {
        this(100, 10);
    }

    public final br<V> a(bu<V> buVar) {
        j.b(buVar, "pollable");
        return new ca<>(buVar, this.f5426a, this.f5427b);
    }
}
