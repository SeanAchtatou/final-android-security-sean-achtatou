package nl.komponents.kovenant;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReferenceArray;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.i.h;
import kotlin.m;

/* compiled from: bulk-jvm.kt */
final class n extends k implements b<Exception, m> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bw f5456a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ AtomicReferenceArray f5457b;
    final /* synthetic */ AtomicInteger c;
    final /* synthetic */ ap d;
    final /* synthetic */ AtomicInteger e;
    final /* synthetic */ boolean f;
    final /* synthetic */ h g;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    n(bw bwVar, AtomicReferenceArray atomicReferenceArray, AtomicInteger atomicInteger, ap apVar, AtomicInteger atomicInteger2, boolean z, h hVar) {
        super(1);
        this.f5456a = bwVar;
        this.f5457b = atomicReferenceArray;
        this.c = atomicInteger;
        this.d = apVar;
        this.e = atomicInteger2;
        this.f = z;
        this.g = hVar;
    }

    public final /* synthetic */ Object invoke(Object obj) {
        Exception exc = (Exception) obj;
        if (this.e.incrementAndGet() == 1) {
            this.d.f(exc);
            if (this.f) {
                Iterator a2 = this.g.a();
                while (a2.hasNext()) {
                    bw bwVar = (bw) a2.next();
                    if ((!j.a(bwVar, this.f5456a)) && (bwVar instanceof q)) {
                        ((q) bwVar).g(new CancelException());
                    }
                }
            }
        }
        return m.f5330a;
    }
}
