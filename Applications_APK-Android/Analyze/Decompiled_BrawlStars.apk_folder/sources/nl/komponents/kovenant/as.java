package nl.komponents.kovenant;

import kotlin.d.b.j;
import kotlin.m;

/* compiled from: dispatcher-api.kt */
public final class as implements av {

    /* renamed from: a  reason: collision with root package name */
    public static final a f5378a = new a((byte) 0);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static final av f5379b = new as();

    public final boolean b(kotlin.d.a.a<m> aVar) {
        j.b(aVar, "task");
        return false;
    }

    /* compiled from: dispatcher-api.kt */
    public static final class a {
        private a() {
        }

        public /* synthetic */ a(byte b2) {
            this();
        }
    }

    private as() {
    }

    public final boolean a(kotlin.d.a.a<m> aVar) {
        j.b(aVar, "task");
        aVar.invoke();
        return true;
    }
}
