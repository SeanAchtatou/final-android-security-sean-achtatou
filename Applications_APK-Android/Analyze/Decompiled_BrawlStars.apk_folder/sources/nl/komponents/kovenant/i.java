package nl.komponents.kovenant;

import kotlin.d.b.j;

/* compiled from: dispatcher-jvm.kt */
final class i<V> implements br<V> {

    /* renamed from: a  reason: collision with root package name */
    private final bu<V> f5451a;

    public i(bu<V> buVar) {
        j.b(buVar, "pollable");
        this.f5451a = buVar;
    }

    public final V a() {
        return this.f5451a.a(true, -1);
    }
}
