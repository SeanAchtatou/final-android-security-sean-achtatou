package nl.komponents.kovenant;

import com.facebook.appevents.UserDataStore;
import kotlin.d.b.j;
import kotlin.m;

/* compiled from: context-api.kt */
public interface ax {

    /* renamed from: b  reason: collision with root package name */
    public static final a f5382b = new a((byte) 0);

    av a();

    void a(kotlin.d.a.a<m> aVar);

    kotlin.d.a.b<Exception, m> b();

    /* compiled from: context-api.kt */
    public static final class a {
        private a() {
        }

        public /* synthetic */ a(byte b2) {
            this();
        }
    }

    /* compiled from: context-api.kt */
    public static final class b {
        public static void a(ax axVar, kotlin.d.a.a<m> aVar) {
            j.b(aVar, UserDataStore.FIRST_NAME);
            try {
                axVar.a().a(aVar);
            } catch (Exception e) {
                axVar.b().invoke(e);
            }
        }
    }
}
