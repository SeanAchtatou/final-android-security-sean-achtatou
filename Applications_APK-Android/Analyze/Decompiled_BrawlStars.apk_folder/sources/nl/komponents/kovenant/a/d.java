package nl.komponents.kovenant.a;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import nl.komponents.kovenant.av;
import nl.komponents.kovenant.ay;
import nl.komponents.kovenant.bb;
import nl.komponents.kovenant.c.l;

/* compiled from: configuration.kt */
public final class d {

    /* renamed from: a  reason: collision with root package name */
    private static final AtomicInteger f5342a = new AtomicInteger(0);

    /* renamed from: b  reason: collision with root package name */
    private static final AtomicReference<c> f5343b = new AtomicReference<>(null);

    public static final void a() {
        AtomicInteger atomicInteger = f5342a;
        if (atomicInteger.incrementAndGet() == 1) {
            AtomicReference<c> atomicReference = f5343b;
            l lVar = l.f5418a;
            l.a(e.f5344a);
            av a2 = ay.a(i.f5353a);
            av a3 = ay.a(k.f5355a);
            bb bbVar = bb.f5389a;
            bb.a(new f(a2, a3));
            atomicReference.set(new b(a3, a2));
            return;
        }
        atomicInteger.decrementAndGet();
    }

    public static final /* synthetic */ kotlin.d.a.d a(int i) {
        return new m(10);
    }
}
