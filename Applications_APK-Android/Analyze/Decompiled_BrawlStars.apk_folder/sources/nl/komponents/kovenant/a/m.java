package nl.komponents.kovenant.a;

import kotlin.d.a.d;
import kotlin.d.b.j;
import kotlin.d.b.k;

/* compiled from: configuration.kt */
final class m extends k implements d<Runnable, String, Integer, Thread> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f5357a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    m(int i) {
        super(3);
        this.f5357a = i;
    }

    public final /* synthetic */ Object invoke(Object obj, Object obj2, Object obj3) {
        Runnable runnable = (Runnable) obj;
        String str = (String) obj2;
        int intValue = ((Number) obj3).intValue();
        j.b(runnable, "target");
        j.b(str, "dispatcherName");
        return new Thread(new n(this, runnable), str + "-" + intValue);
    }
}
