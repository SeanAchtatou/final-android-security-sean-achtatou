package nl.komponents.kovenant.a;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import java.util.concurrent.atomic.AtomicInteger;
import kotlin.TypeCastException;
import kotlin.d.b.i;
import kotlin.d.b.j;

/* compiled from: looper.kt */
public final class o implements Handler.Callback {

    /* renamed from: a  reason: collision with root package name */
    public static final a f5360a = new a((byte) 0);
    /* access modifiers changed from: private */
    public static final o f;

    /* renamed from: b  reason: collision with root package name */
    private final int f5361b = i.f5243a;
    private final Handler c = new Handler(this.e, this);
    private final AtomicInteger d = new AtomicInteger(0);
    private final Looper e;

    /* compiled from: looper.kt */
    public static final class a {
        private a() {
        }

        public /* synthetic */ a(byte b2) {
            this();
        }
    }

    private o(Looper looper) {
        j.b(looper, "looper");
        this.e = looper;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.os.Looper, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    static {
        Looper mainLooper = Looper.getMainLooper();
        j.a((Object) mainLooper, "Looper.getMainLooper()");
        f = new o(mainLooper);
    }

    public final boolean handleMessage(Message message) {
        j.b(message, NotificationCompat.CATEGORY_MESSAGE);
        Object obj = message.obj;
        if (obj != null) {
            ((Runnable) obj).run();
            return true;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.Runnable");
    }

    public static /* synthetic */ void a(o oVar, Runnable runnable, int i, int i2, Object obj) {
        int i3 = oVar.f5361b;
        j.b(runnable, "runnable");
        oVar.c.sendMessage(oVar.c.obtainMessage(i3, runnable));
    }
}
