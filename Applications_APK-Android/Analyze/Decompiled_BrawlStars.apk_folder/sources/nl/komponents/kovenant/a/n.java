package nl.komponents.kovenant.a;

import android.os.Process;

/* compiled from: configuration.kt */
final class n implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ m f5358a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Runnable f5359b;

    n(m mVar, Runnable runnable) {
        this.f5358a = mVar;
        this.f5359b = runnable;
    }

    public final void run() {
        Process.setThreadPriority(this.f5358a.f5357a);
        this.f5359b.run();
    }
}
