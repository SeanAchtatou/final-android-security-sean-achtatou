package nl.komponents.kovenant.a;

import kotlin.d.b.j;
import kotlin.m;
import nl.komponents.kovenant.a.o;
import nl.komponents.kovenant.av;

/* compiled from: dispatcher.kt */
public final class a implements av {

    /* renamed from: a  reason: collision with root package name */
    public static final C0165a f5336a = new C0165a((byte) 0);
    /* access modifiers changed from: private */
    public static final av c = new a(o.f);

    /* renamed from: b  reason: collision with root package name */
    private final o f5337b;

    /* renamed from: nl.komponents.kovenant.a.a$a  reason: collision with other inner class name */
    /* compiled from: dispatcher.kt */
    public static final class C0165a {
        private C0165a() {
        }

        public /* synthetic */ C0165a(byte b2) {
            this();
        }
    }

    private a(o oVar) {
        j.b(oVar, "looperExecutor");
        this.f5337b = oVar;
    }

    static {
        o.a aVar = o.f5360a;
    }

    public final boolean a(kotlin.d.a.a<m> aVar) {
        j.b(aVar, "task");
        o.a(this.f5337b, new b(aVar), 0, 2, null);
        return true;
    }

    public final boolean b(kotlin.d.a.a<m> aVar) {
        j.b(aVar, "task");
        throw new UnsupportedOperationException();
    }

    /* compiled from: dispatcher.kt */
    static final class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private final kotlin.d.a.a<m> f5338a;

        public b(kotlin.d.a.a<m> aVar) {
            j.b(aVar, "body");
            this.f5338a = aVar;
        }

        public final void run() {
            this.f5338a.invoke();
        }
    }
}
