package nl.komponents.kovenant.b;

import kotlin.d.b.j;

/* compiled from: properties-jvm.kt */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f5384a = new Object();

    public static final <V> V a(V v) {
        return v != null ? v : (Object) f5384a;
    }

    public static final <V> V b(Object obj) {
        if (j.a(obj, f5384a)) {
            return null;
        }
        return obj;
    }
}
