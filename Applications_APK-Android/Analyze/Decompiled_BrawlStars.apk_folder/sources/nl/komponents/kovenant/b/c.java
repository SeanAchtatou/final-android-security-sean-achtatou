package nl.komponents.kovenant.b;

import com.facebook.share.internal.ShareConstants;
import kotlin.d.a.a;
import kotlin.d.b.j;
import kotlin.h.h;

/* compiled from: properties-jvm.kt */
public final class c<T> {

    /* renamed from: a  reason: collision with root package name */
    private volatile Object f5387a;

    /* renamed from: b  reason: collision with root package name */
    private final a<T> f5388b;

    public c(a<? extends T> aVar) {
        j.b(aVar, ShareConstants.FEED_SOURCE_PARAM);
        this.f5388b = aVar;
    }

    public final T a(h<?> hVar) {
        j.b(hVar, "property");
        Object obj = this.f5387a;
        return obj != null ? a.b(obj) : this.f5388b.invoke();
    }

    public final void a(h<?> hVar, T t) {
        j.b(hVar, "property");
        this.f5387a = a.a(t);
    }

    public final boolean a() {
        return this.f5387a != null;
    }
}
