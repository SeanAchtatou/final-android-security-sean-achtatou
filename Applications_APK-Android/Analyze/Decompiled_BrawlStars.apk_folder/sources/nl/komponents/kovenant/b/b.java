package nl.komponents.kovenant.b;

import java.util.concurrent.atomic.AtomicInteger;
import kotlin.d.a.a;
import kotlin.d.b.j;
import kotlin.h.h;

/* compiled from: properties-jvm.kt */
public final class b<T> {

    /* renamed from: a  reason: collision with root package name */
    private volatile AtomicInteger f5385a = new AtomicInteger(0);

    /* renamed from: b  reason: collision with root package name */
    private volatile a<? extends T> f5386b;
    private volatile Object c;

    public b(a<? extends T> aVar) {
        j.b(aVar, "initializer");
        this.f5386b = aVar;
    }

    public final T a(h<?> hVar) {
        j.b(hVar, "property");
        while (this.c == null) {
            AtomicInteger atomicInteger = this.f5385a;
            if (atomicInteger != null && atomicInteger.incrementAndGet() == 1) {
                a<? extends T> aVar = this.f5386b;
                if (aVar == null) {
                    j.a();
                }
                this.c = a.a(aVar.invoke());
                this.f5386b = null;
                this.f5385a = null;
            }
            Thread.yield();
        }
        return a.b(this.c);
    }

    public final void a(h<?> hVar, T t) {
        j.b(hVar, "property");
        this.c = a.a(t);
    }

    public final boolean a() {
        return this.c != null;
    }
}
