package nl.komponents.kovenant;

import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.m;
import nl.komponents.kovenant.x;

/* compiled from: context-api.kt */
public final class bb {

    /* renamed from: a  reason: collision with root package name */
    public static final bb f5389a = null;

    /* renamed from: b  reason: collision with root package name */
    private static final x f5390b = null;

    static {
        new bb();
    }

    private bb() {
        f5389a = this;
        f5390b = new x();
    }

    public static ao a() {
        return f5390b.a();
    }

    public static ao a(b<? super bm, m> bVar) {
        by b2;
        by f;
        j.b(bVar, "body");
        x xVar = f5390b;
        j.b(bVar, "body");
        x.b bVar2 = new x.b(xVar.b());
        bVar.invoke(bVar2);
        do {
            b2 = xVar.b();
            f = b2.f();
            bm bmVar = f;
            j.b(bmVar, "context");
            bVar2.f5472b.a(bmVar.b());
            bVar2.c.a(bmVar.d());
            if (bVar2.f5471a.a()) {
                bmVar.a(bVar2.a());
            }
        } while (!xVar.f5467a.compareAndSet(b2, f));
        return xVar.a();
    }

    public static ao b(b<? super bm, m> bVar) {
        j.b(bVar, "body");
        j.b(bVar, "body");
        x.a aVar = new x.a();
        bVar.invoke(aVar);
        return aVar;
    }

    public static <V, E> ap<V, E> a(ao aoVar) {
        j.b(aoVar, "context");
        j.b(aoVar, "context");
        j.b(aoVar, "context");
        return new aq<>(aoVar);
    }

    public static <V, E> ap<V, E> a(ao aoVar, b<? super E, m> bVar) {
        j.b(aoVar, "context");
        j.b(bVar, "onCancelled");
        j.b(aoVar, "context");
        j.b(bVar, "onCancelled");
        j.b(aoVar, "context");
        j.b(bVar, "onCancelled");
        return new o<>(aoVar, bVar);
    }
}
