package nl.komponents.kovenant;

import kotlin.d.a.a;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.m;

/* compiled from: promises-jvm.kt */
public final class ar implements bw<V, E> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ aq f5376a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ aq f5377b;

    public final V a() throws Exception {
        return this.f5377b.a();
    }

    public final bw<V, E> a(b<? super V, m> bVar) {
        j.b(bVar, "callback");
        return this.f5377b.a((b) bVar);
    }

    public final bw<V, E> a(ax axVar, a<m> aVar) {
        j.b(axVar, "context");
        j.b(aVar, "callback");
        return this.f5377b.a(axVar, aVar);
    }

    public final bw<V, E> a(ax axVar, b<? super V, m> bVar) {
        j.b(axVar, "context");
        j.b(bVar, "callback");
        return this.f5377b.a(axVar, bVar);
    }

    public final E b() throws FailedException {
        return this.f5377b.b();
    }

    public final bw<V, E> b(b<? super E, m> bVar) {
        j.b(bVar, "callback");
        return this.f5377b.b((b) bVar);
    }

    public final bw<V, E> b(ax axVar, b<? super E, m> bVar) {
        j.b(axVar, "context");
        j.b(bVar, "callback");
        return this.f5377b.b(axVar, bVar);
    }

    public final boolean d() {
        return this.f5377b.d();
    }

    public final boolean e() {
        return this.f5377b.e();
    }

    public final boolean f() {
        return this.f5377b.f();
    }

    ar(aq aqVar) {
        this.f5376a = aqVar;
        this.f5377b = aqVar;
    }

    public final ao h() {
        return this.f5377b.f5335a;
    }
}
