package nl.komponents.kovenant;

import java.util.concurrent.atomic.AtomicInteger;
import kotlin.d.b.j;
import kotlin.m;

/* compiled from: queue-jvm.kt */
public abstract class k<V> implements cj<V> {

    /* renamed from: a  reason: collision with root package name */
    private final AtomicInteger f5452a = new AtomicInteger(0);

    /* renamed from: b  reason: collision with root package name */
    private final Object f5453b = new Object();

    public abstract V a();

    public abstract boolean a(V v);

    public final boolean b(V v) {
        j.b(v, "elem");
        boolean a2 = a(v);
        if (a2 && this.f5452a.get() > 0) {
            synchronized (this.f5453b) {
                this.f5453b.notifyAll();
                m mVar = m.f5330a;
            }
        }
        return a2;
    }

    public final V a(boolean z, long j) {
        V v;
        if (!z) {
            return a();
        }
        V a2 = a();
        if (a2 != null) {
            return a2;
        }
        this.f5452a.incrementAndGet();
        if (j > -1) {
            try {
                v = a(j);
            } catch (Throwable th) {
                this.f5452a.decrementAndGet();
                throw th;
            }
        } else {
            v = e();
        }
        this.f5452a.decrementAndGet();
        return v;
    }

    private final V e() {
        V a2;
        synchronized (this.f5453b) {
            while (true) {
                a2 = a();
                if (a2 == null) {
                    this.f5453b.wait();
                }
            }
        }
        return a2;
    }

    private final V a(long j) {
        V a2;
        long currentTimeMillis = System.currentTimeMillis() + j;
        synchronized (this.f5453b) {
            while (true) {
                a2 = a();
                if (a2 != null) {
                    break;
                } else if (System.currentTimeMillis() >= currentTimeMillis) {
                    break;
                } else {
                    this.f5453b.wait(j);
                }
            }
        }
        return a2;
    }

    public boolean b() {
        return d() == 0;
    }

    public boolean c() {
        return !b();
    }
}
