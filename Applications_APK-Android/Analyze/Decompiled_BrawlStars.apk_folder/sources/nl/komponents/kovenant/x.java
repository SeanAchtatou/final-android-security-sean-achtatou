package nl.komponents.kovenant;

import com.facebook.appevents.UserDataStore;
import com.facebook.share.internal.ShareConstants;
import java.util.concurrent.atomic.AtomicReference;
import kotlin.d.a.c;
import kotlin.d.b.j;
import kotlin.d.b.n;
import kotlin.d.b.t;
import kotlin.h.h;
import kotlin.m;
import nl.komponents.kovenant.bm;
import nl.komponents.kovenant.bn;

/* compiled from: context-jvm.kt */
public final class x {

    /* renamed from: a  reason: collision with root package name */
    final AtomicReference<ao> f5467a = new AtomicReference<>(new a());

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [nl.komponents.kovenant.ao, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final ao a() {
        ao aoVar = this.f5467a.get();
        j.a((Object) aoVar, "contextRef.get()");
        return aoVar;
    }

    /* access modifiers changed from: package-private */
    public final by b() {
        ao aoVar = this.f5467a.get();
        if (aoVar instanceof by) {
            return (by) aoVar;
        }
        throw new ConfigurationException("Current context [" + aoVar + "] does not implement ReconfigurableContext and therefor can't be reconfigured.");
    }

    /* compiled from: context-jvm.kt */
    static final class a implements by {
        private static final /* synthetic */ h[] g = {t.a(new n(t.a(a.class), "multipleCompletion", "getMultipleCompletion()Lkotlin/jvm/functions/Function2;"))};

        /* renamed from: a  reason: collision with root package name */
        final C0168a f5468a = new C0168a(ad.f5365a);

        /* renamed from: b  reason: collision with root package name */
        private final nl.komponents.kovenant.b.b<c<Object, Object, m>> f5469b = new nl.komponents.kovenant.b.b<>(ab.f5363a);
        private final nl.komponents.kovenant.b.b c = this.f5469b;
        /* access modifiers changed from: private */
        public final C0168a d = new C0168a(af.f5367a);
        private final bn e = new aa(this);
        private final bn f = new ah(this);

        public final c<Object, Object, m> a() {
            return (c) this.c.a(g[0]);
        }

        public final void a(c<Object, Object, m> cVar) {
            j.b(cVar, "<set-?>");
            this.c.a(g[0], cVar);
        }

        public final bn b() {
            return this.e;
        }

        public final bn d() {
            return this.f;
        }

        public final by f() {
            a aVar = new a();
            this.f5468a.a(aVar.e);
            this.d.a(aVar.f);
            if (this.f5469b.a()) {
                aVar.a(a());
            }
            return aVar;
        }

        /* renamed from: nl.komponents.kovenant.x$a$a  reason: collision with other inner class name */
        /* compiled from: context-jvm.kt */
        static final class C0168a implements bn {
            private static final /* synthetic */ h[] f;

            /* renamed from: a  reason: collision with root package name */
            private final nl.komponents.kovenant.b.b<av> f5470a;
            private final nl.komponents.kovenant.b.b<kotlin.d.a.b<Exception, m>> c = new nl.komponents.kovenant.b.b<>(y.f5474a);
            private final nl.komponents.kovenant.b.b d = this.f5470a;
            private final nl.komponents.kovenant.b.b e = this.c;

            static {
                Class<C0168a> cls = C0168a.class;
                f = new h[]{t.a(new n(t.a(cls), "dispatcher", "getDispatcher()Lnl/komponents/kovenant/Dispatcher;")), t.a(new n(t.a(cls), "errorHandler", "getErrorHandler()Lkotlin/jvm/functions/Function1;"))};
            }

            public final av a() {
                return (av) this.d.a(f[0]);
            }

            public final void a(kotlin.d.a.b<? super Exception, m> bVar) {
                j.b(bVar, "<set-?>");
                this.e.a(f[1], bVar);
            }

            public final void a(av avVar) {
                j.b(avVar, "<set-?>");
                this.d.a(f[0], avVar);
            }

            public final kotlin.d.a.b<Exception, m> b() {
                return (kotlin.d.a.b) this.e.a(f[1]);
            }

            public C0168a(kotlin.d.a.a<? extends av> aVar) {
                j.b(aVar, "factory");
                this.f5470a = new nl.komponents.kovenant.b.b<>(aVar);
            }

            public final void a(kotlin.d.a.a<m> aVar) {
                j.b(aVar, UserDataStore.FIRST_NAME);
                bn.a.a(this, aVar);
            }

            public final void a(bn bnVar) {
                j.b(bnVar, "context");
                if (this.f5470a.a()) {
                    bnVar.a(a());
                }
                if (this.c.a()) {
                    bnVar.a(b());
                }
            }
        }

        public final /* bridge */ /* synthetic */ ax c() {
            return this.e;
        }

        public final /* bridge */ /* synthetic */ ax e() {
            return this.f;
        }

        public final void a(kotlin.d.a.b<? super bn, m> bVar) {
            j.b(bVar, "body");
            j.b(bVar, "body");
            bm.a.a(this, bVar);
        }

        public final void b(kotlin.d.a.b<? super bn, m> bVar) {
            j.b(bVar, "body");
            j.b(bVar, "body");
            bm.a.b(this, bVar);
        }
    }

    /* compiled from: context-jvm.kt */
    static final class b implements bm {
        private static final /* synthetic */ h[] h = {t.a(new n(t.a(b.class), "multipleCompletion", "getMultipleCompletion()Lkotlin/jvm/functions/Function2;"))};

        /* renamed from: a  reason: collision with root package name */
        final nl.komponents.kovenant.b.c<c<Object, Object, m>> f5471a = new nl.komponents.kovenant.b.c<>(new al(this));

        /* renamed from: b  reason: collision with root package name */
        final a f5472b = new a(this.g.c());
        final a c = new a(this.g.e());
        private final nl.komponents.kovenant.b.c d = this.f5471a;
        private final bn e = new ak(this);
        private final bn f = new am(this);
        /* access modifiers changed from: private */
        public final ao g;

        public final c<Object, Object, m> a() {
            return (c) this.d.a(h[0]);
        }

        public final void a(c<Object, Object, m> cVar) {
            j.b(cVar, "<set-?>");
            this.d.a(h[0], cVar);
        }

        public b(ao aoVar) {
            j.b(aoVar, "currentContext");
            this.g = aoVar;
        }

        public final void a(kotlin.d.a.b<? super bn, m> bVar) {
            j.b(bVar, "body");
            bm.a.a(this, bVar);
        }

        public final void b(kotlin.d.a.b<? super bn, m> bVar) {
            j.b(bVar, "body");
            bm.a.b(this, bVar);
        }

        public final bn b() {
            return this.e;
        }

        public final bn d() {
            return this.f;
        }

        /* compiled from: context-jvm.kt */
        static final class a implements bn {
            private static final /* synthetic */ h[] g;

            /* renamed from: a  reason: collision with root package name */
            private final nl.komponents.kovenant.b.c<av> f5473a = new nl.komponents.kovenant.b.c<>(new ai(this));
            private final nl.komponents.kovenant.b.c c = this.f5473a;
            private final nl.komponents.kovenant.b.c<kotlin.d.a.b<Exception, m>> d = new nl.komponents.kovenant.b.c<>(new aj(this));
            private final nl.komponents.kovenant.b.c e = this.d;
            /* access modifiers changed from: private */
            public final ax f;

            static {
                Class<a> cls = a.class;
                g = new h[]{t.a(new n(t.a(cls), "dispatcher", "getDispatcher()Lnl/komponents/kovenant/Dispatcher;")), t.a(new n(t.a(cls), "errorHandler", "getErrorHandler()Lkotlin/jvm/functions/Function1;"))};
            }

            public final av a() {
                return (av) this.c.a(g[0]);
            }

            public final void a(kotlin.d.a.b<? super Exception, m> bVar) {
                j.b(bVar, "<set-?>");
                this.e.a(g[1], bVar);
            }

            public final void a(av avVar) {
                j.b(avVar, "<set-?>");
                this.c.a(g[0], avVar);
            }

            public final kotlin.d.a.b<Exception, m> b() {
                return (kotlin.d.a.b) this.e.a(g[1]);
            }

            public a(ax axVar) {
                j.b(axVar, ShareConstants.FEED_SOURCE_PARAM);
                this.f = axVar;
            }

            public final void a(kotlin.d.a.a<m> aVar) {
                j.b(aVar, UserDataStore.FIRST_NAME);
                bn.a.a(this, aVar);
            }

            public final void a(bn bnVar) {
                j.b(bnVar, "context");
                if (this.f5473a.a()) {
                    bnVar.a(a());
                }
                if (this.d.a()) {
                    bnVar.a(b());
                }
            }
        }

        public final /* bridge */ /* synthetic */ ax c() {
            return this.e;
        }

        public final /* bridge */ /* synthetic */ ax e() {
            return this.f;
        }
    }
}
