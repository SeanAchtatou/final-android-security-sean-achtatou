package nl.komponents.kovenant;

import kotlin.d.b.j;

/* compiled from: dispatcher-jvm.kt */
final class cl<V> implements bt<V> {

    /* renamed from: a  reason: collision with root package name */
    private final int f5437a;

    public cl(int i) {
        this.f5437a = i;
    }

    public /* synthetic */ cl(int i, int i2) {
        this(1000);
    }

    public final br<V> a(bu<V> buVar) {
        j.b(buVar, "pollable");
        return new ck<>(buVar, this.f5437a);
    }
}
