package nl.komponents.kovenant;

import kotlin.d.b.j;

/* compiled from: promises-jvm.kt */
final class aq<V, E> extends a<V, E> implements ap<V, E> {
    private final bw<V, E> c = new ar(this);

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public aq(ao aoVar) {
        super(aoVar);
        j.b(aoVar, "context");
    }

    public final void e(V v) {
        if (c(v)) {
            a((Object) v);
        } else {
            g(v);
        }
    }

    public final void f(E e) {
        if (d(e)) {
            b((Object) e);
        } else {
            g(e);
        }
    }

    private final void g(Object obj) {
        while (!c()) {
            Thread.yield();
        }
        this.f5335a.a().invoke(g(), obj);
    }

    public final bw<V, E> i() {
        return this.c;
    }
}
