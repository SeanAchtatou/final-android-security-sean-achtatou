package nl.komponents.kovenant;

import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.m;

/* compiled from: promises-jvm.kt */
final class o<V, E> extends a<V, E> implements ap<V, E>, q<V, E> {
    public static final a c = new a((byte) 0);
    private static final int h = 0;
    private static final int i = 1;
    private static final int j = 2;
    private static final int k = 3;
    private volatile b<? super E, m> e;
    private volatile int f = h;
    private final bw<V, E> g = new p(this);

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public o(ao aoVar, b<? super E, m> bVar) {
        super(aoVar);
        j.b(aoVar, "context");
        j.b(bVar, "onCancelled");
        this.e = bVar;
        a aVar = c;
    }

    /* compiled from: promises-jvm.kt */
    public static final class a {
        private a() {
        }

        public /* synthetic */ a(byte b2) {
            this();
        }
    }

    public final void e(V v) {
        do {
            int i2 = this.f;
            a aVar = c;
            if (i2 != h) {
                int i3 = this.f;
                a aVar2 = c;
                if (i3 != i) {
                    i(v);
                    return;
                }
                return;
            }
        } while (!c(v));
        a aVar3 = c;
        this.f = k;
        a((Object) v);
        this.e = null;
    }

    public final void f(E e2) {
        do {
            int i2 = this.f;
            a aVar = c;
            if (i2 != h) {
                int i3 = this.f;
                a aVar2 = c;
                if (i3 != i) {
                    i(e2);
                    return;
                }
                return;
            }
        } while (!d(e2));
        a aVar3 = c;
        this.f = j;
        b((Object) e2);
        this.e = null;
    }

    public final boolean g(E e2) {
        do {
            int i2 = this.f;
            a aVar = c;
            if (i2 != h) {
                return false;
            }
        } while (!d(e2));
        a aVar2 = c;
        this.f = i;
        Exception h2 = h(e2);
        b((Object) e2);
        this.e = null;
        if (j.a(h2, (Object) null)) {
            return true;
        }
        throw new KovenantException("Promise cancelled but cancel might not have succeeded due to exception in callback", h2);
    }

    private final Exception h(E e2) {
        try {
            b<? super E, m> bVar = this.e;
            if (bVar == null) {
                return null;
            }
            bVar.invoke(e2);
            return null;
        } catch (Exception e3) {
            return e3;
        }
    }

    private final void i(Object obj) {
        while (!c()) {
            Thread.yield();
        }
        this.f5335a.a().invoke(g(), obj);
    }

    public final bw<V, E> i() {
        return this.g;
    }
}
