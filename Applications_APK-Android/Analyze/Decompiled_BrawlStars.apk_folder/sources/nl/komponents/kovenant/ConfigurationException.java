package nl.komponents.kovenant;

import com.facebook.share.internal.ShareConstants;
import kotlin.d.b.j;

/* compiled from: exceptions-api.kt */
public class ConfigurationException extends KovenantException {
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ConfigurationException(String str) {
        super(str, null, 2);
        j.b(str, ShareConstants.WEB_DIALOG_PARAM_MESSAGE);
    }
}
