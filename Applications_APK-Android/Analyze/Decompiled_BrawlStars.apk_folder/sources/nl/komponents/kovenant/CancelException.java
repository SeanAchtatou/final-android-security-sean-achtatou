package nl.komponents.kovenant;

/* compiled from: exceptions-api.kt */
public class CancelException extends KovenantException {
    public CancelException() {
        super(null, null, 3);
    }
}
