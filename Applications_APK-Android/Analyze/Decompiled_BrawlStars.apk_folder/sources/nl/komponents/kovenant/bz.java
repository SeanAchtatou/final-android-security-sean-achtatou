package nl.komponents.kovenant;

import kotlin.d.b.j;

/* compiled from: promises-jvm.kt */
abstract class bz<V, E> extends a<V, E> {
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bz(ao aoVar) {
        super(aoVar);
        j.b(aoVar, "context");
    }

    /* access modifiers changed from: protected */
    public final void e(V v) {
        if (c(v)) {
            a((Object) v);
        }
    }

    /* access modifiers changed from: protected */
    public final void f(E e) {
        if (d(e)) {
            b((Object) e);
        }
    }
}
