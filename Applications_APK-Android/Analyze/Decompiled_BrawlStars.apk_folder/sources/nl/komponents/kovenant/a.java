package nl.komponents.kovenant;

import com.facebook.appevents.UserDataStore;
import com.facebook.internal.ServerProtocol;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.TypeCastException;
import kotlin.d.b.j;
import kotlin.d.b.t;
import kotlin.m;

/* compiled from: promises-jvm.kt */
abstract class a<V, E> implements bw<V, E> {

    /* renamed from: b  reason: collision with root package name */
    public static final d f5334b = new d((byte) 0);
    private static final AtomicReferenceFieldUpdater<a<?, ?>, h> e;
    private static final AtomicReferenceFieldUpdater<a<?, ?>, AtomicInteger> f;
    private static final AtomicReferenceFieldUpdater<a<?, ?>, c<?, ?>> g;
    private volatile c<V, E> _head;
    private volatile AtomicInteger _waitingThreads;

    /* renamed from: a  reason: collision with root package name */
    final ao f5335a;
    /* access modifiers changed from: private */
    public volatile Object c;
    private volatile h state = h.PENDING;

    /* compiled from: promises-jvm.kt */
    interface b<V, E> {
        void a(V v);

        void b(E e);
    }

    /* compiled from: promises-jvm.kt */
    static final class e<V, E> extends c<V, E> {
        public final void a(V v) {
        }

        public final void b(E e) {
        }
    }

    /* compiled from: promises-jvm.kt */
    enum g {
        CHAINED,
        POPPING,
        APPENDING
    }

    /* compiled from: promises-jvm.kt */
    enum h {
        PENDING,
        MUTATING,
        SUCCESS,
        FAIL
    }

    public a(ao aoVar) {
        j.b(aoVar, "context");
        this.f5335a = aoVar;
    }

    public final bw<V, E> a(kotlin.d.a.b<? super V, m> bVar) {
        j.b(bVar, "callback");
        j.b(bVar, "callback");
        return a(h().c(), bVar);
    }

    public final bw<V, E> b(kotlin.d.a.b<? super E, m> bVar) {
        j.b(bVar, "callback");
        j.b(bVar, "callback");
        return b(h().c(), bVar);
    }

    public final ao h() {
        return this.f5335a;
    }

    /* compiled from: promises-jvm.kt */
    public static final class d {
        private d() {
        }

        public /* synthetic */ d(byte b2) {
            this();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.concurrent.atomic.AtomicReferenceFieldUpdater<nl.komponents.kovenant.a<?, ?>, nl.komponents.kovenant.a$h>, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.concurrent.atomic.AtomicReferenceFieldUpdater<nl.komponents.kovenant.a<?, ?>, java.util.concurrent.atomic.AtomicInteger>, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.concurrent.atomic.AtomicReferenceFieldUpdater<nl.komponents.kovenant.a<?, ?>, nl.komponents.kovenant.a$c<?, ?>>, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    static {
        Class<a> cls = a.class;
        if (nl.komponents.kovenant.d.a.a()) {
            e = new nl.komponents.kovenant.d.b(t.a(cls), ServerProtocol.DIALOG_PARAM_STATE);
            f = new nl.komponents.kovenant.d.b(t.a(cls), "_waitingThreads");
            g = new nl.komponents.kovenant.d.b(t.a(cls), "_head");
            return;
        }
        AtomicReferenceFieldUpdater<a<?, ?>, h> newUpdater = AtomicReferenceFieldUpdater.newUpdater(cls, h.class, ServerProtocol.DIALOG_PARAM_STATE);
        j.a((Object) newUpdater, "AtomicReferenceFieldUpda…ate::class.java, \"state\")");
        e = newUpdater;
        AtomicReferenceFieldUpdater<a<?, ?>, AtomicInteger> newUpdater2 = AtomicReferenceFieldUpdater.newUpdater(cls, AtomicInteger.class, "_waitingThreads");
        j.a((Object) newUpdater2, "AtomicReferenceFieldUpda….java, \"_waitingThreads\")");
        f = newUpdater2;
        AtomicReferenceFieldUpdater<a<?, ?>, c<?, ?>> newUpdater3 = AtomicReferenceFieldUpdater.newUpdater(cls, c.class, "_head");
        j.a((Object) newUpdater3, "AtomicReferenceFieldUpda…ode::class.java, \"_head\")");
        g = newUpdater3;
    }

    private final AtomicInteger i() {
        while (true) {
            AtomicInteger atomicInteger = this._waitingThreads;
            if (atomicInteger != null) {
                return atomicInteger;
            }
            d dVar = f5334b;
            f.compareAndSet(this, null, new AtomicInteger(0));
        }
    }

    private final Object j() {
        AtomicInteger i2 = i();
        if (i2 != null) {
            return i2;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.Object");
    }

    public final bw<V, E> a(ax axVar, kotlin.d.a.b<? super V, m> bVar) {
        j.b(axVar, "context");
        j.b(bVar, "callback");
        if (m()) {
            return this;
        }
        if (!l() || !p()) {
            a((c) new i(axVar, bVar));
            if (l()) {
                a(this.c);
            }
            return this;
        }
        axVar.a(new h(this, bVar));
        return this;
    }

    public final bw<V, E> b(ax axVar, kotlin.d.a.b<? super E, m> bVar) {
        j.b(axVar, "context");
        j.b(bVar, "callback");
        if (l()) {
            return this;
        }
        if (!m() || !p()) {
            a((c) new f(axVar, bVar));
            if (m()) {
                b(this.c);
            }
            return this;
        }
        axVar.a(new g(this, bVar));
        return this;
    }

    public final bw<V, E> a(ax axVar, kotlin.d.a.a<m> aVar) {
        j.b(axVar, "context");
        j.b(aVar, "callback");
        if ((l() || m()) && p()) {
            axVar.a(new f(aVar));
            return this;
        }
        a((c) new C0164a(axVar, aVar));
        if (l()) {
            a(this.c);
        } else if (m()) {
            b(this.c);
        }
        return this;
    }

    public final V a() {
        Exception exc;
        if (!c()) {
            i().incrementAndGet();
            try {
                synchronized (j()) {
                    while (!c()) {
                        try {
                            j().wait();
                        } catch (InterruptedException e2) {
                            throw new FailedException(e2);
                        }
                    }
                    m mVar = m.f5330a;
                }
            } finally {
                i().decrementAndGet();
            }
        }
        if (l()) {
            return (Object) this.c;
        }
        Object obj = this.c;
        if (obj instanceof Exception) {
            exc = (Exception) obj;
        } else {
            exc = new FailedException(obj);
        }
        throw exc;
    }

    public final E b() {
        if (!c()) {
            i().incrementAndGet();
            try {
                synchronized (j()) {
                    while (!c()) {
                        try {
                            j().wait();
                        } catch (InterruptedException e2) {
                            throw new FailedException(e2);
                        }
                    }
                    m mVar = m.f5330a;
                }
            } finally {
                i().decrementAndGet();
            }
        }
        if (m()) {
            return (Object) this.c;
        }
        throw new FailedException(this.c);
    }

    public final boolean c(V v) {
        if (!j.a(this.state, h.PENDING)) {
            return false;
        }
        d dVar = f5334b;
        if (!e.compareAndSet(this, h.PENDING, h.MUTATING)) {
            return false;
        }
        this.c = v;
        this.state = h.SUCCESS;
        k();
        return true;
    }

    public final boolean d(E e2) {
        if (!j.a(this.state, h.PENDING)) {
            return false;
        }
        d dVar = f5334b;
        if (!e.compareAndSet(this, h.PENDING, h.MUTATING)) {
            return false;
        }
        this.c = e2;
        this.state = h.FAIL;
        k();
        return true;
    }

    private final void k() {
        AtomicInteger atomicInteger = this._waitingThreads;
        if (atomicInteger != null && atomicInteger.get() > 0) {
            synchronized (j()) {
                j().notifyAll();
                m mVar = m.f5330a;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final boolean c() {
        h hVar = this.state;
        return j.a(hVar, h.SUCCESS) || j.a(hVar, h.FAIL);
    }

    private boolean l() {
        return j.a(this.state, h.SUCCESS);
    }

    private boolean m() {
        return j.a(this.state, h.FAIL);
    }

    public final boolean d() {
        return c();
    }

    public final boolean e() {
        return m();
    }

    public final boolean f() {
        return l();
    }

    /* access modifiers changed from: protected */
    public final Object g() {
        Object obj = this.c;
        if (obj != null) {
            return obj;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Any");
    }

    private final void a(c<V, E> cVar) {
        while (true) {
            c o = o();
            if (o.a(g.CHAINED, g.APPENDING)) {
                if (o.f5341a == null) {
                    o.f5341a = cVar;
                    o.a(g.CHAINED);
                    return;
                }
                o.a(g.CHAINED);
            }
        }
    }

    private final c<V, E> n() {
        while (true) {
            c<V, E> cVar = this._head;
            if (cVar != null) {
                return cVar;
            }
            d dVar = f5334b;
            g.compareAndSet(this, null, new e());
        }
    }

    private final c<V, E> o() {
        c<V, E> n = n();
        while (true) {
            c<V, E> cVar = n.f5341a;
            if (cVar == null) {
                return n;
            }
            n = cVar;
        }
    }

    private final boolean p() {
        c<V, E> cVar = this._head;
        return cVar == null || cVar.f5341a == null;
    }

    /* compiled from: promises-jvm.kt */
    static abstract class c<V, E> implements b<V, E> {

        /* renamed from: b  reason: collision with root package name */
        public static final C0166a f5340b = new C0166a((byte) 0);
        private static final AtomicReferenceFieldUpdater<c<?, ?>, g> c;

        /* renamed from: a  reason: collision with root package name */
        volatile c<V, E> f5341a;
        private volatile g nodeState = g.CHAINED;

        /* renamed from: nl.komponents.kovenant.a$c$a  reason: collision with other inner class name */
        /* compiled from: promises-jvm.kt */
        public static final class C0166a {
            private C0166a() {
            }

            public /* synthetic */ C0166a(byte b2) {
                this();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [java.util.concurrent.atomic.AtomicReferenceFieldUpdater<nl.komponents.kovenant.a$c<?, ?>, nl.komponents.kovenant.a$g>, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        static {
            AtomicReferenceFieldUpdater<c<?, ?>, g> atomicReferenceFieldUpdater;
            Class<c> cls = c.class;
            if (nl.komponents.kovenant.d.a.a()) {
                atomicReferenceFieldUpdater = new nl.komponents.kovenant.d.b<>(t.a(cls), "nodeState");
            } else {
                atomicReferenceFieldUpdater = AtomicReferenceFieldUpdater.newUpdater(cls, g.class, "nodeState");
                j.a((Object) atomicReferenceFieldUpdater, "AtomicReferenceFieldUpda…:class.java, \"nodeState\")");
            }
            c = atomicReferenceFieldUpdater;
        }

        public final boolean a(g gVar, g gVar2) {
            j.b(gVar, "expected");
            j.b(gVar2, "update");
            C0166a aVar = f5340b;
            return c.compareAndSet(this, gVar, gVar2);
        }

        public final void a(g gVar) {
            j.b(gVar, "<set-?>");
            this.nodeState = gVar;
        }
    }

    /* renamed from: nl.komponents.kovenant.a$a  reason: collision with other inner class name */
    /* compiled from: promises-jvm.kt */
    static final class C0164a<V, E> extends c<V, E> {
        private final ax c;
        /* access modifiers changed from: private */
        public final kotlin.d.a.a<m> d;

        public C0164a(ax axVar, kotlin.d.a.a<m> aVar) {
            j.b(axVar, "context");
            j.b(aVar, UserDataStore.FIRST_NAME);
            this.c = axVar;
            this.d = aVar;
        }

        public final void a(V v) {
            this.c.a(new c(this));
        }

        public final void b(E e) {
            this.c.a(new b(this));
        }
    }

    /* compiled from: promises-jvm.kt */
    static final class i<V, E> extends c<V, E> {
        private final ax c;
        /* access modifiers changed from: private */
        public final kotlin.d.a.b<V, m> d;

        public final void b(E e) {
        }

        public i(ax axVar, kotlin.d.a.b<? super V, m> bVar) {
            j.b(axVar, "context");
            j.b(bVar, UserDataStore.FIRST_NAME);
            this.c = axVar;
            this.d = bVar;
        }

        public final void a(V v) {
            this.c.a(new e(this, v));
        }
    }

    /* compiled from: promises-jvm.kt */
    static final class f<V, E> extends c<V, E> {
        private final ax c;
        /* access modifiers changed from: private */
        public final kotlin.d.a.b<E, m> d;

        public final void a(V v) {
        }

        public f(ax axVar, kotlin.d.a.b<? super E, m> bVar) {
            j.b(axVar, "context");
            j.b(bVar, UserDataStore.FIRST_NAME);
            this.c = axVar;
            this.d = bVar;
        }

        public final void b(E e) {
            this.c.a(new d(this, e));
        }
    }

    public final void a(V v) {
        boolean z;
        c<V, E> cVar = this._head;
        if (cVar != null) {
            do {
                c<V, E> cVar2 = cVar.f5341a;
                if (cVar2 != null && cVar.a(g.CHAINED, g.POPPING)) {
                    if (cVar2.a(g.CHAINED, g.POPPING)) {
                        cVar.f5341a = cVar2.f5341a;
                        cVar.a(g.CHAINED);
                        cVar2.f5341a = null;
                        cVar2.a((Object) v);
                    }
                    cVar.a(g.CHAINED);
                }
                if (cVar2 != null) {
                    z = true;
                    continue;
                } else {
                    z = false;
                    continue;
                }
            } while (z);
        }
    }

    public final void b(E e2) {
        boolean z;
        c<V, E> cVar = this._head;
        if (cVar != null) {
            do {
                c<V, E> cVar2 = cVar.f5341a;
                if (cVar2 != null && cVar.a(g.CHAINED, g.POPPING)) {
                    if (cVar2.a(g.CHAINED, g.POPPING)) {
                        cVar.f5341a = cVar2.f5341a;
                        cVar.a(g.CHAINED);
                        cVar2.f5341a = null;
                        cVar2.b(e2);
                    }
                    cVar.a(g.CHAINED);
                }
                if (cVar2 != null) {
                    z = true;
                    continue;
                } else {
                    z = false;
                    continue;
                }
            } while (z);
        }
    }
}
