package nl.komponents.kovenant;

import java.util.Arrays;
import java.util.List;
import kotlin.a.ab;
import kotlin.a.l;
import kotlin.d.b.j;
import kotlin.i.c;
import kotlin.i.h;
import nl.komponents.kovenant.bw;

/* compiled from: bulk-api.kt */
public final class bh {
    private static <V> bw<List<V>, Exception> a(bw<? extends V, ? extends Exception>[] bwVarArr, ao aoVar, boolean z) {
        h hVar;
        j.b(bwVarArr, "promises");
        j.b(aoVar, "context");
        if (((Object[]) bwVarArr).length != 0) {
            bw[] bwVarArr2 = (bw[]) Arrays.copyOf(bwVarArr, bwVarArr.length);
            j.b(bwVarArr2, "promises");
            j.b(aoVar, "context");
            Object[] objArr = (Object[]) bwVarArr2;
            j.b(objArr, "$this$asSequence");
            if (objArr.length == 0) {
                hVar = c.f5269a;
            } else {
                hVar = new l(objArr);
            }
            return l.a(hVar, objArr.length, aoVar, z);
        }
        bw.a aVar = bw.d;
        return bw.a.a(ab.f5210a, aoVar);
    }
}
