package nl.komponents.kovenant;

import kotlin.TypeCastException;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

/* compiled from: context-api.kt */
final class au extends k implements b<Exception, m> {

    /* renamed from: a  reason: collision with root package name */
    public static final au f5381a = new au();

    au() {
        super(1);
    }

    public final /* synthetic */ Object invoke(Object obj) {
        Exception exc = (Exception) obj;
        j.b(exc, "e");
        Throwable th = exc;
        if (th != null) {
            th.printStackTrace();
            return m.f5330a;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.Throwable");
    }
}
