package nl.komponents.kovenant;

import kotlin.d.a.c;
import kotlin.d.b.k;
import kotlin.m;

/* compiled from: context-jvm.kt */
final class ac extends k implements c<Object, Object, m> {

    /* renamed from: a  reason: collision with root package name */
    public static final ac f5364a = new ac();

    ac() {
        super(2);
    }

    public final /* synthetic */ Object invoke(Object obj, Object obj2) {
        throw new IllegalStateException("Value[" + obj + "] is set, can't override with new value[" + obj2 + "]");
    }
}
