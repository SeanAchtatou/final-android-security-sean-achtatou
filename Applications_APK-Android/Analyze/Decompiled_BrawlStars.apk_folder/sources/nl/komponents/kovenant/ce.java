package nl.komponents.kovenant;

import kotlin.d.a.a;
import kotlin.d.b.k;
import kotlin.m;

/* compiled from: promises-jvm.kt */
final class ce extends k implements a<m> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ cd f5428a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ a f5429b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ce(cd cdVar, a aVar) {
        super(0);
        this.f5428a = cdVar;
        this.f5429b = aVar;
    }

    public final /* synthetic */ Object invoke() {
        try {
            this.f5428a.e(this.f5429b.invoke());
        } catch (Exception e) {
            this.f5428a.f(e);
        } catch (Throwable th) {
            this.f5428a.c = null;
            throw th;
        }
        this.f5428a.c = null;
        return m.f5330a;
    }
}
