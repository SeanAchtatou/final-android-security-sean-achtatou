package nl.komponents.kovenant.c;

import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.m;
import nl.komponents.kovenant.c.a;

/* compiled from: context-api.kt */
public final class l {

    /* renamed from: a  reason: collision with root package name */
    public static final l f5418a = null;

    /* renamed from: b  reason: collision with root package name */
    private static final a f5419b = null;

    static {
        new l();
    }

    private l() {
        f5418a = this;
        f5419b = new a();
    }

    public static q a() {
        return f5419b.a();
    }

    public static q a(b<? super o, m> bVar) {
        p b2;
        p c;
        j.b(bVar, "body");
        a aVar = f5419b;
        j.b(bVar, "body");
        a.c cVar = new a.c(aVar.b());
        bVar.invoke(cVar);
        do {
            b2 = aVar.b();
            c = b2.c();
            o oVar = c;
            j.b(oVar, "uiContext");
            if (cVar.f5407b.a()) {
                oVar.a(cVar.b());
            }
            if (cVar.f5406a.a()) {
                oVar.a(cVar.a());
            }
        } while (!aVar.f5403a.compareAndSet(b2, c));
        return aVar.a();
    }
}
