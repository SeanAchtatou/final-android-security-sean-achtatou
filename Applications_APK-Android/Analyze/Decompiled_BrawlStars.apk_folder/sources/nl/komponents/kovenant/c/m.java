package nl.komponents.kovenant.c;

import kotlin.d.a.a;
import kotlin.d.a.b;
import kotlin.d.b.j;
import nl.komponents.kovenant.av;
import nl.komponents.kovenant.ax;
import nl.komponents.kovenant.bv;
import nl.komponents.kovenant.bw;

/* compiled from: callbacks-api.kt */
public final class m {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: nl.komponents.kovenant.c.m.a(nl.komponents.kovenant.bw, nl.komponents.kovenant.c.q, boolean, kotlin.d.a.b, int, java.lang.Object):nl.komponents.kovenant.bw
     arg types: [nl.komponents.kovenant.bw, ?[OBJECT, ARRAY], int, kotlin.d.a.b, int, ?[OBJECT, ARRAY]]
     candidates:
      nl.komponents.kovenant.c.m.a(nl.komponents.kovenant.bw, nl.komponents.kovenant.c.q, boolean, kotlin.d.a.a, int, java.lang.Object):nl.komponents.kovenant.bw
      nl.komponents.kovenant.c.m.a(nl.komponents.kovenant.bw, nl.komponents.kovenant.c.q, boolean, kotlin.d.a.b, int, java.lang.Object):nl.komponents.kovenant.bw */
    public static final <V, E> bw<V, E> a(bw bwVar, b bVar) {
        j.b(bwVar, "$receiver");
        j.b(bVar, "body");
        return l.f5418a;
    }

    private static <V, E> bw<V, E> a(bw<? extends V, ? extends E> bwVar, q qVar, boolean z, b<? super V, kotlin.m> bVar) {
        j.b(bwVar, "$receiver");
        j.b(qVar, "uiContext");
        j.b(bVar, "body");
        ax a2 = n.a(qVar, bwVar.h());
        if (!bwVar.d() || !a(z, a2.a())) {
            bwVar.a(a2, bVar);
        } else if (bwVar.f()) {
            try {
                bVar.invoke(bwVar.a());
            } catch (Exception e) {
                a2.b().invoke(e);
            }
        }
        return bwVar;
    }

    public static final <V, E> bw<V, E> b(bw<? extends V, ? extends E> bwVar, b<? super E, kotlin.m> bVar) {
        j.b(bwVar, "$receiver");
        j.b(bVar, "body");
        return l.f5418a;
    }

    private static <V, E> bw<V, E> b(bw<? extends V, ? extends E> bwVar, q qVar, boolean z, b<? super E, kotlin.m> bVar) {
        j.b(bwVar, "$receiver");
        j.b(qVar, "uiContext");
        j.b(bVar, "body");
        ax a2 = n.a(qVar, bwVar.h());
        if (!bwVar.d() || !a(z, a2.a())) {
            bwVar.b(a2, bVar);
        } else if (bwVar.e()) {
            try {
                bVar.invoke(bwVar.b());
            } catch (Exception e) {
                a2.b().invoke(e);
            }
        }
        return bwVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: nl.komponents.kovenant.c.m.a(nl.komponents.kovenant.bw, nl.komponents.kovenant.c.q, boolean, kotlin.d.a.a, int, java.lang.Object):nl.komponents.kovenant.bw
     arg types: [nl.komponents.kovenant.bw, ?[OBJECT, ARRAY], int, kotlin.d.a.a<kotlin.m>, int, ?[OBJECT, ARRAY]]
     candidates:
      nl.komponents.kovenant.c.m.a(nl.komponents.kovenant.bw, nl.komponents.kovenant.c.q, boolean, kotlin.d.a.b, int, java.lang.Object):nl.komponents.kovenant.bw
      nl.komponents.kovenant.c.m.a(nl.komponents.kovenant.bw, nl.komponents.kovenant.c.q, boolean, kotlin.d.a.a, int, java.lang.Object):nl.komponents.kovenant.bw */
    public static final <V, E> bw<V, E> a(bw bwVar, a<kotlin.m> aVar) {
        j.b(bwVar, "$receiver");
        j.b(aVar, "body");
        return l.f5418a;
    }

    private static <V, E> bw<V, E> a(bw<? extends V, ? extends E> bwVar, q qVar, boolean z, a<kotlin.m> aVar) {
        j.b(bwVar, "$receiver");
        j.b(qVar, "uiContext");
        j.b(aVar, "body");
        ax a2 = n.a(qVar, bwVar.h());
        if (!bwVar.d() || !a(z, a2.a())) {
            bwVar.a(a2, aVar);
        } else {
            try {
                aVar.invoke();
            } catch (Exception e) {
                a2.b().invoke(e);
            }
        }
        return bwVar;
    }

    private static final boolean a(boolean z, av avVar) {
        return !z && (avVar instanceof bv) && ((bv) avVar).a();
    }
}
