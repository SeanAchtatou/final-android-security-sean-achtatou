package nl.komponents.kovenant.c;

import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.d.b.k;
import nl.komponents.kovenant.ao;
import nl.komponents.kovenant.av;
import nl.komponents.kovenant.ax;

/* compiled from: context-jvm.kt */
final class i extends k implements b<ao, ax> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ h f5414a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ av f5415b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    i(h hVar, av avVar) {
        super(1);
        this.f5414a = hVar;
        this.f5415b = avVar;
    }

    public final /* synthetic */ Object invoke(Object obj) {
        ao aoVar = (ao) obj;
        j.b(aoVar, "context");
        return (ax) this.f5414a.f5413a.invoke(this.f5415b, aoVar);
    }
}
