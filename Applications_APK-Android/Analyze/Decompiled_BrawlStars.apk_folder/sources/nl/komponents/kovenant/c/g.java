package nl.komponents.kovenant.c;

import kotlin.d.a.c;
import kotlin.d.b.j;
import kotlin.d.b.k;
import nl.komponents.kovenant.ao;
import nl.komponents.kovenant.av;
import nl.komponents.kovenant.ax;

/* compiled from: context-jvm.kt */
final class g extends k implements c<av, ao, ax> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ r f5412a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    g(r rVar) {
        super(2);
        this.f5412a = rVar;
    }

    public final /* synthetic */ Object invoke(Object obj, Object obj2) {
        av avVar = (av) obj;
        ao aoVar = (ao) obj2;
        j.b(avVar, "dispatcher");
        j.b(aoVar, "context");
        return (ax) ((r) this.f5412a.a(avVar)).a(aoVar);
    }
}
