package nl.komponents.kovenant.c;

import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicReference;
import kotlin.d.a.b;
import kotlin.d.b.j;

/* compiled from: cache-jvm.kt */
public final class r<K, V> {

    /* renamed from: a  reason: collision with root package name */
    private final AtomicReference<a<K, V>> f5420a = new AtomicReference<>(null);

    /* renamed from: b  reason: collision with root package name */
    private final b<K, V> f5421b;

    public r(b<? super K, ? extends V> bVar) {
        j.b(bVar, "factory");
        this.f5421b = bVar;
    }

    /* compiled from: cache-jvm.kt */
    static final class a<K, V> {

        /* renamed from: a  reason: collision with root package name */
        final AtomicReference<a<K, V>> f5422a = new AtomicReference<>(null);

        /* renamed from: b  reason: collision with root package name */
        final WeakReference<K> f5423b;
        final V c;

        public a(K k, V v) {
            j.b(k, "key");
            j.b(v, "value");
            this.c = v;
            this.f5423b = new WeakReference<>(k);
        }
    }

    private final void a(K k, V v) {
        a(new a(k, v));
    }

    private final void a(a aVar) {
        do {
            a aVar2 = this.f5420a.get();
            if (aVar2 != null) {
                do {
                } while (!b(aVar2).f5422a.compareAndSet(null, aVar));
                return;
            }
        } while (!this.f5420a.compareAndSet(null, aVar));
    }

    public final V a(Object obj) {
        j.b(obj, "key");
        a aVar = this.f5420a.get();
        if (aVar != null) {
            while (true) {
                K k = aVar.f5423b.get();
                if (k != null) {
                    V v = aVar.c;
                    if (!j.a(k, obj)) {
                        aVar = aVar.f5422a.get();
                        if (aVar == null) {
                            break;
                        }
                    } else {
                        return v;
                    }
                } else {
                    this.f5420a.set(null);
                    break;
                }
            }
        }
        V invoke = this.f5421b.invoke(obj);
        a(obj, invoke);
        return invoke;
    }

    private static a<K, V> b(a<K, V> aVar) {
        while (true) {
            a<K, V> aVar2 = aVar.f5422a.get();
            if (aVar2 == null) {
                return aVar;
            }
            aVar = aVar2;
        }
    }
}
