package nl.komponents.kovenant.c;

import kotlin.d.a.c;
import kotlin.d.b.k;
import nl.komponents.kovenant.ao;
import nl.komponents.kovenant.av;

/* compiled from: context-jvm.kt */
final class j extends k implements c<av, ao, k> {

    /* renamed from: a  reason: collision with root package name */
    public static final j f5416a = new j();

    j() {
        super(2);
    }

    public final /* synthetic */ Object invoke(Object obj, Object obj2) {
        av avVar = (av) obj;
        ao aoVar = (ao) obj2;
        kotlin.d.b.j.b(avVar, "dispatcher");
        kotlin.d.b.j.b(aoVar, "context");
        return new k(aoVar.c(), avVar);
    }
}
