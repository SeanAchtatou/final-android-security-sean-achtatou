package nl.komponents.kovenant.c;

import com.facebook.appevents.UserDataStore;
import kotlin.d.a.a;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.m;
import nl.komponents.kovenant.av;
import nl.komponents.kovenant.ax;

/* compiled from: context-api.kt */
public final class k implements ax {

    /* renamed from: a  reason: collision with root package name */
    private final ax f5417a;
    private final av c;

    public k(ax axVar, av avVar) {
        j.b(axVar, "base");
        j.b(avVar, "dispatcher");
        this.f5417a = axVar;
        this.c = avVar;
    }

    public final void a(a<m> aVar) {
        j.b(aVar, UserDataStore.FIRST_NAME);
        ax.b.a(this, aVar);
    }

    public final av a() {
        return this.c;
    }

    public final b<Exception, m> b() {
        return this.f5417a.b();
    }
}
