package nl.komponents.kovenant.c;

import kotlin.d.b.j;
import nl.komponents.kovenant.ao;
import nl.komponents.kovenant.ax;

/* compiled from: context-api.kt */
public final class n {
    public static final ax a(q qVar, ao aoVar) {
        j.b(qVar, "$receiver");
        j.b(aoVar, "context");
        return qVar.a().invoke(qVar.b(), aoVar);
    }
}
