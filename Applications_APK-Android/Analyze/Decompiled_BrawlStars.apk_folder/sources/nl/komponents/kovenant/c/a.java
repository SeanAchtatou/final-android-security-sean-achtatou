package nl.komponents.kovenant.c;

import java.util.concurrent.atomic.AtomicReference;
import kotlin.d.b.j;
import kotlin.d.b.n;
import kotlin.d.b.t;
import kotlin.h.h;
import kotlin.m;
import nl.komponents.kovenant.ConfigurationException;
import nl.komponents.kovenant.ao;
import nl.komponents.kovenant.av;
import nl.komponents.kovenant.ax;
import nl.komponents.kovenant.bb;

/* compiled from: context-jvm.kt */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    final AtomicReference<q> f5403a = new AtomicReference<>(new b());

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [nl.komponents.kovenant.c.q, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final q a() {
        q qVar = this.f5403a.get();
        j.a((Object) qVar, "uiContextRef.get()");
        return qVar;
    }

    /* access modifiers changed from: package-private */
    public final p b() {
        q qVar = this.f5403a.get();
        if (qVar instanceof p) {
            return (p) qVar;
        }
        throw new ConfigurationException("Current UiContext [" + qVar + "] does not implement ReconfigurableUiContext and therefor can't be reconfigured.");
    }

    /* compiled from: context-jvm.kt */
    static final class b implements p {
        private static final /* synthetic */ h[] e;

        /* renamed from: a  reason: collision with root package name */
        private final nl.komponents.kovenant.b.b<av> f5404a = new nl.komponents.kovenant.b.b<>(c.f5409a);

        /* renamed from: b  reason: collision with root package name */
        private final nl.komponents.kovenant.b.b<kotlin.d.a.c<av, ao, ax>> f5405b = new nl.komponents.kovenant.b.b<>(b.f5408a);
        private final nl.komponents.kovenant.b.b c = this.f5405b;
        private final nl.komponents.kovenant.b.b d = this.f5404a;

        static {
            Class<b> cls = b.class;
            e = new h[]{t.a(new n(t.a(cls), "dispatcherContextBuilder", "getDispatcherContextBuilder()Lkotlin/jvm/functions/Function2;")), t.a(new n(t.a(cls), "dispatcher", "getDispatcher()Lnl/komponents/kovenant/Dispatcher;"))};
        }

        public final kotlin.d.a.c<av, ao, ax> a() {
            return (kotlin.d.a.c) this.c.a(e[0]);
        }

        public final void a(kotlin.d.a.c<? super av, ? super ao, ? extends ax> cVar) {
            j.b(cVar, "<set-?>");
            this.c.a(e[0], cVar);
        }

        public final void a(av avVar) {
            j.b(avVar, "<set-?>");
            this.d.a(e[1], avVar);
        }

        public final av b() {
            return (av) this.d.a(e[1]);
        }

        public final p c() {
            b bVar = new b();
            if (this.f5404a.a()) {
                bVar.a(b());
            }
            if (this.f5405b.a()) {
                bVar.a(a());
            }
            return bVar;
        }
    }

    /* compiled from: context-jvm.kt */
    static final class c implements o {
        private static final /* synthetic */ h[] f;

        /* renamed from: a  reason: collision with root package name */
        final nl.komponents.kovenant.b.c<kotlin.d.a.c<av, ao, ax>> f5406a = new nl.komponents.kovenant.b.c<>(new d(this));

        /* renamed from: b  reason: collision with root package name */
        final nl.komponents.kovenant.b.c<av> f5407b = new nl.komponents.kovenant.b.c<>(new e(this));
        private final nl.komponents.kovenant.b.c c = this.f5406a;
        private final nl.komponents.kovenant.b.c d = this.f5407b;
        /* access modifiers changed from: private */
        public final q e;

        static {
            Class<c> cls = c.class;
            f = new h[]{t.a(new n(t.a(cls), "dispatcherContextBuilder", "getDispatcherContextBuilder()Lkotlin/jvm/functions/Function2;")), t.a(new n(t.a(cls), "dispatcher", "getDispatcher()Lnl/komponents/kovenant/Dispatcher;"))};
        }

        public final kotlin.d.a.c<av, ao, ax> a() {
            return (kotlin.d.a.c) this.c.a(f[0]);
        }

        public final void a(kotlin.d.a.c<? super av, ? super ao, ? extends ax> cVar) {
            j.b(cVar, "<set-?>");
            this.c.a(f[0], cVar);
        }

        public final void a(av avVar) {
            j.b(avVar, "<set-?>");
            this.d.a(f[1], avVar);
        }

        public final av b() {
            return (av) this.d.a(f[1]);
        }

        public c(q qVar) {
            j.b(qVar, "currentUiContext");
            this.e = qVar;
        }
    }

    /* renamed from: nl.komponents.kovenant.c.a$a  reason: collision with other inner class name */
    /* compiled from: context-jvm.kt */
    static final class C0167a implements av {
        private static av a() {
            bb bbVar = bb.f5389a;
            return bb.a().c().a();
        }

        public final boolean a(kotlin.d.a.a<m> aVar) {
            j.b(aVar, "task");
            return a().a(aVar);
        }

        public final boolean b(kotlin.d.a.a<m> aVar) {
            j.b(aVar, "task");
            return a().b(aVar);
        }
    }
}
