package nl.komponents.kovenant;

import com.facebook.appevents.UserDataStore;
import kotlin.d.a.a;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.m;
import nl.komponents.kovenant.x;

/* compiled from: context-jvm.kt */
public final class am implements bn {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ x.b f5374a;
    private final /* synthetic */ x.b.a c;

    public final av a() {
        return this.c.a();
    }

    public final void a(a<m> aVar) {
        j.b(aVar, UserDataStore.FIRST_NAME);
        this.c.a(aVar);
    }

    public final void a(b<? super Exception, m> bVar) {
        j.b(bVar, "<set-?>");
        this.c.a(bVar);
    }

    public final void a(av avVar) {
        j.b(avVar, "<set-?>");
        this.c.a(avVar);
    }

    public final b<Exception, m> b() {
        return this.c.b();
    }

    am(x.b bVar) {
        this.f5374a = bVar;
        this.c = bVar.c;
    }
}
