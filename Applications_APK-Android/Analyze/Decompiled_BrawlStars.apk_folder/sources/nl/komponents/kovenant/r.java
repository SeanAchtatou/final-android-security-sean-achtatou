package nl.komponents.kovenant;

import java.util.List;

/* compiled from: dispatcher-jvm.kt */
final class r<V> implements br<V> {

    /* renamed from: a  reason: collision with root package name */
    private final List<br<V>> f5460a;

    /* JADX WARN: Type inference failed for: r2v0, types: [java.util.List<nl.komponents.kovenant.br<V>>, java.lang.Object, java.util.List<? extends nl.komponents.kovenant.br<V>>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public r(java.util.List<? extends nl.komponents.kovenant.br<V>> r2) {
        /*
            r1 = this;
            java.lang.String r0 = "strategies"
            kotlin.d.b.j.b(r2, r0)
            r1.<init>()
            r1.f5460a = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: nl.komponents.kovenant.r.<init>(java.util.List):void");
    }

    public final V a() {
        for (br a2 : this.f5460a) {
            V a3 = a2.a();
            if (a3 != null) {
                return a3;
            }
        }
        return null;
    }
}
