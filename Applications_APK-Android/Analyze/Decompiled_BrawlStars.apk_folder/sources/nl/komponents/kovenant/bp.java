package nl.komponents.kovenant;

import java.util.Collection;
import java.util.concurrent.ConcurrentLinkedQueue;
import kotlin.TypeCastException;
import kotlin.d.b.j;
import kotlin.d.b.w;

/* compiled from: queue-jvm.kt */
public final class bp<V> extends k<V> {

    /* renamed from: a  reason: collision with root package name */
    private final ConcurrentLinkedQueue<V> f5401a = new ConcurrentLinkedQueue<>();

    public final V a() {
        return this.f5401a.poll();
    }

    public final boolean a(V v) {
        j.b(v, "elem");
        return this.f5401a.offer(v);
    }

    public final int d() {
        return this.f5401a.size();
    }

    public final boolean b() {
        return this.f5401a.isEmpty();
    }

    public final boolean c(Object obj) {
        Collection collection = this.f5401a;
        if (collection != null) {
            return w.a(collection).remove(obj);
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.MutableCollection<T>");
    }

    public final boolean c() {
        return !this.f5401a.isEmpty();
    }
}
