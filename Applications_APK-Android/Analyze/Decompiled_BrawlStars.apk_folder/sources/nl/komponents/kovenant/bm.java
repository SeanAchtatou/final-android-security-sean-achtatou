package nl.komponents.kovenant;

import kotlin.d.a.b;
import kotlin.d.a.c;
import kotlin.d.b.j;
import kotlin.m;

/* compiled from: context-api.kt */
public interface bm extends ao {
    void a(b<? super bn, m> bVar);

    void a(c<Object, Object, m> cVar);

    bn b();

    void b(b<? super bn, m> bVar);

    bn d();

    /* compiled from: context-api.kt */
    public static final class a {
        public static void a(bm bmVar, b<? super bn, m> bVar) {
            j.b(bVar, "body");
            bVar.invoke(bmVar.b());
        }

        public static void b(bm bmVar, b<? super bn, m> bVar) {
            j.b(bVar, "body");
            bVar.invoke(bmVar.d());
        }
    }
}
