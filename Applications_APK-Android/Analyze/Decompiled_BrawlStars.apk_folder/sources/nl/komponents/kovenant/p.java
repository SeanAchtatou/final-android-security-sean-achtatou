package nl.komponents.kovenant;

import kotlin.d.a.a;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.m;

/* compiled from: promises-jvm.kt */
public final class p implements q<V, E> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ o f5458a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ o f5459b;

    public final V a() throws Exception {
        return this.f5459b.a();
    }

    public final bw<V, E> a(b<? super V, m> bVar) {
        j.b(bVar, "callback");
        return this.f5459b.a((b) bVar);
    }

    public final bw<V, E> a(ax axVar, a<m> aVar) {
        j.b(axVar, "context");
        j.b(aVar, "callback");
        return this.f5459b.a(axVar, aVar);
    }

    public final bw<V, E> a(ax axVar, b<? super V, m> bVar) {
        j.b(axVar, "context");
        j.b(bVar, "callback");
        return this.f5459b.a(axVar, bVar);
    }

    public final E b() throws FailedException {
        return this.f5459b.b();
    }

    public final bw<V, E> b(b<? super E, m> bVar) {
        j.b(bVar, "callback");
        return this.f5459b.b((b) bVar);
    }

    public final bw<V, E> b(ax axVar, b<? super E, m> bVar) {
        j.b(axVar, "context");
        j.b(bVar, "callback");
        return this.f5459b.b(axVar, bVar);
    }

    public final boolean d() {
        return this.f5459b.d();
    }

    public final boolean e() {
        return this.f5459b.e();
    }

    public final boolean f() {
        return this.f5459b.f();
    }

    public final boolean g(E e) {
        return this.f5459b.g(e);
    }

    p(o oVar) {
        this.f5458a = oVar;
        this.f5459b = oVar;
    }

    public final ao h() {
        return this.f5459b.f5335a;
    }
}
