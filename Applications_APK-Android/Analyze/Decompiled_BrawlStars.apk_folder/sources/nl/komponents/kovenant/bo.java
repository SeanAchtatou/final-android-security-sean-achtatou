package nl.komponents.kovenant;

import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import kotlin.d.a.b;
import kotlin.d.a.d;
import kotlin.d.b.j;
import kotlin.m;

/* compiled from: dispatcher-jvm.kt */
final class bo implements bv {

    /* renamed from: a  reason: collision with root package name */
    final String f5397a;

    /* renamed from: b  reason: collision with root package name */
    final b<Exception, m> f5398b;
    final b<Throwable, m> c;
    final cj<kotlin.d.a.a<m>> d;
    final br<kotlin.d.a.a<m>> e;
    final d<Runnable, String, Integer, Thread> f;
    private final AtomicBoolean g;
    private final AtomicInteger h;
    private final AtomicInteger i;
    private final ConcurrentLinkedQueue<a> j;
    private final int k;

    /* JADX WARN: Type inference failed for: r4v0, types: [kotlin.d.a.b<? super java.lang.Exception, kotlin.m>, java.lang.Object, kotlin.d.a.b<java.lang.Exception, kotlin.m>] */
    /* JADX WARN: Type inference failed for: r5v0, types: [kotlin.d.a.b<? super java.lang.Throwable, kotlin.m>, kotlin.d.a.b<java.lang.Throwable, kotlin.m>, java.lang.Object] */
    /* JADX WARN: Type inference failed for: r8v0, types: [kotlin.d.a.d<java.lang.Runnable, java.lang.String, java.lang.Integer, java.lang.Thread>, java.lang.Object, kotlin.d.a.d<? super java.lang.Runnable, ? super java.lang.String, ? super java.lang.Integer, ? extends java.lang.Thread>] */
    /* JADX WARNING: Unknown variable types count: 3 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public bo(java.lang.String r2, int r3, kotlin.d.a.b<? super java.lang.Exception, kotlin.m> r4, kotlin.d.a.b<? super java.lang.Throwable, kotlin.m> r5, nl.komponents.kovenant.cj<kotlin.d.a.a<kotlin.m>> r6, nl.komponents.kovenant.br<kotlin.d.a.a<kotlin.m>> r7, kotlin.d.a.d<? super java.lang.Runnable, ? super java.lang.String, ? super java.lang.Integer, ? extends java.lang.Thread> r8) {
        /*
            r1 = this;
            java.lang.String r0 = "name"
            kotlin.d.b.j.b(r2, r0)
            java.lang.String r0 = "exceptionHandler"
            kotlin.d.b.j.b(r4, r0)
            java.lang.String r0 = "errorHandler"
            kotlin.d.b.j.b(r5, r0)
            java.lang.String r0 = "workQueue"
            kotlin.d.b.j.b(r6, r0)
            java.lang.String r0 = "pollStrategy"
            kotlin.d.b.j.b(r7, r0)
            java.lang.String r0 = "threadFactory"
            kotlin.d.b.j.b(r8, r0)
            r1.<init>()
            r1.f5397a = r2
            r1.k = r3
            r1.f5398b = r4
            r1.c = r5
            r1.d = r6
            r1.e = r7
            r1.f = r8
            int r2 = r1.k
            r3 = 1
            if (r2 < r3) goto L_0x0052
            java.util.concurrent.atomic.AtomicBoolean r2 = new java.util.concurrent.atomic.AtomicBoolean
            r2.<init>(r3)
            r1.g = r2
            java.util.concurrent.atomic.AtomicInteger r2 = new java.util.concurrent.atomic.AtomicInteger
            r3 = 0
            r2.<init>(r3)
            r1.h = r2
            java.util.concurrent.atomic.AtomicInteger r2 = new java.util.concurrent.atomic.AtomicInteger
            r2.<init>(r3)
            r1.i = r2
            java.util.concurrent.ConcurrentLinkedQueue r2 = new java.util.concurrent.ConcurrentLinkedQueue
            r2.<init>()
            r1.j = r2
            return
        L_0x0052:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "numberOfThreads must be at least 1 but was "
            r3.append(r4)
            int r4 = r1.k
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r2.<init>(r3)
            java.lang.Throwable r2 = (java.lang.Throwable) r2
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: nl.komponents.kovenant.bo.<init>(java.lang.String, int, kotlin.d.a.b, kotlin.d.a.b, nl.komponents.kovenant.cj, nl.komponents.kovenant.br, kotlin.d.a.d):void");
    }

    public final boolean a(kotlin.d.a.a<m> aVar) {
        j.b(aVar, "task");
        if (!this.g.get()) {
            return false;
        }
        this.d.b(aVar);
        if (this.i.get() >= this.k) {
            return true;
        }
        if (this.i.incrementAndGet() > this.k || this.d.d() <= 0) {
            this.i.decrementAndGet();
            return true;
        }
        a aVar2 = new a(this.h.incrementAndGet());
        this.j.offer(aVar2);
        if (this.g.get()) {
            return true;
        }
        aVar2.a();
        return true;
    }

    public final boolean b(kotlin.d.a.a<m> aVar) {
        boolean z;
        j.b(aVar, "task");
        if (this.d.c(aVar)) {
            return true;
        }
        Iterator it = this.j.iterator();
        do {
            z = false;
            if (it.hasNext()) {
                a aVar2 = (a) it.next();
                j.b(aVar, "task");
                while (true) {
                    if (aVar != aVar2.e) {
                        break;
                    }
                    if (aVar2.c.compareAndSet(aVar2.f5399a, aVar2.f5400b)) {
                        boolean z2 = aVar == aVar2.e;
                        if (z2) {
                            aVar2.d.interrupt();
                            aVar2.e = null;
                        }
                        aVar2.a(aVar2.f5400b, aVar2.f5399a);
                        if (z2) {
                            z = true;
                            continue;
                            break;
                        }
                    }
                }
            } else {
                return false;
            }
        } while (!z);
        return true;
    }

    public final boolean a() {
        for (a aVar : this.j) {
            if (j.a(Thread.currentThread(), aVar.d)) {
                return true;
            }
        }
        return false;
    }

    public final boolean a(a aVar, boolean z) {
        j.b(aVar, "context");
        boolean remove = this.j.remove(aVar);
        if (!z && remove && this.j.isEmpty() && this.d.c() && this.g.get()) {
            this.j.add(aVar);
            return false;
        } else if (!remove) {
            return true;
        } else {
            this.i.decrementAndGet();
            return true;
        }
    }

    /* compiled from: dispatcher-jvm.kt */
    final class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final int f5399a = 1;

        /* renamed from: b  reason: collision with root package name */
        final int f5400b = 3;
        final AtomicInteger c = new AtomicInteger(this.g);
        final Thread d;
        volatile kotlin.d.a.a<m> e;
        private final int g;
        private final int h = 2;
        private volatile boolean i = true;
        private volatile boolean j = true;
        private final int k;

        public a(int i2) {
            this.k = i2;
            this.d = bo.this.f.invoke(this, bo.this.f5397a, Integer.valueOf(this.k));
            if (!this.d.isAlive()) {
                this.d.start();
            }
        }

        /* access modifiers changed from: package-private */
        public final void a(int i2, int i3) {
            do {
            } while (this.c.compareAndSet(i2, i3));
        }

        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0052, code lost:
            if (r7.i == false) goto L_0x0054;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x0054, code lost:
            r7.d.interrupt();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:38:0x008c, code lost:
            if (r7.i == false) goto L_0x0054;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void run() {
            /*
                r7 = this;
            L_0x0000:
                boolean r0 = r7.i
                if (r0 == 0) goto L_0x00e9
                int r0 = r7.g
                int r1 = r7.h
                r7.a(r0, r1)
                boolean r0 = r7.j     // Catch:{ InterruptedException -> 0x0077, all -> 0x005a }
                if (r0 == 0) goto L_0x001a
                nl.komponents.kovenant.bo r0 = nl.komponents.kovenant.bo.this     // Catch:{ InterruptedException -> 0x0077, all -> 0x005a }
                nl.komponents.kovenant.br<kotlin.d.a.a<kotlin.m>> r0 = r0.e     // Catch:{ InterruptedException -> 0x0077, all -> 0x005a }
                java.lang.Object r0 = r0.a()     // Catch:{ InterruptedException -> 0x0077, all -> 0x005a }
            L_0x0017:
                kotlin.d.a.a r0 = (kotlin.d.a.a) r0     // Catch:{ InterruptedException -> 0x0077, all -> 0x005a }
                goto L_0x0028
            L_0x001a:
                nl.komponents.kovenant.bo r0 = nl.komponents.kovenant.bo.this     // Catch:{ InterruptedException -> 0x0077, all -> 0x005a }
                nl.komponents.kovenant.cj<kotlin.d.a.a<kotlin.m>> r1 = r0.d     // Catch:{ InterruptedException -> 0x0077, all -> 0x005a }
                r2 = 0
                r3 = 0
                r5 = 2
                r6 = 0
                java.lang.Object r0 = r1.a(r2, -1)     // Catch:{ InterruptedException -> 0x0077, all -> 0x005a }
                goto L_0x0017
            L_0x0028:
                r7.e = r0     // Catch:{ InterruptedException -> 0x0077, all -> 0x005a }
                boolean r0 = r7.j     // Catch:{ InterruptedException -> 0x0077, all -> 0x005a }
                if (r0 == 0) goto L_0x0032
                kotlin.d.a.a<kotlin.m> r0 = r7.e     // Catch:{ InterruptedException -> 0x0077, all -> 0x005a }
                if (r0 != 0) goto L_0x003e
            L_0x0032:
                nl.komponents.kovenant.bo r0 = nl.komponents.kovenant.bo.this     // Catch:{ InterruptedException -> 0x0077, all -> 0x005a }
                r1 = 0
                boolean r0 = r0.a(r7, r1)     // Catch:{ InterruptedException -> 0x0077, all -> 0x005a }
                if (r0 == 0) goto L_0x003e
                r7.a()     // Catch:{ InterruptedException -> 0x0077, all -> 0x005a }
            L_0x003e:
                int r0 = r7.h
                int r1 = r7.g
                r7.a(r0, r1)
                boolean r0 = r7.j
                if (r0 != 0) goto L_0x008f
                boolean r0 = r7.i
                if (r0 == 0) goto L_0x008f
                java.lang.Thread.interrupted()
                boolean r0 = r7.i
                if (r0 != 0) goto L_0x008f
            L_0x0054:
                java.lang.Thread r0 = r7.d
                r0.interrupt()
                goto L_0x008f
            L_0x005a:
                r0 = move-exception
                int r1 = r7.h
                int r2 = r7.g
                r7.a(r1, r2)
                boolean r1 = r7.j
                if (r1 != 0) goto L_0x0076
                boolean r1 = r7.i
                if (r1 == 0) goto L_0x0076
                java.lang.Thread.interrupted()
                boolean r1 = r7.i
                if (r1 != 0) goto L_0x0076
                java.lang.Thread r1 = r7.d
                r1.interrupt()
            L_0x0076:
                throw r0
            L_0x0077:
                int r0 = r7.h
                int r1 = r7.g
                r7.a(r0, r1)
                boolean r0 = r7.j
                if (r0 != 0) goto L_0x008f
                boolean r0 = r7.i
                if (r0 == 0) goto L_0x008f
                java.lang.Thread.interrupted()
                boolean r0 = r7.i
                if (r0 != 0) goto L_0x008f
                goto L_0x0054
            L_0x008f:
                kotlin.d.a.a<kotlin.m> r0 = r7.e
                if (r0 == 0) goto L_0x0000
                r1 = 0
                int r2 = r7.g     // Catch:{ InterruptedException -> 0x00cb, Exception -> 0x00c2, all -> 0x00af }
                int r3 = r7.f5399a     // Catch:{ InterruptedException -> 0x00cb, Exception -> 0x00c2, all -> 0x00af }
                r7.a(r2, r3)     // Catch:{ InterruptedException -> 0x00cb, Exception -> 0x00c2, all -> 0x00af }
                r0.invoke()     // Catch:{ all -> 0x00a6 }
                int r0 = r7.f5399a     // Catch:{ InterruptedException -> 0x00cb, Exception -> 0x00c2, all -> 0x00af }
                int r2 = r7.g     // Catch:{ InterruptedException -> 0x00cb, Exception -> 0x00c2, all -> 0x00af }
                r7.a(r0, r2)     // Catch:{ InterruptedException -> 0x00cb, Exception -> 0x00c2, all -> 0x00af }
                goto L_0x00db
            L_0x00a6:
                r0 = move-exception
                int r2 = r7.f5399a     // Catch:{ InterruptedException -> 0x00cb, Exception -> 0x00c2, all -> 0x00af }
                int r3 = r7.g     // Catch:{ InterruptedException -> 0x00cb, Exception -> 0x00c2, all -> 0x00af }
                r7.a(r2, r3)     // Catch:{ InterruptedException -> 0x00cb, Exception -> 0x00c2, all -> 0x00af }
                throw r0     // Catch:{ InterruptedException -> 0x00cb, Exception -> 0x00c2, all -> 0x00af }
            L_0x00af:
                r0 = move-exception
                boolean r2 = r0 instanceof java.lang.StackOverflowError     // Catch:{ all -> 0x00e2 }
                if (r2 != 0) goto L_0x00ba
                nl.komponents.kovenant.bo r2 = nl.komponents.kovenant.bo.this     // Catch:{ all -> 0x00e2 }
                r3 = 1
                r2.a(r7, r3)     // Catch:{ all -> 0x00e2 }
            L_0x00ba:
                nl.komponents.kovenant.bo r2 = nl.komponents.kovenant.bo.this     // Catch:{ all -> 0x00e2 }
                kotlin.d.a.b<java.lang.Throwable, kotlin.m> r2 = r2.c     // Catch:{ all -> 0x00e2 }
                r2.invoke(r0)     // Catch:{ all -> 0x00e2 }
                goto L_0x00db
            L_0x00c2:
                r0 = move-exception
                nl.komponents.kovenant.bo r2 = nl.komponents.kovenant.bo.this     // Catch:{ all -> 0x00e2 }
                kotlin.d.a.b<java.lang.Exception, kotlin.m> r2 = r2.f5398b     // Catch:{ all -> 0x00e2 }
                r2.invoke(r0)     // Catch:{ all -> 0x00e2 }
                goto L_0x00db
            L_0x00cb:
                r0 = move-exception
                kotlin.d.a.a<kotlin.m> r2 = r7.e     // Catch:{ all -> 0x00e2 }
                if (r2 == 0) goto L_0x00db
                boolean r2 = r7.i     // Catch:{ all -> 0x00e2 }
                if (r2 == 0) goto L_0x00db
                nl.komponents.kovenant.bo r2 = nl.komponents.kovenant.bo.this     // Catch:{ all -> 0x00e2 }
                kotlin.d.a.b<java.lang.Exception, kotlin.m> r2 = r2.f5398b     // Catch:{ all -> 0x00e2 }
                r2.invoke(r0)     // Catch:{ all -> 0x00e2 }
            L_0x00db:
                java.lang.Thread.interrupted()
                r7.e = r1
                goto L_0x0000
            L_0x00e2:
                r0 = move-exception
                java.lang.Thread.interrupted()
                r7.e = r1
                throw r0
            L_0x00e9:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: nl.komponents.kovenant.bo.a.run():void");
        }

        public final void a() {
            this.i = false;
            this.d.interrupt();
            bo.this.a(this, true);
        }
    }
}
