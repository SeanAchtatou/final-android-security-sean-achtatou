package nl.komponents.kovenant;

/* compiled from: exceptions-api.kt */
public class EmptyException extends KovenantException {
    public EmptyException() {
        this(null, null, 3);
    }

    private EmptyException(String str, Exception exc) {
        super(str, exc);
    }

    private /* synthetic */ EmptyException(String str, Exception exc, int i) {
        this(null, null);
    }
}
