package androidx.versionedparcelable;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseIntArray;

/* compiled from: VersionedParcelParcel */
class b extends VersionedParcel {

    /* renamed from: a  reason: collision with root package name */
    private final SparseIntArray f13a;

    /* renamed from: b  reason: collision with root package name */
    private final Parcel f14b;
    private final int c;
    private final int d;
    private final String e;
    private int f;
    private int g;

    b(Parcel parcel) {
        this(parcel, parcel.dataPosition(), parcel.dataSize(), "");
    }

    private b(Parcel parcel, int i, int i2, String str) {
        this.f13a = new SparseIntArray();
        this.f = -1;
        this.g = 0;
        this.f14b = parcel;
        this.c = i;
        this.d = i2;
        this.g = this.c;
        this.e = str;
    }

    public final void c(int i) {
        a();
        this.f = i;
        this.f13a.put(i, this.f14b.dataPosition());
        a(0);
        a(i);
    }

    public final void a() {
        int i = this.f;
        if (i >= 0) {
            int i2 = this.f13a.get(i);
            int dataPosition = this.f14b.dataPosition();
            this.f14b.setDataPosition(i2);
            this.f14b.writeInt(dataPosition - i2);
            this.f14b.setDataPosition(dataPosition);
        }
    }

    /* access modifiers changed from: protected */
    public final VersionedParcel b() {
        Parcel parcel = this.f14b;
        int dataPosition = parcel.dataPosition();
        int i = this.g;
        if (i == this.c) {
            i = this.d;
        }
        return new b(parcel, dataPosition, i, this.e + "  ");
    }

    public final void a(byte[] bArr) {
        if (bArr != null) {
            this.f14b.writeInt(bArr.length);
            this.f14b.writeByteArray(bArr);
            return;
        }
        this.f14b.writeInt(-1);
    }

    public final void a(int i) {
        this.f14b.writeInt(i);
    }

    public final void a(String str) {
        this.f14b.writeString(str);
    }

    public final void a(Parcelable parcelable) {
        this.f14b.writeParcelable(parcelable, 0);
    }

    public final int c() {
        return this.f14b.readInt();
    }

    public final String d() {
        return this.f14b.readString();
    }

    public final byte[] e() {
        int readInt = this.f14b.readInt();
        if (readInt < 0) {
            return null;
        }
        byte[] bArr = new byte[readInt];
        this.f14b.readByteArray(bArr);
        return bArr;
    }

    public final <T extends Parcelable> T f() {
        return this.f14b.readParcelable(getClass().getClassLoader());
    }

    /* JADX WARNING: Removed duplicated region for block: B:0:0x0000 A[LOOP_START, MTH_ENTER_BLOCK] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean b(int r5) {
        /*
            r4 = this;
        L_0x0000:
            int r0 = r4.g
            int r1 = r4.d
            r2 = -1
            if (r0 >= r1) goto L_0x0026
            android.os.Parcel r1 = r4.f14b
            r1.setDataPosition(r0)
            android.os.Parcel r0 = r4.f14b
            int r0 = r0.readInt()
            android.os.Parcel r1 = r4.f14b
            int r1 = r1.readInt()
            int r3 = r4.g
            int r3 = r3 + r0
            r4.g = r3
            if (r1 != r5) goto L_0x0000
            android.os.Parcel r5 = r4.f14b
            int r5 = r5.dataPosition()
            goto L_0x0027
        L_0x0026:
            r5 = -1
        L_0x0027:
            if (r5 != r2) goto L_0x002b
            r5 = 0
            return r5
        L_0x002b:
            android.os.Parcel r0 = r4.f14b
            r0.setDataPosition(r5)
            r5 = 1
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.versionedparcelable.b.b(int):boolean");
    }
}
