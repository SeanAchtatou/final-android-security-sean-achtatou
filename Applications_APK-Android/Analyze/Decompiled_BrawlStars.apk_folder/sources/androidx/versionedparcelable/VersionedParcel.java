package androidx.versionedparcelable;

import android.os.Parcelable;
import java.lang.reflect.InvocationTargetException;

public abstract class VersionedParcel {

    public static class ParcelException extends RuntimeException {
    }

    /* access modifiers changed from: protected */
    public abstract void a();

    /* access modifiers changed from: protected */
    public abstract void a(int i);

    /* access modifiers changed from: protected */
    public abstract void a(Parcelable parcelable);

    /* access modifiers changed from: protected */
    public abstract void a(String str);

    /* access modifiers changed from: protected */
    public abstract void a(byte[] bArr);

    /* access modifiers changed from: protected */
    public abstract VersionedParcel b();

    /* access modifiers changed from: protected */
    public abstract boolean b(int i);

    /* access modifiers changed from: protected */
    public abstract int c();

    /* access modifiers changed from: protected */
    public abstract void c(int i);

    /* access modifiers changed from: protected */
    public abstract String d();

    /* access modifiers changed from: protected */
    public abstract byte[] e();

    /* access modifiers changed from: protected */
    public abstract <T extends Parcelable> T f();

    public final void a(byte[] bArr, int i) {
        c(2);
        a(bArr);
    }

    public final void a(int i, int i2) {
        c(i2);
        a(i);
    }

    public final void a(String str, int i) {
        c(7);
        a(str);
    }

    public final void a(Parcelable parcelable, int i) {
        c(i);
        a(parcelable);
    }

    public final int b(int i, int i2) {
        if (!b(i2)) {
            return i;
        }
        return c();
    }

    public final String b(String str, int i) {
        if (!b(7)) {
            return str;
        }
        return d();
    }

    public final byte[] b(byte[] bArr, int i) {
        if (!b(2)) {
            return bArr;
        }
        return e();
    }

    public final <T extends Parcelable> T b(Parcelable parcelable, int i) {
        if (!b(i)) {
            return parcelable;
        }
        return f();
    }

    public final void a(c cVar, int i) {
        c(1);
        a(cVar);
    }

    /* access modifiers changed from: protected */
    public final void a(c cVar) {
        if (cVar == null) {
            a((String) null);
            return;
        }
        b(cVar);
        VersionedParcel b2 = b();
        a(cVar, b2);
        b2.a();
    }

    private void b(c cVar) {
        try {
            a(a((Class<? extends c>) cVar.getClass()).getName());
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(cVar.getClass().getSimpleName() + " does not have a Parcelizer", e);
        }
    }

    public final <T extends c> T b(c cVar, int i) {
        if (!b(1)) {
            return cVar;
        }
        return g();
    }

    /* access modifiers changed from: protected */
    public final <T extends c> T g() {
        String d = d();
        if (d == null) {
            return null;
        }
        return a(d, b());
    }

    private static <T extends c> T a(String str, VersionedParcel versionedParcel) {
        Class<VersionedParcel> cls = VersionedParcel.class;
        try {
            return (c) Class.forName(str, true, cls.getClassLoader()).getDeclaredMethod("read", cls).invoke(null, versionedParcel);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("VersionedParcel encountered IllegalAccessException", e);
        } catch (InvocationTargetException e2) {
            if (e2.getCause() instanceof RuntimeException) {
                throw ((RuntimeException) e2.getCause());
            }
            throw new RuntimeException("VersionedParcel encountered InvocationTargetException", e2);
        } catch (NoSuchMethodException e3) {
            throw new RuntimeException("VersionedParcel encountered NoSuchMethodException", e3);
        } catch (ClassNotFoundException e4) {
            throw new RuntimeException("VersionedParcel encountered ClassNotFoundException", e4);
        }
    }

    private static <T extends c> void a(c cVar, VersionedParcel versionedParcel) {
        try {
            c(cVar).getDeclaredMethod("write", cVar.getClass(), VersionedParcel.class).invoke(null, cVar, versionedParcel);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("VersionedParcel encountered IllegalAccessException", e);
        } catch (InvocationTargetException e2) {
            if (e2.getCause() instanceof RuntimeException) {
                throw ((RuntimeException) e2.getCause());
            }
            throw new RuntimeException("VersionedParcel encountered InvocationTargetException", e2);
        } catch (NoSuchMethodException e3) {
            throw new RuntimeException("VersionedParcel encountered NoSuchMethodException", e3);
        } catch (ClassNotFoundException e4) {
            throw new RuntimeException("VersionedParcel encountered ClassNotFoundException", e4);
        }
    }

    private static <T extends c> Class c(T t) throws ClassNotFoundException {
        return a((Class<? extends c>) t.getClass());
    }

    private static Class a(Class<? extends c> cls) throws ClassNotFoundException {
        return Class.forName(String.format("%s.%sParcelizer", cls.getPackage().getName(), cls.getSimpleName()), false, cls.getClassLoader());
    }
}
