package bolts;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.facebook.internal.NativeProtocol;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class MeasurementEvent {
    public static final String APP_LINK_NAVIGATE_IN_EVENT_NAME = "al_nav_in";
    public static final String APP_LINK_NAVIGATE_OUT_EVENT_NAME = "al_nav_out";
    public static final String MEASUREMENT_EVENT_ARGS_KEY = "event_args";
    public static final String MEASUREMENT_EVENT_NAME_KEY = "event_name";
    public static final String MEASUREMENT_EVENT_NOTIFICATION_NAME = "com.parse.bolts.measurement_event";

    /* renamed from: a  reason: collision with root package name */
    private Context f1247a;

    /* renamed from: b  reason: collision with root package name */
    private String f1248b;
    private Bundle c;

    static void a(Context context, String str, Intent intent, Map<String, String> map) {
        Bundle bundle = new Bundle();
        if (intent != null) {
            Bundle appLinkData = AppLinks.getAppLinkData(intent);
            if (appLinkData != null) {
                bundle = a(context, str, appLinkData, intent);
            } else {
                Uri data = intent.getData();
                if (data != null) {
                    bundle.putString("intentData", data.toString());
                }
                Bundle extras = intent.getExtras();
                if (extras != null) {
                    for (String next : extras.keySet()) {
                        bundle.putString(next, a(extras.get(next)));
                    }
                }
            }
        }
        if (map != null) {
            for (String next2 : map.keySet()) {
                bundle.putString(next2, map.get(next2));
            }
        }
        MeasurementEvent measurementEvent = new MeasurementEvent(context, str, bundle);
        if (measurementEvent.f1248b == null) {
            measurementEvent.getClass().getName();
        }
        try {
            Class<?> cls = Class.forName("android.support.v4.content.LocalBroadcastManager");
            Method method = cls.getMethod("getInstance", Context.class);
            Method method2 = cls.getMethod("sendBroadcast", Intent.class);
            Object invoke = method.invoke(null, measurementEvent.f1247a);
            Intent intent2 = new Intent(MEASUREMENT_EVENT_NOTIFICATION_NAME);
            intent2.putExtra(MEASUREMENT_EVENT_NAME_KEY, measurementEvent.f1248b);
            intent2.putExtra(MEASUREMENT_EVENT_ARGS_KEY, measurementEvent.c);
            method2.invoke(invoke, intent2);
        } catch (Exception unused) {
            measurementEvent.getClass().getName();
        }
    }

    private MeasurementEvent(Context context, String str, Bundle bundle) {
        this.f1247a = context.getApplicationContext();
        this.f1248b = str;
        this.c = bundle;
    }

    private static Bundle a(Context context, String str, Bundle bundle, Intent intent) {
        Bundle bundle2 = new Bundle();
        ComponentName resolveActivity = intent.resolveActivity(context.getPackageManager());
        if (resolveActivity != null) {
            bundle2.putString("class", resolveActivity.getShortClassName());
        }
        if (APP_LINK_NAVIGATE_OUT_EVENT_NAME.equals(str)) {
            if (resolveActivity != null) {
                bundle2.putString("package", resolveActivity.getPackageName());
            }
            if (intent.getData() != null) {
                bundle2.putString("outputURL", intent.getData().toString());
            }
            if (intent.getScheme() != null) {
                bundle2.putString("outputURLScheme", intent.getScheme());
            }
        } else if (APP_LINK_NAVIGATE_IN_EVENT_NAME.equals(str)) {
            if (intent.getData() != null) {
                bundle2.putString("inputURL", intent.getData().toString());
            }
            if (intent.getScheme() != null) {
                bundle2.putString("inputURLScheme", intent.getScheme());
            }
        }
        for (String next : bundle.keySet()) {
            Object obj = bundle.get(next);
            if (obj instanceof Bundle) {
                Bundle bundle3 = (Bundle) obj;
                for (String next2 : bundle3.keySet()) {
                    String a2 = a(bundle3.get(next2));
                    if (next.equals("referer_app_link")) {
                        if (next2.equalsIgnoreCase("url")) {
                            bundle2.putString("refererURL", a2);
                        } else if (next2.equalsIgnoreCase(NativeProtocol.BRIDGE_ARG_APP_NAME_STRING)) {
                            bundle2.putString("refererAppName", a2);
                        } else if (next2.equalsIgnoreCase("package")) {
                            bundle2.putString("sourceApplication", a2);
                        }
                    }
                    bundle2.putString(next + "/" + next2, a2);
                }
            } else {
                String a3 = a(obj);
                if (next.equals("target_url")) {
                    Uri parse = Uri.parse(a3);
                    bundle2.putString("targetURL", parse.toString());
                    bundle2.putString("targetURLHost", parse.getHost());
                } else {
                    bundle2.putString(next, a3);
                }
            }
        }
        return bundle2;
    }

    private static String a(Object obj) {
        if (obj == null) {
            return null;
        }
        if ((obj instanceof JSONArray) || (obj instanceof JSONObject)) {
            return obj.toString();
        }
        try {
            if (obj instanceof Collection) {
                return new JSONArray((Collection) obj).toString();
            }
            if (obj instanceof Map) {
                return new JSONObject((Map) obj).toString();
            }
            return obj.toString();
        } catch (Exception unused) {
            return null;
        }
    }
}
