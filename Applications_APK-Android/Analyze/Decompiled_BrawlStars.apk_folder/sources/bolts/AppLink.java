package bolts;

import android.net.Uri;
import java.util.Collections;
import java.util.List;

public class AppLink {

    /* renamed from: a  reason: collision with root package name */
    private Uri f1228a;

    /* renamed from: b  reason: collision with root package name */
    private List<Target> f1229b;
    private Uri c;

    public static class Target {

        /* renamed from: a  reason: collision with root package name */
        private final Uri f1230a;

        /* renamed from: b  reason: collision with root package name */
        private final String f1231b;
        private final String c;
        private final String d;

        public Target(String str, String str2, Uri uri, String str3) {
            this.f1231b = str;
            this.c = str2;
            this.f1230a = uri;
            this.d = str3;
        }

        public Uri getUrl() {
            return this.f1230a;
        }

        public String getAppName() {
            return this.d;
        }

        public String getClassName() {
            return this.c;
        }

        public String getPackageName() {
            return this.f1231b;
        }
    }

    public AppLink(Uri uri, List<Target> list, Uri uri2) {
        this.f1228a = uri;
        this.f1229b = list == null ? Collections.emptyList() : list;
        this.c = uri2;
    }

    public Uri getSourceUrl() {
        return this.f1228a;
    }

    public List<Target> getTargets() {
        return Collections.unmodifiableList(this.f1229b);
    }

    public Uri getWebUrl() {
        return this.c;
    }
}
