package bolts;

import java.io.Closeable;

public class CancellationTokenRegistration implements Closeable {

    /* renamed from: a  reason: collision with root package name */
    private final Object f1241a = new Object();

    /* renamed from: b  reason: collision with root package name */
    private CancellationTokenSource f1242b;
    private Runnable c;
    private boolean d;

    CancellationTokenRegistration(CancellationTokenSource cancellationTokenSource, Runnable runnable) {
        this.f1242b = cancellationTokenSource;
        this.c = runnable;
    }

    public void close() {
        synchronized (this.f1241a) {
            if (!this.d) {
                this.d = true;
                CancellationTokenSource cancellationTokenSource = this.f1242b;
                synchronized (cancellationTokenSource.f1243a) {
                    cancellationTokenSource.a();
                    cancellationTokenSource.f1244b.remove(this);
                }
                this.f1242b = null;
                this.c = null;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        synchronized (this.f1241a) {
            if (!this.d) {
                this.c.run();
                close();
            } else {
                throw new IllegalStateException("Object already closed");
            }
        }
    }
}
