package bolts;

public class TaskCompletionSource<TResult> {

    /* renamed from: a  reason: collision with root package name */
    private final Task<TResult> f1280a = new Task<>();

    public Task<TResult> getTask() {
        return this.f1280a;
    }

    public boolean trySetCancelled() {
        return this.f1280a.a();
    }

    public boolean trySetResult(TResult tresult) {
        return this.f1280a.a((Object) tresult);
    }

    public boolean trySetError(Exception exc) {
        return this.f1280a.a(exc);
    }

    public void setCancelled() {
        if (!trySetCancelled()) {
            throw new IllegalStateException("Cannot cancel a completed task.");
        }
    }

    public void setResult(TResult tresult) {
        if (!trySetResult(tresult)) {
            throw new IllegalStateException("Cannot set the result of a completed task.");
        }
    }

    public void setError(Exception exc) {
        if (!trySetError(exc)) {
            throw new IllegalStateException("Cannot set the error on a completed task.");
        }
    }
}
