package bolts;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class Task<TResult> {
    public static final ExecutorService BACKGROUND_EXECUTOR = BoltsExecutors.background();
    public static final Executor UI_THREAD_EXECUTOR = AndroidExecutors.uiThread();

    /* renamed from: a  reason: collision with root package name */
    private static final Executor f1249a = BoltsExecutors.b();

    /* renamed from: b  reason: collision with root package name */
    private static volatile UnobservedExceptionHandler f1250b;
    private static Task<?> k = new Task<>((Object) null);
    private static Task<Boolean> l = new Task<>((Object) true);
    private static Task<Boolean> m = new Task<>((Object) false);
    private static Task<?> n = new Task<>(true);
    private final Object c = new Object();
    private boolean d;
    private boolean e;
    private TResult f;
    private Exception g;
    private boolean h;
    private UnobservedErrorNotifier i;
    private List<Continuation<TResult, Void>> j = new ArrayList();

    public interface UnobservedExceptionHandler {
        void unobservedException(Task<?> task, UnobservedTaskException unobservedTaskException);
    }

    public <TOut> Task<TOut> cast() {
        return this;
    }

    public static UnobservedExceptionHandler getUnobservedExceptionHandler() {
        return f1250b;
    }

    public static void setUnobservedExceptionHandler(UnobservedExceptionHandler unobservedExceptionHandler) {
        f1250b = unobservedExceptionHandler;
    }

    Task() {
    }

    private Task(TResult tresult) {
        a((Object) tresult);
    }

    private Task(boolean z) {
        a();
    }

    public static <TResult> Task<TResult>.TaskCompletionSource create() {
        Task task = new Task();
        task.getClass();
        return new TaskCompletionSource();
    }

    public boolean isCompleted() {
        boolean z;
        synchronized (this.c) {
            z = this.d;
        }
        return z;
    }

    public boolean isCancelled() {
        boolean z;
        synchronized (this.c) {
            z = this.e;
        }
        return z;
    }

    public boolean isFaulted() {
        boolean z;
        synchronized (this.c) {
            z = getError() != null;
        }
        return z;
    }

    public TResult getResult() {
        TResult tresult;
        synchronized (this.c) {
            tresult = this.f;
        }
        return tresult;
    }

    public Exception getError() {
        Exception exc;
        synchronized (this.c) {
            if (this.g != null) {
                this.h = true;
                if (this.i != null) {
                    this.i.setObserved();
                    this.i = null;
                }
            }
            exc = this.g;
        }
        return exc;
    }

    public void waitForCompletion() throws InterruptedException {
        synchronized (this.c) {
            if (!isCompleted()) {
                this.c.wait();
            }
        }
    }

    public boolean waitForCompletion(long j2, TimeUnit timeUnit) throws InterruptedException {
        boolean isCompleted;
        synchronized (this.c) {
            if (!isCompleted()) {
                this.c.wait(timeUnit.toMillis(j2));
            }
            isCompleted = isCompleted();
        }
        return isCompleted;
    }

    public static <TResult> Task<TResult> forResult(TResult tresult) {
        if (tresult == null) {
            return k;
        }
        if (tresult instanceof Boolean) {
            return ((Boolean) tresult).booleanValue() ? l : m;
        }
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        taskCompletionSource.setResult(tresult);
        return taskCompletionSource.getTask();
    }

    public static <TResult> Task<TResult> forError(Exception exc) {
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        taskCompletionSource.setError(exc);
        return taskCompletionSource.getTask();
    }

    public static <TResult> Task<TResult> cancelled() {
        return n;
    }

    public static Task<Void> delay(long j2) {
        return a(j2, BoltsExecutors.a(), null);
    }

    public static Task<Void> delay(long j2, CancellationToken cancellationToken) {
        return a(j2, BoltsExecutors.a(), cancellationToken);
    }

    private static Task<Void> a(long j2, ScheduledExecutorService scheduledExecutorService, CancellationToken cancellationToken) {
        if (cancellationToken != null && cancellationToken.isCancellationRequested()) {
            return cancelled();
        }
        if (j2 <= 0) {
            return forResult(null);
        }
        final TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        final ScheduledFuture<?> schedule = scheduledExecutorService.schedule(new Runnable() {
            public final void run() {
                TaskCompletionSource.this.trySetResult(null);
            }
        }, j2, TimeUnit.MILLISECONDS);
        if (cancellationToken != null) {
            cancellationToken.register(new Runnable() {
                public final void run() {
                    schedule.cancel(true);
                    taskCompletionSource.trySetCancelled();
                }
            });
        }
        return taskCompletionSource.getTask();
    }

    public Task<Void> makeVoid() {
        return continueWithTask(new Continuation<TResult, Task<Void>>() {
            public Task<Void> then(Task<TResult> task) throws Exception {
                if (task.isCancelled()) {
                    return Task.cancelled();
                }
                if (task.isFaulted()) {
                    return Task.forError(task.getError());
                }
                return Task.forResult(null);
            }
        });
    }

    public static <TResult> Task<TResult> callInBackground(Callable<TResult> callable) {
        return call(callable, BACKGROUND_EXECUTOR, null);
    }

    public static <TResult> Task<TResult> callInBackground(Callable<TResult> callable, CancellationToken cancellationToken) {
        return call(callable, BACKGROUND_EXECUTOR, cancellationToken);
    }

    public static <TResult> Task<TResult> call(Callable<TResult> callable, Executor executor) {
        return call(callable, executor, null);
    }

    public static <TResult> Task<TResult> call(final Callable<TResult> callable, Executor executor, final CancellationToken cancellationToken) {
        final TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        try {
            executor.execute(new Runnable() {
                public final void run() {
                    CancellationToken cancellationToken = CancellationToken.this;
                    if (cancellationToken == null || !cancellationToken.isCancellationRequested()) {
                        try {
                            taskCompletionSource.setResult(callable.call());
                        } catch (CancellationException unused) {
                            taskCompletionSource.setCancelled();
                        } catch (Exception e) {
                            taskCompletionSource.setError(e);
                        }
                    } else {
                        taskCompletionSource.setCancelled();
                    }
                }
            });
        } catch (Exception e2) {
            taskCompletionSource.setError(new ExecutorException(e2));
        }
        return taskCompletionSource.getTask();
    }

    public static <TResult> Task<TResult> call(Callable<TResult> callable) {
        return call(callable, f1249a, null);
    }

    public static <TResult> Task<TResult> call(Callable<TResult> callable, CancellationToken cancellationToken) {
        return call(callable, f1249a, cancellationToken);
    }

    public static <TResult> Task<Task<TResult>> whenAnyResult(Collection<? extends Task<TResult>> collection) {
        if (collection.size() == 0) {
            return forResult(null);
        }
        final TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        final AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        for (Task continueWith : collection) {
            continueWith.continueWith(new Continuation<TResult, Void>() {
                public final Void then(Task<TResult> task) {
                    if (atomicBoolean.compareAndSet(false, true)) {
                        taskCompletionSource.setResult(task);
                        return null;
                    }
                    task.getError();
                    return null;
                }
            });
        }
        return taskCompletionSource.getTask();
    }

    public static Task<Task<?>> whenAny(Collection<? extends Task<?>> collection) {
        if (collection.size() == 0) {
            return forResult(null);
        }
        final TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        final AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        for (Task continueWith : collection) {
            continueWith.continueWith(new Continuation<Object, Void>() {
                public final Void then(Task<Object> task) {
                    if (atomicBoolean.compareAndSet(false, true)) {
                        taskCompletionSource.setResult(task);
                        return null;
                    }
                    task.getError();
                    return null;
                }
            });
        }
        return taskCompletionSource.getTask();
    }

    public static <TResult> Task<List<TResult>> whenAllResult(final Collection<? extends Task<TResult>> collection) {
        return whenAll(collection).onSuccess(new Continuation<Void, List<TResult>>() {
            public final List<TResult> then(Task<Void> task) throws Exception {
                if (collection.size() == 0) {
                    return Collections.emptyList();
                }
                ArrayList arrayList = new ArrayList();
                for (Task result : collection) {
                    arrayList.add(result.getResult());
                }
                return arrayList;
            }
        });
    }

    public static Task<Void> whenAll(Collection<? extends Task<?>> collection) {
        if (collection.size() == 0) {
            return forResult(null);
        }
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        ArrayList arrayList = new ArrayList();
        Object obj = new Object();
        AtomicInteger atomicInteger = new AtomicInteger(collection.size());
        AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        for (Task continueWith : collection) {
            final Object obj2 = obj;
            final ArrayList arrayList2 = arrayList;
            final AtomicBoolean atomicBoolean2 = atomicBoolean;
            final AtomicInteger atomicInteger2 = atomicInteger;
            final TaskCompletionSource taskCompletionSource2 = taskCompletionSource;
            continueWith.continueWith(new Continuation<Object, Void>() {
                public final Void then(Task<Object> task) {
                    if (task.isFaulted()) {
                        synchronized (obj2) {
                            arrayList2.add(task.getError());
                        }
                    }
                    if (task.isCancelled()) {
                        atomicBoolean2.set(true);
                    }
                    if (atomicInteger2.decrementAndGet() == 0) {
                        if (arrayList2.size() != 0) {
                            if (arrayList2.size() == 1) {
                                taskCompletionSource2.setError((Exception) arrayList2.get(0));
                            } else {
                                taskCompletionSource2.setError(new AggregateException(String.format("There were %d exceptions.", Integer.valueOf(arrayList2.size())), arrayList2));
                            }
                        } else if (atomicBoolean2.get()) {
                            taskCompletionSource2.setCancelled();
                        } else {
                            taskCompletionSource2.setResult(null);
                        }
                    }
                    return null;
                }
            });
        }
        return taskCompletionSource.getTask();
    }

    public Task<Void> continueWhile(Callable<Boolean> callable, Continuation<Void, Task<Void>> continuation) {
        return continueWhile(callable, continuation, f1249a, null);
    }

    public Task<Void> continueWhile(Callable<Boolean> callable, Continuation<Void, Task<Void>> continuation, CancellationToken cancellationToken) {
        return continueWhile(callable, continuation, f1249a, cancellationToken);
    }

    public Task<Void> continueWhile(Callable<Boolean> callable, Continuation<Void, Task<Void>> continuation, Executor executor) {
        return continueWhile(callable, continuation, executor, null);
    }

    public Task<Void> continueWhile(Callable<Boolean> callable, Continuation<Void, Task<Void>> continuation, Executor executor, CancellationToken cancellationToken) {
        Capture capture = new Capture();
        final CancellationToken cancellationToken2 = cancellationToken;
        final Callable<Boolean> callable2 = callable;
        final Continuation<Void, Task<Void>> continuation2 = continuation;
        final Executor executor2 = executor;
        final Capture capture2 = capture;
        capture.set(new Continuation<Void, Task<Void>>() {
            public Task<Void> then(Task<Void> task) throws Exception {
                CancellationToken cancellationToken = cancellationToken2;
                if (cancellationToken != null && cancellationToken.isCancellationRequested()) {
                    return Task.cancelled();
                }
                if (((Boolean) callable2.call()).booleanValue()) {
                    return Task.forResult(null).onSuccessTask(continuation2, executor2).onSuccessTask((Continuation) capture2.get(), executor2);
                }
                return Task.forResult(null);
            }
        });
        return makeVoid().continueWithTask((Continuation) capture.get(), executor);
    }

    public <TContinuationResult> Task<TContinuationResult> continueWith(Continuation<TResult, TContinuationResult> continuation, Executor executor) {
        return continueWith(continuation, executor, null);
    }

    public <TContinuationResult> Task<TContinuationResult> continueWith(Continuation<TResult, TContinuationResult> continuation, Executor executor, CancellationToken cancellationToken) {
        boolean isCompleted;
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        synchronized (this.c) {
            isCompleted = isCompleted();
            if (!isCompleted) {
                final TaskCompletionSource taskCompletionSource2 = taskCompletionSource;
                final Continuation<TResult, TContinuationResult> continuation2 = continuation;
                final Executor executor2 = executor;
                final CancellationToken cancellationToken2 = cancellationToken;
                this.j.add(new Continuation<TResult, Void>() {
                    public Void then(Task<TResult> task) {
                        Task.c(taskCompletionSource2, continuation2, task, executor2, cancellationToken2);
                        return null;
                    }
                });
            }
        }
        if (isCompleted) {
            c(taskCompletionSource, continuation, this, executor, cancellationToken);
        }
        return taskCompletionSource.getTask();
    }

    public <TContinuationResult> Task<TContinuationResult> continueWith(Continuation<TResult, TContinuationResult> continuation) {
        return continueWith(continuation, f1249a, null);
    }

    public <TContinuationResult> Task<TContinuationResult> continueWith(Continuation<TResult, TContinuationResult> continuation, CancellationToken cancellationToken) {
        return continueWith(continuation, f1249a, cancellationToken);
    }

    public <TContinuationResult> Task<TContinuationResult> continueWithTask(Continuation<TResult, Task<TContinuationResult>> continuation, Executor executor) {
        return continueWithTask(continuation, executor, null);
    }

    public <TContinuationResult> Task<TContinuationResult> continueWithTask(Continuation<TResult, Task<TContinuationResult>> continuation, Executor executor, CancellationToken cancellationToken) {
        boolean isCompleted;
        TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        synchronized (this.c) {
            isCompleted = isCompleted();
            if (!isCompleted) {
                final TaskCompletionSource taskCompletionSource2 = taskCompletionSource;
                final Continuation<TResult, Task<TContinuationResult>> continuation2 = continuation;
                final Executor executor2 = executor;
                final CancellationToken cancellationToken2 = cancellationToken;
                this.j.add(new Continuation<TResult, Void>() {
                    public Void then(Task<TResult> task) {
                        Task.d(taskCompletionSource2, continuation2, task, executor2, cancellationToken2);
                        return null;
                    }
                });
            }
        }
        if (isCompleted) {
            d(taskCompletionSource, continuation, this, executor, cancellationToken);
        }
        return taskCompletionSource.getTask();
    }

    public <TContinuationResult> Task<TContinuationResult> continueWithTask(Continuation<TResult, Task<TContinuationResult>> continuation) {
        return continueWithTask(continuation, f1249a, null);
    }

    public <TContinuationResult> Task<TContinuationResult> continueWithTask(Continuation<TResult, Task<TContinuationResult>> continuation, CancellationToken cancellationToken) {
        return continueWithTask(continuation, f1249a, cancellationToken);
    }

    public <TContinuationResult> Task<TContinuationResult> onSuccess(Continuation continuation, Executor executor) {
        return onSuccess(continuation, executor, null);
    }

    public <TContinuationResult> Task<TContinuationResult> onSuccess(final Continuation<TResult, TContinuationResult> continuation, Executor executor, final CancellationToken cancellationToken) {
        return continueWithTask(new Continuation<TResult, Task<TContinuationResult>>() {
            public Task<TContinuationResult> then(Task<TResult> task) {
                CancellationToken cancellationToken = cancellationToken;
                if (cancellationToken != null && cancellationToken.isCancellationRequested()) {
                    return Task.cancelled();
                }
                if (task.isFaulted()) {
                    return Task.forError(task.getError());
                }
                if (task.isCancelled()) {
                    return Task.cancelled();
                }
                return task.continueWith(continuation);
            }
        }, executor);
    }

    public <TContinuationResult> Task<TContinuationResult> onSuccess(Continuation<TResult, TContinuationResult> continuation) {
        return onSuccess(continuation, f1249a, null);
    }

    public <TContinuationResult> Task<TContinuationResult> onSuccess(Continuation continuation, CancellationToken cancellationToken) {
        return onSuccess(continuation, f1249a, cancellationToken);
    }

    public <TContinuationResult> Task<TContinuationResult> onSuccessTask(Continuation<TResult, Task<TContinuationResult>> continuation, Executor executor) {
        return onSuccessTask(continuation, executor, null);
    }

    public <TContinuationResult> Task<TContinuationResult> onSuccessTask(final Continuation<TResult, Task<TContinuationResult>> continuation, Executor executor, final CancellationToken cancellationToken) {
        return continueWithTask(new Continuation<TResult, Task<TContinuationResult>>() {
            public Task<TContinuationResult> then(Task<TResult> task) {
                CancellationToken cancellationToken = cancellationToken;
                if (cancellationToken != null && cancellationToken.isCancellationRequested()) {
                    return Task.cancelled();
                }
                if (task.isFaulted()) {
                    return Task.forError(task.getError());
                }
                if (task.isCancelled()) {
                    return Task.cancelled();
                }
                return task.continueWithTask(continuation);
            }
        }, executor);
    }

    public <TContinuationResult> Task<TContinuationResult> onSuccessTask(Continuation<TResult, Task<TContinuationResult>> continuation) {
        return onSuccessTask(continuation, f1249a);
    }

    public <TContinuationResult> Task<TContinuationResult> onSuccessTask(Continuation<TResult, Task<TContinuationResult>> continuation, CancellationToken cancellationToken) {
        return onSuccessTask(continuation, f1249a, cancellationToken);
    }

    /* access modifiers changed from: private */
    public static <TContinuationResult, TResult> void c(final TaskCompletionSource<TContinuationResult> taskCompletionSource, final Continuation<TResult, TContinuationResult> continuation, final Task<TResult> task, Executor executor, final CancellationToken cancellationToken) {
        try {
            executor.execute(new Runnable() {
                public final void run() {
                    CancellationToken cancellationToken = CancellationToken.this;
                    if (cancellationToken == null || !cancellationToken.isCancellationRequested()) {
                        try {
                            taskCompletionSource.setResult(continuation.then(task));
                        } catch (CancellationException unused) {
                            taskCompletionSource.setCancelled();
                        } catch (Exception e) {
                            taskCompletionSource.setError(e);
                        }
                    } else {
                        taskCompletionSource.setCancelled();
                    }
                }
            });
        } catch (Exception e2) {
            taskCompletionSource.setError(new ExecutorException(e2));
        }
    }

    /* access modifiers changed from: private */
    public static <TContinuationResult, TResult> void d(final TaskCompletionSource<TContinuationResult> taskCompletionSource, final Continuation<TResult, Task<TContinuationResult>> continuation, final Task<TResult> task, Executor executor, final CancellationToken cancellationToken) {
        try {
            executor.execute(new Runnable() {
                public final void run() {
                    CancellationToken cancellationToken = CancellationToken.this;
                    if (cancellationToken == null || !cancellationToken.isCancellationRequested()) {
                        try {
                            Task task = (Task) continuation.then(task);
                            if (task == null) {
                                taskCompletionSource.setResult(null);
                            } else {
                                task.continueWith(new Continuation<TContinuationResult, Void>() {
                                    public Void then(Task<TContinuationResult> task) {
                                        if (CancellationToken.this == null || !CancellationToken.this.isCancellationRequested()) {
                                            if (task.isCancelled()) {
                                                taskCompletionSource.setCancelled();
                                            } else if (task.isFaulted()) {
                                                taskCompletionSource.setError(task.getError());
                                            } else {
                                                taskCompletionSource.setResult(task.getResult());
                                            }
                                            return null;
                                        }
                                        taskCompletionSource.setCancelled();
                                        return null;
                                    }
                                });
                            }
                        } catch (CancellationException unused) {
                            taskCompletionSource.setCancelled();
                        } catch (Exception e) {
                            taskCompletionSource.setError(e);
                        }
                    } else {
                        taskCompletionSource.setCancelled();
                    }
                }
            });
        } catch (Exception e2) {
            taskCompletionSource.setError(new ExecutorException(e2));
        }
    }

    private void b() {
        synchronized (this.c) {
            for (Continuation then : this.j) {
                try {
                    then.then(this);
                } catch (RuntimeException e2) {
                    throw e2;
                } catch (Exception e3) {
                    throw new RuntimeException(e3);
                }
            }
            this.j = null;
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean a() {
        synchronized (this.c) {
            if (this.d) {
                return false;
            }
            this.d = true;
            this.e = true;
            this.c.notifyAll();
            b();
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean a(TResult tresult) {
        synchronized (this.c) {
            if (this.d) {
                return false;
            }
            this.d = true;
            this.f = tresult;
            this.c.notifyAll();
            b();
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002b, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(java.lang.Exception r4) {
        /*
            r3 = this;
            java.lang.Object r0 = r3.c
            monitor-enter(r0)
            boolean r1 = r3.d     // Catch:{ all -> 0x002c }
            r2 = 0
            if (r1 == 0) goto L_0x000a
            monitor-exit(r0)     // Catch:{ all -> 0x002c }
            return r2
        L_0x000a:
            r1 = 1
            r3.d = r1     // Catch:{ all -> 0x002c }
            r3.g = r4     // Catch:{ all -> 0x002c }
            r3.h = r2     // Catch:{ all -> 0x002c }
            java.lang.Object r4 = r3.c     // Catch:{ all -> 0x002c }
            r4.notifyAll()     // Catch:{ all -> 0x002c }
            r3.b()     // Catch:{ all -> 0x002c }
            boolean r4 = r3.h     // Catch:{ all -> 0x002c }
            if (r4 != 0) goto L_0x002a
            bolts.Task$UnobservedExceptionHandler r4 = getUnobservedExceptionHandler()     // Catch:{ all -> 0x002c }
            if (r4 == 0) goto L_0x002a
            bolts.UnobservedErrorNotifier r4 = new bolts.UnobservedErrorNotifier     // Catch:{ all -> 0x002c }
            r4.<init>(r3)     // Catch:{ all -> 0x002c }
            r3.i = r4     // Catch:{ all -> 0x002c }
        L_0x002a:
            monitor-exit(r0)     // Catch:{ all -> 0x002c }
            return r1
        L_0x002c:
            r4 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x002c }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: bolts.Task.a(java.lang.Exception):boolean");
    }

    public class TaskCompletionSource extends TaskCompletionSource<TResult> {
        TaskCompletionSource() {
        }
    }
}
