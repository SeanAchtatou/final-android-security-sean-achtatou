package bolts;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

public class CancellationTokenSource implements Closeable {
    /* access modifiers changed from: package-private */

    /* renamed from: a  reason: collision with root package name */
    public final Object f1243a = new Object();

    /* renamed from: b  reason: collision with root package name */
    final List<CancellationTokenRegistration> f1244b = new ArrayList();
    boolean c;
    private final ScheduledExecutorService d = BoltsExecutors.a();
    /* access modifiers changed from: private */
    public ScheduledFuture<?> e;
    private boolean f;

    public boolean isCancellationRequested() {
        boolean z;
        synchronized (this.f1243a) {
            a();
            z = this.c;
        }
        return z;
    }

    public CancellationToken getToken() {
        CancellationToken cancellationToken;
        synchronized (this.f1243a) {
            a();
            cancellationToken = new CancellationToken(this);
        }
        return cancellationToken;
    }

    public void cancel() {
        synchronized (this.f1243a) {
            a();
            if (!this.c) {
                b();
                this.c = true;
                ArrayList arrayList = new ArrayList(this.f1244b);
                a(arrayList);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void cancelAfter(long r7) {
        /*
            r6 = this;
            java.util.concurrent.TimeUnit r0 = java.util.concurrent.TimeUnit.MILLISECONDS
            r1 = -1
            int r3 = (r7 > r1 ? 1 : (r7 == r1 ? 0 : -1))
            if (r3 < 0) goto L_0x0034
            r3 = 0
            int r5 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            if (r5 != 0) goto L_0x0012
            r6.cancel()
            goto L_0x0030
        L_0x0012:
            java.lang.Object r3 = r6.f1243a
            monitor-enter(r3)
            boolean r4 = r6.c     // Catch:{ all -> 0x0031 }
            if (r4 == 0) goto L_0x001b
            monitor-exit(r3)     // Catch:{ all -> 0x0031 }
            goto L_0x0030
        L_0x001b:
            r6.b()     // Catch:{ all -> 0x0031 }
            int r4 = (r7 > r1 ? 1 : (r7 == r1 ? 0 : -1))
            if (r4 == 0) goto L_0x002f
            java.util.concurrent.ScheduledExecutorService r1 = r6.d     // Catch:{ all -> 0x0031 }
            bolts.CancellationTokenSource$1 r2 = new bolts.CancellationTokenSource$1     // Catch:{ all -> 0x0031 }
            r2.<init>()     // Catch:{ all -> 0x0031 }
            java.util.concurrent.ScheduledFuture r7 = r1.schedule(r2, r7, r0)     // Catch:{ all -> 0x0031 }
            r6.e = r7     // Catch:{ all -> 0x0031 }
        L_0x002f:
            monitor-exit(r3)     // Catch:{ all -> 0x0031 }
        L_0x0030:
            return
        L_0x0031:
            r7 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0031 }
            throw r7
        L_0x0034:
            java.lang.IllegalArgumentException r7 = new java.lang.IllegalArgumentException
            java.lang.String r8 = "Delay must be >= -1"
            r7.<init>(r8)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: bolts.CancellationTokenSource.cancelAfter(long):void");
    }

    public void close() {
        synchronized (this.f1243a) {
            if (!this.f) {
                b();
                for (CancellationTokenRegistration close : this.f1244b) {
                    close.close();
                }
                this.f1244b.clear();
                this.f = true;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final CancellationTokenRegistration a(Runnable runnable) {
        CancellationTokenRegistration cancellationTokenRegistration;
        synchronized (this.f1243a) {
            a();
            cancellationTokenRegistration = new CancellationTokenRegistration(this, runnable);
            if (this.c) {
                cancellationTokenRegistration.a();
            } else {
                this.f1244b.add(cancellationTokenRegistration);
            }
        }
        return cancellationTokenRegistration;
    }

    private static void a(List<CancellationTokenRegistration> list) {
        for (CancellationTokenRegistration a2 : list) {
            a2.a();
        }
    }

    public String toString() {
        return String.format(Locale.US, "%s@%s[cancellationRequested=%s]", getClass().getName(), Integer.toHexString(hashCode()), Boolean.toString(isCancellationRequested()));
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.f) {
            throw new IllegalStateException("Object already closed");
        }
    }

    private void b() {
        ScheduledFuture<?> scheduledFuture = this.e;
        if (scheduledFuture != null) {
            scheduledFuture.cancel(true);
            this.e = null;
        }
    }
}
