package bolts;

import bolts.Task;

class UnobservedErrorNotifier {

    /* renamed from: a  reason: collision with root package name */
    private Task<?> f1281a;

    public UnobservedErrorNotifier(Task<?> task) {
        this.f1281a = task;
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        Task.UnobservedExceptionHandler unobservedExceptionHandler;
        try {
            Task<?> task = this.f1281a;
            if (!(task == null || (unobservedExceptionHandler = Task.getUnobservedExceptionHandler()) == null)) {
                unobservedExceptionHandler.unobservedException(task, new UnobservedTaskException(task.getError()));
            }
        } finally {
            super.finalize();
        }
    }

    public void setObserved() {
        this.f1281a = null;
    }
}
