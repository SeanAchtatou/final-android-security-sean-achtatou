package bolts;

import java.util.Locale;
import java.util.concurrent.CancellationException;

public class CancellationToken {

    /* renamed from: a  reason: collision with root package name */
    private final CancellationTokenSource f1240a;

    CancellationToken(CancellationTokenSource cancellationTokenSource) {
        this.f1240a = cancellationTokenSource;
    }

    public boolean isCancellationRequested() {
        return this.f1240a.isCancellationRequested();
    }

    public CancellationTokenRegistration register(Runnable runnable) {
        return this.f1240a.a(runnable);
    }

    public void throwIfCancellationRequested() throws CancellationException {
        CancellationTokenSource cancellationTokenSource = this.f1240a;
        synchronized (cancellationTokenSource.f1243a) {
            cancellationTokenSource.a();
            if (cancellationTokenSource.c) {
                throw new CancellationException();
            }
        }
    }

    public String toString() {
        return String.format(Locale.US, "%s@%s[cancellationRequested=%s]", getClass().getName(), Integer.toHexString(hashCode()), Boolean.toString(this.f1240a.isCancellationRequested()));
    }
}
