package bolts;

public class Capture<T> {

    /* renamed from: a  reason: collision with root package name */
    private T f1246a;

    public Capture() {
    }

    public Capture(T t) {
        this.f1246a = t;
    }

    public T get() {
        return this.f1246a;
    }

    public void set(T t) {
        this.f1246a = t;
    }
}
