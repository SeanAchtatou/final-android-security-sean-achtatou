package bolts;

import com.facebook.appevents.codeless.internal.Constants;
import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

final class BoltsExecutors {

    /* renamed from: a  reason: collision with root package name */
    private static final BoltsExecutors f1237a = new BoltsExecutors();

    /* renamed from: b  reason: collision with root package name */
    private final ExecutorService f1238b;
    private final ScheduledExecutorService c;
    private final Executor d;

    private BoltsExecutors() {
        boolean z;
        String property = System.getProperty("java.runtime.name");
        if (property == null) {
            z = false;
        } else {
            z = property.toLowerCase(Locale.US).contains(Constants.PLATFORM);
        }
        this.f1238b = !z ? Executors.newCachedThreadPool() : AndroidExecutors.newCachedThreadPool();
        this.c = Executors.newSingleThreadScheduledExecutor();
        this.d = new ImmediateExecutor((byte) 0);
    }

    public static ExecutorService background() {
        return f1237a.f1238b;
    }

    static ScheduledExecutorService a() {
        return f1237a.c;
    }

    static Executor b() {
        return f1237a.d;
    }

    static class ImmediateExecutor implements Executor {

        /* renamed from: a  reason: collision with root package name */
        private ThreadLocal<Integer> f1239a;

        private ImmediateExecutor() {
            this.f1239a = new ThreadLocal<>();
        }

        /* synthetic */ ImmediateExecutor(byte b2) {
            this();
        }

        private int a() {
            Integer num = this.f1239a.get();
            if (num == null) {
                num = 0;
            }
            int intValue = num.intValue() - 1;
            if (intValue == 0) {
                this.f1239a.remove();
            } else {
                this.f1239a.set(Integer.valueOf(intValue));
            }
            return intValue;
        }

        public void execute(Runnable runnable) {
            Integer num = this.f1239a.get();
            if (num == null) {
                num = 0;
            }
            int intValue = num.intValue() + 1;
            this.f1239a.set(Integer.valueOf(intValue));
            if (intValue <= 15) {
                try {
                    runnable.run();
                } catch (Throwable th) {
                    a();
                    throw th;
                }
            } else {
                BoltsExecutors.background().execute(runnable);
            }
            a();
        }
    }
}
