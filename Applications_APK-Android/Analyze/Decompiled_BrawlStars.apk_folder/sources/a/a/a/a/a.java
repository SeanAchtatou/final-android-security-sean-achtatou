package a.a.a.a;

import android.database.DataSetObserver;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import kotlin.d.b.j;

public class a extends PagerAdapter {

    /* renamed from: a  reason: collision with root package name */
    public final PagerAdapter f1a;

    /* renamed from: a.a.a.a.a$a  reason: collision with other inner class name */
    public static final class C0000a extends DataSetObserver {

        /* renamed from: a  reason: collision with root package name */
        public final a f2a;

        public C0000a(a aVar) {
            this.f2a = aVar;
        }

        public final void onChanged() {
            a aVar = this.f2a;
            if (aVar != null) {
                a.super.notifyDataSetChanged();
            }
        }

        public final void onInvalidated() {
            onChanged();
        }
    }

    public a(PagerAdapter pagerAdapter) {
        j.b(pagerAdapter, "delegate");
        this.f1a = pagerAdapter;
        this.f1a.registerDataSetObserver(new C0000a(this));
    }

    public void destroyItem(View view, int i, Object obj) {
        j.b(view, "container");
        j.b(obj, "object");
        this.f1a.destroyItem(view, i, obj);
    }

    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        j.b(viewGroup, "container");
        j.b(obj, "object");
        this.f1a.destroyItem(viewGroup, i, obj);
    }

    public void finishUpdate(View view) {
        j.b(view, "container");
        this.f1a.finishUpdate(view);
    }

    public void finishUpdate(ViewGroup viewGroup) {
        j.b(viewGroup, "container");
        this.f1a.finishUpdate(viewGroup);
    }

    public int getCount() {
        return this.f1a.getCount();
    }

    public int getItemPosition(Object obj) {
        j.b(obj, "object");
        return this.f1a.getItemPosition(obj);
    }

    public Object instantiateItem(View view, int i) {
        j.b(view, "container");
        Object instantiateItem = this.f1a.instantiateItem(view, i);
        j.a(instantiateItem, "delegate.instantiateItem(container, position)");
        return instantiateItem;
    }

    public Object instantiateItem(ViewGroup viewGroup, int i) {
        j.b(viewGroup, "container");
        Object instantiateItem = this.f1a.instantiateItem(viewGroup, i);
        j.a(instantiateItem, "delegate.instantiateItem(container, position)");
        return instantiateItem;
    }

    public boolean isViewFromObject(View view, Object obj) {
        j.b(view, "view");
        j.b(obj, "object");
        return this.f1a.isViewFromObject(view, obj);
    }

    public void notifyDataSetChanged() {
        this.f1a.notifyDataSetChanged();
    }

    public void registerDataSetObserver(DataSetObserver dataSetObserver) {
        j.b(dataSetObserver, "observer");
        this.f1a.registerDataSetObserver(dataSetObserver);
    }

    public void restoreState(Parcelable parcelable, ClassLoader classLoader) {
        this.f1a.restoreState(parcelable, classLoader);
    }

    public Parcelable saveState() {
        return this.f1a.saveState();
    }

    public void setPrimaryItem(View view, int i, Object obj) {
        j.b(view, "container");
        j.b(obj, "object");
        this.f1a.setPrimaryItem(view, i, obj);
    }

    public void setPrimaryItem(ViewGroup viewGroup, int i, Object obj) {
        j.b(viewGroup, "container");
        j.b(obj, "object");
        this.f1a.setPrimaryItem(viewGroup, i, obj);
    }

    public void startUpdate(View view) {
        j.b(view, "container");
        this.f1a.startUpdate(view);
    }

    public void startUpdate(ViewGroup viewGroup) {
        j.b(viewGroup, "container");
        this.f1a.startUpdate(viewGroup);
    }

    public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {
        j.b(dataSetObserver, "observer");
        this.f1a.unregisterDataSetObserver(dataSetObserver);
    }
}
