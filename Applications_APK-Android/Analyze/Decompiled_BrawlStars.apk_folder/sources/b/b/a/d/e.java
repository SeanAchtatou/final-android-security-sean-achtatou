package b.b.a.d;

import b.b.a.h.c;
import b.b.a.h.d;
import b.b.a.h.f;
import b.b.a.j.m;
import com.facebook.internal.NativeProtocol;
import com.facebook.internal.ServerProtocol;
import com.facebook.share.internal.ShareConstants;
import com.supercell.id.IdFriend;
import com.supercell.id.IdIngameFriend;
import com.supercell.id.SupercellId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.a.ai;
import nl.komponents.kovenant.bb;
import nl.komponents.kovenant.bc;
import nl.komponents.kovenant.bw;
import org.json.JSONArray;
import org.json.JSONObject;

public class e extends d {

    public final /* synthetic */ class a extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, JSONObject> {
        public a(e eVar) {
            super(1, eVar);
        }

        public final String getName() {
            return "handleResponseData";
        }

        public final kotlin.h.d getOwner() {
            return kotlin.d.b.t.a(e.class);
        }

        public final String getSignature() {
            return "handleResponseData(Lorg/json/JSONObject;)Lorg/json/JSONObject;";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return ((e) this.receiver).b(jSONObject);
        }
    }

    public final class a0 extends kotlin.d.b.k implements kotlin.d.a.b<kotlin.h<? extends JSONObject, ? extends List<? extends String>>, b.b.a.h.l> {

        /* renamed from: a  reason: collision with root package name */
        public static final a0 f43a = new a0();

        public a0() {
            super(1);
        }

        public final Object invoke(Object obj) {
            kotlin.h hVar = (kotlin.h) obj;
            kotlin.d.b.j.b(hVar, "it");
            return new b.b.a.h.l(new b.b.a.h.h((JSONObject) hVar.f5262a), (List) hVar.f5263b);
        }
    }

    public final /* synthetic */ class b0 extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, JSONObject> {
        public b0(e eVar) {
            super(1, eVar);
        }

        public final String getName() {
            return "handleResponse";
        }

        public final kotlin.h.d getOwner() {
            return kotlin.d.b.t.a(e.class);
        }

        public final String getSignature() {
            return "handleResponse(Lorg/json/JSONObject;)Lorg/json/JSONObject;";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return ((e) this.receiver).a(jSONObject);
        }
    }

    public final /* synthetic */ class c extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, JSONObject> {
        public c(e eVar) {
            super(1, eVar);
        }

        public final String getName() {
            return "handleResponseData";
        }

        public final kotlin.h.d getOwner() {
            return kotlin.d.b.t.a(e.class);
        }

        public final String getSignature() {
            return "handleResponseData(Lorg/json/JSONObject;)Lorg/json/JSONObject;";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return ((e) this.receiver).b(jSONObject);
        }
    }

    public final class c0 extends kotlin.d.b.k implements kotlin.d.a.b<JSONObject, Boolean> {

        /* renamed from: a  reason: collision with root package name */
        public static final c0 f45a = new c0();

        public c0() {
            super(1);
        }

        public final Object invoke(Object obj) {
            kotlin.d.b.j.b((JSONObject) obj, "it");
            return true;
        }
    }

    /* renamed from: b.b.a.d.e$e  reason: collision with other inner class name */
    public final /* synthetic */ class C0003e extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, JSONObject> {
        public C0003e(e eVar) {
            super(1, eVar);
        }

        public final String getName() {
            return "handleResponseData";
        }

        public final kotlin.h.d getOwner() {
            return kotlin.d.b.t.a(e.class);
        }

        public final String getSignature() {
            return "handleResponseData(Lorg/json/JSONObject;)Lorg/json/JSONObject;";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return ((e) this.receiver).b(jSONObject);
        }
    }

    public final class f extends kotlin.d.b.k implements kotlin.d.a.b<JSONObject, JSONArray> {

        /* renamed from: a  reason: collision with root package name */
        public static final f f47a = new f();

        public f() {
            super(1);
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "it");
            return jSONObject.getJSONArray(NativeProtocol.AUDIENCE_FRIENDS);
        }
    }

    public final /* synthetic */ class g extends kotlin.d.b.h implements kotlin.d.a.b<JSONArray, b.b.a.h.d> {
        public g(d.a aVar) {
            super(1, aVar);
        }

        public final String getName() {
            return "parse";
        }

        public final kotlin.h.d getOwner() {
            return kotlin.d.b.t.a(d.a.class);
        }

        public final String getSignature() {
            return "parse(Lorg/json/JSONArray;)Lcom/supercell/id/model/IdFriends;";
        }

        public final Object invoke(Object obj) {
            JSONArray jSONArray = (JSONArray) obj;
            kotlin.d.b.j.b(jSONArray, "p1");
            return ((d.a) this.receiver).a(jSONArray);
        }
    }

    public final /* synthetic */ class h extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, JSONObject> {
        public h(e eVar) {
            super(1, eVar);
        }

        public final String getName() {
            return "handleResponseData";
        }

        public final kotlin.h.d getOwner() {
            return kotlin.d.b.t.a(e.class);
        }

        public final String getSignature() {
            return "handleResponseData(Lorg/json/JSONObject;)Lorg/json/JSONObject;";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return ((e) this.receiver).b(jSONObject);
        }
    }

    public final class i extends kotlin.d.b.k implements kotlin.d.a.b<JSONObject, JSONArray> {

        /* renamed from: a  reason: collision with root package name */
        public static final i f48a = new i();

        public i() {
            super(1);
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "it");
            return jSONObject.getJSONArray(NativeProtocol.AUDIENCE_FRIENDS);
        }
    }

    public final /* synthetic */ class j extends kotlin.d.b.h implements kotlin.d.a.b<JSONArray, List<? extends b.b.a.h.c>> {
        public j(c.a aVar) {
            super(1, aVar);
        }

        public final String getName() {
            return "parse";
        }

        public final kotlin.h.d getOwner() {
            return kotlin.d.b.t.a(c.a.class);
        }

        public final String getSignature() {
            return "parse(Lorg/json/JSONArray;)Ljava/util/List;";
        }

        public final Object invoke(Object obj) {
            JSONArray jSONArray = (JSONArray) obj;
            kotlin.d.b.j.b(jSONArray, "p1");
            return ((c.a) this.receiver).a(jSONArray);
        }
    }

    public final /* synthetic */ class k extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, JSONObject> {
        public k(e eVar) {
            super(1, eVar);
        }

        public final String getName() {
            return "handleResponseData";
        }

        public final kotlin.h.d getOwner() {
            return kotlin.d.b.t.a(e.class);
        }

        public final String getSignature() {
            return "handleResponseData(Lorg/json/JSONObject;)Lorg/json/JSONObject;";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return ((e) this.receiver).b(jSONObject);
        }
    }

    public final /* synthetic */ class m extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, JSONObject> {
        public m(e eVar) {
            super(1, eVar);
        }

        public final String getName() {
            return "handleResponseData";
        }

        public final kotlin.h.d getOwner() {
            return kotlin.d.b.t.a(e.class);
        }

        public final String getSignature() {
            return "handleResponseData(Lorg/json/JSONObject;)Lorg/json/JSONObject;";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return ((e) this.receiver).b(jSONObject);
        }
    }

    public final /* synthetic */ class n extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, b.b.a.h.h> {

        /* renamed from: a  reason: collision with root package name */
        public static final n f50a = new n();

        public n() {
            super(1);
        }

        public final String getName() {
            return "<init>";
        }

        public final kotlin.h.d getOwner() {
            return kotlin.d.b.t.a(b.b.a.h.h.class);
        }

        public final String getSignature() {
            return "<init>(Lorg/json/JSONObject;)V";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return new b.b.a.h.h(jSONObject);
        }
    }

    public final /* synthetic */ class o extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, JSONObject> {
        public o(e eVar) {
            super(1, eVar);
        }

        public final String getName() {
            return "handleResponseData";
        }

        public final kotlin.h.d getOwner() {
            return kotlin.d.b.t.a(e.class);
        }

        public final String getSignature() {
            return "handleResponseData(Lorg/json/JSONObject;)Lorg/json/JSONObject;";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return ((e) this.receiver).b(jSONObject);
        }
    }

    public final /* synthetic */ class p extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, b.b.a.h.i> {

        /* renamed from: a  reason: collision with root package name */
        public static final p f51a = new p();

        public p() {
            super(1);
        }

        public final String getName() {
            return "<init>";
        }

        public final kotlin.h.d getOwner() {
            return kotlin.d.b.t.a(b.b.a.h.i.class);
        }

        public final String getSignature() {
            return "<init>(Lorg/json/JSONObject;)V";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return new b.b.a.h.i(jSONObject);
        }
    }

    public final /* synthetic */ class q extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, JSONObject> {
        public q(e eVar) {
            super(1, eVar);
        }

        public final String getName() {
            return "handleResponseData";
        }

        public final kotlin.h.d getOwner() {
            return kotlin.d.b.t.a(e.class);
        }

        public final String getSignature() {
            return "handleResponseData(Lorg/json/JSONObject;)Lorg/json/JSONObject;";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return ((e) this.receiver).b(jSONObject);
        }
    }

    public final /* synthetic */ class r extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, b.b.a.h.i> {

        /* renamed from: a  reason: collision with root package name */
        public static final r f52a = new r();

        public r() {
            super(1);
        }

        public final String getName() {
            return "<init>";
        }

        public final kotlin.h.d getOwner() {
            return kotlin.d.b.t.a(b.b.a.h.i.class);
        }

        public final String getSignature() {
            return "<init>(Lorg/json/JSONObject;)V";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return new b.b.a.h.i(jSONObject);
        }
    }

    public final /* synthetic */ class s extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, JSONObject> {
        public s(e eVar) {
            super(1, eVar);
        }

        public final String getName() {
            return "handleResponseData";
        }

        public final kotlin.h.d getOwner() {
            return kotlin.d.b.t.a(e.class);
        }

        public final String getSignature() {
            return "handleResponseData(Lorg/json/JSONObject;)Lorg/json/JSONObject;";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return ((e) this.receiver).b(jSONObject);
        }
    }

    public final class t extends kotlin.d.b.k implements kotlin.d.a.b<JSONObject, JSONArray> {

        /* renamed from: a  reason: collision with root package name */
        public static final t f53a = new t();

        public t() {
            super(1);
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "it");
            return jSONObject.getJSONArray(NativeProtocol.AUDIENCE_FRIENDS);
        }
    }

    public final /* synthetic */ class u extends kotlin.d.b.h implements kotlin.d.a.b<JSONArray, List<? extends IdFriend>> {
        public u(IdFriend.a aVar) {
            super(1, aVar);
        }

        public final String getName() {
            return "parseFriends";
        }

        public final kotlin.h.d getOwner() {
            return kotlin.d.b.t.a(IdFriend.a.class);
        }

        public final String getSignature() {
            return "parseFriends(Lorg/json/JSONArray;)Ljava/util/List;";
        }

        public final Object invoke(Object obj) {
            JSONArray jSONArray = (JSONArray) obj;
            kotlin.d.b.j.b(jSONArray, "p1");
            return IdFriend.a.a(jSONArray);
        }
    }

    public final /* synthetic */ class v extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, JSONObject> {
        public v(e eVar) {
            super(1, eVar);
        }

        public final String getName() {
            return "handleResponse";
        }

        public final kotlin.h.d getOwner() {
            return kotlin.d.b.t.a(e.class);
        }

        public final String getSignature() {
            return "handleResponse(Lorg/json/JSONObject;)Lorg/json/JSONObject;";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return ((e) this.receiver).a(jSONObject);
        }
    }

    public final class w extends kotlin.d.b.k implements kotlin.d.a.b<JSONObject, Boolean> {

        /* renamed from: a  reason: collision with root package name */
        public static final w f54a = new w();

        public w() {
            super(1);
        }

        public final Object invoke(Object obj) {
            kotlin.d.b.j.b((JSONObject) obj, "it");
            return true;
        }
    }

    public final /* synthetic */ class x extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, JSONObject> {
        public x(e eVar) {
            super(1, eVar);
        }

        public final String getName() {
            return "handleResponse";
        }

        public final kotlin.h.d getOwner() {
            return kotlin.d.b.t.a(e.class);
        }

        public final String getSignature() {
            return "handleResponse(Lorg/json/JSONObject;)Lorg/json/JSONObject;";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return ((e) this.receiver).a(jSONObject);
        }
    }

    public final class y extends kotlin.d.b.k implements kotlin.d.a.b<JSONObject, Boolean> {

        /* renamed from: a  reason: collision with root package name */
        public static final y f55a = new y();

        public y() {
            super(1);
        }

        public final Object invoke(Object obj) {
            kotlin.d.b.j.b((JSONObject) obj, "it");
            return true;
        }
    }

    public final /* synthetic */ class z extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, kotlin.h<? extends JSONObject, ? extends List<? extends String>>> {
        public z(e eVar) {
            super(1, eVar);
        }

        public final String getName() {
            return "handleResponseDataWithWarnings";
        }

        public final kotlin.h.d getOwner() {
            return kotlin.d.b.t.a(e.class);
        }

        public final String getSignature() {
            return "handleResponseDataWithWarnings(Lorg/json/JSONObject;)Lkotlin/Pair;";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return ((e) this.receiver).c(jSONObject);
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public e(String str, String str2) {
        super(str, str2);
        kotlin.d.b.j.b(str, "url");
    }

    public final bw<b.b.a.h.d, Exception> a() {
        return bc.a(bc.a(bc.a(d.a(this, "/api/friends.list", null, 2, null), new C0003e(this)), f.f47a), new g(b.b.a.h.d.d));
    }

    public final bw<Boolean, Exception> a(String str) {
        kotlin.d.b.j.b(str, "scid");
        return c(str, "/api/friends.cancelRequest");
    }

    public final bw<b.b.a.h.l, Exception> a(String str, String str2, Boolean bool) {
        kotlin.h[] hVarArr = new kotlin.h[3];
        hVarArr[0] = kotlin.k.a("profile_name", str);
        hVarArr[1] = kotlin.k.a("profile_avatar", str2);
        hVarArr[2] = kotlin.k.a("profile_forced_offline", bool != null ? bool.booleanValue() ? ServerProtocol.DIALOG_RETURN_SCOPES_TRUE : "false" : null);
        return bc.a(bc.a(a("/api/profile.set", b.b.a.b.b(hVarArr)), new z(this)), a0.f43a);
    }

    public final bw<Boolean, Exception> a(Collection<String> collection) {
        kotlin.d.b.j.b(collection, "scids");
        return bc.a(bc.a(a("/api/ingame.invitetoplay", ai.a(kotlin.k.a("scids", new JSONArray((Collection) collection).toString()))), new v(this)), w.f54a);
    }

    public final bw<Map<String, b.b.a.j.m<b.b.a.h.i, Exception>>, Exception> a(List<String> list) {
        kotlin.d.b.j.b(list, "scids");
        return bc.a(bc.a(a("/api/friends.acceptRequest", ai.a(kotlin.k.a("scids", new JSONArray((Collection) list).toString()))), new a(this)), new b());
    }

    public final bw<List<b.b.a.h.c>, Exception> b(String str) {
        kotlin.d.b.j.b(str, "scid");
        return bc.a(bc.a(bc.a(a("/api/friends.list", ai.a(kotlin.k.a("scid", str))), new h(this)), i.f48a), new j(b.b.a.h.c.g));
    }

    public final bw<Boolean, Exception> b(String str, String str2) {
        kotlin.d.b.j.b(str, "scid");
        kotlin.d.b.j.b(str2, "name");
        return bc.a(bc.a(a("/api/profile.report", ai.a(kotlin.k.a("scid", str), kotlin.k.a("profile_name", str2))), new x(this)), y.f55a);
    }

    public final bw<Map<String, b.b.a.j.m<Boolean, Exception>>, Exception> b(List<String> list) {
        kotlin.d.b.j.b(list, "scids");
        return bc.a(bc.a(a("/api/friends.createRequest", ai.a(kotlin.k.a("scids", new JSONArray((Collection) list).toString()))), new c(this)), new d());
    }

    public final bw<b.b.a.h.h, Exception> c() {
        return bc.a(bc.a(d.a(this, "/api/profile.get", null, 2, null), new m(this)), n.f50a);
    }

    public final bw<b.b.a.h.i, Exception> c(String str) {
        kotlin.d.b.j.b(str, "scid");
        return bc.a(bc.a(a("/api/profile.get", ai.a(kotlin.k.a("scid", str))), new o(this)), p.f51a);
    }

    public final bw<Boolean, Exception> c(String str, String str2) {
        return bc.a(bc.a(a(str2, ai.a(kotlin.k.a("scid", str))), new b0(this)), c0.f45a);
    }

    public final bw<List<IdFriend>, Exception> d() {
        return bc.a(bc.a(bc.a(a("/api/friends.list", ai.a(kotlin.k.a("format", "SIMPLE"))), new s(this)), t.f53a), new u(IdFriend.Companion));
    }

    public final bw<b.b.a.h.i, Exception> d(String str) {
        kotlin.d.b.j.b(str, "profileId");
        return bc.a(bc.a(a("/api/profile.get", ai.a(kotlin.k.a("profile_id", str))), new q(this)), r.f52a);
    }

    public final bw<Boolean, Exception> e(String str) {
        kotlin.d.b.j.b(str, "scid");
        return c(str, "/api/friends.rejectRequest");
    }

    public final bw<Boolean, Exception> f(String str) {
        kotlin.d.b.j.b(str, "scid");
        return c(str, "/api/friends.remove");
    }

    public final class b extends kotlin.d.b.k implements kotlin.d.a.b<JSONObject, Map<String, ? extends b.b.a.j.m<? extends b.b.a.h.i, ? extends Exception>>> {
        public b() {
            super(1);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [java.util.Iterator<java.lang.String>, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [org.json.JSONObject, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* renamed from: a */
        public final Map<String, b.b.a.j.m<b.b.a.h.i, Exception>> invoke(JSONObject jSONObject) {
            Object obj;
            kotlin.d.b.j.b(jSONObject, ShareConstants.WEB_DIALOG_PARAM_DATA);
            Iterator<String> keys = jSONObject.keys();
            kotlin.d.b.j.a((Object) keys, "data.keys()");
            kotlin.i.h a2 = kotlin.i.i.a(keys);
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            Iterator a3 = a2.a();
            while (a3.hasNext()) {
                String str = (String) a3.next();
                try {
                    JSONObject jSONObject2 = jSONObject.getJSONObject(str);
                    e eVar = e.this;
                    kotlin.d.b.j.a((Object) jSONObject2, "response");
                    obj = new m.a(new b.b.a.h.i(eVar.b(jSONObject2)));
                } catch (Exception e) {
                    obj = new m.b(e);
                }
                kotlin.h a4 = kotlin.k.a(str, obj);
                linkedHashMap.put(a4.f5262a, a4.f5263b);
            }
            return linkedHashMap;
        }
    }

    public final class d extends kotlin.d.b.k implements kotlin.d.a.b<JSONObject, Map<String, ? extends b.b.a.j.m<? extends Boolean, ? extends Exception>>> {
        public d() {
            super(1);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [java.util.Iterator<java.lang.String>, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [org.json.JSONObject, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* renamed from: a */
        public final Map<String, b.b.a.j.m<Boolean, Exception>> invoke(JSONObject jSONObject) {
            Object obj;
            kotlin.d.b.j.b(jSONObject, ShareConstants.WEB_DIALOG_PARAM_DATA);
            Iterator<String> keys = jSONObject.keys();
            kotlin.d.b.j.a((Object) keys, "data.keys()");
            kotlin.i.h a2 = kotlin.i.i.a(keys);
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            Iterator a3 = a2.a();
            while (a3.hasNext()) {
                String str = (String) a3.next();
                try {
                    JSONObject jSONObject2 = jSONObject.getJSONObject(str);
                    e eVar = e.this;
                    kotlin.d.b.j.a((Object) jSONObject2, "response");
                    eVar.a(jSONObject2);
                    obj = new m.a(true);
                } catch (Exception e) {
                    obj = new m.b(e);
                }
                kotlin.h a4 = kotlin.k.a(str, obj);
                linkedHashMap.put(a4.f5262a, a4.f5263b);
            }
            return linkedHashMap;
        }
    }

    public final class l extends kotlin.d.b.k implements kotlin.d.a.b<JSONObject, List<? extends b.b.a.h.f>> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ IdIngameFriend[] f49a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(IdIngameFriend[] idIngameFriendArr) {
            super(1);
            this.f49a = idIngameFriendArr;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [org.json.JSONArray, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* renamed from: a */
        public final List<b.b.a.h.f> invoke(JSONObject jSONObject) {
            kotlin.d.b.j.b(jSONObject, ShareConstants.WEB_DIALOG_PARAM_DATA);
            IdIngameFriend[] idIngameFriendArr = this.f49a;
            LinkedHashMap linkedHashMap = new LinkedHashMap(kotlin.g.d.c(ai.a(idIngameFriendArr.length), 16));
            for (IdIngameFriend idIngameFriend : idIngameFriendArr) {
                kotlin.h a2 = kotlin.k.a(idIngameFriend.getSupercellId(), idIngameFriend.getUsername());
                linkedHashMap.put(a2.f5262a, a2.f5263b);
            }
            f.a aVar = b.b.a.h.f.f;
            JSONArray jSONArray = jSONObject.getJSONArray("profiles");
            kotlin.d.b.j.a((Object) jSONArray, "data.getJSONArray(\"profiles\")");
            return aVar.a(jSONArray, linkedHashMap);
        }
    }

    public final bw<List<b.b.a.h.f>, Exception> b() {
        IdIngameFriend[] ingameFriends = SupercellId.INSTANCE.getIngameFriends();
        if (ingameFriends.length == 0) {
            return bb.f5389a;
        }
        ArrayList arrayList = new ArrayList(ingameFriends.length);
        for (IdIngameFriend supercellId : ingameFriends) {
            arrayList.add(supercellId.getSupercellId());
        }
        return bc.a(bc.a(a("/api/profiles.list", ai.a(kotlin.k.a("scids", new JSONArray((Collection) arrayList).toString()))), new k(this)), new l(ingameFriends));
    }
}
