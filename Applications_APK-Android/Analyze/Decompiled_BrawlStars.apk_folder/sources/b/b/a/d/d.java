package b.b.a.d;

import com.facebook.share.internal.MessengerShareContentUtility;
import com.facebook.share.internal.ShareConstants;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.a.ab;
import kotlin.a.ah;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.h;
import kotlin.j.t;
import kotlin.m;
import nl.komponents.kovenant.bb;
import nl.komponents.kovenant.bw;
import org.json.JSONArray;
import org.json.JSONObject;

public abstract class d {

    /* renamed from: a  reason: collision with root package name */
    public final String f37a;

    /* renamed from: b  reason: collision with root package name */
    public String f38b;
    public String c;

    public final class a extends k implements kotlin.d.a.a<JSONObject> {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ String f40b;
        public final /* synthetic */ Map c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(String str, Map map) {
            super(0);
            this.f40b = str;
            this.c = map;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
         arg types: [java.lang.String, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [byte[], java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [java.io.InputStream, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x00d9, code lost:
            r2 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x00da, code lost:
            kotlin.io.b.a(r1, r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x00dd, code lost:
            throw r2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:47:0x0158, code lost:
            r2 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:48:0x0159, code lost:
            kotlin.io.b.a(r0, r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:49:0x015c, code lost:
            throw r2;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final org.json.JSONObject invoke() {
            /*
                r7 = this;
                r0 = 10000(0x2710, float:1.4013E-41)
                android.net.TrafficStats.setThreadStatsTag(r0)
                java.net.URL r0 = new java.net.URL
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                b.b.a.d.d r2 = b.b.a.d.d.this
                java.lang.String r2 = r2.f38b
                if (r2 != 0) goto L_0x0017
                java.lang.String r3 = "baseUrl"
                kotlin.d.b.j.a(r3)
            L_0x0017:
                r1.append(r2)
                r2 = 47
                r1.append(r2)
                java.lang.String r3 = r7.f40b
                r4 = 1
                char[] r5 = new char[r4]
                r6 = 0
                r5[r6] = r2
                java.lang.String r2 = kotlin.j.t.a(r3, r5)
                r1.append(r2)
                java.lang.String r1 = r1.toString()
                r0.<init>(r1)
                java.net.URLConnection r0 = r0.openConnection()
                boolean r1 = r0 instanceof javax.net.ssl.HttpsURLConnection
                r2 = 0
                if (r1 != 0) goto L_0x003f
                r0 = r2
            L_0x003f:
                javax.net.ssl.HttpsURLConnection r0 = (javax.net.ssl.HttpsURLConnection) r0
                if (r0 == 0) goto L_0x015d
                java.lang.String r1 = "POST"
                r0.setRequestMethod(r1)
                r0.setDoOutput(r4)
                com.supercell.id.SupercellId r1 = com.supercell.id.SupercellId.INSTANCE
                b.b.a.j.x r1 = r1.getSharedServices$supercellId_release()
                b.b.a.i.x1.f r1 = r1.j
                java.lang.String r1 = r1.c()
                java.lang.String r3 = "Accept-Language"
                r0.setRequestProperty(r3, r1)
                b.b.a.d.d r1 = b.b.a.d.d.this
                java.lang.String r1 = r1.c
                if (r1 == 0) goto L_0x0068
                int r1 = r1.length()
                if (r1 != 0) goto L_0x0069
            L_0x0068:
                r6 = 1
            L_0x0069:
                if (r6 != 0) goto L_0x0090
                com.supercell.id.SupercellId r1 = com.supercell.id.SupercellId.INSTANCE
                java.lang.String r1 = r1.getForcedView$supercellId_release()
                java.lang.String r3 = "login"
                boolean r1 = kotlin.d.b.j.a(r1, r3)
                r1 = r1 ^ r4
                if (r1 == 0) goto L_0x0090
                java.lang.String r1 = "Bearer "
                java.lang.StringBuilder r1 = b.a.a.a.a.a(r1)
                b.b.a.d.d r3 = b.b.a.d.d.this
                java.lang.String r3 = r3.c
                r1.append(r3)
                java.lang.String r1 = r1.toString()
                java.lang.String r3 = "Authorization"
                r0.setRequestProperty(r3, r1)
            L_0x0090:
                java.util.Map r1 = r7.c
                if (r1 == 0) goto L_0x00e6
                b.b.a.d.d r3 = b.b.a.d.d.this
                java.lang.String r3 = r3.a(r1)
                java.nio.charset.Charset r4 = kotlin.j.d.f5307a
                if (r3 == 0) goto L_0x00de
                byte[] r3 = r3.getBytes(r4)
                java.lang.String r4 = "(this as java.lang.String).getBytes(charset)"
                kotlin.d.b.j.a(r3, r4)
                java.lang.String r4 = "Post "
                java.lang.StringBuilder r4 = b.a.a.a.a.a(r4)
                java.lang.String r5 = r7.f40b
                r4.append(r5)
                java.lang.String r5 = " params "
                r4.append(r5)
                b.b.a.d.d r5 = b.b.a.d.d.this
                java.lang.String r1 = r5.a(r1)
                r4.append(r1)
                r4.toString()
                int r1 = r3.length
                r0.setFixedLengthStreamingMode(r1)
                java.io.OutputStream r1 = r0.getOutputStream()
                r1.write(r3)     // Catch:{ all -> 0x00d7 }
                r1.flush()     // Catch:{ all -> 0x00d7 }
                kotlin.m r3 = kotlin.m.f5330a     // Catch:{ all -> 0x00d7 }
                kotlin.io.b.a(r1, r2)
                goto L_0x00e6
            L_0x00d7:
                r0 = move-exception
                throw r0     // Catch:{ all -> 0x00d9 }
            L_0x00d9:
                r2 = move-exception
                kotlin.io.b.a(r1, r0)
                throw r2
            L_0x00de:
                kotlin.TypeCastException r0 = new kotlin.TypeCastException
                java.lang.String r1 = "null cannot be cast to non-null type java.lang.String"
                r0.<init>(r1)
                throw r0
            L_0x00e6:
                int r1 = r0.getResponseCode()
                r3 = 200(0xc8, float:2.8E-43)
                if (r1 == r3) goto L_0x011c
                int r1 = r0.getResponseCode()
                r3 = 201(0xc9, float:2.82E-43)
                if (r1 != r3) goto L_0x00f7
                goto L_0x011c
            L_0x00f7:
                b.b.a.d.c r1 = new b.b.a.d.c
                java.lang.String r2 = "Wrong response code "
                java.lang.StringBuilder r2 = b.a.a.a.a.a(r2)
                java.lang.String r3 = r7.f40b
                r2.append(r3)
                r3 = 32
                r2.append(r3)
                int r3 = r0.getResponseCode()
                r2.append(r3)
                java.lang.String r2 = r2.toString()
                int r0 = r0.getResponseCode()
                r1.<init>(r2, r0)
                throw r1
            L_0x011c:
                java.io.InputStream r0 = r0.getInputStream()
                java.lang.String r1 = "connection.inputStream"
                kotlin.d.b.j.a(r0, r1)
                java.nio.charset.Charset r1 = kotlin.j.d.f5307a
                java.io.InputStreamReader r3 = new java.io.InputStreamReader
                r3.<init>(r0, r1)
                java.io.BufferedReader r0 = new java.io.BufferedReader
                r1 = 8192(0x2000, float:1.14794E-41)
                r0.<init>(r3, r1)
                java.lang.String r1 = kotlin.io.m.a(r0)     // Catch:{ all -> 0x0156 }
                kotlin.io.b.a(r0, r2)
                java.lang.String r0 = "From "
                java.lang.StringBuilder r0 = b.a.a.a.a.a(r0)
                java.lang.String r2 = r7.f40b
                r0.append(r2)
                java.lang.String r2 = " received "
                r0.append(r2)
                r0.append(r1)
                r0.toString()
                org.json.JSONObject r0 = new org.json.JSONObject
                r0.<init>(r1)
                return r0
            L_0x0156:
                r1 = move-exception
                throw r1     // Catch:{ all -> 0x0158 }
            L_0x0158:
                r2 = move-exception
                kotlin.io.b.a(r0, r1)
                throw r2
            L_0x015d:
                java.io.IOException r0 = new java.io.IOException
                java.lang.String r1 = "Cannot open connection"
                r0.<init>(r1)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: b.b.a.d.d.a.invoke():org.json.JSONObject");
        }
    }

    public final class b extends k implements kotlin.d.a.b<Exception, m> {
        public b() {
            super(1);
        }

        public final Object invoke(Object obj) {
            Exception exc = (Exception) obj;
            j.b(exc, "it");
            StringBuilder a2 = b.a.a.a.a.a("Error ");
            a2.append(exc.getLocalizedMessage());
            a2.toString();
            return m.f5330a;
        }
    }

    public final class c extends k implements kotlin.d.a.b<Map.Entry<? extends String, ? extends String>, String> {

        /* renamed from: a  reason: collision with root package name */
        public static final c f42a = new c();

        public c() {
            super(1);
        }

        public final Object invoke(Object obj) {
            Map.Entry entry = (Map.Entry) obj;
            j.b(entry, "entry");
            return URLEncoder.encode((String) entry.getKey(), "UTF-8") + '=' + URLEncoder.encode((String) entry.getValue(), "UTF-8");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public d(String str, String str2) {
        j.b(str, "url");
        String simpleName = getClass().getSimpleName();
        j.a((Object) simpleName, "this.javaClass.simpleName");
        this.f37a = simpleName;
        a(str, str2);
    }

    public static /* synthetic */ bw a(d dVar, String str, Map map, int i, Object obj) {
        if (obj == null) {
            if ((i & 2) != 0) {
                map = null;
            }
            return dVar.a(str, map);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: post");
    }

    public final String a(Map<String, String> map) {
        return kotlin.a.m.a(map.entrySet(), "&", (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, c.f42a, 30);
    }

    public bw<JSONObject, Exception> a(String str, Map<String, String> map) {
        j.b(str, "url");
        return bb.f5389a.b(new b());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final JSONObject a(JSONObject jSONObject) {
        j.b(jSONObject, "json");
        if (jSONObject.getBoolean("ok")) {
            return jSONObject;
        }
        String optString = jSONObject.optString("error", MessengerShareContentUtility.TEMPLATE_GENERIC_TYPE);
        j.a((Object) optString, "error");
        throw new b(optString);
    }

    public final void a(String str, String str2) {
        j.b(str, "url");
        this.f38b = t.b(str, '/');
        this.c = str2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [org.json.JSONObject, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final JSONObject b(JSONObject jSONObject) {
        j.b(jSONObject, "json");
        JSONObject jSONObject2 = a(jSONObject).getJSONObject(ShareConstants.WEB_DIALOG_PARAM_DATA);
        j.a((Object) jSONObject2, "handleResponse(json).getJSONObject(\"data\")");
        return jSONObject2;
    }

    public final h<JSONObject, List<String>> c(JSONObject jSONObject) {
        j.b(jSONObject, "json");
        return kotlin.k.a(b(jSONObject), d(jSONObject));
    }

    public final List<String> d(JSONObject jSONObject) {
        JSONArray optJSONArray = jSONObject.optJSONArray("warnings");
        if (optJSONArray == null) {
            return ab.f5210a;
        }
        kotlin.g.c b2 = kotlin.g.d.b(0, optJSONArray.length());
        ArrayList arrayList = new ArrayList();
        Iterator it = b2.iterator();
        while (it.hasNext()) {
            Object opt = optJSONArray.opt(((ah) it).a());
            String str = null;
            if (opt == null || j.a(opt, JSONObject.NULL)) {
                opt = null;
            }
            if (opt != null && (opt instanceof String)) {
                str = (String) opt;
            }
            if (str != null) {
                arrayList.add(str);
            }
        }
        return arrayList;
    }
}
