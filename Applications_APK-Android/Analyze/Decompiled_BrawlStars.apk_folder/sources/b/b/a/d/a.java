package b.b.a.d;

import android.support.v4.app.NotificationCompat;
import b.b.a.h.m;
import b.b.a.h.o;
import com.facebook.internal.ServerProtocol;
import com.facebook.places.model.PlaceFields;
import com.supercell.id.SupercellId;
import java.util.Map;
import kotlin.a.ai;
import kotlin.d.b.t;
import nl.komponents.kovenant.bb;
import nl.komponents.kovenant.bc;
import nl.komponents.kovenant.bw;
import org.json.JSONObject;

public class a extends d {

    /* renamed from: b.b.a.d.a$a  reason: collision with other inner class name */
    public final /* synthetic */ class C0002a extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, JSONObject> {
        public C0002a(a aVar) {
            super(1, aVar);
        }

        public final String getName() {
            return "handleResponse";
        }

        public final kotlin.h.d getOwner() {
            return t.a(a.class);
        }

        public final String getSignature() {
            return "handleResponse(Lorg/json/JSONObject;)Lorg/json/JSONObject;";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return ((a) this.receiver).a(jSONObject);
        }
    }

    public final class b extends kotlin.d.b.k implements kotlin.d.a.b<JSONObject, Boolean> {

        /* renamed from: a  reason: collision with root package name */
        public static final b f30a = new b();

        public b() {
            super(1);
        }

        public final Object invoke(Object obj) {
            kotlin.d.b.j.b((JSONObject) obj, "it");
            return true;
        }
    }

    public final /* synthetic */ class c extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, JSONObject> {
        public c(a aVar) {
            super(1, aVar);
        }

        public final String getName() {
            return "handleResponseData";
        }

        public final kotlin.h.d getOwner() {
            return t.a(a.class);
        }

        public final String getSignature() {
            return "handleResponseData(Lorg/json/JSONObject;)Lorg/json/JSONObject;";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return ((a) this.receiver).b(jSONObject);
        }
    }

    public final /* synthetic */ class d extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, o.a> {

        /* renamed from: a  reason: collision with root package name */
        public static final d f31a = new d();

        public d() {
            super(1);
        }

        public final String getName() {
            return "<init>";
        }

        public final kotlin.h.d getOwner() {
            return t.a(o.a.class);
        }

        public final String getSignature() {
            return "<init>(Lorg/json/JSONObject;)V";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return new o.a(jSONObject);
        }
    }

    public final /* synthetic */ class e extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, JSONObject> {
        public e(a aVar) {
            super(1, aVar);
        }

        public final String getName() {
            return "handleResponseData";
        }

        public final kotlin.h.d getOwner() {
            return t.a(a.class);
        }

        public final String getSignature() {
            return "handleResponseData(Lorg/json/JSONObject;)Lorg/json/JSONObject;";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return ((a) this.receiver).b(jSONObject);
        }
    }

    public final /* synthetic */ class f extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, b.b.a.h.e> {

        /* renamed from: a  reason: collision with root package name */
        public static final f f32a = new f();

        public f() {
            super(1);
        }

        public final String getName() {
            return "<init>";
        }

        public final kotlin.h.d getOwner() {
            return t.a(b.b.a.h.e.class);
        }

        public final String getSignature() {
            return "<init>(Lorg/json/JSONObject;)V";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return new b.b.a.h.e(jSONObject);
        }
    }

    public final /* synthetic */ class g extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, JSONObject> {
        public g(a aVar) {
            super(1, aVar);
        }

        public final String getName() {
            return "handleResponseData";
        }

        public final kotlin.h.d getOwner() {
            return t.a(a.class);
        }

        public final String getSignature() {
            return "handleResponseData(Lorg/json/JSONObject;)Lorg/json/JSONObject;";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return ((a) this.receiver).b(jSONObject);
        }
    }

    public final /* synthetic */ class h extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, b.b.a.h.m> {
        public h(m.a aVar) {
            super(1, aVar);
        }

        public final String getName() {
            return "parse";
        }

        public final kotlin.h.d getOwner() {
            return t.a(m.a.class);
        }

        public final String getSignature() {
            return "parse(Lorg/json/JSONObject;)Lcom/supercell/id/model/IdSubscriptions;";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return ((m.a) this.receiver).a(jSONObject);
        }
    }

    public final /* synthetic */ class i extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, JSONObject> {
        public i(a aVar) {
            super(1, aVar);
        }

        public final String getName() {
            return "handleResponse";
        }

        public final kotlin.h.d getOwner() {
            return t.a(a.class);
        }

        public final String getSignature() {
            return "handleResponse(Lorg/json/JSONObject;)Lorg/json/JSONObject;";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return ((a) this.receiver).a(jSONObject);
        }
    }

    public final class j extends kotlin.d.b.k implements kotlin.d.a.b<JSONObject, Boolean> {

        /* renamed from: a  reason: collision with root package name */
        public static final j f33a = new j();

        public j() {
            super(1);
        }

        public final Object invoke(Object obj) {
            kotlin.d.b.j.b((JSONObject) obj, "it");
            return true;
        }
    }

    public final /* synthetic */ class k extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, JSONObject> {
        public k(a aVar) {
            super(1, aVar);
        }

        public final String getName() {
            return "handleResponseData";
        }

        public final kotlin.h.d getOwner() {
            return t.a(a.class);
        }

        public final String getSignature() {
            return "handleResponseData(Lorg/json/JSONObject;)Lorg/json/JSONObject;";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return ((a) this.receiver).b(jSONObject);
        }
    }

    public final /* synthetic */ class l extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, b.b.a.h.o> {
        public l(o.b bVar) {
            super(1, bVar);
        }

        public final String getName() {
            return "parse";
        }

        public final kotlin.h.d getOwner() {
            return t.a(o.b.class);
        }

        public final String getSignature() {
            return "parse(Lorg/json/JSONObject;)Lcom/supercell/id/model/IdSystemConnection;";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return ((o.b) this.receiver).a(jSONObject);
        }
    }

    public final /* synthetic */ class m extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, JSONObject> {
        public m(a aVar) {
            super(1, aVar);
        }

        public final String getName() {
            return "handleResponseData";
        }

        public final kotlin.h.d getOwner() {
            return t.a(a.class);
        }

        public final String getSignature() {
            return "handleResponseData(Lorg/json/JSONObject;)Lorg/json/JSONObject;";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return ((a) this.receiver).b(jSONObject);
        }
    }

    public final class n extends kotlin.d.b.k implements kotlin.d.a.b<JSONObject, Boolean> {

        /* renamed from: a  reason: collision with root package name */
        public static final n f34a = new n();

        public n() {
            super(1);
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "it");
            return Boolean.valueOf(jSONObject.getBoolean("isBound"));
        }
    }

    public final /* synthetic */ class o extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, JSONObject> {
        public o(a aVar) {
            super(1, aVar);
        }

        public final String getName() {
            return "handleResponseData";
        }

        public final kotlin.h.d getOwner() {
            return t.a(a.class);
        }

        public final String getSignature() {
            return "handleResponseData(Lorg/json/JSONObject;)Lorg/json/JSONObject;";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return ((a) this.receiver).b(jSONObject);
        }
    }

    public final /* synthetic */ class p extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, m.b> {
        public p(m.b.a aVar) {
            super(1, aVar);
        }

        public final String getName() {
            return "parse";
        }

        public final kotlin.h.d getOwner() {
            return t.a(m.b.a.class);
        }

        public final String getSignature() {
            return "parse(Lorg/json/JSONObject;)Lcom/supercell/id/model/IdSubscriptions$Scope;";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return ((m.b.a) this.receiver).a(jSONObject);
        }
    }

    public final /* synthetic */ class q extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, JSONObject> {
        public q(a aVar) {
            super(1, aVar);
        }

        public final String getName() {
            return "handleResponseData";
        }

        public final kotlin.h.d getOwner() {
            return t.a(a.class);
        }

        public final String getSignature() {
            return "handleResponseData(Lorg/json/JSONObject;)Lorg/json/JSONObject;";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return ((a) this.receiver).b(jSONObject);
        }
    }

    public final /* synthetic */ class r extends kotlin.d.b.h implements kotlin.d.a.b<JSONObject, b.b.a.h.m> {
        public r(m.a aVar) {
            super(1, aVar);
        }

        public final String getName() {
            return "parse";
        }

        public final kotlin.h.d getOwner() {
            return t.a(m.a.class);
        }

        public final String getSignature() {
            return "parse(Lorg/json/JSONObject;)Lcom/supercell/id/model/IdSubscriptions;";
        }

        public final Object invoke(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            kotlin.d.b.j.b(jSONObject, "p1");
            return ((m.a) this.receiver).a(jSONObject);
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a(String str, String str2) {
        super(str, str2);
        kotlin.d.b.j.b(str, "url");
    }

    public final bw<b.b.a.h.e, Exception> a() {
        return bc.a(bc.a(d.a(this, "/api/account/info.get", null, 2, null), new e(this)), f.f32a);
    }

    public final bw<o.a, Exception> a(String str, String str2, String str3) {
        Map map;
        kotlin.d.b.j.b(str3, "pin");
        if (str != null) {
            map = ai.a(kotlin.k.a(NotificationCompat.CATEGORY_EMAIL, str), kotlin.k.a("pin", str3));
        } else if (str2 == null) {
            return bb.f5389a;
        } else {
            map = ai.a(kotlin.k.a(PlaceFields.PHONE, str2), kotlin.k.a("pin", str3));
        }
        return bc.a(bc.a(a("/api/account/create.confirm", map), new c(this)), d.f31a);
    }

    public final bw<Boolean, Exception> a(String str, String str2, boolean z) {
        Map map;
        String str3 = ServerProtocol.DIALOG_RETURN_SCOPES_TRUE;
        if (str != null) {
            kotlin.h[] hVarArr = new kotlin.h[4];
            hVarArr[0] = kotlin.k.a("lang", SupercellId.INSTANCE.getSharedServices$supercellId_release().j.c());
            if (!z) {
                str3 = "false";
            }
            hVarArr[1] = kotlin.k.a("accept_marketing", str3);
            hVarArr[2] = kotlin.k.a("g-recaptcha-response", "null");
            hVarArr[3] = kotlin.k.a(NotificationCompat.CATEGORY_EMAIL, str);
            map = ai.a(hVarArr);
        } else if (str2 == null) {
            return bb.f5389a;
        } else {
            kotlin.h[] hVarArr2 = new kotlin.h[4];
            hVarArr2[0] = kotlin.k.a("lang", SupercellId.INSTANCE.getSharedServices$supercellId_release().j.c());
            if (!z) {
                str3 = "false";
            }
            hVarArr2[1] = kotlin.k.a("accept_marketing", str3);
            hVarArr2[2] = kotlin.k.a("g-recaptcha-response", "null");
            hVarArr2[3] = kotlin.k.a(PlaceFields.PHONE, str2);
            map = ai.a(hVarArr2);
        }
        return bc.a(bc.a(a("/api/account/create", map), new C0002a(this)), b.f30a);
    }

    public final bw<m.b, Exception> a(String str, boolean z) {
        kotlin.d.b.j.b(str, "scopeId");
        kotlin.h[] hVarArr = new kotlin.h[2];
        hVarArr[0] = kotlin.k.a("marketing_scope", str);
        hVarArr[1] = kotlin.k.a("marketing_scope_consent", z ? ServerProtocol.DIALOG_RETURN_SCOPES_TRUE : "false");
        return bc.a(bc.a(a("/api/account/settings.setConsent", ai.a(hVarArr)), new o(this)), new p(m.b.d));
    }

    public final bw<b.b.a.h.m, Exception> a(boolean z) {
        return bc.a(bc.a(a("/api/account/settings.set", ai.a(kotlin.k.a("accept_marketing", z ? ServerProtocol.DIALOG_RETURN_SCOPES_TRUE : "false"))), new q(this)), new r(b.b.a.h.m.c));
    }

    public final bw<b.b.a.h.m, Exception> b() {
        return bc.a(bc.a(d.a(this, "/api/account/settings.get", null, 2, null), new g(this)), new h(b.b.a.h.m.c));
    }

    public final bw<Boolean, Exception> b(String str, String str2) {
        Map map;
        if (str != null) {
            map = ai.a(kotlin.k.a("lang", SupercellId.INSTANCE.getSharedServices$supercellId_release().j.c()), kotlin.k.a(NotificationCompat.CATEGORY_EMAIL, str), kotlin.k.a("game", SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getGame()), kotlin.k.a("env", SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getEnvironment()));
        } else if (str2 == null) {
            return bb.f5389a;
        } else {
            map = ai.a(kotlin.k.a("lang", SupercellId.INSTANCE.getSharedServices$supercellId_release().j.c()), kotlin.k.a(PlaceFields.PHONE, str2), kotlin.k.a("game", SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getGame()), kotlin.k.a("env", SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getEnvironment()));
        }
        return bc.a(bc.a(a("/api/account/login", map), new i(this)), j.f33a);
    }

    public final bw<b.b.a.h.o, Exception> b(String str, String str2, String str3) {
        Map map;
        kotlin.d.b.j.b(str3, "pin");
        if (str != null) {
            map = ai.a(kotlin.k.a(NotificationCompat.CATEGORY_EMAIL, str), kotlin.k.a("pin", str3));
        } else if (str2 == null) {
            return bb.f5389a;
        } else {
            map = ai.a(kotlin.k.a(PlaceFields.PHONE, str2), kotlin.k.a("pin", str3));
        }
        return bc.a(bc.a(a("/api/account/login.confirm", map), new k(this)), new l(b.b.a.h.o.f137b));
    }

    public final bw<Boolean, Exception> c(String str, String str2, String str3) {
        Map map;
        kotlin.d.b.j.b(str3, "pin");
        if (str != null) {
            map = ai.a(kotlin.k.a(NotificationCompat.CATEGORY_EMAIL, str), kotlin.k.a("pin", str3));
        } else if (str2 == null) {
            return bb.f5389a;
        } else {
            map = ai.a(kotlin.k.a(PlaceFields.PHONE, str2), kotlin.k.a("pin", str3));
        }
        return bc.a(bc.a(a("/api/account/login.validate", map), new m(this)), n.f34a);
    }
}
