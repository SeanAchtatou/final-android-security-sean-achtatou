package b.b.a.d;

import com.facebook.share.internal.ShareConstants;
import kotlin.d.b.j;

public final class c extends Exception {

    /* renamed from: a  reason: collision with root package name */
    public final int f36a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c(String str, int i) {
        super(str);
        j.b(str, ShareConstants.WEB_DIALOG_PARAM_MESSAGE);
        this.f36a = i;
    }
}
