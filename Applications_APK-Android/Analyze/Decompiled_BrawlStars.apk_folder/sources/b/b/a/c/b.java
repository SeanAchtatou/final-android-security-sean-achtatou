package b.b.a.c;

import android.content.Context;
import com.facebook.internal.NativeProtocol;
import com.google.android.gms.analytics.b;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.d.b.r;
import kotlin.d.b.t;
import kotlin.h.h;
import kotlin.m;
import nl.komponents.kovenant.a.a;
import nl.komponents.kovenant.ao;
import nl.komponents.kovenant.bb;
import nl.komponents.kovenant.bc;
import nl.komponents.kovenant.bl;
import nl.komponents.kovenant.bm;
import nl.komponents.kovenant.bn;

public final class b {
    public static final /* synthetic */ h[] e = {t.a(new r(t.a(b.class), "tracker", "getTracker()Lcom/google/android/gms/analytics/Tracker;"))};
    public static kotlin.h<String, ? extends com.google.android.gms.analytics.d> f;
    public static final ao g = bb.b(a.f20a);
    public static final C0001b h = new C0001b(null);

    /* renamed from: a  reason: collision with root package name */
    public final kotlin.d f18a = kotlin.e.a(new g());

    /* renamed from: b  reason: collision with root package name */
    public final Context f19b;
    public final String c;
    public final boolean d;

    public final class a extends k implements kotlin.d.a.b<bm, m> {

        /* renamed from: a  reason: collision with root package name */
        public static final a f20a = new a();

        public a() {
            super(1);
        }

        public final Object invoke(Object obj) {
            bm bmVar = (bm) obj;
            j.b(bmVar, "$receiver");
            bn b2 = bmVar.b();
            a.C0165a aVar = nl.komponents.kovenant.a.a.f5336a;
            b2.a(nl.komponents.kovenant.a.a.c);
            bmVar.d().a(bl.a(a.f17a));
            return m.f5330a;
        }
    }

    /* renamed from: b.b.a.c.b$b  reason: collision with other inner class name */
    public static final class C0001b {
        public /* synthetic */ C0001b(kotlin.d.b.g gVar) {
        }
    }

    public final class c extends k implements kotlin.d.a.a<m> {
        public c() {
            super(0);
        }

        public final Object invoke() {
            com.google.android.gms.analytics.a.a(b.this.f19b.getApplicationContext()).c();
            return m.f5330a;
        }
    }

    public final class d extends k implements kotlin.d.a.a<m> {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ int f23b;
        public final /* synthetic */ String c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(int i, String str) {
            super(0);
            this.f23b = i;
            this.c = str;
        }

        public final Object invoke() {
            com.google.android.gms.analytics.d b2 = b.b(b.this);
            StringBuilder a2 = b.a.a.a.a.a("&cd");
            a2.append(this.f23b);
            b2.a(a2.toString(), this.c);
            return m.f5330a;
        }
    }

    public final class e extends k implements kotlin.d.a.a<m> {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ String f25b;
        public final /* synthetic */ String c;
        public final /* synthetic */ String d;
        public final /* synthetic */ Long e;
        public final /* synthetic */ boolean f;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(String str, String str2, String str3, Long l, boolean z) {
            super(0);
            this.f25b = str;
            this.c = str2;
            this.d = str3;
            this.e = l;
            this.f = z;
        }

        public final Object invoke() {
            StringBuilder a2 = b.a.a.a.a.a("trackEvent: ");
            a2.append(this.f25b);
            a2.append(' ');
            a2.append(this.c);
            a2.append(' ');
            a2.append(this.d);
            a2.append(' ');
            a2.append(this.e);
            a2.append(' ');
            a2.append(this.f);
            a2.toString();
            b.a aVar = new b.a(this.f25b, this.c);
            String str = this.d;
            if (str != null) {
                aVar.a(str);
            }
            Long l = this.e;
            if (l != null) {
                aVar.a(l.longValue());
            }
            if (this.f) {
                aVar.a("&sc", "end");
            }
            b.b(b.this).a(aVar.a());
            return m.f5330a;
        }
    }

    public final class f extends k implements kotlin.d.a.a<m> {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ String f27b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(String str) {
            super(0);
            this.f27b = str;
        }

        public final Object invoke() {
            StringBuilder a2 = b.a.a.a.a.a("trackView: ");
            a2.append(this.f27b);
            a2.toString();
            b.b(b.this).a(this.f27b);
            b.b(b.this).a(new b.c().a());
            return m.f5330a;
        }
    }

    public final class g extends k implements kotlin.d.a.a<com.google.android.gms.analytics.d> {

        public final class a extends k implements kotlin.d.a.a<com.google.android.gms.analytics.d> {
            public a() {
                super(0);
            }

            public final Object invoke() {
                com.google.android.gms.analytics.d a2 = com.google.android.gms.analytics.a.a(b.this.f19b.getApplicationContext()).a(b.this.c);
                if (b.this.d) {
                    a2.a(1.0d);
                }
                b.f = kotlin.k.a(b.this.c, a2);
                return a2;
            }
        }

        public g() {
            super(0);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
         arg types: [java.lang.String, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
        public final com.google.android.gms.analytics.d invoke() {
            com.google.android.gms.analytics.d dVar;
            synchronized (b.h) {
                kotlin.h<String, ? extends com.google.android.gms.analytics.d> hVar = b.f;
                if (hVar != null) {
                    if (!j.a((Object) ((String) hVar.f5262a), (Object) b.this.c)) {
                        hVar = null;
                    }
                    if (!(hVar == null || (dVar = (com.google.android.gms.analytics.d) hVar.f5263b) == null)) {
                    }
                }
                dVar = (com.google.android.gms.analytics.d) new a().invoke();
            }
            return dVar;
        }
    }

    static {
        bb bbVar = bb.f5389a;
    }

    public b(Context context, String str, boolean z) {
        j.b(context, "context");
        j.b(str, "gaTrackingId");
        this.f19b = context;
        this.c = str;
        this.d = z;
    }

    public static /* synthetic */ void a(b bVar, String str, String str2, String str3, Long l, boolean z, int i) {
        bVar.a(str, str2, (i & 4) != 0 ? null : str3, (i & 8) != 0 ? null : l, (i & 16) != 0 ? false : z);
    }

    public static final /* synthetic */ com.google.android.gms.analytics.d b(b bVar) {
        return (com.google.android.gms.analytics.d) bVar.f18a.a();
    }

    public final void a() {
        bc.a(g, new c());
    }

    public final void a(int i, String str) {
        j.b(str, "value");
        bc.a(g, new d(i, str));
    }

    public final void a(String str) {
        j.b(str, "name");
        bc.a(g, new f(str));
    }

    public final void a(String str, String str2, String str3, Long l, boolean z) {
        j.b(str, "category");
        j.b(str2, NativeProtocol.WEB_DIALOG_ACTION);
        bc.a(g, new e(str, str2, str3, l, z));
    }
}
