package b.b.a.j;

import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public abstract class v0<T> {

    /* renamed from: a  reason: collision with root package name */
    public T f1179a;

    /* renamed from: b  reason: collision with root package name */
    public final ConcurrentHashMap<WeakReference<kotlin.d.a.b<T, m>>, Integer> f1180b = new ConcurrentHashMap<>();
    public final ConcurrentHashMap<WeakReference<kotlin.d.a.b<T, m>>, Integer> c = new ConcurrentHashMap<>();

    public final class a extends k implements kotlin.d.a.a<m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ Iterator f1181a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Iterator it) {
            super(0);
            this.f1181a = it;
        }

        public final Object invoke() {
            this.f1181a.remove();
            return m.f5330a;
        }
    }

    public final class b extends k implements kotlin.d.a.a<m> {

        public final class a extends k implements kotlin.d.a.a<m> {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ Iterator f1183a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(Iterator it) {
                super(0);
                this.f1183a = it;
            }

            public final Object invoke() {
                this.f1183a.remove();
                return m.f5330a;
            }
        }

        public b() {
            super(0);
        }

        public final void invoke() {
            synchronized (v0.this.c) {
                Iterator<Map.Entry<WeakReference<kotlin.d.a.b<T, m>>, Integer>> it = v0.this.c.entrySet().iterator();
                while (it.hasNext()) {
                    kotlin.d.a.b bVar = (kotlin.d.a.b) ((WeakReference) it.next().getKey()).get();
                    if (bVar != null) {
                        bVar.invoke(v0.this.f1179a);
                    } else {
                        new a(it).invoke();
                    }
                }
                m mVar = m.f5330a;
            }
        }
    }

    public final class c extends k implements kotlin.d.a.a<m> {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ kotlin.d.a.b f1185b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(kotlin.d.a.b bVar) {
            super(0);
            this.f1185b = bVar;
        }

        public final Object invoke() {
            this.f1185b.invoke(v0.this.f1179a);
            return m.f5330a;
        }
    }

    public v0() {
        toString();
    }

    public final void a(a aVar) {
        j.b(aVar, "actionObject");
        synchronized (this.f1180b) {
            this.f1179a = aVar.invoke(this.f1179a);
            Iterator<Map.Entry<WeakReference<kotlin.d.a.b<T, m>>, Integer>> it = this.f1180b.entrySet().iterator();
            while (it.hasNext()) {
                kotlin.d.a.b bVar = (kotlin.d.a.b) ((WeakReference) it.next().getKey()).get();
                if (bVar != null) {
                    bVar.invoke(this.f1179a);
                } else {
                    new a(it).invoke();
                }
            }
            m mVar = m.f5330a;
        }
        b.b.a.b.a(new b());
    }

    public final void a(kotlin.d.a.b bVar) {
        j.b(bVar, "listener");
        bVar.invoke(this.f1179a);
        synchronized (this.f1180b) {
            this.f1180b.put(new WeakReference(bVar), 0);
            m mVar = m.f5330a;
        }
    }

    public final void b(kotlin.d.a.b bVar) {
        j.b(bVar, "listener");
        b.b.a.b.a(new c(bVar));
        synchronized (this.c) {
            this.c.put(new WeakReference(bVar), 0);
            m mVar = m.f5330a;
        }
    }

    public final void c(kotlin.d.a.b bVar) {
        j.b(bVar, "listener");
        synchronized (this.f1180b) {
            Iterator<Map.Entry<WeakReference<kotlin.d.a.b<T, m>>, Integer>> it = this.f1180b.entrySet().iterator();
            while (true) {
                if (it.hasNext()) {
                    if (j.a((kotlin.d.a.b) ((WeakReference) it.next().getKey()).get(), bVar)) {
                        it.remove();
                        break;
                    }
                } else {
                    break;
                }
            }
            m mVar = m.f5330a;
        }
        synchronized (this.c) {
            Iterator<Map.Entry<WeakReference<kotlin.d.a.b<T, m>>, Integer>> it2 = this.c.entrySet().iterator();
            while (true) {
                if (it2.hasNext()) {
                    if (j.a((kotlin.d.a.b) ((WeakReference) it2.next().getKey()).get(), bVar)) {
                        it2.remove();
                        break;
                    }
                } else {
                    break;
                }
            }
            m mVar2 = m.f5330a;
        }
    }
}
