package b.b.a.j;

import android.support.constraint.ConstraintLayout;
import android.view.View;
import kotlin.e.a;

public final class y implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ View f1194a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ ConstraintLayout.LayoutParams f1195b;
    public final /* synthetic */ float c;
    public final /* synthetic */ int d;

    public y(View view, ConstraintLayout.LayoutParams layoutParams, float f, int i) {
        this.f1194a = view;
        this.f1195b = layoutParams;
        this.c = f;
        this.d = i;
    }

    public final void run() {
        View view = this.f1194a;
        ConstraintLayout.LayoutParams layoutParams = this.f1195b;
        if (layoutParams != null) {
            layoutParams.width = a.a(this.c);
            layoutParams.height = a.a(this.c);
            layoutParams.circleRadius = this.d;
        } else {
            layoutParams = null;
        }
        view.setLayoutParams(layoutParams);
    }
}
