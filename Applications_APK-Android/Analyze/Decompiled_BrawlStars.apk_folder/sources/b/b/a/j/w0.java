package b.b.a.j;

import android.support.v4.app.Fragment;
import b.a.a.a.a;
import kotlin.d.b.j;

public final class w0 {

    /* renamed from: a  reason: collision with root package name */
    public final String f1187a;

    /* renamed from: b  reason: collision with root package name */
    public final Class<? extends Fragment> f1188b;

    public w0(String str, Class<? extends Fragment> cls) {
        j.b(str, "titleKey");
        j.b(cls, "fragmentClass");
        this.f1187a = str;
        this.f1188b = cls;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof w0)) {
            return false;
        }
        w0 w0Var = (w0) obj;
        return j.a(this.f1187a, w0Var.f1187a) && j.a(this.f1188b, w0Var.f1188b);
    }

    public final int hashCode() {
        String str = this.f1187a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Class<? extends Fragment> cls = this.f1188b;
        if (cls != null) {
            i = cls.hashCode();
        }
        return hashCode + i;
    }

    public final String toString() {
        StringBuilder a2 = a.a("SubPageTabData(titleKey=");
        a2.append(this.f1187a);
        a2.append(", fragmentClass=");
        a2.append(this.f1188b);
        a2.append(")");
        return a2.toString();
    }
}
