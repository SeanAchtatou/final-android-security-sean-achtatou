package b.b.a.j;

import android.os.Build;
import android.support.v4.widget.NestedScrollView;
import android.view.View;
import android.widget.ScrollView;
import b.a.a.a.a;
import kotlin.d.b.j;

public final class g0 extends z {

    /* renamed from: b  reason: collision with root package name */
    public View f1039b;
    public ScrollView c;
    public NestedScrollView d;
    public View e;
    public View f;
    public Integer g;
    public Integer h;
    public Integer i;
    public Integer j;
    public final int k;
    public final boolean l;
    public final int m;

    public g0(int i2, boolean z, int i3) {
        this.k = i2;
        this.l = z;
        this.m = i3;
    }

    /* JADX WARNING: Removed duplicated region for block: B:108:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0093  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0095  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00b6  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00bb  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x00da  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x00dc  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x00df  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x00ff  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0101  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0107  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.graphics.Rect r14) {
        /*
            r13 = this;
            java.lang.String r0 = "systemWindowInsets"
            kotlin.d.b.j.b(r14, r0)
            android.view.View r0 = r13.e
            if (r0 == 0) goto L_0x012d
            android.view.View r1 = r13.f
            if (r1 == 0) goto L_0x012d
            java.lang.Integer r2 = r13.g
            if (r2 == 0) goto L_0x012d
            int r2 = r2.intValue()
            java.lang.Integer r3 = r13.h
            if (r3 == 0) goto L_0x012d
            int r3 = r3.intValue()
            java.lang.Integer r4 = r13.i
            if (r4 == 0) goto L_0x012d
            int r4 = r4.intValue()
            java.lang.Integer r5 = r13.j
            if (r5 == 0) goto L_0x012d
            int r5 = r5.intValue()
            com.supercell.id.view.RootFrameLayout r6 = r13.f1202a
            if (r6 == 0) goto L_0x012d
            boolean r7 = r13.l
            if (r7 == 0) goto L_0x003b
            android.graphics.Rect r7 = new android.graphics.Rect
            r7.<init>()
            goto L_0x003f
        L_0x003b:
            android.graphics.Rect r7 = b.b.a.j.q1.b(r6)
        L_0x003f:
            boolean r8 = r13.l
            if (r8 == 0) goto L_0x0045
            r0 = r7
            goto L_0x0049
        L_0x0045:
            android.graphics.Rect r0 = b.b.a.j.q1.b(r0)
        L_0x0049:
            int r8 = android.support.v4.view.ViewCompat.getLayoutDirection(r6)
            r9 = 1
            r10 = 0
            if (r8 != r9) goto L_0x0053
            r8 = 1
            goto L_0x0054
        L_0x0053:
            r8 = 0
        L_0x0054:
            if (r8 == 0) goto L_0x0059
            r8 = 32
            goto L_0x005b
        L_0x0059:
            r8 = 16
        L_0x005b:
            int r6 = android.support.v4.view.ViewCompat.getLayoutDirection(r6)
            if (r6 != r9) goto L_0x0063
            r6 = 1
            goto L_0x0064
        L_0x0063:
            r6 = 0
        L_0x0064:
            if (r6 == 0) goto L_0x0069
            r6 = 8
            goto L_0x006a
        L_0x0069:
            r6 = 4
        L_0x006a:
            int r11 = r13.k
            r8 = r8 | r11
            if (r8 != r11) goto L_0x0071
            r8 = 1
            goto L_0x0072
        L_0x0071:
            r8 = 0
        L_0x0072:
            if (r8 == 0) goto L_0x008b
            int r8 = r7.left
            int r11 = r14.left
            int r8 = r8 + r11
            int r12 = r0.left
            int r8 = r8 - r12
            int r12 = kotlin.d.b.j.a(r8, r10)
            if (r12 >= 0) goto L_0x0083
            goto L_0x008b
        L_0x0083:
            int r12 = kotlin.d.b.j.a(r8, r11)
            if (r12 <= 0) goto L_0x008c
            r8 = r11
            goto L_0x008c
        L_0x008b:
            r8 = 0
        L_0x008c:
            int r3 = r3 + r8
            int r8 = r13.k
            r11 = r8 | 1
            if (r11 != r8) goto L_0x0095
            r8 = 1
            goto L_0x0096
        L_0x0095:
            r8 = 0
        L_0x0096:
            if (r8 == 0) goto L_0x00af
            int r8 = r7.top
            int r11 = r14.top
            int r8 = r8 + r11
            int r12 = r0.top
            int r8 = r8 - r12
            int r12 = kotlin.d.b.j.a(r8, r10)
            if (r12 >= 0) goto L_0x00a7
            goto L_0x00af
        L_0x00a7:
            int r12 = kotlin.d.b.j.a(r8, r11)
            if (r12 <= 0) goto L_0x00b0
            r8 = r11
            goto L_0x00b0
        L_0x00af:
            r8 = 0
        L_0x00b0:
            int r2 = r2 + r8
            int r8 = r13.k
            r6 = r6 | r8
            if (r6 != r8) goto L_0x00b8
            r6 = 1
            goto L_0x00b9
        L_0x00b8:
            r6 = 0
        L_0x00b9:
            if (r6 == 0) goto L_0x00d2
            int r6 = r0.right
            int r8 = r7.right
            int r11 = r14.right
            int r8 = r8 - r11
            int r6 = r6 - r8
            int r8 = kotlin.d.b.j.a(r6, r10)
            if (r8 >= 0) goto L_0x00ca
            goto L_0x00d2
        L_0x00ca:
            int r8 = kotlin.d.b.j.a(r6, r11)
            if (r8 <= 0) goto L_0x00d3
            r6 = r11
            goto L_0x00d3
        L_0x00d2:
            r6 = 0
        L_0x00d3:
            int r4 = r4 + r6
            int r6 = r13.k
            r8 = r6 | 2
            if (r8 != r6) goto L_0x00dc
            r6 = 1
            goto L_0x00dd
        L_0x00dc:
            r6 = 0
        L_0x00dd:
            if (r6 == 0) goto L_0x00f7
            int r0 = r0.bottom
            int r6 = r7.bottom
            int r14 = r14.bottom
            int r6 = r6 - r14
            int r0 = r0 - r6
            int r6 = kotlin.d.b.j.a(r0, r10)
            if (r6 >= 0) goto L_0x00ee
            goto L_0x00f7
        L_0x00ee:
            int r6 = kotlin.d.b.j.a(r0, r14)
            if (r6 <= 0) goto L_0x00f5
            goto L_0x00f8
        L_0x00f5:
            r14 = r0
            goto L_0x00f8
        L_0x00f7:
            r14 = 0
        L_0x00f8:
            int r5 = r5 + r14
            int r14 = r1.getPaddingBottom()
            if (r5 <= r14) goto L_0x0101
            r14 = 1
            goto L_0x0102
        L_0x0101:
            r14 = 0
        L_0x0102:
            b.b.a.j.q1.a(r1, r3, r2, r4, r5)
            if (r14 == 0) goto L_0x012d
            android.view.View r14 = r13.f1039b
            boolean r14 = kotlin.d.b.j.a(r1, r14)
            r14 = r14 ^ r9
            if (r14 == 0) goto L_0x011f
            android.widget.ScrollView r14 = r13.c
            if (r14 == 0) goto L_0x0117
            b.b.a.j.q1.a(r14, r5)
        L_0x0117:
            android.support.v4.widget.NestedScrollView r14 = r13.d
            if (r14 == 0) goto L_0x012d
            b.b.a.j.q1.a(r14, r5)
            goto L_0x012d
        L_0x011f:
            android.widget.ScrollView r14 = r13.c
            if (r14 == 0) goto L_0x0126
            b.b.a.j.q1.a(r14, r10)
        L_0x0126:
            android.support.v4.widget.NestedScrollView r14 = r13.d
            if (r14 == 0) goto L_0x012d
            b.b.a.j.q1.a(r14, r10)
        L_0x012d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.j.g0.a(android.graphics.Rect):void");
    }

    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof g0) {
                g0 g0Var = (g0) obj;
                if (this.k == g0Var.k) {
                    if (this.l == g0Var.l) {
                        if (this.m == g0Var.m) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        int i2 = this.k * 31;
        boolean z = this.l;
        if (z) {
            z = true;
        }
        return ((i2 + (z ? 1 : 0)) * 31) + this.m;
    }

    public final void onViewAttachedToWindow(View view) {
        View view2;
        j.b(view, "v");
        this.f1039b = view;
        Integer num = null;
        this.c = (ScrollView) (!(view instanceof ScrollView) ? null : view);
        this.d = (NestedScrollView) (!(view instanceof NestedScrollView) ? null : view);
        View a2 = q1.a(view, this.m);
        if (a2 == null) {
            a2 = view;
        }
        this.e = a2;
        ScrollView scrollView = this.c;
        if (!(Build.VERSION.SDK_INT < 28)) {
            scrollView = null;
        }
        if (scrollView == null || (view2 = scrollView.getChildAt(0)) == null) {
            view2 = view;
        }
        this.f = view2;
        Integer num2 = this.g;
        if (num2 == null) {
            View view3 = this.f;
            num2 = view3 != null ? Integer.valueOf(view3.getPaddingTop()) : null;
        }
        this.g = num2;
        Integer num3 = this.h;
        if (num3 == null) {
            View view4 = this.f;
            num3 = view4 != null ? Integer.valueOf(view4.getPaddingLeft()) : null;
        }
        this.h = num3;
        Integer num4 = this.i;
        if (num4 == null) {
            View view5 = this.f;
            num4 = view5 != null ? Integer.valueOf(view5.getPaddingRight()) : null;
        }
        this.i = num4;
        Integer num5 = this.j;
        if (num5 != null) {
            num = num5;
        } else {
            View view6 = this.f;
            if (view6 != null) {
                num = Integer.valueOf(view6.getPaddingBottom());
            }
        }
        this.j = num;
        super.onViewAttachedToWindow(view);
    }

    public final void onViewDetachedFromWindow(View view) {
        j.b(view, "v");
        this.f1039b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        super.onViewDetachedFromWindow(view);
    }

    public final String toString() {
        StringBuilder a2 = a.a("PaddingInsetUpdater(insetFlags=");
        a2.append(this.k);
        a2.append(", force=");
        a2.append(this.l);
        a2.append(", insetParent=");
        a2.append(this.m);
        a2.append(")");
        return a2.toString();
    }
}
