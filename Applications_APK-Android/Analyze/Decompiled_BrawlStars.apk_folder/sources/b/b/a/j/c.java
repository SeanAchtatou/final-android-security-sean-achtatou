package b.b.a.j;

import io.github.inflationx.viewpump.InflateResult;
import io.github.inflationx.viewpump.Interceptor;
import kotlin.d.b.j;

public final class c implements Interceptor {

    /* renamed from: a  reason: collision with root package name */
    public static final c f1019a = new c();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [io.github.inflationx.viewpump.InflateResult, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final InflateResult intercept(Interceptor.Chain chain) {
        j.b(chain, "chain");
        InflateResult proceed = chain.proceed(chain.request());
        InflateResult build = proceed.toBuilder().view(p.a(proceed.view())).build();
        j.a((Object) build, "result.toBuilder().view(…s(result.view())).build()");
        j.a((Object) build, "chain.proceed(chain.requ…iew())).build()\n        }");
        return build;
    }
}
