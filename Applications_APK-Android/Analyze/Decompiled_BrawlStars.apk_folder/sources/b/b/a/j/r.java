package b.b.a.j;

import java.util.List;
import kotlin.a.m;
import kotlin.d.b.j;
import kotlin.j.a;
import kotlin.j.ac;

public final class r {

    /* renamed from: a  reason: collision with root package name */
    public static final r f1160a = new r();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.j.ac.a(java.lang.CharSequence, java.lang.String, boolean, int):java.util.List<java.lang.String>
     arg types: [java.lang.String, java.lang.String, int, int]
     candidates:
      kotlin.j.ac.a(java.lang.CharSequence, char, int, boolean):int
      kotlin.j.ac.a(java.lang.CharSequence, java.lang.String, int, boolean):int
      kotlin.j.ac.a(java.lang.CharSequence, char[], int, boolean):int
      kotlin.j.ac.a(java.lang.CharSequence, java.lang.String[], boolean, int):kotlin.i.h<java.lang.String>
      kotlin.j.ab.a(java.lang.String, java.lang.String, java.lang.String, boolean):java.lang.String
      kotlin.j.ac.a(java.lang.CharSequence, java.lang.String, boolean, int):java.util.List<java.lang.String> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.a(java.util.List, int):T
     arg types: [java.util.List<java.lang.String>, int]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.w.a(java.util.List, int):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final String a(String str) {
        Integer num;
        Long l;
        j.b(str, "supercellId");
        char[] cArr = {'-'};
        j.b(str, "$this$split");
        j.b(cArr, "delimiters");
        List<String> a2 = ac.a((CharSequence) str, String.valueOf(cArr[0]), false, 3);
        String str2 = (String) m.a((List) a2, 0);
        if (str2 != null) {
            try {
                num = Integer.valueOf(Integer.parseInt(str2, a.a(10)) & 255);
            } catch (Exception unused) {
                num = null;
            }
            if (num != null) {
                int intValue = num.intValue();
                String str3 = (String) m.a((List) a2, 1);
                if (str3 != null) {
                    try {
                        l = Long.valueOf(Long.parseLong(str3, 16) & 4294967295L);
                    } catch (Exception unused2) {
                        l = null;
                    }
                    if (l != null) {
                        long longValue = (l.longValue() << 8) + ((long) intValue);
                        StringBuffer stringBuffer = new StringBuffer(12);
                        stringBuffer.append('#');
                        for (int i = 11; i >= 0; i--) {
                            stringBuffer.append("0289PYLQGRJCUV".charAt((int) (longValue % 14)));
                            longValue /= 14;
                            if (longValue == 0) {
                                break;
                            }
                        }
                        String stringBuffer2 = stringBuffer.toString();
                        j.a((Object) stringBuffer2, "res.toString()");
                        return stringBuffer2;
                    }
                }
            }
        }
        return null;
    }
}
