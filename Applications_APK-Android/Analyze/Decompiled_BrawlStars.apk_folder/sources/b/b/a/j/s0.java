package b.b.a.j;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import b.b.a.b;
import b.b.a.g.g;
import com.supercell.id.R;
import com.supercell.id.view.WidthAdjustingMultilineButton;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import kotlin.a.ab;
import kotlin.d.a.c;
import kotlin.d.b.j;
import kotlin.m;

public class s0 extends RecyclerView.Adapter<a> {

    /* renamed from: a  reason: collision with root package name */
    public List<? extends r0> f1164a = ab.f5210a;

    public static final class a extends RecyclerView.ViewHolder implements Observer {

        /* renamed from: a  reason: collision with root package name */
        public c<? super Observable, Object, m> f1165a;

        /* renamed from: b  reason: collision with root package name */
        public r0 f1166b;
        public final View c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(View view) {
            super(view);
            j.b(view, "containerView");
            this.c = view;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.TextView, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, kotlin.d.a.b, int):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String, ?[OBJECT, ARRAY], int]
         candidates:
          b.b.a.i.x1.j.a(android.widget.ImageView, java.lang.String, boolean, int):void
          b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, java.lang.Object, int):void
          b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, kotlin.d.a.b, int):void */
        public final void a(r0 r0Var) {
            j.b(r0Var, "item");
            this.f1166b = r0Var;
            if (r0Var instanceof k) {
                View findViewById = this.c.findViewById(R.id.listDivider);
                j.a((Object) findViewById, "containerView.listDivider");
                b.a(findViewById, 0, 0.0f, 0.0f, 0.0f, g.b.MIDDLE, 15);
            } else if (r0Var instanceof o) {
                TextView textView = (TextView) this.c.findViewById(R.id.errorTitleTextView);
                j.a((Object) textView, "containerView.errorTitleTextView");
                o oVar = (o) r0Var;
                b.b.a.i.x1.j.a(textView, oVar.f1108b.f1036a, (kotlin.d.a.b) null, 2);
                TextView textView2 = (TextView) this.c.findViewById(R.id.errorTextTextView);
                j.a((Object) textView2, "containerView.errorTextTextView");
                f0 f0Var = oVar.f1108b;
                b.b.a.i.x1.j.a(textView2, f0Var.f1037b, b.a(f0Var.d));
                WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) this.c.findViewById(R.id.errorRetryButton);
                j.a((Object) widthAdjustingMultilineButton, "containerView.errorRetryButton");
                b.b.a.i.x1.j.a((TextView) widthAdjustingMultilineButton, "account_friend_api_error_server_btn_retry", (kotlin.d.a.b) null, 2);
            } else if (r0Var instanceof b0) {
                TextView textView3 = (TextView) this.c.findViewById(R.id.message_text);
                j.a((Object) textView3, "containerView.message_text");
                b.b.a.i.x1.j.a(textView3, ((b0) r0Var).f1013b, (kotlin.d.a.b) null, 2);
            }
        }

        public final void a(c<? super Observable, Object, m> cVar) {
            this.f1165a = cVar;
        }

        public final View getContainerView() {
            return this.c;
        }

        public final void update(Observable observable, Object obj) {
            c<? super Observable, Object, m> cVar = this.f1165a;
            if (cVar != null) {
                cVar.invoke(observable, obj);
            }
        }
    }

    public void a(a aVar, int i, r0 r0Var) {
        throw null;
    }

    public final void a(List<? extends r0> list) {
        j.b(list, "<set-?>");
        this.f1164a = list;
    }

    public int getItemCount() {
        return this.f1164a.size();
    }

    public int getItemViewType(int i) {
        return ((r0) this.f1164a.get(i)).a();
    }

    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        a aVar = (a) viewHolder;
        j.b(aVar, "holder");
        r0 r0Var = (r0) this.f1164a.get(i);
        aVar.a(r0Var);
        a(aVar, i, r0Var);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        j.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(i, viewGroup, false);
        j.a((Object) inflate, "LayoutInflater.from(pare…(viewType, parent, false)");
        return new a(inflate);
    }
}
