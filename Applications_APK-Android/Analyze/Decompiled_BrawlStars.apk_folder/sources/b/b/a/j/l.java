package b.b.a.j;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import com.supercell.id.SupercellId;
import java.nio.ByteBuffer;
import java.util.List;
import kotlin.d.a.e;
import kotlin.d.b.j;
import kotlin.m;

public final class l {

    /* renamed from: a  reason: collision with root package name */
    public static final l f1088a = new l();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, int, float, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.graphics.Bitmap, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final Bitmap a(Bitmap bitmap, int i, int i2) {
        Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
        Canvas canvas = new Canvas(createBitmap);
        Paint paint = new Paint();
        paint.setDither(true);
        paint.setShader(new LinearGradient(0.0f, 0.0f, (float) bitmap.getWidth(), (float) bitmap.getHeight(), i, i2, Shader.TileMode.CLAMP));
        canvas.drawPaint(paint);
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, (Paint) null);
        j.a((Object) createBitmap, "newBitmap");
        return createBitmap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.graphics.Bitmap, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final Bitmap a(Drawable drawable, int i, int i2, int i3, int i4) {
        j.b(drawable, "atlas");
        if (i < 0 || !(drawable instanceof BitmapDrawable)) {
            return null;
        }
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        int i5 = i % i2;
        int i6 = i / i2;
        j.a((Object) bitmap, "bitmapAtlas");
        float width = ((float) bitmap.getWidth()) / ((float) i2);
        float f = (float) i3;
        float f2 = width / f;
        return Bitmap.createBitmap(bitmap, (int) (((float) (i5 * i3)) * f2), (int) (((float) (i6 * i4)) * f2), (int) (f * f2), (int) (((float) i4) * f2));
    }

    public final Drawable a(Bitmap bitmap, int i, int i2, Resources resources) {
        j.b(bitmap, "bitmap");
        j.b(resources, "resources");
        RoundedBitmapDrawable create = RoundedBitmapDrawableFactory.create(resources, a(bitmap, i, i2));
        create.setCircular(true);
        return create;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.graphics.Bitmap, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final Drawable a(String str, Drawable drawable, Resources resources) {
        String str2 = str;
        Resources resources2 = resources;
        j.b(str2, "regionCode");
        j.b(drawable, "flagAtlas");
        j.b(resources2, "resources");
        List<String> d = SupercellId.INSTANCE.getRemoteConfiguration$supercellId_release().d(q0.SMS_REGIONS);
        Bitmap a2 = a(drawable, d != null ? d.indexOf(str2) : -1, 15, 24, 16);
        if (a2 == null) {
            return null;
        }
        int width = a2.getWidth();
        int[] iArr = new int[(a2.getHeight() * a2.getWidth())];
        a2.getPixels(iArr, 0, a2.getWidth(), 0, 0, a2.getWidth(), a2.getHeight());
        int width2 = a2.getWidth() - 1;
        loop0:
        while (true) {
            if (width2 < 0) {
                break;
            }
            for (int height = a2.getHeight() - 1; height >= 0; height--) {
                if (iArr[(a2.getWidth() * height) + width2] != 0) {
                    width = width2;
                    break loop0;
                }
            }
            width2--;
        }
        if (width != a2.getWidth() - 1) {
            a2 = Bitmap.createBitmap(a2, 0, 0, width + 1, a2.getHeight());
            j.a((Object) a2, "Bitmap.createBitmap(sour…lastX + 1, source.height)");
        }
        return new BitmapDrawable(resources2, a2);
    }

    public final void a(Bitmap bitmap, int i, int i2, e<? super byte[], ? super Integer, ? super Integer, ? super Integer, m> eVar) {
        j.b(bitmap, "bitmap");
        j.b(eVar, "callback");
        int height = bitmap.getHeight() * bitmap.getRowBytes();
        ByteBuffer allocate = ByteBuffer.allocate(height);
        a(bitmap, i, i2).copyPixelsToBuffer(allocate);
        eVar.a(allocate.array(), Integer.valueOf(height), Integer.valueOf(bitmap.getWidth()), Integer.valueOf(bitmap.getHeight()));
    }
}
