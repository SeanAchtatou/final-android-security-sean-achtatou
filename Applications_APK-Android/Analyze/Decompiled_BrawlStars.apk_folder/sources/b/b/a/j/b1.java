package b.b.a.j;

import kotlin.d.b.k;
import kotlin.m;
import nl.komponents.kovenant.ap;
import nl.komponents.kovenant.bw;
import nl.komponents.kovenant.q;

public final class b1 implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ kotlin.d.a.a f1014a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ ap f1015b;
    public final /* synthetic */ ap c;

    public final class a extends k implements kotlin.d.a.b<V, m> {
        public a() {
            super(1);
        }

        public final Object invoke(Object obj) {
            b1.this.f1015b.e(obj);
            return m.f5330a;
        }
    }

    public final class b extends k implements kotlin.d.a.b<E, m> {
        public b() {
            super(1);
        }

        public final Object invoke(Object obj) {
            b1.this.f1015b.f(obj);
            return m.f5330a;
        }
    }

    public final class c extends k implements kotlin.d.a.b<E, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ q f1018a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(q qVar) {
            super(1);
            this.f1018a = qVar;
        }

        public final Object invoke(Object obj) {
            this.f1018a.g(obj);
            return m.f5330a;
        }
    }

    public b1(kotlin.d.a.a aVar, ap apVar, ap apVar2) {
        this.f1014a = aVar;
        this.f1015b = apVar;
        this.c = apVar2;
    }

    public final void run() {
        bw bwVar = (bw) this.f1014a.invoke();
        bwVar.a(new a());
        bwVar.b(new b());
        if (!(bwVar instanceof q)) {
            bwVar = null;
        }
        q qVar = (q) bwVar;
        if (qVar != null) {
            this.c.i().b(new c(qVar));
        }
    }
}
