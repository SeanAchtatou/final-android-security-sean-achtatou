package b.b.a.j;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.a.ah;
import kotlin.g.c;
import kotlin.g.d;
import org.json.JSONArray;
import org.json.JSONObject;

public final class j {

    /* renamed from: a  reason: collision with root package name */
    public final JSONObject f1058a;

    public j(JSONObject jSONObject) {
        this.f1058a = jSONObject;
    }

    public final boolean a(q0 q0Var) {
        kotlin.d.b.j.b(q0Var, "key");
        JSONObject jSONObject = this.f1058a;
        if (jSONObject != null) {
            Object opt = jSONObject.opt(q0Var.f1141a);
            Boolean bool = null;
            if (opt == null || kotlin.d.b.j.a(opt, JSONObject.NULL)) {
                opt = null;
            }
            if (opt != null && (opt instanceof Boolean)) {
                bool = (Boolean) opt;
            }
            if (bool != null) {
                return bool.booleanValue();
            }
        }
        return false;
    }

    public final List<Integer> b(q0 q0Var) {
        JSONArray optJSONArray;
        kotlin.d.b.j.b(q0Var, "key");
        JSONObject jSONObject = this.f1058a;
        if (jSONObject == null || (optJSONArray = jSONObject.optJSONArray(q0Var.f1141a)) == null) {
            return null;
        }
        c b2 = d.b(0, optJSONArray.length());
        ArrayList arrayList = new ArrayList();
        Iterator it = b2.iterator();
        while (it.hasNext()) {
            arrayList.add(Integer.valueOf(optJSONArray.getInt(((ah) it).a())));
        }
        return arrayList;
    }

    public final List<List<String>> c(q0 q0Var) {
        JSONArray optJSONArray;
        ArrayList arrayList;
        kotlin.d.b.j.b(q0Var, "key");
        JSONObject jSONObject = this.f1058a;
        if (jSONObject == null || (optJSONArray = jSONObject.optJSONArray(q0Var.f1141a)) == null) {
            return null;
        }
        c b2 = d.b(0, optJSONArray.length());
        ArrayList arrayList2 = new ArrayList();
        Iterator it = b2.iterator();
        while (it.hasNext()) {
            JSONArray optJSONArray2 = optJSONArray.optJSONArray(((ah) it).a());
            if (optJSONArray2 != null) {
                c b3 = d.b(0, optJSONArray2.length());
                arrayList = new ArrayList();
                Iterator it2 = b3.iterator();
                while (it2.hasNext()) {
                    Object opt = optJSONArray2.opt(((ah) it2).a());
                    if (opt == null || kotlin.d.b.j.a(opt, JSONObject.NULL)) {
                        opt = null;
                    }
                    String str = (opt == null || !(opt instanceof String)) ? null : (String) opt;
                    if (str != null) {
                        arrayList.add(str);
                    }
                }
            } else {
                arrayList = null;
            }
            if (arrayList != null) {
                arrayList2.add(arrayList);
            }
        }
        return arrayList2;
    }

    public final List<String> d(q0 q0Var) {
        JSONArray optJSONArray;
        kotlin.d.b.j.b(q0Var, "key");
        JSONObject jSONObject = this.f1058a;
        if (jSONObject == null || (optJSONArray = jSONObject.optJSONArray(q0Var.f1141a)) == null) {
            return null;
        }
        c b2 = d.b(0, optJSONArray.length());
        ArrayList arrayList = new ArrayList();
        Iterator it = b2.iterator();
        while (it.hasNext()) {
            Object opt = optJSONArray.opt(((ah) it).a());
            if (opt == null || kotlin.d.b.j.a(opt, JSONObject.NULL)) {
                opt = null;
            }
            String str = (opt == null || !(opt instanceof String)) ? null : (String) opt;
            if (str != null) {
                arrayList.add(str);
            }
        }
        return arrayList;
    }

    public final long e(q0 q0Var) {
        kotlin.d.b.j.b(q0Var, "key");
        JSONObject jSONObject = this.f1058a;
        if (jSONObject != null) {
            Object opt = jSONObject.opt(q0Var.f1141a);
            Long l = null;
            if (opt == null || kotlin.d.b.j.a(opt, JSONObject.NULL)) {
                opt = null;
            }
            if (opt != null) {
                if (opt instanceof Integer) {
                    l = Long.valueOf((long) ((Number) opt).intValue());
                } else if (opt instanceof Long) {
                    l = (Long) opt;
                }
            }
            if (l != null) {
                return l.longValue();
            }
        }
        return 0;
    }

    public final String toString() {
        return String.valueOf(this.f1058a);
    }
}
