package b.b.a.j;

import kotlin.d.b.g;

public abstract class m<A, B> {

    public static final class a<A> extends m {

        /* renamed from: a  reason: collision with root package name */
        public final A f1097a;

        public a(A a2) {
            super(null);
            this.f1097a = a2;
        }
    }

    public static final class b<B> extends m {

        /* renamed from: a  reason: collision with root package name */
        public final B f1098a;

        public b(B b2) {
            super(null);
            this.f1098a = b2;
        }
    }

    public m() {
    }

    public /* synthetic */ m(g gVar) {
    }

    public final A a() {
        if (this instanceof a) {
            return ((a) this).f1097a;
        }
        return null;
    }

    public final B b() {
        if (this instanceof b) {
            return ((b) this).f1098a;
        }
        return null;
    }
}
