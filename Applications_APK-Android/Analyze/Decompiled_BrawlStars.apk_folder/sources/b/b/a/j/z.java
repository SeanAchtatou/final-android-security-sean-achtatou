package b.b.a.j;

import android.view.View;
import com.supercell.id.view.RootFrameLayout;
import kotlin.d.b.j;

public abstract class z implements View.OnAttachStateChangeListener, RootFrameLayout.a {

    /* renamed from: a  reason: collision with root package name */
    public RootFrameLayout f1202a;

    public void onViewAttachedToWindow(View view) {
        j.b(view, "v");
        this.f1202a = q1.f(view);
        RootFrameLayout rootFrameLayout = this.f1202a;
        if (rootFrameLayout != null) {
            rootFrameLayout.a(this);
        }
    }

    public void onViewDetachedFromWindow(View view) {
        j.b(view, "v");
        RootFrameLayout rootFrameLayout = this.f1202a;
        if (rootFrameLayout != null) {
            rootFrameLayout.b(this);
        }
        this.f1202a = null;
    }
}
