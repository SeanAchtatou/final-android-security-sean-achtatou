package b.b.a.j;

import android.text.style.ClickableSpan;
import android.view.View;
import kotlin.d.a.a;
import kotlin.d.b.j;
import kotlin.m;

public final class f extends ClickableSpan {

    /* renamed from: a  reason: collision with root package name */
    public final a<m> f1035a;

    public f(a<m> aVar) {
        j.b(aVar, "callback");
        this.f1035a = aVar;
    }

    public final void onClick(View view) {
        j.b(view, "widget");
        this.f1035a.invoke();
    }
}
