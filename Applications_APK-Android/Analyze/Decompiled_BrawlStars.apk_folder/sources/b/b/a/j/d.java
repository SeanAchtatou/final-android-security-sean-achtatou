package b.b.a.j;

import b.a.a.a.a;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    public final int f1023a;

    /* renamed from: b  reason: collision with root package name */
    public final int f1024b;

    public d(int i, int i2) {
        this.f1023a = i;
        this.f1024b = i2;
    }

    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof d) {
                d dVar = (d) obj;
                if (this.f1023a == dVar.f1023a) {
                    if (this.f1024b == dVar.f1024b) {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        return (this.f1023a * 31) + this.f1024b;
    }

    public final String toString() {
        StringBuilder a2 = a.a("AvatarBackground(startColor=");
        a2.append(this.f1023a);
        a2.append(", endColor=");
        a2.append(this.f1024b);
        a2.append(")");
        return a2.toString();
    }
}
