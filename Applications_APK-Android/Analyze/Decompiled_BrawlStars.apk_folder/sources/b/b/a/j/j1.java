package b.b.a.j;

import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.widget.TextView;
import java.lang.ref.WeakReference;
import kotlin.d.a.a;
import kotlin.d.a.c;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class j1 extends k implements c<Drawable, b.b.a.i.x1.c, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f1074a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ Rect f1075b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public j1(WeakReference weakReference, Rect rect) {
        super(2);
        this.f1074a = weakReference;
        this.f1075b = rect;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final Object invoke(Object obj, Object obj2) {
        a aVar;
        Drawable drawable = (Drawable) obj;
        j.b(drawable, "drawable");
        j.b((b.b.a.i.x1.c) obj2, "<anonymous parameter 1>");
        TextView textView = (TextView) this.f1074a.get();
        if (textView != null) {
            j.a((Object) textView, "this");
            Rect rect = this.f1075b;
            j.b(textView, "$this$setCompoundDrawableStart");
            j.b(drawable, "drawable");
            if (rect != null) {
                drawable.setBounds(rect);
                aVar = new k1(textView, drawable);
            } else {
                aVar = new l1(textView, drawable);
            }
            q1.a(textView, aVar);
        }
        return m.f5330a;
    }
}
