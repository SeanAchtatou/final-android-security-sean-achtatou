package b.b.a.j;

import android.os.Handler;
import kotlin.d.a.b;
import kotlin.d.b.k;
import kotlin.m;

public final class z0 extends k implements b<E, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ Handler f1203a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ Runnable f1204b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public z0(Handler handler, Runnable runnable) {
        super(1);
        this.f1203a = handler;
        this.f1204b = runnable;
    }

    public final Object invoke(Object obj) {
        this.f1203a.removeCallbacks(this.f1204b);
        return m.f5330a;
    }
}
