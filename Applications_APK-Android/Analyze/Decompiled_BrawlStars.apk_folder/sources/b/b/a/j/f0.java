package b.b.a.j;

import android.os.Parcel;
import android.os.Parcelable;
import b.b.a.d.c;
import com.facebook.internal.NativeProtocol;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.facebook.share.internal.ShareConstants;
import com.supercell.id.SupercellId;
import kotlin.d.b.g;
import kotlin.d.b.j;
import kotlin.h;
import kotlin.j.t;
import kotlin.k;

public final class f0 implements a0 {
    public static final Parcelable.Creator<f0> CREATOR = new a();
    public static final b e = new b(null);

    /* renamed from: a  reason: collision with root package name */
    public final String f1036a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1037b;
    public final String c;
    public final h<String, String> d;

    public final class a implements Parcelable.Creator<f0> {
        public final f0 createFromParcel(Parcel parcel) {
            j.b(parcel, ShareConstants.FEED_SOURCE_PARAM);
            String readString = parcel.readString();
            if (readString == null) {
                j.a();
            }
            String readString2 = parcel.readString();
            if (readString2 == null) {
                j.a();
            }
            String readString3 = parcel.readString();
            if (readString3 == null) {
                j.a();
            }
            String readString4 = parcel.readString();
            String readString5 = parcel.readString();
            return new f0(readString, readString2, readString3, (readString4 == null || readString5 == null) ? null : new h(readString4, readString5));
        }

        public final f0[] newArray(int i) {
            return new f0[i];
        }
    }

    public f0(String str, String str2, String str3, h<String, String> hVar) {
        j.b(str, "titleKey");
        j.b(str2, "textKey");
        j.b(str3, "buttonKey");
        this.f1036a = str;
        this.f1037b = str2;
        this.c = str3;
        this.d = hVar;
    }

    public final int describeContents() {
        return 0;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof f0)) {
            return false;
        }
        f0 f0Var = (f0) obj;
        return j.a(this.f1036a, f0Var.f1036a) && j.a(this.f1037b, f0Var.f1037b) && j.a(this.c, f0Var.c) && j.a(this.d, f0Var.d);
    }

    public final int hashCode() {
        String str = this.f1036a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.f1037b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        h<String, String> hVar = this.d;
        if (hVar != null) {
            i = hVar.hashCode();
        }
        return hashCode3 + i;
    }

    public final String toString() {
        StringBuilder a2 = b.a.a.a.a.a("NormalizedError(titleKey=");
        a2.append(this.f1036a);
        a2.append(", textKey=");
        a2.append(this.f1037b);
        a2.append(", buttonKey=");
        a2.append(this.c);
        a2.append(", textReplacement=");
        a2.append(this.d);
        a2.append(")");
        return a2.toString();
    }

    public static final class b {
        public /* synthetic */ b(g gVar) {
        }

        public final f0 a(Exception exc) {
            j.b(exc, "exception");
            if (exc instanceof c) {
                return new f0("api_error_server_heading", "api_error_server_description", "api_error_server_btn", k.a(NativeProtocol.BRIDGE_ARG_ERROR_CODE, String.valueOf(((c) exc).f36a)));
            }
            return a(exc instanceof b.b.a.d.b ? ((b.b.a.d.b) exc).f35a : MessengerShareContentUtility.TEMPLATE_GENERIC_TYPE);
        }

        public final f0 a(String str) {
            j.b(str, "error");
            if (t.a((CharSequence) str)) {
                str = MessengerShareContentUtility.TEMPLATE_GENERIC_TYPE;
            }
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Error", str, null, null, false, 28);
            if (SupercellId.INSTANCE.getSharedServices$supercellId_release().j.b("api_error_" + str + "_heading") == null) {
                str = MessengerShareContentUtility.TEMPLATE_GENERIC_TYPE;
            }
            return new f0("api_error_" + str + "_heading", "api_error_" + str + "_description", "api_error_" + str + "_btn", null);
        }
    }

    public final void writeToParcel(Parcel parcel, int i) {
        j.b(parcel, "dest");
        parcel.writeString(this.f1036a);
        parcel.writeString(this.f1037b);
        parcel.writeString(this.c);
        h<String, String> hVar = this.d;
        String str = null;
        parcel.writeString(hVar != null ? (String) hVar.f5262a : null);
        if (hVar != null) {
            str = (String) hVar.f5263b;
        }
        parcel.writeString(str);
    }
}
