package b.b.a.j;

import android.support.v7.util.DiffUtil;
import b.b.a.b;
import java.util.List;
import kotlin.a.ab;
import kotlin.d.b.g;

public class t0 extends DiffUtil.Callback {
    public static final a c = new a(null);

    /* renamed from: a  reason: collision with root package name */
    public final List<r0> f1172a;

    /* renamed from: b  reason: collision with root package name */
    public final List<r0> f1173b;

    /* JADX WARN: Type inference failed for: r2v0, types: [java.util.List<b.b.a.j.r0>, java.lang.Object, java.util.List<? extends b.b.a.j.r0>] */
    /* JADX WARN: Type inference failed for: r3v0, types: [java.util.List<b.b.a.j.r0>, java.lang.Object, java.util.List<? extends b.b.a.j.r0>] */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public t0(java.util.List<? extends b.b.a.j.r0> r2, java.util.List<? extends b.b.a.j.r0> r3) {
        /*
            r1 = this;
            java.lang.String r0 = "oldRows"
            kotlin.d.b.j.b(r2, r0)
            java.lang.String r0 = "newRows"
            kotlin.d.b.j.b(r3, r0)
            r1.<init>()
            r1.f1172a = r2
            r1.f1173b = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.j.t0.<init>(java.util.List, java.util.List):void");
    }

    public boolean a(int i, int i2) {
        return b.b(this.f1172a, i) == b.b(this.f1173b, i2) && b.a(this.f1172a, i) == b.a(this.f1173b, i2);
    }

    public boolean areContentsTheSame(int i, int i2) {
        return this.f1172a.get(i).b(this.f1173b.get(i2)) && a(i, i2);
    }

    public boolean areItemsTheSame(int i, int i2) {
        return this.f1172a.get(i).a(this.f1173b.get(i2));
    }

    public int getNewListSize() {
        return this.f1173b.size();
    }

    public int getOldListSize() {
        return this.f1172a.size();
    }

    public static final class a {
        public /* synthetic */ a(g gVar) {
        }

        public final t0 a(List<? extends r0> list, List<? extends r0> list2) {
            if (list == null) {
                list = ab.f5210a;
            }
            if (list2 == null) {
                list2 = ab.f5210a;
            }
            return new t0(list, list2);
        }
    }
}
