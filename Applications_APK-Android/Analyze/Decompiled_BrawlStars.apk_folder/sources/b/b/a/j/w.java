package b.b.a.j;

import b.b.a.h.f;
import java.util.ArrayList;
import java.util.List;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class w extends k implements b<List<? extends f>, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ b f1186a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public w(b bVar) {
        super(1);
        this.f1186a = bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.p.a(java.lang.Iterable, int):int
     arg types: [java.util.List<b.b.a.h.f>, int]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.p.a(java.lang.Iterable, int):int */
    public final void a(List<f> list) {
        j.b(list, "list");
        b bVar = this.f1186a;
        ArrayList arrayList = new ArrayList(kotlin.a.m.a((Iterable) list, 10));
        for (f fVar : list) {
            arrayList.add(fVar.f116a);
        }
        bVar.invoke(arrayList);
    }

    public final /* synthetic */ Object invoke(Object obj) {
        a((List) obj);
        return m.f5330a;
    }
}
