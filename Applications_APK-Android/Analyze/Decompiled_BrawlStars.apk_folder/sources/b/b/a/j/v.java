package b.b.a.j;

import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;
import nl.komponents.kovenant.ap;

public final class v extends k implements b<Exception, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ ap f1178a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public v(ap apVar) {
        super(1);
        this.f1178a = apVar;
    }

    public final Object invoke(Object obj) {
        Exception exc = (Exception) obj;
        j.b(exc, "it");
        this.f1178a.f(f0.e.a(exc));
        return m.f5330a;
    }
}
