package b.b.a.j;

import com.facebook.internal.NativeProtocol;
import com.supercell.id.IdFriend;
import java.util.ArrayList;
import java.util.List;
import kotlin.d.b.g;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class t extends v0<s> {
    public final kotlin.d.a.b<s, m> d;

    public static abstract class a implements a<s> {

        /* renamed from: b.b.a.j.t$a$a  reason: collision with other inner class name */
        public static final class C0106a extends a {

            /* renamed from: a  reason: collision with root package name */
            public final List<IdFriend> f1168a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0106a(List<IdFriend> list) {
                super(null);
                j.b(list, "newFriends");
                this.f1168a = list;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: kotlin.a.p.a(java.lang.Iterable, int):int
             arg types: [java.util.List<com.supercell.id.IdFriend>, int]
             candidates:
              kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
              kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
              kotlin.a.w.a(java.util.List, int):T
              kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
              kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
              kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
              kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
              kotlin.a.s.a(java.util.List, java.util.Comparator):void
              kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
              kotlin.a.p.a(java.lang.Iterable, int):int */
            /* renamed from: a */
            public final s invoke(s sVar) {
                if (sVar == null) {
                    return null;
                }
                List<IdFriend> list = this.f1168a;
                ArrayList arrayList = new ArrayList(kotlin.a.m.a((Iterable) list, 10));
                for (IdFriend supercellId : list) {
                    arrayList.add(supercellId.getSupercellId());
                }
                List<IdFriend> list2 = sVar.f1162a;
                ArrayList arrayList2 = new ArrayList();
                for (T next : list2) {
                    if (!arrayList.contains(((IdFriend) next).getSupercellId())) {
                        arrayList2.add(next);
                    }
                }
                return new s(kotlin.a.m.d(arrayList2, this.f1168a), false);
            }

            public final boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof C0106a) && j.a(this.f1168a, ((C0106a) obj).f1168a);
                }
                return true;
            }

            public final int hashCode() {
                List<IdFriend> list = this.f1168a;
                if (list != null) {
                    return list.hashCode();
                }
                return 0;
            }

            public final String toString() {
                StringBuilder a2 = b.a.a.a.a.a("Add(newFriends=");
                a2.append(this.f1168a);
                a2.append(")");
                return a2.toString();
            }
        }

        public static final class b extends a {

            /* renamed from: a  reason: collision with root package name */
            public final String f1169a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(String str) {
                super(null);
                j.b(str, "scid");
                this.f1169a = str;
            }

            public final boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof b) && j.a(this.f1169a, ((b) obj).f1169a);
                }
                return true;
            }

            public final int hashCode() {
                String str = this.f1169a;
                if (str != null) {
                    return str.hashCode();
                }
                return 0;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
             arg types: [java.lang.String, java.lang.String]
             candidates:
              kotlin.d.b.j.a(int, int):int
              kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
              kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
              kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
            public final Object invoke(Object obj) {
                s sVar = (s) obj;
                if (sVar == null) {
                    return null;
                }
                List<IdFriend> list = sVar.f1162a;
                ArrayList arrayList = new ArrayList();
                for (T next : list) {
                    if (!j.a((Object) ((IdFriend) next).getSupercellId(), (Object) this.f1169a)) {
                        arrayList.add(next);
                    }
                }
                return new s(arrayList, false);
            }

            public final String toString() {
                return b.a.a.a.a.a(b.a.a.a.a.a("Remove(scid="), this.f1169a, ")");
            }
        }

        public static final class c extends a {

            /* renamed from: a  reason: collision with root package name */
            public final List<IdFriend> f1170a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(List<IdFriend> list) {
                super(null);
                j.b(list, NativeProtocol.AUDIENCE_FRIENDS);
                this.f1170a = list;
            }

            public final boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof c) && j.a(this.f1170a, ((c) obj).f1170a);
                }
                return true;
            }

            public final int hashCode() {
                List<IdFriend> list = this.f1170a;
                if (list != null) {
                    return list.hashCode();
                }
                return 0;
            }

            public final Object invoke(Object obj) {
                return new s(this.f1170a, true);
            }

            public final String toString() {
                StringBuilder a2 = b.a.a.a.a.a("ResetTo(friends=");
                a2.append(this.f1170a);
                a2.append(")");
                return a2.toString();
            }
        }

        public /* synthetic */ a(g gVar) {
        }
    }

    public final class b extends k implements kotlin.d.a.b<s, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ kotlin.d.a.b f1171a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(kotlin.d.a.b bVar) {
            super(1);
            this.f1171a = bVar;
        }

        public final Object invoke(Object obj) {
            s sVar = (s) obj;
            if (sVar != null && !sVar.f1163b) {
                this.f1171a.invoke(sVar.f1162a);
            }
            return m.f5330a;
        }
    }

    public t(kotlin.d.a.b<? super List<IdFriend>, m> bVar) {
        j.b(bVar, "onChanges");
        this.d = new b(bVar);
        b(this.d);
    }
}
