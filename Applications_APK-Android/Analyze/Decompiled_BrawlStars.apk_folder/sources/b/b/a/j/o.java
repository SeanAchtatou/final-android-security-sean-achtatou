package b.b.a.j;

import b.a.a.a.a;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.supercell.id.R;
import kotlin.d.b.j;

public final class o implements r0 {

    /* renamed from: a  reason: collision with root package name */
    public final int f1107a = R.layout.list_item_error;

    /* renamed from: b  reason: collision with root package name */
    public final f0 f1108b;

    public o(f0 f0Var) {
        j.b(f0Var, "error");
        this.f1108b = f0Var;
    }

    public final int a() {
        return this.f1107a;
    }

    public final boolean a(r0 r0Var) {
        j.b(r0Var, FacebookRequestErrorClassification.KEY_OTHER);
        return r0Var instanceof o;
    }

    public final boolean b(r0 r0Var) {
        j.b(r0Var, FacebookRequestErrorClassification.KEY_OTHER);
        if (!(r0Var instanceof o)) {
            return false;
        }
        return j.a(this.f1108b, ((o) r0Var).f1108b);
    }

    public final boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof o) && j.a(this.f1108b, ((o) obj).f1108b);
        }
        return true;
    }

    public final int hashCode() {
        f0 f0Var = this.f1108b;
        if (f0Var != null) {
            return f0Var.hashCode();
        }
        return 0;
    }

    public final String toString() {
        StringBuilder a2 = a.a("ErrorRow(error=");
        a2.append(this.f1108b);
        a2.append(")");
        return a2.toString();
    }
}
