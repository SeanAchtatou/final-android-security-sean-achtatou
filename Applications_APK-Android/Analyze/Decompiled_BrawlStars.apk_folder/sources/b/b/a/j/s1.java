package b.b.a.j;

import android.support.animation.DynamicAnimation;
import android.support.animation.SpringAnimation;
import android.support.animation.SpringForce;
import b.b.a.j.q1;
import kotlin.d.b.j;

public final class s1 implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ q1.i f1167a;

    public s1(q1.i iVar) {
        this.f1167a = iVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.animation.SpringForce, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void run() {
        SpringAnimation springAnimation = new SpringAnimation(this.f1167a.f1158a, DynamicAnimation.SCALE_X, 1.0f);
        SpringForce spring = springAnimation.getSpring();
        j.a((Object) spring, "spring");
        spring.setDampingRatio(0.5f);
        SpringForce spring2 = springAnimation.getSpring();
        j.a((Object) spring2, "spring");
        spring2.setStiffness(500.0f);
        springAnimation.start();
        SpringAnimation springAnimation2 = new SpringAnimation(this.f1167a.f1158a, DynamicAnimation.SCALE_Y, 1.0f);
        SpringForce spring3 = springAnimation2.getSpring();
        j.a((Object) spring3, "spring");
        spring3.setDampingRatio(0.5f);
        SpringForce spring4 = springAnimation2.getSpring();
        j.a((Object) spring4, "spring");
        spring4.setStiffness(500.0f);
        springAnimation2.start();
    }
}
