package b.b.a.j;

import com.supercell.id.SupercellId;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.d.b.g;
import kotlin.d.b.j;
import kotlin.h;
import kotlin.j.t;
import kotlin.k;
import org.json.JSONException;
import org.json.JSONObject;

public enum q0 {
    MAINTENANCE("maintenance"),
    FRIENDS_CACHE_LIFETIME("friendsCacheTime"),
    FRIENDS_REFRESH_RATE("friendsRefreshRate"),
    SMS_ENABLED("smsLogin"),
    SMS_REGIONS("smsRegions"),
    NAMES("names"),
    SORT_ORDER("sortOrder"),
    GRADIENTS("gradients"),
    NAME_LENGTH_LIMITS("nameLengthLimits");
    
    public static final a l = new a(null);

    /* renamed from: a  reason: collision with root package name */
    public final String f1141a;

    /* access modifiers changed from: public */
    q0(String str) {
        this.f1141a = str;
    }

    public static final class a {
        public /* synthetic */ a(g gVar) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [java.util.Iterator<java.lang.String>, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.j.ab.a(java.lang.String, java.lang.String, boolean):boolean
         arg types: [java.lang.String, java.lang.String, int]
         candidates:
          kotlin.j.ac.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean
          kotlin.j.y.a(java.lang.Appendable, java.lang.Object, kotlin.d.a.b):void
          kotlin.j.ab.a(java.lang.String, java.lang.String, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void a(Map<String, h<Integer, Object>> map, JSONObject jSONObject, int i) {
            Iterator<String> keys = jSONObject.keys();
            j.a((Object) keys, "currentConf.keys()");
            while (keys.hasNext()) {
                String next = keys.next();
                Object obj = jSONObject.get(next);
                if (!(obj instanceof JSONObject)) {
                    h hVar = map.get(next);
                    if (hVar == null || ((Number) hVar.f5262a).intValue() < i) {
                        j.a((Object) next, "key");
                        map.put(next, k.a(Integer.valueOf(i), obj));
                    }
                } else if (t.a(next, "Android", true) || t.a(next, "portraits", true) || t.a(next, SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getGame(), true)) {
                    a(map, (JSONObject) obj, i + 1);
                }
            }
        }

        public final j b(JSONObject jSONObject) {
            j.b(jSONObject, "json");
            try {
                return new j(a(jSONObject));
            } catch (JSONException unused) {
                return new j(null);
            }
        }

        public final JSONObject a(JSONObject jSONObject) {
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            a(linkedHashMap, jSONObject, 0);
            JSONObject jSONObject2 = new JSONObject();
            for (Map.Entry entry : linkedHashMap.entrySet()) {
                jSONObject2.put((String) entry.getKey(), ((h) entry.getValue()).f5263b);
            }
            return jSONObject2;
        }
    }
}
