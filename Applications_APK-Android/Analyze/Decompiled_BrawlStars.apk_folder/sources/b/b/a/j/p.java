package b.b.a.j;

import android.content.Context;
import android.os.Build;
import android.support.v4.view.GravityCompat;
import android.text.Annotation;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.TransformationMethod;
import android.view.View;
import android.widget.TextView;
import com.facebook.internal.ServerProtocol;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyTypefaceSpan;
import io.github.inflationx.calligraphy3.CalligraphyUtils;
import io.github.inflationx.calligraphy3.TypefaceUtils;
import java.util.Iterator;
import java.util.Locale;
import kotlin.TypeCastException;
import kotlin.d.b.j;
import kotlin.j.o;
import kotlin.j.s;

public final class p {

    /* renamed from: a  reason: collision with root package name */
    public static final CalligraphyConfig f1115a;

    /* renamed from: b  reason: collision with root package name */
    public static final o f1116b = new o("[\\u0600-\\u06FF]+(\\u0020[\\u0600-\\u06FF]+)*", s.IGNORE_CASE);
    public static final boolean c = (Build.VERSION.SDK_INT >= 17);

    public final class a implements TextWatcher {

        /* renamed from: a  reason: collision with root package name */
        public boolean f1117a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ TextView f1118b;

        public a(TextView textView) {
            this.f1118b = textView;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.Object[], java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.text.Annotation, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
         arg types: [java.lang.String, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void afterTextChanged(Editable editable) {
            boolean z;
            if (editable != null) {
                Object[] spans = editable.getSpans(0, editable.length(), Annotation.class);
                j.a((Object) spans, "s.getSpans(0, s.length, Annotation::class.java)");
                int length = spans.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        z = false;
                        break;
                    }
                    Annotation annotation = (Annotation) spans[i];
                    j.a((Object) annotation, "it");
                    if (j.a((Object) annotation.getKey(), (Object) "SupercellIdApplyFixes")) {
                        z = true;
                        break;
                    }
                    i++;
                }
                if (!z) {
                    if (this.f1117a) {
                        String obj = editable.toString();
                        Locale locale = SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getLocale();
                        if (obj != null) {
                            String upperCase = obj.toUpperCase(locale);
                            j.a((Object) upperCase, "(this as java.lang.String).toUpperCase(locale)");
                            if (!j.a((Object) editable.toString(), (Object) upperCase)) {
                                editable.replace(0, editable.length(), editable.toString());
                            }
                        } else {
                            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                        }
                    }
                    Iterator a2 = o.a(p.f1116b, editable, 0, 2).a();
                    while (a2.hasNext()) {
                        kotlin.j.j jVar = (kotlin.j.j) a2.next();
                        editable.setSpan(new b(), jVar.a().f5258a, jVar.a().f5259b + 1, 17);
                    }
                    editable.setSpan(new Annotation("SupercellIdApplyFixes", ServerProtocol.DIALOG_RETURN_SCOPES_TRUE), 0, editable.length(), 17);
                }
            }
        }

        public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
         arg types: [java.lang.String, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
        public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            boolean z;
            Class<?> cls;
            if (charSequence != null) {
                o oVar = p.f1116b;
                j.b(charSequence, "input");
                if (oVar.f5321a.matcher(charSequence).find()) {
                    TextView textView = this.f1118b;
                    j.b(textView, "textView");
                    if (Build.VERSION.SDK_INT >= 28) {
                        z = textView.isAllCaps();
                    } else {
                        TransformationMethod transformationMethod = textView.getTransformationMethod();
                        z = j.a((Object) ((transformationMethod == null || (cls = transformationMethod.getClass()) == null) ? null : cls.getCanonicalName()), (Object) "android.text.method.AllCapsTransformationMethod");
                    }
                    if (z) {
                        this.f1118b.setAllCaps(false);
                        this.f1117a = true;
                    }
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [io.github.inflationx.calligraphy3.CalligraphyConfig, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    static {
        CalligraphyConfig build = new CalligraphyConfig.Builder().setDefaultFontPath("fonts/SupercellTextAndroid_ACorp_Md.ttf").setFontAttrId(R.attr.fontPath).build();
        j.a((Object) build, "CalligraphyConfig.Builde…ontPath)\n        .build()");
        f1115a = build;
    }

    public static final View a(View view) {
        TextView textView = (TextView) (!(view instanceof TextView) ? null : view);
        if (textView == null) {
            return view;
        }
        if (!c) {
            if ((textView.getGravity() & GravityCompat.START) == 8388611) {
                textView.setGravity((textView.getGravity() ^ GravityCompat.START) | 3);
            }
            if ((textView.getGravity() & GravityCompat.END) == 8388613) {
                textView.setGravity((textView.getGravity() ^ GravityCompat.END) | 5);
            }
        }
        textView.addTextChangedListener(new a(textView));
        return textView;
    }

    public static final CalligraphyTypefaceSpan a(Context context, String str) {
        j.b(context, "context");
        j.b(str, "fontPath");
        return new CalligraphyTypefaceSpan(TypefaceUtils.load(context.getAssets(), str));
    }

    public static final void a(TextView textView, String str) {
        j.b(textView, "$this$applyFont");
        CalligraphyUtils.applyFontToTextView(textView.getContext(), textView, f1115a, str);
        a(textView);
    }

    public static /* synthetic */ void a(TextView textView, String str, int i) {
        if ((i & 1) != 0) {
            str = null;
        }
        j.b(textView, "$this$applyFont");
        CalligraphyUtils.applyFontToTextView(textView.getContext(), textView, f1115a, str);
        a(textView);
    }
}
