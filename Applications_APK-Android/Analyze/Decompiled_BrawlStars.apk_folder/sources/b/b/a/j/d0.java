package b.b.a.j;

import android.graphics.Bitmap;
import kotlin.d.a.b;
import kotlin.d.b.k;

public final class d0 extends k implements b<Bitmap, Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    public static final d0 f1025a = new d0();

    public d0() {
        super(1);
    }

    public final Object invoke(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        if (bitmap != null) {
            return bitmap;
        }
        throw new Exception("Invalid bitmap stream");
    }
}
