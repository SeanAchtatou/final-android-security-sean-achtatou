package b.b.a.j;

import android.graphics.drawable.Drawable;
import android.support.v4.widget.TextViewCompat;
import android.widget.TextView;
import kotlin.d.a.a;
import kotlin.d.b.k;
import kotlin.m;

public final class k1 extends k implements a<m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ TextView f1086a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ Drawable f1087b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k1(TextView textView, Drawable drawable) {
        super(0);
        this.f1086a = textView;
        this.f1087b = drawable;
    }

    public final Object invoke() {
        TextViewCompat.setCompoundDrawablesRelative(this.f1086a, this.f1087b, null, null, null);
        return m.f5330a;
    }
}
