package b.b.a.j;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import java.util.Iterator;
import java.util.List;
import kotlin.d.b.j;

public final class d1 extends FragmentPagerAdapter {

    /* renamed from: a  reason: collision with root package name */
    public List<c1> f1026a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d1(FragmentManager fragmentManager, List<c1> list) {
        super(fragmentManager);
        j.b(fragmentManager, "fm");
        j.b(list, "tabs");
        this.f1026a = list;
    }

    public final int getCount() {
        return this.f1026a.size();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [? extends android.support.v4.app.Fragment, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final Fragment getItem(int i) {
        Object newInstance = this.f1026a.get(i).f.newInstance();
        j.a((Object) newInstance, "tabs[position].fragmentClass.newInstance()");
        return (Fragment) newInstance;
    }

    public final long getItemId(int i) {
        return (long) this.f1026a.get(i).f.hashCode();
    }

    public final int getItemPosition(Object obj) {
        j.b(obj, "object");
        Class<?> cls = obj.getClass();
        Iterator<c1> it = this.f1026a.iterator();
        int i = 0;
        while (true) {
            if (!it.hasNext()) {
                i = -1;
                break;
            } else if (j.a(it.next().f, cls)) {
                break;
            } else {
                i++;
            }
        }
        if (i > -1) {
            return i;
        }
        return -2;
    }
}
