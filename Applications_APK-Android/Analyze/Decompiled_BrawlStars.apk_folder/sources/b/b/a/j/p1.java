package b.b.a.j;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.View;
import b.b.a.g.b;
import com.supercell.id.R;
import io.github.inflationx.viewpump.InflateResult;
import io.github.inflationx.viewpump.Interceptor;
import kotlin.d.b.j;

public final class p1 implements Interceptor {
    /* JADX INFO: finally extract failed */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.Context, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [io.github.inflationx.viewpump.InflateResult, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final InflateResult intercept(Interceptor.Chain chain) {
        j.b(chain, "chain");
        InflateResult proceed = chain.proceed(chain.request());
        View view = proceed.view();
        Context context = proceed.context();
        j.a((Object) context, "result.context()");
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(proceed.attrs(), R.styleable.View, 0, 0);
        try {
            int integer = obtainStyledAttributes.getInteger(R.styleable.View_insetSystemWindow, 0);
            boolean z = obtainStyledAttributes.getBoolean(R.styleable.View_forceInsetSystemWindow, false);
            int resourceId = obtainStyledAttributes.getResourceId(R.styleable.View_insetSystemWindowUsingParent, 0);
            boolean z2 = obtainStyledAttributes.getBoolean(R.styleable.View_dropShadow, false);
            int color = obtainStyledAttributes.getColor(R.styleable.View_dropShadowColor, 335544320);
            float dimension = obtainStyledAttributes.getDimension(R.styleable.View_dropShadowCornerRadius, b.f.b());
            float dimension2 = obtainStyledAttributes.getDimension(R.styleable.View_dropShadowBlur, b.f.a());
            float dimension3 = obtainStyledAttributes.getDimension(R.styleable.View_dropShadowDY, b.f.c());
            boolean z3 = obtainStyledAttributes.getBoolean(R.styleable.View_innerShadow, false);
            if (!(integer == 0 || view == null)) {
                q1.a(view, integer, z, resourceId);
            }
            int integer2 = obtainStyledAttributes.getInteger(R.styleable.View_audioEffect, -1);
            int integer3 = obtainStyledAttributes.getInteger(R.styleable.View_animateOnPress, -1);
            if (integer3 >= 0 || integer2 >= 0) {
                if (view != null) {
                    q1.a(view, integer3, integer2);
                }
                if (integer2 >= 0 && view != null) {
                    view.setSoundEffectsEnabled(false);
                }
            }
            if (z2 && view != null) {
                b.b.a.b.a(view, color, dimension2, dimension3, dimension, null, 16);
            }
            if (z3 && view != null) {
                q1.a(view, 0.0f, 1.0f, 1.0f, 0.1f, 8.0f);
            }
            obtainStyledAttributes.recycle();
            InflateResult build = proceed.toBuilder().view(view).build();
            j.a((Object) build, "result.toBuilder().view(viewWithInsets).build()");
            return build;
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
            throw th;
        }
    }
}
