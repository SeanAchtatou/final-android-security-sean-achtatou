package b.b.a.j;

import b.b.a.j.t;
import com.facebook.internal.NativeProtocol;
import com.supercell.id.IdFriend;
import java.util.List;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;
import nl.komponents.kovenant.ap;

public final class u extends k implements b<List<? extends IdFriend>, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ t f1174a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ ap f1175b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public u(t tVar, ap apVar) {
        super(1);
        this.f1174a = tVar;
        this.f1175b = apVar;
    }

    public final Object invoke(Object obj) {
        List list = (List) obj;
        j.b(list, NativeProtocol.AUDIENCE_FRIENDS);
        this.f1174a.a(new t.a.c(list));
        this.f1175b.e(list);
        return m.f5330a;
    }
}
