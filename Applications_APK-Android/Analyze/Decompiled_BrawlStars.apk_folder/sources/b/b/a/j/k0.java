package b.b.a.j;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.supercell.id.SupercellId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.a.ab;
import kotlin.a.g;
import kotlin.d.a.e;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.j.t;
import kotlin.m;

public final class k0 {

    /* renamed from: a  reason: collision with root package name */
    public static final Integer f1077a;

    /* renamed from: b  reason: collision with root package name */
    public static final k0 f1078b = new k0();

    public final class a extends k implements kotlin.d.a.c<Drawable, b.b.a.i.x1.c, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ e f1079a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ Resources f1080b;
        public final /* synthetic */ ImageView c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(e eVar, Resources resources, ImageView imageView) {
            super(2);
            this.f1079a = eVar;
            this.f1080b = resources;
            this.c = imageView;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.graphics.Bitmap, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final Object invoke(Object obj, Object obj2) {
            Bitmap bitmap;
            Drawable drawable = (Drawable) obj;
            j.b(drawable, "avatarAtlas");
            j.b((b.b.a.i.x1.c) obj2, "<anonymous parameter 1>");
            e eVar = this.f1079a;
            String str = eVar.f1027a;
            d dVar = eVar.f1028b;
            int i = dVar.f1023a;
            int i2 = dVar.f1024b;
            Resources resources = this.f1080b;
            j.b(drawable, "avatarAtlas");
            j.b(str, "name");
            j.b(resources, "resources");
            List<String> d = SupercellId.INSTANCE.getRemoteConfiguration$supercellId_release().d(q0.NAMES);
            int indexOf = d != null ? d.indexOf(str) : -1;
            Drawable drawable2 = null;
            if (indexOf >= 0) {
                j.b(drawable, "atlas");
                if (indexOf < 0 || !(drawable instanceof BitmapDrawable)) {
                    bitmap = null;
                } else {
                    Bitmap bitmap2 = ((BitmapDrawable) drawable).getBitmap();
                    j.a((Object) bitmap2, "bitmapAtlas");
                    float width = (((float) bitmap2.getWidth()) / 10.0f) / 90.0f;
                    int i3 = (int) (width * 90.0f);
                    bitmap = Bitmap.createBitmap(bitmap2, (int) (((float) ((indexOf % 10) * 90)) * width), (int) (((float) ((indexOf / 10) * 90)) * width), i3, i3);
                }
                if (bitmap != null) {
                    drawable2 = l.f1088a.a(bitmap, i, i2, resources);
                }
            }
            if (drawable2 != null) {
                ImageView imageView = this.c;
                if (imageView != null) {
                    imageView.setImageDrawable(drawable2);
                }
            } else {
                k0.f1078b.a(this.c, this.f1080b);
            }
            return m.f5330a;
        }
    }

    public final class b extends k implements kotlin.d.a.c<Drawable, b.b.a.i.x1.c, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ int f1081a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ e f1082b;
        public final /* synthetic */ e c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(int i, e eVar, e eVar2) {
            super(2);
            this.f1081a = i;
            this.f1082b = eVar;
            this.c = eVar2;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.graphics.Bitmap, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final Object invoke(Object obj, Object obj2) {
            Bitmap bitmap;
            Drawable drawable = (Drawable) obj;
            j.b(drawable, "avatarAtlas");
            j.b((b.b.a.i.x1.c) obj2, "<anonymous parameter 1>");
            int i = this.f1081a;
            j.b(drawable, "atlas");
            if (i < 0 || !(drawable instanceof BitmapDrawable)) {
                bitmap = null;
            } else {
                Bitmap bitmap2 = ((BitmapDrawable) drawable).getBitmap();
                j.a((Object) bitmap2, "bitmapAtlas");
                float width = (((float) bitmap2.getWidth()) / 10.0f) / 90.0f;
                int i2 = (int) (width * 90.0f);
                bitmap = Bitmap.createBitmap(bitmap2, (int) (((float) ((i % 10) * 90)) * width), (int) (((float) ((i / 10) * 90)) * width), i2, i2);
            }
            if (bitmap != null) {
                l lVar = l.f1088a;
                d dVar = this.f1082b.f1028b;
                lVar.a(bitmap, dVar.f1023a, dVar.f1024b, this.c);
            } else {
                this.c.a(null, 0, 0, 0);
            }
            return m.f5330a;
        }
    }

    public final class c extends k implements kotlin.d.a.c<Drawable, b.b.a.i.x1.c, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ e f1083a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(e eVar) {
            super(2);
            this.f1083a = eVar;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.graphics.Bitmap, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final Object invoke(Object obj, Object obj2) {
            Drawable drawable = (Drawable) obj;
            j.b(drawable, "drawable");
            j.b((b.b.a.i.x1.c) obj2, "<anonymous parameter 1>");
            if (!(drawable instanceof BitmapDrawable)) {
                drawable = null;
            }
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable != null) {
                l lVar = l.f1088a;
                Bitmap bitmap = bitmapDrawable.getBitmap();
                j.a((Object) bitmap, "it.bitmap");
                lVar.a(bitmap, e.e.a().f1023a, e.e.a().f1024b, this.f1083a);
            } else {
                this.f1083a.a(null, 0, 0, 0);
            }
            return m.f5330a;
        }
    }

    public final class d extends k implements kotlin.d.a.c<Drawable, b.b.a.i.x1.c, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ ImageView f1084a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ Resources f1085b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ImageView imageView, Resources resources) {
            super(2);
            this.f1084a = imageView;
            this.f1085b = resources;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.graphics.Bitmap, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final Object invoke(Object obj, Object obj2) {
            Drawable drawable = (Drawable) obj;
            j.b(drawable, "drawable");
            j.b((b.b.a.i.x1.c) obj2, "<anonymous parameter 1>");
            if (drawable instanceof BitmapDrawable) {
                ImageView imageView = this.f1084a;
                if (imageView != null) {
                    l lVar = l.f1088a;
                    Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                    j.a((Object) bitmap, "drawable.bitmap");
                    imageView.setImageDrawable(lVar.a(bitmap, -1447447, -1447447, this.f1085b));
                }
            } else {
                ImageView imageView2 = this.f1084a;
                if (imageView2 != null) {
                    imageView2.setImageDrawable(null);
                }
            }
            return m.f5330a;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.a(java.util.List, int):T
     arg types: [java.util.List<java.lang.Integer>, int]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.w.a(java.util.List, int):T */
    static {
        List<Integer> b2 = SupercellId.INSTANCE.getRemoteConfiguration$supercellId_release().b(q0.NAME_LENGTH_LIMITS);
        f1077a = b2 != null ? (Integer) kotlin.a.m.a((List) b2, 1) : null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.a(java.util.List, int):T
     arg types: [java.util.List<java.lang.Integer>, int]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.w.a(java.util.List, int):T */
    public final b.b.a.d.b a(String str) {
        j.b(str, "name");
        List<Integer> b2 = SupercellId.INSTANCE.getRemoteConfiguration$supercellId_release().b(q0.NAME_LENGTH_LIMITS);
        Integer num = b2 != null ? (Integer) kotlin.a.m.a((List) b2, 0) : null;
        Integer num2 = b2 != null ? (Integer) kotlin.a.m.a((List) b2, 1) : null;
        if (t.a((CharSequence) str) || (num != null && j.a(str.length(), num.intValue()) < 0)) {
            return new b.b.a.d.b("profile_name_too_short");
        }
        if (num2 == null || j.a(str.length(), num2.intValue()) <= 0) {
            return null;
        }
        return new b.b.a.d.b("profile_name_too_long");
    }

    public final void a(e eVar, ImageView imageView, Resources resources) {
        j.b(eVar, MessengerShareContentUtility.ELEMENTS);
        j.b(resources, "resources");
        SupercellId.INSTANCE.getSharedServices$supercellId_release().j.a("portraits.png", new a(eVar, resources, imageView));
    }

    public final Integer c() {
        return f1077a;
    }

    public final void a(String str, ImageView imageView, Resources resources) {
        j.b(resources, "resources");
        e a2 = str != null ? e.e.a(str) : null;
        if (a2 != null) {
            a(a2, imageView, resources);
        } else {
            SupercellId.INSTANCE.getSharedServices$supercellId_release().j.a("unknown_user.png", new d(imageView, resources));
        }
    }

    public final void a(String str, e<? super byte[], ? super Integer, ? super Integer, ? super Integer, m> eVar) {
        int i;
        String str2;
        j.b(eVar, "callback");
        Integer num = null;
        e a2 = str != null ? e.e.a(str) : null;
        if (!(a2 == null || (str2 = a2.f1027a) == null)) {
            List<String> d2 = SupercellId.INSTANCE.getRemoteConfiguration$supercellId_release().d(q0.NAMES);
            if (d2 != null) {
                num = Integer.valueOf(d2.indexOf(str2));
            }
            if (num != null) {
                i = num.intValue();
                if (a2 != null || i < 0) {
                    SupercellId.INSTANCE.getSharedServices$supercellId_release().j.a("unknown_user.png", new c(eVar));
                } else {
                    SupercellId.INSTANCE.getSharedServices$supercellId_release().j.a("portraits.png", new b(i, a2, eVar));
                    return;
                }
            }
        }
        i = -1;
        if (a2 != null) {
        }
        SupercellId.INSTANCE.getSharedServices$supercellId_release().j.a("unknown_user.png", new c(eVar));
    }

    public final void a(ImageView imageView, Resources resources) {
        SupercellId.INSTANCE.getSharedServices$supercellId_release().j.a("unknown_user.png", new d(imageView, resources));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.p.a(java.lang.Iterable, int):int
     arg types: [java.util.List<java.lang.Integer>, int]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.p.a(java.lang.Iterable, int):int */
    public final List<String> b() {
        List<String> d2 = SupercellId.INSTANCE.getRemoteConfiguration$supercellId_release().d(q0.NAMES);
        if (d2 == null) {
            d2 = ab.f5210a;
        }
        List<Integer> b2 = SupercellId.INSTANCE.getRemoteConfiguration$supercellId_release().b(q0.SORT_ORDER);
        if (b2 == null) {
            b2 = ab.f5210a;
        }
        if (!a(b2, d2.size())) {
            return d2;
        }
        ArrayList arrayList = new ArrayList(kotlin.a.m.a((Iterable) b2, 10));
        for (Number intValue : b2) {
            arrayList.add(d2.get(intValue.intValue()));
        }
        return arrayList;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0045 A[SYNTHETIC, Splitter:B:14:0x0045] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x001c A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<b.b.a.j.d> a() {
        /*
            r6 = this;
            com.supercell.id.SupercellId r0 = com.supercell.id.SupercellId.INSTANCE
            b.b.a.j.j r0 = r0.getRemoteConfiguration$supercellId_release()
            b.b.a.j.q0 r1 = b.b.a.j.q0.GRADIENTS
            java.util.List r0 = r0.c(r1)
            if (r0 == 0) goto L_0x000f
            goto L_0x0013
        L_0x000f:
            kotlin.a.ab r0 = kotlin.a.ab.f5210a
            java.util.List r0 = (java.util.List) r0
        L_0x0013:
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.util.Iterator r0 = r0.iterator()
        L_0x001c:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L_0x0073
            java.lang.Object r2 = r0.next()
            java.util.List r2 = (java.util.List) r2
            r3 = 0
            java.lang.Object r3 = kotlin.a.m.a(r2, r3)
            java.lang.String r3 = (java.lang.String) r3
            r4 = 0
            if (r3 == 0) goto L_0x003b
            int r3 = android.graphics.Color.parseColor(r3)     // Catch:{ Exception -> 0x003b }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x003b }
            goto L_0x003c
        L_0x003b:
            r3 = r4
        L_0x003c:
            r5 = 1
            java.lang.Object r2 = kotlin.a.m.a(r2, r5)
            java.lang.String r2 = (java.lang.String) r2
            if (r2 == 0) goto L_0x004e
            int r2 = android.graphics.Color.parseColor(r2)     // Catch:{ Exception -> 0x004e }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x004e }
            goto L_0x004f
        L_0x004e:
            r2 = r4
        L_0x004f:
            kotlin.h r5 = new kotlin.h
            r5.<init>(r3, r2)
            A r2 = r5.f5262a
            B r3 = r5.f5263b
            if (r2 == 0) goto L_0x006d
            if (r3 == 0) goto L_0x006d
            java.lang.Number r3 = (java.lang.Number) r3
            int r3 = r3.intValue()
            java.lang.Number r2 = (java.lang.Number) r2
            int r2 = r2.intValue()
            b.b.a.j.d r4 = new b.b.a.j.d
            r4.<init>(r2, r3)
        L_0x006d:
            if (r4 == 0) goto L_0x001c
            r1.add(r4)
            goto L_0x001c
        L_0x0073:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.j.k0.a():java.util.List");
    }

    public final boolean a(List<Integer> list, int i) {
        List list2;
        j.b(list, "$this$distinct");
        List g = kotlin.a.m.g(kotlin.a.m.k(list));
        j.b(g, "$this$sorted");
        if (g instanceof Collection) {
            Collection collection = g;
            if (collection.size() <= 1) {
                list2 = kotlin.a.m.g(g);
            } else {
                Object[] array = collection.toArray(new Comparable[0]);
                if (array == null) {
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                } else if (array != null) {
                    Comparable[] comparableArr = (Comparable[]) array;
                    if (comparableArr != null) {
                        Object[] objArr = (Object[]) comparableArr;
                        j.b(objArr, "$this$sort");
                        if (objArr.length > 1) {
                            Arrays.sort(objArr);
                        }
                        list2 = g.a(objArr);
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
                    }
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                }
            }
        } else {
            list2 = kotlin.a.m.h(g);
            j.b(list2, "$this$sort");
            if (list2.size() > 1) {
                Collections.sort(list2);
            }
        }
        if (list2.size() != i) {
            return false;
        }
        Integer num = (Integer) kotlin.a.m.d(list2);
        if (!(num != null && num.intValue() == 0)) {
            return false;
        }
        Integer num2 = (Integer) kotlin.a.m.f(list2);
        if (num2 != null && num2.intValue() == i - 1) {
            return true;
        }
        return false;
    }
}
