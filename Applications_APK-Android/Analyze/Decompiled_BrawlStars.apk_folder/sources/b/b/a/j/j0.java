package b.b.a.j;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import b.b.a.h.l;
import b.b.a.j.i0;
import com.supercell.id.SupercellId;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;
import nl.komponents.kovenant.ap;
import nl.komponents.kovenant.bb;
import nl.komponents.kovenant.bw;
import org.json.JSONException;
import org.json.JSONObject;

public final class j0 extends v0<i0> {
    public static final b d = new b(null);

    public static abstract class a implements a<i0> {

        /* renamed from: b.b.a.j.j0$a$a  reason: collision with other inner class name */
        public static final class C0104a extends a {

            /* renamed from: a  reason: collision with root package name */
            public final b.b.a.h.h f1059a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0104a(b.b.a.h.h hVar) {
                super(null);
                j.b(hVar, "profile");
                this.f1059a = hVar;
            }

            public final boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof C0104a) && j.a(this.f1059a, ((C0104a) obj).f1059a);
                }
                return true;
            }

            public final int hashCode() {
                b.b.a.h.h hVar = this.f1059a;
                if (hVar != null) {
                    return hVar.hashCode();
                }
                return 0;
            }

            public final Object invoke(Object obj) {
                i0 i0Var = (i0) obj;
                return i0Var != null ? i0Var : new i0.a(this.f1059a, null);
            }

            public final String toString() {
                StringBuilder a2 = b.a.a.a.a.a("ResetFromPersistentStorage(profile=");
                a2.append(this.f1059a);
                a2.append(")");
                return a2.toString();
            }
        }

        public static final class b extends a {

            /* renamed from: a  reason: collision with root package name */
            public final b.b.a.h.h f1060a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(b.b.a.h.h hVar) {
                super(null);
                j.b(hVar, "profile");
                this.f1060a = hVar;
            }

            public final boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof b) && j.a(this.f1060a, ((b) obj).f1060a);
                }
                return true;
            }

            public final int hashCode() {
                b.b.a.h.h hVar = this.f1060a;
                if (hVar != null) {
                    return hVar.hashCode();
                }
                return 0;
            }

            public final Object invoke(Object obj) {
                return new i0.b(this.f1060a, null);
            }

            public final String toString() {
                StringBuilder a2 = b.a.a.a.a.a("ResetFromServer(profile=");
                a2.append(this.f1060a);
                a2.append(")");
                return a2.toString();
            }
        }

        public static final class c extends a {

            /* renamed from: a  reason: collision with root package name */
            public static final c f1061a = new c();

            public c() {
                super(null);
            }

            public final Object invoke(Object obj) {
                i0 i0Var = (i0) obj;
                if (i0Var != null) {
                    return i0Var.a(null);
                }
                return null;
            }
        }

        public static final class d extends a {

            /* renamed from: a  reason: collision with root package name */
            public final String f1062a;

            /* renamed from: b  reason: collision with root package name */
            public final String f1063b;
            public final Boolean c;

            public d(String str, String str2, Boolean bool) {
                super(null);
                this.f1062a = str;
                this.f1063b = str2;
                this.c = bool;
            }

            public final boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof d)) {
                    return false;
                }
                d dVar = (d) obj;
                return j.a(this.f1062a, dVar.f1062a) && j.a(this.f1063b, dVar.f1063b) && j.a(this.c, dVar.c);
            }

            public final int hashCode() {
                String str = this.f1062a;
                int i = 0;
                int hashCode = (str != null ? str.hashCode() : 0) * 31;
                String str2 = this.f1063b;
                int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
                Boolean bool = this.c;
                if (bool != null) {
                    i = bool.hashCode();
                }
                return hashCode2 + i;
            }

            public final Object invoke(Object obj) {
                boolean z;
                i0 i0Var = (i0) obj;
                if (i0Var == null) {
                    return null;
                }
                b.b.a.h.h a2 = i0Var.a();
                String str = this.f1062a;
                if (str == null) {
                    str = a2.f121b;
                }
                String str2 = str;
                String str3 = this.f1063b;
                if (str3 == null) {
                    str3 = a2.d;
                }
                String str4 = str3;
                Boolean bool = this.c;
                if (bool != null) {
                    z = bool.booleanValue();
                } else {
                    z = a2.h;
                }
                return i0Var.a(b.b.a.h.h.a(a2, null, str2, null, str4, null, null, null, z, null, null, 885));
            }

            public final String toString() {
                StringBuilder a2 = b.a.a.a.a.a("UpdateLocal(name=");
                a2.append(this.f1062a);
                a2.append(", avatarImage=");
                a2.append(this.f1063b);
                a2.append(", forcedOfflineStatus=");
                a2.append(this.c);
                a2.append(")");
                return a2.toString();
            }
        }

        public /* synthetic */ a(kotlin.d.b.g gVar) {
        }
    }

    public static final class b {

        public final class a extends k implements kotlin.d.a.a<m> {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ Context f1064a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(Context context) {
                super(0);
                this.f1064a = context;
            }

            public final Object invoke() {
                SharedPreferences.Editor edit;
                if (Build.VERSION.SDK_INT >= 24) {
                    this.f1064a.deleteSharedPreferences("IdProfiles");
                } else {
                    SharedPreferences sharedPreferences = this.f1064a.getSharedPreferences("IdProfiles", 0);
                    if (!(sharedPreferences == null || (edit = sharedPreferences.edit()) == null)) {
                        edit.clear();
                        edit.apply();
                    }
                }
                return m.f5330a;
            }
        }

        public /* synthetic */ b(kotlin.d.b.g gVar) {
        }

        public final void a(Context context) {
            j.b(context, "context");
            bw unused = bb.f5389a;
        }
    }

    public final class c extends k implements kotlin.d.a.b<b.b.a.h.h, m> {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ ap f1066b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ap apVar) {
            super(1);
            this.f1066b = apVar;
        }

        public final Object invoke(Object obj) {
            b.b.a.h.h hVar = (b.b.a.h.h) obj;
            j.b(hVar, "profile");
            j0.this.a(new a.b(hVar));
            this.f1066b.e(hVar);
            return m.f5330a;
        }
    }

    public final class d extends k implements kotlin.d.a.b<Exception, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ ap f1067a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ap apVar) {
            super(1);
            this.f1067a = apVar;
        }

        public final Object invoke(Object obj) {
            Exception exc = (Exception) obj;
            j.b(exc, "it");
            this.f1067a.f(exc);
            return m.f5330a;
        }
    }

    public final class e extends k implements kotlin.d.a.a<a.C0104a> {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ Context f1069b;
        public final /* synthetic */ String c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(Context context, String str) {
            super(0);
            this.f1069b = context;
            this.c = str;
        }

        public final Object invoke() {
            String string;
            b.b.a.h.h hVar;
            SharedPreferences sharedPreferences = this.f1069b.getSharedPreferences("IdProfiles", 0);
            if (sharedPreferences == null || (string = sharedPreferences.getString(this.c, null)) == null) {
                return null;
            }
            try {
                hVar = new b.b.a.h.h(new JSONObject(string));
            } catch (JSONException unused) {
                hVar = null;
            }
            if (hVar == null) {
                return null;
            }
            a.C0104a aVar = new a.C0104a(hVar);
            j0.this.a(aVar);
            return aVar;
        }
    }

    public final class f extends k implements kotlin.d.a.a<m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ b.b.a.h.h f1070a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ Context f1071b;
        public final /* synthetic */ String c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(b.b.a.h.h hVar, Context context, String str) {
            super(0);
            this.f1070a = hVar;
            this.f1071b = context;
            this.c = str;
        }

        public final Object invoke() {
            SharedPreferences.Editor edit = this.f1071b.getSharedPreferences("IdProfiles", 0).edit();
            edit.putString(this.c, this.f1070a.c().toString());
            edit.apply();
            return m.f5330a;
        }
    }

    public final class g extends k implements kotlin.d.a.b<l, m> {
        public g() {
            super(1);
        }

        public final Object invoke(Object obj) {
            l lVar = (l) obj;
            j.b(lVar, "it");
            j0.this.a(new a.b(lVar.f129a));
            return m.f5330a;
        }
    }

    public final class h extends k implements kotlin.d.a.b<Exception, m> {
        public h() {
            super(1);
        }

        public final Object invoke(Object obj) {
            j.b((Exception) obj, "it");
            j0.this.a(a.c.f1061a);
            return m.f5330a;
        }
    }

    public static /* synthetic */ bw a(j0 j0Var, String str, String str2, Boolean bool, int i) {
        if ((i & 1) != 0) {
            str = null;
        }
        if ((i & 2) != 0) {
            str2 = null;
        }
        if ((i & 4) != 0) {
            bool = null;
        }
        return j0Var.a(str, str2, bool);
    }

    public final bw<b.b.a.h.h, Exception> a() {
        ap a2 = bb.f5389a;
        SupercellId.INSTANCE.getSharedServices$supercellId_release().h.c().a(new c(a2)).b(new d(a2));
        return a2.i();
    }

    public final void a(Context context, String str) {
        j.b(context, "context");
        j.b(str, "supercellId");
        if (!(str.length() == 0)) {
            bw unused = bb.f5389a;
        }
    }

    public final void b(Context context, String str) {
        i0 i0Var;
        b.b.a.h.h b2;
        j.b(context, "context");
        j.b(str, "supercellId");
        if (!(str.length() == 0) && (i0Var = (i0) this.f1179a) != null && (b2 = i0Var.b()) != null) {
            bw unused = bb.f5389a;
        }
    }

    public final bw<l, Exception> a(String str, String str2, Boolean bool) {
        a(new a.d(str, str2, bool));
        return SupercellId.INSTANCE.getSharedServices$supercellId_release().h.a(str, str2, bool).a(new g()).b(new h());
    }
}
