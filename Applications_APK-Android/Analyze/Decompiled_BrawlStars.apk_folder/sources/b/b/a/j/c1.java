package b.b.a.j;

import android.support.v4.app.Fragment;
import b.a.a.a.a;
import kotlin.d.b.j;

public final class c1 {

    /* renamed from: a  reason: collision with root package name */
    public final String f1021a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1022b;
    public final String c;
    public final String d;
    public final String e;
    public final Class<? extends Fragment> f;

    public c1(String str, String str2, String str3, String str4, String str5, Class<? extends Fragment> cls) {
        j.b(str, "titleKey");
        j.b(str2, "leftIconKey");
        j.b(str3, "rightIconKey");
        j.b(str4, "leftIconDisabledKey");
        j.b(str5, "rightIconDisabledKey");
        j.b(cls, "fragmentClass");
        this.f1021a = str;
        this.f1022b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
        this.f = cls;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof c1)) {
            return false;
        }
        c1 c1Var = (c1) obj;
        return j.a(this.f1021a, c1Var.f1021a) && j.a(this.f1022b, c1Var.f1022b) && j.a(this.c, c1Var.c) && j.a(this.d, c1Var.d) && j.a(this.e, c1Var.e) && j.a(this.f, c1Var.f);
    }

    public final int hashCode() {
        String str = this.f1021a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.f1022b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.d;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.e;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        Class<? extends Fragment> cls = this.f;
        if (cls != null) {
            i = cls.hashCode();
        }
        return hashCode5 + i;
    }

    public final String toString() {
        StringBuilder a2 = a.a("TabData(titleKey=");
        a2.append(this.f1021a);
        a2.append(", leftIconKey=");
        a2.append(this.f1022b);
        a2.append(", rightIconKey=");
        a2.append(this.c);
        a2.append(", leftIconDisabledKey=");
        a2.append(this.d);
        a2.append(", rightIconDisabledKey=");
        a2.append(this.e);
        a2.append(", fragmentClass=");
        a2.append(this.f);
        a2.append(")");
        return a2.toString();
    }
}
