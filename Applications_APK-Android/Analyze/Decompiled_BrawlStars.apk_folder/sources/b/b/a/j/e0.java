package b.b.a.j;

import android.graphics.Bitmap;
import java.io.InputStream;
import kotlin.d.b.j;
import kotlin.d.b.k;
import nl.komponents.kovenant.bb;
import nl.komponents.kovenant.bc;
import nl.komponents.kovenant.bw;

public final class e0 {

    /* renamed from: a  reason: collision with root package name */
    public static final l0<Bitmap> f1029a = new l0<>(a.f1031a);

    /* renamed from: b  reason: collision with root package name */
    public static final e0 f1030b = new e0();

    public final class a extends k implements kotlin.d.a.b<String, bw<? extends Bitmap, ? extends Exception>> {

        /* renamed from: a  reason: collision with root package name */
        public static final a f1031a = new a();

        public a() {
            super(1);
        }

        public final Object invoke(Object obj) {
            String str = (String) obj;
            j.b(str, "url");
            return bc.a(e0.f1030b.a(str, true, c0.f1020a), d0.f1025a);
        }
    }

    public final class b extends k implements kotlin.d.a.a<R> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ String f1032a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ boolean f1033b;
        public final /* synthetic */ kotlin.d.a.b c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(String str, boolean z, kotlin.d.a.b bVar) {
            super(0);
            this.f1032a = str;
            this.f1033b = z;
            this.c = bVar;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x002a, code lost:
            r2 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x002b, code lost:
            kotlin.io.b.a(r0, r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:0x002e, code lost:
            throw r2;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final R invoke() {
            /*
                r3 = this;
                r0 = 10000(0x2710, float:1.4013E-41)
                android.net.TrafficStats.setThreadStatsTag(r0)
                java.net.URL r0 = new java.net.URL
                java.lang.String r1 = r3.f1032a
                r0.<init>(r1)
                java.net.URLConnection r0 = r0.openConnection()
                if (r0 == 0) goto L_0x002f
                javax.net.ssl.HttpsURLConnection r0 = (javax.net.ssl.HttpsURLConnection) r0
                boolean r1 = r3.f1033b
                r0.setUseCaches(r1)
                java.io.InputStream r0 = r0.getInputStream()
                kotlin.d.a.b r1 = r3.c
                java.lang.Object r1 = r1.invoke(r0)     // Catch:{ all -> 0x0028 }
                r2 = 0
                kotlin.io.b.a(r0, r2)
                return r1
            L_0x0028:
                r1 = move-exception
                throw r1     // Catch:{ all -> 0x002a }
            L_0x002a:
                r2 = move-exception
                kotlin.io.b.a(r0, r1)
                throw r2
            L_0x002f:
                kotlin.TypeCastException r0 = new kotlin.TypeCastException
                java.lang.String r1 = "null cannot be cast to non-null type javax.net.ssl.HttpsURLConnection"
                r0.<init>(r1)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: b.b.a.j.e0.b.invoke():java.lang.Object");
        }
    }

    public final bw<Bitmap, Exception> a(String str) {
        j.b(str, "url");
        return f1029a.a(str);
    }

    public final <R> bw<R, Exception> a(String str, boolean z, kotlin.d.a.b<? super InputStream, ? extends R> bVar) {
        j.b(str, "url");
        j.b(bVar, "transform");
        return bb.f5389a;
    }

    public final void a() {
        f1029a.f1089a.clear();
    }
}
