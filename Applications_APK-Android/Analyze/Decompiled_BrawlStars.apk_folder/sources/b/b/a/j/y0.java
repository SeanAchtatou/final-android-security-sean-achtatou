package b.b.a.j;

import android.os.Handler;
import android.os.Looper;
import b.b.a.i.x1.g;
import java.lang.ref.WeakReference;
import kotlin.TypeCastException;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;
import nl.komponents.kovenant.CancelException;
import nl.komponents.kovenant.ao;
import nl.komponents.kovenant.ap;
import nl.komponents.kovenant.bb;
import nl.komponents.kovenant.bw;
import nl.komponents.kovenant.q;

public final class y0<V> {

    /* renamed from: a  reason: collision with root package name */
    public bw<? extends V, ? extends Exception> f1196a;

    /* renamed from: b  reason: collision with root package name */
    public final Handler f1197b = new Handler(Looper.getMainLooper());
    public final kotlin.d.a.b<V, m> c;
    public final kotlin.d.a.b<Exception, m> d;

    public final class a extends k implements kotlin.d.a.b<V, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f1198a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ bw f1199b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(WeakReference weakReference, bw bwVar) {
            super(1);
            this.f1198a = weakReference;
            this.f1199b = bwVar;
        }

        public final Object invoke(Object obj) {
            Object obj2 = this.f1198a.get();
            if (obj2 != null) {
                y0 y0Var = (y0) obj2;
                if (j.a(y0Var.f1196a, this.f1199b)) {
                    y0Var.c.invoke(obj);
                }
            }
            return m.f5330a;
        }
    }

    public final class b extends k implements kotlin.d.a.b<Exception, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f1200a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ bw f1201b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(WeakReference weakReference, bw bwVar) {
            super(1);
            this.f1200a = weakReference;
            this.f1201b = bwVar;
        }

        public final Object invoke(Object obj) {
            Object obj2 = this.f1200a.get();
            if (obj2 != null) {
                Exception exc = (Exception) obj;
                y0 y0Var = (y0) obj2;
                if (j.a(y0Var.f1196a, this.f1201b)) {
                    y0Var.d.invoke(exc);
                }
            }
            return m.f5330a;
        }
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [kotlin.d.a.b<? super java.lang.Exception, kotlin.m>, java.lang.Object, kotlin.d.a.b<java.lang.Exception, kotlin.m>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public y0(kotlin.d.a.b<? super V, kotlin.m> r2, kotlin.d.a.b<? super java.lang.Exception, kotlin.m> r3) {
        /*
            r1 = this;
            java.lang.String r0 = "onSuccess"
            kotlin.d.b.j.b(r2, r0)
            java.lang.String r0 = "onFail"
            kotlin.d.b.j.b(r3, r0)
            r1.<init>()
            r1.c = r2
            r1.d = r3
            android.os.Handler r2 = new android.os.Handler
            android.os.Looper r3 = android.os.Looper.getMainLooper()
            r2.<init>(r3)
            r1.f1197b = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.j.y0.<init>(kotlin.d.a.b, kotlin.d.a.b):void");
    }

    public final void a(long j, kotlin.d.a.a<? extends bw<? extends V, ? extends Exception>> aVar) {
        j.b(aVar, "block");
        Handler handler = this.f1197b;
        j.b(handler, "$this$delayedPromise");
        j.b(aVar, "block");
        ap a2 = bb.f5389a;
        a1 a1Var = new a1(a2);
        bb bbVar = bb.f5389a;
        ao a3 = bb.a();
        j.b(a3, "context");
        j.b(a1Var, "onCancelled");
        bb bbVar2 = bb.f5389a;
        ap a4 = bb.a(a3, a1Var);
        b1 b1Var = new b1(aVar, a4, a2);
        handler.postDelayed(b1Var, j);
        a2.i().b(new z0(handler, b1Var));
        bw i = a4.i();
        if (i != null) {
            a((q) i);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type nl.komponents.kovenant.CancelablePromise<V, E>");
    }

    public final void a(bw<? extends g.b, ? extends Exception> bwVar) {
        j.b(bwVar, "promise");
        bw<? extends V, ? extends Exception> bwVar2 = this.f1196a;
        this.f1196a = bwVar;
        WeakReference weakReference = new WeakReference(this);
        nl.komponents.kovenant.c.m.a(bwVar, new a(weakReference, bwVar));
        nl.komponents.kovenant.c.m.b(bwVar, new b(weakReference, bwVar));
        if (!(bwVar2 instanceof q)) {
            bwVar2 = null;
        }
        q qVar = (q) bwVar2;
        if (qVar != null) {
            qVar.g(new CancelException());
        }
    }
}
