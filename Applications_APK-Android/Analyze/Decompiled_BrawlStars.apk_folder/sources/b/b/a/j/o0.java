package b.b.a.j;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import kotlin.d.a.a;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class o0 extends k implements a<m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ RecyclerView f1109a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ int f1110b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public o0(RecyclerView recyclerView, int i) {
        super(0);
        this.f1109a = recyclerView;
        this.f1110b = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.Context, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final Object invoke() {
        Context context = this.f1109a.getContext();
        j.a((Object) context, "context");
        g gVar = new g(context);
        gVar.setTargetPosition(this.f1110b);
        RecyclerView.LayoutManager layoutManager = this.f1109a.getLayoutManager();
        if (layoutManager != null) {
            layoutManager.startSmoothScroll(gVar);
        }
        return m.f5330a;
    }
}
