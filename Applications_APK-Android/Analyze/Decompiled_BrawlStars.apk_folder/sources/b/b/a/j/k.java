package b.b.a.j;

import b.a.a.a.a;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.supercell.id.R;
import kotlin.d.b.j;

public final class k implements r0 {

    /* renamed from: a  reason: collision with root package name */
    public final int f1076a;

    public k() {
        this(0, 1);
    }

    public k(int i) {
        this.f1076a = i;
    }

    public /* synthetic */ k(int i, int i2) {
        this.f1076a = (i2 & 1) != 0 ? R.layout.list_item_divider : i;
    }

    public final int a() {
        return this.f1076a;
    }

    public final boolean a(r0 r0Var) {
        j.b(r0Var, FacebookRequestErrorClassification.KEY_OTHER);
        return r0Var.a() == this.f1076a;
    }

    public final boolean b(r0 r0Var) {
        j.b(r0Var, FacebookRequestErrorClassification.KEY_OTHER);
        return true;
    }

    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof k) {
                if (this.f1076a == ((k) obj).f1076a) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        return this.f1076a;
    }

    public final String toString() {
        StringBuilder a2 = a.a("DividerRow(layoutResId=");
        a2.append(this.f1076a);
        a2.append(")");
        return a2.toString();
    }
}
