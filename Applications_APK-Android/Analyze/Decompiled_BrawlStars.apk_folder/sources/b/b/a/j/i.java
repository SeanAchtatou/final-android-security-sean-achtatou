package b.b.a.j;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import com.facebook.internal.ServerProtocol;
import java.util.Set;
import kotlin.a.ad;
import kotlin.a.an;
import kotlin.d.b.g;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;
import nl.komponents.kovenant.bb;
import nl.komponents.kovenant.bw;

public final class i extends v0<b.b.a.h.a> {
    public static final b d = new b(null);

    public static abstract class a implements a<b.b.a.h.a> {

        /* renamed from: b.b.a.j.i$a$a  reason: collision with other inner class name */
        public static final class C0103a extends a {

            /* renamed from: a  reason: collision with root package name */
            public final b.b.a.h.a f1045a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0103a(b.b.a.h.a aVar) {
                super(null);
                j.b(aVar, ServerProtocol.DIALOG_PARAM_STATE);
                this.f1045a = aVar;
            }

            public final boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof C0103a) && j.a(this.f1045a, ((C0103a) obj).f1045a);
                }
                return true;
            }

            public final int hashCode() {
                b.b.a.h.a aVar = this.f1045a;
                if (aVar != null) {
                    return aVar.hashCode();
                }
                return 0;
            }

            public final Object invoke(Object obj) {
                return this.f1045a;
            }

            public final String toString() {
                StringBuilder a2 = b.a.a.a.a.a("ResetTo(state=");
                a2.append(this.f1045a);
                a2.append(")");
                return a2.toString();
            }
        }

        public static final class b extends a {

            /* renamed from: a  reason: collision with root package name */
            public final Iterable<String> f1046a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(Iterable<String> iterable) {
                super(null);
                j.b(iterable, "seenFriends");
                this.f1046a = iterable;
            }

            /* renamed from: a */
            public final b.b.a.h.a invoke(b.b.a.h.a aVar) {
                if (aVar == null) {
                    return new b.b.a.h.a(an.b(ad.f5212a, this.f1046a));
                }
                Set b2 = an.b(aVar.f107a, this.f1046a);
                j.b(b2, "seenInGameFriends");
                return new b.b.a.h.a(b2);
            }

            public final boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof b) && j.a(this.f1046a, ((b) obj).f1046a);
                }
                return true;
            }

            public final int hashCode() {
                Iterable<String> iterable = this.f1046a;
                if (iterable != null) {
                    return iterable.hashCode();
                }
                return 0;
            }

            public final String toString() {
                StringBuilder a2 = b.a.a.a.a.a("SawIngameFriends(seenFriends=");
                a2.append(this.f1046a);
                a2.append(")");
                return a2.toString();
            }
        }

        public /* synthetic */ a(g gVar) {
        }
    }

    public static final class b {

        public final class a extends k implements kotlin.d.a.a<m> {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ Context f1047a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(Context context) {
                super(0);
                this.f1047a = context;
            }

            public final Object invoke() {
                SharedPreferences.Editor edit;
                if (Build.VERSION.SDK_INT >= 24) {
                    this.f1047a.deleteSharedPreferences("ClientState");
                } else {
                    SharedPreferences sharedPreferences = this.f1047a.getSharedPreferences("ClientState", 0);
                    if (!(sharedPreferences == null || (edit = sharedPreferences.edit()) == null)) {
                        edit.clear();
                        edit.apply();
                    }
                }
                return m.f5330a;
            }
        }

        public /* synthetic */ b(g gVar) {
        }

        public final void a(Context context) {
            j.b(context, "context");
            bw unused = bb.f5389a;
        }
    }

    public final class c extends k implements kotlin.d.a.a<a.C0103a> {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ Context f1049b;
        public final /* synthetic */ String c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(Context context, String str) {
            super(0);
            this.f1049b = context;
            this.c = str;
        }

        /* JADX WARNING: Removed duplicated region for block: B:9:0x0023  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invoke() {
            /*
                r3 = this;
                android.content.Context r0 = r3.f1049b
                java.lang.String r1 = "ClientState"
                r2 = 0
                android.content.SharedPreferences r0 = r0.getSharedPreferences(r1, r2)
                r1 = 0
                if (r0 == 0) goto L_0x001f
                java.lang.String r2 = r3.c
                java.lang.String r0 = r0.getString(r2, r1)
                if (r0 == 0) goto L_0x001f
                org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x001f }
                r2.<init>(r0)     // Catch:{ JSONException -> 0x001f }
                b.b.a.h.a r0 = new b.b.a.h.a     // Catch:{ JSONException -> 0x001f }
                r0.<init>(r2)     // Catch:{ JSONException -> 0x001f }
                goto L_0x0020
            L_0x001f:
                r0 = r1
            L_0x0020:
                if (r0 == 0) goto L_0x0023
                goto L_0x0029
            L_0x0023:
                b.b.a.h.a r0 = new b.b.a.h.a
                r2 = 1
                r0.<init>(r1, r2)
            L_0x0029:
                b.b.a.j.i$a$a r1 = new b.b.a.j.i$a$a
                r1.<init>(r0)
                b.b.a.j.i r0 = b.b.a.j.i.this
                r0.a(r1)
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: b.b.a.j.i.c.invoke():java.lang.Object");
        }
    }

    public final class d extends k implements kotlin.d.a.a<m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ b.b.a.h.a f1050a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ Context f1051b;
        public final /* synthetic */ String c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(b.b.a.h.a aVar, Context context, String str) {
            super(0);
            this.f1050a = aVar;
            this.f1051b = context;
            this.c = str;
        }

        public final Object invoke() {
            SharedPreferences.Editor edit = this.f1051b.getSharedPreferences("ClientState", 0).edit();
            edit.putString(this.c, this.f1050a.a().toString());
            edit.apply();
            return m.f5330a;
        }
    }

    public final void a(Context context, String str) {
        j.b(context, "context");
        j.b(str, "supercellId");
        if (str.length() == 0) {
            a(new a.C0103a(new b.b.a.h.a(null, 1)));
        } else {
            bw unused = bb.f5389a;
        }
    }

    public final void b(Context context, String str) {
        b.b.a.h.a aVar;
        j.b(context, "context");
        j.b(str, "supercellId");
        if (!(str.length() == 0) && (aVar = (b.b.a.h.a) this.f1179a) != null) {
            bw unused = bb.f5389a;
        }
    }
}
