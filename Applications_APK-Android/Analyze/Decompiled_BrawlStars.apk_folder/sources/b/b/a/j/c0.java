package b.b.a.j;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.InputStream;
import kotlin.d.a.b;
import kotlin.d.b.h;
import kotlin.d.b.t;
import kotlin.h.d;

public final /* synthetic */ class c0 extends h implements b<InputStream, Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    public static final c0 f1020a = new c0();

    public c0() {
        super(1);
    }

    public final String getName() {
        return "decodeStream";
    }

    public final d getOwner() {
        return t.a(BitmapFactory.class);
    }

    public final String getSignature() {
        return "decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;";
    }

    public final Object invoke(Object obj) {
        return BitmapFactory.decodeStream((InputStream) obj);
    }
}
