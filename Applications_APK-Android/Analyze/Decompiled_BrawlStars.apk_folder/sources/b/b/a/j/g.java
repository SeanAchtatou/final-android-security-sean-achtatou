package b.b.a.j;

import android.content.Context;
import android.support.v7.widget.LinearSmoothScroller;
import android.util.DisplayMetrics;
import kotlin.d.b.j;

public final class g extends LinearSmoothScroller {
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public g(Context context) {
        super(context);
        j.b(context, "context");
    }

    public final int calculateDtToFit(int i, int i2, int i3, int i4, int i5) {
        return (((i4 - i3) / 2) + i3) - (((i2 - i) / 2) + i);
    }

    public final float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
        j.b(displayMetrics, "displayMetrics");
        return 10.0f / ((float) displayMetrics.densityDpi);
    }
}
