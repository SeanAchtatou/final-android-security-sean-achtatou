package b.b.a.j;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.style.ImageSpan;
import kotlin.d.b.j;

public final class h extends ImageSpan {

    /* renamed from: a  reason: collision with root package name */
    public final Drawable f1041a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public h(Drawable drawable) {
        super(drawable);
        j.b(drawable, "d");
        this.f1041a = drawable;
    }

    public final void draw(Canvas canvas, CharSequence charSequence, int i, int i2, float f, int i3, int i4, int i5, Paint paint) {
        j.b(canvas, "canvas");
        j.b(charSequence, "text");
        j.b(paint, "paint");
        canvas.save();
        canvas.translate(f, (float) (((i5 - i3) - this.f1041a.getBounds().height()) / 2));
        this.f1041a.draw(canvas);
        canvas.restore();
    }

    public final int getSize(Paint paint, CharSequence charSequence, int i, int i2, Paint.FontMetricsInt fontMetricsInt) {
        j.b(paint, "paint");
        Rect bounds = this.f1041a.getBounds();
        if (fontMetricsInt != null) {
            Paint.FontMetricsInt fontMetricsInt2 = paint.getFontMetricsInt();
            int height = this.f1041a.getBounds().height() / 2;
            int i3 = fontMetricsInt2.descent;
            int i4 = fontMetricsInt2.ascent;
            int i5 = (i3 + i4) / 2;
            fontMetricsInt.top = i5 - height;
            fontMetricsInt.bottom = i5 + height;
            fontMetricsInt.ascent = Math.max(i4, fontMetricsInt.top);
            fontMetricsInt.descent = Math.min(fontMetricsInt2.descent, fontMetricsInt.bottom);
        }
        return bounds.right;
    }
}
