package b.b.a.j;

import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;
import nl.komponents.kovenant.ap;
import nl.komponents.kovenant.bb;
import nl.komponents.kovenant.bw;

public final class l0<T> {

    /* renamed from: a  reason: collision with root package name */
    public final ConcurrentHashMap<String, SoftReference<T>> f1089a = new ConcurrentHashMap<>();

    /* renamed from: b  reason: collision with root package name */
    public final Map<String, List<ap<T, Exception>>> f1090b = new LinkedHashMap();
    public final kotlin.d.a.b<String, bw<T, Exception>> c;

    public final class a extends k implements kotlin.d.a.b<T, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f1091a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ String f1092b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(WeakReference weakReference, String str) {
            super(1);
            this.f1091a = weakReference;
            this.f1092b = str;
        }

        public final Object invoke(Object obj) {
            l0 l0Var = (l0) this.f1091a.get();
            if (l0Var != null) {
                l0Var.f1089a.put(this.f1092b, new SoftReference(obj));
                l0Var.a(this.f1092b, obj);
            }
            return m.f5330a;
        }
    }

    public final class b extends k implements kotlin.d.a.b<Exception, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f1093a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ String f1094b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(WeakReference weakReference, String str) {
            super(1);
            this.f1093a = weakReference;
            this.f1094b = str;
        }

        public final Object invoke(Object obj) {
            Exception exc = (Exception) obj;
            j.b(exc, "it");
            l0 l0Var = (l0) this.f1093a.get();
            if (l0Var != null) {
                l0Var.a(this.f1094b, exc);
            }
            return m.f5330a;
        }
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [kotlin.d.a.b<java.lang.String, nl.komponents.kovenant.bw<T, java.lang.Exception>>, java.lang.Object, kotlin.d.a.b<? super java.lang.String, ? extends nl.komponents.kovenant.bw<? extends T, ? extends java.lang.Exception>>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public l0(kotlin.d.a.b<? super java.lang.String, ? extends nl.komponents.kovenant.bw<? extends T, ? extends java.lang.Exception>> r2) {
        /*
            r1 = this;
            java.lang.String r0 = "fetch"
            kotlin.d.b.j.b(r2, r0)
            r1.<init>()
            r1.c = r2
            java.util.concurrent.ConcurrentHashMap r2 = new java.util.concurrent.ConcurrentHashMap
            r2.<init>()
            r1.f1089a = r2
            java.util.LinkedHashMap r2 = new java.util.LinkedHashMap
            r2.<init>()
            r1.f1090b = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.j.l0.<init>(kotlin.d.a.b):void");
    }

    public final bw<T, Exception> a(String str) {
        boolean z;
        j.b(str, "key");
        Object obj = null;
        ap a2 = bb.f5389a;
        SoftReference softReference = this.f1089a.get(str);
        if (softReference != null) {
            obj = softReference.get();
        }
        if (obj != null) {
            a2.e(obj);
        } else {
            synchronized (this.f1090b) {
                boolean containsKey = this.f1090b.containsKey(str);
                if (containsKey) {
                    List list = this.f1090b.get(str);
                    if (list != null) {
                        list.add(a2);
                    }
                } else {
                    this.f1090b.put(str, kotlin.a.m.b((Object[]) new ap[]{a2}));
                }
                z = true ^ containsKey;
            }
            if (z) {
                WeakReference weakReference = new WeakReference(this);
                this.c.invoke(str).a(new a(weakReference, str)).b(new b(weakReference, str));
            }
        }
        return a2.i();
    }

    public final void a(String str, Exception exc) {
        synchronized (this.f1090b) {
            List<ap> remove = this.f1090b.remove(str);
            if (remove != null) {
                for (ap f : remove) {
                    f.f(exc);
                }
                m mVar = m.f5330a;
            }
        }
    }

    public final void a(String str, T t) {
        synchronized (this.f1090b) {
            List<ap> remove = this.f1090b.remove(str);
            if (remove != null) {
                for (ap e : remove) {
                    e.e(t);
                }
                m mVar = m.f5330a;
            }
        }
    }
}
