package b.b.a.j;

import b.b.a.h.h;
import kotlin.NoWhenBranchMatchedException;
import kotlin.d.b.g;
import kotlin.d.b.j;

public abstract class i0 {

    public static final class a extends i0 {

        /* renamed from: a  reason: collision with root package name */
        public final h f1052a;

        /* renamed from: b  reason: collision with root package name */
        public final h f1053b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(h hVar, h hVar2) {
            super(null);
            j.b(hVar, "fromPersistentStorage");
            this.f1052a = hVar;
            this.f1053b = hVar2;
        }

        public final a a(h hVar, h hVar2) {
            j.b(hVar, "fromPersistentStorage");
            return new a(hVar, hVar2);
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return j.a(this.f1052a, aVar.f1052a) && j.a(this.f1053b, aVar.f1053b);
        }

        public final int hashCode() {
            h hVar = this.f1052a;
            int i = 0;
            int hashCode = (hVar != null ? hVar.hashCode() : 0) * 31;
            h hVar2 = this.f1053b;
            if (hVar2 != null) {
                i = hVar2.hashCode();
            }
            return hashCode + i;
        }

        public final String toString() {
            StringBuilder a2 = b.a.a.a.a.a("FromPersistentStorage(fromPersistentStorage=");
            a2.append(this.f1052a);
            a2.append(", local=");
            a2.append(this.f1053b);
            a2.append(")");
            return a2.toString();
        }
    }

    public static final class b extends i0 {

        /* renamed from: a  reason: collision with root package name */
        public final h f1054a;

        /* renamed from: b  reason: collision with root package name */
        public final h f1055b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(h hVar, h hVar2) {
            super(null);
            j.b(hVar, "fromServer");
            this.f1054a = hVar;
            this.f1055b = hVar2;
        }

        public final b a(h hVar, h hVar2) {
            j.b(hVar, "fromServer");
            return new b(hVar, hVar2);
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return j.a(this.f1054a, bVar.f1054a) && j.a(this.f1055b, bVar.f1055b);
        }

        public final int hashCode() {
            h hVar = this.f1054a;
            int i = 0;
            int hashCode = (hVar != null ? hVar.hashCode() : 0) * 31;
            h hVar2 = this.f1055b;
            if (hVar2 != null) {
                i = hVar2.hashCode();
            }
            return hashCode + i;
        }

        public final String toString() {
            StringBuilder a2 = b.a.a.a.a.a("FromServer(fromServer=");
            a2.append(this.f1054a);
            a2.append(", local=");
            a2.append(this.f1055b);
            a2.append(")");
            return a2.toString();
        }
    }

    public i0() {
    }

    public /* synthetic */ i0(g gVar) {
    }

    public final h a() {
        if (this instanceof a) {
            a aVar = (a) this;
            h hVar = aVar.f1053b;
            if (hVar != null) {
                return hVar;
            }
            return aVar.f1052a;
        } else if (this instanceof b) {
            b bVar = (b) this;
            h hVar2 = bVar.f1055b;
            return hVar2 != null ? hVar2 : bVar.f1054a;
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    public final h b() {
        if (this instanceof b) {
            return ((b) this).f1054a;
        }
        return null;
    }

    public final i0 a(h hVar) {
        if (this instanceof a) {
            a aVar = (a) this;
            return aVar.a(aVar.f1052a, hVar);
        } else if (this instanceof b) {
            b bVar = (b) this;
            return bVar.a(bVar.f1054a, hVar);
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }
}
