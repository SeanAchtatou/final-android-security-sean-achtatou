package b.b.a.j;

import android.content.Context;
import android.telephony.TelephonyManager;
import com.facebook.places.model.PlaceFields;
import com.google.i18n.phonenumbers.g;
import com.google.i18n.phonenumbers.j;
import com.supercell.id.SupercellId;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Set;
import kotlin.a.ab;
import kotlin.a.m;
import kotlin.d.b.j;
import kotlin.j.t;

public final class h0 {

    /* renamed from: a  reason: collision with root package name */
    public static final h0 f1042a = new h0();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Locale, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    public final String a(Context context) {
        String simCountryIso;
        Object systemService = context != null ? context.getSystemService(PlaceFields.PHONE) : null;
        if (!(systemService instanceof TelephonyManager)) {
            systemService = null;
        }
        TelephonyManager telephonyManager = (TelephonyManager) systemService;
        if (!(telephonyManager == null || (simCountryIso = telephonyManager.getSimCountryIso()) == null)) {
            Locale locale = Locale.ENGLISH;
            j.a((Object) locale, "Locale.ENGLISH");
            String upperCase = simCountryIso.toUpperCase(locale);
            j.a((Object) upperCase, "(this as java.lang.String).toUpperCase(locale)");
            if (upperCase != null) {
                if (upperCase.length() > 0) {
                    for (String str : f1042a.a()) {
                        if (j.a((Object) str, (Object) upperCase)) {
                            return str;
                        }
                    }
                    throw new NoSuchElementException("Collection contains no element matching the predicate.");
                }
                Locale locale2 = Locale.getDefault();
                j.a((Object) locale2, "Locale.getDefault()");
                String country = locale2.getCountry();
                for (String str2 : f1042a.a()) {
                    if (j.a((Object) str2, (Object) country)) {
                        return str2;
                    }
                }
                throw new NoSuchElementException("Collection contains no element matching the predicate.");
            }
        }
        return null;
    }

    public final String b(j.a aVar) {
        if (aVar != null) {
            return g.a().c(aVar);
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    public final boolean c(j.a aVar) {
        String b2 = b(aVar);
        if (b2 != null) {
            List<String> a2 = f1042a.a();
            if (!(a2 instanceof Collection) || !a2.isEmpty()) {
                for (String str : a2) {
                    if (kotlin.d.b.j.a((Object) str, (Object) b2)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.google.i18n.phonenumbers.g, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Set, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.b(java.lang.Iterable, java.lang.Iterable):java.util.Set<T>
     arg types: [java.util.Set, java.util.List<java.lang.String>]
     candidates:
      kotlin.a.w.b(java.lang.Iterable, int):T
      kotlin.a.t.b(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.w.b(java.lang.Iterable, java.lang.Iterable):java.util.Set<T> */
    public final List<String> a() {
        List<String> d = SupercellId.INSTANCE.getRemoteConfiguration$supercellId_release().d(q0.SMS_REGIONS);
        if (d == null || d.isEmpty()) {
            return ab.f5210a;
        }
        g a2 = g.a();
        kotlin.d.b.j.a((Object) a2, "PhoneNumberUtil.getInstance()");
        Set unmodifiableSet = Collections.unmodifiableSet(a2.f);
        kotlin.d.b.j.a((Object) unmodifiableSet, "PhoneNumberUtil.getInstance().supportedRegions");
        return m.g(m.b((Iterable) unmodifiableSet, (Iterable) d));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.j.ac.a(java.lang.CharSequence, int, java.lang.CharSequence, int, int, boolean):boolean
     arg types: [java.lang.CharSequence, int, java.lang.String, int, int, int]
     candidates:
      kotlin.j.ac.a(java.lang.CharSequence, java.lang.CharSequence, int, int, boolean, boolean):int
      kotlin.j.ac.a(java.lang.CharSequence, java.lang.String[], int, boolean, int, int):kotlin.i.h
      kotlin.j.ab.a(java.lang.String, int, java.lang.String, int, int, boolean):boolean
      kotlin.j.ac.a(java.lang.CharSequence, int, java.lang.CharSequence, int, int, boolean):boolean */
    public final String a(j.a aVar) {
        boolean z;
        if (aVar == null) {
            return "";
        }
        String a2 = g.a().a(aVar, g.a.INTERNATIONAL);
        kotlin.d.b.j.a((Object) a2, "PhoneNumberUtil.getInsta…mberFormat.INTERNATIONAL)");
        StringBuilder sb = new StringBuilder();
        sb.append('+');
        sb.append(aVar.f2875a);
        sb.append(' ');
        String sb2 = sb.toString();
        kotlin.d.b.j.b(a2, "$this$removePrefix");
        kotlin.d.b.j.b(sb2, "prefix");
        CharSequence charSequence = a2;
        kotlin.d.b.j.b(charSequence, "$this$startsWith");
        kotlin.d.b.j.b(sb2, "prefix");
        if (!(charSequence instanceof String) || !(sb2 instanceof String)) {
            z = t.a(charSequence, 0, (CharSequence) sb2, 0, sb2.length(), false);
        } else {
            z = t.b((String) charSequence, sb2, false);
        }
        if (!z) {
            return a2;
        }
        String substring = a2.substring(sb2.length());
        kotlin.d.b.j.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
        return substring;
    }
}
