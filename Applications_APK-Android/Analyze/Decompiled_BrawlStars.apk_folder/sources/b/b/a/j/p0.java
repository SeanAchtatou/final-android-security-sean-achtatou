package b.b.a.j;

import android.os.Parcel;
import android.os.Parcelable;
import com.supercell.id.SupercellId;
import java.text.Normalizer;
import kotlin.TypeCastException;
import kotlin.a.m;
import kotlin.d.b.g;
import kotlin.d.b.j;

public final class p0 implements Parcelable {
    public static final a CREATOR = new a(null);

    /* renamed from: a  reason: collision with root package name */
    public final String f1119a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1120b;
    public final int c;

    public static final class a implements Parcelable.Creator<p0> {
        public /* synthetic */ a(g gVar) {
        }

        public final Object createFromParcel(Parcel parcel) {
            j.b(parcel, "parcel");
            j.b(parcel, "parcel");
            String readString = parcel.readString();
            if (readString == null) {
                j.a();
            }
            String readString2 = parcel.readString();
            if (readString2 == null) {
                j.a();
            }
            return new p0(readString, readString2, parcel.readInt());
        }

        public final Object[] newArray(int i) {
            return new p0[i];
        }
    }

    public p0(String str, String str2, int i) {
        j.b(str, "name");
        j.b(str2, "code");
        this.f1119a = str;
        this.f1120b = str2;
        this.c = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final String a() {
        String str = this.f1119a;
        if (str != null) {
            String substring = str.substring(0, 1);
            j.a((Object) substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            if (!b()) {
                return substring;
            }
            String normalize = Normalizer.normalize(substring, Normalizer.Form.NFD);
            j.a((Object) normalize, "Normalizer.normalize(fir…ter, Normalizer.Form.NFD)");
            if (normalize != null) {
                String substring2 = normalize.substring(0, 1);
                j.a((Object) substring2, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                return substring2;
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    public final boolean b() {
        return !m.a((Object[]) new String[]{"fi", "sv", "da", "no"}).contains(SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getLanguage());
    }

    public final int describeContents() {
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof p0) {
                p0 p0Var = (p0) obj;
                if (j.a((Object) this.f1119a, (Object) p0Var.f1119a) && j.a((Object) this.f1120b, (Object) p0Var.f1120b)) {
                    if (this.c == p0Var.c) {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        String str = this.f1119a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.f1120b;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return ((hashCode + i) * 31) + this.c;
    }

    public final String toString() {
        StringBuilder a2 = b.a.a.a.a.a("Region(name=");
        a2.append(this.f1119a);
        a2.append(", code=");
        a2.append(this.f1120b);
        a2.append(", countryCode=");
        a2.append(this.c);
        a2.append(")");
        return a2.toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        j.b(parcel, "dest");
        parcel.writeString(this.f1119a);
        parcel.writeString(this.f1120b);
        parcel.writeInt(this.c);
    }
}
