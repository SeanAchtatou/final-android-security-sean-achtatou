package b.b.a.j;

import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import kotlin.d.a.b;
import kotlin.d.a.c;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class o1<T> {

    /* renamed from: a  reason: collision with root package name */
    public final ConcurrentHashMap<String, SoftReference<T>> f1111a = new ConcurrentHashMap<>();

    /* renamed from: b  reason: collision with root package name */
    public final Map<String, List<b<T, m>>> f1112b = new LinkedHashMap();
    public final c<String, b<? super T, m>, m> c;

    public final class a extends k implements b<T, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f1113a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ o1 f1114b;
        public final /* synthetic */ String c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(WeakReference weakReference, o1 o1Var, String str, b bVar) {
            super(1);
            this.f1113a = weakReference;
            this.f1114b = o1Var;
            this.c = str;
        }

        public final Object invoke(Object obj) {
            ConcurrentHashMap concurrentHashMap = (ConcurrentHashMap) this.f1113a.get();
            if (concurrentHashMap != null) {
                concurrentHashMap.put(this.c, new SoftReference(obj));
            }
            b.b.a.b.a(new n1(this, obj));
            return m.f5330a;
        }
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [kotlin.d.a.c<java.lang.String, kotlin.d.a.b<? super T, kotlin.m>, kotlin.m>, java.lang.Object, kotlin.d.a.c<? super java.lang.String, ? super kotlin.d.a.b<? super T, kotlin.m>, kotlin.m>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public o1(kotlin.d.a.c<? super java.lang.String, ? super kotlin.d.a.b<? super T, kotlin.m>, kotlin.m> r2) {
        /*
            r1 = this;
            java.lang.String r0 = "fetch"
            kotlin.d.b.j.b(r2, r0)
            r1.<init>()
            r1.c = r2
            java.util.concurrent.ConcurrentHashMap r2 = new java.util.concurrent.ConcurrentHashMap
            r2.<init>()
            r1.f1111a = r2
            java.util.LinkedHashMap r2 = new java.util.LinkedHashMap
            r2.<init>()
            r1.f1112b = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.j.o1.<init>(kotlin.d.a.c):void");
    }

    public final void a(String str, List<String> list) {
        synchronized (this.f1112b) {
            List<b> list2 = this.f1112b.get(str);
            if (list2 != null) {
                for (b invoke : list2) {
                    invoke.invoke(list);
                }
            }
            this.f1112b.remove(str);
        }
    }

    public final void a(String str, b<? super List<String>, m> bVar) {
        j.b(str, "key");
        j.b(bVar, "callback");
        SoftReference softReference = this.f1111a.get(str);
        Object obj = softReference != null ? softReference.get() : null;
        if (obj != null) {
            bVar.invoke(obj);
            return;
        }
        synchronized (this.f1112b) {
            if (this.f1112b.containsKey(str)) {
                List list = this.f1112b.get(str);
                if (list != null) {
                    Boolean.valueOf(list.add(bVar));
                }
            } else {
                this.f1112b.put(str, kotlin.a.m.b((Object[]) new b[]{bVar}));
                this.c.invoke(str, new a(new WeakReference(this.f1111a), this, str, bVar));
            }
        }
    }
}
