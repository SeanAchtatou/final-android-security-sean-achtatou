package b.b.a.j;

import android.graphics.drawable.Drawable;
import android.support.v4.widget.TextViewCompat;
import android.widget.TextView;
import kotlin.d.a.a;
import kotlin.d.b.k;
import kotlin.m;

public final class l1 extends k implements a<m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ TextView f1095a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ Drawable f1096b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l1(TextView textView, Drawable drawable) {
        super(0);
        this.f1095a = textView;
        this.f1096b = drawable;
    }

    public final Object invoke() {
        TextViewCompat.setCompoundDrawablesRelativeWithIntrinsicBounds(this.f1095a, this.f1096b, (Drawable) null, (Drawable) null, (Drawable) null);
        return m.f5330a;
    }
}
