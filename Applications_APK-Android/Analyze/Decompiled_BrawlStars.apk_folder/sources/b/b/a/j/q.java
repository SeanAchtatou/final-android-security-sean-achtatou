package b.b.a.j;

import b.b.a.j.m;
import b.b.a.j.t;
import com.facebook.internal.NativeProtocol;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.facebook.share.internal.ShareConstants;
import com.supercell.id.IdFriend;
import com.supercell.id.SupercellId;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.NoWhenBranchMatchedException;
import kotlin.d.b.k;
import nl.komponents.kovenant.ap;
import nl.komponents.kovenant.bb;
import nl.komponents.kovenant.bc;
import nl.komponents.kovenant.bw;

public final class q extends v0<m<? extends b.b.a.h.d, ? extends f0>> {
    public t d;
    public long e = 10000;
    public long f;

    public static abstract class a implements a<m<? extends b.b.a.h.d, ? extends f0>> {

        /* renamed from: b.b.a.j.q$a$a  reason: collision with other inner class name */
        public static final class C0105a extends a {

            /* renamed from: a  reason: collision with root package name */
            public final List<b.b.a.h.i> f1121a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0105a(List<b.b.a.h.i> list) {
                super(null);
                kotlin.d.b.j.b(list, "profiles");
                this.f1121a = list;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: kotlin.a.p.a(java.lang.Iterable, int):int
             arg types: [java.util.List<b.b.a.h.i>, int]
             candidates:
              kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
              kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
              kotlin.a.w.a(java.util.List, int):T
              kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
              kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
              kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
              kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
              kotlin.a.s.a(java.util.List, java.util.Comparator):void
              kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
              kotlin.a.p.a(java.lang.Iterable, int):int */
            /* renamed from: a */
            public final m<b.b.a.h.d, f0> invoke(m<b.b.a.h.d, f0> mVar) {
                b.b.a.h.d a2;
                m.a aVar;
                if (mVar == null || (a2 = mVar.a()) == null) {
                    return mVar;
                }
                List<b.b.a.h.i> list = this.f1121a;
                ArrayList arrayList = new ArrayList(kotlin.a.m.a((Iterable) list, 10));
                for (b.b.a.h.i iVar : list) {
                    arrayList.add(iVar.f122a);
                }
                HashSet f = kotlin.a.m.f((Iterable) arrayList);
                if (!(!f.isEmpty())) {
                    f = null;
                }
                if (f != null) {
                    List<b.b.a.h.i> list2 = this.f1121a;
                    ArrayList arrayList2 = new ArrayList(kotlin.a.m.a((Iterable) list2, 10));
                    for (b.b.a.h.i iVar2 : list2) {
                        kotlin.d.b.j.b(iVar2, ShareConstants.WEB_DIALOG_PARAM_DATA);
                        arrayList2.add(new b.b.a.h.c(iVar2.f122a, iVar2.f123b, iVar2.c, iVar2.i, iVar2.f, 0));
                    }
                    List<b.b.a.h.c> list3 = a2.f112a;
                    ArrayList arrayList3 = new ArrayList();
                    for (T next : list3) {
                        if (!f.contains(((b.b.a.h.c) next).f110a)) {
                            arrayList3.add(next);
                        }
                    }
                    List d = kotlin.a.m.d(arrayList3, arrayList2);
                    List<b.b.a.h.c> list4 = a2.c;
                    ArrayList arrayList4 = new ArrayList();
                    for (T next2 : list4) {
                        if (!f.contains(((b.b.a.h.c) next2).f110a)) {
                            arrayList4.add(next2);
                        }
                    }
                    aVar = new m.a(a2.a(d, a2.f113b, arrayList4));
                } else {
                    aVar = null;
                }
                return aVar != null ? aVar : mVar;
            }

            public final boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof C0105a) && kotlin.d.b.j.a(this.f1121a, ((C0105a) obj).f1121a);
                }
                return true;
            }

            public final int hashCode() {
                List<b.b.a.h.i> list = this.f1121a;
                if (list != null) {
                    return list.hashCode();
                }
                return 0;
            }

            public final String toString() {
                StringBuilder a2 = b.a.a.a.a.a("AcceptRequests(profiles=");
                a2.append(this.f1121a);
                a2.append(")");
                return a2.toString();
            }
        }

        public static final class b extends a {

            /* renamed from: a  reason: collision with root package name */
            public static final b f1122a = new b();

            public b() {
                super(null);
            }

            public final Object invoke(Object obj) {
                m mVar = (m) obj;
                if (mVar instanceof m.b) {
                    return null;
                }
                return mVar;
            }
        }

        public static final class c extends a {

            /* renamed from: a  reason: collision with root package name */
            public final String f1123a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(String str) {
                super(null);
                kotlin.d.b.j.b(str, "scid");
                this.f1123a = str;
            }

            public final boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof c) && kotlin.d.b.j.a(this.f1123a, ((c) obj).f1123a);
                }
                return true;
            }

            public final int hashCode() {
                String str = this.f1123a;
                if (str != null) {
                    return str.hashCode();
                }
                return 0;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
             arg types: [java.lang.String, java.lang.String]
             candidates:
              kotlin.d.b.j.a(int, int):int
              kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
              kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
              kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
            public final Object invoke(Object obj) {
                b.b.a.h.d dVar;
                m mVar = (m) obj;
                if (mVar == null || (dVar = (b.b.a.h.d) mVar.a()) == null) {
                    return mVar;
                }
                List<b.b.a.h.c> list = dVar.f112a;
                ArrayList arrayList = new ArrayList();
                for (T next : list) {
                    if (!kotlin.d.b.j.a((Object) ((b.b.a.h.c) next).f110a, (Object) this.f1123a)) {
                        arrayList.add(next);
                    }
                }
                List<b.b.a.h.c> list2 = dVar.f113b;
                ArrayList arrayList2 = new ArrayList();
                for (T next2 : list2) {
                    if (!kotlin.d.b.j.a((Object) ((b.b.a.h.c) next2).f110a, (Object) this.f1123a)) {
                        arrayList2.add(next2);
                    }
                }
                List<b.b.a.h.c> list3 = dVar.c;
                ArrayList arrayList3 = new ArrayList();
                for (T next3 : list3) {
                    if (!kotlin.d.b.j.a((Object) ((b.b.a.h.c) next3).f110a, (Object) this.f1123a)) {
                        arrayList3.add(next3);
                    }
                }
                return new m.a(dVar.a(arrayList, arrayList2, arrayList3));
            }

            public final String toString() {
                return b.a.a.a.a.a(b.a.a.a.a.a("Remove(scid="), this.f1123a, ")");
            }
        }

        public static final class d extends a {

            /* renamed from: a  reason: collision with root package name */
            public final b.b.a.h.d f1124a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public d(b.b.a.h.d dVar) {
                super(null);
                kotlin.d.b.j.b(dVar, NativeProtocol.AUDIENCE_FRIENDS);
                this.f1124a = dVar;
            }

            public final boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof d) && kotlin.d.b.j.a(this.f1124a, ((d) obj).f1124a);
                }
                return true;
            }

            public final int hashCode() {
                b.b.a.h.d dVar = this.f1124a;
                if (dVar != null) {
                    return dVar.hashCode();
                }
                return 0;
            }

            public final Object invoke(Object obj) {
                return new m.a(this.f1124a);
            }

            public final String toString() {
                StringBuilder a2 = b.a.a.a.a.a("ResetTo(friends=");
                a2.append(this.f1124a);
                a2.append(")");
                return a2.toString();
            }
        }

        public static final class e extends a {

            /* renamed from: a  reason: collision with root package name */
            public final f0 f1125a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public e(f0 f0Var) {
                super(null);
                kotlin.d.b.j.b(f0Var, "exception");
                this.f1125a = f0Var;
            }

            public final boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof e) && kotlin.d.b.j.a(this.f1125a, ((e) obj).f1125a);
                }
                return true;
            }

            public final int hashCode() {
                f0 f0Var = this.f1125a;
                if (f0Var != null) {
                    return f0Var.hashCode();
                }
                return 0;
            }

            public final Object invoke(Object obj) {
                return new m.b(this.f1125a);
            }

            public final String toString() {
                StringBuilder a2 = b.a.a.a.a.a("SetError(exception=");
                a2.append(this.f1125a);
                a2.append(")");
                return a2.toString();
            }
        }

        public /* synthetic */ a(kotlin.d.b.g gVar) {
        }
    }

    public final class b extends k implements kotlin.d.a.b<Map<String, ? extends m<? extends b.b.a.h.i, ? extends Exception>>, b.b.a.h.i> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ String f1126a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(String str) {
            super(1);
            this.f1126a = str;
        }

        public final Object invoke(Object obj) {
            Map map = (Map) obj;
            kotlin.d.b.j.b(map, "it");
            m mVar = (m) map.get(this.f1126a);
            if (mVar instanceof m.a) {
                return (b.b.a.h.i) ((m.a) mVar).f1097a;
            }
            if (mVar instanceof m.b) {
                throw ((Throwable) ((m.b) mVar).f1098a);
            }
            throw new b.b.a.d.b(MessengerShareContentUtility.TEMPLATE_GENERIC_TYPE);
        }
    }

    public final class c extends k implements kotlin.d.a.b<Map<String, ? extends m<? extends b.b.a.h.i, ? extends Exception>>, kotlin.m> {
        public c() {
            super(1);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
         arg types: [java.lang.String, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
        public final Object invoke(Object obj) {
            t a2;
            T t;
            IdFriend idFriend;
            Map map = (Map) obj;
            kotlin.d.b.j.b(map, "response");
            ArrayList<b.b.a.h.i> arrayList = new ArrayList<>();
            Iterator it = map.entrySet().iterator();
            while (true) {
                b.b.a.h.i iVar = null;
                if (it.hasNext()) {
                    Map.Entry entry = (Map.Entry) it.next();
                    entry.getKey();
                    m mVar = (m) entry.getValue();
                    if (mVar instanceof m.a) {
                        iVar = (b.b.a.h.i) ((m.a) mVar).f1097a;
                    } else if (!(mVar instanceof m.b)) {
                        throw new NoWhenBranchMatchedException();
                    }
                    if (iVar != null) {
                        arrayList.add(iVar);
                    }
                } else {
                    q.this.a(new a.C0105a(arrayList));
                    ArrayList arrayList2 = new ArrayList();
                    for (b.b.a.h.i iVar2 : arrayList) {
                        Iterator<T> it2 = iVar2.h.iterator();
                        while (true) {
                            if (!it2.hasNext()) {
                                t = null;
                                break;
                            }
                            t = it2.next();
                            if (kotlin.d.b.j.a((Object) ((b.b.a.h.b) t).f108a, (Object) SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getGame())) {
                                break;
                            }
                        }
                        if (t != null) {
                            String str = iVar2.f122a;
                            String str2 = iVar2.f123b;
                            if (str2 == null) {
                                str2 = "";
                            }
                            String str3 = iVar2.c;
                            if (str3 == null) {
                                str3 = "";
                            }
                            idFriend = new IdFriend(str, str2, str3);
                        } else {
                            idFriend = null;
                        }
                        if (idFriend != null) {
                            arrayList2.add(idFriend);
                        }
                    }
                    if (!(!arrayList2.isEmpty())) {
                        arrayList2 = null;
                    }
                    if (!(arrayList2 == null || (a2 = q.this.d) == null)) {
                        kotlin.d.b.j.b(arrayList2, NativeProtocol.AUDIENCE_FRIENDS);
                        a2.a(new t.a.C0106a(arrayList2));
                    }
                    return kotlin.m.f5330a;
                }
            }
        }
    }

    public final class d extends k implements kotlin.d.a.b<Boolean, kotlin.m> {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ String f1129b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(String str) {
            super(1);
            this.f1129b = str;
        }

        public final Object invoke(Object obj) {
            ((Boolean) obj).booleanValue();
            q.this.a(new a.c(this.f1129b));
            return kotlin.m.f5330a;
        }
    }

    public final class e extends k implements kotlin.d.a.b<Map<String, ? extends m<? extends Boolean, ? extends Exception>>, Boolean> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ String f1130a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(String str) {
            super(1);
            this.f1130a = str;
        }

        public final Object invoke(Object obj) {
            Map map = (Map) obj;
            kotlin.d.b.j.b(map, "it");
            m mVar = (m) map.get(this.f1130a);
            if (mVar instanceof m.a) {
                return true;
            }
            if (mVar instanceof m.b) {
                throw ((Throwable) ((m.b) mVar).f1098a);
            }
            throw new b.b.a.d.b(MessengerShareContentUtility.TEMPLATE_GENERIC_TYPE);
        }
    }

    public final class f extends k implements kotlin.d.a.b<Map<String, ? extends m<? extends Boolean, ? extends Exception>>, kotlin.m> {
        public f() {
            super(1);
        }

        public final Object invoke(Object obj) {
            kotlin.d.b.j.b((Map) obj, "it");
            q.this.f = 0;
            return kotlin.m.f5330a;
        }
    }

    public final class g extends k implements kotlin.d.a.b<b.b.a.h.d, kotlin.m> {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ ap f1133b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(ap apVar) {
            super(1);
            this.f1133b = apVar;
        }

        public final Object invoke(Object obj) {
            b.b.a.h.d dVar = (b.b.a.h.d) obj;
            kotlin.d.b.j.b(dVar, NativeProtocol.AUDIENCE_FRIENDS);
            q.this.a(new a.d(dVar));
            this.f1133b.e(dVar);
            return kotlin.m.f5330a;
        }
    }

    public final class h extends k implements kotlin.d.a.b<Exception, kotlin.m> {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ ap f1135b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(ap apVar) {
            super(1);
            this.f1135b = apVar;
        }

        public final Object invoke(Object obj) {
            Exception exc = (Exception) obj;
            kotlin.d.b.j.b(exc, "it");
            f0 a2 = f0.e.a(exc);
            q.this.a(new a.e(a2));
            this.f1135b.f(a2);
            return kotlin.m.f5330a;
        }
    }

    public final class i extends k implements kotlin.d.a.b<Boolean, kotlin.m> {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ String f1137b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(String str) {
            super(1);
            this.f1137b = str;
        }

        public final Object invoke(Object obj) {
            ((Boolean) obj).booleanValue();
            q.this.a(new a.c(this.f1137b));
            return kotlin.m.f5330a;
        }
    }

    public final class j extends k implements kotlin.d.a.b<Boolean, kotlin.m> {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ String f1139b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(String str) {
            super(1);
            this.f1139b = str;
        }

        public final Object invoke(Object obj) {
            ((Boolean) obj).booleanValue();
            q.this.a(new a.c(this.f1139b));
            t a2 = q.this.d;
            if (a2 != null) {
                String str = this.f1139b;
                kotlin.d.b.j.b(str, "scid");
                a2.a(new t.a.b(str));
            }
            return kotlin.m.f5330a;
        }
    }

    public final bw<b.b.a.h.i, Exception> a(String str) {
        kotlin.d.b.j.b(str, "scid");
        return bc.a(a(kotlin.a.m.a(str)), new b(str));
    }

    public final bw<Map<String, m<b.b.a.h.i, Exception>>, Exception> a(List<String> list) {
        kotlin.d.b.j.b(list, "scids");
        bw<Map<String, m<b.b.a.h.i, Exception>>, Exception> a2 = SupercellId.INSTANCE.getSharedServices$supercellId_release().h.a(list);
        a2.a(new c());
        return a2;
    }

    public final bw<Boolean, Exception> b(String str) {
        kotlin.d.b.j.b(str, "scid");
        bw<Boolean, Exception> a2 = SupercellId.INSTANCE.getSharedServices$supercellId_release().h.a(str);
        a2.a(new d(str));
        return a2;
    }

    public final bw<Boolean, Exception> c(String str) {
        kotlin.d.b.j.b(str, "scid");
        return bc.a(b(kotlin.a.m.a(str)), new e(str));
    }

    public final bw<List<IdFriend>, f0> d(kotlin.d.a.b<? super List<IdFriend>, kotlin.m> bVar) {
        kotlin.d.b.j.b(bVar, "onChanges");
        t tVar = new t(bVar);
        this.d = tVar;
        ap a2 = bb.f5389a;
        SupercellId.INSTANCE.getSharedServices$supercellId_release().h.d().a(new u(tVar, a2)).b(new v(a2));
        return a2.i();
    }

    public final bw<Boolean, Exception> e(String str) {
        kotlin.d.b.j.b(str, "scid");
        bw<Boolean, Exception> f2 = SupercellId.INSTANCE.getSharedServices$supercellId_release().h.f(str);
        f2.a(new j(str));
        return f2;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v11, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v4, resolved type: b.b.a.h.d} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final nl.komponents.kovenant.bw<b.b.a.h.d, b.b.a.j.f0> a() {
        /*
            r9 = this;
            r0 = 1
            r1 = 0
            nl.komponents.kovenant.ap r2 = nl.komponents.kovenant.bb.f5389a
            T r3 = r9.f1179a
            b.b.a.j.m r3 = (b.b.a.j.m) r3
            long r4 = java.lang.System.currentTimeMillis()
            long r6 = r9.e
            long r4 = r4 - r6
            long r6 = r9.f
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 <= 0) goto L_0x0018
            goto L_0x0019
        L_0x0018:
            r0 = 0
        L_0x0019:
            if (r0 != 0) goto L_0x002f
            if (r3 == 0) goto L_0x0024
            java.lang.Object r0 = r3.a()
            r1 = r0
            b.b.a.h.d r1 = (b.b.a.h.d) r1
        L_0x0024:
            if (r1 != 0) goto L_0x0027
            goto L_0x002f
        L_0x0027:
            java.lang.Object r0 = r3.a()
            r2.e(r0)
            goto L_0x0057
        L_0x002f:
            b.b.a.j.q$a$b r0 = b.b.a.j.q.a.b.f1122a
            r9.a(r0)
            com.supercell.id.SupercellId r0 = com.supercell.id.SupercellId.INSTANCE
            b.b.a.j.x r0 = r0.getSharedServices$supercellId_release()
            b.b.a.d.e r0 = r0.h
            nl.komponents.kovenant.bw r0 = r0.a()
            b.b.a.j.q$g r1 = new b.b.a.j.q$g
            r1.<init>(r2)
            nl.komponents.kovenant.bw r0 = r0.a(r1)
            b.b.a.j.q$h r1 = new b.b.a.j.q$h
            r1.<init>(r2)
            r0.b(r1)
            long r0 = java.lang.System.currentTimeMillis()
            r9.f = r0
        L_0x0057:
            nl.komponents.kovenant.bw r0 = r2.i()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.j.q.a():nl.komponents.kovenant.bw");
    }

    public final bw<Map<String, m<Boolean, Exception>>, Exception> b(List<String> list) {
        kotlin.d.b.j.b(list, "scids");
        bw<Map<String, m<Boolean, Exception>>, Exception> b2 = SupercellId.INSTANCE.getSharedServices$supercellId_release().h.b(list);
        b2.a(new f());
        return b2;
    }

    public final bw<Boolean, Exception> d(String str) {
        kotlin.d.b.j.b(str, "scid");
        bw<Boolean, Exception> e2 = SupercellId.INSTANCE.getSharedServices$supercellId_release().h.e(str);
        e2.a(new i(str));
        return e2;
    }
}
