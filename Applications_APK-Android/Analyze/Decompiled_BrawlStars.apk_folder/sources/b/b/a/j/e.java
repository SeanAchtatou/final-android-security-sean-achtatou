package b.b.a.j;

import android.graphics.Color;
import android.support.v4.view.ViewCompat;
import com.facebook.appevents.AppEventsConstants;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import kotlin.a.m;
import kotlin.d.b.g;
import kotlin.d.b.j;
import kotlin.d.b.v;
import kotlin.f.d;
import kotlin.j.t;

public final class e {
    public static final d c = new d(-1447447, -1447447);
    public static final e d = new e("archer", c);
    public static final a e = new a(null);

    /* renamed from: a  reason: collision with root package name */
    public final String f1027a;

    /* renamed from: b  reason: collision with root package name */
    public final d f1028b;

    public static final class a {
        public /* synthetic */ a(g gVar) {
        }

        public final e a(String str, d dVar) {
            if (str == null || dVar == null) {
                return null;
            }
            return new e(str, dVar);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
         arg types: [java.util.List<java.lang.String>, kotlin.f.d$b]
         candidates:
          kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
          kotlin.a.w.a(java.util.List, int):T
          kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
          kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
          kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
          kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
          kotlin.a.s.a(java.util.List, java.util.Comparator):void
          kotlin.a.p.a(java.lang.Iterable, int):int
          kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
          kotlin.a.w.a(java.util.Collection, kotlin.f.d):T */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
         arg types: [java.util.List<b.b.a.j.d>, kotlin.f.d$b]
         candidates:
          kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
          kotlin.a.w.a(java.util.List, int):T
          kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
          kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
          kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
          kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
          kotlin.a.s.a(java.util.List, java.util.Comparator):void
          kotlin.a.p.a(java.lang.Iterable, int):int
          kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
          kotlin.a.w.a(java.util.Collection, kotlin.f.d):T */
        public final e b() {
            List<String> b2 = k0.f1078b.b();
            List<d> a2 = k0.f1078b.a();
            Object obj = null;
            String str = (String) (b2.isEmpty() ? null : m.a((Collection) b2, (d) d.c));
            if (str == null) {
                str = "archer";
            }
            if (!a2.isEmpty()) {
                obj = m.a((Collection) a2, (d) d.c);
            }
            d dVar = (d) obj;
            if (dVar == null) {
                dVar = e.c;
            }
            return new e(str, dVar);
        }

        public final d a() {
            return e.c;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.j.ac.b(java.lang.CharSequence, java.lang.String[], boolean, int):java.util.List<java.lang.String>
         arg types: [java.lang.String, java.lang.String[], int, int]
         candidates:
          kotlin.j.ac.b(java.lang.CharSequence, java.lang.String, int, boolean):int
          kotlin.j.ac.b(java.lang.CharSequence, java.lang.String[], boolean, int):java.util.List<java.lang.String> */
        public final e a(String str) {
            j.b(str, "avatarString");
            List<String> b2 = t.b((CharSequence) str, new String[]{","}, false, 0);
            return (b2.size() != 4 || (j.a(b2.get(0), AppEventsConstants.EVENT_PARAM_VALUE_NO) ^ true)) ? e.d : new e(b2.get(1), new d(Color.parseColor(b2.get(2)), Color.parseColor(b2.get(3))));
        }
    }

    public e(String str, d dVar) {
        j.b(str, "name");
        j.b(dVar, "background");
        this.f1027a = str;
        this.f1028b = dVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final String a() {
        StringBuilder a2 = b.a.a.a.a.a("0,");
        a2.append(this.f1027a);
        a2.append(',');
        v vVar = v.f5255a;
        String format = String.format("#%06X", Arrays.copyOf(new Object[]{Integer.valueOf(this.f1028b.f1023a & ViewCompat.MEASURED_SIZE_MASK)}, 1));
        j.a((Object) format, "java.lang.String.format(format, *args)");
        a2.append(format);
        a2.append(',');
        v vVar2 = v.f5255a;
        String format2 = String.format("#%06X", Arrays.copyOf(new Object[]{Integer.valueOf(this.f1028b.f1024b & ViewCompat.MEASURED_SIZE_MASK)}, 1));
        j.a((Object) format2, "java.lang.String.format(format, *args)");
        a2.append(format2);
        return a2.toString();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof e)) {
            return false;
        }
        e eVar = (e) obj;
        return j.a(this.f1027a, eVar.f1027a) && j.a(this.f1028b, eVar.f1028b);
    }

    public final int hashCode() {
        String str = this.f1027a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        d dVar = this.f1028b;
        if (dVar != null) {
            i = dVar.hashCode();
        }
        return hashCode + i;
    }

    public final String toString() {
        return a();
    }
}
