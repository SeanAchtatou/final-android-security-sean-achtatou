package b.b.a.j;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.AppCompatButton;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ScrollView;
import b.b.a.e.a;
import com.supercell.id.SupercellId;
import com.supercell.id.view.RootFrameLayout;
import java.util.WeakHashMap;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.d.b.r;
import kotlin.d.b.s;
import kotlin.d.b.t;
import kotlin.m;

public final class q1 {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ kotlin.h.h[] f1142a = {t.a(new r(t.a(q1.class, "supercellId_release"), "innerShadowLayerType", "getInnerShadowLayerType(Landroid/view/View;)I"))};

    /* renamed from: b  reason: collision with root package name */
    public static final kotlin.d f1143b = kotlin.e.a(b.f1146a);
    public static final WeakHashMap<View, z> c = new WeakHashMap<>();

    public final class a implements View.OnLayoutChangeListener {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ View f1144a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ kotlin.d.a.a f1145b;

        public a(View view, kotlin.d.a.a aVar) {
            this.f1144a = view;
            this.f1145b = aVar;
        }

        /* JADX WARN: Type inference failed for: r3v1, types: [b.b.a.j.r1] */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void onLayoutChange(android.view.View r1, int r2, int r3, int r4, int r5, int r6, int r7, int r8, int r9) {
            /*
                r0 = this;
                android.view.View r1 = r0.f1144a
                r1.removeOnLayoutChangeListener(r0)
                android.view.View r1 = r0.f1144a
                kotlin.d.a.a r2 = r0.f1145b
                if (r2 == 0) goto L_0x0011
                b.b.a.j.r1 r3 = new b.b.a.j.r1
                r3.<init>(r2)
                r2 = r3
            L_0x0011:
                java.lang.Runnable r2 = (java.lang.Runnable) r2
                r1.post(r2)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: b.b.a.j.q1.a.onLayoutChange(android.view.View, int, int, int, int, int, int, int, int):void");
        }
    }

    public final class b extends k implements kotlin.d.a.a<Integer> {

        /* renamed from: a  reason: collision with root package name */
        public static final b f1146a = new b();

        public b() {
            super(0);
        }

        public final Object invoke() {
            return Integer.valueOf(Build.VERSION.SDK_INT >= 28 ? 2 : 1);
        }
    }

    public final class c implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ View f1147a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ ScrollView f1148b;
        public final /* synthetic */ int c;

        public c(View view, ScrollView scrollView, int i) {
            this.f1147a = view;
            this.f1148b = scrollView;
            this.c = i;
        }

        public final void run() {
            int[] iArr = new int[2];
            this.f1147a.getLocationInWindow(iArr);
            int height = this.f1147a.getHeight() + iArr[1];
            this.f1148b.getLocationInWindow(iArr);
            int height2 = (((this.f1148b.getHeight() + iArr[1]) - this.f1148b.getPaddingBottom()) - this.c) - ((int) b.b.a.b.a(4));
            if (height > height2) {
                this.f1148b.smoothScrollTo(0, height - height2);
            }
        }
    }

    public final class d implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ View f1149a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ NestedScrollView f1150b;
        public final /* synthetic */ int c;

        public d(View view, NestedScrollView nestedScrollView, int i) {
            this.f1149a = view;
            this.f1150b = nestedScrollView;
            this.c = i;
        }

        public final void run() {
            int[] iArr = new int[2];
            this.f1149a.getLocationInWindow(iArr);
            int height = this.f1149a.getHeight() + iArr[1];
            this.f1150b.getLocationInWindow(iArr);
            int height2 = (((this.f1150b.getHeight() + iArr[1]) - this.f1150b.getPaddingBottom()) - this.c) - ((int) b.b.a.b.a(4));
            if (height > height2) {
                this.f1150b.smoothScrollTo(0, height - height2);
            }
        }
    }

    public final class e extends View.AccessibilityDelegate {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ int f1151a;

        public e(int i) {
            this.f1151a = i;
        }

        public final void sendAccessibilityEvent(View view, int i) {
            super.sendAccessibilityEvent(view, i);
            if (i == 1) {
                SupercellId.INSTANCE.getSharedServices$supercellId_release().l.a(a.C0004a.g.a(this.f1151a));
            }
        }
    }

    public final class f implements View.OnTouchListener {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ s.a f1152a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ int f1153b;
        public final /* synthetic */ int c;

        public f(s.a aVar, int i, int i2) {
            this.f1152a = aVar;
            this.f1153b = i;
            this.c = i2;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.MotionEvent, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX WARNING: Code restructure failed: missing block: B:10:0x0021, code lost:
            if (r7 != 1) goto L_0x00ce;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0057, code lost:
            if (r7 != 1) goto L_0x00ce;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final boolean onTouch(android.view.View r6, android.view.MotionEvent r7) {
            /*
                r5 = this;
                java.lang.String r0 = "event"
                kotlin.d.b.j.a(r7, r0)
                int r0 = r7.getAction()
                r1 = 0
                r2 = 1
                if (r0 == 0) goto L_0x008b
                r3 = 1065353216(0x3f800000, float:1.0)
                if (r0 == r2) goto L_0x005b
                r4 = 2
                if (r0 == r4) goto L_0x003c
                r7 = 3
                if (r0 == r7) goto L_0x0019
                goto L_0x00ce
            L_0x0019:
                kotlin.d.b.s$a r7 = r5.f1152a
                r7.f5251a = r1
                int r7 = r5.f1153b
                if (r7 == 0) goto L_0x002f
                if (r7 == r2) goto L_0x0025
                goto L_0x00ce
            L_0x0025:
                android.view.ViewPropertyAnimator r6 = r6.animate()
                android.view.ViewPropertyAnimator r6 = r6.alpha(r3)
                goto L_0x00a0
            L_0x002f:
                android.view.ViewPropertyAnimator r6 = r6.animate()
                android.view.ViewPropertyAnimator r6 = r6.scaleX(r3)
                android.view.ViewPropertyAnimator r6 = r6.scaleY(r3)
                goto L_0x007b
            L_0x003c:
                kotlin.d.b.s$a r0 = r5.f1152a
                boolean r0 = r0.f5251a
                if (r0 == 0) goto L_0x00ce
                java.lang.String r0 = "v"
                kotlin.d.b.j.a(r6, r0)
                int r0 = r5.c
                boolean r7 = b.b.a.j.q1.a(r6, r7, r0)
                if (r7 != 0) goto L_0x00ce
                kotlin.d.b.s$a r7 = r5.f1152a
                r7.f5251a = r1
                int r7 = r5.f1153b
                if (r7 == 0) goto L_0x006f
                if (r7 == r2) goto L_0x0025
                goto L_0x00ce
            L_0x005b:
                kotlin.d.b.s$a r7 = r5.f1152a
                r7.f5251a = r1
                int r7 = r5.f1153b
                if (r7 == 0) goto L_0x006f
                if (r7 == r2) goto L_0x0066
                goto L_0x00ce
            L_0x0066:
                android.view.ViewPropertyAnimator r6 = r6.animate()
                android.view.ViewPropertyAnimator r6 = r6.alpha(r3)
                goto L_0x00a0
            L_0x006f:
                android.view.ViewPropertyAnimator r6 = r6.animate()
                android.view.ViewPropertyAnimator r6 = r6.scaleX(r3)
                android.view.ViewPropertyAnimator r6 = r6.scaleY(r3)
            L_0x007b:
                android.view.animation.Interpolator r7 = b.b.a.f.a.f
                android.view.ViewPropertyAnimator r6 = r6.setInterpolator(r7)
                r2 = 120(0x78, double:5.93E-322)
                android.view.ViewPropertyAnimator r6 = r6.setDuration(r2)
                r6.start()
                goto L_0x00ce
            L_0x008b:
                kotlin.d.b.s$a r7 = r5.f1152a
                r7.f5251a = r2
                int r7 = r5.f1153b
                if (r7 == 0) goto L_0x00b0
                if (r7 == r2) goto L_0x0096
                goto L_0x00ce
            L_0x0096:
                android.view.ViewPropertyAnimator r6 = r6.animate()
                r7 = 1056964608(0x3f000000, float:0.5)
                android.view.ViewPropertyAnimator r6 = r6.alpha(r7)
            L_0x00a0:
                android.view.animation.Interpolator r7 = b.b.a.f.a.c
                android.view.ViewPropertyAnimator r6 = r6.setInterpolator(r7)
                r2 = 100
                android.view.ViewPropertyAnimator r6 = r6.setDuration(r2)
                r6.start()
                goto L_0x00ce
            L_0x00b0:
                android.view.ViewPropertyAnimator r6 = r6.animate()
                r7 = 1064514355(0x3f733333, float:0.95)
                android.view.ViewPropertyAnimator r6 = r6.scaleX(r7)
                android.view.ViewPropertyAnimator r6 = r6.scaleY(r7)
                android.view.animation.Interpolator r7 = b.b.a.f.a.d
                android.view.ViewPropertyAnimator r6 = r6.setInterpolator(r7)
                r2 = 180(0xb4, double:8.9E-322)
                android.view.ViewPropertyAnimator r6 = r6.setDuration(r2)
                r6.start()
            L_0x00ce:
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: b.b.a.j.q1.f.onTouch(android.view.View, android.view.MotionEvent):boolean");
        }
    }

    public final class g implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ ScrollView f1154a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ View f1155b;
        public final /* synthetic */ int c;

        public g(ScrollView scrollView, View view, int i) {
            this.f1154a = scrollView;
            this.f1155b = view;
            this.c = i;
        }

        public final void run() {
            int[] iArr = new int[2];
            this.f1155b.getLocationInWindow(iArr);
            int height = this.f1155b.getHeight() + iArr[1];
            this.f1154a.getLocationInWindow(iArr);
            int height2 = (((this.f1154a.getHeight() + iArr[1]) - this.f1154a.getPaddingBottom()) - this.c) - ((int) b.b.a.b.a(4));
            if (height > height2) {
                this.f1154a.smoothScrollTo(0, height - height2);
            }
        }
    }

    public final class h implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ NestedScrollView f1156a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ View f1157b;
        public final /* synthetic */ int c;

        public h(NestedScrollView nestedScrollView, View view, int i) {
            this.f1156a = nestedScrollView;
            this.f1157b = view;
            this.c = i;
        }

        public final void run() {
            int[] iArr = new int[2];
            this.f1157b.getLocationInWindow(iArr);
            int height = this.f1157b.getHeight() + iArr[1];
            this.f1156a.getLocationInWindow(iArr);
            int height2 = (((this.f1156a.getHeight() + iArr[1]) - this.f1156a.getPaddingBottom()) - this.c) - ((int) b.b.a.b.a(4));
            if (height > height2) {
                this.f1156a.smoothScrollTo(0, height - height2);
            }
        }
    }

    public final class i extends k implements kotlin.d.a.a<m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ View f1158a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ long f1159b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(View view, long j) {
            super(0);
            this.f1158a = view;
            this.f1159b = j;
        }

        public final Object invoke() {
            this.f1158a.postDelayed(new s1(this), this.f1159b);
            return m.f5330a;
        }
    }

    public static final int a(View view) {
        j.b(view, "$this$endPadding");
        return Build.VERSION.SDK_INT >= 17 ? view.getPaddingEnd() : view.getPaddingRight();
    }

    public static final void a(NestedScrollView nestedScrollView, int i2) {
        View currentFocus;
        Context context = nestedScrollView.getContext();
        if (!(context instanceof Activity)) {
            context = null;
        }
        Activity activity = (Activity) context;
        if (activity != null && (currentFocus = activity.getCurrentFocus()) != null) {
            nestedScrollView.post(new d(currentFocus, nestedScrollView, i2));
        }
    }

    public static final void a(NestedScrollView nestedScrollView, View view) {
        RootFrameLayout f2;
        j.b(nestedScrollView, "$this$smoothScrollTo");
        j.b(view, "view");
        int i2 = 0;
        if (Build.VERSION.SDK_INT < 28 && (f2 = f(nestedScrollView)) != null) {
            int i3 = b(nestedScrollView).bottom - (b(f2).bottom - f2.getSystemWindowInsets().bottom);
            int i4 = f2.getSystemWindowInsets().bottom;
            if (j.a(i3, 0) >= 0) {
                i2 = j.a(i3, i4) > 0 ? i4 : i3;
            }
        }
        nestedScrollView.post(new h(nestedScrollView, view, i2));
    }

    public static final void a(AppCompatButton appCompatButton, boolean z) {
        j.b(appCompatButton, "$this$isDimmed");
        appCompatButton.setTextColor(b.b.a.b.a(appCompatButton.getCurrentTextColor(), z ? 0.5f : 1.0f));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.ViewConfiguration, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final void a(View view, int i2, int i3) {
        j.b(view, "$this$setTouchListener");
        if (i3 >= 0) {
            view.setAccessibilityDelegate(new e(i3));
        }
        if (i2 >= 0) {
            s.a aVar = new s.a();
            aVar.f5251a = false;
            ViewConfiguration viewConfiguration = ViewConfiguration.get(view.getContext());
            j.a((Object) viewConfiguration, "ViewConfiguration.get(context)");
            view.setOnTouchListener(new f(aVar, i2, viewConfiguration.getScaledTouchSlop()));
        }
    }

    public static final void a(View view, int i2, int i3, int i4, int i5) {
        if (i2 != view.getPaddingLeft() || i3 != view.getPaddingTop() || i4 != view.getPaddingRight() || i5 != view.getPaddingBottom()) {
            view.setPadding(i2, i3, i4, i5);
        }
    }

    public static final void a(View view, int i2, boolean z, int i3) {
        j.b(view, "$this$setPaddingInsetUpdater");
        g0 g0Var = new g0(i2, z, i3);
        if (!j.a(c.get(view), g0Var)) {
            a(view, g0Var);
        }
    }

    public static final void a(View view, long j) {
        j.b(view, "$this$springEntry");
        view.setScaleY(0.0f);
        view.setScaleX(0.0f);
        a(view, new i(view, j));
    }

    public static final void a(View view, z zVar) {
        j.b(view, "$this$setInsetUpdater");
        j.b(zVar, "updater");
        boolean isAttachedToWindow = ViewCompat.isAttachedToWindow(view);
        z remove = c.remove(view);
        if (remove != null) {
            view.removeOnAttachStateChangeListener(remove);
            if (isAttachedToWindow) {
                remove.onViewDetachedFromWindow(view);
            }
        }
        c.put(view, zVar);
        view.addOnAttachStateChangeListener(zVar);
        if (isAttachedToWindow) {
            zVar.onViewAttachedToWindow(view);
        }
    }

    public static final void a(View view, kotlin.d.a.a<m> aVar) {
        j.b(view, "$this$afterLaidOut");
        j.b(aVar, "block");
        if (ViewCompat.isLaidOut(view)) {
            aVar.invoke();
        } else {
            view.addOnLayoutChangeListener(new a(view, aVar));
        }
    }

    public static final void a(ScrollView scrollView, int i2) {
        View currentFocus;
        Context context = scrollView.getContext();
        if (!(context instanceof Activity)) {
            context = null;
        }
        Activity activity = (Activity) context;
        if (activity != null && (currentFocus = activity.getCurrentFocus()) != null) {
            scrollView.post(new c(currentFocus, scrollView, i2));
        }
    }

    public static final void a(ScrollView scrollView, View view) {
        RootFrameLayout f2;
        j.b(scrollView, "$this$smoothScrollTo");
        j.b(view, "view");
        int i2 = 0;
        if (Build.VERSION.SDK_INT < 28 && (f2 = f(scrollView)) != null) {
            int i3 = b(scrollView).bottom - (b(f2).bottom - f2.getSystemWindowInsets().bottom);
            int i4 = f2.getSystemWindowInsets().bottom;
            if (j.a(i3, 0) >= 0) {
                i2 = j.a(i3, i4) > 0 ? i4 : i3;
            }
        }
        scrollView.post(new g(scrollView, view, i2));
    }

    public static final /* synthetic */ boolean a(View view, MotionEvent motionEvent, int i2) {
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        float f2 = (float) (-i2);
        return x >= f2 && y >= f2 && x < ((float) ((view.getRight() - view.getLeft()) + i2)) && y < ((float) ((view.getBottom() - view.getTop()) + i2));
    }

    public static final Rect b(View view) {
        j.b(view, "$this$frameOnScreen");
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        return new Rect(iArr[0], iArr[1], view.getWidth() + iArr[0], view.getHeight() + iArr[1]);
    }

    public static final void b(View view, int i2) {
        j.b(view, "$this$endMargin");
        ViewGroup.MarginLayoutParams d2 = d(view);
        if (d2 != null) {
            boolean z = true;
            if (ViewCompat.getLayoutDirection(view) != 1) {
                z = false;
            }
            if (z) {
                d2.leftMargin = i2;
            } else {
                d2.rightMargin = i2;
            }
            if (Build.VERSION.SDK_INT >= 17) {
                d2.setMarginEnd(i2);
            }
        }
    }

    public static final int c(View view) {
        j.b(view, "$this$innerShadowLayerType");
        return ((Number) f1143b.a()).intValue();
    }

    public static final void c(View view, int i2) {
        j.b(view, "$this$endPadding");
        if (Build.VERSION.SDK_INT >= 17) {
            view.setPaddingRelative(view.getPaddingStart(), view.getPaddingTop(), i2, view.getPaddingBottom());
        } else {
            view.setPadding(view.getPaddingLeft(), view.getPaddingTop(), i2, view.getPaddingBottom());
        }
    }

    public static final ViewGroup.MarginLayoutParams d(View view) {
        j.b(view, "$this$marginLayoutParams");
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (!(layoutParams instanceof ViewGroup.MarginLayoutParams)) {
            layoutParams = null;
        }
        return (ViewGroup.MarginLayoutParams) layoutParams;
    }

    public static final void d(View view, int i2) {
        j.b(view, "$this$originalPaddingTop");
        z zVar = c.get(view);
        if (!(zVar instanceof g0)) {
            zVar = null;
        }
        g0 g0Var = (g0) zVar;
        if (g0Var != null) {
            g0Var.g = Integer.valueOf(i2);
            return;
        }
        int paddingLeft = view.getPaddingLeft();
        int paddingRight = view.getPaddingRight();
        int paddingBottom = view.getPaddingBottom();
        if (paddingLeft != view.getPaddingLeft() || i2 != view.getPaddingTop() || paddingRight != view.getPaddingRight() || paddingBottom != view.getPaddingBottom()) {
            view.setPadding(paddingLeft, i2, paddingRight, paddingBottom);
        }
    }

    public static final int e(View view) {
        Integer num;
        j.b(view, "$this$originalPaddingTop");
        z zVar = c.get(view);
        if (!(zVar instanceof g0)) {
            zVar = null;
        }
        g0 g0Var = (g0) zVar;
        return (g0Var == null || (num = g0Var.g) == null) ? view.getPaddingTop() : num.intValue();
    }

    public static final RootFrameLayout f(View view) {
        while (true) {
            ViewParent parent = view.getParent();
            if (parent == null || !(parent instanceof View)) {
                return null;
            }
            if (parent instanceof RootFrameLayout) {
                return (RootFrameLayout) parent;
            }
            view = (View) parent;
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static final <T extends android.view.View> void g(T r4) {
        /*
            java.lang.String r0 = "$this$relayout"
            kotlin.d.b.j.b(r4, r0)
            int r0 = r4.getMeasuredWidth()
            r1 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r1)
            int r2 = r4.getMeasuredHeight()
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r1)
            r4.measure(r0, r1)
            int r0 = r4.getLeft()
            int r1 = r4.getTop()
            int r2 = r4.getRight()
            int r3 = r4.getBottom()
            r4.layout(r0, r1, r2, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.j.q1.g(android.view.View):void");
    }

    public static final /* synthetic */ View a(View view, int i2) {
        if (i2 > 0) {
            do {
                ViewParent parent = view.getParent();
                if (parent != null && (parent instanceof View)) {
                    view = (View) parent;
                }
            } while (view.getId() != i2);
            return view;
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final void a(View view, float f2, float f3, float f4, float f5, float f6) {
        j.b(view, "$this$setupBackgroundInnerShadow");
        view.setLayerType(c(view), null);
        Drawable background = view.getBackground();
        if (!(background instanceof ColorDrawable)) {
            background = null;
        }
        ColorDrawable colorDrawable = (ColorDrawable) background;
        if (colorDrawable != null) {
            int color = colorDrawable.getColor();
            b.b.a.g.e eVar = b.b.a.g.e.f75b;
            Resources resources = view.getResources();
            j.a((Object) resources, "resources");
            float f7 = b.b.a.b.f16a;
            ViewCompat.setBackground(view, eVar.a(resources, color, f2 * f7, f3 * f7, f4 * f7, f5, f6 * f7));
        }
    }
}
