package b.b.a.j;

import android.support.v4.app.NotificationCompat;
import java.util.ArrayList;
import java.util.List;
import kotlin.a.m;
import kotlin.d.b.j;
import kotlin.i.h;
import kotlin.i.i;
import kotlin.j.o;
import kotlin.j.t;

public final class n {

    /* renamed from: a  reason: collision with root package name */
    public static final o f1101a = new o("^(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$");

    /* renamed from: b  reason: collision with root package name */
    public static final n f1102b = new n();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.j.ac.a(java.lang.CharSequence, char, int, boolean, int):int
     arg types: [java.lang.String, int, int, int, int]
     candidates:
      kotlin.j.ac.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int
      kotlin.j.ac.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):kotlin.i.h
      kotlin.j.ac.a(java.lang.CharSequence, char, int, boolean, int):int */
    public final boolean b(String str) {
        int a2 = t.a((CharSequence) str, '@', 0, false, 6);
        if (a2 <= 0) {
            return false;
        }
        CharSequence subSequence = str.subSequence(0, a2);
        Character ch = null;
        int i = 0;
        int i2 = 0;
        while (i < subSequence.length()) {
            char charAt = subSequence.charAt(i);
            int i3 = i2 + 1;
            switch (charAt) {
                case '!':
                case '#':
                case '$':
                case '%':
                case '&':
                case '\'':
                case '*':
                case '+':
                case '-':
                case '/':
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case '=':
                case '?':
                case 'A':
                case 'B':
                case 'C':
                case 'D':
                case 'E':
                case 'F':
                case 'G':
                case 'H':
                case 'I':
                case 'J':
                case 'K':
                case 'L':
                case 'M':
                case 'N':
                case 'O':
                case 'P':
                case 'Q':
                case 'R':
                case 'S':
                case 'T':
                case 'U':
                case 'V':
                case 'W':
                case 'X':
                case 'Y':
                case 'Z':
                case '^':
                case '_':
                case '`':
                case 'a':
                case 'b':
                case 'c':
                case 'd':
                case 'e':
                case 'f':
                case 'g':
                case 'h':
                case 'i':
                case 'j':
                case 'k':
                case 'l':
                case 'm':
                case 'n':
                case 'o':
                case 'p':
                case 'q':
                case 'r':
                case 's':
                case 't':
                case 'u':
                case 'v':
                case 'w':
                case 'x':
                case 'y':
                case 'z':
                case '{':
                case '|':
                case '}':
                case '~':
                    break;
                case '\"':
                case '(':
                case ')':
                case ',':
                case ':':
                case ';':
                case '<':
                case '>':
                case '@':
                case '[':
                case '\\':
                case ']':
                default:
                    return false;
                case '.':
                    if (i2 == 0 || i2 == a2 - 1) {
                        return false;
                    }
                    if (ch != null && ch.charValue() == charAt) {
                        return false;
                    }
                    break;
            }
            ch = Character.valueOf(charAt);
            i++;
            i2 = i3;
        }
        return true;
    }

    public final boolean c(String str) {
        j.b(str, NotificationCompat.CATEGORY_EMAIL);
        return (str.length() > 0) && str.length() < 128 && a(str) && b(str);
    }

    public final String d(String str) {
        StringBuilder sb;
        j.b(str, NotificationCompat.CATEGORY_EMAIL);
        String str2 = "";
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt != '+') {
                if (charAt != '@') {
                    if (charAt != '-') {
                        if (charAt != '.') {
                            sb = new StringBuilder();
                            sb.append(str2);
                            sb.append(charAt);
                            str2 = sb.toString();
                        }
                    }
                }
                sb = new StringBuilder();
                sb.append(str2);
                sb.append(8203);
                sb.append(charAt);
                str2 = sb.toString();
            }
            sb = new StringBuilder();
            sb.append(str2);
            sb.append(charAt);
            sb.append(8203);
            str2 = sb.toString();
        }
        return str2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.j.ac.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):kotlin.i.h
     arg types: [java.lang.String, java.lang.String[], int, int, int]
     candidates:
      kotlin.j.ac.a(java.lang.CharSequence, char, int, boolean, int):int
      kotlin.j.ac.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int
      kotlin.j.ac.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):kotlin.i.h */
    public final boolean a(String str) {
        h a2 = t.a((CharSequence) str, new String[]{"@"}, false, 3, 2);
        j.b(a2, "$this$toList");
        j.b(a2, "$this$toMutableList");
        List b2 = m.b((List) i.a(a2, new ArrayList()));
        if (b2.size() != 2) {
            return false;
        }
        String str2 = (String) m.e(b2);
        o oVar = f1101a;
        j.b(str2, "input");
        return oVar.f5321a.matcher(str2).matches();
    }
}
