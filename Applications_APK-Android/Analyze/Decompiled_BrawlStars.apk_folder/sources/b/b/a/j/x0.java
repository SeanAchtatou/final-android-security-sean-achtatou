package b.b.a.j;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import com.supercell.id.SupercellId;
import java.util.List;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.m;

public final class x0 extends FragmentPagerAdapter {

    /* renamed from: a  reason: collision with root package name */
    public List<w0> f1192a;

    /* renamed from: b  reason: collision with root package name */
    public View f1193b;
    public final b<View, m> c;

    /* JADX WARN: Type inference failed for: r4v0, types: [kotlin.d.a.b<? super android.view.View, kotlin.m>, java.lang.Object, kotlin.d.a.b<android.view.View, kotlin.m>] */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public x0(android.support.v4.app.FragmentManager r2, java.util.List<b.b.a.j.w0> r3, kotlin.d.a.b<? super android.view.View, kotlin.m> r4) {
        /*
            r1 = this;
            java.lang.String r0 = "fm"
            kotlin.d.b.j.b(r2, r0)
            java.lang.String r0 = "tabs"
            kotlin.d.b.j.b(r3, r0)
            java.lang.String r0 = "selectedListener"
            kotlin.d.b.j.b(r4, r0)
            r1.<init>(r2)
            r1.c = r4
            r1.f1192a = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.j.x0.<init>(android.support.v4.app.FragmentManager, java.util.List, kotlin.d.a.b):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.a(java.util.List, int):T
     arg types: [java.util.List<b.b.a.j.w0>, int]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.w.a(java.util.List, int):T */
    public final String a(int i) {
        w0 w0Var = (w0) kotlin.a.m.a((List) this.f1192a, i);
        if (w0Var != null) {
            return w0Var.f1187a;
        }
        return null;
    }

    public final int getCount() {
        return this.f1192a.size();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [? extends android.support.v4.app.Fragment, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final Fragment getItem(int i) {
        Object newInstance = this.f1192a.get(i).f1188b.newInstance();
        j.a((Object) newInstance, "tabs[position].fragmentClass.newInstance()");
        return (Fragment) newInstance;
    }

    public final CharSequence getPageTitle(int i) {
        return SupercellId.INSTANCE.getSharedServices$supercellId_release().j.b(this.f1192a.get(i).f1187a);
    }

    public final void setPrimaryItem(ViewGroup viewGroup, int i, Object obj) {
        j.b(viewGroup, "container");
        j.b(obj, "object");
        super.setPrimaryItem(viewGroup, i, obj);
        View view = null;
        if (!(obj instanceof Fragment)) {
            obj = null;
        }
        Fragment fragment = (Fragment) obj;
        if (fragment != null) {
            view = fragment.getView();
        }
        if (!j.a(view, this.f1193b)) {
            this.f1193b = view;
            this.c.invoke(view);
        }
    }
}
