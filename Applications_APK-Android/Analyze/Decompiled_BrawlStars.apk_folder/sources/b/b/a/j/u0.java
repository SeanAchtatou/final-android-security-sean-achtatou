package b.b.a.j;

import android.support.v7.util.DiffUtil;
import b.a.a.a.a;
import java.util.List;
import kotlin.d.b.j;

public final class u0 {

    /* renamed from: a  reason: collision with root package name */
    public final List<r0> f1176a;

    /* renamed from: b  reason: collision with root package name */
    public final List<r0> f1177b;
    public final DiffUtil.DiffResult c;

    /* JADX WARN: Type inference failed for: r2v0, types: [java.util.List<b.b.a.j.r0>, java.util.List<? extends b.b.a.j.r0>] */
    /* JADX WARN: Type inference failed for: r3v0, types: [java.util.List<b.b.a.j.r0>, java.util.List<? extends b.b.a.j.r0>] */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public u0(java.util.List<? extends b.b.a.j.r0> r2, java.util.List<? extends b.b.a.j.r0> r3, android.support.v7.util.DiffUtil.DiffResult r4) {
        /*
            r1 = this;
            java.lang.String r0 = "result"
            kotlin.d.b.j.b(r4, r0)
            r1.<init>()
            r1.f1176a = r2
            r1.f1177b = r3
            r1.c = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.j.u0.<init>(java.util.List, java.util.List, android.support.v7.util.DiffUtil$DiffResult):void");
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof u0)) {
            return false;
        }
        u0 u0Var = (u0) obj;
        return j.a(this.f1176a, u0Var.f1176a) && j.a(this.f1177b, u0Var.f1177b) && j.a(this.c, u0Var.c);
    }

    public final int hashCode() {
        List<r0> list = this.f1176a;
        int i = 0;
        int hashCode = (list != null ? list.hashCode() : 0) * 31;
        List<r0> list2 = this.f1177b;
        int hashCode2 = (hashCode + (list2 != null ? list2.hashCode() : 0)) * 31;
        DiffUtil.DiffResult diffResult = this.c;
        if (diffResult != null) {
            i = diffResult.hashCode();
        }
        return hashCode2 + i;
    }

    public final String toString() {
        StringBuilder a2 = a.a("RowDiffUtilResult(oldRows=");
        a2.append(this.f1176a);
        a2.append(", newRows=");
        a2.append(this.f1177b);
        a2.append(", result=");
        a2.append(this.c);
        a2.append(")");
        return a2.toString();
    }
}
