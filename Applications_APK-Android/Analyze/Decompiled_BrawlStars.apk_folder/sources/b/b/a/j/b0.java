package b.b.a.j;

import b.a.a.a.a;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.supercell.id.R;
import kotlin.d.b.j;

public final class b0 implements r0 {

    /* renamed from: a  reason: collision with root package name */
    public final int f1012a = R.layout.list_item_message;

    /* renamed from: b  reason: collision with root package name */
    public final String f1013b;

    public b0(String str) {
        j.b(str, "messageKey");
        this.f1013b = str;
    }

    public final int a() {
        return this.f1012a;
    }

    public final boolean a(r0 r0Var) {
        j.b(r0Var, FacebookRequestErrorClassification.KEY_OTHER);
        return r0Var instanceof b0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    public final boolean b(r0 r0Var) {
        j.b(r0Var, FacebookRequestErrorClassification.KEY_OTHER);
        if (!(r0Var instanceof b0)) {
            return false;
        }
        return j.a((Object) this.f1013b, (Object) ((b0) r0Var).f1013b);
    }

    public final boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof b0) && j.a(this.f1013b, ((b0) obj).f1013b);
        }
        return true;
    }

    public final int hashCode() {
        String str = this.f1013b;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    public final String toString() {
        return a.a(a.a("MessageRow(messageKey="), this.f1013b, ")");
    }
}
