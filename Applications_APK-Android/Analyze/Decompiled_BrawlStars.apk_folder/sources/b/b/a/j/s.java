package b.b.a.j;

import b.a.a.a.a;
import com.facebook.internal.NativeProtocol;
import com.supercell.id.IdFriend;
import java.util.List;
import kotlin.d.b.j;

public final class s {

    /* renamed from: a  reason: collision with root package name */
    public final List<IdFriend> f1162a;

    /* renamed from: b  reason: collision with root package name */
    public final boolean f1163b;

    public s(List<IdFriend> list, boolean z) {
        j.b(list, NativeProtocol.AUDIENCE_FRIENDS);
        this.f1162a = list;
        this.f1163b = z;
    }

    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof s) {
                s sVar = (s) obj;
                if (j.a(this.f1162a, sVar.f1162a)) {
                    if (this.f1163b == sVar.f1163b) {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        List<IdFriend> list = this.f1162a;
        int hashCode = (list != null ? list.hashCode() : 0) * 31;
        boolean z = this.f1163b;
        if (z) {
            z = true;
        }
        return hashCode + (z ? 1 : 0);
    }

    public final String toString() {
        StringBuilder a2 = a.a("IdFriendsData(friends=");
        a2.append(this.f1162a);
        a2.append(", initial=");
        a2.append(this.f1163b);
        a2.append(")");
        return a2.toString();
    }
}
