package b.b.a.j;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.View;
import b.b.a.b;
import b.b.a.e.a;
import com.supercell.id.SupercellId;
import java.util.List;
import kotlin.d.a.a;

public final class i1 extends ViewPager.SimpleOnPageChangeListener {

    /* renamed from: a  reason: collision with root package name */
    public int f1056a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ Context f1057b;
    public final /* synthetic */ List c;
    public final /* synthetic */ a d;

    public i1(Context context, List list, a aVar) {
        this.f1057b = context;
        this.c = list;
        this.d = aVar;
    }

    public final void onPageScrollStateChanged(int i) {
        this.f1056a = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.b.a(android.content.Context, java.util.List<? extends android.view.View>, java.util.List<b.b.a.j.c1>, int, boolean):void
     arg types: [android.content.Context, java.util.List, java.util.List, int, int]
     candidates:
      b.b.a.b.a(android.content.Context, java.util.List, int, boolean, int):void
      b.b.a.b.a(android.content.Context, java.util.List<? extends android.view.View>, kotlin.d.a.a<? extends java.util.List<b.b.a.j.c1>>, android.support.v4.view.ViewPager, kotlin.d.a.c<? super android.view.View, ? super java.lang.Integer, kotlin.m>):void
      b.b.a.b.a(android.view.View, boolean, boolean, int, int):void
      b.b.a.b.a(android.content.Context, java.util.List<? extends android.view.View>, java.util.List<b.b.a.j.c1>, int, boolean):void */
    public final void onPageSelected(int i) {
        if (this.f1056a != 2) {
            b.a(this.f1057b, (List<? extends View>) this.c, (List<c1>) ((List) this.d.invoke()), i, false);
            return;
        }
        SupercellId.INSTANCE.getSharedServices$supercellId_release().l.a(a.C0004a.TAB_SWITCH);
        b.a(this.f1057b, (List<? extends View>) this.c, (List<c1>) ((List) this.d.invoke()), i, true);
    }
}
