package b.b.a.j;

import android.support.v4.app.NotificationCompat;
import android.view.MotionEvent;
import android.view.View;
import b.b.a.b;
import b.b.a.e.a;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.view.EdgeAntialiasingImageView;
import kotlin.d.b.j;

public final class g1 implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ View f1040a;

    public g1(View view) {
        this.f1040a = view;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.MotionEvent, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.EdgeAntialiasingImageView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        j.a((Object) motionEvent, NotificationCompat.CATEGORY_EVENT);
        if (motionEvent.getAction() == 1) {
            SupercellId.INSTANCE.getSharedServices$supercellId_release().l.a(a.C0004a.TAB_SWITCH);
            EdgeAntialiasingImageView edgeAntialiasingImageView = (EdgeAntialiasingImageView) this.f1040a.findViewById(R.id.tab_icon_left);
            j.a((Object) edgeAntialiasingImageView, "tab_icon_left");
            EdgeAntialiasingImageView edgeAntialiasingImageView2 = (EdgeAntialiasingImageView) this.f1040a.findViewById(R.id.tab_icon_right);
            j.a((Object) edgeAntialiasingImageView2, "tab_icon_right");
            b.a(edgeAntialiasingImageView, edgeAntialiasingImageView2, 0, 4);
            return true;
        } else if (motionEvent.getAction() == 0) {
            return true;
        } else {
            return false;
        }
    }
}
