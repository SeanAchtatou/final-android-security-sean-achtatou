package b.b.a.j;

import kotlin.d.a.b;
import kotlin.d.b.k;
import kotlin.m;
import nl.komponents.kovenant.ap;

public final class n0 extends k implements b<E, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ ap f1103a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ b f1104b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public n0(ap apVar, b bVar) {
        super(1);
        this.f1103a = apVar;
        this.f1104b = bVar;
    }

    public final Object invoke(Object obj) {
        try {
            this.f1103a.e(this.f1104b.invoke(obj));
        } catch (Exception e) {
            this.f1103a.f(e);
        }
        return m.f5330a;
    }
}
