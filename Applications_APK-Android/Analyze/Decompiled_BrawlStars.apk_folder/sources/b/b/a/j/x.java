package b.b.a.j;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import b.b.a.c.b;
import b.b.a.d.e;
import b.b.a.i.x1.f;
import b.b.a.i.x1.i;
import com.facebook.internal.NativeProtocol;
import com.facebook.internal.ServerProtocol;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.g;
import com.google.i18n.phonenumbers.j;
import com.supercell.id.IdAccount;
import com.supercell.id.IdConfiguration;
import com.supercell.id.SupercellId;
import java.util.List;
import java.util.Locale;
import kotlin.TypeCastException;
import kotlin.d.a.c;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class x {

    /* renamed from: a  reason: collision with root package name */
    public j0 f1189a;

    /* renamed from: b  reason: collision with root package name */
    public q f1190b;
    public i c;
    public o1<List<String>> d;
    public IdConfiguration e;
    public b f;
    public final b.b.a.d.a g;
    public final e h;
    public IdAccount i;
    public final f j;
    public i k;
    public final b.b.a.e.a l;
    public final Context m;

    public final class a extends k implements c<String, kotlin.d.a.b<? super List<? extends String>, ? extends m>, m> {

        /* renamed from: a  reason: collision with root package name */
        public static final a f1191a = new a();

        public a() {
            super(2);
        }

        public final Object invoke(Object obj, Object obj2) {
            kotlin.d.a.b bVar = (kotlin.d.a.b) obj2;
            j.b((String) obj, "<anonymous parameter 0>");
            j.b(bVar, "callback");
            SupercellId.INSTANCE.getSharedServices$supercellId_release().h.b().a(new w(bVar));
            return m.f5330a;
        }
    }

    public x(Context context, IdConfiguration idConfiguration, IdAccount idAccount) {
        String gameAccountToken;
        j.b(context, "context");
        j.b(idConfiguration, "config");
        this.m = context;
        this.e = idConfiguration;
        this.f = new b(this.m, idConfiguration.getGaTrackingId(), idConfiguration.isProdEnvironment());
        this.g = new b.b.a.d.a(idConfiguration.getApiUrl(), (idAccount == null || (gameAccountToken = idAccount.getScidToken()) == null) ? idConfiguration.getGameAccountToken() : gameAccountToken);
        this.h = new e(idConfiguration.getSocialApiUrl(), idAccount != null ? idAccount.getScidToken() : null);
        this.j = new f(this.m);
        this.k = new i(idConfiguration.getAssetsUrl());
        this.l = new b.b.a.e.a();
        a();
        a(idConfiguration, idAccount);
        this.f.a(2, SupercellId.INSTANCE.getVersionString());
    }

    public final void a() {
        this.f1189a = new j0();
        this.f1190b = new q();
        q qVar = this.f1190b;
        if (qVar == null) {
            j.a(NativeProtocol.AUDIENCE_FRIENDS);
        }
        qVar.e = SupercellId.INSTANCE.getRemoteConfiguration$supercellId_release().e(q0.FRIENDS_CACHE_LIFETIME);
        this.c = new i();
        this.d = new o1<>(a.f1191a);
    }

    public final i b() {
        return this.k;
    }

    public final i c() {
        i iVar = this.c;
        if (iVar == null) {
            j.a("clientState");
        }
        return iVar;
    }

    public final String d() {
        String phone;
        String email;
        IdAccount idAccount = this.i;
        if (idAccount != null && (email = idAccount.getEmail()) != null) {
            return email;
        }
        IdAccount idAccount2 = this.i;
        if (idAccount2 == null || (phone = idAccount2.getPhone()) == null) {
            return null;
        }
        j.b(phone, "number");
        try {
            j.a a2 = g.a().a(phone, "ZZ");
            return 8234 + g.a().a(a2, g.a.INTERNATIONAL) + 8236;
        } catch (NumberParseException unused) {
            return phone;
        }
    }

    public final q e() {
        q qVar = this.f1190b;
        if (qVar == null) {
            kotlin.d.b.j.a(NativeProtocol.AUDIENCE_FRIENDS);
        }
        return qVar;
    }

    public final j0 f() {
        j0 j0Var = this.f1189a;
        if (j0Var == null) {
            kotlin.d.b.j.a("profile");
        }
        return j0Var;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Locale, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void a(IdConfiguration idConfiguration, IdAccount idAccount) {
        String str;
        String supercellId;
        SharedPreferences.Editor edit;
        kotlin.d.b.j.b(idConfiguration, "config");
        IdAccount idAccount2 = this.i;
        String str2 = null;
        if (!kotlin.d.b.j.a((Object) (idAccount2 != null ? idAccount2.getSupercellId() : null), (Object) (idAccount != null ? idAccount.getSupercellId() : null))) {
            a();
            if (Build.VERSION.SDK_INT >= 24) {
                this.m.deleteSharedPreferences("SupercellIdSystems");
            } else {
                SharedPreferences sharedPreferences = this.m.getSharedPreferences("SupercellIdSystems", 0);
                if (!(sharedPreferences == null || (edit = sharedPreferences.edit()) == null)) {
                    edit.clear();
                    edit.apply();
                }
            }
            if (!(idAccount == null || (supercellId = idAccount.getSupercellId()) == null)) {
                "load from persistent storage " + supercellId;
                j0 j0Var = this.f1189a;
                if (j0Var == null) {
                    kotlin.d.b.j.a("profile");
                }
                j0Var.a(this.m, supercellId);
                i iVar = this.c;
                if (iVar == null) {
                    kotlin.d.b.j.a("clientState");
                }
                iVar.a(this.m, supercellId);
            }
        }
        this.e = idConfiguration;
        this.i = idAccount;
        this.f = new b(this.m, idConfiguration.getGaTrackingId(), idConfiguration.isProdEnvironment());
        this.f.a(1, idConfiguration.getSocialFeatureEnabled() ? ServerProtocol.DIALOG_RETURN_SCOPES_TRUE : "false");
        b.b.a.d.a aVar = this.g;
        String apiUrl = idConfiguration.getApiUrl();
        if (idAccount == null || (str = idAccount.getScidToken()) == null) {
            str = idConfiguration.getGameAccountToken();
        }
        aVar.a(apiUrl, str);
        e eVar = this.h;
        String socialApiUrl = idConfiguration.getSocialApiUrl();
        if (idAccount != null) {
            str2 = idAccount.getScidToken();
        }
        eVar.a(socialApiUrl, str2);
        this.k = new i(idConfiguration.getAssetsUrl());
        f fVar = this.j;
        String language = idConfiguration.getLanguage();
        Locale locale = Locale.ENGLISH;
        kotlin.d.b.j.a((Object) locale, "Locale.ENGLISH");
        if (language != null) {
            String lowerCase = language.toLowerCase(locale);
            kotlin.d.b.j.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            fVar.c(lowerCase);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
}
