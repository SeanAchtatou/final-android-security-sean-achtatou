package b.b.a.k;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;

public final class c extends InsetDrawable {

    /* renamed from: a  reason: collision with root package name */
    public Drawable f1207a;

    public c(Drawable drawable, int i, int i2, int i3, int i4) {
        super(drawable, i, i2, i3, i4);
        this.f1207a = drawable;
    }

    public final Drawable getDrawable() {
        return this.f1207a;
    }
}
