package b.b.a.k;

import android.animation.ValueAnimator;
import com.supercell.id.view.ViewAnimator;

public final class g implements ValueAnimator.AnimatorUpdateListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ ViewAnimator f1220a;

    public g(ViewAnimator viewAnimator) {
        this.f1220a = viewAnimator;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f1220a.requestLayout();
    }
}
