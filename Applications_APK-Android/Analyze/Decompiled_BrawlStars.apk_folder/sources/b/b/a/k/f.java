package b.b.a.k;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import com.supercell.id.R;
import kotlin.d.b.j;

public final class f extends View {

    /* renamed from: a  reason: collision with root package name */
    public final Paint f1218a;

    /* renamed from: b  reason: collision with root package name */
    public final RectF f1219b = new RectF();

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public f(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        j.b(context, "context");
        Paint paint = new Paint();
        paint.setColor(ContextCompat.getColor(context, R.color.blackTranslucent88));
        this.f1218a = paint;
    }

    public final void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (canvas != null) {
            float width = (((float) getWidth()) * 8.0f) / 34.0f;
            RectF rectF = this.f1219b;
            rectF.left = 0.0f;
            rectF.bottom = (((((float) getHeight()) * 30.0f) / 110.0f) / 2.0f) + ((float) (getHeight() / 2));
            rectF.right = rectF.left + ((float) getWidth());
            rectF.top = rectF.bottom - ((((float) getHeight()) * 20.0f) / 110.0f);
            canvas.drawRoundRect(rectF, width, width, this.f1218a);
        }
    }
}
