package b.b.a.k;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;
import kotlin.d.b.j;

public final class i extends AnimatorListenerAdapter {

    /* renamed from: a  reason: collision with root package name */
    public boolean f1223a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ View f1224b;

    public i(View view) {
        this.f1224b = view;
    }

    public final void onAnimationCancel(Animator animator) {
        this.f1223a = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onAnimationEnd(Animator animator) {
        j.b(animator, "animation");
        if (!this.f1223a) {
            View view = this.f1224b;
            j.a((Object) view, "child");
            view.setVisibility(8);
        }
    }
}
