package b.b.a.k;

import android.graphics.drawable.Drawable;
import com.supercell.id.view.ProgressBar;
import java.lang.ref.WeakReference;
import kotlin.d.a.c;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class d extends k implements c<Drawable, b.b.a.i.x1.c, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ String f1208a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ WeakReference f1209b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d(String str, WeakReference weakReference) {
        super(2);
        this.f1208a = str;
        this.f1209b = weakReference;
    }

    public final Object invoke(Object obj, Object obj2) {
        Drawable drawable = (Drawable) obj;
        j.b(drawable, "drawable");
        j.b((b.b.a.i.x1.c) obj2, "<anonymous parameter 1>");
        ProgressBar progressBar = (ProgressBar) this.f1209b.get();
        if (progressBar != null) {
            ProgressBar.a(progressBar, drawable, this.f1208a);
        }
        return m.f5330a;
    }
}
