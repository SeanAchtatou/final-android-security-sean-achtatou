package b.b.a.k;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import com.supercell.id.view.ViewAnimator;
import kotlin.d.b.j;

public final class h extends AnimatorListenerAdapter {

    /* renamed from: a  reason: collision with root package name */
    public boolean f1221a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ ViewAnimator f1222b;

    public h(ViewAnimator viewAnimator) {
        this.f1222b = viewAnimator;
    }

    public final void onAnimationCancel(Animator animator) {
        this.f1221a = true;
    }

    public final void onAnimationEnd(Animator animator) {
        j.b(animator, "animation");
        this.f1222b.requestLayout();
    }
}
