package b.b.a.k;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.ImageView;
import b.b.a.b;
import com.supercell.id.R;
import com.supercell.id.view.FastScroll;
import kotlin.d.b.j;

public final class a implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ RecyclerView f1205a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ FastScroll f1206b;

    public a(RecyclerView recyclerView, FastScroll fastScroll) {
        this.f1205a = recyclerView;
        this.f1206b = fastScroll;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.ImageView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void run() {
        int computeVerticalScrollRange = this.f1205a.computeVerticalScrollRange();
        int height = this.f1205a.getHeight();
        int max = Math.max((this.f1206b.getHeight() * (computeVerticalScrollRange == 0 ? height : Math.min(height, (height * height) / computeVerticalScrollRange))) / height, kotlin.e.a.a(b.a(30)));
        ImageView imageView = (ImageView) this.f1206b.a(R.id.fastscroll_thumb);
        j.a((Object) imageView, "fastscroll_thumb");
        ImageView imageView2 = (ImageView) this.f1206b.a(R.id.fastscroll_thumb);
        j.a((Object) imageView2, "fastscroll_thumb");
        ViewGroup.LayoutParams layoutParams = imageView2.getLayoutParams();
        layoutParams.height = max;
        imageView.setLayoutParams(layoutParams);
    }
}
