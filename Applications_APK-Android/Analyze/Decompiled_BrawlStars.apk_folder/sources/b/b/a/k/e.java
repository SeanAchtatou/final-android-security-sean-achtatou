package b.b.a.k;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.view.animation.PathInterpolatorCompat;
import android.util.AttributeSet;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import kotlin.a.m;
import kotlin.d.b.j;

public final class e extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    public final ImageView f1210a;

    /* renamed from: b  reason: collision with root package name */
    public final f f1211b;
    public final ObjectAnimator c;
    public final ObjectAnimator d;
    public final ObjectAnimator e;
    public final ObjectAnimator f;
    public final ObjectAnimator g;
    public final ObjectAnimator h;
    public final ObjectAnimator i;
    public final AnimatorSet j;
    public final AnimatorSet k;
    public final AnimatorSet l;
    public Drawable m;
    public float n;
    public float o = 15.0f;

    public final class a implements ValueAnimator.AnimatorUpdateListener {
        public a() {
        }

        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            e.a(e.this);
        }
    }

    public final class b implements ValueAnimator.AnimatorUpdateListener {
        public b() {
        }

        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            e.a(e.this);
        }
    }

    public final class c implements ValueAnimator.AnimatorUpdateListener {
        public c() {
        }

        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            e.a(e.this);
        }
    }

    public final class d implements ValueAnimator.AnimatorUpdateListener {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ Drawable f1215a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ e f1216b;

        public d(Drawable drawable, e eVar) {
            this.f1215a = drawable;
            this.f1216b = eVar;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.animation.ValueAnimator, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            ImageView imageView = this.f1216b.f1210a;
            j.a((Object) valueAnimator, "it");
            Object animatedValue = valueAnimator.getAnimatedValue();
            if (!(animatedValue instanceof Drawable)) {
                animatedValue = null;
            }
            Drawable drawable = (Drawable) animatedValue;
            if (drawable == null) {
                drawable = this.f1215a;
            }
            imageView.setImageDrawable(drawable);
        }
    }

    /* renamed from: b.b.a.k.e$e  reason: collision with other inner class name */
    public final class C0107e<T> implements TypeEvaluator<Object> {

        /* renamed from: a  reason: collision with root package name */
        public static final C0107e f1217a = new C0107e();

        public final Object evaluate(float f, Object obj, Object obj2) {
            return (f < 0.0625f || f > 0.6875f) ? obj : obj2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.animation.ObjectAnimator, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public e(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        j.b(context, "context");
        this.f1210a = new ImageView(context, attributeSet, i2);
        this.f1211b = new f(context, attributeSet, i2);
        this.f1211b.setAlpha(0.0f);
        addView(this.f1211b);
        addView(this.f1210a);
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.f1210a, "translationY", getTranslationY());
        ofFloat.setInterpolator(PathInterpolatorCompat.create(0.5f, 0.0f, 0.65f, 1.0f));
        ofFloat.setDuration(120L);
        ofFloat.addUpdateListener(new a());
        j.a((Object) ofFloat, "ObjectAnimator.ofFloat(i…ShadowAlpha() }\n        }");
        this.c = ofFloat;
        ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(this.f1210a, "translationY", getTranslationY());
        ofFloat2.setInterpolator(PathInterpolatorCompat.create(0.35f, 0.1f, 0.35f, 1.0f));
        ofFloat2.setDuration(480L);
        ofFloat2.addUpdateListener(new b());
        j.a((Object) ofFloat2, "ObjectAnimator.ofFloat(i…ShadowAlpha() }\n        }");
        this.d = ofFloat2;
        ObjectAnimator ofFloat3 = ObjectAnimator.ofFloat(this.f1210a, "translationY", getTranslationY());
        ofFloat3.setInterpolator(PathInterpolatorCompat.create(0.85f, 0.0f, 0.5f, 1.0f));
        ofFloat3.setDuration(360L);
        ofFloat3.addUpdateListener(new c());
        j.a((Object) ofFloat3, "ObjectAnimator.ofFloat(i…ShadowAlpha() }\n        }");
        this.e = ofFloat3;
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playSequentially(this.c, this.d, this.e);
        this.j = animatorSet;
        ObjectAnimator ofFloat4 = ObjectAnimator.ofFloat(this.f1210a, "scaleX", getScaleX());
        ofFloat4.setInterpolator(b.b.a.f.a.f62a);
        ofFloat4.setDuration(120L);
        j.a((Object) ofFloat4, "ObjectAnimator.ofFloat(i…* 0.1).toLong()\n        }");
        this.f = ofFloat4;
        ObjectAnimator ofFloat5 = ObjectAnimator.ofFloat(this.f1210a, "scaleX", 1.0f);
        ofFloat5.setInterpolator(b.b.a.f.a.f62a);
        ofFloat5.setDuration(120L);
        j.a((Object) ofFloat5, "ObjectAnimator.ofFloat(i…* 0.1).toLong()\n        }");
        this.g = ofFloat5;
        ObjectAnimator ofFloat6 = ObjectAnimator.ofFloat(this.f1210a, "scaleY", getScaleY());
        ofFloat6.setInterpolator(b.b.a.f.a.f62a);
        ofFloat6.setDuration(120L);
        j.a((Object) ofFloat6, "ObjectAnimator.ofFloat(i…* 0.1).toLong()\n        }");
        this.h = ofFloat6;
        ObjectAnimator ofFloat7 = ObjectAnimator.ofFloat(this.f1210a, "scaleY", 1.0f);
        ofFloat7.setInterpolator(b.b.a.f.a.f62a);
        ofFloat7.setDuration(120L);
        j.a((Object) ofFloat7, "ObjectAnimator.ofFloat(i…* 0.1).toLong()\n        }");
        this.i = ofFloat7;
        AnimatorSet animatorSet2 = new AnimatorSet();
        animatorSet2.playSequentially(this.f, this.g);
        this.k = animatorSet2;
        AnimatorSet animatorSet3 = new AnimatorSet();
        animatorSet3.playSequentially(this.h, this.i);
        this.l = animatorSet3;
    }

    public static final /* synthetic */ void a(e eVar) {
        f fVar = eVar.f1211b;
        float f2 = (-(eVar.f1210a.getTranslationY() - eVar.n)) / eVar.o;
        float f3 = 0.0f;
        if (Float.compare(f2, 0.0f) >= 0) {
            f3 = Float.compare(f2, 1.0f) > 0 ? 1.0f : f2;
        }
        fVar.setAlpha(1.0f - f3);
    }

    public final AnimatorSet a() {
        ValueAnimator valueAnimator;
        Drawable drawable = this.m;
        if (drawable != null) {
            Drawable drawable2 = this.f1210a.getDrawable();
            valueAnimator = ValueAnimator.ofObject(C0107e.f1217a, drawable2, drawable);
            valueAnimator.setDuration(960L);
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new d(drawable2, this));
        } else {
            valueAnimator = null;
        }
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(m.d(this.j, this.k, this.l, valueAnimator));
        return animatorSet;
    }

    public final void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        float measuredWidth = (((float) getMeasuredWidth()) * 38.0f) / 34.0f;
        float measuredHeight = (((float) getMeasuredHeight()) * 20.0f) / 110.0f;
        float measuredHeight2 = (((float) getMeasuredHeight()) * 30.0f) / 110.0f;
        float measuredHeight3 = ((float) getMeasuredHeight()) - ((((float) getMeasuredHeight()) * 41.0f) / 110.0f);
        float f2 = (0.95f * measuredHeight3) - measuredHeight3;
        this.n = f2;
        this.o = 0.5f * measuredHeight2;
        this.f1210a.setTranslationY(this.n);
        float f3 = (measuredHeight2 - measuredHeight) / 2.0f;
        this.c.setFloatValues(f2, f3);
        float f4 = (-measuredHeight3) + measuredHeight2;
        this.d.setFloatValues(f3, f4);
        this.e.setFloatValues(f4, f2);
        this.f1210a.setScaleX(1.0f);
        this.f.setFloatValues(1.0f, measuredWidth / ((float) getMeasuredWidth()));
        this.g.setFloatValues(measuredWidth / ((float) getMeasuredWidth()), 1.0f);
        this.f1210a.setScaleY(1.0f);
        float f5 = measuredHeight / measuredHeight2;
        this.h.setFloatValues(1.0f, f5);
        this.i.setFloatValues(f5, 1.0f);
    }
}
