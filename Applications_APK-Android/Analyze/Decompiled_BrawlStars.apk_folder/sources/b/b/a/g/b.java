package b.b.a.g;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import kotlin.d.b.g;
import kotlin.d.b.j;

public final class b extends Drawable implements Drawable.Callback {
    public static final a f = new a(null);

    /* renamed from: a  reason: collision with root package name */
    public final C0006b f66a;

    /* renamed from: b  reason: collision with root package name */
    public int f67b;
    public int c;
    public int d;
    public int e;

    public static final class a {
        public /* synthetic */ a(g gVar) {
        }

        public final float a() {
            return b.b.a.b.a(8);
        }

        public final float b() {
            return b.b.a.b.a(12);
        }

        public final float c() {
            return b.b.a.b.a(3);
        }
    }

    /* renamed from: b.b.a.g.b$b  reason: collision with other inner class name */
    public final class C0006b extends Drawable.ConstantState {

        /* renamed from: a  reason: collision with root package name */
        public Drawable f68a;

        /* renamed from: b  reason: collision with root package name */
        public Drawable f69b;
        public int c;
        public boolean d;
        public boolean e;

        public C0006b(b bVar, C0006b bVar2, b bVar3, Resources resources) {
            Drawable.ConstantState constantState;
            Drawable.ConstantState constantState2;
            if (bVar2 != null) {
                Drawable drawable = bVar2.f68a;
                Drawable drawable2 = null;
                this.f68a = (drawable == null || (constantState2 = drawable.getConstantState()) == null) ? null : constantState2.newDrawable(resources);
                Drawable drawable3 = bVar2.f69b;
                if (!(drawable3 == null || (constantState = drawable3.getConstantState()) == null)) {
                    drawable2 = constantState.newDrawable();
                }
                this.f69b = drawable2;
                Drawable drawable4 = this.f68a;
                if (drawable4 != null) {
                    drawable4.setCallback(bVar3);
                }
            }
        }

        public final int getChangingConfigurations() {
            return this.c;
        }

        public final Drawable newDrawable() {
            return new b(this, null);
        }
    }

    public b(Drawable drawable, Drawable drawable2, int i, int i2, int i3, int i4) {
        j.b(drawable, "drawable");
        j.b(drawable2, "shadowDrawable");
        this.f67b = i;
        this.c = i2;
        this.d = i3;
        this.e = i4;
        this.f66a = new C0006b(this, null, this, null);
        C0006b bVar = this.f66a;
        bVar.f68a = drawable;
        bVar.f69b = drawable2;
        onBoundsChange(null);
        drawable.setCallback(this);
    }

    public b(C0006b bVar, Resources resources) {
        this.f66a = new C0006b(this, bVar, this, resources);
    }

    public final void draw(Canvas canvas) {
        Rect bounds;
        Drawable drawable;
        j.b(canvas, "canvas");
        Drawable drawable2 = this.f66a.f68a;
        if (!(drawable2 == null || (bounds = drawable2.getBounds()) == null || (drawable = this.f66a.f69b) == null)) {
            drawable.setBounds(new Rect(bounds.left - this.f67b, bounds.top - this.d, bounds.right + this.c, bounds.bottom + this.e));
        }
        Drawable drawable3 = this.f66a.f69b;
        if (drawable3 != null) {
            drawable3.draw(canvas);
        }
        Drawable drawable4 = this.f66a.f68a;
        if (drawable4 != null) {
            drawable4.draw(canvas);
        }
    }

    public final int getChangingConfigurations() {
        int changingConfigurations = super.getChangingConfigurations();
        C0006b bVar = this.f66a;
        int i = changingConfigurations | bVar.c;
        Drawable drawable = bVar.f68a;
        int i2 = 0;
        int changingConfigurations2 = i | (drawable != null ? drawable.getChangingConfigurations() : 0);
        Drawable drawable2 = this.f66a.f69b;
        if (drawable2 != null) {
            i2 = drawable2.getChangingConfigurations();
        }
        return changingConfigurations2 | i2;
    }

    public final Drawable.ConstantState getConstantState() {
        C0006b bVar = this.f66a;
        if (!bVar.d) {
            Drawable drawable = bVar.f68a;
            bVar.e = (drawable != null ? drawable.getConstantState() : null) != null;
            bVar.d = true;
        }
        if (!bVar.e) {
            return null;
        }
        this.f66a.c = getChangingConfigurations();
        return this.f66a;
    }

    public final int getOpacity() {
        return -3;
    }

    public final void invalidateDrawable(Drawable drawable) {
        j.b(drawable, "who");
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.invalidateDrawable(this);
        }
    }

    public final boolean isStateful() {
        Drawable drawable = this.f66a.f68a;
        if (drawable != null) {
            return drawable.isStateful();
        }
        return false;
    }

    public final void onBoundsChange(Rect rect) {
        Rect bounds;
        Drawable drawable;
        Drawable drawable2;
        if (!(rect == null || (drawable2 = this.f66a.f68a) == null)) {
            drawable2.setBounds(rect);
        }
        Drawable drawable3 = this.f66a.f68a;
        if (drawable3 != null && (bounds = drawable3.getBounds()) != null && (drawable = this.f66a.f69b) != null) {
            drawable.setBounds(new Rect(bounds.left - this.f67b, bounds.top - this.d, bounds.right + this.c, bounds.bottom + this.e));
        }
    }

    public final boolean onStateChange(int[] iArr) {
        if (iArr != null) {
            Drawable drawable = this.f66a.f68a;
            Boolean valueOf = drawable != null ? Boolean.valueOf(drawable.setState(iArr)) : null;
            if (valueOf != null) {
                return valueOf.booleanValue();
            }
        }
        return false;
    }

    public final void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        j.b(drawable, "who");
        j.b(runnable, "what");
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.scheduleDrawable(this, runnable, j);
        }
    }

    public final void setAlpha(int i) {
    }

    public final void setColorFilter(ColorFilter colorFilter) {
    }

    public final void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        j.b(drawable, "who");
        j.b(runnable, "what");
        Drawable.Callback callback = getCallback();
        if (callback != null) {
            callback.unscheduleDrawable(this, runnable);
        }
    }
}
