package b.b.a.g;

import android.graphics.Bitmap;
import kotlin.d.a.b;
import kotlin.d.b.k;
import kotlin.m;

public final class j extends k implements b<Bitmap, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ i f99a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ String f100b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public j(i iVar, String str) {
        super(1);
        this.f99a = iVar;
        this.f100b = str;
    }

    public final Object invoke(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        kotlin.d.b.j.b(bitmap, "it");
        this.f99a.n.put(this.f100b, bitmap);
        this.f99a.b();
        return m.f5330a;
    }
}
