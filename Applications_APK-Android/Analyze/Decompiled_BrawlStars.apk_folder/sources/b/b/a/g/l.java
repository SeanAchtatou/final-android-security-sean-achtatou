package b.b.a.g;

import android.graphics.Bitmap;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class l extends k implements b<Bitmap, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ i f103a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l(i iVar) {
        super(1);
        this.f103a = iVar;
    }

    public final Object invoke(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        j.b(bitmap, "it");
        i iVar = this.f103a;
        iVar.p = bitmap;
        iVar.b();
        return m.f5330a;
    }
}
