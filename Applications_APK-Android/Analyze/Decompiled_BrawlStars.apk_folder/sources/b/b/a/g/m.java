package b.b.a.g;

import android.graphics.Bitmap;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.d.b.k;

public final class m extends k implements b<Bitmap, kotlin.m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ i f104a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m(i iVar) {
        super(1);
        this.f104a = iVar;
    }

    public final Object invoke(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        j.b(bitmap, "it");
        i iVar = this.f104a;
        iVar.q = bitmap;
        iVar.b();
        return kotlin.m.f5330a;
    }
}
