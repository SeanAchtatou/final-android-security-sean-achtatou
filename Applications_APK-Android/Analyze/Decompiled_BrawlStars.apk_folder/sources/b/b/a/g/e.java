package b.b.a.g;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewCompat;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.d.b.j;
import kotlin.m;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    public static final Map<a, Bitmap> f74a = new LinkedHashMap();

    /* renamed from: b  reason: collision with root package name */
    public static final e f75b = new e();

    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        public final int f76a;

        /* renamed from: b  reason: collision with root package name */
        public final float f77b;
        public final float c;
        public final float d;
        public final float e;
        public final float f;

        public a(int i, float f2, float f3, float f4, float f5, float f6) {
            this.f76a = i;
            this.f77b = f2;
            this.c = f3;
            this.d = f4;
            this.e = f5;
            this.f = f6;
        }

        public final boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (!((this.f76a == aVar.f76a) && Float.compare(this.f77b, aVar.f77b) == 0 && Float.compare(this.c, aVar.c) == 0 && Float.compare(this.d, aVar.d) == 0 && Float.compare(this.e, aVar.e) == 0 && Float.compare(this.f, aVar.f) == 0)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        public final int hashCode() {
            int floatToIntBits = Float.floatToIntBits(this.f77b);
            int floatToIntBits2 = Float.floatToIntBits(this.c);
            int floatToIntBits3 = Float.floatToIntBits(this.d);
            int floatToIntBits4 = Float.floatToIntBits(this.e);
            return Float.floatToIntBits(this.f) + ((floatToIntBits4 + ((floatToIntBits3 + ((floatToIntBits2 + ((floatToIntBits + (this.f76a * 31)) * 31)) * 31)) * 31)) * 31);
        }

        public final String toString() {
            StringBuilder a2 = b.a.a.a.a.a("ShadowDetails(color=");
            a2.append(this.f76a);
            a2.append(", dx=");
            a2.append(this.f77b);
            a2.append(", dy=");
            a2.append(this.c);
            a2.append(", radius=");
            a2.append(this.d);
            a2.append(", alpha=");
            a2.append(this.e);
            a2.append(", cornerRadius=");
            a2.append(this.f);
            a2.append(")");
            return a2.toString();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.graphics.Bitmap, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final Drawable a(Resources resources, int i, float f, float f2, float f3, float f4, float f5) {
        float f6 = f;
        float f7 = f2;
        float f8 = f5;
        j.b(resources, "resources");
        float max = Math.max(f8, f6);
        float max2 = Math.max(f8, f7);
        float f9 = (max * 2.0f) + 2.0f;
        float f10 = (max2 * 2.0f) + 2.0f;
        a aVar = new a(i, f, f2, f3, f4, f5);
        if (!f74a.containsKey(aVar)) {
            synchronized (this) {
                if (!f74a.containsKey(aVar)) {
                    Map<a, Bitmap> map = f74a;
                    Bitmap createBitmap = Bitmap.createBitmap(kotlin.e.a.a(f9), kotlin.e.a.a(f10), Bitmap.Config.ARGB_8888);
                    Paint paint = new Paint(1);
                    paint.setColor(i);
                    Paint paint2 = new Paint(1);
                    paint2.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                    Paint paint3 = new Paint(1);
                    paint3.setColor((int) ViewCompat.MEASURED_STATE_MASK);
                    paint3.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_ATOP));
                    paint3.setAlpha(kotlin.e.a.a(255.0f * f4));
                    paint3.setShadowLayer(f3, f6, f7, (int) ViewCompat.MEASURED_STATE_MASK);
                    Path path = new Path();
                    path.setFillType(Path.FillType.EVEN_ODD);
                    j.a((Object) createBitmap, "this");
                    RectF rectF = new RectF(0.0f, 0.0f, (float) createBitmap.getWidth(), (float) createBitmap.getHeight());
                    RectF rectF2 = new RectF(rectF);
                    rectF2.inset(-10.0f, -10.0f);
                    path.addRect(rectF2, Path.Direction.CW);
                    path.addRoundRect(rectF, f8, f8, Path.Direction.CW);
                    Canvas canvas = new Canvas(createBitmap);
                    canvas.drawPaint(paint);
                    canvas.drawPath(path, paint2);
                    canvas.drawPath(path, paint3);
                    j.a((Object) createBitmap, "Bitmap.createBitmap(widt…nt)\n                    }");
                    map.put(aVar, createBitmap);
                }
                m mVar = m.f5330a;
            }
        }
        Bitmap bitmap = f74a.get(aVar);
        if (bitmap == null) {
            return null;
        }
        return f.f78a.a(resources, bitmap, kotlin.e.a.a(max), kotlin.e.a.a(max2), kotlin.e.a.a(f9) - kotlin.e.a.a(max), kotlin.e.a.a(f10) - kotlin.e.a.a(max2), null);
    }
}
