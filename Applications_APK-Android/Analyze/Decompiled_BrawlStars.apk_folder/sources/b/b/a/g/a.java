package b.b.a.g;

import android.graphics.Bitmap;
import com.facebook.share.internal.MessengerShareContentUtility;
import kotlin.d.b.j;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    public final Bitmap f64a;

    /* renamed from: b  reason: collision with root package name */
    public final boolean f65b;
    public final Bitmap c;
    public final float d;
    public final float e;
    public final float f;
    public final float g;
    public final float h;
    public final long i;
    public float j;

    public a(Bitmap bitmap, boolean z, Bitmap bitmap2, float f2, float f3, float f4, float f5, float f6, long j2, float f7) {
        j.b(bitmap, MessengerShareContentUtility.MEDIA_IMAGE);
        j.b(bitmap2, "shadowImage");
        this.f64a = bitmap;
        this.f65b = z;
        this.c = bitmap2;
        this.d = f2;
        this.e = f3;
        this.f = f4;
        this.g = f5;
        this.h = f6;
        this.i = j2;
        this.j = f7;
    }

    public final float a() {
        float f2 = this.f;
        return ((this.g - f2) * this.j) + f2;
    }

    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof a) {
                a aVar = (a) obj;
                if (j.a(this.f64a, aVar.f64a)) {
                    if ((this.f65b == aVar.f65b) && j.a(this.c, aVar.c) && Float.compare(this.d, aVar.d) == 0 && Float.compare(this.e, aVar.e) == 0 && Float.compare(this.f, aVar.f) == 0 && Float.compare(this.g, aVar.g) == 0 && Float.compare(this.h, aVar.h) == 0) {
                        if (!(this.i == aVar.i) || Float.compare(this.j, aVar.j) != 0) {
                            return false;
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        Bitmap bitmap = this.f64a;
        int i2 = 0;
        int hashCode = (bitmap != null ? bitmap.hashCode() : 0) * 31;
        boolean z = this.f65b;
        if (z) {
            z = true;
        }
        int i3 = (hashCode + (z ? 1 : 0)) * 31;
        Bitmap bitmap2 = this.c;
        if (bitmap2 != null) {
            i2 = bitmap2.hashCode();
        }
        int floatToIntBits = Float.floatToIntBits(this.d);
        int floatToIntBits2 = Float.floatToIntBits(this.e);
        int floatToIntBits3 = Float.floatToIntBits(this.f);
        int floatToIntBits4 = Float.floatToIntBits(this.g);
        int floatToIntBits5 = Float.floatToIntBits(this.h);
        long j2 = this.i;
        return Float.floatToIntBits(this.j) + ((((floatToIntBits5 + ((floatToIntBits4 + ((floatToIntBits3 + ((floatToIntBits2 + ((floatToIntBits + ((i3 + i2) * 31)) * 31)) * 31)) * 31)) * 31)) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31);
    }

    public final String toString() {
        StringBuilder a2 = b.a.a.a.a.a("AnimatedIcon(image=");
        a2.append(this.f64a);
        a2.append(", large=");
        a2.append(this.f65b);
        a2.append(", shadowImage=");
        a2.append(this.c);
        a2.append(", shadowOffset=");
        a2.append(this.d);
        a2.append(", scale=");
        a2.append(this.e);
        a2.append(", startX=");
        a2.append(this.f);
        a2.append(", endX=");
        a2.append(this.g);
        a2.append(", y=");
        a2.append(this.h);
        a2.append(", duration=");
        a2.append(this.i);
        a2.append(", animationPosition=");
        a2.append(this.j);
        a2.append(")");
        return a2.toString();
    }
}
