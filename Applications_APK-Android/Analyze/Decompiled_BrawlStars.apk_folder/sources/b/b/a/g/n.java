package b.b.a.g;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import kotlin.d.a.c;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;
import nl.komponents.kovenant.ap;

public final class n extends k implements c<Drawable, b.b.a.i.x1.c, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ ap f105a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ String f106b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public n(ap apVar, String str) {
        super(2);
        this.f105a = apVar;
        this.f106b = str;
    }

    public final Object invoke(Object obj, Object obj2) {
        Drawable drawable = (Drawable) obj;
        j.b(drawable, "drawable");
        j.b((b.b.a.i.x1.c) obj2, "<anonymous parameter 1>");
        Bitmap bitmap = null;
        if (!(drawable instanceof BitmapDrawable)) {
            drawable = null;
        }
        try {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable != null) {
                bitmap = bitmapDrawable.getBitmap();
            }
            if (bitmap instanceof Bitmap) {
                this.f105a.e(bitmap);
            } else {
                this.f105a.f(new Resources.NotFoundException(this.f106b + " not found"));
            }
        } catch (Exception unused) {
        }
        return m.f5330a;
    }
}
