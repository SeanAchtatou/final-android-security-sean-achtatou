package b.b.a.g;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.ColorUtils;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.NoWhenBranchMatchedException;
import kotlin.d.b.j;
import kotlin.m;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    public static final Map<a, Bitmap> f79a = new LinkedHashMap();

    /* renamed from: b  reason: collision with root package name */
    public static final g f80b = new g();

    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        public final int f81a;

        /* renamed from: b  reason: collision with root package name */
        public final float f82b;
        public final float c;
        public final b d;

        public a(int i, float f, float f2, b bVar) {
            j.b(bVar, "shape");
            this.f81a = i;
            this.f82b = f;
            this.c = f2;
            this.d = bVar;
        }

        public final boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (!(this.f81a == aVar.f81a) || Float.compare(this.f82b, aVar.f82b) != 0 || Float.compare(this.c, aVar.c) != 0 || !j.a(this.d, aVar.d)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        public final int hashCode() {
            int floatToIntBits = (Float.floatToIntBits(this.c) + ((Float.floatToIntBits(this.f82b) + (this.f81a * 31)) * 31)) * 31;
            b bVar = this.d;
            return floatToIntBits + (bVar != null ? bVar.hashCode() : 0);
        }

        public final String toString() {
            StringBuilder a2 = b.a.a.a.a.a("ShadowDetails(color=");
            a2.append(this.f81a);
            a2.append(", radius=");
            a2.append(this.f82b);
            a2.append(", cornerRadius=");
            a2.append(this.c);
            a2.append(", shape=");
            a2.append(this.d);
            a2.append(")");
            return a2.toString();
        }
    }

    public enum b {
        TOP,
        MIDDLE,
        BOTTOM,
        FULL
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.graphics.Bitmap, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final Drawable a(Resources resources, int i, float f, float f2, b bVar) {
        int i2;
        int i3;
        int i4;
        float f3;
        int i5 = i;
        float f4 = f;
        float f5 = f2;
        b bVar2 = bVar;
        j.b(resources, "resources");
        j.b(bVar2, "shape");
        a aVar = new a(i5, f4, f5, bVar2);
        int i6 = 2;
        if (!f79a.containsKey(aVar)) {
            synchronized (this) {
                if (!f79a.containsKey(aVar)) {
                    float max = Math.max(0.0f, f5 - (f4 / 2.0f));
                    float f6 = f4 + max;
                    int a2 = kotlin.e.a.a(f6) * 2;
                    int i7 = h.f85a[bVar.ordinal()];
                    if (i7 == 1 || i7 == 2) {
                        i4 = (a2 / 2) + 1;
                    } else if (i7 == 3) {
                        i4 = 2;
                    } else if (i7 == 4) {
                        i4 = a2;
                    } else {
                        throw new NoWhenBranchMatchedException();
                    }
                    Map<a, Bitmap> map = f79a;
                    Bitmap createBitmap = Bitmap.createBitmap(a2, i4, Bitmap.Config.ARGB_8888);
                    float f7 = ((float) a2) / 2.0f;
                    int i8 = h.f86b[bVar.ordinal()];
                    if (i8 == 1) {
                        f3 = ((float) i4) - 1.0f;
                    } else if (i8 == 2 || i8 == 3) {
                        f3 = ((float) i4) / 2.0f;
                    } else if (i8 == 4) {
                        f3 = 1.0f;
                    } else {
                        throw new NoWhenBranchMatchedException();
                    }
                    Paint paint = new Paint(5);
                    paint.setStyle(Paint.Style.FILL);
                    int[] iArr = {i5, i5, ColorUtils.setAlphaComponent(i5, 0)};
                    paint.setShader(new RadialGradient(f7, f3, f7, iArr, new float[]{0.0f, max / f6, 1.0f}, Shader.TileMode.CLAMP));
                    new Canvas(createBitmap).drawPaint(paint);
                    j.a((Object) createBitmap, "Bitmap.createBitmap(widt…nt)\n                    }");
                    map.put(aVar, createBitmap);
                }
                m mVar = m.f5330a;
            }
        }
        Bitmap bitmap = f79a.get(aVar);
        if (bitmap == null) {
            return null;
        }
        int width = (bitmap.getWidth() / 2) - 1;
        int i9 = h.c[bVar.ordinal()];
        if (i9 == 1) {
            i2 = bitmap.getHeight() - 2;
        } else if (i9 == 2 || i9 == 3) {
            i2 = (bitmap.getHeight() / 2) - 1;
        } else if (i9 == 4) {
            i2 = 0;
        } else {
            throw new NoWhenBranchMatchedException();
        }
        int i10 = h.d[bVar.ordinal()];
        if (i10 == 1) {
            i3 = bitmap.getHeight();
        } else if (i10 == 2 || i10 == 3) {
            i3 = bitmap.getHeight() - i2;
        } else {
            if (i10 != 4) {
                throw new NoWhenBranchMatchedException();
            }
            return f.f78a.a(resources, bitmap, width, i2, bitmap.getWidth() - width, i6, null);
        }
        i6 = i3;
        return f.f78a.a(resources, bitmap, width, i2, bitmap.getWidth() - width, i6, null);
    }
}
