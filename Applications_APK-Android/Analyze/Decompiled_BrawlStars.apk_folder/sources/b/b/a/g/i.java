package b.b.a.g;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import b.b.a.i.b;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.RootFrameLayout;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.TypeCastException;
import kotlin.a.ab;
import kotlin.a.m;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.d.b.r;
import kotlin.d.b.t;
import kotlin.f.d;

public final class i extends Drawable {
    public static final /* synthetic */ kotlin.h.h[] F = {t.a(new r(t.a(i.class), "maxSize", "getMaxSize()Landroid/graphics/Rect;"))};
    public static final float G = ((float) Math.sqrt(2.0d));
    public static final List<String> H = m.a((Object[]) new String[]{"AppIcon_scroll.png", "AppIcon_reef.png", "AppIcon_magic.png", "AppIcon_soil.png", "AppIcon_laser.png"});
    public static final List<String> I = m.a((Object[]) new String[]{"small_icon_1.png", "small_icon_2.png", "small_icon_3.png", "small_icon_4.png"});
    public static final int[] J = {Color.parseColor("#5BC5E8"), Color.parseColor("#3780BD"), Color.parseColor("#103A8B"), Color.parseColor("#021847")};
    public static final float[] K = {0.0f, 0.222f, 0.557f, 1.0f};
    public float A = 1.0f;
    public int B = -1;
    public int C = -1;
    public final RectF D = new RectF();
    public final MainActivity E;

    /* renamed from: a  reason: collision with root package name */
    public final Paint f87a;

    /* renamed from: b  reason: collision with root package name */
    public final Paint f88b;
    public final Paint c;
    public final Path d = new Path();
    public boolean e;
    public final int f = ((Number) new h().invoke()).intValue();
    public final kotlin.d g = kotlin.e.a(new c());
    public float h;
    public boolean i;
    public boolean j = true;
    public Animator k;
    public boolean l;
    public Animator m;
    public final Map<String, Bitmap> n = new LinkedHashMap();
    public final Map<String, Bitmap> o = new LinkedHashMap();
    public Bitmap p;
    public Bitmap q;
    public List<a> r = ab.f5210a;
    public long s;
    public long t;
    public long u;
    public float v = 2.0f;
    public float w;
    public float x;
    public float y;
    public float z = 1.0f;

    public final class a implements ValueAnimator.AnimatorUpdateListener {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ ValueAnimator f89a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ i f90b;

        public a(ValueAnimator valueAnimator, i iVar) {
            this.f89a = valueAnimator;
            this.f90b = iVar;
        }

        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            i iVar = this.f90b;
            Object animatedValue = this.f89a.getAnimatedValue();
            if (animatedValue != null) {
                iVar.a(((Integer) animatedValue).intValue());
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Int");
        }
    }

    public final class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ ValueAnimator f91a;

        public b(ValueAnimator valueAnimator) {
            this.f91a = valueAnimator;
        }

        public final void run() {
            this.f91a.start();
        }
    }

    public final class c extends k implements kotlin.d.a.a<Rect> {
        public c() {
            super(0);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.content.res.Resources, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.RootFrameLayout, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final Object invoke() {
            Resources resources = i.this.E.getResources();
            j.a((Object) resources, "mainActivity.resources");
            if (b.b.a.b.b(resources)) {
                DisplayMetrics displayMetrics = new DisplayMetrics();
                Object systemService = i.this.E.getSystemService("window");
                if (systemService != null) {
                    ((WindowManager) systemService).getDefaultDisplay().getMetrics(displayMetrics);
                    MainActivity mainActivity = i.this.E;
                    b.a.C0016a aVar = b.a.d;
                    RootFrameLayout rootFrameLayout = (RootFrameLayout) mainActivity.a(R.id.root_layout);
                    j.a((Object) rootFrameLayout, "root_layout");
                    int width = rootFrameLayout.getWidth();
                    boolean z = true;
                    int i = ViewCompat.getLayoutDirection((RootFrameLayout) mainActivity.a(R.id.root_layout)) == 1 ? ((RootFrameLayout) mainActivity.a(R.id.root_layout)).getSystemWindowInsets().right : ((RootFrameLayout) mainActivity.a(R.id.root_layout)).getSystemWindowInsets().left;
                    if (ViewCompat.getLayoutDirection((RootFrameLayout) mainActivity.a(R.id.root_layout)) != 1) {
                        z = false;
                    }
                    return new Rect(0, 0, mainActivity.h() + aVar.a(width, i, z ? ((RootFrameLayout) mainActivity.a(R.id.root_layout)).getSystemWindowInsets().left : ((RootFrameLayout) mainActivity.a(R.id.root_layout)).getSystemWindowInsets().right), displayMetrics.heightPixels);
                }
                throw new TypeCastException("null cannot be cast to non-null type android.view.WindowManager");
            }
            Resources resources2 = i.this.E.getResources();
            j.a((Object) resources2, "mainActivity.resources");
            if (b.b.a.b.c(resources2)) {
                return new Rect(0, 0, i.this.E.getResources().getDimensionPixelSize(R.dimen.tablet_main_content_width), i.this.E.f());
            }
            DisplayMetrics displayMetrics2 = new DisplayMetrics();
            Object systemService2 = i.this.E.getSystemService("window");
            if (systemService2 != null) {
                ((WindowManager) systemService2).getDefaultDisplay().getMetrics(displayMetrics2);
                return new Rect(0, 0, displayMetrics2.widthPixels, i.this.E.f());
            }
            throw new TypeCastException("null cannot be cast to non-null type android.view.WindowManager");
        }
    }

    public final class d extends k implements kotlin.d.a.a<Bitmap> {
        public d() {
            super(0);
        }

        public final Bitmap invoke() {
            i iVar = i.this;
            int i = iVar.B;
            int size = iVar.n.size();
            int i2 = 100;
            while (i == i.this.B && i2 > 0) {
                d.b bVar = kotlin.f.d.c;
                i = kotlin.f.d.f5256a.b(size);
                i2--;
            }
            i iVar2 = i.this;
            iVar2.B = i;
            return (Bitmap) m.g(iVar2.n.values()).get(i);
        }
    }

    public final class e extends k implements kotlin.d.a.a<Bitmap> {
        public e() {
            super(0);
        }

        public final Bitmap invoke() {
            i iVar = i.this;
            int i = iVar.C;
            int size = iVar.o.size();
            int i2 = 100;
            while (i == i.this.C && i2 > 0) {
                d.b bVar = kotlin.f.d.c;
                i = kotlin.f.d.f5256a.b(size);
                i2--;
            }
            i iVar2 = i.this;
            iVar2.C = i;
            return (Bitmap) m.g(iVar2.o.values()).get(i);
        }
    }

    public final class f implements ValueAnimator.AnimatorUpdateListener {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ ValueAnimator f95a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ i f96b;

        public f(ValueAnimator valueAnimator, i iVar) {
            this.f95a = valueAnimator;
            this.f96b = iVar;
        }

        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            i iVar = this.f96b;
            Object animatedValue = this.f95a.getAnimatedValue();
            if (animatedValue != null) {
                int intValue = ((Integer) animatedValue).intValue();
                if (iVar.f87a.getAlpha() != intValue) {
                    iVar.f87a.setAlpha(intValue);
                }
                iVar.invalidateSelf();
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Int");
        }
    }

    public final class g implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ ValueAnimator f97a;

        public g(ValueAnimator valueAnimator) {
            this.f97a = valueAnimator;
        }

        public final void run() {
            this.f97a.start();
        }
    }

    public final class h extends k implements kotlin.d.a.a<Integer> {
        public h() {
            super(0);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.content.res.Resources, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final Object invoke() {
            int i;
            Resources resources = i.this.E.getResources();
            j.a((Object) resources, "mainActivity.resources");
            if (b.b.a.b.c(resources)) {
                i = i.this.E.getResources().getDimensionPixelSize(R.dimen.tablet_main_content_width);
            } else {
                DisplayMetrics displayMetrics = new DisplayMetrics();
                Object systemService = i.this.E.getSystemService("window");
                if (systemService != null) {
                    ((WindowManager) systemService).getDefaultDisplay().getMetrics(displayMetrics);
                    i = Math.min(displayMetrics.widthPixels, displayMetrics.heightPixels);
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type android.view.WindowManager");
                }
            }
            return Integer.valueOf(i);
        }
    }

    public final Rect a() {
        return (Rect) this.g.a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.graphics.Rect, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void a(float f2) {
        if (this.h != f2) {
            this.h = f2;
            Rect bounds = getBounds();
            j.a((Object) bounds, "bounds");
            a(bounds);
            invalidateSelf();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
     arg types: [java.util.List<b.b.a.g.a>, b.b.a.g.a]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T> */
    public final void a(Bitmap bitmap, boolean z2, boolean z3, float f2) {
        float f3;
        float f4;
        float f5;
        float f6;
        float f7;
        Bitmap bitmap2 = z2 ? this.p : this.q;
        if (bitmap2 == null) {
            j.a();
        }
        float f8 = z2 ? this.z : this.A;
        float max = ((float) Math.max(bitmap.getWidth(), bitmap2.getWidth())) * f8;
        float max2 = ((float) Math.max(bitmap.getHeight(), bitmap2.getHeight())) * f8;
        if (z2) {
            f3 = this.z;
        } else {
            f3 = this.A;
        }
        float height = ((float) bitmap.getHeight()) * 0.5f * f3;
        float f9 = this.y;
        float f10 = f9 - height;
        if (height < f10) {
            d.b bVar = kotlin.f.d.c;
            f4 = ((f10 - height) * kotlin.f.d.f5256a.c()) + height;
        } else {
            f4 = f9 * 0.5f;
        }
        if (z3) {
            f5 = this.w;
        } else {
            f5 = this.x;
        }
        float f11 = f4 + f5;
        float f12 = (-f11) - ((max + max2) * 0.5f);
        float width = f12 + ((((float) a().width()) + max) * G) + max2;
        d.b bVar2 = kotlin.f.d.c;
        if (z2) {
            f7 = kotlin.f.d.f5256a.c() * 1.5f;
            f6 = 15.0f;
        } else {
            f7 = kotlin.f.d.f5256a.c();
            f6 = 8.0f;
        }
        long j2 = (long) ((f7 + f6) * this.v * 1000.0f);
        long max3 = (long) Math.max(1.0f, ((z2 ? 0.6f : 0.4f) - f2) * ((float) j2));
        if (z3) {
            this.s = max3;
        } else {
            this.t = max3;
        }
        this.r = m.a((Collection) this.r, (Object) new a(bitmap, z2, bitmap2, b.b.a.b.a(z2 ? 50 : 30) * f8, f8, f12, width, f11, j2, f2));
    }

    public final void a(Rect rect) {
        this.d.reset();
        if (this.h > 0.0f) {
            Path path = this.d;
            RectF rectF = new RectF(rect);
            float[] fArr = new float[8];
            int i2 = 0;
            while (i2 < 8) {
                fArr[i2] = i2 < 4 ? this.h : 0.0f;
                i2++;
            }
            path.addRoundRect(rectF, fArr, Path.Direction.CW);
            return;
        }
        this.d.addRect(new RectF(rect), Path.Direction.CW);
    }

    public final void a(boolean z2, float f2) {
        a((Bitmap) new e().invoke(), false, z2, f2);
    }

    public final void a(boolean z2, float f2, String str) {
        Bitmap bitmap;
        if (str == null || (bitmap = this.n.get(str)) == null) {
            bitmap = (Bitmap) new d().invoke();
        }
        a(bitmap, true, z2, f2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.g.i.a(boolean, float):void
     arg types: [int, int]
     candidates:
      b.b.a.g.i.a(b.b.a.g.i, int):void
      b.b.a.g.i.a(boolean, boolean):void
      b.b.a.g.i.a(boolean, float):void */
    public final void b() {
        if (!this.l) {
            if (this.i && this.e && this.y > 0.0f && (this.n.isEmpty() ^ true) && (this.o.isEmpty() ^ true) && this.p != null && this.q != null) {
                b(true);
                Bitmap bitmap = (Bitmap) m.d(this.n.values());
                if (bitmap != null) {
                    float height = (float) bitmap.getHeight();
                    float f2 = this.y;
                    if (height > f2 * 0.9f || height < f2 * 0.85f) {
                        float f3 = (this.y * 0.9f) / height;
                        float f4 = 0.01f;
                        if (Float.compare(f3, 0.01f) >= 0) {
                            f4 = Float.compare(f3, 2.0f) > 0 ? 2.0f : f3;
                        }
                        this.z = f4;
                        this.A = this.z / 2.0f;
                    }
                }
                StringBuilder a2 = b.a.a.a.a.a("AppIcon_");
                a2.append(SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getGame());
                a2.append(".png");
                a(false, 0.5f, a2.toString());
                a(true, 0.3f);
                this.u = System.currentTimeMillis();
            }
        }
    }

    public final void b(int i2) {
        if (this.f87a.getAlpha() != i2) {
            this.f87a.setAlpha(i2);
        }
        invalidateSelf();
    }

    public final void c(boolean z2) {
        this.i = z2;
        if (z2) {
            b();
        } else {
            b(false);
        }
    }

    public final void draw(Canvas canvas) {
        j.b(canvas, "canvas");
        if (this.f87a.getAlpha() < 255) {
            canvas.drawPath(this.d, this.f88b);
            if (this.c.getAlpha() > 0) {
                canvas.save();
                canvas.clipPath(this.d);
                canvas.rotate(-45.0f);
                for (a aVar : this.r) {
                    Bitmap bitmap = aVar.c;
                    RectF rectF = this.D;
                    j.b(canvas, "canvas");
                    j.b(rectF, "outRectF");
                    float scaledWidth = ((float) aVar.c.getScaledWidth(canvas)) * aVar.e * 0.5f;
                    float scaledHeight = ((float) aVar.c.getScaledHeight(canvas)) * aVar.e * 0.5f;
                    float a2 = aVar.a();
                    float f2 = aVar.d;
                    float a3 = aVar.a();
                    float f3 = aVar.d;
                    rectF.set((a2 - f2) - scaledWidth, (aVar.h + f2) - scaledHeight, (a3 - f3) + scaledWidth, aVar.h + f3 + scaledHeight);
                    canvas.drawBitmap(bitmap, (Rect) null, rectF, this.c);
                }
                for (a aVar2 : this.r) {
                    Bitmap bitmap2 = aVar2.f64a;
                    RectF rectF2 = this.D;
                    j.b(canvas, "canvas");
                    j.b(rectF2, "outRectF");
                    float scaledWidth2 = ((float) aVar2.f64a.getScaledWidth(canvas)) * aVar2.e * 0.5f;
                    float scaledHeight2 = ((float) aVar2.f64a.getScaledHeight(canvas)) * aVar2.e * 0.5f;
                    rectF2.set(aVar2.a() - scaledWidth2, aVar2.h - scaledHeight2, aVar2.a() + scaledWidth2, aVar2.h + scaledHeight2);
                    canvas.drawBitmap(bitmap2, (Rect) null, rectF2, this.c);
                }
                canvas.restore();
                long currentTimeMillis = System.currentTimeMillis();
                long j2 = currentTimeMillis - this.u;
                this.u = currentTimeMillis;
                for (a aVar3 : this.r) {
                    aVar3.j = (((float) j2) / ((float) aVar3.i)) + aVar3.j;
                }
                List<a> list = this.r;
                ArrayList arrayList = new ArrayList();
                Iterator<T> it = list.iterator();
                while (true) {
                    boolean z2 = false;
                    if (!it.hasNext()) {
                        break;
                    }
                    T next = it.next();
                    if (((a) next).j > 1.0f) {
                        z2 = true;
                    }
                    if (!z2) {
                        arrayList.add(next);
                    }
                }
                this.r = arrayList;
                long j3 = this.s;
                if (j3 > 0) {
                    this.s = j3 - j2;
                    if (this.s <= 0) {
                        a(true);
                    }
                }
                long j4 = this.t;
                if (j4 > 0) {
                    this.t = j4 - j2;
                    if (this.t <= 0) {
                        a(false);
                    }
                }
                invalidateSelf();
            }
        }
        if (this.f87a.getAlpha() > 0) {
            canvas.drawPath(this.d, this.f87a);
        }
    }

    public final int getOpacity() {
        return -1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.RadialGradient.<init>(float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
     arg types: [int, float, float, int[], float[], android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.RadialGradient.<init>(float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.RadialGradient.<init>(float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.RadialGradient.<init>(float, float, float, int, int, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.RadialGradient.<init>(float, float, float, int[], float[], android.graphics.Shader$TileMode):void} */
    public final void onBoundsChange(Rect rect) {
        if (rect != null) {
            this.f88b.setShader(new RadialGradient(0.0f, (float) rect.height(), ((float) this.f) * 1.25f, J, K, Shader.TileMode.CLAMP));
            a(rect);
            this.e = true;
            b();
            float width = (float) a().width();
            float height = (float) a().height();
            float f2 = 1.0f / G;
            float cos = (float) Math.cos((double) (((float) Math.atan((double) (height / width))) - 0.7853982f));
            this.w = b.b.a.b.a(10) * f2;
            float sqrt = (cos * ((float) Math.sqrt((double) ((height * height) + (width * width))))) / 2.0f;
            this.x = (b.b.a.b.a(15) * f2) + sqrt;
            this.y = sqrt - (b.b.a.b.a(25) * f2);
            b();
        }
    }

    public final void setAlpha(int i2) {
    }

    public final void setColorFilter(ColorFilter colorFilter) {
    }

    public final void a(boolean z2) {
        List<a> list = this.r;
        int i2 = 0;
        if (!(list instanceof Collection) || !list.isEmpty()) {
            for (a aVar : list) {
                if (aVar.f65b && (i2 = i2 + 1) < 0) {
                    m.b();
                }
            }
        }
        if (i2 >= 1) {
            d.b bVar = kotlin.f.d.c;
            if (kotlin.f.d.f5256a.b(3) != 0) {
                a(z2, 0.0f);
                return;
            }
        }
        a(z2, 0.0f, null);
    }

    public final void a(boolean z2, boolean z3) {
        int i2 = z2 ? 0 : 255;
        if (z2 != this.j) {
            this.j = z2;
            Animator animator = this.k;
            if (animator != null) {
                animator.cancel();
            }
            this.k = null;
            if (z3) {
                ValueAnimator ofInt = ValueAnimator.ofInt(this.f87a.getAlpha(), i2);
                ofInt.addUpdateListener(new f(ofInt, this));
                this.E.runOnUiThread(new g(ofInt));
                this.k = ofInt;
                return;
            }
            b(i2);
        } else if (!z3) {
            Animator animator2 = this.k;
            if (animator2 != null) {
                animator2.cancel();
            }
            this.k = null;
            b(i2);
        }
    }

    public final void b(boolean z2) {
        if (z2 != this.l) {
            this.l = z2;
            int i2 = z2 ? 77 : 0;
            Animator animator = this.m;
            if (animator != null) {
                animator.cancel();
            }
            this.m = null;
            ValueAnimator ofInt = ValueAnimator.ofInt(this.c.getAlpha(), i2);
            ofInt.addUpdateListener(new a(ofInt, this));
            this.E.runOnUiThread(new b(ofInt));
            this.m = ofInt;
        }
    }

    public i(MainActivity mainActivity) {
        j.b(mainActivity, "mainActivity");
        this.E = mainActivity;
        Paint paint = new Paint(1);
        paint.setColor(ContextCompat.getColor(this.E, R.color.white));
        this.f87a = paint;
        Paint paint2 = new Paint(1);
        paint2.setDither(true);
        this.f88b = paint2;
        Paint paint3 = new Paint(1);
        paint3.setAlpha(0);
        paint3.setDither(true);
        this.c = paint3;
        for (String next : H) {
            nl.komponents.kovenant.c.m.a(b.b.a.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().j, next), new j(this, next));
        }
        for (String next2 : I) {
            nl.komponents.kovenant.c.m.a(b.b.a.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().j, next2), new k(this, next2));
        }
        nl.komponents.kovenant.c.m.a(b.b.a.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().j, "AppIcon_shadow.png"), new l(this));
        nl.komponents.kovenant.c.m.a(b.b.a.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().j, "small_icon_shadow.png"), new m(this));
    }

    public final void a(int i2) {
        if (this.c.getAlpha() != i2) {
            this.c.setAlpha(i2);
            if (i2 == 0) {
                this.r = ab.f5210a;
            }
            invalidateSelf();
        }
    }
}
