package b.b.a.g;

import android.graphics.Bitmap;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.m;

public final class k extends kotlin.d.b.k implements b<Bitmap, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ i f101a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ String f102b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k(i iVar, String str) {
        super(1);
        this.f101a = iVar;
        this.f102b = str;
    }

    public final Object invoke(Object obj) {
        Bitmap bitmap = (Bitmap) obj;
        j.b(bitmap, "it");
        this.f101a.o.put(this.f102b, bitmap);
        this.f101a.b();
        return m.f5330a;
    }
}
