package b.b.a.g;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import b.b.a.b;
import com.supercell.id.R;
import kotlin.d.b.j;
import kotlin.e.a;

public final class d extends Drawable {

    /* renamed from: a  reason: collision with root package name */
    public final Paint f72a;

    /* renamed from: b  reason: collision with root package name */
    public final Paint f73b;
    public final Path c = new Path();
    public final Path d = new Path();
    public float e;

    public d(Context context) {
        j.b(context, "context");
        Paint paint = new Paint(1);
        paint.setColor(ContextCompat.getColor(context, R.color.white));
        this.f72a = paint;
        Paint paint2 = new Paint(1);
        paint2.setColor(ContextCompat.getColor(context, R.color.gray91));
        this.f73b = paint2;
    }

    public final void a(Rect rect) {
        this.c.reset();
        this.d.reset();
        RectF rectF = new RectF(rect);
        if (this.e > 0.0f) {
            rectF.inset(b.a(1), b.a(1));
            float a2 = this.e - b.a(1);
            if (a2 > 0.0f) {
                this.c.addRoundRect(rectF, a2, a2, Path.Direction.CW);
            } else {
                this.c.addRect(rectF, Path.Direction.CW);
            }
            Path path = this.d;
            RectF rectF2 = new RectF(rect);
            float f = this.e;
            path.addRoundRect(rectF2, f, f, Path.Direction.CW);
            return;
        }
        rectF.bottom -= (float) a.a(b.a(2));
        this.c.addRect(rectF, Path.Direction.CW);
        this.d.addRect(new RectF(rect), Path.Direction.CW);
    }

    public final void draw(Canvas canvas) {
        j.b(canvas, "canvas");
        canvas.drawPath(this.d, this.f73b);
        canvas.drawPath(this.c, this.f72a);
    }

    public final int getOpacity() {
        return -1;
    }

    public final void onBoundsChange(Rect rect) {
        if (rect != null) {
            a(rect);
        }
    }

    public final void setAlpha(int i) {
    }

    public final void setColorFilter(ColorFilter colorFilter) {
    }
}
