package b.b.a.i;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import kotlin.d.b.j;

public abstract class f1 extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    public Boolean f244a;

    public abstract void a();

    public final void a(Boolean bool) {
        Boolean bool2 = this.f244a;
        boolean z = true;
        if (bool2 == null || bool == null || !(!j.a(bool2, bool)) || !bool.booleanValue()) {
            z = false;
        }
        this.f244a = bool;
        if (z) {
            c();
        }
    }

    public boolean b() {
        return isResumed() && getUserVisibleHint();
    }

    public void c() {
    }

    public final void d() {
        a(Boolean.valueOf(b()));
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        boolean z = false;
        if (bundle != null) {
            z = bundle.getBoolean("tabSelected", false);
        }
        a(Boolean.valueOf(z));
    }

    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    public void onResume() {
        super.onResume();
        d();
    }

    public void onSaveInstanceState(Bundle bundle) {
        j.b(bundle, "outState");
        Boolean bool = this.f244a;
        if (bool != null) {
            bundle.putBoolean("tabSelected", bool.booleanValue());
        }
        super.onSaveInstanceState(bundle);
    }

    public void setUserVisibleHint(boolean z) {
        super.setUserVisibleHint(z);
        d();
    }
}
