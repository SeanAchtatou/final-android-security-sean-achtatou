package b.b.a.i.o1;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import b.b.a.i.e;
import b.b.a.i.u;
import b.b.a.i.v;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.WidthAdjustingMultilineButton;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class a extends q implements u {
    public HashMap c;

    /* renamed from: b.b.a.i.o1.a$a  reason: collision with other inner class name */
    public final class C0038a extends k implements kotlin.d.a.b<String, m> {
        public C0038a() {
            super(1);
        }

        public final Object invoke(Object obj) {
            j.b((String) obj, "it");
            p h = a.this.h();
            if (h != null) {
                h.k();
            }
            return m.f5330a;
        }
    }

    public final class b extends k implements kotlin.d.a.b<String, m> {
        public b() {
            super(1);
        }

        public final Object invoke(Object obj) {
            String str = (String) obj;
            j.b(str, "it");
            MainActivity a2 = b.b.a.b.a(a.this);
            if (a2 != null) {
                a2.a("gameclient_error_" + str, (kotlin.d.a.b<? super e, m>) null);
            }
            return m.f5330a;
        }
    }

    public final class c implements View.OnClickListener {
        public c() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) a.this.a(R.id.okButton);
            j.a((Object) widthAdjustingMultilineButton, "okButton");
            widthAdjustingMultilineButton.setEnabled(false);
            WidthAdjustingMultilineButton widthAdjustingMultilineButton2 = (WidthAdjustingMultilineButton) a.this.a(R.id.cancelButton);
            j.a((Object) widthAdjustingMultilineButton2, "cancelButton");
            widthAdjustingMultilineButton2.setEnabled(false);
            SupercellId.INSTANCE.clearPendingLogin$supercellId_release();
            MainActivity a2 = b.b.a.b.a(a.this);
            if (a2 != null) {
                a2.d();
            }
        }
    }

    public final class d implements View.OnClickListener {
        public d() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            String str;
            WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) a.this.a(R.id.okButton);
            j.a((Object) widthAdjustingMultilineButton, "okButton");
            widthAdjustingMultilineButton.setEnabled(false);
            WidthAdjustingMultilineButton widthAdjustingMultilineButton2 = (WidthAdjustingMultilineButton) a.this.a(R.id.cancelButton);
            j.a((Object) widthAdjustingMultilineButton2, "cancelButton");
            widthAdjustingMultilineButton2.setEnabled(false);
            a aVar = a.this;
            String g = aVar.g();
            String i = aVar.i();
            p h = aVar.h();
            if (h == null || (str = h.p) == null) {
                str = "";
            }
            WeakReference weakReference = new WeakReference(aVar);
            nl.komponents.kovenant.c.m.b(nl.komponents.kovenant.c.m.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().g.b(g, i, str), new b(weakReference)), new c(weakReference));
        }
    }

    public final View a(int i) {
        if (this.c == null) {
            this.c = new HashMap();
        }
        View view = (View) this.c.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.c.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.c;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void a(v vVar) {
        j.b(vVar, "dialog");
        WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) a(R.id.okButton);
        j.a((Object) widthAdjustingMultilineButton, "okButton");
        widthAdjustingMultilineButton.setEnabled(true);
        WidthAdjustingMultilineButton widthAdjustingMultilineButton2 = (WidthAdjustingMultilineButton) a(R.id.cancelButton);
        j.a((Object) widthAdjustingMultilineButton2, "cancelButton");
        widthAdjustingMultilineButton2.setEnabled(true);
    }

    public final void a(String str, String str2) {
        nl.komponents.kovenant.c.m.b(nl.komponents.kovenant.c.m.a(SupercellId.INSTANCE.bindAccount$supercellId_release(str, str2, g(), i(), j()), new C0038a()), new b());
    }

    public final void c() {
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a("Log In Progress Step 3");
    }

    public final void c(String str) {
        SupercellId.INSTANCE.loadAccount$supercellId_release(g(), i(), str, j());
        MainActivity a2 = b.b.a.b.a(this);
        if (a2 != null) {
            a2.e();
        }
    }

    public final void e() {
        k();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.i.x1.j.a(android.widget.ImageView, java.lang.String, boolean, int):void
     arg types: [android.widget.ImageView, java.lang.String, int, int]
     candidates:
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, java.lang.Object, int):void
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, kotlin.d.a.b, int):void
      b.b.a.i.x1.j.a(android.widget.ImageView, java.lang.String, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, kotlin.d.a.b, int):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String, ?[OBJECT, ARRAY], int]
     candidates:
      b.b.a.i.x1.j.a(android.widget.ImageView, java.lang.String, boolean, int):void
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, java.lang.Object, int):void
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, kotlin.d.a.b, int):void */
    public final void k() {
        String str;
        WidthAdjustingMultilineButton widthAdjustingMultilineButton;
        if (f()) {
            ImageView imageView = (ImageView) a(R.id.illustrationImageView);
            if (imageView != null) {
                b.b.a.i.x1.j.a(imageView, "login_confirm_load_game_illustration.png", false, 2);
            }
            TextView textView = (TextView) a(R.id.titleTextView);
            if (textView != null) {
                b.b.a.i.x1.j.a(textView, "log_in_load_game_heading", (kotlin.d.a.b) null, 2);
            }
            TextView textView2 = (TextView) a(R.id.subtitleTextView);
            if (textView2 != null) {
                b.b.a.i.x1.j.a(textView2, "log_in_load_game_description", (kotlin.d.a.b) null, 2);
            }
            WidthAdjustingMultilineButton widthAdjustingMultilineButton2 = (WidthAdjustingMultilineButton) a(R.id.cancelButton);
            if (widthAdjustingMultilineButton2 != null) {
                b.b.a.i.x1.j.a((TextView) widthAdjustingMultilineButton2, "log_in_load_game_btn_cancel", (kotlin.d.a.b) null, 2);
            }
            widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) a(R.id.okButton);
            if (widthAdjustingMultilineButton != null) {
                str = "log_in_load_game_btn_confirm";
            } else {
                return;
            }
        } else {
            ImageView imageView2 = (ImageView) a(R.id.illustrationImageView);
            if (imageView2 != null) {
                b.b.a.i.x1.j.a(imageView2, "login_confirm_connect_game_illustration.png", false, 2);
            }
            TextView textView3 = (TextView) a(R.id.titleTextView);
            if (textView3 != null) {
                b.b.a.i.x1.j.a(textView3, "log_in_connect_game_heading", (kotlin.d.a.b) null, 2);
            }
            TextView textView4 = (TextView) a(R.id.subtitleTextView);
            if (textView4 != null) {
                b.b.a.i.x1.j.a(textView4, "log_in_connect_game_description", (kotlin.d.a.b) null, 2);
            }
            WidthAdjustingMultilineButton widthAdjustingMultilineButton3 = (WidthAdjustingMultilineButton) a(R.id.cancelButton);
            if (widthAdjustingMultilineButton3 != null) {
                b.b.a.i.x1.j.a((TextView) widthAdjustingMultilineButton3, "log_in_connect_game_btn_cancel", (kotlin.d.a.b) null, 2);
            }
            widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) a(R.id.okButton);
            if (widthAdjustingMultilineButton != null) {
                str = "log_in_connect_game_btn_confirm";
            } else {
                return;
            }
        }
        b.b.a.i.x1.j.a((TextView) widthAdjustingMultilineButton, str, (kotlin.d.a.b) null, 2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_login_confirm_page, viewGroup, false);
    }

    public final void onDestroyView() {
        super.onDestroyView();
        MainActivity a2 = b.b.a.b.a(this);
        if (a2 != null) {
            a2.b(this);
        }
        a();
    }

    public final void onViewCreated(View view, Bundle bundle) {
        j.b(view, "view");
        super.onViewCreated(view, bundle);
        MainActivity a2 = b.b.a.b.a(this);
        if (a2 != null) {
            a2.a(this);
        }
        k();
        ((WidthAdjustingMultilineButton) a(R.id.cancelButton)).setOnClickListener(new c());
        ((WidthAdjustingMultilineButton) a(R.id.okButton)).setOnClickListener(new d());
    }
}
