package b.b.a.i.q1;

import android.support.v4.app.Fragment;
import b.b.a.i.x;

public abstract class b extends x {
    public a f() {
        Fragment parentFragment = getParentFragment();
        if (!(parentFragment instanceof a)) {
            parentFragment = null;
        }
        return (a) parentFragment;
    }

    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }
}
