package b.b.a.i.v1;

import b.b.a.j.y0;
import java.lang.ref.WeakReference;
import kotlin.d.a.b;
import kotlin.d.b.k;
import kotlin.m;
import nl.komponents.kovenant.bb;
import nl.komponents.kovenant.bw;

public final class o extends k implements b<Boolean, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f807a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public o(WeakReference weakReference) {
        super(1);
        this.f807a = weakReference;
    }

    public final Object invoke(Object obj) {
        Object obj2 = this.f807a.get();
        if (obj2 != null) {
            ((Boolean) obj).booleanValue();
            j jVar = (j) obj2;
            y0<h> y0Var = jVar.p;
            bw.a aVar = bw.d;
            h hVar = jVar.q;
            y0Var.a(bb.f5389a);
        }
        return m.f5330a;
    }
}
