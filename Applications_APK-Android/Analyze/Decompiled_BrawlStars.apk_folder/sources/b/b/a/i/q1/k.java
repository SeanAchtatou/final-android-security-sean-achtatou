package b.b.a.i.q1;

import b.b.a.j.t0;
import b.b.a.j.u0;
import java.util.List;
import kotlin.d.a.a;

public final class k extends kotlin.d.b.k implements a<u0> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ List f566a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k(List list) {
        super(0);
        this.f566a = list;
    }

    public final Object invoke() {
        List list = this.f566a;
        return new u0(list, null, b.a.a.a.a.a(t0.c, list, null, "DiffUtil.calculateDiff(R…create(oldRows, newRows))"));
    }
}
