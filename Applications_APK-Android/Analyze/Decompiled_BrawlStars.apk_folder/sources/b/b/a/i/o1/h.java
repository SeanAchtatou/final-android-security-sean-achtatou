package b.b.a.i.o1;

import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import b.b.a.i.u;
import b.b.a.i.v;
import b.b.a.j.n;
import b.b.a.j.q1;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.Checkbox;
import com.supercell.id.view.WidthAdjustingMultilineButton;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import kotlin.TypeCastException;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.j.t;
import nl.komponents.kovenant.c.m;

public final class h extends q implements u {
    public String c = "";
    public boolean d;
    public HashMap e;

    public final class a implements TextWatcher {
        public a() {
        }

        public final void afterTextChanged(Editable editable) {
            h.this.k();
        }

        public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    public final class b implements View.OnFocusChangeListener {

        public final class a implements Runnable {

            /* renamed from: b.b.a.i.o1.h$b$a$a  reason: collision with other inner class name */
            public final class C0041a extends k implements kotlin.d.a.b<View, Boolean> {

                /* renamed from: a  reason: collision with root package name */
                public static final C0041a f424a = new C0041a();

                public C0041a() {
                    super(1);
                }

                public final Object invoke(Object obj) {
                    View view = (View) obj;
                    j.b(view, "it");
                    return Boolean.valueOf(view instanceof EditText);
                }
            }

            public a() {
            }

            public final void run() {
                p h = h.this.h();
                if (h != null) {
                    h.a(C0041a.f424a);
                }
            }
        }

        public b() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.ScrollView, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onFocusChange(View view, boolean z) {
            if (z) {
                ScrollView scrollView = (ScrollView) h.this.a(R.id.loginEnterEmailScrollView);
                j.a((Object) scrollView, "loginEnterEmailScrollView");
                j.a((Object) view, "v");
                q1.a(scrollView, view);
                return;
            }
            view.post(new a());
        }
    }

    public final class c implements View.OnClickListener {
        public c() {
        }

        public final void onClick(View view) {
            ((Checkbox) h.this.a(R.id.rememberCheckBox)).toggle();
        }
    }

    public final class d implements View.OnClickListener {
        public d() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Log In Progress Step 1", "click", "Remember me info", null, false, 24);
            MainActivity a2 = b.b.a.b.a(h.this);
            if (a2 != null) {
                j.a((Object) view, "it");
                b.b.a.b.b(a2, view);
            }
        }
    }

    public final class e implements View.OnClickListener {
        public e() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) h.this.a(R.id.okButton);
            j.a((Object) widthAdjustingMultilineButton, "okButton");
            widthAdjustingMultilineButton.setEnabled(false);
            WidthAdjustingMultilineButton widthAdjustingMultilineButton2 = (WidthAdjustingMultilineButton) h.this.a(R.id.cancelButton);
            j.a((Object) widthAdjustingMultilineButton2, "cancelButton");
            widthAdjustingMultilineButton2.setEnabled(false);
            SupercellId.INSTANCE.clearPendingLogin$supercellId_release();
            MainActivity a2 = b.b.a.b.a(h.this);
            if (a2 != null) {
                a2.d();
            }
        }
    }

    public final class f implements View.OnClickListener {
        public f() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.Checkbox, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            MainActivity a2;
            String str;
            WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) h.this.a(R.id.okButton);
            j.a((Object) widthAdjustingMultilineButton, "okButton");
            boolean z = false;
            widthAdjustingMultilineButton.setEnabled(false);
            WidthAdjustingMultilineButton widthAdjustingMultilineButton2 = (WidthAdjustingMultilineButton) h.this.a(R.id.cancelButton);
            j.a((Object) widthAdjustingMultilineButton2, "cancelButton");
            widthAdjustingMultilineButton2.setEnabled(false);
            h hVar = h.this;
            if (hVar.c.length() == 0) {
                z = true;
            }
            if (z) {
                a2 = b.b.a.b.a(hVar);
                if (a2 != null) {
                    str = "missing_required_data";
                } else {
                    return;
                }
            } else if (!hVar.d) {
                a2 = b.b.a.b.a(hVar);
                if (a2 != null) {
                    str = "invalid_email_address";
                } else {
                    return;
                }
            } else {
                String str2 = hVar.c;
                Checkbox checkbox = (Checkbox) hVar.a(R.id.rememberCheckBox);
                j.a((Object) checkbox, "rememberCheckBox");
                boolean isChecked = checkbox.isChecked();
                WeakReference weakReference = new WeakReference(hVar);
                m.b(m.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().g.b(str2, null), new f(weakReference, str2, isChecked)), new g(weakReference));
                return;
            }
            a2.a(str, (kotlin.d.a.b<? super b.b.a.i.e, kotlin.m>) null);
        }
    }

    public final View a(int i) {
        if (this.e == null) {
            this.e = new HashMap();
        }
        View view = (View) this.e.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.e.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.e;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void a(v vVar) {
        j.b(vVar, "dialog");
        WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) a(R.id.okButton);
        j.a((Object) widthAdjustingMultilineButton, "okButton");
        widthAdjustingMultilineButton.setEnabled(true);
        WidthAdjustingMultilineButton widthAdjustingMultilineButton2 = (WidthAdjustingMultilineButton) a(R.id.cancelButton);
        j.a((Object) widthAdjustingMultilineButton2, "cancelButton");
        widthAdjustingMultilineButton2.setEnabled(true);
    }

    public final void c() {
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a("Log In Progress Step 1");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.EditText, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.ImageView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.j.q1.a(android.support.v7.widget.AppCompatButton, boolean):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, boolean]
     candidates:
      b.b.a.j.q1.a(android.view.View, int):android.view.View
      b.b.a.j.q1.a(android.support.v4.widget.NestedScrollView, int):void
      b.b.a.j.q1.a(android.support.v4.widget.NestedScrollView, android.view.View):void
      b.b.a.j.q1.a(android.view.View, long):void
      b.b.a.j.q1.a(android.view.View, b.b.a.j.z):void
      b.b.a.j.q1.a(android.view.View, kotlin.d.a.a<kotlin.m>):void
      b.b.a.j.q1.a(android.widget.ScrollView, int):void
      b.b.a.j.q1.a(android.widget.ScrollView, android.view.View):void
      b.b.a.j.q1.a(android.support.v7.widget.AppCompatButton, boolean):void */
    public final void k() {
        EditText editText = (EditText) a(R.id.emailEditText);
        j.a((Object) editText, "emailEditText");
        String obj = editText.getText().toString();
        if (obj != null) {
            this.c = t.b(obj).toString();
            this.d = n.f1102b.c(this.c);
            ImageView imageView = (ImageView) a(R.id.validImageView);
            j.a((Object) imageView, "validImageView");
            int i = 8;
            if (imageView.getVisibility() == 8 && this.d) {
                ImageView imageView2 = (ImageView) a(R.id.validImageView);
                j.a((Object) imageView2, "validImageView");
                imageView2.setScaleX(0.0f);
                ImageView imageView3 = (ImageView) a(R.id.validImageView);
                j.a((Object) imageView3, "validImageView");
                imageView3.setScaleY(0.0f);
                ((ImageView) a(R.id.validImageView)).animate().scaleX(1.0f).scaleY(1.0f).setDuration(300).setInterpolator(b.b.a.f.a.g).start();
            }
            ImageView imageView4 = (ImageView) a(R.id.validImageView);
            j.a((Object) imageView4, "validImageView");
            if (this.d) {
                i = 0;
            }
            imageView4.setVisibility(i);
            WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) a(R.id.okButton);
            j.a((Object) widthAdjustingMultilineButton, "okButton");
            q1.a((AppCompatButton) widthAdjustingMultilineButton, !this.d);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_login_enter_email_page, viewGroup, false);
    }

    public final void onDestroyView() {
        super.onDestroyView();
        MainActivity a2 = b.b.a.b.a(this);
        if (a2 != null) {
            a2.b(this);
        }
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.Checkbox, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        j.b(view, "view");
        super.onViewCreated(view, bundle);
        MainActivity a2 = b.b.a.b.a(this);
        if (a2 != null) {
            a2.a(this);
        }
        ((EditText) a(R.id.emailEditText)).setText(g());
        k();
        ((EditText) a(R.id.emailEditText)).addTextChangedListener(new a());
        ((EditText) a(R.id.emailEditText)).setOnFocusChangeListener(new b());
        Checkbox checkbox = (Checkbox) a(R.id.rememberCheckBox);
        j.a((Object) checkbox, "rememberCheckBox");
        checkbox.setChecked(j());
        ((LinearLayout) a(R.id.rememberCheckBoxRow)).setOnClickListener(new c());
        ((ImageButton) a(R.id.rememberInfoButton)).setOnClickListener(new d());
        ((WidthAdjustingMultilineButton) a(R.id.cancelButton)).setOnClickListener(new e());
        ((WidthAdjustingMultilineButton) a(R.id.okButton)).setOnClickListener(new f());
    }
}
