package b.b.a.i.w1;

import android.support.v4.app.Fragment;
import b.b.a.i.x;

public abstract class o extends x {
    public final void b(String str) {
        n i = i();
        if (i != null) {
            i.m = str;
        }
    }

    public final boolean f() {
        n i = i();
        if (i != null) {
            return i.o;
        }
        return false;
    }

    public final String g() {
        n i = i();
        if (i != null) {
            return i.m;
        }
        return null;
    }

    public final String h() {
        n i = i();
        if (i != null) {
            return i.n;
        }
        return null;
    }

    public n i() {
        Fragment parentFragment = getParentFragment();
        if (!(parentFragment instanceof n)) {
            parentFragment = null;
        }
        return (n) parentFragment;
    }

    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }
}
