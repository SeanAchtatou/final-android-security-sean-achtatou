package b.b.a.i.j1;

import b.b.a.j.t0;
import b.b.a.j.u0;
import java.util.List;
import kotlin.d.a.a;
import kotlin.d.b.k;

public final class g extends k implements a<u0> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ List f341a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public g(List list) {
        super(0);
        this.f341a = list;
    }

    public final Object invoke() {
        List list = this.f341a;
        return new u0(list, null, b.a.a.a.a.a(t0.c, list, null, "DiffUtil.calculateDiff(R…create(oldRows, newRows))"));
    }
}
