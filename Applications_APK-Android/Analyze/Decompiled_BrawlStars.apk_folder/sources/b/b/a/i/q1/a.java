package b.b.a.i.q1;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.FlowPager;
import android.view.View;
import b.b.a.h.h;
import b.b.a.i.b;
import b.b.a.i.g;
import b.b.a.i.w;
import b.b.a.i.x;
import b.b.a.j.a0;
import com.facebook.share.internal.ShareConstants;
import com.supercell.id.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import kotlin.a.an;
import kotlin.d.b.j;
import kotlin.d.b.k;

public final class a extends w {
    public String m;
    public String n;
    public final kotlin.d.a.a<x>[] o = {b.f517a, c.f518a, d.f519a, e.f520a};
    public HashMap p;

    /* renamed from: b.b.a.i.q1.a$a  reason: collision with other inner class name */
    public static final class C0048a extends w.a implements a0 {
        public static final Parcelable.Creator<C0048a> CREATOR = new C0049a();
        public final Set<Integer> e = an.a(Integer.valueOf(R.id.nav_area_close_button));
        public final boolean f;
        public final Class<? extends g> g = a.class;
        public final h h;
        public final List<b.a> i;
        public final boolean j;

        /* renamed from: b.b.a.i.q1.a$a$a  reason: collision with other inner class name */
        public final class C0049a implements Parcelable.Creator<C0048a> {
            public final C0048a createFromParcel(Parcel parcel) {
                j.b(parcel, ShareConstants.FEED_SOURCE_PARAM);
                j.b(parcel, "parcel");
                Parcelable readParcelable = parcel.readParcelable(h.class.getClassLoader());
                if (readParcelable == null) {
                    j.a();
                }
                h hVar = (h) readParcelable;
                ArrayList arrayList = new ArrayList();
                parcel.readList(arrayList, b.a.class.getClassLoader());
                return new C0048a(hVar, arrayList, parcel.readByte() != 0);
            }

            public final C0048a[] newArray(int i) {
                return new C0048a[i];
            }
        }

        /* JADX WARN: Type inference failed for: r3v0, types: [java.lang.Object, java.util.List<b.b.a.i.b$a>, java.util.List<? extends b.b.a.i.b$a>] */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public C0048a(b.b.a.h.h r2, java.util.List<? extends b.b.a.i.b.a> r3, boolean r4) {
            /*
                r1 = this;
                java.lang.String r0 = "profile"
                kotlin.d.b.j.b(r2, r0)
                java.lang.String r0 = "backStackEntries"
                kotlin.d.b.j.b(r3, r0)
                r1.<init>()
                r1.h = r2
                r1.i = r3
                r1.j = r4
                int r2 = com.supercell.id.R.id.nav_area_close_button
                java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
                java.util.Set r2 = kotlin.a.an.a(r2)
                r1.e = r2
                java.lang.Class<b.b.a.i.q1.a> r2 = b.b.a.i.q1.a.class
                r1.g = r2
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: b.b.a.i.q1.a.C0048a.<init>(b.b.a.h.h, java.util.List, boolean):void");
        }

        public final Class<? extends g> a() {
            return this.g;
        }

        public final g c(Context context) {
            j.b(context, "context");
            return w.c.n.a("onboarding_top_area_nickname", "onboarding_top_area_avatar", "onboarding_top_area_add_friends", false);
        }

        public final Set<Integer> c() {
            return this.e;
        }

        public final boolean d(Resources resources) {
            j.b(resources, "resources");
            return true;
        }

        public final int describeContents() {
            return 0;
        }

        public final boolean e() {
            return this.f;
        }

        public final boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof C0048a) {
                    C0048a aVar = (C0048a) obj;
                    if (j.a(this.h, aVar.h) && j.a(this.i, aVar.i)) {
                        if (this.j == aVar.j) {
                            return true;
                        }
                    }
                }
                return false;
            }
            return true;
        }

        public final int hashCode() {
            h hVar = this.h;
            int i2 = 0;
            int hashCode = (hVar != null ? hVar.hashCode() : 0) * 31;
            List<b.a> list = this.i;
            if (list != null) {
                i2 = list.hashCode();
            }
            int i3 = (hashCode + i2) * 31;
            boolean z = this.j;
            if (z) {
                z = true;
            }
            return i3 + (z ? 1 : 0);
        }

        public final String toString() {
            StringBuilder a2 = b.a.a.a.a.a("BackStackEntry(profile=");
            a2.append(this.h);
            a2.append(", backStackEntries=");
            a2.append(this.i);
            a2.append(", skipIntro=");
            a2.append(this.j);
            a2.append(")");
            return a2.toString();
        }

        public final void writeToParcel(Parcel parcel, int i2) {
            j.b(parcel, "dest");
            parcel.writeParcelable(this.h, i2);
            parcel.writeList(this.i);
            parcel.writeByte(this.j ? (byte) 1 : 0);
        }
    }

    public final class b extends k implements kotlin.d.a.a<c> {

        /* renamed from: a  reason: collision with root package name */
        public static final b f517a = new b();

        public b() {
            super(0);
        }

        public final Object invoke() {
            return new c();
        }
    }

    public final class c extends k implements kotlin.d.a.a<l> {

        /* renamed from: a  reason: collision with root package name */
        public static final c f518a = new c();

        public c() {
            super(0);
        }

        public final Object invoke() {
            return new l();
        }
    }

    public final class d extends k implements kotlin.d.a.a<o> {

        /* renamed from: a  reason: collision with root package name */
        public static final d f519a = new d();

        public d() {
            super(0);
        }

        public final Object invoke() {
            return new o();
        }
    }

    public final class e extends k implements kotlin.d.a.a<d> {

        /* renamed from: a  reason: collision with root package name */
        public static final e f520a = new e();

        public e() {
            super(0);
        }

        public final Object invoke() {
            return new d();
        }
    }

    public final View a(int i) {
        if (this.p == null) {
            this.p = new HashMap();
        }
        View view = (View) this.p.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.p.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public final kotlin.d.a.a<x>[] i() {
        return this.o;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.view.FlowPager, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.Boolean, boolean]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    public final void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        String str = this.m;
        if (!(str == null || str.length() == 0)) {
            FlowPager flowPager = (FlowPager) a(R.id.flowPager);
            j.a((Object) flowPager, "flowPager");
            flowPager.setCurrentItem(2);
            return;
        }
        C0048a aVar = (C0048a) b.b.a.b.a((g) this);
        if (j.a((Object) (aVar != null ? Boolean.valueOf(aVar.j) : null), (Object) true)) {
            FlowPager flowPager2 = (FlowPager) a(R.id.flowPager);
            j.a((Object) flowPager2, "flowPager");
            flowPager2.setCurrentItem(1);
        }
    }

    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C0048a aVar = (C0048a) b.b.a.b.a((g) this);
        String str = null;
        h hVar = aVar != null ? aVar.h : null;
        this.m = hVar != null ? hVar.f121b : null;
        C0048a aVar2 = (C0048a) b.b.a.b.a((g) this);
        h hVar2 = aVar2 != null ? aVar2.h : null;
        if (hVar2 != null) {
            str = hVar2.d;
        }
        this.n = str;
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }
}
