package b.b.a.i;

import android.os.Build;
import android.view.View;
import android.view.ViewPropertyAnimator;
import kotlin.d.a.a;
import kotlin.d.b.k;
import kotlin.m;

public final class i extends k implements a<m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ View f290a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ g f291b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public i(View view, g gVar) {
        super(0);
        this.f290a = view;
        this.f291b = gVar;
    }

    public final Object invoke() {
        if (this.f291b.isAdded()) {
            ViewPropertyAnimator listener = this.f290a.animate().setStartDelay(175).setDuration(175).setInterpolator(b.b.a.f.a.h).alpha(1.0f).setListener(null);
            if (Build.VERSION.SDK_INT >= 21) {
                listener.setUpdateListener(null);
            }
            listener.start();
        }
        return m.f5330a;
    }
}
