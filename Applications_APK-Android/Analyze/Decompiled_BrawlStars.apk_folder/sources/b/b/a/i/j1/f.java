package b.b.a.i.j1;

import b.b.a.j.f0;
import b.b.a.j.o;
import b.b.a.j.t0;
import b.b.a.j.u0;
import java.util.List;
import kotlin.a.m;
import kotlin.d.a.a;
import kotlin.d.b.k;

public final class f extends k implements a<u0> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ f0 f339a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ List f340b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public f(f0 f0Var, List list) {
        super(0);
        this.f339a = f0Var;
        this.f340b = list;
    }

    public final u0 invoke() {
        List a2 = m.a(new o(this.f339a));
        List list = this.f340b;
        return new u0(list, a2, b.a.a.a.a.a(t0.c, list, a2, "DiffUtil.calculateDiff(R…create(oldRows, newRows))"));
    }
}
