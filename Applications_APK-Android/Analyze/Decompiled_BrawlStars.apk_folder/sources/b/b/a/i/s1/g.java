package b.b.a.i.s1;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import com.supercell.id.R;
import java.lang.ref.WeakReference;
import kotlin.d.a.c;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class g extends k implements c<Drawable, b.b.a.i.x1.c, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f668a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public g(WeakReference weakReference) {
        super(2);
        this.f668a = weakReference;
    }

    public final Object invoke(Object obj, Object obj2) {
        ImageView imageView;
        Drawable drawable = (Drawable) obj;
        b.b.a.i.x1.c cVar = (b.b.a.i.x1.c) obj2;
        j.b(drawable, "drawable");
        j.b(cVar, "assetLocation");
        View view = (View) this.f668a.get();
        if (!(view == null || (imageView = (ImageView) view.findViewById(R.id.systemImageView)) == null)) {
            imageView.setImageDrawable(drawable);
            if (cVar == b.b.a.i.x1.c.EXTERNAL) {
                imageView.setAlpha(0.0f);
                imageView.animate().alpha(1.0f).setDuration(300).start();
            }
        }
        return m.f5330a;
    }
}
