package b.b.a.i.z1;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import b.b.a.h.m;
import b.b.a.i.e;
import b.b.a.i.f1;
import b.b.a.j.f0;
import b.b.a.j.y0;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.Checkbox;
import com.supercell.id.view.Switch;
import com.supercell.id.view.ViewAnimator;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.a.ah;
import kotlin.d.b.j;
import kotlin.d.b.k;

public final class l extends f1 {

    /* renamed from: b  reason: collision with root package name */
    public m f1002b;
    public f0 c;
    public final y0<m> d = new y0<>(new b(), new c());
    public final Map<String, y0<m.b>> e = new LinkedHashMap();
    public HashMap f;

    public final class a implements View.OnClickListener {
        public a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.Switch, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.TextView, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.Checkbox, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            Switch switchR = (Switch) l.this.a(R.id.generalSubscribeSwitch);
            j.a((Object) switchR, "generalSubscribeSwitch");
            boolean isChecked = switchR.isChecked();
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Subscriptions", "click", "Accept marketing", Long.valueOf(isChecked ? 1 : 0), false, 16);
            l lVar = l.this;
            if (lVar.getView() != null) {
                for (View view2 : lVar.e()) {
                    TextView textView = (TextView) view2.findViewById(R.id.titleTextView);
                    j.a((Object) textView, "it.titleTextView");
                    textView.setEnabled(isChecked);
                    Checkbox checkbox = (Checkbox) view2.findViewById(R.id.consentCheckBox);
                    j.a((Object) checkbox, "it.consentCheckBox");
                    checkbox.setEnabled(isChecked);
                }
                lVar.d.a(500, new n(isChecked));
            }
        }
    }

    public final class b extends k implements kotlin.d.a.b<m, kotlin.m> {
        public b() {
            super(1);
        }

        public final Object invoke(Object obj) {
            m mVar = (m) obj;
            j.b(mVar, "response");
            l.this.f1002b = mVar;
            l.this.f();
            return kotlin.m.f5330a;
        }
    }

    public final class c extends k implements kotlin.d.a.b<Exception, kotlin.m> {
        public c() {
            super(1);
        }

        public final Object invoke(Object obj) {
            Exception exc = (Exception) obj;
            j.b(exc, "it");
            l.a(l.this);
            MainActivity a2 = b.b.a.b.a(l.this);
            if (a2 != null) {
                a2.a(exc, (kotlin.d.a.b<? super e, kotlin.m>) null);
            }
            return kotlin.m.f5330a;
        }
    }

    public final class d implements View.OnClickListener {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ View f1006a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ l f1007b;
        public final /* synthetic */ m.b c;

        public d(View view, l lVar, m.b bVar) {
            this.f1006a = view;
            this.f1007b = lVar;
            this.c = bVar;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.Checkbox, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            Checkbox checkbox = (Checkbox) this.f1006a.findViewById(R.id.consentCheckBox);
            j.a((Object) checkbox, "consentCheckBox");
            boolean isChecked = checkbox.isChecked();
            l lVar = this.f1007b;
            String str = this.c.f133a;
            Map<String, y0<m.b>> map = lVar.e;
            y0<m.b> y0Var = map.get(str);
            if (y0Var == null) {
                y0Var = new y0<>(new g(lVar, str), new h(lVar, str));
                map.put(str, y0Var);
            }
            y0Var.a(500, new m(str, isChecked));
            b.b.a.c.b bVar = SupercellId.INSTANCE.getSharedServices$supercellId_release().f;
            StringBuilder a2 = b.a.a.a.a.a("Marketing scope consent: ");
            a2.append(this.c.f133a);
            b.b.a.c.b.a(bVar, "Subscriptions", "click", a2.toString(), Long.valueOf(isChecked ? 1 : 0), false, 16);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.Switch, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.Checkbox, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final /* synthetic */ void a(l lVar) {
        m mVar;
        if (lVar.getView() != null && (mVar = lVar.f1002b) != null) {
            Switch switchR = (Switch) lVar.a(R.id.generalSubscribeSwitch);
            j.a((Object) switchR, "generalSubscribeSwitch");
            switchR.setChecked(mVar.f131a);
            for (View view : lVar.e()) {
                TextView textView = (TextView) view.findViewById(R.id.titleTextView);
                j.a((Object) textView, "it.titleTextView");
                textView.setEnabled(mVar.f131a);
                Checkbox checkbox = (Checkbox) view.findViewById(R.id.consentCheckBox);
                j.a((Object) checkbox, "it.consentCheckBox");
                checkbox.setEnabled(mVar.f131a);
            }
        }
    }

    public final View a(int i) {
        if (this.f == null) {
            this.f = new HashMap();
        }
        View view = (View) this.f.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.f.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.f;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void b(String str) {
        m mVar;
        T t;
        if (getView() != null && (mVar = this.f1002b) != null) {
            Iterator<T> it = mVar.f132b.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                if (j.a((Object) ((m.b) t).f133a, (Object) str)) {
                    break;
                }
            }
            m.b bVar = (m.b) t;
            if (bVar != null) {
                LinearLayout linearLayout = (LinearLayout) a(R.id.profileSubscriptionsContainer);
                kotlin.g.c b2 = kotlin.g.d.b(0, linearLayout.getChildCount());
                ArrayList arrayList = new ArrayList();
                Iterator it2 = b2.iterator();
                while (it2.hasNext()) {
                    View childAt = linearLayout.getChildAt(((ah) it2).a());
                    j.a((Object) childAt, "it");
                    Object tag = childAt.getTag();
                    if (!((tag instanceof m.b) && j.a(((m.b) tag).f133a, str))) {
                        childAt = null;
                    }
                    if (childAt != null) {
                        arrayList.add(childAt);
                    }
                }
                View view = (View) kotlin.a.m.d((List) arrayList);
                if (view != null) {
                    a(view, bVar);
                }
            }
        }
    }

    public final void c() {
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a("Subscriptions");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final List<View> e() {
        LinearLayout linearLayout = (LinearLayout) a(R.id.profileSubscriptionsContainer);
        kotlin.g.c b2 = kotlin.g.d.b(0, linearLayout.getChildCount());
        ArrayList arrayList = new ArrayList();
        Iterator it = b2.iterator();
        while (it.hasNext()) {
            View childAt = linearLayout.getChildAt(((ah) it).a());
            j.a((Object) childAt, "it");
            if (!(childAt.getTag() instanceof m.b)) {
                childAt = null;
            }
            if (childAt != null) {
                arrayList.add(childAt);
            }
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.Switch, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.LinearLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.Checkbox, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void f() {
        View view;
        ViewAnimator viewAnimator;
        View view2;
        if (getView() != null) {
            m mVar = this.f1002b;
            f0 f0Var = this.c;
            if (mVar != null) {
                Switch switchR = (Switch) a(R.id.generalSubscribeSwitch);
                j.a((Object) switchR, "generalSubscribeSwitch");
                switchR.setChecked(mVar.f131a);
                List<View> e2 = e();
                int i = 0;
                for (T next : mVar.f132b) {
                    int i2 = i + 1;
                    if (i < 0) {
                        kotlin.a.m.a();
                    }
                    m.b bVar = (m.b) next;
                    if (i < 0 || i > kotlin.a.m.a((List) e2)) {
                        view2 = LayoutInflater.from(getContext()).inflate(R.layout.fragment_settings_list_item_subscription, (ViewGroup) ((LinearLayout) a(R.id.profileSubscriptionsContainer)), false);
                        ((LinearLayout) a(R.id.profileSubscriptionsContainer)).addView(view2);
                        j.a((Object) view2, "newScopeView()");
                    } else {
                        view2 = e2.get(i);
                    }
                    View view3 = view2;
                    j.a((Object) view3, "this");
                    a(view3, bVar);
                    TextView textView = (TextView) view3.findViewById(R.id.titleTextView);
                    j.a((Object) textView, "titleTextView");
                    textView.setEnabled(mVar.f131a);
                    Checkbox checkbox = (Checkbox) view3.findViewById(R.id.consentCheckBox);
                    j.a((Object) checkbox, "consentCheckBox");
                    checkbox.setEnabled(mVar.f131a);
                    i = i2;
                }
                for (View removeView : kotlin.a.m.c(e2, mVar.f132b.size())) {
                    ((LinearLayout) a(R.id.profileSubscriptionsContainer)).removeView(removeView);
                }
                ((ViewAnimator) a(R.id.profileSubscriptionsViewAnimator)).setCurrentView((LinearLayout) a(R.id.profileSubscriptionsContainer));
                return;
            }
            if (f0Var != null) {
                TextView textView2 = (TextView) a(R.id.profileSubscriptionsErrorTextView);
                j.a((Object) textView2, "profileSubscriptionsErrorTextView");
                b.b.a.i.x1.j.a(textView2, f0Var.f1037b, (kotlin.d.a.b) null, 2);
                viewAnimator = (ViewAnimator) a(R.id.profileSubscriptionsViewAnimator);
                view = (TextView) a(R.id.profileSubscriptionsErrorTextView);
            } else {
                viewAnimator = (ViewAnimator) a(R.id.profileSubscriptionsViewAnimator);
                view = (FrameLayout) a(R.id.profileSubscriptionsLoadingContainer);
            }
            viewAnimator.setCurrentView(view);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_settings_subscriptions_page, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.ViewAnimator, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        j.b(view, "view");
        super.onViewCreated(view, bundle);
        ViewAnimator viewAnimator = (ViewAnimator) a(R.id.profileSubscriptionsViewAnimator);
        j.a((Object) viewAnimator, "profileSubscriptionsViewAnimator");
        viewAnimator.setSaveFromParentEnabled(false);
        ((Switch) a(R.id.generalSubscribeSwitch)).setOnClickListener(new a());
        f();
        if (this.f1002b == null) {
            this.c = null;
            WeakReference weakReference = new WeakReference(this);
            nl.komponents.kovenant.c.m.a(nl.komponents.kovenant.c.m.b(nl.komponents.kovenant.c.m.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().g.b(), new i(weakReference)), new j(weakReference)), new k(weakReference));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.p.a(java.lang.Iterable, int):int
     arg types: [java.util.List<b.b.a.h.m$b>, int]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.p.a(java.lang.Iterable, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void a(m.b bVar) {
        m mVar;
        m mVar2 = this.f1002b;
        if (mVar2 != null) {
            List<m.b> list = mVar2.f132b;
            ArrayList arrayList = new ArrayList(kotlin.a.m.a((Iterable) list, 10));
            for (m.b bVar2 : list) {
                if (j.a((Object) bVar2.f133a, (Object) bVar.f133a)) {
                    bVar2 = m.b.a(bVar2, null, null, bVar.c, 3);
                }
                arrayList.add(bVar2);
            }
            mVar = mVar2.a(mVar2.f131a, arrayList);
        } else {
            mVar = null;
        }
        this.f1002b = mVar;
        if (getView() != null) {
            LinearLayout linearLayout = (LinearLayout) a(R.id.profileSubscriptionsContainer);
            kotlin.g.c b2 = kotlin.g.d.b(0, linearLayout.getChildCount());
            ArrayList arrayList2 = new ArrayList();
            Iterator it = b2.iterator();
            while (it.hasNext()) {
                View childAt = linearLayout.getChildAt(((ah) it).a());
                j.a((Object) childAt, "it");
                Object tag = childAt.getTag();
                if (!(tag instanceof m.b)) {
                    tag = null;
                }
                m.b bVar3 = (m.b) tag;
                if (!j.a((Object) (bVar3 != null ? bVar3.f133a : null), (Object) bVar.f133a)) {
                    childAt = null;
                }
                if (childAt != null) {
                    arrayList2.add(childAt);
                }
            }
            View view = (View) kotlin.a.m.d((List) arrayList2);
            if (view != null) {
                a(view, bVar);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.Checkbox, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final View a(View view, m.b bVar) {
        view.setTag(bVar);
        TextView textView = (TextView) view.findViewById(R.id.titleTextView);
        j.a((Object) textView, "titleTextView");
        textView.setText(bVar.f134b);
        Checkbox checkbox = (Checkbox) view.findViewById(R.id.consentCheckBox);
        j.a((Object) checkbox, "consentCheckBox");
        checkbox.setChecked(bVar.c);
        ((Checkbox) view.findViewById(R.id.consentCheckBox)).setOnClickListener(new d(view, this, bVar));
        return view;
    }
}
