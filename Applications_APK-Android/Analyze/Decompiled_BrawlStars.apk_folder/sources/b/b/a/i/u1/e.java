package b.b.a.i.u1;

import android.support.v4.app.Fragment;
import android.view.View;
import b.b.a.c.b;
import b.b.a.i.o1.p;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;

public final class e implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ b f735a;

    public e(b bVar) {
        this.f735a = bVar;
    }

    public final void onClick(View view) {
        b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Saved Credentials", "click", "Log in with other ID", null, false, 24);
        MainActivity a2 = b.b.a.b.a((Fragment) this.f735a);
        if (a2 != null) {
            a2.a(new p.a(null, null, null, 7));
        }
    }
}
