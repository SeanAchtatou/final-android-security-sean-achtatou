package b.b.a.i.x1;

import android.text.SpannableStringBuilder;
import b.b.a.b;
import b.b.a.i.x1.j;
import kotlin.d.a.c;
import kotlin.d.b.k;
import kotlin.m;

public final class l extends k implements c<String, SpannableStringBuilder, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ j.e f958a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l(j.e eVar) {
        super(2);
        this.f958a = eVar;
    }

    public final Object invoke(Object obj, Object obj2) {
        String str = (String) obj;
        SpannableStringBuilder spannableStringBuilder = (SpannableStringBuilder) obj2;
        kotlin.d.b.j.b(str, "text");
        kotlin.d.b.j.b(spannableStringBuilder, "result");
        j.e eVar = this.f958a;
        b.a(spannableStringBuilder, str, eVar.f955a, eVar.f956b);
        return m.f5330a;
    }
}
