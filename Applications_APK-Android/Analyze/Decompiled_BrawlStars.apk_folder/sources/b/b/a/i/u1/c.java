package b.b.a.i.u1;

import android.support.v4.app.Fragment;
import android.view.View;
import b.b.a.c.b;
import b.b.a.i.e;
import b.b.a.i.o1.p;
import com.supercell.id.IdAccount;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import kotlin.m;

public final class c implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ IdAccount f731a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ b f732b;

    public c(IdAccount idAccount, b bVar) {
        this.f731a = idAccount;
        this.f732b = bVar;
    }

    public final void onClick(View view) {
        b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Saved Credentials", "click", "Login with saved credentials", null, false, 24);
        String error = this.f731a.getError();
        boolean z = false;
        if (error == null || error.length() == 0) {
            if (this.f731a.getScidToken().length() > 0) {
                z = true;
            }
            if (z) {
                SupercellId.INSTANCE.loadAccount$supercellId_release(this.f731a.getEmail(), this.f731a.getPhone(), this.f731a.getScidToken(), true);
                MainActivity a2 = b.b.a.b.a((Fragment) this.f732b);
                if (a2 != null) {
                    a2.e();
                    return;
                }
                return;
            }
        }
        MainActivity a3 = b.b.a.b.a((Fragment) this.f732b);
        if (a3 != null) {
            a3.a("login_expired", (kotlin.d.a.b<? super e, m>) null);
        }
        MainActivity a4 = b.b.a.b.a((Fragment) this.f732b);
        if (a4 != null) {
            a4.a(new p.a(null, this.f731a.getEmail(), this.f731a.getPhone(), 1));
        }
    }
}
