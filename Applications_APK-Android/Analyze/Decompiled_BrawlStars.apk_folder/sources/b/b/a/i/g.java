package b.b.a.i;

import android.animation.ValueAnimator;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewPropertyAnimator;
import b.b.a.j.q1;
import com.supercell.id.R;
import com.supercell.id.ui.MainActivity;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.a.ab;
import kotlin.d.b.j;
import kotlin.d.b.t;
import kotlin.k;
import kotlin.m;
import nl.komponents.kovenant.ap;
import nl.komponents.kovenant.bb;
import nl.komponents.kovenant.bw;

public abstract class g extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    public kotlin.h<? extends a, Boolean> f245a = k.a(a.NONE, true);

    /* renamed from: b  reason: collision with root package name */
    public float f246b;
    public float c = 1.0f;
    public final View d;
    public final List<View> e = ab.f5210a;
    public final float f = 1.0f;
    public final NestedScrollView g;
    public final RecyclerView h;
    public final View i;
    public final View j;
    public ValueAnimator k;
    public HashMap l;

    public enum a {
        NONE,
        SLIDE_IN,
        FADE_IN,
        ENTRY,
        PAGE_CHANGED
    }

    public enum b {
        SLIDE_OUT,
        FADE_OUT,
        EXIT
    }

    public static final class c extends Exception {
    }

    public final class d extends kotlin.d.b.k implements kotlin.d.a.a<m> {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ a f252b;
        public final /* synthetic */ boolean c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(a aVar, boolean z) {
            super(0);
            this.f252b = aVar;
            this.c = z;
        }

        public final Object invoke() {
            g.this.f245a = k.a(this.f252b, Boolean.valueOf(this.c));
            return m.f5330a;
        }
    }

    public final /* synthetic */ class e extends kotlin.d.b.h implements kotlin.d.a.b<View, m> {
        public e(g gVar) {
            super(1, gVar);
        }

        public final String getName() {
            return "handleBackAction";
        }

        public final kotlin.h.d getOwner() {
            return t.a(g.class);
        }

        public final String getSignature() {
            return "handleBackAction(Landroid/view/View;)V";
        }

        public final Object invoke(Object obj) {
            View view = (View) obj;
            j.b(view, "p1");
            ((g) this.receiver).a(view);
            return m.f5330a;
        }
    }

    public final /* synthetic */ class f extends kotlin.d.b.h implements kotlin.d.a.b<View, m> {
        public f(g gVar) {
            super(1, gVar);
        }

        public final String getName() {
            return "handleCloseAction";
        }

        public final kotlin.h.d getOwner() {
            return t.a(g.class);
        }

        public final String getSignature() {
            return "handleCloseAction(Landroid/view/View;)V";
        }

        public final Object invoke(Object obj) {
            View view = (View) obj;
            j.b(view, "p1");
            ((g) this.receiver).b(view);
            return m.f5330a;
        }
    }

    /* renamed from: b.b.a.i.g$g  reason: collision with other inner class name */
    public final class C0019g implements NestedScrollView.OnScrollChangeListener {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ View f253a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ g f254b;

        public C0019g(View view, g gVar, boolean z) {
            this.f253a = view;
            this.f254b = gVar;
        }

        public final void onScrollChange(NestedScrollView nestedScrollView, int i, int i2, int i3, int i4) {
            this.f254b.a(this.f253a, i2, i2 - i4);
        }
    }

    public final class h extends RecyclerView.OnScrollListener {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ RecyclerView f255a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ View f256b;
        public final /* synthetic */ g c;

        public h(RecyclerView recyclerView, View view, g gVar, boolean z) {
            this.f255a = recyclerView;
            this.f256b = view;
            this.c = gVar;
        }

        public final void onScrolled(RecyclerView recyclerView, int i, int i2) {
            j.b(recyclerView, "recyclerView");
            g gVar = this.c;
            gVar.a(this.f256b, b.b.a.b.a(this.f255a, kotlin.e.a.a(gVar.g())), i2);
        }
    }

    public final class i implements ValueAnimator.AnimatorUpdateListener {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ ValueAnimator f257a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ g f258b;
        public final /* synthetic */ View c;
        public final /* synthetic */ float d;
        public final /* synthetic */ float e;

        public i(ValueAnimator valueAnimator, g gVar, View view, float f, float f2) {
            this.f257a = valueAnimator;
            this.f258b = gVar;
            this.c = view;
            this.d = f;
            this.e = f2;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.animation.ValueAnimator, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            this.f257a.setDuration(200L);
            this.f257a.setInterpolator(b.b.a.f.a.f62a);
            g gVar = this.f258b;
            j.a((Object) valueAnimator, "it");
            Object animatedValue = valueAnimator.getAnimatedValue();
            if (animatedValue != null) {
                gVar.f246b = ((Float) animatedValue).floatValue();
                this.c.setTranslationY(this.f258b.f246b);
                g gVar2 = this.f258b;
                float f = this.d;
                gVar2.b(((this.e - f) * valueAnimator.getAnimatedFraction()) + f);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Float");
        }
    }

    public static /* synthetic */ void a(g gVar, boolean z, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 1) != 0) {
                z = true;
            }
            gVar.a(z);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: scrollViewChanged");
    }

    public final float a(float f2) {
        float g2 = f2 / g();
        if (Float.compare(g2, 0.0f) < 0) {
            return 0.0f;
        }
        if (Float.compare(g2, 1.0f) > 0) {
            return 1.0f;
        }
        return g2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final bw<Boolean, Exception> a(b bVar, boolean z) {
        j.b(bVar, "animation");
        ap a2 = bb.f5389a;
        View view = getView();
        if (view != null) {
            j.a((Object) view, "this");
            a(view, bVar, z, a2);
        } else {
            a2.f(new c());
            m mVar = m.f5330a;
        }
        return a2.i();
    }

    public void a() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public void a(View view) {
        j.b(view, "view");
        MainActivity a2 = b.b.a.b.a((Fragment) this);
        if (a2 != null) {
            a2.d();
        }
    }

    public final void a(View view, int i2) {
        ValueAnimator valueAnimator = this.k;
        if (valueAnimator == null || !valueAnimator.isStarted()) {
            ValueAnimator valueAnimator2 = this.k;
            if (valueAnimator2 != null) {
                valueAnimator2.cancel();
            }
            this.k = null;
            float f2 = this.c;
            float a2 = a(((float) i2) + 0.0f);
            ValueAnimator ofFloat = ValueAnimator.ofFloat(view.getTranslationY(), 0.0f);
            ofFloat.addUpdateListener(new i(ofFloat, this, view, f2, a2));
            ofFloat.start();
            this.k = ofFloat;
        }
    }

    public final void a(View view, int i2, int i3) {
        ValueAnimator valueAnimator = this.k;
        if (valueAnimator != null) {
            valueAnimator.cancel();
        }
        this.k = null;
        int height = ViewCompat.isLaidOut(view) ? view.getHeight() : -kotlin.e.a.a(this.f246b);
        float f2 = this.f246b - ((float) i3);
        float f3 = -((float) Math.min(i2, height));
        if (Float.compare(f2, f3) >= 0) {
            f3 = Float.compare(f2, 0.0f) > 0 ? 0.0f : f2;
        }
        this.f246b = f3;
        view.setTranslationY(this.f246b);
        b(a(((float) i2) + this.f246b));
    }

    public void a(View view, a aVar, boolean z) {
        j.b(view, "view");
        j.b(aVar, "animation");
        int i2 = h.f278a[aVar.ordinal()];
        if (i2 != 1) {
            if (i2 == 2) {
                q1.a(view, new i(view, this));
            } else if (i2 == 3 || i2 == 4 || i2 == 5) {
                view.setAlpha(1.0f);
            }
        } else if (z) {
            q1.a(view, new o(view, this));
        } else {
            q1.a(view, new l(view, this));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void a(a aVar, boolean z) {
        j.b(aVar, "animation");
        View view = getView();
        if (view != null) {
            j.a((Object) view, "this");
            a(view, aVar, z);
            return;
        }
        new d(aVar, z).invoke();
    }

    public final void a(boolean z) {
        View f2 = f();
        if (f2 != null) {
            NestedScrollView d2 = d();
            if (d2 != null) {
                d2.setOnScrollChangeListener(new C0019g(f2, this, z));
                if (z) {
                    a(f2, d2.getScrollY());
                }
            }
            RecyclerView e2 = e();
            if (e2 != null) {
                e2.addOnScrollListener(new h(e2, f2, this, z));
                if (z) {
                    a(f2, b.b.a.b.a(e2, kotlin.e.a.a(g())));
                }
            }
        }
    }

    public View b() {
        return this.i;
    }

    public final void b(float f2) {
        this.c = f2;
        for (View alpha : h()) {
            alpha.setAlpha(f2);
        }
    }

    public void b(View view) {
        j.b(view, "view");
        MainActivity a2 = b.b.a.b.a((Fragment) this);
        if (a2 != null) {
            a2.e();
        }
    }

    public View c() {
        return this.j;
    }

    public NestedScrollView d() {
        return this.g;
    }

    public RecyclerView e() {
        return this.h;
    }

    public View f() {
        return this.d;
    }

    public float g() {
        return this.f;
    }

    public List<View> h() {
        return this.e;
    }

    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    public void onSaveInstanceState(Bundle bundle) {
        j.b(bundle, "outState");
        float f2 = this.f246b;
        if (f2 != 0.0f) {
            bundle.putFloat("toolbarTranslationY", f2);
        }
        super.onSaveInstanceState(bundle);
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0046  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onViewStateRestored(android.os.Bundle r4) {
        /*
            r3 = this;
            super.onViewStateRestored(r4)
            android.view.View r0 = r3.f()
            if (r0 == 0) goto L_0x0054
            if (r4 == 0) goto L_0x0013
            r1 = 0
            java.lang.String r2 = "toolbarTranslationY"
            float r4 = r4.getFloat(r2, r1)
            goto L_0x0015
        L_0x0013:
            float r4 = r3.f246b
        L_0x0015:
            r3.f246b = r4
            float r4 = r3.f246b
            r0.setTranslationY(r4)
            android.support.v4.widget.NestedScrollView r4 = r3.d()
            if (r4 == 0) goto L_0x002b
            int r4 = r4.getScrollY()
        L_0x0026:
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            goto L_0x003f
        L_0x002b:
            android.support.v7.widget.RecyclerView r4 = r3.e()
            if (r4 == 0) goto L_0x003e
            float r1 = r3.g()
            int r1 = kotlin.e.a.a(r1)
            int r4 = b.b.a.b.a(r4, r1)
            goto L_0x0026
        L_0x003e:
            r4 = 0
        L_0x003f:
            if (r4 == 0) goto L_0x0046
            int r4 = r4.intValue()
            goto L_0x0047
        L_0x0046:
            r4 = 0
        L_0x0047:
            float r4 = (float) r4
            float r0 = r0.getTranslationY()
            float r0 = r0 + r4
            float r4 = r3.a(r0)
            r3.b(r4)
        L_0x0054:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.i.g.onViewStateRestored(android.os.Bundle):void");
    }

    public void a(View view, b bVar, boolean z, ap<Boolean, Exception> apVar) {
        View a2;
        j.b(view, "view");
        j.b(bVar, "animation");
        j.b(apVar, "result");
        int i2 = h.f279b[bVar.ordinal()];
        boolean z2 = true;
        if (i2 == 1) {
            float f2 = -1.0f;
            if (z) {
                ViewPropertyAnimator interpolator = view.animate().setStartDelay(0).setDuration(350).setInterpolator(b.b.a.f.a.f62a);
                if (ViewCompat.getLayoutDirection(view) != 1) {
                    z2 = false;
                }
                if (!z2) {
                    f2 = 1.0f;
                }
                ViewPropertyAnimator listener = interpolator.translationX(f2 * -0.5f * ((float) view.getWidth())).setListener(new q(apVar));
                if (Build.VERSION.SDK_INT >= 21) {
                    listener.setUpdateListener(null);
                }
                listener.start();
                return;
            }
            MainActivity a3 = b.b.a.b.a((Fragment) this);
            if (!(a3 == null || (a2 = a3.a(R.id.bottom_area_dimmer)) == null)) {
                a2.setVisibility(0);
                a2.bringToFront();
                if (a2.getAlpha() == 0.0f) {
                    a2.setAlpha(1.0f);
                }
                a2.animate().setStartDelay(0).setDuration(350).setInterpolator(b.b.a.f.a.f62a).alpha(0.0f);
            }
            view.bringToFront();
            if (Build.VERSION.SDK_INT >= 21 && view.getElevation() == 0.0f) {
                view.setElevation(8.0f);
            }
            ViewPropertyAnimator interpolator2 = view.animate().setStartDelay(0).setDuration(350).setInterpolator(b.b.a.f.a.f62a);
            if (ViewCompat.getLayoutDirection(view) != 1) {
                z2 = false;
            }
            if (!z2) {
                f2 = 1.0f;
            }
            ViewPropertyAnimator listener2 = interpolator2.translationX(f2 * ((float) view.getWidth())).setListener(new p(view, this, apVar));
            if (Build.VERSION.SDK_INT >= 21) {
                listener2.setUpdateListener(new f(view));
            }
            listener2.start();
        } else if (i2 == 2) {
            ViewPropertyAnimator listener3 = view.animate().setStartDelay(0).setDuration(175).setInterpolator(b.b.a.f.a.h).alpha(0.0f).setListener(new j(apVar));
            if (Build.VERSION.SDK_INT >= 21) {
                listener3.setUpdateListener(null);
            }
            listener3.start();
        }
    }

    public void onViewCreated(View view, Bundle bundle) {
        j.b(view, "view");
        super.onViewCreated(view, bundle);
        View b2 = b();
        if (b2 != null) {
            MainActivity a2 = b.b.a.b.a((Fragment) this);
            b2.setVisibility((a2 != null ? a2.b() : 0) > 1 ? 0 : 4);
        }
        View b3 = b();
        if (b3 != null) {
            b3.setOnClickListener(new k(new e(this)));
        }
        View c2 = c();
        if (c2 != null) {
            c2.setOnClickListener(new k(new f(this)));
        }
        view.setAlpha(0.0f);
        kotlin.h<? extends a, Boolean> hVar = this.f245a;
        a(view, (a) hVar.f5262a, ((Boolean) hVar.f5263b).booleanValue());
        this.f245a = k.a(a.NONE, true);
        a(false);
    }
}
