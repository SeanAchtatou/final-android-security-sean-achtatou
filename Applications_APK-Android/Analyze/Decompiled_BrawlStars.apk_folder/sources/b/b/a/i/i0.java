package b.b.a.i;

import android.animation.ValueAnimator;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import b.b.a.j.q1;
import com.supercell.id.R;
import kotlin.d.b.j;
import kotlin.e.a;

public final class i0 implements ValueAnimator.AnimatorUpdateListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ ValueAnimator f292a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ int f293b;
    public final /* synthetic */ m0 c;

    public i0(ValueAnimator valueAnimator, int i, m0 m0Var) {
        this.f292a = valueAnimator;
        this.f293b = i;
        this.c = m0Var;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.FrameLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        FrameLayout frameLayout = (FrameLayout) this.c.f376a.a(R.id.content);
        j.a((Object) frameLayout, "content");
        ViewGroup.MarginLayoutParams d = q1.d(frameLayout);
        if (d != null) {
            int i = this.f293b;
            d.leftMargin = a.a((((float) (this.c.f376a.l() - i)) * this.f292a.getAnimatedFraction()) + ((float) i));
        }
        ((FrameLayout) this.c.f376a.a(R.id.content)).requestLayout();
    }
}
