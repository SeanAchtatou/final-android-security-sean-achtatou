package b.b.a.i.q1;

import b.b.a.h.l;
import java.lang.ref.WeakReference;
import kotlin.d.a.b;
import kotlin.d.b.k;
import kotlin.m;

public final class p extends k implements b<l, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f577a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public p(WeakReference weakReference) {
        super(1);
        this.f577a = weakReference;
    }

    public final Object invoke(Object obj) {
        a f;
        Object obj2 = this.f577a.get();
        if (!(obj2 == null || (f = ((o) obj2).f()) == null)) {
            f.k();
        }
        return m.f5330a;
    }
}
