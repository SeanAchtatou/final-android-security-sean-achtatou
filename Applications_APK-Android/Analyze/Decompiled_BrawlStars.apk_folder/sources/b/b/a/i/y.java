package b.b.a.i;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.animation.SpringAnimation;
import android.support.animation.SpringForce;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import b.b.a.j.q1;
import com.supercell.id.R;
import java.util.HashMap;
import kotlin.d.b.g;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public class y extends e {
    public static final a h = new a(null);
    public final long e = 70;
    public final boolean f = true;
    public HashMap g;

    public static final class a {
        public /* synthetic */ a(g gVar) {
        }

        public final y a(Rect rect, int i, boolean z) {
            j.b(rect, "rect");
            y yVar = new y();
            Bundle arguments = yVar.getArguments();
            if (arguments == null) {
                arguments = new Bundle();
            }
            arguments.putParcelable("rect", rect);
            arguments.putInt("layout", i);
            arguments.putBoolean("side", z);
            yVar.setArguments(arguments);
            return yVar;
        }
    }

    public final class b extends k implements kotlin.d.a.a<m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ FrameLayout f959a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ y f960b;
        public final /* synthetic */ boolean c;
        public final /* synthetic */ View d;
        public final /* synthetic */ Rect e;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(FrameLayout frameLayout, y yVar, boolean z, View view, Rect rect) {
            super(0);
            this.f959a = frameLayout;
            this.f960b = yVar;
            this.c = z;
            this.d = view;
            this.e = rect;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.FrameLayout, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.ImageView, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.support.animation.SpringForce, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final Object invoke() {
            int i;
            int i2;
            int i3;
            FrameLayout frameLayout;
            int i4;
            float f;
            if (this.f960b.isAdded()) {
                FrameLayout frameLayout2 = this.f959a;
                j.a((Object) frameLayout2, "it");
                ViewGroup.LayoutParams layoutParams = frameLayout2.getLayoutParams();
                if (!(layoutParams instanceof FrameLayout.LayoutParams)) {
                    layoutParams = null;
                }
                FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) layoutParams;
                boolean z = false;
                if (layoutParams2 != null) {
                    layoutParams2.gravity = ((ViewCompat.getLayoutDirection(this.f959a) == 1) == (this.c ^ true) ? 3 : 5) | 16;
                }
                ImageView imageView = (ImageView) this.f960b.a(R.id.dialogTailStart);
                j.a((Object) imageView, "dialogTailStart");
                int i5 = 8;
                imageView.setVisibility(this.c ? 0 : 8);
                ImageView imageView2 = (ImageView) this.f960b.a(R.id.dialogTail);
                j.a((Object) imageView2, "dialogTail");
                if (!this.c) {
                    i5 = 0;
                }
                imageView2.setVisibility(i5);
                q1.g(this.d);
                Rect b2 = q1.b(this.d);
                FrameLayout frameLayout3 = this.f959a;
                j.a((Object) frameLayout3, "it");
                Rect b3 = q1.b(frameLayout3);
                if ((ViewCompat.getLayoutDirection(this.f959a) == 1) == (!this.c)) {
                    i4 = this.e.right - b3.left;
                    frameLayout = this.f959a;
                    j.a((Object) frameLayout, "it");
                    i3 = frameLayout.getPaddingLeft() + i4;
                    FrameLayout frameLayout4 = this.f959a;
                    j.a((Object) frameLayout4, "it");
                    i2 = frameLayout4.getPaddingTop();
                    FrameLayout frameLayout5 = this.f959a;
                    j.a((Object) frameLayout5, "it");
                    i = frameLayout5.getPaddingRight();
                } else {
                    i4 = b3.right - this.e.left;
                    frameLayout = this.f959a;
                    j.a((Object) frameLayout, "it");
                    i3 = frameLayout.getPaddingLeft();
                    FrameLayout frameLayout6 = this.f959a;
                    j.a((Object) frameLayout6, "it");
                    i2 = frameLayout6.getPaddingTop();
                    FrameLayout frameLayout7 = this.f959a;
                    j.a((Object) frameLayout7, "it");
                    i = frameLayout7.getPaddingRight() + i4;
                }
                FrameLayout frameLayout8 = this.f959a;
                j.a((Object) frameLayout8, "it");
                frameLayout.setPadding(i3, i2, i, frameLayout8.getPaddingBottom());
                q1.g(this.d);
                FrameLayout frameLayout9 = this.f959a;
                j.a((Object) frameLayout9, "it");
                if (ViewCompat.getLayoutDirection(this.f959a) == 1) {
                    z = true;
                }
                if (z == (true ^ this.c)) {
                    f = (float) i4;
                } else {
                    FrameLayout frameLayout10 = this.f959a;
                    j.a((Object) frameLayout10, "it");
                    f = (float) (frameLayout10.getWidth() - i4);
                }
                frameLayout9.setPivotX(f);
                FrameLayout frameLayout11 = this.f959a;
                j.a((Object) frameLayout11, "it");
                FrameLayout frameLayout12 = this.f959a;
                j.a((Object) frameLayout12, "it");
                frameLayout11.setPivotY(((float) frameLayout12.getHeight()) * 0.5f);
                float exactCenterY = this.e.exactCenterY() - b3.exactCenterY();
                int i6 = b2.bottom - b3.bottom;
                FrameLayout frameLayout13 = this.f959a;
                j.a((Object) frameLayout13, "it");
                float f2 = (float) (b2.top - b3.top);
                float f3 = (float) i6;
                if (Float.compare(exactCenterY, f2) < 0) {
                    f3 = f2;
                } else if (Float.compare(exactCenterY, f3) <= 0) {
                    f3 = exactCenterY;
                }
                frameLayout13.setTranslationY(f3);
                ImageView imageView3 = (ImageView) this.f960b.a(R.id.dialogTail);
                if (imageView3 != null) {
                    FrameLayout frameLayout14 = this.f959a;
                    j.a((Object) frameLayout14, "it");
                    imageView3.setTranslationY(exactCenterY - frameLayout14.getTranslationY());
                }
                ImageView imageView4 = (ImageView) this.f960b.a(R.id.dialogTailStart);
                if (imageView4 != null) {
                    FrameLayout frameLayout15 = this.f959a;
                    j.a((Object) frameLayout15, "it");
                    imageView4.setTranslationY(exactCenterY - frameLayout15.getTranslationY());
                }
                FrameLayout frameLayout16 = this.f959a;
                j.a((Object) frameLayout16, "it");
                frameLayout16.setScaleX(0.7f);
                FrameLayout frameLayout17 = this.f959a;
                j.a((Object) frameLayout17, "it");
                frameLayout17.setScaleY(0.7f);
                SpringAnimation springAnimation = new SpringAnimation(this.f959a, SpringAnimation.SCALE_X, 1.0f);
                SpringForce spring = springAnimation.getSpring();
                j.a((Object) spring, "spring");
                spring.setDampingRatio(0.5f);
                SpringForce spring2 = springAnimation.getSpring();
                j.a((Object) spring2, "spring");
                spring2.setStiffness(600.0f);
                springAnimation.start();
                SpringAnimation springAnimation2 = new SpringAnimation(this.f959a, SpringAnimation.SCALE_Y, 1.0f);
                SpringForce spring3 = springAnimation2.getSpring();
                j.a((Object) spring3, "spring");
                spring3.setDampingRatio(0.5f);
                SpringForce spring4 = springAnimation2.getSpring();
                j.a((Object) spring4, "spring");
                spring4.setStiffness(600.0f);
                springAnimation2.start();
                FrameLayout frameLayout18 = this.f959a;
                j.a((Object) frameLayout18, "it");
                frameLayout18.setAlpha(1.0f);
            }
            return m.f5330a;
        }
    }

    public final class c implements View.OnClickListener {
        public c() {
        }

        public final void onClick(View view) {
            y.this.b();
        }
    }

    public View a(int i) {
        if (this.g == null) {
            this.g = new HashMap();
        }
        View view = (View) this.g.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.g.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public void a() {
        HashMap hashMap = this.g;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public void b() {
        ViewPropertyAnimator animate;
        super.b();
        FrameLayout frameLayout = (FrameLayout) a(R.id.dialogContainer);
        if (frameLayout != null && (animate = frameLayout.animate()) != null) {
            animate.setDuration(c());
            animate.setInterpolator(b.b.a.f.a.c);
            animate.scaleX(0.7f);
            animate.scaleY(0.7f);
            animate.start();
        }
    }

    public long c() {
        return this.e;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(1, R.style.SupercellIdInfoDialogTheme);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.FrameLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        View inflate = layoutInflater.inflate(R.layout.fragment_info_dialog, viewGroup, false);
        Bundle arguments = getArguments();
        if (arguments != null) {
            int i = arguments.getInt("layout");
            j.a((Object) inflate, "view");
            layoutInflater.inflate(i, (ViewGroup) ((FrameLayout) inflate.findViewById(R.id.info_dialog_content)), true);
        }
        return inflate;
    }

    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.FrameLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public void onViewCreated(View view, Bundle bundle) {
        j.b(view, "view");
        super.onViewCreated(view, bundle);
        if (this.f) {
            view.setSoundEffectsEnabled(false);
            view.setOnClickListener(new c());
        }
        Bundle arguments = getArguments();
        Rect rect = arguments != null ? (Rect) arguments.getParcelable("rect") : null;
        if (!(rect instanceof Rect)) {
            rect = null;
        }
        if (rect == null) {
            rect = new Rect();
        }
        Rect rect2 = rect;
        Bundle arguments2 = getArguments();
        boolean z = arguments2 != null ? arguments2.getBoolean("side") : false;
        FrameLayout frameLayout = (FrameLayout) a(R.id.dialogContainer);
        j.a((Object) frameLayout, "it");
        frameLayout.setAlpha(0.0f);
        q1.a(frameLayout, new b(frameLayout, this, z, view, rect2));
    }
}
