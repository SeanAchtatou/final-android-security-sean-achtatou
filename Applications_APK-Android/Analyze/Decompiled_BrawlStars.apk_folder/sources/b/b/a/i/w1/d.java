package b.b.a.i.w1;

import com.facebook.internal.ServerProtocol;
import com.supercell.id.SupercellId;
import java.lang.ref.WeakReference;
import kotlin.d.a.b;
import kotlin.d.b.k;
import kotlin.m;

public final class d extends k implements b<Boolean, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f835a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ String f836b;
    public final /* synthetic */ boolean c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d(WeakReference weakReference, String str, boolean z) {
        super(1);
        this.f835a = weakReference;
        this.f836b = str;
        this.c = z;
    }

    public final Object invoke(Object obj) {
        ((Boolean) obj).booleanValue();
        f fVar = (f) this.f835a.get();
        if (fVar != null) {
            SupercellId.INSTANCE.setPendingRegistrationWithEmail$supercellId_release(this.f836b, this.c);
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Accept marketing", "Selection", this.c ? ServerProtocol.DIALOG_RETURN_SCOPES_TRUE : "false", null, false, 24);
            fVar.b(this.f836b);
            n i = fVar.i();
            if (i != null) {
                i.k();
            }
        }
        return m.f5330a;
    }
}
