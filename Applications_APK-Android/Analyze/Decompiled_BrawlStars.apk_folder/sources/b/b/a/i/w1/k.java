package b.b.a.i.w1;

import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.AppCompatButton;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import b.b.a.i.u;
import b.b.a.i.v;
import b.b.a.j.n;
import b.b.a.j.q1;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.g;
import com.google.i18n.phonenumbers.j;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.PinEntryView;
import com.supercell.id.view.WidthAdjustingMultilineButton;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import kotlin.d.b.j;
import kotlin.h;
import kotlin.m;

public final class k extends o implements u {
    public HashMap c;

    public final class a extends kotlin.d.b.k implements kotlin.d.a.b<String, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f881a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(WeakReference weakReference) {
            super(1);
            this.f881a = weakReference;
        }

        public final Object invoke(Object obj) {
            j.b((String) obj, "it");
            k kVar = (k) this.f881a.get();
            if (kVar != null) {
                SupercellId.INSTANCE.clearPendingRegistration$supercellId_release();
                n i = kVar.i();
                if (i != null) {
                    i.k();
                }
            }
            return m.f5330a;
        }
    }

    public final class b extends kotlin.d.b.k implements kotlin.d.a.b<String, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f882a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(WeakReference weakReference) {
            super(1);
            this.f882a = weakReference;
        }

        public final Object invoke(Object obj) {
            MainActivity a2;
            String str = (String) obj;
            j.b(str, "it");
            k kVar = (k) this.f882a.get();
            if (!(kVar == null || (a2 = b.b.a.b.a(kVar)) == null)) {
                a2.a("gameclient_error_" + str, (kotlin.d.a.b<? super b.b.a.i.e, m>) null);
            }
            return m.f5330a;
        }
    }

    public final class c implements View.OnClickListener {
        public c() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) k.this.a(R.id.okButton);
            j.a((Object) widthAdjustingMultilineButton, "okButton");
            widthAdjustingMultilineButton.setEnabled(false);
            WidthAdjustingMultilineButton widthAdjustingMultilineButton2 = (WidthAdjustingMultilineButton) k.this.a(R.id.cancelButton);
            j.a((Object) widthAdjustingMultilineButton2, "cancelButton");
            widthAdjustingMultilineButton2.setEnabled(false);
            SupercellId.INSTANCE.clearPendingRegistration$supercellId_release();
            MainActivity a2 = b.b.a.b.a(k.this);
            if (a2 != null) {
                a2.d();
            }
        }
    }

    public final class d implements View.OnClickListener {
        public d() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) k.this.a(R.id.okButton);
            j.a((Object) widthAdjustingMultilineButton, "okButton");
            widthAdjustingMultilineButton.setEnabled(false);
            WidthAdjustingMultilineButton widthAdjustingMultilineButton2 = (WidthAdjustingMultilineButton) k.this.a(R.id.cancelButton);
            j.a((Object) widthAdjustingMultilineButton2, "cancelButton");
            widthAdjustingMultilineButton2.setEnabled(false);
            k kVar = k.this;
            String g = kVar.g();
            String h = kVar.h();
            String obj = ((PinEntryView) kVar.a(R.id.pinEditText)).getText().toString();
            if (!((PinEntryView) kVar.a(R.id.pinEditText)).a()) {
                MainActivity a2 = b.b.a.b.a(kVar);
                if (a2 != null) {
                    a2.a("invalid_pin", (kotlin.d.a.b<? super b.b.a.i.e, m>) null);
                    return;
                }
                return;
            }
            WeakReference weakReference = new WeakReference(kVar);
            nl.komponents.kovenant.c.m.b(nl.komponents.kovenant.c.m.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().g.a(g, h, obj), new l(weakReference)), new m(weakReference));
        }
    }

    public final class e extends kotlin.d.b.k implements kotlin.d.a.c<PinEntryView, CharSequence, m> {
        public e() {
            super(2);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: b.b.a.j.q1.a(android.support.v7.widget.AppCompatButton, boolean):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, boolean]
         candidates:
          b.b.a.j.q1.a(android.view.View, int):android.view.View
          b.b.a.j.q1.a(android.support.v4.widget.NestedScrollView, int):void
          b.b.a.j.q1.a(android.support.v4.widget.NestedScrollView, android.view.View):void
          b.b.a.j.q1.a(android.view.View, long):void
          b.b.a.j.q1.a(android.view.View, b.b.a.j.z):void
          b.b.a.j.q1.a(android.view.View, kotlin.d.a.a<kotlin.m>):void
          b.b.a.j.q1.a(android.widget.ScrollView, int):void
          b.b.a.j.q1.a(android.widget.ScrollView, android.view.View):void
          b.b.a.j.q1.a(android.support.v7.widget.AppCompatButton, boolean):void */
        public final Object invoke(Object obj, Object obj2) {
            PinEntryView pinEntryView = (PinEntryView) obj;
            j.b(pinEntryView, "pinEditText");
            j.b((CharSequence) obj2, "<anonymous parameter 1>");
            WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) k.this.a(R.id.okButton);
            j.a((Object) widthAdjustingMultilineButton, "okButton");
            q1.a((AppCompatButton) widthAdjustingMultilineButton, !pinEntryView.a());
            return m.f5330a;
        }
    }

    public final View a(int i) {
        if (this.c == null) {
            this.c = new HashMap();
        }
        View view = (View) this.c.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.c.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.c;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void a(v vVar) {
        j.b(vVar, "dialog");
        WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) a(R.id.okButton);
        j.a((Object) widthAdjustingMultilineButton, "okButton");
        widthAdjustingMultilineButton.setEnabled(true);
        WidthAdjustingMultilineButton widthAdjustingMultilineButton2 = (WidthAdjustingMultilineButton) a(R.id.cancelButton);
        j.a((Object) widthAdjustingMultilineButton2, "cancelButton");
        widthAdjustingMultilineButton2.setEnabled(true);
    }

    public final void a(String str, String str2) {
        WeakReference weakReference = new WeakReference(this);
        nl.komponents.kovenant.c.m.b(nl.komponents.kovenant.c.m.a(SupercellId.INSTANCE.bindAccount$supercellId_release(str, str2, g(), h(), true), new a(weakReference)), new b(weakReference));
    }

    public final void c() {
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a("Register Progress Step 2");
    }

    public final void e() {
        j();
        PinEntryView pinEntryView = (PinEntryView) a(R.id.pinEditText);
        if (pinEntryView != null) {
            pinEntryView.setPin("");
        }
    }

    public final void j() {
        TextView textView;
        TextView textView2;
        String g = g();
        if (!(g == null || (textView2 = (TextView) a(R.id.subtitleTextView)) == null)) {
            b.b.a.i.x1.j.a(textView2, "register_pin_description", kotlin.k.a("email address", b.b.a.b.a(new SpannableStringBuilder(), n.f1102b.d(g), new ForegroundColorSpan(ResourcesCompat.getColor(getResources(), R.color.text_green, null)), 33)));
        }
        String h = h();
        if (h != null && (textView = (TextView) a(R.id.subtitleTextView)) != null) {
            h[] hVarArr = new h[1];
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
            j.b(h, "number");
            try {
                j.a a2 = g.a().a(h, "ZZ");
                h = 8234 + g.a().a(a2, g.a.INTERNATIONAL) + 8236;
            } catch (NumberParseException unused) {
            }
            hVarArr[0] = kotlin.k.a("phone number", b.b.a.b.a(spannableStringBuilder, h, new ForegroundColorSpan(ResourcesCompat.getColor(getResources(), R.color.text_green, null)), 33));
            b.b.a.i.x1.j.a(textView, "register_pin_description_phone", hVarArr);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kotlin.d.b.j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_register_enter_pin_page, viewGroup, false);
    }

    public final void onDestroyView() {
        super.onDestroyView();
        MainActivity a2 = b.b.a.b.a(this);
        if (a2 != null) {
            a2.b(this);
        }
        a();
    }

    public final void onResume() {
        super.onResume();
        PinEntryView pinEntryView = (PinEntryView) a(R.id.pinEditText);
        if (pinEntryView != null) {
            pinEntryView.b();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.j.q1.a(android.support.v7.widget.AppCompatButton, boolean):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, boolean]
     candidates:
      b.b.a.j.q1.a(android.view.View, int):android.view.View
      b.b.a.j.q1.a(android.support.v4.widget.NestedScrollView, int):void
      b.b.a.j.q1.a(android.support.v4.widget.NestedScrollView, android.view.View):void
      b.b.a.j.q1.a(android.view.View, long):void
      b.b.a.j.q1.a(android.view.View, b.b.a.j.z):void
      b.b.a.j.q1.a(android.view.View, kotlin.d.a.a<kotlin.m>):void
      b.b.a.j.q1.a(android.widget.ScrollView, int):void
      b.b.a.j.q1.a(android.widget.ScrollView, android.view.View):void
      b.b.a.j.q1.a(android.support.v7.widget.AppCompatButton, boolean):void */
    public final void onViewCreated(View view, Bundle bundle) {
        kotlin.d.b.j.b(view, "view");
        super.onViewCreated(view, bundle);
        MainActivity a2 = b.b.a.b.a(this);
        if (a2 != null) {
            a2.a(this);
        }
        j();
        ((WidthAdjustingMultilineButton) a(R.id.cancelButton)).setOnClickListener(new c());
        ((WidthAdjustingMultilineButton) a(R.id.okButton)).setOnClickListener(new d());
        WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) a(R.id.okButton);
        kotlin.d.b.j.a((Object) widthAdjustingMultilineButton, "okButton");
        q1.a((AppCompatButton) widthAdjustingMultilineButton, !((PinEntryView) a(R.id.pinEditText)).a());
        ((PinEntryView) a(R.id.pinEditText)).setOnPinChangedListener(new e());
    }
}
