package b.b.a.i.q1;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import b.b.a.i.q1.d;
import com.supercell.id.R;
import kotlin.d.a.b;
import kotlin.d.a.c;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class f extends k implements c<Drawable, b.b.a.i.x1.c, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ d.b.e f553a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ b f554b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public f(d.b.e eVar, b bVar) {
        super(2);
        this.f553a = eVar;
        this.f554b = bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.Context, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final Object invoke(Object obj, Object obj2) {
        Drawable drawable = (Drawable) obj;
        j.b(drawable, "drawable");
        j.b((b.b.a.i.x1.c) obj2, "<anonymous parameter 1>");
        Context context = d.b.this.f524b.get();
        if (context != null) {
            j.a((Object) context, "weakContext.get() ?: return@getDrawable");
            BitmapDrawable bitmapDrawable = null;
            if (!(drawable instanceof BitmapDrawable)) {
                drawable = null;
            }
            BitmapDrawable bitmapDrawable2 = (BitmapDrawable) drawable;
            if (bitmapDrawable2 != null) {
                bitmapDrawable = new BitmapDrawable(context.getResources(), bitmapDrawable2.getBitmap());
            }
            int dimensionPixelSize = context.getResources().getDimensionPixelSize(R.dimen.friend_list_game_icon_size);
            if (bitmapDrawable != null) {
                bitmapDrawable.setBounds(new Rect(0, 0, dimensionPixelSize, dimensionPixelSize));
            }
            if (bitmapDrawable != null) {
                this.f554b.invoke(bitmapDrawable);
            }
        }
        return m.f5330a;
    }
}
