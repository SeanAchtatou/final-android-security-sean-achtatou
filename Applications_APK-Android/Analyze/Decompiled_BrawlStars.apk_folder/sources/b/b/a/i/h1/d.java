package b.b.a.i.h1;

import android.graphics.Rect;
import android.support.v4.widget.NestedScrollView;
import android.view.View;
import kotlin.e.a;

public final class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ NestedScrollView f288a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ View f289b;
    public final /* synthetic */ int c;
    public final /* synthetic */ float d;

    public d(NestedScrollView nestedScrollView, View view, int i, float f) {
        this.f288a = nestedScrollView;
        this.f289b = view;
        this.c = i;
        this.d = f;
    }

    public final void run() {
        Rect rect = new Rect(0, 0, 0, this.f289b.getHeight());
        this.f288a.offsetDescendantRectToMyCoords(this.f289b, rect);
        int height = (rect.bottom + this.c) - this.f288a.getHeight();
        if (height > this.f288a.getScrollY()) {
            NestedScrollView nestedScrollView = this.f288a;
            int scrollY = nestedScrollView.getScrollY();
            nestedScrollView.scrollTo(0, a.a((((float) (height - scrollY)) * b.b.a.f.a.e.getInterpolation(this.d)) + ((float) scrollY)));
        }
    }
}
