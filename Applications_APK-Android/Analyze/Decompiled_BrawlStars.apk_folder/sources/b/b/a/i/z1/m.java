package b.b.a.i.z1;

import b.b.a.h.m;
import com.supercell.id.SupercellId;
import kotlin.d.a.a;
import kotlin.d.b.k;
import nl.komponents.kovenant.bw;

public final class m extends k implements a<bw<? extends m.b, ? extends Exception>> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ String f1008a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ boolean f1009b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m(String str, boolean z) {
        super(0);
        this.f1008a = str;
        this.f1009b = z;
    }

    public final Object invoke() {
        return SupercellId.INSTANCE.getSharedServices$supercellId_release().g.a(this.f1008a, this.f1009b);
    }
}
