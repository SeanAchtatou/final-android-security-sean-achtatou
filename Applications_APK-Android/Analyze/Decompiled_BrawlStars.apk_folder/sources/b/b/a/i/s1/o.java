package b.b.a.i.s1;

import java.lang.ref.WeakReference;
import kotlin.d.a.a;
import kotlin.d.b.k;
import kotlin.m;

public final class o extends k implements a<m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f684a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public o(WeakReference weakReference) {
        super(0);
        this.f684a = weakReference;
    }

    public final Object invoke() {
        p pVar = (p) this.f684a.get();
        if (pVar != null) {
            pVar.f();
        }
        return m.f5330a;
    }
}
