package b.b.a.i.s1;

import b.b.a.i.e;
import com.supercell.id.ui.MainActivity;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class l extends k implements b<Exception, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ p f680a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ String f681b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l(p pVar, String str) {
        super(1);
        this.f680a = pVar;
        this.f681b = str;
    }

    public final Object invoke(Object obj) {
        Exception exc = (Exception) obj;
        j.b(exc, "it");
        this.f680a.b(this.f681b);
        MainActivity a2 = b.b.a.b.a(this.f680a);
        if (a2 != null) {
            a2.a(exc, (b<? super e, m>) null);
        }
        return m.f5330a;
    }
}
