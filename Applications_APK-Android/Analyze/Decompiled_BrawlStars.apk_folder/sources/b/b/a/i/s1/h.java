package b.b.a.i.s1;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.graphics.drawable.AnimatedVectorDrawableCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.RtlViewPager;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import b.b.a.e.a;
import b.b.a.g.i;
import b.b.a.i.b;
import b.b.a.i.f1;
import b.b.a.i.g;
import b.b.a.j.a0;
import b.b.a.j.q1;
import com.facebook.share.internal.ShareConstants;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.EdgeAntialiasingImageView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class h extends g {
    public HashMap m;

    public static final class a extends b.a implements a0 {
        public static final Parcelable.Creator<a> CREATOR = new C0063a();
        public static final b g = new b(null);
        public final boolean e;
        public final Class<? extends g> f = h.class;

        /* renamed from: b.b.a.i.s1.h$a$a  reason: collision with other inner class name */
        public final class C0063a implements Parcelable.Creator<a> {
            public final a createFromParcel(Parcel parcel) {
                j.b(parcel, ShareConstants.FEED_SOURCE_PARAM);
                j.b(parcel, "parcel");
                return new a();
            }

            public final a[] newArray(int i) {
                return new a[i];
            }
        }

        public static final class b {
            public /* synthetic */ b(kotlin.d.b.g gVar) {
            }

            public final int a(int i, int i2, int i3) {
                int a2 = i - kotlin.e.a.a(b.b.a.b.a(220) + Math.max(b.b.a.b.a(320) + ((float) i3), ((float) i) * 0.523f));
                if (a2 >= i2 + kotlin.e.a.a(b.b.a.b.a(40))) {
                    return a2;
                }
                return 0;
            }

            public final int b(int i, int i2, int i3) {
                float a2 = ((float) ((i - i2) - i3)) - b.b.a.b.a(320);
                float a3 = b.b.a.b.a(175);
                float a4 = b.b.a.b.a(220);
                if (Float.compare(a2, a3) < 0) {
                    a2 = a3;
                } else if (Float.compare(a2, a4) > 0) {
                    a2 = a4;
                }
                return i2 + kotlin.e.a.a(a2);
            }
        }

        public final int a(int i, int i2, int i3) {
            return g.a(i, i2, i3);
        }

        public final int a(Resources resources, int i, int i2, int i3) {
            j.b(resources, "resources");
            if ((i - i2) - i3 >= kotlin.e.a.a(b.b.a.b.a(460))) {
                return kotlin.e.a.a(((float) i) * 0.1f);
            }
            return 0;
        }

        public final Class<? extends g> a() {
            return this.f;
        }

        public final int b(Resources resources, int i, int i2, int i3) {
            j.b(resources, "resources");
            return i2 + kotlin.e.a.a(b.b.a.b.a((int) ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION));
        }

        public final int c(Resources resources, int i, int i2, int i3) {
            j.b(resources, "resources");
            return g.b(i, i2, i3);
        }

        public final int describeContents() {
            return 0;
        }

        public final Class<? extends g> e(Resources resources) {
            j.b(resources, "resources");
            return c.class;
        }

        public final boolean e() {
            return this.e;
        }

        public final void writeToParcel(Parcel parcel, int i) {
            j.b(parcel, "dest");
        }
    }

    public static final class b extends FragmentPagerAdapter {

        /* renamed from: a  reason: collision with root package name */
        public final kotlin.d.a.a<f1>[] f669a = {a.f670a, C0064b.f671a};

        public final class a extends k implements kotlin.d.a.a<c> {

            /* renamed from: a  reason: collision with root package name */
            public static final a f670a = new a();

            public a() {
                super(0);
            }

            public final Object invoke() {
                return new c();
            }
        }

        /* renamed from: b.b.a.i.s1.h$b$b  reason: collision with other inner class name */
        public final class C0064b extends k implements kotlin.d.a.a<p> {

            /* renamed from: a  reason: collision with root package name */
            public static final C0064b f671a = new C0064b();

            public C0064b() {
                super(0);
            }

            public final Object invoke() {
                return new p();
            }
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(FragmentManager fragmentManager) {
            super(fragmentManager);
            j.b(fragmentManager, "fm");
        }

        public final int getCount() {
            return this.f669a.length;
        }

        public final Fragment getItem(int i) {
            return this.f669a[i].invoke();
        }
    }

    public final class d extends k implements kotlin.d.a.c<View, Integer, m> {
        public d() {
            super(2);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.EdgeAntialiasingImageView, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final Object invoke(Object obj, Object obj2) {
            View view = (View) obj;
            int intValue = ((Number) obj2).intValue();
            j.b(view, "view");
            if (h.this.i().getCurrentItem() != intValue) {
                h.this.i().setCurrentItem(intValue);
            } else {
                SupercellId.INSTANCE.getSharedServices$supercellId_release().l.a(a.C0004a.TAB_SWITCH);
                EdgeAntialiasingImageView edgeAntialiasingImageView = (EdgeAntialiasingImageView) view.findViewById(R.id.tab_icon_left);
                j.a((Object) edgeAntialiasingImageView, "view.tab_icon_left");
                EdgeAntialiasingImageView edgeAntialiasingImageView2 = (EdgeAntialiasingImageView) view.findViewById(R.id.tab_icon_right);
                j.a((Object) edgeAntialiasingImageView2, "view.tab_icon_right");
                b.b.a.b.a(edgeAntialiasingImageView, edgeAntialiasingImageView2, 0, 4);
            }
            return m.f5330a;
        }
    }

    public final View a(int i) {
        if (this.m == null) {
            this.m = new HashMap();
        }
        View view = (View) this.m.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.m.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public final ViewPager i() {
        RtlViewPager rtlViewPager = (RtlViewPager) a(R.id.tabPager);
        if (rtlViewPager != null) {
            return rtlViewPager;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.support.v4.view.ViewPager");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.app.FragmentManager, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.app.Fragment, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onActivityCreated(Bundle bundle) {
        g gVar;
        int i;
        List<Fragment> fragments;
        super.onActivityCreated(bundle);
        FragmentManager childFragmentManager = getChildFragmentManager();
        j.a((Object) childFragmentManager, "childFragmentManager");
        i().setAdapter(new b(childFragmentManager));
        FragmentManager fragmentManager = getFragmentManager();
        int i2 = 0;
        if (fragmentManager == null || (fragments = fragmentManager.getFragments()) == null) {
            gVar = null;
        } else {
            ArrayList arrayList = new ArrayList();
            for (T next : fragments) {
                Fragment fragment = (Fragment) next;
                j.a((Object) fragment, "it");
                if (fragment.getId() == R.id.top_area) {
                    arrayList.add(next);
                }
            }
            ArrayList arrayList2 = new ArrayList();
            for (Object next2 : arrayList) {
                if (next2 instanceof c) {
                    arrayList2.add(next2);
                }
            }
            gVar = (g) kotlin.a.m.d((List) arrayList2);
        }
        c cVar = (c) gVar;
        if (cVar != null) {
            cVar.a(i());
        }
        LinearLayout linearLayout = (LinearLayout) a(R.id.tabBarView);
        if (linearLayout != null) {
            List<View> a2 = b.b.a.b.a(linearLayout);
            for (T next3 : a2) {
                int i3 = i2 + 1;
                if (i2 < 0) {
                    kotlin.a.m.a();
                }
                View view = (View) next3;
                if (i2 == 0) {
                    i = R.drawable.tab_button_start;
                } else if (i2 == a2.size() - 1) {
                    i = R.drawable.tab_button_end;
                } else {
                    i2 = i3;
                }
                view.setBackgroundResource(i);
                i2 = i3;
            }
            b.b.a.b.a(getContext(), a2, i(), new d());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_profile_v1, viewGroup, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.app.Fragment, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onDestroyView() {
        g gVar;
        List<Fragment> fragments;
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager == null || (fragments = fragmentManager.getFragments()) == null) {
            gVar = null;
        } else {
            ArrayList arrayList = new ArrayList();
            for (T next : fragments) {
                Fragment fragment = (Fragment) next;
                j.a((Object) fragment, "it");
                if (fragment.getId() == R.id.top_area) {
                    arrayList.add(next);
                }
            }
            ArrayList arrayList2 = new ArrayList();
            for (Object next2 : arrayList) {
                if (next2 instanceof c) {
                    arrayList2.add(next2);
                }
            }
            gVar = (g) kotlin.a.m.d((List) arrayList2);
        }
        c cVar = (c) gVar;
        if (cVar != null) {
            cVar.a((ViewPager) null);
        }
        super.onDestroyView();
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.app.Fragment, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.b.a(android.content.Context, java.util.List, int, boolean, int):void
     arg types: [android.content.Context, java.util.List<android.view.View>, int, int, int]
     candidates:
      b.b.a.b.a(android.content.Context, java.util.List<? extends android.view.View>, java.util.List<b.b.a.j.c1>, int, boolean):void
      b.b.a.b.a(android.content.Context, java.util.List<? extends android.view.View>, kotlin.d.a.a<? extends java.util.List<b.b.a.j.c1>>, android.support.v4.view.ViewPager, kotlin.d.a.c<? super android.view.View, ? super java.lang.Integer, kotlin.m>):void
      b.b.a.b.a(android.view.View, boolean, boolean, int, int):void
      b.b.a.b.a(android.content.Context, java.util.List, int, boolean, int):void */
    public final void onViewStateRestored(Bundle bundle) {
        g gVar;
        List<Fragment> fragments;
        super.onViewStateRestored(bundle);
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager == null || (fragments = fragmentManager.getFragments()) == null) {
            gVar = null;
        } else {
            ArrayList arrayList = new ArrayList();
            for (T next : fragments) {
                Fragment fragment = (Fragment) next;
                j.a((Object) fragment, "it");
                if (fragment.getId() == R.id.top_area) {
                    arrayList.add(next);
                }
            }
            ArrayList arrayList2 = new ArrayList();
            for (Object next2 : arrayList) {
                if (next2 instanceof c) {
                    arrayList2.add(next2);
                }
            }
            gVar = (g) kotlin.a.m.d((List) arrayList2);
        }
        c cVar = (c) gVar;
        if (cVar != null) {
            Integer num = cVar.m;
            if (num != null) {
                i().setCurrentItem(num.intValue());
            }
            cVar.m = null;
            cVar.b(i().getCurrentItem());
        }
        LinearLayout linearLayout = (LinearLayout) a(R.id.tabBarView);
        if (linearLayout != null) {
            b.b.a.b.a(getContext(), (List) b.b.a.b.a(linearLayout), i().getCurrentItem(), false, 8);
        }
    }

    public static final class c extends g {
        public Integer m;
        public HashMap n;

        public final class a extends k implements kotlin.d.a.c<View, Integer, m> {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ List f672a;

            /* renamed from: b  reason: collision with root package name */
            public final /* synthetic */ c f673b;
            public final /* synthetic */ ViewPager c;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(List list, c cVar, ViewPager viewPager) {
                super(2);
                this.f672a = list;
                this.f673b = cVar;
                this.c = viewPager;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
             arg types: [com.supercell.id.view.EdgeAntialiasingImageView, java.lang.String]
             candidates:
              kotlin.d.b.j.a(int, int):int
              kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
              kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
              kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: b.b.a.b.a(android.content.Context, java.util.List, int, boolean, int):void
             arg types: [android.content.Context, java.util.List, int, int, int]
             candidates:
              b.b.a.b.a(android.content.Context, java.util.List<? extends android.view.View>, java.util.List<b.b.a.j.c1>, int, boolean):void
              b.b.a.b.a(android.content.Context, java.util.List<? extends android.view.View>, kotlin.d.a.a<? extends java.util.List<b.b.a.j.c1>>, android.support.v4.view.ViewPager, kotlin.d.a.c<? super android.view.View, ? super java.lang.Integer, kotlin.m>):void
              b.b.a.b.a(android.view.View, boolean, boolean, int, int):void
              b.b.a.b.a(android.content.Context, java.util.List, int, boolean, int):void */
            public final Object invoke(Object obj, Object obj2) {
                View view = (View) obj;
                int intValue = ((Number) obj2).intValue();
                j.b(view, "view");
                ViewPager viewPager = this.c;
                if (viewPager == null) {
                    b.b.a.b.a(this.f673b.getContext(), this.f672a, intValue, false, 8);
                    SupercellId.INSTANCE.getSharedServices$supercellId_release().l.a(a.C0004a.TAB_SWITCH);
                    EdgeAntialiasingImageView edgeAntialiasingImageView = (EdgeAntialiasingImageView) view.findViewById(R.id.tab_icon_left);
                    j.a((Object) edgeAntialiasingImageView, "view.tab_icon_left");
                    EdgeAntialiasingImageView edgeAntialiasingImageView2 = (EdgeAntialiasingImageView) view.findViewById(R.id.tab_icon_right);
                    j.a((Object) edgeAntialiasingImageView2, "view.tab_icon_right");
                    b.b.a.b.a(edgeAntialiasingImageView, edgeAntialiasingImageView2, 0, 4);
                    this.f673b.a(Integer.valueOf(intValue));
                    MainActivity a2 = b.b.a.b.a((Fragment) this.f673b);
                    if (a2 != null) {
                        b.b.a.b.b(a2);
                    }
                } else if (viewPager.getCurrentItem() != intValue) {
                    this.c.setCurrentItem(intValue);
                } else {
                    SupercellId.INSTANCE.getSharedServices$supercellId_release().l.a(a.C0004a.TAB_SWITCH);
                    EdgeAntialiasingImageView edgeAntialiasingImageView3 = (EdgeAntialiasingImageView) view.findViewById(R.id.tab_icon_left);
                    j.a((Object) edgeAntialiasingImageView3, "view.tab_icon_left");
                    EdgeAntialiasingImageView edgeAntialiasingImageView4 = (EdgeAntialiasingImageView) view.findViewById(R.id.tab_icon_right);
                    j.a((Object) edgeAntialiasingImageView4, "view.tab_icon_right");
                    b.b.a.b.a(edgeAntialiasingImageView3, edgeAntialiasingImageView4, 0, 4);
                }
                return m.f5330a;
            }
        }

        public final View a(int i) {
            if (this.n == null) {
                this.n = new HashMap();
            }
            View view = (View) this.n.get(Integer.valueOf(i));
            if (view != null) {
                return view;
            }
            View view2 = getView();
            if (view2 == null) {
                return null;
            }
            View findViewById = view2.findViewById(i);
            this.n.put(Integer.valueOf(i), findViewById);
            return findViewById;
        }

        public final void a() {
            HashMap hashMap = this.n;
            if (hashMap != null) {
                hashMap.clear();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.ViewPropertyAnimator, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void a(View view, g.a aVar, boolean z) {
            View a2;
            j.b(view, "view");
            j.b(aVar, "animation");
            super.a(view, aVar, z);
            if (aVar == g.a.ENTRY) {
                Context context = getContext();
                if (!(context == null || (a2 = a(R.id.checkmarkstrip)) == null)) {
                    AnimatedVectorDrawableCompat create = AnimatedVectorDrawableCompat.create(context, R.drawable.checkmarkstrip_circle_animate);
                    if (create != null) {
                        ((ImageView) a2.findViewById(R.id.checkmarkstripCenterCircle)).setImageDrawable(create);
                        create.start();
                    }
                    AnimatedVectorDrawableCompat create2 = AnimatedVectorDrawableCompat.create(context, R.drawable.checkmarkstrip_left_line_animate);
                    if (create2 != null) {
                        ((ImageView) a2.findViewById(R.id.checkmarkstripLeft)).setImageDrawable(create2);
                        create2.start();
                    }
                    AnimatedVectorDrawableCompat create3 = AnimatedVectorDrawableCompat.create(context, R.drawable.checkmarkstrip_right_line_animate);
                    if (create3 != null) {
                        ((ImageView) a2.findViewById(R.id.checkmarkstripRight)).setImageDrawable(create3);
                        create3.start();
                    }
                    AnimatedVectorDrawableCompat create4 = AnimatedVectorDrawableCompat.create(context, R.drawable.checkmarkstrip_checkmark_animate);
                    if (create4 != null) {
                        ((ImageView) a2.findViewById(R.id.checkmarkstripCheckmark)).setImageDrawable(create4);
                        create4.start();
                    }
                }
                TextView textView = (TextView) a(R.id.loggedInLabel);
                if (textView != null) {
                    textView.setTranslationY(b.b.a.b.a(8));
                    textView.setAlpha(0.0f);
                    ViewPropertyAnimator animate = textView.animate();
                    animate.translationY(b.b.a.b.a(0));
                    animate.alpha(0.5f);
                    j.a((Object) animate, "it");
                    animate.setStartDelay(1400);
                    animate.setDuration(1100);
                    animate.setInterpolator(b.b.a.f.a.c);
                    animate.start();
                }
                TextView textView2 = (TextView) a(R.id.emailTextView);
                if (textView2 != null) {
                    textView2.setTranslationY(b.b.a.b.a(8));
                    textView2.setAlpha(0.0f);
                    ViewPropertyAnimator animate2 = textView2.animate();
                    animate2.translationY(b.b.a.b.a(0));
                    animate2.alpha(1.0f);
                    j.a((Object) animate2, "it");
                    animate2.setStartDelay(1600);
                    animate2.setDuration(1000);
                    animate2.setInterpolator(b.b.a.f.a.c);
                    animate2.start();
                }
            }
        }

        public final void a(Integer num) {
            this.m = num;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: b.b.a.b.a(android.content.Context, java.util.List, int, boolean, int):void
         arg types: [android.content.Context, java.util.List<android.view.View>, int, int, int]
         candidates:
          b.b.a.b.a(android.content.Context, java.util.List<? extends android.view.View>, java.util.List<b.b.a.j.c1>, int, boolean):void
          b.b.a.b.a(android.content.Context, java.util.List<? extends android.view.View>, kotlin.d.a.a<? extends java.util.List<b.b.a.j.c1>>, android.support.v4.view.ViewPager, kotlin.d.a.c<? super android.view.View, ? super java.lang.Integer, kotlin.m>):void
          b.b.a.b.a(android.view.View, boolean, boolean, int, int):void
          b.b.a.b.a(android.content.Context, java.util.List, int, boolean, int):void */
        public final void b(int i) {
            List<View> a2;
            FrameLayout frameLayout = (FrameLayout) a(R.id.topAreaTabBarView);
            if (frameLayout != null && (a2 = b.b.a.b.a(frameLayout)) != null) {
                b.b.a.b.a(getContext(), (List) a2, i, false, 8);
            }
        }

        public final void onCreate(Bundle bundle) {
            super.onCreate(bundle);
            if (bundle != null && bundle.containsKey("selectedTab")) {
                this.m = Integer.valueOf(bundle.getInt("selectedTab"));
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
            j.b(layoutInflater, "inflater");
            return layoutInflater.inflate(R.layout.fragment_profile_v1_top_area, viewGroup, false);
        }

        public final /* synthetic */ void onDestroyView() {
            super.onDestroyView();
            a();
        }

        public final void onSaveInstanceState(Bundle bundle) {
            j.b(bundle, "outState");
            Integer num = this.m;
            if (num != null) {
                bundle.putInt("selectedTab", num.intValue());
            }
            super.onSaveInstanceState(bundle);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: b.b.a.g.i.a(boolean, boolean):void
         arg types: [int, int]
         candidates:
          b.b.a.g.i.a(b.b.a.g.i, int):void
          b.b.a.g.i.a(boolean, float):void
          b.b.a.g.i.a(boolean, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.content.res.Resources, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onViewCreated(View view, Bundle bundle) {
            List<View> a2;
            j.b(view, "view");
            super.onViewCreated(view, bundle);
            TextView textView = (TextView) a(R.id.emailTextView);
            if (textView != null) {
                textView.setText(SupercellId.INSTANCE.getSharedServices$supercellId_release().d());
            }
            a((ViewPager) null);
            Integer num = this.m;
            b(num != null ? num.intValue() : 0);
            FrameLayout frameLayout = (FrameLayout) a(R.id.blueArea);
            if (frameLayout != null) {
                MainActivity a3 = b.b.a.b.a((Fragment) this);
                if (a3 != null) {
                    i iVar = new i(a3);
                    iVar.a(true, false);
                    ViewCompat.setBackground(frameLayout, iVar);
                }
                if (Build.VERSION.SDK_INT < 18) {
                    frameLayout.setLayerType(1, null);
                }
            }
            Resources resources = getResources();
            j.a((Object) resources, "resources");
            if (b.b.a.b.b(resources)) {
                int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.top_area_shadow_width);
                FrameLayout frameLayout2 = (FrameLayout) view.findViewById(R.id.root);
                if (frameLayout2 != null) {
                    q1.b(frameLayout2, -dimensionPixelSize);
                    q1.c(frameLayout2, q1.a(frameLayout2) + dimensionPixelSize);
                }
                LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.content);
                if (linearLayout != null) {
                    q1.b(linearLayout, -dimensionPixelSize);
                    q1.c(linearLayout, q1.a(linearLayout) + dimensionPixelSize);
                }
                FrameLayout frameLayout3 = (FrameLayout) view.findViewById(R.id.topAreaTabBarView);
                if (frameLayout3 != null) {
                    q1.b(frameLayout3, -dimensionPixelSize);
                }
                int dimensionPixelSize2 = view.getResources().getDimensionPixelSize(R.dimen.tab_button_inset_end);
                View findViewById = view.findViewById(R.id.topAreaTabBarViewBackground);
                if (findViewById != null) {
                    q1.b(findViewById, dimensionPixelSize);
                }
                FrameLayout frameLayout4 = (FrameLayout) view.findViewById(R.id.topAreaTabBarView);
                if (frameLayout4 != null && (a2 = b.b.a.b.a(frameLayout4)) != null) {
                    for (View b2 : a2) {
                        q1.b(b2, (-dimensionPixelSize2) + dimensionPixelSize);
                    }
                }
            }
        }

        public final void a(ViewPager viewPager) {
            FrameLayout frameLayout = (FrameLayout) a(R.id.topAreaTabBarView);
            if (frameLayout != null) {
                List<View> a2 = b.b.a.b.a(frameLayout);
                b.b.a.b.a(getContext(), a2, viewPager, new a(a2, this, viewPager));
            }
        }
    }
}
