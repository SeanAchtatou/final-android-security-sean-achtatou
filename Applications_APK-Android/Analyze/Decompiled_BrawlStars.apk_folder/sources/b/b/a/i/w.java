package b.b.a.i;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.FlowPager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import b.b.a.i.b;
import com.supercell.id.R;
import com.supercell.id.view.FlowPhaseIndicator;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.TypeCastException;
import kotlin.a.m;
import kotlin.d.b.g;
import kotlin.d.b.j;

public abstract class w extends g {

    public static abstract class a extends b.a {
        public int b(Resources resources, int i, int i2, int i3) {
            j.b(resources, "resources");
            float f = (float) ((i - i2) - i3);
            float a2 = f - b.b.a.b.a(410);
            float a3 = b.b.a.b.a(70);
            float f2 = f * 0.2f;
            if (Float.compare(a2, a3) < 0) {
                f2 = a3;
            } else if (Float.compare(a2, f2) <= 0) {
                f2 = a2;
            }
            return i2 + kotlin.e.a.a(f2);
        }

        public int c(Resources resources, int i, int i2, int i3) {
            j.b(resources, "resources");
            float a2 = ((float) ((i - i2) - i3)) - b.b.a.b.a(380);
            float a3 = b.b.a.b.a(100);
            float a4 = b.b.a.b.a(180);
            if (Float.compare(a2, a3) < 0) {
                a2 = a3;
            } else if (Float.compare(a2, a4) > 0) {
                a2 = a4;
            }
            return i2 + kotlin.e.a.a(a2);
        }

        public Class<? extends g> e(Resources resources) {
            j.b(resources, "resources");
            return c.class;
        }

        public String f(Resources resources) {
            j.b(resources, "resources");
            return b() + '$' + e(resources);
        }
    }

    public static final class b extends FragmentPagerAdapter {

        /* renamed from: a  reason: collision with root package name */
        public final Map<Integer, WeakReference<x>> f822a = new LinkedHashMap();

        /* renamed from: b  reason: collision with root package name */
        public final kotlin.d.a.a<x>[] f823b;

        /* JADX WARN: Type inference failed for: r3v0, types: [kotlin.d.a.a<b.b.a.i.x>[], java.lang.Object, kotlin.d.a.a<? extends b.b.a.i.x>[]] */
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public b(android.support.v4.app.FragmentManager r2, kotlin.d.a.a<? extends b.b.a.i.x>[] r3) {
            /*
                r1 = this;
                java.lang.String r0 = "fm"
                kotlin.d.b.j.b(r2, r0)
                java.lang.String r0 = "fragments"
                kotlin.d.b.j.b(r3, r0)
                r1.<init>(r2)
                r1.f823b = r3
                java.util.LinkedHashMap r2 = new java.util.LinkedHashMap
                r2.<init>()
                r1.f822a = r2
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: b.b.a.i.w.b.<init>(android.support.v4.app.FragmentManager, kotlin.d.a.a[]):void");
        }

        public final int getCount() {
            return this.f823b.length;
        }

        public final Fragment getItem(int i) {
            return this.f823b[i].invoke();
        }

        public final Object instantiateItem(ViewGroup viewGroup, int i) {
            j.b(viewGroup, "container");
            Object instantiateItem = super.instantiateItem(viewGroup, i);
            Map<Integer, WeakReference<x>> map = this.f822a;
            Integer valueOf = Integer.valueOf(i);
            if (instantiateItem != null) {
                map.put(valueOf, new WeakReference((x) instantiateItem));
                j.a(instantiateItem, "super.instantiateItem(co…geFragment)\n            }");
                return instantiateItem;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.supercell.id.ui.FlowPageFragment");
        }
    }

    public static final class c extends g {
        public static final a n = new a(null);
        public HashMap m;

        public static final class a {
            public /* synthetic */ a(g gVar) {
            }

            public final c a(String str, String str2, String str3, boolean z) {
                j.b(str, "title0");
                j.b(str2, "title1");
                j.b(str3, "title2");
                c cVar = new c();
                Bundle arguments = cVar.getArguments();
                if (arguments == null) {
                    arguments = new Bundle();
                }
                arguments.putString("title0", str);
                arguments.putString("title1", str2);
                arguments.putString("title2", str3);
                arguments.putBoolean("clampPosition", z);
                cVar.setArguments(arguments);
                return cVar;
            }
        }

        public final class b implements View.OnTouchListener {
            public b() {
            }

            public final boolean onTouch(View view, MotionEvent motionEvent) {
                View currentFocus;
                FragmentActivity activity = c.this.getActivity();
                if (activity == null || (currentFocus = activity.getCurrentFocus()) == null) {
                    return false;
                }
                currentFocus.clearFocus();
                return false;
            }
        }

        public final View a(int i) {
            if (this.m == null) {
                this.m = new HashMap();
            }
            View view = (View) this.m.get(Integer.valueOf(i));
            if (view != null) {
                return view;
            }
            View view2 = getView();
            if (view2 == null) {
                return null;
            }
            View findViewById = view2.findViewById(i);
            this.m.put(Integer.valueOf(i), findViewById);
            return findViewById;
        }

        public final void a() {
            HashMap hashMap = this.m;
            if (hashMap != null) {
                hashMap.clear();
            }
        }

        public final void c(float f) {
            Bundle arguments = getArguments();
            float f2 = 0.0f;
            float f3 = 1.0f;
            if (arguments == null || !arguments.getBoolean("clampPosition")) {
                f2 = f - 1.0f;
            } else {
                float f4 = f - 1.0f;
                if (Float.compare(f4, 0.0f) >= 0) {
                    f2 = Float.compare(f4, 2.0f) > 0 ? 2.0f : f4;
                }
            }
            int a2 = kotlin.e.a.a(f2);
            TextView textView = (TextView) a(R.id.flowProgressTitle0);
            if (textView != null) {
                textView.setAlpha(a2 == 0 ? 1.0f : 0.5f);
            }
            TextView textView2 = (TextView) a(R.id.flowProgressTitle1);
            if (textView2 != null) {
                textView2.setAlpha(a2 == 1 ? 1.0f : 0.5f);
            }
            TextView textView3 = (TextView) a(R.id.flowProgressTitle2);
            if (textView3 != null) {
                if (a2 != 2) {
                    f3 = 0.5f;
                }
                textView3.setAlpha(f3);
            }
            FlowPhaseIndicator flowPhaseIndicator = (FlowPhaseIndicator) a(R.id.flowProgressBar);
            if (flowPhaseIndicator != null) {
                flowPhaseIndicator.setProgress(f2 / 2.0f);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.TextView, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
            String string;
            String string2;
            String string3;
            j.b(layoutInflater, "inflater");
            View inflate = layoutInflater.inflate(R.layout.fragment_flow_top_area, viewGroup, false);
            Bundle arguments = getArguments();
            if (!(arguments == null || (string3 = arguments.getString("title0")) == null)) {
                j.a((Object) inflate, "rootView");
                TextView textView = (TextView) inflate.findViewById(R.id.flowProgressTitle0);
                j.a((Object) textView, "rootView.flowProgressTitle0");
                b.b.a.i.x1.j.a(textView, string3, (kotlin.d.a.b) null, 2);
            }
            Bundle arguments2 = getArguments();
            if (!(arguments2 == null || (string2 = arguments2.getString("title1")) == null)) {
                j.a((Object) inflate, "rootView");
                TextView textView2 = (TextView) inflate.findViewById(R.id.flowProgressTitle1);
                j.a((Object) textView2, "rootView.flowProgressTitle1");
                b.b.a.i.x1.j.a(textView2, string2, (kotlin.d.a.b) null, 2);
            }
            Bundle arguments3 = getArguments();
            if (!(arguments3 == null || (string = arguments3.getString("title2")) == null)) {
                j.a((Object) inflate, "rootView");
                TextView textView3 = (TextView) inflate.findViewById(R.id.flowProgressTitle2);
                j.a((Object) textView3, "rootView.flowProgressTitle2");
                b.b.a.i.x1.j.a(textView3, string, (kotlin.d.a.b) null, 2);
            }
            return inflate;
        }

        public final /* synthetic */ void onDestroyView() {
            super.onDestroyView();
            a();
        }

        public final void onViewCreated(View view, Bundle bundle) {
            j.b(view, "view");
            super.onViewCreated(view, bundle);
            view.setOnTouchListener(new b());
        }
    }

    public final class d extends ViewPager.SimpleOnPageChangeListener {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ b f826b;

        public d(b bVar) {
            this.f826b = bVar;
        }

        public final void onPageScrolled(int i, float f, int i2) {
            c a2 = w.this.j();
            if (a2 != null) {
                a2.c(((float) i) + f);
            }
        }

        public final void onPageSelected(int i) {
            WeakReference weakReference = this.f826b.f822a.get(Integer.valueOf(i));
            x xVar = weakReference != null ? (x) weakReference.get() : null;
            if (xVar != null) {
                xVar.e();
            }
        }
    }

    public abstract View a(int i);

    public final void a(kotlin.d.a.b<? super View, Boolean> bVar) {
        View currentFocus;
        FragmentActivity activity = getActivity();
        if (!(activity == null || (currentFocus = activity.getCurrentFocus()) == null)) {
            if (bVar != null && bVar.invoke(currentFocus).booleanValue()) {
                return;
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            b.b.a.b.a((Activity) activity2);
        }
    }

    public abstract kotlin.d.a.a<x>[] i();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.app.Fragment, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final c j() {
        g gVar;
        List<Fragment> fragments;
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager == null || (fragments = fragmentManager.getFragments()) == null) {
            gVar = null;
        } else {
            ArrayList arrayList = new ArrayList();
            for (T next : fragments) {
                Fragment fragment = (Fragment) next;
                j.a((Object) fragment, "it");
                if (fragment.getId() == R.id.top_area) {
                    arrayList.add(next);
                }
            }
            ArrayList arrayList2 = new ArrayList();
            for (Object next2 : arrayList) {
                if (next2 instanceof c) {
                    arrayList2.add(next2);
                }
            }
            gVar = (g) m.d((List) arrayList2);
        }
        return (c) gVar;
    }

    public final void k() {
        FlowPager flowPager = (FlowPager) a(R.id.flowPager);
        if (flowPager != null) {
            flowPager.setCurrentItem(flowPager.getCurrentItem() + 1);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.app.FragmentManager, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.view.FlowPager, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FragmentManager childFragmentManager = getChildFragmentManager();
        j.a((Object) childFragmentManager, "childFragmentManager");
        b bVar = new b(childFragmentManager, i());
        FlowPager flowPager = (FlowPager) a(R.id.flowPager);
        j.a((Object) flowPager, "flowPager");
        flowPager.setAdapter(bVar);
        ((FlowPager) a(R.id.flowPager)).addOnPageChangeListener(new d(bVar));
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setRetainInstance(true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_flow, viewGroup, false);
    }

    public void onDestroy() {
        a((kotlin.d.a.b<? super View, Boolean>) null);
        super.onDestroy();
    }

    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.view.FlowPager, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public void onViewStateRestored(Bundle bundle) {
        super.onViewStateRestored(bundle);
        FlowPager flowPager = (FlowPager) a(R.id.flowPager);
        j.a((Object) flowPager, "flowPager");
        if (flowPager.getCurrentItem() == 0) {
            c j = j();
            if (j != null) {
                j.c(0.0f);
            }
            FlowPager flowPager2 = (FlowPager) a(R.id.flowPager);
            j.a((Object) flowPager2, "flowPager");
            PagerAdapter adapter = flowPager2.getAdapter();
            if (!(adapter instanceof b)) {
                adapter = null;
            }
            b bVar = (b) adapter;
            Fragment item = bVar != null ? bVar.getItem(0) : null;
            if (!(item instanceof x)) {
                item = null;
            }
            x xVar = (x) item;
            if (xVar != null) {
                xVar.e();
            }
        }
    }
}
