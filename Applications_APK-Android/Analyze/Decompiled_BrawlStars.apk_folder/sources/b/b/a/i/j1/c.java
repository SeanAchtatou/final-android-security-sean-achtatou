package b.b.a.i.j1;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import b.b.a.h.j;
import b.b.a.i.a0;
import b.b.a.i.b;
import b.b.a.i.d0;
import b.b.a.i.e0;
import b.b.a.i.r;
import b.b.a.i.v1.j;
import b.b.a.i.x0;
import b.b.a.i.z;
import b.b.a.j.i;
import b.b.a.j.k0;
import b.b.a.j.o;
import b.b.a.j.o1;
import b.b.a.j.q;
import b.b.a.j.q1;
import b.b.a.j.r0;
import b.b.a.j.s0;
import b.b.a.j.t0;
import b.b.a.j.u0;
import b.b.a.j.y;
import b.b.a.j.y0;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.SubPageTabLayout;
import com.supercell.id.view.WidthAdjustingMultilineButton;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.a.ab;
import kotlin.a.ad;
import kotlin.a.ai;
import kotlin.a.an;
import nl.komponents.kovenant.ao;
import nl.komponents.kovenant.bb;
import nl.komponents.kovenant.bc;
import nl.komponents.kovenant.bw;

public final class c extends b.b.a.i.g {
    public final float m = b.b.a.b.a(20);
    public final Set<String> n = new LinkedHashSet();
    public List<? extends r0> o;
    public final y0<List<b.b.a.h.f>> p = new y0<>(new e(), new f());
    public final y0<u0> q = new y0<>(new g(), new h());
    public HashMap r;

    public final class a extends kotlin.d.b.k implements kotlin.d.a.b<List<? extends Map<String, ? extends Exception>>, kotlin.m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f308a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ List f309b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(WeakReference weakReference, List list) {
            super(1);
            this.f308a = weakReference;
            this.f309b = list;
        }

        public final void invoke(List<? extends Map<String, ? extends Exception>> list) {
            Object obj = this.f308a.get();
            if (obj != null) {
                c cVar = (c) obj;
                LinkedHashMap linkedHashMap = new LinkedHashMap();
                for (Map putAll : list) {
                    linkedHashMap.putAll(putAll);
                }
                if (!linkedHashMap.isEmpty()) {
                    MainActivity a2 = b.b.a.b.a((Fragment) cVar);
                    if (a2 != null) {
                        a2.a((Exception) kotlin.a.m.c(linkedHashMap.values()), (kotlin.d.a.b<? super b.b.a.i.e, kotlin.m>) null);
                    }
                    List list2 = this.f309b;
                    ArrayList arrayList = new ArrayList();
                    for (Object next : list2) {
                        if (linkedHashMap.containsKey(((b) next).f307b)) {
                            arrayList.add(next);
                        }
                    }
                    cVar.a(arrayList);
                }
            }
        }
    }

    public static final class b extends b.a {
        public static final a CREATOR = new a(null);
        public final Set<Integer> e = an.a((Object[]) new Integer[]{Integer.valueOf(R.id.nav_area_back_button), Integer.valueOf(R.id.nav_area_close_button)});
        public final boolean f = true;
        public final Class<? extends b.b.a.i.g> g = c.class;

        public static final class a implements Parcelable.Creator<b> {
            public /* synthetic */ a(kotlin.d.b.g gVar) {
            }

            public final Object createFromParcel(Parcel parcel) {
                kotlin.d.b.j.b(parcel, "parcel");
                return new b();
            }

            public final Object[] newArray(int i) {
                return new b[i];
            }
        }

        public final int a(int i, int i2, int i3) {
            return a0.u.a(i, i2, i3);
        }

        public final Class<? extends b.b.a.i.g> a() {
            return this.g;
        }

        public final Class<? extends x0> a(Resources resources) {
            kotlin.d.b.j.b(resources, "resources");
            return b.b.a.b.b(resources) ? z.class : b.b.a.i.a.class;
        }

        public final int b(Resources resources, int i, int i2, int i3) {
            float f2;
            kotlin.d.b.j.b(resources, "resources");
            kotlin.d.b.j.b(resources, "$this$isSmallScreen");
            if (resources.getBoolean(R.bool.isSmallScreen)) {
                f2 = b.b.a.b.a(64);
            } else {
                f2 = b.b.a.b.a(150);
            }
            return i2 + kotlin.e.a.a(f2);
        }

        public final int c(Resources resources, int i, int i2, int i3) {
            kotlin.d.b.j.b(resources, "resources");
            return a0.u.b(i, i2, i3);
        }

        public final Set<Integer> c() {
            return this.e;
        }

        public final boolean c(Resources resources) {
            kotlin.d.b.j.b(resources, "resources");
            return !b.b.a.b.b(resources);
        }

        public final int describeContents() {
            return 0;
        }

        public final Class<? extends b.b.a.i.g> e(Resources resources) {
            kotlin.d.b.j.b(resources, "resources");
            if (b.b.a.b.b(resources)) {
                return a0.class;
            }
            kotlin.d.b.j.b(resources, "$this$isSmallScreen");
            return resources.getBoolean(R.bool.isSmallScreen) ? d0.class : e0.class;
        }

        public final boolean e() {
            return this.f;
        }

        public final void writeToParcel(Parcel parcel, int i) {
        }
    }

    public final class d extends kotlin.d.b.k implements kotlin.d.a.b<String, kotlin.m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f322a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(WeakReference weakReference) {
            super(1);
            this.f322a = weakReference;
        }

        public final Object invoke(Object obj) {
            String str = (String) obj;
            kotlin.d.b.j.b(str, "it");
            c cVar = (c) this.f322a.get();
            if (cVar != null) {
                TextView textView = (TextView) cVar.a(R.id.toolbarTitle);
                if (textView != null) {
                    b.b.a.i.x1.j.a(textView, "account_invite_friends_ingame_title", kotlin.k.a("game", str));
                }
                b.b.a.b.a((SubPageTabLayout) cVar.a(R.id.tabBarView), "account_invite_friends_ingame_title", kotlin.k.a("game", str));
            }
            return kotlin.m.f5330a;
        }
    }

    public final class e extends kotlin.d.b.k implements kotlin.d.a.b<List<? extends b.b.a.h.f>, kotlin.m> {
        public e() {
            super(1);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.a.p.a(java.lang.Iterable, int):int
         arg types: [java.util.List<b.b.a.h.f>, int]
         candidates:
          kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
          kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
          kotlin.a.w.a(java.util.List, int):T
          kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
          kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
          kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
          kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
          kotlin.a.s.a(java.util.List, java.util.Comparator):void
          kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
          kotlin.a.p.a(java.lang.Iterable, int):int */
        public final void a(List<b.b.a.h.f> list) {
            kotlin.d.b.j.b(list, "list");
            c cVar = c.this;
            ArrayList arrayList = new ArrayList(kotlin.a.m.a((Iterable) list, 10));
            for (b.b.a.h.f fVar : list) {
                arrayList.add(new b(fVar.f116a, fVar.f117b, fVar.c, fVar.e, fVar.d, false));
            }
            cVar.a(arrayList);
        }

        public final /* synthetic */ Object invoke(Object obj) {
            a((List) obj);
            return kotlin.m.f5330a;
        }
    }

    public final class f extends kotlin.d.b.k implements kotlin.d.a.b<Exception, kotlin.m> {
        public f() {
            super(1);
        }

        public final Object invoke(Object obj) {
            Exception exc = (Exception) obj;
            kotlin.d.b.j.b(exc, "it");
            c.this.getClass().getName();
            "Error receiving friends " + exc.getMessage();
            c cVar = c.this;
            cVar.q.a(bb.f5389a);
            return kotlin.m.f5330a;
        }
    }

    public final class g extends kotlin.d.b.k implements kotlin.d.a.b<u0, kotlin.m> {
        public g() {
            super(1);
        }

        public final void a(u0 u0Var) {
            kotlin.d.b.j.b(u0Var, "it");
            c cVar = c.this;
            if (cVar.o == u0Var.f1176a) {
                cVar.o = u0Var.f1177b;
                if (cVar.o == null) {
                    RecyclerView recyclerView = (RecyclerView) cVar.a(R.id.friendsList);
                    if (recyclerView != null) {
                        recyclerView.setVisibility(4);
                    }
                    View a2 = c.this.a(R.id.progressBar);
                    if (a2 != null) {
                        a2.setVisibility(0);
                    }
                } else {
                    RecyclerView recyclerView2 = (RecyclerView) cVar.a(R.id.friendsList);
                    if (recyclerView2 != null) {
                        recyclerView2.setVisibility(0);
                    }
                    View a3 = c.this.a(R.id.progressBar);
                    if (a3 != null) {
                        a3.setVisibility(4);
                    }
                }
                RecyclerView recyclerView3 = (RecyclerView) c.this.a(R.id.friendsList);
                RecyclerView.Adapter adapter = recyclerView3 != null ? recyclerView3.getAdapter() : null;
                if (!(adapter instanceof C0028c)) {
                    adapter = null;
                }
                C0028c cVar2 = (C0028c) adapter;
                if (cVar2 != null) {
                    List<? extends r0> list = c.this.o;
                    if (list == null) {
                        list = ab.f5210a;
                    }
                    cVar2.a(list);
                    u0Var.c.dispatchUpdatesTo(cVar2);
                }
            }
        }

        public final /* synthetic */ Object invoke(Object obj) {
            a((u0) obj);
            return kotlin.m.f5330a;
        }
    }

    public final class h extends kotlin.d.b.k implements kotlin.d.a.b<Exception, kotlin.m> {
        public h() {
            super(1);
        }

        public final Object invoke(Object obj) {
            Exception exc = (Exception) obj;
            kotlin.d.b.j.b(exc, "it");
            MainActivity a2 = b.b.a.b.a((Fragment) c.this);
            if (a2 != null) {
                a2.a(exc, (kotlin.d.a.b<? super b.b.a.i.e, kotlin.m>) null);
            }
            return kotlin.m.f5330a;
        }
    }

    public final class i extends kotlin.d.b.k implements kotlin.d.a.b<Map<String, ? extends b.b.a.j.m<? extends b.b.a.h.i, ? extends Exception>>, Map<String, ? extends Exception>> {

        /* renamed from: a  reason: collision with root package name */
        public static final i f327a = new i();

        public i() {
            super(1);
        }

        /* renamed from: a */
        public final Map<String, Exception> invoke(Map<String, ? extends b.b.a.j.m<b.b.a.h.i, ? extends Exception>> map) {
            kotlin.d.b.j.b(map, "response");
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (Map.Entry next : map.entrySet()) {
                if (((b.b.a.j.m) next.getValue()).b() != null) {
                    linkedHashMap.put(next.getKey(), next.getValue());
                }
            }
            LinkedHashMap linkedHashMap2 = new LinkedHashMap(ai.a(linkedHashMap.size()));
            for (Map.Entry entry : linkedHashMap.entrySet()) {
                Object key = entry.getKey();
                Object b2 = ((b.b.a.j.m) entry.getValue()).b();
                if (b2 == null) {
                    kotlin.d.b.j.a();
                }
                linkedHashMap2.put(key, (Exception) b2);
            }
            return linkedHashMap2;
        }
    }

    public final class j extends kotlin.d.b.k implements kotlin.d.a.b<Exception, Map<String, ? extends Exception>> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ List f328a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(List list) {
            super(1);
            this.f328a = list;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.a.p.a(java.lang.Iterable, int):int
         arg types: [java.util.List, int]
         candidates:
          kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
          kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
          kotlin.a.w.a(java.util.List, int):T
          kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
          kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
          kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
          kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
          kotlin.a.s.a(java.util.List, java.util.Comparator):void
          kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
          kotlin.a.p.a(java.lang.Iterable, int):int */
        /* renamed from: a */
        public final Map<String, Exception> invoke(Exception exc) {
            kotlin.d.b.j.b(exc, "error");
            List<b> list = this.f328a;
            LinkedHashMap linkedHashMap = new LinkedHashMap(kotlin.g.d.c(ai.a(kotlin.a.m.a((Iterable) list, 10)), 16));
            for (b bVar : list) {
                kotlin.h a2 = kotlin.k.a(bVar.f307b, exc);
                linkedHashMap.put(a2.f5262a, a2.f5263b);
            }
            return linkedHashMap;
        }
    }

    public final class k extends kotlin.d.b.k implements kotlin.d.a.b<Map<String, ? extends b.b.a.j.m<? extends Boolean, ? extends Exception>>, Map<String, ? extends Exception>> {

        /* renamed from: a  reason: collision with root package name */
        public static final k f329a = new k();

        public k() {
            super(1);
        }

        /* renamed from: a */
        public final Map<String, Exception> invoke(Map<String, ? extends b.b.a.j.m<Boolean, ? extends Exception>> map) {
            kotlin.d.b.j.b(map, "response");
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (Map.Entry next : map.entrySet()) {
                if (((b.b.a.j.m) next.getValue()).b() != null) {
                    linkedHashMap.put(next.getKey(), next.getValue());
                }
            }
            LinkedHashMap linkedHashMap2 = new LinkedHashMap(ai.a(linkedHashMap.size()));
            for (Map.Entry entry : linkedHashMap.entrySet()) {
                Object key = entry.getKey();
                Object b2 = ((b.b.a.j.m) entry.getValue()).b();
                if (b2 == null) {
                    kotlin.d.b.j.a();
                }
                linkedHashMap2.put(key, (Exception) b2);
            }
            return linkedHashMap2;
        }
    }

    public final class l extends kotlin.d.b.k implements kotlin.d.a.b<Exception, Map<String, ? extends Exception>> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ List f330a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(List list) {
            super(1);
            this.f330a = list;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.a.p.a(java.lang.Iterable, int):int
         arg types: [java.util.List, int]
         candidates:
          kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
          kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
          kotlin.a.w.a(java.util.List, int):T
          kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
          kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
          kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
          kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
          kotlin.a.s.a(java.util.List, java.util.Comparator):void
          kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
          kotlin.a.p.a(java.lang.Iterable, int):int */
        /* renamed from: a */
        public final Map<String, Exception> invoke(Exception exc) {
            kotlin.d.b.j.b(exc, "error");
            List<b> list = this.f330a;
            LinkedHashMap linkedHashMap = new LinkedHashMap(kotlin.g.d.c(ai.a(kotlin.a.m.a((Iterable) list, 10)), 16));
            for (b bVar : list) {
                kotlin.h a2 = kotlin.k.a(bVar.f307b, exc);
                linkedHashMap.put(a2.f5262a, a2.f5263b);
            }
            return linkedHashMap;
        }
    }

    public final class m extends kotlin.d.b.k implements kotlin.d.a.a<u0> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ List f331a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ List f332b;
        public final /* synthetic */ Set c;

        public final class a<T> implements Comparator<T> {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ Comparator f333a;

            public a(Comparator comparator) {
                this.f333a = comparator;
            }

            public final int compare(T t, T t2) {
                Comparator comparator = this.f333a;
                String str = ((b) t).c;
                if (str == null) {
                    str = "";
                }
                String str2 = ((b) t2).c;
                if (str2 == null) {
                    str2 = "";
                }
                return comparator.compare(str, str2);
            }
        }

        public final class b<T> implements Comparator<b> {

            /* renamed from: a  reason: collision with root package name */
            public static final b f334a = new b();

            public final int compare(Object obj, Object obj2) {
                return b.b.a.b.a(((b) obj).c, ((b) obj2).c);
            }
        }

        /* renamed from: b.b.a.i.j1.c$m$c  reason: collision with other inner class name */
        public final class C0030c<T> implements Comparator<b> {

            /* renamed from: a  reason: collision with root package name */
            public static final C0030c f335a = new C0030c();

            public final int compare(Object obj, Object obj2) {
                b bVar = (b) obj;
                b bVar2 = (b) obj2;
                if (!bVar.g || bVar2.g) {
                    return (bVar.g || !bVar2.g) ? 0 : -1;
                }
                return 1;
            }
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(List list, List list2, Set set) {
            super(0);
            this.f331a = list;
            this.f332b = list2;
            this.c = set;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.a.p.a(java.lang.Iterable, int):int
         arg types: [java.util.List, int]
         candidates:
          kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
          kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
          kotlin.a.w.a(java.util.List, int):T
          kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
          kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
          kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
          kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
          kotlin.a.s.a(java.util.List, java.util.Comparator):void
          kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
          kotlin.a.p.a(java.lang.Iterable, int):int */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
         arg types: [java.util.List, b.b.a.i.j1.c$m$c]
         candidates:
          kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
          kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
          kotlin.a.w.a(java.util.List, int):T
          kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
          kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
          kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
          kotlin.a.s.a(java.util.List, java.util.Comparator):void
          kotlin.a.p.a(java.lang.Iterable, int):int
          kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
          kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T> */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
         arg types: [java.util.List, b.b.a.i.j1.c$m$b]
         candidates:
          kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
          kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
          kotlin.a.w.a(java.util.List, int):T
          kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
          kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
          kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
          kotlin.a.s.a(java.util.List, java.util.Comparator):void
          kotlin.a.p.a(java.lang.Iterable, int):int
          kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
          kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T> */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
         arg types: [java.util.ArrayList, b.b.a.i.j1.c$m$a]
         candidates:
          kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
          kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
          kotlin.a.w.a(java.util.List, int):T
          kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
          kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
          kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
          kotlin.a.s.a(java.util.List, java.util.Comparator):void
          kotlin.a.p.a(java.lang.Iterable, int):int
          kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
          kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T> */
        public final u0 invoke() {
            r0 r0Var;
            Object obj;
            boolean z = true;
            if (!(!this.f332b.isEmpty())) {
                List list = this.f331a;
                if (list != null) {
                    Iterator it = list.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            obj = null;
                            break;
                        }
                        obj = it.next();
                        if (((r0) obj) instanceof a) {
                            break;
                        }
                    }
                    r0Var = (r0) obj;
                } else {
                    r0Var = null;
                }
                if (!(r0Var instanceof a)) {
                    r0Var = null;
                }
                a aVar = (a) r0Var;
                if (!(aVar != null ? aVar.c : false)) {
                    z = false;
                }
            }
            List<b> list2 = this.f332b;
            ArrayList arrayList = new ArrayList(kotlin.a.m.a((Iterable) list2, 10));
            for (b bVar : list2) {
                if (this.c.contains(bVar.f307b)) {
                    bVar = bVar.a(bVar.f307b, bVar.c, bVar.d, bVar.e, bVar.f, true);
                }
                arrayList.add(bVar);
            }
            List<r0> a2 = h.a(kotlin.a.m.a((Iterable) kotlin.a.m.a((Iterable) kotlin.a.m.a((Iterable) arrayList, (Comparator) new a(SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getCollator())), (Comparator) b.f334a), (Comparator) C0030c.f335a), z);
            List list3 = this.f331a;
            return new u0(list3, a2, b.a.a.a.a.a(t0.c, list3, a2, "DiffUtil.calculateDiff(R…create(oldRows, newRows))"));
        }
    }

    public final View a(int i2) {
        if (this.r == null) {
            this.r = new HashMap();
        }
        View view = (View) this.r.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i2);
        this.r.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.p.a(java.lang.Iterable, int):int
     arg types: [java.util.List<b.b.a.i.j1.b>, int]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.p.a(java.lang.Iterable, int):int */
    public final void a(List<b> list) {
        Set<String> set;
        b.b.a.h.a aVar = (b.b.a.h.a) SupercellId.INSTANCE.getSharedServices$supercellId_release().c().f1179a;
        if (aVar == null || (set = aVar.f107a) == null) {
            set = ad.f5212a;
        }
        Set<String> set2 = this.n;
        ArrayList arrayList = new ArrayList(kotlin.a.m.a((Iterable) list, 10));
        for (b bVar : list) {
            arrayList.add(bVar.f307b);
        }
        set2.addAll(arrayList);
        this.q.a(bb.f5389a);
    }

    public final View b() {
        return (ImageButton) a(R.id.back_button);
    }

    public final RecyclerView e() {
        return (RecyclerView) a(R.id.friendsList);
    }

    public final View f() {
        return (RelativeLayout) a(R.id.ingameFriendsToolbar);
    }

    public final float g() {
        return this.m;
    }

    public final List<View> h() {
        return kotlin.a.m.d(a(R.id.toolbarBackground), a(R.id.toolbarShadow));
    }

    public final void i() {
        this.p.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().h.b());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kotlin.d.b.j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_ingame_friends, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    public final void onDetach() {
        SupercellId.INSTANCE.getSharedServices$supercellId_release().c().a(new i.a.b(this.n));
        super.onDetach();
    }

    public final void onResume() {
        super.onResume();
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a("In-game Friends");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v7.widget.RecyclerView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.Context, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        kotlin.d.b.j.b(view, "view");
        super.onViewCreated(view, bundle);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().e.gameLocalizedName(new d(new WeakReference(this)));
        if (this.o == null) {
            RecyclerView recyclerView = (RecyclerView) a(R.id.friendsList);
            kotlin.d.b.j.a((Object) recyclerView, "friendsList");
            recyclerView.setVisibility(4);
            View a2 = a(R.id.progressBar);
            kotlin.d.b.j.a((Object) a2, "progressBar");
            a2.setVisibility(0);
        } else {
            RecyclerView recyclerView2 = (RecyclerView) a(R.id.friendsList);
            kotlin.d.b.j.a((Object) recyclerView2, "friendsList");
            recyclerView2.setVisibility(0);
            View a3 = a(R.id.progressBar);
            kotlin.d.b.j.a((Object) a3, "progressBar");
            a3.setVisibility(4);
        }
        Context context = view.getContext();
        kotlin.d.b.j.a((Object) context, "view.context");
        C0028c cVar = new C0028c(context, this);
        List<? extends r0> list = this.o;
        if (list == null) {
            list = ab.f5210a;
        }
        cVar.a(list);
        RecyclerView recyclerView3 = (RecyclerView) a(R.id.friendsList);
        kotlin.d.b.j.a((Object) recyclerView3, "friendsList");
        recyclerView3.setLayoutManager(new LinearLayoutManager(getContext()));
        RecyclerView recyclerView4 = (RecyclerView) a(R.id.friendsList);
        kotlin.d.b.j.a((Object) recyclerView4, "friendsList");
        recyclerView4.setAdapter(cVar);
        this.p.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().h.b());
    }

    /* renamed from: b.b.a.i.j1.c$c  reason: collision with other inner class name */
    public static final class C0028c extends s0 {

        /* renamed from: b  reason: collision with root package name */
        public final o1<Drawable> f310b = new o1<>(a.f311a);
        public final c c;

        /* renamed from: b.b.a.i.j1.c$c$a */
        public final class a extends kotlin.d.b.k implements kotlin.d.a.c<String, kotlin.d.a.b<? super Drawable, ? extends kotlin.m>, kotlin.m> {

            /* renamed from: a  reason: collision with root package name */
            public static final a f311a = new a();

            public a() {
                super(2);
            }

            public final Object invoke(Object obj, Object obj2) {
                String str = (String) obj;
                kotlin.d.a.b bVar = (kotlin.d.a.b) obj2;
                kotlin.d.b.j.b(str, "key");
                kotlin.d.b.j.b(bVar, "callback");
                SupercellId.INSTANCE.getSharedServices$supercellId_release().j.a(str, new d(bVar));
                return kotlin.m.f5330a;
            }
        }

        /* renamed from: b.b.a.i.j1.c$c$b */
        public final class b extends kotlin.d.b.k implements kotlin.d.a.b<Drawable, kotlin.m> {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ WeakReference f312a;

            /* renamed from: b  reason: collision with root package name */
            public final /* synthetic */ r0 f313b;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(WeakReference weakReference, C0028c cVar, r0 r0Var, int i) {
                super(1);
                this.f312a = weakReference;
                this.f313b = r0Var;
            }

            public final Object invoke(Object obj) {
                Drawable drawable = (Drawable) obj;
                kotlin.d.b.j.b(drawable, "bitmapDrawable");
                s0.a aVar = (s0.a) this.f312a.get();
                if (aVar != null && !(!kotlin.d.b.j.a(aVar.f1166b, this.f313b))) {
                    ((ImageView) aVar.c.findViewById(R.id.gameIconView)).setImageDrawable(drawable);
                }
                return kotlin.m.f5330a;
            }
        }

        /* renamed from: b.b.a.i.j1.c$c$c  reason: collision with other inner class name */
        public final class C0029c implements View.OnClickListener {

            /* renamed from: b  reason: collision with root package name */
            public final /* synthetic */ r0 f315b;

            public C0029c(r0 r0Var, int i) {
                this.f315b = r0Var;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
             arg types: [android.view.View, java.lang.String]
             candidates:
              kotlin.d.b.j.a(int, int):int
              kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
              kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
              kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
            public final void onClick(View view) {
                Object obj;
                b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "In-game Friends", "click", "Add", null, false, 24);
                c cVar = C0028c.this.c;
                b bVar = (b) this.f315b;
                List<? extends r0> list = cVar.o;
                if (list != null) {
                    ArrayList arrayList = new ArrayList();
                    for (r0 r0Var : list) {
                        if (!(r0Var instanceof b)) {
                            r0Var = null;
                        }
                        b bVar2 = (b) r0Var;
                        if (bVar2 != null) {
                            arrayList.add(bVar2);
                        }
                    }
                    ArrayList arrayList2 = new ArrayList();
                    for (Object next : arrayList) {
                        if (!((b) next).a(bVar)) {
                            arrayList2.add(next);
                        }
                    }
                    cVar.a(arrayList2);
                }
                if (bVar.f instanceof j.a.b) {
                    obj = SupercellId.INSTANCE.getSharedServices$supercellId_release().e().a(bVar.f307b);
                } else {
                    obj = SupercellId.INSTANCE.getSharedServices$supercellId_release().e().c(bVar.f307b);
                }
                nl.komponents.kovenant.c.m.b(obj, new e(new WeakReference(cVar), bVar));
                kotlin.d.b.j.a((Object) view, "it");
                view.setEnabled(false);
            }
        }

        /* renamed from: b.b.a.i.j1.c$c$d */
        public final class d implements View.OnClickListener {

            /* renamed from: b  reason: collision with root package name */
            public final /* synthetic */ r0 f317b;

            public d(r0 r0Var, int i) {
                this.f317b = r0Var;
            }

            public final void onClick(View view) {
                MainActivity a2 = b.b.a.b.a((Fragment) C0028c.this.c);
                if (a2 != null) {
                    b bVar = (b) this.f317b;
                    a2.a(new j.a(null, bVar.f307b, bVar.c, bVar.d, bVar.f, null, false, 64));
                }
            }
        }

        /* renamed from: b.b.a.i.j1.c$c$e */
        public final class e implements View.OnClickListener {
            public e(r0 r0Var, int i) {
            }

            public final void onClick(View view) {
                b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "In-game Friends", "click", "Add all", null, false, 24);
                MainActivity a2 = b.b.a.b.a((Fragment) C0028c.this.c);
                if (a2 != null) {
                    C0028c.this.a(a2);
                }
            }
        }

        /* renamed from: b.b.a.i.j1.c$c$f */
        public final class f implements View.OnClickListener {
            public f(r0 r0Var, int i) {
            }

            public final void onClick(View view) {
                b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "In-game Friends", "click", "Retry", null, false, 24);
                c cVar = C0028c.this.c;
                cVar.q.a(bb.f5389a);
                C0028c.this.c.i();
            }
        }

        /* renamed from: b.b.a.i.j1.c$c$g */
        public final class g extends kotlin.d.b.k implements kotlin.d.a.a<kotlin.m> {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ s0.a f320a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public g(s0.a aVar) {
                super(0);
                this.f320a = aVar;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
             arg types: [android.widget.ImageView, java.lang.String]
             candidates:
              kotlin.d.b.j.a(int, int):int
              kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
              kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
              kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
            public final Object invoke() {
                ImageView imageView = (ImageView) this.f320a.c.findViewById(R.id.newFriendIndicator);
                kotlin.d.b.j.a((Object) imageView, "containerView.newFriendIndicator");
                ImageView imageView2 = (ImageView) this.f320a.c.findViewById(R.id.friendImageView);
                kotlin.d.b.j.a((Object) imageView2, "containerView.friendImageView");
                kotlin.d.b.j.b(imageView, "indicator");
                kotlin.d.b.j.b(imageView2, "companion");
                float width = (((float) imageView2.getWidth()) / b.b.a.b.a(1)) / 3.3f;
                if (Float.compare(width, 14.0f) < 0) {
                    width = 14.0f;
                } else if (Float.compare(width, 24.0f) > 0) {
                    width = 24.0f;
                }
                float f = width * b.b.a.b.f16a;
                int width2 = imageView2.getWidth() / 2;
                ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
                if (!(layoutParams instanceof ConstraintLayout.LayoutParams)) {
                    layoutParams = null;
                }
                ConstraintLayout.LayoutParams layoutParams2 = (ConstraintLayout.LayoutParams) layoutParams;
                if (layoutParams2 == null || width2 != layoutParams2.circleRadius) {
                    imageView.post(new y(imageView, layoutParams2, f, width2));
                }
                return kotlin.m.f5330a;
            }
        }

        /* renamed from: b.b.a.i.j1.c$c$h */
        public final class h extends kotlin.d.b.k implements kotlin.d.a.b<r, kotlin.m> {
            public h() {
                super(1);
            }

            public final Object invoke(Object obj) {
                kotlin.d.b.j.b((r) obj, "it");
                b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "In-game Friends - Add all dialog", "click", "Yes", null, false, 24);
                C0028c.this.c.j();
                return kotlin.m.f5330a;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.content.res.Resources, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public C0028c(Context context, c cVar) {
            kotlin.d.b.j.b(context, "context");
            kotlin.d.b.j.b(cVar, "fragment");
            this.c = cVar;
            Resources resources = context.getResources();
            kotlin.d.b.j.a((Object) resources, "context.resources");
            b.b.a.b.b(resources);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.TextView, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.Button, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.ImageView, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void a(s0.a aVar, int i, r0 r0Var) {
            Resources resources;
            kotlin.d.b.j.b(aVar, "holder");
            kotlin.d.b.j.b(r0Var, "item");
            int i2 = 8;
            if (r0Var instanceof b) {
                b.b.a.b.a((LinearLayout) aVar.c.findViewById(R.id.friendContainer), b.b.a.b.b(this.f1164a, i), b.b.a.b.a(this.f1164a, i), 0, 0);
                Context context = aVar.c.getContext();
                if (!(context == null || (resources = context.getResources()) == null)) {
                    k0.f1078b.a(((b) r0Var).d, (ImageView) aVar.c.findViewById(R.id.friendImageView), resources);
                }
                TextView textView = (TextView) aVar.c.findViewById(R.id.friendNameLabel);
                kotlin.d.b.j.a((Object) textView, "containerView.friendNameLabel");
                b bVar = (b) r0Var;
                String str = bVar.c;
                if (str == null) {
                    str = b.b.a.j.r.f1160a.a(bVar.f307b);
                }
                textView.setText(str);
                ((TextView) aVar.c.findViewById(R.id.friendNameLabel)).setTextColor(ContextCompat.getColor(aVar.c.getContext(), bVar.c == null ? R.color.gray40 : R.color.black));
                WeakReference weakReference = new WeakReference(aVar);
                o1<Drawable> o1Var = this.f310b;
                StringBuilder a2 = b.a.a.a.a.a("AppIcon_");
                a2.append(SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getGame());
                a2.append(".png");
                o1Var.a(a2.toString(), new b(weakReference, this, r0Var, i));
                TextView textView2 = (TextView) aVar.c.findViewById(R.id.friendPlayingNameLabel);
                kotlin.d.b.j.a((Object) textView2, "containerView.friendPlayingNameLabel");
                textView2.setText(bVar.e);
                Button button = (Button) aVar.c.findViewById(R.id.addButton);
                kotlin.d.b.j.a((Object) button, "containerView.addButton");
                button.setEnabled(true);
                ((Button) aVar.c.findViewById(R.id.addButton)).setOnClickListener(new C0029c(r0Var, i));
                ((LinearLayout) aVar.c.findViewById(R.id.friendContainer)).setOnClickListener(new d(r0Var, i));
                ImageView imageView = (ImageView) aVar.c.findViewById(R.id.newFriendIndicator);
                kotlin.d.b.j.a((Object) imageView, "containerView.newFriendIndicator");
                if (!bVar.g) {
                    i2 = 0;
                }
                imageView.setVisibility(i2);
            } else if (r0Var instanceof a) {
                a aVar2 = (a) r0Var;
                int i3 = aVar2.f305b;
                if (i3 > 0) {
                    WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) aVar.c.findViewById(R.id.addAllFriendsButton);
                    if (widthAdjustingMultilineButton != null) {
                        widthAdjustingMultilineButton.setVisibility(0);
                        widthAdjustingMultilineButton.setOnClickListener(new e(r0Var, i));
                    }
                    ImageView imageView2 = (ImageView) aVar.c.findViewById(R.id.allSentIconView);
                    if (imageView2 != null) {
                        imageView2.setVisibility(4);
                    }
                    TextView textView3 = (TextView) aVar.c.findViewById(R.id.friendsToAddView);
                    if (textView3 != null) {
                        b.b.a.i.x1.j.a(textView3, "account_ingame_friend_add_all_title", kotlin.k.a("count", String.valueOf(i3)));
                        return;
                    }
                    return;
                }
                WidthAdjustingMultilineButton widthAdjustingMultilineButton2 = (WidthAdjustingMultilineButton) aVar.c.findViewById(R.id.addAllFriendsButton);
                if (widthAdjustingMultilineButton2 != null) {
                    widthAdjustingMultilineButton2.setVisibility(8);
                }
                ImageView imageView3 = (ImageView) aVar.c.findViewById(R.id.allSentIconView);
                if (imageView3 != null) {
                    imageView3.setVisibility(0);
                }
                TextView textView4 = (TextView) aVar.c.findViewById(R.id.friendsToAddView);
                if (textView4 != null) {
                    b.b.a.i.x1.j.a(textView4, aVar2.c ? "account_ingame_friend_all_added_title" : "account_ingame_friend_none_to_add_title", (kotlin.d.a.b) null, 2);
                }
            } else if (r0Var instanceof o) {
                LinearLayout linearLayout = (LinearLayout) aVar.c.findViewById(R.id.errorContainer);
                ViewGroup.MarginLayoutParams d2 = q1.d(linearLayout);
                if (d2 != null) {
                    d2.topMargin = 0;
                }
                linearLayout.requestLayout();
                ((WidthAdjustingMultilineButton) aVar.c.findViewById(R.id.errorRetryButton)).setOnClickListener(new f(r0Var, i));
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: b.b.a.j.s0.onCreateViewHolder(android.view.ViewGroup, int):b.b.a.j.s0$a
         arg types: [android.view.ViewGroup, int]
         candidates:
          b.b.a.i.j1.c.c.onCreateViewHolder(android.view.ViewGroup, int):android.support.v7.widget.RecyclerView$ViewHolder
          b.b.a.j.s0.onCreateViewHolder(android.view.ViewGroup, int):android.support.v7.widget.RecyclerView$ViewHolder
          android.support.v7.widget.RecyclerView.Adapter.onCreateViewHolder(android.view.ViewGroup, int):VH
          b.b.a.j.s0.onCreateViewHolder(android.view.ViewGroup, int):b.b.a.j.s0$a */
        public final s0.a onCreateViewHolder(ViewGroup viewGroup, int i) {
            kotlin.d.b.j.b(viewGroup, "parent");
            s0.a onCreateViewHolder = super.onCreateViewHolder(viewGroup, i);
            if (!(((ImageView) onCreateViewHolder.c.findViewById(R.id.newFriendIndicator)) == null || ((ImageView) onCreateViewHolder.c.findViewById(R.id.friendImageView)) == null)) {
                q1.a(onCreateViewHolder.c, new g(onCreateViewHolder));
            }
            return onCreateViewHolder;
        }

        public final void a(MainActivity mainActivity) {
            r a2 = r.a.a(r.g, "account_ingame_friend_dialog_add_all_heading", "account_ingame_friend_dialog_add_all_ok", "account_ingame_friend_dialog_add_all_cancel", kotlin.k.a("game", SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getInstantGameLocalizedName()), false, 16);
            a2.e = new h();
            mainActivity.a(a2, "popupDialog");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.p.a(java.lang.Iterable, int):int
     arg types: [java.util.List, int]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.p.a(java.lang.Iterable, int):int */
    public final void j() {
        bw bwVar;
        bw bwVar2;
        List<? extends r0> list = this.o;
        if (list != null) {
            ArrayList arrayList = new ArrayList();
            for (r0 r0Var : list) {
                if (!(r0Var instanceof b)) {
                    r0Var = null;
                }
                b bVar = (b) r0Var;
                if (bVar != null) {
                    arrayList.add(bVar);
                }
            }
            a(ab.f5210a);
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (Object next : arrayList) {
                Boolean valueOf = Boolean.valueOf(((b) next).f instanceof j.a.b);
                Object obj = linkedHashMap.get(valueOf);
                if (obj == null) {
                    obj = new ArrayList();
                    linkedHashMap.put(valueOf, obj);
                }
                ((List) obj).add(next);
            }
            kotlin.h a2 = kotlin.k.a(linkedHashMap.get(true), linkedHashMap.get(false));
            List<b> list2 = (List) a2.f5262a;
            List<b> list3 = (List) a2.f5263b;
            if (list2 != null) {
                q e2 = SupercellId.INSTANCE.getSharedServices$supercellId_release().e();
                ArrayList arrayList2 = new ArrayList(kotlin.a.m.a((Iterable) list2, 10));
                for (b bVar2 : list2) {
                    arrayList2.add(bVar2.f307b);
                }
                bwVar = b.b.a.b.a(bc.a(e2.a(arrayList2), i.f327a), (ao) null, new j(list2), 1);
            } else {
                bwVar = bb.f5389a;
            }
            if (list3 != null) {
                q e3 = SupercellId.INSTANCE.getSharedServices$supercellId_release().e();
                ArrayList arrayList3 = new ArrayList(kotlin.a.m.a((Iterable) list3, 10));
                for (b bVar3 : list3) {
                    arrayList3.add(bVar3.f307b);
                }
                bwVar2 = b.b.a.b.a(bc.a(e3.b(arrayList3), k.f329a), (ao) null, new l(list3), 1);
            } else {
                bwVar2 = bb.f5389a;
            }
            nl.komponents.kovenant.c.m.a(bb.f5389a, new a(new WeakReference(this), arrayList));
        }
    }
}
