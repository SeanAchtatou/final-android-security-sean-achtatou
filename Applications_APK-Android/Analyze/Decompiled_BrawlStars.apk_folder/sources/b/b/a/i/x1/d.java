package b.b.a.i.x1;

import android.content.Context;
import com.facebook.share.internal.ShareConstants;
import java.io.File;
import java.io.IOException;
import kotlin.d.b.g;
import kotlin.d.b.j;
import kotlin.io.f;

public final class d {

    /* renamed from: b  reason: collision with root package name */
    public static final a f911b = new a(null);

    /* renamed from: a  reason: collision with root package name */
    public final Context f912a;

    public static final class a {
        public /* synthetic */ a(g gVar) {
        }

        public final File a(Context context) {
            j.b(context, "context");
            File a2 = a.d.a(context);
            if (a2 != null) {
                return new File(a2, "Android/Audio");
            }
            return null;
        }
    }

    public d(Context context) {
        j.b(context, "context");
        this.f912a = context;
    }

    public final File a(String str) {
        j.b(str, "assetName");
        File a2 = f911b.a(this.f912a);
        if (a2 != null) {
            return new File(a2, str);
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.io.File, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final boolean a(String str, byte[] bArr) {
        j.b(str, "assetName");
        j.b(bArr, ShareConstants.WEB_DIALOG_PARAM_DATA);
        File a2 = a(str);
        if (a2 == null) {
            return false;
        }
        File parentFile = a2.getParentFile();
        j.a((Object) parentFile, "directory");
        if (!parentFile.isDirectory() && !parentFile.mkdirs()) {
            return false;
        }
        try {
            f.a(a2, bArr);
            return true;
        } catch (IOException e) {
            a2.delete();
            d.class.getName();
            StringBuilder a3 = b.a.a.a.a.a("Storing audio failed ");
            a3.append(e.getMessage());
            a3.toString();
            return false;
        }
    }
}
