package b.b.a.i;

import com.supercell.id.ui.h;
import com.supercell.id.view.RootFrameLayout;

public final class q0 implements RootFrameLayout.a {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ h f516a;

    public q0(h hVar) {
        this.f516a = hVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001b, code lost:
        if (r1 == r0.right) goto L_0x002b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.graphics.Rect r4) {
        /*
            r3 = this;
            java.lang.String r0 = "systemWindowInsets"
            kotlin.d.b.j.b(r4, r0)
            com.supercell.id.ui.h r0 = r3.f516a
            com.supercell.id.ui.MainActivity r0 = r0.f4892a
            android.graphics.Rect r0 = r0.h
            if (r0 == 0) goto L_0x002b
            int r1 = r4.left
            if (r0 == 0) goto L_0x001d
            int r2 = r0.left
            if (r1 != r2) goto L_0x001d
            int r1 = r4.right
            if (r0 == 0) goto L_0x001d
            int r0 = r0.right
            if (r1 == r0) goto L_0x002b
        L_0x001d:
            com.supercell.id.ui.h r0 = r3.f516a
            com.supercell.id.ui.MainActivity r0 = r0.f4892a
            com.supercell.id.ui.MainActivity.e(r0)
            com.supercell.id.ui.h r0 = r3.f516a
            com.supercell.id.ui.MainActivity r0 = r0.f4892a
            com.supercell.id.ui.MainActivity.c(r0)
        L_0x002b:
            com.supercell.id.ui.h r0 = r3.f516a
            com.supercell.id.ui.MainActivity r0 = r0.f4892a
            r0.h = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.i.q0.a(android.graphics.Rect):void");
    }
}
