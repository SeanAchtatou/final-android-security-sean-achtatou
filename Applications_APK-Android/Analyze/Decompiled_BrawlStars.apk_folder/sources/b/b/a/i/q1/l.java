package b.b.a.i.q1;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ScrollView;
import b.b.a.i.e;
import b.b.a.i.u;
import b.b.a.i.v;
import b.b.a.j.j0;
import b.b.a.j.k0;
import b.b.a.j.q1;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.WidthAdjustingMultilineButton;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import kotlin.TypeCastException;
import kotlin.a.g;
import kotlin.d.b.j;
import kotlin.j.t;
import kotlin.m;
import nl.komponents.kovenant.bw;

public final class l extends b implements u {
    public String c = "";
    public b.b.a.d.b d;
    public boolean e;
    public HashMap f;

    public final class a implements View.OnClickListener {
        public a() {
        }

        public final void onClick(View view) {
            l lVar = l.this;
            b.b.a.d.b bVar = lVar.d;
            if (bVar != null) {
                MainActivity a2 = b.b.a.b.a(lVar);
                if (a2 != null) {
                    a2.a(bVar, (kotlin.d.a.b<? super e, m>) null);
                    return;
                }
                return;
            }
            lVar.a(true);
            bw a3 = j0.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f(), lVar.c, null, null, 6);
            WeakReference weakReference = new WeakReference(lVar);
            nl.komponents.kovenant.c.m.a(a3, new m(weakReference));
            nl.komponents.kovenant.c.m.b(a3, new n(weakReference));
        }
    }

    public final class b implements TextWatcher {
        public b() {
        }

        public final void afterTextChanged(Editable editable) {
            l.this.h();
        }

        public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    public final class c implements View.OnFocusChangeListener {

        public final class a implements Runnable {
            public a() {
            }

            public final void run() {
                MainActivity a2 = b.b.a.b.a(l.this);
                if (a2 != null) {
                    b.b.a.b.a((Activity) a2);
                }
            }
        }

        public c() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onFocusChange(View view, boolean z) {
            if (z) {
                ScrollView scrollView = (ScrollView) l.this.a(R.id.nickname_scrollview);
                if (scrollView != null) {
                    j.a((Object) view, "v");
                    q1.a(scrollView, view);
                    return;
                }
                return;
            }
            view.post(new a());
        }
    }

    public final View a(int i) {
        if (this.f == null) {
            this.f = new HashMap();
        }
        View view = (View) this.f.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.f.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.f;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public final void a(v vVar) {
        j.b(vVar, "dialog");
        this.e = false;
        g();
    }

    public final void a(boolean z) {
        this.e = z;
        g();
    }

    public final void c() {
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a("Onboarding Nickname");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.j.q1.a(android.support.v7.widget.AppCompatButton, boolean):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, boolean]
     candidates:
      b.b.a.j.q1.a(android.view.View, int):android.view.View
      b.b.a.j.q1.a(android.support.v4.widget.NestedScrollView, int):void
      b.b.a.j.q1.a(android.support.v4.widget.NestedScrollView, android.view.View):void
      b.b.a.j.q1.a(android.view.View, long):void
      b.b.a.j.q1.a(android.view.View, b.b.a.j.z):void
      b.b.a.j.q1.a(android.view.View, kotlin.d.a.a<kotlin.m>):void
      b.b.a.j.q1.a(android.widget.ScrollView, int):void
      b.b.a.j.q1.a(android.widget.ScrollView, android.view.View):void
      b.b.a.j.q1.a(android.support.v7.widget.AppCompatButton, boolean):void */
    public final void g() {
        WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) a(R.id.nickname_continue_button);
        if (widthAdjustingMultilineButton != null) {
            q1.a((AppCompatButton) widthAdjustingMultilineButton, this.d != null);
        }
        WidthAdjustingMultilineButton widthAdjustingMultilineButton2 = (WidthAdjustingMultilineButton) a(R.id.nickname_continue_button);
        if (widthAdjustingMultilineButton2 != null) {
            widthAdjustingMultilineButton2.setEnabled(true ^ this.e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.EditText, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void h() {
        EditText editText = (EditText) a(R.id.nickname_edit_text);
        j.a((Object) editText, "nickname_edit_text");
        String obj = editText.getText().toString();
        if (obj != null) {
            this.c = t.b(obj).toString();
            this.d = k0.f1078b.a(this.c);
            g();
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_onboarding_nickname_page, viewGroup, false);
    }

    public final void onDestroyView() {
        MainActivity a2 = b.b.a.b.a(this);
        if (a2 != null) {
            a2.b(this);
        }
        super.onDestroyView();
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.EditText, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.j.a(java.lang.Object[], java.lang.Object[]):T[]
     arg types: [android.text.InputFilter[], android.text.InputFilter$LengthFilter[]]
     candidates:
      kotlin.a.k.a(java.lang.Object[], java.util.Collection):C
      kotlin.a.k.a(char[], char):boolean
      kotlin.a.j.a(java.lang.Object[], java.util.Comparator):void
      kotlin.a.j.a(java.lang.Object[], java.lang.Object):T[]
      kotlin.a.j.a(java.lang.Object[], java.lang.Object[]):T[] */
    public final void onViewCreated(View view, Bundle bundle) {
        j.b(view, "view");
        super.onViewCreated(view, bundle);
        MainActivity a2 = b.b.a.b.a(this);
        if (a2 != null) {
            a2.a(this);
        }
        Integer c2 = k0.f1078b.c();
        boolean z = false;
        if (c2 != null) {
            int intValue = c2.intValue();
            EditText editText = (EditText) a(R.id.nickname_edit_text);
            j.a((Object) editText, "nickname_edit_text");
            editText.setFilters((InputFilter[]) g.a((Object[]) editText.getFilters(), (Object[]) new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(intValue)}));
        }
        EditText editText2 = (EditText) a(R.id.nickname_edit_text);
        a f2 = f();
        String str = null;
        String str2 = f2 != null ? f2.m : null;
        if (str2 == null || str2.length() == 0) {
            z = true;
        }
        if (!z) {
            a f3 = f();
            if (f3 != null) {
                str = f3.m;
            }
        } else {
            str = SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getGameAccountNickname();
        }
        editText2.setText(str);
        h();
        WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) a(R.id.nickname_continue_button);
        if (widthAdjustingMultilineButton != null) {
            widthAdjustingMultilineButton.setOnClickListener(new a());
        }
        ((EditText) a(R.id.nickname_edit_text)).addTextChangedListener(new b());
        ((EditText) a(R.id.nickname_edit_text)).setOnFocusChangeListener(new c());
    }
}
