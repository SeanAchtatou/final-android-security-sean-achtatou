package b.b.a.i.j1;

import b.a.a.a.a;
import b.b.a.h.j;
import b.b.a.j.r0;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.supercell.id.R;

public final class b implements r0 {

    /* renamed from: a  reason: collision with root package name */
    public final int f306a = R.layout.fragment_ingame_friends_list_item_friend;

    /* renamed from: b  reason: collision with root package name */
    public final String f307b;
    public final String c;
    public final String d;
    public final String e;
    public final j f;
    public final boolean g;

    public b(String str, String str2, String str3, String str4, j jVar, boolean z) {
        kotlin.d.b.j.b(str, "scid");
        kotlin.d.b.j.b(str4, "gameNickname");
        kotlin.d.b.j.b(jVar, "relationship");
        this.f307b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = jVar;
        this.g = z;
    }

    public final int a() {
        return this.f306a;
    }

    public final b a(String str, String str2, String str3, String str4, j jVar, boolean z) {
        kotlin.d.b.j.b(str, "scid");
        kotlin.d.b.j.b(str4, "gameNickname");
        kotlin.d.b.j.b(jVar, "relationship");
        return new b(str, str2, str3, str4, jVar, z);
    }

    public final boolean a(r0 r0Var) {
        kotlin.d.b.j.b(r0Var, FacebookRequestErrorClassification.KEY_OTHER);
        return (r0Var instanceof b) && kotlin.d.b.j.a(((b) r0Var).f307b, this.f307b);
    }

    public final boolean b(r0 r0Var) {
        kotlin.d.b.j.b(r0Var, FacebookRequestErrorClassification.KEY_OTHER);
        if (!(r0Var instanceof b)) {
            return false;
        }
        b bVar = (b) r0Var;
        return kotlin.d.b.j.a(this.c, bVar.c) && kotlin.d.b.j.a(this.e, bVar.e) && this.g == bVar.g;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof b) {
                b bVar = (b) obj;
                if (kotlin.d.b.j.a((Object) this.f307b, (Object) bVar.f307b) && kotlin.d.b.j.a((Object) this.c, (Object) bVar.c) && kotlin.d.b.j.a((Object) this.d, (Object) bVar.d) && kotlin.d.b.j.a((Object) this.e, (Object) bVar.e) && kotlin.d.b.j.a(this.f, bVar.f)) {
                    if (this.g == bVar.g) {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        String str = this.f307b;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.c;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.d;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.e;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        j jVar = this.f;
        if (jVar != null) {
            i = jVar.hashCode();
        }
        int i2 = (hashCode4 + i) * 31;
        boolean z = this.g;
        if (z) {
            z = true;
        }
        return i2 + (z ? 1 : 0);
    }

    public final String toString() {
        StringBuilder a2 = a.a("FriendRow(scid=");
        a2.append(this.f307b);
        a2.append(", name=");
        a2.append(this.c);
        a2.append(", avatarUrl=");
        a2.append(this.d);
        a2.append(", gameNickname=");
        a2.append(this.e);
        a2.append(", relationship=");
        a2.append(this.f);
        a2.append(", seen=");
        a2.append(this.g);
        a2.append(")");
        return a2.toString();
    }
}
