package b.b.a.i;

import android.view.View;
import android.widget.FrameLayout;
import com.supercell.id.R;
import com.supercell.id.ui.MainActivity;
import kotlin.d.a.a;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class p0 extends k implements a<m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ MainActivity f470a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public p0(MainActivity mainActivity) {
        super(0);
        this.f470a = mainActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.FrameLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final Object invoke() {
        FrameLayout frameLayout = (FrameLayout) this.f470a.a(R.id.content);
        j.a((Object) frameLayout, "content");
        frameLayout.setVisibility(0);
        View a2 = this.f470a.a(R.id.dimmer);
        j.a((Object) a2, "dimmer");
        a2.setVisibility(0);
        MainActivity.e(this.f470a);
        MainActivity.c(this.f470a);
        return m.f5330a;
    }
}
