package b.b.a.i;

import com.supercell.id.ui.MainActivity;
import kotlin.d.a.a;
import kotlin.d.b.k;
import kotlin.m;

public final class m0 extends k implements a<m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ MainActivity f376a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m0(MainActivity mainActivity) {
        super(0);
        this.f376a = mainActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.g.i.a(boolean, boolean):void
     arg types: [boolean, int]
     candidates:
      b.b.a.g.i.a(b.b.a.g.i, int):void
      b.b.a.g.i.a(boolean, float):void
      b.b.a.g.i.a(boolean, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.FrameLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.RootFrameLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0158, code lost:
        if (r3 != (r10.f376a.j() == 0 ? 0.0f : b.b.a.b.a(12))) goto L_0x015a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invoke() {
        /*
            r10 = this;
            com.supercell.id.ui.MainActivity r0 = r10.f376a
            b.b.a.i.b r0 = com.supercell.id.ui.MainActivity.b(r0)
            b.b.a.i.b$a r0 = r0.a()
            com.supercell.id.ui.MainActivity r1 = r10.f376a
            b.b.a.g.i r1 = r1.g()
            r2 = 0
            java.lang.String r3 = "resources"
            if (r0 == 0) goto L_0x0023
            com.supercell.id.ui.MainActivity r4 = r10.f376a
            android.content.res.Resources r4 = r4.getResources()
            kotlin.d.b.j.a(r4, r3)
            boolean r4 = r0.d(r4)
            goto L_0x0024
        L_0x0023:
            r4 = 0
        L_0x0024:
            r1.c(r4)
            com.supercell.id.ui.MainActivity r1 = r10.f376a
            b.b.a.g.i r1 = r1.g()
            r4 = 1
            if (r0 == 0) goto L_0x003e
            com.supercell.id.ui.MainActivity r5 = r10.f376a
            android.content.res.Resources r5 = r5.getResources()
            kotlin.d.b.j.a(r5, r3)
            boolean r5 = r0.c(r5)
            goto L_0x003f
        L_0x003e:
            r5 = 1
        L_0x003f:
            r1.a(r5, r4)
            com.supercell.id.ui.MainActivity r1 = r10.f376a
            android.content.res.Resources r1 = r1.getResources()
            kotlin.d.b.j.a(r1, r3)
            boolean r1 = b.b.a.b.c(r1)
            r5 = 2
            r6 = 0
            java.lang.String r7 = "content"
            if (r1 == 0) goto L_0x00b1
            com.supercell.id.ui.MainActivity r1 = r10.f376a
            int r8 = com.supercell.id.R.id.content
            android.view.View r1 = r1.a(r8)
            android.widget.FrameLayout r1 = (android.widget.FrameLayout) r1
            kotlin.d.b.j.a(r1, r7)
            int r1 = r1.getWidth()
            if (r0 == 0) goto L_0x0082
            boolean r0 = r0.f()
            if (r0 != r4) goto L_0x0082
            com.supercell.id.ui.MainActivity r0 = r10.f376a
            int r4 = com.supercell.id.R.id.root_layout
            android.view.View r0 = r0.a(r4)
            com.supercell.id.view.RootFrameLayout r0 = (com.supercell.id.view.RootFrameLayout) r0
            java.lang.String r4 = "root_layout"
            kotlin.d.b.j.a(r0, r4)
            int r0 = r0.getWidth()
            goto L_0x008e
        L_0x0082:
            com.supercell.id.ui.MainActivity r0 = r10.f376a
            android.content.res.Resources r0 = r0.getResources()
            int r4 = com.supercell.id.R.dimen.tablet_main_content_width
            int r0 = r0.getDimensionPixelSize(r4)
        L_0x008e:
            com.supercell.id.ui.MainActivity r4 = r10.f376a
            android.animation.Animator r4 = r4.l
            if (r4 == 0) goto L_0x0097
            r4.cancel()
        L_0x0097:
            com.supercell.id.ui.MainActivity r4 = r10.f376a
            r4.l = r6
            float[] r8 = new float[r5]
            r8 = {0, 1065353216} // fill-array
            android.animation.ValueAnimator r8 = android.animation.ValueAnimator.ofFloat(r8)
            b.b.a.i.h0 r9 = new b.b.a.i.h0
            r9.<init>(r8, r10, r1, r0)
            r8.addUpdateListener(r9)
            r8.start()
            r4.l = r8
        L_0x00b1:
            com.supercell.id.ui.MainActivity r0 = r10.f376a
            android.animation.Animator r0 = r0.k
            if (r0 == 0) goto L_0x00ba
            r0.cancel()
        L_0x00ba:
            com.supercell.id.ui.MainActivity r0 = r10.f376a
            r0.k = r6
            float[] r1 = new float[r5]
            r1 = {0, 1065353216} // fill-array
            android.animation.ValueAnimator r1 = android.animation.ValueAnimator.ofFloat(r1)
            r4 = 350(0x15e, double:1.73E-321)
            r1.setDuration(r4)
            android.view.animation.Interpolator r4 = b.b.a.f.a.f62a
            r1.setInterpolator(r4)
            com.supercell.id.ui.MainActivity r4 = r10.f376a
            android.content.res.Resources r4 = r4.getResources()
            kotlin.d.b.j.a(r4, r3)
            boolean r3 = b.b.a.b.b(r4)
            java.lang.String r4 = "top_area"
            if (r3 == 0) goto L_0x0121
            com.supercell.id.ui.MainActivity r3 = r10.f376a
            int r5 = com.supercell.id.R.id.content
            android.view.View r3 = r3.a(r5)
            android.widget.FrameLayout r3 = (android.widget.FrameLayout) r3
            kotlin.d.b.j.a(r3, r7)
            android.view.ViewGroup$MarginLayoutParams r3 = b.b.a.j.q1.d(r3)
            if (r3 == 0) goto L_0x00f7
            int r2 = r3.leftMargin
        L_0x00f7:
            com.supercell.id.ui.MainActivity r3 = r10.f376a
            int r3 = r3.l()
            if (r2 == r3) goto L_0x0107
            b.b.a.i.i0 r3 = new b.b.a.i.i0
            r3.<init>(r1, r2, r10)
            r1.addUpdateListener(r3)
        L_0x0107:
            com.supercell.id.ui.MainActivity r2 = r10.f376a
            int r3 = com.supercell.id.R.id.top_area
            android.view.View r2 = r2.a(r3)
            android.widget.FrameLayout r2 = (android.widget.FrameLayout) r2
            kotlin.d.b.j.a(r2, r4)
            int r2 = r2.getWidth()
            b.b.a.i.j0 r3 = new b.b.a.i.j0
            r3.<init>(r1, r2, r10)
            r1.addUpdateListener(r3)
            goto L_0x017b
        L_0x0121:
            com.supercell.id.ui.MainActivity r3 = r10.f376a
            int r5 = com.supercell.id.R.id.content
            android.view.View r3 = r3.a(r5)
            android.widget.FrameLayout r3 = (android.widget.FrameLayout) r3
            kotlin.d.b.j.a(r3, r7)
            android.view.ViewGroup$MarginLayoutParams r3 = b.b.a.j.q1.d(r3)
            if (r3 == 0) goto L_0x0136
            int r2 = r3.topMargin
        L_0x0136:
            com.supercell.id.ui.MainActivity r3 = r10.f376a
            b.b.a.g.i r3 = r3.g()
            float r3 = r3.h
            com.supercell.id.ui.MainActivity r5 = r10.f376a
            int r5 = r5.j()
            if (r2 != r5) goto L_0x015a
            com.supercell.id.ui.MainActivity r5 = r10.f376a
            int r5 = r5.j()
            if (r5 != 0) goto L_0x0150
            r5 = 0
            goto L_0x0156
        L_0x0150:
            r5 = 12
            float r5 = b.b.a.b.a(r5)
        L_0x0156:
            int r5 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r5 == 0) goto L_0x0162
        L_0x015a:
            b.b.a.i.k0 r5 = new b.b.a.i.k0
            r5.<init>(r1, r2, r3, r10)
            r1.addUpdateListener(r5)
        L_0x0162:
            com.supercell.id.ui.MainActivity r2 = r10.f376a
            int r3 = com.supercell.id.R.id.top_area
            android.view.View r2 = r2.a(r3)
            android.widget.FrameLayout r2 = (android.widget.FrameLayout) r2
            kotlin.d.b.j.a(r2, r4)
            int r2 = r2.getHeight()
            b.b.a.i.l0 r3 = new b.b.a.i.l0
            r3.<init>(r1, r2, r10)
            r1.addUpdateListener(r3)
        L_0x017b:
            r1.start()
            r0.k = r1
            kotlin.m r0 = kotlin.m.f5330a
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.i.m0.invoke():java.lang.Object");
    }
}
