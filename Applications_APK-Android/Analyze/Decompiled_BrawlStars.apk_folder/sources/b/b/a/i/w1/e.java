package b.b.a.i.w1;

import com.supercell.id.ui.MainActivity;
import java.lang.ref.WeakReference;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class e extends k implements b<Exception, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f837a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public e(WeakReference weakReference) {
        super(1);
        this.f837a = weakReference;
    }

    public final Object invoke(Object obj) {
        MainActivity a2;
        Exception exc = (Exception) obj;
        j.b(exc, "it");
        f fVar = (f) this.f837a.get();
        if (!(fVar == null || (a2 = b.b.a.b.a(fVar)) == null)) {
            a2.a(exc, (b<? super b.b.a.i.e, m>) null);
        }
        return m.f5330a;
    }
}
