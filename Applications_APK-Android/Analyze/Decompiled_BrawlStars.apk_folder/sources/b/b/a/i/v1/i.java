package b.b.a.i.v1;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.animation.SpringAnimation;
import android.support.animation.SpringForce;
import android.support.v4.app.ShareCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.widget.FrameLayout;
import android.widget.TextView;
import b.b.a.i.c1;
import b.b.a.i.r;
import b.b.a.j.q1;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import java.util.HashMap;
import kotlin.d.b.g;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public class i extends b.b.a.i.e {
    public static final a h = new a(null);
    public final long e = 70;
    public final boolean f = true;
    public HashMap g;

    public static final class a {
        public /* synthetic */ a(g gVar) {
        }

        public final i a(Rect rect, String str, String str2, String str3, String str4) {
            j.b(rect, "rect");
            j.b(str, "scid");
            i iVar = new i();
            Bundle arguments = iVar.getArguments();
            if (arguments == null) {
                arguments = new Bundle();
            }
            arguments.putParcelable("rect", rect);
            arguments.putString("scid", str);
            arguments.putString("name", str2);
            arguments.putString("qrCodeUrl", str3);
            arguments.putString("universalLink", str4);
            iVar.setArguments(arguments);
            return iVar;
        }
    }

    public final class b extends k implements kotlin.d.a.a<m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ FrameLayout f782a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ i f783b;
        public final /* synthetic */ View c;
        public final /* synthetic */ Rect d;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(FrameLayout frameLayout, i iVar, View view, Rect rect) {
            super(0);
            this.f782a = frameLayout;
            this.f783b = iVar;
            this.c = view;
            this.d = rect;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.FrameLayout, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.support.animation.SpringForce, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final Object invoke() {
            if (this.f783b.isAdded()) {
                FrameLayout frameLayout = this.f782a;
                j.a((Object) frameLayout, "it");
                ViewGroup.LayoutParams layoutParams = frameLayout.getLayoutParams();
                if (!(layoutParams instanceof FrameLayout.LayoutParams)) {
                    layoutParams = null;
                }
                FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) layoutParams;
                if (layoutParams2 != null) {
                    layoutParams2.gravity = 49;
                }
                q1.g(this.c);
                FrameLayout frameLayout2 = this.f782a;
                j.a((Object) frameLayout2, "it");
                FrameLayout frameLayout3 = this.f782a;
                j.a((Object) frameLayout3, "it");
                frameLayout2.setPivotX(((float) frameLayout3.getWidth()) * 0.5f);
                FrameLayout frameLayout4 = this.f782a;
                j.a((Object) frameLayout4, "it");
                frameLayout4.setPivotY(0.0f);
                FrameLayout frameLayout5 = this.f782a;
                j.a((Object) frameLayout5, "it");
                float exactCenterX = this.d.exactCenterX();
                FrameLayout frameLayout6 = this.f782a;
                j.a((Object) frameLayout6, "it");
                frameLayout5.setTranslationX(exactCenterX - q1.b(frameLayout6).exactCenterX());
                FrameLayout frameLayout7 = this.f782a;
                j.a((Object) frameLayout7, "it");
                int i = this.d.bottom;
                FrameLayout frameLayout8 = this.f782a;
                j.a((Object) frameLayout8, "it");
                frameLayout7.setTranslationY(b.b.a.b.a(15) + ((float) (i - q1.b(frameLayout8).top)));
                FrameLayout frameLayout9 = this.f782a;
                j.a((Object) frameLayout9, "it");
                frameLayout9.setScaleX(0.7f);
                FrameLayout frameLayout10 = this.f782a;
                j.a((Object) frameLayout10, "it");
                frameLayout10.setScaleY(0.7f);
                SpringAnimation springAnimation = new SpringAnimation(this.f782a, SpringAnimation.SCALE_X, 1.0f);
                SpringForce spring = springAnimation.getSpring();
                j.a((Object) spring, "spring");
                spring.setDampingRatio(0.5f);
                SpringForce spring2 = springAnimation.getSpring();
                j.a((Object) spring2, "spring");
                spring2.setStiffness(600.0f);
                springAnimation.start();
                SpringAnimation springAnimation2 = new SpringAnimation(this.f782a, SpringAnimation.SCALE_Y, 1.0f);
                SpringForce spring3 = springAnimation2.getSpring();
                j.a((Object) spring3, "spring");
                spring3.setDampingRatio(0.5f);
                SpringForce spring4 = springAnimation2.getSpring();
                j.a((Object) spring4, "spring");
                spring4.setStiffness(600.0f);
                springAnimation2.start();
                FrameLayout frameLayout11 = this.f782a;
                j.a((Object) frameLayout11, "it");
                frameLayout11.setAlpha(1.0f);
            }
            return m.f5330a;
        }
    }

    public final class c implements View.OnClickListener {
        public c() {
        }

        public final void onClick(View view) {
            i.this.b();
        }
    }

    public final class d implements View.OnClickListener {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ String f786b;
        public final /* synthetic */ String c;

        public d(String str, String str2) {
            this.f786b = str;
            this.c = str2;
        }

        public final void onClick(View view) {
            MainActivity a2;
            if (this.f786b != null && (a2 = b.b.a.b.a(i.this)) != null) {
                c1.a aVar = c1.h;
                String str = this.c;
                if (str == null) {
                    str = "";
                }
                a2.a(aVar.a(str, this.f786b), "popupDialog");
            }
        }
    }

    public final class e implements View.OnClickListener {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ String f788b;

        public e(String str) {
            this.f788b = str;
        }

        public final void onClick(View view) {
            MainActivity a2 = b.b.a.b.a(i.this);
            if (a2 != null) {
                ShareCompat.IntentBuilder type = ShareCompat.IntentBuilder.from(a2).setType("text/plain");
                String b2 = SupercellId.INSTANCE.getSharedServices$supercellId_release().j.b("account_friend_profile_dropdown_share_link");
                if (b2 == null) {
                    b2 = "";
                }
                type.setChooserTitle(b2).setText(this.f788b).startChooser();
            }
            i.this.b();
        }
    }

    public final class f implements View.OnClickListener {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ String f790b;
        public final /* synthetic */ String c;

        public final class a extends k implements kotlin.d.a.b<r, m> {
            public a() {
                super(1);
            }

            public final Object invoke(Object obj) {
                j.b((r) obj, "it");
                b.b.a.d.e eVar = SupercellId.INSTANCE.getSharedServices$supercellId_release().h;
                f fVar = f.this;
                eVar.b(fVar.f790b, fVar.c);
                return m.f5330a;
            }
        }

        public f(String str, String str2) {
            this.f790b = str;
            this.c = str2;
        }

        public final void onClick(View view) {
            MainActivity a2;
            if (this.f790b != null && this.c != null && (a2 = b.b.a.b.a(i.this)) != null) {
                r a3 = r.g.a("account_friend_profile_report_dialog_title", "account_friend_profile_report_dialog_ok", "account_friend_profile_report_dialog_cancel", kotlin.k.a("name", this.c), true);
                a3.e = new a();
                a2.a(a3, "popupDialog");
            }
        }
    }

    public View a(int i) {
        if (this.g == null) {
            this.g = new HashMap();
        }
        View view = (View) this.g.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.g.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public void a() {
        HashMap hashMap = this.g;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public void b() {
        ViewPropertyAnimator animate;
        super.b();
        FrameLayout frameLayout = (FrameLayout) a(R.id.dialogContainer);
        if (frameLayout != null && (animate = frameLayout.animate()) != null) {
            animate.setDuration(c());
            animate.setInterpolator(b.b.a.f.a.c);
            animate.scaleX(0.7f);
            animate.scaleY(0.7f);
            animate.start();
        }
    }

    public long c() {
        return this.e;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(1, R.style.SupercellIdInfoDialogTheme);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_report_drop_down, viewGroup, false);
    }

    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.FrameLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public void onViewCreated(View view, Bundle bundle) {
        j.b(view, "view");
        super.onViewCreated(view, bundle);
        if (this.f) {
            view.setOnClickListener(new c());
        }
        Bundle arguments = getArguments();
        String str = null;
        Rect rect = arguments != null ? (Rect) arguments.getParcelable("rect") : null;
        if (!(rect instanceof Rect)) {
            rect = null;
        }
        if (rect == null) {
            rect = new Rect();
        }
        Bundle arguments2 = getArguments();
        String string = arguments2 != null ? arguments2.getString("scid") : null;
        Bundle arguments3 = getArguments();
        String string2 = arguments3 != null ? arguments3.getString("name") : null;
        Bundle arguments4 = getArguments();
        String string3 = arguments4 != null ? arguments4.getString("qrCodeUrl") : null;
        Bundle arguments5 = getArguments();
        if (arguments5 != null) {
            str = arguments5.getString("universalLink");
        }
        TextView textView = (TextView) a(R.id.button_show_qr_code_title);
        if (textView != null) {
            b.b.a.b.a(textView, "AccountIcon.png", new Rect(0, 0, kotlin.e.a.a(b.b.a.b.a(25)), kotlin.e.a.a(b.b.a.b.a(25))));
        }
        TextView textView2 = (TextView) a(R.id.button_share_link_title);
        if (textView2 != null) {
            b.b.a.b.a(textView2, "ui_icon_share.png", new Rect(0, 0, kotlin.e.a.a(b.b.a.b.a(25)), kotlin.e.a.a(b.b.a.b.a(25))));
        }
        TextView textView3 = (TextView) a(R.id.button_report_name_title);
        if (textView3 != null) {
            b.b.a.b.a(textView3, "ui_icon_report.png", new Rect(0, 0, kotlin.e.a.a(b.b.a.b.a(25)), kotlin.e.a.a(b.b.a.b.a(25))));
        }
        FrameLayout frameLayout = (FrameLayout) a(R.id.button_show_qr_code);
        int i = 8;
        if (frameLayout != null) {
            frameLayout.setVisibility(string3 == null ? 8 : 0);
        }
        FrameLayout frameLayout2 = (FrameLayout) a(R.id.button_share_link);
        if (frameLayout2 != null) {
            frameLayout2.setVisibility(str == null ? 8 : 0);
        }
        FrameLayout frameLayout3 = (FrameLayout) a(R.id.button_report_name);
        if (frameLayout3 != null) {
            if (string2 != null) {
                i = 0;
            }
            frameLayout3.setVisibility(i);
        }
        FrameLayout frameLayout4 = (FrameLayout) a(R.id.button_show_qr_code);
        if (frameLayout4 != null) {
            frameLayout4.setOnClickListener(new d(string3, string2));
        }
        FrameLayout frameLayout5 = (FrameLayout) a(R.id.button_share_link);
        if (frameLayout5 != null) {
            frameLayout5.setOnClickListener(new e(str));
        }
        FrameLayout frameLayout6 = (FrameLayout) a(R.id.button_report_name);
        if (frameLayout6 != null) {
            frameLayout6.setOnClickListener(new f(string, string2));
        }
        FrameLayout frameLayout7 = (FrameLayout) a(R.id.dialogContainer);
        j.a((Object) frameLayout7, "it");
        frameLayout7.setAlpha(0.0f);
        q1.a(frameLayout7, new b(frameLayout7, this, view, rect));
    }
}
