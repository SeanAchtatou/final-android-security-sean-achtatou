package b.b.a.i.p1;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import b.b.a.h.j;
import b.b.a.i.a0;
import b.b.a.i.b;
import b.b.a.i.x0;
import b.b.a.i.z;
import b.b.a.i.z0;
import b.b.a.j.f0;
import b.b.a.j.m;
import b.b.a.j.q1;
import b.b.a.j.r0;
import b.b.a.j.t0;
import b.b.a.j.u0;
import b.b.a.j.y0;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import kotlin.a.ab;
import kotlin.a.an;
import kotlin.d.b.j;
import kotlin.d.b.k;
import nl.komponents.kovenant.bb;

public final class b extends b.b.a.i.g implements h {
    public final float m = b.b.a.b.a(20);
    public final kotlin.d.a.b<m<b.b.a.h.d, f0>, kotlin.m> n = new e();
    public List<? extends r0> o;
    public final y0<u0> p = new y0<>(new g(), new h());
    public HashMap q;

    public static final class a extends b.a {
        public static final C0045a CREATOR = new C0045a(null);
        public final Set<Integer> e = an.a((Object[]) new Integer[]{Integer.valueOf(R.id.nav_area_back_button), Integer.valueOf(R.id.nav_area_close_button)});
        public final boolean f = true;
        public final Class<? extends b.b.a.i.g> g = b.class;

        /* renamed from: b.b.a.i.p1.b$a$a  reason: collision with other inner class name */
        public static final class C0045a implements Parcelable.Creator<a> {
            public /* synthetic */ C0045a(kotlin.d.b.g gVar) {
            }

            public final Object createFromParcel(Parcel parcel) {
                j.b(parcel, "parcel");
                return new a();
            }

            public final Object[] newArray(int i) {
                return new a[i];
            }
        }

        public final int a(int i, int i2, int i3) {
            return a0.u.a(i, i2, i3);
        }

        public final int a(Resources resources, int i, int i2, int i3) {
            j.b(resources, "resources");
            if (i >= kotlin.e.a.a(b.b.a.b.a(600))) {
                return kotlin.e.a.a(((float) i) * 0.1f);
            }
            return 0;
        }

        public final Class<? extends b.b.a.i.g> a() {
            return this.g;
        }

        public final Class<? extends x0> a(Resources resources) {
            j.b(resources, "resources");
            return b.b.a.b.b(resources) ? z.class : z0.class;
        }

        public final int b(Resources resources, int i, int i2, int i3) {
            j.b(resources, "resources");
            return i2 + kotlin.e.a.a(b.b.a.b.a(68));
        }

        public final int c(Resources resources, int i, int i2, int i3) {
            j.b(resources, "resources");
            return a0.u.b(i, i2, i3);
        }

        public final Set<Integer> c() {
            return this.e;
        }

        public final boolean c(Resources resources) {
            j.b(resources, "resources");
            return false;
        }

        public final int describeContents() {
            return 0;
        }

        public final Class<? extends b.b.a.i.g> e(Resources resources) {
            j.b(resources, "resources");
            return b.b.a.b.b(resources) ? a0.class : C0046b.class;
        }

        public final boolean e() {
            return this.f;
        }

        public final void writeToParcel(Parcel parcel, int i) {
        }
    }

    /* renamed from: b.b.a.i.p1.b$b  reason: collision with other inner class name */
    public static final class C0046b extends b.b.a.i.g {
        public HashMap m;

        public final View a(int i) {
            if (this.m == null) {
                this.m = new HashMap();
            }
            View view = (View) this.m.get(Integer.valueOf(i));
            if (view != null) {
                return view;
            }
            View view2 = getView();
            if (view2 == null) {
                return null;
            }
            View findViewById = view2.findViewById(i);
            this.m.put(Integer.valueOf(i), findViewById);
            return findViewById;
        }

        public final void a() {
            HashMap hashMap = this.m;
            if (hashMap != null) {
                hashMap.clear();
            }
        }

        public final View b() {
            return (ImageButton) a(R.id.top_area_back_button);
        }

        public final View c() {
            return (ImageButton) a(R.id.top_area_close_button);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
            j.b(layoutInflater, "inflater");
            return layoutInflater.inflate(R.layout.fragment_ingame_friend_requests_top_area, viewGroup, false);
        }

        public final /* synthetic */ void onDestroyView() {
            super.onDestroyView();
            a();
        }
    }

    public final class c extends k implements kotlin.d.a.b<Exception, kotlin.m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f473a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(WeakReference weakReference) {
            super(1);
            this.f473a = weakReference;
        }

        public final Object invoke(Object obj) {
            Object obj2 = this.f473a.get();
            if (obj2 != null) {
                Exception exc = (Exception) obj;
                b bVar = (b) obj2;
                MainActivity a2 = b.b.a.b.a((Fragment) bVar);
                if (a2 != null) {
                    a2.a(exc, (kotlin.d.a.b<? super b.b.a.i.e, kotlin.m>) null);
                }
                bVar.n.invoke(SupercellId.INSTANCE.getSharedServices$supercellId_release().e().f1179a);
            }
            return kotlin.m.f5330a;
        }
    }

    public final class d extends k implements kotlin.d.a.b<Exception, kotlin.m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f474a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(WeakReference weakReference) {
            super(1);
            this.f474a = weakReference;
        }

        public final Object invoke(Object obj) {
            Object obj2 = this.f474a.get();
            if (obj2 != null) {
                Exception exc = (Exception) obj;
                b bVar = (b) obj2;
                MainActivity a2 = b.b.a.b.a((Fragment) bVar);
                if (a2 != null) {
                    a2.a(exc, (kotlin.d.a.b<? super b.b.a.i.e, kotlin.m>) null);
                }
                m mVar = (m) SupercellId.INSTANCE.getSharedServices$supercellId_release().e().f1179a;
                if (mVar != null) {
                    bVar.n.invoke(mVar);
                }
            }
            return kotlin.m.f5330a;
        }
    }

    public final class e extends k implements kotlin.d.a.b<m<? extends b.b.a.h.d, ? extends f0>, kotlin.m> {
        public e() {
            super(1);
        }

        public final Object invoke(Object obj) {
            m mVar = (m) obj;
            if (mVar instanceof m.a) {
                m.a aVar = (m.a) mVar;
                if (((b.b.a.h.d) aVar.f1097a).c.size() > 0) {
                    b bVar = b.this;
                    List<b.b.a.h.c> list = ((b.b.a.h.d) aVar.f1097a).c;
                    ArrayList arrayList = new ArrayList();
                    for (b.b.a.h.c cVar : list) {
                        b.b.a.h.j jVar = cVar.e;
                        if (!(jVar instanceof j.a)) {
                            jVar = null;
                        }
                        j.a aVar2 = (j.a) jVar;
                        a aVar3 = aVar2 != null ? new a(cVar.f110a, cVar.f111b, cVar.c, aVar2) : null;
                        if (aVar3 != null) {
                            arrayList.add(aVar3);
                        }
                    }
                    bVar.a(arrayList);
                } else {
                    b bVar2 = b.this;
                    bVar2.p.a(bb.f5389a);
                }
            } else if (mVar instanceof m.b) {
                b bVar3 = b.this;
                bVar3.p.a(bb.f5389a);
            } else {
                b bVar4 = b.this;
                bVar4.p.a(bb.f5389a);
            }
            return kotlin.m.f5330a;
        }
    }

    public final class f implements View.OnLayoutChangeListener {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ int f477b;

        public final class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ RecyclerView f478a;

            /* renamed from: b  reason: collision with root package name */
            public final /* synthetic */ int f479b;

            public a(RecyclerView recyclerView, int i) {
                this.f478a = recyclerView;
                this.f479b = i;
            }

            public final void run() {
                RecyclerView.LayoutManager layoutManager = this.f478a.getLayoutManager();
                Parcelable onSaveInstanceState = layoutManager != null ? layoutManager.onSaveInstanceState() : null;
                q1.d(this.f478a, this.f479b);
                RecyclerView.LayoutManager layoutManager2 = this.f478a.getLayoutManager();
                if (layoutManager2 != null) {
                    layoutManager2.onRestoreInstanceState(onSaveInstanceState);
                }
            }
        }

        public f(int i) {
            this.f477b = i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            RecyclerView recyclerView = (RecyclerView) b.this.a(R.id.messagesList);
            if (recyclerView != null) {
                kotlin.d.b.j.a((Object) view, "v");
                int height = view.getHeight() + this.f477b;
                if (height != q1.e(recyclerView)) {
                    recyclerView.post(new a(recyclerView, height));
                }
            }
        }
    }

    public final class g extends k implements kotlin.d.a.b<u0, kotlin.m> {
        public g() {
            super(1);
        }

        public final void a(u0 u0Var) {
            kotlin.d.b.j.b(u0Var, "it");
            b bVar = b.this;
            if (bVar.o == u0Var.f1176a) {
                bVar.o = u0Var.f1177b;
                if (bVar.o == null) {
                    RecyclerView recyclerView = (RecyclerView) bVar.a(R.id.messagesList);
                    if (recyclerView != null) {
                        recyclerView.setVisibility(4);
                    }
                    View a2 = b.this.a(R.id.progressBar);
                    if (a2 != null) {
                        a2.setVisibility(0);
                    }
                } else {
                    RecyclerView recyclerView2 = (RecyclerView) bVar.a(R.id.messagesList);
                    if (recyclerView2 != null) {
                        recyclerView2.setVisibility(0);
                    }
                    View a3 = b.this.a(R.id.progressBar);
                    if (a3 != null) {
                        a3.setVisibility(4);
                    }
                }
                RecyclerView recyclerView3 = (RecyclerView) b.this.a(R.id.messagesList);
                RecyclerView.Adapter adapter = recyclerView3 != null ? recyclerView3.getAdapter() : null;
                if (!(adapter instanceof g)) {
                    adapter = null;
                }
                g gVar = (g) adapter;
                if (gVar != null) {
                    List<? extends r0> list = b.this.o;
                    if (list == null) {
                        list = ab.f5210a;
                    }
                    gVar.a(list);
                    u0Var.c.dispatchUpdatesTo(gVar);
                }
            }
        }

        public final /* synthetic */ Object invoke(Object obj) {
            a((u0) obj);
            return kotlin.m.f5330a;
        }
    }

    public final class h extends k implements kotlin.d.a.b<Exception, kotlin.m> {
        public h() {
            super(1);
        }

        public final Object invoke(Object obj) {
            Exception exc = (Exception) obj;
            kotlin.d.b.j.b(exc, "it");
            MainActivity a2 = b.b.a.b.a((Fragment) b.this);
            if (a2 != null) {
                a2.a(exc, (kotlin.d.a.b<? super b.b.a.i.e, kotlin.m>) null);
            }
            return kotlin.m.f5330a;
        }
    }

    public final class i extends k implements kotlin.d.a.a<u0> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ List f482a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ List f483b;

        public final class a<T> implements Comparator<T> {
            public final int compare(T t, T t2) {
                return kotlin.b.a.a(((a) t2).e.a(), ((a) t).e.a());
            }
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(List list, List list2) {
            super(0);
            this.f482a = list;
            this.f483b = list2;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
         arg types: [java.util.List, b.b.a.i.p1.b$i$a]
         candidates:
          kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
          kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
          kotlin.a.w.a(java.util.List, int):T
          kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
          kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
          kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
          kotlin.a.s.a(java.util.List, java.util.Comparator):void
          kotlin.a.p.a(java.lang.Iterable, int):int
          kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
          kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T> */
        public final u0 invoke() {
            List<r0> a2 = f.a(kotlin.a.m.a((Iterable) this.f482a, (Comparator) new a()));
            List list = this.f483b;
            return new u0(list, a2, b.a.a.a.a.a(t0.c, list, a2, "DiffUtil.calculateDiff(R…create(oldRows, newRows))"));
        }
    }

    public final View a(int i2) {
        if (this.q == null) {
            this.q = new HashMap();
        }
        View view = (View) this.q.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i2);
        this.q.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public final void a(a aVar) {
        kotlin.d.b.j.b(aVar, "row");
        List<? extends r0> list = this.o;
        if (list != null) {
            ArrayList arrayList = new ArrayList();
            for (r0 r0Var : list) {
                if (!(r0Var instanceof a)) {
                    r0Var = null;
                }
                a aVar2 = (a) r0Var;
                if (aVar2 != null) {
                    arrayList.add(aVar2);
                }
            }
            ArrayList arrayList2 = new ArrayList();
            for (Object next : arrayList) {
                if (!((a) next).a(aVar)) {
                    arrayList2.add(next);
                }
            }
            a(arrayList2);
        }
        nl.komponents.kovenant.c.m.b(SupercellId.INSTANCE.getSharedServices$supercellId_release().e().d(aVar.f472b), new d(new WeakReference(this)));
    }

    public final void a(List<a> list) {
        this.p.a(bb.f5389a);
    }

    public final View b() {
        return (ImageButton) a(R.id.back_button);
    }

    public final void b(a aVar) {
        kotlin.d.b.j.b(aVar, "row");
        List<? extends r0> list = this.o;
        if (list != null) {
            ArrayList arrayList = new ArrayList();
            for (r0 r0Var : list) {
                if (!(r0Var instanceof a)) {
                    r0Var = null;
                }
                a aVar2 = (a) r0Var;
                if (aVar2 != null) {
                    arrayList.add(aVar2);
                }
            }
            ArrayList arrayList2 = new ArrayList();
            for (Object next : arrayList) {
                if (!((a) next).a(aVar)) {
                    arrayList2.add(next);
                }
            }
            a(arrayList2);
        }
        nl.komponents.kovenant.c.m.b(SupercellId.INSTANCE.getSharedServices$supercellId_release().e().a(aVar.f472b), new c(new WeakReference(this)));
    }

    public final RecyclerView e() {
        return (RecyclerView) a(R.id.messagesList);
    }

    public final View f() {
        return (RelativeLayout) a(R.id.friendRequestsToolbar);
    }

    public final float g() {
        return this.m;
    }

    public final List<View> h() {
        return kotlin.a.m.d(a(R.id.toolbarBackground), a(R.id.toolbarShadow));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kotlin.d.b.j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_ingame_friend_requests, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    public final void onResume() {
        super.onResume();
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a("In-Game - Friend Requests");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v7.widget.RecyclerView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.Context, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        FrameLayout frameLayout;
        kotlin.d.b.j.b(view, "view");
        super.onViewCreated(view, bundle);
        Resources resources = getResources();
        kotlin.d.b.j.a((Object) resources, "resources");
        if (b.b.a.b.b(resources)) {
            int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.list_padding_vertical);
            Fragment parentFragment = getParentFragment();
            if (!(parentFragment instanceof i)) {
                parentFragment = null;
            }
            i iVar = (i) parentFragment;
            if (!(iVar == null || (frameLayout = (FrameLayout) iVar.a(R.id.toolbarContainer)) == null)) {
                frameLayout.addOnLayoutChangeListener(new f(dimensionPixelSize));
            }
        }
        if (this.o == null) {
            RecyclerView recyclerView = (RecyclerView) a(R.id.messagesList);
            kotlin.d.b.j.a((Object) recyclerView, "messagesList");
            recyclerView.setVisibility(4);
            View a2 = a(R.id.progressBar);
            kotlin.d.b.j.a((Object) a2, "progressBar");
            a2.setVisibility(0);
        } else {
            RecyclerView recyclerView2 = (RecyclerView) a(R.id.messagesList);
            kotlin.d.b.j.a((Object) recyclerView2, "messagesList");
            recyclerView2.setVisibility(0);
            View a3 = a(R.id.progressBar);
            kotlin.d.b.j.a((Object) a3, "progressBar");
            a3.setVisibility(4);
        }
        Context context = view.getContext();
        kotlin.d.b.j.a((Object) context, "view.context");
        g gVar = new g(context, this);
        List<? extends r0> list = this.o;
        if (list == null) {
            list = ab.f5210a;
        }
        gVar.a(list);
        RecyclerView recyclerView3 = (RecyclerView) a(R.id.messagesList);
        kotlin.d.b.j.a((Object) recyclerView3, "messagesList");
        recyclerView3.setLayoutManager(new LinearLayoutManager(getContext()));
        RecyclerView recyclerView4 = (RecyclerView) a(R.id.messagesList);
        kotlin.d.b.j.a((Object) recyclerView4, "messagesList");
        recyclerView4.setAdapter(gVar);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().e().b(this.n);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().e().a();
    }
}
