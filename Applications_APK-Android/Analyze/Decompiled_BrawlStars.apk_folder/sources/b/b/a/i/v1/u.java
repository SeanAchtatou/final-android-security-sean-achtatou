package b.b.a.i.v1;

import android.support.v4.app.Fragment;
import b.b.a.i.e;
import b.b.a.i.v1.j;
import com.supercell.id.ui.MainActivity;
import kotlin.d.a.b;
import kotlin.d.b.k;
import kotlin.m;

public final class u extends k implements b<e, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ j.C0077j f815a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public u(j.C0077j jVar) {
        super(1);
        this.f815a = jVar;
    }

    public final Object invoke(Object obj) {
        kotlin.d.b.j.b((e) obj, "it");
        MainActivity a2 = b.b.a.b.a((Fragment) j.this);
        if (a2 != null) {
            a2.d();
        }
        return m.f5330a;
    }
}
