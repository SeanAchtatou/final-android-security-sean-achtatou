package b.b.a.i.j1;

import android.support.v4.app.Fragment;
import b.b.a.j.r0;
import com.supercell.id.ui.MainActivity;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.d.a.b;
import kotlin.d.b.k;
import kotlin.m;

public final class e extends k implements b<Exception, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f337a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ b f338b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public e(WeakReference weakReference, b bVar) {
        super(1);
        this.f337a = weakReference;
        this.f338b = bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
     arg types: [java.util.ArrayList, b.b.a.i.j1.b]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T> */
    public final void invoke(Exception exc) {
        Object obj = this.f337a.get();
        if (obj != null) {
            Exception exc2 = exc;
            c cVar = (c) obj;
            MainActivity a2 = b.b.a.b.a((Fragment) cVar);
            if (a2 != null) {
                a2.a(exc2, (b<? super b.b.a.i.e, m>) null);
            }
            List<? extends r0> list = cVar.o;
            if (list != null) {
                ArrayList arrayList = new ArrayList();
                for (r0 r0Var : list) {
                    if (!(r0Var instanceof b)) {
                        r0Var = null;
                    }
                    b bVar = (b) r0Var;
                    if (bVar != null) {
                        arrayList.add(bVar);
                    }
                }
                cVar.a(kotlin.a.m.a((Collection) arrayList, (Object) this.f338b));
            }
        }
    }
}
