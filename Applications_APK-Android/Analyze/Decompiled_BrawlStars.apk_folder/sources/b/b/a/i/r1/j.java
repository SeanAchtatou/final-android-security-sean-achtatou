package b.b.a.i.r1;

import android.graphics.drawable.BitmapDrawable;
import android.text.SpannableStringBuilder;
import android.widget.TextView;
import b.b.a.j.h;
import b.b.a.j.s0;
import com.supercell.id.R;
import kotlin.d.a.b;
import kotlin.d.b.k;
import kotlin.m;

public final class j extends k implements b<String, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ k f624a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ BitmapDrawable f625b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public j(k kVar, BitmapDrawable bitmapDrawable) {
        super(1);
        this.f624a = kVar;
        this.f625b = bitmapDrawable;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final Object invoke(Object obj) {
        String str = (String) obj;
        kotlin.d.b.j.b(str, "systemName");
        s0.a aVar = (s0.a) this.f624a.f627b.get();
        if (aVar != null && !(!kotlin.d.b.j.a(aVar.f1166b, this.f624a.c))) {
            TextView textView = (TextView) aVar.c.findViewById(R.id.friendStatusLabel);
            kotlin.d.b.j.a((Object) textView, "containerView.friendStatusLabel");
            b.b.a.i.x1.j.a(textView, "account_friend_status_playing", kotlin.k.a("game", b.b.a.b.a(new SpannableStringBuilder(), " ", new h(this.f625b), 33).append((CharSequence) " ").append((CharSequence) str)));
        }
        return m.f5330a;
    }
}
