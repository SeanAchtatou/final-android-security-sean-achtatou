package b.b.a.i.s1;

import android.view.View;
import b.b.a.c.b;
import b.b.a.h.n;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;

public final class e implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ c f665a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ n f666b;

    public e(c cVar, n nVar, boolean z, int i) {
        this.f665a = cVar;
        this.f666b = nVar;
    }

    public final void onClick(View view) {
        b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Connected Games", "click", b.b.a.b.a(this.f666b), null, false, 24);
        MainActivity a2 = b.b.a.b.a(this.f665a);
        if (a2 != null) {
            a2.a(this.f666b.c);
        }
    }
}
