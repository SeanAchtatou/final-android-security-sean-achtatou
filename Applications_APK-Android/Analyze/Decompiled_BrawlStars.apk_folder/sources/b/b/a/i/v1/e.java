package b.b.a.i.v1;

import b.b.a.h.j;
import b.b.a.j.t0;
import b.b.a.j.u0;
import com.supercell.id.SupercellId;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import kotlin.a.m;
import kotlin.d.b.k;

public final class e extends k implements kotlin.d.a.a<u0> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ List f764a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ List f765b;

    public final class a<T> implements Comparator<T> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ Comparator f766a;

        public a(Comparator comparator) {
            this.f766a = comparator;
        }

        public final int compare(T t, T t2) {
            Comparator comparator = this.f766a;
            String str = ((a) t).c;
            if (str == null) {
                str = "";
            }
            String str2 = ((a) t2).c;
            if (str2 == null) {
                str2 = "";
            }
            return comparator.compare(str, str2);
        }
    }

    public final class b<T> implements Comparator<a> {

        /* renamed from: a  reason: collision with root package name */
        public static final b f767a = new b();

        public final int compare(Object obj, Object obj2) {
            return b.b.a.b.a(((a) obj).c, ((a) obj2).c);
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public e(List list, List list2) {
        super(0);
        this.f764a = list;
        this.f765b = list2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
     arg types: [java.util.List, b.b.a.i.v1.e$b]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
     arg types: [java.util.List, b.b.a.i.v1.e$a]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.p.a(java.lang.Iterable, int):int
     arg types: [java.util.ArrayList, int]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.p.a(java.lang.Iterable, int):int */
    public final u0 invoke() {
        List a2 = m.a((Iterable) m.a((Iterable) this.f764a, (Comparator) new a(SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getCollator())), (Comparator) b.f767a);
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Object next : a2) {
            Boolean valueOf = Boolean.valueOf(((a) next).f instanceof j.d);
            Object obj = linkedHashMap.get(valueOf);
            if (obj == null) {
                obj = new ArrayList();
                linkedHashMap.put(valueOf, obj);
            }
            ((List) obj).add(next);
        }
        List d = m.d((List) linkedHashMap.get(false), (List) linkedHashMap.get(true));
        ArrayList<List> arrayList = new ArrayList<>();
        for (Object next2 : d) {
            if (!((List) next2).isEmpty()) {
                arrayList.add(next2);
            }
        }
        ArrayList arrayList2 = new ArrayList(m.a((Iterable) arrayList, 10));
        for (List a3 : arrayList) {
            arrayList2.add(f.a(a3));
        }
        List b2 = m.b((Iterable) arrayList2);
        List list = this.f765b;
        return new u0(list, b2, b.a.a.a.a.a(t0.c, list, b2, "DiffUtil.calculateDiff(R…create(oldRows, newRows))"));
    }
}
