package b.b.a.i.r1;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.TextView;
import b.b.a.e.a;
import b.b.a.i.f1;
import b.b.a.i.n1.a;
import b.b.a.i.r1.d;
import b.b.a.i.v1.j;
import b.b.a.j.f0;
import b.b.a.j.k0;
import b.b.a.j.m;
import b.b.a.j.o;
import b.b.a.j.o1;
import b.b.a.j.q0;
import b.b.a.j.q1;
import b.b.a.j.r;
import b.b.a.j.r0;
import b.b.a.j.s0;
import b.b.a.j.u0;
import b.b.a.j.y;
import b.b.a.j.y0;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.ui.profile.LinearLayoutManagerWrapper;
import com.supercell.id.view.WidthAdjustingMultilineButton;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import kotlin.a.ab;
import kotlin.d.b.k;
import nl.komponents.kovenant.bb;

public final class e extends f1 {

    /* renamed from: b  reason: collision with root package name */
    public List<? extends r0> f600b;
    public final kotlin.d.a.b<m<b.b.a.h.d, f0>, kotlin.m> c = new b();
    public final y0<u0> d = new y0<>(new d(), new C0059e());
    public Timer e;
    public HashMap f;

    public static final class a extends s0 {

        /* renamed from: b  reason: collision with root package name */
        public final WeakReference<Context> f601b;
        public final o1<BitmapDrawable> c = new o1<>(new f());
        public final e d;

        /* renamed from: b.b.a.i.r1.e$a$a  reason: collision with other inner class name */
        public final class C0057a implements View.OnClickListener {

            /* renamed from: b  reason: collision with root package name */
            public final /* synthetic */ r0 f603b;

            public C0057a(r0 r0Var, int i) {
                this.f603b = r0Var;
            }

            public final void onClick(View view) {
                MainActivity a2 = b.b.a.b.a(a.this.d);
                if (a2 != null) {
                    String d = ((d) this.f603b).d();
                    String c = ((d) this.f603b).c();
                    String b2 = ((d) this.f603b).b();
                    d dVar = (d) this.f603b;
                    a2.a(new j.a(null, d, c, b2, dVar.f595b.e, dVar.f595b.d, false, 64));
                }
                SupercellId.INSTANCE.getSharedServices$supercellId_release().l.a(a.C0004a.BUTTON_01);
            }
        }

        public final class b implements View.OnClickListener {
            public b(r0 r0Var, int i) {
            }

            public final void onClick(View view) {
                MainActivity a2 = b.b.a.b.a(a.this.d);
                if (a2 != null) {
                    a2.a(new a.C0036a());
                }
            }
        }

        public final class c extends k implements kotlin.d.a.b<b.b.a.h.d, kotlin.m> {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ s0.a f605a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(s0.a aVar) {
                super(1);
                this.f605a = aVar;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
             arg types: [android.widget.ImageView, java.lang.String]
             candidates:
              kotlin.d.b.j.a(int, int):int
              kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
              kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
              kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
             arg types: [android.widget.Space, java.lang.String]
             candidates:
              kotlin.d.b.j.a(int, int):int
              kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
              kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
              kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
            public final Object invoke(Object obj) {
                b.b.a.h.d dVar = (b.b.a.h.d) obj;
                if (dVar != null) {
                    ImageView imageView = (ImageView) this.f605a.c.findViewById(R.id.addFriendsIllustration);
                    kotlin.d.b.j.a((Object) imageView, "containerView.addFriendsIllustration");
                    int i = 8;
                    imageView.setVisibility(dVar.f113b.size() > 0 ? 8 : 0);
                    Space space = (Space) this.f605a.c.findViewById(R.id.addFriendsSpace);
                    kotlin.d.b.j.a((Object) space, "containerView.addFriendsSpace");
                    if (dVar.f113b.size() > 0) {
                        i = 0;
                    }
                    space.setVisibility(i);
                }
                return kotlin.m.f5330a;
            }
        }

        public final class d implements View.OnClickListener {

            /* renamed from: a  reason: collision with root package name */
            public static final d f606a = new d();

            public final void onClick(View view) {
                SupercellId.INSTANCE.getSharedServices$supercellId_release().e().a();
            }
        }

        /* renamed from: b.b.a.i.r1.e$a$e  reason: collision with other inner class name */
        public final class C0058e extends k implements kotlin.d.a.a<kotlin.m> {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ s0.a f607a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0058e(s0.a aVar) {
                super(0);
                this.f607a = aVar;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
             arg types: [android.widget.ImageView, java.lang.String]
             candidates:
              kotlin.d.b.j.a(int, int):int
              kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
              kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
              kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
            public final Object invoke() {
                ImageView imageView = (ImageView) this.f607a.c.findViewById(R.id.onlineStatusIndicator);
                kotlin.d.b.j.a((Object) imageView, "containerView.onlineStatusIndicator");
                ImageView imageView2 = (ImageView) this.f607a.c.findViewById(R.id.friendImageView);
                kotlin.d.b.j.a((Object) imageView2, "containerView.friendImageView");
                kotlin.d.b.j.b(imageView, "indicator");
                kotlin.d.b.j.b(imageView2, "companion");
                float width = (((float) imageView2.getWidth()) / b.b.a.b.a(1)) / 3.3f;
                if (Float.compare(width, 14.0f) < 0) {
                    width = 14.0f;
                } else if (Float.compare(width, 24.0f) > 0) {
                    width = 24.0f;
                }
                float f = width * b.b.a.b.f16a;
                int width2 = imageView2.getWidth() / 2;
                ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
                if (!(layoutParams instanceof ConstraintLayout.LayoutParams)) {
                    layoutParams = null;
                }
                ConstraintLayout.LayoutParams layoutParams2 = (ConstraintLayout.LayoutParams) layoutParams;
                if (layoutParams2 == null || width2 != layoutParams2.circleRadius) {
                    imageView.post(new y(imageView, layoutParams2, f, width2));
                }
                return kotlin.m.f5330a;
            }
        }

        public final class f extends k implements kotlin.d.a.c<String, kotlin.d.a.b<? super BitmapDrawable, ? extends kotlin.m>, kotlin.m> {
            public f() {
                super(2);
            }

            public final Object invoke(Object obj, Object obj2) {
                String str = (String) obj;
                kotlin.d.a.b bVar = (kotlin.d.a.b) obj2;
                kotlin.d.b.j.b(str, "key");
                kotlin.d.b.j.b(bVar, "callback");
                SupercellId.INSTANCE.getSharedServices$supercellId_release().j.a(str, new f(this, bVar));
                return kotlin.m.f5330a;
            }
        }

        public a(Context context, e eVar) {
            kotlin.d.b.j.b(context, "context");
            kotlin.d.b.j.b(eVar, "fragment");
            this.d = eVar;
            this.f601b = new WeakReference<>(context);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.TextView, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.ImageView, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void a(s0.a aVar, int i, r0 r0Var) {
            Resources resources;
            kotlin.d.b.j.b(aVar, "holder");
            kotlin.d.b.j.b(r0Var, "item");
            if (r0Var instanceof d) {
                b.b.a.b.a((LinearLayout) aVar.c.findViewById(R.id.friendContainer), b.b.a.b.b(this.f1164a, i), b.b.a.b.a(this.f1164a, i), l.b(this.f1164a, i) ? aVar.c.getResources().getDimensionPixelSize(R.dimen.list_padding_horizontal) : 0, l.a(this.f1164a, i) ? aVar.c.getResources().getDimensionPixelSize(R.dimen.list_padding_horizontal) : 0);
                Context context = aVar.c.getContext();
                if (!(context == null || (resources = context.getResources()) == null)) {
                    k0.f1078b.a(((d) r0Var).b(), (ImageView) aVar.c.findViewById(R.id.friendImageView), resources);
                }
                TextView textView = (TextView) aVar.c.findViewById(R.id.friendNameLabel);
                kotlin.d.b.j.a((Object) textView, "containerView.friendNameLabel");
                d dVar = (d) r0Var;
                String c2 = dVar.c();
                if (c2 == null) {
                    c2 = r.f1160a.a(dVar.d());
                }
                textView.setText(c2);
                ((TextView) aVar.c.findViewById(R.id.friendNameLabel)).setTextColor(ContextCompat.getColor(aVar.c.getContext(), dVar.c() == null ? R.color.gray40 : R.color.black));
                d.a e = dVar.e();
                if (e instanceof d.a.b) {
                    TextView textView2 = (TextView) aVar.c.findViewById(R.id.friendStatusLabel);
                    kotlin.d.b.j.a((Object) textView2, "containerView.friendStatusLabel");
                    b.b.a.i.x1.j.a(textView2, "account_friend_status_pending", (kotlin.d.a.b) null, 2);
                } else if (e instanceof d.a.C0056a) {
                    TextView textView3 = (TextView) aVar.c.findViewById(R.id.friendStatusLabel);
                    kotlin.d.b.j.a((Object) textView3, "containerView.friendStatusLabel");
                    b.b.a.i.x1.j.a(textView3, "account_friend_status_offline", (kotlin.d.a.b) null, 2);
                } else if (e instanceof d.a.c) {
                    TextView textView4 = (TextView) aVar.c.findViewById(R.id.friendStatusLabel);
                    kotlin.d.b.j.a((Object) textView4, "containerView.friendStatusLabel");
                    textView4.setText("");
                }
                int i2 = 8;
                if (e instanceof d.a.c) {
                    o1<BitmapDrawable> o1Var = this.c;
                    d.a.c cVar = (d.a.c) e;
                    String str = cVar.f598a;
                    TextView textView5 = (TextView) aVar.c.findViewById(R.id.friendStatusLabel);
                    kotlin.d.b.j.a((Object) textView5, "containerView.friendStatusLabel");
                    textView5.setText((CharSequence) null);
                    WeakReference weakReference = new WeakReference(aVar);
                    o1Var.a("AppIcon_" + str + ".png", new k(str, weakReference, r0Var));
                    ((TextView) aVar.c.findViewById(R.id.friendStatusLabel)).setTextColor(ContextCompat.getColor(aVar.c.getContext(), R.color.black));
                    TextView textView6 = (TextView) aVar.c.findViewById(R.id.friendPlayingNameLabel);
                    kotlin.d.b.j.a((Object) textView6, "containerView.friendPlayingNameLabel");
                    textView6.setText(cVar.f599b);
                    TextView textView7 = (TextView) aVar.c.findViewById(R.id.friendPlayingNameLabel);
                    kotlin.d.b.j.a((Object) textView7, "containerView.friendPlayingNameLabel");
                    if (cVar.f599b != null) {
                        i2 = 0;
                    }
                    textView7.setVisibility(i2);
                    ImageView imageView = (ImageView) aVar.c.findViewById(R.id.onlineStatusIndicator);
                    kotlin.d.b.j.a((Object) imageView, "containerView.onlineStatusIndicator");
                    imageView.setVisibility(0);
                } else {
                    ((TextView) aVar.c.findViewById(R.id.friendStatusLabel)).setTextColor(ContextCompat.getColor(aVar.c.getContext(), R.color.gray67));
                    TextView textView8 = (TextView) aVar.c.findViewById(R.id.friendPlayingNameLabel);
                    kotlin.d.b.j.a((Object) textView8, "containerView.friendPlayingNameLabel");
                    textView8.setVisibility(8);
                    ImageView imageView2 = (ImageView) aVar.c.findViewById(R.id.onlineStatusIndicator);
                    kotlin.d.b.j.a((Object) imageView2, "containerView.onlineStatusIndicator");
                    imageView2.setVisibility(8);
                }
                aVar.c.setSoundEffectsEnabled(false);
                aVar.c.setOnClickListener(new C0057a(r0Var, i));
            } else if (kotlin.d.b.j.a(r0Var, a.f582b)) {
                ((WidthAdjustingMultilineButton) aVar.c.findViewById(R.id.addFriendsButton)).setOnClickListener(new b(r0Var, i));
                nl.komponents.kovenant.c.m.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().e().a(), new c(aVar));
            } else if (r0Var instanceof o) {
                ((WidthAdjustingMultilineButton) aVar.c.findViewById(R.id.errorRetryButton)).setOnClickListener(d.f606a);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: b.b.a.j.s0.onCreateViewHolder(android.view.ViewGroup, int):b.b.a.j.s0$a
         arg types: [android.view.ViewGroup, int]
         candidates:
          b.b.a.i.r1.e.a.onCreateViewHolder(android.view.ViewGroup, int):android.support.v7.widget.RecyclerView$ViewHolder
          b.b.a.j.s0.onCreateViewHolder(android.view.ViewGroup, int):android.support.v7.widget.RecyclerView$ViewHolder
          android.support.v7.widget.RecyclerView.Adapter.onCreateViewHolder(android.view.ViewGroup, int):VH
          b.b.a.j.s0.onCreateViewHolder(android.view.ViewGroup, int):b.b.a.j.s0$a */
        public final s0.a onCreateViewHolder(ViewGroup viewGroup, int i) {
            kotlin.d.b.j.b(viewGroup, "parent");
            s0.a onCreateViewHolder = super.onCreateViewHolder(viewGroup, i);
            if (!(((ImageView) onCreateViewHolder.c.findViewById(R.id.onlineStatusIndicator)) == null || ((ImageView) onCreateViewHolder.c.findViewById(R.id.friendImageView)) == null)) {
                q1.a(onCreateViewHolder.c, new C0058e(onCreateViewHolder));
            }
            return onCreateViewHolder;
        }
    }

    public final class b extends k implements kotlin.d.a.b<m<? extends b.b.a.h.d, ? extends f0>, kotlin.m> {
        public b() {
            super(1);
        }

        public final Object invoke(Object obj) {
            m mVar = (m) obj;
            if (mVar instanceof m.a) {
                e eVar = e.this;
                eVar.d.a(bb.f5389a);
            } else if (mVar instanceof m.b) {
                e eVar2 = e.this;
                eVar2.d.a(bb.f5389a);
            } else {
                e eVar3 = e.this;
                eVar3.d.a(bb.f5389a);
            }
            return kotlin.m.f5330a;
        }
    }

    public final class c extends TimerTask {
        public final void run() {
            SupercellId.INSTANCE.getSharedServices$supercellId_release().e().a();
        }
    }

    public final class d extends k implements kotlin.d.a.b<u0, kotlin.m> {
        public d() {
            super(1);
        }

        public final void a(u0 u0Var) {
            kotlin.d.b.j.b(u0Var, "it");
            e eVar = e.this;
            if (eVar.f600b == u0Var.f1176a) {
                eVar.f600b = u0Var.f1177b;
                if (eVar.f600b == null) {
                    RecyclerView recyclerView = (RecyclerView) eVar.a(R.id.friendsList);
                    if (recyclerView != null) {
                        recyclerView.setVisibility(4);
                    }
                    View a2 = e.this.a(R.id.progressBar);
                    if (a2 != null) {
                        a2.setVisibility(0);
                    }
                } else {
                    RecyclerView recyclerView2 = (RecyclerView) eVar.a(R.id.friendsList);
                    if (recyclerView2 != null) {
                        recyclerView2.setVisibility(0);
                    }
                    View a3 = e.this.a(R.id.progressBar);
                    if (a3 != null) {
                        a3.setVisibility(4);
                    }
                }
                RecyclerView recyclerView3 = (RecyclerView) e.this.a(R.id.friendsList);
                RecyclerView.Adapter adapter = recyclerView3 != null ? recyclerView3.getAdapter() : null;
                if (!(adapter instanceof a)) {
                    adapter = null;
                }
                a aVar = (a) adapter;
                if (aVar != null) {
                    List<? extends r0> list = e.this.f600b;
                    if (list == null) {
                        list = ab.f5210a;
                    }
                    aVar.a(list);
                    u0Var.c.dispatchUpdatesTo(aVar);
                }
            }
        }

        public final /* synthetic */ Object invoke(Object obj) {
            a((u0) obj);
            return kotlin.m.f5330a;
        }
    }

    /* renamed from: b.b.a.i.r1.e$e  reason: collision with other inner class name */
    public final class C0059e extends k implements kotlin.d.a.b<Exception, kotlin.m> {
        public C0059e() {
            super(1);
        }

        public final Object invoke(Object obj) {
            Exception exc = (Exception) obj;
            kotlin.d.b.j.b(exc, "it");
            MainActivity a2 = b.b.a.b.a(e.this);
            if (a2 != null) {
                a2.a(exc, (kotlin.d.a.b<? super b.b.a.i.e, kotlin.m>) null);
            }
            return kotlin.m.f5330a;
        }
    }

    public final View a(int i) {
        if (this.f == null) {
            this.f = new HashMap();
        }
        View view = (View) this.f.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.f.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.f;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public final void c() {
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a("Friends");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kotlin.d.b.j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_profile_friends, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    public final void onPause() {
        super.onPause();
        Timer timer = this.e;
        if (timer != null) {
            timer.cancel();
        }
        this.e = null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v7.widget.RecyclerView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.Context, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        kotlin.d.b.j.b(view, "view");
        if (this.f600b == null) {
            RecyclerView recyclerView = (RecyclerView) a(R.id.friendsList);
            kotlin.d.b.j.a((Object) recyclerView, "friendsList");
            recyclerView.setVisibility(4);
            View a2 = a(R.id.progressBar);
            kotlin.d.b.j.a((Object) a2, "progressBar");
            a2.setVisibility(0);
        } else {
            RecyclerView recyclerView2 = (RecyclerView) a(R.id.friendsList);
            kotlin.d.b.j.a((Object) recyclerView2, "friendsList");
            recyclerView2.setVisibility(0);
            View a3 = a(R.id.progressBar);
            kotlin.d.b.j.a((Object) a3, "progressBar");
            a3.setVisibility(4);
        }
        Context context = view.getContext();
        kotlin.d.b.j.a((Object) context, "view.context");
        a aVar = new a(context, this);
        List<? extends r0> list = this.f600b;
        if (list == null) {
            list = ab.f5210a;
        }
        aVar.a(list);
        RecyclerView recyclerView3 = (RecyclerView) a(R.id.friendsList);
        kotlin.d.b.j.a((Object) recyclerView3, "friendsList");
        recyclerView3.setLayoutManager(new LinearLayoutManagerWrapper(getContext()));
        RecyclerView recyclerView4 = (RecyclerView) a(R.id.friendsList);
        kotlin.d.b.j.a((Object) recyclerView4, "friendsList");
        recyclerView4.setAdapter(aVar);
        RecyclerView recyclerView5 = (RecyclerView) a(R.id.friendsList);
        q1.d(recyclerView5, recyclerView5.getResources().getDimensionPixelSize(R.dimen.list_padding_vertical) + q1.e(recyclerView5));
        SupercellId.INSTANCE.getSharedServices$supercellId_release().e().b(this.c);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().e().a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public final void onResume() {
        super.onResume();
        long e2 = SupercellId.INSTANCE.getRemoteConfiguration$supercellId_release().e(q0.FRIENDS_REFRESH_RATE);
        if (e2 != 0) {
            long max = Math.max(e2, 30000L);
            Timer timer = new Timer("profileRefresh", false);
            timer.scheduleAtFixedRate(new c(), max, max);
            this.e = timer;
        }
    }
}
