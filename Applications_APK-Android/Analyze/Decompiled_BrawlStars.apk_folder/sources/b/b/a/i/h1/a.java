package b.b.a.i.h1;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.os.ParcelCompat;
import android.support.v4.widget.NestedScrollView;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import b.b.a.i.b;
import b.b.a.i.b1;
import b.b.a.i.d0;
import b.b.a.i.e0;
import b.b.a.i.g;
import b.b.a.i.g1.a;
import b.b.a.i.s1.h;
import b.b.a.i.x0;
import b.b.a.i.z;
import b.b.a.j.a0;
import b.b.a.j.f;
import b.b.a.j.q0;
import com.facebook.share.internal.ShareConstants;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.ExpandableFrameLayout;
import com.supercell.id.view.SubPageTabLayout;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import kotlin.a.an;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.j.t;
import kotlin.m;

public final class a extends g {
    public final float m = b.b.a.b.a(20);
    public HashMap n;

    public static final class b extends b.a implements a0 {
        public static final Parcelable.Creator<b> CREATOR = new C0024a();
        public final Set<Integer> e = an.a((Object[]) new Integer[]{Integer.valueOf(R.id.nav_area_back_button), Integer.valueOf(R.id.nav_area_close_button)});
        public final boolean f = true;
        public final Class<? extends g> g = a.class;
        public final boolean h;

        /* renamed from: b.b.a.i.h1.a$b$a  reason: collision with other inner class name */
        public final class C0024a implements Parcelable.Creator<b> {
            public final b createFromParcel(Parcel parcel) {
                j.b(parcel, ShareConstants.FEED_SOURCE_PARAM);
                j.b(parcel, "parcel");
                return new b(ParcelCompat.readBoolean(parcel));
            }

            public final b[] newArray(int i) {
                return new b[i];
            }
        }

        public b(boolean z) {
            this.h = z;
        }

        public final int a(int i, int i2, int i3) {
            if (this.h) {
                return b.b.a.i.a0.u.a(i, i2, i3);
            }
            return 0;
        }

        public final g a(Context context) {
            j.b(context, "context");
            return (g) b.b.a.b.a(super.a(context), "faqItemPrefix", "faq_topics_v2");
        }

        public final Class<? extends g> a() {
            return this.g;
        }

        public final Class<? extends x0> a(Resources resources) {
            j.b(resources, "resources");
            return b.b.a.b.b(resources) ? this.h ? z.class : super.a(resources) : b.b.a.i.a.class;
        }

        public final int b(Resources resources, int i, int i2, int i3) {
            float f2;
            j.b(resources, "resources");
            j.b(resources, "$this$isSmallScreen");
            if (resources.getBoolean(R.bool.isSmallScreen)) {
                f2 = b.b.a.b.a(64);
            } else {
                f2 = b.b.a.b.a(150);
            }
            return i2 + kotlin.e.a.a(f2);
        }

        public final int c(Resources resources, int i, int i2, int i3) {
            j.b(resources, "resources");
            if (this.h) {
                return b.b.a.i.a0.u.b(i, i2, i3);
            }
            j.b(resources, "resources");
            return b.a.d.a(i, i2, i3);
        }

        public final Set<Integer> c() {
            return this.e;
        }

        public final boolean c(Resources resources) {
            j.b(resources, "resources");
            return !b.b.a.b.b(resources) || !this.h;
        }

        public final boolean d(Resources resources) {
            j.b(resources, "resources");
            return b.b.a.b.b(resources) && !this.h;
        }

        public final int describeContents() {
            return 0;
        }

        public final Class<? extends g> e(Resources resources) {
            j.b(resources, "resources");
            if (b.b.a.b.b(resources) && this.h) {
                return b.b.a.i.a0.class;
            }
            if (b.b.a.b.b(resources) && !this.h) {
                return a.d.class;
            }
            j.b(resources, "$this$isSmallScreen");
            if (resources.getBoolean(R.bool.isSmallScreen) && this.h) {
                return d0.class;
            }
            j.b(resources, "$this$isSmallScreen");
            return (resources.getBoolean(R.bool.isSmallScreen) || !this.h) ? b1.class : e0.class;
        }

        public final boolean e() {
            return this.f;
        }

        public final boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof b) {
                    if (this.h == ((b) obj).h) {
                        return true;
                    }
                }
                return false;
            }
            return true;
        }

        public final int hashCode() {
            boolean z = this.h;
            if (z) {
                return 1;
            }
            return z ? 1 : 0;
        }

        public final String toString() {
            StringBuilder a2 = b.a.a.a.a.a("BackStackEntry(showProfile=");
            a2.append(this.h);
            a2.append(")");
            return a2.toString();
        }

        public final void writeToParcel(Parcel parcel, int i) {
            j.b(parcel, "dest");
            ParcelCompat.writeBoolean(parcel, this.h);
        }
    }

    public static final class c extends b.a implements a0 {
        public static final Parcelable.Creator<c> CREATOR = new C0025a();
        public final Set<Integer> e = an.a((Object[]) new Integer[]{Integer.valueOf(R.id.back_button), Integer.valueOf(R.id.close_button)});
        public final boolean f = true;
        public final Class<? extends g> g = a.class;
        public final boolean h;

        /* renamed from: b.b.a.i.h1.a$c$a  reason: collision with other inner class name */
        public final class C0025a implements Parcelable.Creator<c> {
            public final c createFromParcel(Parcel parcel) {
                j.b(parcel, ShareConstants.FEED_SOURCE_PARAM);
                j.b(parcel, "parcel");
                return new c(parcel.readByte() != 0);
            }

            public final c[] newArray(int i) {
                return new c[i];
            }
        }

        public c(boolean z) {
            this.h = z;
        }

        public final int a(int i, int i2, int i3) {
            if (this.h) {
                return h.a.g.a(i, i2, i3);
            }
            return 0;
        }

        public final g a(Context context) {
            j.b(context, "context");
            return (g) b.b.a.b.a(super.a(context), "faqItemPrefix", "faq_topics");
        }

        public final Class<? extends g> a() {
            return this.g;
        }

        public final Class<? extends x0> a(Resources resources) {
            j.b(resources, "resources");
            return b.b.a.b.b(resources) ? super.a(resources) : b.b.a.i.a.class;
        }

        public final int b(Resources resources, int i, int i2, int i3) {
            j.b(resources, "resources");
            return i2 + kotlin.e.a.a(b.b.a.b.a(150));
        }

        public final int c(Resources resources, int i, int i2, int i3) {
            j.b(resources, "resources");
            if (this.h) {
                return h.a.g.b(i, i2, i3);
            }
            j.b(resources, "resources");
            return b.a.d.a(i, i2, i3);
        }

        public final Set<Integer> c() {
            return this.e;
        }

        public final boolean d(Resources resources) {
            j.b(resources, "resources");
            return b.b.a.b.b(resources) && !this.h;
        }

        public final int describeContents() {
            return 0;
        }

        public final Class<? extends g> e(Resources resources) {
            j.b(resources, "resources");
            return b.b.a.b.b(resources) ? this.h ? h.c.class : a.d.class : b1.class;
        }

        public final boolean e() {
            return this.f;
        }

        public final boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof c) {
                    if (this.h == ((c) obj).h) {
                        return true;
                    }
                }
                return false;
            }
            return true;
        }

        public final int hashCode() {
            boolean z = this.h;
            if (z) {
                return 1;
            }
            return z ? 1 : 0;
        }

        public final String toString() {
            StringBuilder a2 = b.a.a.a.a.a("BackStackEntryV1(showProfile=");
            a2.append(this.h);
            a2.append(")");
            return a2.toString();
        }

        public final void writeToParcel(Parcel parcel, int i) {
            j.b(parcel, "dest");
            parcel.writeByte(this.h ? (byte) 1 : 0);
        }
    }

    public final class d extends k implements kotlin.d.a.a<m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ String f282a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ a f283b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(String str, a aVar, String str2) {
            super(0);
            this.f282a = str;
            this.f283b = aVar;
        }

        public final Object invoke() {
            b.b.a.c.b bVar = SupercellId.INSTANCE.getSharedServices$supercellId_release().f;
            StringBuilder a2 = b.a.a.a.a.a("Link ");
            a2.append(this.f282a);
            b.b.a.c.b.a(bVar, "FAQ", "click", a2.toString(), null, false, 24);
            MainActivity a3 = b.b.a.b.a((Fragment) this.f283b);
            if (a3 != null) {
                a3.a(this.f282a);
            }
            return m.f5330a;
        }
    }

    public final View a(int i) {
        if (this.n == null) {
            this.n = new HashMap();
        }
        View view = (View) this.n.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.n.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.n;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public final View b() {
        return (ImageButton) a(R.id.back_button);
    }

    public final View c() {
        return null;
    }

    public final NestedScrollView d() {
        return (NestedScrollView) a(R.id.faqScrollView);
    }

    public final View f() {
        return (RelativeLayout) a(R.id.faqToolbar);
    }

    public final float g() {
        return this.m;
    }

    public final List<View> h() {
        return kotlin.a.m.d(a(R.id.toolbarBackground), a(R.id.toolbarShadow));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_faq, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    public final void onResume() {
        super.onResume();
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a("FAQ");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.j.ac.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int
     arg types: [java.lang.String, java.lang.String, int, int, int]
     candidates:
      kotlin.j.ac.a(java.lang.CharSequence, char, int, boolean, int):int
      kotlin.j.ac.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):kotlin.i.h
      kotlin.j.ac.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
     arg types: [java.util.ArrayList, b.b.a.i.h1.a$a]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.LinearLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        String str;
        String str2;
        j.b(view, "view");
        super.onViewCreated(view, bundle);
        b.b.a.b.a((SubPageTabLayout) a(R.id.tabBarView), "faq_topics_heading", new kotlin.h[0]);
        Bundle arguments = getArguments();
        if (arguments == null || (str = arguments.getString("faqItemPrefix")) == null) {
            str = "";
        }
        b.b.a.i.x1.g gVar = SupercellId.INSTANCE.getSharedServices$supercellId_release().j.f;
        Set keySet = (gVar.c.size() > 0 ? gVar.c : gVar.d).keySet();
        ArrayList arrayList = new ArrayList();
        Iterator it = keySet.iterator();
        while (true) {
            kotlin.j jVar = null;
            if (!it.hasNext()) {
                break;
            }
            String str3 = (String) it.next();
            if (t.b(str3, str + "_item_heading_", false)) {
                String str4 = str + "_item_heading_";
                j.b(str3, "$this$substringAfter");
                j.b(str4, "delimiter");
                j.b(str3, "missingDelimiterValue");
                int a2 = t.a((CharSequence) str3, str4, 0, false, 6);
                if (a2 == -1) {
                    str2 = str3;
                } else {
                    str2 = str3.substring(a2 + str4.length(), str3.length());
                    j.a((Object) str2, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                }
                int parseInt = Integer.parseInt(str2);
                String str5 = str + "_item_description_" + parseInt;
                if (keySet.contains(str5)) {
                    String str6 = str + "_item_description_" + parseInt + "_phone";
                    Integer valueOf = Integer.valueOf(parseInt);
                    if (!keySet.contains(str6) || !SupercellId.INSTANCE.getRemoteConfiguration$supercellId_release().a(q0.SMS_ENABLED)) {
                        str6 = str5;
                    }
                    jVar = new kotlin.j(valueOf, str3, str6);
                }
            }
            if (jVar != null) {
                arrayList.add(jVar);
            }
        }
        int i = 0;
        for (Object next : kotlin.a.m.a((Iterable) arrayList, (Comparator) new C0023a())) {
            int i2 = i + 1;
            if (i < 0) {
                kotlin.a.m.a();
            }
            kotlin.j jVar2 = (kotlin.j) next;
            int intValue = ((Number) jVar2.f5299a).intValue();
            String str7 = (String) jVar2.f5300b;
            String str8 = (String) jVar2.c;
            String b2 = SupercellId.INSTANCE.getSharedServices$supercellId_release().j.b(str + "_item_link_" + intValue);
            d dVar = b2 != null ? new d(b2, this, str) : null;
            boolean z = i == 0;
            View inflate = LayoutInflater.from(getContext()).inflate(R.layout.fragment_faq_item, (ViewGroup) ((LinearLayout) a(R.id.faq_content)), false);
            ((LinearLayout) a(R.id.faq_content)).addView(inflate);
            j.a((Object) inflate, "itemRow");
            View findViewById = inflate.findViewById(R.id.rowSeparator);
            j.a((Object) findViewById, "itemRow.rowSeparator");
            findViewById.setVisibility(z ? 8 : 0);
            TextView textView = (TextView) inflate.findViewById(R.id.titleTextView);
            j.a((Object) textView, "itemRow.titleTextView");
            b.b.a.i.x1.j.a(textView, str7, (kotlin.d.a.b) null, 2);
            if (dVar != null) {
                TextView textView2 = (TextView) inflate.findViewById(R.id.descriptionTextView);
                j.a((Object) textView2, "itemRow.descriptionTextView");
                textView2.setMovementMethod(LinkMovementMethod.getInstance());
                TextView textView3 = (TextView) inflate.findViewById(R.id.descriptionTextView);
                j.a((Object) textView3, "itemRow.descriptionTextView");
                textView3.setLinksClickable(true);
                TextView textView4 = (TextView) inflate.findViewById(R.id.descriptionTextView);
                j.a((Object) textView4, "itemRow.descriptionTextView");
                b.b.a.i.x1.j.a(textView4, str8, new f(dVar), 33);
            } else {
                TextView textView5 = (TextView) inflate.findViewById(R.id.descriptionTextView);
                j.a((Object) textView5, "itemRow.descriptionTextView");
                b.b.a.i.x1.j.a(textView5, str8, (kotlin.d.a.b) null, 2);
            }
            String a3 = SupercellId.INSTANCE.getSharedServices$supercellId_release().j.a(str7);
            if (a3 == null) {
                a3 = str7;
            }
            ((LinearLayout) inflate.findViewById(R.id.titleRow)).setOnClickListener(new b(inflate, a3));
            ((ExpandableFrameLayout) inflate.findViewById(R.id.descriptionContainer)).setOnStateChangeListener(new c(this, inflate));
            i = i2;
        }
    }

    /* renamed from: b.b.a.i.h1.a$a  reason: collision with other inner class name */
    public final class C0023a<T> implements Comparator<T> {
        public final int compare(T t, T t2) {
            return kotlin.b.a.a(Integer.valueOf(((Number) ((kotlin.j) t).f5299a).intValue()), Integer.valueOf(((Number) ((kotlin.j) t2).f5299a).intValue()));
        }
    }
}
