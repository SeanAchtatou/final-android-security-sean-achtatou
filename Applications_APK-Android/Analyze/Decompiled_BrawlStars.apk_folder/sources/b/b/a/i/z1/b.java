package b.b.a.i.z1;

import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.FlowPager;
import android.support.v4.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import b.b.a.i.b;
import b.b.a.i.d0;
import b.b.a.i.e0;
import b.b.a.i.g;
import b.b.a.i.x0;
import b.b.a.i.z;
import b.b.a.j.a0;
import b.b.a.j.w0;
import com.facebook.share.internal.ShareConstants;
import com.supercell.id.IdAccount;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.view.SubPageTabLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import kotlin.a.an;
import kotlin.a.m;
import kotlin.d.b.h;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.j.t;

public final class b extends g {
    public static final w0 o = new w0("account_settings_heading", d.class);
    public static final w0 p = new w0("account_settings_subscriptions_heading", l.class);
    public static final C0101b q = new C0101b(null);
    public NestedScrollView m;
    public HashMap n;

    public static final class a extends b.a implements a0 {
        public static final Parcelable.Creator<a> CREATOR = new C0100a();
        public final Set<Integer> e = an.a((Object[]) new Integer[]{Integer.valueOf(R.id.nav_area_back_button), Integer.valueOf(R.id.nav_area_close_button)});
        public final boolean f = true;
        public final Class<? extends g> g = b.class;

        /* renamed from: b.b.a.i.z1.b$a$a  reason: collision with other inner class name */
        public final class C0100a implements Parcelable.Creator<a> {
            public final a createFromParcel(Parcel parcel) {
                j.b(parcel, ShareConstants.FEED_SOURCE_PARAM);
                j.b(parcel, "parcel");
                return new a();
            }

            public final a[] newArray(int i) {
                return new a[i];
            }
        }

        public final int a(int i, int i2, int i3) {
            return b.b.a.i.a0.u.a(i, i2, i3);
        }

        public final Class<? extends g> a() {
            return this.g;
        }

        public final Class<? extends x0> a(Resources resources) {
            j.b(resources, "resources");
            return b.b.a.b.b(resources) ? z.class : b.b.a.i.a.class;
        }

        public final int b(Resources resources, int i, int i2, int i3) {
            float f2;
            j.b(resources, "resources");
            j.b(resources, "$this$isSmallScreen");
            if (resources.getBoolean(R.bool.isSmallScreen)) {
                f2 = b.b.a.b.a(64);
            } else {
                f2 = b.b.a.b.a(150);
            }
            return i2 + kotlin.e.a.a(f2);
        }

        public final int c(Resources resources, int i, int i2, int i3) {
            j.b(resources, "resources");
            return b.b.a.i.a0.u.b(i, i2, i3);
        }

        public final Set<Integer> c() {
            return this.e;
        }

        public final boolean c(Resources resources) {
            j.b(resources, "resources");
            return !b.b.a.b.b(resources);
        }

        public final int describeContents() {
            return 0;
        }

        public final Class<? extends g> e(Resources resources) {
            j.b(resources, "resources");
            if (b.b.a.b.b(resources)) {
                return b.b.a.i.a0.class;
            }
            j.b(resources, "$this$isSmallScreen");
            return resources.getBoolean(R.bool.isSmallScreen) ? d0.class : e0.class;
        }

        public final boolean e() {
            return this.f;
        }

        public final void writeToParcel(Parcel parcel, int i) {
            j.b(parcel, "dest");
        }
    }

    /* renamed from: b.b.a.i.z1.b$b  reason: collision with other inner class name */
    public static final class C0101b {
        public /* synthetic */ C0101b(kotlin.d.b.g gVar) {
        }

        public final List<w0> a() {
            IdAccount idAccount = SupercellId.INSTANCE.getSharedServices$supercellId_release().i;
            String email = idAccount != null ? idAccount.getEmail() : null;
            if (email == null || t.a(email)) {
                return m.a(b.o);
            }
            return m.a((Object[]) new w0[]{b.o, b.p});
        }
    }

    public final /* synthetic */ class c extends h implements kotlin.d.a.b<Integer, String> {
        public c(b.b.a.j.x0 x0Var) {
            super(1, x0Var);
        }

        public final String getName() {
            return "getTitleKey";
        }

        public final kotlin.h.d getOwner() {
            return kotlin.d.b.t.a(b.b.a.j.x0.class);
        }

        public final String getSignature() {
            return "getTitleKey(I)Ljava/lang/String;";
        }

        public final Object invoke(Object obj) {
            return ((b.b.a.j.x0) this.receiver).a(((Number) obj).intValue());
        }
    }

    public final class d extends k implements kotlin.d.a.b<View, kotlin.m> {
        public d() {
            super(1);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: b.b.a.i.g.a(b.b.a.i.g, boolean, int, java.lang.Object):void
         arg types: [b.b.a.i.z1.b, int, int, ?[OBJECT, ARRAY]]
         candidates:
          b.b.a.i.g.a(b.b.a.i.g, android.view.View, int, int):void
          b.b.a.i.g.a(android.view.View, b.b.a.i.g$b, boolean, nl.komponents.kovenant.ap<java.lang.Boolean, java.lang.Exception>):void
          b.b.a.i.g.a(b.b.a.i.g, boolean, int, java.lang.Object):void */
        public final Object invoke(Object obj) {
            View view = (View) obj;
            b bVar = b.this;
            if (!(view instanceof NestedScrollView)) {
                view = null;
            }
            bVar.m = (NestedScrollView) view;
            g.a((g) b.this, false, 1, (Object) null);
            return kotlin.m.f5330a;
        }
    }

    public final View a(int i) {
        if (this.n == null) {
            this.n = new HashMap();
        }
        View view = (View) this.n.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.n.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.n;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public final NestedScrollView d() {
        return this.m;
    }

    public final View f() {
        return (FrameLayout) a(R.id.tabToolbar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_subpage_with_tabs, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.app.Fragment, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onStart() {
        g gVar;
        List<Fragment> fragments;
        super.onStart();
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager == null || (fragments = fragmentManager.getFragments()) == null) {
            gVar = null;
        } else {
            ArrayList arrayList = new ArrayList();
            for (T next : fragments) {
                Fragment fragment = (Fragment) next;
                j.a((Object) fragment, "it");
                if (fragment.getId() == R.id.top_area) {
                    arrayList.add(next);
                }
            }
            ArrayList arrayList2 = new ArrayList();
            for (Object next2 : arrayList) {
                if (next2 instanceof b.b.a.i.a0) {
                    arrayList2.add(next2);
                }
            }
            gVar = (g) m.d((List) arrayList2);
        }
        b.b.a.i.a0 a0Var = (b.b.a.i.a0) gVar;
        if (a0Var != null) {
            a0Var.b(-1);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.app.FragmentManager, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        j.b(view, "view");
        super.onViewCreated(view, bundle);
        FragmentManager childFragmentManager = getChildFragmentManager();
        j.a((Object) childFragmentManager, "childFragmentManager");
        b.b.a.j.x0 x0Var = new b.b.a.j.x0(childFragmentManager, q.a(), new d());
        FlowPager flowPager = (FlowPager) a(R.id.tabPager);
        if (flowPager != null) {
            flowPager.setAdapter(x0Var);
        }
        SubPageTabLayout subPageTabLayout = (SubPageTabLayout) a(R.id.tabBarView);
        if (subPageTabLayout != null) {
            subPageTabLayout.setGetTitleKey(new c(x0Var));
            subPageTabLayout.setupWithViewPager((FlowPager) a(R.id.tabPager), false);
        }
    }
}
