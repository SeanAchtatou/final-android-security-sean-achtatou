package b.b.a.i.q1;

import b.b.a.j.f0;
import b.b.a.j.o;
import b.b.a.j.t0;
import b.b.a.j.u0;
import java.util.List;
import kotlin.a.m;
import kotlin.d.a.a;
import kotlin.d.b.k;

public final class j extends k implements a<u0> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ f0 f564a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ List f565b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public j(f0 f0Var, List list) {
        super(0);
        this.f564a = f0Var;
        this.f565b = list;
    }

    public final u0 invoke() {
        List a2 = m.a(new o(this.f564a));
        List list = this.f565b;
        return new u0(list, a2, b.a.a.a.a.a(t0.c, list, a2, "DiffUtil.calculateDiff(R…create(oldRows, newRows))"));
    }
}
