package b.b.a.i;

import android.graphics.Bitmap;
import android.widget.ImageView;
import com.supercell.id.R;
import java.lang.ref.WeakReference;
import kotlin.d.a.b;
import kotlin.d.b.k;
import kotlin.m;

public final class b0 extends k implements b<Bitmap, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f195a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b0(WeakReference weakReference) {
        super(1);
        this.f195a = weakReference;
    }

    public final Object invoke(Object obj) {
        Object obj2 = this.f195a.get();
        if (obj2 != null) {
            Bitmap bitmap = (Bitmap) obj;
            ImageView imageView = (ImageView) ((a0) obj2).a(R.id.qr_code);
            if (imageView != null) {
                imageView.setImageBitmap(bitmap);
            }
        }
        return m.f5330a;
    }
}
