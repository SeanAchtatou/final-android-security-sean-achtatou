package b.b.a.i.p1;

import android.content.Context;
import android.content.res.Resources;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import b.b.a.i.p1.h;
import b.b.a.i.v1.j;
import b.b.a.j.b0;
import b.b.a.j.k0;
import b.b.a.j.o;
import b.b.a.j.q1;
import b.b.a.j.r;
import b.b.a.j.r0;
import b.b.a.j.s0;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.WidthAdjustingMultilineButton;

public final class g<T extends Fragment & h> extends s0 {

    /* renamed from: b  reason: collision with root package name */
    public final T f489b;

    public final class a implements View.OnClickListener {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ r0 f491b;

        public a(r0 r0Var, int i) {
            this.f491b = r0Var;
        }

        public final void onClick(View view) {
            if (!(view instanceof Button)) {
                view = null;
            }
            Button button = (Button) view;
            if (button != null) {
                button.setEnabled(false);
            }
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Messages - Friends", "click", "Accept", null, false, 24);
            ((h) g.this.f489b).b((a) this.f491b);
        }
    }

    public final class b implements View.OnClickListener {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ r0 f493b;

        public b(r0 r0Var, int i) {
            this.f493b = r0Var;
        }

        public final void onClick(View view) {
            if (!(view instanceof Button)) {
                view = null;
            }
            Button button = (Button) view;
            if (button != null) {
                button.setEnabled(false);
            }
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Messages - Friends", "click", "Decline", null, false, 24);
            ((h) g.this.f489b).a((a) this.f493b);
        }
    }

    public final class c implements View.OnClickListener {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ r0 f495b;

        public c(r0 r0Var, int i) {
            this.f495b = r0Var;
        }

        public final void onClick(View view) {
            MainActivity a2 = b.b.a.b.a((Fragment) g.this.f489b);
            if (a2 != null) {
                a aVar = (a) this.f495b;
                a2.a(new j.a(null, aVar.f472b, aVar.c, aVar.d, aVar.e, null, false, 64));
            }
        }
    }

    public final class d implements View.OnClickListener {

        /* renamed from: a  reason: collision with root package name */
        public static final d f496a = new d();

        public final void onClick(View view) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Messages - Friends", "click", "Retry", null, false, 24);
            SupercellId.INSTANCE.getSharedServices$supercellId_release().e().a();
        }
    }

    public g(Context context, T t) {
        kotlin.d.b.j.b(context, "context");
        kotlin.d.b.j.b(t, "fragment");
        this.f489b = t;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.ImageView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void a(s0.a aVar, int i, r0 r0Var) {
        Resources resources;
        kotlin.d.b.j.b(aVar, "holder");
        kotlin.d.b.j.b(r0Var, "item");
        if (r0Var instanceof a) {
            b.b.a.b.a((ConstraintLayout) aVar.c.findViewById(R.id.messageContainer), b.b.a.b.b(this.f1164a, i), b.b.a.b.a(this.f1164a, i), 0, 0);
            Context context = aVar.c.getContext();
            if (!(context == null || (resources = context.getResources()) == null)) {
                k0.f1078b.a(((a) r0Var).d, (ImageView) aVar.c.findViewById(R.id.friendImageView), resources);
            }
            TextView textView = (TextView) aVar.c.findViewById(R.id.friendNameLabel);
            kotlin.d.b.j.a((Object) textView, "containerView.friendNameLabel");
            a aVar2 = (a) r0Var;
            String str = aVar2.c;
            if (str == null) {
                str = r.f1160a.a(aVar2.f472b);
            }
            textView.setText(str);
            ((TextView) aVar.c.findViewById(R.id.friendNameLabel)).setTextColor(ContextCompat.getColor(aVar.c.getContext(), aVar2.c == null ? R.color.gray40 : R.color.black));
            TextView textView2 = (TextView) aVar.c.findViewById(R.id.requestStatusLabel);
            kotlin.d.b.j.a((Object) textView2, "containerView.requestStatusLabel");
            b.b.a.i.x1.j.a(textView2, aVar2.e.f125b);
            WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) aVar.c.findViewById(R.id.confirmButton);
            kotlin.d.b.j.a((Object) widthAdjustingMultilineButton, "containerView.confirmButton");
            widthAdjustingMultilineButton.setEnabled(true);
            WidthAdjustingMultilineButton widthAdjustingMultilineButton2 = (WidthAdjustingMultilineButton) aVar.c.findViewById(R.id.declineButton);
            kotlin.d.b.j.a((Object) widthAdjustingMultilineButton2, "containerView.declineButton");
            widthAdjustingMultilineButton2.setEnabled(true);
            ((WidthAdjustingMultilineButton) aVar.c.findViewById(R.id.confirmButton)).setOnClickListener(new a(r0Var, i));
            ((WidthAdjustingMultilineButton) aVar.c.findViewById(R.id.declineButton)).setOnClickListener(new b(r0Var, i));
            ((ConstraintLayout) aVar.c.findViewById(R.id.messageContainer)).setOnClickListener(new c(r0Var, i));
        } else if (r0Var instanceof o) {
            LinearLayout linearLayout = (LinearLayout) aVar.c.findViewById(R.id.errorContainer);
            ViewGroup.MarginLayoutParams d2 = q1.d(linearLayout);
            if (d2 != null) {
                d2.topMargin = 0;
            }
            linearLayout.requestLayout();
            ((WidthAdjustingMultilineButton) aVar.c.findViewById(R.id.errorRetryButton)).setOnClickListener(d.f496a);
        } else if (r0Var instanceof b0) {
            ImageView imageView = (ImageView) aVar.c.findViewById(R.id.message_image);
            kotlin.d.b.j.a((Object) imageView, "containerView.message_image");
            imageView.setVisibility(0);
        }
    }
}
