package b.b.a.i.x1;

import com.supercell.id.view.AvatarView;
import java.util.NoSuchElementException;
import kotlin.d.b.g;
import kotlin.d.b.j;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class e extends Enum<e> {
    public static final /* synthetic */ e[] c = {new e("LDPI", 0, "ldpi", 120), new e("MDPI", 1, "mdpi", AvatarView.INTRINSIC_POINT_SIZE), new e("HDPI", 2, "hdpi", 240), new e("XHDPI", 3, "xhdpi", 320), new e("XXHDPI", 4, "xxhdpi", 480), new e("XXXHDPI", 5, "xxxhdpi", 640)};
    public static final a d = new a(null);

    /* renamed from: a  reason: collision with root package name */
    public final String f913a;

    /* renamed from: b  reason: collision with root package name */
    public final int f914b;

    public static final class a {
        public /* synthetic */ a(g gVar) {
        }

        public final e a(int i) {
            e eVar;
            e[] values = e.values();
            int length = values.length;
            boolean z = false;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    eVar = null;
                    break;
                }
                eVar = values[i2];
                if (eVar.f914b >= i) {
                    break;
                }
                i2++;
            }
            if (eVar != null) {
                return eVar;
            }
            j.b(values, "$this$last");
            if (values.length == 0) {
                z = true;
            }
            if (!z) {
                j.b(values, "$this$lastIndex");
                return values[values.length - 1];
            }
            throw new NoSuchElementException("Array is empty.");
        }
    }

    public e(String str, int i, String str2, int i2) {
        this.f913a = str2;
        this.f914b = i2;
    }

    public static e valueOf(String str) {
        return (e) Enum.valueOf(e.class, str);
    }

    public static e[] values() {
        return (e[]) c.clone();
    }

    public final int a() {
        return this.f914b;
    }
}
