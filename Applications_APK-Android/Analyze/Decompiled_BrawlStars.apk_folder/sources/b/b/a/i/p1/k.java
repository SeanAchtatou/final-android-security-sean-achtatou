package b.b.a.i.p1;

import b.b.a.j.b0;
import b.b.a.j.t0;
import b.b.a.j.u0;
import java.util.List;
import kotlin.a.m;
import kotlin.d.a.a;

public final class k extends kotlin.d.b.k implements a<u0> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ List f510a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k(List list) {
        super(0);
        this.f510a = list;
    }

    public final u0 invoke() {
        List a2 = m.a(new b0("account_messages_no_new_messages"));
        List list = this.f510a;
        return new u0(list, a2, b.a.a.a.a.a(t0.c, list, a2, "DiffUtil.calculateDiff(R…create(oldRows, newRows))"));
    }
}
