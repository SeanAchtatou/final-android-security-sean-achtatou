package b.b.a.i.k1;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import b.b.a.i.a1;
import b.b.a.i.b;
import b.b.a.i.x0;
import b.b.a.i.z0;
import b.b.a.j.a0;
import b.b.a.j.f0;
import b.b.a.j.k0;
import b.b.a.j.m;
import b.b.a.j.r0;
import b.b.a.j.s0;
import b.b.a.j.t0;
import b.b.a.j.u0;
import b.b.a.j.y0;
import com.facebook.share.internal.ShareConstants;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.WidthAdjustingMultilineButton;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import kotlin.a.ab;
import kotlin.a.an;
import kotlin.d.b.j;
import kotlin.k;
import nl.komponents.kovenant.bb;

public final class c extends b.b.a.i.g {
    public List<? extends r0> m;
    public final kotlin.d.a.b<m<b.b.a.h.d, f0>, kotlin.m> n = new d();
    public final y0<u0> o = new y0<>(new e(), new f());
    public HashMap p;

    public static final class a extends b.a implements a0 {
        public static final Parcelable.Creator<a> CREATOR = new C0031a();
        public static final b g = new b(null);
        public final boolean e;
        public final Class<? extends b.b.a.i.g> f = c.class;

        /* renamed from: b.b.a.i.k1.c$a$a  reason: collision with other inner class name */
        public final class C0031a implements Parcelable.Creator<a> {
            public final a createFromParcel(Parcel parcel) {
                j.b(parcel, ShareConstants.FEED_SOURCE_PARAM);
                j.b(parcel, "parcel");
                return new a();
            }

            public final a[] newArray(int i) {
                return new a[i];
            }
        }

        public static final class b {
            public /* synthetic */ b(kotlin.d.b.g gVar) {
            }

            public final int a(int i, int i2, int i3) {
                int a2 = i - kotlin.e.a.a(Math.max(b.b.a.b.a(320) + ((float) i3), ((float) i) * 0.556f));
                if (a2 >= i2 + kotlin.e.a.a(b.b.a.b.a(40))) {
                    return a2;
                }
                return 0;
            }
        }

        public final int a(int i, int i2, int i3) {
            return g.a(i, i2, i3);
        }

        public final int a(Resources resources, int i, int i2, int i3) {
            j.b(resources, "resources");
            if ((i - i2) - i3 >= kotlin.e.a.a(b.b.a.b.a(460))) {
                return kotlin.e.a.a(((float) i) * 0.3f);
            }
            return 0;
        }

        public final Class<? extends b.b.a.i.g> a() {
            return this.f;
        }

        public final Class<? extends x0> a(Resources resources) {
            j.b(resources, "resources");
            return z0.class;
        }

        public final int b(Resources resources, int i, int i2, int i3) {
            j.b(resources, "resources");
            return i2 + kotlin.e.a.a(b.b.a.b.a(68));
        }

        public final int c(Resources resources, int i, int i2, int i3) {
            j.b(resources, "resources");
            return 0;
        }

        public final boolean c(Resources resources) {
            j.b(resources, "resources");
            return b.b.a.b.b(resources);
        }

        public final int describeContents() {
            return 0;
        }

        public final Class<? extends b.b.a.i.g> e(Resources resources) {
            j.b(resources, "resources");
            return b.b.a.b.b(resources) ? a1.class : C0033c.class;
        }

        public final boolean e() {
            return this.e;
        }

        public final void writeToParcel(Parcel parcel, int i) {
            j.b(parcel, "dest");
        }
    }

    public static final class b extends s0 {

        /* renamed from: b  reason: collision with root package name */
        public final c f350b;

        public final class a implements View.OnClickListener {

            /* renamed from: b  reason: collision with root package name */
            public final /* synthetic */ r0 f352b;

            public a(r0 r0Var, int i) {
                this.f352b = r0Var;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
             arg types: [android.view.View, java.lang.String]
             candidates:
              kotlin.d.b.j.a(int, int):int
              kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
              kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
              kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
            public final void onClick(View view) {
                j.a((Object) view, "it");
                view.setEnabled(false);
                if (!(view instanceof TextView)) {
                    view = null;
                }
                TextView textView = (TextView) view;
                if (textView != null) {
                    b.b.a.i.x1.j.a(textView, "ingame_invite_to_play_sent", (kotlin.d.a.b) null, 2);
                }
                b.this.f350b.a((b) this.f352b);
            }
        }

        /* renamed from: b.b.a.i.k1.c$b$b  reason: collision with other inner class name */
        public final class C0032b implements View.OnClickListener {
            public C0032b(r0 r0Var, int i) {
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
             arg types: [android.view.View, java.lang.String]
             candidates:
              kotlin.d.b.j.a(int, int):int
              kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
              kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
              kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
            public final void onClick(View view) {
                j.a((Object) view, "it");
                view.setEnabled(false);
                b.this.f350b.i();
            }
        }

        public b(c cVar) {
            j.b(cVar, "fragment");
            this.f350b = cVar;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: b.b.a.b.a(android.view.View, boolean, boolean, int, int):void
         arg types: [android.widget.LinearLayout, int, boolean, int, int]
         candidates:
          b.b.a.b.a(android.content.Context, java.util.List, int, boolean, int):void
          b.b.a.b.a(android.content.Context, java.util.List<? extends android.view.View>, java.util.List<b.b.a.j.c1>, int, boolean):void
          b.b.a.b.a(android.content.Context, java.util.List<? extends android.view.View>, kotlin.d.a.a<? extends java.util.List<b.b.a.j.c1>>, android.support.v4.view.ViewPager, kotlin.d.a.c<? super android.view.View, ? super java.lang.Integer, kotlin.m>):void
          b.b.a.b.a(android.view.View, boolean, boolean, int, int):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.TextView, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, kotlin.d.a.b, int):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String, ?[OBJECT, ARRAY], int]
         candidates:
          b.b.a.i.x1.j.a(android.widget.ImageView, java.lang.String, boolean, int):void
          b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, java.lang.Object, int):void
          b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, kotlin.d.a.b, int):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.Button, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void a(s0.a aVar, int i, r0 r0Var) {
            Resources resources;
            j.b(aVar, "holder");
            j.b(r0Var, "item");
            if (r0Var instanceof b) {
                b.b.a.b.a((View) ((LinearLayout) aVar.c.findViewById(R.id.friend_container)), false, b.b.a.b.a(this.f1164a, i), 0, 0);
                Context context = aVar.c.getContext();
                if (!(context == null || (resources = context.getResources()) == null)) {
                    k0.f1078b.a(((b) r0Var).d, (ImageView) aVar.c.findViewById(R.id.friendImageView), resources);
                }
                TextView textView = (TextView) aVar.c.findViewById(R.id.friendNameLabel);
                j.a((Object) textView, "containerView.friendNameLabel");
                b bVar = (b) r0Var;
                textView.setText(bVar.c);
                ((WidthAdjustingMultilineButton) aVar.c.findViewById(R.id.inviteButton)).setOnClickListener(new a(r0Var, i));
                WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) aVar.c.findViewById(R.id.inviteButton);
                j.a((Object) widthAdjustingMultilineButton, "containerView.inviteButton");
                widthAdjustingMultilineButton.setEnabled(!bVar.e);
                WidthAdjustingMultilineButton widthAdjustingMultilineButton2 = (WidthAdjustingMultilineButton) aVar.c.findViewById(R.id.inviteButton);
                j.a((Object) widthAdjustingMultilineButton2, "containerView.inviteButton");
                b.b.a.i.x1.j.a((TextView) widthAdjustingMultilineButton2, bVar.e ? "ingame_invite_to_play_sent" : "ingame_invite_to_play_invite", (kotlin.d.a.b) null, 2);
            } else if (r0Var instanceof a) {
                b.b.a.b.a((LinearLayout) aVar.c.findViewById(R.id.invite_all_container), b.b.a.b.b(this.f1164a, i), b.b.a.b.a(this.f1164a, i), 0, 0);
                a aVar2 = (a) r0Var;
                int i2 = aVar2.f347b;
                TextView textView2 = (TextView) aVar.c.findViewById(R.id.online_friend_count_label);
                if (textView2 != null) {
                    b.b.a.i.x1.j.a(textView2, "ingame_invite_to_play_online_count", k.a("count", String.valueOf(i2)));
                }
                ((Button) aVar.c.findViewById(R.id.invite_all_button)).setOnClickListener(new C0032b(r0Var, i));
                ((Button) aVar.c.findViewById(R.id.invite_all_button)).setTextColor(ContextCompat.getColor(aVar.c.getContext(), !aVar2.c ? R.color.text_blue : R.color.gray80));
                Button button = (Button) aVar.c.findViewById(R.id.invite_all_button);
                j.a((Object) button, "containerView.invite_all_button");
                button.setEnabled(!aVar2.c);
            }
        }
    }

    /* renamed from: b.b.a.i.k1.c$c  reason: collision with other inner class name */
    public static final class C0033c extends b.b.a.i.g {
        public HashMap m;

        public final void a() {
            HashMap hashMap = this.m;
            if (hashMap != null) {
                hashMap.clear();
            }
        }

        public final View c() {
            int i = R.id.top_area_close_button;
            if (this.m == null) {
                this.m = new HashMap();
            }
            View view = (View) this.m.get(Integer.valueOf(i));
            if (view == null) {
                View view2 = getView();
                if (view2 == null) {
                    view = null;
                } else {
                    view = view2.findViewById(i);
                    this.m.put(Integer.valueOf(i), view);
                }
            }
            return (ImageButton) view;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
            j.b(layoutInflater, "inflater");
            return layoutInflater.inflate(R.layout.fragment_ingame_invite_to_play_top_area, viewGroup, false);
        }

        public final /* synthetic */ void onDestroyView() {
            super.onDestroyView();
            a();
        }
    }

    public final class d extends kotlin.d.b.k implements kotlin.d.a.b<m<? extends b.b.a.h.d, ? extends f0>, kotlin.m> {
        public d() {
            super(1);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.a.p.a(java.lang.Iterable, int):int
         arg types: [java.util.List<b.b.a.h.c>, int]
         candidates:
          kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
          kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
          kotlin.a.w.a(java.util.List, int):T
          kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
          kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
          kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
          kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
          kotlin.a.s.a(java.util.List, java.util.Comparator):void
          kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
          kotlin.a.p.a(java.lang.Iterable, int):int */
        public final void a(m<b.b.a.h.d, f0> mVar) {
            b.b.a.h.d a2;
            if (mVar != null && (a2 = mVar.a()) != null) {
                c cVar = c.this;
                List<b.b.a.h.c> list = a2.f112a;
                ArrayList arrayList = new ArrayList(kotlin.a.m.a((Iterable) list, 10));
                for (b.b.a.h.c cVar2 : list) {
                    arrayList.add(new b(cVar2.f110a, cVar2.f111b, cVar2.c, false));
                }
                cVar.a(arrayList);
            }
        }

        public final /* synthetic */ Object invoke(Object obj) {
            a((m) obj);
            return kotlin.m.f5330a;
        }
    }

    public final class e extends kotlin.d.b.k implements kotlin.d.a.b<u0, kotlin.m> {
        public e() {
            super(1);
        }

        public final void a(u0 u0Var) {
            j.b(u0Var, "it");
            c cVar = c.this;
            if (cVar.m == u0Var.f1176a) {
                cVar.m = u0Var.f1177b;
                if (cVar.m == null) {
                    RecyclerView recyclerView = (RecyclerView) cVar.a(R.id.friends_list);
                    if (recyclerView != null) {
                        recyclerView.setVisibility(4);
                    }
                    View a2 = c.this.a(R.id.progressBar);
                    if (a2 != null) {
                        a2.setVisibility(0);
                    }
                } else {
                    RecyclerView recyclerView2 = (RecyclerView) cVar.a(R.id.friends_list);
                    if (recyclerView2 != null) {
                        recyclerView2.setVisibility(0);
                    }
                    View a3 = c.this.a(R.id.progressBar);
                    if (a3 != null) {
                        a3.setVisibility(4);
                    }
                }
                RecyclerView recyclerView3 = (RecyclerView) c.this.a(R.id.friends_list);
                RecyclerView.Adapter adapter = recyclerView3 != null ? recyclerView3.getAdapter() : null;
                if (!(adapter instanceof b)) {
                    adapter = null;
                }
                b bVar = (b) adapter;
                if (bVar != null) {
                    List<? extends r0> list = c.this.m;
                    if (list == null) {
                        list = ab.f5210a;
                    }
                    bVar.a(list);
                    u0Var.c.dispatchUpdatesTo(bVar);
                }
            }
        }

        public final /* synthetic */ Object invoke(Object obj) {
            a((u0) obj);
            return kotlin.m.f5330a;
        }
    }

    public final class f extends kotlin.d.b.k implements kotlin.d.a.b<Exception, kotlin.m> {
        public f() {
            super(1);
        }

        public final Object invoke(Object obj) {
            Exception exc = (Exception) obj;
            j.b(exc, "it");
            MainActivity a2 = b.b.a.b.a((Fragment) c.this);
            if (a2 != null) {
                a2.a(exc, (kotlin.d.a.b<? super b.b.a.i.e, kotlin.m>) null);
            }
            return kotlin.m.f5330a;
        }
    }

    public final class g extends kotlin.d.b.k implements kotlin.d.a.b<Exception, kotlin.m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f357a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ Set f358b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(WeakReference weakReference, Set set) {
            super(1);
            this.f357a = weakReference;
            this.f358b = set;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.a.p.a(java.lang.Iterable, int):int
         arg types: [java.util.ArrayList, int]
         candidates:
          kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
          kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
          kotlin.a.w.a(java.util.List, int):T
          kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
          kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
          kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
          kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
          kotlin.a.s.a(java.util.List, java.util.Comparator):void
          kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
          kotlin.a.p.a(java.lang.Iterable, int):int */
        public final void invoke(Exception exc) {
            Object obj = this.f357a.get();
            if (obj != null) {
                Exception exc2 = exc;
                c cVar = (c) obj;
                List<? extends r0> list = cVar.m;
                if (list != null) {
                    ArrayList<b> arrayList = new ArrayList<>();
                    for (T next : list) {
                        if (next instanceof b) {
                            arrayList.add(next);
                        }
                    }
                    ArrayList arrayList2 = new ArrayList(kotlin.a.m.a((Iterable) arrayList, 10));
                    for (b bVar : arrayList) {
                        if (this.f358b.contains(bVar.f349b)) {
                            bVar = b.a(bVar, null, null, null, false, 7);
                        }
                        arrayList2.add(bVar);
                    }
                    cVar.a(arrayList2);
                }
                MainActivity a2 = b.b.a.b.a((Fragment) cVar);
                if (a2 != null) {
                    a2.a(exc2, (kotlin.d.a.b<? super b.b.a.i.e, kotlin.m>) null);
                }
            }
        }
    }

    public final class h extends kotlin.d.b.k implements kotlin.d.a.b<Exception, kotlin.m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f359a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ b f360b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(WeakReference weakReference, b bVar) {
            super(1);
            this.f359a = weakReference;
            this.f360b = bVar;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.a.p.a(java.lang.Iterable, int):int
         arg types: [java.util.ArrayList, int]
         candidates:
          kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
          kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
          kotlin.a.w.a(java.util.List, int):T
          kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
          kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
          kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
          kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
          kotlin.a.s.a(java.util.List, java.util.Comparator):void
          kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
          kotlin.a.p.a(java.lang.Iterable, int):int */
        public final void invoke(Exception exc) {
            Object obj = this.f359a.get();
            if (obj != null) {
                Exception exc2 = exc;
                c cVar = (c) obj;
                List<? extends r0> list = cVar.m;
                if (list != null) {
                    ArrayList<b> arrayList = new ArrayList<>();
                    for (T next : list) {
                        if (next instanceof b) {
                            arrayList.add(next);
                        }
                    }
                    ArrayList arrayList2 = new ArrayList(kotlin.a.m.a((Iterable) arrayList, 10));
                    for (b bVar : arrayList) {
                        if (bVar.a(this.f360b)) {
                            bVar = b.a(bVar, null, null, null, this.f360b.e, 7);
                        }
                        arrayList2.add(bVar);
                    }
                    cVar.a(arrayList2);
                }
                MainActivity a2 = b.b.a.b.a((Fragment) cVar);
                if (a2 != null) {
                    a2.a(exc2, (kotlin.d.a.b<? super b.b.a.i.e, kotlin.m>) null);
                }
            }
        }
    }

    public final class i extends kotlin.d.b.k implements kotlin.d.a.a<u0> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ List f361a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ List f362b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(List list, List list2) {
            super(0);
            this.f361a = list;
            this.f362b = list2;
        }

        public final Object invoke() {
            List<r0> a2 = d.a(this.f361a);
            List list = this.f362b;
            return new u0(list, a2, b.a.a.a.a.a(t0.c, list, a2, "DiffUtil.calculateDiff(R…create(oldRows, newRows))"));
        }
    }

    public final View a(int i2) {
        if (this.p == null) {
            this.p = new HashMap();
        }
        View view = (View) this.p.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i2);
        this.p.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.p.a(java.lang.Iterable, int):int
     arg types: [java.util.ArrayList, int]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.p.a(java.lang.Iterable, int):int */
    public final void a(b bVar) {
        b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Invite to Play", "click", "Invite", null, false, 24);
        List<? extends r0> list = this.m;
        if (list != null) {
            ArrayList<b> arrayList = new ArrayList<>();
            for (T next : list) {
                if (next instanceof b) {
                    arrayList.add(next);
                }
            }
            ArrayList arrayList2 = new ArrayList(kotlin.a.m.a((Iterable) arrayList, 10));
            for (b bVar2 : arrayList) {
                if (bVar2.a(bVar)) {
                    bVar2 = b.a(bVar2, null, null, null, true, 7);
                }
                arrayList2.add(bVar2);
            }
            a(arrayList2);
        }
        nl.komponents.kovenant.c.m.b(SupercellId.INSTANCE.getSharedServices$supercellId_release().h.a(an.a(bVar.f349b)), new h(new WeakReference(this), bVar));
    }

    public final void a(List<b> list) {
        this.o.a(bb.f5389a);
    }

    public final View c() {
        return (ImageButton) a(R.id.close_button);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.p.a(java.lang.Iterable, int):int
     arg types: [java.util.ArrayList, int]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.p.a(java.lang.Iterable, int):int */
    public final void i() {
        ArrayList<b> arrayList;
        List<? extends r0> list = this.m;
        Set set = null;
        if (list != null) {
            arrayList = new ArrayList<>();
            for (T next : list) {
                if (next instanceof b) {
                    arrayList.add(next);
                }
            }
        } else {
            arrayList = null;
        }
        boolean z = true;
        if (arrayList != null) {
            ArrayList<b> arrayList2 = new ArrayList<>();
            for (Object next2 : arrayList) {
                if (!((b) next2).e) {
                    arrayList2.add(next2);
                }
            }
            ArrayList arrayList3 = new ArrayList(kotlin.a.m.a((Iterable) arrayList2, 10));
            for (b bVar : arrayList2) {
                arrayList3.add(bVar.f349b);
            }
            set = kotlin.a.m.i(arrayList3);
        }
        if (set != null && !set.isEmpty()) {
            z = false;
        }
        if (!z) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Invite to Play", "click", "Invite all", null, false, 24);
            ArrayList arrayList4 = new ArrayList(kotlin.a.m.a((Iterable) arrayList, 10));
            for (b bVar2 : arrayList) {
                if (!bVar2.e) {
                    bVar2 = b.a(bVar2, null, null, null, true, 7);
                }
                arrayList4.add(bVar2);
            }
            a(arrayList4);
            nl.komponents.kovenant.c.m.b(SupercellId.INSTANCE.getSharedServices$supercellId_release().h.a(set), new g(new WeakReference(this), set));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_ingame_invite_to_play, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    public final void onResume() {
        super.onResume();
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a("Invite to Play");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v7.widget.RecyclerView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        j.b(view, "view");
        super.onViewCreated(view, bundle);
        if (this.m == null) {
            RecyclerView recyclerView = (RecyclerView) a(R.id.friends_list);
            j.a((Object) recyclerView, "friends_list");
            recyclerView.setVisibility(4);
            View a2 = a(R.id.progressBar);
            j.a((Object) a2, "progressBar");
            a2.setVisibility(0);
        } else {
            RecyclerView recyclerView2 = (RecyclerView) a(R.id.friends_list);
            j.a((Object) recyclerView2, "friends_list");
            recyclerView2.setVisibility(0);
            View a3 = a(R.id.progressBar);
            j.a((Object) a3, "progressBar");
            a3.setVisibility(4);
        }
        b bVar = new b(this);
        List<? extends r0> list = this.m;
        if (list == null) {
            list = ab.f5210a;
        }
        bVar.a(list);
        RecyclerView recyclerView3 = (RecyclerView) a(R.id.friends_list);
        j.a((Object) recyclerView3, "friends_list");
        recyclerView3.setLayoutManager(new LinearLayoutManager(getContext()));
        RecyclerView recyclerView4 = (RecyclerView) a(R.id.friends_list);
        j.a((Object) recyclerView4, "friends_list");
        recyclerView4.setAdapter(bVar);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().e().a(this.n);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().e().a();
    }
}
