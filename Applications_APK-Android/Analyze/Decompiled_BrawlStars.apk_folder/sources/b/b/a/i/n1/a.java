package b.b.a.i.n1;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.ShareCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import b.b.a.i.a0;
import b.b.a.i.b;
import b.b.a.i.d0;
import b.b.a.i.e0;
import b.b.a.i.j1.c;
import b.b.a.i.x0;
import b.b.a.i.y1.a;
import b.b.a.i.z;
import b.b.a.j.f0;
import b.b.a.j.i0;
import b.b.a.j.o1;
import b.b.a.j.q;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.SubPageTabLayout;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import kotlin.a.ad;
import kotlin.a.an;
import kotlin.d.b.k;
import kotlin.m;
import nl.komponents.kovenant.bw;

public final class a extends b.b.a.i.g {
    public final kotlin.d.a.b<i0, m> m = new d();
    public final kotlin.d.a.b<b.b.a.j.m<b.b.a.h.d, f0>, m> n = new c();
    public final kotlin.d.a.b<b.b.a.h.a, m> o = new b();
    public boolean p;
    public HashMap q;

    /* renamed from: b.b.a.i.n1.a$a  reason: collision with other inner class name */
    public static final class C0036a extends b.a {
        public static final C0037a CREATOR = new C0037a(null);
        public final Set<Integer> e = an.a((Object[]) new Integer[]{Integer.valueOf(R.id.nav_area_back_button), Integer.valueOf(R.id.nav_area_close_button)});
        public final boolean f = true;
        public final Class<? extends b.b.a.i.g> g = a.class;

        /* renamed from: b.b.a.i.n1.a$a$a  reason: collision with other inner class name */
        public static final class C0037a implements Parcelable.Creator<C0036a> {
            public /* synthetic */ C0037a(kotlin.d.b.g gVar) {
            }

            public final Object createFromParcel(Parcel parcel) {
                kotlin.d.b.j.b(parcel, "parcel");
                return new C0036a();
            }

            public final Object[] newArray(int i) {
                return new C0036a[i];
            }
        }

        public final int a(int i, int i2, int i3) {
            return a0.u.a(i, i2, i3);
        }

        public final Class<? extends b.b.a.i.g> a() {
            return this.g;
        }

        public final Class<? extends x0> a(Resources resources) {
            kotlin.d.b.j.b(resources, "resources");
            return b.b.a.b.b(resources) ? z.class : b.b.a.i.a.class;
        }

        public final int b(Resources resources, int i, int i2, int i3) {
            float f2;
            kotlin.d.b.j.b(resources, "resources");
            kotlin.d.b.j.b(resources, "$this$isSmallScreen");
            if (resources.getBoolean(R.bool.isSmallScreen)) {
                f2 = b.b.a.b.a(64);
            } else {
                f2 = b.b.a.b.a(150);
            }
            return i2 + kotlin.e.a.a(f2);
        }

        public final int c(Resources resources, int i, int i2, int i3) {
            kotlin.d.b.j.b(resources, "resources");
            return a0.u.b(i, i2, i3);
        }

        public final Set<Integer> c() {
            return this.e;
        }

        public final boolean c(Resources resources) {
            kotlin.d.b.j.b(resources, "resources");
            return !b.b.a.b.b(resources);
        }

        public final int describeContents() {
            return 0;
        }

        public final Class<? extends b.b.a.i.g> e(Resources resources) {
            kotlin.d.b.j.b(resources, "resources");
            if (b.b.a.b.b(resources)) {
                return a0.class;
            }
            kotlin.d.b.j.b(resources, "$this$isSmallScreen");
            return resources.getBoolean(R.bool.isSmallScreen) ? d0.class : e0.class;
        }

        public final boolean e() {
            return this.f;
        }

        public final void writeToParcel(Parcel parcel, int i) {
        }
    }

    public final class b extends k implements kotlin.d.a.b<b.b.a.h.a, m> {
        public b() {
            super(1);
        }

        public final Object invoke(Object obj) {
            a.this.a((i0) SupercellId.INSTANCE.getSharedServices$supercellId_release().f().f1179a);
            return m.f5330a;
        }
    }

    public final class c extends k implements kotlin.d.a.b<b.b.a.j.m<? extends b.b.a.h.d, ? extends f0>, m> {
        public c() {
            super(1);
        }

        public final Object invoke(Object obj) {
            a.this.a((i0) SupercellId.INSTANCE.getSharedServices$supercellId_release().f().f1179a);
            return m.f5330a;
        }
    }

    public final class d extends k implements kotlin.d.a.b<i0, m> {
        public d() {
            super(1);
        }

        public final Object invoke(Object obj) {
            b.b.a.h.h a2;
            bw<Bitmap, Exception> a3;
            i0 i0Var = (i0) obj;
            if (!(!a.this.isAdded() || i0Var == null || (a2 = i0Var.a()) == null)) {
                String str = a2.f;
                if (!(str == null || (a3 = b.b.a.j.e0.f1030b.a(str)) == null)) {
                    nl.komponents.kovenant.c.m.a(a3, new b(new WeakReference(a.this)));
                }
                a.this.a(i0Var);
            }
            return m.f5330a;
        }
    }

    public final class e extends k implements kotlin.d.a.b<String, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f395a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(WeakReference weakReference) {
            super(1);
            this.f395a = weakReference;
        }

        public final Object invoke(Object obj) {
            TextView textView;
            String str = (String) obj;
            kotlin.d.b.j.b(str, "it");
            a aVar = (a) this.f395a.get();
            if (!(aVar == null || (textView = (TextView) aVar.a(R.id.invite_ingame_friends_title)) == null)) {
                b.b.a.i.x1.j.a(textView, "account_invite_friends_ingame_title", kotlin.k.a("game", str));
            }
            return m.f5330a;
        }
    }

    public final class f implements View.OnClickListener {
        public f() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Invite Friends", "click", "My QR code info", null, false, 24);
            MainActivity a2 = b.b.a.b.a((Fragment) a.this);
            if (a2 != null) {
                kotlin.d.b.j.a((Object) view, "it");
                b.b.a.b.a(a2, view);
            }
        }
    }

    public final class g implements View.OnClickListener {
        public g() {
        }

        public final void onClick(View view) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Invite Friends", "click", "Scan QR code", null, false, 24);
            MainActivity a2 = b.b.a.b.a((Fragment) a.this);
            if (a2 != null) {
                a2.a(new a.C0095a());
            }
        }
    }

    public final class h implements View.OnClickListener {
        public h() {
        }

        public final void onClick(View view) {
            b.b.a.h.h a2;
            String str;
            if (!a.this.p) {
                b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Invite Friends", "click", "Share invite", null, false, 24);
                i0 i0Var = (i0) SupercellId.INSTANCE.getSharedServices$supercellId_release().f().f1179a;
                if (i0Var != null && (a2 = i0Var.a()) != null && (str = a2.g) != null) {
                    a aVar = a.this;
                    aVar.p = true;
                    MainActivity a3 = b.b.a.b.a((Fragment) aVar);
                    if (a3 != null) {
                        ShareCompat.IntentBuilder type = ShareCompat.IntentBuilder.from(a3).setType("text/plain");
                        String b2 = SupercellId.INSTANCE.getSharedServices$supercellId_release().j.b("account_invite_friends_share_title");
                        if (b2 == null) {
                            b2 = "";
                        }
                        type.setChooserTitle(b2).setText(str).startChooser();
                    }
                }
            }
        }
    }

    public final class i implements View.OnClickListener {
        public i() {
        }

        public final void onClick(View view) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Invite Friends", "click", "Invite in-game friends", null, false, 24);
            MainActivity a2 = b.b.a.b.a((Fragment) a.this);
            if (a2 != null) {
                a2.a(new c.b());
            }
        }
    }

    public final class j extends k implements kotlin.d.a.b<List<? extends String>, m> {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ WeakReference f401b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(WeakReference weakReference) {
            super(1);
            this.f401b = weakReference;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [b.b.a.i.n1.a, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.a.p.a(java.lang.Iterable, int):int
         arg types: [java.util.List<b.b.a.h.c>, int]
         candidates:
          kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
          kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
          kotlin.a.w.a(java.util.List, int):T
          kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
          kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
          kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
          kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
          kotlin.a.s.a(java.util.List, java.util.Comparator):void
          kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
          kotlin.a.p.a(java.lang.Iterable, int):int */
        public final void a(List<String> list) {
            Set<String> set;
            Collection collection;
            q e;
            b.b.a.j.m mVar;
            b.b.a.h.d dVar;
            List<b.b.a.h.c> list2;
            b.b.a.j.i c;
            b.b.a.h.a aVar;
            kotlin.d.b.j.b(list, "ingameFriends");
            a aVar2 = (a) this.f401b.get();
            if (aVar2 != null) {
                kotlin.d.b.j.a((Object) aVar2, "weakSelf.get() ?: return@get");
                if (aVar2.isAdded()) {
                    Set i = kotlin.a.m.i(list);
                    if (b.b.a.b.a((Fragment) a.this) == null || (c = SupercellId.INSTANCE.getSharedServices$supercellId_release().c()) == null || (aVar = c.f1179a) == null || (set = aVar.f107a) == null) {
                        set = ad.f5212a;
                    }
                    Set a2 = an.a(i, set);
                    if (b.b.a.b.a((Fragment) a.this) == null || (e = SupercellId.INSTANCE.getSharedServices$supercellId_release().e()) == null || (mVar = e.f1179a) == null || (dVar = (b.b.a.h.d) mVar.a()) == null || (list2 = dVar.f112a) == null) {
                        collection = (Set) ad.f5212a;
                    } else {
                        collection = new ArrayList(kotlin.a.m.a((Iterable) list2, 10));
                        for (b.b.a.h.c cVar : list2) {
                            collection.add(cVar.f110a);
                        }
                    }
                    int size = an.a(a2, collection).size();
                    TextView textView = (TextView) aVar2.a(R.id.invite_ingame_friends_notification);
                    if (textView != null) {
                        textView.setVisibility(size > 0 ? 0 : 8);
                        textView.setText(String.valueOf(size));
                    }
                }
            }
        }

        public final /* synthetic */ Object invoke(Object obj) {
            a((List) obj);
            return m.f5330a;
        }
    }

    public final View a(int i2) {
        if (this.q == null) {
            this.q = new HashMap();
        }
        View view = (View) this.q.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i2);
        this.q.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public final void a(i0 i0Var) {
        if (i0Var != null) {
            WeakReference weakReference = new WeakReference(this);
            o1<List<String>> o1Var = SupercellId.INSTANCE.getSharedServices$supercellId_release().d;
            if (o1Var == null) {
                kotlin.d.b.j.a("ingameFriendIds");
            }
            o1Var.a(i0Var.a().f120a, new j(weakReference));
        }
    }

    public final View b() {
        return (ImageButton) a(R.id.back_button);
    }

    public final void onAttach(Context context) {
        super.onAttach(context);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f().b(this.m);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().c().b(this.o);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().e().b(this.n);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kotlin.d.b.j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_invite_friends, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    public final void onDetach() {
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f().c(this.m);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().c().c(this.o);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().e().c(this.n);
        super.onDetach();
    }

    public final void onResume() {
        super.onResume();
        this.p = false;
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a("Invite Friends");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.Context, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.LinearLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        kotlin.d.b.j.b(view, "view");
        super.onViewCreated(view, bundle);
        b.b.a.b.a((SubPageTabLayout) a(R.id.tabBarView), "account_invite_friends_title", new kotlin.h[0]);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().e.gameLocalizedName(new e(new WeakReference(this)));
        ImageButton imageButton = (ImageButton) a(R.id.my_code_info_button);
        if (imageButton != null) {
            imageButton.setOnClickListener(new f());
        }
        Context context = view.getContext();
        kotlin.d.b.j.a((Object) context, "view.context");
        if (b.b.a.b.a(context)) {
            ((LinearLayout) a(R.id.scan_friends_code)).setOnClickListener(new g());
        } else {
            LinearLayout linearLayout = (LinearLayout) a(R.id.scan_friends_code);
            kotlin.d.b.j.a((Object) linearLayout, "scan_friends_code");
            linearLayout.setVisibility(8);
        }
        ((LinearLayout) a(R.id.share_invite)).setOnClickListener(new h());
        ((ConstraintLayout) a(R.id.invite_ingame_friends)).setOnClickListener(new i());
        this.m.invoke(SupercellId.INSTANCE.getSharedServices$supercellId_release().f().f1179a);
        this.n.invoke(SupercellId.INSTANCE.getSharedServices$supercellId_release().e().f1179a);
    }
}
