package b.b.a.i.o1;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import b.b.a.i.g;
import b.b.a.i.w;
import b.b.a.i.x;
import b.b.a.j.q0;
import com.facebook.places.model.PlaceFields;
import com.supercell.id.IdPendingLogin;
import com.supercell.id.SupercellId;
import java.util.HashMap;
import kotlin.d.b.j;
import kotlin.d.b.k;

public final class p extends w {
    public String m;
    public String n;
    public boolean o;
    public String p;
    public boolean q;
    public final kotlin.d.a.a<x>[] r = {b.f460a, c.f461a, d.f462a, e.f463a, f.f464a};
    public HashMap s;

    public static final class a extends w.a {
        public static final C0044a CREATOR = new C0044a(null);
        public final boolean e;
        public final Class<? extends g> f;
        public final IdPendingLogin g;
        public final String h;
        public final String i;

        /* renamed from: b.b.a.i.o1.p$a$a  reason: collision with other inner class name */
        public static final class C0044a implements Parcelable.Creator<a> {
            public /* synthetic */ C0044a(kotlin.d.b.g gVar) {
            }

            public final Object createFromParcel(Parcel parcel) {
                j.b(parcel, "parcel");
                j.b(parcel, "parcel");
                return new a((IdPendingLogin) parcel.readParcelable(IdPendingLogin.class.getClassLoader()), parcel.readString(), parcel.readString());
            }

            public final Object[] newArray(int i) {
                return new a[i];
            }
        }

        public a() {
            this(null, null, null, 7);
        }

        public a(IdPendingLogin idPendingLogin, String str, String str2) {
            this.g = idPendingLogin;
            this.h = str;
            this.i = str2;
            this.f = p.class;
        }

        public /* synthetic */ a(IdPendingLogin idPendingLogin, String str, String str2, int i2) {
            idPendingLogin = (i2 & 1) != 0 ? null : idPendingLogin;
            str = (i2 & 2) != 0 ? null : str;
            str2 = (i2 & 4) != 0 ? null : str2;
            this.g = idPendingLogin;
            this.h = str;
            this.i = str2;
            this.f = p.class;
        }

        public final g a(Context context) {
            j.b(context, "context");
            g a2 = super.a(context);
            Bundle arguments = a2.getArguments();
            if (arguments == null) {
                arguments = new Bundle();
            }
            IdPendingLogin idPendingLogin = this.g;
            if (idPendingLogin != null) {
                arguments.putString(NotificationCompat.CATEGORY_EMAIL, idPendingLogin.getEmail());
                arguments.putString(PlaceFields.PHONE, this.g.getPhone());
                arguments.putBoolean("remember", this.g.getRemember());
            }
            String str = this.h;
            if (str != null) {
                arguments.putString("preFilledEmail", str);
                arguments.putBoolean("remember", true);
            }
            String str2 = this.i;
            if (str2 != null) {
                arguments.putString("preFilledPhone", str2);
                arguments.putBoolean("remember", true);
            }
            a2.setArguments(arguments);
            return a2;
        }

        public final Class<? extends g> a() {
            return this.f;
        }

        public final g c(Context context) {
            j.b(context, "context");
            return w.c.n.a("log_in_progress_step_1", "log_in_progress_step_2", "log_in_progress_step_3", true);
        }

        public final boolean d(Resources resources) {
            j.b(resources, "resources");
            return true;
        }

        public final int describeContents() {
            return 0;
        }

        public final boolean e() {
            return this.e;
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return j.a(this.g, aVar.g) && j.a(this.h, aVar.h) && j.a(this.i, aVar.i);
        }

        public final int hashCode() {
            IdPendingLogin idPendingLogin = this.g;
            int i2 = 0;
            int hashCode = (idPendingLogin != null ? idPendingLogin.hashCode() : 0) * 31;
            String str = this.h;
            int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.i;
            if (str2 != null) {
                i2 = str2.hashCode();
            }
            return hashCode2 + i2;
        }

        public final String toString() {
            StringBuilder a2 = b.a.a.a.a.a("BackStackEntry(pendingLogin=");
            a2.append(this.g);
            a2.append(", preFillEmail=");
            a2.append(this.h);
            a2.append(", preFillPhone=");
            return b.a.a.a.a.a(a2, this.i, ")");
        }

        public final void writeToParcel(Parcel parcel, int i2) {
            if (parcel != null) {
                parcel.writeParcelable(this.g, i2);
            }
            if (parcel != null) {
                parcel.writeString(this.h);
            }
            if (parcel != null) {
                parcel.writeString(this.i);
            }
        }
    }

    public final class b extends k implements kotlin.d.a.a<s> {

        /* renamed from: a  reason: collision with root package name */
        public static final b f460a = new b();

        public b() {
            super(0);
        }

        public final Object invoke() {
            return new s();
        }
    }

    public final class c extends k implements kotlin.d.a.a<x> {

        /* renamed from: a  reason: collision with root package name */
        public static final c f461a = new c();

        public c() {
            super(0);
        }

        public final Object invoke() {
            return SupercellId.INSTANCE.getRemoteConfiguration$supercellId_release().a(q0.SMS_ENABLED) ? new d() : new h();
        }
    }

    public final class d extends k implements kotlin.d.a.a<o> {

        /* renamed from: a  reason: collision with root package name */
        public static final d f462a = new d();

        public d() {
            super(0);
        }

        public final Object invoke() {
            return new o();
        }
    }

    public final class e extends k implements kotlin.d.a.a<a> {

        /* renamed from: a  reason: collision with root package name */
        public static final e f463a = new e();

        public e() {
            super(0);
        }

        public final Object invoke() {
            return new a();
        }
    }

    public final class f extends k implements kotlin.d.a.a<r> {

        /* renamed from: a  reason: collision with root package name */
        public static final f f464a = new f();

        public f() {
            super(0);
        }

        public final Object invoke() {
            return new r();
        }
    }

    public final View a(int i) {
        if (this.s == null) {
            this.s = new HashMap();
        }
        View view = (View) this.s.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.s.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public final kotlin.d.a.a<x>[] i() {
        return this.r;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.view.FlowPager, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0040, code lost:
        if ((r4 == null || r4.length() == 0) == false) goto L_0x0042;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onActivityCreated(android.os.Bundle r7) {
        /*
            r6 = this;
            super.onActivityCreated(r7)
            java.lang.String r0 = "flowPager"
            r1 = 0
            r2 = 0
            r3 = 1
            if (r7 != 0) goto L_0x0051
            android.os.Bundle r4 = r6.getArguments()
            if (r4 == 0) goto L_0x0017
            java.lang.String r5 = "email"
            java.lang.String r4 = r4.getString(r5)
            goto L_0x0018
        L_0x0017:
            r4 = r2
        L_0x0018:
            if (r4 == 0) goto L_0x0023
            int r4 = r4.length()
            if (r4 != 0) goto L_0x0021
            goto L_0x0023
        L_0x0021:
            r4 = 0
            goto L_0x0024
        L_0x0023:
            r4 = 1
        L_0x0024:
            if (r4 == 0) goto L_0x0042
            android.os.Bundle r4 = r6.getArguments()
            if (r4 == 0) goto L_0x0033
            java.lang.String r5 = "phone"
            java.lang.String r4 = r4.getString(r5)
            goto L_0x0034
        L_0x0033:
            r4 = r2
        L_0x0034:
            if (r4 == 0) goto L_0x003f
            int r4 = r4.length()
            if (r4 != 0) goto L_0x003d
            goto L_0x003f
        L_0x003d:
            r4 = 0
            goto L_0x0040
        L_0x003f:
            r4 = 1
        L_0x0040:
            if (r4 != 0) goto L_0x0051
        L_0x0042:
            int r4 = com.supercell.id.R.id.flowPager
            android.view.View r4 = r6.a(r4)
            android.support.v4.view.FlowPager r4 = (android.support.v4.view.FlowPager) r4
            kotlin.d.b.j.a(r4, r0)
            r5 = 2
            r4.setCurrentItem(r5)
        L_0x0051:
            if (r7 != 0) goto L_0x0094
            android.os.Bundle r7 = r6.getArguments()
            if (r7 == 0) goto L_0x0060
            java.lang.String r4 = "preFilledEmail"
            java.lang.String r7 = r7.getString(r4)
            goto L_0x0061
        L_0x0060:
            r7 = r2
        L_0x0061:
            if (r7 == 0) goto L_0x006c
            int r7 = r7.length()
            if (r7 != 0) goto L_0x006a
            goto L_0x006c
        L_0x006a:
            r7 = 0
            goto L_0x006d
        L_0x006c:
            r7 = 1
        L_0x006d:
            if (r7 == 0) goto L_0x0086
            android.os.Bundle r7 = r6.getArguments()
            if (r7 == 0) goto L_0x007b
            java.lang.String r2 = "preFilledPhone"
            java.lang.String r2 = r7.getString(r2)
        L_0x007b:
            if (r2 == 0) goto L_0x0083
            int r7 = r2.length()
            if (r7 != 0) goto L_0x0084
        L_0x0083:
            r1 = 1
        L_0x0084:
            if (r1 != 0) goto L_0x0094
        L_0x0086:
            int r7 = com.supercell.id.R.id.flowPager
            android.view.View r7 = r6.a(r7)
            android.support.v4.view.FlowPager r7 = (android.support.v4.view.FlowPager) r7
            kotlin.d.b.j.a(r7, r0)
            r7.setCurrentItem(r3)
        L_0x0094:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.i.o1.p.onActivityCreated(android.os.Bundle):void");
    }

    public final void onCreate(Bundle bundle) {
        String str;
        String str2;
        super.onCreate(bundle);
        Bundle arguments = bundle != null ? bundle : getArguments();
        String str3 = null;
        if (arguments == null || (str = arguments.getString(NotificationCompat.CATEGORY_EMAIL)) == null) {
            Bundle arguments2 = getArguments();
            str = arguments2 != null ? arguments2.getString("preFilledEmail") : null;
        }
        this.m = str;
        Bundle arguments3 = bundle != null ? bundle : getArguments();
        if (arguments3 == null || (str2 = arguments3.getString(PlaceFields.PHONE)) == null) {
            Bundle arguments4 = getArguments();
            str2 = arguments4 != null ? arguments4.getString("preFilledPhone") : null;
        }
        this.n = str2;
        Bundle arguments5 = bundle != null ? bundle : getArguments();
        boolean z = false;
        this.o = arguments5 != null ? arguments5.getBoolean("remember") : false;
        if (bundle != null) {
            str3 = bundle.getString("pin");
        }
        this.p = str3;
        if (bundle != null) {
            z = bundle.getBoolean("bound");
        }
        this.q = z;
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    public final void onSaveInstanceState(Bundle bundle) {
        j.b(bundle, "outState");
        String str = this.m;
        if (str != null) {
            bundle.putString(NotificationCompat.CATEGORY_EMAIL, str);
        }
        String str2 = this.n;
        if (str2 != null) {
            bundle.putString(PlaceFields.PHONE, str2);
        }
        bundle.putBoolean("remember", this.o);
        String str3 = this.p;
        if (str3 != null) {
            bundle.putString("pin", str3);
        }
        bundle.putBoolean("bound", this.q);
        super.onSaveInstanceState(bundle);
    }
}
