package b.b.a.i;

import android.animation.ValueAnimator;
import nl.komponents.kovenant.ap;

public final class v0 implements ValueAnimator.AnimatorUpdateListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ ValueAnimator f739a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ float f740b;
    public final /* synthetic */ float c;
    public final /* synthetic */ x0 d;
    public final /* synthetic */ boolean e;

    public v0(ValueAnimator valueAnimator, float f, float f2, x0 x0Var, boolean z, ap apVar) {
        this.f739a = valueAnimator;
        this.f740b = f;
        this.c = f2;
        this.d = x0Var;
        this.e = z;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        x0 x0Var = this.d;
        float f = this.f740b;
        float animatedFraction = ((this.c - f) * this.f739a.getAnimatedFraction() * 2.0f) + f;
        float f2 = 0.0f;
        if (Float.compare(animatedFraction, 0.0f) >= 0) {
            f2 = Float.compare(animatedFraction, 1.0f) > 0 ? 1.0f : animatedFraction;
        }
        x0Var.a(f2, this.e);
    }
}
