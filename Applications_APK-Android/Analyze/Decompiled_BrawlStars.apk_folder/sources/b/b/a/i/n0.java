package b.b.a.i;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.a;

public final class n0 implements Animator.AnimatorListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ a f391a;

    public n0(a aVar, ObjectAnimator objectAnimator, ObjectAnimator objectAnimator2) {
        this.f391a = aVar;
    }

    public final void onAnimationCancel(Animator animator) {
    }

    public final void onAnimationEnd(Animator animator) {
        SupercellId.INSTANCE.dismiss$supercellId_release(this.f391a.f4885a);
    }

    public final void onAnimationRepeat(Animator animator) {
    }

    public final void onAnimationStart(Animator animator) {
    }
}
