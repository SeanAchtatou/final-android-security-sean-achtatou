package b.b.a.i.h1;

import android.view.View;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.view.ExpandableFrameLayout;
import kotlin.d.b.j;

public final class b implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ View f284a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ String f285b;

    public b(View view, String str) {
        this.f284a = view;
        this.f285b = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onClick(View view) {
        View view2 = this.f284a;
        j.a((Object) view2, "itemRow");
        ((ExpandableFrameLayout) view2.findViewById(R.id.descriptionContainer)).a(!((ExpandableFrameLayout) view2.findViewById(R.id.descriptionContainer)).a(), true);
        b.b.a.c.b bVar = SupercellId.INSTANCE.getSharedServices$supercellId_release().f;
        String str = this.f285b;
        View view3 = this.f284a;
        j.a((Object) view3, "itemRow");
        b.b.a.c.b.a(bVar, "FAQ", "click", str, Long.valueOf(((ExpandableFrameLayout) view3.findViewById(R.id.descriptionContainer)).a() ? 1 : 0), false, 16);
    }
}
