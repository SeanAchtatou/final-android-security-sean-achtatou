package b.b.a.i.u1;

import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import b.b.a.i.b;
import b.b.a.i.g;
import b.b.a.j.g1;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.g;
import com.google.i18n.phonenumbers.j;
import com.supercell.id.IdAccount;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.view.EdgeAntialiasingImageView;
import com.supercell.id.view.ExpandableFrameLayout;
import com.supercell.id.view.WidthAdjustingMultilineButton;
import java.util.HashMap;
import kotlin.d.b.j;

public final class b extends g {
    public HashMap m;

    public static final class a extends b.a {
        public static final C0070a CREATOR = new C0070a(null);
        public final boolean e;
        public final Class<? extends g> f = b.class;

        /* renamed from: b.b.a.i.u1.b$a$a  reason: collision with other inner class name */
        public static final class C0070a implements Parcelable.Creator<a> {
            public /* synthetic */ C0070a(kotlin.d.b.g gVar) {
            }

            public final Object createFromParcel(Parcel parcel) {
                j.b(parcel, "parcel");
                return new a();
            }

            public final Object[] newArray(int i) {
                return new a[i];
            }
        }

        public final Class<? extends g> a() {
            return this.f;
        }

        public final int b(Resources resources, int i, int i2, int i3) {
            j.b(resources, "resources");
            return (i * 1) / 3;
        }

        public final boolean d(Resources resources) {
            j.b(resources, "resources");
            return true;
        }

        public final int describeContents() {
            return 0;
        }

        public final Class<? extends g> e(Resources resources) {
            j.b(resources, "resources");
            return C0071b.class;
        }

        public final boolean e() {
            return this.e;
        }

        public final void writeToParcel(Parcel parcel, int i) {
        }
    }

    /* renamed from: b.b.a.i.u1.b$b  reason: collision with other inner class name */
    public static final class C0071b extends g {
        public HashMap m;

        public final void a() {
            HashMap hashMap = this.m;
            if (hashMap != null) {
                hashMap.clear();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
            j.b(layoutInflater, "inflater");
            return layoutInflater.inflate(R.layout.fragment_profile_selector_top_area, viewGroup, false);
        }

        public final /* synthetic */ void onDestroyView() {
            super.onDestroyView();
            a();
        }
    }

    public final View a(int i) {
        if (this.m == null) {
            this.m = new HashMap();
        }
        View view = (View) this.m.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.m.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_profile_selector, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    public final void onResume() {
        super.onResume();
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a("Saved Credentials");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.EdgeAntialiasingImageView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.i.x1.j.a(android.widget.ImageView, java.lang.String, boolean, int):void
     arg types: [com.supercell.id.view.EdgeAntialiasingImageView, java.lang.String, int, int]
     candidates:
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, java.lang.Object, int):void
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, kotlin.d.a.b, int):void
      b.b.a.i.x1.j.a(android.widget.ImageView, java.lang.String, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.LinearLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.supercell.id.view.ExpandableFrameLayout.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.supercell.id.view.ExpandableFrameLayout.a(com.supercell.id.view.ExpandableFrameLayout, float):void
      com.supercell.id.view.ExpandableFrameLayout.a(boolean, boolean):void */
    public final void onViewCreated(View view, Bundle bundle) {
        j.b(view, "view");
        super.onViewCreated(view, bundle);
        View a2 = a(R.id.saved_logins);
        j.b("switch_heading", "titleKey");
        j.b("tab_icon_id.png", "leftIconKey");
        j.b("tab_icon_gear.png", "rightIconKey");
        if (a2 != null) {
            a2.setBackgroundResource(R.drawable.tab_button_single);
            ((RelativeLayout) a2.findViewById(R.id.tab_icon)).setBackgroundResource(R.drawable.tab_icon_shadows);
            EdgeAntialiasingImageView edgeAntialiasingImageView = (EdgeAntialiasingImageView) a2.findViewById(R.id.tab_icon_left);
            j.a((Object) edgeAntialiasingImageView, "tab_icon_left");
            b.b.a.i.x1.j.a((ImageView) edgeAntialiasingImageView, "tab_icon_id.png", false, 2);
            EdgeAntialiasingImageView edgeAntialiasingImageView2 = (EdgeAntialiasingImageView) a2.findViewById(R.id.tab_icon_right);
            j.a((Object) edgeAntialiasingImageView2, "tab_icon_right");
            b.b.a.i.x1.j.a((ImageView) edgeAntialiasingImageView2, "tab_icon_gear.png", false, 2);
            TextView textView = (TextView) a2.findViewById(R.id.tab_title);
            j.a((Object) textView, "tab_title");
            b.b.a.i.x1.j.a(textView, "switch_heading", (kotlin.d.a.b) null, 2);
            a2.setOnTouchListener(new g1(a2));
        }
        for (IdAccount idAccount : SupercellId.INSTANCE.getAccounts()) {
            View inflate = LayoutInflater.from(getContext()).inflate(R.layout.fragment_profile_selector_account_row_view, (ViewGroup) ((LinearLayout) a(R.id.profileList)), false);
            if (!(inflate instanceof ExpandableFrameLayout)) {
                inflate = null;
            }
            ExpandableFrameLayout expandableFrameLayout = (ExpandableFrameLayout) inflate;
            if (expandableFrameLayout != null) {
                TextView textView2 = (TextView) expandableFrameLayout.a(R.id.accountEmailView);
                j.a((Object) textView2, "rowView.accountEmailView");
                String email = idAccount.getEmail();
                if (email == null) {
                    email = idAccount.getPhone();
                    if (email != null) {
                        j.b(email, "number");
                        try {
                            j.a a3 = com.google.i18n.phonenumbers.g.a().a(email, "ZZ");
                            email = 8234 + com.google.i18n.phonenumbers.g.a().a(a3, g.a.INTERNATIONAL) + 8236;
                        } catch (NumberParseException unused) {
                        }
                    } else {
                        email = null;
                    }
                }
                textView2.setText(email);
                TextView textView3 = (TextView) expandableFrameLayout.a(R.id.accountPlayerIdView);
                String playerId = idAccount.getPlayerId();
                boolean z = !(playerId == null || playerId.length() == 0);
                String playerId2 = idAccount.getPlayerId();
                if (playerId2 == null || !z) {
                    playerId2 = null;
                }
                textView3.setText(playerId2);
                textView3.setVisibility(z ? 0 : 8);
                expandableFrameLayout.a(true, false);
                ((LinearLayout) a(R.id.profileList)).addView(expandableFrameLayout);
                ((FrameLayout) expandableFrameLayout.a(R.id.profileAccountView)).setOnClickListener(new c(idAccount, this));
                expandableFrameLayout.setTag(R.id.profileAccountView, idAccount);
                ((ImageView) expandableFrameLayout.a(R.id.deleteAccountButton)).setOnClickListener(new d(idAccount, this));
            }
        }
        ((WidthAdjustingMultilineButton) a(R.id.loginWithAnotherIdButton)).setOnClickListener(new e(this));
    }
}
