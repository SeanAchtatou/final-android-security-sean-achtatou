package b.b.a.i.m1;

import android.graphics.drawable.BitmapDrawable;
import android.widget.TextView;
import b.a.a.a.a;
import com.supercell.id.SupercellId;
import java.lang.ref.WeakReference;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class c extends k implements b<String, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f388a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ String f389b;
    public final /* synthetic */ String c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c(WeakReference weakReference, String str, String str2) {
        super(1);
        this.f388a = weakReference;
        this.f389b = str;
        this.c = str2;
    }

    public final Object invoke(Object obj) {
        String str = (String) obj;
        j.b(str, "localizedGame");
        TextView textView = (TextView) this.f388a.get();
        if (textView != null) {
            b.b.a.b.a(textView, this.f389b, str, (BitmapDrawable) null);
        }
        SupercellId.INSTANCE.getSharedServices$supercellId_release().j.a(a.a(a.a("AppIcon_"), this.c, ".png"), new b(this, str));
        return m.f5330a;
    }
}
