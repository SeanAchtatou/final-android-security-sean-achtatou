package b.b.a.i.z1;

import b.b.a.h.m;
import java.lang.ref.WeakReference;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.d.b.k;

public final class i extends k implements b<m, kotlin.m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f999a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public i(WeakReference weakReference) {
        super(1);
        this.f999a = weakReference;
    }

    public final Object invoke(Object obj) {
        m mVar = (m) obj;
        j.b(mVar, "response");
        l lVar = (l) this.f999a.get();
        if (lVar != null) {
            lVar.f1002b = mVar;
        }
        return kotlin.m.f5330a;
    }
}
