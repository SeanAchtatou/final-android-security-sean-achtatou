package b.b.a.i.m1;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import b.b.a.e.a;
import b.b.a.i.x1.f;
import b.b.a.j.p;
import b.b.a.j.q1;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.view.WidthAdjustingMultilineButton;
import java.lang.ref.WeakReference;
import java.util.Date;
import java.util.Locale;
import kotlin.d.b.j;

public final class a extends b.b.a.i.d {

    /* renamed from: a  reason: collision with root package name */
    public final WeakReference<Activity> f377a;

    /* renamed from: b  reason: collision with root package name */
    public final long f378b;
    public final String c;
    public final String d;

    /* renamed from: b.b.a.i.m1.a$a  reason: collision with other inner class name */
    public final class C0035a extends AnimatorListenerAdapter {
        public C0035a() {
        }

        public final void onAnimationEnd(Animator animator) {
            a.this.dismiss();
        }
    }

    public final class b implements View.OnClickListener {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ View f381b;
        public final /* synthetic */ Activity c;

        public b(View view, Activity activity) {
            this.f381b = view;
            this.c = activity;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Invite To Play Notification", "click", "Mute Invites", null, false, 24);
            a aVar = a.this;
            View view2 = this.f381b;
            j.a((Object) view2, "view");
            aVar.a(view2);
            new b.b.a.i.l1.a(this.c).show();
        }
    }

    public final class c implements View.OnClickListener {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ View f383b;

        public c(View view) {
            this.f383b = view;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Invite To Play Notification", "click", "Dismiss", null, false, 24);
            a aVar = a.this;
            View view2 = this.f383b;
            j.a((Object) view2, "view");
            aVar.a(view2);
        }
    }

    public final class d implements View.OnClickListener {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ View f385b;

        public d(View view) {
            this.f385b = view;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Invite To Play Notification", "click", "Play", null, false, 24);
            a aVar = a.this;
            View view2 = this.f385b;
            j.a((Object) view2, "view");
            aVar.a(view2);
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a(Activity activity, long j, String str, String str2) {
        super(activity, R.style.SupercellIdNonModalDialog);
        j.b(activity, "activity");
        j.b(str2, "game");
        this.f378b = j;
        this.c = str;
        this.d = str2;
        this.f377a = new WeakReference<>(activity);
    }

    public final void a(View view) {
        j.b(view, "view");
        view.animate().setDuration(150).setInterpolator(b.b.a.f.a.c).alpha(0.0f).setListener(new C0035a()).start();
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.app.Activity, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.constraint.ConstraintLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.FrameLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.ImageView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.i.x1.j.a(android.widget.ImageView, java.lang.String, boolean, int):void
     arg types: [android.widget.ImageView, java.lang.String, int, int]
     candidates:
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, java.lang.Object, int):void
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, kotlin.d.a.b, int):void
      b.b.a.i.x1.j.a(android.widget.ImageView, java.lang.String, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, kotlin.d.a.b, int):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String, ?[OBJECT, ARRAY], int]
     candidates:
      b.b.a.i.x1.j.a(android.widget.ImageView, java.lang.String, boolean, int):void
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, java.lang.Object, int):void
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, kotlin.d.a.b, int):void */
    public final void onCreate(Bundle bundle) {
        ContextThemeWrapper contextThemeWrapper;
        super.onCreate(bundle);
        Activity activity = this.f377a.get();
        if (activity != null) {
            j.a((Object) activity, "weakActivity.get() ?: return");
            requestWindowFeature(1);
            Window window = getWindow();
            if (window != null) {
                window.setGravity(49);
                window.addFlags(32);
                if (Build.VERSION.SDK_INT >= 21) {
                    window.addFlags(256);
                    window.clearFlags(67108864);
                }
                if (b.b.a.b.b(activity)) {
                    window.addFlags(1056);
                }
            }
            Locale locale = SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getLocale();
            Resources resources = activity.getResources();
            j.a((Object) resources, "activity.resources");
            Configuration configuration = new Configuration(resources.getConfiguration());
            if (Build.VERSION.SDK_INT >= 17) {
                configuration.setLocale(locale);
                contextThemeWrapper = activity.createConfigurationContext(configuration);
            } else {
                configuration.locale = locale;
                ContextThemeWrapper contextThemeWrapper2 = new ContextThemeWrapper(activity, R.style.SupercellIdTheme);
                Resources resources2 = contextThemeWrapper2.getResources();
                Resources resources3 = activity.getResources();
                j.a((Object) resources3, "activity.resources");
                resources2.updateConfiguration(configuration, resources3.getDisplayMetrics());
                contextThemeWrapper = contextThemeWrapper2;
            }
            View inflate = LayoutInflater.from(contextThemeWrapper).inflate(R.layout.dialog_notification, (ViewGroup) null, false);
            setContentView(inflate);
            Window window2 = getWindow();
            if (window2 != null) {
                window2.setLayout(-1, -2);
            }
            inflate.setAlpha(0.0f);
            inflate.animate().setDuration(300).setInterpolator(b.b.a.f.a.c).alpha(1.0f).start();
            ConstraintLayout constraintLayout = (ConstraintLayout) findViewById(R.id.dialogContainer);
            j.a((Object) constraintLayout, "dialogContainer");
            q1.a(constraintLayout, 61, true, 0);
            FrameLayout frameLayout = (FrameLayout) findViewById(R.id.statusBarBackground);
            j.a((Object) frameLayout, "statusBarBackground");
            q1.a(frameLayout, 1, true, 0);
            ImageView imageView = (ImageView) findViewById(R.id.logoImageView);
            j.a((Object) imageView, "logoImageView");
            b.b.a.i.x1.j.a(imageView, "AccountIcon.png", false, 2);
            TextView textView = (TextView) findViewById(R.id.messageTextView);
            j.a((Object) textView, "messageTextView");
            p.a(textView, null, 1);
            TextView textView2 = (TextView) findViewById(R.id.messageTextView);
            j.a((Object) textView2, "messageTextView");
            String str = this.c;
            String str2 = this.d;
            WeakReference weakReference = new WeakReference(textView2);
            f fVar = SupercellId.INSTANCE.getSharedServices$supercellId_release().j;
            fVar.a("game_name_" + str2, new c(weakReference, str, str2));
            TextView textView3 = (TextView) findViewById(R.id.timestampTextView);
            j.a((Object) textView3, "timestampTextView");
            p.a(textView3, null, 1);
            TextView textView4 = (TextView) findViewById(R.id.timestampTextView);
            j.a((Object) textView4, "timestampTextView");
            b.b.a.i.x1.j.a(textView4, new Date(this.f378b));
            WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) findViewById(R.id.muteButton);
            j.a((Object) widthAdjustingMultilineButton, "muteButton");
            p.a(widthAdjustingMultilineButton, null, 1);
            WidthAdjustingMultilineButton widthAdjustingMultilineButton2 = (WidthAdjustingMultilineButton) findViewById(R.id.muteButton);
            j.a((Object) widthAdjustingMultilineButton2, "muteButton");
            b.b.a.i.x1.j.a((TextView) widthAdjustingMultilineButton2, "ingame_notification_mute_invites_btn", (kotlin.d.a.b) null, 2);
            WidthAdjustingMultilineButton widthAdjustingMultilineButton3 = (WidthAdjustingMultilineButton) findViewById(R.id.muteButton);
            j.a((Object) widthAdjustingMultilineButton3, "muteButton");
            q1.a(widthAdjustingMultilineButton3, 0, a.C0004a.BUTTON_01.ordinal());
            ((WidthAdjustingMultilineButton) findViewById(R.id.muteButton)).setOnClickListener(new b(inflate, activity));
            WidthAdjustingMultilineButton widthAdjustingMultilineButton4 = (WidthAdjustingMultilineButton) findViewById(R.id.cancelButton);
            j.a((Object) widthAdjustingMultilineButton4, "cancelButton");
            p.a(widthAdjustingMultilineButton4, "fonts/SupercellTextAndroid_ACorp_Bd.ttf");
            WidthAdjustingMultilineButton widthAdjustingMultilineButton5 = (WidthAdjustingMultilineButton) findViewById(R.id.cancelButton);
            j.a((Object) widthAdjustingMultilineButton5, "cancelButton");
            b.b.a.i.x1.j.a((TextView) widthAdjustingMultilineButton5, "ingame_notification_not_now_btn", (kotlin.d.a.b) null, 2);
            WidthAdjustingMultilineButton widthAdjustingMultilineButton6 = (WidthAdjustingMultilineButton) findViewById(R.id.cancelButton);
            j.a((Object) widthAdjustingMultilineButton6, "cancelButton");
            q1.a(widthAdjustingMultilineButton6, 0, a.C0004a.BUTTON_01.ordinal());
            ((WidthAdjustingMultilineButton) findViewById(R.id.cancelButton)).setOnClickListener(new c(inflate));
            WidthAdjustingMultilineButton widthAdjustingMultilineButton7 = (WidthAdjustingMultilineButton) findViewById(R.id.okButton);
            j.a((Object) widthAdjustingMultilineButton7, "okButton");
            p.a(widthAdjustingMultilineButton7, "fonts/SupercellTextAndroid_ACorp_Bd.ttf");
            WidthAdjustingMultilineButton widthAdjustingMultilineButton8 = (WidthAdjustingMultilineButton) findViewById(R.id.okButton);
            j.a((Object) widthAdjustingMultilineButton8, "okButton");
            b.b.a.i.x1.j.a((TextView) widthAdjustingMultilineButton8, "ingame_notification_play_btn", (kotlin.d.a.b) null, 2);
            WidthAdjustingMultilineButton widthAdjustingMultilineButton9 = (WidthAdjustingMultilineButton) findViewById(R.id.okButton);
            j.a((Object) widthAdjustingMultilineButton9, "okButton");
            q1.a(widthAdjustingMultilineButton9, 0, a.C0004a.BUTTON_01.ordinal());
            ((WidthAdjustingMultilineButton) findViewById(R.id.okButton)).setOnClickListener(new d(inflate));
        }
    }

    public final void onStart() {
        super.onStart();
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a("Invite To Play Notification");
    }
}
