package b.b.a.i.j1;

import b.b.a.j.k;
import b.b.a.j.r0;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.a.ae;
import kotlin.a.m;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    public static final k f342a = new k(0, 1);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
     arg types: [java.util.ArrayList, java.util.List]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean */
    public static final List<r0> a(List<b> list, boolean z) {
        List list2;
        List a2 = m.a(new a(list.size(), z));
        Iterable<ae> j = m.j(list);
        ArrayList arrayList = new ArrayList();
        for (ae aeVar : j) {
            if (aeVar.f5213a == 0) {
                list2 = m.a((Object) aeVar.f5214b);
            } else {
                list2 = m.a((Object[]) new r0[]{f342a, (r0) aeVar.f5214b});
            }
            m.a((Collection) arrayList, (Iterable) list2);
        }
        return m.d(a2, arrayList);
    }
}
