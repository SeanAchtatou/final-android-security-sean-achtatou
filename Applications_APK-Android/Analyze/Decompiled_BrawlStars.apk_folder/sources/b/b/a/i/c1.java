package b.b.a.i;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.animation.SpringAnimation;
import android.support.animation.SpringForce;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import b.b.a.j.e0;
import com.supercell.id.R;
import com.supercell.id.view.WidthAdjustingMultilineTextView;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import kotlin.d.b.g;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;
import nl.komponents.kovenant.bw;

public class c1 extends e {
    public static final a h = new a(null);
    public final long e = 70;
    public final boolean f = true;
    public HashMap g;

    public static final class a {
        public /* synthetic */ a(g gVar) {
        }

        public final c1 a(String str, String str2) {
            j.b(str, "name");
            j.b(str2, "qrCodeUrl");
            c1 c1Var = new c1();
            Bundle arguments = c1Var.getArguments();
            if (arguments == null) {
                arguments = new Bundle();
            }
            arguments.putString("name", str);
            arguments.putString("qrCodeUrl", str2);
            c1Var.setArguments(arguments);
            return c1Var;
        }
    }

    public final class b extends k implements kotlin.d.a.b<Bitmap, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f200a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(WeakReference weakReference) {
            super(1);
            this.f200a = weakReference;
        }

        public final Object invoke(Object obj) {
            Object obj2 = this.f200a.get();
            if (obj2 != null) {
                Bitmap bitmap = (Bitmap) obj;
                ImageView imageView = (ImageView) ((c1) obj2).a(R.id.qr_code_view);
                if (imageView != null) {
                    imageView.setImageBitmap(bitmap);
                }
            }
            return m.f5330a;
        }
    }

    public final class c implements View.OnClickListener {
        public c() {
        }

        public final void onClick(View view) {
            c1.this.b();
        }
    }

    public View a(int i) {
        if (this.g == null) {
            this.g = new HashMap();
        }
        View view = (View) this.g.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.g.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public void a() {
        HashMap hashMap = this.g;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public void b() {
        ViewPropertyAnimator animate;
        super.b();
        FrameLayout frameLayout = (FrameLayout) a(R.id.dialogContainer);
        if (frameLayout != null && (animate = frameLayout.animate()) != null) {
            animate.setDuration(c());
            animate.setInterpolator(b.b.a.f.a.c);
            animate.scaleX(0.7f);
            animate.scaleY(0.7f);
            animate.start();
        }
    }

    public long c() {
        return this.e;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_qr_code_dialog, viewGroup, false);
    }

    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.FrameLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.animation.SpringForce, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public void onViewCreated(View view, Bundle bundle) {
        bw<Bitmap, Exception> a2;
        j.b(view, "view");
        super.onViewCreated(view, bundle);
        if (this.f) {
            view.setOnClickListener(new c());
        }
        Bundle arguments = getArguments();
        String str = null;
        String string = arguments != null ? arguments.getString("qrCodeUrl") : null;
        Bundle arguments2 = getArguments();
        if (arguments2 != null) {
            str = arguments2.getString("name");
        }
        WidthAdjustingMultilineTextView widthAdjustingMultilineTextView = (WidthAdjustingMultilineTextView) a(R.id.qr_code_title);
        if (widthAdjustingMultilineTextView != null) {
            b.b.a.b.a(widthAdjustingMultilineTextView, "AccountIcon.png", new Rect(0, 0, kotlin.e.a.a(b.b.a.b.a(25)), kotlin.e.a.a(b.b.a.b.a(23))));
        }
        WidthAdjustingMultilineTextView widthAdjustingMultilineTextView2 = (WidthAdjustingMultilineTextView) a(R.id.qr_code_title);
        if (widthAdjustingMultilineTextView2 != null) {
            widthAdjustingMultilineTextView2.setText(str);
        }
        if (!(string == null || (a2 = e0.f1030b.a(string)) == null)) {
            nl.komponents.kovenant.c.m.a(a2, new b(new WeakReference(this)));
        }
        FrameLayout frameLayout = (FrameLayout) a(R.id.dialogContainer);
        j.a((Object) frameLayout, "it");
        frameLayout.setScaleX(0.8f);
        frameLayout.setScaleY(0.8f);
        SpringAnimation springAnimation = new SpringAnimation(frameLayout, SpringAnimation.SCALE_X, 1.0f);
        SpringForce spring = springAnimation.getSpring();
        j.a((Object) spring, "spring");
        spring.setDampingRatio(0.3f);
        SpringForce spring2 = springAnimation.getSpring();
        j.a((Object) spring2, "spring");
        spring2.setStiffness(400.0f);
        springAnimation.start();
        SpringAnimation springAnimation2 = new SpringAnimation(frameLayout, SpringAnimation.SCALE_Y, 1.0f);
        SpringForce spring3 = springAnimation2.getSpring();
        j.a((Object) spring3, "spring");
        spring3.setDampingRatio(0.3f);
        SpringForce spring4 = springAnimation2.getSpring();
        j.a((Object) spring4, "spring");
        spring4.setStiffness(400.0f);
        springAnimation2.start();
    }
}
