package b.b.a.i.v1;

import b.b.a.h.i;
import b.b.a.i.v1.h;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.d.b.k;

public final class s extends k implements b<i, h.b> {

    /* renamed from: a  reason: collision with root package name */
    public static final s f813a = new s();

    public s() {
        super(1);
    }

    public final Object invoke(Object obj) {
        i iVar = (i) obj;
        j.b(iVar, "it");
        return new h.b(iVar.f122a, iVar.f123b, iVar.c, iVar.f, iVar.a(), iVar.h, iVar.i, false, iVar.c(), iVar.b());
    }
}
