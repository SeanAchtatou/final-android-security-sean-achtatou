package b.b.a.i;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.supercell.id.R;
import com.supercell.id.view.WidthAdjustingMultilineButton;
import java.util.ArrayList;
import java.util.HashMap;
import kotlin.d.b.g;
import kotlin.d.b.j;
import kotlin.h;
import kotlin.m;

public final class r extends c {
    public static final a g = new a(null);
    public kotlin.d.a.b<? super r, m> e;
    public HashMap f;

    public final class b implements View.OnClickListener {
        public b() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) r.this.a(R.id.okButton);
            j.a((Object) widthAdjustingMultilineButton, "okButton");
            widthAdjustingMultilineButton.setEnabled(false);
            r rVar = r.this;
            kotlin.d.a.b<? super r, m> bVar = rVar.e;
            if (bVar != null) {
                bVar.invoke(rVar);
            }
            r.this.b();
        }
    }

    public final View a(int i) {
        if (this.f == null) {
            this.f = new HashMap();
        }
        View view = (View) this.f.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.f.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.f;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        j.b(view, "view");
        super.onViewCreated(view, bundle);
        TextView textView = (TextView) a(R.id.dialogTextTextView);
        j.a((Object) textView, "dialogTextTextView");
        textView.setVisibility(8);
        ((WidthAdjustingMultilineButton) a(R.id.okButton)).setOnClickListener(new b());
    }

    public static final class a {
        public /* synthetic */ a(g gVar) {
        }

        public static /* synthetic */ r a(a aVar, String str, String str2, String str3, h hVar, boolean z, int i) {
            if ((i & 8) != 0) {
                hVar = null;
            }
            return aVar.a(str, str2, str3, hVar, (i & 16) != 0 ? false : z);
        }

        public final r a(String str, String str2, String str3, h<String, String> hVar, boolean z) {
            ArrayList arrayList;
            j.b(str, "titleKey");
            j.b(str2, "okButtonKey");
            j.b(str3, "cancelButtonKey");
            r rVar = new r();
            Bundle arguments = rVar.getArguments();
            if (arguments == null) {
                arguments = new Bundle();
            }
            arguments.putString("titleKey", str);
            arguments.putString("okButtonKey", str2);
            arguments.putString("cancelButtonKey", str3);
            if (hVar != null) {
                arrayList = kotlin.a.m.c((String) hVar.f5262a, (String) hVar.f5263b);
            } else {
                arrayList = null;
            }
            arguments.putStringArrayList("titleStringKey", arrayList);
            arguments.putBoolean("destructiveKey", z);
            rVar.setArguments(arguments);
            return rVar;
        }
    }
}
