package b.b.a.i.s1;

import android.widget.LinearLayout;
import b.b.a.h.e;
import b.b.a.i.s1.c;
import com.supercell.id.R;
import com.supercell.id.view.ProgressBar;
import kotlin.d.a.a;
import kotlin.d.b.k;
import kotlin.m;

public final class b extends k implements a<m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ c.a f652a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ e f653b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b(c.a aVar, e eVar) {
        super(0);
        this.f652a = aVar;
        this.f653b = eVar;
    }

    public final Object invoke() {
        e eVar = this.f653b;
        if (eVar == null) {
            LinearLayout linearLayout = (LinearLayout) c.this.a(R.id.connectedGamesView);
            if (linearLayout != null) {
                linearLayout.setVisibility(4);
            }
            ProgressBar progressBar = (ProgressBar) c.this.a(R.id.progressBar);
            if (progressBar != null) {
                progressBar.setVisibility(0);
            }
        } else {
            c.this.a(eVar.f115b, eVar.f114a);
            LinearLayout linearLayout2 = (LinearLayout) c.this.a(R.id.connectedGamesView);
            if (linearLayout2 != null) {
                linearLayout2.setVisibility(0);
            }
            ProgressBar progressBar2 = (ProgressBar) c.this.a(R.id.progressBar);
            if (progressBar2 != null) {
                progressBar2.setVisibility(4);
            }
        }
        return m.f5330a;
    }
}
