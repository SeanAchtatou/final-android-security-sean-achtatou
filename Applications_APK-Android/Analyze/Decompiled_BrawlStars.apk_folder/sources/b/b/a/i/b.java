package b.b.a.i;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import b.b.a.i.g;
import com.supercell.id.R;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import kotlin.TypeCastException;
import kotlin.a.ab;
import kotlin.a.an;
import kotlin.d.b.g;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    public ArrayList<a> f181a;

    /* renamed from: b  reason: collision with root package name */
    public boolean f182b;
    public final Context c;
    public final FragmentManager d;
    public final kotlin.d.a.a<m> e;

    public static abstract class a implements Parcelable {
        public static final C0016a d = new C0016a(null);

        /* renamed from: a  reason: collision with root package name */
        public final Set<Integer> f183a = an.a(Integer.valueOf(R.id.nav_area_close_button));

        /* renamed from: b  reason: collision with root package name */
        public final boolean f184b;
        public final boolean c;

        /* renamed from: b.b.a.i.b$a$a  reason: collision with other inner class name */
        public static final class C0016a {
            public /* synthetic */ C0016a(g gVar) {
            }

            public final int a(int i, int i2, int i3) {
                float f = (float) ((i - i2) - i3);
                float f2 = 0.45f * f;
                float a2 = b.b.a.b.a(180);
                float a3 = f - b.b.a.b.a(300);
                if (Float.compare(f2, a2) < 0) {
                    a3 = a2;
                } else if (Float.compare(f2, a3) <= 0) {
                    a3 = f2;
                }
                return i2 + kotlin.e.a.a(a3);
            }
        }

        public int a(int i, int i2, int i3) {
            return 0;
        }

        public int a(Resources resources, int i, int i2, int i3) {
            j.b(resources, "resources");
            return 0;
        }

        public g a(Context context) {
            j.b(context, "context");
            Fragment instantiate = Fragment.instantiate(context, a().getName());
            if (instantiate != null) {
                return (g) instantiate;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.supercell.id.ui.BaseFragment");
        }

        public abstract Class<? extends g> a();

        public Class<? extends x0> a(Resources resources) {
            j.b(resources, "resources");
            return y0.class;
        }

        public abstract int b(Resources resources, int i, int i2, int i3);

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.content.res.Resources, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final x0 b(Context context) {
            j.b(context, "context");
            Resources resources = context.getResources();
            j.a((Object) resources, "context.resources");
            Fragment instantiate = Fragment.instantiate(context, a(resources).getName());
            if (instantiate != null) {
                return (x0) instantiate;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.supercell.id.ui.NavAreaBaseFragment");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public String b() {
            String name = a().getName();
            j.a((Object) name, "fragmentClass.name");
            return name;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final String b(Resources resources) {
            j.b(resources, "resources");
            String name = a(resources).getName();
            j.a((Object) name, "navAreaFragmentClass(resources).name");
            return name;
        }

        public int c(Resources resources, int i, int i2, int i3) {
            j.b(resources, "resources");
            return d.a(i, i2, i3);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.content.res.Resources, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public g c(Context context) {
            j.b(context, "context");
            Resources resources = context.getResources();
            j.a((Object) resources, "context.resources");
            Fragment instantiate = Fragment.instantiate(context, e(resources).getName());
            if (instantiate != null) {
                return (g) instantiate;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.supercell.id.ui.BaseFragment");
        }

        public Set<Integer> c() {
            return this.f183a;
        }

        public boolean c(Resources resources) {
            j.b(resources, "resources");
            return true;
        }

        public boolean d() {
            return this.f184b;
        }

        public boolean d(Resources resources) {
            j.b(resources, "resources");
            return false;
        }

        public abstract Class<? extends g> e(Resources resources);

        public abstract boolean e();

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public String f(Resources resources) {
            j.b(resources, "resources");
            String name = e(resources).getName();
            j.a((Object) name, "topAreaFragmentClass(resources).name");
            return name;
        }

        public boolean f() {
            return this.c;
        }
    }

    /* renamed from: b.b.a.i.b$b  reason: collision with other inner class name */
    public final class C0017b extends k implements kotlin.d.a.a<x0> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ FragmentTransaction f185a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ b f186b;
        public final /* synthetic */ a c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C0017b(FragmentTransaction fragmentTransaction, b bVar, List list, List list2, a aVar, a aVar2, boolean z, boolean z2, boolean z3, Set set, boolean z4, boolean z5) {
            super(0);
            this.f185a = fragmentTransaction;
            this.f186b = bVar;
            this.c = aVar;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.content.res.Resources, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final Object invoke() {
            b bVar = this.f186b;
            a aVar = this.c;
            x0 x0Var = (x0) b.b.a.b.a(aVar.b(bVar.c), "backStackEntry", aVar);
            FragmentTransaction fragmentTransaction = this.f185a;
            int i = R.id.nav_area;
            b bVar2 = this.f186b;
            a aVar2 = this.c;
            Resources resources = bVar2.c.getResources();
            j.a((Object) resources, "context.resources");
            fragmentTransaction.add(i, x0Var, aVar2.b(resources));
            return x0Var;
        }
    }

    public final class c extends k implements kotlin.d.a.a<g> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ FragmentTransaction f187a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ b f188b;
        public final /* synthetic */ a c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(FragmentTransaction fragmentTransaction, b bVar, List list, List list2, a aVar, a aVar2, boolean z, boolean z2, boolean z3, Set set, boolean z4, boolean z5) {
            super(0);
            this.f187a = fragmentTransaction;
            this.f188b = bVar;
            this.c = aVar;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.content.res.Resources, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final Object invoke() {
            b bVar = this.f188b;
            a aVar = this.c;
            g gVar = (g) b.b.a.b.a(aVar.c(bVar.c), "backStackEntry", aVar);
            FragmentTransaction fragmentTransaction = this.f187a;
            int i = R.id.top_area;
            b bVar2 = this.f188b;
            a aVar2 = this.c;
            Resources resources = bVar2.c.getResources();
            j.a((Object) resources, "context.resources");
            fragmentTransaction.add(i, gVar, aVar2.f(resources));
            return gVar;
        }
    }

    public final class d extends k implements kotlin.d.a.a<g> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ FragmentTransaction f189a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ b f190b;
        public final /* synthetic */ a c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(FragmentTransaction fragmentTransaction, b bVar, List list, List list2, a aVar, a aVar2, boolean z, boolean z2, boolean z3, Set set, boolean z4, boolean z5) {
            super(0);
            this.f189a = fragmentTransaction;
            this.f190b = bVar;
            this.c = aVar;
        }

        public final Object invoke() {
            b bVar = this.f190b;
            a aVar = this.c;
            g gVar = (g) b.b.a.b.a(aVar.a(bVar.c), "backStackEntry", aVar);
            this.f189a.add(R.id.bottom_area, gVar, this.c.b());
            return gVar;
        }
    }

    public final class e extends k implements kotlin.d.a.b<List<? extends Boolean>, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f191a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ a f192b;
        public final /* synthetic */ boolean c;
        public final /* synthetic */ boolean d;
        public final /* synthetic */ List e;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(WeakReference weakReference, a aVar, b bVar, boolean z, boolean z2, Set set, boolean z3, boolean z4, List list) {
            super(1);
            this.f191a = weakReference;
            this.f192b = aVar;
            this.c = z;
            this.d = z3;
            this.e = list;
        }

        public final Object invoke(Object obj) {
            j.b((List) obj, "it");
            b bVar = (b) this.f191a.get();
            if (bVar != null) {
                bVar.f182b = false;
                b.a(bVar, this.e, this.f192b, this.c, this.d);
            }
            return m.f5330a;
        }
    }

    public final class f extends k implements kotlin.d.a.b<Exception, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f193a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ a f194b;
        public final /* synthetic */ boolean c;
        public final /* synthetic */ boolean d;
        public final /* synthetic */ List e;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(WeakReference weakReference, a aVar, b bVar, boolean z, boolean z2, Set set, boolean z3, boolean z4, List list) {
            super(1);
            this.f193a = weakReference;
            this.f194b = aVar;
            this.c = z;
            this.d = z3;
            this.e = list;
        }

        public final Object invoke(Object obj) {
            b bVar;
            Exception exc = (Exception) obj;
            j.b(exc, "it");
            if ((exc instanceof g.c) && (bVar = (b) this.f193a.get()) != null) {
                bVar.f182b = false;
                b.a(bVar, this.e, this.f194b, this.c, this.d);
            }
            return m.f5330a;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.app.Fragment, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final /* synthetic */ void a(b bVar, List list, a aVar, boolean z, boolean z2) {
        FragmentTransaction disallowAddToBackStack = bVar.d.beginTransaction().disallowAddToBackStack();
        if (list.contains(aVar)) {
            if (z) {
                FragmentManager fragmentManager = bVar.d;
                Resources resources = bVar.c.getResources();
                j.a((Object) resources, "context.resources");
                Fragment findFragmentByTag = fragmentManager.findFragmentByTag(aVar.b(resources));
                if (findFragmentByTag != null) {
                    j.a((Object) findFragmentByTag, "it");
                    if (!findFragmentByTag.isDetached()) {
                        disallowAddToBackStack.detach(findFragmentByTag);
                    }
                }
            }
            if (z2) {
                FragmentManager fragmentManager2 = bVar.d;
                Resources resources2 = bVar.c.getResources();
                j.a((Object) resources2, "context.resources");
                Fragment findFragmentByTag2 = fragmentManager2.findFragmentByTag(aVar.f(resources2));
                if (findFragmentByTag2 != null) {
                    j.a((Object) findFragmentByTag2, "it");
                    if (!findFragmentByTag2.isDetached()) {
                        disallowAddToBackStack.detach(findFragmentByTag2);
                    }
                }
            }
            Fragment findFragmentByTag3 = bVar.d.findFragmentByTag(aVar.b());
            if (findFragmentByTag3 != null) {
                j.a((Object) findFragmentByTag3, "it");
                if (!findFragmentByTag3.isDetached()) {
                    disallowAddToBackStack.detach(findFragmentByTag3);
                }
            }
        } else {
            if (z) {
                FragmentManager fragmentManager3 = bVar.d;
                Resources resources3 = bVar.c.getResources();
                j.a((Object) resources3, "context.resources");
                Fragment findFragmentByTag4 = fragmentManager3.findFragmentByTag(aVar.b(resources3));
                if (findFragmentByTag4 != null) {
                    disallowAddToBackStack.remove(findFragmentByTag4);
                }
            }
            if (z2) {
                FragmentManager fragmentManager4 = bVar.d;
                Resources resources4 = bVar.c.getResources();
                j.a((Object) resources4, "context.resources");
                Fragment findFragmentByTag5 = fragmentManager4.findFragmentByTag(aVar.f(resources4));
                if (findFragmentByTag5 != null) {
                    disallowAddToBackStack.remove(findFragmentByTag5);
                }
            }
            Fragment findFragmentByTag6 = bVar.d.findFragmentByTag(aVar.b());
            if (findFragmentByTag6 != null) {
                disallowAddToBackStack.remove(findFragmentByTag6);
            }
        }
        disallowAddToBackStack.commitNowAllowingStateLoss();
    }

    public final a a() {
        return (a) kotlin.a.m.f((List) this.f181a);
    }

    public final void a(a... aVarArr) {
        j.b(aVarArr, "stack");
        if (this.d.isStateSaved()) {
            b.class.getCanonicalName();
            return;
        }
        ArrayList<a> arrayList = this.f181a;
        this.f181a = kotlin.a.m.c((a[]) Arrays.copyOf(aVarArr, aVarArr.length));
        a(arrayList, this.f181a, false, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final String b(a aVar) {
        Resources resources = this.c.getResources();
        j.a((Object) resources, "context.resources");
        return aVar.b(resources);
    }

    public final void c(a aVar) {
        j.b(aVar, "entry");
        if (this.d.isStateSaved()) {
            b.class.getCanonicalName();
            return;
        }
        ArrayList<a> arrayList = this.f181a;
        this.f181a = b.b.a.b.a(arrayList, aVar);
        a(arrayList, this.f181a, true, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.b.a(java.util.ArrayList, int):java.util.ArrayList<T>
     arg types: [java.util.ArrayList<b.b.a.i.b$a>, int]
     candidates:
      b.b.a.b.a(int, float):int
      b.b.a.b.a(android.support.v7.widget.RecyclerView, int):int
      b.b.a.b.a(java.lang.String, java.lang.String):int
      b.b.a.b.a(android.widget.ImageView, int):android.animation.Animator
      b.b.a.b.a(java.util.ArrayList, java.lang.Object):java.util.ArrayList<T>
      b.b.a.b.a(java.util.Map, java.util.Map):java.util.Map<K, V>
      b.b.a.b.a(b.b.a.i.x1.f, java.lang.String):nl.komponents.kovenant.bw
      b.b.a.b.a(com.supercell.id.ui.MainActivity, android.view.View):void
      b.b.a.b.a(com.supercell.id.ui.MainActivity, com.supercell.id.IdAccount):void
      b.b.a.b.a(java.util.List<? extends android.view.View>, java.util.List<b.b.a.j.c1>):void
      b.b.a.b.a(java.util.List<? extends b.b.a.j.r0>, int):boolean
      b.b.a.b.a(java.util.ArrayList, int):java.util.ArrayList<T> */
    public final boolean d(a aVar) {
        j.b(aVar, "entry");
        if (this.f181a.size() <= 0) {
            return false;
        }
        ArrayList<a> arrayList = this.f181a;
        this.f181a = b.b.a.b.a(b.b.a.b.a((ArrayList) arrayList, 1), aVar);
        a(arrayList, this.f181a, true, true);
        return true;
    }

    public final String toString() {
        return kotlin.a.m.a(this.f181a, ", ", (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, (kotlin.d.a.b) null, 62);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
     arg types: [java.util.ArrayList, java.util.Set]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.List<android.support.v4.app.Fragment>, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.app.Fragment, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00bb  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0076 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.util.ArrayList<b.b.a.i.b.a> r11) {
        /*
            r10 = this;
            r0 = 3
            java.lang.Integer[] r1 = new java.lang.Integer[r0]
            int r2 = com.supercell.id.R.id.nav_area
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r3 = 0
            r1[r3] = r2
            int r2 = com.supercell.id.R.id.top_area
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r4 = 1
            r1[r4] = r2
            int r2 = com.supercell.id.R.id.bottom_area
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r5 = 2
            r1[r5] = r2
            java.util.Set r1 = kotlin.a.an.a(r1)
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            java.util.Iterator r11 = r11.iterator()
        L_0x002b:
            boolean r6 = r11.hasNext()
            if (r6 == 0) goto L_0x005e
            java.lang.Object r6 = r11.next()
            b.b.a.i.b$a r6 = (b.b.a.i.b.a) r6
            java.lang.String[] r7 = new java.lang.String[r0]
            java.lang.String r8 = r10.b(r6)
            r7[r3] = r8
            android.content.Context r8 = r10.c
            android.content.res.Resources r8 = r8.getResources()
            java.lang.String r9 = "context.resources"
            kotlin.d.b.j.a(r8, r9)
            java.lang.String r8 = r6.f(r8)
            r7[r4] = r8
            java.lang.String r6 = r6.b()
            r7[r5] = r6
            java.util.Set r6 = kotlin.a.an.a(r7)
            kotlin.a.m.a(r2, r6)
            goto L_0x002b
        L_0x005e:
            java.util.Set r11 = kotlin.a.m.i(r2)
            android.support.v4.app.FragmentManager r0 = r10.d
            java.util.List r0 = r0.getFragments()
            java.lang.String r2 = "supportFragmentManager.fragments"
            kotlin.d.b.j.a(r0, r2)
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            java.util.Iterator r0 = r0.iterator()
        L_0x0076:
            boolean r5 = r0.hasNext()
            if (r5 == 0) goto L_0x00bf
            java.lang.Object r5 = r0.next()
            r6 = r5
            android.support.v4.app.Fragment r6 = (android.support.v4.app.Fragment) r6
            java.lang.String r7 = "it"
            kotlin.d.b.j.a(r6, r7)
            int r7 = r6.getId()
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)
            boolean r7 = r1.contains(r7)
            if (r7 == 0) goto L_0x00b8
            java.lang.String r6 = r6.getTag()
            java.lang.String r7 = "$this$contains"
            kotlin.d.b.j.b(r11, r7)
            boolean r7 = r11 instanceof java.util.Collection
            if (r7 == 0) goto L_0x00ab
            r7 = r11
            java.util.Collection r7 = (java.util.Collection) r7
            boolean r6 = r7.contains(r6)
            goto L_0x00b4
        L_0x00ab:
            int r6 = kotlin.a.m.a(r11, r6)
            if (r6 < 0) goto L_0x00b3
            r6 = 1
            goto L_0x00b4
        L_0x00b3:
            r6 = 0
        L_0x00b4:
            if (r6 != 0) goto L_0x00b8
            r6 = 1
            goto L_0x00b9
        L_0x00b8:
            r6 = 0
        L_0x00b9:
            if (r6 == 0) goto L_0x0076
            r2.add(r5)
            goto L_0x0076
        L_0x00bf:
            boolean r11 = r2.isEmpty()
            r11 = r11 ^ r4
            if (r11 == 0) goto L_0x00e7
            android.support.v4.app.FragmentManager r11 = r10.d
            android.support.v4.app.FragmentTransaction r11 = r11.beginTransaction()
            android.support.v4.app.FragmentTransaction r11 = r11.disallowAddToBackStack()
            java.util.Iterator r0 = r2.iterator()
        L_0x00d4:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x00e4
            java.lang.Object r1 = r0.next()
            android.support.v4.app.Fragment r1 = (android.support.v4.app.Fragment) r1
            r11.remove(r1)
            goto L_0x00d4
        L_0x00e4:
            r11.commitNow()
        L_0x00e7:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.i.b.a(java.util.ArrayList):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final Set<String> a(a aVar) {
        Resources resources = this.c.getResources();
        j.a((Object) resources, "context.resources");
        Resources resources2 = this.c.getResources();
        j.a((Object) resources2, "context.resources");
        return an.a((Object[]) new String[]{aVar.b(resources), aVar.f(resources2), aVar.b()});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.b(java.lang.Iterable, java.lang.Iterable):java.util.Set<T>
     arg types: [java.util.Set<java.lang.Integer>, java.util.Set<java.lang.Integer>]
     candidates:
      kotlin.a.w.b(java.lang.Iterable, int):T
      kotlin.a.t.b(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.w.b(java.lang.Iterable, java.lang.Iterable):java.util.Set<T> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.c(java.lang.Iterable, java.lang.Iterable):java.util.Set<T>
     arg types: [java.util.List<? extends b.b.a.i.b$a>, java.util.List<? extends b.b.a.i.b$a>]
     candidates:
      kotlin.a.w.c(java.lang.Iterable, int):java.util.List<T>
      kotlin.a.t.c(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.w.c(java.lang.Iterable, java.lang.Iterable):java.util.Set<T> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
     arg types: [java.util.ArrayList, java.util.Set<java.lang.String>]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.c(java.lang.Iterable, java.lang.Iterable):java.util.Set<T>
     arg types: [java.util.ArrayList, java.util.Set<java.lang.String>]
     candidates:
      kotlin.a.w.c(java.lang.Iterable, int):java.util.List<T>
      kotlin.a.t.c(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.w.c(java.lang.Iterable, java.lang.Iterable):java.util.Set<T> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.c(java.lang.Iterable, java.lang.Iterable):java.util.Set<T>
     arg types: [java.util.Set, java.util.Set<java.lang.String>]
     candidates:
      kotlin.a.w.c(java.lang.Iterable, int):java.util.List<T>
      kotlin.a.t.c(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.w.c(java.lang.Iterable, java.lang.Iterable):java.util.Set<T> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.app.Fragment, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x02fa  */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x030e  */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x0312  */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x0319  */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x0329  */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x0351  */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x0368  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.util.List<? extends b.b.a.i.b.a> r29, java.util.List<? extends b.b.a.i.b.a> r30, boolean r31, boolean r32) {
        /*
            r28 = this;
            r13 = r28
            r14 = r31
            java.lang.Object r0 = kotlin.a.m.f(r29)
            b.b.a.i.b$a r0 = (b.b.a.i.b.a) r0
            java.lang.Object r1 = kotlin.a.m.e(r30)
            r15 = r1
            b.b.a.i.b$a r15 = (b.b.a.i.b.a) r15
            boolean r1 = kotlin.d.b.j.a(r15, r0)
            if (r1 == 0) goto L_0x0018
            return
        L_0x0018:
            r16 = 0
            if (r32 == 0) goto L_0x001e
            r12 = r0
            goto L_0x0020
        L_0x001e:
            r12 = r16
        L_0x0020:
            r17 = 0
            r11 = 1
            if (r14 == 0) goto L_0x002c
            boolean r0 = r15.e()
            r18 = r0
            goto L_0x0041
        L_0x002c:
            if (r12 == 0) goto L_0x0033
            boolean r0 = r12.e()
            goto L_0x0034
        L_0x0033:
            r0 = 0
        L_0x0034:
            if (r0 == 0) goto L_0x003f
            int r0 = r29.size()
            if (r0 <= r11) goto L_0x003f
            r18 = 1
            goto L_0x0041
        L_0x003f:
            r18 = 0
        L_0x0041:
            java.lang.String r0 = r13.b(r15)
            java.lang.String r10 = "context.resources"
            if (r12 == 0) goto L_0x0057
            android.content.Context r1 = r13.c
            android.content.res.Resources r1 = r1.getResources()
            kotlin.d.b.j.a(r1, r10)
            java.lang.String r1 = r12.b(r1)
            goto L_0x0059
        L_0x0057:
            r1 = r16
        L_0x0059:
            boolean r0 = kotlin.d.b.j.a(r0, r1)
            r19 = r0 ^ 1
            java.util.Set r0 = r15.c()
            if (r12 == 0) goto L_0x006c
            java.util.Set r1 = r12.c()
            if (r1 == 0) goto L_0x006c
            goto L_0x0070
        L_0x006c:
            kotlin.a.ad r1 = kotlin.a.ad.f5212a
            java.util.Set r1 = (java.util.Set) r1
        L_0x0070:
            java.util.Set r9 = kotlin.a.m.b(r0, r1)
            android.content.Context r0 = r13.c
            android.content.res.Resources r0 = r0.getResources()
            kotlin.d.b.j.a(r0, r10)
            java.lang.String r0 = r15.f(r0)
            if (r12 == 0) goto L_0x0091
            android.content.Context r1 = r13.c
            android.content.res.Resources r1 = r1.getResources()
            kotlin.d.b.j.a(r1, r10)
            java.lang.String r1 = r12.f(r1)
            goto L_0x0093
        L_0x0091:
            r1 = r16
        L_0x0093:
            boolean r0 = kotlin.d.b.j.a(r0, r1)
            r20 = r0 ^ 1
            android.support.v4.app.FragmentManager r0 = r13.d
            android.support.v4.app.FragmentTransaction r0 = r0.beginTransaction()
            android.support.v4.app.FragmentTransaction r8 = r0.disallowAddToBackStack()
            java.util.Set r0 = kotlin.a.m.c(r29, r30)
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.util.Iterator r0 = r0.iterator()
        L_0x00b0:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L_0x00c4
            java.lang.Object r2 = r0.next()
            b.b.a.i.b$a r2 = (b.b.a.i.b.a) r2
            java.util.Set r2 = r13.a(r2)
            kotlin.a.m.a(r1, r2)
            goto L_0x00b0
        L_0x00c4:
            java.util.Set r0 = r13.a(r15)
            java.util.Set r0 = kotlin.a.m.c(r1, r0)
            if (r12 == 0) goto L_0x00d5
            java.util.Set r1 = r13.a(r12)
            if (r1 == 0) goto L_0x00d5
            goto L_0x00d9
        L_0x00d5:
            kotlin.a.ad r1 = kotlin.a.ad.f5212a
            java.util.Set r1 = (java.util.Set) r1
        L_0x00d9:
            java.util.Set r0 = kotlin.a.m.c(r0, r1)
            java.util.Iterator r0 = r0.iterator()
        L_0x00e1:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x00f9
            java.lang.Object r1 = r0.next()
            java.lang.String r1 = (java.lang.String) r1
            android.support.v4.app.FragmentManager r2 = r13.d
            android.support.v4.app.Fragment r1 = r2.findFragmentByTag(r1)
            if (r1 == 0) goto L_0x00e1
            r8.remove(r1)
            goto L_0x00e1
        L_0x00f9:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.util.Iterator r1 = r30.iterator()
        L_0x0102:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x0116
            java.lang.Object r2 = r1.next()
            b.b.a.i.b$a r2 = (b.b.a.i.b.a) r2
            java.util.Set r2 = r13.a(r2)
            kotlin.a.m.a(r0, r2)
            goto L_0x0102
        L_0x0116:
            java.util.Set r1 = r13.a(r15)
            java.util.Set r0 = kotlin.a.m.c(r0, r1)
            if (r12 == 0) goto L_0x0127
            java.util.Set r1 = r13.a(r12)
            if (r1 == 0) goto L_0x0127
            goto L_0x012b
        L_0x0127:
            kotlin.a.ad r1 = kotlin.a.ad.f5212a
            java.util.Set r1 = (java.util.Set) r1
        L_0x012b:
            java.util.Set r0 = kotlin.a.m.c(r0, r1)
            java.util.Iterator r0 = r0.iterator()
        L_0x0133:
            boolean r1 = r0.hasNext()
            java.lang.String r7 = "it"
            if (r1 == 0) goto L_0x0156
            java.lang.Object r1 = r0.next()
            java.lang.String r1 = (java.lang.String) r1
            android.support.v4.app.FragmentManager r2 = r13.d
            android.support.v4.app.Fragment r1 = r2.findFragmentByTag(r1)
            if (r1 == 0) goto L_0x0133
            kotlin.d.b.j.a(r1, r7)
            boolean r2 = r1.isDetached()
            if (r2 != 0) goto L_0x0133
            r8.detach(r1)
            goto L_0x0133
        L_0x0156:
            android.support.v4.app.FragmentManager r0 = r13.d
            android.content.Context r1 = r13.c
            android.content.res.Resources r1 = r1.getResources()
            kotlin.d.b.j.a(r1, r10)
            java.lang.String r1 = r15.b(r1)
            android.support.v4.app.Fragment r0 = r0.findFragmentByTag(r1)
            if (r0 == 0) goto L_0x0181
            kotlin.d.b.j.a(r0, r7)
            boolean r1 = r0.isDetached()
            if (r1 == 0) goto L_0x0177
            r8.attach(r0)
        L_0x0177:
            r24 = r7
            r22 = r8
            r23 = r9
            r14 = r10
            r25 = r12
            goto L_0x01ad
        L_0x0181:
            b.b.a.i.b$b r21 = new b.b.a.i.b$b
            r0 = r21
            r1 = r8
            r2 = r28
            r3 = r29
            r4 = r30
            r5 = r15
            r6 = r12
            r14 = r7
            r7 = r32
            r22 = r8
            r8 = r19
            r23 = r9
            r9 = r31
            r24 = r14
            r14 = r10
            r10 = r23
            r11 = r20
            r25 = r12
            r12 = r18
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
            java.lang.Object r0 = r21.invoke()
            android.support.v4.app.Fragment r0 = (android.support.v4.app.Fragment) r0
        L_0x01ad:
            r12 = r0
            java.lang.String r11 = "supportFragmentManager.f…        }\n            }()"
            kotlin.d.b.j.a(r12, r11)
            android.support.v4.app.FragmentManager r0 = r13.d
            android.content.Context r1 = r13.c
            android.content.res.Resources r1 = r1.getResources()
            kotlin.d.b.j.a(r1, r14)
            java.lang.String r1 = r15.f(r1)
            android.support.v4.app.Fragment r0 = r0.findFragmentByTag(r1)
            if (r0 == 0) goto L_0x01e3
            r10 = r24
            kotlin.d.b.j.a(r0, r10)
            boolean r1 = r0.isDetached()
            r9 = r22
            if (r1 == 0) goto L_0x01d8
            r9.attach(r0)
        L_0x01d8:
            r24 = r9
            r26 = r10
            r27 = r12
            r22 = r14
            r12 = r0
            r14 = r11
            goto L_0x0216
        L_0x01e3:
            r9 = r22
            r10 = r24
            b.b.a.i.b$c r21 = new b.b.a.i.b$c
            r0 = r21
            r1 = r9
            r2 = r28
            r3 = r29
            r4 = r30
            r5 = r15
            r6 = r25
            r7 = r32
            r8 = r19
            r22 = r14
            r14 = r9
            r9 = r31
            r24 = r14
            r14 = r10
            r10 = r23
            r26 = r14
            r14 = r11
            r11 = r20
            r27 = r12
            r12 = r18
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
            java.lang.Object r0 = r21.invoke()
            android.support.v4.app.Fragment r0 = (android.support.v4.app.Fragment) r0
            r12 = r0
        L_0x0216:
            kotlin.d.b.j.a(r12, r14)
            android.support.v4.app.FragmentManager r0 = r13.d
            java.lang.String r1 = r15.b()
            android.support.v4.app.Fragment r0 = r0.findFragmentByTag(r1)
            if (r0 == 0) goto L_0x0237
            r1 = r26
            kotlin.d.b.j.a(r0, r1)
            boolean r1 = r0.isDetached()
            r14 = r24
            if (r1 == 0) goto L_0x0235
            r14.attach(r0)
        L_0x0235:
            r15 = r12
            goto L_0x025d
        L_0x0237:
            r14 = r24
            b.b.a.i.b$d r21 = new b.b.a.i.b$d
            r0 = r21
            r1 = r14
            r2 = r28
            r3 = r29
            r4 = r30
            r5 = r15
            r6 = r25
            r7 = r32
            r8 = r19
            r9 = r31
            r10 = r23
            r11 = r20
            r15 = r12
            r12 = r18
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
            java.lang.Object r0 = r21.invoke()
            android.support.v4.app.Fragment r0 = (android.support.v4.app.Fragment) r0
        L_0x025d:
            java.lang.String r1 = "((supportFragmentManager…       }\n            }())"
            kotlin.d.b.j.a(r0, r1)
            java.lang.String r1 = "sharedElements"
            java.lang.String r2 = "animation"
            if (r32 == 0) goto L_0x02a7
            if (r19 == 0) goto L_0x026d
            b.b.a.i.g$a r3 = b.b.a.i.g.a.FADE_IN
            goto L_0x026f
        L_0x026d:
            b.b.a.i.g$a r3 = b.b.a.i.g.a.PAGE_CHANGED
        L_0x026f:
            r4 = r3
            r3 = r27
            boolean r5 = r3 instanceof b.b.a.i.x0
            if (r5 != 0) goto L_0x0278
            r3 = r16
        L_0x0278:
            b.b.a.i.x0 r3 = (b.b.a.i.x0) r3
            if (r3 == 0) goto L_0x028e
            kotlin.d.b.j.b(r4, r2)
            r10 = r23
            kotlin.d.b.j.b(r10, r1)
            r3.m = r10
            r11 = r31
            r3.a(r4, r11)
            kotlin.m r3 = kotlin.m.f5330a
            goto L_0x0292
        L_0x028e:
            r11 = r31
            r10 = r23
        L_0x0292:
            if (r20 == 0) goto L_0x0297
            b.b.a.i.g$a r3 = b.b.a.i.g.a.FADE_IN
            goto L_0x0299
        L_0x0297:
            b.b.a.i.g$a r3 = b.b.a.i.g.a.PAGE_CHANGED
        L_0x0299:
            b.b.a.b.a(r15, r3, r11)
            if (r18 == 0) goto L_0x02a1
            b.b.a.i.g$a r3 = b.b.a.i.g.a.SLIDE_IN
            goto L_0x02a3
        L_0x02a1:
            b.b.a.i.g$a r3 = b.b.a.i.g.a.FADE_IN
        L_0x02a3:
            b.b.a.b.a(r0, r3, r11)
            goto L_0x02ab
        L_0x02a7:
            r11 = r31
            r10 = r23
        L_0x02ab:
            r14.commitNow()
            r12 = r25
            if (r12 == 0) goto L_0x0396
            r0 = 1
            r13.f182b = r0
            java.lang.ref.WeakReference r14 = new java.lang.ref.WeakReference
            r14.<init>(r13)
            r3 = 3
            nl.komponents.kovenant.bw[] r3 = new nl.komponents.kovenant.bw[r3]
            if (r19 == 0) goto L_0x02d5
            android.support.v4.app.FragmentManager r4 = r13.d
            android.content.Context r5 = r13.c
            android.content.res.Resources r5 = r5.getResources()
            r6 = r22
            kotlin.d.b.j.a(r5, r6)
            java.lang.String r5 = r12.b(r5)
            android.support.v4.app.Fragment r4 = r4.findFragmentByTag(r5)
            goto L_0x02d9
        L_0x02d5:
            r6 = r22
            r4 = r16
        L_0x02d9:
            if (r4 == 0) goto L_0x02f4
            b.b.a.i.g$b r5 = b.b.a.i.g.b.FADE_OUT
            boolean r7 = r4 instanceof b.b.a.i.x0
            if (r7 != 0) goto L_0x02e3
            r4 = r16
        L_0x02e3:
            b.b.a.i.x0 r4 = (b.b.a.i.x0) r4
            if (r4 == 0) goto L_0x02f4
            kotlin.d.b.j.b(r5, r2)
            kotlin.d.b.j.b(r10, r1)
            r4.m = r10
            nl.komponents.kovenant.bw r1 = r4.a(r5, r11)
            goto L_0x02f6
        L_0x02f4:
            r1 = r16
        L_0x02f6:
            r3[r17] = r1
            if (r20 == 0) goto L_0x030e
            android.support.v4.app.FragmentManager r1 = r13.d
            android.content.Context r2 = r13.c
            android.content.res.Resources r2 = r2.getResources()
            kotlin.d.b.j.a(r2, r6)
            java.lang.String r2 = r12.f(r2)
            android.support.v4.app.Fragment r1 = r1.findFragmentByTag(r2)
            goto L_0x0310
        L_0x030e:
            r1 = r16
        L_0x0310:
            if (r1 == 0) goto L_0x0319
            b.b.a.i.g$b r2 = b.b.a.i.g.b.FADE_OUT
            nl.komponents.kovenant.bw r1 = b.b.a.b.a(r1, r2, r11)
            goto L_0x031b
        L_0x0319:
            r1 = r16
        L_0x031b:
            r3[r0] = r1
            android.support.v4.app.FragmentManager r1 = r13.d
            java.lang.String r2 = r12.b()
            android.support.v4.app.Fragment r1 = r1.findFragmentByTag(r2)
            if (r1 == 0) goto L_0x0334
            if (r18 == 0) goto L_0x032e
            b.b.a.i.g$b r2 = b.b.a.i.g.b.SLIDE_OUT
            goto L_0x0330
        L_0x032e:
            b.b.a.i.g$b r2 = b.b.a.i.g.b.FADE_OUT
        L_0x0330:
            nl.komponents.kovenant.bw r16 = b.b.a.b.a(r1, r2, r11)
        L_0x0334:
            r1 = 2
            r3[r1] = r16
            java.util.List r1 = kotlin.a.m.d(r3)
            nl.komponents.kovenant.bb r2 = nl.komponents.kovenant.bb.f5389a
            nl.komponents.kovenant.ao r2 = nl.komponents.kovenant.bb.a()
            java.lang.String r3 = "promises"
            kotlin.d.b.j.b(r1, r3)
            java.lang.String r4 = "context"
            kotlin.d.b.j.b(r2, r4)
            int r5 = r1.size()
            if (r5 == 0) goto L_0x0368
            kotlin.d.b.j.b(r1, r3)
            kotlin.d.b.j.b(r2, r4)
            r3 = r1
            java.lang.Iterable r3 = (java.lang.Iterable) r3
            kotlin.i.h r3 = kotlin.a.m.m(r3)
            int r1 = r1.size()
            nl.komponents.kovenant.bw r0 = nl.komponents.kovenant.l.a(r3, r1, r2, r0)
        L_0x0366:
            r15 = r0
            goto L_0x0373
        L_0x0368:
            nl.komponents.kovenant.bw$a r0 = nl.komponents.kovenant.bw.d
            kotlin.a.ab r0 = kotlin.a.ab.f5210a
            java.util.List r0 = (java.util.List) r0
            nl.komponents.kovenant.bw r0 = nl.komponents.kovenant.bw.a.a(r0, r2)
            goto L_0x0366
        L_0x0373:
            b.b.a.i.b$e r9 = new b.b.a.i.b$e
            r0 = r9
            r1 = r14
            r2 = r12
            r3 = r28
            r4 = r19
            r5 = r31
            r6 = r10
            r7 = r20
            r8 = r18
            r11 = r9
            r9 = r30
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9)
            nl.komponents.kovenant.bw r11 = nl.komponents.kovenant.c.m.a(r15, r11)
            b.b.a.i.b$f r15 = new b.b.a.i.b$f
            r0 = r15
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9)
            nl.komponents.kovenant.c.m.b(r11, r15)
        L_0x0396:
            if (r32 == 0) goto L_0x039d
            kotlin.d.a.a<kotlin.m> r0 = r13.e
            r0.invoke()
        L_0x039d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.i.b.a(java.util.List, java.util.List, boolean, boolean):void");
    }

    public b(Context context, FragmentManager fragmentManager, kotlin.d.a.a<m> aVar, a... aVarArr) {
        j.b(context, "context");
        j.b(fragmentManager, "supportFragmentManager");
        j.b(aVar, "animateChangeCallback");
        j.b(aVarArr, "entries");
        this.c = context;
        this.d = fragmentManager;
        this.e = aVar;
        this.f181a = kotlin.a.m.c((a[]) Arrays.copyOf(aVarArr, aVarArr.length));
        a(this.f181a);
        a(ab.f5210a, this.f181a, true, false);
    }
}
