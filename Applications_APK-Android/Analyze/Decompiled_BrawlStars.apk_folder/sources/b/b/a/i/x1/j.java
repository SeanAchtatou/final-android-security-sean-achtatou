package b.b.a.i.x1;

import android.graphics.drawable.Drawable;
import android.text.SpannableStringBuilder;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.supercell.id.SupercellId;
import java.lang.ref.WeakReference;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.WeakHashMap;
import kotlin.a.ai;
import kotlin.d.b.k;
import kotlin.h;
import kotlin.m;

public final class j {

    /* renamed from: a  reason: collision with root package name */
    public static final Map<TextView, kotlin.d.a.b<String, m>> f947a = Collections.synchronizedMap(new WeakHashMap());

    /* renamed from: b  reason: collision with root package name */
    public static final Map<EditText, kotlin.d.a.b<String, m>> f948b = Collections.synchronizedMap(new WeakHashMap());
    public static final Map<ImageView, kotlin.d.a.c<Drawable, c, m>> c = Collections.synchronizedMap(new WeakHashMap());

    public final class a implements kotlin.d.a.b<String, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f949a;

        public a(WeakReference weakReference) {
            this.f949a = weakReference;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.EditText, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final Object invoke(Object obj) {
            String str = (String) obj;
            kotlin.d.b.j.b(str, "value");
            EditText editText = (EditText) this.f949a.get();
            if (editText != null) {
                kotlin.d.b.j.a((Object) editText, "weakView.get() ?: return");
                if (!(!kotlin.d.b.j.a(this, j.f948b.get(editText)))) {
                    editText.setHint(str);
                }
            }
            return m.f5330a;
        }
    }

    public final class b implements kotlin.d.a.c<Drawable, c, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f950a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ boolean f951b;

        public b(WeakReference weakReference, boolean z) {
            this.f950a = weakReference;
            this.f951b = z;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.ImageView, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final Object invoke(Object obj, Object obj2) {
            Drawable drawable = (Drawable) obj;
            c cVar = (c) obj2;
            kotlin.d.b.j.b(drawable, "drawable");
            kotlin.d.b.j.b(cVar, "assetLocation");
            ImageView imageView = (ImageView) this.f950a.get();
            if (imageView != null) {
                kotlin.d.b.j.a((Object) imageView, "weakView.get() ?: return");
                if (!(!kotlin.d.b.j.a(this, j.c.get(imageView)))) {
                    imageView.setImageDrawable(drawable);
                    if (this.f951b && cVar == c.EXTERNAL) {
                        imageView.setAlpha(0.0f);
                        imageView.animate().alpha(1.0f).setDuration(300).start();
                    }
                }
            }
            return m.f5330a;
        }
    }

    public final class c extends k implements kotlin.d.a.b<String, SpannableStringBuilder> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ Map f952a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(Map map) {
            super(1);
            this.f952a = map;
        }

        public final Object invoke(Object obj) {
            String str = (String) obj;
            kotlin.d.b.j.b(str, "value");
            return j.a(str, new k(this));
        }
    }

    public final class d implements kotlin.d.a.b<String, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f953a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ kotlin.d.a.b f954b;

        public d(WeakReference weakReference, kotlin.d.a.b bVar) {
            this.f953a = weakReference;
            this.f954b = bVar;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.TextView, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final Object invoke(Object obj) {
            CharSequence charSequence;
            CharSequence charSequence2 = (String) obj;
            kotlin.d.b.j.b(charSequence2, "value");
            TextView textView = (TextView) this.f953a.get();
            if (textView != null) {
                kotlin.d.b.j.a((Object) textView, "weakView.get() ?: return");
                if (!(!kotlin.d.b.j.a(this, j.f947a.get(textView)))) {
                    kotlin.d.a.b bVar = this.f954b;
                    if (!(bVar == null || (charSequence = (CharSequence) bVar.invoke(charSequence2)) == null)) {
                        charSequence2 = charSequence;
                    }
                    textView.setText(charSequence2);
                }
            }
            return m.f5330a;
        }
    }

    public final class e extends k implements kotlin.d.a.b<String, SpannableStringBuilder> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ Object f955a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ int f956b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(Object obj, int i) {
            super(1);
            this.f955a = obj;
            this.f956b = i;
        }

        public final Object invoke(Object obj) {
            String str = (String) obj;
            kotlin.d.b.j.b(str, "value");
            return j.a(str, new l(this));
        }
    }

    public static final /* synthetic */ SpannableStringBuilder a(CharSequence charSequence, kotlin.d.a.c cVar) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        int i = 0;
        while (i < charSequence.length()) {
            char charAt = charSequence.charAt(i);
            if (charAt == '[') {
                int i2 = i + 1;
                int i3 = i2;
                while (charSequence.charAt(i3) != ']' && i3 < charSequence.length()) {
                    i3++;
                }
                if (i3 < charSequence.length()) {
                    cVar.invoke(charSequence.subSequence(i2, i3).toString(), spannableStringBuilder);
                }
                i = i3;
            } else {
                spannableStringBuilder.append(charAt);
            }
            i++;
        }
        if (i < charSequence.length()) {
            spannableStringBuilder.append(charSequence.charAt(i));
        }
        return spannableStringBuilder;
    }

    public static /* synthetic */ void a(ImageView imageView, String str, boolean z, int i) {
        if ((i & 2) != 0) {
            z = true;
        }
        a(imageView, str, z);
    }

    public static final void a(TextView textView) {
        kotlin.d.b.j.b(textView, "$this$clearPendingTextKey");
        f947a.remove(textView);
    }

    public static final void a(TextView textView, String str, Object obj, int i) {
        kotlin.d.b.j.b(textView, "$this$setTextKeyLinkSpanned");
        kotlin.d.b.j.b(str, "textKey");
        kotlin.d.b.j.b(obj, "what");
        a(textView, str, new e(obj, i));
    }

    public static final void a(TextView textView, String str, Map<String, ? extends CharSequence> map) {
        kotlin.d.b.j.b(textView, "$this$setTextKey");
        kotlin.d.b.j.b(str, "textKey");
        kotlin.d.b.j.b(map, "replacements");
        a(textView, str, new c(map));
    }

    public static /* synthetic */ void a(TextView textView, String str, kotlin.d.a.b bVar, int i) {
        if ((i & 2) != 0) {
            bVar = null;
        }
        a(textView, str, bVar);
    }

    public static final void a(TextView textView, String str, h<String, ? extends CharSequence>... hVarArr) {
        kotlin.d.b.j.b(textView, "$this$setTextKey");
        kotlin.d.b.j.b(str, "textKey");
        kotlin.d.b.j.b(hVarArr, "replacements");
        a(textView, str, ai.b(hVarArr));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Calendar, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Date, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final void a(TextView textView, Date date) {
        String str;
        long j;
        kotlin.d.b.j.b(textView, "$this$setTextKeyTimeAgo");
        kotlin.d.b.j.b(date, "date");
        Calendar instance = Calendar.getInstance();
        kotlin.d.b.j.a((Object) instance, "Calendar.getInstance()");
        Date time = instance.getTime();
        kotlin.d.b.j.a((Object) time, "now");
        long time2 = time.getTime() - date.getTime();
        if (time2 < 3600000) {
            j = time2 / 60000;
            str = "date_util_time_ago_minute";
        } else if (time2 < 86400000) {
            j = time2 / 3600000;
            str = "date_util_time_ago_hour";
        } else if (time2 < 604800000) {
            j = time2 / 86400000;
            str = "date_util_time_ago_day";
        } else if (time2 < 2592000000L) {
            j = time2 / 604800000;
            str = "date_util_time_ago_week";
        } else if (time2 < 31536000000L) {
            j = time2 / 2592000000L;
            str = "date_util_time_ago_month";
        } else {
            j = time2 / 31536000000L;
            str = "date_util_time_ago_year";
        }
        a(textView, str, ai.a(kotlin.k.a("time", String.valueOf(j))));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Map<android.widget.EditText, kotlin.d.a.b<java.lang.String, kotlin.m>>, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final void a(EditText editText, String str) {
        kotlin.d.b.j.b(editText, "$this$setHintKey");
        kotlin.d.b.j.b(str, "hintKey");
        a aVar = new a(new WeakReference(editText));
        Map<EditText, kotlin.d.a.b<String, m>> map = f948b;
        kotlin.d.b.j.a((Object) map, "lastEditTextHintKeyCallbacks");
        map.put(editText, aVar);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().j.a(str, aVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Map<android.widget.ImageView, kotlin.d.a.c<android.graphics.drawable.Drawable, b.b.a.i.x1.c, kotlin.m>>, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final void a(ImageView imageView, String str, boolean z) {
        kotlin.d.b.j.b(imageView, "$this$setSrcKey");
        kotlin.d.b.j.b(str, "srcKey");
        b bVar = new b(new WeakReference(imageView), z);
        Map<ImageView, kotlin.d.a.c<Drawable, c, m>> map = c;
        kotlin.d.b.j.a((Object) map, "lastImageViewSrcKeyCallbacks");
        map.put(imageView, bVar);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().j.a(str, bVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Map<android.widget.TextView, kotlin.d.a.b<java.lang.String, kotlin.m>>, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final void a(TextView textView, String str, kotlin.d.a.b<? super String, ? extends CharSequence> bVar) {
        kotlin.d.b.j.b(textView, "$this$setTextKey");
        kotlin.d.b.j.b(str, "textKey");
        d dVar = new d(new WeakReference(textView), bVar);
        Map<TextView, kotlin.d.a.b<String, m>> map = f947a;
        kotlin.d.b.j.a((Object) map, "lastTextViewTextKeyCallbacks");
        map.put(textView, dVar);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().j.a(str, dVar);
    }
}
