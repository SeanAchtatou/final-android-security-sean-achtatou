package b.b.a.i.o1;

import b.b.a.h.o;
import com.facebook.share.internal.ShareConstants;
import java.lang.ref.WeakReference;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class b extends k implements kotlin.d.a.b<o, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f410a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b(WeakReference weakReference) {
        super(1);
        this.f410a = weakReference;
    }

    public final Object invoke(Object obj) {
        o oVar = (o) obj;
        j.b(oVar, ShareConstants.WEB_DIALOG_PARAM_DATA);
        a aVar = (a) this.f410a.get();
        if (aVar != null) {
            if (oVar instanceof o.c) {
                aVar.c(oVar.f138a);
            } else if (oVar instanceof o.a) {
                aVar.a(((o.a) oVar).a(), oVar.f138a);
            }
        }
        return m.f5330a;
    }
}
