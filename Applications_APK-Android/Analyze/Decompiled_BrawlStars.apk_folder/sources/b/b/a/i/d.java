package b.b.a.i;

import android.app.Activity;
import android.app.Dialog;
import com.supercell.id.SupercellId;
import kotlin.d.b.j;

public class d extends Dialog {
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d(Activity activity, int i) {
        super(activity, i);
        j.b(activity, "activity");
    }

    public void onStart() {
        super.onStart();
        SupercellId.INSTANCE.onWindowClientStart$supercellId_release();
    }

    public void onStop() {
        SupercellId.INSTANCE.onWindowClientStop$supercellId_release();
        super.onStop();
    }
}
