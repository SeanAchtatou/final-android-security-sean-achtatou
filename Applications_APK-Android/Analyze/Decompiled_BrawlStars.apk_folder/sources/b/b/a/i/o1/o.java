package b.b.a.i.o1;

import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.AppCompatButton;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import b.b.a.i.e;
import b.b.a.i.u;
import b.b.a.i.v;
import b.b.a.j.n;
import b.b.a.j.q1;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.g;
import com.google.i18n.phonenumbers.j;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.PinEntryView;
import com.supercell.id.view.WidthAdjustingMultilineButton;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.h;
import kotlin.m;

public final class o extends q implements u {
    public HashMap c;

    public final class a implements View.OnClickListener {
        public a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) o.this.a(R.id.okButton);
            j.a((Object) widthAdjustingMultilineButton, "okButton");
            widthAdjustingMultilineButton.setEnabled(false);
            WidthAdjustingMultilineButton widthAdjustingMultilineButton2 = (WidthAdjustingMultilineButton) o.this.a(R.id.cancelButton);
            j.a((Object) widthAdjustingMultilineButton2, "cancelButton");
            widthAdjustingMultilineButton2.setEnabled(false);
            SupercellId.INSTANCE.clearPendingLogin$supercellId_release();
            MainActivity a2 = b.b.a.b.a(o.this);
            if (a2 != null) {
                a2.d();
            }
        }
    }

    public final class b implements View.OnClickListener {
        public b() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) o.this.a(R.id.okButton);
            j.a((Object) widthAdjustingMultilineButton, "okButton");
            widthAdjustingMultilineButton.setEnabled(false);
            WidthAdjustingMultilineButton widthAdjustingMultilineButton2 = (WidthAdjustingMultilineButton) o.this.a(R.id.cancelButton);
            j.a((Object) widthAdjustingMultilineButton2, "cancelButton");
            widthAdjustingMultilineButton2.setEnabled(false);
            o oVar = o.this;
            String g = oVar.g();
            String i = oVar.i();
            String obj = ((PinEntryView) oVar.a(R.id.pinEditText)).getText().toString();
            if (!((PinEntryView) oVar.a(R.id.pinEditText)).a()) {
                MainActivity a2 = b.b.a.b.a(oVar);
                if (a2 != null) {
                    a2.a("invalid_pin", (kotlin.d.a.b<? super e, m>) null);
                    return;
                }
                return;
            }
            WeakReference weakReference = new WeakReference(oVar);
            nl.komponents.kovenant.c.m.b(nl.komponents.kovenant.c.m.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().g.c(g, i, obj), new m(weakReference, obj)), new n(weakReference));
        }
    }

    public final class c extends k implements kotlin.d.a.c<PinEntryView, CharSequence, m> {
        public c() {
            super(2);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: b.b.a.j.q1.a(android.support.v7.widget.AppCompatButton, boolean):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, boolean]
         candidates:
          b.b.a.j.q1.a(android.view.View, int):android.view.View
          b.b.a.j.q1.a(android.support.v4.widget.NestedScrollView, int):void
          b.b.a.j.q1.a(android.support.v4.widget.NestedScrollView, android.view.View):void
          b.b.a.j.q1.a(android.view.View, long):void
          b.b.a.j.q1.a(android.view.View, b.b.a.j.z):void
          b.b.a.j.q1.a(android.view.View, kotlin.d.a.a<kotlin.m>):void
          b.b.a.j.q1.a(android.widget.ScrollView, int):void
          b.b.a.j.q1.a(android.widget.ScrollView, android.view.View):void
          b.b.a.j.q1.a(android.support.v7.widget.AppCompatButton, boolean):void */
        public final Object invoke(Object obj, Object obj2) {
            PinEntryView pinEntryView = (PinEntryView) obj;
            j.b(pinEntryView, "pinEditText");
            j.b((CharSequence) obj2, "<anonymous parameter 1>");
            WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) o.this.a(R.id.okButton);
            j.a((Object) widthAdjustingMultilineButton, "okButton");
            q1.a((AppCompatButton) widthAdjustingMultilineButton, !pinEntryView.a());
            return m.f5330a;
        }
    }

    public final View a(int i) {
        if (this.c == null) {
            this.c = new HashMap();
        }
        View view = (View) this.c.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.c.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.c;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void a(v vVar) {
        j.b(vVar, "dialog");
        WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) a(R.id.okButton);
        j.a((Object) widthAdjustingMultilineButton, "okButton");
        widthAdjustingMultilineButton.setEnabled(true);
        WidthAdjustingMultilineButton widthAdjustingMultilineButton2 = (WidthAdjustingMultilineButton) a(R.id.cancelButton);
        j.a((Object) widthAdjustingMultilineButton2, "cancelButton");
        widthAdjustingMultilineButton2.setEnabled(true);
    }

    public final void c() {
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a("Log In Progress Step 2");
    }

    public final void e() {
        k();
        PinEntryView pinEntryView = (PinEntryView) a(R.id.pinEditText);
        if (pinEntryView != null) {
            pinEntryView.setPin("");
        }
    }

    public final void k() {
        TextView textView;
        TextView textView2;
        String g = g();
        if (!(g == null || (textView2 = (TextView) a(R.id.subtitleTextView)) == null)) {
            b.b.a.i.x1.j.a(textView2, "log_in_pin_description", kotlin.k.a("email address", b.b.a.b.a(new SpannableStringBuilder(), n.f1102b.d(g), new ForegroundColorSpan(ResourcesCompat.getColor(getResources(), R.color.text_green, null)), 33)));
        }
        String i = i();
        if (i != null && (textView = (TextView) a(R.id.subtitleTextView)) != null) {
            h[] hVarArr = new h[1];
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
            j.b(i, "number");
            try {
                j.a a2 = g.a().a(i, "ZZ");
                i = 8234 + g.a().a(a2, g.a.INTERNATIONAL) + 8236;
            } catch (NumberParseException unused) {
            }
            hVarArr[0] = kotlin.k.a("phone number", b.b.a.b.a(spannableStringBuilder, i, new ForegroundColorSpan(ResourcesCompat.getColor(getResources(), R.color.text_green, null)), 33));
            b.b.a.i.x1.j.a(textView, "log_in_pin_description_phone", hVarArr);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kotlin.d.b.j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_login_enter_pin_page, viewGroup, false);
    }

    public final void onDestroyView() {
        super.onDestroyView();
        MainActivity a2 = b.b.a.b.a(this);
        if (a2 != null) {
            a2.b(this);
        }
        a();
    }

    public final void onResume() {
        super.onResume();
        PinEntryView pinEntryView = (PinEntryView) a(R.id.pinEditText);
        if (pinEntryView != null) {
            pinEntryView.b();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.j.q1.a(android.support.v7.widget.AppCompatButton, boolean):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, boolean]
     candidates:
      b.b.a.j.q1.a(android.view.View, int):android.view.View
      b.b.a.j.q1.a(android.support.v4.widget.NestedScrollView, int):void
      b.b.a.j.q1.a(android.support.v4.widget.NestedScrollView, android.view.View):void
      b.b.a.j.q1.a(android.view.View, long):void
      b.b.a.j.q1.a(android.view.View, b.b.a.j.z):void
      b.b.a.j.q1.a(android.view.View, kotlin.d.a.a<kotlin.m>):void
      b.b.a.j.q1.a(android.widget.ScrollView, int):void
      b.b.a.j.q1.a(android.widget.ScrollView, android.view.View):void
      b.b.a.j.q1.a(android.support.v7.widget.AppCompatButton, boolean):void */
    public final void onViewCreated(View view, Bundle bundle) {
        kotlin.d.b.j.b(view, "view");
        super.onViewCreated(view, bundle);
        MainActivity a2 = b.b.a.b.a(this);
        if (a2 != null) {
            a2.a(this);
        }
        k();
        ((WidthAdjustingMultilineButton) a(R.id.cancelButton)).setOnClickListener(new a());
        ((WidthAdjustingMultilineButton) a(R.id.okButton)).setOnClickListener(new b());
        WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) a(R.id.okButton);
        kotlin.d.b.j.a((Object) widthAdjustingMultilineButton, "okButton");
        q1.a((AppCompatButton) widthAdjustingMultilineButton, !((PinEntryView) a(R.id.pinEditText)).a());
        ((PinEntryView) a(R.id.pinEditText)).setOnPinChangedListener(new c());
    }
}
