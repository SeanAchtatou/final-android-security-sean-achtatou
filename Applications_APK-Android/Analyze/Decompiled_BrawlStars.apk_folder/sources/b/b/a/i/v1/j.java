package b.b.a.i.v1;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.os.ParcelCompat;
import android.support.v4.view.FlowPager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import b.b.a.h.j;
import b.b.a.i.b;
import b.b.a.i.d0;
import b.b.a.i.e0;
import b.b.a.i.g;
import b.b.a.i.r;
import b.b.a.i.s;
import b.b.a.i.v1.h;
import b.b.a.i.x0;
import b.b.a.i.z;
import b.b.a.j.a0;
import b.b.a.j.k0;
import b.b.a.j.q1;
import b.b.a.j.w0;
import b.b.a.j.y;
import b.b.a.j.y0;
import com.facebook.share.internal.ShareConstants;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.SubPageTabLayout;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import kotlin.NoWhenBranchMatchedException;
import kotlin.a.ah;
import kotlin.a.an;
import kotlin.a.m;
import kotlin.d.b.t;
import nl.komponents.kovenant.ap;
import nl.komponents.kovenant.bb;
import nl.komponents.kovenant.bc;
import nl.komponents.kovenant.bw;

public final class j extends b.b.a.i.g {
    public static final w0 u = new w0("account_friend_profile_games_tab", g.class);
    public static final w0 v = new w0("account_friend_profile_friends_tab", b.class);
    public static final b w = new b(null);
    public NestedScrollView m;
    public RecyclerView n;
    public boolean o;
    public final y0<h> p = new y0<>(new i(this), new C0077j());
    public h q;
    public final ap<String, Exception> r = bb.f5389a;
    public final bw<String, Exception> s = this.r.i();
    public HashMap t;

    public static final class a extends b.a implements a0 {
        public static final Parcelable.Creator<a> CREATOR = new C0076a();
        public final Set<Integer> e;
        public final boolean f;
        public final Class<? extends b.b.a.i.g> g;
        public final String h;
        public final String i;
        public final String j;
        public final String k;
        public final b.b.a.h.j l;
        public final b.b.a.h.g m;
        public final boolean n;

        /* renamed from: b.b.a.i.v1.j$a$a  reason: collision with other inner class name */
        public final class C0076a implements Parcelable.Creator<a> {
            public final a createFromParcel(Parcel parcel) {
                kotlin.d.b.j.b(parcel, ShareConstants.FEED_SOURCE_PARAM);
                kotlin.d.b.j.b(parcel, "parcel");
                return new a(parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), (b.b.a.h.j) parcel.readParcelable(b.b.a.h.j.class.getClassLoader()), (b.b.a.h.g) parcel.readParcelable(b.b.a.h.g.class.getClassLoader()), ParcelCompat.readBoolean(parcel));
            }

            public final a[] newArray(int i) {
                return new a[i];
            }
        }

        public a(String str, String str2, String str3, String str4, b.b.a.h.j jVar, b.b.a.h.g gVar, boolean z) {
            this.h = str;
            this.i = str2;
            this.j = str3;
            this.k = str4;
            this.l = jVar;
            this.m = gVar;
            this.n = z;
            this.e = an.a((Object[]) new Integer[]{Integer.valueOf(R.id.nav_area_back_button), Integer.valueOf(R.id.nav_area_close_button)});
            this.f = true;
            this.g = j.class;
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(String str, String str2, String str3, String str4, b.b.a.h.j jVar, b.b.a.h.g gVar, boolean z, int i2) {
            this(str, str2, str3, str4, jVar, gVar, (i2 & 64) != 0 ? false : z);
        }

        public final int a(int i2, int i3, int i4) {
            return b.b.a.i.a0.u.a(i2, i3, i4);
        }

        public final Class<? extends b.b.a.i.g> a() {
            return this.g;
        }

        public final Class<? extends x0> a(Resources resources) {
            kotlin.d.b.j.b(resources, "resources");
            return b.b.a.b.b(resources) ? z.class : b.b.a.i.a.class;
        }

        public final int b(Resources resources, int i2, int i3, int i4) {
            int i5;
            kotlin.d.b.j.b(resources, "resources");
            kotlin.d.b.j.b(resources, "$this$isSmallScreen");
            if (resources.getBoolean(R.bool.isSmallScreen)) {
                i5 = kotlin.e.a.a(b.b.a.b.a(80));
            } else {
                i5 = kotlin.e.a.a(b.b.a.b.a(150));
            }
            return i3 + i5;
        }

        public final String b() {
            return super.b() + '/' + this.h + '-' + this.i;
        }

        public final int c(Resources resources, int i2, int i3, int i4) {
            kotlin.d.b.j.b(resources, "resources");
            return b.b.a.i.a0.u.b(i2, i3, i4);
        }

        public final Set<Integer> c() {
            return this.e;
        }

        public final boolean c(Resources resources) {
            kotlin.d.b.j.b(resources, "resources");
            return !b.b.a.b.b(resources);
        }

        public final boolean d(Resources resources) {
            kotlin.d.b.j.b(resources, "resources");
            return false;
        }

        public final int describeContents() {
            return 0;
        }

        public final Class<? extends b.b.a.i.g> e(Resources resources) {
            kotlin.d.b.j.b(resources, "resources");
            if (b.b.a.b.b(resources)) {
                return b.b.a.i.a0.class;
            }
            kotlin.d.b.j.b(resources, "$this$isSmallScreen");
            return resources.getBoolean(R.bool.isSmallScreen) ? d0.class : e0.class;
        }

        public final boolean e() {
            return this.f;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
         arg types: [java.lang.String, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
        public final boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (kotlin.d.b.j.a((Object) this.h, (Object) aVar.h) && kotlin.d.b.j.a((Object) this.i, (Object) aVar.i) && kotlin.d.b.j.a((Object) this.j, (Object) aVar.j) && kotlin.d.b.j.a((Object) this.k, (Object) aVar.k) && kotlin.d.b.j.a(this.l, aVar.l) && kotlin.d.b.j.a(this.m, aVar.m)) {
                        if (this.n == aVar.n) {
                            return true;
                        }
                    }
                }
                return false;
            }
            return true;
        }

        public final b.b.a.h.j g() {
            return this.l;
        }

        public final int hashCode() {
            String str = this.h;
            int i2 = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.i;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.j;
            int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
            String str4 = this.k;
            int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
            b.b.a.h.j jVar = this.l;
            int hashCode5 = (hashCode4 + (jVar != null ? jVar.hashCode() : 0)) * 31;
            b.b.a.h.g gVar = this.m;
            if (gVar != null) {
                i2 = gVar.hashCode();
            }
            int i3 = (hashCode5 + i2) * 31;
            boolean z = this.n;
            if (z) {
                z = true;
            }
            return i3 + (z ? 1 : 0);
        }

        public final String toString() {
            StringBuilder a2 = b.a.a.a.a.a("BackStackEntry(profileId=");
            a2.append(this.h);
            a2.append(", scid=");
            a2.append(this.i);
            a2.append(", nickname=");
            a2.append(this.j);
            a2.append(", avatarUrl=");
            a2.append(this.k);
            a2.append(", relationship=");
            a2.append(this.l);
            a2.append(", presence=");
            a2.append(this.m);
            a2.append(", openAddFriendDialog=");
            a2.append(this.n);
            a2.append(")");
            return a2.toString();
        }

        public final void writeToParcel(Parcel parcel, int i2) {
            kotlin.d.b.j.b(parcel, "dest");
            parcel.writeString(this.h);
            parcel.writeString(this.i);
            parcel.writeString(this.j);
            parcel.writeString(this.k);
            parcel.writeParcelable(this.l, i2);
            parcel.writeParcelable(this.m, i2);
            ParcelCompat.writeBoolean(parcel, this.n);
        }
    }

    public static final class b {
        public /* synthetic */ b(kotlin.d.b.g gVar) {
        }

        public final List<w0> a(h hVar) {
            if (hVar == null || hVar.b() || !(hVar.g() instanceof j.a.C0008a)) {
                return m.a(j.u);
            }
            return m.a((Object[]) new w0[]{j.u, j.v});
        }
    }

    public final /* synthetic */ class c extends kotlin.d.b.h implements kotlin.d.a.b<Integer, String> {
        public c(b.b.a.j.x0 x0Var) {
            super(1, x0Var);
        }

        public final String getName() {
            return "getTitleKey";
        }

        public final kotlin.h.d getOwner() {
            return t.a(b.b.a.j.x0.class);
        }

        public final String getSignature() {
            return "getTitleKey(I)Ljava/lang/String;";
        }

        public final Object invoke(Object obj) {
            return ((b.b.a.j.x0) this.receiver).a(((Number) obj).intValue());
        }
    }

    public final class d extends kotlin.d.b.k implements kotlin.d.a.a<kotlin.m> {
        public d() {
            super(0);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.ImageView, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final Object invoke() {
            ImageView imageView = (ImageView) j.this.a(R.id.online_status_indicator);
            if (imageView != null) {
                ImageView imageView2 = (ImageView) j.this.a(R.id.profile_image);
                kotlin.d.b.j.a((Object) imageView2, "profile_image");
                kotlin.d.b.j.b(imageView, "indicator");
                kotlin.d.b.j.b(imageView2, "companion");
                float width = (((float) imageView2.getWidth()) / b.b.a.b.a(1)) / 3.3f;
                if (Float.compare(width, 14.0f) < 0) {
                    width = 14.0f;
                } else if (Float.compare(width, 24.0f) > 0) {
                    width = 24.0f;
                }
                float f = width * b.b.a.b.f16a;
                int width2 = imageView2.getWidth() / 2;
                ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
                if (!(layoutParams instanceof ConstraintLayout.LayoutParams)) {
                    layoutParams = null;
                }
                ConstraintLayout.LayoutParams layoutParams2 = (ConstraintLayout.LayoutParams) layoutParams;
                if (layoutParams2 == null || width2 != layoutParams2.circleRadius) {
                    imageView.post(new y(imageView, layoutParams2, f, width2));
                }
            }
            return kotlin.m.f5330a;
        }
    }

    public final class e extends kotlin.d.b.k implements kotlin.d.a.b<View, kotlin.m> {
        public e() {
            super(1);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: b.b.a.i.g.a(b.b.a.i.g, boolean, int, java.lang.Object):void
         arg types: [b.b.a.i.v1.j, int, int, ?[OBJECT, ARRAY]]
         candidates:
          b.b.a.i.g.a(b.b.a.i.g, android.view.View, int, int):void
          b.b.a.i.g.a(android.view.View, b.b.a.i.g$b, boolean, nl.komponents.kovenant.ap<java.lang.Boolean, java.lang.Exception>):void
          b.b.a.i.g.a(b.b.a.i.g, boolean, int, java.lang.Object):void */
        public final void a(View view) {
            View view2;
            Object obj;
            j.this.m = (NestedScrollView) (!(view instanceof NestedScrollView) ? null : view);
            RecyclerView recyclerView = j.this.n;
            if (recyclerView != null) {
                recyclerView.clearOnScrollListeners();
            }
            j jVar = j.this;
            if (!(view instanceof ViewGroup)) {
                view = null;
            }
            ViewGroup viewGroup = (ViewGroup) view;
            if (viewGroup != null) {
                kotlin.g.c b2 = kotlin.g.d.b(0, viewGroup.getChildCount());
                ArrayList arrayList = new ArrayList();
                Iterator it = b2.iterator();
                while (it.hasNext()) {
                    View childAt = viewGroup.getChildAt(((ah) it).a());
                    if (childAt != null) {
                        arrayList.add(childAt);
                    }
                }
                Iterator it2 = arrayList.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        obj = null;
                        break;
                    }
                    obj = it2.next();
                    if (((View) obj) instanceof RecyclerView) {
                        break;
                    }
                }
                view2 = (View) obj;
            } else {
                view2 = null;
            }
            if (!(view2 instanceof RecyclerView)) {
                view2 = null;
            }
            jVar.n = (RecyclerView) view2;
            b.b.a.i.g.a((b.b.a.i.g) j.this, false, 1, (Object) null);
        }

        public final /* synthetic */ Object invoke(Object obj) {
            a((View) obj);
            return kotlin.m.f5330a;
        }
    }

    public final class f extends kotlin.d.b.k implements kotlin.d.a.c<b.b.a.i.t, s, kotlin.m> {
        public f() {
            super(2);
        }

        public final Object invoke(Object obj, Object obj2) {
            s sVar = (s) obj2;
            kotlin.d.b.j.b((b.b.a.i.t) obj, "<anonymous parameter 0>");
            kotlin.d.b.j.b(sVar, "decision");
            int i = k.f802a[sVar.ordinal()];
            if (i == 1) {
                b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Public Profile - Friend Request Dialog", "click", "Decline", null, false, 24);
                j.c(j.this);
            } else if (i == 2) {
                b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Public Profile - Friend Request Dialog", "click", "Accept", null, false, 24);
                j.a(j.this);
            }
            return kotlin.m.f5330a;
        }
    }

    public final class g extends kotlin.d.b.k implements kotlin.d.a.b<r, kotlin.m> {
        public g() {
            super(1);
        }

        public final Object invoke(Object obj) {
            kotlin.d.b.j.b((r) obj, "it");
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Public Profile - Pending Request Dialog", "click", "Delete", null, false, 24);
            j.b(j.this);
            return kotlin.m.f5330a;
        }
    }

    public final class h extends kotlin.d.b.k implements kotlin.d.a.b<r, kotlin.m> {
        public h() {
            super(1);
        }

        public final Object invoke(Object obj) {
            kotlin.d.b.j.b((r) obj, "it");
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Public Profile - Remove Friend Dialog", "click", "Remove", null, false, 24);
            j.d(j.this);
            return kotlin.m.f5330a;
        }
    }

    public final /* synthetic */ class i extends kotlin.d.b.h implements kotlin.d.a.b<h, kotlin.m> {
        public i(j jVar) {
            super(1, jVar);
        }

        public final String getName() {
            return "updateProfile";
        }

        public final kotlin.h.d getOwner() {
            return t.a(j.class);
        }

        public final String getSignature() {
            return "updateProfile(Lcom/supercell/id/ui/publicprofile/IdPublicProfileData;)V";
        }

        public final Object invoke(Object obj) {
            ((j) this.receiver).a((h) obj);
            return kotlin.m.f5330a;
        }
    }

    /* renamed from: b.b.a.i.v1.j$j  reason: collision with other inner class name */
    public final class C0077j extends kotlin.d.b.k implements kotlin.d.a.b<Exception, kotlin.m> {
        public C0077j() {
            super(1);
        }

        public final Object invoke(Object obj) {
            Exception exc = (Exception) obj;
            kotlin.d.b.j.b(exc, "it");
            MainActivity a2 = b.b.a.b.a((Fragment) j.this);
            if (a2 != null) {
                a2.a(exc, new u(this));
            }
            return kotlin.m.f5330a;
        }
    }

    public final class k implements View.OnClickListener {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ h f799b;

        public k(h hVar) {
            this.f799b = hVar;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            MainActivity a2;
            h hVar = this.f799b;
            if (hVar == null) {
                return;
            }
            if ((hVar.c() != null || this.f799b.f() != null || this.f799b.j() != null) && (a2 = b.b.a.b.a((Fragment) j.this)) != null) {
                j jVar = j.this;
                kotlin.d.b.j.a((Object) view, "it");
                jVar.a(a2, view, this.f799b.h(), this.f799b.c(), this.f799b.f(), this.f799b.j());
            }
        }
    }

    public final class l implements View.OnClickListener {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ h f801b;

        public l(h hVar) {
            this.f801b = hVar;
        }

        public final void onClick(View view) {
            h hVar = this.f801b;
            b.b.a.h.j g = hVar != null ? hVar.g() : null;
            if (g instanceof j.d) {
                b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Public Profile", "click", "Add friend", null, false, 24);
                j.e(j.this);
            } else if (g instanceof j.a.c) {
                b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Public Profile", "click", "Delete pending request", null, false, 24);
                MainActivity a2 = b.b.a.b.a((Fragment) j.this);
                if (a2 != null) {
                    j.this.b(a2);
                }
            } else if (g instanceof j.a.C0008a) {
                b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Public Profile", "click", "Remove friend", null, false, 24);
                MainActivity a3 = b.b.a.b.a((Fragment) j.this);
                if (a3 != null) {
                    j.this.c(a3);
                }
            } else if (g instanceof j.a.b) {
                b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Public Profile", "click", "Respond to friend request", null, false, 24);
                MainActivity a4 = b.b.a.b.a((Fragment) j.this);
                if (a4 != null) {
                    j.this.a(a4);
                }
            }
        }
    }

    public static final /* synthetic */ void a(j jVar) {
        h hVar;
        b.b.a.h.j g2;
        String k2 = jVar.k();
        if (k2 != null && (hVar = jVar.q) != null && (g2 = hVar.g()) != null) {
            y0<h> y0Var = jVar.p;
            bw.a aVar = bw.d;
            h hVar2 = jVar.q;
            y0Var.a(bb.f5389a);
            bw a2 = bc.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().e().a(k2), n.f806a);
            WeakReference weakReference = new WeakReference(jVar);
            nl.komponents.kovenant.c.m.a(a2, new l(weakReference));
            nl.komponents.kovenant.c.m.b(a2, new m(weakReference, g2));
        }
    }

    public static final /* synthetic */ void b(j jVar) {
        h hVar;
        b.b.a.h.j g2;
        String k2 = jVar.k();
        if (k2 != null && (hVar = jVar.q) != null && (g2 = hVar.g()) != null) {
            y0<h> y0Var = jVar.p;
            bw.a aVar = bw.d;
            h hVar2 = jVar.q;
            y0Var.a(bb.f5389a);
            bw<Boolean, Exception> b2 = SupercellId.INSTANCE.getSharedServices$supercellId_release().e().b(k2);
            WeakReference weakReference = new WeakReference(jVar);
            nl.komponents.kovenant.c.m.a(b2, new o(weakReference));
            nl.komponents.kovenant.c.m.b(b2, new p(weakReference, g2));
        }
    }

    public static final /* synthetic */ void c(j jVar) {
        h hVar;
        b.b.a.h.j g2;
        String k2 = jVar.k();
        if (k2 != null && (hVar = jVar.q) != null && (g2 = hVar.g()) != null) {
            y0<h> y0Var = jVar.p;
            bw.a aVar = bw.d;
            h hVar2 = jVar.q;
            y0Var.a(bb.f5389a);
            bw<Boolean, Exception> d2 = SupercellId.INSTANCE.getSharedServices$supercellId_release().e().d(k2);
            WeakReference weakReference = new WeakReference(jVar);
            nl.komponents.kovenant.c.m.a(d2, new q(weakReference));
            nl.komponents.kovenant.c.m.b(d2, new r(weakReference, g2));
        }
    }

    public static final /* synthetic */ void d(j jVar) {
        h hVar;
        b.b.a.h.j g2;
        String k2 = jVar.k();
        if (k2 != null && (hVar = jVar.q) != null && (g2 = hVar.g()) != null) {
            y0<h> y0Var = jVar.p;
            bw.a aVar = bw.d;
            h hVar2 = jVar.q;
            y0Var.a(bb.f5389a);
            bw<Boolean, Exception> e2 = SupercellId.INSTANCE.getSharedServices$supercellId_release().e().e(k2);
            WeakReference weakReference = new WeakReference(jVar);
            nl.komponents.kovenant.c.m.a(e2, new v(weakReference));
            nl.komponents.kovenant.c.m.b(e2, new w(weakReference, g2));
        }
    }

    public static final /* synthetic */ void e(j jVar) {
        h hVar;
        b.b.a.h.j g2;
        String k2 = jVar.k();
        if (k2 != null && (hVar = jVar.q) != null && (g2 = hVar.g()) != null) {
            y0<h> y0Var = jVar.p;
            bw.a aVar = bw.d;
            h hVar2 = jVar.q;
            y0Var.a(bb.f5389a);
            bw<Boolean, Exception> c2 = SupercellId.INSTANCE.getSharedServices$supercellId_release().e().c(k2);
            WeakReference weakReference = new WeakReference(jVar);
            nl.komponents.kovenant.c.m.a(c2, new x(weakReference));
            nl.komponents.kovenant.c.m.b(c2, new y(weakReference, g2));
        }
    }

    public final View a(int i2) {
        if (this.t == null) {
            this.t = new HashMap();
        }
        View view = (View) this.t.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i2);
        this.t.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.t;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public final void a(MainActivity mainActivity, View view, String str, String str2, String str3, String str4) {
        mainActivity.a(i.h.a(q1.b(view), str, str2, str3, str4), "popupDialog");
    }

    public final View b() {
        return (ImageButton) a(R.id.back_button);
    }

    public final NestedScrollView d() {
        return this.m;
    }

    public final RecyclerView e() {
        return this.n;
    }

    public final View f() {
        return (LinearLayout) a(R.id.publicProfileToolbar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.app.FragmentManager, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.List<android.support.v4.app.Fragment>, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final g i() {
        try {
            FragmentManager childFragmentManager = getChildFragmentManager();
            kotlin.d.b.j.a((Object) childFragmentManager, "childFragmentManager");
            List<Fragment> fragments = childFragmentManager.getFragments();
            kotlin.d.b.j.a((Object) fragments, "childFragmentManager.fragments");
            ArrayList arrayList = new ArrayList();
            for (Fragment fragment : fragments) {
                if (!(fragment instanceof g)) {
                    fragment = null;
                }
                g gVar = (g) fragment;
                if (gVar != null) {
                    arrayList.add(gVar);
                }
            }
            return (g) m.d((List) arrayList);
        } catch (IllegalStateException unused) {
            return null;
        }
    }

    public final ViewPager j() {
        return (FlowPager) a(R.id.tabPager);
    }

    public final String k() {
        String h2;
        h hVar = this.q;
        if (hVar != null && (h2 = hVar.h()) != null) {
            return h2;
        }
        a aVar = (a) b.b.a.b.a((b.b.a.i.g) this);
        if (aVar != null) {
            return aVar.i;
        }
        return null;
    }

    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a aVar = (a) b.b.a.b.a((b.b.a.i.g) this);
        this.o = aVar != null ? aVar.n : false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kotlin.d.b.j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_public_profile, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.app.FragmentManager, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        bw<b.b.a.h.i, Exception> bwVar;
        kotlin.d.b.j.b(view, "view");
        super.onViewCreated(view, bundle);
        FragmentManager childFragmentManager = getChildFragmentManager();
        kotlin.d.b.j.a((Object) childFragmentManager, "childFragmentManager");
        b.b.a.j.x0 x0Var = new b.b.a.j.x0(childFragmentManager, w.a(this.q), new e());
        ViewPager j = j();
        if (j != null) {
            j.setAdapter(x0Var);
        }
        SubPageTabLayout subPageTabLayout = (SubPageTabLayout) a(R.id.tabBarView);
        if (subPageTabLayout != null) {
            subPageTabLayout.setGetTitleKey(new c(x0Var));
            subPageTabLayout.setupWithViewPager(j(), true);
        }
        Resources resources = getResources();
        kotlin.d.b.j.a((Object) resources, "resources");
        if (!b.b.a.b.b(resources)) {
            int abs = Math.abs(getResources().getDimensionPixelSize(R.dimen.profile_margin_top));
            ViewGroup.MarginLayoutParams d2 = q1.d(view);
            if (d2 != null) {
                d2.topMargin = -abs;
            }
            view.setPadding(view.getPaddingLeft(), view.getPaddingTop() + abs, view.getPaddingRight(), view.getPaddingBottom());
        }
        h hVar = this.q;
        String str = null;
        if (hVar == null) {
            a aVar = (a) b.b.a.b.a((b.b.a.i.g) this);
            hVar = (aVar == null || aVar.i == null || aVar.g() == null) ? null : new h.a(aVar.i, aVar.j, aVar.k, aVar.g(), aVar.m, false, null, null);
        }
        a(hVar);
        y0<h> y0Var = this.p;
        String k2 = k();
        if (k2 == null || (bwVar = SupercellId.INSTANCE.getSharedServices$supercellId_release().h.c(k2)) == null) {
            b.b.a.d.e eVar = SupercellId.INSTANCE.getSharedServices$supercellId_release().h;
            a aVar2 = (a) b.b.a.b.a((b.b.a.i.g) this);
            if (aVar2 != null) {
                str = aVar2.h;
            }
            if (str == null) {
                str = "";
            }
            bwVar = eVar.d(str);
        }
        y0Var.a(bc.a(bwVar, s.f813a));
        ImageView imageView = (ImageView) a(R.id.profile_image);
        if (imageView != null) {
            q1.a(imageView, new d());
        }
    }

    public final void onViewStateRestored(Bundle bundle) {
        super.onViewStateRestored(bundle);
        List<w0> a2 = w.a(this.q);
        SubPageTabLayout subPageTabLayout = (SubPageTabLayout) a(R.id.tabBarView);
        if (subPageTabLayout != null) {
            subPageTabLayout.setVisibility(a2.size() > 1 ? 0 : 8);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.j.q1.a(android.view.View, long):void
     arg types: [android.widget.FrameLayout, int]
     candidates:
      b.b.a.j.q1.a(android.view.View, int):android.view.View
      b.b.a.j.q1.a(android.support.v4.widget.NestedScrollView, int):void
      b.b.a.j.q1.a(android.support.v4.widget.NestedScrollView, android.view.View):void
      b.b.a.j.q1.a(android.support.v7.widget.AppCompatButton, boolean):void
      b.b.a.j.q1.a(android.view.View, b.b.a.j.z):void
      b.b.a.j.q1.a(android.view.View, kotlin.d.a.a<kotlin.m>):void
      b.b.a.j.q1.a(android.widget.ScrollView, int):void
      b.b.a.j.q1.a(android.widget.ScrollView, android.view.View):void
      b.b.a.j.q1.a(android.view.View, long):void */
    public final void a(View view, g.a aVar, boolean z) {
        kotlin.d.b.j.b(view, "view");
        kotlin.d.b.j.b(aVar, "animation");
        super.a(view, aVar, z);
        Resources resources = getResources();
        kotlin.d.b.j.a((Object) resources, "resources");
        if (b.b.a.b.b(resources)) {
            return;
        }
        if (aVar == g.a.SLIDE_IN && !z) {
            for (View view2 : m.d((FrameLayout) a(R.id.profile_image_container), (ImageView) a(R.id.online_status_indicator), (TextView) a(R.id.online_status_text))) {
                view2.setAlpha(0.0f);
                view2.animate().alpha(1.0f).setStartDelay(175).setDuration(175).setInterpolator(b.b.a.f.a.f62a).start();
            }
        } else if (aVar == g.a.ENTRY) {
            FrameLayout frameLayout = (FrameLayout) a(R.id.profile_image_container);
            if (frameLayout != null) {
                q1.a((View) frameLayout, 300L);
            }
            for (View view3 : m.d((ImageView) a(R.id.online_status_indicator), (TextView) a(R.id.online_status_text))) {
                view3.setAlpha(0.0f);
                view3.animate().alpha(1.0f).setStartDelay(600).setDuration(175).setInterpolator(b.b.a.f.a.f62a).start();
            }
        }
    }

    public final void b(MainActivity mainActivity) {
        r a2 = r.a.a(r.g, "account_friend_profile_dialog_pending_sent_heading", "account_friend_profile_dialog_pending_sent_ok", "account_friend_profile_dialog_pending_sent_cancel", null, false, 24);
        a2.e = new g();
        mainActivity.a(a2, "popupDialog");
    }

    public final void c(MainActivity mainActivity) {
        r a2 = r.a.a(r.g, "account_friend_profile_dialog_remove_heading", "account_friend_profile_dialog_remove_ok", "account_friend_profile_dialog_remove_cancel", null, false, 24);
        a2.e = new h();
        mainActivity.a(a2, "popupDialog");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void a(View view, g.b bVar, boolean z, ap<Boolean, Exception> apVar) {
        kotlin.d.b.j.b(view, "view");
        kotlin.d.b.j.b(bVar, "animation");
        kotlin.d.b.j.b(apVar, "result");
        Resources resources = getResources();
        kotlin.d.b.j.a((Object) resources, "resources");
        if (!b.b.a.b.b(resources) && bVar == g.b.SLIDE_OUT && z) {
            for (View view2 : m.d((FrameLayout) a(R.id.profile_image_container), (ImageView) a(R.id.online_status_indicator), (TextView) a(R.id.online_status_text))) {
                view2.setAlpha(1.0f);
                view2.animate().alpha(0.0f).setStartDelay(0).setDuration(175).setInterpolator(b.b.a.f.a.f62a).start();
            }
        }
        super.a(view, bVar, z, apVar);
    }

    public final void a(MainActivity mainActivity) {
        b.b.a.i.t a2 = b.b.a.i.t.g.a("account_friend_profile_dialog_pending_received_heading", "account_friend_profile_dialog_pending_received_ok", "account_friend_profile_dialog_pending_received_cancel", null);
        a2.e = new f();
        mainActivity.a(a2, "popupDialog");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.FrameLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.i.x1.j.a(android.widget.ImageView, java.lang.String, boolean, int):void
     arg types: [android.widget.ImageView, java.lang.String, int, int]
     candidates:
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, java.lang.Object, int):void
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, kotlin.d.a.b, int):void
      b.b.a.i.x1.j.a(android.widget.ImageView, java.lang.String, boolean, int):void */
    public final void a(h hVar) {
        String h2;
        MainActivity a2;
        String str;
        b.b.a.h.j g2;
        b.b.a.h.j g3;
        b.b.a.h.j g4;
        int i2;
        int i3;
        Context context;
        h hVar2 = hVar;
        if (getView() != null) {
            List<w0> a3 = w.a(this.q);
            this.q = hVar2;
            int i4 = 8;
            FrameLayout frameLayout = (FrameLayout) a(R.id.tab_container);
            if (hVar2 != null) {
                if (frameLayout != null) {
                    frameLayout.setVisibility(0);
                }
                View a4 = a(R.id.profile_container_shadow);
                if (a4 != null) {
                    a4.setVisibility(0);
                }
                View a5 = a(R.id.profile_container_background);
                if (a5 != null) {
                    a5.setVisibility(0);
                }
                ConstraintLayout constraintLayout = (ConstraintLayout) a(R.id.profile_container);
                if (constraintLayout != null) {
                    constraintLayout.setVisibility(0);
                }
                RelativeLayout relativeLayout = (RelativeLayout) a(R.id.public_profile_content);
                if (relativeLayout != null) {
                    relativeLayout.setVisibility(0);
                }
                FrameLayout frameLayout2 = (FrameLayout) a(R.id.public_profile_progress_bar);
                kotlin.d.b.j.a((Object) frameLayout2, "public_profile_progress_bar");
                frameLayout2.setVisibility(8);
            } else {
                if (frameLayout != null) {
                    frameLayout.setVisibility(8);
                }
                View a6 = a(R.id.profile_container_shadow);
                if (a6 != null) {
                    a6.setVisibility(8);
                }
                View a7 = a(R.id.profile_container_background);
                if (a7 != null) {
                    a7.setVisibility(8);
                }
                ConstraintLayout constraintLayout2 = (ConstraintLayout) a(R.id.profile_container);
                if (constraintLayout2 != null) {
                    constraintLayout2.setVisibility(8);
                }
                RelativeLayout relativeLayout2 = (RelativeLayout) a(R.id.public_profile_content);
                if (relativeLayout2 != null) {
                    relativeLayout2.setVisibility(8);
                }
                FrameLayout frameLayout3 = (FrameLayout) a(R.id.public_profile_progress_bar);
                kotlin.d.b.j.a((Object) frameLayout3, "public_profile_progress_bar");
                frameLayout3.setVisibility(0);
            }
            boolean z = (hVar2 != null ? hVar.h() : null) != null && hVar.c() == null;
            TextView textView = (TextView) a(R.id.profile_name);
            if (textView != null) {
                if (z) {
                    textView.setText(hVar2 != null ? hVar.i() : null);
                    context = textView.getContext();
                    i3 = R.color.gray40;
                } else {
                    textView.setText(hVar2 != null ? hVar.c() : null);
                    context = textView.getContext();
                    i3 = R.color.black;
                }
                textView.setTextColor(ContextCompat.getColor(context, i3));
            }
            TextView textView2 = (TextView) a(R.id.profile_tag);
            if (textView2 != null) {
                if (z) {
                    b.b.a.i.x1.j.a(textView2, "account_friend_not_updated", (kotlin.d.a.b) null, 2);
                } else {
                    b.b.a.i.x1.j.a(textView2);
                    textView2.setText(hVar2 != null ? hVar.i() : null);
                }
            }
            k0 k0Var = k0.f1078b;
            String a8 = hVar2 != null ? hVar.a() : null;
            Resources resources = getResources();
            kotlin.d.b.j.a((Object) resources, "resources");
            k0Var.a(a8, (ImageView) a(R.id.profile_image), resources);
            ImageView imageView = (ImageView) a(R.id.online_status_indicator);
            if (imageView != null) {
                imageView.setEnabled(hVar2 != null && hVar.d());
            }
            ImageView imageView2 = (ImageView) a(R.id.online_status_indicator);
            if (imageView2 != null) {
                imageView2.setVisibility((hVar2 != null ? hVar.g() : null) instanceof j.a.C0008a ? 0 : 8);
            }
            TextView textView3 = (TextView) a(R.id.online_status_text);
            if (textView3 != null) {
                textView3.setVisibility((!((hVar2 != null ? hVar.g() : null) instanceof j.a.C0008a) || !hVar.d()) ? 8 : 0);
            }
            TextView textView4 = (TextView) a(R.id.profile_name);
            if (textView4 != null) {
                textView4.setOnClickListener(new k(hVar2));
            }
            FrameLayout frameLayout4 = (FrameLayout) a(R.id.profile_status_button);
            if (frameLayout4 != null) {
                frameLayout4.setOnClickListener(new l(hVar2));
            }
            ImageView imageView3 = (ImageView) a(R.id.profile_status_indicator);
            if (!(imageView3 == null || hVar2 == null || (g4 = hVar.g()) == null)) {
                if (g4 instanceof j.d) {
                    i2 = R.drawable.ic_friend_status_add;
                } else if (g4 instanceof j.a.c) {
                    i2 = R.drawable.ic_friend_status_pending;
                } else if (g4 instanceof j.a.C0008a) {
                    i2 = R.drawable.ic_friend_status;
                } else if (g4 instanceof j.a.b) {
                    i2 = R.drawable.ic_friend_status_received;
                } else {
                    throw new NoWhenBranchMatchedException();
                }
                imageView3.setImageDrawable(ContextCompat.getDrawable(imageView3.getContext(), i2));
                ViewCompat.setBackground(imageView3, ContextCompat.getDrawable(imageView3.getContext(), g4 instanceof j.a.b ? R.drawable.friend_status_received_indicator_background : R.drawable.friend_status_indicator_background));
            }
            ImageView imageView4 = (ImageView) a(R.id.profile_status_image);
            if (imageView4 != null) {
                String str2 = (hVar2 == null || (g3 = hVar.g()) == null) ? null : g3 instanceof j.a.C0008a ? "friend_face_icon.png" : "friend_face_icon_grayscale.png";
                if (str2 != null) {
                    b.b.a.i.x1.j.a(imageView4, str2, false, 2);
                } else {
                    imageView4.setImageDrawable(null);
                }
            }
            TextView textView5 = (TextView) a(R.id.profile_status_text);
            if (textView5 != null) {
                if (hVar2 == null || (g2 = hVar.g()) == null) {
                    str = null;
                } else if (g2 instanceof j.d) {
                    str = "account_friend_profile_status_add";
                } else if (g2 instanceof j.a.c) {
                    str = "account_friend_profile_status_pending_invite_sent";
                } else if (g2 instanceof j.a.C0008a) {
                    str = "account_friend_profile_status_friend";
                } else if (g2 instanceof j.a.b) {
                    str = "account_friend_profile_status_pending_invite_received";
                } else {
                    throw new NoWhenBranchMatchedException();
                }
                if (str != null) {
                    b.b.a.i.x1.j.a(textView5, str, (kotlin.d.a.b) null, 2);
                } else {
                    textView5.setText((CharSequence) null);
                }
            }
            List<w0> a9 = w.a(hVar2);
            if (!kotlin.d.b.j.a(a3, a9)) {
                SubPageTabLayout subPageTabLayout = (SubPageTabLayout) a(R.id.tabBarView);
                if (subPageTabLayout != null) {
                    if (a9.size() > 1) {
                        i4 = 0;
                    }
                    subPageTabLayout.setVisibility(i4);
                }
                ViewPager j = j();
                if (j != null) {
                    PagerAdapter adapter = j.getAdapter();
                    if (!(adapter instanceof b.b.a.j.x0)) {
                        adapter = null;
                    }
                    b.b.a.j.x0 x0Var = (b.b.a.j.x0) adapter;
                    if (x0Var != null) {
                        kotlin.d.b.j.b(a9, "value");
                        x0Var.f1192a = a9;
                        x0Var.notifyDataSetChanged();
                    }
                    j.setCurrentItem(0);
                }
            }
            g i5 = i();
            if (i5 != null) {
                i5.a(hVar2);
            }
            if (this.o && hVar2 != null) {
                this.o = false;
                b.b.a.h.j g5 = hVar.g();
                if (kotlin.d.b.j.a(g5, j.d.f126b)) {
                    MainActivity a10 = b.b.a.b.a((Fragment) this);
                    if (a10 != null) {
                        String c2 = hVar.c();
                        if (c2 == null) {
                            c2 = hVar.i();
                        }
                        if (c2 == null) {
                            c2 = "";
                        }
                        r a11 = r.a.a(r.g, "account_friend_profile_dialog_send_request_heading", "account_friend_profile_dialog_send_request_ok", "account_friend_profile_dialog_send_request_cancel", kotlin.k.a("name", c2), false, 16);
                        a11.e = new t(this);
                        a10.a(a11, "popupDialog");
                    }
                } else if ((g5 instanceof j.a.b) && (a2 = b.b.a.b.a((Fragment) this)) != null) {
                    a(a2);
                }
            }
            if (!this.s.d() && hVar2 != null && (h2 = hVar.h()) != null) {
                this.r.e(h2);
            }
        }
    }
}
