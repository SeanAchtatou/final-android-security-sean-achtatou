package b.b.a.i.s1;

import java.lang.ref.WeakReference;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.d.b.k;

public final class m extends k implements b<b.b.a.h.m, kotlin.m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f682a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m(WeakReference weakReference) {
        super(1);
        this.f682a = weakReference;
    }

    public final Object invoke(Object obj) {
        b.b.a.h.m mVar = (b.b.a.h.m) obj;
        j.b(mVar, "response");
        p pVar = (p) this.f682a.get();
        if (pVar != null) {
            pVar.f685b = mVar;
        }
        return kotlin.m.f5330a;
    }
}
