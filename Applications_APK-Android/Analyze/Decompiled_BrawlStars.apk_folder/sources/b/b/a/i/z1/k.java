package b.b.a.i.z1;

import java.lang.ref.WeakReference;
import kotlin.d.a.a;
import kotlin.m;

public final class k extends kotlin.d.b.k implements a<m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f1001a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k(WeakReference weakReference) {
        super(0);
        this.f1001a = weakReference;
    }

    public final Object invoke() {
        l lVar = (l) this.f1001a.get();
        if (lVar != null) {
            lVar.f();
        }
        return m.f5330a;
    }
}
