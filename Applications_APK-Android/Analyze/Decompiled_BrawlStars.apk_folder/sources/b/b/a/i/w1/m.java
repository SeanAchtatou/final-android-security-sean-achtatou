package b.b.a.i.w1;

import b.b.a.i.e;
import com.supercell.id.ui.MainActivity;
import java.lang.ref.WeakReference;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.d.b.k;

public final class m extends k implements b<Exception, kotlin.m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f887a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m(WeakReference weakReference) {
        super(1);
        this.f887a = weakReference;
    }

    public final Object invoke(Object obj) {
        MainActivity a2;
        Exception exc = (Exception) obj;
        j.b(exc, "it");
        k kVar = (k) this.f887a.get();
        if (!(kVar == null || (a2 = b.b.a.b.a(kVar)) == null)) {
            a2.a(exc, (b<? super e, kotlin.m>) null);
        }
        return kotlin.m.f5330a;
    }
}
