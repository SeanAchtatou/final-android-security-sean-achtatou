package b.b.a.i.v1;

import b.b.a.i.v1.h;
import java.lang.ref.WeakReference;
import kotlin.d.a.b;
import kotlin.d.b.k;
import kotlin.m;
import nl.komponents.kovenant.bb;

public final class l extends k implements b<h.b, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f803a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l(WeakReference weakReference) {
        super(1);
        this.f803a = weakReference;
    }

    public final Object invoke(Object obj) {
        Object obj2 = this.f803a.get();
        if (obj2 != null) {
            ((j) obj2).p.a(bb.f5389a);
        }
        return m.f5330a;
    }
}
