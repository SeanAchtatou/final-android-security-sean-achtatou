package b.b.a.i;

import b.b.a.i.g;

public final /* synthetic */ class h {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ int[] f278a = new int[g.a.values().length];

    /* renamed from: b  reason: collision with root package name */
    public static final /* synthetic */ int[] f279b = new int[g.b.values().length];

    static {
        f278a[g.a.SLIDE_IN.ordinal()] = 1;
        f278a[g.a.FADE_IN.ordinal()] = 2;
        f278a[g.a.NONE.ordinal()] = 3;
        f278a[g.a.ENTRY.ordinal()] = 4;
        f278a[g.a.PAGE_CHANGED.ordinal()] = 5;
        f279b[g.b.SLIDE_OUT.ordinal()] = 1;
        f279b[g.b.FADE_OUT.ordinal()] = 2;
        f279b[g.b.EXIT.ordinal()] = 3;
    }
}
