package b.b.a.i.v1;

import b.b.a.h.g;
import b.b.a.h.j;
import b.b.a.h.n;
import b.b.a.j.r;
import java.util.List;
import kotlin.NoWhenBranchMatchedException;

public abstract class h {

    public static final class a extends h {

        /* renamed from: a  reason: collision with root package name */
        public final String f778a;

        /* renamed from: b  reason: collision with root package name */
        public final String f779b;
        public final String c;
        public final j d;
        public final g e;
        public final boolean f;
        public final String g;
        public final String h;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(String str, String str2, String str3, j jVar, g gVar, boolean z, String str4, String str5) {
            super(null);
            kotlin.d.b.j.b(str, "scid");
            kotlin.d.b.j.b(jVar, "relationship");
            this.f778a = str;
            this.f779b = str2;
            this.c = str3;
            this.d = jVar;
            this.e = gVar;
            this.f = z;
            this.g = str4;
            this.h = str5;
        }

        public final a a(String str, String str2, String str3, j jVar, g gVar, boolean z, String str4, String str5) {
            kotlin.d.b.j.b(str, "scid");
            kotlin.d.b.j.b(jVar, "relationship");
            return new a(str, str2, str3, jVar, gVar, z, str4, str5);
        }

        public final String a() {
            return this.c;
        }

        public final boolean b() {
            return this.f;
        }

        public final String c() {
            return this.f779b;
        }

        public final g e() {
            return this.e;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
         arg types: [java.lang.String, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
        public final boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (kotlin.d.b.j.a((Object) this.f778a, (Object) aVar.f778a) && kotlin.d.b.j.a((Object) this.f779b, (Object) aVar.f779b) && kotlin.d.b.j.a((Object) this.c, (Object) aVar.c) && kotlin.d.b.j.a(this.d, aVar.d) && kotlin.d.b.j.a(this.e, aVar.e)) {
                        if (!(this.f == aVar.f) || !kotlin.d.b.j.a((Object) this.g, (Object) aVar.g) || !kotlin.d.b.j.a((Object) this.h, (Object) aVar.h)) {
                            return false;
                        }
                    }
                }
                return false;
            }
            return true;
        }

        public final String f() {
            return this.h;
        }

        public final j g() {
            return this.d;
        }

        public final String h() {
            return this.f778a;
        }

        public final int hashCode() {
            String str = this.f778a;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.f779b;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.c;
            int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
            j jVar = this.d;
            int hashCode4 = (hashCode3 + (jVar != null ? jVar.hashCode() : 0)) * 31;
            g gVar = this.e;
            int hashCode5 = (hashCode4 + (gVar != null ? gVar.hashCode() : 0)) * 31;
            boolean z = this.f;
            if (z) {
                z = true;
            }
            int i2 = (hashCode5 + (z ? 1 : 0)) * 31;
            String str4 = this.g;
            int hashCode6 = (i2 + (str4 != null ? str4.hashCode() : 0)) * 31;
            String str5 = this.h;
            if (str5 != null) {
                i = str5.hashCode();
            }
            return hashCode6 + i;
        }

        public final String j() {
            return this.g;
        }

        public final String toString() {
            StringBuilder a2 = b.a.a.a.a.a("FromEntry(scid=");
            a2.append(this.f778a);
            a2.append(", name=");
            a2.append(this.f779b);
            a2.append(", avatarImage=");
            a2.append(this.c);
            a2.append(", relationship=");
            a2.append(this.d);
            a2.append(", presence=");
            a2.append(this.e);
            a2.append(", localChange=");
            a2.append(this.f);
            a2.append(", universalLink=");
            a2.append(this.g);
            a2.append(", qrCodeUrl=");
            return b.a.a.a.a.a(a2, this.h, ")");
        }
    }

    public static final class b extends h {

        /* renamed from: a  reason: collision with root package name */
        public final String f780a;

        /* renamed from: b  reason: collision with root package name */
        public final String f781b;
        public final String c;
        public final j d;
        public final List<n> e;
        public final List<b.b.a.h.b> f;
        public final g g;
        public final boolean h;
        public final String i;
        public final String j;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(String str, String str2, String str3, j jVar, List<n> list, List<b.b.a.h.b> list2, g gVar, boolean z, String str4, String str5) {
            super(null);
            kotlin.d.b.j.b(str, "scid");
            kotlin.d.b.j.b(jVar, "relationship");
            kotlin.d.b.j.b(list, "availableSystems");
            kotlin.d.b.j.b(list2, "connectedSystems");
            this.f780a = str;
            this.f781b = str2;
            this.c = str3;
            this.d = jVar;
            this.e = list;
            this.f = list2;
            this.g = gVar;
            this.h = z;
            this.i = str4;
            this.j = str5;
        }

        public final b a(String str, String str2, String str3, j jVar, List<n> list, List<b.b.a.h.b> list2, g gVar, boolean z, String str4, String str5) {
            kotlin.d.b.j.b(str, "scid");
            j jVar2 = jVar;
            kotlin.d.b.j.b(jVar2, "relationship");
            List<n> list3 = list;
            kotlin.d.b.j.b(list3, "availableSystems");
            List<b.b.a.h.b> list4 = list2;
            kotlin.d.b.j.b(list4, "connectedSystems");
            return new b(str, str2, str3, jVar2, list3, list4, gVar, z, str4, str5);
        }

        public final String a() {
            return this.c;
        }

        public final boolean b() {
            return this.h;
        }

        public final String c() {
            return this.f781b;
        }

        public final g e() {
            return this.g;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
         arg types: [java.lang.String, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
        public final boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof b) {
                    b bVar = (b) obj;
                    if (kotlin.d.b.j.a((Object) this.f780a, (Object) bVar.f780a) && kotlin.d.b.j.a((Object) this.f781b, (Object) bVar.f781b) && kotlin.d.b.j.a((Object) this.c, (Object) bVar.c) && kotlin.d.b.j.a(this.d, bVar.d) && kotlin.d.b.j.a(this.e, bVar.e) && kotlin.d.b.j.a(this.f, bVar.f) && kotlin.d.b.j.a(this.g, bVar.g)) {
                        if (!(this.h == bVar.h) || !kotlin.d.b.j.a((Object) this.i, (Object) bVar.i) || !kotlin.d.b.j.a((Object) this.j, (Object) bVar.j)) {
                            return false;
                        }
                    }
                }
                return false;
            }
            return true;
        }

        public final String f() {
            return this.j;
        }

        public final j g() {
            return this.d;
        }

        public final String h() {
            return this.f780a;
        }

        public final int hashCode() {
            String str = this.f780a;
            int i2 = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.f781b;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.c;
            int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
            j jVar = this.d;
            int hashCode4 = (hashCode3 + (jVar != null ? jVar.hashCode() : 0)) * 31;
            List<n> list = this.e;
            int hashCode5 = (hashCode4 + (list != null ? list.hashCode() : 0)) * 31;
            List<b.b.a.h.b> list2 = this.f;
            int hashCode6 = (hashCode5 + (list2 != null ? list2.hashCode() : 0)) * 31;
            g gVar = this.g;
            int hashCode7 = (hashCode6 + (gVar != null ? gVar.hashCode() : 0)) * 31;
            boolean z = this.h;
            if (z) {
                z = true;
            }
            int i3 = (hashCode7 + (z ? 1 : 0)) * 31;
            String str4 = this.i;
            int hashCode8 = (i3 + (str4 != null ? str4.hashCode() : 0)) * 31;
            String str5 = this.j;
            if (str5 != null) {
                i2 = str5.hashCode();
            }
            return hashCode8 + i2;
        }

        public final String j() {
            return this.i;
        }

        public final String toString() {
            StringBuilder a2 = b.a.a.a.a.a("FromServer(scid=");
            a2.append(this.f780a);
            a2.append(", name=");
            a2.append(this.f781b);
            a2.append(", avatarImage=");
            a2.append(this.c);
            a2.append(", relationship=");
            a2.append(this.d);
            a2.append(", availableSystems=");
            a2.append(this.e);
            a2.append(", connectedSystems=");
            a2.append(this.f);
            a2.append(", presence=");
            a2.append(this.g);
            a2.append(", localChange=");
            a2.append(this.h);
            a2.append(", universalLink=");
            a2.append(this.i);
            a2.append(", qrCodeUrl=");
            return b.a.a.a.a.a(a2, this.j, ")");
        }
    }

    public h() {
    }

    public /* synthetic */ h(kotlin.d.b.g gVar) {
    }

    public static /* synthetic */ h a(h hVar, j jVar, boolean z, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                jVar = hVar.g();
            }
            return hVar.a(jVar, z);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: copyWith");
    }

    public final h a(j jVar, boolean z) {
        kotlin.d.b.j.b(jVar, "relationship");
        if (this instanceof a) {
            a aVar = (a) this;
            return aVar.a(aVar.f778a, aVar.f779b, aVar.c, jVar, aVar.e, z, aVar.g, aVar.h);
        } else if (this instanceof b) {
            b bVar = (b) this;
            return bVar.a(bVar.f780a, bVar.f781b, bVar.c, jVar, bVar.e, bVar.f, bVar.g, z, bVar.i, bVar.j);
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    public abstract String a();

    public abstract boolean b();

    public abstract String c();

    public final boolean d() {
        return e() != null;
    }

    public abstract g e();

    public abstract String f();

    public abstract j g();

    public abstract String h();

    public final String i() {
        return r.f1160a.a(h());
    }

    public abstract String j();
}
