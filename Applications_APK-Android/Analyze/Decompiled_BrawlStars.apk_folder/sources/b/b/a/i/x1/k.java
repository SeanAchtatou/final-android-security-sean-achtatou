package b.b.a.i.x1;

import android.text.SpannableStringBuilder;
import b.b.a.i.x1.j;
import kotlin.d.a.c;
import kotlin.m;

public final class k extends kotlin.d.b.k implements c<String, SpannableStringBuilder, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ j.c f957a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k(j.c cVar) {
        super(2);
        this.f957a = cVar;
    }

    public final Object invoke(Object obj, Object obj2) {
        String str = (String) obj;
        SpannableStringBuilder spannableStringBuilder = (SpannableStringBuilder) obj2;
        kotlin.d.b.j.b(str, "text");
        kotlin.d.b.j.b(spannableStringBuilder, "result");
        CharSequence charSequence = (CharSequence) this.f957a.f952a.get(str);
        if (charSequence != null) {
            spannableStringBuilder.append(charSequence);
        }
        return m.f5330a;
    }
}
