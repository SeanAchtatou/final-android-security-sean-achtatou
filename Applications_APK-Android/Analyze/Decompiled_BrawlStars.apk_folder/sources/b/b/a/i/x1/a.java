package b.b.a.i.x1;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import b.b.a.i.x1.f;
import com.facebook.share.internal.ShareConstants;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import kotlin.a.an;
import kotlin.d.b.g;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;
import nl.komponents.kovenant.bb;

public abstract class a<T> {
    public static final C0091a d = new C0091a(null);

    /* renamed from: a  reason: collision with root package name */
    public Map<String, Set<kotlin.d.a.c<T, c, m>>> f897a = new LinkedHashMap();

    /* renamed from: b  reason: collision with root package name */
    public final ConcurrentHashMap<String, T> f898b = new ConcurrentHashMap<>();
    public final Context c;

    /* renamed from: b.b.a.i.x1.a$a  reason: collision with other inner class name */
    public static final class C0091a {
        public /* synthetic */ C0091a(g gVar) {
        }

        public final File a(Context context) {
            j.b(context, "context");
            File noBackupFilesDir = ContextCompat.getNoBackupFilesDir(context);
            if (noBackupFilesDir != null) {
                return new File(noBackupFilesDir, "supercellid");
            }
            return null;
        }
    }

    public final class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ Object f899a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ a f900b;
        public final /* synthetic */ Object c;

        public b(Object obj, a aVar, String str, Object obj2) {
            this.f899a = obj;
            this.f900b = aVar;
            this.c = obj2;
        }

        public final void run() {
            this.f900b.getClass().getSimpleName();
            ((kotlin.d.a.c) this.f899a).invoke(this.c, c.EXTERNAL);
        }
    }

    public final class c extends k implements kotlin.d.a.a<m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ kotlin.d.a.c f901a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ Object f902b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(kotlin.d.a.c cVar, Object obj) {
            super(0);
            this.f901a = cVar;
            this.f902b = obj;
        }

        public final Object invoke() {
            this.f901a.invoke(this.f902b, c.CACHE);
            return m.f5330a;
        }
    }

    public final class d extends k implements kotlin.d.a.a<T> {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ String f904b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(String str) {
            super(0);
            this.f904b = str;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:13:0x002e, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x002f, code lost:
            kotlin.io.b.a(r4, r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0032, code lost:
            throw r1;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final T invoke() {
            /*
                r5 = this;
                b.b.a.i.x1.a r0 = b.b.a.i.x1.a.this
                java.util.concurrent.ConcurrentHashMap<java.lang.String, T> r0 = r0.f898b
                java.lang.String r1 = r5.f904b
                java.lang.Object r0 = r0.get(r1)
                r1 = 0
                if (r0 == 0) goto L_0x000e
                goto L_0x0041
            L_0x000e:
                b.b.a.i.x1.a r0 = b.b.a.i.x1.a.this
                java.lang.String r2 = r5.f904b
                java.io.File r3 = r0.a(r2)
                if (r3 == 0) goto L_0x0033
                boolean r4 = r3.isFile()
                if (r4 != 0) goto L_0x001f
                goto L_0x0033
            L_0x001f:
                java.io.FileInputStream r4 = new java.io.FileInputStream
                r4.<init>(r3)
                java.lang.Object r0 = r0.a(r2, r4)     // Catch:{ all -> 0x002c }
                kotlin.io.b.a(r4, r1)
                goto L_0x0034
            L_0x002c:
                r0 = move-exception
                throw r0     // Catch:{ all -> 0x002e }
            L_0x002e:
                r1 = move-exception
                kotlin.io.b.a(r4, r0)
                throw r1
            L_0x0033:
                r0 = r1
            L_0x0034:
                if (r0 == 0) goto L_0x0040
                b.b.a.i.x1.a r1 = b.b.a.i.x1.a.this
                java.util.concurrent.ConcurrentHashMap<java.lang.String, T> r1 = r1.f898b
                java.lang.String r2 = r5.f904b
                r1.putIfAbsent(r2, r0)
                goto L_0x0041
            L_0x0040:
                r0 = r1
            L_0x0041:
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: b.b.a.i.x1.a.d.invoke():java.lang.Object");
        }
    }

    public final class e extends k implements kotlin.d.a.b<T, m> {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ String f906b;
        public final /* synthetic */ kotlin.d.a.c c;
        public final /* synthetic */ kotlin.d.a.b d;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(String str, kotlin.d.a.c cVar, kotlin.d.a.b bVar) {
            super(1);
            this.f906b = str;
            this.c = cVar;
            this.d = bVar;
        }

        public final Object invoke(Object obj) {
            if (obj != null) {
                a.this.getClass().getSimpleName();
                "Asset callback from disk fetch " + this.f906b;
                b.b.a.b.a(new b(this, obj));
            } else {
                a.this.a(this.f906b, this.c);
                kotlin.d.a.b bVar = this.d;
                if (bVar != null) {
                    bVar.invoke(c.PERSISTENT_STORAGE);
                }
            }
            return m.f5330a;
        }
    }

    public a(Context context) {
        j.b(context, "context");
        this.c = context;
    }

    public static /* synthetic */ void a(a aVar, String str, kotlin.d.a.c cVar, kotlin.d.a.b bVar, int i, Object obj) {
        if (obj == null) {
            if ((i & 4) != 0) {
                bVar = null;
            }
            aVar.a(str, cVar, bVar);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getItem");
    }

    public final File a(String str) {
        File a2 = d.a(this.c);
        if (a2 == null) {
            return null;
        }
        return new File(a2, ((f.c) this).e + '/' + str);
    }

    public abstract T a(String str, InputStream inputStream);

    public final Set<String> a() {
        Set<String> keySet;
        synchronized (this.f897a) {
            keySet = this.f897a.keySet();
        }
        return keySet;
    }

    public final void a(String str, Drawable drawable) {
        synchronized (this.f897a) {
            Set<kotlin.d.a.c> set = this.f897a.get(str);
            if (!(set == null || set.isEmpty())) {
                Looper mainLooper = Looper.getMainLooper();
                if (j.a(Looper.myLooper(), mainLooper)) {
                    for (kotlin.d.a.c invoke : set) {
                        getClass().getSimpleName();
                        invoke.invoke(drawable, c.EXTERNAL);
                    }
                } else {
                    Handler handler = new Handler(mainLooper);
                    for (Object bVar : set) {
                        handler.post(new b(bVar, this, str, drawable));
                    }
                }
                set.clear();
                m mVar = m.f5330a;
            }
        }
    }

    public final void a(String str, kotlin.d.a.c<? super Drawable, ? super c, m> cVar) {
        synchronized (this.f897a) {
            if (this.f897a.containsKey(str)) {
                Set<kotlin.d.a.c<T, c, m>> set = this.f897a.get(str);
                if (set == null) {
                    j.a();
                }
                Boolean.valueOf(set.add(cVar));
            } else {
                this.f897a.put(str, an.b(cVar));
                m mVar = m.f5330a;
            }
        }
    }

    public final void a(String str, kotlin.d.a.c<? super T, ? super c, m> cVar, kotlin.d.a.b<? super c, m> bVar) {
        j.b(str, "assetName");
        j.b(cVar, "receiver");
        T t = this.f898b.get(str);
        if (t != null) {
            getClass().getSimpleName();
            b.b.a.b.a(new c(cVar, t));
            return;
        }
        if (bVar != null) {
            bVar.invoke(c.CACHE);
        }
        bb.f5389a.a(new e(str, cVar, bVar));
    }

    public final boolean b(String str) {
        boolean containsKey;
        synchronized (this.f897a) {
            containsKey = this.f897a.containsKey(str);
        }
        return containsKey;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.io.File, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final boolean b(String str, byte[] bArr) {
        File a2 = a(str);
        if (a2 == null) {
            return false;
        }
        File parentFile = a2.getParentFile();
        j.a((Object) parentFile, "directory");
        if (!parentFile.isDirectory() && !parentFile.mkdirs()) {
            return false;
        }
        try {
            kotlin.io.f.a(a2, bArr);
            return true;
        } catch (IOException e2) {
            a2.delete();
            throw e2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.i.x1.a.a(java.lang.String, android.graphics.drawable.Drawable):void
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      b.b.a.i.x1.a.a(java.lang.String, java.io.InputStream):T
      b.b.a.i.x1.a.a(java.lang.String, kotlin.d.a.c<? super android.graphics.drawable.Drawable, ? super b.b.a.i.x1.c, kotlin.m>):void
      b.b.a.i.x1.a.a(java.lang.String, byte[]):boolean
      b.b.a.i.x1.a.a(java.lang.String, android.graphics.drawable.Drawable):void */
    public final boolean a(String str, byte[] bArr) {
        j.b(str, "assetName");
        j.b(bArr, ShareConstants.WEB_DIALOG_PARAM_DATA);
        try {
            return b(str, bArr);
        } finally {
            if (b(str)) {
                Object a2 = a(str, new ByteArrayInputStream(bArr));
                this.f898b.put(str, a2);
                a(str, (Drawable) a2);
            }
        }
    }
}
