package b.b.a.i.s1;

import b.b.a.h.m;
import com.supercell.id.SupercellId;
import kotlin.d.a.a;
import kotlin.d.b.k;
import nl.komponents.kovenant.bw;

public final class s extends k implements a<bw<? extends m, ? extends Exception>> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ boolean f702a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public s(boolean z) {
        super(0);
        this.f702a = z;
    }

    public final Object invoke() {
        return SupercellId.INSTANCE.getSharedServices$supercellId_release().g.a(this.f702a);
    }
}
