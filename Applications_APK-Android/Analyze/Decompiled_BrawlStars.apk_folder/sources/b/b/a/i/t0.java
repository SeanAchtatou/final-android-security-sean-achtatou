package b.b.a.i;

import android.animation.ValueAnimator;
import kotlin.TypeCastException;

public final class t0 implements ValueAnimator.AnimatorUpdateListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ ValueAnimator f705a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ u0 f706b;

    public t0(ValueAnimator valueAnimator, u0 u0Var) {
        this.f705a = valueAnimator;
        this.f706b = u0Var;
    }

    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        x0 x0Var = this.f706b.f727b;
        Object animatedValue = this.f705a.getAnimatedValue();
        if (animatedValue != null) {
            x0Var.a(((Float) animatedValue).floatValue(), this.f706b.c);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Float");
    }
}
