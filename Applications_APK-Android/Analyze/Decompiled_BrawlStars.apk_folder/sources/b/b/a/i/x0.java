package b.b.a.i;

import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewPropertyAnimator;
import b.b.a.f.a;
import b.b.a.i.g;
import b.b.a.j.q1;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;
import kotlin.a.ab;
import kotlin.a.ad;
import kotlin.d.b.j;
import nl.komponents.kovenant.ap;

public abstract class x0 extends g {
    public Set<Integer> m = ad.f5212a;
    public ValueAnimator n;
    public float o;
    public Collection<? extends View> p = ab.f5210a;
    public HashMap q;

    public void a() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public final void a(float f, boolean z) {
        this.o = f;
        for (View view : this.p) {
            view.setAlpha((z || !this.m.contains(Integer.valueOf(view.getId()))) ? f : 1.0f);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.i.x0.a(float, boolean):void
     arg types: [int, int]
     candidates:
      b.b.a.i.g.a(b.b.a.i.g$b, boolean):nl.komponents.kovenant.bw<java.lang.Boolean, java.lang.Exception>
      b.b.a.i.g.a(android.view.View, int):void
      b.b.a.i.g.a(b.b.a.i.g$a, boolean):void
      b.b.a.i.x0.a(float, boolean):void */
    public void a(View view, g.a aVar, boolean z) {
        j.b(view, "view");
        j.b(aVar, "animation");
        ValueAnimator valueAnimator = this.n;
        if (valueAnimator != null) {
            valueAnimator.cancel();
        }
        this.n = null;
        view.setAlpha(1.0f);
        int i = s0.f649a[aVar.ordinal()];
        if (i == 1 || i == 2) {
            a(1.0f, true);
        } else if (i == 3 || i == 4) {
            a(this.o, z);
            q1.a(view, new u0(view, this, z));
        } else if (i == 5) {
            a(1.0f, true);
            c(view);
        }
    }

    public final void a(Collection<? extends View> collection) {
        j.b(collection, "<set-?>");
        this.p = collection;
    }

    public void c(View view) {
        j.b(view, "view");
    }

    public void a(View view, g.b bVar, boolean z, ap<Boolean, Exception> apVar) {
        ViewPropertyAnimator animate;
        j.b(view, "view");
        j.b(bVar, "animation");
        j.b(apVar, "result");
        ValueAnimator valueAnimator = this.n;
        if (valueAnimator != null) {
            valueAnimator.cancel();
        }
        this.n = null;
        if (s0.f650b[bVar.ordinal()] != 1) {
            boolean z2 = !z;
            if (z2) {
                view.bringToFront();
            }
            float f = this.o;
            ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
            ofFloat.setDuration(350L);
            ofFloat.setInterpolator(a.h);
            ofFloat.addUpdateListener(new v0(ofFloat, f, 0.0f, this, z2, apVar));
            ofFloat.addListener(new w0(f, 0.0f, this, z2, apVar));
            ofFloat.start();
            this.n = ofFloat;
            return;
        }
        View c = c();
        if (c != null && (animate = c.animate()) != null) {
            animate.alpha(0.0f);
            animate.scaleX(0.0f);
            animate.scaleY(0.0f);
            animate.setDuration(300);
            animate.setInterpolator(a.a());
            animate.start();
        }
    }

    public void onDestroyView() {
        this.p = ab.f5210a;
        super.onDestroyView();
        a();
    }
}
