package b.b.a.i.q1;

import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import b.b.a.j.q1;
import com.supercell.id.R;
import kotlin.d.b.j;

public final class g implements View.OnLayoutChangeListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ View f555a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ int f556b;
    public final /* synthetic */ d c;

    public final class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ RecyclerView f557a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ int f558b;

        public a(RecyclerView recyclerView, int i) {
            this.f557a = recyclerView;
            this.f558b = i;
        }

        public final void run() {
            RecyclerView.LayoutManager layoutManager = this.f557a.getLayoutManager();
            Parcelable onSaveInstanceState = layoutManager != null ? layoutManager.onSaveInstanceState() : null;
            q1.d(this.f557a, this.f558b);
            RecyclerView.LayoutManager layoutManager2 = this.f557a.getLayoutManager();
            if (layoutManager2 != null) {
                layoutManager2.onRestoreInstanceState(onSaveInstanceState);
            }
        }
    }

    public g(View view, int i, d dVar) {
        this.f555a = view;
        this.f556b = i;
        this.c = dVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        RecyclerView recyclerView = (RecyclerView) this.c.a(R.id.friends_list);
        if (recyclerView != null) {
            View view2 = this.f555a;
            j.a((Object) view2, "selectAllBackground");
            int bottom = view2.getBottom() + this.f556b;
            if (bottom != q1.e(recyclerView)) {
                recyclerView.post(new a(recyclerView, bottom));
            }
        }
    }
}
