package b.b.a.i.s1;

import android.support.v4.app.NotificationCompat;
import android.view.MotionEvent;
import android.view.View;
import kotlin.d.a.c;
import kotlin.d.b.j;

public final class i implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ View f675a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ int f676b;
    public final /* synthetic */ c c;

    public i(View view, int i, c cVar) {
        this.f675a = view;
        this.f676b = i;
        this.c = cVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.MotionEvent, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        j.a((Object) motionEvent, NotificationCompat.CATEGORY_EVENT);
        if (motionEvent.getAction() != 1) {
            return motionEvent.getAction() == 0;
        }
        c cVar = this.c;
        if (cVar == null) {
            return true;
        }
        cVar.invoke(this.f675a, Integer.valueOf(this.f676b));
        return true;
    }
}
