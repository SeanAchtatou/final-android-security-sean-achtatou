package b.b.a.i.x1;

import b.b.a.i.x1.g;
import java.io.File;
import java.io.FileInputStream;
import kotlin.d.a.a;
import kotlin.d.b.k;

public final class h extends k implements a<FileInputStream> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ g.e f933a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public h(g.e eVar) {
        super(0);
        this.f933a = eVar;
    }

    public final Object invoke() {
        g.e eVar = this.f933a;
        File a2 = g.this.a(eVar.c);
        if (a2 == null || !a2.isFile()) {
            return null;
        }
        g.this.getClass().getSimpleName();
        return new FileInputStream(a2);
    }
}
