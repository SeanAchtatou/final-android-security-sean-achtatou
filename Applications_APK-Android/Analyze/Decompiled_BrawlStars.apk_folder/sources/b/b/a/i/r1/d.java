package b.b.a.i.r1;

import b.b.a.h.c;
import b.b.a.h.j;
import b.b.a.j.r0;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.supercell.id.R;
import kotlin.d.b.g;
import kotlin.d.b.j;

public final class d implements r0 {

    /* renamed from: a  reason: collision with root package name */
    public final int f594a = R.layout.fragment_profile_list_item_friend;

    /* renamed from: b  reason: collision with root package name */
    public final c f595b;

    public static abstract class a {

        /* renamed from: b.b.a.i.r1.d$a$a  reason: collision with other inner class name */
        public static final class C0056a extends a {

            /* renamed from: a  reason: collision with root package name */
            public static final C0056a f596a = new C0056a();

            public C0056a() {
                super(null);
            }
        }

        public static final class b extends a {

            /* renamed from: a  reason: collision with root package name */
            public static final b f597a = new b();

            public b() {
                super(null);
            }
        }

        public static final class c extends a {

            /* renamed from: a  reason: collision with root package name */
            public final String f598a;

            /* renamed from: b  reason: collision with root package name */
            public final String f599b;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(String str, String str2) {
                super(null);
                j.b(str, "system");
                this.f598a = str;
                this.f599b = str2;
            }

            public final boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof c)) {
                    return false;
                }
                c cVar = (c) obj;
                return j.a(this.f598a, cVar.f598a) && j.a(this.f599b, cVar.f599b);
            }

            public final int hashCode() {
                String str = this.f598a;
                int i = 0;
                int hashCode = (str != null ? str.hashCode() : 0) * 31;
                String str2 = this.f599b;
                if (str2 != null) {
                    i = str2.hashCode();
                }
                return hashCode + i;
            }

            public final String toString() {
                StringBuilder a2 = b.a.a.a.a.a("Playing(system=");
                a2.append(this.f598a);
                a2.append(", username=");
                return b.a.a.a.a.a(a2, this.f599b, ")");
            }
        }

        public /* synthetic */ a(g gVar) {
        }
    }

    public d(c cVar) {
        j.b(cVar, "friend");
        this.f595b = cVar;
    }

    public final int a() {
        return this.f594a;
    }

    public final boolean a(r0 r0Var) {
        j.b(r0Var, FacebookRequestErrorClassification.KEY_OTHER);
        return (r0Var instanceof d) && j.a(((d) r0Var).d(), d());
    }

    public final String b() {
        return this.f595b.c;
    }

    public final boolean b(r0 r0Var) {
        j.b(r0Var, FacebookRequestErrorClassification.KEY_OTHER);
        if (!(r0Var instanceof d)) {
            return false;
        }
        d dVar = (d) r0Var;
        return j.a(c(), dVar.c()) && j.a(b(), dVar.b()) && j.a(e(), dVar.e());
    }

    public final String c() {
        return this.f595b.f111b;
    }

    public final String d() {
        return this.f595b.f110a;
    }

    public final a e() {
        c cVar = this.f595b;
        if (!(cVar.e instanceof j.a.C0008a)) {
            return a.b.f597a;
        }
        b.b.a.h.g gVar = cVar.d;
        return gVar != null ? new a.c(gVar.f118a, gVar.f119b) : a.C0056a.f596a;
    }

    public final boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof d) && kotlin.d.b.j.a(this.f595b, ((d) obj).f595b);
        }
        return true;
    }

    public final int hashCode() {
        c cVar = this.f595b;
        if (cVar != null) {
            return cVar.hashCode();
        }
        return 0;
    }

    public final String toString() {
        StringBuilder a2 = b.a.a.a.a.a("FriendRow(friend=");
        a2.append(this.f595b);
        a2.append(")");
        return a2.toString();
    }
}
