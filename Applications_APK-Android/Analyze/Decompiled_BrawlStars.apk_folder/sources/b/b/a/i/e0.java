package b.b.a.i;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import b.b.a.h.h;
import b.b.a.j.i0;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.view.WidthAdjustingMultilineTextView;
import java.util.HashMap;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public class e0 extends g {
    public final b<i0, m> m = new a();
    public HashMap n;

    public final class a extends k implements b<i0, m> {
        public a() {
            super(1);
        }

        public final Object invoke(Object obj) {
            String str;
            h a2;
            i0 i0Var = (i0) obj;
            WidthAdjustingMultilineTextView widthAdjustingMultilineTextView = (WidthAdjustingMultilineTextView) e0.this.a(R.id.my_nickname);
            if (widthAdjustingMultilineTextView != null) {
                if (i0Var == null || (a2 = i0Var.a()) == null || (str = a2.f121b) == null) {
                    str = SupercellId.INSTANCE.getSharedServices$supercellId_release().d();
                }
                widthAdjustingMultilineTextView.setText(str);
            }
            return m.f5330a;
        }
    }

    public View a(int i) {
        if (this.n == null) {
            this.n = new HashMap();
        }
        View view = (View) this.n.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.n.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public void a() {
        HashMap hashMap = this.n;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f().b(this.m);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_top_area_logged_in, viewGroup, false);
    }

    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    public void onDetach() {
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f().c(this.m);
        super.onDetach();
    }

    public void onViewCreated(View view, Bundle bundle) {
        j.b(view, "view");
        super.onViewCreated(view, bundle);
        this.m.invoke(SupercellId.INSTANCE.getSharedServices$supercellId_release().f().f1179a);
    }
}
