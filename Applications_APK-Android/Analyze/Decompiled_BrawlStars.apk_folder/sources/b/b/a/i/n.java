package b.b.a.i;

import android.animation.ValueAnimator;
import android.view.View;
import kotlin.d.b.j;

public final class n implements ValueAnimator.AnimatorUpdateListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ o f390a;

    public n(o oVar) {
        this.f390a = oVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.animation.ValueAnimator, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        View view = this.f390a.f403a;
        j.a((Object) valueAnimator, "animator");
        view.setElevation(valueAnimator.getAnimatedFraction() * 8.0f);
    }
}
