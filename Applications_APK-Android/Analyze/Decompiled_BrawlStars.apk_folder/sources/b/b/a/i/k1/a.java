package b.b.a.i.k1;

import b.b.a.j.r0;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.supercell.id.R;
import kotlin.d.b.j;

public final class a implements r0 {

    /* renamed from: a  reason: collision with root package name */
    public final int f346a = R.layout.fragment_ingame_invite_to_play_list_item_invite_all;

    /* renamed from: b  reason: collision with root package name */
    public final int f347b;
    public final boolean c;

    public a(int i, boolean z) {
        this.f347b = i;
        this.c = z;
    }

    public final int a() {
        return this.f346a;
    }

    public final boolean a(r0 r0Var) {
        j.b(r0Var, FacebookRequestErrorClassification.KEY_OTHER);
        return r0Var instanceof a;
    }

    public final boolean b(r0 r0Var) {
        j.b(r0Var, FacebookRequestErrorClassification.KEY_OTHER);
        if (!(r0Var instanceof a)) {
            return false;
        }
        a aVar = (a) r0Var;
        return this.f347b == aVar.f347b && this.c == aVar.c;
    }

    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof a) {
                a aVar = (a) obj;
                if (this.f347b == aVar.f347b) {
                    if (this.c == aVar.c) {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        int i = this.f347b * 31;
        boolean z = this.c;
        if (z) {
            z = true;
        }
        return i + (z ? 1 : 0);
    }

    public final String toString() {
        StringBuilder a2 = b.a.a.a.a.a("AddAllRow(friendsCount=");
        a2.append(this.f347b);
        a2.append(", invitesSent=");
        a2.append(this.c);
        a2.append(")");
        return a2.toString();
    }
}
