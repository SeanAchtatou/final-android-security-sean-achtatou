package b.b.a.i.z1;

import com.supercell.id.ui.MainActivity;
import java.lang.ref.WeakReference;
import kotlin.d.a.b;
import kotlin.d.b.k;
import kotlin.m;

public final class e extends k implements b<Exception, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f994a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public e(WeakReference weakReference) {
        super(1);
        this.f994a = weakReference;
    }

    public final Object invoke(Object obj) {
        Object obj2 = this.f994a.get();
        if (obj2 != null) {
            ((MainActivity) obj2).a((Exception) obj, (b<? super b.b.a.i.e, m>) null);
        }
        return m.f5330a;
    }
}
