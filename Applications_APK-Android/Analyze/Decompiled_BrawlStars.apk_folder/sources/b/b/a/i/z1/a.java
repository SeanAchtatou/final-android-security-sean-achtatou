package b.b.a.i.z1;

import android.os.Bundle;
import android.support.animation.SpringAnimation;
import android.support.animation.SpringForce;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import b.b.a.i.e;
import b.b.a.i.s;
import b.b.a.j.k0;
import b.b.a.j.q1;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.WidthAdjustingMultilineButton;
import java.util.HashMap;
import kotlin.TypeCastException;
import kotlin.d.b.g;
import kotlin.d.b.j;
import kotlin.j.t;
import kotlin.m;

public final class a extends e {
    public static final C0099a g = new C0099a(null);
    public kotlin.d.a.d<? super a, ? super s, ? super String, m> e;
    public HashMap f;

    /* renamed from: b.b.a.i.z1.a$a  reason: collision with other inner class name */
    public static final class C0099a {
        public /* synthetic */ C0099a(g gVar) {
        }

        public final a a(String str) {
            a aVar = new a();
            if (str != null) {
                Bundle bundle = new Bundle();
                bundle.putString("currentNicknameKey", str);
                aVar.setArguments(bundle);
            }
            return aVar;
        }
    }

    public final class b implements View.OnClickListener {
        public b() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Change nickname popup", "click", "Cancel", null, false, 24);
            WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) a.this.a(R.id.cancel_button);
            j.a((Object) widthAdjustingMultilineButton, "cancel_button");
            widthAdjustingMultilineButton.setEnabled(false);
            a aVar = a.this;
            kotlin.d.a.d<? super a, ? super s, ? super String, m> dVar = aVar.e;
            if (dVar != null) {
                dVar.invoke(aVar, s.NEGATIVE, null);
            }
            a.this.b();
        }
    }

    public final class c implements View.OnClickListener {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ String f974b;

        public c(String str) {
            this.f974b = str;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.EditText, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
         arg types: [java.lang.String, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            EditText editText = (EditText) a.this.a(R.id.nickname_edit_text);
            j.a((Object) editText, "nickname_edit_text");
            String obj = editText.getText().toString();
            if (obj != null) {
                String obj2 = t.b(obj).toString();
                if (!j.a((Object) obj2, (Object) this.f974b)) {
                    b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Change nickname popup", "click", "Continue", null, false, 24);
                    b.b.a.d.b a2 = k0.f1078b.a(obj2);
                    if (a2 != null) {
                        MainActivity a3 = b.b.a.b.a(a.this);
                        if (a3 != null) {
                            a3.a(a2, (kotlin.d.a.b<? super e, m>) null);
                            return;
                        }
                        return;
                    }
                    WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) a.this.a(R.id.continue_button);
                    j.a((Object) widthAdjustingMultilineButton, "continue_button");
                    widthAdjustingMultilineButton.setEnabled(false);
                    a aVar = a.this;
                    kotlin.d.a.d<? super a, ? super s, ? super String, m> dVar = aVar.e;
                    if (dVar != null) {
                        dVar.invoke(aVar, s.POSITIVE, obj2);
                    }
                    a.this.b();
                    return;
                }
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    public final class d implements TextWatcher {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ String f976b;

        public d(String str) {
            this.f976b = str;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: b.b.a.j.q1.a(android.support.v7.widget.AppCompatButton, boolean):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, boolean]
         candidates:
          b.b.a.j.q1.a(android.view.View, int):android.view.View
          b.b.a.j.q1.a(android.support.v4.widget.NestedScrollView, int):void
          b.b.a.j.q1.a(android.support.v4.widget.NestedScrollView, android.view.View):void
          b.b.a.j.q1.a(android.view.View, long):void
          b.b.a.j.q1.a(android.view.View, b.b.a.j.z):void
          b.b.a.j.q1.a(android.view.View, kotlin.d.a.a<kotlin.m>):void
          b.b.a.j.q1.a(android.widget.ScrollView, int):void
          b.b.a.j.q1.a(android.widget.ScrollView, android.view.View):void
          b.b.a.j.q1.a(android.support.v7.widget.AppCompatButton, boolean):void */
        public final void afterTextChanged(Editable editable) {
            String obj;
            String obj2 = (editable == null || (obj = editable.toString()) == null) ? null : t.b(obj).toString();
            WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) a.this.a(R.id.continue_button);
            j.a((Object) widthAdjustingMultilineButton, "continue_button");
            q1.a((AppCompatButton) widthAdjustingMultilineButton, obj2 == null || j.a(obj2, this.f976b) || k0.f1078b.a(obj2) != null);
        }

        public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    public final View a(int i) {
        if (this.f == null) {
            this.f = new HashMap();
        }
        View view = (View) this.f.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.f.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.f;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_edit_nickname_dialog, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.EditText, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.j.a(java.lang.Object[], java.lang.Object[]):T[]
     arg types: [android.text.InputFilter[], android.text.InputFilter$LengthFilter[]]
     candidates:
      kotlin.a.k.a(java.lang.Object[], java.util.Collection):C
      kotlin.a.k.a(char[], char):boolean
      kotlin.a.j.a(java.lang.Object[], java.util.Comparator):void
      kotlin.a.j.a(java.lang.Object[], java.lang.Object):T[]
      kotlin.a.j.a(java.lang.Object[], java.lang.Object[]):T[] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.j.q1.a(android.support.v7.widget.AppCompatButton, boolean):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, boolean]
     candidates:
      b.b.a.j.q1.a(android.view.View, int):android.view.View
      b.b.a.j.q1.a(android.support.v4.widget.NestedScrollView, int):void
      b.b.a.j.q1.a(android.support.v4.widget.NestedScrollView, android.view.View):void
      b.b.a.j.q1.a(android.view.View, long):void
      b.b.a.j.q1.a(android.view.View, b.b.a.j.z):void
      b.b.a.j.q1.a(android.view.View, kotlin.d.a.a<kotlin.m>):void
      b.b.a.j.q1.a(android.widget.ScrollView, int):void
      b.b.a.j.q1.a(android.widget.ScrollView, android.view.View):void
      b.b.a.j.q1.a(android.support.v7.widget.AppCompatButton, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.LinearLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.animation.SpringForce, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        j.b(view, "view");
        Integer c2 = k0.f1078b.c();
        boolean z = false;
        if (c2 != null) {
            int intValue = c2.intValue();
            EditText editText = (EditText) a(R.id.nickname_edit_text);
            j.a((Object) editText, "nickname_edit_text");
            editText.setFilters((InputFilter[]) kotlin.a.g.a((Object[]) editText.getFilters(), (Object[]) new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(intValue)}));
        }
        Bundle arguments = getArguments();
        String string = arguments != null ? arguments.getString("currentNicknameKey") : null;
        if (string != null) {
            ((EditText) a(R.id.nickname_edit_text)).setText(string);
            ((EditText) a(R.id.nickname_edit_text)).setSelection(string.length());
        }
        ((WidthAdjustingMultilineButton) a(R.id.cancel_button)).setOnClickListener(new b());
        EditText editText2 = (EditText) a(R.id.nickname_edit_text);
        j.a((Object) editText2, "nickname_edit_text");
        String obj = editText2.getText().toString();
        WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) a(R.id.continue_button);
        j.a((Object) widthAdjustingMultilineButton, "continue_button");
        if (j.a((Object) obj, (Object) string) || k0.f1078b.a(obj) != null) {
            z = true;
        }
        q1.a((AppCompatButton) widthAdjustingMultilineButton, z);
        ((WidthAdjustingMultilineButton) a(R.id.continue_button)).setOnClickListener(new c(string));
        ((EditText) a(R.id.nickname_edit_text)).addTextChangedListener(new d(string));
        LinearLayout linearLayout = (LinearLayout) a(R.id.dialog_container);
        j.a((Object) linearLayout, "it");
        linearLayout.setScaleX(0.8f);
        linearLayout.setScaleY(0.8f);
        SpringAnimation springAnimation = new SpringAnimation(linearLayout, SpringAnimation.SCALE_X, 1.0f);
        SpringForce spring = springAnimation.getSpring();
        j.a((Object) spring, "spring");
        spring.setDampingRatio(0.3f);
        SpringForce spring2 = springAnimation.getSpring();
        j.a((Object) spring2, "spring");
        spring2.setStiffness(400.0f);
        springAnimation.start();
        SpringAnimation springAnimation2 = new SpringAnimation(linearLayout, SpringAnimation.SCALE_Y, 1.0f);
        SpringForce spring3 = springAnimation2.getSpring();
        j.a((Object) spring3, "spring");
        spring3.setDampingRatio(0.3f);
        SpringForce spring4 = springAnimation2.getSpring();
        j.a((Object) spring4, "spring");
        spring4.setStiffness(400.0f);
        springAnimation2.start();
    }
}
