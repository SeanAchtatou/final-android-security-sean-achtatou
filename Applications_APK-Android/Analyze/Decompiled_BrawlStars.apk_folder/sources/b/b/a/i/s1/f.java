package b.b.a.i.s1;

import android.view.View;
import android.widget.TextView;
import com.supercell.id.R;
import java.lang.ref.WeakReference;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class f extends k implements b<String, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f667a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public f(WeakReference weakReference) {
        super(1);
        this.f667a = weakReference;
    }

    public final Object invoke(Object obj) {
        TextView textView;
        String str = (String) obj;
        j.b(str, "value");
        View view = (View) this.f667a.get();
        if (!(view == null || (textView = (TextView) view.findViewById(R.id.systemStatusLabel)) == null)) {
            textView.setText(str);
        }
        return m.f5330a;
    }
}
