package b.b.a.i.k1;

import b.b.a.j.k;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    public static final k f363a = new k(0, 1);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
     arg types: [java.util.ArrayList, java.util.List]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0048  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.util.List<b.b.a.j.r0> a(java.util.List<b.b.a.i.k1.b> r6) {
        /*
            boolean r0 = r6 instanceof java.util.Collection
            r1 = 0
            r2 = 1
            if (r0 == 0) goto L_0x000d
            boolean r0 = r6.isEmpty()
            if (r0 == 0) goto L_0x000d
            goto L_0x0023
        L_0x000d:
            java.util.Iterator r0 = r6.iterator()
        L_0x0011:
            boolean r3 = r0.hasNext()
            if (r3 == 0) goto L_0x0023
            java.lang.Object r3 = r0.next()
            b.b.a.i.k1.b r3 = (b.b.a.i.k1.b) r3
            boolean r3 = r3.e
            if (r3 != 0) goto L_0x0011
            r0 = 0
            goto L_0x0024
        L_0x0023:
            r0 = 1
        L_0x0024:
            b.b.a.i.k1.a r3 = new b.b.a.i.k1.a
            int r4 = r6.size()
            r3.<init>(r4, r0)
            java.util.List r0 = kotlin.a.m.a(r3)
            java.util.List r6 = kotlin.a.m.d(r0, r6)
            java.lang.Iterable r6 = kotlin.a.m.j(r6)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.util.Iterator r6 = r6.iterator()
        L_0x0042:
            boolean r3 = r6.hasNext()
            if (r3 == 0) goto L_0x006e
            java.lang.Object r3 = r6.next()
            kotlin.a.ae r3 = (kotlin.a.ae) r3
            int r4 = r3.f5213a
            if (r4 != 0) goto L_0x0059
            T r3 = r3.f5214b
            java.util.List r3 = kotlin.a.m.a(r3)
            goto L_0x006a
        L_0x0059:
            r4 = 2
            b.b.a.j.r0[] r4 = new b.b.a.j.r0[r4]
            b.b.a.j.k r5 = b.b.a.i.k1.d.f363a
            r4[r1] = r5
            T r3 = r3.f5214b
            b.b.a.j.r0 r3 = (b.b.a.j.r0) r3
            r4[r2] = r3
            java.util.List r3 = kotlin.a.m.a(r4)
        L_0x006a:
            kotlin.a.m.a(r0, r3)
            goto L_0x0042
        L_0x006e:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.i.k1.d.a(java.util.List):java.util.List");
    }
}
