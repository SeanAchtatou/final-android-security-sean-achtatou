package b.b.a.i.p1;

import b.b.a.h.j;
import b.b.a.j.r0;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.supercell.id.R;

public final class a implements r0 {

    /* renamed from: a  reason: collision with root package name */
    public final int f471a = R.layout.fragment_messages_list_item_friend_request;

    /* renamed from: b  reason: collision with root package name */
    public final String f472b;
    public final String c;
    public final String d;
    public final j.a e;

    public a(String str, String str2, String str3, j.a aVar) {
        kotlin.d.b.j.b(str, "scid");
        kotlin.d.b.j.b(aVar, "relationship");
        this.f472b = str;
        this.c = str2;
        this.d = str3;
        this.e = aVar;
    }

    public final int a() {
        return this.f471a;
    }

    public final boolean a(r0 r0Var) {
        kotlin.d.b.j.b(r0Var, FacebookRequestErrorClassification.KEY_OTHER);
        return (r0Var instanceof a) && kotlin.d.b.j.a(((a) r0Var).f472b, this.f472b);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    public final boolean b(r0 r0Var) {
        kotlin.d.b.j.b(r0Var, FacebookRequestErrorClassification.KEY_OTHER);
        if (!(r0Var instanceof a)) {
            return false;
        }
        a aVar = (a) r0Var;
        if (!kotlin.d.b.j.a((Object) this.c, (Object) aVar.c) || !kotlin.d.b.j.a((Object) this.d, (Object) aVar.d) || !kotlin.d.b.j.a(this.e.a(), aVar.e.a())) {
            return false;
        }
        return true;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof a)) {
            return false;
        }
        a aVar = (a) obj;
        return kotlin.d.b.j.a(this.f472b, aVar.f472b) && kotlin.d.b.j.a(this.c, aVar.c) && kotlin.d.b.j.a(this.d, aVar.d) && kotlin.d.b.j.a(this.e, aVar.e);
    }

    public final int hashCode() {
        String str = this.f472b;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.c;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.d;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        j.a aVar = this.e;
        if (aVar != null) {
            i = aVar.hashCode();
        }
        return hashCode3 + i;
    }

    public final String toString() {
        StringBuilder a2 = b.a.a.a.a.a("FriendRow(scid=");
        a2.append(this.f472b);
        a2.append(", name=");
        a2.append(this.c);
        a2.append(", avatarUrl=");
        a2.append(this.d);
        a2.append(", relationship=");
        a2.append(this.e);
        a2.append(")");
        return a2.toString();
    }
}
