package b.b.a.i.a2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.FlowPager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.RtlViewPager;
import android.view.View;
import android.widget.Button;
import b.b.a.i.f1;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import java.util.HashMap;
import kotlin.d;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.d.b.r;
import kotlin.d.b.t;
import kotlin.h.h;

public abstract class e extends f1 {
    public static final /* synthetic */ h[] d = {t.a(new r(t.a(e.class), "analyticsLabel", "getAnalyticsLabel()Ljava/lang/String;"))};

    /* renamed from: b  reason: collision with root package name */
    public final d f178b = kotlin.e.a(new a());
    public HashMap c;

    public final class a extends k implements kotlin.d.a.a<String> {
        public a() {
            super(0);
        }

        public final Object invoke() {
            Bundle arguments = e.this.getArguments();
            if (arguments != null) {
                return arguments.getString("analyticsLabel");
            }
            return null;
        }
    }

    public final class b implements View.OnClickListener {
        public b() {
        }

        public final void onClick(View view) {
            RtlViewPager rtlViewPager;
            c a2 = e.a(e.this);
            if (a2 != null && (rtlViewPager = (RtlViewPager) a2.a(R.id.pager)) != null) {
                int currentItem = rtlViewPager.getCurrentItem() + 1;
                PagerAdapter adapter = rtlViewPager.getAdapter();
                int i = 0;
                if (currentItem < (adapter != null ? adapter.getCount() : 0)) {
                    rtlViewPager.setCurrentItem(currentItem, true);
                } else {
                    MainActivity a3 = b.b.a.b.a((Fragment) a2);
                    if (a3 != null) {
                        b.b.a.b.a(a3);
                    }
                }
                FlowPager flowPager = (FlowPager) a2.a(R.id.imagePager);
                if (flowPager != null) {
                    PagerAdapter adapter2 = flowPager.getAdapter();
                    if (adapter2 != null) {
                        i = adapter2.getCount();
                    }
                    if (currentItem < i) {
                        flowPager.setCurrentItem(currentItem, true);
                    }
                }
            }
        }
    }

    public static final /* synthetic */ c a(e eVar) {
        Fragment parentFragment = eVar.getParentFragment();
        if (!(parentFragment instanceof c)) {
            parentFragment = null;
        }
        return (c) parentFragment;
    }

    public View a(int i) {
        if (this.c == null) {
            this.c = new HashMap();
        }
        View view = (View) this.c.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.c.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public void a() {
        HashMap hashMap = this.c;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public void c() {
        String str = (String) this.f178b.a();
        if (str != null) {
            SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a(str);
        }
    }

    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    public void onViewCreated(View view, Bundle bundle) {
        j.b(view, "view");
        super.onViewCreated(view, bundle);
        ((Button) a(R.id.button)).setOnClickListener(new b());
    }
}
