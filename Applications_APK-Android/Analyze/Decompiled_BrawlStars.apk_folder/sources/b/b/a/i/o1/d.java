package b.b.a.i.o1;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.FlowPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import b.b.a.i.x;
import com.supercell.id.R;
import com.supercell.id.view.FlatTabLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import kotlin.d.b.h;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.d.b.t;

public final class d extends q {
    public boolean c;
    public boolean d;
    public HashMap e;

    public static final class a extends FragmentPagerAdapter {

        /* renamed from: a  reason: collision with root package name */
        public final kotlin.d.a.a<q>[] f412a = {C0039a.f413a, b.f414a};

        /* renamed from: b.b.a.i.o1.d$a$a  reason: collision with other inner class name */
        public final class C0039a extends k implements kotlin.d.a.a<k> {

            /* renamed from: a  reason: collision with root package name */
            public static final C0039a f413a = new C0039a();

            public C0039a() {
                super(0);
            }

            public final Object invoke() {
                return new k();
            }
        }

        public final class b extends k implements kotlin.d.a.a<l> {

            /* renamed from: a  reason: collision with root package name */
            public static final b f414a = new b();

            public b() {
                super(0);
            }

            public final Object invoke() {
                return new l();
            }
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(FragmentManager fragmentManager) {
            super(fragmentManager);
            j.b(fragmentManager, "fm");
        }

        public final int getCount() {
            return this.f412a.length;
        }

        public final Fragment getItem(int i) {
            return this.f412a[i].invoke();
        }
    }

    public final /* synthetic */ class b extends h implements kotlin.d.a.b<Integer, String> {

        /* renamed from: a  reason: collision with root package name */
        public static final b f415a = new b();

        public b() {
            super(1);
        }

        public final String getName() {
            return "getTitleKey";
        }

        public final kotlin.h.d getOwner() {
            return t.a(e.class, "supercellId_release");
        }

        public final String getSignature() {
            return "getTitleKey(I)Ljava/lang/String;";
        }

        public final Object invoke(Object obj) {
            int intValue = ((Number) obj).intValue();
            if (intValue == 0) {
                return "log_in_start_tab_email";
            }
            if (intValue != 1) {
                return null;
            }
            return "log_in_start_tab_phone";
        }
    }

    public final /* synthetic */ class c extends h implements kotlin.d.a.b<Integer, String> {

        /* renamed from: a  reason: collision with root package name */
        public static final c f416a = new c();

        public c() {
            super(1);
        }

        public final String getName() {
            return "getIconKey";
        }

        public final kotlin.h.d getOwner() {
            return t.a(e.class, "supercellId_release");
        }

        public final String getSignature() {
            return "getIconKey(I)Ljava/lang/String;";
        }

        public final Object invoke(Object obj) {
            int intValue = ((Number) obj).intValue();
            if (intValue == 0) {
                return "tab_icon_email.png";
            }
            if (intValue != 1) {
                return null;
            }
            return "tab_icon_phone.png";
        }
    }

    /* renamed from: b.b.a.i.o1.d$d  reason: collision with other inner class name */
    public final /* synthetic */ class C0040d extends h implements kotlin.d.a.b<Integer, String> {

        /* renamed from: a  reason: collision with root package name */
        public static final C0040d f417a = new C0040d();

        public C0040d() {
            super(1);
        }

        public final String getName() {
            return "getIconDisabledKey";
        }

        public final kotlin.h.d getOwner() {
            return t.a(e.class, "supercellId_release");
        }

        public final String getSignature() {
            return "getIconDisabledKey(I)Ljava/lang/String;";
        }

        public final Object invoke(Object obj) {
            int intValue = ((Number) obj).intValue();
            if (intValue == 0) {
                return "tab_icon_email_disabled.png";
            }
            if (intValue != 1) {
                return null;
            }
            return "tab_icon_phone_disabled.png";
        }
    }

    public final View a(int i) {
        if (this.e == null) {
            this.e = new HashMap();
        }
        View view = (View) this.e.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.e.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.e;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.app.FragmentManager, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.List<android.support.v4.app.Fragment>, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void c() {
        FragmentManager childFragmentManager = getChildFragmentManager();
        j.a((Object) childFragmentManager, "childFragmentManager");
        List<Fragment> fragments = childFragmentManager.getFragments();
        j.a((Object) fragments, "childFragmentManager.fragments");
        ArrayList<x> arrayList = new ArrayList<>();
        for (Fragment fragment : fragments) {
            if (!(fragment instanceof x)) {
                fragment = null;
            }
            x xVar = (x) fragment;
            if (xVar != null) {
                arrayList.add(xVar);
            }
        }
        for (x d2 : arrayList) {
            d2.d();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.app.FragmentManager, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.view.FlowPager, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FlowPager flowPager = (FlowPager) a(R.id.tabPager);
        if (flowPager != null) {
            FragmentManager childFragmentManager = getChildFragmentManager();
            j.a((Object) childFragmentManager, "childFragmentManager");
            flowPager.setAdapter(new a(childFragmentManager));
        }
        FlatTabLayout flatTabLayout = (FlatTabLayout) a(R.id.tabBarView);
        boolean z = false;
        if (flatTabLayout != null) {
            flatTabLayout.setGetTitleKey(b.f415a);
            flatTabLayout.setGetIconKey(c.f416a);
            flatTabLayout.setGetIconDisabledKey(C0040d.f417a);
            flatTabLayout.setupWithViewPager((FlowPager) a(R.id.tabPager), false);
        }
        if (bundle == null) {
            String i = i();
            if (i == null || i.length() == 0) {
                z = true;
            }
            if (!z) {
                FlowPager flowPager2 = (FlowPager) a(R.id.tabPager);
                j.a((Object) flowPager2, "tabPager");
                flowPager2.setCurrentItem(1);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_login_enter_contact_details_page, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }
}
