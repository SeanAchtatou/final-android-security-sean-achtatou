package b.b.a.i.z1;

import b.b.a.h.l;
import b.b.a.j.j0;
import com.supercell.id.SupercellId;
import kotlin.d.a.a;
import kotlin.d.b.k;
import nl.komponents.kovenant.bw;

public final class f extends k implements a<bw<? extends l, ? extends Exception>> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ boolean f995a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public f(boolean z) {
        super(0);
        this.f995a = z;
    }

    public final Object invoke() {
        return j0.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f(), null, null, Boolean.valueOf(!this.f995a), 3);
    }
}
