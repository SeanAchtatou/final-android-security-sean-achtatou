package b.b.a.i;

import b.b.a.i.g;

public final /* synthetic */ class s0 {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ int[] f649a = new int[g.a.values().length];

    /* renamed from: b  reason: collision with root package name */
    public static final /* synthetic */ int[] f650b = new int[g.b.values().length];

    static {
        f649a[g.a.NONE.ordinal()] = 1;
        f649a[g.a.PAGE_CHANGED.ordinal()] = 2;
        f649a[g.a.SLIDE_IN.ordinal()] = 3;
        f649a[g.a.FADE_IN.ordinal()] = 4;
        f649a[g.a.ENTRY.ordinal()] = 5;
        f650b[g.b.EXIT.ordinal()] = 1;
    }
}
