package b.b.a.i.o1;

import b.b.a.i.e;
import com.supercell.id.ui.MainActivity;
import java.lang.ref.WeakReference;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class g extends k implements b<Exception, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f420a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public g(WeakReference weakReference) {
        super(1);
        this.f420a = weakReference;
    }

    public final Object invoke(Object obj) {
        MainActivity a2;
        Exception exc = (Exception) obj;
        j.b(exc, "it");
        h hVar = (h) this.f420a.get();
        if (!(hVar == null || (a2 = b.b.a.b.a(hVar)) == null)) {
            a2.a(exc, (b<? super e, m>) null);
        }
        return m.f5330a;
    }
}
