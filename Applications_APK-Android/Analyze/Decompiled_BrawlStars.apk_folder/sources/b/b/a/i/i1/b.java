package b.b.a.i.i1;

import android.graphics.Bitmap;
import android.widget.ImageView;
import com.supercell.id.R;
import java.lang.ref.WeakReference;
import kotlin.d.b.k;
import kotlin.m;

public final class b extends k implements kotlin.d.a.b<Bitmap, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f300a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b(WeakReference weakReference) {
        super(1);
        this.f300a = weakReference;
    }

    public final Object invoke(Object obj) {
        Object obj2 = this.f300a.get();
        if (obj2 != null) {
            Bitmap bitmap = (Bitmap) obj;
            ImageView imageView = (ImageView) ((a) obj2).a(R.id.qr_code);
            if (imageView != null) {
                imageView.setImageBitmap(bitmap);
            }
        }
        return m.f5330a;
    }
}
