package b.b.a.i.o1;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import b.b.a.e.a;
import b.b.a.i.d1;
import b.b.a.i.u;
import b.b.a.i.v;
import b.b.a.j.h0;
import b.b.a.j.q1;
import com.facebook.internal.ServerProtocol;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.g;
import com.google.i18n.phonenumbers.j;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.Checkbox;
import com.supercell.id.view.WidthAdjustingMultilineButton;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import kotlin.d.b.k;
import kotlin.j.t;
import kotlin.m;

public final class l extends q implements d1.f, u {
    public String c = "us";
    public j.a d;
    public boolean e = true;
    public boolean f;
    public HashMap g;

    public final class a extends k implements kotlin.d.a.b<Boolean, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f440a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ String f441b;
        public final /* synthetic */ boolean c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(WeakReference weakReference, String str, boolean z) {
            super(1);
            this.f440a = weakReference;
            this.f441b = str;
            this.c = z;
        }

        public final Object invoke(Object obj) {
            ((Boolean) obj).booleanValue();
            l lVar = (l) this.f440a.get();
            if (lVar != null) {
                SupercellId.INSTANCE.setPendingLoginWithPhone$supercellId_release(this.f441b, this.c);
                b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Remember me", "Selection", this.c ? ServerProtocol.DIALOG_RETURN_SCOPES_TRUE : "false", null, false, 24);
                String str = this.f441b;
                p h = lVar.h();
                if (h != null) {
                    h.n = str;
                }
                lVar.a(this.c);
                p h2 = lVar.h();
                if (h2 != null) {
                    h2.k();
                }
            }
            return m.f5330a;
        }
    }

    public final class b extends k implements kotlin.d.a.b<Exception, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f442a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(WeakReference weakReference) {
            super(1);
            this.f442a = weakReference;
        }

        public final Object invoke(Object obj) {
            MainActivity a2;
            Exception exc = (Exception) obj;
            kotlin.d.b.j.b(exc, "it");
            l lVar = (l) this.f442a.get();
            if (!(lVar == null || (a2 = b.b.a.b.a(lVar)) == null)) {
                a2.a(exc, (kotlin.d.a.b<? super b.b.a.i.e, m>) null);
            }
            return m.f5330a;
        }
    }

    public final class c implements View.OnClickListener {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ String f444b;

        public c(String str) {
            this.f444b = str;
        }

        public final void onClick(View view) {
            View view2;
            View view3;
            d a2 = l.this.k();
            int i = 0;
            int width = (a2 == null || (view3 = a2.getView()) == null) ? 0 : view3.getWidth();
            d k = l.this.k();
            if (!(k == null || (view2 = k.getView()) == null)) {
                i = view2.getHeight();
            }
            FragmentTransaction beginTransaction = l.this.getChildFragmentManager().beginTransaction();
            Fragment findFragmentByTag = l.this.getChildFragmentManager().findFragmentByTag("regionListDialog");
            if (findFragmentByTag != null) {
                beginTransaction.remove(findFragmentByTag);
            }
            SupercellId.INSTANCE.getSharedServices$supercellId_release().l.a(a.C0004a.BUTTON_01);
            d1.c.a(this.f444b, l.this.c, width, i).show(beginTransaction, "regionListDialog");
        }
    }

    public final class d implements TextWatcher {
        public d() {
        }

        public final void afterTextChanged(Editable editable) {
            kotlin.d.b.j.b(editable, "s");
            l.this.n();
        }

        public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            kotlin.d.b.j.b(charSequence, "s");
        }

        public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            kotlin.d.b.j.b(charSequence, "s");
        }
    }

    public final class e implements View.OnFocusChangeListener {

        public final class a implements Runnable {

            /* renamed from: b.b.a.i.o1.l$e$a$a  reason: collision with other inner class name */
            public final class C0043a extends k implements kotlin.d.a.b<View, Boolean> {

                /* renamed from: a  reason: collision with root package name */
                public static final C0043a f448a = new C0043a();

                public C0043a() {
                    super(1);
                }

                public final Object invoke(Object obj) {
                    View view = (View) obj;
                    kotlin.d.b.j.b(view, "it");
                    return Boolean.valueOf(view instanceof EditText);
                }
            }

            public a() {
            }

            public final void run() {
                p h = l.this.h();
                if (h != null) {
                    h.a(C0043a.f448a);
                }
            }
        }

        public e() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onFocusChange(View view, boolean z) {
            NestedScrollView nestedScrollView;
            if (z) {
                d a2 = l.this.k();
                if (a2 != null && (nestedScrollView = (NestedScrollView) a2.a(R.id.loginEnterContactDetailsScrollView)) != null) {
                    kotlin.d.b.j.a((Object) view, "v");
                    q1.a(nestedScrollView, view);
                    return;
                }
                return;
            }
            view.post(new a());
        }
    }

    public final class f implements View.OnClickListener {
        public f() {
        }

        public final void onClick(View view) {
            ((Checkbox) l.this.a(R.id.rememberCheckBox)).toggle();
        }
    }

    public final class g implements View.OnClickListener {
        public g() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Log In Progress Step 1 - Phone", "click", "Remember me info", null, false, 24);
            MainActivity a2 = b.b.a.b.a(l.this);
            if (a2 != null) {
                kotlin.d.b.j.a((Object) view, "it");
                b.b.a.b.b(a2, view);
            }
        }
    }

    public final class h implements View.OnClickListener {
        public h() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) l.this.a(R.id.okButton);
            kotlin.d.b.j.a((Object) widthAdjustingMultilineButton, "okButton");
            widthAdjustingMultilineButton.setEnabled(false);
            WidthAdjustingMultilineButton widthAdjustingMultilineButton2 = (WidthAdjustingMultilineButton) l.this.a(R.id.cancelButton);
            kotlin.d.b.j.a((Object) widthAdjustingMultilineButton2, "cancelButton");
            widthAdjustingMultilineButton2.setEnabled(false);
            SupercellId.INSTANCE.clearPendingLogin$supercellId_release();
            MainActivity a2 = b.b.a.b.a(l.this);
            if (a2 != null) {
                a2.d();
            }
        }
    }

    public final class i implements View.OnClickListener {
        public i() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            d a2 = l.this.k();
            if (a2 != null && a2.c) {
                b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Log In Progress Step 1 - Phone", "click", "Log In with both tabs filled", null, false, 24);
            }
            WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) l.this.a(R.id.okButton);
            kotlin.d.b.j.a((Object) widthAdjustingMultilineButton, "okButton");
            widthAdjustingMultilineButton.setEnabled(false);
            WidthAdjustingMultilineButton widthAdjustingMultilineButton2 = (WidthAdjustingMultilineButton) l.this.a(R.id.cancelButton);
            kotlin.d.b.j.a((Object) widthAdjustingMultilineButton2, "cancelButton");
            widthAdjustingMultilineButton2.setEnabled(false);
            l.this.l();
        }
    }

    public final class j extends k implements kotlin.d.a.c<Drawable, b.b.a.i.x1.c, m> {
        public j() {
            super(2);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.content.res.Resources, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final Object invoke(Object obj, Object obj2) {
            ImageView imageView;
            Drawable drawable = (Drawable) obj;
            kotlin.d.b.j.b(drawable, "drawable");
            kotlin.d.b.j.b((b.b.a.i.x1.c) obj2, "<anonymous parameter 1>");
            ImageView imageView2 = (ImageView) l.this.a(R.id.region_flag);
            if (imageView2 != null) {
                b.b.a.j.l lVar = b.b.a.j.l.f1088a;
                String b2 = l.this.c;
                Resources resources = l.this.getResources();
                kotlin.d.b.j.a((Object) resources, "resources");
                imageView2.setImageDrawable(lVar.a(b2, drawable, resources));
            }
            Context context = l.this.getContext();
            if (!(context == null || (imageView = (ImageView) l.this.a(R.id.region_flag)) == null)) {
                b.b.a.b.a(imageView, ContextCompat.getColor(context, R.color.blackTranslucent11), b.b.a.b.a(3), b.b.a.b.a(2), b.b.a.b.a(3), null, 16);
            }
            return m.f5330a;
        }
    }

    public final View a(int i2) {
        if (this.g == null) {
            this.g = new HashMap();
        }
        View view = (View) this.g.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i2);
        this.g.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.g;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void a(v vVar) {
        kotlin.d.b.j.b(vVar, "dialog");
        WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) a(R.id.okButton);
        kotlin.d.b.j.a((Object) widthAdjustingMultilineButton, "okButton");
        widthAdjustingMultilineButton.setEnabled(true);
        WidthAdjustingMultilineButton widthAdjustingMultilineButton2 = (WidthAdjustingMultilineButton) a(R.id.cancelButton);
        kotlin.d.b.j.a((Object) widthAdjustingMultilineButton2, "cancelButton");
        widthAdjustingMultilineButton2.setEnabled(true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.EditText, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    public final void a(String str) {
        j.a aVar;
        kotlin.d.b.j.b(str, "regionCode");
        this.c = str;
        m();
        EditText editText = (EditText) a(R.id.phoneEditText);
        kotlin.d.b.j.a((Object) editText, "phoneEditText");
        String obj = editText.getText().toString();
        kotlin.d.b.j.b(obj, "phoneNumber");
        kotlin.d.b.j.b(str, "regionCode");
        try {
            aVar = com.google.i18n.phonenumbers.g.a().b(obj, str);
        } catch (NumberParseException unused) {
            aVar = null;
        }
        if (aVar != null && (!kotlin.d.b.j.a((Object) com.google.i18n.phonenumbers.g.a().c(aVar), (Object) str))) {
            ((EditText) a(R.id.phoneEditText)).setText("");
        }
        n();
    }

    public final boolean b() {
        d k;
        return super.b() && (k = k()) != null && k.getUserVisibleHint();
    }

    public final void c() {
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a("Log In Progress Step 1 - Phone");
    }

    public final p h() {
        d k = k();
        if (k != null) {
            return k.h();
        }
        return null;
    }

    public final d k() {
        Fragment parentFragment = getParentFragment();
        if (!(parentFragment instanceof d)) {
            parentFragment = null;
        }
        return (d) parentFragment;
    }

    public final void m() {
        TextView textView = (TextView) a(R.id.region_code);
        if (textView != null) {
            StringBuilder sb = new StringBuilder();
            sb.append('+');
            sb.append(com.google.i18n.phonenumbers.g.a().a(this.c));
            textView.setText(sb.toString());
        }
        SupercellId.INSTANCE.getSharedServices$supercellId_release().j.a("regionFlags.png", new j());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.EditText, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.ImageView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.j.q1.a(android.support.v7.widget.AppCompatButton, boolean):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, boolean]
     candidates:
      b.b.a.j.q1.a(android.view.View, int):android.view.View
      b.b.a.j.q1.a(android.support.v4.widget.NestedScrollView, int):void
      b.b.a.j.q1.a(android.support.v4.widget.NestedScrollView, android.view.View):void
      b.b.a.j.q1.a(android.view.View, long):void
      b.b.a.j.q1.a(android.view.View, b.b.a.j.z):void
      b.b.a.j.q1.a(android.view.View, kotlin.d.a.a<kotlin.m>):void
      b.b.a.j.q1.a(android.widget.ScrollView, int):void
      b.b.a.j.q1.a(android.widget.ScrollView, android.view.View):void
      b.b.a.j.q1.a(android.support.v7.widget.AppCompatButton, boolean):void */
    public final void n() {
        j.a aVar;
        EditText editText = (EditText) a(R.id.phoneEditText);
        kotlin.d.b.j.a((Object) editText, "phoneEditText");
        String obj = editText.getText().toString();
        String str = this.c;
        kotlin.d.b.j.b(obj, "phoneNumber");
        kotlin.d.b.j.b(str, "regionCode");
        String str2 = null;
        try {
            aVar = com.google.i18n.phonenumbers.g.a().b(obj, str);
        } catch (NumberParseException unused) {
            aVar = null;
        }
        boolean c2 = h0.f1042a.c(aVar);
        int i2 = 0;
        boolean z = (aVar != null ? com.google.i18n.phonenumbers.g.a().b(aVar) : false) && c2;
        ImageView imageView = (ImageView) a(R.id.validImageView);
        kotlin.d.b.j.a((Object) imageView, "validImageView");
        if (imageView.getVisibility() == 8 && this.f) {
            ImageView imageView2 = (ImageView) a(R.id.validImageView);
            kotlin.d.b.j.a((Object) imageView2, "validImageView");
            imageView2.setScaleX(0.0f);
            ImageView imageView3 = (ImageView) a(R.id.validImageView);
            kotlin.d.b.j.a((Object) imageView3, "validImageView");
            imageView3.setScaleY(0.0f);
            ((ImageView) a(R.id.validImageView)).animate().scaleX(1.0f).scaleY(1.0f).setDuration(300).setInterpolator(b.b.a.f.a.g).start();
        }
        if (c2) {
            if (aVar != null) {
                str2 = com.google.i18n.phonenumbers.g.a().c(aVar);
            }
            if (str2 != null && (!kotlin.d.b.j.a((Object) str2, (Object) this.c))) {
                this.c = str2;
                m();
            }
        }
        if (z) {
            String a2 = h0.f1042a.a(aVar);
            if (!kotlin.d.b.j.a((Object) a2, (Object) obj)) {
                ((EditText) a(R.id.phoneEditText)).setText(a2);
                ((EditText) a(R.id.phoneEditText)).setSelection(a2.length());
            }
        }
        ImageView imageView4 = (ImageView) a(R.id.validImageView);
        kotlin.d.b.j.a((Object) imageView4, "validImageView");
        if (!z) {
            i2 = 8;
        }
        imageView4.setVisibility(i2);
        d k = k();
        if (k != null) {
            k.d = z;
        }
        WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) a(R.id.okButton);
        kotlin.d.b.j.a((Object) widthAdjustingMultilineButton, "okButton");
        q1.a((AppCompatButton) widthAdjustingMultilineButton, !z);
        this.e = c2;
        this.f = z;
        this.d = aVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kotlin.d.b.j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_login_enter_phone_tab_page, viewGroup, false);
    }

    public final void onDestroyView() {
        super.onDestroyView();
        MainActivity a2 = b.b.a.b.a(this);
        if (a2 != null) {
            a2.b(this);
        }
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.Button, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.Checkbox, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        kotlin.d.b.j.b(view, "view");
        super.onViewCreated(view, bundle);
        MainActivity a2 = b.b.a.b.a(this);
        if (a2 != null) {
            a2.a(this);
        }
        String a3 = h0.f1042a.a(getContext());
        String str = a3 != null ? a3 : (String) kotlin.a.m.d((List) h0.f1042a.a());
        if (str == null) {
            str = "us";
        }
        a(str);
        ((Button) a(R.id.phoneRegionButton)).setOnClickListener(new c(a3));
        Button button = (Button) a(R.id.phoneRegionButton);
        kotlin.d.b.j.a((Object) button, "phoneRegionButton");
        button.setSoundEffectsEnabled(false);
        ((EditText) a(R.id.phoneEditText)).setText(i());
        n();
        ((EditText) a(R.id.phoneEditText)).addTextChangedListener(new d());
        ((EditText) a(R.id.phoneEditText)).setOnFocusChangeListener(new e());
        Checkbox checkbox = (Checkbox) a(R.id.rememberCheckBox);
        kotlin.d.b.j.a((Object) checkbox, "rememberCheckBox");
        checkbox.setChecked(j());
        ((LinearLayout) a(R.id.rememberCheckBoxRow)).setOnClickListener(new f());
        ((ImageButton) a(R.id.rememberInfoButton)).setOnClickListener(new g());
        ((WidthAdjustingMultilineButton) a(R.id.cancelButton)).setOnClickListener(new h());
        ((WidthAdjustingMultilineButton) a(R.id.okButton)).setOnClickListener(new i());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.Checkbox, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void l() {
        j.a aVar = this.d;
        if (aVar != null) {
            String str = aVar.j;
            boolean z = false;
            if (!(str == null || t.a(str))) {
                if (!this.e) {
                    MainActivity a2 = b.b.a.b.a(this);
                    if (a2 != null) {
                        a2.a("region_not_supported", (kotlin.d.a.b<? super b.b.a.i.e, m>) null);
                        return;
                    }
                    return;
                }
                if (!this.f) {
                    kotlin.d.b.j.b(aVar, "number");
                    com.google.i18n.phonenumbers.g.a();
                    if (com.google.i18n.phonenumbers.g.a(aVar).length() > 3) {
                        z = true;
                    }
                    if (!z) {
                        MainActivity a3 = b.b.a.b.a(this);
                        if (a3 != null) {
                            a3.a("invalid_phone_number", (kotlin.d.a.b<? super b.b.a.i.e, m>) null);
                            return;
                        }
                        return;
                    }
                }
                kotlin.d.b.j.b(aVar, "number");
                String a4 = com.google.i18n.phonenumbers.g.a().a(aVar, g.a.E164);
                kotlin.d.b.j.a((Object) a4, "PhoneNumberUtil.getInsta…l.PhoneNumberFormat.E164)");
                Checkbox checkbox = (Checkbox) a(R.id.rememberCheckBox);
                kotlin.d.b.j.a((Object) checkbox, "rememberCheckBox");
                boolean isChecked = checkbox.isChecked();
                WeakReference weakReference = new WeakReference(this);
                nl.komponents.kovenant.c.m.b(nl.komponents.kovenant.c.m.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().g.b(null, a4), new a(weakReference, a4, isChecked)), new b(weakReference));
                return;
            }
        }
        MainActivity a5 = b.b.a.b.a(this);
        if (a5 != null) {
            a5.a("missing_required_data", (kotlin.d.a.b<? super b.b.a.i.e, m>) null);
        }
    }
}
