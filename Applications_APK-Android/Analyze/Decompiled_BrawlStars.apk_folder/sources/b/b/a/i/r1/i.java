package b.b.a.i.r1;

import android.support.v7.util.DiffUtil;
import b.b.a.i.r1.d;
import b.b.a.j.u0;
import com.supercell.id.SupercellId;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import kotlin.a.m;
import kotlin.d.b.j;
import kotlin.d.b.k;

public final class i extends k implements kotlin.d.a.a<u0> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ b.b.a.h.d f617a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ List f618b;

    public final class a<T> implements Comparator<T> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ Comparator f619a;

        public a(Comparator comparator) {
            this.f619a = comparator;
        }

        public final int compare(T t, T t2) {
            Comparator comparator = this.f619a;
            String c = ((d) t).c();
            if (c == null) {
                c = "";
            }
            String c2 = ((d) t2).c();
            if (c2 == null) {
                c2 = "";
            }
            return comparator.compare(c, c2);
        }
    }

    public final class b<T> implements Comparator<T> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ Comparator f620a;

        public b(Comparator comparator) {
            this.f620a = comparator;
        }

        public final int compare(T t, T t2) {
            Comparator comparator = this.f620a;
            String c = ((d) t).c();
            if (c == null) {
                c = "";
            }
            String c2 = ((d) t2).c();
            if (c2 == null) {
                c2 = "";
            }
            return comparator.compare(c, c2);
        }
    }

    public final class c<T> implements Comparator<d> {

        /* renamed from: a  reason: collision with root package name */
        public static final c f621a = new c();

        public final int compare(Object obj, Object obj2) {
            return b.b.a.b.a(((d) obj).c(), ((d) obj2).c());
        }
    }

    public final class d<T> implements Comparator<d> {

        /* renamed from: a  reason: collision with root package name */
        public static final d f622a = new d();

        public final int compare(Object obj, Object obj2) {
            boolean z = ((d) obj).e() instanceof d.a.c;
            boolean z2 = ((d) obj2).e() instanceof d.a.c;
            if (!z || z2) {
                return (z || !z2) ? 0 : 1;
            }
            return -1;
        }
    }

    public final class e<T> implements Comparator<d> {

        /* renamed from: a  reason: collision with root package name */
        public static final e f623a = new e();

        public final int compare(Object obj, Object obj2) {
            return b.b.a.b.a(((d) obj).c(), ((d) obj2).c());
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public i(b.b.a.h.d dVar, List list) {
        super(0);
        this.f617a = dVar;
        this.f618b = list;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.p.a(java.lang.Iterable, int):int
     arg types: [java.util.List<b.b.a.h.c>, int]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.p.a(java.lang.Iterable, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
     arg types: [java.util.List, b.b.a.i.r1.i$d]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
     arg types: [java.util.List, b.b.a.i.r1.i$c]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
     arg types: [java.util.ArrayList, b.b.a.i.r1.i$a]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
     arg types: [java.util.List, b.b.a.i.r1.i$e]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
     arg types: [java.util.ArrayList, b.b.a.i.r1.i$b]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.p.a(java.lang.Iterable, int):int
     arg types: [java.util.ArrayList, int]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.p.a(java.lang.Iterable, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v7.util.DiffUtil$DiffResult, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final u0 invoke() {
        List<b.b.a.h.c> list = this.f617a.f112a;
        ArrayList arrayList = new ArrayList(m.a((Iterable) list, 10));
        for (b.b.a.h.c dVar : list) {
            arrayList.add(new d(dVar));
        }
        List a2 = m.a((Iterable) m.a((Iterable) m.a((Iterable) arrayList, (Comparator) new a(SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getCollator())), (Comparator) c.f621a), (Comparator) d.f622a);
        List<b.b.a.h.c> list2 = this.f617a.f113b;
        ArrayList arrayList2 = new ArrayList(m.a((Iterable) list2, 10));
        for (b.b.a.h.c dVar2 : list2) {
            arrayList2.add(new d(dVar2));
        }
        List a3 = m.a((Iterable) m.a((Iterable) arrayList2, (Comparator) new b(SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getCollator())), (Comparator) e.f623a);
        List[] listArr = new List[2];
        if (a2.size() <= 0) {
            a2 = m.a(a.f582b);
        }
        listArr[0] = a2;
        listArr[1] = a3;
        List d2 = m.d(listArr);
        ArrayList<List> arrayList3 = new ArrayList<>();
        for (Object next : d2) {
            if (!((List) next).isEmpty()) {
                arrayList3.add(next);
            }
        }
        ArrayList arrayList4 = new ArrayList(m.a((Iterable) arrayList3, 10));
        for (List a4 : arrayList3) {
            arrayList4.add(l.a(a4));
        }
        List a5 = l.a(arrayList4, m.f630b);
        List list3 = this.f618b;
        DiffUtil.DiffResult calculateDiff = DiffUtil.calculateDiff(new p(list3, a5));
        j.a((Object) calculateDiff, "DiffUtil.calculateDiff(R…llback(oldRows, newRows))");
        return new u0(list3, a5, calculateDiff);
    }
}
