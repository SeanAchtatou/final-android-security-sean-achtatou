package b.b.a.i.p1;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import b.b.a.h.j;
import b.b.a.i.f1;
import b.b.a.j.f0;
import b.b.a.j.m;
import b.b.a.j.q;
import b.b.a.j.q1;
import b.b.a.j.r0;
import b.b.a.j.t0;
import b.b.a.j.u0;
import b.b.a.j.y0;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import kotlin.a.ab;
import kotlin.d.b.k;
import nl.komponents.kovenant.bb;

public final class j extends f1 implements h {

    /* renamed from: b  reason: collision with root package name */
    public final kotlin.d.a.b<m<b.b.a.h.d, f0>, kotlin.m> f498b = new c();
    public List<? extends r0> c;
    public final y0<u0> d = new y0<>(new e(), new f());
    public HashMap e;

    public final class a extends k implements kotlin.d.a.b<Exception, kotlin.m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f499a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(WeakReference weakReference) {
            super(1);
            this.f499a = weakReference;
        }

        public final Object invoke(Object obj) {
            Object obj2 = this.f499a.get();
            if (obj2 != null) {
                Exception exc = (Exception) obj;
                j jVar = (j) obj2;
                MainActivity a2 = b.b.a.b.a(jVar);
                if (a2 != null) {
                    a2.a(exc, (kotlin.d.a.b<? super b.b.a.i.e, kotlin.m>) null);
                }
                jVar.f498b.invoke(SupercellId.INSTANCE.getSharedServices$supercellId_release().e().f1179a);
            }
            return kotlin.m.f5330a;
        }
    }

    public final class b extends k implements kotlin.d.a.b<Exception, kotlin.m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f500a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(WeakReference weakReference) {
            super(1);
            this.f500a = weakReference;
        }

        public final Object invoke(Object obj) {
            Object obj2 = this.f500a.get();
            if (obj2 != null) {
                Exception exc = (Exception) obj;
                j jVar = (j) obj2;
                MainActivity a2 = b.b.a.b.a(jVar);
                if (a2 != null) {
                    a2.a(exc, (kotlin.d.a.b<? super b.b.a.i.e, kotlin.m>) null);
                }
                jVar.f498b.invoke(SupercellId.INSTANCE.getSharedServices$supercellId_release().e().f1179a);
            }
            return kotlin.m.f5330a;
        }
    }

    public final class c extends k implements kotlin.d.a.b<m<? extends b.b.a.h.d, ? extends f0>, kotlin.m> {
        public c() {
            super(1);
        }

        public final Object invoke(Object obj) {
            m mVar = (m) obj;
            if (mVar instanceof m.a) {
                m.a aVar = (m.a) mVar;
                if (((b.b.a.h.d) aVar.f1097a).c.size() > 0) {
                    j jVar = j.this;
                    List<b.b.a.h.c> list = ((b.b.a.h.d) aVar.f1097a).c;
                    ArrayList arrayList = new ArrayList();
                    for (b.b.a.h.c cVar : list) {
                        b.b.a.h.j jVar2 = cVar.e;
                        if (!(jVar2 instanceof j.a)) {
                            jVar2 = null;
                        }
                        j.a aVar2 = (j.a) jVar2;
                        a aVar3 = aVar2 != null ? new a(cVar.f110a, cVar.f111b, cVar.c, aVar2) : null;
                        if (aVar3 != null) {
                            arrayList.add(aVar3);
                        }
                    }
                    jVar.a(arrayList);
                } else {
                    j jVar3 = j.this;
                    jVar3.d.a(bb.f5389a);
                }
            } else if (mVar instanceof m.b) {
                j jVar4 = j.this;
                jVar4.d.a(bb.f5389a);
            } else {
                j jVar5 = j.this;
                jVar5.d.a(bb.f5389a);
            }
            return kotlin.m.f5330a;
        }
    }

    public final class d implements View.OnLayoutChangeListener {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ int f503b;

        public final class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ RecyclerView f504a;

            /* renamed from: b  reason: collision with root package name */
            public final /* synthetic */ int f505b;

            public a(RecyclerView recyclerView, int i) {
                this.f504a = recyclerView;
                this.f505b = i;
            }

            public final void run() {
                RecyclerView.LayoutManager layoutManager = this.f504a.getLayoutManager();
                Parcelable onSaveInstanceState = layoutManager != null ? layoutManager.onSaveInstanceState() : null;
                q1.d(this.f504a, this.f505b);
                RecyclerView.LayoutManager layoutManager2 = this.f504a.getLayoutManager();
                if (layoutManager2 != null) {
                    layoutManager2.onRestoreInstanceState(onSaveInstanceState);
                }
            }
        }

        public d(int i) {
            this.f503b = i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            RecyclerView recyclerView = (RecyclerView) j.this.a(R.id.messagesList);
            if (recyclerView != null) {
                kotlin.d.b.j.a((Object) view, "v");
                int height = view.getHeight() + this.f503b;
                if (height != q1.e(recyclerView)) {
                    recyclerView.post(new a(recyclerView, height));
                }
            }
        }
    }

    public final class e extends k implements kotlin.d.a.b<u0, kotlin.m> {
        public e() {
            super(1);
        }

        public final void a(u0 u0Var) {
            kotlin.d.b.j.b(u0Var, "it");
            j jVar = j.this;
            if (jVar.c == u0Var.f1176a) {
                jVar.c = u0Var.f1177b;
                if (jVar.c == null) {
                    RecyclerView recyclerView = (RecyclerView) jVar.a(R.id.messagesList);
                    if (recyclerView != null) {
                        recyclerView.setVisibility(4);
                    }
                    View a2 = j.this.a(R.id.progressBar);
                    if (a2 != null) {
                        a2.setVisibility(0);
                    }
                } else {
                    RecyclerView recyclerView2 = (RecyclerView) jVar.a(R.id.messagesList);
                    if (recyclerView2 != null) {
                        recyclerView2.setVisibility(0);
                    }
                    View a3 = j.this.a(R.id.progressBar);
                    if (a3 != null) {
                        a3.setVisibility(4);
                    }
                }
                RecyclerView recyclerView3 = (RecyclerView) j.this.a(R.id.messagesList);
                RecyclerView.Adapter adapter = recyclerView3 != null ? recyclerView3.getAdapter() : null;
                if (!(adapter instanceof g)) {
                    adapter = null;
                }
                g gVar = (g) adapter;
                if (gVar != null) {
                    List<? extends r0> list = j.this.c;
                    if (list == null) {
                        list = ab.f5210a;
                    }
                    gVar.a(list);
                    u0Var.c.dispatchUpdatesTo(gVar);
                }
            }
        }

        public final /* synthetic */ Object invoke(Object obj) {
            a((u0) obj);
            return kotlin.m.f5330a;
        }
    }

    public final class f extends k implements kotlin.d.a.b<Exception, kotlin.m> {
        public f() {
            super(1);
        }

        public final Object invoke(Object obj) {
            Exception exc = (Exception) obj;
            kotlin.d.b.j.b(exc, "it");
            MainActivity a2 = b.b.a.b.a(j.this);
            if (a2 != null) {
                a2.a(exc, (kotlin.d.a.b<? super b.b.a.i.e, kotlin.m>) null);
            }
            return kotlin.m.f5330a;
        }
    }

    public final class g extends k implements kotlin.d.a.a<u0> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ List f508a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ List f509b;

        public final class a<T> implements Comparator<T> {
            public final int compare(T t, T t2) {
                return kotlin.b.a.a(((a) t2).e.a(), ((a) t).e.a());
            }
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(List list, List list2) {
            super(0);
            this.f508a = list;
            this.f509b = list2;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
         arg types: [java.util.List, b.b.a.i.p1.j$g$a]
         candidates:
          kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
          kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
          kotlin.a.w.a(java.util.List, int):T
          kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
          kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
          kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
          kotlin.a.s.a(java.util.List, java.util.Comparator):void
          kotlin.a.p.a(java.lang.Iterable, int):int
          kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
          kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T> */
        public final u0 invoke() {
            List<r0> a2 = n.a(kotlin.a.m.a((Iterable) this.f508a, (Comparator) new a()));
            List list = this.f509b;
            return new u0(list, a2, b.a.a.a.a.a(t0.c, list, a2, "DiffUtil.calculateDiff(R…create(oldRows, newRows))"));
        }
    }

    public final View a(int i) {
        if (this.e == null) {
            this.e = new HashMap();
        }
        View view = (View) this.e.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.e.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.e;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public final void a(a aVar) {
        q e2;
        kotlin.d.b.j.b(aVar, "row");
        if (b.b.a.b.a(this) != null && (e2 = SupercellId.INSTANCE.getSharedServices$supercellId_release().e()) != null) {
            List<? extends r0> list = this.c;
            if (list != null) {
                ArrayList arrayList = new ArrayList();
                for (r0 r0Var : list) {
                    if (!(r0Var instanceof a)) {
                        r0Var = null;
                    }
                    a aVar2 = (a) r0Var;
                    if (aVar2 != null) {
                        arrayList.add(aVar2);
                    }
                }
                ArrayList arrayList2 = new ArrayList();
                for (Object next : arrayList) {
                    if (!((a) next).a(aVar)) {
                        arrayList2.add(next);
                    }
                }
                a(arrayList2);
            }
            nl.komponents.kovenant.c.m.b(e2.d(aVar.f472b), new b(new WeakReference(this)));
        }
    }

    public final void a(List<a> list) {
        this.d.a(bb.f5389a);
    }

    public final void b(a aVar) {
        kotlin.d.b.j.b(aVar, "row");
        List<? extends r0> list = this.c;
        if (list != null) {
            ArrayList arrayList = new ArrayList();
            for (r0 r0Var : list) {
                if (!(r0Var instanceof a)) {
                    r0Var = null;
                }
                a aVar2 = (a) r0Var;
                if (aVar2 != null) {
                    arrayList.add(aVar2);
                }
            }
            ArrayList arrayList2 = new ArrayList();
            for (Object next : arrayList) {
                if (!((a) next).a(aVar)) {
                    arrayList2.add(next);
                }
            }
            a(arrayList2);
        }
        nl.komponents.kovenant.c.m.b(SupercellId.INSTANCE.getSharedServices$supercellId_release().e().a(aVar.f472b), new a(new WeakReference(this)));
    }

    public final void c() {
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a("Messages - Friends");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kotlin.d.b.j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_messages_invites_page, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v7.widget.RecyclerView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.Context, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        FrameLayout frameLayout;
        kotlin.d.b.j.b(view, "view");
        super.onViewCreated(view, bundle);
        Resources resources = getResources();
        kotlin.d.b.j.a((Object) resources, "resources");
        if (b.b.a.b.b(resources)) {
            int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.list_padding_vertical);
            Fragment parentFragment = getParentFragment();
            if (!(parentFragment instanceof i)) {
                parentFragment = null;
            }
            i iVar = (i) parentFragment;
            if (!(iVar == null || (frameLayout = (FrameLayout) iVar.a(R.id.toolbarContainer)) == null)) {
                frameLayout.addOnLayoutChangeListener(new d(dimensionPixelSize));
            }
        }
        if (this.c == null) {
            RecyclerView recyclerView = (RecyclerView) a(R.id.messagesList);
            kotlin.d.b.j.a((Object) recyclerView, "messagesList");
            recyclerView.setVisibility(4);
            View a2 = a(R.id.progressBar);
            kotlin.d.b.j.a((Object) a2, "progressBar");
            a2.setVisibility(0);
        } else {
            RecyclerView recyclerView2 = (RecyclerView) a(R.id.messagesList);
            kotlin.d.b.j.a((Object) recyclerView2, "messagesList");
            recyclerView2.setVisibility(0);
            View a3 = a(R.id.progressBar);
            kotlin.d.b.j.a((Object) a3, "progressBar");
            a3.setVisibility(4);
        }
        Context context = view.getContext();
        kotlin.d.b.j.a((Object) context, "view.context");
        g gVar = new g(context, this);
        List<? extends r0> list = this.c;
        if (list == null) {
            list = ab.f5210a;
        }
        gVar.a(list);
        RecyclerView recyclerView3 = (RecyclerView) a(R.id.messagesList);
        kotlin.d.b.j.a((Object) recyclerView3, "messagesList");
        recyclerView3.setLayoutManager(new LinearLayoutManager(getContext()));
        RecyclerView recyclerView4 = (RecyclerView) a(R.id.messagesList);
        kotlin.d.b.j.a((Object) recyclerView4, "messagesList");
        recyclerView4.setAdapter(gVar);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().e().b(this.f498b);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().e().a();
    }
}
