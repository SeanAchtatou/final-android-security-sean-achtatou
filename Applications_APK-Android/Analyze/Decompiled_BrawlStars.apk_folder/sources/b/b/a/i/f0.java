package b.b.a.i;

import android.os.Bundle;
import android.view.View;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.WidthAdjustingMultilineButton;
import java.util.HashMap;
import kotlin.d.b.g;
import kotlin.d.b.j;

public final class f0 extends c {
    public static final a f = new a(null);
    public HashMap e;

    public static final class a {
        public /* synthetic */ a(g gVar) {
        }

        public final f0 a(String str, String str2, String str3, String str4) {
            j.b(str, "titleKey");
            j.b(str2, "textKey");
            j.b(str3, "okButtonKey");
            j.b(str4, "cancelButtonKey");
            f0 f0Var = new f0();
            Bundle arguments = f0Var.getArguments();
            if (arguments == null) {
                arguments = new Bundle();
            }
            arguments.putString("titleKey", str);
            arguments.putString("textKey", str2);
            arguments.putString("okButtonKey", str3);
            arguments.putString("cancelButtonKey", str4);
            f0Var.setArguments(arguments);
            return f0Var;
        }
    }

    public final class b implements View.OnClickListener {
        public b() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) f0.this.a(R.id.okButton);
            j.a((Object) widthAdjustingMultilineButton, "okButton");
            widthAdjustingMultilineButton.setEnabled(false);
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Settings", "click", "Confirm logout", null, true, 8);
            SupercellId.INSTANCE.logout$supercellId_release();
            f0.this.b();
            MainActivity a2 = b.b.a.b.a(f0.this);
            if (a2 != null) {
                a2.e();
            }
        }
    }

    public final View a(int i) {
        if (this.e == null) {
            this.e = new HashMap();
        }
        View view = (View) this.e.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.e.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.e;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    public final void onViewCreated(View view, Bundle bundle) {
        j.b(view, "view");
        super.onViewCreated(view, bundle);
        ((WidthAdjustingMultilineButton) a(R.id.okButton)).setOnClickListener(new b());
    }
}
