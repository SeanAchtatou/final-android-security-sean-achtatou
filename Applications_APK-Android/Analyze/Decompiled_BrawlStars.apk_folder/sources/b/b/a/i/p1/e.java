package b.b.a.i.p1;

import b.b.a.j.t0;
import b.b.a.j.u0;
import java.util.List;
import kotlin.d.a.a;
import kotlin.d.b.k;

public final class e extends k implements a<u0> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ List f487a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public e(List list) {
        super(0);
        this.f487a = list;
    }

    public final Object invoke() {
        List list = this.f487a;
        return new u0(list, null, b.a.a.a.a.a(t0.c, list, null, "DiffUtil.calculateDiff(R…create(oldRows, newRows))"));
    }
}
