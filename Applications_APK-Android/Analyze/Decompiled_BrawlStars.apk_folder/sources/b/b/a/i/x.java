package b.b.a.i;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import java.util.HashMap;
import kotlin.d.b.j;

public abstract class x extends f1 {

    /* renamed from: b  reason: collision with root package name */
    public HashMap f894b;

    public final class a implements View.OnTouchListener {
        public a() {
        }

        public final boolean onTouch(View view, MotionEvent motionEvent) {
            View currentFocus;
            FragmentActivity activity = x.this.getActivity();
            if (activity != null) {
                b.b.a.b.a((Activity) activity);
            }
            FragmentActivity activity2 = x.this.getActivity();
            if (activity2 == null || (currentFocus = activity2.getCurrentFocus()) == null) {
                return false;
            }
            currentFocus.clearFocus();
            return false;
        }
    }

    public final class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ ViewGroup f896a;

        public b(ViewGroup viewGroup) {
            this.f896a = viewGroup;
        }

        public final void run() {
            this.f896a.requestFocus();
        }
    }

    public void a() {
        HashMap hashMap = this.f894b;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public void e() {
    }

    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    public void onViewCreated(View view, Bundle bundle) {
        j.b(view, "view");
        super.onViewCreated(view, bundle);
        if (!(view instanceof ViewGroup)) {
            view = null;
        }
        ViewGroup viewGroup = (ViewGroup) view;
        if (viewGroup != null) {
            viewGroup.setFocusable(true);
        }
        if (viewGroup != null) {
            viewGroup.setFocusableInTouchMode(true);
        }
        if (viewGroup != null) {
            viewGroup.setDescendantFocusability(131072);
        }
        if (viewGroup != null) {
            viewGroup.setOnTouchListener(new a());
        }
        if (viewGroup != null) {
            viewGroup.post(new b(viewGroup));
        }
    }
}
