package b.b.a.i;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.animation.SpringAnimation;
import android.support.animation.SpringForce;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import b.b.a.j.f0;
import com.supercell.id.R;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.WidthAdjustingMultilineButton;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import kotlin.d.b.g;
import kotlin.d.b.j;
import kotlin.h;
import kotlin.j.t;

public final class v extends e {
    public static final a f = new a(null);
    public HashMap e;

    public static final class a {
        public /* synthetic */ a(g gVar) {
        }

        public final v a(f0 f0Var) {
            j.b(f0Var, "error");
            return (v) b.b.a.b.a(new v(), "error", f0Var);
        }
    }

    public final class b implements View.OnClickListener {
        public b() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) v.this.a(R.id.errorOkButton);
            j.a((Object) widthAdjustingMultilineButton, "errorOkButton");
            widthAdjustingMultilineButton.setEnabled(false);
            v.this.b();
        }
    }

    public final View a(int i) {
        if (this.e == null) {
            this.e = new HashMap();
        }
        View view = (View) this.e.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.e.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.e;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_error_dialog, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, kotlin.d.a.b, int):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String, ?[OBJECT, ARRAY], int]
     candidates:
      b.b.a.i.x1.j.a(android.widget.ImageView, java.lang.String, boolean, int):void
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, java.lang.Object, int):void
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, kotlin.d.a.b, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.LinearLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.animation.SpringForce, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        f0 f0Var;
        j.b(view, "view");
        Bundle arguments = getArguments();
        if (!(arguments == null || (f0Var = (f0) arguments.getParcelable("error")) == null)) {
            TextView textView = (TextView) a(R.id.errorTitleTextView);
            j.a((Object) textView, "errorTitleTextView");
            b.b.a.i.x1.j.a(textView, f0Var.f1036a, (kotlin.d.a.b) null, 2);
            TextView textView2 = (TextView) a(R.id.errorTextTextView);
            j.a((Object) textView2, "errorTextTextView");
            String str = f0Var.f1037b;
            h<String, String> hVar = f0Var.d;
            h[] hVarArr = hVar != null ? new h[]{hVar} : new h[0];
            b.b.a.i.x1.j.a(textView2, str, (h[]) Arrays.copyOf(hVarArr, hVarArr.length));
            WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) a(R.id.errorOkButton);
            j.a((Object) widthAdjustingMultilineButton, "errorOkButton");
            String str2 = f0Var.c;
            if (!(!t.a((CharSequence) str2))) {
                str2 = null;
            }
            if (str2 == null) {
                str2 = "api_error_generic_btn";
            }
            b.b.a.i.x1.j.a((TextView) widthAdjustingMultilineButton, str2, (kotlin.d.a.b) null, 2);
        }
        ((WidthAdjustingMultilineButton) a(R.id.errorOkButton)).setOnClickListener(new b());
        LinearLayout linearLayout = (LinearLayout) a(R.id.errorContainer);
        j.a((Object) linearLayout, "it");
        linearLayout.setScaleX(0.8f);
        linearLayout.setScaleY(0.8f);
        SpringAnimation springAnimation = new SpringAnimation(linearLayout, SpringAnimation.SCALE_X, 1.0f);
        SpringForce spring = springAnimation.getSpring();
        j.a((Object) spring, "spring");
        spring.setDampingRatio(0.3f);
        SpringForce spring2 = springAnimation.getSpring();
        j.a((Object) spring2, "spring");
        spring2.setStiffness(400.0f);
        springAnimation.start();
        SpringAnimation springAnimation2 = new SpringAnimation(linearLayout, SpringAnimation.SCALE_Y, 1.0f);
        SpringForce spring3 = springAnimation2.getSpring();
        j.a((Object) spring3, "spring");
        spring3.setDampingRatio(0.3f);
        SpringForce spring4 = springAnimation2.getSpring();
        j.a((Object) spring4, "spring");
        spring4.setStiffness(400.0f);
        springAnimation2.start();
    }

    public final void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        MainActivity a2 = b.b.a.b.a(this);
        if (a2 != null) {
            j.b(this, "dialog");
            for (Map.Entry<u, Integer> key : a2.f.entrySet()) {
                ((u) key.getKey()).a(this);
            }
        }
    }
}
