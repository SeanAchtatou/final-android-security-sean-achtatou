package b.b.a.i.s1;

import android.content.Context;
import android.content.SharedPreferences;
import b.b.a.h.e;
import kotlin.d.b.k;
import org.json.JSONObject;

public final class a extends k implements kotlin.d.a.a<e> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ c f651a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a(c cVar) {
        super(0);
        this.f651a = cVar;
    }

    public final Object invoke() {
        SharedPreferences sharedPreferences;
        String string;
        try {
            Context context = this.f651a.getContext();
            if (context == null || (sharedPreferences = context.getSharedPreferences("SupercellIdSystems", 0)) == null || (string = sharedPreferences.getString("info", null)) == null) {
                return null;
            }
            return new e(new JSONObject(string));
        } catch (Exception unused) {
            return null;
        }
    }
}
