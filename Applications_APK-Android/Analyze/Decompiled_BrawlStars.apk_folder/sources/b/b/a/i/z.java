package b.b.a.i;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ImageViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import b.b.a.b;
import b.b.a.j.q1;
import com.supercell.id.R;
import com.supercell.id.ui.MainActivity;
import java.util.HashMap;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class z extends x0 {
    public HashMap r;

    public final class a extends k implements kotlin.d.a.a<m> {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ View f971b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(View view) {
            super(0);
            this.f971b = view;
        }

        public final Object invoke() {
            MainActivity a2;
            ImageButton c;
            if (z.this.isAdded()) {
                if (!(ViewCompat.getLayoutDirection(this.f971b) == 1) && (((a2 = b.a((Fragment) z.this)) == null || !a2.c()) && (c = z.this.c()) != null)) {
                    ImageViewCompat.setImageTintList(c, ColorStateList.valueOf(ContextCompat.getColor(this.f971b.getContext(), R.color.white)));
                }
            }
            return m.f5330a;
        }
    }

    public final void a() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public final ImageButton c() {
        int i = R.id.nav_area_close_button;
        if (this.r == null) {
            this.r = new HashMap();
        }
        View view = (View) this.r.get(Integer.valueOf(i));
        if (view == null) {
            View view2 = getView();
            if (view2 == null) {
                view = null;
            } else {
                view = view2.findViewById(i);
                this.r.put(Integer.valueOf(i), view);
            }
        }
        return (ImageButton) view;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void c(View view) {
        j.b(view, "view");
        ImageButton c = c();
        if (c != null) {
            Resources resources = getResources();
            j.a((Object) resources, "resources");
            q1.a(c, b.b(resources) ? 500 : 300);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_nav_area_logged_in_landscape, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        j.b(view, "view");
        a(kotlin.a.m.b(c()));
        Resources resources = getResources();
        j.a((Object) resources, "resources");
        if (b.b(resources)) {
            q1.a(view, new a(view));
        }
        super.onViewCreated(view, bundle);
    }
}
