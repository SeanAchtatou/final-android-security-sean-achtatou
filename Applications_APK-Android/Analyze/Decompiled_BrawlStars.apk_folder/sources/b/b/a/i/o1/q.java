package b.b.a.i.o1;

import android.support.v4.app.Fragment;
import b.b.a.i.x;

public abstract class q extends x {
    public final void a(boolean z) {
        p h = h();
        if (h != null) {
            h.o = z;
        }
    }

    public final void b(String str) {
        p h = h();
        if (h != null) {
            h.m = str;
        }
    }

    public final boolean f() {
        p h = h();
        if (h != null) {
            return h.q;
        }
        return false;
    }

    public final String g() {
        p h = h();
        if (h != null) {
            return h.m;
        }
        return null;
    }

    public p h() {
        Fragment parentFragment = getParentFragment();
        if (!(parentFragment instanceof p)) {
            parentFragment = null;
        }
        return (p) parentFragment;
    }

    public final String i() {
        p h = h();
        if (h != null) {
            return h.n;
        }
        return null;
    }

    public final boolean j() {
        p h = h();
        if (h != null) {
            return h.o;
        }
        return false;
    }

    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }
}
