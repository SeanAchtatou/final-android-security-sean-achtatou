package b.b.a.i;

import android.animation.ValueAnimator;
import android.support.constraint.Placeholder;
import android.view.ViewGroup;
import kotlin.TypeCastException;
import kotlin.d.b.j;

public final class c0 implements ValueAnimator.AnimatorUpdateListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ ViewGroup.MarginLayoutParams f198a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ Placeholder f199b;

    public c0(ViewGroup.MarginLayoutParams marginLayoutParams, Placeholder placeholder, a0 a0Var, boolean z) {
        this.f198a = marginLayoutParams;
        this.f199b = placeholder;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.animation.ValueAnimator, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        ViewGroup.MarginLayoutParams marginLayoutParams = this.f198a;
        j.a((Object) valueAnimator, "it");
        Object animatedValue = valueAnimator.getAnimatedValue();
        if (animatedValue != null) {
            marginLayoutParams.topMargin = ((Integer) animatedValue).intValue();
            this.f199b.setLayoutParams(this.f198a);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Int");
    }
}
