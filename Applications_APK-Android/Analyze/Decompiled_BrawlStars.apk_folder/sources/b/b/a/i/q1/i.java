package b.b.a.i.q1;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import b.b.a.b;
import b.b.a.g.d;
import b.b.a.j.z;
import com.supercell.id.R;
import java.lang.ref.WeakReference;
import kotlin.d.b.j;

public final class i extends z {

    /* renamed from: b  reason: collision with root package name */
    public Rect f561b;
    public final /* synthetic */ WeakReference c;
    public final /* synthetic */ d d;

    public final class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ d f562a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ View f563b;
        public final /* synthetic */ View c;
        public final /* synthetic */ FrameLayout d;
        public final /* synthetic */ i e;

        public a(d dVar, View view, View view2, FrameLayout frameLayout, i iVar, Rect rect) {
            this.f562a = dVar;
            this.f563b = view;
            this.c = view2;
            this.d = frameLayout;
            this.e = iVar;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.support.v7.widget.RecyclerView, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void run() {
            d dVar = this.f562a;
            View view = this.f563b;
            View view2 = this.c;
            FrameLayout frameLayout = this.d;
            d dVar2 = this.e.d;
            RecyclerView recyclerView = (RecyclerView) dVar.a(R.id.friends_list);
            j.a((Object) recyclerView, "friends_list");
            dVar.a(view, view2, frameLayout, dVar2, b.a(recyclerView, 0), 0);
        }
    }

    public i(WeakReference weakReference, d dVar) {
        this.c = weakReference;
        this.d = dVar;
    }

    public final void a(Rect rect) {
        View a2;
        j.b(rect, "systemWindowInsets");
        d dVar = (d) this.c.get();
        if (dVar != null) {
            Rect rect2 = this.f561b;
            if (!(rect2 != null && rect2.left == rect.left && rect2.right == rect.right)) {
                View a3 = dVar.a(R.id.sticky_header_container);
                if (a3 != null && (a2 = dVar.a(R.id.friends_header_container)) != null) {
                    a3.post(new a(dVar, a3, a2, (FrameLayout) dVar.a(R.id.end_system_inset_guide), this, rect));
                } else {
                    return;
                }
            }
            this.f561b = rect;
        }
    }
}
