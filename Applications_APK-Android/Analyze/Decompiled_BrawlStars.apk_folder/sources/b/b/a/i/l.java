package b.b.a.i;

import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.ViewPropertyAnimator;
import kotlin.d.a.a;
import kotlin.d.b.k;
import kotlin.m;

public final class l extends k implements a<m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ View f364a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ g f365b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l(View view, g gVar) {
        super(0);
        this.f364a = view;
        this.f365b = gVar;
    }

    public final Object invoke() {
        if (this.f365b.isAdded()) {
            boolean z = true;
            if (ViewCompat.getLayoutDirection(this.f364a) != 1) {
                z = false;
            }
            float f = z ? -1.0f : 1.0f;
            if (this.f364a.getTranslationX() == 0.0f) {
                View view = this.f364a;
                view.setTranslationX(f * -0.5f * ((float) view.getWidth()));
            }
            this.f364a.setAlpha(1.0f);
            ViewPropertyAnimator listener = this.f364a.animate().setStartDelay(0).setDuration(350).setInterpolator(b.b.a.f.a.f62a).translationX(0.0f).setListener(null);
            if (Build.VERSION.SDK_INT >= 21) {
                listener.setUpdateListener(null);
            }
            listener.start();
        }
        return m.f5330a;
    }
}
