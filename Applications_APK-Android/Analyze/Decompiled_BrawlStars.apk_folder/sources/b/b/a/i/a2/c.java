package b.b.a.i.a2;

import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.os.ParcelCompat;
import android.support.v4.view.FlowPager;
import android.support.v4.view.RtlViewPager;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import b.b.a.i.a1;
import b.b.a.i.b;
import b.b.a.i.g;
import b.b.a.i.x0;
import b.b.a.j.a0;
import b.b.a.j.q1;
import com.facebook.share.internal.ShareConstants;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import kotlin.a.an;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class c extends g {
    public HashMap m;

    public static final class a extends b.a implements a0 {
        public static final Parcelable.Creator<a> CREATOR = new C0010a();
        public final Set<Integer> e;
        public final Class<? extends g> f;
        public final boolean g;

        /* renamed from: b.b.a.i.a2.c$a$a  reason: collision with other inner class name */
        public final class C0010a implements Parcelable.Creator<a> {
            public final a createFromParcel(Parcel parcel) {
                j.b(parcel, ShareConstants.FEED_SOURCE_PARAM);
                j.b(parcel, "parcel");
                return new a(ParcelCompat.readBoolean(parcel));
            }

            public final a[] newArray(int i) {
                return new a[i];
            }
        }

        public a(boolean z) {
            Set<Integer> set;
            this.g = z;
            if (this.g) {
                set = an.a(Integer.valueOf(R.id.navAreaLogo));
            } else {
                set = an.a((Object[]) new Integer[]{Integer.valueOf(R.id.nav_area_back_button), Integer.valueOf(R.id.nav_area_close_button)});
            }
            this.e = set;
            this.f = c.class;
        }

        public final Class<? extends g> a() {
            return this.f;
        }

        public final Class<? extends x0> a(Resources resources) {
            j.b(resources, "resources");
            return b.class;
        }

        public final int b(Resources resources, int i, int i2, int i3) {
            j.b(resources, "resources");
            return 0;
        }

        public final int c(Resources resources, int i, int i2, int i3) {
            j.b(resources, "resources");
            return 0;
        }

        public final Set<Integer> c() {
            return this.e;
        }

        public final boolean c(Resources resources) {
            j.b(resources, "resources");
            return !b.b.a.b.b(resources) || this.g || !SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getSocialFeatureEnabled();
        }

        public final int describeContents() {
            return 0;
        }

        public final Class<? extends g> e(Resources resources) {
            j.b(resources, "resources");
            return a1.class;
        }

        public final boolean e() {
            return !this.g;
        }

        public final boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    if (this.g == ((a) obj).g) {
                        return true;
                    }
                }
                return false;
            }
            return true;
        }

        public final int hashCode() {
            boolean z = this.g;
            if (z) {
                return 1;
            }
            return z ? 1 : 0;
        }

        public final String toString() {
            StringBuilder a2 = b.a.a.a.a.a("BackStackEntry(isFirstPage=");
            a2.append(this.g);
            a2.append(")");
            return a2.toString();
        }

        public final void writeToParcel(Parcel parcel, int i) {
            j.b(parcel, "dest");
            ParcelCompat.writeBoolean(parcel, this.g);
        }
    }

    public static final class b extends x0 {
        public HashMap r;

        public final class a extends k implements kotlin.d.a.a<m> {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ View f160a;

            /* renamed from: b  reason: collision with root package name */
            public final /* synthetic */ b f161b;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(View view, b bVar) {
                super(0);
                this.f160a = view;
                this.f161b = bVar;
            }

            public final Object invoke() {
                if (this.f161b.isAdded()) {
                    this.f160a.animate().alpha(1.0f).setStartDelay(300).setDuration(300).setInterpolator(b.b.a.f.a.h).start();
                }
                return m.f5330a;
            }
        }

        /* renamed from: b.b.a.i.a2.c$b$b  reason: collision with other inner class name */
        public final class C0011b implements View.OnClickListener {
            public C0011b() {
            }

            public final void onClick(View view) {
                b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Intro", "click", "Skip", null, false, 24);
                MainActivity a2 = b.b.a.b.a((Fragment) b.this);
                if (a2 != null) {
                    b.b.a.b.a(a2);
                }
            }
        }

        public final View a(int i) {
            if (this.r == null) {
                this.r = new HashMap();
            }
            View view = (View) this.r.get(Integer.valueOf(i));
            if (view != null) {
                return view;
            }
            View view2 = getView();
            if (view2 == null) {
                return null;
            }
            View findViewById = view2.findViewById(i);
            this.r.put(Integer.valueOf(i), findViewById);
            return findViewById;
        }

        public final void a() {
            HashMap hashMap = this.r;
            if (hashMap != null) {
                hashMap.clear();
            }
        }

        public final View b() {
            return (ImageButton) a(R.id.nav_area_back_button);
        }

        public final View c() {
            return (ImageButton) a(R.id.nav_area_close_button);
        }

        public final void c(View view) {
            j.b(view, "view");
            for (View view2 : kotlin.a.m.d((ImageView) a(R.id.navAreaLogo), (Button) a(R.id.skip_button), a(R.id.navAreaDivider))) {
                view2.setAlpha(0.0f);
                q1.a(view2, new a(view2, this));
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
            j.b(layoutInflater, "inflater");
            return layoutInflater.inflate(R.layout.fragment_tutorial_nav_area, viewGroup, false);
        }

        public final /* synthetic */ void onDestroyView() {
            super.onDestroyView();
            a();
        }

        public final void onViewCreated(View view, Bundle bundle) {
            List list;
            j.b(view, "view");
            if (b.b.a.b.b(this)) {
                ImageView imageView = (ImageView) a(R.id.navAreaLogo);
                if (imageView != null) {
                    imageView.setVisibility(0);
                }
                Button button = (Button) a(R.id.skip_button);
                if (button != null) {
                    button.setVisibility(0);
                }
                Button button2 = (Button) a(R.id.skip_button);
                if (button2 != null) {
                    button2.setOnClickListener(new C0011b());
                }
                list = kotlin.a.m.d((ImageView) a(R.id.navAreaLogo), (Button) a(R.id.skip_button), a(R.id.navAreaDivider));
            } else {
                View b2 = b();
                if (b2 != null) {
                    b2.setVisibility(0);
                }
                View c = c();
                if (c != null) {
                    c.setVisibility(0);
                }
                list = kotlin.a.m.d(b(), c(), a(R.id.navAreaDivider));
            }
            a(list);
            super.onViewCreated(view, bundle);
        }
    }

    /* renamed from: b.b.a.i.a2.c$c  reason: collision with other inner class name */
    public static final class C0012c extends FragmentPagerAdapter {

        /* renamed from: a  reason: collision with root package name */
        public final kotlin.d.a.a<d>[] f163a;

        /* renamed from: b.b.a.i.a2.c$c$a */
        public final class a extends k implements kotlin.d.a.a<d> {

            /* renamed from: a  reason: collision with root package name */
            public static final a f164a = new a();

            public a() {
                super(0);
            }

            public final Object invoke() {
                return d.c.a("tutorial_1.png");
            }
        }

        /* renamed from: b.b.a.i.a2.c$c$b */
        public final class b extends k implements kotlin.d.a.a<d> {

            /* renamed from: a  reason: collision with root package name */
            public static final b f165a = new b();

            public b() {
                super(0);
            }

            public final Object invoke() {
                return d.c.a("tutorial_2.png");
            }
        }

        /* renamed from: b.b.a.i.a2.c$c$c  reason: collision with other inner class name */
        public final class C0013c extends k implements kotlin.d.a.a<d> {

            /* renamed from: a  reason: collision with root package name */
            public static final C0013c f166a = new C0013c();

            public C0013c() {
                super(0);
            }

            public final Object invoke() {
                return d.c.a("tutorial_3.png");
            }
        }

        /* renamed from: b.b.a.i.a2.c$c$d */
        public final class d extends k implements kotlin.d.a.a<d> {

            /* renamed from: a  reason: collision with root package name */
            public static final d f167a = new d();

            public d() {
                super(0);
            }

            public final Object invoke() {
                return d.c.a("tutorial_4.png");
            }
        }

        /* renamed from: b.b.a.i.a2.c$c$e */
        public final class e extends k implements kotlin.d.a.a<d> {

            /* renamed from: a  reason: collision with root package name */
            public static final e f168a = new e();

            public e() {
                super(0);
            }

            public final Object invoke() {
                return d.c.a("tutorial_5.png");
            }
        }

        public C0012c(FragmentManager fragmentManager) {
            super(fragmentManager);
            kotlin.d.a.a<d>[] aVarArr = {a.f164a, b.f165a, C0013c.f166a, d.f167a};
            this.f163a = SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getSocialFeatureEnabled() ? (kotlin.d.a.a[]) kotlin.a.g.a(aVarArr, e.f168a) : aVarArr;
        }

        public final int getCount() {
            return this.f163a.length;
        }

        public final Fragment getItem(int i) {
            return this.f163a[i].invoke();
        }
    }

    public static final class d extends FragmentPagerAdapter {

        /* renamed from: a  reason: collision with root package name */
        public final kotlin.d.a.a<e>[] f169a;

        public final class a extends k implements kotlin.d.a.a<b> {

            /* renamed from: a  reason: collision with root package name */
            public static final a f170a = new a();

            public a() {
                super(0);
            }

            public final Object invoke() {
                return b.f.a("Intro 1");
            }
        }

        public final class b extends k implements kotlin.d.a.a<a> {

            /* renamed from: a  reason: collision with root package name */
            public static final b f171a = new b();

            public b() {
                super(0);
            }

            public final Object invoke() {
                return a.f.a("tutorial_2.png", "tutorial_title_page_1", "tutorial_content_page_1", "tutorial_action_btn_page_1", "Intro 2");
            }
        }

        /* renamed from: b.b.a.i.a2.c$d$c  reason: collision with other inner class name */
        public final class C0014c extends k implements kotlin.d.a.a<a> {

            /* renamed from: a  reason: collision with root package name */
            public static final C0014c f172a = new C0014c();

            public C0014c() {
                super(0);
            }

            public final Object invoke() {
                return a.f.a("tutorial_3.png", "tutorial_title_page_2", "tutorial_content_page_2", "tutorial_action_btn_page_2", "Intro 3");
            }
        }

        /* renamed from: b.b.a.i.a2.c$d$d  reason: collision with other inner class name */
        public final class C0015d extends k implements kotlin.d.a.a<a> {

            /* renamed from: a  reason: collision with root package name */
            public static final C0015d f173a = new C0015d();

            public C0015d() {
                super(0);
            }

            public final Object invoke() {
                return a.f.a("tutorial_4.png", "tutorial_title_page_3", "tutorial_content_page_3", SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getSocialFeatureEnabled() ? "tutorial_action_btn_page_3" : "tutorial_action_btn_page_4", "Intro 4");
            }
        }

        public final class e extends k implements kotlin.d.a.a<a> {

            /* renamed from: a  reason: collision with root package name */
            public static final e f174a = new e();

            public e() {
                super(0);
            }

            public final Object invoke() {
                return a.f.a("tutorial_5.png", "tutorial_title_page_4", "tutorial_content_page_4", "tutorial_action_btn_page_4", "Intro 5");
            }
        }

        public d(FragmentManager fragmentManager) {
            super(fragmentManager);
            kotlin.d.a.a<e>[] aVarArr = {a.f170a, b.f171a, C0014c.f172a, C0015d.f173a};
            this.f169a = SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getSocialFeatureEnabled() ? (kotlin.d.a.a[]) kotlin.a.g.a(aVarArr, e.f174a) : aVarArr;
        }

        public final int getCount() {
            return this.f169a.length;
        }

        public final Fragment getItem(int i) {
            return this.f169a[i].invoke();
        }
    }

    public final class e implements View.OnClickListener {
        public e() {
        }

        public final void onClick(View view) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Intro", "click", "Skip", null, false, 24);
            MainActivity a2 = b.b.a.b.a((Fragment) c.this);
            if (a2 != null) {
                b.b.a.b.a(a2);
            }
        }
    }

    public final class f extends ViewPager.SimpleOnPageChangeListener {
        public f() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onPageScrolled(int i, float f, int i2) {
            FlowPager flowPager = (FlowPager) c.this.a(R.id.imagePager);
            if (flowPager != null) {
                float a2 = b.b.a.b.a(-20);
                int i3 = 1;
                if (ViewCompat.getLayoutDirection(flowPager) == 1) {
                    i3 = -1;
                }
                float f2 = (f + ((float) i)) * ((float) i3);
                float rint = (float) Math.rint((double) f2);
                float abs = Math.abs(f2 - rint) / 0.5f;
                float f3 = 1.0f - (0.3f * abs);
                flowPager.setScrollX((int) (((float) flowPager.getWidth()) * f2));
                int childCount = flowPager.getChildCount();
                for (int i4 = 0; i4 < childCount; i4++) {
                    View childAt = flowPager.getChildAt(i4);
                    int abs2 = Math.abs((int) rint);
                    j.a((Object) childAt, "child");
                    if (i4 == abs2) {
                        ImageView imageView = (ImageView) childAt.findViewById(R.id.image);
                        if (imageView != null) {
                            imageView.setScaleX(f3);
                        }
                        ImageView imageView2 = (ImageView) childAt.findViewById(R.id.image);
                        if (imageView2 != null) {
                            imageView2.setScaleY(f3);
                        }
                        ImageView imageView3 = (ImageView) childAt.findViewById(R.id.image);
                        if (imageView3 != null) {
                            imageView3.setTranslationY(abs * a2);
                        }
                    } else {
                        ImageView imageView4 = (ImageView) childAt.findViewById(R.id.image);
                        if (imageView4 != null) {
                            imageView4.setScaleX(0.7f);
                        }
                        ImageView imageView5 = (ImageView) childAt.findViewById(R.id.image);
                        if (imageView5 != null) {
                            imageView5.setScaleY(0.7f);
                        }
                        ImageView imageView6 = (ImageView) childAt.findViewById(R.id.image);
                        if (imageView6 != null) {
                            imageView6.setTranslationY(a2);
                        }
                    }
                }
            }
        }
    }

    public final View a(int i) {
        if (this.m == null) {
            this.m = new HashMap();
        }
        View view = (View) this.m.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.m.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public final View b() {
        return (ImageButton) a(R.id.back_button);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_tutorial, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.view.RtlViewPager, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        j.b(view, "view");
        super.onViewCreated(view, bundle);
        if (b.b.a.b.b(this)) {
            ImageView imageView = (ImageView) a(R.id.contentToolbarLogo);
            if (imageView != null) {
                imageView.setVisibility(0);
            }
            Button button = (Button) a(R.id.contentSkipButton);
            if (button != null) {
                button.setVisibility(0);
            }
            Button button2 = (Button) a(R.id.contentSkipButton);
            if (button2 != null) {
                button2.setOnClickListener(new e());
            }
        } else {
            View b2 = b();
            if (b2 != null) {
                b2.setVisibility(0);
            }
            ImageView imageView2 = (ImageView) a(R.id.contentToolbarLogoEnd);
            if (imageView2 != null) {
                imageView2.setVisibility(0);
            }
        }
        d dVar = new d(getChildFragmentManager());
        RtlViewPager rtlViewPager = (RtlViewPager) a(R.id.pager);
        j.a((Object) rtlViewPager, "pager");
        rtlViewPager.setAdapter(dVar);
        ((TabLayout) a(R.id.indicator)).setupWithViewPager((RtlViewPager) a(R.id.pager), true);
        RtlViewPager rtlViewPager2 = (RtlViewPager) a(R.id.pager);
        if (rtlViewPager2 != null) {
            rtlViewPager2.addOnPageChangeListener(new f());
        }
        FlowPager flowPager = (FlowPager) a(R.id.imagePager);
        if (flowPager != null) {
            flowPager.setOffscreenPageLimit(Math.max(1, dVar.f169a.length - 1));
        }
        FlowPager flowPager2 = (FlowPager) a(R.id.imagePager);
        if (flowPager2 != null) {
            flowPager2.setAdapter(new C0012c(getChildFragmentManager()));
        }
    }
}
