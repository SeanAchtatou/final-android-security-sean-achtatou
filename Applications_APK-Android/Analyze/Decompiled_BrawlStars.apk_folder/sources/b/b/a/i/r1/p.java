package b.b.a.i.r1;

import b.b.a.j.r0;
import b.b.a.j.t0;
import java.util.List;
import kotlin.a.ab;

public final class p extends t0 {
    public final boolean a(int i, int i2) {
        if (!super.a(i, i2)) {
            return false;
        }
        boolean z = l.b(this.f1172a, i) == l.b(this.f1173b, i2);
        boolean z2 = l.a(this.f1172a, i) == l.a(this.f1173b, i2);
        if (!z || !z2) {
            return false;
        }
        return true;
    }

    public final boolean areContentsTheSame(int i, int i2) {
        return this.f1172a.get(i).a(this.f1173b.get(i2)) && super.areContentsTheSame(i, i2);
    }

    public final boolean areItemsTheSame(int i, int i2) {
        return i == i2;
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public p(List<? extends r0> list, List<? extends r0> list2) {
        super(list == null ? ab.f5210a : list, list2 == null ? ab.f5210a : list2);
    }
}
