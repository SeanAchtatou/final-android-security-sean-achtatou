package b.b.a.i;

import android.animation.Animator;
import nl.komponents.kovenant.CancelException;
import nl.komponents.kovenant.ap;

public final class w0 implements Animator.AnimatorListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ ap f827a;

    public w0(float f, float f2, x0 x0Var, boolean z, ap apVar) {
        this.f827a = apVar;
    }

    public final void onAnimationCancel(Animator animator) {
        this.f827a.f(new CancelException());
    }

    public final void onAnimationEnd(Animator animator) {
        try {
            this.f827a.e(true);
        } catch (IllegalStateException unused) {
        }
    }

    public final void onAnimationRepeat(Animator animator) {
    }

    public final void onAnimationStart(Animator animator) {
    }
}
