package b.b.a.i;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.view.View;
import b.b.a.b;
import com.supercell.id.R;
import com.supercell.id.ui.MainActivity;
import nl.komponents.kovenant.CancelException;
import nl.komponents.kovenant.ap;

public final class p extends AnimatorListenerAdapter {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ View f468a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ g f469b;
    public final /* synthetic */ ap c;

    public p(View view, g gVar, ap apVar) {
        this.f468a = view;
        this.f469b = gVar;
        this.c = apVar;
    }

    public final void onAnimationCancel(Animator animator) {
        this.c.f(new CancelException());
    }

    public final void onAnimationEnd(Animator animator) {
        View a2;
        MainActivity a3 = b.a((Fragment) this.f469b);
        if (!(a3 == null || (a2 = a3.a(R.id.bottom_area_dimmer)) == null)) {
            a2.setVisibility(4);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            this.f468a.setElevation(0.0f);
        }
        try {
            this.c.e(true);
        } catch (IllegalStateException unused) {
        }
    }
}
