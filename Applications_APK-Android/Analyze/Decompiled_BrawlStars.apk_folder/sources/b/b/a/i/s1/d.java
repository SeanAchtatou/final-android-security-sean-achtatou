package b.b.a.i.s1;

import android.view.View;
import b.a.a.a.a;
import b.b.a.b;
import b.b.a.h.n;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;

public final class d implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ String f663a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ c f664b;
    public final /* synthetic */ n c;

    public d(String str, View view, c cVar, n nVar, boolean z, int i) {
        this.f663a = str;
        this.f664b = cVar;
        this.c = nVar;
    }

    public final void onClick(View view) {
        MainActivity a2 = b.a(this.f664b);
        if (a2 != null) {
            a2.a(this.f663a);
        }
        b.b.a.c.b bVar = SupercellId.INSTANCE.getSharedServices$supercellId_release().f;
        StringBuilder a3 = a.a("Install ");
        a3.append(b.a(this.c));
        b.b.a.c.b.a(bVar, "Connected Games", "click", a3.toString(), null, false, 24);
    }
}
