package b.b.a.i;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import nl.komponents.kovenant.CancelException;
import nl.komponents.kovenant.ap;

public final class q extends AnimatorListenerAdapter {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ ap f515a;

    public q(ap apVar) {
        this.f515a = apVar;
    }

    public final void onAnimationCancel(Animator animator) {
        this.f515a.f(new CancelException());
    }

    public final void onAnimationEnd(Animator animator) {
        try {
            this.f515a.e(true);
        } catch (IllegalStateException unused) {
        }
    }
}
