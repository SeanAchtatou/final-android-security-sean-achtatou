package b.b.a.i;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.TextViewCompat;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import b.b.a.e.a;
import b.b.a.i.g;
import b.b.a.i.t1.a;
import b.b.a.j.c1;
import b.b.a.j.e0;
import b.b.a.j.f0;
import b.b.a.j.i0;
import b.b.a.j.k0;
import b.b.a.j.q1;
import b.b.a.j.y;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.AvatarView;
import com.supercell.id.view.EdgeAntialiasingImageView;
import com.supercell.id.view.WidthAdjustingMultilineTextView;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import kotlin.a.ah;
import nl.komponents.kovenant.bw;

public final class a0 extends g {
    public static final a u = new a(null);
    public int m;
    public Integer n;
    public String o;
    public boolean p;
    public ValueAnimator q;
    public final kotlin.d.a.b<i0, kotlin.m> r = new f();
    public final kotlin.d.a.b<b.b.a.j.m<b.b.a.h.d, f0>, kotlin.m> s = new e();
    public HashMap t;

    public static final class a {
        public /* synthetic */ a(kotlin.d.b.g gVar) {
        }

        public final int a(int i, int i2, int i3) {
            int a2 = kotlin.e.a.a(((float) ((i - i2) - i3)) * 0.107f);
            if (a2 >= kotlin.e.a.a(b.b.a.b.a(57))) {
                return i2 + a2;
            }
            return 0;
        }

        public final int b(int i, int i2, int i3) {
            float f = ((float) ((i - i2) - i3)) * 0.33f;
            float a2 = b.b.a.b.a((int) AvatarView.INTRINSIC_POINT_SIZE);
            float a3 = b.b.a.b.a(220);
            if (Float.compare(f, a2) < 0) {
                f = a2;
            } else if (Float.compare(f, a3) > 0) {
                f = a3;
            }
            return i2 + kotlin.e.a.a(f);
        }
    }

    public final class b extends kotlin.d.b.k implements kotlin.d.a.a<kotlin.m> {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ g.a f142b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(g.a aVar) {
            super(0);
            this.f142b = aVar;
        }

        public final Object invoke() {
            a0.this.b(this.f142b == g.a.PAGE_CHANGED);
            return kotlin.m.f5330a;
        }
    }

    public final class c extends kotlin.d.b.k implements kotlin.d.a.c<View, Integer, kotlin.m> {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ ViewPager f144b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ViewPager viewPager) {
            super(2);
            this.f144b = viewPager;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.EdgeAntialiasingImageView, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final Object invoke(Object obj, Object obj2) {
            View view = (View) obj;
            int intValue = ((Number) obj2).intValue();
            kotlin.d.b.j.b(view, "view");
            ViewPager viewPager = this.f144b;
            if (viewPager != null) {
                a0.this.m = intValue;
                if (viewPager.getCurrentItem() != intValue) {
                    this.f144b.setCurrentItem(intValue);
                } else {
                    SupercellId.INSTANCE.getSharedServices$supercellId_release().l.a(a.C0004a.TAB_SWITCH);
                    EdgeAntialiasingImageView edgeAntialiasingImageView = (EdgeAntialiasingImageView) view.findViewById(R.id.tab_icon_left);
                    kotlin.d.b.j.a((Object) edgeAntialiasingImageView, "view.tab_icon_left");
                    EdgeAntialiasingImageView edgeAntialiasingImageView2 = (EdgeAntialiasingImageView) view.findViewById(R.id.tab_icon_right);
                    kotlin.d.b.j.a((Object) edgeAntialiasingImageView2, "view.tab_icon_right");
                    b.b.a.b.a(edgeAntialiasingImageView, edgeAntialiasingImageView2, 0, 4);
                }
            } else {
                a0.this.a(Integer.valueOf(intValue));
                a0.this.b(intValue);
                SupercellId.INSTANCE.getSharedServices$supercellId_release().l.a(a.C0004a.TAB_SWITCH);
                EdgeAntialiasingImageView edgeAntialiasingImageView3 = (EdgeAntialiasingImageView) view.findViewById(R.id.tab_icon_left);
                kotlin.d.b.j.a((Object) edgeAntialiasingImageView3, "view.tab_icon_left");
                EdgeAntialiasingImageView edgeAntialiasingImageView4 = (EdgeAntialiasingImageView) view.findViewById(R.id.tab_icon_right);
                kotlin.d.b.j.a((Object) edgeAntialiasingImageView4, "view.tab_icon_right");
                b.b.a.b.a(edgeAntialiasingImageView3, edgeAntialiasingImageView4, 0, 4);
                MainActivity a2 = b.b.a.b.a((Fragment) a0.this);
                if (a2 != null) {
                    b.b.a.b.d(a2);
                }
            }
            return kotlin.m.f5330a;
        }
    }

    public final class d extends kotlin.d.b.k implements kotlin.d.a.a<List<? extends c1>> {

        /* renamed from: a  reason: collision with root package name */
        public static final d f145a = new d();

        public d() {
            super(0);
        }

        public final Object invoke() {
            return b.b.a.i.r1.n.s.a();
        }
    }

    public final class e extends kotlin.d.b.k implements kotlin.d.a.b<b.b.a.j.m<? extends b.b.a.h.d, ? extends f0>, kotlin.m> {
        public e() {
            super(1);
        }

        public final Object invoke(Object obj) {
            b.b.a.h.d dVar;
            List<b.b.a.h.c> list;
            b.b.a.j.m mVar = (b.b.a.j.m) obj;
            int i = 0;
            int size = (mVar == null || (dVar = (b.b.a.h.d) mVar.a()) == null || (list = dVar.c) == null) ? 0 : list.size();
            TextView textView = (TextView) a0.this.a(R.id.top_area_messages_indicator);
            if (textView != null) {
                if (size <= 0) {
                    i = 8;
                }
                textView.setVisibility(i);
                textView.setText(String.valueOf(size));
            }
            return kotlin.m.f5330a;
        }
    }

    public final class f extends kotlin.d.b.k implements kotlin.d.a.b<i0, kotlin.m> {
        public f() {
            super(1);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.content.res.Resources, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final Object invoke(Object obj) {
            String str;
            bw<Bitmap, Exception> a2;
            String str2;
            i0 i0Var = (i0) obj;
            String str3 = null;
            b.b.a.h.h a3 = i0Var != null ? i0Var.a() : null;
            boolean z = false;
            if (a3 == null || a3.b()) {
                FrameLayout frameLayout = (FrameLayout) a0.this.a(R.id.top_area_profile_content);
                if (frameLayout != null) {
                    frameLayout.setVisibility(8);
                }
            } else {
                FrameLayout frameLayout2 = (FrameLayout) a0.this.a(R.id.top_area_profile_content);
                if (frameLayout2 != null) {
                    frameLayout2.setVisibility(0);
                }
            }
            ImageView imageView = (ImageView) a0.this.a(R.id.top_area_online_status_indicator);
            if (imageView != null) {
                if (a3 != null) {
                    z = a3.h;
                }
                imageView.setEnabled(!z);
            }
            TextView textView = (TextView) a0.this.a(R.id.top_area_online_status_text);
            if (textView != null) {
                textView.animate().alpha((a3 == null || a3.h) ? 0.0f : 1.0f).setStartDelay((a3 == null || a3.h) ? 0 : 50).setDuration(100).setInterpolator(b.b.a.f.a.h).start();
            }
            a0 a0Var = a0.this;
            if (a3 != null) {
                str3 = a3.d;
            }
            a0Var.o = str3;
            k0 k0Var = k0.f1078b;
            String str4 = a0.this.o;
            if (str4 == null) {
                str4 = "";
            }
            Resources resources = a0.this.getResources();
            kotlin.d.b.j.a((Object) resources, "resources");
            k0Var.a(str4, (ImageView) a0.this.a(R.id.top_area_profile_image), resources);
            WidthAdjustingMultilineTextView widthAdjustingMultilineTextView = (WidthAdjustingMultilineTextView) a0.this.a(R.id.top_area_profile_name);
            if (widthAdjustingMultilineTextView != null) {
                if (a3 == null || (str2 = a3.f121b) == null) {
                    str2 = SupercellId.INSTANCE.getSharedServices$supercellId_release().d();
                }
                widthAdjustingMultilineTextView.setText(str2);
            }
            if (!(a3 == null || (str = a3.f) == null || (a2 = e0.f1030b.a(str)) == null)) {
                nl.komponents.kovenant.c.m.a(a2, new b0(new WeakReference(a0.this)));
            }
            return kotlin.m.f5330a;
        }
    }

    public final class g implements View.OnClickListener {
        public g() {
        }

        public final void onClick(View view) {
            a0.this.b(-1);
            MainActivity a2 = b.b.a.b.a((Fragment) a0.this);
            if (a2 != null) {
                b.b.a.b.e(a2);
            }
        }
    }

    public final class h implements View.OnClickListener {
        public h() {
        }

        public final void onClick(View view) {
            a0.this.b(-2);
            MainActivity a2 = b.b.a.b.a((Fragment) a0.this);
            if (a2 != null) {
                b.b.a.b.c(a2);
            }
        }
    }

    public final class i implements View.OnClickListener {
        public i() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Invite Friends", "click", "My QR code info", null, false, 24);
            MainActivity a2 = b.b.a.b.a((Fragment) a0.this);
            if (a2 != null) {
                kotlin.d.b.j.a((Object) view, "it");
                b.b.a.b.a(a2, view);
            }
        }
    }

    public final class j implements View.OnClickListener {
        public j() {
        }

        public final void onClick(View view) {
            b.b.a.h.h a2;
            i0 i0Var = (i0) SupercellId.INSTANCE.getSharedServices$supercellId_release().f().f1179a;
            if (!(i0Var == null || (a2 = i0Var.a()) == null)) {
                if (new Date().compareTo(a2.e) >= 0) {
                    MainActivity a3 = b.b.a.b.a((Fragment) a0.this);
                    if (a3 != null) {
                        a3.a(new a.c());
                    }
                    SupercellId.INSTANCE.getSharedServices$supercellId_release().l.a(a.C0004a.BUTTON_01);
                    return;
                }
            }
            MainActivity a4 = b.b.a.b.a((Fragment) a0.this);
            if (a4 != null) {
                a4.a("cannot_change_avatar", (kotlin.d.a.b<? super e, kotlin.m>) null);
            }
        }
    }

    public final class k extends kotlin.d.b.k implements kotlin.d.a.c<Drawable, b.b.a.i.x1.c, kotlin.m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f152a;

        public final class a extends kotlin.d.b.k implements kotlin.d.a.a<kotlin.m> {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ WidthAdjustingMultilineTextView f153a;

            /* renamed from: b  reason: collision with root package name */
            public final /* synthetic */ BitmapDrawable f154b;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(WidthAdjustingMultilineTextView widthAdjustingMultilineTextView, BitmapDrawable bitmapDrawable) {
                super(0);
                this.f153a = widthAdjustingMultilineTextView;
                this.f154b = bitmapDrawable;
            }

            public final Object invoke() {
                TextViewCompat.setCompoundDrawablesRelative(this.f153a, this.f154b, null, null, null);
                return kotlin.m.f5330a;
            }
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(WeakReference weakReference) {
            super(2);
            this.f152a = weakReference;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineTextView, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.content.Context, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
         arg types: [int, android.util.TypedValue, int]
         candidates:
          ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
          ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException} */
        public final void a(Drawable drawable, b.b.a.i.x1.c cVar) {
            kotlin.d.b.j.b(drawable, "drawable");
            kotlin.d.b.j.b(cVar, "<anonymous parameter 1>");
            WidthAdjustingMultilineTextView widthAdjustingMultilineTextView = (WidthAdjustingMultilineTextView) this.f152a.get();
            if (widthAdjustingMultilineTextView != null) {
                TypedValue typedValue = new TypedValue();
                kotlin.d.b.j.a((Object) widthAdjustingMultilineTextView, "textView");
                Context context = widthAdjustingMultilineTextView.getContext();
                kotlin.d.b.j.a((Object) context, "textView.context");
                context.getResources().getValue(R.integer.id_icon_scale, typedValue, true);
                float f = typedValue.getFloat();
                BitmapDrawable bitmapDrawable = null;
                if (!(drawable instanceof BitmapDrawable)) {
                    drawable = null;
                }
                BitmapDrawable bitmapDrawable2 = (BitmapDrawable) drawable;
                if (bitmapDrawable2 != null) {
                    Context context2 = widthAdjustingMultilineTextView.getContext();
                    kotlin.d.b.j.a((Object) context2, "textView.context");
                    bitmapDrawable = new BitmapDrawable(context2.getResources(), bitmapDrawable2.getBitmap());
                }
                if (bitmapDrawable != null) {
                    bitmapDrawable.setBounds(new Rect(0, 0, kotlin.e.a.a(b.b.a.b.a(32) * f), kotlin.e.a.a(b.b.a.b.a(29) * f)));
                }
                q1.a(widthAdjustingMultilineTextView, new a(widthAdjustingMultilineTextView, bitmapDrawable));
            }
        }

        public final /* synthetic */ Object invoke(Object obj, Object obj2) {
            a((Drawable) obj, (b.b.a.i.x1.c) obj2);
            return kotlin.m.f5330a;
        }
    }

    public final class l implements View.OnLayoutChangeListener {
        public l() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.ImageView, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            ImageView imageView = (ImageView) a0.this.a(R.id.top_area_online_status_indicator);
            kotlin.d.b.j.a((Object) imageView, "top_area_online_status_indicator");
            kotlin.d.b.j.a((Object) view, "v");
            kotlin.d.b.j.b(imageView, "indicator");
            kotlin.d.b.j.b(view, "companion");
            float width = (((float) view.getWidth()) / b.b.a.b.a(1)) / 3.3f;
            if (Float.compare(width, 14.0f) < 0) {
                width = 14.0f;
            } else if (Float.compare(width, 24.0f) > 0) {
                width = 24.0f;
            }
            float f = width * b.b.a.b.f16a;
            int width2 = view.getWidth() / 2;
            ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
            if (!(layoutParams instanceof ConstraintLayout.LayoutParams)) {
                layoutParams = null;
            }
            ConstraintLayout.LayoutParams layoutParams2 = (ConstraintLayout.LayoutParams) layoutParams;
            if (layoutParams2 == null || width2 != layoutParams2.circleRadius) {
                imageView.post(new y(imageView, layoutParams2, f, width2));
            }
        }
    }

    public final class m extends AnimatorListenerAdapter {

        /* renamed from: a  reason: collision with root package name */
        public boolean f156a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ View f157b;
        public final /* synthetic */ int c;

        public m(View view, float f, long j, int i) {
            this.f157b = view;
            this.c = i;
        }

        public final void onAnimationCancel(Animator animator) {
            this.f156a = true;
        }

        public final void onAnimationEnd(Animator animator) {
            kotlin.d.b.j.b(animator, "animation");
            this.f157b.setVisibility(this.c);
        }
    }

    public final class n extends AnimatorListenerAdapter {

        /* renamed from: a  reason: collision with root package name */
        public boolean f158a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ ImageView f159b;
        public final /* synthetic */ int c;

        public n(ImageView imageView, float f, long j, int i) {
            this.f159b = imageView;
            this.c = i;
        }

        public final void onAnimationCancel(Animator animator) {
            this.f158a = true;
        }

        public final void onAnimationEnd(Animator animator) {
            kotlin.d.b.j.b(animator, "animation");
            this.f159b.setVisibility(this.c);
        }
    }

    public final View a(int i2) {
        if (this.t == null) {
            this.t = new HashMap();
        }
        View view = (View) this.t.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i2);
        this.t.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.t;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public final void a(ViewPager viewPager) {
        FrameLayout frameLayout = (FrameLayout) a(R.id.topAreaTabBarView);
        if (frameLayout != null) {
            kotlin.g.c b2 = kotlin.g.d.b(0, frameLayout.getChildCount());
            ArrayList arrayList = new ArrayList();
            Iterator it = b2.iterator();
            while (it.hasNext()) {
                View childAt = frameLayout.getChildAt(((ah) it).a());
                if (childAt != null) {
                    arrayList.add(childAt);
                }
            }
            b.b.a.b.a(getContext(), arrayList, b.b.a.i.r1.n.s.a());
            b.b.a.b.a(getContext(), arrayList, d.f145a, viewPager, new c(viewPager));
        }
    }

    public final void a(View view, g.a aVar, boolean z) {
        kotlin.d.b.j.b(view, "view");
        kotlin.d.b.j.b(aVar, "animation");
        super.a(view, aVar, z);
        q1.a(view, new b(aVar));
    }

    public final void a(Integer num) {
        this.n = num;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.b.a(android.content.Context, java.util.List<? extends android.view.View>, java.util.List<b.b.a.j.c1>, int, boolean):void
     arg types: [android.content.Context, java.util.ArrayList, java.util.List<b.b.a.j.c1>, int, int]
     candidates:
      b.b.a.b.a(android.content.Context, java.util.List, int, boolean, int):void
      b.b.a.b.a(android.content.Context, java.util.List<? extends android.view.View>, kotlin.d.a.a<? extends java.util.List<b.b.a.j.c1>>, android.support.v4.view.ViewPager, kotlin.d.a.c<? super android.view.View, ? super java.lang.Integer, kotlin.m>):void
      b.b.a.b.a(android.view.View, boolean, boolean, int, int):void
      b.b.a.b.a(android.content.Context, java.util.List<? extends android.view.View>, java.util.List<b.b.a.j.c1>, int, boolean):void */
    public final void b(int i2) {
        FrameLayout frameLayout = (FrameLayout) a(R.id.topAreaTabBarView);
        if (frameLayout != null) {
            kotlin.g.c b2 = kotlin.g.d.b(0, frameLayout.getChildCount());
            ArrayList arrayList = new ArrayList();
            Iterator it = b2.iterator();
            while (it.hasNext()) {
                View childAt = frameLayout.getChildAt(((ah) it).a());
                if (childAt != null) {
                    arrayList.add(childAt);
                }
            }
            this.m = i2;
            ImageButton imageButton = (ImageButton) a(R.id.top_area_settings_button);
            boolean z = true;
            if (imageButton != null) {
                imageButton.setSelected(i2 == -1);
            }
            ImageButton imageButton2 = (ImageButton) a(R.id.top_area_messages_button);
            if (imageButton2 != null) {
                if (i2 != -2) {
                    z = false;
                }
                imageButton2.setSelected(z);
            }
            b.b.a.b.a(getContext(), (List<? extends View>) arrayList, b.b.a.i.r1.n.s.a(), i2, false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kotlin.d.b.j.b(layoutInflater, "inflater");
        View inflate = layoutInflater.inflate(R.layout.fragment_top_area_logged_in_landscape, viewGroup, false);
        this.p = false;
        return inflate;
    }

    public final void onDestroyView() {
        SupercellId.INSTANCE.getSharedServices$supercellId_release().e().c(this.s);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f().c(this.r);
        super.onDestroyView();
        a();
    }

    public final void onSaveInstanceState(Bundle bundle) {
        kotlin.d.b.j.b(bundle, "outState");
        bundle.putInt("selectedTab", this.m);
        String str = this.o;
        if (str != null) {
            bundle.putString("avatarImage", str);
        }
        super.onSaveInstanceState(bundle);
    }

    public final void onStart() {
        super.onStart();
        b(this.m);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        kotlin.d.b.j.b(view, "view");
        super.onViewCreated(view, bundle);
        ImageButton imageButton = (ImageButton) a(R.id.top_area_settings_button);
        if (imageButton != null) {
            imageButton.setOnClickListener(new g());
        }
        ImageButton imageButton2 = (ImageButton) a(R.id.top_area_messages_button);
        if (imageButton2 != null) {
            imageButton2.setOnClickListener(new h());
        }
        ImageButton imageButton3 = (ImageButton) a(R.id.my_code_info_button);
        if (imageButton3 != null) {
            imageButton3.setOnClickListener(new i());
        }
        ImageView imageView = (ImageView) a(R.id.top_area_profile_image);
        if (imageView != null) {
            imageView.setOnClickListener(new j());
        }
        ImageView imageView2 = (ImageView) a(R.id.top_area_profile_image);
        if (imageView2 != null) {
            imageView2.setSoundEffectsEnabled(false);
        }
        WidthAdjustingMultilineTextView widthAdjustingMultilineTextView = (WidthAdjustingMultilineTextView) a(R.id.top_area_profile_name);
        if (widthAdjustingMultilineTextView != null) {
            SupercellId.INSTANCE.getSharedServices$supercellId_release().j.a("AccountIcon.png", new k(new WeakReference(widthAdjustingMultilineTextView)));
        }
        a((ViewPager) null);
        Resources resources = getResources();
        kotlin.d.b.j.a((Object) resources, "resources");
        if (b.b.a.b.b(resources)) {
            int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.top_area_shadow_width);
            FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.root);
            if (frameLayout != null) {
                q1.b(frameLayout, -dimensionPixelSize);
                q1.c(frameLayout, q1.a(frameLayout) + dimensionPixelSize);
            }
            FrameLayout frameLayout2 = (FrameLayout) view.findViewById(R.id.top_area_profile_content);
            if (frameLayout2 != null) {
                q1.b(frameLayout2, -dimensionPixelSize);
                q1.c(frameLayout2, q1.a(frameLayout2) + dimensionPixelSize);
            }
            FrameLayout frameLayout3 = (FrameLayout) view.findViewById(R.id.topAreaTabBarView);
            if (frameLayout3 != null) {
                q1.b(frameLayout3, -dimensionPixelSize);
            }
            int dimensionPixelSize2 = view.getResources().getDimensionPixelSize(R.dimen.tab_button_inset_end);
            FrameLayout frameLayout4 = (FrameLayout) view.findViewById(R.id.topAreaTabBarView);
            if (frameLayout4 != null) {
                kotlin.g.c b2 = kotlin.g.d.b(0, frameLayout4.getChildCount());
                ArrayList<View> arrayList = new ArrayList<>();
                Iterator it = b2.iterator();
                while (it.hasNext()) {
                    View childAt = frameLayout4.getChildAt(((ah) it).a());
                    if (childAt != null) {
                        arrayList.add(childAt);
                    }
                }
                for (View b3 : arrayList) {
                    q1.b(b3, (-dimensionPixelSize2) + dimensionPixelSize);
                }
            }
        }
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f().b(this.r);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().e().b(this.s);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().e().a();
        ((ImageView) a(R.id.top_area_profile_image)).addOnLayoutChangeListener(new l());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewStateRestored(Bundle bundle) {
        super.onViewStateRestored(bundle);
        if (bundle != null) {
            if (bundle.containsKey("avatarImage")) {
                this.o = bundle.getString("avatarImage");
                k0 k0Var = k0.f1078b;
                String str = this.o;
                if (str == null) {
                    str = "";
                }
                Resources resources = getResources();
                kotlin.d.b.j.a((Object) resources, "resources");
                k0Var.a(str, (ImageView) a(R.id.top_area_profile_image), resources);
            }
            if (bundle.containsKey("selectedTab")) {
                this.m = bundle.getInt("selectedTab");
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.ViewPropertyAnimator, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0062, code lost:
        if ((android.support.v4.view.ViewCompat.getLayoutDirection(r8) == 1) != true) goto L_0x0064;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x006e, code lost:
        if (r8.c() == true) goto L_0x0070;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0070, code lost:
        r8 = getResources().getDimensionPixelSize(com.supercell.id.R.dimen.landscape_nav_area_height);
     */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00b4 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00b5  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b(boolean r24) {
        /*
            r23 = this;
            r0 = r23
            r1 = r24
            boolean r2 = r0.p
            com.supercell.id.ui.MainActivity r3 = b.b.a.b.a(r23)
            r4 = 1
            r5 = 0
            if (r3 == 0) goto L_0x003b
            java.util.List r3 = r3.a()
            if (r3 == 0) goto L_0x003b
            boolean r6 = r3.isEmpty()
            if (r6 == 0) goto L_0x001b
            goto L_0x003b
        L_0x001b:
            java.util.Iterator r3 = r3.iterator()
        L_0x001f:
            boolean r6 = r3.hasNext()
            if (r6 == 0) goto L_0x003b
            java.lang.Object r6 = r3.next()
            b.b.a.i.b$a r6 = (b.b.a.i.b.a) r6
            boolean r7 = r6 instanceof b.b.a.i.n1.a.C0036a
            if (r7 != 0) goto L_0x0036
            boolean r6 = r6 instanceof b.b.a.i.i1.a.C0026a
            if (r6 == 0) goto L_0x0034
            goto L_0x0036
        L_0x0034:
            r6 = 0
            goto L_0x0037
        L_0x0036:
            r6 = 1
        L_0x0037:
            if (r6 == 0) goto L_0x001f
            r3 = 1
            goto L_0x003c
        L_0x003b:
            r3 = 0
        L_0x003c:
            r0.p = r3
            int r3 = com.supercell.id.R.id.toolbarGuide
            android.view.View r3 = r0.a(r3)
            android.support.constraint.Placeholder r3 = (android.support.constraint.Placeholder) r3
            r6 = 2
            if (r3 == 0) goto L_0x00b0
            android.view.ViewGroup$MarginLayoutParams r7 = b.b.a.j.q1.d(r3)
            if (r7 == 0) goto L_0x00b0
            boolean r8 = r0.p
            if (r8 != 0) goto L_0x007b
            android.view.View r8 = r23.getView()
            if (r8 == 0) goto L_0x0064
            int r8 = android.support.v4.view.ViewCompat.getLayoutDirection(r8)
            if (r8 != r4) goto L_0x0061
            r8 = 1
            goto L_0x0062
        L_0x0061:
            r8 = 0
        L_0x0062:
            if (r8 == r4) goto L_0x0070
        L_0x0064:
            com.supercell.id.ui.MainActivity r8 = b.b.a.b.a(r23)
            if (r8 == 0) goto L_0x007b
            boolean r8 = r8.c()
            if (r8 != r4) goto L_0x007b
        L_0x0070:
            android.content.res.Resources r8 = r23.getResources()
            int r9 = com.supercell.id.R.dimen.landscape_nav_area_height
            int r8 = r8.getDimensionPixelSize(r9)
            goto L_0x007c
        L_0x007b:
            r8 = 0
        L_0x007c:
            int r9 = r7.topMargin
            if (r8 != r9) goto L_0x0081
            goto L_0x00b0
        L_0x0081:
            android.animation.ValueAnimator r10 = r0.q
            if (r10 == 0) goto L_0x0088
            r10.cancel()
        L_0x0088:
            r10 = 0
            r0.q = r10
            if (r1 == 0) goto L_0x00ab
            int[] r10 = new int[r6]
            r10[r5] = r9
            r10[r4] = r8
            android.animation.ValueAnimator r8 = android.animation.ValueAnimator.ofInt(r10)
            r9 = 350(0x15e, double:1.73E-321)
            android.animation.ValueAnimator r8 = r8.setDuration(r9)
            b.b.a.i.c0 r9 = new b.b.a.i.c0
            r9.<init>(r7, r3, r0, r1)
            r8.addUpdateListener(r9)
            r8.start()
            r0.q = r8
            goto L_0x00b0
        L_0x00ab:
            r7.topMargin = r8
            r3.setLayoutParams(r7)
        L_0x00b0:
            boolean r3 = r0.p
            if (r3 != r2) goto L_0x00b5
            return
        L_0x00b5:
            r2 = 6
            android.view.View[] r2 = new android.view.View[r2]
            int r3 = com.supercell.id.R.id.top_area_messages_button
            android.view.View r3 = r0.a(r3)
            android.widget.ImageButton r3 = (android.widget.ImageButton) r3
            r2[r5] = r3
            int r3 = com.supercell.id.R.id.top_area_messages_indicator_container
            android.view.View r3 = r0.a(r3)
            android.widget.FrameLayout r3 = (android.widget.FrameLayout) r3
            r2[r4] = r3
            int r3 = com.supercell.id.R.id.top_area_settings_button
            android.view.View r3 = r0.a(r3)
            android.widget.ImageButton r3 = (android.widget.ImageButton) r3
            r2[r6] = r3
            int r3 = com.supercell.id.R.id.topAreaTabBarDivider
            android.view.View r3 = r0.a(r3)
            r7 = 3
            r2[r7] = r3
            int r3 = com.supercell.id.R.id.topAreaTabBarBackground
            android.view.View r3 = r0.a(r3)
            r7 = 4
            r2[r7] = r3
            int r3 = com.supercell.id.R.id.topAreaTabBarView
            android.view.View r3 = r0.a(r3)
            android.widget.FrameLayout r3 = (android.widget.FrameLayout) r3
            r8 = 5
            r2[r8] = r3
            java.util.List r2 = kotlin.a.m.d(r2)
            android.widget.ImageView[] r3 = new android.widget.ImageView[r6]
            int r6 = com.supercell.id.R.id.my_code_info_button
            android.view.View r6 = r0.a(r6)
            android.widget.ImageButton r6 = (android.widget.ImageButton) r6
            r3[r5] = r6
            int r6 = com.supercell.id.R.id.qr_code
            android.view.View r6 = r0.a(r6)
            android.widget.ImageView r6 = (android.widget.ImageView) r6
            r3[r4] = r6
            java.util.List r3 = kotlin.a.m.d(r3)
            boolean r4 = r0.p
            if (r4 == 0) goto L_0x0117
            r4 = 4
            goto L_0x0118
        L_0x0117:
            r4 = 0
        L_0x0118:
            boolean r6 = r0.p
            if (r6 == 0) goto L_0x011d
            r7 = 0
        L_0x011d:
            r6 = 1065353216(0x3f800000, float:1.0)
            if (r1 == 0) goto L_0x01f6
            boolean r1 = r0.p
            r8 = 0
            if (r1 == 0) goto L_0x0128
            r1 = 0
            goto L_0x012a
        L_0x0128:
            r1 = 1065353216(0x3f800000, float:1.0)
        L_0x012a:
            boolean r9 = r0.p
            r10 = 0
            if (r9 == 0) goto L_0x0132
            r12 = r10
            goto L_0x0134
        L_0x0132:
            r12 = 175(0xaf, double:8.65E-322)
        L_0x0134:
            boolean r9 = r0.p
            if (r9 == 0) goto L_0x013b
            r9 = 1065353216(0x3f800000, float:1.0)
            goto L_0x013c
        L_0x013b:
            r9 = 0
        L_0x013c:
            boolean r8 = r0.p
            if (r8 == 0) goto L_0x0142
            r10 = 175(0xaf, double:8.65E-322)
        L_0x0142:
            java.util.Iterator r2 = r2.iterator()
        L_0x0146:
            boolean r8 = r2.hasNext()
            java.lang.String r14 = "setListener(object : Ani…d = true\n        }\n    })"
            java.lang.String r15 = "view.animate()\n         … .setInterpolator(linear)"
            if (r8 == 0) goto L_0x01a1
            java.lang.Object r8 = r2.next()
            android.view.View r8 = (android.view.View) r8
            r8.setVisibility(r5)
            float r5 = r6 - r1
            r8.setAlpha(r5)
            android.view.ViewPropertyAnimator r5 = r8.animate()
            android.view.ViewPropertyAnimator r5 = r5.alpha(r1)
            android.view.ViewPropertyAnimator r5 = r5.setStartDelay(r12)
            r18 = r7
            r6 = 175(0xaf, double:8.65E-322)
            android.view.ViewPropertyAnimator r5 = r5.setDuration(r6)
            android.view.animation.LinearInterpolator r6 = b.b.a.f.a.h
            android.view.ViewPropertyAnimator r5 = r5.setInterpolator(r6)
            kotlin.d.b.j.a(r5, r15)
            b.b.a.i.a0$m r6 = new b.b.a.i.a0$m
            r7 = r8
            r8 = r6
            r15 = r9
            r9 = r7
            r19 = r10
            r10 = r1
            r21 = r12
            r11 = r21
            r13 = r4
            r8.<init>(r9, r10, r11, r13)
            android.view.ViewPropertyAnimator r5 = r5.setListener(r6)
            kotlin.d.b.j.a(r5, r14)
            r5.start()
            r9 = r15
            r7 = r18
            r10 = r19
            r12 = r21
            r5 = 0
            r6 = 1065353216(0x3f800000, float:1.0)
            goto L_0x0146
        L_0x01a1:
            r18 = r7
            r6 = r9
            r19 = r10
            java.util.Iterator r1 = r3.iterator()
        L_0x01aa:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x022c
            java.lang.Object r2 = r1.next()
            r9 = r2
            android.widget.ImageView r9 = (android.widget.ImageView) r9
            r2 = 0
            r9.setVisibility(r2)
            r3 = 1065353216(0x3f800000, float:1.0)
            float r4 = r3 - r6
            r9.setAlpha(r4)
            android.view.ViewPropertyAnimator r3 = r9.animate()
            android.view.ViewPropertyAnimator r3 = r3.alpha(r6)
            r4 = r19
            android.view.ViewPropertyAnimator r3 = r3.setStartDelay(r4)
            r11 = 175(0xaf, double:8.65E-322)
            android.view.ViewPropertyAnimator r3 = r3.setDuration(r11)
            android.view.animation.LinearInterpolator r7 = b.b.a.f.a.h
            android.view.ViewPropertyAnimator r3 = r3.setInterpolator(r7)
            kotlin.d.b.j.a(r3, r15)
            b.b.a.i.a0$n r7 = new b.b.a.i.a0$n
            r8 = r7
            r10 = r6
            r16 = r11
            r11 = r4
            r13 = r18
            r8.<init>(r9, r10, r11, r13)
            android.view.ViewPropertyAnimator r3 = r3.setListener(r7)
            kotlin.d.b.j.a(r3, r14)
            r3.start()
            goto L_0x01aa
        L_0x01f6:
            r18 = r7
            java.util.Iterator r1 = r2.iterator()
        L_0x01fc:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x0211
            java.lang.Object r2 = r1.next()
            android.view.View r2 = (android.view.View) r2
            r2.setVisibility(r4)
            r5 = 1065353216(0x3f800000, float:1.0)
            r2.setAlpha(r5)
            goto L_0x01fc
        L_0x0211:
            r5 = 1065353216(0x3f800000, float:1.0)
            java.util.Iterator r1 = r3.iterator()
        L_0x0217:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x022c
            java.lang.Object r2 = r1.next()
            android.widget.ImageView r2 = (android.widget.ImageView) r2
            r7 = r18
            r2.setVisibility(r7)
            r2.setAlpha(r5)
            goto L_0x0217
        L_0x022c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.i.a0.b(boolean):void");
    }
}
