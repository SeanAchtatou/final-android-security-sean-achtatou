package b.b.a.i;

import android.animation.ValueAnimator;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import b.b.a.g.i;
import b.b.a.j.q1;
import com.supercell.id.R;
import kotlin.d.b.j;
import kotlin.e.a;

public final class k0 implements ValueAnimator.AnimatorUpdateListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ ValueAnimator f344a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ int f345b;
    public final /* synthetic */ float c;
    public final /* synthetic */ m0 d;

    public k0(ValueAnimator valueAnimator, int i, float f, m0 m0Var) {
        this.f344a = valueAnimator;
        this.f345b = i;
        this.c = f;
        this.d = m0Var;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.FrameLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        FrameLayout frameLayout = (FrameLayout) this.d.f376a.a(R.id.content);
        j.a((Object) frameLayout, "content");
        ViewGroup.MarginLayoutParams d2 = q1.d(frameLayout);
        if (d2 != null) {
            int i = this.f345b;
            d2.topMargin = a.a((((float) (this.d.f376a.j() - i)) * this.f344a.getAnimatedFraction()) + ((float) i));
        }
        i g = this.d.f376a.g();
        float f = this.c;
        g.a(((this.d.f376a.k() - f) * this.f344a.getAnimatedFraction()) + f);
        ((FrameLayout) this.d.f376a.a(R.id.content)).requestLayout();
    }
}
