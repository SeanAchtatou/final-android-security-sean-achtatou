package b.b.a.i.q1;

import android.graphics.drawable.BitmapDrawable;
import android.support.v4.widget.TextViewCompat;
import android.widget.TextView;
import b.b.a.i.q1.d;
import b.b.a.j.s0;
import com.supercell.id.R;
import kotlin.d.a.a;
import kotlin.d.b.k;
import kotlin.m;

public final class e extends k implements a<m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ s0.a f551a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ BitmapDrawable f552b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public e(s0.a aVar, d.b.c cVar, BitmapDrawable bitmapDrawable) {
        super(0);
        this.f551a = aVar;
        this.f552b = bitmapDrawable;
    }

    public final Object invoke() {
        TextViewCompat.setCompoundDrawablesRelative((TextView) this.f551a.c.findViewById(R.id.friend_playing_name_label), this.f552b, null, null, null);
        return m.f5330a;
    }
}
