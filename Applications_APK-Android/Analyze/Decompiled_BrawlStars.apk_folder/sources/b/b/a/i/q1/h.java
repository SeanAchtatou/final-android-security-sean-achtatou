package b.b.a.i.q1;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import b.b.a.b;
import b.b.a.g.d;
import kotlin.d.b.j;

public final class h extends RecyclerView.OnScrollListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ View f559a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ View f560b;
    public final /* synthetic */ FrameLayout c;
    public final /* synthetic */ d d;
    public final /* synthetic */ d e;

    public h(View view, View view2, FrameLayout frameLayout, d dVar, d dVar2) {
        this.f559a = view;
        this.f560b = view2;
        this.c = frameLayout;
        this.d = dVar;
        this.e = dVar2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onScrolled(RecyclerView recyclerView, int i, int i2) {
        j.b(recyclerView, "recyclerView");
        d dVar = this.e;
        View view = this.f559a;
        j.a((Object) view, "this@apply");
        View view2 = this.f560b;
        j.a((Object) view2, "selectAllBackground");
        dVar.a(view, view2, this.c, this.d, b.a(recyclerView, 0), i2);
    }
}
