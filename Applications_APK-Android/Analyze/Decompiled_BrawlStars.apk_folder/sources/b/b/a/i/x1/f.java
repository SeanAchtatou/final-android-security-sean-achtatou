package b.b.a.i.x1;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import b.b.a.i.x1.a;
import b.b.a.i.x1.e;
import com.facebook.share.internal.ShareConstants;
import com.supercell.id.SupercellId;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import kotlin.TypeCastException;
import kotlin.a.ai;
import kotlin.d.b.h;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.d.b.t;
import kotlin.h.d;
import kotlin.m;
import nl.komponents.kovenant.bb;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    public final Context f915a;

    /* renamed from: b  reason: collision with root package name */
    public final ConcurrentHashMap<String, String> f916b = new ConcurrentHashMap<>();
    public Map<String, String> c = ai.a();
    public final e d;
    public final a<Drawable> e;
    public final g f;
    public final d g;

    public final class a extends k implements kotlin.d.a.a<Map<String, ? extends String>> {
        public a() {
            super(0);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.content.SharedPreferences, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [java.util.Map<java.lang.String, ?>, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.content.Context, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final Map<String, String> invoke() {
            SharedPreferences sharedPreferences = f.this.f915a.getSharedPreferences("SupercellIdLocalAssetHashes", 0);
            j.a((Object) sharedPreferences, "sharedPreferences");
            Map<String, ?> all = sharedPreferences.getAll();
            j.a((Object) all, "sharedPreferences.all");
            LinkedHashMap linkedHashMap = new LinkedHashMap(ai.a(all.size()));
            for (Map.Entry entry : all.entrySet()) {
                Object key = entry.getKey();
                Object value = entry.getValue();
                if (value != null) {
                    linkedHashMap.put(key, (String) value);
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.String");
                }
            }
            LinkedHashMap linkedHashMap2 = new LinkedHashMap();
            for (Map.Entry entry2 : linkedHashMap.entrySet()) {
                a.C0091a aVar = a.d;
                Context context = f.this.f915a;
                j.a((Object) context, "context");
                File a2 = aVar.a(context);
                if (a2 != null ? new File(a2, (String) entry2.getKey()).exists() : false) {
                    linkedHashMap2.put(entry2.getKey(), entry2.getValue());
                }
            }
            return linkedHashMap2;
        }
    }

    public final /* synthetic */ class b extends h implements kotlin.d.a.b<Map<? extends String, ? extends String>, m> {
        public b(ConcurrentHashMap concurrentHashMap) {
            super(1, concurrentHashMap);
        }

        public final String getName() {
            return "putAll";
        }

        public final d getOwner() {
            return t.a(ConcurrentHashMap.class);
        }

        public final String getSignature() {
            return "putAll(Ljava/util/Map;)V";
        }

        public final Object invoke(Object obj) {
            Map map = (Map) obj;
            j.b(map, "p1");
            ((ConcurrentHashMap) this.receiver).putAll(map);
            return m.f5330a;
        }
    }

    public final class c extends a<Drawable> {
        public final String e;

        public c(Context context) {
            super(context);
            StringBuilder a2 = b.a.a.a.a.a("Android/");
            a2.append(f.this.d.f913a);
            this.e = a2.toString();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.content.Context, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final Object a(String str, InputStream inputStream) {
            j.b(str, "assetName");
            j.b(inputStream, ShareConstants.WEB_DIALOG_PARAM_DATA);
            TypedValue typedValue = new TypedValue();
            typedValue.density = f.this.d.a();
            Context context = f.this.f915a;
            j.a((Object) context, "context");
            Drawable createFromResourceStream = Drawable.createFromResourceStream(context.getResources(), typedValue, inputStream, str);
            if (createFromResourceStream != null) {
                return createFromResourceStream;
            }
            throw new IOException("Failed to parse " + str);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.Context, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public f(Context context) {
        j.b(context, "initialContext");
        this.f915a = context.getApplicationContext();
        e.a aVar = e.d;
        Context context2 = this.f915a;
        j.a((Object) context2, "context");
        Resources resources = context2.getResources();
        j.a((Object) resources, "context.resources");
        this.d = aVar.a(resources.getDisplayMetrics().densityDpi);
        Context context3 = this.f915a;
        j.a((Object) context3, "context");
        this.e = new c(context3);
        Context context4 = this.f915a;
        j.a((Object) context4, "context");
        this.f = new g(context4);
        Context context5 = this.f915a;
        j.a((Object) context5, "context");
        this.g = new d(context5);
        bb.f5389a.a(new b(this.f916b));
    }

    public final String a(String str) {
        j.b(str, "key");
        return this.f.c(str);
    }

    public final void a(String str, kotlin.d.a.b<? super String, m> bVar) {
        j.b(str, "key");
        j.b(bVar, "receiver");
        this.f.b(str, bVar);
    }

    public final void a(String str, kotlin.d.a.c<? super Drawable, ? super c, m> cVar) {
        j.b(str, "assetName");
        j.b(cVar, "receiver");
        a.a(this.e, str, cVar, null, 4, null);
    }

    public final String b(String str) {
        j.b(str, "key");
        return this.f.b(str);
    }

    public final void b() {
        this.e.f898b.clear();
    }

    public final String c() {
        String str = this.f.f918a;
        return str != null ? str : "en";
    }

    public final void c(String str) {
        j.b(str, "language");
        g.a(this.f, str, null, 2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.p.a(java.lang.Iterable, int):int
     arg types: [java.util.ArrayList, int]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.p.a(java.lang.Iterable, int):int */
    public final List<String> d() {
        Set<Map.Entry<String, String>> entrySet = this.c.entrySet();
        ArrayList<Map.Entry> arrayList = new ArrayList<>();
        for (T next : entrySet) {
            Map.Entry entry = (Map.Entry) next;
            String str = this.f916b.get(entry.getKey());
            boolean z = true;
            if (str != null && !(!j.a((Object) str, (Object) ((String) entry.getValue())))) {
                z = false;
            }
            if (z) {
                arrayList.add(next);
            }
        }
        ArrayList arrayList2 = new ArrayList(kotlin.a.m.a((Iterable) arrayList, 10));
        for (Map.Entry key : arrayList) {
            arrayList2.add((String) key.getKey());
        }
        return arrayList2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Set<java.util.Map$Entry<java.lang.String, java.lang.String>>, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void e() {
        SharedPreferences.Editor edit = this.f915a.getSharedPreferences("SupercellIdLocalAssetHashes", 0).edit();
        edit.clear();
        Set<Map.Entry<String, String>> entrySet = this.f916b.entrySet();
        j.a((Object) entrySet, "localAssets.entries");
        for (Map.Entry entry : entrySet) {
            edit.putString((String) entry.getKey(), (String) entry.getValue());
        }
        edit.apply();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [org.json.JSONObject, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00a8, code lost:
        if (r2 != null) goto L_0x00ad;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(org.json.JSONObject r11) {
        /*
            r10 = this;
            java.lang.String r0 = "hashes"
            kotlin.d.b.j.b(r11, r0)
            java.lang.String r0 = "RemoteAssets"
            org.json.JSONObject r11 = r11.getJSONObject(r0)
            java.lang.String r0 = "assetsDirectory"
            kotlin.d.b.j.a(r11, r0)
            java.lang.String r0 = "Android"
            org.json.JSONObject r0 = r11.optJSONObject(r0)
            r1 = 0
            if (r0 == 0) goto L_0x0022
            b.b.a.i.x1.e r2 = r10.d
            java.lang.String r2 = r2.f913a
            org.json.JSONObject r2 = r0.optJSONObject(r2)
            goto L_0x0023
        L_0x0022:
            r2 = r1
        L_0x0023:
            r3 = 47
            if (r2 == 0) goto L_0x006f
            java.util.Iterator r4 = r2.keys()
            if (r4 == 0) goto L_0x006f
            kotlin.i.h r4 = kotlin.i.i.a(r4)
            if (r4 == 0) goto L_0x006f
            java.util.LinkedHashMap r5 = new java.util.LinkedHashMap
            r5.<init>()
            java.util.Iterator r4 = r4.a()
        L_0x003c:
            boolean r6 = r4.hasNext()
            if (r6 == 0) goto L_0x0070
            java.lang.Object r6 = r4.next()
            java.lang.String r6 = (java.lang.String) r6
            java.lang.String r7 = "/Android/"
            java.lang.StringBuilder r7 = b.a.a.a.a.a(r7)
            b.b.a.i.x1.e r8 = r10.d
            java.lang.String r8 = r8.f913a
            r7.append(r8)
            r7.append(r3)
            r7.append(r6)
            java.lang.String r7 = r7.toString()
            java.lang.String r6 = r2.getString(r6)
            kotlin.h r6 = kotlin.k.a(r7, r6)
            A r7 = r6.f5262a
            B r6 = r6.f5263b
            r5.put(r7, r6)
            goto L_0x003c
        L_0x006f:
            r5 = r1
        L_0x0070:
            java.lang.String r2 = "Localizations"
            org.json.JSONObject r11 = r11.optJSONObject(r2)
            b.b.a.i.x1.g r2 = r10.f
            java.lang.String r2 = r2.f918a
            if (r2 == 0) goto L_0x00ab
            if (r11 == 0) goto L_0x00a7
            java.util.Iterator r4 = r11.keys()
            if (r4 == 0) goto L_0x00a7
            kotlin.i.h r4 = kotlin.i.i.a(r4)
            if (r4 == 0) goto L_0x00a7
            java.util.Iterator r4 = r4.a()
        L_0x008e:
            boolean r6 = r4.hasNext()
            if (r6 == 0) goto L_0x00a2
            java.lang.Object r6 = r4.next()
            r7 = r6
            java.lang.String r7 = (java.lang.String) r7
            boolean r7 = kotlin.d.b.j.a(r7, r2)
            if (r7 == 0) goto L_0x008e
            goto L_0x00a3
        L_0x00a2:
            r6 = r1
        L_0x00a3:
            r2 = r6
            java.lang.String r2 = (java.lang.String) r2
            goto L_0x00a8
        L_0x00a7:
            r2 = r1
        L_0x00a8:
            if (r2 == 0) goto L_0x00ab
            goto L_0x00ad
        L_0x00ab:
            java.lang.String r2 = "en"
        L_0x00ad:
            java.lang.String r4 = "language"
            kotlin.d.b.j.b(r2, r4)
            b.b.a.i.x1.g r4 = r10.f
            r6 = 2
            b.b.a.i.x1.g.a(r4, r2, r1, r6)
            if (r11 == 0) goto L_0x00bf
            org.json.JSONObject r11 = r11.optJSONObject(r2)
            goto L_0x00c0
        L_0x00bf:
            r11 = r1
        L_0x00c0:
            if (r11 == 0) goto L_0x010a
            java.util.Iterator r4 = r11.keys()
            if (r4 == 0) goto L_0x010a
            kotlin.i.h r4 = kotlin.i.i.a(r4)
            if (r4 == 0) goto L_0x010a
            java.util.LinkedHashMap r6 = new java.util.LinkedHashMap
            r6.<init>()
            java.util.Iterator r4 = r4.a()
        L_0x00d7:
            boolean r7 = r4.hasNext()
            if (r7 == 0) goto L_0x010b
            java.lang.Object r7 = r4.next()
            java.lang.String r7 = (java.lang.String) r7
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "/Localizations/"
            r8.append(r9)
            r8.append(r2)
            r8.append(r3)
            r8.append(r7)
            java.lang.String r8 = r8.toString()
            java.lang.String r7 = r11.getString(r7)
            kotlin.h r7 = kotlin.k.a(r8, r7)
            A r8 = r7.f5262a
            B r7 = r7.f5263b
            r6.put(r8, r7)
            goto L_0x00d7
        L_0x010a:
            r6 = r1
        L_0x010b:
            if (r0 == 0) goto L_0x0114
            java.lang.String r11 = "Audio"
            org.json.JSONObject r11 = r0.optJSONObject(r11)
            goto L_0x0115
        L_0x0114:
            r11 = r1
        L_0x0115:
            if (r11 == 0) goto L_0x0159
            java.util.Iterator r0 = r11.keys()
            if (r0 == 0) goto L_0x0159
            kotlin.i.h r0 = kotlin.i.i.a(r0)
            if (r0 == 0) goto L_0x0159
            java.util.LinkedHashMap r1 = new java.util.LinkedHashMap
            r1.<init>()
            java.util.Iterator r0 = r0.a()
        L_0x012c:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L_0x0159
            java.lang.Object r2 = r0.next()
            java.lang.String r2 = (java.lang.String) r2
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "/Android/Audio/"
            r3.append(r4)
            r3.append(r2)
            java.lang.String r3 = r3.toString()
            java.lang.String r2 = r11.getString(r2)
            kotlin.h r2 = kotlin.k.a(r3, r2)
            A r3 = r2.f5262a
            B r2 = r2.f5263b
            r1.put(r3, r2)
            goto L_0x012c
        L_0x0159:
            java.util.Map r11 = kotlin.a.ai.a()
            java.util.Map r11 = b.b.a.b.a(r11, r5)
            java.util.Map r11 = b.b.a.b.a(r11, r6)
            java.util.Map r11 = b.b.a.b.a(r11, r1)
            r10.c = r11
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.i.x1.f.a(org.json.JSONObject):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    public final void a(String str, String str2) {
        "Downloaded new asset " + str;
        String str3 = this.c.get(str);
        if (str3 == null || (!j.a((Object) str3, (Object) str2))) {
            j.b("Calculated hash for asset " + str + " is " + str2 + ", should be " + str3, ShareConstants.WEB_DIALOG_PARAM_MESSAGE);
        }
        this.f916b.put(str, str2);
        e();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.j.ac.b(java.lang.CharSequence, java.lang.String[], boolean, int):java.util.List<java.lang.String>
     arg types: [java.lang.String, java.lang.String[], int, int]
     candidates:
      kotlin.j.ac.b(java.lang.CharSequence, java.lang.String, int, boolean):int
      kotlin.j.ac.b(java.lang.CharSequence, java.lang.String[], boolean, int):java.util.List<java.lang.String> */
    public final void a(byte[] bArr, String str) {
        j.b(str, "assetPath");
        List<String> b2 = kotlin.j.t.b((CharSequence) str, new String[]{"/"}, false, 0);
        if (bArr != null) {
            try {
                if (b2.contains("Localizations") && b2.size() > 3) {
                    if (!this.f.a(b2.get(b2.indexOf("Localizations") + 1), bArr)) {
                        return;
                    }
                } else if (!b2.contains("Audio") || b2.size() <= 3) {
                    if (!b2.contains("Android") || b2.size() <= 3 || !this.e.a((String) kotlin.a.m.e((List) b2), bArr)) {
                        return;
                    }
                } else if (this.g.a((String) kotlin.a.m.e((List) b2), bArr)) {
                    File a2 = this.g.a((String) kotlin.a.m.e((List) b2));
                    if (a2 != null) {
                        SupercellId.INSTANCE.getSharedServices$supercellId_release().l.a(a2);
                    }
                } else {
                    return;
                }
                a(str, b.b.a.b.a(bArr));
            } catch (IOException e2) {
                f.class.getSimpleName();
                StringBuilder a3 = b.a.a.a.a.a("Storing remote asset failed ");
                a3.append(e2.getMessage());
                a3.toString();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.Context, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void a() {
        a.C0091a aVar = a.d;
        Context context = this.f915a;
        j.a((Object) context, "context");
        File a2 = aVar.a(context);
        if (a2 != null) {
            j.b(a2, "$this$deleteRecursively");
            j.b(a2, "$this$walkBottomUp");
            kotlin.io.e eVar = kotlin.io.e.BOTTOM_UP;
            j.b(a2, "$this$walk");
            j.b(eVar, "direction");
            Iterator a3 = new kotlin.io.c(a2, eVar).a();
            while (true) {
                boolean z = true;
                while (true) {
                    if (a3.hasNext()) {
                        File file = (File) a3.next();
                        if (file.delete() || !file.exists()) {
                            if (z) {
                            }
                        }
                        z = false;
                    } else {
                        b();
                        this.f.b();
                        this.f916b.clear();
                        e();
                        return;
                    }
                }
            }
        }
    }
}
