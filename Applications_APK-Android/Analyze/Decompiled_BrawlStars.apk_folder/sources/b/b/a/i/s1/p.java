package b.b.a.i.s1;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import b.b.a.h.m;
import b.b.a.i.a2.c;
import b.b.a.i.f1;
import b.b.a.i.h1.a;
import b.b.a.j.f0;
import b.b.a.j.y0;
import com.supercell.id.IdAccount;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.Checkbox;
import com.supercell.id.view.Switch;
import com.supercell.id.view.ViewAnimator;
import com.supercell.id.view.WidthAdjustingMultilineButton;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.a.ah;
import kotlin.j.t;

public final class p extends f1 {

    /* renamed from: b  reason: collision with root package name */
    public m f685b;
    public f0 c;
    public final y0<m> d = new y0<>(new i(), new j());
    public final Map<String, y0<m.b>> e = new LinkedHashMap();
    public HashMap f;

    public final class a implements View.OnClickListener {
        public a() {
        }

        public final void onClick(View view) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Settings", "click", "Log out", null, false, 24);
            FragmentActivity activity = p.this.getActivity();
            if (!(activity instanceof MainActivity)) {
                activity = null;
            }
            MainActivity mainActivity = (MainActivity) activity;
            if (mainActivity != null) {
                kotlin.d.b.j.b(mainActivity, "$this$showLogoutDialogPopup");
                mainActivity.a(b.b.a.i.f0.f.a("account_confirm_heading", "account_confirm_description", "account_confirm_btn_confirm", "account_confirm_btn_cancel"), "popupDialog");
            }
        }
    }

    public final class b implements View.OnClickListener {
        public b() {
        }

        public final void onClick(View view) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Settings", "click", "What is Supercell ID?", null, false, 24);
            MainActivity a2 = b.b.a.b.a(p.this);
            if (a2 != null) {
                a2.a(new c.a(false));
            }
        }
    }

    public final class c implements View.OnClickListener {
        public c() {
        }

        public final void onClick(View view) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Settings", "click", "FAQ", null, false, 24);
            MainActivity a2 = b.b.a.b.a(p.this);
            if (a2 != null) {
                a2.a(new a.c(true));
            }
        }
    }

    public final class d implements View.OnClickListener {
        public d() {
        }

        public final void onClick(View view) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Settings", "click", "Tutorial Video", null, false, 24);
            MainActivity a2 = b.b.a.b.a(p.this);
            if (a2 != null) {
                a2.a("https://youtu.be/VymLtGx_itc");
            }
        }
    }

    public final class e extends kotlin.d.b.k implements kotlin.d.a.a<kotlin.m> {
        public e() {
            super(0);
        }

        public final Object invoke() {
            SupercellId.INSTANCE.openSelfHelpPortal$supercellId_release();
            MainActivity a2 = b.b.a.b.a(p.this);
            if (a2 != null) {
                SupercellId.INSTANCE.dismiss$supercellId_release(a2);
            }
            return kotlin.m.f5330a;
        }
    }

    public final class f extends kotlin.d.b.k implements kotlin.d.a.a<kotlin.m> {
        public f() {
            super(0);
        }

        public final Object invoke() {
            MainActivity a2 = b.b.a.b.a(p.this);
            if (a2 != null) {
                String gameHelpLink = SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getGameHelpLink();
                if (gameHelpLink == null) {
                    kotlin.d.b.j.a();
                }
                a2.a(gameHelpLink);
            }
            return kotlin.m.f5330a;
        }
    }

    public final class g implements View.OnClickListener {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ kotlin.d.a.a f692a;

        public g(kotlin.d.a.a aVar) {
            this.f692a = aVar;
        }

        public final void onClick(View view) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Settings", "click", "Help & Support", null, false, 24);
            this.f692a.invoke();
        }
    }

    public final class h extends kotlin.d.b.k implements kotlin.d.a.a<kotlin.m> {
        public h() {
            super(0);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.FrameLayout, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final Object invoke() {
            ((FrameLayout) p.this.a(R.id.infoButtonTutorial)).setBackgroundResource(R.drawable.list_button_bottom);
            View a2 = p.this.a(R.id.infoSpacerHelp);
            kotlin.d.b.j.a((Object) a2, "infoSpacerHelp");
            a2.setVisibility(8);
            FrameLayout frameLayout = (FrameLayout) p.this.a(R.id.infoButtonHelp);
            kotlin.d.b.j.a((Object) frameLayout, "infoButtonHelp");
            frameLayout.setVisibility(8);
            return kotlin.m.f5330a;
        }
    }

    public final class i extends kotlin.d.b.k implements kotlin.d.a.b<m, kotlin.m> {
        public i() {
            super(1);
        }

        public final Object invoke(Object obj) {
            m mVar = (m) obj;
            kotlin.d.b.j.b(mVar, "response");
            p.this.f685b = mVar;
            p.this.f();
            return kotlin.m.f5330a;
        }
    }

    public final class j extends kotlin.d.b.k implements kotlin.d.a.b<Exception, kotlin.m> {
        public j() {
            super(1);
        }

        public final Object invoke(Object obj) {
            Exception exc = (Exception) obj;
            kotlin.d.b.j.b(exc, "it");
            p.a(p.this);
            MainActivity a2 = b.b.a.b.a(p.this);
            if (a2 != null) {
                a2.a(exc, (kotlin.d.a.b<? super b.b.a.i.e, kotlin.m>) null);
            }
            return kotlin.m.f5330a;
        }
    }

    public final class k extends kotlin.d.b.k implements kotlin.d.a.b<Boolean, kotlin.m> {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ m.b f697b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(m.b bVar) {
            super(1);
            this.f697b = bVar;
        }

        public final Object invoke(Object obj) {
            boolean booleanValue = ((Boolean) obj).booleanValue();
            p pVar = p.this;
            String str = this.f697b.f133a;
            Map<String, y0<m.b>> map = pVar.e;
            y0<m.b> y0Var = map.get(str);
            if (y0Var == null) {
                y0Var = new y0<>(new k(pVar, str), new l(pVar, str));
                map.put(str, y0Var);
            }
            y0Var.a(500, new r(str, booleanValue));
            b.b.a.c.b bVar = SupercellId.INSTANCE.getSharedServices$supercellId_release().f;
            StringBuilder a2 = b.a.a.a.a.a("Marketing scope consent: ");
            a2.append(this.f697b.f133a);
            b.b.a.c.b.a(bVar, "Settings", "click", a2.toString(), Long.valueOf(booleanValue ? 1 : 0), false, 16);
            return kotlin.m.f5330a;
        }
    }

    public final class l implements CompoundButton.OnCheckedChangeListener {
        public l() {
        }

        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            p.a(p.this, z);
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Settings", "click", "Accept marketing", Long.valueOf(z ? 1 : 0), false, 16);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.Switch, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.Checkbox, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final /* synthetic */ void a(p pVar) {
        m mVar;
        if (pVar.getView() != null && (mVar = pVar.f685b) != null) {
            ((Switch) pVar.a(R.id.generalSubscribeSwitch)).setOnCheckedChangeListener(null);
            Switch switchR = (Switch) pVar.a(R.id.generalSubscribeSwitch);
            kotlin.d.b.j.a((Object) switchR, "generalSubscribeSwitch");
            switchR.setChecked(mVar.f131a);
            ((Switch) pVar.a(R.id.generalSubscribeSwitch)).setOnCheckedChangeListener(new q(pVar));
            for (View view : pVar.e()) {
                TextView textView = (TextView) view.findViewById(R.id.titleTextView);
                kotlin.d.b.j.a((Object) textView, "it.titleTextView");
                textView.setEnabled(mVar.f131a);
                Checkbox checkbox = (Checkbox) view.findViewById(R.id.consentCheckBox);
                kotlin.d.b.j.a((Object) checkbox, "it.consentCheckBox");
                checkbox.setEnabled(mVar.f131a);
            }
        }
    }

    public final View a(int i2) {
        if (this.f == null) {
            this.f = new HashMap();
        }
        View view = (View) this.f.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i2);
        this.f.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.f;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void b(String str) {
        m mVar;
        T t;
        if (getView() != null && (mVar = this.f685b) != null) {
            Iterator<T> it = mVar.f132b.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                if (kotlin.d.b.j.a((Object) ((m.b) t).f133a, (Object) str)) {
                    break;
                }
            }
            m.b bVar = (m.b) t;
            if (bVar != null) {
                LinearLayout linearLayout = (LinearLayout) a(R.id.profileSubscriptionsContainer);
                kotlin.g.c b2 = kotlin.g.d.b(0, linearLayout.getChildCount());
                ArrayList arrayList = new ArrayList();
                Iterator it2 = b2.iterator();
                while (it2.hasNext()) {
                    View childAt = linearLayout.getChildAt(((ah) it2).a());
                    kotlin.d.b.j.a((Object) childAt, "it");
                    Object tag = childAt.getTag();
                    if (!((tag instanceof m.b) && kotlin.d.b.j.a(((m.b) tag).f133a, str))) {
                        childAt = null;
                    }
                    if (childAt != null) {
                        arrayList.add(childAt);
                    }
                }
                View view = (View) kotlin.a.m.d((List) arrayList);
                if (view != null) {
                    a(view, bVar);
                }
            }
        }
    }

    public final void c() {
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a("Settings");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final List<View> e() {
        LinearLayout linearLayout = (LinearLayout) a(R.id.profileSubscriptionsContainer);
        kotlin.g.c b2 = kotlin.g.d.b(0, linearLayout.getChildCount());
        ArrayList arrayList = new ArrayList();
        Iterator it = b2.iterator();
        while (it.hasNext()) {
            View childAt = linearLayout.getChildAt(((ah) it).a());
            kotlin.d.b.j.a((Object) childAt, "it");
            if (!(childAt.getTag() instanceof m.b)) {
                childAt = null;
            }
            if (childAt != null) {
                arrayList.add(childAt);
            }
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.Switch, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.LinearLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.Checkbox, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void f() {
        View view;
        ViewAnimator viewAnimator;
        View view2;
        if (getView() != null) {
            m mVar = this.f685b;
            f0 f0Var = this.c;
            if (mVar != null) {
                ((Switch) a(R.id.generalSubscribeSwitch)).setOnCheckedChangeListener(null);
                Switch switchR = (Switch) a(R.id.generalSubscribeSwitch);
                kotlin.d.b.j.a((Object) switchR, "generalSubscribeSwitch");
                switchR.setChecked(mVar.f131a);
                ((Switch) a(R.id.generalSubscribeSwitch)).setOnCheckedChangeListener(new l());
                List<View> e2 = e();
                int i2 = 0;
                for (T next : mVar.f132b) {
                    int i3 = i2 + 1;
                    if (i2 < 0) {
                        kotlin.a.m.a();
                    }
                    m.b bVar = (m.b) next;
                    if (i2 < 0 || i2 > kotlin.a.m.a((List) e2)) {
                        LinearLayout linearLayout = (LinearLayout) a(R.id.profileSubscriptionsContainer);
                        View inflate = LayoutInflater.from(linearLayout.getContext()).inflate(R.layout.fragment_profile_v1_list_item_subscription, (ViewGroup) ((LinearLayout) a(R.id.profileSubscriptionsContainer)), false);
                        linearLayout.addView(inflate);
                        kotlin.d.b.j.a((Object) inflate, "newScopeView()");
                        view2 = inflate;
                    } else {
                        view2 = e2.get(i2);
                    }
                    View view3 = view2;
                    kotlin.d.b.j.a((Object) view3, "this");
                    a(view3, bVar);
                    TextView textView = (TextView) view3.findViewById(R.id.titleTextView);
                    kotlin.d.b.j.a((Object) textView, "titleTextView");
                    textView.setEnabled(mVar.f131a);
                    Checkbox checkbox = (Checkbox) view3.findViewById(R.id.consentCheckBox);
                    kotlin.d.b.j.a((Object) checkbox, "consentCheckBox");
                    checkbox.setEnabled(mVar.f131a);
                    i2 = i3;
                }
                for (View removeView : kotlin.a.m.c(e2, mVar.f132b.size())) {
                    ((LinearLayout) a(R.id.profileSubscriptionsContainer)).removeView(removeView);
                }
                ((ViewAnimator) a(R.id.profileSubscriptionsViewAnimator)).setCurrentView((LinearLayout) a(R.id.profileSubscriptionsContainer));
                return;
            }
            if (f0Var != null) {
                TextView textView2 = (TextView) a(R.id.profileSubscriptionsErrorTextView);
                kotlin.d.b.j.a((Object) textView2, "profileSubscriptionsErrorTextView");
                b.b.a.i.x1.j.a(textView2, f0Var.f1037b, (kotlin.d.a.b) null, 2);
                viewAnimator = (ViewAnimator) a(R.id.profileSubscriptionsViewAnimator);
                view = (TextView) a(R.id.profileSubscriptionsErrorTextView);
            } else {
                viewAnimator = (ViewAnimator) a(R.id.profileSubscriptionsViewAnimator);
                view = (FrameLayout) a(R.id.profileSubscriptionsLoadingContainer);
            }
            viewAnimator.setCurrentView(view);
        }
    }

    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setRetainInstance(true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kotlin.d.b.j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_profile_v1_settings, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.LinearLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.ViewAnimator, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        kotlin.d.a.a aVar;
        kotlin.d.b.j.b(view, "view");
        TextView textView = (TextView) a(R.id.settingsEmailView);
        kotlin.d.b.j.a((Object) textView, "settingsEmailView");
        textView.setText(SupercellId.INSTANCE.getSharedServices$supercellId_release().d());
        TextView textView2 = (TextView) a(R.id.versionLabel);
        kotlin.d.b.j.a((Object) textView2, "versionLabel");
        textView2.setText("Version " + SupercellId.INSTANCE.getVersionString() + ' ' + SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getVersionSuffix());
        ((WidthAdjustingMultilineButton) a(R.id.logoutButton)).setOnClickListener(new a());
        LinearLayout linearLayout = (LinearLayout) a(R.id.profileSubscriptionsView);
        kotlin.d.b.j.a((Object) linearLayout, "profileSubscriptionsView");
        IdAccount idAccount = SupercellId.INSTANCE.getSharedServices$supercellId_release().i;
        String email = idAccount != null ? idAccount.getEmail() : null;
        linearLayout.setVisibility(email == null || t.a(email) ? 8 : 0);
        ViewAnimator viewAnimator = (ViewAnimator) a(R.id.profileSubscriptionsViewAnimator);
        kotlin.d.b.j.a((Object) viewAnimator, "profileSubscriptionsViewAnimator");
        viewAnimator.setSaveFromParentEnabled(false);
        ((FrameLayout) a(R.id.infoButtonSupercellId)).setOnClickListener(new b());
        ((FrameLayout) a(R.id.infoButtonFaq)).setOnClickListener(new c());
        ((FrameLayout) a(R.id.infoButtonTutorial)).setOnClickListener(new d());
        if (SupercellId.INSTANCE.isSelfHelpPortalAvailable$supercellId_release()) {
            aVar = new e();
        } else {
            String gameHelpLink = SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getGameHelpLink();
            aVar = (gameHelpLink == null || !b.b.a.b.a(gameHelpLink)) ? null : new f();
        }
        if (aVar != null) {
            ((FrameLayout) a(R.id.infoButtonHelp)).setOnClickListener(new g(aVar));
        } else {
            new h().invoke();
        }
        f();
        if (this.f685b == null) {
            this.c = null;
            WeakReference weakReference = new WeakReference(this);
            nl.komponents.kovenant.c.m.a(nl.komponents.kovenant.c.m.b(nl.komponents.kovenant.c.m.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().g.b(), new m(weakReference)), new n(weakReference)), new o(weakReference));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.Checkbox, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final /* synthetic */ void a(p pVar, boolean z) {
        if (pVar.getView() != null) {
            for (View view : pVar.e()) {
                TextView textView = (TextView) view.findViewById(R.id.titleTextView);
                kotlin.d.b.j.a((Object) textView, "it.titleTextView");
                textView.setEnabled(z);
                Checkbox checkbox = (Checkbox) view.findViewById(R.id.consentCheckBox);
                kotlin.d.b.j.a((Object) checkbox, "it.consentCheckBox");
                checkbox.setEnabled(z);
            }
            pVar.d.a(500, new s(z));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.p.a(java.lang.Iterable, int):int
     arg types: [java.util.List<b.b.a.h.m$b>, int]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.p.a(java.lang.Iterable, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void a(m.b bVar) {
        m mVar;
        m mVar2 = this.f685b;
        if (mVar2 != null) {
            List<m.b> list = mVar2.f132b;
            ArrayList arrayList = new ArrayList(kotlin.a.m.a((Iterable) list, 10));
            for (m.b bVar2 : list) {
                if (kotlin.d.b.j.a((Object) bVar2.f133a, (Object) bVar.f133a)) {
                    bVar2 = m.b.a(bVar2, null, null, bVar.c, 3);
                }
                arrayList.add(bVar2);
            }
            mVar = mVar2.a(mVar2.f131a, arrayList);
        } else {
            mVar = null;
        }
        this.f685b = mVar;
        if (getView() != null) {
            LinearLayout linearLayout = (LinearLayout) a(R.id.profileSubscriptionsContainer);
            kotlin.g.c b2 = kotlin.g.d.b(0, linearLayout.getChildCount());
            ArrayList arrayList2 = new ArrayList();
            Iterator it = b2.iterator();
            while (it.hasNext()) {
                View childAt = linearLayout.getChildAt(((ah) it).a());
                kotlin.d.b.j.a((Object) childAt, "it");
                Object tag = childAt.getTag();
                if (!((tag instanceof m.b) && kotlin.d.b.j.a(((m.b) tag).f133a, bVar.f133a))) {
                    childAt = null;
                }
                if (childAt != null) {
                    arrayList2.add(childAt);
                }
            }
            View view = (View) kotlin.a.m.d((List) arrayList2);
            if (view != null) {
                a(view, bVar);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.Checkbox, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final View a(View view, m.b bVar) {
        view.setTag(bVar);
        TextView textView = (TextView) view.findViewById(R.id.titleTextView);
        kotlin.d.b.j.a((Object) textView, "titleTextView");
        textView.setText(bVar.f134b);
        ((Checkbox) view.findViewById(R.id.consentCheckBox)).setOnCheckedChangeListener(null);
        Checkbox checkbox = (Checkbox) view.findViewById(R.id.consentCheckBox);
        kotlin.d.b.j.a((Object) checkbox, "consentCheckBox");
        checkbox.setChecked(bVar.c);
        ((Checkbox) view.findViewById(R.id.consentCheckBox)).setOnCheckedChangeListener(new k(bVar));
        return view;
    }
}
