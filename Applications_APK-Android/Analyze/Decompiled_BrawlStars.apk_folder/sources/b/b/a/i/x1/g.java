package b.b.a.i.x1;

import android.content.Context;
import b.b.a.j.y0;
import com.facebook.share.internal.ShareConstants;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import kotlin.a.ai;
import kotlin.a.an;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.i.h;
import kotlin.i.i;
import kotlin.i.q;
import kotlin.j.o;
import kotlin.j.t;
import kotlin.m;
import nl.komponents.kovenant.bb;

public final class g {
    public static final o g = new o("\"(.+)\" = \"(.*)\";");
    public static final a h = new a(null);

    /* renamed from: a  reason: collision with root package name */
    public String f918a;

    /* renamed from: b  reason: collision with root package name */
    public Map<String, Set<kotlin.d.a.b<String, m>>> f919b = new LinkedHashMap();
    public volatile Map<String, String> c = ai.a();
    public volatile Map<String, String> d = ai.a();
    public final y0<b> e = new y0<>(new f(), new C0093g());
    public final Context f;

    public static final class b {

        /* renamed from: a  reason: collision with root package name */
        public final Map<String, String> f923a;

        /* renamed from: b  reason: collision with root package name */
        public final Map<String, String> f924b;

        public b(Map<String, String> map, Map<String, String> map2) {
            j.b(map, "defaultEnglishCache");
            this.f923a = map;
            this.f924b = map2;
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return j.a(this.f923a, bVar.f923a) && j.a(this.f924b, bVar.f924b);
        }

        public final int hashCode() {
            Map<String, String> map = this.f923a;
            int i = 0;
            int hashCode = (map != null ? map.hashCode() : 0) * 31;
            Map<String, String> map2 = this.f924b;
            if (map2 != null) {
                i = map2.hashCode();
            }
            return hashCode + i;
        }

        public final String toString() {
            StringBuilder a2 = b.a.a.a.a.a("UpdateLanguageResult(defaultEnglishCache=");
            a2.append(this.f923a);
            a2.append(", cache=");
            a2.append(this.f924b);
            a2.append(")");
            return a2.toString();
        }
    }

    public final class c implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ Object f925a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ String f926b;
        public final /* synthetic */ String c;
        public final /* synthetic */ g d;

        public c(Object obj, String str, String str2, g gVar) {
            this.f925a = obj;
            this.f926b = str;
            this.c = str2;
            this.d = gVar;
        }

        public final void run() {
            kotlin.d.a.b bVar = (kotlin.d.a.b) this.f925a;
            this.d.getClass().getSimpleName();
            StringBuilder sb = new StringBuilder();
            sb.append("Localization callback from ");
            sb.append(this.f926b == null ? "defaults" : "remote server");
            sb.toString();
            bVar.invoke(this.c);
        }
    }

    public final class d extends k implements kotlin.d.a.a<m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ kotlin.d.a.b f927a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ String f928b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(kotlin.d.a.b bVar, String str) {
            super(0);
            this.f927a = bVar;
            this.f928b = str;
        }

        public final Object invoke() {
            this.f927a.invoke(this.f928b);
            return m.f5330a;
        }
    }

    public final class e extends k implements kotlin.d.a.a<b> {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ InputStream f930b;
        public final /* synthetic */ String c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(InputStream inputStream, String str) {
            super(0);
            this.f930b = inputStream;
            this.c = str;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.content.res.Resources, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0050, code lost:
            r2 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0051, code lost:
            kotlin.io.b.a(r1, r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0054, code lost:
            throw r2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x005d, code lost:
            r2 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x005e, code lost:
            kotlin.io.b.a(r0, r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x0061, code lost:
            throw r2;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invoke() {
            /*
                r4 = this;
                b.b.a.i.x1.g r0 = b.b.a.i.x1.g.this
                java.util.Map<java.lang.String, java.lang.String> r0 = r0.d
                boolean r1 = r0.isEmpty()
                r2 = 0
                if (r1 != 0) goto L_0x000c
                goto L_0x000d
            L_0x000c:
                r0 = r2
            L_0x000d:
                if (r0 == 0) goto L_0x0010
                goto L_0x0031
            L_0x0010:
                b.b.a.i.x1.g r0 = b.b.a.i.x1.g.this
                android.content.Context r0 = r0.f
                android.content.res.Resources r0 = r0.getResources()
                java.lang.String r1 = "context.resources"
                kotlin.d.b.j.a(r0, r1)
                android.content.res.AssetManager r0 = r0.getAssets()
                java.lang.String r1 = "defaultEnglish.strings"
                java.io.InputStream r0 = r0.open(r1)
                b.b.a.i.x1.g$a r1 = b.b.a.i.x1.g.h
                java.util.Map r1 = r1.a(r0)     // Catch:{ all -> 0x005b }
                kotlin.io.b.a(r0, r2)
                r0 = r1
            L_0x0031:
                java.io.InputStream r1 = r4.f930b
                if (r1 == 0) goto L_0x0036
                goto L_0x0041
            L_0x0036:
                b.b.a.i.x1.h r1 = new b.b.a.i.x1.h
                r1.<init>(r4)
                java.lang.Object r1 = r1.invoke()
                java.io.InputStream r1 = (java.io.InputStream) r1
            L_0x0041:
                if (r1 == 0) goto L_0x0055
                b.b.a.i.x1.g$a r3 = b.b.a.i.x1.g.h
                java.util.Map r3 = r3.a(r1)     // Catch:{ all -> 0x004e }
                kotlin.io.b.a(r1, r2)
                r2 = r3
                goto L_0x0055
            L_0x004e:
                r0 = move-exception
                throw r0     // Catch:{ all -> 0x0050 }
            L_0x0050:
                r2 = move-exception
                kotlin.io.b.a(r1, r0)
                throw r2
            L_0x0055:
                b.b.a.i.x1.g$b r1 = new b.b.a.i.x1.g$b
                r1.<init>(r0, r2)
                return r1
            L_0x005b:
                r1 = move-exception
                throw r1     // Catch:{ all -> 0x005d }
            L_0x005d:
                r2 = move-exception
                kotlin.io.b.a(r0, r1)
                throw r2
            */
            throw new UnsupportedOperationException("Method not decompiled: b.b.a.i.x1.g.e.invoke():java.lang.Object");
        }
    }

    public final class f extends k implements kotlin.d.a.b<b, m> {
        public f() {
            super(1);
        }

        public final Object invoke(Object obj) {
            b bVar = (b) obj;
            j.b(bVar, "result");
            g gVar = g.this;
            gVar.d = bVar.f923a;
            Map<String, String> map = bVar.f924b;
            if (map != null) {
                gVar.c = map;
            }
            g.this.a();
            return m.f5330a;
        }
    }

    /* renamed from: b.b.a.i.x1.g$g  reason: collision with other inner class name */
    public final class C0093g extends k implements kotlin.d.a.b<Exception, m> {
        public C0093g() {
            super(1);
        }

        public final Object invoke(Object obj) {
            j.b((Exception) obj, "e");
            g.this.getClass().getSimpleName();
            return m.f5330a;
        }
    }

    public g(Context context) {
        j.b(context, "context");
        this.f = context;
    }

    public static /* synthetic */ void a(g gVar, String str, InputStream inputStream, int i) {
        if ((i & 2) != 0) {
            inputStream = null;
        }
        gVar.a(str, inputStream);
    }

    public final File a(String str) {
        File a2 = a.d.a(this.f);
        if (a2 == null) {
            return null;
        }
        return new File(a2, "Localizations/" + str + "/SupercellID.strings");
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0044  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0057  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a() {
        /*
            r9 = this;
            java.util.Map<java.lang.String, java.util.Set<kotlin.d.a.b<java.lang.String, kotlin.m>>> r0 = r9.f919b
            monitor-enter(r0)
            java.util.Map<java.lang.String, java.util.Set<kotlin.d.a.b<java.lang.String, kotlin.m>>> r1 = r9.f919b     // Catch:{ all -> 0x00be }
            java.util.Set r1 = r1.entrySet()     // Catch:{ all -> 0x00be }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ all -> 0x00be }
        L_0x000d:
            boolean r2 = r1.hasNext()     // Catch:{ all -> 0x00be }
            if (r2 == 0) goto L_0x00ba
            java.lang.Object r2 = r1.next()     // Catch:{ all -> 0x00be }
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2     // Catch:{ all -> 0x00be }
            java.util.Map<java.lang.String, java.lang.String> r3 = r9.c     // Catch:{ all -> 0x00be }
            java.lang.Object r4 = r2.getKey()     // Catch:{ all -> 0x00be }
            java.lang.Object r3 = r3.get(r4)     // Catch:{ all -> 0x00be }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ all -> 0x00be }
            r4 = 0
            r5 = 1
            if (r3 == 0) goto L_0x0032
            int r6 = r3.length()     // Catch:{ all -> 0x00be }
            if (r6 != 0) goto L_0x0030
            goto L_0x0032
        L_0x0030:
            r6 = 0
            goto L_0x0033
        L_0x0032:
            r6 = 1
        L_0x0033:
            if (r6 == 0) goto L_0x0044
            java.util.Map<java.lang.String, java.lang.String> r6 = r9.d     // Catch:{ all -> 0x00be }
            java.lang.Object r7 = r2.getKey()     // Catch:{ all -> 0x00be }
            java.lang.Object r6 = r6.get(r7)     // Catch:{ all -> 0x00be }
            java.lang.String r6 = (java.lang.String) r6     // Catch:{ all -> 0x00be }
            if (r6 == 0) goto L_0x000d
            goto L_0x0045
        L_0x0044:
            r6 = r3
        L_0x0045:
            java.lang.Object r2 = r2.getValue()     // Catch:{ all -> 0x00be }
            java.util.Set r2 = (java.util.Set) r2     // Catch:{ all -> 0x00be }
            if (r2 == 0) goto L_0x0053
            boolean r7 = r2.isEmpty()     // Catch:{ all -> 0x00be }
            if (r7 == 0) goto L_0x0054
        L_0x0053:
            r4 = 1
        L_0x0054:
            if (r4 == 0) goto L_0x0057
            goto L_0x000d
        L_0x0057:
            android.os.Looper r4 = android.os.Looper.getMainLooper()     // Catch:{ all -> 0x00be }
            android.os.Looper r5 = android.os.Looper.myLooper()     // Catch:{ all -> 0x00be }
            boolean r5 = kotlin.d.b.j.a(r5, r4)     // Catch:{ all -> 0x00be }
            if (r5 == 0) goto L_0x0097
            java.util.Iterator r4 = r2.iterator()     // Catch:{ all -> 0x00be }
        L_0x0069:
            boolean r5 = r4.hasNext()     // Catch:{ all -> 0x00be }
            if (r5 == 0) goto L_0x00b3
            java.lang.Object r5 = r4.next()     // Catch:{ all -> 0x00be }
            kotlin.d.a.b r5 = (kotlin.d.a.b) r5     // Catch:{ all -> 0x00be }
            java.lang.Class r7 = r9.getClass()     // Catch:{ all -> 0x00be }
            r7.getSimpleName()     // Catch:{ all -> 0x00be }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x00be }
            r7.<init>()     // Catch:{ all -> 0x00be }
            java.lang.String r8 = "Localization callback from "
            r7.append(r8)     // Catch:{ all -> 0x00be }
            if (r3 != 0) goto L_0x008b
            java.lang.String r8 = "defaults"
            goto L_0x008d
        L_0x008b:
            java.lang.String r8 = "remote server"
        L_0x008d:
            r7.append(r8)     // Catch:{ all -> 0x00be }
            r7.toString()     // Catch:{ all -> 0x00be }
            r5.invoke(r6)     // Catch:{ all -> 0x00be }
            goto L_0x0069
        L_0x0097:
            android.os.Handler r5 = new android.os.Handler     // Catch:{ all -> 0x00be }
            r5.<init>(r4)     // Catch:{ all -> 0x00be }
            java.util.Iterator r4 = r2.iterator()     // Catch:{ all -> 0x00be }
        L_0x00a0:
            boolean r7 = r4.hasNext()     // Catch:{ all -> 0x00be }
            if (r7 == 0) goto L_0x00b3
            java.lang.Object r7 = r4.next()     // Catch:{ all -> 0x00be }
            b.b.a.i.x1.g$c r8 = new b.b.a.i.x1.g$c     // Catch:{ all -> 0x00be }
            r8.<init>(r7, r3, r6, r9)     // Catch:{ all -> 0x00be }
            r5.post(r8)     // Catch:{ all -> 0x00be }
            goto L_0x00a0
        L_0x00b3:
            if (r3 == 0) goto L_0x000d
            r2.clear()     // Catch:{ all -> 0x00be }
            goto L_0x000d
        L_0x00ba:
            kotlin.m r1 = kotlin.m.f5330a     // Catch:{ all -> 0x00be }
            monitor-exit(r0)
            return
        L_0x00be:
            r1 = move-exception
            monitor-exit(r0)
            goto L_0x00c2
        L_0x00c1:
            throw r1
        L_0x00c2:
            goto L_0x00c1
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.i.x1.g.a():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    public final void a(String str, InputStream inputStream) {
        j.b(str, "language");
        if (!j.a((Object) str, (Object) this.f918a)) {
            this.f918a = str;
            this.c = ai.a();
        }
        this.e.a(bb.f5389a);
    }

    public final void a(String str, kotlin.d.a.b<? super String, m> bVar) {
        synchronized (this.f919b) {
            if (this.f919b.containsKey(str)) {
                Set<kotlin.d.a.b<String, m>> set = this.f919b.get(str);
                if (set == null) {
                    j.a();
                }
                Boolean.valueOf(set.add(bVar));
            } else {
                this.f919b.put(str, an.b(bVar));
                m mVar = m.f5330a;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.io.File, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final boolean a(String str, byte[] bArr) {
        j.b(str, "language");
        j.b(bArr, ShareConstants.WEB_DIALOG_PARAM_DATA);
        File a2 = a(str);
        if (a2 == null) {
            return false;
        }
        File parentFile = a2.getParentFile();
        j.a((Object) parentFile, "directory");
        if (!parentFile.isDirectory() && !parentFile.mkdirs()) {
            return false;
        }
        try {
            kotlin.io.f.a(a2, bArr);
            a(str, new ByteArrayInputStream(bArr));
            return true;
        } catch (IOException e2) {
            a2.delete();
            throw e2;
        } catch (Throwable th) {
            a(str, new ByteArrayInputStream(bArr));
            throw th;
        }
    }

    public final String b(String str) {
        j.b(str, "key");
        String str2 = this.c.get(str);
        return str2 == null || str2.length() == 0 ? this.d.get(str) : str2;
    }

    public final void b() {
        this.f918a = null;
        this.e.a(bb.f5389a);
    }

    public final void b(String str, kotlin.d.a.b<? super String, m> bVar) {
        String str2;
        j.b(str, "key");
        j.b(bVar, "receiver");
        String str3 = this.c.get(str);
        if (str3 == null || str3.length() == 0) {
            str2 = this.d.get(str);
            if (str2 == null) {
                str2 = "";
            }
        } else {
            str2 = str3;
        }
        b.b.a.b.a(new d(bVar, str2));
        if (str3 == null) {
            a(str, bVar);
        }
    }

    public final String c(String str) {
        j.b(str, "key");
        return this.d.get(str);
    }

    public static final class a {

        /* renamed from: b.b.a.i.x1.g$a$a  reason: collision with other inner class name */
        public final class C0092a extends k implements kotlin.d.a.b<String, Boolean> {

            /* renamed from: a  reason: collision with root package name */
            public static final C0092a f920a = new C0092a();

            public C0092a() {
                super(1);
            }

            public final boolean a(String str) {
                j.b(str, "it");
                return !t.a((CharSequence) str);
            }

            public final /* synthetic */ Object invoke(Object obj) {
                return Boolean.valueOf(a((String) obj));
            }
        }

        public final class b extends k implements kotlin.d.a.b<String, kotlin.j.j> {

            /* renamed from: a  reason: collision with root package name */
            public static final b f921a = new b();

            public b() {
                super(1);
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
             arg types: [java.util.regex.Matcher, java.lang.String]
             candidates:
              kotlin.d.b.j.a(int, int):int
              kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
              kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
              kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
            public final Object invoke(Object obj) {
                String str = (String) obj;
                j.b(str, "it");
                o oVar = g.g;
                j.b(str, "input");
                Matcher matcher = oVar.f5321a.matcher(str);
                j.a((Object) matcher, "nativePattern.matcher(input)");
                if (!matcher.matches()) {
                    return null;
                }
                return new kotlin.j.k(matcher, str);
            }
        }

        public final class c extends k implements kotlin.d.a.b<kotlin.j.j, Boolean> {

            /* renamed from: a  reason: collision with root package name */
            public static final c f922a = new c();

            public c() {
                super(1);
            }

            public final Object invoke(Object obj) {
                kotlin.j.j jVar = (kotlin.j.j) obj;
                j.b(jVar, "it");
                return Boolean.valueOf(jVar.b().size() == 3);
            }
        }

        public /* synthetic */ a(kotlin.d.b.g gVar) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.j.ab.a(java.lang.String, java.lang.String, java.lang.String, boolean):java.lang.String
         arg types: [java.lang.String, java.lang.String, java.lang.String, int]
         candidates:
          kotlin.j.ac.a(java.lang.CharSequence, char, int, boolean):int
          kotlin.j.ac.a(java.lang.CharSequence, java.lang.String, int, boolean):int
          kotlin.j.ac.a(java.lang.CharSequence, char[], int, boolean):int
          kotlin.j.ac.a(java.lang.CharSequence, java.lang.String, boolean, int):java.util.List<java.lang.String>
          kotlin.j.ac.a(java.lang.CharSequence, java.lang.String[], boolean, int):kotlin.i.h<java.lang.String>
          kotlin.j.ab.a(java.lang.String, java.lang.String, java.lang.String, boolean):java.lang.String */
        public final Map<String, String> a(InputStream inputStream) {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, kotlin.j.d.f5307a), 8192);
            j.b(bufferedReader, "$this$lineSequence");
            h b2 = i.b(i.a(i.a(new kotlin.io.k(bufferedReader)), C0092a.f920a), b.f921a);
            j.b(b2, "$this$filterNotNull");
            kotlin.d.a.b bVar = q.f5280a;
            j.b(b2, "$this$filterNot");
            j.b(bVar, "predicate");
            h a2 = i.a(new kotlin.i.d(b2, false, bVar), c.f922a);
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            Iterator a3 = a2.a();
            while (a3.hasNext()) {
                kotlin.j.j jVar = (kotlin.j.j) a3.next();
                kotlin.h a4 = kotlin.k.a(jVar.b().get(1), t.a(t.a(jVar.b().get(2), "\\\"", "\"", false), "\\n", "\n", false));
                linkedHashMap.put(a4.f5262a, a4.f5263b);
            }
            return linkedHashMap;
        }
    }
}
