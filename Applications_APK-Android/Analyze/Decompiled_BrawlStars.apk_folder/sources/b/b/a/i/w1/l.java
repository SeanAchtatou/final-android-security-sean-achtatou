package b.b.a.i.w1;

import b.b.a.h.o;
import com.facebook.share.internal.ShareConstants;
import java.lang.ref.WeakReference;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class l extends k implements b<o.a, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f886a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l(WeakReference weakReference) {
        super(1);
        this.f886a = weakReference;
    }

    public final Object invoke(Object obj) {
        o.a aVar = (o.a) obj;
        j.b(aVar, ShareConstants.WEB_DIALOG_PARAM_DATA);
        k kVar = (k) this.f886a.get();
        if (kVar != null) {
            kVar.a(aVar.a(), aVar.f138a);
        }
        return m.f5330a;
    }
}
