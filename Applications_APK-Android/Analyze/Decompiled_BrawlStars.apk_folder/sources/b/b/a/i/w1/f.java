package b.b.a.i.w1;

import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import b.b.a.i.u;
import b.b.a.i.v;
import b.b.a.j.n;
import b.b.a.j.q1;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.Checkbox;
import com.supercell.id.view.WidthAdjustingMultilineButton;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import kotlin.TypeCastException;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.j.t;
import nl.komponents.kovenant.c.m;

public final class f extends o implements u {
    public String c = "";
    public boolean d;
    public boolean e;
    public HashMap f;

    public final class a implements TextWatcher {
        public a() {
        }

        public final void afterTextChanged(Editable editable) {
            f.this.j();
        }

        public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    public final class b implements View.OnFocusChangeListener {

        public final class a implements Runnable {

            /* renamed from: b.b.a.i.w1.f$b$a$a  reason: collision with other inner class name */
            public final class C0082a extends k implements kotlin.d.a.b<View, Boolean> {

                /* renamed from: a  reason: collision with root package name */
                public static final C0082a f841a = new C0082a();

                public C0082a() {
                    super(1);
                }

                public final Object invoke(Object obj) {
                    View view = (View) obj;
                    j.b(view, "it");
                    return Boolean.valueOf(view instanceof EditText);
                }
            }

            public a() {
            }

            public final void run() {
                n i = f.this.i();
                if (i != null) {
                    i.a(C0082a.f841a);
                }
            }
        }

        public b() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.ScrollView, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onFocusChange(View view, boolean z) {
            if (z) {
                ScrollView scrollView = (ScrollView) f.this.a(R.id.registerEnterEmailScrollView);
                j.a((Object) scrollView, "registerEnterEmailScrollView");
                j.a((Object) view, "v");
                q1.a(scrollView, view);
                return;
            }
            view.post(new a());
        }
    }

    public final class c implements View.OnFocusChangeListener {

        public final class a implements Runnable {

            /* renamed from: b.b.a.i.w1.f$c$a$a  reason: collision with other inner class name */
            public final class C0083a extends k implements kotlin.d.a.b<View, Boolean> {

                /* renamed from: a  reason: collision with root package name */
                public static final C0083a f844a = new C0083a();

                public C0083a() {
                    super(1);
                }

                public final Object invoke(Object obj) {
                    View view = (View) obj;
                    j.b(view, "it");
                    return Boolean.valueOf(view instanceof EditText);
                }
            }

            public a() {
            }

            public final void run() {
                n i = f.this.i();
                if (i != null) {
                    i.a(C0083a.f844a);
                }
            }
        }

        public c() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.ScrollView, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onFocusChange(View view, boolean z) {
            if (z) {
                ScrollView scrollView = (ScrollView) f.this.a(R.id.registerEnterEmailScrollView);
                j.a((Object) scrollView, "registerEnterEmailScrollView");
                j.a((Object) view, "v");
                q1.a(scrollView, view);
                return;
            }
            view.post(new a());
        }
    }

    public final class d implements TextWatcher {
        public d() {
        }

        public final void afterTextChanged(Editable editable) {
            f.this.j();
        }

        public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    public final class e implements View.OnClickListener {
        public e() {
        }

        public final void onClick(View view) {
            ((Checkbox) f.this.a(R.id.acceptMarketingCheckBox)).toggle();
        }
    }

    /* renamed from: b.b.a.i.w1.f$f  reason: collision with other inner class name */
    public final class C0084f implements View.OnClickListener {
        public C0084f() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) f.this.a(R.id.okButton);
            j.a((Object) widthAdjustingMultilineButton, "okButton");
            widthAdjustingMultilineButton.setEnabled(false);
            WidthAdjustingMultilineButton widthAdjustingMultilineButton2 = (WidthAdjustingMultilineButton) f.this.a(R.id.cancelButton);
            j.a((Object) widthAdjustingMultilineButton2, "cancelButton");
            widthAdjustingMultilineButton2.setEnabled(false);
            SupercellId.INSTANCE.clearPendingRegistration$supercellId_release();
            MainActivity a2 = b.b.a.b.a(f.this);
            if (a2 != null) {
                a2.d();
            }
        }
    }

    public final class g implements View.OnClickListener {
        public g() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.Checkbox, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            MainActivity a2;
            String str;
            WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) f.this.a(R.id.okButton);
            j.a((Object) widthAdjustingMultilineButton, "okButton");
            boolean z = false;
            widthAdjustingMultilineButton.setEnabled(false);
            WidthAdjustingMultilineButton widthAdjustingMultilineButton2 = (WidthAdjustingMultilineButton) f.this.a(R.id.cancelButton);
            j.a((Object) widthAdjustingMultilineButton2, "cancelButton");
            widthAdjustingMultilineButton2.setEnabled(false);
            f fVar = f.this;
            if (fVar.c.length() == 0) {
                z = true;
            }
            if (z) {
                a2 = b.b.a.b.a(fVar);
                if (a2 != null) {
                    str = "missing_required_data";
                } else {
                    return;
                }
            } else if (!fVar.d) {
                a2 = b.b.a.b.a(fVar);
                if (a2 != null) {
                    str = "email_addresses_do_not_match";
                } else {
                    return;
                }
            } else if (!fVar.e) {
                a2 = b.b.a.b.a(fVar);
                if (a2 != null) {
                    str = "invalid_email_address";
                } else {
                    return;
                }
            } else {
                String str2 = fVar.c;
                Checkbox checkbox = (Checkbox) fVar.a(R.id.acceptMarketingCheckBox);
                j.a((Object) checkbox, "acceptMarketingCheckBox");
                boolean isChecked = checkbox.isChecked();
                WeakReference weakReference = new WeakReference(fVar);
                m.b(m.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().g.a(str2, (String) null, isChecked), new d(weakReference, str2, isChecked)), new e(weakReference));
                return;
            }
            a2.a(str, (kotlin.d.a.b<? super b.b.a.i.e, kotlin.m>) null);
        }
    }

    public final class h extends k implements kotlin.d.a.b<String, kotlin.m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f849a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(WeakReference weakReference) {
            super(1);
            this.f849a = weakReference;
        }

        public final Object invoke(Object obj) {
            TextView textView;
            String str = (String) obj;
            j.b(str, "url");
            f fVar = (f) this.f849a.get();
            if (!(fVar == null || (textView = (TextView) fVar.a(R.id.termsTextView)) == null)) {
                b.b.a.i.x1.j.a(textView, "register_terms_text", new URLSpan(str), 33);
            }
            return kotlin.m.f5330a;
        }
    }

    public final class i extends k implements kotlin.d.a.b<String, kotlin.m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f850a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(WeakReference weakReference) {
            super(1);
            this.f850a = weakReference;
        }

        public final Object invoke(Object obj) {
            TextView textView;
            String str = (String) obj;
            j.b(str, "url");
            f fVar = (f) this.f850a.get();
            if (!(fVar == null || (textView = (TextView) fVar.a(R.id.privacyTextView)) == null)) {
                b.b.a.i.x1.j.a(textView, "register_privacy_text", new URLSpan(str), 33);
            }
            return kotlin.m.f5330a;
        }
    }

    public final View a(int i2) {
        if (this.f == null) {
            this.f = new HashMap();
        }
        View view = (View) this.f.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i2);
        this.f.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.f;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void a(v vVar) {
        j.b(vVar, "dialog");
        WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) a(R.id.okButton);
        j.a((Object) widthAdjustingMultilineButton, "okButton");
        widthAdjustingMultilineButton.setEnabled(true);
        WidthAdjustingMultilineButton widthAdjustingMultilineButton2 = (WidthAdjustingMultilineButton) a(R.id.cancelButton);
        j.a((Object) widthAdjustingMultilineButton2, "cancelButton");
        widthAdjustingMultilineButton2.setEnabled(true);
    }

    public final void c() {
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a("Register Progress Step 1");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.EditText, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.ImageView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.j.q1.a(android.support.v7.widget.AppCompatButton, boolean):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, boolean]
     candidates:
      b.b.a.j.q1.a(android.view.View, int):android.view.View
      b.b.a.j.q1.a(android.support.v4.widget.NestedScrollView, int):void
      b.b.a.j.q1.a(android.support.v4.widget.NestedScrollView, android.view.View):void
      b.b.a.j.q1.a(android.view.View, long):void
      b.b.a.j.q1.a(android.view.View, b.b.a.j.z):void
      b.b.a.j.q1.a(android.view.View, kotlin.d.a.a<kotlin.m>):void
      b.b.a.j.q1.a(android.widget.ScrollView, int):void
      b.b.a.j.q1.a(android.widget.ScrollView, android.view.View):void
      b.b.a.j.q1.a(android.support.v7.widget.AppCompatButton, boolean):void */
    public final void j() {
        EditText editText = (EditText) a(R.id.emailEditText);
        j.a((Object) editText, "emailEditText");
        String obj = editText.getText().toString();
        if (obj != null) {
            this.c = t.b(obj).toString();
            this.e = n.f1102b.c(this.c);
            ImageView imageView = (ImageView) a(R.id.validImageView);
            j.a((Object) imageView, "validImageView");
            int i2 = 8;
            if (imageView.getVisibility() == 8 && this.e) {
                ImageView imageView2 = (ImageView) a(R.id.validImageView);
                j.a((Object) imageView2, "validImageView");
                imageView2.setScaleX(0.0f);
                ImageView imageView3 = (ImageView) a(R.id.validImageView);
                j.a((Object) imageView3, "validImageView");
                imageView3.setScaleY(0.0f);
                ((ImageView) a(R.id.validImageView)).animate().scaleX(1.0f).scaleY(1.0f).setDuration(300).setInterpolator(b.b.a.f.a.g).start();
            }
            ImageView imageView4 = (ImageView) a(R.id.validImageView);
            j.a((Object) imageView4, "validImageView");
            boolean z = false;
            imageView4.setVisibility(this.e ? 0 : 8);
            EditText editText2 = (EditText) a(R.id.confirmEditText);
            j.a((Object) editText2, "confirmEditText");
            String obj2 = editText2.getText().toString();
            if (obj2 != null) {
                this.d = j.a((Object) t.b(obj2).toString(), (Object) this.c);
                ImageView imageView5 = (ImageView) a(R.id.confirmValidImageView);
                j.a((Object) imageView5, "confirmValidImageView");
                if (imageView5.getVisibility() == 8 && this.e && this.d) {
                    ImageView imageView6 = (ImageView) a(R.id.confirmValidImageView);
                    j.a((Object) imageView6, "confirmValidImageView");
                    imageView6.setScaleX(0.0f);
                    ImageView imageView7 = (ImageView) a(R.id.confirmValidImageView);
                    j.a((Object) imageView7, "confirmValidImageView");
                    imageView7.setScaleY(0.0f);
                    ((ImageView) a(R.id.confirmValidImageView)).animate().scaleX(1.0f).scaleY(1.0f).setDuration(300).setInterpolator(b.b.a.f.a.g).start();
                }
                ImageView imageView8 = (ImageView) a(R.id.confirmValidImageView);
                j.a((Object) imageView8, "confirmValidImageView");
                if (this.e && this.d) {
                    i2 = 0;
                }
                imageView8.setVisibility(i2);
                WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) a(R.id.okButton);
                j.a((Object) widthAdjustingMultilineButton, "okButton");
                if (!this.e || !this.d) {
                    z = true;
                }
                q1.a((AppCompatButton) widthAdjustingMultilineButton, z);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_register_enter_email_page, viewGroup, false);
    }

    public final void onDestroyView() {
        super.onDestroyView();
        MainActivity a2 = b.b.a.b.a(this);
        if (a2 != null) {
            a2.b(this);
        }
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.Checkbox, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        j.b(view, "view");
        super.onViewCreated(view, bundle);
        MainActivity a2 = b.b.a.b.a(this);
        if (a2 != null) {
            a2.a(this);
        }
        ((EditText) a(R.id.emailEditText)).setText(g());
        j();
        ((EditText) a(R.id.emailEditText)).addTextChangedListener(new a());
        ((EditText) a(R.id.emailEditText)).setOnFocusChangeListener(new b());
        ((EditText) a(R.id.confirmEditText)).setOnFocusChangeListener(new c());
        ((EditText) a(R.id.confirmEditText)).addTextChangedListener(new d());
        Checkbox checkbox = (Checkbox) a(R.id.acceptMarketingCheckBox);
        j.a((Object) checkbox, "acceptMarketingCheckBox");
        checkbox.setChecked(f());
        ((LinearLayout) a(R.id.acceptMarketingCheckBoxRow)).setOnClickListener(new e());
        ((WidthAdjustingMultilineButton) a(R.id.cancelButton)).setOnClickListener(new C0084f());
        ((WidthAdjustingMultilineButton) a(R.id.okButton)).setOnClickListener(new g());
        TextView textView = (TextView) a(R.id.termsTextView);
        j.a((Object) textView, "termsTextView");
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        TextView textView2 = (TextView) a(R.id.termsTextView);
        j.a((Object) textView2, "termsTextView");
        textView2.setLinksClickable(true);
        TextView textView3 = (TextView) a(R.id.privacyTextView);
        j.a((Object) textView3, "privacyTextView");
        textView3.setMovementMethod(LinkMovementMethod.getInstance());
        TextView textView4 = (TextView) a(R.id.privacyTextView);
        j.a((Object) textView4, "privacyTextView");
        textView4.setLinksClickable(true);
        WeakReference weakReference = new WeakReference(this);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().j.a("register_terms_url", new h(weakReference));
        SupercellId.INSTANCE.getSharedServices$supercellId_release().j.a("register_privacy_url", new i(weakReference));
    }
}
