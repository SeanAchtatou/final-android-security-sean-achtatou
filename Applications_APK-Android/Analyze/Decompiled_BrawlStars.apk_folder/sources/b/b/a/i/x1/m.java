package b.b.a.i.x1;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.supercell.id.R;
import io.github.inflationx.viewpump.InflateResult;
import io.github.inflationx.viewpump.Interceptor;
import kotlin.d.a.b;
import kotlin.d.b.j;

public final class m implements Interceptor {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.Context, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.Boolean, boolean]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [io.github.inflationx.viewpump.InflateResult, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final InflateResult intercept(Interceptor.Chain chain) {
        j.b(chain, "chain");
        InflateResult proceed = chain.proceed(chain.request());
        View view = proceed.view();
        Context context = proceed.context();
        j.a((Object) context, "result.context()");
        AttributeSet attrs = proceed.attrs();
        if (view != null) {
            Object tag = view.getTag(R.id.remoteresources_tag_id);
            String str = null;
            if (!(tag instanceof Boolean)) {
                tag = null;
            }
            if (!j.a((Object) ((Boolean) tag), (Object) true)) {
                if (view instanceof TextView) {
                    String attributeValue = attrs != null ? attrs.getAttributeValue(null, context.getResources().getResourceEntryName(R.attr.textKey)) : null;
                    if (attributeValue != null) {
                        j.a((TextView) view, attributeValue, (b) null, 2);
                    }
                    if (view instanceof EditText) {
                        String resourceEntryName = context.getResources().getResourceEntryName(R.attr.hintKey);
                        if (attrs != null) {
                            str = attrs.getAttributeValue(null, resourceEntryName);
                        }
                        if (str != null) {
                            j.a((EditText) view, str);
                        }
                    }
                } else if (view instanceof ImageView) {
                    int i = R.attr.srcKey;
                    String resourceEntryName2 = context.getResources().getResourceEntryName(i);
                    int i2 = -1;
                    if (attrs != null) {
                        i2 = attrs.getAttributeResourceValue(null, resourceEntryName2, -1);
                    }
                    String string = i2 > 0 ? context.getString(i2) : attrs != null ? attrs.getAttributeValue(null, resourceEntryName2) : null;
                    boolean z = false;
                    if (string == null || string.length() == 0) {
                        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attrs, new int[]{i});
                        try {
                            string = obtainStyledAttributes.getString(0);
                            if (string == null || string.length() == 0) {
                                string = null;
                            }
                        } finally {
                            obtainStyledAttributes.recycle();
                        }
                    }
                    String resourceEntryName3 = context.getResources().getResourceEntryName(R.attr.fadeInExternalDrawable);
                    if (attrs != null) {
                        z = attrs.getAttributeBooleanValue(null, resourceEntryName3, false);
                    }
                    if (string != null) {
                        j.a((ImageView) view, string, z);
                    }
                }
                view.setTag(R.id.remoteresources_tag_id, true);
            }
        }
        InflateResult build = proceed.toBuilder().view(view).build();
        j.a((Object) build, "result.toBuilder().view(…ewWithRemoteText).build()");
        return build;
    }
}
