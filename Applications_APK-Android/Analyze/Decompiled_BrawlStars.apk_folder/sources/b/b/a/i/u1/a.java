package b.b.a.i.u1;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import b.b.a.i.e;
import com.supercell.id.IdAccount;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.WidthAdjustingMultilineButton;
import java.util.HashMap;
import kotlin.d.b.g;
import kotlin.d.b.j;

public final class a extends e {
    public static final C0069a f = new C0069a(null);
    public HashMap e;

    /* renamed from: b.b.a.i.u1.a$a  reason: collision with other inner class name */
    public static final class C0069a {
        public /* synthetic */ C0069a(g gVar) {
        }

        public final a a(String str, String str2, String str3, String str4, IdAccount idAccount) {
            j.b(str, "titleKey");
            j.b(str2, "textKey");
            j.b(str3, "okButtonKey");
            j.b(str4, "cancelButtonKey");
            j.b(idAccount, "account");
            a aVar = new a();
            Bundle bundle = new Bundle();
            bundle.putString("titleKey", str);
            bundle.putString("textKey", str2);
            bundle.putString("okButtonKey", str3);
            bundle.putString("cancelButtonKey", str4);
            bundle.putParcelable("account", idAccount);
            aVar.setArguments(bundle);
            return aVar;
        }
    }

    public final class b implements View.OnClickListener {
        public b() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) a.this.a(R.id.cancelButton);
            j.a((Object) widthAdjustingMultilineButton, "cancelButton");
            widthAdjustingMultilineButton.setEnabled(false);
            a.this.b();
        }
    }

    public final class c implements View.OnClickListener {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ IdAccount f730b;

        public c(IdAccount idAccount) {
            this.f730b = idAccount;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) a.this.a(R.id.okButton);
            j.a((Object) widthAdjustingMultilineButton, "okButton");
            widthAdjustingMultilineButton.setEnabled(false);
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Saved Credentials", "click", "Confirm forget saved credentials", null, false, 24);
            IdAccount idAccount = this.f730b;
            if (idAccount != null) {
                SupercellId.INSTANCE.forgetAccount$supercellId_release(idAccount.getSupercellId(), idAccount.getEmail());
                MainActivity a2 = b.b.a.b.a(a.this);
                if (a2 != null) {
                    b.b.a.b.a(a2, idAccount);
                }
            }
            a.this.b();
        }
    }

    public final View a(int i) {
        if (this.e == null) {
            this.e = new HashMap();
        }
        View view = (View) this.e.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.e.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.e;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_confirm_dialog, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    /* JADX WARN: Type inference failed for: r6v17, types: [android.os.Parcelable] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, kotlin.d.a.b, int):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String, ?[OBJECT, ARRAY], int]
     candidates:
      b.b.a.i.x1.j.a(android.widget.ImageView, java.lang.String, boolean, int):void
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, java.lang.Object, int):void
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, kotlin.d.a.b, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.LinearLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.animation.SpringForce, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onViewCreated(android.view.View r6, android.os.Bundle r7) {
        /*
            r5 = this;
            java.lang.String r7 = "view"
            kotlin.d.b.j.b(r6, r7)
            android.os.Bundle r6 = r5.getArguments()
            r7 = 2
            r0 = 0
            if (r6 == 0) goto L_0x0025
            java.lang.String r1 = "titleKey"
            java.lang.String r6 = r6.getString(r1)
            if (r6 == 0) goto L_0x0025
            int r1 = com.supercell.id.R.id.dialogTitleTextView
            android.view.View r1 = r5.a(r1)
            android.widget.TextView r1 = (android.widget.TextView) r1
            java.lang.String r2 = "dialogTitleTextView"
            kotlin.d.b.j.a(r1, r2)
            b.b.a.i.x1.j.a(r1, r6, r0, r7)
        L_0x0025:
            android.os.Bundle r6 = r5.getArguments()
            if (r6 == 0) goto L_0x0043
            java.lang.String r1 = "textKey"
            java.lang.String r6 = r6.getString(r1)
            if (r6 == 0) goto L_0x0043
            int r1 = com.supercell.id.R.id.dialogTextTextView
            android.view.View r1 = r5.a(r1)
            android.widget.TextView r1 = (android.widget.TextView) r1
            java.lang.String r2 = "dialogTextTextView"
            kotlin.d.b.j.a(r1, r2)
            b.b.a.i.x1.j.a(r1, r6, r0, r7)
        L_0x0043:
            android.os.Bundle r6 = r5.getArguments()
            if (r6 == 0) goto L_0x0061
            java.lang.String r1 = "okButtonKey"
            java.lang.String r6 = r6.getString(r1)
            if (r6 == 0) goto L_0x0061
            int r1 = com.supercell.id.R.id.okButton
            android.view.View r1 = r5.a(r1)
            com.supercell.id.view.WidthAdjustingMultilineButton r1 = (com.supercell.id.view.WidthAdjustingMultilineButton) r1
            java.lang.String r2 = "okButton"
            kotlin.d.b.j.a(r1, r2)
            b.b.a.i.x1.j.a(r1, r6, r0, r7)
        L_0x0061:
            android.os.Bundle r6 = r5.getArguments()
            if (r6 == 0) goto L_0x007f
            java.lang.String r1 = "cancelButtonKey"
            java.lang.String r6 = r6.getString(r1)
            if (r6 == 0) goto L_0x007f
            int r1 = com.supercell.id.R.id.cancelButton
            android.view.View r1 = r5.a(r1)
            com.supercell.id.view.WidthAdjustingMultilineButton r1 = (com.supercell.id.view.WidthAdjustingMultilineButton) r1
            java.lang.String r2 = "cancelButton"
            kotlin.d.b.j.a(r1, r2)
            b.b.a.i.x1.j.a(r1, r6, r0, r7)
        L_0x007f:
            android.os.Bundle r6 = r5.getArguments()
            if (r6 == 0) goto L_0x008e
            java.lang.String r7 = "account"
            android.os.Parcelable r6 = r6.getParcelable(r7)
            r0 = r6
            com.supercell.id.IdAccount r0 = (com.supercell.id.IdAccount) r0
        L_0x008e:
            int r6 = com.supercell.id.R.id.cancelButton
            android.view.View r6 = r5.a(r6)
            com.supercell.id.view.WidthAdjustingMultilineButton r6 = (com.supercell.id.view.WidthAdjustingMultilineButton) r6
            b.b.a.i.u1.a$b r7 = new b.b.a.i.u1.a$b
            r7.<init>()
            r6.setOnClickListener(r7)
            int r6 = com.supercell.id.R.id.okButton
            android.view.View r6 = r5.a(r6)
            com.supercell.id.view.WidthAdjustingMultilineButton r6 = (com.supercell.id.view.WidthAdjustingMultilineButton) r6
            b.b.a.i.u1.a$c r7 = new b.b.a.i.u1.a$c
            r7.<init>(r0)
            r6.setOnClickListener(r7)
            int r6 = com.supercell.id.R.id.dialogContainer
            android.view.View r6 = r5.a(r6)
            android.widget.LinearLayout r6 = (android.widget.LinearLayout) r6
            java.lang.String r7 = "it"
            kotlin.d.b.j.a(r6, r7)
            r7 = 1061997773(0x3f4ccccd, float:0.8)
            r6.setScaleX(r7)
            r6.setScaleY(r7)
            android.support.animation.SpringAnimation r7 = new android.support.animation.SpringAnimation
            android.support.animation.DynamicAnimation$ViewProperty r0 = android.support.animation.SpringAnimation.SCALE_X
            r1 = 1065353216(0x3f800000, float:1.0)
            r7.<init>(r6, r0, r1)
            android.support.animation.SpringForce r0 = r7.getSpring()
            java.lang.String r2 = "spring"
            kotlin.d.b.j.a(r0, r2)
            r3 = 1050253722(0x3e99999a, float:0.3)
            r0.setDampingRatio(r3)
            android.support.animation.SpringForce r0 = r7.getSpring()
            kotlin.d.b.j.a(r0, r2)
            r4 = 1137180672(0x43c80000, float:400.0)
            r0.setStiffness(r4)
            r7.start()
            android.support.animation.SpringAnimation r7 = new android.support.animation.SpringAnimation
            android.support.animation.DynamicAnimation$ViewProperty r0 = android.support.animation.SpringAnimation.SCALE_Y
            r7.<init>(r6, r0, r1)
            android.support.animation.SpringForce r6 = r7.getSpring()
            kotlin.d.b.j.a(r6, r2)
            r6.setDampingRatio(r3)
            android.support.animation.SpringForce r6 = r7.getSpring()
            kotlin.d.b.j.a(r6, r2)
            r6.setStiffness(r4)
            r7.start()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.i.u1.a.onViewCreated(android.view.View, android.os.Bundle):void");
    }
}
