package b.b.a.i.s1;

import android.content.Context;
import android.support.v4.view.ViewPager;
import b.b.a.b;
import b.b.a.e.a;
import com.supercell.id.SupercellId;
import java.util.List;

public final class j extends ViewPager.SimpleOnPageChangeListener {

    /* renamed from: a  reason: collision with root package name */
    public int f677a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ Context f678b;
    public final /* synthetic */ List c;

    public j(Context context, List list) {
        this.f678b = context;
        this.c = list;
    }

    public final void onPageScrollStateChanged(int i) {
        this.f677a = i;
    }

    public final void onPageSelected(int i) {
        boolean z;
        List list;
        Context context;
        if (this.f677a != 2) {
            context = this.f678b;
            list = this.c;
            z = false;
        } else {
            SupercellId.INSTANCE.getSharedServices$supercellId_release().l.a(a.C0004a.TAB_SWITCH);
            context = this.f678b;
            list = this.c;
            z = true;
        }
        b.a(context, list, i, z);
    }
}
