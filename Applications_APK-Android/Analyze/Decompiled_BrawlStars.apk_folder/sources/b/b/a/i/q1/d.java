package b.b.a.i.q1;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import b.b.a.g.g;
import b.b.a.j.f0;
import b.b.a.j.i;
import b.b.a.j.k0;
import b.b.a.j.o;
import b.b.a.j.o1;
import b.b.a.j.q1;
import b.b.a.j.r0;
import b.b.a.j.s0;
import b.b.a.j.t0;
import b.b.a.j.u0;
import b.b.a.j.y0;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.Checkbox;
import com.supercell.id.view.WidthAdjustingMultilineButton;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Set;
import kotlin.a.ab;
import kotlin.a.ae;
import kotlin.a.ai;
import kotlin.d.b.r;
import kotlin.d.b.t;
import nl.komponents.kovenant.bb;

public final class d extends b {
    public static final /* synthetic */ kotlin.h.h[] n;
    public List<? extends r0> c;
    public final Set<String> d = new LinkedHashSet();
    public final m e = new m();
    public final y0<List<b.b.a.h.f>> f = new y0<>(new i(), new j());
    public final y0<u0> g = new y0<>(new k(), new l());
    public final kotlin.d h = kotlin.e.a(new e());
    public final kotlin.d i = kotlin.e.a(new c());
    public final kotlin.d j = kotlin.e.a(new C0052d());
    public boolean k = true;
    public final b.b.a.j.k l = new b.b.a.j.k(R.layout.fragment_onboarding_friends_list_item_divider);
    public HashMap m;

    public static final class a implements r0 {

        /* renamed from: a  reason: collision with root package name */
        public final int f522a = R.layout.fragment_onboarding_friends_list_item_friend;

        /* renamed from: b  reason: collision with root package name */
        public final String f523b;
        public final String c;
        public final String d;
        public final String e;
        public final b.b.a.h.j f;

        public a(String str, String str2, String str3, String str4, b.b.a.h.j jVar) {
            kotlin.d.b.j.b(str, "scid");
            kotlin.d.b.j.b(str4, "gameNickname");
            kotlin.d.b.j.b(jVar, "relationship");
            this.f523b = str;
            this.c = str2;
            this.d = str3;
            this.e = str4;
            this.f = jVar;
        }

        public final int a() {
            return this.f522a;
        }

        public final boolean a(r0 r0Var) {
            kotlin.d.b.j.b(r0Var, FacebookRequestErrorClassification.KEY_OTHER);
            return (r0Var instanceof a) && kotlin.d.b.j.a(((a) r0Var).f523b, this.f523b);
        }

        public final boolean b(r0 r0Var) {
            kotlin.d.b.j.b(r0Var, FacebookRequestErrorClassification.KEY_OTHER);
            if (!(r0Var instanceof a)) {
                return false;
            }
            a aVar = (a) r0Var;
            return kotlin.d.b.j.a(this.c, aVar.c) && kotlin.d.b.j.a(this.e, aVar.e);
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return kotlin.d.b.j.a(this.f523b, aVar.f523b) && kotlin.d.b.j.a(this.c, aVar.c) && kotlin.d.b.j.a(this.d, aVar.d) && kotlin.d.b.j.a(this.e, aVar.e) && kotlin.d.b.j.a(this.f, aVar.f);
        }

        public final int hashCode() {
            String str = this.f523b;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.c;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.d;
            int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
            String str4 = this.e;
            int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
            b.b.a.h.j jVar = this.f;
            if (jVar != null) {
                i = jVar.hashCode();
            }
            return hashCode4 + i;
        }

        public final String toString() {
            StringBuilder a2 = b.a.a.a.a.a("FriendRow(scid=");
            a2.append(this.f523b);
            a2.append(", nickname=");
            a2.append(this.c);
            a2.append(", avatarUrl=");
            a2.append(this.d);
            a2.append(", gameNickname=");
            a2.append(this.e);
            a2.append(", relationship=");
            a2.append(this.f);
            a2.append(")");
            return a2.toString();
        }
    }

    public static final class b extends s0 {

        /* renamed from: b  reason: collision with root package name */
        public final WeakReference<Context> f524b;
        public final o1<BitmapDrawable> c = new o1<>(new e());
        public final d d;

        public final class a implements View.OnClickListener {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ s0.a f525a;

            /* renamed from: b  reason: collision with root package name */
            public final /* synthetic */ b f526b;
            public final /* synthetic */ r0 c;

            public a(s0.a aVar, b bVar, r0 r0Var, int i, s0.a aVar2) {
                this.f525a = aVar;
                this.f526b = bVar;
                this.c = r0Var;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
             arg types: [com.supercell.id.view.Checkbox, java.lang.String]
             candidates:
              kotlin.d.b.j.a(int, int):int
              kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
              kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
              kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
            public final void onClick(View view) {
                ((Checkbox) this.f525a.c.findViewById(R.id.select_friend)).toggle();
                d dVar = this.f526b.d;
                String str = ((a) this.c).f523b;
                Checkbox checkbox = (Checkbox) this.f525a.c.findViewById(R.id.select_friend);
                kotlin.d.b.j.a((Object) checkbox, "containerView.select_friend");
                dVar.a(str, checkbox.isChecked());
            }
        }

        /* renamed from: b.b.a.i.q1.d$b$b  reason: collision with other inner class name */
        public final class C0050b extends kotlin.d.b.k implements kotlin.d.a.c<Observable, Object, kotlin.m> {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ s0.a f527a;

            /* renamed from: b  reason: collision with root package name */
            public final /* synthetic */ b f528b;
            public final /* synthetic */ r0 c;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0050b(s0.a aVar, b bVar, r0 r0Var, int i, s0.a aVar2) {
                super(2);
                this.f527a = aVar;
                this.f528b = bVar;
                this.c = r0Var;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
             arg types: [com.supercell.id.view.Checkbox, java.lang.String]
             candidates:
              kotlin.d.b.j.a(int, int):int
              kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
              kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
              kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
            public final Object invoke(Object obj, Object obj2) {
                Checkbox checkbox = (Checkbox) this.f527a.c.findViewById(R.id.select_friend);
                kotlin.d.b.j.a((Object) checkbox, "containerView.select_friend");
                checkbox.setChecked(this.f528b.d.d.contains(((a) this.c).f523b));
                return kotlin.m.f5330a;
            }
        }

        public final class c extends kotlin.d.b.k implements kotlin.d.a.b<BitmapDrawable, kotlin.m> {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ WeakReference f529a;

            /* renamed from: b  reason: collision with root package name */
            public final /* synthetic */ r0 f530b;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(WeakReference weakReference, b bVar, r0 r0Var, int i, s0.a aVar) {
                super(1);
                this.f529a = weakReference;
                this.f530b = r0Var;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
             arg types: [android.widget.TextView, java.lang.String]
             candidates:
              kotlin.d.b.j.a(int, int):int
              kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
              kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
              kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
            public final Object invoke(Object obj) {
                BitmapDrawable bitmapDrawable = (BitmapDrawable) obj;
                kotlin.d.b.j.b(bitmapDrawable, "drawable");
                s0.a aVar = (s0.a) this.f529a.get();
                if (aVar != null && !(!kotlin.d.b.j.a(aVar.f1166b, this.f530b))) {
                    TextView textView = (TextView) aVar.c.findViewById(R.id.friend_playing_name_label);
                    kotlin.d.b.j.a((Object) textView, "containerView.friend_playing_name_label");
                    q1.a(textView, new e(aVar, this, bitmapDrawable));
                }
                return kotlin.m.f5330a;
            }
        }

        /* renamed from: b.b.a.i.q1.d$b$d  reason: collision with other inner class name */
        public final class C0051d implements View.OnClickListener {
            public C0051d(r0 r0Var, int i, s0.a aVar) {
            }

            public final void onClick(View view) {
                d dVar = b.this.d;
                dVar.g.a(bb.f5389a);
                b.this.d.g();
            }
        }

        public final class e extends kotlin.d.b.k implements kotlin.d.a.c<String, kotlin.d.a.b<? super BitmapDrawable, ? extends kotlin.m>, kotlin.m> {
            public e() {
                super(2);
            }

            public final Object invoke(Object obj, Object obj2) {
                String str = (String) obj;
                kotlin.d.a.b bVar = (kotlin.d.a.b) obj2;
                kotlin.d.b.j.b(str, "key");
                kotlin.d.b.j.b(bVar, "callback");
                SupercellId.INSTANCE.getSharedServices$supercellId_release().j.a(str, new f(this, bVar));
                return kotlin.m.f5330a;
            }
        }

        public b(Context context, d dVar) {
            kotlin.d.b.j.b(context, "context");
            kotlin.d.b.j.b(dVar, "fragment");
            this.d = dVar;
            this.f524b = new WeakReference<>(context);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.TextView, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void a(s0.a aVar, int i, r0 r0Var) {
            Resources resources;
            int i2;
            g.b bVar;
            float f;
            float f2;
            float f3;
            int i3;
            s0.a aVar2 = aVar;
            int i4 = i;
            r0 r0Var2 = r0Var;
            kotlin.d.b.j.b(aVar2, "holder");
            kotlin.d.b.j.b(r0Var2, "item");
            if (r0Var2 instanceof a) {
                WeakReference weakReference = new WeakReference(aVar2);
                LinearLayout linearLayout = (LinearLayout) aVar2.c.findViewById(R.id.friend_container);
                boolean b2 = b.b.a.b.b(this.f1164a, i4);
                boolean a2 = b.b.a.b.a(this.f1164a, i4);
                if (linearLayout != null) {
                    if (!b2 || !a2) {
                        if (b2) {
                            linearLayout.setBackgroundResource(R.drawable.border_container_top_pressable);
                            bVar = g.b.TOP;
                        } else if (a2) {
                            linearLayout.setBackgroundResource(R.drawable.border_container_bottom_pressable);
                            bVar = g.b.BOTTOM;
                        } else {
                            linearLayout.setBackgroundResource(R.drawable.border_container_middle_pressable);
                            bVar = g.b.MIDDLE;
                        }
                        i3 = 0;
                        f3 = 0.0f;
                        f2 = 0.0f;
                        f = 0.0f;
                        i2 = 15;
                    } else {
                        linearLayout.setBackgroundResource(R.drawable.border_container_pressable);
                        i3 = 0;
                        f3 = 0.0f;
                        f2 = 0.0f;
                        f = 0.0f;
                        bVar = null;
                        i2 = 31;
                    }
                    b.b.a.b.a(linearLayout, i3, f3, f2, f, bVar, i2);
                    ViewGroup.MarginLayoutParams d2 = q1.d(linearLayout);
                    if (d2 != null) {
                        d2.topMargin = 0;
                    }
                    ViewGroup.MarginLayoutParams d3 = q1.d(linearLayout);
                    if (d3 != null) {
                        d3.bottomMargin = 0;
                    }
                }
                Context context = aVar2.c.getContext();
                if (!(context == null || (resources = context.getResources()) == null)) {
                    k0.f1078b.a(((a) r0Var2).d, (ImageView) aVar2.c.findViewById(R.id.friend_image_view), resources);
                }
                TextView textView = (TextView) aVar2.c.findViewById(R.id.friend_name_label);
                kotlin.d.b.j.a((Object) textView, "containerView.friend_name_label");
                a aVar3 = (a) r0Var2;
                textView.setText(aVar3.c);
                ((Checkbox) aVar2.c.findViewById(R.id.select_friend)).a(this.d.d.contains(aVar3.f523b), false, false);
                s0.a aVar4 = aVar;
                r0 r0Var3 = r0Var;
                int i5 = i;
                s0.a aVar5 = aVar;
                ((LinearLayout) aVar2.c.findViewById(R.id.friend_container)).setOnClickListener(new a(aVar4, this, r0Var3, i5, aVar5));
                aVar2.f1165a = new C0050b(aVar4, this, r0Var3, i5, aVar5);
                this.d.e.addObserver(aVar2);
                o1<BitmapDrawable> o1Var = this.c;
                StringBuilder a3 = b.a.a.a.a.a("AppIcon_");
                a3.append(SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getGame());
                a3.append(".png");
                o1Var.a(a3.toString(), new c(weakReference, this, r0Var3, i5, aVar5));
                TextView textView2 = (TextView) aVar2.c.findViewById(R.id.friend_playing_name_label);
                kotlin.d.b.j.a((Object) textView2, "containerView.friend_playing_name_label");
                textView2.setText(aVar3.e);
            } else if (r0Var2 instanceof o) {
                ((WidthAdjustingMultilineButton) aVar2.c.findViewById(R.id.errorRetryButton)).setOnClickListener(new C0051d(r0Var2, i4, aVar2));
            }
        }

        public final void onViewRecycled(RecyclerView.ViewHolder viewHolder) {
            s0.a aVar = (s0.a) viewHolder;
            kotlin.d.b.j.b(aVar, "holder");
            aVar.a((kotlin.d.a.c<? super Observable, Object, kotlin.m>) null);
            this.d.e.deleteObserver(aVar);
        }
    }

    public final class c extends kotlin.d.b.k implements kotlin.d.a.a<Integer> {
        public c() {
            super(0);
        }

        public final Object invoke() {
            return Integer.valueOf(d.this.getResources().getDimensionPixelSize(R.dimen.list_padding_horizontal));
        }
    }

    /* renamed from: b.b.a.i.q1.d$d  reason: collision with other inner class name */
    public final class C0052d extends kotlin.d.b.k implements kotlin.d.a.a<Integer> {
        public C0052d() {
            super(0);
        }

        public final Object invoke() {
            return Integer.valueOf(d.this.getResources().getDimensionPixelSize(R.dimen.onboarding_friends_max_width));
        }
    }

    public final class e extends kotlin.d.b.k implements kotlin.d.a.a<Boolean> {
        public e() {
            super(0);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.content.res.Resources, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final Object invoke() {
            Resources resources = d.this.getResources();
            kotlin.d.b.j.a((Object) resources, "resources");
            return Boolean.valueOf(b.b.a.b.b(resources));
        }
    }

    public final class f implements View.OnClickListener {

        public final class a extends kotlin.d.b.k implements kotlin.d.a.b<List<? extends Map<String, ? extends Exception>>, kotlin.m> {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ WeakReference f537a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(WeakReference weakReference) {
                super(1);
                this.f537a = weakReference;
            }

            public final void invoke(List<? extends Map<String, ? extends Exception>> list) {
                if (this.f537a.get() != null) {
                    SupercellId.INSTANCE.getSharedServices$supercellId_release().e().a();
                    LinkedHashMap linkedHashMap = new LinkedHashMap();
                    for (Map putAll : list) {
                        linkedHashMap.putAll(putAll);
                    }
                    Exception exc = (Exception) kotlin.a.m.d(linkedHashMap.values());
                    if (exc != null) {
                        d.class.getSimpleName();
                        StringBuilder a2 = b.a.a.a.a.a("Failed to invite in-game friends: ");
                        a2.append(exc.getLocalizedMessage());
                        a2.toString();
                        f0.e.a(exc);
                    }
                }
            }
        }

        public final class b extends kotlin.d.b.k implements kotlin.d.a.b<Map<String, ? extends b.b.a.j.m<? extends b.b.a.h.i, ? extends Exception>>, Map<String, ? extends Exception>> {

            /* renamed from: a  reason: collision with root package name */
            public static final b f538a = new b();

            public b() {
                super(1);
            }

            /* renamed from: a */
            public final Map<String, Exception> invoke(Map<String, ? extends b.b.a.j.m<b.b.a.h.i, ? extends Exception>> map) {
                kotlin.d.b.j.b(map, "response");
                LinkedHashMap linkedHashMap = new LinkedHashMap();
                for (Map.Entry next : map.entrySet()) {
                    if (((b.b.a.j.m) next.getValue()).b() != null) {
                        linkedHashMap.put(next.getKey(), next.getValue());
                    }
                }
                LinkedHashMap linkedHashMap2 = new LinkedHashMap(ai.a(linkedHashMap.size()));
                for (Map.Entry entry : linkedHashMap.entrySet()) {
                    Object key = entry.getKey();
                    Object b2 = ((b.b.a.j.m) entry.getValue()).b();
                    if (b2 == null) {
                        kotlin.d.b.j.a();
                    }
                    linkedHashMap2.put(key, (Exception) b2);
                }
                return linkedHashMap2;
            }
        }

        public final class c extends kotlin.d.b.k implements kotlin.d.a.b<Exception, Map<String, ? extends Exception>> {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ List f539a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(List list) {
                super(1);
                this.f539a = list;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: kotlin.a.p.a(java.lang.Iterable, int):int
             arg types: [java.util.List, int]
             candidates:
              kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
              kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
              kotlin.a.w.a(java.util.List, int):T
              kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
              kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
              kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
              kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
              kotlin.a.s.a(java.util.List, java.util.Comparator):void
              kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
              kotlin.a.p.a(java.lang.Iterable, int):int */
            /* renamed from: a */
            public final Map<String, Exception> invoke(Exception exc) {
                kotlin.d.b.j.b(exc, "error");
                List<a> list = this.f539a;
                LinkedHashMap linkedHashMap = new LinkedHashMap(kotlin.g.d.c(ai.a(kotlin.a.m.a((Iterable) list, 10)), 16));
                for (a aVar : list) {
                    kotlin.h a2 = kotlin.k.a(aVar.f523b, exc);
                    linkedHashMap.put(a2.f5262a, a2.f5263b);
                }
                return linkedHashMap;
            }
        }

        /* renamed from: b.b.a.i.q1.d$f$d  reason: collision with other inner class name */
        public final class C0053d extends kotlin.d.b.k implements kotlin.d.a.b<Map<String, ? extends b.b.a.j.m<? extends Boolean, ? extends Exception>>, Map<String, ? extends Exception>> {

            /* renamed from: a  reason: collision with root package name */
            public static final C0053d f540a = new C0053d();

            public C0053d() {
                super(1);
            }

            /* renamed from: a */
            public final Map<String, Exception> invoke(Map<String, ? extends b.b.a.j.m<Boolean, ? extends Exception>> map) {
                kotlin.d.b.j.b(map, "response");
                LinkedHashMap linkedHashMap = new LinkedHashMap();
                for (Map.Entry next : map.entrySet()) {
                    if (((b.b.a.j.m) next.getValue()).b() != null) {
                        linkedHashMap.put(next.getKey(), next.getValue());
                    }
                }
                LinkedHashMap linkedHashMap2 = new LinkedHashMap(ai.a(linkedHashMap.size()));
                for (Map.Entry entry : linkedHashMap.entrySet()) {
                    Object key = entry.getKey();
                    Object b2 = ((b.b.a.j.m) entry.getValue()).b();
                    if (b2 == null) {
                        kotlin.d.b.j.a();
                    }
                    linkedHashMap2.put(key, (Exception) b2);
                }
                return linkedHashMap2;
            }
        }

        public final class e extends kotlin.d.b.k implements kotlin.d.a.b<Exception, Map<String, ? extends Exception>> {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ List f541a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public e(List list) {
                super(1);
                this.f541a = list;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: kotlin.a.p.a(java.lang.Iterable, int):int
             arg types: [java.util.List, int]
             candidates:
              kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
              kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
              kotlin.a.w.a(java.util.List, int):T
              kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
              kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
              kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
              kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
              kotlin.a.s.a(java.util.List, java.util.Comparator):void
              kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
              kotlin.a.p.a(java.lang.Iterable, int):int */
            /* renamed from: a */
            public final Map<String, Exception> invoke(Exception exc) {
                kotlin.d.b.j.b(exc, "error");
                List<a> list = this.f541a;
                LinkedHashMap linkedHashMap = new LinkedHashMap(kotlin.g.d.c(ai.a(kotlin.a.m.a((Iterable) list, 10)), 16));
                for (a aVar : list) {
                    kotlin.h a2 = kotlin.k.a(aVar.f523b, exc);
                    linkedHashMap.put(a2.f5262a, a2.f5263b);
                }
                return linkedHashMap;
            }
        }

        public f() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.a.p.a(java.lang.Iterable, int):int
         arg types: [java.util.List, int]
         candidates:
          kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
          kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
          kotlin.a.w.a(java.util.List, int):T
          kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
          kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
          kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
          kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
          kotlin.a.s.a(java.util.List, java.util.Comparator):void
          kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
          kotlin.a.p.a(java.lang.Iterable, int):int */
        /* JADX WARNING: Code restructure failed: missing block: B:56:0x01a3, code lost:
            if (r0 != null) goto L_0x01af;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void onClick(android.view.View r12) {
            /*
                r11 = this;
                b.b.a.i.q1.d r12 = b.b.a.i.q1.d.this
                java.util.List<? extends b.b.a.j.r0> r12 = r12.c
                r0 = 0
                if (r12 == 0) goto L_0x004f
                java.util.ArrayList r1 = new java.util.ArrayList
                r1.<init>()
                java.util.Iterator r12 = r12.iterator()
            L_0x0010:
                boolean r2 = r12.hasNext()
                if (r2 == 0) goto L_0x0029
                java.lang.Object r2 = r12.next()
                b.b.a.j.r0 r2 = (b.b.a.j.r0) r2
                boolean r3 = r2 instanceof b.b.a.i.q1.d.a
                if (r3 != 0) goto L_0x0021
                r2 = r0
            L_0x0021:
                b.b.a.i.q1.d$a r2 = (b.b.a.i.q1.d.a) r2
                if (r2 == 0) goto L_0x0010
                r1.add(r2)
                goto L_0x0010
            L_0x0029:
                java.util.ArrayList r12 = new java.util.ArrayList
                r12.<init>()
                java.util.Iterator r1 = r1.iterator()
            L_0x0032:
                boolean r2 = r1.hasNext()
                if (r2 == 0) goto L_0x0050
                java.lang.Object r2 = r1.next()
                r3 = r2
                b.b.a.i.q1.d$a r3 = (b.b.a.i.q1.d.a) r3
                b.b.a.i.q1.d r4 = b.b.a.i.q1.d.this
                java.util.Set<java.lang.String> r4 = r4.d
                java.lang.String r3 = r3.f523b
                boolean r3 = r4.contains(r3)
                if (r3 == 0) goto L_0x0032
                r12.add(r2)
                goto L_0x0032
            L_0x004f:
                r12 = r0
            L_0x0050:
                r1 = 0
                r2 = 1
                if (r12 == 0) goto L_0x005d
                boolean r3 = r12.isEmpty()
                if (r3 == 0) goto L_0x005b
                goto L_0x005d
            L_0x005b:
                r3 = 0
                goto L_0x005e
            L_0x005d:
                r3 = 1
            L_0x005e:
                if (r3 == 0) goto L_0x0077
                com.supercell.id.SupercellId r12 = com.supercell.id.SupercellId.INSTANCE
                b.b.a.j.x r12 = r12.getSharedServices$supercellId_release()
                b.b.a.c.b r2 = r12.f
                r6 = 0
                r7 = 0
                r8 = 24
                java.lang.String r3 = "Onboarding Add Friends"
                java.lang.String r4 = "click"
                java.lang.String r5 = "Skip"
                b.b.a.c.b.a(r2, r3, r4, r5, r6, r7, r8)
                goto L_0x0189
            L_0x0077:
                com.supercell.id.SupercellId r3 = com.supercell.id.SupercellId.INSTANCE
                b.b.a.j.x r3 = r3.getSharedServices$supercellId_release()
                b.b.a.c.b r4 = r3.f
                r8 = 0
                r9 = 0
                r10 = 24
                java.lang.String r5 = "Onboarding Add Friends"
                java.lang.String r6 = "click"
                java.lang.String r7 = "Confirm"
                b.b.a.c.b.a(r4, r5, r6, r7, r8, r9, r10)
                java.util.LinkedHashMap r3 = new java.util.LinkedHashMap
                r3.<init>()
                java.util.Iterator r12 = r12.iterator()
            L_0x0095:
                boolean r4 = r12.hasNext()
                if (r4 == 0) goto L_0x00be
                java.lang.Object r4 = r12.next()
                r5 = r4
                b.b.a.i.q1.d$a r5 = (b.b.a.i.q1.d.a) r5
                b.b.a.h.j r5 = r5.f
                boolean r5 = r5 instanceof b.b.a.h.j.a.b
                java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)
                java.lang.Object r6 = r3.get(r5)
                if (r6 != 0) goto L_0x00b8
                java.util.ArrayList r6 = new java.util.ArrayList
                r6.<init>()
                r3.put(r5, r6)
            L_0x00b8:
                java.util.List r6 = (java.util.List) r6
                r6.add(r4)
                goto L_0x0095
            L_0x00be:
                java.lang.Boolean r12 = java.lang.Boolean.valueOf(r2)
                java.lang.Object r12 = r3.get(r12)
                java.lang.Boolean r4 = java.lang.Boolean.valueOf(r1)
                java.lang.Object r3 = r3.get(r4)
                kotlin.h r12 = kotlin.k.a(r12, r3)
                A r3 = r12.f5262a
                java.util.List r3 = (java.util.List) r3
                B r12 = r12.f5263b
                java.util.List r12 = (java.util.List) r12
                r4 = 10
                r5 = 2
                if (r3 == 0) goto L_0x011c
                com.supercell.id.SupercellId r6 = com.supercell.id.SupercellId.INSTANCE
                b.b.a.j.x r6 = r6.getSharedServices$supercellId_release()
                b.b.a.j.q r6 = r6.e()
                java.util.ArrayList r7 = new java.util.ArrayList
                int r8 = kotlin.a.m.a(r3, r4)
                r7.<init>(r8)
                java.util.Iterator r8 = r3.iterator()
            L_0x00f6:
                boolean r9 = r8.hasNext()
                if (r9 == 0) goto L_0x0108
                java.lang.Object r9 = r8.next()
                b.b.a.i.q1.d$a r9 = (b.b.a.i.q1.d.a) r9
                java.lang.String r9 = r9.f523b
                r7.add(r9)
                goto L_0x00f6
            L_0x0108:
                nl.komponents.kovenant.bw r6 = r6.a(r7)
                b.b.a.i.q1.d$f$b r7 = b.b.a.i.q1.d.f.b.f538a
                nl.komponents.kovenant.bw r6 = nl.komponents.kovenant.bc.a(r6, r7)
                b.b.a.i.q1.d$f$c r7 = new b.b.a.i.q1.d$f$c
                r7.<init>(r3)
                nl.komponents.kovenant.bw r3 = b.b.a.b.a(r6, r0, r7, r2)
                goto L_0x0126
            L_0x011c:
                nl.komponents.kovenant.bw$a r3 = nl.komponents.kovenant.bw.d
                java.util.Map r6 = kotlin.a.ai.a()
                nl.komponents.kovenant.bw r3 = nl.komponents.kovenant.bb.f5389a
            L_0x0126:
                if (r12 == 0) goto L_0x0165
                com.supercell.id.SupercellId r6 = com.supercell.id.SupercellId.INSTANCE
                b.b.a.j.x r6 = r6.getSharedServices$supercellId_release()
                b.b.a.j.q r6 = r6.e()
                java.util.ArrayList r7 = new java.util.ArrayList
                int r4 = kotlin.a.m.a(r12, r4)
                r7.<init>(r4)
                java.util.Iterator r4 = r12.iterator()
            L_0x013f:
                boolean r8 = r4.hasNext()
                if (r8 == 0) goto L_0x0151
                java.lang.Object r8 = r4.next()
                b.b.a.i.q1.d$a r8 = (b.b.a.i.q1.d.a) r8
                java.lang.String r8 = r8.f523b
                r7.add(r8)
                goto L_0x013f
            L_0x0151:
                nl.komponents.kovenant.bw r4 = r6.b(r7)
                b.b.a.i.q1.d$f$d r6 = b.b.a.i.q1.d.f.C0053d.f540a
                nl.komponents.kovenant.bw r4 = nl.komponents.kovenant.bc.a(r4, r6)
                b.b.a.i.q1.d$f$e r6 = new b.b.a.i.q1.d$f$e
                r6.<init>(r12)
                nl.komponents.kovenant.bw r12 = b.b.a.b.a(r4, r0, r6, r2)
                goto L_0x016f
            L_0x0165:
                nl.komponents.kovenant.bw$a r12 = nl.komponents.kovenant.bw.d
                java.util.Map r4 = kotlin.a.ai.a()
                nl.komponents.kovenant.bw r12 = nl.komponents.kovenant.bb.f5389a
            L_0x016f:
                nl.komponents.kovenant.bw[] r4 = new nl.komponents.kovenant.bw[r5]
                r4[r1] = r3
                r4[r2] = r12
                r12 = 6
                nl.komponents.kovenant.bw r12 = nl.komponents.kovenant.bb.f5389a
                b.b.a.i.q1.d r2 = b.b.a.i.q1.d.this
                java.lang.ref.WeakReference r3 = new java.lang.ref.WeakReference
                r3.<init>(r2)
                b.b.a.i.q1.d$f$a r2 = new b.b.a.i.q1.d$f$a
                r2.<init>(r3)
                nl.komponents.kovenant.c.m.a(r12, r2)
            L_0x0189:
                b.b.a.i.q1.d r12 = b.b.a.i.q1.d.this
                com.supercell.id.ui.MainActivity r12 = b.b.a.b.a(r12)
                if (r12 == 0) goto L_0x01cc
                b.b.a.i.q1.d r2 = b.b.a.i.q1.d.this
                b.b.a.i.q1.a r2 = r2.f()
                if (r2 == 0) goto L_0x01a6
                b.b.a.i.b$a r2 = b.b.a.b.a(r2)
                b.b.a.i.q1.a$a r2 = (b.b.a.i.q1.a.C0048a) r2
                if (r2 == 0) goto L_0x01a3
                java.util.List<b.b.a.i.b$a> r0 = r2.i
            L_0x01a3:
                if (r0 == 0) goto L_0x01a6
                goto L_0x01af
            L_0x01a6:
                b.b.a.i.r1.n$a r0 = new b.b.a.i.r1.n$a
                r0.<init>()
                java.util.List r0 = kotlin.a.m.a(r0)
            L_0x01af:
                b.b.a.i.b$a[] r1 = new b.b.a.i.b.a[r1]
                java.lang.Object[] r0 = r0.toArray(r1)
                if (r0 == 0) goto L_0x01c4
                b.b.a.i.b$a[] r0 = (b.b.a.i.b.a[]) r0
                int r1 = r0.length
                java.lang.Object[] r0 = java.util.Arrays.copyOf(r0, r1)
                b.b.a.i.b$a[] r0 = (b.b.a.i.b.a[]) r0
                r12.a(r0)
                goto L_0x01cc
            L_0x01c4:
                kotlin.TypeCastException r12 = new kotlin.TypeCastException
                java.lang.String r0 = "null cannot be cast to non-null type kotlin.Array<T>"
                r12.<init>(r0)
                throw r12
            L_0x01cc:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: b.b.a.i.q1.d.f.onClick(android.view.View):void");
        }
    }

    public final class g implements View.OnClickListener {
        public g() {
        }

        public final void onClick(View view) {
            d.this.h();
        }
    }

    public final class h extends kotlin.d.b.k implements kotlin.d.a.b<String, kotlin.m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f543a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(WeakReference weakReference) {
            super(1);
            this.f543a = weakReference;
        }

        public final Object invoke(Object obj) {
            TextView textView;
            String str = (String) obj;
            kotlin.d.b.j.b(str, "it");
            d dVar = (d) this.f543a.get();
            if (!(dVar == null || (textView = (TextView) dVar.a(R.id.friends_title_view)) == null)) {
                b.b.a.i.x1.j.a(textView, "onboarding_invite_friends_title", kotlin.k.a("game", str));
            }
            return kotlin.m.f5330a;
        }
    }

    public final class i extends kotlin.d.b.k implements kotlin.d.a.b<List<? extends b.b.a.h.f>, kotlin.m> {
        public i() {
            super(1);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.a.p.a(java.lang.Iterable, int):int
         arg types: [java.util.List<b.b.a.h.f>, int]
         candidates:
          kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
          kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
          kotlin.a.w.a(java.util.List, int):T
          kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
          kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
          kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
          kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
          kotlin.a.s.a(java.util.List, java.util.Comparator):void
          kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
          kotlin.a.p.a(java.lang.Iterable, int):int */
        public final void a(List<b.b.a.h.f> list) {
            kotlin.d.b.j.b(list, "list");
            d dVar = d.this;
            ArrayList arrayList = new ArrayList(kotlin.a.m.a((Iterable) list, 10));
            for (b.b.a.h.f fVar : list) {
                arrayList.add(new a(fVar.f116a, fVar.f117b, fVar.c, fVar.e, fVar.d));
            }
            dVar.b(arrayList);
        }

        public final /* synthetic */ Object invoke(Object obj) {
            a((List) obj);
            return kotlin.m.f5330a;
        }
    }

    public final class j extends kotlin.d.b.k implements kotlin.d.a.b<Exception, kotlin.m> {
        public j() {
            super(1);
        }

        public final Object invoke(Object obj) {
            Exception exc = (Exception) obj;
            kotlin.d.b.j.b(exc, "it");
            d.this.getClass().getName();
            "Error receiving friends " + exc.getMessage();
            d dVar = d.this;
            dVar.g.a(bb.f5389a);
            return kotlin.m.f5330a;
        }
    }

    public final class k extends kotlin.d.b.k implements kotlin.d.a.b<u0, kotlin.m> {
        public k() {
            super(1);
        }

        public final void a(u0 u0Var) {
            kotlin.d.b.j.b(u0Var, "it");
            d dVar = d.this;
            if (dVar.c == u0Var.f1176a) {
                dVar.c = u0Var.f1177b;
                if (dVar.c == null) {
                    View a2 = dVar.a(R.id.sticky_header_container);
                    if (a2 != null) {
                        a2.setVisibility(4);
                    }
                    RecyclerView recyclerView = (RecyclerView) d.this.a(R.id.friends_list);
                    if (recyclerView != null) {
                        recyclerView.setVisibility(4);
                    }
                    View a3 = d.this.a(R.id.progress_bar);
                    if (a3 != null) {
                        a3.setVisibility(0);
                    }
                } else {
                    View a4 = dVar.a(R.id.sticky_header_container);
                    if (a4 != null) {
                        a4.setVisibility(0);
                    }
                    RecyclerView recyclerView2 = (RecyclerView) d.this.a(R.id.friends_list);
                    if (recyclerView2 != null) {
                        recyclerView2.setVisibility(0);
                    }
                    View a5 = d.this.a(R.id.progress_bar);
                    if (a5 != null) {
                        a5.setVisibility(4);
                    }
                }
                d.this.i();
                RecyclerView recyclerView3 = (RecyclerView) d.this.a(R.id.friends_list);
                RecyclerView.Adapter adapter = recyclerView3 != null ? recyclerView3.getAdapter() : null;
                if (!(adapter instanceof b)) {
                    adapter = null;
                }
                b bVar = (b) adapter;
                if (bVar != null) {
                    List<? extends r0> list = d.this.c;
                    if (list == null) {
                        list = ab.f5210a;
                    }
                    bVar.a(list);
                    u0Var.c.dispatchUpdatesTo(bVar);
                }
            }
        }

        public final /* synthetic */ Object invoke(Object obj) {
            a((u0) obj);
            return kotlin.m.f5330a;
        }
    }

    public final class l extends kotlin.d.b.k implements kotlin.d.a.b<Exception, kotlin.m> {
        public l() {
            super(1);
        }

        public final Object invoke(Object obj) {
            Exception exc = (Exception) obj;
            kotlin.d.b.j.b(exc, "it");
            MainActivity a2 = b.b.a.b.a(d.this);
            if (a2 != null) {
                a2.a(exc, (kotlin.d.a.b<? super b.b.a.i.e, kotlin.m>) null);
            }
            return kotlin.m.f5330a;
        }
    }

    public final class m extends Observable {
        public final void notifyObservers() {
            setChanged();
            super.notifyObservers();
        }
    }

    public final class n extends kotlin.d.b.k implements kotlin.d.a.a<u0> {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ List f549b;
        public final /* synthetic */ List c;

        public final class a<T> implements Comparator<T> {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ Comparator f550a;

            public a(Comparator comparator) {
                this.f550a = comparator;
            }

            public final int compare(T t, T t2) {
                Comparator comparator = this.f550a;
                String str = ((a) t).c;
                if (str == null) {
                    str = "";
                }
                String str2 = ((a) t2).c;
                if (str2 == null) {
                    str2 = "";
                }
                return comparator.compare(str, str2);
            }
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public n(List list, List list2) {
            super(0);
            this.f549b = list;
            this.c = list2;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
         arg types: [java.util.List, b.b.a.i.q1.d$n$a]
         candidates:
          kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
          kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
          kotlin.a.w.a(java.util.List, int):T
          kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
          kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
          kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
          kotlin.a.s.a(java.util.List, java.util.Comparator):void
          kotlin.a.p.a(java.lang.Iterable, int):int
          kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
          kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T> */
        public final u0 invoke() {
            List a2 = d.this.a(kotlin.a.m.a((Iterable) this.f549b, (Comparator) new a(SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getCollator())));
            List list = this.c;
            return new u0(list, a2, b.a.a.a.a.a(t0.c, list, a2, "DiffUtil.calculateDiff(R…create(oldRows, newRows))"));
        }
    }

    static {
        Class<d> cls = d.class;
        n = new kotlin.h.h[]{t.a(new r(t.a(cls), "isMobileLandscape", "isMobileLandscape()Z")), t.a(new r(t.a(cls), "friendsHorizontalMargin", "getFriendsHorizontalMargin()I")), t.a(new r(t.a(cls), "friendsMaxWidth", "getFriendsMaxWidth()I"))};
    }

    public final View a(int i2) {
        if (this.m == null) {
            this.m = new HashMap();
        }
        View view = (View) this.m.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i2);
        this.m.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public final void a(String str, boolean z) {
        b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Onboarding Add Friends", "click", "Friend", Long.valueOf(z ? 1 : 0), false, 16);
        if (z) {
            this.d.add(str);
        } else {
            this.d.remove(str);
        }
        i();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.p.a(java.lang.Iterable, int):int
     arg types: [java.util.List<b.b.a.i.q1.d$a>, int]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.p.a(java.lang.Iterable, int):int */
    public final void b(List<a> list) {
        List<? extends r0> list2 = this.c;
        b.b.a.j.i c2 = SupercellId.INSTANCE.getSharedServices$supercellId_release().c();
        ArrayList arrayList = new ArrayList(kotlin.a.m.a((Iterable) list, 10));
        for (a aVar : list) {
            arrayList.add(aVar.f523b);
        }
        c2.a(new i.a.b(arrayList));
        this.g.a(bb.f5389a);
    }

    public final void c() {
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a("Onboarding Add Friends");
    }

    public final void g() {
        this.f.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().h.b());
    }

    public final void h() {
        boolean z;
        List<? extends r0> list = this.c;
        List list2 = null;
        if (list != null) {
            ArrayList arrayList = new ArrayList();
            for (r0 r0Var : list) {
                if (!(r0Var instanceof a)) {
                    r0Var = null;
                }
                a aVar = (a) r0Var;
                String str = aVar != null ? aVar.f523b : null;
                if (str != null) {
                    arrayList.add(str);
                }
            }
            list2 = arrayList;
        }
        if (list2 == null) {
            z = false;
        } else {
            if (!list2.isEmpty()) {
                Iterator it = list2.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    if (!this.d.contains((String) it.next())) {
                        break;
                    }
                }
            }
            z = true;
        }
        if (z) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Onboarding Add Friends", "click", "Deselect all", null, false, 24);
            this.d.clear();
        } else {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Onboarding Add Friends", "click", "Select all", null, false, 24);
            Set<String> set = this.d;
            if (list2 == null) {
                list2 = ab.f5210a;
            }
            set.addAll(list2);
        }
        this.e.hasChanged();
        this.e.notifyObservers();
        i();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, kotlin.d.a.b, int):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String, ?[OBJECT, ARRAY], int]
     candidates:
      b.b.a.i.x1.j.a(android.widget.ImageView, java.lang.String, boolean, int):void
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, java.lang.Object, int):void
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, kotlin.d.a.b, int):void */
    public final void i() {
        ArrayList<String> arrayList;
        int i2;
        List<? extends r0> list = this.c;
        if (list != null) {
            arrayList = new ArrayList<>();
            for (r0 r0Var : list) {
                if (!(r0Var instanceof a)) {
                    r0Var = null;
                }
                a aVar = (a) r0Var;
                String str = aVar != null ? aVar.f523b : null;
                if (str != null) {
                    arrayList.add(str);
                }
            }
        } else {
            arrayList = null;
        }
        if (arrayList == null || arrayList.isEmpty()) {
            i2 = 0;
        } else {
            i2 = 0;
            for (String contains : arrayList) {
                if (this.d.contains(contains) && (i2 = i2 + 1) < 0) {
                    kotlin.a.m.b();
                }
            }
        }
        int size = arrayList != null ? arrayList.size() : 0;
        boolean z = i2 == size;
        TextView textView = (TextView) a(R.id.friends_selected_text_view);
        if (textView != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(i2);
            sb.append('/');
            sb.append(size);
            b.b.a.i.x1.j.a(textView, "onboarding_invite_friends_selected", kotlin.k.a("count", sb.toString()));
        }
        WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) a(R.id.all_friends_button);
        if (widthAdjustingMultilineButton != null) {
            b.b.a.i.x1.j.a((TextView) widthAdjustingMultilineButton, z ? "onboarding_invite_friends_deselect_all" : "onboarding_invite_friends_select_all", (kotlin.d.a.b) null, 2);
        }
        WidthAdjustingMultilineButton widthAdjustingMultilineButton2 = (WidthAdjustingMultilineButton) a(R.id.profile_continue_button);
        if (widthAdjustingMultilineButton2 != null) {
            b.b.a.i.x1.j.a((TextView) widthAdjustingMultilineButton2, i2 == 0 ? "onboarding_skip" : "onboarding_confirm", (kotlin.d.a.b) null, 2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kotlin.d.b.j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_onboarding_invite_friends_page, viewGroup, false);
    }

    public final void onDestroyView() {
        this.e.deleteObservers();
        super.onDestroyView();
        a();
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.graphics.Rect, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0049, code lost:
        if (r12 > r9.getBottom()) goto L_0x0068;
     */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x006e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.view.View r8, android.view.View r9, android.view.View r10, b.b.a.g.d r11, int r12, int r13) {
        /*
            r7 = this;
            int r0 = r9.getTop()
            kotlin.d r1 = r7.h
            java.lang.Object r1 = r1.a()
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            boolean r1 = r1.booleanValue()
            r2 = 0
            r3 = 0
            r4 = 1
            r5 = 1065353216(0x3f800000, float:1.0)
            if (r1 == 0) goto L_0x0072
            int r1 = r8.getHeight()
            boolean r6 = r7.k
            if (r6 == 0) goto L_0x0024
            int r13 = java.lang.Math.min(r12, r1)
            goto L_0x0041
        L_0x0024:
            float r6 = r8.getTranslationY()
            int r6 = kotlin.e.a.a(r6)
            int r6 = -r6
            int r13 = r13 + r6
            int r6 = kotlin.d.b.j.a(r13, r0)
            if (r6 >= 0) goto L_0x0036
            r13 = r0
            goto L_0x003d
        L_0x0036:
            int r6 = kotlin.d.b.j.a(r13, r1)
            if (r6 <= 0) goto L_0x003d
            r13 = r1
        L_0x003d:
            int r13 = java.lang.Math.min(r12, r13)
        L_0x0041:
            boolean r1 = r7.k
            if (r1 == 0) goto L_0x004f
            int r0 = r9.getBottom()
            if (r12 <= r0) goto L_0x004c
            goto L_0x0068
        L_0x004c:
            r2 = 1065353216(0x3f800000, float:1.0)
            goto L_0x0068
        L_0x004f:
            int r12 = r12 - r0
            float r12 = (float) r12
            int r0 = r9.getHeight()
            float r0 = (float) r0
            float r12 = r12 / r0
            float r12 = r5 - r12
            int r0 = java.lang.Float.compare(r12, r2)
            if (r0 >= 0) goto L_0x0060
            goto L_0x0068
        L_0x0060:
            int r0 = java.lang.Float.compare(r12, r5)
            if (r0 <= 0) goto L_0x0067
            goto L_0x004c
        L_0x0067:
            r2 = r12
        L_0x0068:
            int r12 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r12 != 0) goto L_0x006e
            r12 = 1
            goto L_0x006f
        L_0x006e:
            r12 = 0
        L_0x006f:
            r7.k = r12
            goto L_0x0090
        L_0x0072:
            int r13 = java.lang.Math.min(r12, r0)
            float r12 = (float) r13
            int r0 = java.lang.Math.max(r0, r4)
            float r0 = (float) r0
            float r12 = r12 / r0
            float r12 = r5 - r12
            int r0 = java.lang.Float.compare(r12, r2)
            if (r0 >= 0) goto L_0x0086
            goto L_0x0090
        L_0x0086:
            int r0 = java.lang.Float.compare(r12, r5)
            if (r0 <= 0) goto L_0x008f
            r2 = 1065353216(0x3f800000, float:1.0)
            goto L_0x0090
        L_0x008f:
            r2 = r12
        L_0x0090:
            r12 = 12
            float r12 = b.b.a.b.a(r12)
            float r12 = r12 * r2
            float r0 = r11.e
            int r0 = (r0 > r12 ? 1 : (r0 == r12 ? 0 : -1))
            if (r0 == 0) goto L_0x00af
            r11.e = r12
            android.graphics.Rect r12 = r11.getBounds()
            java.lang.String r0 = "bounds"
            kotlin.d.b.j.a(r12, r0)
            r11.a(r12)
            r11.invalidateSelf()
        L_0x00af:
            if (r10 == 0) goto L_0x00b6
            int r10 = r10.getWidth()
            goto L_0x00b7
        L_0x00b6:
            r10 = 0
        L_0x00b7:
            int r11 = android.support.v4.view.ViewCompat.getLayoutDirection(r9)
            if (r11 != r4) goto L_0x00bf
            r11 = 1
            goto L_0x00c0
        L_0x00bf:
            r11 = 0
        L_0x00c0:
            if (r11 == 0) goto L_0x00c4
            r11 = r10
            goto L_0x00c5
        L_0x00c4:
            r11 = 0
        L_0x00c5:
            int r12 = android.support.v4.view.ViewCompat.getLayoutDirection(r9)
            if (r12 != r4) goto L_0x00cc
            goto L_0x00cd
        L_0x00cc:
            r4 = 0
        L_0x00cd:
            if (r4 == 0) goto L_0x00d0
            r10 = 0
        L_0x00d0:
            kotlin.d r12 = r7.i
            java.lang.Object r12 = r12.a()
            java.lang.Number r12 = (java.lang.Number) r12
            int r12 = r12.intValue()
            int r12 = r12 + r11
            float r11 = (float) r12
            float r11 = r11 * r2
            int r11 = kotlin.e.a.a(r11)
            kotlin.d r12 = r7.i
            java.lang.Object r12 = r12.a()
            java.lang.Number r12 = (java.lang.Number) r12
            int r12 = r12.intValue()
            int r12 = r12 + r10
            float r10 = (float) r12
            float r10 = r10 * r2
            int r10 = kotlin.e.a.a(r10)
            kotlin.d r12 = r7.h
            java.lang.Object r12 = r12.a()
            java.lang.Boolean r12 = (java.lang.Boolean) r12
            boolean r12 = r12.booleanValue()
            if (r12 == 0) goto L_0x0134
            android.view.ViewGroup$LayoutParams r12 = r9.getLayoutParams()
            boolean r0 = r12 instanceof android.support.constraint.ConstraintLayout.LayoutParams
            if (r0 != 0) goto L_0x010f
            r12 = 0
        L_0x010f:
            android.support.constraint.ConstraintLayout$LayoutParams r12 = (android.support.constraint.ConstraintLayout.LayoutParams) r12
            if (r12 == 0) goto L_0x0141
            r12.leftMargin = r11
            r12.rightMargin = r10
            int r10 = r8.getWidth()
            kotlin.d r11 = r7.j
            java.lang.Object r11 = r11.a()
            java.lang.Number r11 = (java.lang.Number) r11
            int r11 = r11.intValue()
            float r0 = (float) r10
            int r11 = r11 - r10
            float r10 = (float) r11
            float r10 = r10 * r2
            float r10 = r10 + r0
            int r10 = kotlin.e.a.a(r10)
            r12.matchConstraintMaxWidth = r10
            goto L_0x013e
        L_0x0134:
            android.view.ViewGroup$MarginLayoutParams r12 = b.b.a.j.q1.d(r9)
            if (r12 == 0) goto L_0x0141
            r12.leftMargin = r11
            r12.rightMargin = r10
        L_0x013e:
            r9.setLayoutParams(r12)
        L_0x0141:
            float r9 = (float) r13
            float r9 = -r9
            r8.setTranslationY(r9)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.i.q1.d.a(android.view.View, android.view.View, android.view.View, b.b.a.g.d, int, int):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v7.widget.RecyclerView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.Context, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.graphics.Rect, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        kotlin.d.b.j.b(view, "view");
        super.onViewCreated(view, bundle);
        WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) a(R.id.profile_continue_button);
        if (widthAdjustingMultilineButton != null) {
            widthAdjustingMultilineButton.setOnClickListener(new f());
        }
        if (this.c == null) {
            View a2 = a(R.id.sticky_header_container);
            kotlin.d.b.j.a((Object) a2, "sticky_header_container");
            a2.setVisibility(4);
            RecyclerView recyclerView = (RecyclerView) a(R.id.friends_list);
            kotlin.d.b.j.a((Object) recyclerView, "friends_list");
            recyclerView.setVisibility(4);
            View a3 = a(R.id.progress_bar);
            kotlin.d.b.j.a((Object) a3, "progress_bar");
            a3.setVisibility(0);
        } else {
            View a4 = a(R.id.sticky_header_container);
            kotlin.d.b.j.a((Object) a4, "sticky_header_container");
            a4.setVisibility(0);
            RecyclerView recyclerView2 = (RecyclerView) a(R.id.friends_list);
            kotlin.d.b.j.a((Object) recyclerView2, "friends_list");
            recyclerView2.setVisibility(0);
            View a5 = a(R.id.progress_bar);
            kotlin.d.b.j.a((Object) a5, "progress_bar");
            a5.setVisibility(4);
        }
        Context context = view.getContext();
        kotlin.d.b.j.a((Object) context, "view.context");
        b bVar = new b(context, this);
        List<? extends r0> list = this.c;
        if (list == null) {
            list = ab.f5210a;
        }
        bVar.a(list);
        RecyclerView recyclerView3 = (RecyclerView) a(R.id.friends_list);
        kotlin.d.b.j.a((Object) recyclerView3, "friends_list");
        recyclerView3.setLayoutManager(new LinearLayoutManager(getContext()));
        RecyclerView recyclerView4 = (RecyclerView) a(R.id.friends_list);
        kotlin.d.b.j.a((Object) recyclerView4, "friends_list");
        recyclerView4.setAdapter(bVar);
        View a6 = a(R.id.sticky_header_container);
        View a7 = a(R.id.friends_header_container);
        FrameLayout frameLayout = (FrameLayout) a(R.id.end_system_inset_guide);
        a6.addOnLayoutChangeListener(new g(a7, a6.getResources().getDimensionPixelSize(R.dimen.list_padding_horizontal), this));
        kotlin.d.b.j.a((Object) a7, "selectAllBackground");
        Context context2 = a7.getContext();
        kotlin.d.b.j.a((Object) context2, "selectAllBackground.context");
        b.b.a.g.d dVar = new b.b.a.g.d(context2);
        float a8 = b.b.a.b.a(12);
        if (dVar.e != a8) {
            dVar.e = a8;
            Rect bounds = dVar.getBounds();
            kotlin.d.b.j.a((Object) bounds, "bounds");
            dVar.a(bounds);
            dVar.invalidateSelf();
        }
        ViewCompat.setBackground(a7, dVar);
        b.b.a.b.a(a7, 0, 0.0f, 0.0f, 0.0f, null, 31);
        ((RecyclerView) a(R.id.friends_list)).addOnScrollListener(new h(a6, a7, frameLayout, dVar, this));
        q1.a(a6, new i(new WeakReference(this), dVar));
        this.f.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().h.b());
        i();
        ((WidthAdjustingMultilineButton) a(R.id.all_friends_button)).setOnClickListener(new g());
        SupercellId.INSTANCE.getSharedServices$supercellId_release().e.gameLocalizedName(new h(new WeakReference(this)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
     arg types: [java.util.ArrayList, java.util.List]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean */
    public final List<r0> a(List<a> list) {
        List list2;
        Iterable<ae> j2 = kotlin.a.m.j(list);
        ArrayList arrayList = new ArrayList();
        for (ae aeVar : j2) {
            if (aeVar.f5213a == 0) {
                list2 = kotlin.a.m.a((Object) aeVar.f5214b);
            } else {
                list2 = kotlin.a.m.a((Object[]) new r0[]{this.l, (r0) aeVar.f5214b});
            }
            kotlin.a.m.a((Collection) arrayList, (Iterable) list2);
        }
        return arrayList;
    }
}
