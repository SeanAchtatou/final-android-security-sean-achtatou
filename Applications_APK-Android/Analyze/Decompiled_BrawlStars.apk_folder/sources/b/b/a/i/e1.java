package b.b.a.i;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import b.b.a.i.d1;
import kotlin.d.b.j;
import kotlin.d.b.s;

public final class e1 extends RecyclerView.OnScrollListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ TextView f240a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ d1.b f241b;
    public final /* synthetic */ s.b c;

    public e1(TextView textView, d1.b bVar, s.b bVar2) {
        this.f240a = textView;
        this.f241b = bVar;
        this.c = bVar2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onScrolled(RecyclerView recyclerView, int i, int i2) {
        j.b(recyclerView, "recyclerView");
        View childAt = recyclerView.getChildAt(0);
        if (childAt == null) {
            this.f240a.setVisibility(4);
            return;
        }
        int childAdapterPosition = recyclerView.getChildAdapterPosition(childAt);
        if (childAdapterPosition == -1) {
            this.f240a.setVisibility(4);
            return;
        }
        d1.b bVar = this.f241b;
        while (true) {
            if (bVar.getItemViewType(childAdapterPosition) == 0) {
                break;
            }
            childAdapterPosition--;
            if (childAdapterPosition < 0) {
                childAdapterPosition = -1;
                break;
            }
        }
        if (childAdapterPosition < 0) {
            this.f240a.setVisibility(4);
            return;
        }
        s.b bVar2 = this.c;
        if (childAdapterPosition != bVar2.f5252a) {
            bVar2.f5252a = childAdapterPosition;
            d1.e a2 = this.f241b.a(childAdapterPosition);
            String str = null;
            if (!(a2 instanceof d1.e.c)) {
                a2 = null;
            }
            d1.e.c cVar = (d1.e.c) a2;
            TextView textView = this.f240a;
            if (cVar != null) {
                str = cVar.f216a;
            }
            textView.setText(str);
        }
        int bottom = this.f240a.getBottom();
        int height = this.f240a.getHeight();
        float f = 0.0f;
        int childCount = recyclerView.getChildCount();
        int i3 = 0;
        while (true) {
            if (i3 >= childCount) {
                break;
            }
            View childAt2 = recyclerView.getChildAt(i3);
            int childAdapterPosition2 = recyclerView.getChildAdapterPosition(childAt2);
            if (this.f241b.getItemViewType(childAdapterPosition2) == 0) {
                j.a((Object) childAt2, "child");
                if ((childAt2.getTop() > 0 ? (childAt2.getBottom() + height) - childAt2.getHeight() : childAt2.getBottom()) > bottom && childAt2.getTop() <= bottom) {
                    if (childAdapterPosition2 == childAdapterPosition) {
                        this.f240a.setVisibility(4);
                        return;
                    }
                    f = (float) (childAt2.getTop() - height);
                }
            }
            i3++;
        }
        this.f240a.setVisibility(0);
        this.f240a.setTranslationY(f);
    }
}
