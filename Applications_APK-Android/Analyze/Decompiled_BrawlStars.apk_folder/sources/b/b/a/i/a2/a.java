package b.b.a.i.a2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.supercell.id.R;
import java.util.HashMap;
import kotlin.d.a.b;
import kotlin.d.b.g;
import kotlin.d.b.j;

public final class a extends e {
    public static final C0009a f = new C0009a(null);
    public HashMap e;

    /* renamed from: b.b.a.i.a2.a$a  reason: collision with other inner class name */
    public static final class C0009a {
        public /* synthetic */ C0009a(g gVar) {
        }

        public final a a(String str, String str2, String str3, String str4, String str5) {
            j.b(str, "imageKey");
            j.b(str2, "titleKey");
            j.b(str3, "contentKey");
            j.b(str4, "actionKey");
            j.b(str5, "analyticsLabel");
            a aVar = new a();
            Bundle arguments = aVar.getArguments();
            if (arguments == null) {
                arguments = new Bundle();
            }
            arguments.putString("imageKey", str);
            arguments.putString("titleKey", str2);
            arguments.putString("contentKey", str3);
            arguments.putString("actionKey", str4);
            arguments.putString("analyticsLabel", str5);
            aVar.setArguments(arguments);
            return aVar;
        }
    }

    public final View a(int i) {
        if (this.e == null) {
            this.e = new HashMap();
        }
        View view = (View) this.e.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.e.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.e;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_tutorial_content_page, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.i.x1.j.a(android.widget.ImageView, java.lang.String, boolean, int):void
     arg types: [android.widget.ImageView, java.lang.String, int, int]
     candidates:
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, java.lang.Object, int):void
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, kotlin.d.a.b, int):void
      b.b.a.i.x1.j.a(android.widget.ImageView, java.lang.String, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.Button, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, kotlin.d.a.b, int):void
     arg types: [android.widget.Button, java.lang.String, ?[OBJECT, ARRAY], int]
     candidates:
      b.b.a.i.x1.j.a(android.widget.ImageView, java.lang.String, boolean, int):void
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, java.lang.Object, int):void
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, kotlin.d.a.b, int):void */
    public final void onViewCreated(View view, Bundle bundle) {
        j.b(view, "view");
        super.onViewCreated(view, bundle);
        ImageView imageView = (ImageView) a(R.id.image);
        if (imageView != null) {
            Bundle arguments = getArguments();
            if (arguments == null) {
                j.a();
            }
            String string = arguments.getString("imageKey");
            if (string == null) {
                j.a();
            }
            b.b.a.i.x1.j.a(imageView, string, false, 2);
        }
        TextView textView = (TextView) a(R.id.title);
        j.a((Object) textView, "title");
        Bundle arguments2 = getArguments();
        if (arguments2 == null) {
            j.a();
        }
        String string2 = arguments2.getString("titleKey");
        if (string2 == null) {
            j.a();
        }
        b.b.a.i.x1.j.a(textView, string2, (b) null, 2);
        TextView textView2 = (TextView) a(R.id.content);
        j.a((Object) textView2, "content");
        Bundle arguments3 = getArguments();
        if (arguments3 == null) {
            j.a();
        }
        String string3 = arguments3.getString("contentKey");
        if (string3 == null) {
            j.a();
        }
        b.b.a.i.x1.j.a(textView2, string3, (b) null, 2);
        Button button = (Button) a(R.id.button);
        j.a((Object) button, "button");
        Bundle arguments4 = getArguments();
        if (arguments4 == null) {
            j.a();
        }
        String string4 = arguments4.getString("actionKey");
        if (string4 == null) {
            j.a();
        }
        b.b.a.i.x1.j.a((TextView) button, string4, (b) null, 2);
    }
}
