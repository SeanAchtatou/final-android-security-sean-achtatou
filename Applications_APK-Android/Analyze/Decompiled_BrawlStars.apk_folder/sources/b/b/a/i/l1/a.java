package b.b.a.i.l1;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import b.b.a.e.a;
import b.b.a.i.d;
import b.b.a.j.p;
import b.b.a.j.q1;
import com.facebook.places.model.PlaceFields;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.view.WidthAdjustingMultilineButton;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import kotlin.a.ah;
import kotlin.a.m;
import kotlin.d.b.j;
import kotlin.k;

public final class a extends d {

    /* renamed from: a  reason: collision with root package name */
    public final WeakReference<Activity> f368a;

    /* renamed from: b.b.a.i.l1.a$a  reason: collision with other inner class name */
    public final class C0034a extends AnimatorListenerAdapter {
        public C0034a() {
        }

        public final void onAnimationEnd(Animator animator) {
            a.this.dismiss();
        }
    }

    public final class b implements View.OnClickListener {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ View f371b;

        public b(View view) {
            this.f371b = view;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            a aVar = a.this;
            View view2 = this.f371b;
            j.a((Object) view2, "view");
            aVar.a(view2);
        }
    }

    public final class c implements View.OnClickListener {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ String f372a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ a f373b;
        public final /* synthetic */ View c;

        public c(String str, a aVar, View view) {
            this.f372a = str;
            this.f373b = aVar;
            this.c = view;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Mute Invites", "click", b.a.a.a.a.a(new StringBuilder(), this.f372a, " hours"), null, false, 24);
            a aVar = this.f373b;
            View view2 = this.c;
            j.a((Object) view2, "view");
            aVar.a(view2);
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a(Activity activity) {
        super(activity, R.style.SupercellIdTheme);
        j.b(activity, "activity");
        this.f368a = new WeakReference<>(activity);
    }

    public final void a(View view) {
        j.b(view, "view");
        view.animate().setDuration(150).setInterpolator(b.b.a.f.a.c).alpha(0.0f).setListener(new C0034a()).start();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.app.Activity, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.ImageView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.i.x1.j.a(android.widget.ImageView, java.lang.String, boolean, int):void
     arg types: [android.widget.ImageView, java.lang.String, int, int]
     candidates:
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, java.lang.Object, int):void
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, kotlin.d.a.b, int):void
      b.b.a.i.x1.j.a(android.widget.ImageView, java.lang.String, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.ImageButton, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.Context, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.LinearLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, kotlin.d.a.b, int):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String, ?[OBJECT, ARRAY], int]
     candidates:
      b.b.a.i.x1.j.a(android.widget.ImageView, java.lang.String, boolean, int):void
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, java.lang.Object, int):void
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, kotlin.d.a.b, int):void */
    public final void onCreate(Bundle bundle) {
        Context context;
        super.onCreate(bundle);
        Activity activity = this.f368a.get();
        if (activity != null) {
            j.a((Object) activity, "weakActivity.get() ?: return");
            requestWindowFeature(1);
            Window window = getWindow();
            if (window != null) {
                if (Build.VERSION.SDK_INT >= 21) {
                    window.clearFlags(67108864);
                }
                if (b.b.a.b.b(activity)) {
                    window.addFlags(1056);
                }
            }
            Locale locale = SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getLocale();
            Resources resources = activity.getResources();
            j.a((Object) resources, "activity.resources");
            Configuration configuration = new Configuration(resources.getConfiguration());
            if (Build.VERSION.SDK_INT >= 17) {
                configuration.setLocale(locale);
                context = activity.createConfigurationContext(configuration);
            } else {
                configuration.locale = locale;
                ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(activity, R.style.SupercellIdTheme);
                Resources resources2 = contextThemeWrapper.getResources();
                Resources resources3 = activity.getResources();
                j.a((Object) resources3, "activity.resources");
                resources2.updateConfiguration(configuration, resources3.getDisplayMetrics());
                context = contextThemeWrapper;
            }
            View inflate = LayoutInflater.from(context).inflate(R.layout.dialog_mute_invites, (ViewGroup) null, false);
            setContentView(inflate);
            inflate.setAlpha(0.0f);
            inflate.animate().setDuration(300).setInterpolator(b.b.a.f.a.c).alpha(1.0f).start();
            ImageView imageView = (ImageView) findViewById(R.id.logoImageView);
            j.a((Object) imageView, "logoImageView");
            b.b.a.i.x1.j.a(imageView, "id_logo_black.png", false, 2);
            TextView textView = (TextView) findViewById(R.id.toolbarTitle);
            p.a(textView, "fonts/SupercellTextAndroid_ACorp_Bd.ttf");
            b.b.a.i.x1.j.a(textView, "ingame_mute_invites_title", (kotlin.d.a.b) null, 2);
            ImageButton imageButton = (ImageButton) findViewById(R.id.closeButton);
            j.a((Object) imageButton, "this");
            Context context2 = imageButton.getContext();
            j.a((Object) context2, "this.context");
            imageButton.setImageDrawable(VectorDrawableCompat.create(context2.getResources(), R.drawable.cross, null));
            q1.a(imageButton, -1, a.C0004a.BUTTON_01.ordinal());
            imageButton.setOnClickListener(new b(inflate));
            LinearLayout linearLayout = (LinearLayout) findViewById(R.id.rowsContainer);
            j.a((Object) linearLayout, "rowsContainer");
            b.b.a.b.a(linearLayout, 0, 0.0f, 0.0f, 0.0f, null, 31);
            LinearLayout linearLayout2 = (LinearLayout) findViewById(R.id.rowsContainer);
            kotlin.g.c b2 = kotlin.g.d.b(0, linearLayout2.getChildCount());
            ArrayList arrayList = new ArrayList();
            Iterator it = b2.iterator();
            while (it.hasNext()) {
                View childAt = linearLayout2.getChildAt(((ah) it).a());
                if (childAt != null) {
                    arrayList.add(childAt);
                }
            }
            ArrayList arrayList2 = new ArrayList();
            for (Object next : arrayList) {
                if (next instanceof ViewGroup) {
                    arrayList2.add(next);
                }
            }
            int i = 0;
            for (Object next2 : arrayList2) {
                int i2 = i + 1;
                if (i < 0) {
                    m.a();
                }
                ViewGroup viewGroup = (ViewGroup) next2;
                String valueOf = i != 0 ? String.valueOf(i * 24) : "8";
                TextView textView2 = (TextView) viewGroup.findViewById(R.id.titleTextView);
                p.a(textView2, null, 1);
                b.b.a.i.x1.j.a(textView2, "ingame_mute_invites_hours", k.a(PlaceFields.HOURS, valueOf));
                WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) viewGroup.findViewById(R.id.selectButton);
                p.a(widthAdjustingMultilineButton, "fonts/SupercellTextAndroid_ACorp_Bd.ttf");
                q1.a(widthAdjustingMultilineButton, 0, a.C0004a.BUTTON_01.ordinal());
                b.b.a.i.x1.j.a((TextView) widthAdjustingMultilineButton, "ingame_mute_invites_select_btn", (kotlin.d.a.b) null, 2);
                widthAdjustingMultilineButton.setOnClickListener(new c(valueOf, this, inflate));
                i = i2;
            }
        }
    }

    public final void onStart() {
        super.onStart();
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a("Mute Invites");
    }
}
