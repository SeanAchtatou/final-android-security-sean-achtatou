package b.b.a.i.t1;

import android.view.View;
import b.b.a.e.a;
import b.b.a.i.t1.a;
import b.b.a.j.d;
import com.supercell.id.SupercellId;
import com.supercell.id.view.AvatarView;
import kotlin.d.a.c;
import kotlin.m;

public final class b implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ a.C0065a.C0066a f721a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ d f722b;
    public final /* synthetic */ a.C0065a c;

    public b(a.C0065a.C0066a aVar, d dVar, a.C0065a aVar2, int i) {
        this.f721a = aVar;
        this.f722b = dVar;
        this.c = aVar2;
    }

    public final void onClick(View view) {
        AvatarView.a aVar;
        a.C0065a aVar2 = this.c;
        int i = aVar2.f708b;
        aVar2.f708b = this.f721a.getAdapterPosition();
        if (i >= 0) {
            this.c.notifyItemChanged(i);
        }
        a.C0065a aVar3 = this.c;
        aVar3.notifyItemChanged(aVar3.f708b);
        a.C0065a aVar4 = this.c;
        c<d, AvatarView.a, m> cVar = aVar4.d;
        d dVar = this.f722b;
        int i2 = aVar4.f708b;
        if (i == i2) {
            aVar = AvatarView.a.NONE;
        } else {
            aVar = (i > i2) == SupercellId.INSTANCE.getSharedServices$supercellId_release().e.isRTL() ? AvatarView.a.FROM_RIGHT : AvatarView.a.FROM_LEFT;
        }
        cVar.invoke(dVar, aVar);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().l.a(a.C0004a.BUTTON_01);
    }
}
