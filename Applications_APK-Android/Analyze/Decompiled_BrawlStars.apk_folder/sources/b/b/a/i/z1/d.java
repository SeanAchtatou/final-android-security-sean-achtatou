package b.b.a.i.z1;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import b.b.a.i.a2.c;
import b.b.a.i.f0;
import b.b.a.i.f1;
import b.b.a.i.h1.a;
import b.b.a.i.s;
import b.b.a.j.i0;
import b.b.a.j.j0;
import b.b.a.j.r;
import b.b.a.j.y0;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.Switch;
import com.supercell.id.view.WidthAdjustingMultilineButton;
import java.lang.ref.WeakReference;
import java.util.Date;
import java.util.HashMap;

public final class d extends f1 {

    /* renamed from: b  reason: collision with root package name */
    public final kotlin.d.a.b<i0, kotlin.m> f979b = new k();
    public final y0<b.b.a.h.l> c = new y0<>(l.f992a, new m());
    public HashMap d;

    public final class a extends kotlin.d.b.k implements kotlin.d.a.a<kotlin.m> {
        public a() {
            super(0);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.FrameLayout, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final Object invoke() {
            ((FrameLayout) d.this.a(R.id.infoButtonTutorial)).setBackgroundResource(R.drawable.list_button_bottom);
            View a2 = d.this.a(R.id.infoSpacerHelp);
            kotlin.d.b.j.a((Object) a2, "infoSpacerHelp");
            a2.setVisibility(8);
            FrameLayout frameLayout = (FrameLayout) d.this.a(R.id.infoButtonHelp);
            kotlin.d.b.j.a((Object) frameLayout, "infoButtonHelp");
            frameLayout.setVisibility(8);
            return kotlin.m.f5330a;
        }
    }

    public final class b implements View.OnClickListener {
        public b() {
        }

        public final void onClick(View view) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Settings", "click", "Log out", null, false, 24);
            MainActivity a2 = b.b.a.b.a(d.this);
            if (a2 != null) {
                kotlin.d.b.j.b(a2, "$this$showLogoutDialogPopup");
                a2.a(f0.f.a("account_confirm_heading", "account_confirm_description", "account_confirm_btn_confirm", "account_confirm_btn_cancel"), "popupDialog");
            }
        }
    }

    public final class c implements View.OnClickListener {

        public final class a extends kotlin.d.b.k implements kotlin.d.a.d<a, s, String, kotlin.m> {
            public a() {
                super(3);
            }

            public final Object invoke(Object obj, Object obj2, Object obj3) {
                s sVar = (s) obj2;
                String str = (String) obj3;
                kotlin.d.b.j.b((a) obj, "<anonymous parameter 0>");
                kotlin.d.b.j.b(sVar, "decision");
                int i = c.f978a[sVar.ordinal()];
                if (!(i == 1 || i != 2 || str == null)) {
                    b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Edit nickname dialog", "click", "Continue", null, false, 24);
                    nl.komponents.kovenant.c.m.b(j0.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f(), str, null, null, 6), new e(new WeakReference(b.b.a.b.a(d.this))));
                }
                return kotlin.m.f5330a;
            }
        }

        public c() {
        }

        public final void onClick(View view) {
            i0 i0Var = (i0) SupercellId.INSTANCE.getSharedServices$supercellId_release().f().f1179a;
            b.b.a.h.h a2 = i0Var != null ? i0Var.a() : null;
            if (a2 != null) {
                if (new Date().compareTo(a2.c) >= 0) {
                    b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Settings", "click", "Change name", null, false, 24);
                    MainActivity a3 = b.b.a.b.a(d.this);
                    if (a3 != null) {
                        String str = a2.f121b;
                        kotlin.d.b.j.b(a3, "$this$showEditNicknameDialogPopup");
                        a a4 = a.g.a(str);
                        a3.a(a4, "popupDialog");
                        a4.e = new a();
                        return;
                    }
                    return;
                }
            }
            MainActivity a5 = b.b.a.b.a(d.this);
            if (a5 != null) {
                a5.a("cannot_change_nickname", (kotlin.d.a.b<? super b.b.a.i.e, kotlin.m>) null);
            }
        }
    }

    /* renamed from: b.b.a.i.z1.d$d  reason: collision with other inner class name */
    public final class C0102d implements View.OnClickListener {
        public C0102d() {
        }

        public final void onClick(View view) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Settings", "click", "What is Supercell ID?", null, false, 24);
            MainActivity a2 = b.b.a.b.a(d.this);
            if (a2 != null) {
                a2.a(new c.a(false));
            }
        }
    }

    public final class e implements View.OnClickListener {
        public e() {
        }

        public final void onClick(View view) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Settings", "click", "FAQ", null, false, 24);
            MainActivity a2 = b.b.a.b.a(d.this);
            if (a2 != null) {
                a2.a(new a.b(true));
            }
        }
    }

    public final class f implements View.OnClickListener {
        public f() {
        }

        public final void onClick(View view) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Settings", "click", "Tutorial Video", null, false, 24);
            MainActivity a2 = b.b.a.b.a(d.this);
            if (a2 != null) {
                a2.a("https://youtu.be/VymLtGx_itc");
            }
        }
    }

    public final class g implements View.OnClickListener {
        public g() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.Switch, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            Switch switchR = (Switch) d.this.a(R.id.onlineStatusSwitch);
            kotlin.d.b.j.a((Object) switchR, "onlineStatusSwitch");
            boolean isChecked = switchR.isChecked();
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Settings", "click", "Appear online", Long.valueOf(isChecked ? 1 : 0), false, 16);
            d dVar = d.this;
            if (dVar.getView() != null) {
                dVar.c.a(500, new f(isChecked));
            }
        }
    }

    public final class h extends kotlin.d.b.k implements kotlin.d.a.a<kotlin.m> {
        public h() {
            super(0);
        }

        public final Object invoke() {
            SupercellId.INSTANCE.openSelfHelpPortal$supercellId_release();
            MainActivity a2 = b.b.a.b.a(d.this);
            if (a2 != null) {
                SupercellId.INSTANCE.dismiss$supercellId_release(a2);
            }
            return kotlin.m.f5330a;
        }
    }

    public final class i extends kotlin.d.b.k implements kotlin.d.a.a<kotlin.m> {
        public i() {
            super(0);
        }

        public final Object invoke() {
            MainActivity a2 = b.b.a.b.a(d.this);
            if (a2 != null) {
                String gameHelpLink = SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getGameHelpLink();
                if (gameHelpLink == null) {
                    kotlin.d.b.j.a();
                }
                a2.a(gameHelpLink);
            }
            return kotlin.m.f5330a;
        }
    }

    public final class j implements View.OnClickListener {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ kotlin.d.a.a f990a;

        public j(kotlin.d.a.a aVar) {
            this.f990a = aVar;
        }

        public final void onClick(View view) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Settings", "click", "Help & Support", null, false, 24);
            this.f990a.invoke();
        }
    }

    public final class k extends kotlin.d.b.k implements kotlin.d.a.b<i0, kotlin.m> {
        public k() {
            super(1);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.TextView, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.Button, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final Object invoke(Object obj) {
            i0 i0Var = (i0) obj;
            if (!(d.this.getView() == null || i0Var == null)) {
                TextView textView = (TextView) d.this.a(R.id.tag_text_view);
                kotlin.d.b.j.a((Object) textView, "tag_text_view");
                textView.setText(r.f1160a.a(i0Var.a().f120a));
                TextView textView2 = (TextView) d.this.a(R.id.nickname_text_view);
                kotlin.d.b.j.a((Object) textView2, "nickname_text_view");
                textView2.setText(i0Var.a().f121b);
                Button button = (Button) d.this.a(R.id.change_name_button);
                Button button2 = (Button) d.this.a(R.id.change_name_button);
                kotlin.d.b.j.a((Object) button2, "change_name_button");
                button.setTextColor(ContextCompat.getColor(button2.getContext(), i0Var.a().a() ? R.color.text_blue : R.color.gray80));
            }
            return kotlin.m.f5330a;
        }
    }

    public final class l extends kotlin.d.b.k implements kotlin.d.a.b<b.b.a.h.l, kotlin.m> {

        /* renamed from: a  reason: collision with root package name */
        public static final l f992a = new l();

        public l() {
            super(1);
        }

        public final Object invoke(Object obj) {
            kotlin.d.b.j.b((b.b.a.h.l) obj, "it");
            return kotlin.m.f5330a;
        }
    }

    public final class m extends kotlin.d.b.k implements kotlin.d.a.b<Exception, kotlin.m> {
        public m() {
            super(1);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.Switch, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final Object invoke(Object obj) {
            b.b.a.h.h b2;
            Exception exc = (Exception) obj;
            kotlin.d.b.j.b(exc, "it");
            MainActivity a2 = b.b.a.b.a(d.this);
            if (a2 != null) {
                a2.a(exc, (kotlin.d.a.b<? super b.b.a.i.e, kotlin.m>) null);
            }
            i0 i0Var = (i0) SupercellId.INSTANCE.getSharedServices$supercellId_release().f().f1179a;
            boolean z = (i0Var == null || (b2 = i0Var.b()) == null || b2.h) ? false : true;
            Switch switchR = (Switch) d.this.a(R.id.onlineStatusSwitch);
            kotlin.d.b.j.a((Object) switchR, "onlineStatusSwitch");
            switchR.setChecked(z);
            return kotlin.m.f5330a;
        }
    }

    public final View a(int i2) {
        if (this.d == null) {
            this.d = new HashMap();
        }
        View view = (View) this.d.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i2);
        this.d.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.d;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public final void c() {
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a("Settings");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kotlin.d.b.j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_settings_general_page, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    public final void onPause() {
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f().c(this.f979b);
        super.onPause();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.Switch, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onResume() {
        b.b.a.h.h a2;
        super.onResume();
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f().b(this.f979b);
        i0 i0Var = (i0) SupercellId.INSTANCE.getSharedServices$supercellId_release().f().f1179a;
        if (i0Var != null && (a2 = i0Var.a()) != null) {
            Switch switchR = (Switch) a(R.id.onlineStatusSwitch);
            kotlin.d.b.j.a((Object) switchR, "onlineStatusSwitch");
            switchR.setChecked(!a2.h);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        kotlin.d.a.a aVar;
        kotlin.d.b.j.b(view, "view");
        super.onViewCreated(view, bundle);
        TextView textView = (TextView) a(R.id.versionLabel);
        kotlin.d.b.j.a((Object) textView, "versionLabel");
        textView.setText("Version " + SupercellId.INSTANCE.getVersionString() + ' ' + SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getVersionSuffix());
        TextView textView2 = (TextView) a(R.id.contact_detail_label);
        kotlin.d.b.j.a((Object) textView2, "contact_detail_label");
        textView2.setText(SupercellId.INSTANCE.getSharedServices$supercellId_release().d());
        ((WidthAdjustingMultilineButton) a(R.id.logoutButton)).setOnClickListener(new b());
        ((Button) a(R.id.change_name_button)).setOnClickListener(new c());
        ((FrameLayout) a(R.id.infoButtonSupercellId)).setOnClickListener(new C0102d());
        ((FrameLayout) a(R.id.infoButtonFaq)).setOnClickListener(new e());
        ((FrameLayout) a(R.id.infoButtonTutorial)).setOnClickListener(new f());
        ((Switch) a(R.id.onlineStatusSwitch)).setOnClickListener(new g());
        if (SupercellId.INSTANCE.isSelfHelpPortalAvailable$supercellId_release()) {
            aVar = new h();
        } else {
            String gameHelpLink = SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getGameHelpLink();
            aVar = (gameHelpLink == null || !b.b.a.b.b(gameHelpLink)) ? null : new i();
        }
        if (aVar != null) {
            ((FrameLayout) a(R.id.infoButtonHelp)).setOnClickListener(new j(aVar));
        } else {
            new a().invoke();
        }
    }
}
