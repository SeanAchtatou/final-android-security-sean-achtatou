package b.b.a.i;

import android.animation.ValueAnimator;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.supercell.id.R;
import kotlin.d.b.j;
import kotlin.e.a;

public final class j0 implements ValueAnimator.AnimatorUpdateListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ ValueAnimator f302a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ int f303b;
    public final /* synthetic */ m0 c;

    public j0(ValueAnimator valueAnimator, int i, m0 m0Var) {
        this.f302a = valueAnimator;
        this.f303b = i;
        this.c = m0Var;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.FrameLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        Integer f = this.c.f376a.o();
        if (f != null) {
            int intValue = f.intValue();
            FrameLayout frameLayout = (FrameLayout) this.c.f376a.a(R.id.top_area);
            j.a((Object) frameLayout, "top_area");
            ViewGroup.LayoutParams layoutParams = frameLayout.getLayoutParams();
            int i = this.f303b;
            layoutParams.width = a.a((((float) (intValue - i)) * this.f302a.getAnimatedFraction()) + ((float) i));
            ((FrameLayout) this.c.f376a.a(R.id.top_area)).requestLayout();
        }
    }
}
