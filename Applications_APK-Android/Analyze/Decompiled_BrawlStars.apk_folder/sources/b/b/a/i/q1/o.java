package b.b.a.i.q1;

import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import b.b.a.i.t1.a;
import b.b.a.i.u;
import b.b.a.i.v;
import b.b.a.j.d;
import b.b.a.j.e;
import b.b.a.j.j0;
import b.b.a.j.q1;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.AvatarView;
import com.supercell.id.view.WidthAdjustingMultilineButton;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import kotlin.d.b.j;
import kotlin.d.b.k;
import nl.komponents.kovenant.bw;
import nl.komponents.kovenant.c.m;

public final class o extends b implements u {
    public String c;
    public d d;
    public boolean e;
    public a.C0065a f;
    public a.b g;
    public RecyclerView.LayoutManager h;
    public RecyclerView.LayoutManager i;
    public HashMap j;

    public final class a implements View.OnClickListener {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ String f574b;

        public a(String str) {
            this.f574b = str;
        }

        public final void onClick(View view) {
            String a2 = o.this.g();
            if (a2 != null) {
                b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Onboarding Avatar", "click", "Continue", Long.valueOf(j.a(a2, this.f574b) ^ true ? 1 : 0), false, 16);
                o oVar = o.this;
                oVar.a(true);
                bw a3 = j0.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f(), null, a2, null, 5);
                WeakReference weakReference = new WeakReference(oVar);
                m.a(a3, new p(weakReference));
                m.b(a3, new q(weakReference));
            }
        }
    }

    public final class b extends k implements kotlin.d.a.c<d, AvatarView.a, kotlin.m> {
        public b() {
            super(2);
        }

        public final Object invoke(Object obj, Object obj2) {
            d dVar = (d) obj;
            AvatarView.a aVar = (AvatarView.a) obj2;
            j.b(dVar, "bg");
            j.b(aVar, "animation");
            o oVar = o.this;
            oVar.a(oVar.c, dVar, AvatarView.a.NONE, aVar);
            o.this.a(dVar);
            return kotlin.m.f5330a;
        }
    }

    public final class c extends k implements kotlin.d.a.c<String, AvatarView.a, kotlin.m> {
        public c() {
            super(2);
        }

        public final Object invoke(Object obj, Object obj2) {
            String str = (String) obj;
            AvatarView.a aVar = (AvatarView.a) obj2;
            j.b(str, "avatarName");
            j.b(aVar, "animation");
            o oVar = o.this;
            oVar.a(str, oVar.d, aVar, AvatarView.a.NONE);
            o.this.b(str);
            return kotlin.m.f5330a;
        }
    }

    public final View a(int i2) {
        if (this.j == null) {
            this.j = new HashMap();
        }
        View view = (View) this.j.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i2);
        this.j.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public final void a(v vVar) {
        j.b(vVar, "dialog");
        this.e = false;
        h();
    }

    public final void a(d dVar) {
        this.d = dVar;
        h();
    }

    public final void a(boolean z) {
        this.e = z;
        h();
    }

    public final void b(String str) {
        this.c = str;
        h();
    }

    public final void c() {
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a("Onboarding Avatar");
    }

    public final String g() {
        e a2 = e.e.a(this.c, this.d);
        if (a2 != null) {
            return a2.a();
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.j.q1.a(android.support.v7.widget.AppCompatButton, boolean):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, boolean]
     candidates:
      b.b.a.j.q1.a(android.view.View, int):android.view.View
      b.b.a.j.q1.a(android.support.v4.widget.NestedScrollView, int):void
      b.b.a.j.q1.a(android.support.v4.widget.NestedScrollView, android.view.View):void
      b.b.a.j.q1.a(android.view.View, long):void
      b.b.a.j.q1.a(android.view.View, b.b.a.j.z):void
      b.b.a.j.q1.a(android.view.View, kotlin.d.a.a<kotlin.m>):void
      b.b.a.j.q1.a(android.widget.ScrollView, int):void
      b.b.a.j.q1.a(android.widget.ScrollView, android.view.View):void
      b.b.a.j.q1.a(android.support.v7.widget.AppCompatButton, boolean):void */
    public final void h() {
        WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) a(R.id.profile_continue_button);
        j.a((Object) widthAdjustingMultilineButton, "profile_continue_button");
        boolean z = false;
        q1.a((AppCompatButton) widthAdjustingMultilineButton, !((this.c == null || this.d == null) ? false : true));
        WidthAdjustingMultilineButton widthAdjustingMultilineButton2 = (WidthAdjustingMultilineButton) a(R.id.profile_continue_button);
        j.a((Object) widthAdjustingMultilineButton2, "profile_continue_button");
        if (((this.c == null || this.d == null) ? false : true) && !this.e) {
            z = true;
        }
        widthAdjustingMultilineButton2.setEnabled(z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_onboarding_profile_page, viewGroup, false);
    }

    public final void onDestroyView() {
        MainActivity a2 = b.b.a.b.a(this);
        if (a2 != null) {
            a2.b(this);
        }
        super.onDestroyView();
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.Context, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v7.widget.RecyclerView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0028, code lost:
        if (r9 != null) goto L_0x0035;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onViewCreated(android.view.View r8, android.os.Bundle r9) {
        /*
            r7 = this;
            java.lang.String r0 = "view"
            kotlin.d.b.j.b(r8, r0)
            super.onViewCreated(r8, r9)
            com.supercell.id.ui.MainActivity r9 = b.b.a.b.a(r7)
            if (r9 == 0) goto L_0x0011
            r9.a(r7)
        L_0x0011:
            b.b.a.i.q1.a r9 = r7.f()
            r0 = 0
            if (r9 == 0) goto L_0x001b
            java.lang.String r9 = r9.n
            goto L_0x001c
        L_0x001b:
            r9 = r0
        L_0x001c:
            if (r9 == 0) goto L_0x002b
            boolean r1 = kotlin.j.t.a(r9)
            r1 = r1 ^ 1
            if (r1 == 0) goto L_0x0027
            goto L_0x0028
        L_0x0027:
            r9 = r0
        L_0x0028:
            if (r9 == 0) goto L_0x002b
            goto L_0x0035
        L_0x002b:
            b.b.a.j.e$a r9 = b.b.a.j.e.e
            b.b.a.j.e r9 = r9.b()
            java.lang.String r9 = r9.a()
        L_0x0035:
            int r0 = com.supercell.id.R.id.profile_continue_button
            android.view.View r0 = r7.a(r0)
            com.supercell.id.view.WidthAdjustingMultilineButton r0 = (com.supercell.id.view.WidthAdjustingMultilineButton) r0
            if (r0 == 0) goto L_0x0047
            b.b.a.i.q1.o$a r1 = new b.b.a.i.q1.o$a
            r1.<init>(r9)
            r0.setOnClickListener(r1)
        L_0x0047:
            b.b.a.i.t1.a$a r0 = new b.b.a.i.t1.a$a
            android.content.Context r1 = r8.getContext()
            java.lang.String r2 = "view.context"
            kotlin.d.b.j.a(r1, r2)
            com.supercell.id.ui.MainActivity r3 = b.b.a.b.a(r7)
            b.b.a.i.q1.o$b r4 = new b.b.a.i.q1.o$b
            r4.<init>()
            r0.<init>(r1, r3, r4)
            r7.f = r0
            b.b.a.i.t1.a$a r0 = r7.f
            java.lang.String r1 = "avatarBackgroundAdapter"
            if (r0 != 0) goto L_0x0069
            kotlin.d.b.j.a(r1)
        L_0x0069:
            b.b.a.j.k0 r3 = b.b.a.j.k0.f1078b
            java.util.List r3 = r3.a()
            r0.a(r3)
            android.support.v7.widget.LinearLayoutManager r0 = new android.support.v7.widget.LinearLayoutManager
            android.content.Context r3 = r7.getContext()
            r4 = 0
            r0.<init>(r3, r4, r4)
            r7.h = r0
            int r0 = com.supercell.id.R.id.avatar_backgrounds
            android.view.View r0 = r7.a(r0)
            android.support.v7.widget.RecyclerView r0 = (android.support.v7.widget.RecyclerView) r0
            java.lang.String r3 = "avatar_backgrounds"
            kotlin.d.b.j.a(r0, r3)
            android.support.v7.widget.RecyclerView$LayoutManager r5 = r7.h
            if (r5 != 0) goto L_0x0094
            java.lang.String r6 = "avatarBackgroundsLayoutManager"
            kotlin.d.b.j.a(r6)
        L_0x0094:
            r0.setLayoutManager(r5)
            int r0 = com.supercell.id.R.id.avatar_backgrounds
            android.view.View r0 = r7.a(r0)
            android.support.v7.widget.RecyclerView r0 = (android.support.v7.widget.RecyclerView) r0
            kotlin.d.b.j.a(r0, r3)
            b.b.a.i.t1.a$a r5 = r7.f
            if (r5 != 0) goto L_0x00a9
            kotlin.d.b.j.a(r1)
        L_0x00a9:
            r0.setAdapter(r5)
            b.b.a.i.t1.a$b r0 = new b.b.a.i.t1.a$b
            android.content.Context r8 = r8.getContext()
            kotlin.d.b.j.a(r8, r2)
            com.supercell.id.ui.MainActivity r2 = b.b.a.b.a(r7)
            b.b.a.i.q1.o$c r5 = new b.b.a.i.q1.o$c
            r5.<init>()
            r0.<init>(r8, r2, r5)
            r7.g = r0
            b.b.a.i.t1.a$b r8 = r7.g
            java.lang.String r0 = "avatarImageAdapter"
            if (r8 != 0) goto L_0x00cc
            kotlin.d.b.j.a(r0)
        L_0x00cc:
            b.b.a.j.k0 r2 = b.b.a.j.k0.f1078b
            java.util.List r2 = r2.b()
            r8.a(r2)
            android.support.v7.widget.LinearLayoutManager r8 = new android.support.v7.widget.LinearLayoutManager
            android.content.Context r2 = r7.getContext()
            r8.<init>(r2, r4, r4)
            r7.i = r8
            int r8 = com.supercell.id.R.id.avatar_images
            android.view.View r8 = r7.a(r8)
            android.support.v7.widget.RecyclerView r8 = (android.support.v7.widget.RecyclerView) r8
            java.lang.String r2 = "avatar_images"
            kotlin.d.b.j.a(r8, r2)
            android.support.v7.widget.RecyclerView$LayoutManager r5 = r7.i
            if (r5 != 0) goto L_0x00f6
            java.lang.String r6 = "avatarImagesLayoutManager"
            kotlin.d.b.j.a(r6)
        L_0x00f6:
            r8.setLayoutManager(r5)
            int r8 = com.supercell.id.R.id.avatar_images
            android.view.View r8 = r7.a(r8)
            android.support.v7.widget.RecyclerView r8 = (android.support.v7.widget.RecyclerView) r8
            kotlin.d.b.j.a(r8, r2)
            b.b.a.i.t1.a$b r5 = r7.g
            if (r5 != 0) goto L_0x010b
            kotlin.d.b.j.a(r0)
        L_0x010b:
            r8.setAdapter(r5)
            b.b.a.j.e$a r8 = b.b.a.j.e.e
            b.b.a.j.e r8 = r8.a(r9)
            java.lang.String r9 = r8.f1027a
            r7.c = r9
            r7.h()
            b.b.a.j.d r9 = r8.f1028b
            r7.d = r9
            r7.h()
            java.lang.String r9 = r8.f1027a
            b.b.a.j.d r5 = r8.f1028b
            com.supercell.id.view.AvatarView$a r6 = com.supercell.id.view.AvatarView.a.NONE
            r7.a(r9, r5, r6, r6)
            java.lang.String r9 = r8.f1027a
            b.b.a.j.k0 r5 = b.b.a.j.k0.f1078b
            java.util.List r5 = r5.b()
            int r9 = r5.indexOf(r9)
            r5 = -1
            if (r9 == r5) goto L_0x013b
            goto L_0x013c
        L_0x013b:
            r9 = 0
        L_0x013c:
            b.b.a.i.t1.a$b r6 = r7.g
            if (r6 != 0) goto L_0x0143
            kotlin.d.b.j.a(r0)
        L_0x0143:
            r6.f711b = r9
            b.b.a.i.t1.a$b r6 = r7.g
            if (r6 != 0) goto L_0x014c
            kotlin.d.b.j.a(r0)
        L_0x014c:
            r6.notifyItemChanged(r9)
            int r0 = com.supercell.id.R.id.avatar_images
            android.view.View r0 = r7.a(r0)
            android.support.v7.widget.RecyclerView r0 = (android.support.v7.widget.RecyclerView) r0
            kotlin.d.b.j.a(r0, r2)
            b.b.a.b.b(r0, r9)
            b.b.a.j.d r8 = r8.f1028b
            int r9 = r8.f1023a
            int r8 = r8.f1024b
            b.b.a.j.k0 r0 = b.b.a.j.k0.f1078b
            java.util.List r0 = r0.a()
            b.b.a.j.d r2 = new b.b.a.j.d
            r2.<init>(r9, r8)
            int r8 = r0.indexOf(r2)
            if (r8 == r5) goto L_0x0175
            goto L_0x0176
        L_0x0175:
            r8 = 0
        L_0x0176:
            b.b.a.i.t1.a$a r9 = r7.f
            if (r9 != 0) goto L_0x017d
            kotlin.d.b.j.a(r1)
        L_0x017d:
            r9.f708b = r8
            b.b.a.i.t1.a$a r9 = r7.f
            if (r9 != 0) goto L_0x0186
            kotlin.d.b.j.a(r1)
        L_0x0186:
            r9.notifyItemChanged(r8)
            int r9 = com.supercell.id.R.id.avatar_backgrounds
            android.view.View r9 = r7.a(r9)
            android.support.v7.widget.RecyclerView r9 = (android.support.v7.widget.RecyclerView) r9
            kotlin.d.b.j.a(r9, r3)
            b.b.a.b.b(r9, r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.i.q1.o.onViewCreated(android.view.View, android.os.Bundle):void");
    }

    public final void a(String str, d dVar, AvatarView.a aVar, AvatarView.a aVar2) {
        if (str != null && dVar != null) {
            AvatarView avatarView = (AvatarView) a(R.id.profile_image);
            if (avatarView != null) {
                avatarView.setBackgroundGradient(dVar.f1023a, dVar.f1024b, aVar2);
            }
            AvatarView avatarView2 = (AvatarView) a(R.id.profile_image);
            if (avatarView2 != null) {
                avatarView2.setAvatar(str, aVar);
            }
        }
    }
}
