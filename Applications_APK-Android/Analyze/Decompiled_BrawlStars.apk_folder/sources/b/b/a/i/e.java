package b.b.a.i;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;
import com.supercell.id.R;
import java.util.HashMap;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public abstract class e extends DialogFragment {

    /* renamed from: a  reason: collision with root package name */
    public kotlin.d.a.b<? super e, m> f234a;

    /* renamed from: b  reason: collision with root package name */
    public final long f235b = 300;
    public final long c = 150;
    public HashMap d;

    public final class a extends AnimatorListenerAdapter {
        public a() {
        }

        public final void onAnimationEnd(Animator animator) {
            try {
                e.this.dismissAllowingStateLoss();
            } catch (Exception unused) {
            }
        }
    }

    public final class b extends k implements kotlin.d.a.a<m> {
        public b() {
            super(0);
        }

        public final Object invoke() {
            e.super.dismiss();
            return m.f5330a;
        }
    }

    public final class c extends Dialog {
        public c(Context context, int i) {
            super(context, i);
        }

        public final void onBackPressed() {
            e.this.b();
        }
    }

    public void a() {
        HashMap hashMap = this.d;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public void b() {
        View view = getView();
        if (view != null) {
            view.animate().setDuration(c()).setInterpolator(b.b.a.f.a.c).alpha(0.0f).setListener(new a()).start();
        } else {
            new b().invoke();
        }
    }

    public long c() {
        return this.c;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.app.Dialog, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        Dialog dialog = getDialog();
        j.a((Object) dialog, "dialog");
        Window window = dialog.getWindow();
        if (window != null) {
            if (Build.VERSION.SDK_INT >= 21) {
                window.clearFlags(67108864);
            }
            FragmentActivity activity = getActivity();
            if (activity != null && b.b.a.b.b(activity)) {
                window.addFlags(1056);
            }
        }
        View view = getView();
        if (view != null) {
            view.setAlpha(0.0f);
            view.animate().setDuration(this.f235b).setInterpolator(b.b.a.f.a.c).alpha(1.0f).start();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(1, R.style.SupercellIdPopupDialogTheme);
    }

    public Dialog onCreateDialog(Bundle bundle) {
        FragmentActivity activity = getActivity();
        if (activity == null) {
            j.a();
        }
        return new c(activity, getTheme());
    }

    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        kotlin.d.a.b<? super e, m> bVar = this.f234a;
        if (bVar != null) {
            bVar.invoke(this);
        }
    }
}
