package b.b.a.i.r1;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.FlowPager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import b.b.a.e.a;
import b.b.a.i.b;
import b.b.a.i.g;
import b.b.a.i.n1.a;
import b.b.a.i.p1.i;
import b.b.a.i.t1.a;
import b.b.a.i.x0;
import b.b.a.i.y0;
import b.b.a.i.z;
import b.b.a.i.z1.b;
import b.b.a.j.a0;
import b.b.a.j.c1;
import b.b.a.j.d1;
import b.b.a.j.f0;
import b.b.a.j.i0;
import b.b.a.j.k0;
import b.b.a.j.o1;
import b.b.a.j.q1;
import b.b.a.j.y;
import com.facebook.share.internal.ShareConstants;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.EdgeAntialiasingImageView;
import com.supercell.id.view.WidthAdjustingMultilineTextView;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import kotlin.a.ad;
import kotlin.a.ah;
import kotlin.a.an;
import nl.komponents.kovenant.ap;

public final class n extends b.b.a.i.g {
    public static final List<c1> r = kotlin.a.m.a((Object[]) new c1[]{new c1("account_friends_tab", "tab_icon_face_blue.png", "tab_icon_face_red.png", "tab_icon_face_disabled.png", "tab_icon_face_disabled.png", e.class), new c1("account_games_tab", "tab_icon_star.png", "tab_icon_sword.png", "tab_icon_star_disabled.png", "tab_icon_sword_disabled.png", c.class)});
    public static final b s = new b(null);
    public final kotlin.d.a.b<i0, kotlin.m> m = new m();
    public final kotlin.d.a.b<b.b.a.j.m<b.b.a.h.d, f0>, kotlin.m> n = new l();
    public final kotlin.d.a.b<b.b.a.h.a, kotlin.m> o = new k();
    public int p;
    public HashMap q;

    public static final class a extends b.a implements a0 {
        public static final Parcelable.Creator<a> CREATOR = new C0060a();
        public static final b g = new b(null);
        public final boolean e;
        public final Class<? extends b.b.a.i.g> f = n.class;

        /* renamed from: b.b.a.i.r1.n$a$a  reason: collision with other inner class name */
        public final class C0060a implements Parcelable.Creator<a> {
            public final a createFromParcel(Parcel parcel) {
                kotlin.d.b.j.b(parcel, ShareConstants.FEED_SOURCE_PARAM);
                kotlin.d.b.j.b(parcel, "parcel");
                return new a();
            }

            public final a[] newArray(int i) {
                return new a[i];
            }
        }

        public static final class b {
            public /* synthetic */ b(kotlin.d.b.g gVar) {
            }

            public final float a() {
                return b.b.a.b.a(80);
            }

            public final float b() {
                return b.b.a.b.a(87);
            }
        }

        public final int a(int i, int i2, int i3) {
            return b.b.a.i.a0.u.a(i, i2, i3);
        }

        public final int a(Resources resources, int i, int i2, int i3) {
            kotlin.d.b.j.b(resources, "resources");
            kotlin.d.b.j.b(resources, "$this$isSmallScreen");
            if (resources.getBoolean(R.bool.isSmallScreen)) {
                return 0;
            }
            return (i2 + kotlin.e.a.a(b.b.a.b.a(150))) - kotlin.e.a.a(g.b());
        }

        public final Class<? extends b.b.a.i.g> a() {
            return this.f;
        }

        public final Class<? extends x0> a(Resources resources) {
            kotlin.d.b.j.b(resources, "resources");
            return b.b.a.b.b(resources) ? z.class : y0.class;
        }

        public final int b(Resources resources, int i, int i2, int i3) {
            float f2;
            kotlin.d.b.j.b(resources, "resources");
            kotlin.d.b.j.b(resources, "$this$isSmallScreen");
            if (resources.getBoolean(R.bool.isSmallScreen)) {
                f2 = g.a();
            } else {
                kotlin.d.b.j.b(resources, "$this$isSortOfATablet");
                if (!resources.getBoolean(R.bool.isSortOfATablet)) {
                    return kotlin.e.a.a(g.b());
                }
                f2 = b.b.a.b.a(150);
            }
            return i2 + kotlin.e.a.a(f2);
        }

        public final int c(Resources resources, int i, int i2, int i3) {
            kotlin.d.b.j.b(resources, "resources");
            return b.b.a.i.a0.u.b(i, i2, i3);
        }

        public final boolean c(Resources resources) {
            kotlin.d.b.j.b(resources, "resources");
            return !b.b.a.b.b(resources);
        }

        public final int describeContents() {
            return 0;
        }

        public final Class<? extends b.b.a.i.g> e(Resources resources) {
            kotlin.d.b.j.b(resources, "resources");
            return b.b.a.b.b(resources) ? b.b.a.i.a0.class : c.class;
        }

        public final boolean e() {
            return this.e;
        }

        public final void writeToParcel(Parcel parcel, int i) {
            kotlin.d.b.j.b(parcel, "dest");
        }
    }

    public static final class b {
        public /* synthetic */ b(kotlin.d.b.g gVar) {
        }

        public final List<c1> a() {
            return n.r;
        }
    }

    public static final class c extends b.b.a.i.g {
        public HashMap m;

        public final void a() {
            HashMap hashMap = this.m;
            if (hashMap != null) {
                hashMap.clear();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
            kotlin.d.b.j.b(layoutInflater, "inflater");
            return layoutInflater.inflate(R.layout.fragment_profile_top_area, viewGroup, false);
        }

        public final /* synthetic */ void onDestroyView() {
            super.onDestroyView();
            a();
        }
    }

    public final class d extends kotlin.d.b.k implements kotlin.d.a.c<View, Integer, kotlin.m> {
        public d() {
            super(2);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.support.v4.view.FlowPager, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.EdgeAntialiasingImageView, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final Object invoke(Object obj, Object obj2) {
            View view = (View) obj;
            int intValue = ((Number) obj2).intValue();
            kotlin.d.b.j.b(view, "view");
            FlowPager flowPager = (FlowPager) n.this.a(R.id.tabPager);
            kotlin.d.b.j.a((Object) flowPager, "tabPager");
            if (flowPager.getCurrentItem() != intValue) {
                FlowPager flowPager2 = (FlowPager) n.this.a(R.id.tabPager);
                kotlin.d.b.j.a((Object) flowPager2, "tabPager");
                flowPager2.setCurrentItem(intValue);
            } else {
                SupercellId.INSTANCE.getSharedServices$supercellId_release().l.a(a.C0004a.TAB_SWITCH);
                EdgeAntialiasingImageView edgeAntialiasingImageView = (EdgeAntialiasingImageView) view.findViewById(R.id.tab_icon_left);
                kotlin.d.b.j.a((Object) edgeAntialiasingImageView, "view.tab_icon_left");
                EdgeAntialiasingImageView edgeAntialiasingImageView2 = (EdgeAntialiasingImageView) view.findViewById(R.id.tab_icon_right);
                kotlin.d.b.j.a((Object) edgeAntialiasingImageView2, "view.tab_icon_right");
                b.b.a.b.a(edgeAntialiasingImageView, edgeAntialiasingImageView2, 0, 4);
            }
            return kotlin.m.f5330a;
        }
    }

    public final class e implements View.OnClickListener {
        public e() {
        }

        public final void onClick(View view) {
            MainActivity a2 = b.b.a.b.a((Fragment) n.this);
            if (a2 != null) {
                a2.a(new a.C0036a());
            }
        }
    }

    public final class f extends ViewPager.SimpleOnPageChangeListener {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ View f633a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ n f634b;

        public final class a extends AnimatorListenerAdapter {

            /* renamed from: a  reason: collision with root package name */
            public boolean f635a;
            public final /* synthetic */ int c;

            public a(int i) {
                this.c = i;
            }

            public final void onAnimationCancel(Animator animator) {
                this.f635a = true;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
             arg types: [android.view.View, java.lang.String]
             candidates:
              kotlin.d.b.j.a(int, int):int
              kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
              kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
              kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
            public final void onAnimationEnd(Animator animator) {
                kotlin.d.b.j.b(animator, "animation");
                boolean z = this.f635a;
                View view = f.this.f633a;
                kotlin.d.b.j.a((Object) view, "add_friends_button");
                view.setEnabled(true);
                if (!z && this.c != 0) {
                    View view2 = f.this.f633a;
                    kotlin.d.b.j.a((Object) view2, "add_friends_button");
                    view2.setVisibility(8);
                }
            }
        }

        public f(View view, n nVar) {
            this.f633a = view;
            this.f634b = nVar;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.ViewPropertyAnimator, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onPageSelected(int i) {
            this.f634b.p = i;
            View view = this.f633a;
            kotlin.d.b.j.a((Object) view, "add_friends_button");
            view.setEnabled(false);
            View view2 = this.f633a;
            kotlin.d.b.j.a((Object) view2, "add_friends_button");
            view2.setVisibility(0);
            float f = i == 0 ? 1.0f : 0.0f;
            ViewPropertyAnimator scaleY = this.f633a.animate().setDuration(200).setInterpolator(b.b.a.f.a.f62a).alpha(f).scaleX(f).scaleY(f);
            kotlin.d.b.j.a((Object) scaleY, "add_friends_button.anima…          .scaleY(target)");
            ViewPropertyAnimator listener = scaleY.setListener(new a(i));
            kotlin.d.b.j.a((Object) listener, "setListener(object : Ani…d = true\n        }\n    })");
            listener.start();
        }
    }

    public final class g implements View.OnClickListener {
        public g() {
        }

        public final void onClick(View view) {
            MainActivity a2 = b.b.a.b.a((Fragment) n.this);
            if (a2 != null) {
                a2.a(new b.a());
            }
        }
    }

    public final class h implements View.OnClickListener {
        public h() {
        }

        public final void onClick(View view) {
            MainActivity a2 = b.b.a.b.a((Fragment) n.this);
            if (a2 != null) {
                a2.a(new i.a());
            }
        }
    }

    public final class i implements View.OnClickListener {
        public i() {
        }

        public final void onClick(View view) {
            b.b.a.h.h a2;
            i0 i0Var = (i0) SupercellId.INSTANCE.getSharedServices$supercellId_release().f().f1179a;
            if (!(i0Var == null || (a2 = i0Var.a()) == null)) {
                if (new Date().compareTo(a2.e) >= 0) {
                    MainActivity a3 = b.b.a.b.a((Fragment) n.this);
                    if (a3 != null) {
                        a3.a(new a.c());
                    }
                    SupercellId.INSTANCE.getSharedServices$supercellId_release().l.a(a.C0004a.BUTTON_01);
                    return;
                }
            }
            MainActivity a4 = b.b.a.b.a((Fragment) n.this);
            if (a4 != null) {
                a4.a("cannot_change_avatar", (kotlin.d.a.b<? super b.b.a.i.e, kotlin.m>) null);
            }
        }
    }

    public final class j extends kotlin.d.b.k implements kotlin.d.a.a<List<? extends c1>> {

        /* renamed from: a  reason: collision with root package name */
        public static final j f640a = new j();

        public j() {
            super(0);
        }

        public final Object invoke() {
            return n.s.a();
        }
    }

    public final class k extends kotlin.d.b.k implements kotlin.d.a.b<b.b.a.h.a, kotlin.m> {
        public k() {
            super(1);
        }

        public final Object invoke(Object obj) {
            n.this.a((i0) SupercellId.INSTANCE.getSharedServices$supercellId_release().f().f1179a);
            return kotlin.m.f5330a;
        }
    }

    public final class l extends kotlin.d.b.k implements kotlin.d.a.b<b.b.a.j.m<? extends b.b.a.h.d, ? extends f0>, kotlin.m> {
        public l() {
            super(1);
        }

        public final Object invoke(Object obj) {
            b.b.a.h.d dVar;
            List<b.b.a.h.c> list;
            b.b.a.j.m mVar = (b.b.a.j.m) obj;
            int i = 0;
            int size = (mVar == null || (dVar = (b.b.a.h.d) mVar.a()) == null || (list = dVar.c) == null) ? 0 : list.size();
            TextView textView = (TextView) n.this.a(R.id.profile_messages_indicator);
            if (textView != null) {
                if (size <= 0) {
                    i = 8;
                }
                textView.setVisibility(i);
                textView.setText(String.valueOf(size));
            }
            n.this.a((i0) SupercellId.INSTANCE.getSharedServices$supercellId_release().f().f1179a);
            return kotlin.m.f5330a;
        }
    }

    public final class m extends kotlin.d.b.k implements kotlin.d.a.b<i0, kotlin.m> {
        public m() {
            super(1);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.content.res.Resources, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final Object invoke(Object obj) {
            String str;
            String str2;
            i0 i0Var = (i0) obj;
            b.b.a.h.h a2 = i0Var != null ? i0Var.a() : null;
            int i = 8;
            if (a2 == null || a2.b()) {
                View a3 = n.this.a(R.id.profile_content);
                if (a3 != null) {
                    a3.setVisibility(8);
                }
                FrameLayout frameLayout = (FrameLayout) n.this.a(R.id.profile_progress_bar);
                if (frameLayout != null) {
                    frameLayout.setVisibility(0);
                }
            } else {
                View a4 = n.this.a(R.id.profile_content);
                if (a4 != null) {
                    a4.setVisibility(0);
                }
                FrameLayout frameLayout2 = (FrameLayout) n.this.a(R.id.profile_progress_bar);
                if (frameLayout2 != null) {
                    frameLayout2.setVisibility(8);
                }
            }
            ImageView imageView = (ImageView) n.this.a(R.id.online_status_indicator);
            if (imageView != null) {
                imageView.setEnabled(!(a2 != null ? a2.h : false));
            }
            TextView textView = (TextView) n.this.a(R.id.online_status_text);
            if (textView != null) {
                if (a2 != null && !a2.h) {
                    i = 0;
                }
                textView.setVisibility(i);
            }
            ImageView imageView2 = (ImageView) n.this.a(R.id.profile_image);
            if (imageView2 != null) {
                k0 k0Var = k0.f1078b;
                if (a2 == null || (str2 = a2.d) == null) {
                    str2 = "";
                }
                Resources resources = n.this.getResources();
                kotlin.d.b.j.a((Object) resources, "resources");
                k0Var.a(str2, imageView2, resources);
            }
            WidthAdjustingMultilineTextView widthAdjustingMultilineTextView = (WidthAdjustingMultilineTextView) n.this.a(R.id.profile_name);
            if (widthAdjustingMultilineTextView != null) {
                if (a2 == null || (str = a2.f121b) == null) {
                    str = SupercellId.INSTANCE.getSharedServices$supercellId_release().d();
                }
                widthAdjustingMultilineTextView.setText(str);
            }
            n.this.a(i0Var);
            return kotlin.m.f5330a;
        }
    }

    /* renamed from: b.b.a.i.r1.n$n  reason: collision with other inner class name */
    public final class C0061n extends kotlin.d.b.k implements kotlin.d.a.a<kotlin.m> {
        public C0061n() {
            super(0);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.ImageView, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final Object invoke() {
            ImageView imageView = (ImageView) n.this.a(R.id.online_status_indicator);
            if (imageView != null) {
                ImageView imageView2 = (ImageView) n.this.a(R.id.profile_image);
                kotlin.d.b.j.a((Object) imageView2, "profile_image");
                kotlin.d.b.j.b(imageView, "indicator");
                kotlin.d.b.j.b(imageView2, "companion");
                float width = (((float) imageView2.getWidth()) / b.b.a.b.a(1)) / 3.3f;
                if (Float.compare(width, 14.0f) < 0) {
                    width = 14.0f;
                } else if (Float.compare(width, 24.0f) > 0) {
                    width = 24.0f;
                }
                float f = width * b.b.a.b.f16a;
                int width2 = imageView2.getWidth() / 2;
                ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
                if (!(layoutParams instanceof ConstraintLayout.LayoutParams)) {
                    layoutParams = null;
                }
                ConstraintLayout.LayoutParams layoutParams2 = (ConstraintLayout.LayoutParams) layoutParams;
                if (layoutParams2 == null || width2 != layoutParams2.circleRadius) {
                    imageView.post(new y(imageView, layoutParams2, f, width2));
                }
            }
            return kotlin.m.f5330a;
        }
    }

    public final class o extends kotlin.d.b.k implements kotlin.d.a.b<List<? extends String>, kotlin.m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f645a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public o(WeakReference weakReference) {
            super(1);
            this.f645a = weakReference;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [b.b.a.i.r1.n, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.a.p.a(java.lang.Iterable, int):int
         arg types: [java.util.List<b.b.a.h.c>, int]
         candidates:
          kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
          kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
          kotlin.a.w.a(java.util.List, int):T
          kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
          kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
          kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
          kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
          kotlin.a.s.a(java.util.List, java.util.Comparator):void
          kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
          kotlin.a.p.a(java.lang.Iterable, int):int */
        public final void a(List<String> list) {
            Set<String> set;
            Collection collection;
            b.b.a.h.d dVar;
            List<b.b.a.h.c> list2;
            kotlin.d.b.j.b(list, "ingameFriends");
            n nVar = (n) this.f645a.get();
            if (nVar != null) {
                kotlin.d.b.j.a((Object) nVar, "weakView.get() ?: return@get");
                if (nVar.isAdded()) {
                    Set i = kotlin.a.m.i(list);
                    b.b.a.h.a aVar = SupercellId.INSTANCE.getSharedServices$supercellId_release().c().f1179a;
                    if (aVar == null || (set = aVar.f107a) == null) {
                        set = ad.f5212a;
                    }
                    Set a2 = an.a(i, set);
                    b.b.a.j.m mVar = SupercellId.INSTANCE.getSharedServices$supercellId_release().e().f1179a;
                    if (mVar == null || (dVar = (b.b.a.h.d) mVar.a()) == null || (list2 = dVar.f112a) == null) {
                        collection = (Set) ad.f5212a;
                    } else {
                        collection = new ArrayList(kotlin.a.m.a((Iterable) list2, 10));
                        for (b.b.a.h.c cVar : list2) {
                            collection.add(cVar.f110a);
                        }
                    }
                    int size = an.a(a2, collection).size();
                    ImageView imageView = (ImageView) nVar.a(R.id.addFriendsButtonPlusSign);
                    int i2 = 8;
                    if (imageView != null) {
                        imageView.setVisibility(size > 0 ? 8 : 0);
                    }
                    TextView textView = (TextView) nVar.a(R.id.addFriendsButtonNotification);
                    if (textView != null) {
                        if (size > 0) {
                            i2 = 0;
                        }
                        textView.setVisibility(i2);
                    }
                    TextView textView2 = (TextView) nVar.a(R.id.addFriendsButtonNotification);
                    if (textView2 != null) {
                        textView2.setText(String.valueOf(size));
                    }
                }
            }
        }

        public final /* synthetic */ Object invoke(Object obj) {
            a((List) obj);
            return kotlin.m.f5330a;
        }
    }

    public final View a(int i2) {
        if (this.q == null) {
            this.q = new HashMap();
        }
        View view = (View) this.q.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i2);
        this.q.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.j.q1.a(android.view.View, long):void
     arg types: [android.widget.FrameLayout, int]
     candidates:
      b.b.a.j.q1.a(android.view.View, int):android.view.View
      b.b.a.j.q1.a(android.support.v4.widget.NestedScrollView, int):void
      b.b.a.j.q1.a(android.support.v4.widget.NestedScrollView, android.view.View):void
      b.b.a.j.q1.a(android.support.v7.widget.AppCompatButton, boolean):void
      b.b.a.j.q1.a(android.view.View, b.b.a.j.z):void
      b.b.a.j.q1.a(android.view.View, kotlin.d.a.a<kotlin.m>):void
      b.b.a.j.q1.a(android.widget.ScrollView, int):void
      b.b.a.j.q1.a(android.widget.ScrollView, android.view.View):void
      b.b.a.j.q1.a(android.view.View, long):void */
    public final void a(View view, g.a aVar, boolean z) {
        kotlin.d.b.j.b(view, "view");
        kotlin.d.b.j.b(aVar, "animation");
        super.a(view, aVar, z);
        Resources resources = getResources();
        kotlin.d.b.j.a((Object) resources, "resources");
        if (!b.b.a.b.b(resources)) {
            int i2 = o.f646a[aVar.ordinal()];
            if (i2 == 1) {
                for (View view2 : kotlin.a.m.d((FrameLayout) a(R.id.profile_image_container), (ImageView) a(R.id.online_status_indicator), (TextView) a(R.id.online_status_text))) {
                    view2.setAlpha(0.0f);
                    view2.animate().alpha(1.0f).setStartDelay(175).setDuration(175).setInterpolator(b.b.a.f.a.f62a).start();
                }
            } else if (i2 == 2) {
                FrameLayout frameLayout = (FrameLayout) a(R.id.profile_image_container);
                if (frameLayout != null) {
                    q1.a((View) frameLayout, 175L);
                }
                for (View view3 : kotlin.a.m.d((ImageView) a(R.id.online_status_indicator), (TextView) a(R.id.online_status_text))) {
                    view3.setAlpha(0.0f);
                    view3.animate().alpha(1.0f).setStartDelay(475).setDuration(175).setInterpolator(b.b.a.f.a.f62a).start();
                }
            } else if (i2 == 3) {
                FrameLayout frameLayout2 = (FrameLayout) a(R.id.profile_image_container);
                if (frameLayout2 != null) {
                    q1.a((View) frameLayout2, 300L);
                }
                for (View view4 : kotlin.a.m.d((ImageView) a(R.id.online_status_indicator), (TextView) a(R.id.online_status_text))) {
                    view4.setAlpha(0.0f);
                    view4.animate().alpha(1.0f).setStartDelay(600).setDuration(175).setInterpolator(b.b.a.f.a.f62a).start();
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.app.FragmentManager, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.view.FlowPager, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        ImageButton imageButton = (ImageButton) a(R.id.profile_settings_button);
        if (imageButton != null) {
            imageButton.setOnClickListener(new g());
        }
        ImageButton imageButton2 = (ImageButton) a(R.id.profile_messages_button);
        if (imageButton2 != null) {
            imageButton2.setOnClickListener(new h());
        }
        ImageView imageView = (ImageView) a(R.id.profile_image);
        if (imageView != null) {
            imageView.setOnClickListener(new i());
        }
        ImageView imageView2 = (ImageView) a(R.id.profile_image);
        if (imageView2 != null) {
            imageView2.setSoundEffectsEnabled(false);
        }
        FragmentManager childFragmentManager = getChildFragmentManager();
        kotlin.d.b.j.a((Object) childFragmentManager, "childFragmentManager");
        d1 d1Var = new d1(childFragmentManager, r);
        FlowPager flowPager = (FlowPager) a(R.id.tabPager);
        kotlin.d.b.j.a((Object) flowPager, "tabPager");
        flowPager.setAdapter(d1Var);
        View a2 = a(R.id.addFriendsButton);
        a2.setOnClickListener(new e());
        ((FlowPager) a(R.id.tabPager)).addOnPageChangeListener(new f(a2, this));
        LinearLayout linearLayout = (LinearLayout) a(R.id.tabBarView);
        if (linearLayout != null) {
            kotlin.g.c b2 = kotlin.g.d.b(0, linearLayout.getChildCount());
            ArrayList arrayList = new ArrayList();
            Iterator it = b2.iterator();
            while (it.hasNext()) {
                View childAt = linearLayout.getChildAt(((ah) it).a());
                if (childAt != null) {
                    arrayList.add(childAt);
                }
            }
            b.b.a.b.a(getContext(), arrayList, r);
            b.b.a.b.a(getContext(), arrayList, j.f640a, (FlowPager) a(R.id.tabPager), new d());
        }
        this.m.invoke(SupercellId.INSTANCE.getSharedServices$supercellId_release().f().f1179a);
        this.n.invoke(SupercellId.INSTANCE.getSharedServices$supercellId_release().e().f1179a);
    }

    public final void onAttach(Context context) {
        super.onAttach(context);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f().b(this.m);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().e().b(this.n);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().c().b(this.o);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kotlin.d.b.j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_profile, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    public final void onDetach() {
        SupercellId.INSTANCE.getSharedServices$supercellId_release().e().c(this.n);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f().c(this.m);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().c().c(this.o);
        super.onDetach();
    }

    public final void onSaveInstanceState(Bundle bundle) {
        kotlin.d.b.j.b(bundle, "outState");
        bundle.putInt("selectedTab", this.p);
        super.onSaveInstanceState(bundle);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.app.Fragment, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.view.FlowPager, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.b.a(android.content.Context, java.util.List<? extends android.view.View>, java.util.List<b.b.a.j.c1>, int, boolean):void
     arg types: [android.content.Context, java.util.ArrayList, java.util.List<b.b.a.j.c1>, int, int]
     candidates:
      b.b.a.b.a(android.content.Context, java.util.List, int, boolean, int):void
      b.b.a.b.a(android.content.Context, java.util.List<? extends android.view.View>, kotlin.d.a.a<? extends java.util.List<b.b.a.j.c1>>, android.support.v4.view.ViewPager, kotlin.d.a.c<? super android.view.View, ? super java.lang.Integer, kotlin.m>):void
      b.b.a.b.a(android.view.View, boolean, boolean, int, int):void
      b.b.a.b.a(android.content.Context, java.util.List<? extends android.view.View>, java.util.List<b.b.a.j.c1>, int, boolean):void */
    public final void onStart() {
        b.b.a.i.g gVar;
        List<Fragment> fragments;
        super.onStart();
        FragmentManager fragmentManager = getFragmentManager();
        boolean z = true;
        if (fragmentManager == null || (fragments = fragmentManager.getFragments()) == null) {
            gVar = null;
        } else {
            ArrayList arrayList = new ArrayList();
            for (T next : fragments) {
                Fragment fragment = (Fragment) next;
                kotlin.d.b.j.a((Object) fragment, "it");
                if (fragment.getId() == R.id.top_area) {
                    arrayList.add(next);
                }
            }
            ArrayList arrayList2 = new ArrayList();
            for (Object next2 : arrayList) {
                if (next2 instanceof b.b.a.i.a0) {
                    arrayList2.add(next2);
                }
            }
            gVar = (b.b.a.i.g) kotlin.a.m.d((List) arrayList2);
        }
        b.b.a.i.a0 a0Var = (b.b.a.i.a0) gVar;
        if (a0Var != null) {
            Integer num = a0Var.n;
            if (num != null) {
                if (num.intValue() < 0) {
                    z = false;
                }
                if (!z) {
                    num = null;
                }
                if (num != null) {
                    int intValue = num.intValue();
                    FlowPager flowPager = (FlowPager) a(R.id.tabPager);
                    kotlin.d.b.j.a((Object) flowPager, "tabPager");
                    flowPager.setCurrentItem(intValue);
                }
            }
            a0Var.n = null;
            a0Var.a((ViewPager) ((FlowPager) a(R.id.tabPager)));
            FlowPager flowPager2 = (FlowPager) a(R.id.tabPager);
            kotlin.d.b.j.a((Object) flowPager2, "tabPager");
            a0Var.b(flowPager2.getCurrentItem());
        }
        LinearLayout linearLayout = (LinearLayout) a(R.id.tabBarView);
        if (linearLayout != null) {
            kotlin.g.c b2 = kotlin.g.d.b(0, linearLayout.getChildCount());
            ArrayList arrayList3 = new ArrayList();
            Iterator it = b2.iterator();
            while (it.hasNext()) {
                View childAt = linearLayout.getChildAt(((ah) it).a());
                if (childAt != null) {
                    arrayList3.add(childAt);
                }
            }
            Context context = getContext();
            List<c1> list = r;
            FlowPager flowPager3 = (FlowPager) a(R.id.tabPager);
            kotlin.d.b.j.a((Object) flowPager3, "tabPager");
            b.b.a.b.a(context, (List<? extends View>) arrayList3, list, flowPager3.getCurrentItem(), false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.app.Fragment, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onStop() {
        b.b.a.i.g gVar;
        List<Fragment> fragments;
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager == null || (fragments = fragmentManager.getFragments()) == null) {
            gVar = null;
        } else {
            ArrayList arrayList = new ArrayList();
            for (T next : fragments) {
                Fragment fragment = (Fragment) next;
                kotlin.d.b.j.a((Object) fragment, "it");
                if (fragment.getId() == R.id.top_area) {
                    arrayList.add(next);
                }
            }
            ArrayList arrayList2 = new ArrayList();
            for (Object next2 : arrayList) {
                if (next2 instanceof b.b.a.i.a0) {
                    arrayList2.add(next2);
                }
            }
            gVar = (b.b.a.i.g) kotlin.a.m.d((List) arrayList2);
        }
        b.b.a.i.a0 a0Var = (b.b.a.i.a0) gVar;
        if (a0Var != null) {
            a0Var.a((ViewPager) null);
        }
        super.onStop();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        kotlin.d.b.j.b(view, "view");
        super.onViewCreated(view, bundle);
        Resources resources = getResources();
        kotlin.d.b.j.a((Object) resources, "resources");
        if (!b.b.a.b.b(resources)) {
            int abs = Math.abs(getResources().getDimensionPixelSize(R.dimen.bottom_area_overdraw));
            ViewGroup.MarginLayoutParams d2 = q1.d(view);
            if (d2 != null) {
                d2.topMargin = -abs;
            }
            view.setPadding(view.getPaddingLeft(), view.getPaddingTop() + abs, view.getPaddingRight(), view.getPaddingBottom());
        }
        ImageView imageView = (ImageView) a(R.id.profile_image);
        if (imageView != null) {
            q1.a(imageView, new C0061n());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.view.FlowPager, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewStateRestored(Bundle bundle) {
        super.onViewStateRestored(bundle);
        if (bundle != null && bundle.containsKey("selectedTab")) {
            int i2 = bundle.getInt("selectedTab");
            FlowPager flowPager = (FlowPager) a(R.id.tabPager);
            kotlin.d.b.j.a((Object) flowPager, "tabPager");
            flowPager.setCurrentItem(i2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void a(View view, g.b bVar, boolean z, ap<Boolean, Exception> apVar) {
        kotlin.d.b.j.b(view, "view");
        kotlin.d.b.j.b(bVar, "animation");
        kotlin.d.b.j.b(apVar, "result");
        Resources resources = getResources();
        kotlin.d.b.j.a((Object) resources, "resources");
        if (!b.b.a.b.b(resources) && bVar == g.b.SLIDE_OUT) {
            for (View view2 : kotlin.a.m.d((FrameLayout) a(R.id.profile_image_container), (ImageView) a(R.id.online_status_indicator), (TextView) a(R.id.online_status_text))) {
                view2.setAlpha(1.0f);
                view2.animate().alpha(0.0f).setStartDelay(0).setDuration(175).setInterpolator(b.b.a.f.a.f62a).start();
            }
        }
        super.a(view, bVar, z, apVar);
    }

    public final void a(i0 i0Var) {
        if (i0Var != null) {
            WeakReference weakReference = new WeakReference(this);
            if (b.b.a.b.a((Fragment) this) != null) {
                o1<List<String>> o1Var = SupercellId.INSTANCE.getSharedServices$supercellId_release().d;
                if (o1Var == null) {
                    kotlin.d.b.j.a("ingameFriendIds");
                }
                if (o1Var != null) {
                    o1Var.a(i0Var.a().f120a, new o(weakReference));
                }
            }
        }
    }
}
