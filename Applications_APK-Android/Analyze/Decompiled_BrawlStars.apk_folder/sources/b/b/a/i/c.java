package b.b.a.i;

import android.os.Bundle;
import android.support.animation.SpringAnimation;
import android.support.animation.SpringForce;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.supercell.id.R;
import com.supercell.id.view.WidthAdjustingMultilineButton;
import java.util.ArrayList;
import java.util.Arrays;
import kotlin.d.b.j;
import kotlin.h;
import kotlin.k;

public class c extends e {

    public final class a implements View.OnClickListener {
        public a() {
        }

        public final void onClick(View view) {
            c.this.b();
        }
    }

    public final class b implements View.OnClickListener {
        public b() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) c.this.a(R.id.cancelButton);
            j.a((Object) widthAdjustingMultilineButton, "cancelButton");
            widthAdjustingMultilineButton.setEnabled(false);
            c.this.b();
        }
    }

    public View a(int i) {
        throw null;
    }

    public void a() {
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_confirm_dialog, viewGroup, false);
    }

    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, kotlin.d.a.b, int):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String, ?[OBJECT, ARRAY], int]
     candidates:
      b.b.a.i.x1.j.a(android.widget.ImageView, java.lang.String, boolean, int):void
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, java.lang.Object, int):void
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, kotlin.d.a.b, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.LinearLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.animation.SpringForce, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public void onViewCreated(View view, Bundle bundle) {
        String string;
        String string2;
        String string3;
        String string4;
        ArrayList<String> stringArrayList;
        j.b(view, "view");
        view.setOnClickListener(new a());
        Bundle arguments = getArguments();
        if (arguments != null ? arguments.getBoolean("destructiveKey") : false) {
            ViewCompat.setBackground((WidthAdjustingMultilineButton) a(R.id.okButton), ResourcesCompat.getDrawable(getResources(), R.drawable.button_destructive_bg, null));
            WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) a(R.id.okButton);
            if (widthAdjustingMultilineButton != null) {
                widthAdjustingMultilineButton.setShadowLayer(1.0f, 0.0f, (float) getResources().getInteger(R.integer.primary_button_shadowDy), ResourcesCompat.getColor(getResources(), R.color.black, null));
            }
        }
        Bundle arguments2 = getArguments();
        h a2 = (arguments2 == null || (stringArrayList = arguments2.getStringArrayList("titleStringKey")) == null) ? null : k.a(stringArrayList.get(0), stringArrayList.get(1));
        Bundle arguments3 = getArguments();
        if (!(arguments3 == null || (string4 = arguments3.getString("titleKey")) == null)) {
            TextView textView = (TextView) a(R.id.dialogTitleTextView);
            j.a((Object) textView, "dialogTitleTextView");
            h[] hVarArr = a2 != null ? new h[]{a2} : new h[0];
            b.b.a.i.x1.j.a(textView, string4, (h[]) Arrays.copyOf(hVarArr, hVarArr.length));
        }
        Bundle arguments4 = getArguments();
        if (!(arguments4 == null || (string3 = arguments4.getString("textKey")) == null)) {
            TextView textView2 = (TextView) a(R.id.dialogTextTextView);
            j.a((Object) textView2, "dialogTextTextView");
            b.b.a.i.x1.j.a(textView2, string3, (kotlin.d.a.b) null, 2);
        }
        Bundle arguments5 = getArguments();
        if (!(arguments5 == null || (string2 = arguments5.getString("okButtonKey")) == null)) {
            WidthAdjustingMultilineButton widthAdjustingMultilineButton2 = (WidthAdjustingMultilineButton) a(R.id.okButton);
            j.a((Object) widthAdjustingMultilineButton2, "okButton");
            b.b.a.i.x1.j.a((TextView) widthAdjustingMultilineButton2, string2, (kotlin.d.a.b) null, 2);
        }
        Bundle arguments6 = getArguments();
        if (!(arguments6 == null || (string = arguments6.getString("cancelButtonKey")) == null)) {
            WidthAdjustingMultilineButton widthAdjustingMultilineButton3 = (WidthAdjustingMultilineButton) a(R.id.cancelButton);
            j.a((Object) widthAdjustingMultilineButton3, "cancelButton");
            b.b.a.i.x1.j.a((TextView) widthAdjustingMultilineButton3, string, (kotlin.d.a.b) null, 2);
        }
        ((WidthAdjustingMultilineButton) a(R.id.cancelButton)).setOnClickListener(new b());
        LinearLayout linearLayout = (LinearLayout) a(R.id.dialogContainer);
        j.a((Object) linearLayout, "it");
        linearLayout.setScaleX(0.8f);
        linearLayout.setScaleY(0.8f);
        SpringAnimation springAnimation = new SpringAnimation(linearLayout, SpringAnimation.SCALE_X, 1.0f);
        SpringForce spring = springAnimation.getSpring();
        j.a((Object) spring, "spring");
        spring.setDampingRatio(0.3f);
        SpringForce spring2 = springAnimation.getSpring();
        j.a((Object) spring2, "spring");
        spring2.setStiffness(400.0f);
        springAnimation.start();
        SpringAnimation springAnimation2 = new SpringAnimation(linearLayout, SpringAnimation.SCALE_Y, 1.0f);
        SpringForce spring3 = springAnimation2.getSpring();
        j.a((Object) spring3, "spring");
        spring3.setDampingRatio(0.3f);
        SpringForce spring4 = springAnimation2.getSpring();
        j.a((Object) spring4, "spring");
        spring4.setStiffness(400.0f);
        springAnimation2.start();
    }
}
