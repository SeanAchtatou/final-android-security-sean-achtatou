package b.b.a.i.m1;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.widget.TextView;
import kotlin.d.a.c;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.e.a;
import kotlin.m;

public final class b extends k implements c<Drawable, b.b.a.i.x1.c, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ c f386a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ String f387b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b(c cVar, String str) {
        super(2);
        this.f386a = cVar;
        this.f387b = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void a(Drawable drawable, b.b.a.i.x1.c cVar) {
        BitmapDrawable bitmapDrawable;
        j.b(drawable, "drawable");
        j.b(cVar, "<anonymous parameter 1>");
        TextView textView = (TextView) this.f386a.f388a.get();
        if (textView != null) {
            Resources resources = null;
            if (!(drawable instanceof BitmapDrawable)) {
                drawable = null;
            }
            BitmapDrawable bitmapDrawable2 = (BitmapDrawable) drawable;
            if (bitmapDrawable2 != null) {
                j.a((Object) textView, "this");
                Context context = textView.getContext();
                if (context != null) {
                    resources = context.getResources();
                }
                bitmapDrawable = new BitmapDrawable(resources, bitmapDrawable2.getBitmap());
            } else {
                bitmapDrawable = null;
            }
            int a2 = a.a(b.b.a.b.a(15));
            if (bitmapDrawable != null) {
                bitmapDrawable.setBounds(new Rect(0, 0, a2, a2));
            }
            TextView textView2 = (TextView) this.f386a.f388a.get();
            if (textView2 != null) {
                b.b.a.b.a(textView2, this.f386a.f389b, this.f387b, bitmapDrawable);
            }
        }
    }

    public final /* synthetic */ Object invoke(Object obj, Object obj2) {
        a((Drawable) obj, (b.b.a.i.x1.c) obj2);
        return m.f5330a;
    }
}
