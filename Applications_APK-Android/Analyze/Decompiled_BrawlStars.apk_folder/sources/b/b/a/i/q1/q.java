package b.b.a.i.q1;

import b.b.a.i.e;
import com.supercell.id.ui.MainActivity;
import java.lang.ref.WeakReference;
import kotlin.d.a.b;
import kotlin.d.b.k;
import kotlin.m;

public final class q extends k implements b<Exception, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f578a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public q(WeakReference weakReference) {
        super(1);
        this.f578a = weakReference;
    }

    public final Object invoke(Object obj) {
        Object obj2 = this.f578a.get();
        if (obj2 != null) {
            Exception exc = (Exception) obj;
            MainActivity a2 = b.b.a.b.a((o) obj2);
            if (a2 != null) {
                a2.a(exc, (b<? super e, m>) null);
            }
        }
        return m.f5330a;
    }
}
