package b.b.a.i.t1;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import b.b.a.i.b;
import b.b.a.i.d0;
import b.b.a.i.s;
import b.b.a.i.t;
import b.b.a.i.x0;
import b.b.a.i.z;
import b.b.a.j.a0;
import b.b.a.j.i0;
import b.b.a.j.j0;
import b.b.a.j.k0;
import b.b.a.j.q1;
import com.facebook.share.internal.ShareConstants;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.AvatarView;
import com.supercell.id.view.SubPageTabLayout;
import com.supercell.id.view.WidthAdjustingMultilineButton;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import kotlin.a.ab;
import kotlin.a.an;
import kotlin.m;

public final class a extends b.b.a.i.g {
    public String m;
    public b.b.a.j.d n;
    public String o;
    public b.b.a.j.d p;
    public C0065a q;
    public b r;
    public RecyclerView.LayoutManager s;
    public RecyclerView.LayoutManager t;
    public final float u = b.b.a.b.a(20);
    public final kotlin.d.a.b<i0, m> v = new f();
    public HashMap w;

    public static final class c extends b.a implements a0 {
        public static final Parcelable.Creator<c> CREATOR = new C0068a();
        public static final b h = new b(null);
        public final Set<Integer> e = an.a((Object[]) new Integer[]{Integer.valueOf(R.id.nav_area_back_button), Integer.valueOf(R.id.nav_area_close_button)});
        public final boolean f = true;
        public final Class<? extends b.b.a.i.g> g = a.class;

        /* renamed from: b.b.a.i.t1.a$c$a  reason: collision with other inner class name */
        public final class C0068a implements Parcelable.Creator<c> {
            public final c createFromParcel(Parcel parcel) {
                kotlin.d.b.j.b(parcel, ShareConstants.FEED_SOURCE_PARAM);
                kotlin.d.b.j.b(parcel, "parcel");
                return new c();
            }

            public final c[] newArray(int i) {
                return new c[i];
            }
        }

        public static final class b {
            public /* synthetic */ b(kotlin.d.b.g gVar) {
            }

            public final int a(int i, int i2, int i3) {
                float a2 = ((float) ((i - i2) - i3)) - b.b.a.b.a(320);
                float a3 = b.b.a.b.a(175);
                float a4 = b.b.a.b.a(280);
                if (Float.compare(a2, a3) < 0) {
                    a2 = a3;
                } else if (Float.compare(a2, a4) > 0) {
                    a2 = a4;
                }
                return i2 + kotlin.e.a.a(a2);
            }
        }

        public final Class<? extends b.b.a.i.g> a() {
            return this.g;
        }

        public final Class<? extends x0> a(Resources resources) {
            kotlin.d.b.j.b(resources, "resources");
            return b.b.a.b.b(resources) ? z.class : d.class;
        }

        public final int b(Resources resources, int i, int i2, int i3) {
            kotlin.d.b.j.b(resources, "resources");
            return i2 + kotlin.e.a.a(b.b.a.b.a(64));
        }

        public final int c(Resources resources, int i, int i2, int i3) {
            kotlin.d.b.j.b(resources, "resources");
            return h.a(i, i2, i3);
        }

        public final Set<Integer> c() {
            return this.e;
        }

        public final boolean c(Resources resources) {
            kotlin.d.b.j.b(resources, "resources");
            return !b.b.a.b.b(resources);
        }

        public final int describeContents() {
            return 0;
        }

        public final Class<? extends b.b.a.i.g> e(Resources resources) {
            kotlin.d.b.j.b(resources, "resources");
            return b.b.a.b.b(resources) ? e.class : d0.class;
        }

        public final boolean e() {
            return this.f;
        }

        public final void writeToParcel(Parcel parcel, int i) {
            kotlin.d.b.j.b(parcel, "dest");
        }
    }

    public static final class d extends b.b.a.i.a {
        public HashMap s;

        public final View a(int i) {
            if (this.s == null) {
                this.s = new HashMap();
            }
            View view = (View) this.s.get(Integer.valueOf(i));
            if (view != null) {
                return view;
            }
            View view2 = getView();
            if (view2 == null) {
                return null;
            }
            View findViewById = view2.findViewById(i);
            this.s.put(Integer.valueOf(i), findViewById);
            return findViewById;
        }

        public final void a() {
            HashMap hashMap = this.s;
            if (hashMap != null) {
                hashMap.clear();
            }
        }

        public final void a(View view) {
            kotlin.d.b.j.b(view, "view");
            a i = i();
            if (i != null) {
                b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Profile Picture Editor", "click", "Back", Long.valueOf(i.i() ? 1 : 0), false, 16);
                if (i.i()) {
                    i.j();
                    return;
                }
                MainActivity a2 = b.b.a.b.a((Fragment) i);
                if (a2 != null) {
                    a2.d();
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.support.v4.app.Fragment, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final a i() {
            b.b.a.i.g gVar;
            List<Fragment> fragments;
            FragmentManager fragmentManager = getFragmentManager();
            if (fragmentManager == null || (fragments = fragmentManager.getFragments()) == null) {
                gVar = null;
            } else {
                ArrayList arrayList = new ArrayList();
                for (T next : fragments) {
                    Fragment fragment = (Fragment) next;
                    kotlin.d.b.j.a((Object) fragment, "it");
                    if (fragment.getId() == R.id.bottom_area) {
                        arrayList.add(next);
                    }
                }
                ArrayList arrayList2 = new ArrayList();
                for (Object next2 : arrayList) {
                    if (next2 instanceof a) {
                        arrayList2.add(next2);
                    }
                }
                gVar = (b.b.a.i.g) kotlin.a.m.d((List) arrayList2);
            }
            return (a) gVar;
        }

        public final /* synthetic */ void onDestroyView() {
            super.onDestroyView();
            a();
        }
    }

    public static final class e extends b.b.a.i.g {
        public HashMap m;

        public final View a(int i) {
            if (this.m == null) {
                this.m = new HashMap();
            }
            View view = (View) this.m.get(Integer.valueOf(i));
            if (view != null) {
                return view;
            }
            View view2 = getView();
            if (view2 == null) {
                return null;
            }
            View findViewById = view2.findViewById(i);
            this.m.put(Integer.valueOf(i), findViewById);
            return findViewById;
        }

        public final void a() {
            HashMap hashMap = this.m;
            if (hashMap != null) {
                hashMap.clear();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
            kotlin.d.b.j.b(layoutInflater, "inflater");
            return layoutInflater.inflate(R.layout.fragment_profile_pic_editor_top_area, viewGroup, false);
        }

        public final /* synthetic */ void onDestroyView() {
            super.onDestroyView();
            a();
        }
    }

    public final class f extends kotlin.d.b.k implements kotlin.d.a.b<i0, m> {
        public f() {
            super(1);
        }

        public final Object invoke(Object obj) {
            String str;
            b.b.a.h.h a2;
            i0 i0Var = (i0) obj;
            if (a.this.isAdded()) {
                a aVar = a.this;
                if (i0Var == null || (a2 = i0Var.a()) == null || (str = a2.d) == null) {
                    str = "";
                }
                aVar.b(str);
            }
            return m.f5330a;
        }
    }

    public final class g implements View.OnClickListener {
        public g() {
        }

        public final void onClick(View view) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Profile Picture Editor", "click", "Cancel", null, false, 24);
            MainActivity a2 = b.b.a.b.a((Fragment) a.this);
            if (a2 != null) {
                a2.d();
            }
        }
    }

    public final class h implements View.OnClickListener {
        public h() {
        }

        public final void onClick(View view) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Profile Picture Editor", "click", "Save", Long.valueOf(a.this.i() ? 1 : 0), false, 16);
            if (a.this.i()) {
                a.this.k();
            }
            MainActivity a2 = b.b.a.b.a((Fragment) a.this);
            if (a2 != null) {
                a2.d();
            }
        }
    }

    public final class i extends kotlin.d.b.k implements kotlin.d.a.c<b.b.a.j.d, AvatarView.a, m> {
        public i() {
            super(2);
        }

        public final Object invoke(Object obj, Object obj2) {
            b.b.a.j.d dVar = (b.b.a.j.d) obj;
            AvatarView.a aVar = (AvatarView.a) obj2;
            kotlin.d.b.j.b(dVar, "bg");
            kotlin.d.b.j.b(aVar, "animation");
            a aVar2 = a.this;
            aVar2.a(aVar2.m, dVar, AvatarView.a.NONE, aVar);
            a.this.a(dVar);
            return m.f5330a;
        }
    }

    public final class j extends kotlin.d.b.k implements kotlin.d.a.c<String, AvatarView.a, m> {
        public j() {
            super(2);
        }

        public final Object invoke(Object obj, Object obj2) {
            String str = (String) obj;
            AvatarView.a aVar = (AvatarView.a) obj2;
            kotlin.d.b.j.b(str, "avatarName");
            kotlin.d.b.j.b(aVar, "animation");
            a aVar2 = a.this;
            aVar2.a(str, aVar2.n, aVar, AvatarView.a.NONE);
            a.this.a(str);
            return m.f5330a;
        }
    }

    public final class k extends kotlin.d.b.k implements kotlin.d.a.c<t, s, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ t f718a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ a f719b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(t tVar, a aVar) {
            super(2);
            this.f718a = tVar;
            this.f719b = aVar;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:5:0x003d, code lost:
            if (r8 != null) goto L_0x0048;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:7:0x0046, code lost:
            if (r8 != null) goto L_0x0048;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invoke(java.lang.Object r8, java.lang.Object r9) {
            /*
                r7 = this;
                b.b.a.i.t r8 = (b.b.a.i.t) r8
                b.b.a.i.s r9 = (b.b.a.i.s) r9
                java.lang.String r0 = "<anonymous parameter 0>"
                kotlin.d.b.j.b(r8, r0)
                java.lang.String r8 = "decision"
                kotlin.d.b.j.b(r9, r8)
                int[] r8 = b.b.a.i.t1.d.f725a
                int r9 = r9.ordinal()
                r8 = r8[r9]
                r9 = 1
                if (r8 == r9) goto L_0x0040
                r9 = 2
                if (r8 == r9) goto L_0x001d
                goto L_0x004b
            L_0x001d:
                com.supercell.id.SupercellId r8 = com.supercell.id.SupercellId.INSTANCE
                b.b.a.j.x r8 = r8.getSharedServices$supercellId_release()
                b.b.a.c.b r0 = r8.f
                r4 = 0
                r5 = 0
                r6 = 24
                java.lang.String r1 = "Profile Picture Editor - Save Changes Dialog"
                java.lang.String r2 = "click"
                java.lang.String r3 = "Save"
                b.b.a.c.b.a(r0, r1, r2, r3, r4, r5, r6)
                b.b.a.i.t1.a r8 = r7.f719b
                r8.k()
                b.b.a.i.t r8 = r7.f718a
                com.supercell.id.ui.MainActivity r8 = b.b.a.b.a(r8)
                if (r8 == 0) goto L_0x004b
                goto L_0x0048
            L_0x0040:
                b.b.a.i.t r8 = r7.f718a
                com.supercell.id.ui.MainActivity r8 = b.b.a.b.a(r8)
                if (r8 == 0) goto L_0x004b
            L_0x0048:
                r8.d()
            L_0x004b:
                kotlin.m r8 = kotlin.m.f5330a
                return r8
            */
            throw new UnsupportedOperationException("Method not decompiled: b.b.a.i.t1.a.k.invoke(java.lang.Object, java.lang.Object):java.lang.Object");
        }
    }

    public final class l extends kotlin.d.b.k implements kotlin.d.a.b<Exception, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f720a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(WeakReference weakReference) {
            super(1);
            this.f720a = weakReference;
        }

        public final Object invoke(Object obj) {
            Object obj2 = this.f720a.get();
            if (obj2 != null) {
                ((MainActivity) obj2).a((Exception) obj, (kotlin.d.a.b<? super b.b.a.i.e, m>) null);
            }
            return m.f5330a;
        }
    }

    public final View a(int i2) {
        if (this.w == null) {
            this.w = new HashMap();
        }
        View view = (View) this.w.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i2);
        this.w.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.w;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public final void a(View view) {
        kotlin.d.b.j.b(view, "view");
        b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Profile Picture Editor", "click", "Back", Long.valueOf(i() ? 1 : 0), false, 16);
        if (i()) {
            j();
            return;
        }
        MainActivity a2 = b.b.a.b.a((Fragment) this);
        if (a2 != null) {
            a2.d();
        }
    }

    public final View b() {
        return (ImageButton) a(R.id.back_button);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v7.widget.RecyclerView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void b(String str) {
        b.b.a.j.e a2 = b.b.a.j.e.e.a(str);
        String str2 = a2.f1027a;
        b.b.a.j.d dVar = a2.f1028b;
        AvatarView.a aVar = AvatarView.a.NONE;
        a(str2, dVar, aVar, aVar);
        String str3 = a2.f1027a;
        List<String> b2 = k0.f1078b.b();
        int indexOf = b2.indexOf(str3);
        if (indexOf == -1) {
            indexOf = 0;
        }
        this.o = b2.get(indexOf);
        b bVar = this.r;
        if (bVar == null) {
            kotlin.d.b.j.a("avatarImageAdapter");
        }
        bVar.f711b = indexOf;
        b bVar2 = this.r;
        if (bVar2 == null) {
            kotlin.d.b.j.a("avatarImageAdapter");
        }
        bVar2.notifyItemChanged(indexOf);
        RecyclerView recyclerView = (RecyclerView) a(R.id.avatar_images);
        kotlin.d.b.j.a((Object) recyclerView, "avatar_images");
        b.b.a.b.b(recyclerView, indexOf);
        b.b.a.j.d dVar2 = a2.f1028b;
        a(dVar2.f1023a, dVar2.f1024b);
        a(a2.f1027a);
        a(a2.f1028b);
    }

    public final float g() {
        return this.u;
    }

    public final boolean i() {
        return (kotlin.d.b.j.a(this.n, this.p) ^ true) || (kotlin.d.b.j.a(this.m, this.o) ^ true);
    }

    public final void j() {
        MainActivity a2 = b.b.a.b.a((Fragment) this);
        if (a2 != null) {
            t a3 = t.g.a("account_profile_wizard_dialog_save_heading", "account_profile_wizard_dialog_save_btn_ok", "account_profile_wizard_dialog_save_btn_discard", null);
            a3.e = new k(a3, this);
            a2.a(a3, "popupDialog");
        }
    }

    public final void k() {
        String a2;
        b.b.a.j.e a3 = b.b.a.j.e.e.a(this.m, this.n);
        if (a3 != null && (a2 = a3.a()) != null) {
            SupercellId.INSTANCE.getSharedServices$supercellId_release().f().c(this.v);
            nl.komponents.kovenant.c.m.b(j0.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f(), null, a2, null, 5), new l(new WeakReference(b.b.a.b.a((Fragment) this))));
        }
    }

    public final void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        this.o = null;
        this.p = null;
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f().b(this.v);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kotlin.d.b.j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_profile_pic_editor, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    public final void onResume() {
        super.onResume();
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a("Profile Picture Editor");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.Context, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v7.widget.RecyclerView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        kotlin.d.b.j.b(view, "view");
        super.onViewCreated(view, bundle);
        b.b.a.b.a((SubPageTabLayout) a(R.id.tabBarView), "account_profile_wizard_edit_profile_pic", new kotlin.h[0]);
        ((WidthAdjustingMultilineButton) a(R.id.cancelButton)).setOnClickListener(new g());
        ((WidthAdjustingMultilineButton) a(R.id.saveButton)).setOnClickListener(new h());
        Context context = view.getContext();
        kotlin.d.b.j.a((Object) context, "view.context");
        this.q = new C0065a(context, b.b.a.b.a((Fragment) this), new i());
        C0065a aVar = this.q;
        if (aVar == null) {
            kotlin.d.b.j.a("avatarBackgroundAdapter");
        }
        aVar.a(k0.f1078b.a());
        this.s = new LinearLayoutManager(getContext(), 0, false);
        RecyclerView recyclerView = (RecyclerView) a(R.id.avatar_backgrounds);
        kotlin.d.b.j.a((Object) recyclerView, "avatar_backgrounds");
        RecyclerView.LayoutManager layoutManager = this.s;
        if (layoutManager == null) {
            kotlin.d.b.j.a("avatarBackgroundsLayoutManager");
        }
        recyclerView.setLayoutManager(layoutManager);
        RecyclerView recyclerView2 = (RecyclerView) a(R.id.avatar_backgrounds);
        kotlin.d.b.j.a((Object) recyclerView2, "avatar_backgrounds");
        C0065a aVar2 = this.q;
        if (aVar2 == null) {
            kotlin.d.b.j.a("avatarBackgroundAdapter");
        }
        recyclerView2.setAdapter(aVar2);
        Context context2 = view.getContext();
        kotlin.d.b.j.a((Object) context2, "view.context");
        this.r = new b(context2, b.b.a.b.a((Fragment) this), new j());
        b bVar = this.r;
        if (bVar == null) {
            kotlin.d.b.j.a("avatarImageAdapter");
        }
        bVar.a(k0.f1078b.b());
        this.t = new LinearLayoutManager(getContext(), 0, false);
        RecyclerView recyclerView3 = (RecyclerView) a(R.id.avatar_images);
        kotlin.d.b.j.a((Object) recyclerView3, "avatar_images");
        RecyclerView.LayoutManager layoutManager2 = this.t;
        if (layoutManager2 == null) {
            kotlin.d.b.j.a("avatarImagesLayoutManager");
        }
        recyclerView3.setLayoutManager(layoutManager2);
        RecyclerView recyclerView4 = (RecyclerView) a(R.id.avatar_images);
        kotlin.d.b.j.a((Object) recyclerView4, "avatar_images");
        b bVar2 = this.r;
        if (bVar2 == null) {
            kotlin.d.b.j.a("avatarImageAdapter");
        }
        recyclerView4.setAdapter(bVar2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.a(java.util.List, int):T
     arg types: [java.util.List<b.b.a.j.d>, int]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.w.a(java.util.List, int):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v7.widget.RecyclerView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void a(int i2, int i3) {
        List<b.b.a.j.d> a2 = k0.f1078b.a();
        int indexOf = a2.indexOf(new b.b.a.j.d(i2, i3));
        if (indexOf == -1) {
            indexOf = 0;
        }
        this.p = (b.b.a.j.d) kotlin.a.m.a((List) a2, indexOf);
        C0065a aVar = this.q;
        if (aVar == null) {
            kotlin.d.b.j.a("avatarBackgroundAdapter");
        }
        aVar.f708b = indexOf;
        C0065a aVar2 = this.q;
        if (aVar2 == null) {
            kotlin.d.b.j.a("avatarBackgroundAdapter");
        }
        aVar2.notifyItemChanged(indexOf);
        RecyclerView recyclerView = (RecyclerView) a(R.id.avatar_backgrounds);
        kotlin.d.b.j.a((Object) recyclerView, "avatar_backgrounds");
        b.b.a.b.b(recyclerView, indexOf);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.j.q1.a(android.support.v7.widget.AppCompatButton, boolean):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, boolean]
     candidates:
      b.b.a.j.q1.a(android.view.View, int):android.view.View
      b.b.a.j.q1.a(android.support.v4.widget.NestedScrollView, int):void
      b.b.a.j.q1.a(android.support.v4.widget.NestedScrollView, android.view.View):void
      b.b.a.j.q1.a(android.view.View, long):void
      b.b.a.j.q1.a(android.view.View, b.b.a.j.z):void
      b.b.a.j.q1.a(android.view.View, kotlin.d.a.a<kotlin.m>):void
      b.b.a.j.q1.a(android.widget.ScrollView, int):void
      b.b.a.j.q1.a(android.widget.ScrollView, android.view.View):void
      b.b.a.j.q1.a(android.support.v7.widget.AppCompatButton, boolean):void */
    public final void a(b.b.a.j.d dVar) {
        this.n = dVar;
        WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) a(R.id.saveButton);
        if (widthAdjustingMultilineButton != null) {
            q1.a((AppCompatButton) widthAdjustingMultilineButton, !i());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.j.q1.a(android.support.v7.widget.AppCompatButton, boolean):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, boolean]
     candidates:
      b.b.a.j.q1.a(android.view.View, int):android.view.View
      b.b.a.j.q1.a(android.support.v4.widget.NestedScrollView, int):void
      b.b.a.j.q1.a(android.support.v4.widget.NestedScrollView, android.view.View):void
      b.b.a.j.q1.a(android.view.View, long):void
      b.b.a.j.q1.a(android.view.View, b.b.a.j.z):void
      b.b.a.j.q1.a(android.view.View, kotlin.d.a.a<kotlin.m>):void
      b.b.a.j.q1.a(android.widget.ScrollView, int):void
      b.b.a.j.q1.a(android.widget.ScrollView, android.view.View):void
      b.b.a.j.q1.a(android.support.v7.widget.AppCompatButton, boolean):void */
    public final void a(String str) {
        this.m = str;
        WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) a(R.id.saveButton);
        if (widthAdjustingMultilineButton != null) {
            q1.a((AppCompatButton) widthAdjustingMultilineButton, !i());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.app.Fragment, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void a(String str, b.b.a.j.d dVar, AvatarView.a aVar, AvatarView.a aVar2) {
        b.b.a.i.g gVar;
        List<Fragment> fragments;
        if (str != null && dVar != null) {
            b.b.a.j.e eVar = new b.b.a.j.e(str, dVar);
            AvatarView avatarView = (AvatarView) a(R.id.profile_image);
            if (avatarView != null) {
                avatarView.setBackgroundGradient(dVar.f1023a, dVar.f1024b, aVar2);
            }
            AvatarView avatarView2 = (AvatarView) a(R.id.profile_image);
            if (avatarView2 != null) {
                avatarView2.setAvatar(str, aVar);
            }
            FragmentManager fragmentManager = getFragmentManager();
            if (fragmentManager == null || (fragments = fragmentManager.getFragments()) == null) {
                gVar = null;
            } else {
                ArrayList arrayList = new ArrayList();
                for (T next : fragments) {
                    Fragment fragment = (Fragment) next;
                    kotlin.d.b.j.a((Object) fragment, "it");
                    if (fragment.getId() == R.id.top_area) {
                        arrayList.add(next);
                    }
                }
                ArrayList arrayList2 = new ArrayList();
                for (Object next2 : arrayList) {
                    if (next2 instanceof e) {
                        arrayList2.add(next2);
                    }
                }
                gVar = (b.b.a.i.g) kotlin.a.m.d((List) arrayList2);
            }
            e eVar2 = (e) gVar;
            if (eVar2 != null) {
                kotlin.d.b.j.b(aVar, "imageAnimation");
                kotlin.d.b.j.b(aVar2, "bgAnimation");
                AvatarView avatarView3 = (AvatarView) eVar2.a(R.id.top_area_profile_image);
                if (avatarView3 != null) {
                    b.b.a.j.d dVar2 = eVar.f1028b;
                    avatarView3.setBackgroundGradient(dVar2.f1023a, dVar2.f1024b, aVar2);
                }
                AvatarView avatarView4 = (AvatarView) eVar2.a(R.id.top_area_profile_image);
                if (avatarView4 != null) {
                    avatarView4.setAvatar(eVar.f1027a, aVar);
                }
            }
        }
    }

    /* renamed from: b.b.a.i.t1.a$a  reason: collision with other inner class name */
    public static final class C0065a extends RecyclerView.Adapter<C0066a> {

        /* renamed from: a  reason: collision with root package name */
        public List<b.b.a.j.d> f707a = ab.f5210a;

        /* renamed from: b  reason: collision with root package name */
        public int f708b = -1;
        public final Context c;
        public final kotlin.d.a.c<b.b.a.j.d, AvatarView.a, m> d;

        /* renamed from: b.b.a.i.t1.a$a$a  reason: collision with other inner class name */
        public static final class C0066a extends RecyclerView.ViewHolder {

            /* renamed from: a  reason: collision with root package name */
            public final View f709a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0066a(View view) {
                super(view);
                kotlin.d.b.j.b(view, "containerView");
                this.f709a = view;
            }

            public final void a(b.b.a.j.d dVar) {
            }

            public final View getContainerView() {
                return this.f709a;
            }
        }

        public final void a(List<b.b.a.j.d> list) {
            kotlin.d.b.j.b(list, "<set-?>");
            this.f707a = list;
        }

        public final int getItemCount() {
            return this.f707a.size();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.ImageView, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
            C0066a aVar = (C0066a) viewHolder;
            kotlin.d.b.j.b(aVar, "holder");
            b.b.a.j.d dVar = this.f707a.get(i);
            aVar.a(dVar);
            int i2 = 0;
            GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TL_BR, new int[]{dVar.f1023a, dVar.f1024b});
            gradientDrawable.setShape(1);
            ViewCompat.setBackground((ImageView) aVar.f709a.findViewById(R.id.background_image), gradientDrawable);
            if (this.f708b == i) {
                View view = aVar.itemView;
                kotlin.d.b.j.a((Object) view, "itemView");
                ImageView imageView = (ImageView) view.findViewById(R.id.background_selected);
                kotlin.d.b.j.a((Object) imageView, "itemView.background_selected");
                imageView.setVisibility(0);
                View view2 = aVar.itemView;
                kotlin.d.b.j.a((Object) view2, "itemView");
                ((ImageView) view2.findViewById(R.id.background_selected)).startAnimation(AnimationUtils.loadAnimation(this.c, R.anim.bounce));
            } else {
                View view3 = aVar.itemView;
                kotlin.d.b.j.a((Object) view3, "itemView");
                ((ImageView) view3.findViewById(R.id.background_selected)).clearAnimation();
                View view4 = aVar.itemView;
                kotlin.d.b.j.a((Object) view4, "itemView");
                ImageView imageView2 = (ImageView) view4.findViewById(R.id.background_selected);
                kotlin.d.b.j.a((Object) imageView2, "itemView.background_selected");
                i2 = 4;
                imageView2.setVisibility(4);
            }
            View view5 = aVar.itemView;
            kotlin.d.b.j.a((Object) view5, "itemView");
            ImageView imageView3 = (ImageView) view5.findViewById(R.id.background_outer_circle);
            kotlin.d.b.j.a((Object) imageView3, "itemView.background_outer_circle");
            imageView3.setVisibility(i2);
            aVar.itemView.setOnClickListener(new b(aVar, dVar, this, i));
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            kotlin.d.b.j.b(viewGroup, "parent");
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_profile_pic_editor_avatar_background_item, viewGroup, false);
            kotlin.d.b.j.a((Object) inflate, "LayoutInflater.from(pare…ound_item, parent, false)");
            return new C0066a(inflate);
        }

        /* JADX WARN: Type inference failed for: r3v0, types: [kotlin.d.a.c<b.b.a.j.d, com.supercell.id.view.AvatarView$a, kotlin.m>, java.lang.Object, kotlin.d.a.c<? super b.b.a.j.d, ? super com.supercell.id.view.AvatarView$a, kotlin.m>] */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public C0065a(android.content.Context r1, com.supercell.id.ui.MainActivity r2, kotlin.d.a.c<? super b.b.a.j.d, ? super com.supercell.id.view.AvatarView.a, kotlin.m> r3) {
            /*
                r0 = this;
                java.lang.String r2 = "context"
                kotlin.d.b.j.b(r1, r2)
                java.lang.String r2 = "listener"
                kotlin.d.b.j.b(r3, r2)
                r0.<init>()
                r0.c = r1
                r0.d = r3
                kotlin.a.ab r1 = kotlin.a.ab.f5210a
                java.util.List r1 = (java.util.List) r1
                r0.f707a = r1
                r1 = -1
                r0.f708b = r1
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: b.b.a.i.t1.a.C0065a.<init>(android.content.Context, com.supercell.id.ui.MainActivity, kotlin.d.a.c):void");
        }
    }

    public static final class b extends RecyclerView.Adapter<C0067a> {

        /* renamed from: a  reason: collision with root package name */
        public List<String> f710a = ab.f5210a;

        /* renamed from: b  reason: collision with root package name */
        public int f711b = -1;
        public final Context c;
        public final kotlin.d.a.c<String, AvatarView.a, m> d;

        /* renamed from: b.b.a.i.t1.a$b$a  reason: collision with other inner class name */
        public static final class C0067a extends RecyclerView.ViewHolder {

            /* renamed from: a  reason: collision with root package name */
            public final View f712a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0067a(View view) {
                super(view);
                kotlin.d.b.j.b(view, "containerView");
                this.f712a = view;
            }

            public final void a(String str) {
            }

            public final View getContainerView() {
                return this.f712a;
            }
        }

        public final void a(List<String> list) {
            kotlin.d.b.j.b(list, "<set-?>");
            this.f710a = list;
        }

        public final int getItemCount() {
            return this.f710a.size();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.ImageView, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.content.res.Resources, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
            C0067a aVar = (C0067a) viewHolder;
            kotlin.d.b.j.b(aVar, "holder");
            String str = this.f710a.get(i);
            aVar.a(str);
            if (this.f711b == i) {
                View view = aVar.itemView;
                kotlin.d.b.j.a((Object) view, "itemView");
                ImageView imageView = (ImageView) view.findViewById(R.id.checkmark);
                kotlin.d.b.j.a((Object) imageView, "itemView.checkmark");
                imageView.setVisibility(0);
                View view2 = aVar.itemView;
                kotlin.d.b.j.a((Object) view2, "itemView");
                ((ImageView) view2.findViewById(R.id.checkmark)).startAnimation(AnimationUtils.loadAnimation(this.c, R.anim.bounce));
                View view3 = aVar.itemView;
                kotlin.d.b.j.a((Object) view3, "itemView");
                ImageView imageView2 = (ImageView) view3.findViewById(R.id.image_outer_circle);
                kotlin.d.b.j.a((Object) imageView2, "itemView.image_outer_circle");
                imageView2.setVisibility(0);
            } else {
                View view4 = aVar.itemView;
                kotlin.d.b.j.a((Object) view4, "itemView");
                ((ImageView) view4.findViewById(R.id.checkmark)).clearAnimation();
                View view5 = aVar.itemView;
                kotlin.d.b.j.a((Object) view5, "itemView");
                ImageView imageView3 = (ImageView) view5.findViewById(R.id.checkmark);
                kotlin.d.b.j.a((Object) imageView3, "itemView.checkmark");
                imageView3.setVisibility(4);
                View view6 = aVar.itemView;
                kotlin.d.b.j.a((Object) view6, "itemView");
                ImageView imageView4 = (ImageView) view6.findViewById(R.id.image_outer_circle);
                kotlin.d.b.j.a((Object) imageView4, "itemView.image_outer_circle");
                imageView4.setVisibility(4);
            }
            aVar.itemView.setOnClickListener(new c(aVar, str, this, i));
            k0 k0Var = k0.f1078b;
            b.b.a.j.e eVar = new b.b.a.j.e(str, new b.b.a.j.d(0, 0));
            View view7 = aVar.itemView;
            kotlin.d.b.j.a((Object) view7, "itemView");
            Resources resources = this.c.getResources();
            kotlin.d.b.j.a((Object) resources, "context.resources");
            k0Var.a(eVar, (ImageView) view7.findViewById(R.id.profile_image_list_item), resources);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            kotlin.d.b.j.b(viewGroup, "parent");
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_profile_pic_editor_avatar_image_item, viewGroup, false);
            kotlin.d.b.j.a((Object) inflate, "LayoutInflater.from(pare…mage_item, parent, false)");
            return new C0067a(inflate);
        }

        /* JADX WARN: Type inference failed for: r3v0, types: [java.lang.Object, kotlin.d.a.c<java.lang.String, com.supercell.id.view.AvatarView$a, kotlin.m>, kotlin.d.a.c<? super java.lang.String, ? super com.supercell.id.view.AvatarView$a, kotlin.m>] */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public b(android.content.Context r1, com.supercell.id.ui.MainActivity r2, kotlin.d.a.c<? super java.lang.String, ? super com.supercell.id.view.AvatarView.a, kotlin.m> r3) {
            /*
                r0 = this;
                java.lang.String r2 = "context"
                kotlin.d.b.j.b(r1, r2)
                java.lang.String r2 = "listener"
                kotlin.d.b.j.b(r3, r2)
                r0.<init>()
                r0.c = r1
                r0.d = r3
                kotlin.a.ab r1 = kotlin.a.ab.f5210a
                java.util.List r1 = (java.util.List) r1
                r0.f710a = r1
                r1 = -1
                r0.f711b = r1
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: b.b.a.i.t1.a.b.<init>(android.content.Context, com.supercell.id.ui.MainActivity, kotlin.d.a.c):void");
        }
    }
}
