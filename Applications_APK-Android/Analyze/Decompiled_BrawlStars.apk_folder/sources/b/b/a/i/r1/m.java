package b.b.a.i.r1;

import b.b.a.j.r0;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.supercell.id.R;
import kotlin.d.b.j;

public final class m implements r0 {

    /* renamed from: a  reason: collision with root package name */
    public static final int f629a = R.layout.fragment_profile_list_item_friend_pending_header;

    /* renamed from: b  reason: collision with root package name */
    public static final m f630b = new m();

    public final int a() {
        return f629a;
    }

    public final boolean a(r0 r0Var) {
        j.b(r0Var, FacebookRequestErrorClassification.KEY_OTHER);
        return j.a(r0Var, this);
    }

    public final boolean b(r0 r0Var) {
        j.b(r0Var, FacebookRequestErrorClassification.KEY_OTHER);
        return true;
    }
}
