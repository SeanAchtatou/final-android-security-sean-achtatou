package b.b.a.i.u1;

import android.support.v4.app.Fragment;
import android.view.View;
import b.b.a.c.b;
import com.supercell.id.IdAccount;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import kotlin.d.b.j;

public final class d implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ IdAccount f733a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ b f734b;

    public d(IdAccount idAccount, b bVar) {
        this.f733a = idAccount;
        this.f734b = bVar;
    }

    public final void onClick(View view) {
        b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Saved Credentials", "click", "Forget saved credentials", null, false, 24);
        MainActivity a2 = b.b.a.b.a((Fragment) this.f734b);
        if (a2 != null) {
            IdAccount idAccount = this.f733a;
            j.b(a2, "$this$showForgetAccountDialogPopup");
            j.b(idAccount, "account");
            a2.a(a.f.a("switch_forget_confirm_heading", "switch_forget_confirm_description", "switch_forget_confirm_btn_confirm", "switch_forget_confirm_btn_cancel", idAccount), "popupDialog");
        }
    }
}
