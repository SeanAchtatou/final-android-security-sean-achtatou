package b.b.a.i;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import b.b.a.g.i;
import b.b.a.i.b;
import b.b.a.j.q1;
import com.supercell.id.R;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.RootFrameLayout;
import kotlin.TypeCastException;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.d.b.s;
import kotlin.m;

public final class g0 extends k implements kotlin.d.a.a<m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ MainActivity f259a;

    public final class a implements ValueAnimator.AnimatorUpdateListener {
        public a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.FrameLayout, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.animation.ValueAnimator, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            FrameLayout frameLayout = (FrameLayout) g0.this.f259a.a(R.id.content);
            j.a((Object) frameLayout, "content");
            int width = frameLayout.getWidth();
            if (width > 0) {
                FrameLayout frameLayout2 = (FrameLayout) g0.this.f259a.a(R.id.content);
                j.a((Object) frameLayout2, "content");
                frameLayout2.setVisibility(0);
            }
            FrameLayout frameLayout3 = (FrameLayout) g0.this.f259a.a(R.id.content);
            j.a((Object) frameLayout3, "content");
            float f = (float) width;
            j.a((Object) valueAnimator, "it");
            Object animatedValue = valueAnimator.getAnimatedValue();
            if (animatedValue != null) {
                frameLayout3.setTranslationX(((Float) animatedValue).floatValue() * f);
                ((RootFrameLayout) g0.this.f259a.a(R.id.root_layout)).getPropagateSystemWindowInsets().run();
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Float");
        }
    }

    public final class b implements ValueAnimator.AnimatorUpdateListener {
        public b() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.FrameLayout, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.animation.ValueAnimator, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            FrameLayout frameLayout = (FrameLayout) g0.this.f259a.a(R.id.content);
            j.a((Object) frameLayout, "content");
            int height = frameLayout.getHeight();
            if (height > 0) {
                FrameLayout frameLayout2 = (FrameLayout) g0.this.f259a.a(R.id.content);
                j.a((Object) frameLayout2, "content");
                frameLayout2.setVisibility(0);
            }
            FrameLayout frameLayout3 = (FrameLayout) g0.this.f259a.a(R.id.content);
            j.a((Object) frameLayout3, "content");
            float f = (float) height;
            j.a((Object) valueAnimator, "it");
            Object animatedValue = valueAnimator.getAnimatedValue();
            if (animatedValue != null) {
                frameLayout3.setTranslationY(((Float) animatedValue).floatValue() * f);
                ((RootFrameLayout) g0.this.f259a.a(R.id.root_layout)).getPropagateSystemWindowInsets().run();
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Float");
        }
    }

    public final class c implements ValueAnimator.AnimatorUpdateListener {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ ValueAnimator f262a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ g0 f263b;

        public c(ValueAnimator valueAnimator, g0 g0Var) {
            this.f262a = valueAnimator;
            this.f263b = g0Var;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.FrameLayout, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            Integer d = this.f263b.f259a.n();
            if (d != null) {
                int intValue = d.intValue();
                FrameLayout frameLayout = (FrameLayout) this.f263b.f259a.a(R.id.content);
                j.a((Object) frameLayout, "content");
                int height = frameLayout.getHeight();
                Object animatedValue = this.f262a.getAnimatedValue();
                if (animatedValue != null) {
                    int a2 = kotlin.e.a.a((((float) (intValue - height)) * ((Float) animatedValue).floatValue()) + ((float) height));
                    FrameLayout frameLayout2 = (FrameLayout) this.f263b.f259a.a(R.id.top_area);
                    j.a((Object) frameLayout2, "top_area");
                    frameLayout2.getLayoutParams().height = a2;
                    ((FrameLayout) this.f263b.f259a.a(R.id.top_area)).requestLayout();
                    FrameLayout frameLayout3 = (FrameLayout) this.f263b.f259a.a(R.id.bottom_area);
                    j.a((Object) frameLayout3, "bottom_area");
                    ViewGroup.MarginLayoutParams d2 = q1.d(frameLayout3);
                    if (d2 != null) {
                        d2.bottomMargin = intValue - a2;
                    }
                    ((FrameLayout) this.f263b.f259a.a(R.id.bottom_area)).requestLayout();
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Float");
            }
        }
    }

    public final class d implements ValueAnimator.AnimatorUpdateListener {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ s.b f265b;

        public d(s.b bVar) {
            this.f265b = bVar;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.FrameLayout, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            FrameLayout frameLayout = (FrameLayout) g0.this.f259a.a(R.id.content);
            j.a((Object) frameLayout, "content");
            int width = frameLayout.getWidth();
            if (this.f265b.f5252a != width) {
                MainActivity.c(g0.this.f259a);
            }
            this.f265b.f5252a = width;
        }
    }

    public final class e implements ValueAnimator.AnimatorUpdateListener {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ s.b f267b;

        public e(s.b bVar) {
            this.f267b = bVar;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.FrameLayout, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            FrameLayout frameLayout = (FrameLayout) g0.this.f259a.a(R.id.content);
            j.a((Object) frameLayout, "content");
            int height = frameLayout.getHeight();
            if (this.f267b.f5252a != height) {
                MainActivity.c(g0.this.f259a);
            }
            this.f267b.f5252a = height;
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public g0(MainActivity mainActivity) {
        super(0);
        this.f259a = mainActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.FrameLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.RootFrameLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void invoke() {
        ValueAnimator valueAnimator;
        View a2 = this.f259a.a(R.id.dimmer);
        j.a((Object) a2, "dimmer");
        float f = 0.0f;
        a2.setAlpha(0.0f);
        View a3 = this.f259a.a(R.id.dimmer);
        j.a((Object) a3, "dimmer");
        a3.setVisibility(0);
        Resources resources = this.f259a.getResources();
        j.a((Object) resources, "resources");
        if (!b.b.a.b.a(resources)) {
            Resources resources2 = this.f259a.getResources();
            j.a((Object) resources2, "resources");
            if (!b.b.a.b.c(resources2)) {
                i g = this.f259a.g();
                if (this.f259a.j() != 0) {
                    f = b.b.a.b.a(12);
                }
                g.a(f);
                FrameLayout frameLayout = (FrameLayout) this.f259a.a(R.id.content);
                j.a((Object) frameLayout, "content");
                ViewGroup.MarginLayoutParams d2 = q1.d(frameLayout);
                if (d2 != null) {
                    d2.topMargin = this.f259a.j();
                }
                ((FrameLayout) this.f259a.a(R.id.content)).requestLayout();
                ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.f259a.a(R.id.dimmer), "alpha", 0.0f, 1.0f);
                ofFloat.setDuration(300L);
                ValueAnimator ofFloat2 = ValueAnimator.ofFloat(1.0f, 0.0f);
                ofFloat2.setDuration(500L);
                ofFloat2.setInterpolator(b.b.a.f.a.f63b);
                ofFloat2.addUpdateListener(new b());
                b.a a4 = MainActivity.b(this.f259a).a();
                if (a4 == null || !a4.d()) {
                    MainActivity.c(this.f259a);
                    s.b bVar = new s.b();
                    FrameLayout frameLayout2 = (FrameLayout) this.f259a.a(R.id.content);
                    j.a((Object) frameLayout2, "content");
                    bVar.f5252a = frameLayout2.getHeight();
                    ofFloat2.addUpdateListener(new e(bVar));
                    valueAnimator = null;
                } else {
                    FrameLayout frameLayout3 = (FrameLayout) this.f259a.a(R.id.top_area);
                    j.a((Object) frameLayout3, "top_area");
                    ViewGroup.LayoutParams layoutParams = frameLayout3.getLayoutParams();
                    FrameLayout frameLayout4 = (FrameLayout) this.f259a.a(R.id.content);
                    j.a((Object) frameLayout4, "content");
                    layoutParams.height = frameLayout4.getHeight();
                    ((FrameLayout) this.f259a.a(R.id.top_area)).requestLayout();
                    valueAnimator = ValueAnimator.ofFloat(0.0f, 1.0f);
                    valueAnimator.setStartDelay(150);
                    valueAnimator.setDuration(500L);
                    valueAnimator.setInterpolator(b.b.a.f.a.f63b);
                    valueAnimator.addUpdateListener(new c(valueAnimator, this));
                }
                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(kotlin.a.m.d(ofFloat2, ofFloat, valueAnimator));
                animatorSet.start();
                return;
            }
        }
        FrameLayout frameLayout5 = (FrameLayout) this.f259a.a(R.id.content);
        j.a((Object) frameLayout5, "content");
        ViewGroup.MarginLayoutParams d3 = q1.d(frameLayout5);
        if (d3 != null) {
            d3.leftMargin = this.f259a.l();
        }
        b.a a5 = MainActivity.b(this.f259a).a();
        Resources resources3 = this.f259a.getResources();
        j.a((Object) resources3, "resources");
        if (b.b.a.b.c(resources3) && a5 != null && a5.f()) {
            FrameLayout frameLayout6 = (FrameLayout) this.f259a.a(R.id.content);
            j.a((Object) frameLayout6, "content");
            ViewGroup.LayoutParams layoutParams2 = frameLayout6.getLayoutParams();
            RootFrameLayout rootFrameLayout = (RootFrameLayout) this.f259a.a(R.id.root_layout);
            j.a((Object) rootFrameLayout, "root_layout");
            layoutParams2.width = rootFrameLayout.getWidth();
        }
        ((FrameLayout) this.f259a.a(R.id.content)).requestLayout();
        ObjectAnimator ofFloat3 = ObjectAnimator.ofFloat(this.f259a.a(R.id.dimmer), "alpha", 0.0f, 1.0f);
        ofFloat3.setDuration(500L);
        ValueAnimator ofFloat4 = ValueAnimator.ofFloat(1.0f, 0.0f);
        ofFloat4.setDuration(500L);
        ofFloat4.setInterpolator(b.b.a.f.a.f62a);
        ofFloat4.addUpdateListener(new a());
        Resources resources4 = this.f259a.getResources();
        j.a((Object) resources4, "resources");
        if (b.b.a.b.b(resources4)) {
            MainActivity.c(this.f259a);
            s.b bVar2 = new s.b();
            FrameLayout frameLayout7 = (FrameLayout) this.f259a.a(R.id.content);
            j.a((Object) frameLayout7, "content");
            bVar2.f5252a = frameLayout7.getWidth();
            ofFloat4.addUpdateListener(new d(bVar2));
        } else {
            MainActivity.c(this.f259a);
        }
        AnimatorSet animatorSet2 = new AnimatorSet();
        animatorSet2.playTogether(ofFloat4, ofFloat3);
        animatorSet2.start();
    }
}
