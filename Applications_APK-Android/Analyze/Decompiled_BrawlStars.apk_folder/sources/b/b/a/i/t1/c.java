package b.b.a.i.t1;

import android.view.View;
import b.b.a.e.a;
import b.b.a.i.t1.a;
import com.supercell.id.SupercellId;
import com.supercell.id.view.AvatarView;
import kotlin.m;

public final class c implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ a.b.C0067a f723a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ String f724b;
    public final /* synthetic */ a.b c;

    public c(a.b.C0067a aVar, String str, a.b bVar, int i) {
        this.f723a = aVar;
        this.f724b = str;
        this.c = bVar;
    }

    public final void onClick(View view) {
        AvatarView.a aVar;
        a.b bVar = this.c;
        int i = bVar.f711b;
        bVar.f711b = this.f723a.getAdapterPosition();
        if (i >= 0) {
            this.c.notifyItemChanged(i);
        }
        a.b bVar2 = this.c;
        bVar2.notifyItemChanged(bVar2.f711b);
        a.b bVar3 = this.c;
        kotlin.d.a.c<String, AvatarView.a, m> cVar = bVar3.d;
        String str = this.f724b;
        int i2 = bVar3.f711b;
        if (i == i2) {
            aVar = AvatarView.a.NONE;
        } else {
            aVar = (i > i2) == SupercellId.INSTANCE.getSharedServices$supercellId_release().e.isRTL() ? AvatarView.a.FROM_RIGHT : AvatarView.a.FROM_LEFT;
        }
        cVar.invoke(str, aVar);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().l.a(a.C0004a.BUTTON_01);
    }
}
