package b.b.a.i.v1;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import b.b.a.i.f1;
import b.b.a.i.x1.f;
import b.b.a.j.q1;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.a.ah;
import kotlin.a.ai;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.j.t;
import kotlin.m;

public final class g extends f1 {
    public static final Object c = new Object();
    public static final Object d = new Object();

    /* renamed from: b  reason: collision with root package name */
    public HashMap f769b;

    public final class a implements View.OnLayoutChangeListener {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ int f771b;

        /* renamed from: b.b.a.i.v1.g$a$a  reason: collision with other inner class name */
        public final class C0075a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ FrameLayout f772a;

            /* renamed from: b  reason: collision with root package name */
            public final /* synthetic */ a f773b;
            public final /* synthetic */ View c;

            public C0075a(FrameLayout frameLayout, a aVar, View view) {
                this.f772a = frameLayout;
                this.f773b = aVar;
                this.c = view;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
             arg types: [android.view.View, java.lang.String]
             candidates:
              kotlin.d.b.j.a(int, int):int
              kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
              kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
              kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
            public final void run() {
                FrameLayout frameLayout = this.f772a;
                View view = this.c;
                j.a((Object) view, "v");
                q1.d(frameLayout, view.getHeight() + this.f773b.f771b);
            }
        }

        public a(int i) {
            this.f771b = i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            FrameLayout frameLayout = (FrameLayout) g.this.a(R.id.games_content);
            if (frameLayout != null) {
                j.a((Object) view, "v");
                if (view.getHeight() + this.f771b != q1.e(frameLayout)) {
                    frameLayout.post(new C0075a(frameLayout, this, view));
                }
            }
        }
    }

    public final class b implements View.OnClickListener {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ b.b.a.h.b f775b;

        public b(b.b.a.h.b bVar, int i, boolean z) {
            this.f775b = bVar;
        }

        public final void onClick(View view) {
            String str;
            b.b.a.c.b bVar = SupercellId.INSTANCE.getSharedServices$supercellId_release().f;
            b.b.a.h.b bVar2 = this.f775b;
            j.b(bVar2, "$this$analyticsName");
            f fVar = SupercellId.INSTANCE.getSharedServices$supercellId_release().j;
            StringBuilder a2 = b.a.a.a.a.a("game_name_");
            a2.append(bVar2.f108a);
            String a3 = fVar.a(a2.toString());
            if (a3 != null) {
                str = a3;
            } else {
                str = bVar2.f108a;
            }
            b.b.a.c.b.a(bVar, "Public Profile Games", "click", str, null, false, 24);
            MainActivity a4 = b.b.a.b.a(g.this);
            if (a4 != null) {
                a4.a(this.f775b.d);
            }
        }
    }

    public final class c extends k implements kotlin.d.a.c<Drawable, b.b.a.i.x1.c, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f776a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(WeakReference weakReference) {
            super(2);
            this.f776a = weakReference;
        }

        public final Object invoke(Object obj, Object obj2) {
            ImageView imageView;
            Drawable drawable = (Drawable) obj;
            b.b.a.i.x1.c cVar = (b.b.a.i.x1.c) obj2;
            j.b(drawable, "drawable");
            j.b(cVar, "assetLocation");
            View view = (View) this.f776a.get();
            if (!(view == null || (imageView = (ImageView) view.findViewById(R.id.systemImageView)) == null)) {
                imageView.setImageDrawable(drawable);
                if (cVar == b.b.a.i.x1.c.EXTERNAL) {
                    imageView.setAlpha(0.0f);
                    imageView.animate().alpha(1.0f).setDuration(300).start();
                }
            }
            return m.f5330a;
        }
    }

    public final class d<T> implements Comparator<b.b.a.h.b> {

        /* renamed from: a  reason: collision with root package name */
        public static final d f777a = new d();

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [b.b.a.h.b, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* renamed from: a */
        public final int compare(b.b.a.h.b bVar, b.b.a.h.b bVar2) {
            j.a((Object) bVar, "o1");
            String a2 = b.b.a.b.a(bVar);
            j.a((Object) bVar2, "o2");
            return t.c(a2, b.b.a.b.a(bVar2), true);
        }
    }

    public final View a(int i) {
        if (this.f769b == null) {
            this.f769b = new HashMap();
        }
        View view = (View) this.f769b.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.f769b.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.p.a(java.lang.Iterable, int):int
     arg types: [java.util.List<java.lang.String>, int]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.p.a(java.lang.Iterable, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.ImageView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.constraint.ConstraintLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final View a(View view, b.b.a.h.b bVar, int i, boolean z) {
        TextView textView = (TextView) view.findViewById(R.id.systemNameLabel);
        j.a((Object) textView, "systemNameLabel");
        b.b.a.i.x1.j.a(textView, "game_name_" + bVar.f108a, (kotlin.d.a.b) null, 2);
        TextView textView2 = (TextView) view.findViewById(R.id.systemNicknameLabel);
        j.a((Object) textView2, "systemNicknameLabel");
        textView2.setText(bVar.f109b);
        TextView textView3 = (TextView) view.findViewById(R.id.systemNicknameLabel);
        j.a((Object) textView3, "systemNicknameLabel");
        String str = bVar.f109b;
        boolean z2 = true;
        int i2 = 0;
        textView3.setVisibility(str == null || t.a(str) ? 8 : 0);
        List<String> list = bVar.c;
        if (list != null) {
            ArrayList arrayList = new ArrayList(kotlin.a.m.a((Iterable) list, 10));
            int i3 = 0;
            for (T next : list) {
                int i4 = i3 + 1;
                if (i3 < 0) {
                    kotlin.a.m.a();
                }
                arrayList.add(kotlin.k.a(String.valueOf(i4), (String) next));
                i3 = i4;
            }
            Map a2 = ai.a(arrayList);
            TextView textView4 = (TextView) view.findViewById(R.id.systemLevelLabel);
            j.a((Object) textView4, "systemLevelLabel");
            b.b.a.i.x1.j.a(textView4, "player_level_info_" + bVar.f108a, a2);
            TextView textView5 = (TextView) view.findViewById(R.id.systemLevelLabel);
            j.a((Object) textView5, "systemLevelLabel");
            textView5.setVisibility(0);
        } else {
            TextView textView6 = (TextView) view.findViewById(R.id.systemLevelLabel);
            j.a((Object) textView6, "systemLevelLabel");
            textView6.setText((CharSequence) null);
            TextView textView7 = (TextView) view.findViewById(R.id.systemLevelLabel);
            j.a((Object) textView7, "systemLevelLabel");
            textView7.setVisibility(8);
        }
        ImageView imageView = (ImageView) view.findViewById(R.id.externalLinkImage);
        j.a((Object) imageView, "externalLinkImage");
        imageView.setVisibility(bVar.d != null ? 0 : 8);
        ConstraintLayout constraintLayout = (ConstraintLayout) view.findViewById(R.id.systemRowView);
        j.a((Object) constraintLayout, "systemRowView");
        if (bVar.d == null) {
            z2 = false;
        }
        constraintLayout.setEnabled(z2);
        ViewCompat.setBackground((ConstraintLayout) view.findViewById(R.id.systemRowView), ContextCompat.getDrawable(view.getContext(), i));
        if (bVar.d != null) {
            ((ConstraintLayout) view.findViewById(R.id.systemRowView)).setOnClickListener(new b(bVar, i, z));
        } else {
            ((ConstraintLayout) view.findViewById(R.id.systemRowView)).setOnClickListener(null);
        }
        ConstraintLayout constraintLayout2 = (ConstraintLayout) view.findViewById(R.id.systemRowView);
        j.a((Object) constraintLayout2, "systemRowView");
        constraintLayout2.setSoundEffectsEnabled(false);
        ImageView imageView2 = (ImageView) view.findViewById(R.id.onlineStatusIndicator);
        j.a((Object) imageView2, "onlineStatusIndicator");
        if (!z) {
            i2 = 8;
        }
        imageView2.setVisibility(i2);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().j.a(b.a.a.a.a.a(b.a.a.a.a.a("AppIcon_"), bVar.f108a, ".png"), new c(new WeakReference(view)));
        return view;
    }

    public final void a() {
        HashMap hashMap = this.f769b;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public final void c() {
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a("Public Profile Games");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final List<View> e() {
        LinearLayout linearLayout = (LinearLayout) a(R.id.connectedGamesView);
        kotlin.g.c b2 = kotlin.g.d.b(0, linearLayout.getChildCount());
        ArrayList arrayList = new ArrayList();
        Iterator it = b2.iterator();
        while (it.hasNext()) {
            View childAt = linearLayout.getChildAt(((ah) it).a());
            j.a((Object) childAt, "it");
            if (!j.a(childAt.getTag(), c)) {
                childAt = null;
            }
            if (childAt != null) {
                arrayList.add(childAt);
            }
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final List<View> f() {
        LinearLayout linearLayout = (LinearLayout) a(R.id.connectedGamesView);
        kotlin.g.c b2 = kotlin.g.d.b(0, linearLayout.getChildCount());
        ArrayList arrayList = new ArrayList();
        Iterator it = b2.iterator();
        while (it.hasNext()) {
            View childAt = linearLayout.getChildAt(((ah) it).a());
            j.a((Object) childAt, "it");
            if (!j.a(childAt.getTag(), d)) {
                childAt = null;
            }
            if (childAt != null) {
                arrayList.add(childAt);
            }
        }
        return arrayList;
    }

    public final void g() {
        View view = new View(getContext());
        view.setTag(c);
        view.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.gray95));
        view.setLayoutParams(new ViewGroup.LayoutParams(-1, kotlin.e.a.a(b.b.a.b.a(1))));
        ((LinearLayout) a(R.id.connectedGamesView)).addView(view);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_public_profile_games, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        ConstraintLayout constraintLayout;
        j.b(view, "view");
        Resources resources = getResources();
        j.a((Object) resources, "resources");
        h hVar = null;
        if (b.b.a.b.b(resources)) {
            int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.list_padding_vertical);
            Fragment parentFragment = getParentFragment();
            if (!(parentFragment instanceof j)) {
                parentFragment = null;
            }
            j jVar = (j) parentFragment;
            if (!(jVar == null || (constraintLayout = (ConstraintLayout) jVar.a(R.id.toolbarContainer)) == null)) {
                constraintLayout.addOnLayoutChangeListener(new a(dimensionPixelSize));
            }
        }
        Fragment parentFragment2 = getParentFragment();
        if (!(parentFragment2 instanceof j)) {
            parentFragment2 = null;
        }
        j jVar2 = (j) parentFragment2;
        if (jVar2 != null) {
            hVar = jVar2.q;
        }
        a(hVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.LinearLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.ProgressBar, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0044, code lost:
        if (r7 != null) goto L_0x0076;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0074, code lost:
        if (r7 != null) goto L_0x0076;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(b.b.a.i.v1.h r7) {
        /*
            r6 = this;
            android.view.View r0 = r6.getView()
            if (r0 == 0) goto L_0x007d
            boolean r0 = r7 instanceof b.b.a.i.v1.h.b
            r1 = 0
            java.lang.String r2 = "progressBar"
            java.lang.String r3 = "connectedGamesView"
            r4 = 4
            if (r0 == 0) goto L_0x0047
            int r0 = com.supercell.id.R.id.connectedGamesView
            android.view.View r0 = r6.a(r0)
            android.widget.LinearLayout r0 = (android.widget.LinearLayout) r0
            kotlin.d.b.j.a(r0, r3)
            r3 = r7
            b.b.a.i.v1.h$b r3 = (b.b.a.i.v1.h.b) r3
            java.util.List<b.b.a.h.b> r5 = r3.f
            boolean r5 = r5.isEmpty()
            if (r5 == 0) goto L_0x0027
            r1 = 4
        L_0x0027:
            r0.setVisibility(r1)
            int r0 = com.supercell.id.R.id.progressBar
            android.view.View r0 = r6.a(r0)
            com.supercell.id.view.ProgressBar r0 = (com.supercell.id.view.ProgressBar) r0
            kotlin.d.b.j.a(r0, r2)
            r0.setVisibility(r4)
            java.util.List<b.b.a.h.b> r0 = r3.f
            boolean r1 = r7.d()
            if (r1 == 0) goto L_0x0079
            b.b.a.h.g r7 = r7.e()
            if (r7 == 0) goto L_0x0079
            goto L_0x0076
        L_0x0047:
            int r0 = com.supercell.id.R.id.connectedGamesView
            android.view.View r0 = r6.a(r0)
            android.widget.LinearLayout r0 = (android.widget.LinearLayout) r0
            kotlin.d.b.j.a(r0, r3)
            r0.setVisibility(r4)
            int r0 = com.supercell.id.R.id.progressBar
            android.view.View r0 = r6.a(r0)
            com.supercell.id.view.ProgressBar r0 = (com.supercell.id.view.ProgressBar) r0
            kotlin.d.b.j.a(r0, r2)
            r0.setVisibility(r1)
            kotlin.a.ab r0 = kotlin.a.ab.f5210a
            java.util.List r0 = (java.util.List) r0
            if (r7 == 0) goto L_0x0079
            boolean r1 = r7.d()
            r2 = 1
            if (r1 != r2) goto L_0x0079
            b.b.a.h.g r7 = r7.e()
            if (r7 == 0) goto L_0x0079
        L_0x0076:
            java.lang.String r7 = r7.f118a
            goto L_0x007a
        L_0x0079:
            r7 = 0
        L_0x007a:
            r6.a(r0, r7)
        L_0x007d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.i.v1.g.a(b.b.a.i.v1.h):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
     arg types: [java.util.List<b.b.a.h.b>, b.b.a.i.v1.g$d]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.LinearLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    public final void a(List<b.b.a.h.b> list, String str) {
        View view;
        if (getView() != null) {
            List<View> e = e();
            List<View> f = f();
            List a2 = kotlin.a.m.a((Iterable) list, (Comparator) d.f777a);
            int i = 0;
            for (Object next : a2) {
                int i2 = i + 1;
                if (i < 0) {
                    kotlin.a.m.a();
                }
                b.b.a.h.b bVar = (b.b.a.h.b) next;
                if (i > 0) {
                    int i3 = i - 1;
                    if (i3 < 0 || i3 > kotlin.a.m.a((List) e)) {
                        g();
                    } else {
                        e.get(i3);
                    }
                }
                if (i < 0 || i > kotlin.a.m.a((List) f)) {
                    LinearLayout linearLayout = (LinearLayout) a(R.id.connectedGamesView);
                    View inflate = LayoutInflater.from(linearLayout.getContext()).inflate(R.layout.fragment_public_profile_games_item, (ViewGroup) ((LinearLayout) a(R.id.connectedGamesView)), false);
                    inflate.setTag(d);
                    linearLayout.addView(inflate);
                    j.a((Object) inflate, "newSystemRow()");
                    view = inflate;
                } else {
                    view = f.get(i);
                }
                View view2 = view;
                int size = a2.size() - 1;
                int i4 = i == 0 ? i == size ? R.drawable.list_button : R.drawable.list_button_top : i == size ? R.drawable.list_button_bottom : R.drawable.list_button_middle;
                j.a((Object) view2, "view");
                a(view2, bVar, i4, j.a((Object) bVar.f108a, (Object) str));
                i = i2;
            }
            for (View removeView : kotlin.a.m.c(e, Math.max(0, a2.size() - 1))) {
                ((LinearLayout) a(R.id.connectedGamesView)).removeView(removeView);
            }
            for (View removeView2 : kotlin.a.m.c(f, a2.size())) {
                ((LinearLayout) a(R.id.connectedGamesView)).removeView(removeView2);
            }
        }
    }
}
