package b.b.a.i.i1;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.ShareCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import b.b.a.i.a0;
import b.b.a.i.b;
import b.b.a.i.p1.b;
import b.b.a.i.x0;
import b.b.a.i.y;
import b.b.a.i.y1.a;
import b.b.a.i.z;
import b.b.a.i.z0;
import b.b.a.j.e0;
import b.b.a.j.f0;
import b.b.a.j.i0;
import b.b.a.j.q1;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import kotlin.a.an;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;
import nl.komponents.kovenant.bw;

public final class a extends b.b.a.i.g {
    public final kotlin.d.a.b<i0, m> m = new d();
    public final kotlin.d.a.b<b.b.a.j.m<b.b.a.h.d, f0>, m> n = new c();
    public boolean o;
    public HashMap p;

    /* renamed from: b.b.a.i.i1.a$a  reason: collision with other inner class name */
    public static final class C0026a extends b.a {
        public static final C0027a CREATOR = new C0027a(null);
        public final Set<Integer> e = an.a((Object[]) new Integer[]{Integer.valueOf(R.id.nav_area_back_button), Integer.valueOf(R.id.nav_area_close_button)});
        public final boolean f = true;
        public final Class<? extends b.b.a.i.g> g = a.class;

        /* renamed from: b.b.a.i.i1.a$a$a  reason: collision with other inner class name */
        public static final class C0027a implements Parcelable.Creator<C0026a> {
            public /* synthetic */ C0027a(kotlin.d.b.g gVar) {
            }

            public final Object createFromParcel(Parcel parcel) {
                j.b(parcel, "parcel");
                return new C0026a();
            }

            public final Object[] newArray(int i) {
                return new C0026a[i];
            }
        }

        public final int a(int i, int i2, int i3) {
            return a0.u.a(i, i2, i3);
        }

        public final int a(Resources resources, int i, int i2, int i3) {
            j.b(resources, "resources");
            if (i >= kotlin.e.a.a(b.b.a.b.a(600))) {
                return kotlin.e.a.a(((float) i) * 0.1f);
            }
            return 0;
        }

        public final Class<? extends b.b.a.i.g> a() {
            return this.g;
        }

        public final Class<? extends x0> a(Resources resources) {
            j.b(resources, "resources");
            return b.b.a.b.b(resources) ? z.class : z0.class;
        }

        public final int b(Resources resources, int i, int i2, int i3) {
            j.b(resources, "resources");
            return i2 + kotlin.e.a.a(b.b.a.b.a(68));
        }

        public final int c(Resources resources, int i, int i2, int i3) {
            j.b(resources, "resources");
            return a0.u.b(i, i2, i3);
        }

        public final Set<Integer> c() {
            return this.e;
        }

        public final boolean c(Resources resources) {
            j.b(resources, "resources");
            return false;
        }

        public final int describeContents() {
            return 0;
        }

        public final Class<? extends b.b.a.i.g> e(Resources resources) {
            j.b(resources, "resources");
            return b.b.a.b.b(resources) ? a0.class : b.class;
        }

        public final boolean e() {
            return this.f;
        }

        public final void writeToParcel(Parcel parcel, int i) {
        }
    }

    public static final class b extends b.b.a.i.g {
        public HashMap m;

        public final void a() {
            HashMap hashMap = this.m;
            if (hashMap != null) {
                hashMap.clear();
            }
        }

        public final View c() {
            int i = R.id.top_area_close_button;
            if (this.m == null) {
                this.m = new HashMap();
            }
            View view = (View) this.m.get(Integer.valueOf(i));
            if (view == null) {
                View view2 = getView();
                if (view2 == null) {
                    view = null;
                } else {
                    view = view2.findViewById(i);
                    this.m.put(Integer.valueOf(i), view);
                }
            }
            return (ImageButton) view;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
            j.b(layoutInflater, "inflater");
            return layoutInflater.inflate(R.layout.fragment_ingame_add_friend_top_area, viewGroup, false);
        }

        public final /* synthetic */ void onDestroyView() {
            super.onDestroyView();
            a();
        }
    }

    public final class c extends k implements kotlin.d.a.b<b.b.a.j.m<? extends b.b.a.h.d, ? extends f0>, m> {
        public c() {
            super(1);
        }

        public final Object invoke(Object obj) {
            b.b.a.h.d dVar;
            List<b.b.a.h.c> list;
            b.b.a.j.m mVar = (b.b.a.j.m) obj;
            int i = 0;
            int size = (mVar == null || (dVar = (b.b.a.h.d) mVar.a()) == null || (list = dVar.c) == null) ? 0 : list.size();
            TextView textView = (TextView) a.this.a(R.id.friend_requests_notification);
            if (textView != null) {
                if (size <= 0) {
                    i = 8;
                }
                textView.setVisibility(i);
                textView.setText(String.valueOf(size));
            }
            return m.f5330a;
        }
    }

    public final class d extends k implements kotlin.d.a.b<i0, m> {
        public d() {
            super(1);
        }

        public final Object invoke(Object obj) {
            b.b.a.h.h a2;
            String str;
            bw<Bitmap, Exception> a3;
            i0 i0Var = (i0) obj;
            if (!(!a.this.isAdded() || i0Var == null || (a2 = i0Var.a()) == null || (str = a2.f) == null || (a3 = e0.f1030b.a(str)) == null)) {
                nl.komponents.kovenant.c.m.a(a3, new b(new WeakReference(a.this)));
            }
            return m.f5330a;
        }
    }

    public final class e implements View.OnClickListener {
        public e() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.content.res.Resources, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onClick(View view) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "In-game - Invite Friends", "click", "My QR code info", null, false, 24);
            MainActivity a2 = b.b.a.b.a((Fragment) a.this);
            if (a2 != null) {
                j.a((Object) view, "it");
                j.b(a2, "$this$showMyCodeInfoDialogPopup");
                j.b(view, "view");
                y.a aVar = y.h;
                Rect b2 = q1.b(view);
                int i = R.layout.my_code_info_dialog_content;
                Resources resources = a2.getResources();
                j.a((Object) resources, "resources");
                a2.a(aVar.a(b2, i, b.b.a.b.b(resources)), "popupDialog");
            }
        }
    }

    public final class f implements View.OnClickListener {
        public f() {
        }

        public final void onClick(View view) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "In-game - Invite Friends", "click", "Scan QR code", null, false, 24);
            MainActivity a2 = b.b.a.b.a((Fragment) a.this);
            if (a2 != null) {
                a2.a(new a.C0095a());
            }
        }
    }

    public final class g implements View.OnClickListener {
        public g() {
        }

        public final void onClick(View view) {
            b.b.a.h.h a2;
            String str;
            if (!a.this.o) {
                b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "In-game - Invite Friends", "click", "Share invite", null, false, 24);
                i0 i0Var = (i0) SupercellId.INSTANCE.getSharedServices$supercellId_release().f().f1179a;
                if (i0Var != null && (a2 = i0Var.a()) != null && (str = a2.g) != null) {
                    a aVar = a.this;
                    aVar.o = true;
                    MainActivity a3 = b.b.a.b.a((Fragment) aVar);
                    if (a3 != null) {
                        ShareCompat.IntentBuilder type = ShareCompat.IntentBuilder.from(a3).setType("text/plain");
                        String b2 = SupercellId.INSTANCE.getSharedServices$supercellId_release().j.b("account_invite_friends_share_title");
                        if (b2 == null) {
                            b2 = "";
                        }
                        type.setChooserTitle(b2).setText(str).startChooser();
                    }
                }
            }
        }
    }

    public final class h implements View.OnClickListener {
        public h() {
        }

        public final void onClick(View view) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "In-game - Invite Friends", "click", "Friend requests", null, false, 24);
            MainActivity a2 = b.b.a.b.a((Fragment) a.this);
            if (a2 != null) {
                a2.a(new b.a());
            }
        }
    }

    public final View a(int i) {
        if (this.p == null) {
            this.p = new HashMap();
        }
        View view = (View) this.p.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.p.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public final void onAttach(Context context) {
        super.onAttach(context);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f().b(this.m);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().e().b(this.n);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_ingame_add_friend, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    public final void onDetach() {
        super.onDetach();
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f().c(this.m);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().e().c(this.n);
    }

    public final void onResume() {
        super.onResume();
        this.o = false;
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a("In-Game - Invite Friends");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.Context, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.LinearLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        j.b(view, "view");
        super.onViewCreated(view, bundle);
        ImageButton imageButton = (ImageButton) a(R.id.my_code_info_button);
        if (imageButton != null) {
            imageButton.setOnClickListener(new e());
        }
        Context context = view.getContext();
        j.a((Object) context, "view.context");
        if (b.b.a.b.a(context)) {
            ((LinearLayout) a(R.id.scan_friends_code)).setOnClickListener(new f());
        } else {
            LinearLayout linearLayout = (LinearLayout) a(R.id.scan_friends_code);
            j.a((Object) linearLayout, "scan_friends_code");
            linearLayout.setVisibility(8);
        }
        ((LinearLayout) a(R.id.share_invite)).setOnClickListener(new g());
        ((ConstraintLayout) a(R.id.friend_requests)).setOnClickListener(new h());
        this.m.invoke(SupercellId.INSTANCE.getSharedServices$supercellId_release().f().f1179a);
        this.n.invoke(SupercellId.INSTANCE.getSharedServices$supercellId_release().e().f1179a);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().e().a();
    }
}
