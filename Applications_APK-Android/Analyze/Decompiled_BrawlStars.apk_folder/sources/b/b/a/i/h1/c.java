package b.b.a.i.h1;

import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.facebook.internal.ServerProtocol;
import com.supercell.id.R;
import com.supercell.id.view.ExpandableFrameLayout;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class c extends k implements kotlin.d.a.c<Float, ExpandableFrameLayout.b, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ a f286a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ View f287b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c(a aVar, View view) {
        super(2);
        this.f286a = aVar;
        this.f287b = view;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.ImageView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.widget.NestedScrollView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.FrameLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final Object invoke(Object obj, Object obj2) {
        float floatValue = ((Number) obj).floatValue();
        ExpandableFrameLayout.b bVar = (ExpandableFrameLayout.b) obj2;
        j.b(bVar, ServerProtocol.DIALOG_PARAM_STATE);
        boolean z = true;
        if (ViewCompat.getLayoutDirection(this.f287b) != 1) {
            z = false;
        }
        float f = z ? -1.0f : 1.0f;
        View view = this.f287b;
        j.a((Object) view, "itemRow");
        ImageView imageView = (ImageView) view.findViewById(R.id.titleArrow);
        j.a((Object) imageView, "itemRow.titleArrow");
        imageView.setRotation(90.0f - ((f * 180.0f) * floatValue));
        if (bVar == ExpandableFrameLayout.b.EXPANDING) {
            NestedScrollView nestedScrollView = (NestedScrollView) this.f286a.a(R.id.faqScrollView);
            j.a((Object) nestedScrollView, "faqScrollView");
            View view2 = this.f287b;
            j.a((Object) view2, "itemRow");
            FrameLayout frameLayout = (FrameLayout) this.f286a.a(R.id.faqContainer);
            j.a((Object) frameLayout, "faqContainer");
            nestedScrollView.post(new d(nestedScrollView, view2, frameLayout.getPaddingBottom(), floatValue));
        }
        return m.f5330a;
    }
}
