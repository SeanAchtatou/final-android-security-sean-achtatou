package b.b.a.i.w1;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.FlowPager;
import android.view.View;
import b.b.a.i.g;
import b.b.a.i.w;
import b.b.a.i.x;
import b.b.a.j.q0;
import com.facebook.places.model.PlaceFields;
import com.supercell.id.IdPendingRegistration;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import java.util.HashMap;
import kotlin.d.b.j;
import kotlin.d.b.k;

public final class n extends w {
    public String m;
    public String n;
    public boolean o;
    public final kotlin.d.a.a<x>[] p = {b.f888a, c.f889a, d.f890a, e.f891a};
    public HashMap q;

    public static final class a extends w.a {
        public static final C0090a CREATOR = new C0090a(null);
        public final boolean e;
        public final Class<? extends g> f;
        public final IdPendingRegistration g;

        /* renamed from: b.b.a.i.w1.n$a$a  reason: collision with other inner class name */
        public static final class C0090a implements Parcelable.Creator<a> {
            public /* synthetic */ C0090a(kotlin.d.b.g gVar) {
            }

            public final Object createFromParcel(Parcel parcel) {
                j.b(parcel, "parcel");
                j.b(parcel, "parcel");
                return new a((IdPendingRegistration) parcel.readParcelable(IdPendingRegistration.class.getClassLoader()));
            }

            public final Object[] newArray(int i) {
                return new a[i];
            }
        }

        public a() {
            this(null);
        }

        public a(IdPendingRegistration idPendingRegistration) {
            this.g = idPendingRegistration;
            this.f = n.class;
        }

        public final g a(Context context) {
            j.b(context, "context");
            g a2 = super.a(context);
            if (this.g != null) {
                Bundle arguments = a2.getArguments();
                if (arguments == null) {
                    arguments = new Bundle();
                }
                arguments.putString(NotificationCompat.CATEGORY_EMAIL, this.g.getEmail());
                arguments.putString(PlaceFields.PHONE, this.g.getPhone());
                arguments.putBoolean("acceptMarketing", this.g.getAcceptMarketing());
                a2.setArguments(arguments);
            }
            return a2;
        }

        public final Class<? extends g> a() {
            return this.f;
        }

        public final g c(Context context) {
            j.b(context, "context");
            return w.c.n.a("register_progress_step_1", "register_progress_step_2", "register_progress_step_3", true);
        }

        public final boolean d(Resources resources) {
            j.b(resources, "resources");
            return true;
        }

        public final int describeContents() {
            return 0;
        }

        public final boolean e() {
            return this.e;
        }

        public final boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof a) && j.a(this.g, ((a) obj).g);
            }
            return true;
        }

        public final int hashCode() {
            IdPendingRegistration idPendingRegistration = this.g;
            if (idPendingRegistration != null) {
                return idPendingRegistration.hashCode();
            }
            return 0;
        }

        public final String toString() {
            StringBuilder a2 = b.a.a.a.a.a("BackStackEntry(pendingRegistration=");
            a2.append(this.g);
            a2.append(")");
            return a2.toString();
        }

        public final void writeToParcel(Parcel parcel, int i) {
            if (parcel != null) {
                parcel.writeParcelable(this.g, i);
            }
        }
    }

    public final class b extends k implements kotlin.d.a.a<p> {

        /* renamed from: a  reason: collision with root package name */
        public static final b f888a = new b();

        public b() {
            super(0);
        }

        public final Object invoke() {
            return new p();
        }
    }

    public final class c extends k implements kotlin.d.a.a<x> {

        /* renamed from: a  reason: collision with root package name */
        public static final c f889a = new c();

        public c() {
            super(0);
        }

        public final Object invoke() {
            return SupercellId.INSTANCE.getRemoteConfiguration$supercellId_release().a(q0.SMS_ENABLED) ? new b() : new f();
        }
    }

    public final class d extends k implements kotlin.d.a.a<k> {

        /* renamed from: a  reason: collision with root package name */
        public static final d f890a = new d();

        public d() {
            super(0);
        }

        public final Object invoke() {
            return new k();
        }
    }

    public final class e extends k implements kotlin.d.a.a<a> {

        /* renamed from: a  reason: collision with root package name */
        public static final e f891a = new e();

        public e() {
            super(0);
        }

        public final Object invoke() {
            return new a();
        }
    }

    public final View a(int i) {
        if (this.q == null) {
            this.q = new HashMap();
        }
        View view = (View) this.q.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.q.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public final kotlin.d.a.a<x>[] i() {
        return this.p;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v4.view.FlowPager, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        if (bundle == null) {
            Bundle arguments = getArguments();
            String str = null;
            String string = arguments != null ? arguments.getString(NotificationCompat.CATEGORY_EMAIL) : null;
            boolean z = false;
            if (string == null || string.length() == 0) {
                Bundle arguments2 = getArguments();
                if (arguments2 != null) {
                    str = arguments2.getString(PlaceFields.PHONE);
                }
                if (str == null || str.length() == 0) {
                    z = true;
                }
                if (z) {
                    return;
                }
            }
            FlowPager flowPager = (FlowPager) a(R.id.flowPager);
            j.a((Object) flowPager, "flowPager");
            flowPager.setCurrentItem(2);
        }
    }

    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = bundle != null ? bundle : getArguments();
        String str = null;
        this.m = arguments != null ? arguments.getString(NotificationCompat.CATEGORY_EMAIL) : null;
        Bundle arguments2 = bundle != null ? bundle : getArguments();
        if (arguments2 != null) {
            str = arguments2.getString(PlaceFields.PHONE);
        }
        this.n = str;
        if (bundle == null) {
            bundle = getArguments();
        }
        this.o = bundle != null ? bundle.getBoolean("acceptMarketing") : false;
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    public final void onSaveInstanceState(Bundle bundle) {
        j.b(bundle, "outState");
        String str = this.m;
        if (str != null) {
            bundle.putString(NotificationCompat.CATEGORY_EMAIL, str);
        }
        String str2 = this.n;
        if (str2 != null) {
            bundle.putString(PlaceFields.PHONE, str2);
        }
        bundle.putBoolean("acceptMarketing", this.o);
        super.onSaveInstanceState(bundle);
    }
}
