package b.b.a.i.s1;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import b.b.a.h.n;
import b.b.a.i.f1;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.ProgressBar;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import kotlin.a.ah;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.j.t;
import kotlin.m;
import nl.komponents.kovenant.bb;
import nl.komponents.kovenant.bc;
import nl.komponents.kovenant.bw;

public final class c extends f1 {
    public static final Object d = new Object();
    public static final Object e = new Object();

    /* renamed from: b  reason: collision with root package name */
    public final kotlin.d.a.b<b.b.a.h.e, m> f654b = new a();
    public HashMap c;

    public final class a extends k implements kotlin.d.a.b<b.b.a.h.e, m> {
        public a() {
            super(1);
        }

        public final Object invoke(Object obj) {
            b.b.a.b.a(new b(this, (b.b.a.h.e) obj));
            return m.f5330a;
        }
    }

    public final class b extends k implements kotlin.d.a.b<b.b.a.h.e, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f656a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(WeakReference weakReference) {
            super(1);
            this.f656a = weakReference;
        }

        public final Object invoke(Object obj) {
            Object obj2 = this.f656a.get();
            if (obj2 != null) {
                b.b.a.h.e eVar = (b.b.a.h.e) obj;
                c cVar = (c) obj2;
                cVar.f654b.invoke(eVar);
                cVar.a(eVar);
            }
            return m.f5330a;
        }
    }

    /* renamed from: b.b.a.i.s1.c$c  reason: collision with other inner class name */
    public final class C0062c extends k implements kotlin.d.a.b<Exception, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f657a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C0062c(WeakReference weakReference) {
            super(1);
            this.f657a = weakReference;
        }

        public final Object invoke(Object obj) {
            Object obj2 = this.f657a.get();
            if (obj2 != null) {
                Exception exc = (Exception) obj;
                MainActivity a2 = b.b.a.b.a((c) obj2);
                if (a2 != null) {
                    a2.a(exc, (kotlin.d.a.b<? super b.b.a.i.e, m>) null);
                }
            }
            return m.f5330a;
        }
    }

    public final class d extends k implements kotlin.d.a.b<b.b.a.h.e, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f658a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(WeakReference weakReference) {
            super(1);
            this.f658a = weakReference;
        }

        public final Object invoke(Object obj) {
            Object obj2 = this.f658a.get();
            if (obj2 != null) {
                ((c) obj2).f654b.invoke((b.b.a.h.e) obj);
            }
            return m.f5330a;
        }
    }

    public final class e extends k implements kotlin.d.a.b<b.b.a.h.e, bw<? extends b.b.a.h.e, ? extends Exception>> {

        /* renamed from: a  reason: collision with root package name */
        public static final e f659a = new e();

        public e() {
            super(1);
        }

        public final Object invoke(Object obj) {
            return SupercellId.INSTANCE.getSharedServices$supercellId_release().g.a();
        }
    }

    public final class f extends k implements kotlin.d.a.a<SharedPreferences.Editor> {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ b.b.a.h.e f661b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(b.b.a.h.e eVar) {
            super(0);
            this.f661b = eVar;
        }

        public final Object invoke() {
            SharedPreferences sharedPreferences;
            SharedPreferences.Editor edit;
            Context context = c.this.getContext();
            if (context == null || (sharedPreferences = context.getSharedPreferences("SupercellIdSystems", 0)) == null || (edit = sharedPreferences.edit()) == null) {
                return null;
            }
            edit.clear();
            edit.putString("info", this.f661b.a().toString(0));
            edit.apply();
            return edit;
        }
    }

    public final class g<T> implements Comparator<n> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ List f662a;

        public g(List list) {
            this.f662a = list;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [b.b.a.h.n, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* renamed from: a */
        public final int compare(n nVar, n nVar2) {
            boolean contains = this.f662a.contains(nVar.f135a);
            boolean contains2 = this.f662a.contains(nVar2.f135a);
            if (contains && !contains2) {
                return -1;
            }
            if (!contains && contains2) {
                return 1;
            }
            j.a((Object) nVar, "o1");
            String b2 = b.b.a.b.b(nVar);
            j.a((Object) nVar2, "o2");
            return t.c(b2, b.b.a.b.b(nVar2), true);
        }
    }

    public final View a(int i) {
        if (this.c == null) {
            this.c = new HashMap();
        }
        View view = (View) this.c.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.c.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.c;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public final void a(b.b.a.h.e eVar) {
        bw unused = bb.f5389a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
     arg types: [java.util.List<b.b.a.h.n>, b.b.a.i.s1.c$g]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.ImageView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.LinearLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x01a2  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x01b0  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x01da  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x01dd  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.util.List<java.lang.String> r20, java.util.List<b.b.a.h.n> r21) {
        /*
            r19 = this;
            r7 = r19
            r8 = r20
            android.view.View r0 = r19.getView()
            if (r0 == 0) goto L_0x025c
            java.util.List r9 = r19.e()
            java.util.List r10 = r19.f()
            b.b.a.i.s1.c$g r0 = new b.b.a.i.s1.c$g
            r0.<init>(r8)
            r1 = r21
            java.util.List r11 = kotlin.a.m.a(r1, r0)
            java.util.Iterator r12 = r11.iterator()
            r13 = 0
            r0 = 0
        L_0x0023:
            boolean r1 = r12.hasNext()
            r14 = 1
            if (r1 == 0) goto L_0x020d
            java.lang.Object r1 = r12.next()
            int r15 = r0 + 1
            if (r0 >= 0) goto L_0x0035
            kotlin.a.m.a()
        L_0x0035:
            r6 = r1
            b.b.a.h.n r6 = (b.b.a.h.n) r6
            if (r0 <= 0) goto L_0x004b
            int r1 = r0 + -1
            if (r1 < 0) goto L_0x0048
            int r2 = kotlin.a.m.a(r9)
            if (r1 > r2) goto L_0x0048
            r9.get(r1)
            goto L_0x004b
        L_0x0048:
            r19.g()
        L_0x004b:
            if (r0 < 0) goto L_0x0058
            int r1 = kotlin.a.m.a(r10)
            if (r0 > r1) goto L_0x0058
            java.lang.Object r1 = r10.get(r0)
            goto L_0x0084
        L_0x0058:
            int r1 = com.supercell.id.R.id.connectedGamesView
            android.view.View r1 = r7.a(r1)
            android.widget.LinearLayout r1 = (android.widget.LinearLayout) r1
            android.content.Context r2 = r1.getContext()
            android.view.LayoutInflater r2 = android.view.LayoutInflater.from(r2)
            int r3 = com.supercell.id.R.layout.fragment_profile_v1_list_item_game
            int r4 = com.supercell.id.R.id.connectedGamesView
            android.view.View r4 = r7.a(r4)
            android.widget.LinearLayout r4 = (android.widget.LinearLayout) r4
            android.view.View r2 = r2.inflate(r3, r4, r13)
            java.lang.Object r3 = b.b.a.i.s1.c.e
            r2.setTag(r3)
            r1.addView(r2)
            java.lang.String r1 = "newSystemRow()"
            kotlin.d.b.j.a(r2, r1)
            r1 = r2
        L_0x0084:
            r5 = r1
            android.view.View r5 = (android.view.View) r5
            int r1 = r11.size()
            int r1 = r1 - r14
            if (r0 != 0) goto L_0x0096
            if (r0 != r1) goto L_0x0093
            int r0 = com.supercell.id.R.drawable.list_button
            goto L_0x009d
        L_0x0093:
            int r0 = com.supercell.id.R.drawable.list_button_top
            goto L_0x009d
        L_0x0096:
            if (r0 != r1) goto L_0x009b
            int r0 = com.supercell.id.R.drawable.list_button_bottom
            goto L_0x009d
        L_0x009b:
            int r0 = com.supercell.id.R.drawable.list_button_middle
        L_0x009d:
            r4 = r0
            java.lang.String r0 = "view"
            kotlin.d.b.j.a(r5, r0)
            java.lang.String r0 = r6.f135a
            boolean r3 = r8.contains(r0)
            int r0 = com.supercell.id.R.id.systemNameLabel
            android.view.View r0 = r5.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            java.lang.String r1 = "systemNameLabel"
            kotlin.d.b.j.a(r0, r1)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "game_name_"
            r1.append(r2)
            java.lang.String r2 = r6.f135a
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r2 = 2
            r14 = 0
            b.b.a.i.x1.j.a(r0, r1, r14, r2)
            java.lang.String r0 = "installButton"
            java.lang.String r2 = "checkmark"
            if (r3 == 0) goto L_0x0109
            int r14 = com.supercell.id.R.id.systemStatusLabel
            android.view.View r14 = r5.findViewById(r14)
            android.widget.TextView r14 = (android.widget.TextView) r14
            android.content.Context r1 = r5.getContext()
            int r13 = com.supercell.id.R.color.accent_green
            int r1 = android.support.v4.content.ContextCompat.getColor(r1, r13)
            r14.setTextColor(r1)
            int r1 = com.supercell.id.R.id.checkmark
            android.view.View r1 = r5.findViewById(r1)
            android.widget.ImageView r1 = (android.widget.ImageView) r1
            kotlin.d.b.j.a(r1, r2)
            r2 = 0
            r1.setVisibility(r2)
            int r1 = com.supercell.id.R.id.installButton
            android.view.View r1 = r5.findViewById(r1)
            com.supercell.id.view.WidthAdjustingMultilineButton r1 = (com.supercell.id.view.WidthAdjustingMultilineButton) r1
            kotlin.d.b.j.a(r1, r0)
            r0 = 8
            r1.setVisibility(r0)
            goto L_0x0166
        L_0x0109:
            int r1 = com.supercell.id.R.id.systemStatusLabel
            android.view.View r1 = r5.findViewById(r1)
            android.widget.TextView r1 = (android.widget.TextView) r1
            android.content.Context r13 = r5.getContext()
            int r14 = com.supercell.id.R.color.gray67
            int r13 = android.support.v4.content.ContextCompat.getColor(r13, r14)
            r1.setTextColor(r13)
            int r1 = com.supercell.id.R.id.checkmark
            android.view.View r1 = r5.findViewById(r1)
            android.widget.ImageView r1 = (android.widget.ImageView) r1
            kotlin.d.b.j.a(r1, r2)
            r2 = 8
            r1.setVisibility(r2)
            int r1 = com.supercell.id.R.id.installButton
            android.view.View r1 = r5.findViewById(r1)
            com.supercell.id.view.WidthAdjustingMultilineButton r1 = (com.supercell.id.view.WidthAdjustingMultilineButton) r1
            kotlin.d.b.j.a(r1, r0)
            r0 = 0
            r1.setVisibility(r0)
            java.lang.String r1 = r6.a()
            if (r1 == 0) goto L_0x0166
            int r0 = com.supercell.id.R.id.installButton
            android.view.View r0 = r5.findViewById(r0)
            r13 = r0
            com.supercell.id.view.WidthAdjustingMultilineButton r13 = (com.supercell.id.view.WidthAdjustingMultilineButton) r13
            b.b.a.i.s1.d r14 = new b.b.a.i.s1.d
            r0 = r14
            r2 = r5
            r16 = r3
            r3 = r19
            r17 = r4
            r4 = r6
            r8 = r5
            r5 = r16
            r18 = r12
            r12 = r6
            r6 = r17
            r0.<init>(r1, r2, r3, r4, r5, r6)
            r13.setOnClickListener(r14)
            goto L_0x016e
        L_0x0166:
            r16 = r3
            r17 = r4
            r8 = r5
            r18 = r12
            r12 = r6
        L_0x016e:
            int r0 = com.supercell.id.R.id.systemRowView
            android.view.View r0 = r8.findViewById(r0)
            android.widget.LinearLayout r0 = (android.widget.LinearLayout) r0
            java.lang.String r1 = "systemRowView"
            kotlin.d.b.j.a(r0, r1)
            r2 = r16
            if (r2 == 0) goto L_0x0185
            java.lang.String r3 = r12.c
            if (r3 == 0) goto L_0x0185
            r3 = 1
            goto L_0x0186
        L_0x0185:
            r3 = 0
        L_0x0186:
            r0.setEnabled(r3)
            int r0 = com.supercell.id.R.id.systemRowView
            android.view.View r0 = r8.findViewById(r0)
            android.widget.LinearLayout r0 = (android.widget.LinearLayout) r0
            android.content.Context r3 = r8.getContext()
            r4 = r17
            android.graphics.drawable.Drawable r3 = android.support.v4.content.ContextCompat.getDrawable(r3, r4)
            android.support.v4.view.ViewCompat.setBackground(r0, r3)
            java.lang.String r0 = r12.c
            if (r0 == 0) goto L_0x01b0
            int r0 = com.supercell.id.R.id.systemRowView
            android.view.View r0 = r8.findViewById(r0)
            android.widget.LinearLayout r0 = (android.widget.LinearLayout) r0
            b.b.a.i.s1.e r3 = new b.b.a.i.s1.e
            r3.<init>(r7, r12, r2, r4)
            goto L_0x01b9
        L_0x01b0:
            int r0 = com.supercell.id.R.id.systemRowView
            android.view.View r0 = r8.findViewById(r0)
            android.widget.LinearLayout r0 = (android.widget.LinearLayout) r0
            r3 = 0
        L_0x01b9:
            r0.setOnClickListener(r3)
            int r0 = com.supercell.id.R.id.systemRowView
            android.view.View r0 = r8.findViewById(r0)
            android.widget.LinearLayout r0 = (android.widget.LinearLayout) r0
            kotlin.d.b.j.a(r0, r1)
            r1 = 0
            r0.setSoundEffectsEnabled(r1)
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference
            r0.<init>(r8)
            com.supercell.id.SupercellId r1 = com.supercell.id.SupercellId.INSTANCE
            b.b.a.j.x r1 = r1.getSharedServices$supercellId_release()
            b.b.a.i.x1.f r1 = r1.j
            if (r2 == 0) goto L_0x01dd
            java.lang.String r2 = "account_games_info_connected"
            goto L_0x01df
        L_0x01dd:
            java.lang.String r2 = "account_games_info_not_connected"
        L_0x01df:
            b.b.a.i.s1.f r3 = new b.b.a.i.s1.f
            r3.<init>(r0)
            r1.a(r2, r3)
            com.supercell.id.SupercellId r1 = com.supercell.id.SupercellId.INSTANCE
            b.b.a.j.x r1 = r1.getSharedServices$supercellId_release()
            b.b.a.i.x1.f r1 = r1.j
            java.lang.String r2 = "AppIcon_"
            java.lang.StringBuilder r2 = b.a.a.a.a.a(r2)
            java.lang.String r3 = r12.f135a
            java.lang.String r4 = ".png"
            java.lang.String r2 = b.a.a.a.a.a(r2, r3, r4)
            b.b.a.i.s1.g r3 = new b.b.a.i.s1.g
            r3.<init>(r0)
            r1.a(r2, r3)
            r8 = r20
            r0 = r15
            r12 = r18
            r13 = 0
            goto L_0x0023
        L_0x020d:
            int r0 = r11.size()
            r1 = 1
            int r0 = r0 - r1
            r1 = 0
            int r0 = java.lang.Math.max(r1, r0)
            java.util.List r0 = kotlin.a.m.c(r9, r0)
            java.util.Iterator r0 = r0.iterator()
        L_0x0220:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0238
            java.lang.Object r1 = r0.next()
            android.view.View r1 = (android.view.View) r1
            int r2 = com.supercell.id.R.id.connectedGamesView
            android.view.View r2 = r7.a(r2)
            android.widget.LinearLayout r2 = (android.widget.LinearLayout) r2
            r2.removeView(r1)
            goto L_0x0220
        L_0x0238:
            int r0 = r11.size()
            java.util.List r0 = kotlin.a.m.c(r10, r0)
            java.util.Iterator r0 = r0.iterator()
        L_0x0244:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x025c
            java.lang.Object r1 = r0.next()
            android.view.View r1 = (android.view.View) r1
            int r2 = com.supercell.id.R.id.connectedGamesView
            android.view.View r2 = r7.a(r2)
            android.widget.LinearLayout r2 = (android.widget.LinearLayout) r2
            r2.removeView(r1)
            goto L_0x0244
        L_0x025c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.i.s1.c.a(java.util.List, java.util.List):void");
    }

    public final void c() {
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a("Connected Games");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final List<View> e() {
        LinearLayout linearLayout = (LinearLayout) a(R.id.connectedGamesView);
        kotlin.g.c b2 = kotlin.g.d.b(0, linearLayout.getChildCount());
        ArrayList arrayList = new ArrayList();
        Iterator it = b2.iterator();
        while (it.hasNext()) {
            View childAt = linearLayout.getChildAt(((ah) it).a());
            j.a((Object) childAt, "it");
            if (!j.a(childAt.getTag(), d)) {
                childAt = null;
            }
            if (childAt != null) {
                arrayList.add(childAt);
            }
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final List<View> f() {
        LinearLayout linearLayout = (LinearLayout) a(R.id.connectedGamesView);
        kotlin.g.c b2 = kotlin.g.d.b(0, linearLayout.getChildCount());
        ArrayList arrayList = new ArrayList();
        Iterator it = b2.iterator();
        while (it.hasNext()) {
            View childAt = linearLayout.getChildAt(((ah) it).a());
            j.a((Object) childAt, "it");
            if (!j.a(childAt.getTag(), e)) {
                childAt = null;
            }
            if (childAt != null) {
                arrayList.add(childAt);
            }
        }
        return arrayList;
    }

    public final void g() {
        View view = new View(getContext());
        view.setTag(d);
        view.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.gray95));
        view.setLayoutParams(new ViewGroup.LayoutParams(-1, kotlin.e.a.a(b.b.a.b.a(1))));
        ((LinearLayout) a(R.id.connectedGamesView)).addView(view);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_profile_v1_connected_games, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.LinearLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.ProgressBar, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        j.b(view, "view");
        LinearLayout linearLayout = (LinearLayout) a(R.id.connectedGamesView);
        j.a((Object) linearLayout, "connectedGamesView");
        linearLayout.setVisibility(4);
        ProgressBar progressBar = (ProgressBar) a(R.id.progressBar);
        j.a((Object) progressBar, "progressBar");
        progressBar.setVisibility(0);
        bw a2 = bb.f5389a;
        nl.komponents.kovenant.c.m.a(a2, new d(new WeakReference(this)));
        bw a3 = bc.a(bc.a(a2, e.f659a), bc.a(a2, e.f659a).h());
        WeakReference weakReference = new WeakReference(this);
        nl.komponents.kovenant.c.m.a(a3, new b(weakReference));
        nl.komponents.kovenant.c.m.b(a3, new C0062c(weakReference));
    }
}
