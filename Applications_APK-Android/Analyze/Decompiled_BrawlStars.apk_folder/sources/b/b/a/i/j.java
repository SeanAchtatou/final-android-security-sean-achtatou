package b.b.a.i;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import nl.komponents.kovenant.CancelException;
import nl.komponents.kovenant.ap;

public final class j extends AnimatorListenerAdapter {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ ap f301a;

    public j(ap apVar) {
        this.f301a = apVar;
    }

    public final void onAnimationCancel(Animator animator) {
        this.f301a.f(new CancelException());
    }

    public final void onAnimationEnd(Animator animator) {
        try {
            this.f301a.e(true);
        } catch (IllegalStateException unused) {
        }
    }
}
