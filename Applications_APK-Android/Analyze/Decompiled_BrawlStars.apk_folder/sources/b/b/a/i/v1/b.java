package b.b.a.i.v1;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import b.b.a.h.j;
import b.b.a.i.f1;
import b.b.a.i.v1.j;
import b.b.a.j.k0;
import b.b.a.j.o;
import b.b.a.j.q1;
import b.b.a.j.r;
import b.b.a.j.r0;
import b.b.a.j.s0;
import b.b.a.j.u0;
import b.b.a.j.y0;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.WidthAdjustingMultilineButton;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import kotlin.a.ab;
import kotlin.k;
import kotlin.m;
import nl.komponents.kovenant.bb;
import nl.komponents.kovenant.bc;
import nl.komponents.kovenant.bw;

public final class b extends f1 {

    /* renamed from: b  reason: collision with root package name */
    public List<? extends r0> f743b;
    public final y0<List<b.b.a.h.c>> c = new y0<>(new d(), new e());
    public final y0<u0> d = new y0<>(new f(), new g());
    public HashMap e;

    public static final class a extends s0 {

        /* renamed from: b  reason: collision with root package name */
        public final b f744b;

        /* renamed from: b.b.a.i.v1.b$a$a  reason: collision with other inner class name */
        public final class C0072a implements View.OnClickListener {

            /* renamed from: b  reason: collision with root package name */
            public final /* synthetic */ r0 f746b;

            public C0072a(r0 r0Var, int i) {
                this.f746b = r0Var;
            }

            public final void onClick(View view) {
                MainActivity a2 = b.b.a.b.a(a.this.f744b);
                if (a2 != null) {
                    a aVar = (a) this.f746b;
                    a2.a(new j.a(null, aVar.f742b, aVar.c, aVar.d, aVar.f, aVar.e, false, 64));
                }
            }
        }

        /* renamed from: b.b.a.i.v1.b$a$b  reason: collision with other inner class name */
        public final class C0073b implements View.OnClickListener {

            /* renamed from: b  reason: collision with root package name */
            public final /* synthetic */ r0 f748b;

            public C0073b(r0 r0Var, int i) {
                this.f748b = r0Var;
            }

            public final void onClick(View view) {
                b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Public Profile Friends", "click", "Add", null, false, 24);
                a.this.f744b.a((a) this.f748b);
            }
        }

        public final class c implements View.OnClickListener {
            public c(r0 r0Var, int i) {
            }

            public final void onClick(View view) {
                b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Public Profile Friends", "click", "Retry", null, false, 24);
                b bVar = a.this.f744b;
                bVar.d.a(bb.f5389a);
                a.this.f744b.e();
            }
        }

        public a(Context context, b bVar) {
            kotlin.d.b.j.b(context, "context");
            kotlin.d.b.j.b(bVar, "fragment");
            this.f744b = bVar;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.TextView, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.ImageView, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.Button, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void a(s0.a aVar, int i, r0 r0Var) {
            int i2;
            String str;
            Resources resources;
            kotlin.d.b.j.b(aVar, "holder");
            kotlin.d.b.j.b(r0Var, "item");
            int i3 = 0;
            if (r0Var instanceof a) {
                if (i == this.f1164a.size() - 1) {
                    i2 = 0;
                } else {
                    i2 = aVar.c.getResources().getDimensionPixelSize(R.dimen.list_padding_horizontal);
                }
                b.b.a.b.a((LinearLayout) aVar.c.findViewById(R.id.friendContainer), b.b.a.b.b(this.f1164a, i), b.b.a.b.a(this.f1164a, i), 0, i2);
                Context context = aVar.c.getContext();
                if (!(context == null || (resources = context.getResources()) == null)) {
                    k0.f1078b.a(((a) r0Var).d, (ImageView) aVar.c.findViewById(R.id.friendImageView), resources);
                }
                TextView textView = (TextView) aVar.c.findViewById(R.id.friendNameLabel);
                kotlin.d.b.j.a((Object) textView, "containerView.friendNameLabel");
                a aVar2 = (a) r0Var;
                String str2 = aVar2.c;
                if (str2 == null) {
                    str2 = r.f1160a.a(aVar2.f742b);
                }
                textView.setText(str2);
                ((TextView) aVar.c.findViewById(R.id.friendNameLabel)).setTextColor(ContextCompat.getColor(aVar.c.getContext(), aVar2.c == null ? R.color.gray40 : R.color.black));
                b.b.a.h.j jVar = aVar2.f;
                if (jVar instanceof j.d) {
                    TextView textView2 = (TextView) aVar.c.findViewById(R.id.friendStatusLabel);
                    kotlin.d.b.j.a((Object) textView2, "containerView.friendStatusLabel");
                    int i4 = aVar2.g;
                    if (i4 == 0) {
                        str = "account_friend_profile_friend_status_no_mutual_friends";
                    } else if (i4 != 1) {
                        b.b.a.i.x1.j.a(textView2, "account_friend_profile_friend_status_more_than_one_mutual_friends", k.a("number", String.valueOf(i4)));
                    } else {
                        str = "account_friend_profile_friend_status_single_mutual_friend";
                    }
                    b.b.a.i.x1.j.a(textView2, str, (kotlin.d.a.b) null, 2);
                } else if (jVar instanceof j.a.c) {
                    TextView textView3 = (TextView) aVar.c.findViewById(R.id.friendStatusLabel);
                    kotlin.d.b.j.a((Object) textView3, "containerView.friendStatusLabel");
                    b.b.a.i.x1.j.a(textView3, "account_friend_profile_friend_status_invite_sent", (kotlin.d.a.b) null, 2);
                } else if (jVar instanceof j.a.b) {
                    TextView textView4 = (TextView) aVar.c.findViewById(R.id.friendStatusLabel);
                    kotlin.d.b.j.a((Object) textView4, "containerView.friendStatusLabel");
                    b.b.a.i.x1.j.a(textView4, "account_friend_profile_friend_status_invite_received", (kotlin.d.a.b) null, 2);
                } else if (jVar instanceof j.a.C0008a) {
                    TextView textView5 = (TextView) aVar.c.findViewById(R.id.friendStatusLabel);
                    kotlin.d.b.j.a((Object) textView5, "containerView.friendStatusLabel");
                    b.b.a.i.x1.j.a(textView5, "account_friend_profile_friend_status_friend", (kotlin.d.a.b) null, 2);
                }
                ImageView imageView = (ImageView) aVar.c.findViewById(R.id.arrowImageView);
                kotlin.d.b.j.a((Object) imageView, "containerView.arrowImageView");
                imageView.setVisibility(aVar2.f instanceof j.d ? 8 : 0);
                View findViewById = aVar.c.findViewById(R.id.buttonSeparator);
                kotlin.d.b.j.a((Object) findViewById, "containerView.buttonSeparator");
                findViewById.setVisibility(aVar2.f instanceof j.d ? 0 : 8);
                Button button = (Button) aVar.c.findViewById(R.id.addButton);
                kotlin.d.b.j.a((Object) button, "containerView.addButton");
                if (!(aVar2.f instanceof j.d)) {
                    i3 = 8;
                }
                button.setVisibility(i3);
                ((LinearLayout) aVar.c.findViewById(R.id.friendContainer)).setOnClickListener(new C0072a(r0Var, i));
                ((Button) aVar.c.findViewById(R.id.addButton)).setOnClickListener(new C0073b(r0Var, i));
            } else if (r0Var instanceof o) {
                LinearLayout linearLayout = (LinearLayout) aVar.c.findViewById(R.id.errorContainer);
                ViewGroup.MarginLayoutParams d = q1.d(linearLayout);
                if (d != null) {
                    d.topMargin = 0;
                }
                linearLayout.requestLayout();
                ((WidthAdjustingMultilineButton) aVar.c.findViewById(R.id.errorRetryButton)).setOnClickListener(new c(r0Var, i));
            }
        }
    }

    /* renamed from: b.b.a.i.v1.b$b  reason: collision with other inner class name */
    public final class C0074b extends kotlin.d.b.k implements kotlin.d.a.b<String, bw<? extends List<? extends b.b.a.h.c>, ? extends Exception>> {

        /* renamed from: a  reason: collision with root package name */
        public static final C0074b f750a = new C0074b();

        public C0074b() {
            super(1);
        }

        public final Object invoke(Object obj) {
            String str = (String) obj;
            kotlin.d.b.j.b(str, "it");
            return SupercellId.INSTANCE.getSharedServices$supercellId_release().h.b(str);
        }
    }

    public final class c implements View.OnLayoutChangeListener {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ int f752b;

        public final class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ RecyclerView f753a;

            /* renamed from: b  reason: collision with root package name */
            public final /* synthetic */ int f754b;

            public a(RecyclerView recyclerView, int i) {
                this.f753a = recyclerView;
                this.f754b = i;
            }

            public final void run() {
                RecyclerView.LayoutManager layoutManager = this.f753a.getLayoutManager();
                Parcelable onSaveInstanceState = layoutManager != null ? layoutManager.onSaveInstanceState() : null;
                q1.d(this.f753a, this.f754b);
                RecyclerView.LayoutManager layoutManager2 = this.f753a.getLayoutManager();
                if (layoutManager2 != null) {
                    layoutManager2.onRestoreInstanceState(onSaveInstanceState);
                }
            }
        }

        public c(int i) {
            this.f752b = i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            RecyclerView recyclerView = (RecyclerView) b.this.a(R.id.friendsList);
            if (recyclerView != null) {
                kotlin.d.b.j.a((Object) view, "v");
                int height = view.getHeight() + this.f752b;
                if (height != q1.e(recyclerView)) {
                    recyclerView.post(new a(recyclerView, height));
                }
            }
        }
    }

    public final class d extends kotlin.d.b.k implements kotlin.d.a.b<List<? extends b.b.a.h.c>, m> {
        public d() {
            super(1);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.a.p.a(java.lang.Iterable, int):int
         arg types: [java.util.List<b.b.a.h.c>, int]
         candidates:
          kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
          kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
          kotlin.a.w.a(java.util.List, int):T
          kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
          kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
          kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
          kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
          kotlin.a.s.a(java.util.List, java.util.Comparator):void
          kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
          kotlin.a.p.a(java.lang.Iterable, int):int */
        public final void a(List<b.b.a.h.c> list) {
            kotlin.d.b.j.b(list, "list");
            b bVar = b.this;
            ArrayList arrayList = new ArrayList(kotlin.a.m.a((Iterable) list, 10));
            for (b.b.a.h.c cVar : list) {
                arrayList.add(new a(cVar.f110a, cVar.f111b, cVar.c, cVar.d, cVar.e, cVar.f));
            }
            bVar.d.a(bb.f5389a);
        }

        public final /* synthetic */ Object invoke(Object obj) {
            a((List) obj);
            return m.f5330a;
        }
    }

    public final class e extends kotlin.d.b.k implements kotlin.d.a.b<Exception, m> {
        public e() {
            super(1);
        }

        public final Object invoke(Object obj) {
            Exception exc = (Exception) obj;
            kotlin.d.b.j.b(exc, "it");
            b.this.getClass().getName();
            "Error receiving friends " + exc.getMessage();
            b bVar = b.this;
            bVar.d.a(bb.f5389a);
            return m.f5330a;
        }
    }

    public final class f extends kotlin.d.b.k implements kotlin.d.a.b<u0, m> {
        public f() {
            super(1);
        }

        public final void a(u0 u0Var) {
            kotlin.d.b.j.b(u0Var, "it");
            b bVar = b.this;
            if (bVar.f743b == u0Var.f1176a) {
                bVar.f743b = u0Var.f1177b;
                if (bVar.f743b == null) {
                    RecyclerView recyclerView = (RecyclerView) bVar.a(R.id.friendsList);
                    if (recyclerView != null) {
                        recyclerView.setVisibility(4);
                    }
                    View a2 = b.this.a(R.id.progressBar);
                    if (a2 != null) {
                        a2.setVisibility(0);
                    }
                } else {
                    RecyclerView recyclerView2 = (RecyclerView) bVar.a(R.id.friendsList);
                    if (recyclerView2 != null) {
                        recyclerView2.setVisibility(0);
                    }
                    View a3 = b.this.a(R.id.progressBar);
                    if (a3 != null) {
                        a3.setVisibility(4);
                    }
                }
                RecyclerView recyclerView3 = (RecyclerView) b.this.a(R.id.friendsList);
                RecyclerView.Adapter adapter = recyclerView3 != null ? recyclerView3.getAdapter() : null;
                if (!(adapter instanceof a)) {
                    adapter = null;
                }
                a aVar = (a) adapter;
                if (aVar != null) {
                    List<? extends r0> list = b.this.f743b;
                    if (list == null) {
                        list = ab.f5210a;
                    }
                    aVar.a(list);
                    u0Var.c.dispatchUpdatesTo(aVar);
                }
            }
        }

        public final /* synthetic */ Object invoke(Object obj) {
            a((u0) obj);
            return m.f5330a;
        }
    }

    public final class g extends kotlin.d.b.k implements kotlin.d.a.b<Exception, m> {
        public g() {
            super(1);
        }

        public final Object invoke(Object obj) {
            Exception exc = (Exception) obj;
            kotlin.d.b.j.b(exc, "it");
            MainActivity a2 = b.b.a.b.a(b.this);
            if (a2 != null) {
                a2.a(exc, (kotlin.d.a.b<? super b.b.a.i.e, m>) null);
            }
            return m.f5330a;
        }
    }

    public final class h extends kotlin.d.b.k implements kotlin.d.a.b<Exception, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f759a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ a f760b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(WeakReference weakReference, a aVar) {
            super(1);
            this.f759a = weakReference;
            this.f760b = aVar;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.a.p.a(java.lang.Iterable, int):int
         arg types: [java.util.ArrayList, int]
         candidates:
          kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
          kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
          kotlin.a.w.a(java.util.List, int):T
          kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
          kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
          kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
          kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
          kotlin.a.s.a(java.util.List, java.util.Comparator):void
          kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
          kotlin.a.p.a(java.lang.Iterable, int):int */
        public final void invoke(Exception exc) {
            Object obj = this.f759a.get();
            if (obj != null) {
                Exception exc2 = exc;
                b bVar = (b) obj;
                MainActivity a2 = b.b.a.b.a(bVar);
                if (a2 != null) {
                    a2.a(exc2, (kotlin.d.a.b<? super b.b.a.i.e, m>) null);
                }
                List<? extends r0> list = bVar.f743b;
                if (list != null) {
                    ArrayList<a> arrayList = new ArrayList<>();
                    for (r0 r0Var : list) {
                        if (!(r0Var instanceof a)) {
                            r0Var = null;
                        }
                        a aVar = (a) r0Var;
                        if (aVar != null) {
                            arrayList.add(aVar);
                        }
                    }
                    ArrayList arrayList2 = new ArrayList(kotlin.a.m.a((Iterable) arrayList, 10));
                    for (a aVar2 : arrayList) {
                        if (aVar2.a(this.f760b)) {
                            aVar2 = a.a(aVar2, null, null, null, null, this.f760b.f, 0, 47);
                        }
                        arrayList2.add(aVar2);
                    }
                    bVar.d.a(bb.f5389a);
                }
            }
        }
    }

    public final View a(int i) {
        if (this.e == null) {
            this.e = new HashMap();
        }
        View view = (View) this.e.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.e.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.e;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public final void c() {
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a("Public Profile Friends");
    }

    public final void e() {
        bw<String, Exception> bwVar;
        Fragment parentFragment = getParentFragment();
        if (!(parentFragment instanceof j)) {
            parentFragment = null;
        }
        j jVar = (j) parentFragment;
        if (jVar == null || (bwVar = jVar.s) == null) {
            bwVar = bb.f5389a;
        }
        this.c.a(bc.a(bc.a(bwVar, C0074b.f750a), bc.a(bwVar, C0074b.f750a).h()));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kotlin.d.b.j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_public_profile_friends, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v7.widget.RecyclerView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.Context, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        ConstraintLayout constraintLayout;
        kotlin.d.b.j.b(view, "view");
        Resources resources = getResources();
        kotlin.d.b.j.a((Object) resources, "resources");
        if (b.b.a.b.b(resources)) {
            int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.list_padding_vertical);
            Fragment parentFragment = getParentFragment();
            if (!(parentFragment instanceof j)) {
                parentFragment = null;
            }
            j jVar = (j) parentFragment;
            if (!(jVar == null || (constraintLayout = (ConstraintLayout) jVar.a(R.id.toolbarContainer)) == null)) {
                constraintLayout.addOnLayoutChangeListener(new c(dimensionPixelSize));
            }
        }
        if (this.f743b == null) {
            RecyclerView recyclerView = (RecyclerView) a(R.id.friendsList);
            kotlin.d.b.j.a((Object) recyclerView, "friendsList");
            recyclerView.setVisibility(4);
            View a2 = a(R.id.progressBar);
            kotlin.d.b.j.a((Object) a2, "progressBar");
            a2.setVisibility(0);
        } else {
            RecyclerView recyclerView2 = (RecyclerView) a(R.id.friendsList);
            kotlin.d.b.j.a((Object) recyclerView2, "friendsList");
            recyclerView2.setVisibility(0);
            View a3 = a(R.id.progressBar);
            kotlin.d.b.j.a((Object) a3, "progressBar");
            a3.setVisibility(4);
        }
        Context context = view.getContext();
        kotlin.d.b.j.a((Object) context, "view.context");
        a aVar = new a(context, this);
        List<? extends r0> list = this.f743b;
        if (list == null) {
            list = ab.f5210a;
        }
        aVar.a(list);
        RecyclerView recyclerView3 = (RecyclerView) a(R.id.friendsList);
        kotlin.d.b.j.a((Object) recyclerView3, "friendsList");
        recyclerView3.setLayoutManager(new LinearLayoutManager(getContext()));
        RecyclerView recyclerView4 = (RecyclerView) a(R.id.friendsList);
        kotlin.d.b.j.a((Object) recyclerView4, "friendsList");
        recyclerView4.setAdapter(aVar);
        e();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.p.a(java.lang.Iterable, int):int
     arg types: [java.util.ArrayList, int]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.p.a(java.lang.Iterable, int):int */
    public final void a(a aVar) {
        List<? extends r0> list = this.f743b;
        if (list != null) {
            ArrayList<a> arrayList = new ArrayList<>();
            for (r0 r0Var : list) {
                if (!(r0Var instanceof a)) {
                    r0Var = null;
                }
                a aVar2 = (a) r0Var;
                if (aVar2 != null) {
                    arrayList.add(aVar2);
                }
            }
            ArrayList arrayList2 = new ArrayList(kotlin.a.m.a((Iterable) arrayList, 10));
            for (a aVar3 : arrayList) {
                if (aVar3.a(aVar)) {
                    aVar3 = a.a(aVar3, null, null, null, null, new j.a.c(new Date()), 0, 47);
                }
                arrayList2.add(aVar3);
            }
            this.d.a(bb.f5389a);
        }
        nl.komponents.kovenant.c.m.b(SupercellId.INSTANCE.getSharedServices$supercellId_release().e().c(aVar.f742b), new h(new WeakReference(this), aVar));
    }
}
