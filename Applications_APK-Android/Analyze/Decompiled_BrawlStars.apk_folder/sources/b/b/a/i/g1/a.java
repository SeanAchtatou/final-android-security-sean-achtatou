package b.b.a.i.g1;

import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import b.b.a.i.b;
import b.b.a.i.h1.a;
import b.b.a.i.o1.p;
import b.b.a.i.x0;
import b.b.a.j.a0;
import b.b.a.j.q1;
import com.facebook.share.internal.ShareConstants;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class a extends b.b.a.i.g {
    public static final b n = new b(null);
    public HashMap m;

    /* renamed from: b.b.a.i.g1.a$a  reason: collision with other inner class name */
    public static final class C0020a extends b.a implements a0 {
        public static final Parcelable.Creator<C0020a> CREATOR = new C0021a();
        public static final b h = new b(null);
        public final boolean e;
        public final boolean f = true;
        public final Class<? extends b.b.a.i.g> g = a.class;

        /* renamed from: b.b.a.i.g1.a$a$a  reason: collision with other inner class name */
        public final class C0021a implements Parcelable.Creator<C0020a> {
            public final C0020a createFromParcel(Parcel parcel) {
                j.b(parcel, ShareConstants.FEED_SOURCE_PARAM);
                j.b(parcel, "parcel");
                return new C0020a();
            }

            public final C0020a[] newArray(int i) {
                return new C0020a[i];
            }
        }

        /* renamed from: b.b.a.i.g1.a$a$b */
        public static final class b {
            public /* synthetic */ b(kotlin.d.b.g gVar) {
            }

            public final int a(int i, int i2, int i3) {
                int a2 = kotlin.e.a.a(b.b.a.b.a(65));
                int i4 = (i - i2) - i3;
                int a3 = i4 - kotlin.e.a.a(b.b.a.b.a(240));
                int i5 = a2 + ((i4 - a2) / 2);
                int i6 = (i4 * 2) / 3;
                if (j.a(a3, i5) < 0) {
                    i6 = i5;
                } else if (j.a(a3, i6) <= 0) {
                    i6 = a3;
                }
                return i2 + i6;
            }
        }

        public final Class<? extends b.b.a.i.g> a() {
            return this.g;
        }

        public final Class<? extends x0> a(Resources resources) {
            j.b(resources, "resources");
            return b.b.a.b.b(resources) ? super.a(resources) : c.class;
        }

        public final int b(Resources resources, int i, int i2, int i3) {
            j.b(resources, "resources");
            return h.a(i, i2, i3);
        }

        public final boolean d() {
            return this.f;
        }

        public final boolean d(Resources resources) {
            j.b(resources, "resources");
            return true;
        }

        public final int describeContents() {
            return 0;
        }

        public final Class<? extends b.b.a.i.g> e(Resources resources) {
            j.b(resources, "resources");
            return d.class;
        }

        public final boolean e() {
            return this.e;
        }

        public final void writeToParcel(Parcel parcel, int i) {
            j.b(parcel, "dest");
        }
    }

    public static final class b {
        public /* synthetic */ b(kotlin.d.b.g gVar) {
        }

        public final b.a a() {
            return SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getSocialFeatureEnabled() ? new a.b(false) : new a.c(false);
        }
    }

    public static final class c extends x0 {
        public HashMap r;

        /* renamed from: b.b.a.i.g1.a$c$a  reason: collision with other inner class name */
        public final class C0022a extends k implements kotlin.d.a.a<m> {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ View f268a;

            /* renamed from: b  reason: collision with root package name */
            public final /* synthetic */ c f269b;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0022a(View view, c cVar) {
                super(0);
                this.f268a = view;
                this.f269b = cVar;
            }

            public final Object invoke() {
                if (this.f269b.isAdded()) {
                    this.f268a.animate().alpha(1.0f).setStartDelay(900).setDuration(300).setInterpolator(b.b.a.f.a.h).start();
                }
                return m.f5330a;
            }
        }

        public final class b implements View.OnClickListener {
            public b() {
            }

            public final void onClick(View view) {
                MainActivity a2 = b.b.a.b.a((Fragment) c.this);
                if (a2 != null) {
                    a2.a(a.n.a());
                }
                b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Authentication", "click", "FAQ", null, false, 24);
            }
        }

        public final View a(int i) {
            if (this.r == null) {
                this.r = new HashMap();
            }
            View view = (View) this.r.get(Integer.valueOf(i));
            if (view != null) {
                return view;
            }
            View view2 = getView();
            if (view2 == null) {
                return null;
            }
            View findViewById = view2.findViewById(i);
            this.r.put(Integer.valueOf(i), findViewById);
            return findViewById;
        }

        public final void a() {
            HashMap hashMap = this.r;
            if (hashMap != null) {
                hashMap.clear();
            }
        }

        public final View c() {
            return (ImageButton) a(R.id.nav_area_close_button);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.content.res.Resources, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void c(View view) {
            j.b(view, "view");
            for (View view2 : kotlin.a.m.a((Object[]) new View[]{(ImageView) a(R.id.navAreaLogo), a(R.id.navAreaDivider), (Button) a(R.id.faqButton), a(R.id.faqButtonDivider)})) {
                view2.setAlpha(0.0f);
                q1.a(view2, new C0022a(view2, this));
            }
            View c = c();
            if (c != null) {
                Resources resources = getResources();
                j.a((Object) resources, "resources");
                q1.a(c, b.b.a.b.b(resources) ? 700 : 500);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
            j.b(layoutInflater, "inflater");
            return layoutInflater.inflate(R.layout.fragment_authentication_nav_area, viewGroup, false);
        }

        public final /* synthetic */ void onDestroyView() {
            super.onDestroyView();
            a();
        }

        public final void onViewCreated(View view, Bundle bundle) {
            j.b(view, "view");
            ((Button) a(R.id.faqButton)).setOnClickListener(new b());
            a(kotlin.a.m.d((ImageView) a(R.id.navAreaLogo), c(), a(R.id.navAreaDivider), (Button) a(R.id.faqButton), a(R.id.faqButtonDivider)));
            super.onViewCreated(view, bundle);
        }
    }

    public static final class d extends b.b.a.i.g {
        public HashMap m;

        public final void a() {
            HashMap hashMap = this.m;
            if (hashMap != null) {
                hashMap.clear();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
            j.b(layoutInflater, "inflater");
            return layoutInflater.inflate(R.layout.fragment_authentication_top_area, viewGroup, false);
        }

        public final /* synthetic */ void onDestroyView() {
            super.onDestroyView();
            a();
        }
    }

    public final class e implements View.OnClickListener {
        public e() {
        }

        public final void onClick(View view) {
            MainActivity a2 = b.b.a.b.a((Fragment) a.this);
            if (a2 != null) {
                a2.a(new p.a(null, null, null, 7));
            }
        }
    }

    public final class f extends k implements kotlin.d.a.b<String, m> {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ WeakReference f273b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(WeakReference weakReference) {
            super(1);
            this.f273b = weakReference;
        }

        public final Object invoke(Object obj) {
            String str = (String) obj;
            j.b(str, "descriptionText");
            SupercellId.INSTANCE.getSharedServices$supercellId_release().j.a("start_register_btn", new b(this, str));
            return m.f5330a;
        }
    }

    public final class g implements View.OnClickListener {
        public g() {
        }

        public final void onClick(View view) {
            MainActivity a2 = b.b.a.b.a((Fragment) a.this);
            if (a2 != null) {
                a2.a(a.n.a());
            }
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Authentication", "click", "FAQ", null, false, 24);
        }
    }

    public final View a(int i) {
        if (this.m == null) {
            this.m = new HashMap();
        }
        View view = (View) this.m.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.m.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_authentication, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    public final void onResume() {
        super.onResume();
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a("Authentication");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        j.b(view, "view");
        super.onViewCreated(view, bundle);
        ((Button) a(R.id.loginButton)).setOnClickListener(new e());
        TextView textView = (TextView) a(R.id.register);
        j.a((Object) textView, "register");
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        TextView textView2 = (TextView) a(R.id.register);
        j.a((Object) textView2, "register");
        textView2.setHighlightColor(0);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().j.a("start_register_description", new f(new WeakReference((TextView) a(R.id.register))));
        Button button = (Button) a(R.id.inlineFaqButton);
        if (button != null) {
            button.setOnClickListener(new g());
        }
    }
}
