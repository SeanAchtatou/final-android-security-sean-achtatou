package b.b.a.i.r1;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import b.b.a.h.n;
import b.b.a.i.f1;
import b.b.a.j.i0;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.ProgressBar;
import com.supercell.id.view.WidthAdjustingMultilineButton;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.a.ah;
import kotlin.a.ai;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.j.t;
import kotlin.m;

public final class c extends f1 {
    public static final Object d = new Object();
    public static final Object e = new Object();

    /* renamed from: b  reason: collision with root package name */
    public final kotlin.d.a.b<i0, m> f585b = new a();
    public HashMap c;

    public final class a extends k implements kotlin.d.a.b<i0, m> {
        public a() {
            super(1);
        }

        public final Object invoke(Object obj) {
            b.b.a.b.a(new b(this, (i0) obj));
            return m.f5330a;
        }
    }

    public final class b implements View.OnClickListener {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ String f587a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ c f588b;
        public final /* synthetic */ n c;

        public b(String str, View view, c cVar, n nVar, b.b.a.h.b bVar, int i) {
            this.f587a = str;
            this.f588b = cVar;
            this.c = nVar;
        }

        public final void onClick(View view) {
            MainActivity a2 = b.b.a.b.a(this.f588b);
            if (a2 != null) {
                a2.a(this.f587a);
            }
            b.b.a.c.b bVar = SupercellId.INSTANCE.getSharedServices$supercellId_release().f;
            StringBuilder a3 = b.a.a.a.a.a("Install ");
            a3.append(b.b.a.b.a(this.c));
            b.b.a.c.b.a(bVar, "Connected Games", "click", a3.toString(), null, false, 24);
        }
    }

    /* renamed from: b.b.a.i.r1.c$c  reason: collision with other inner class name */
    public final class C0055c implements View.OnClickListener {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ n f590b;

        public C0055c(n nVar, b.b.a.h.b bVar, int i) {
            this.f590b = nVar;
        }

        public final void onClick(View view) {
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Connected Games", "click", b.b.a.b.a(this.f590b), null, false, 24);
            MainActivity a2 = b.b.a.b.a(c.this);
            if (a2 != null) {
                a2.a(this.f590b.c);
            }
        }
    }

    public final class d extends k implements kotlin.d.a.b<String, String> {

        /* renamed from: a  reason: collision with root package name */
        public static final d f591a = new d();

        public d() {
            super(1);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final Object invoke(Object obj) {
            String str = (String) obj;
            j.b(str, "it");
            String upperCase = str.toUpperCase(SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getLocale());
            j.a((Object) upperCase, "(this as java.lang.String).toUpperCase(locale)");
            return upperCase;
        }
    }

    public final class e extends k implements kotlin.d.a.c<Drawable, b.b.a.i.x1.c, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ WeakReference f592a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(WeakReference weakReference) {
            super(2);
            this.f592a = weakReference;
        }

        public final Object invoke(Object obj, Object obj2) {
            ImageView imageView;
            Drawable drawable = (Drawable) obj;
            b.b.a.i.x1.c cVar = (b.b.a.i.x1.c) obj2;
            j.b(drawable, "drawable");
            j.b(cVar, "assetLocation");
            View view = (View) this.f592a.get();
            if (!(view == null || (imageView = (ImageView) view.findViewById(R.id.systemImageView)) == null)) {
                imageView.setImageDrawable(drawable);
                if (cVar == b.b.a.i.x1.c.EXTERNAL) {
                    imageView.setAlpha(0.0f);
                    imageView.animate().alpha(1.0f).setDuration(300).start();
                }
            }
            return m.f5330a;
        }
    }

    public final class f<T> implements Comparator<n> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ Map f593a;

        public f(Map map) {
            this.f593a = map;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [b.b.a.h.n, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* renamed from: a */
        public final int compare(n nVar, n nVar2) {
            boolean containsKey = this.f593a.containsKey(nVar.f135a);
            boolean containsKey2 = this.f593a.containsKey(nVar2.f135a);
            if (containsKey && !containsKey2) {
                return -1;
            }
            if (!containsKey && containsKey2) {
                return 1;
            }
            j.a((Object) nVar, "o1");
            String b2 = b.b.a.b.b(nVar);
            j.a((Object) nVar2, "o2");
            return t.c(b2, b.b.a.b.b(nVar2), true);
        }
    }

    public final View a(int i) {
        if (this.c == null) {
            this.c = new HashMap();
        }
        View view = (View) this.c.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.c.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.p.a(java.lang.Iterable, int):int
     arg types: [java.util.List<java.lang.String>, int]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.p.a(java.lang.Iterable, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.ImageView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.WidthAdjustingMultilineButton, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.LinearLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final View a(View view, n nVar, b.b.a.h.b bVar, int i) {
        View view2 = view;
        n nVar2 = nVar;
        b.b.a.h.b bVar2 = bVar;
        int i2 = i;
        TextView textView = (TextView) view2.findViewById(R.id.systemNameLabel);
        j.a((Object) textView, "systemNameLabel");
        b.b.a.i.x1.j.a(textView, "game_name_" + nVar2.f135a, (kotlin.d.a.b) null, 2);
        boolean z = true;
        if (bVar2 != null) {
            TextView textView2 = (TextView) view2.findViewById(R.id.systemNicknameLabel);
            j.a((Object) textView2, "systemNicknameLabel");
            textView2.setText(bVar2.f109b);
            TextView textView3 = (TextView) view2.findViewById(R.id.systemNicknameLabel);
            j.a((Object) textView3, "systemNicknameLabel");
            String str = bVar2.f109b;
            textView3.setVisibility(str == null || t.a(str) ? 8 : 0);
            List<String> list = bVar2.c;
            if (list != null) {
                ArrayList arrayList = new ArrayList(kotlin.a.m.a((Iterable) list, 10));
                int i3 = 0;
                for (T next : list) {
                    int i4 = i3 + 1;
                    if (i3 < 0) {
                        kotlin.a.m.a();
                    }
                    arrayList.add(kotlin.k.a(String.valueOf(i4), (String) next));
                    i3 = i4;
                }
                Map a2 = ai.a(arrayList);
                TextView textView4 = (TextView) view2.findViewById(R.id.systemLevelLabel);
                j.a((Object) textView4, "systemLevelLabel");
                b.b.a.i.x1.j.a(textView4, "player_level_info_" + nVar2.f135a, a2);
                TextView textView5 = (TextView) view2.findViewById(R.id.systemLevelLabel);
                j.a((Object) textView5, "systemLevelLabel");
                textView5.setVisibility(0);
            } else {
                TextView textView6 = (TextView) view2.findViewById(R.id.systemLevelLabel);
                j.a((Object) textView6, "systemLevelLabel");
                textView6.setText((CharSequence) null);
                TextView textView7 = (TextView) view2.findViewById(R.id.systemLevelLabel);
                j.a((Object) textView7, "systemLevelLabel");
                textView7.setVisibility(8);
            }
            ImageView imageView = (ImageView) view2.findViewById(R.id.checkmark);
            j.a((Object) imageView, "checkmark");
            imageView.setVisibility(0);
            WidthAdjustingMultilineButton widthAdjustingMultilineButton = (WidthAdjustingMultilineButton) view2.findViewById(R.id.installButton);
            j.a((Object) widthAdjustingMultilineButton, "installButton");
            widthAdjustingMultilineButton.setVisibility(8);
        } else {
            TextView textView8 = (TextView) view2.findViewById(R.id.systemNicknameLabel);
            j.a((Object) textView8, "systemNicknameLabel");
            b.b.a.i.x1.j.a(textView8, "account_games_info_not_connected", d.f591a);
            TextView textView9 = (TextView) view2.findViewById(R.id.systemNicknameLabel);
            j.a((Object) textView9, "systemNicknameLabel");
            textView9.setVisibility(0);
            TextView textView10 = (TextView) view2.findViewById(R.id.systemLevelLabel);
            j.a((Object) textView10, "systemLevelLabel");
            textView10.setVisibility(8);
            ImageView imageView2 = (ImageView) view2.findViewById(R.id.checkmark);
            j.a((Object) imageView2, "checkmark");
            imageView2.setVisibility(8);
            WidthAdjustingMultilineButton widthAdjustingMultilineButton2 = (WidthAdjustingMultilineButton) view2.findViewById(R.id.installButton);
            j.a((Object) widthAdjustingMultilineButton2, "installButton");
            widthAdjustingMultilineButton2.setVisibility(0);
            String str2 = nVar2.f136b;
            if (str2 != null) {
                ((WidthAdjustingMultilineButton) view2.findViewById(R.id.installButton)).setOnClickListener(new b(str2, view, this, nVar, bVar, i));
            }
        }
        LinearLayout linearLayout = (LinearLayout) view2.findViewById(R.id.systemRowView);
        j.a((Object) linearLayout, "systemRowView");
        if (bVar2 == null || nVar2.c == null) {
            z = false;
        }
        linearLayout.setEnabled(z);
        ViewCompat.setBackground((LinearLayout) view2.findViewById(R.id.systemRowView), ContextCompat.getDrawable(view.getContext(), i2));
        if (nVar2.c != null) {
            ((LinearLayout) view2.findViewById(R.id.systemRowView)).setOnClickListener(new C0055c(nVar2, bVar2, i2));
        } else {
            ((LinearLayout) view2.findViewById(R.id.systemRowView)).setOnClickListener(null);
        }
        LinearLayout linearLayout2 = (LinearLayout) view2.findViewById(R.id.systemRowView);
        j.a((Object) linearLayout2, "systemRowView");
        linearLayout2.setSoundEffectsEnabled(false);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().j.a(b.a.a.a.a.a(b.a.a.a.a.a("AppIcon_"), nVar2.f135a, ".png"), new e(new WeakReference(view2)));
        return view2;
    }

    public final void a() {
        HashMap hashMap = this.c;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public final void c() {
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a("Connected Games");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final List<View> e() {
        LinearLayout linearLayout = (LinearLayout) a(R.id.connectedGamesView);
        kotlin.g.c b2 = kotlin.g.d.b(0, linearLayout.getChildCount());
        ArrayList arrayList = new ArrayList();
        Iterator it = b2.iterator();
        while (it.hasNext()) {
            View childAt = linearLayout.getChildAt(((ah) it).a());
            j.a((Object) childAt, "it");
            if (!j.a(childAt.getTag(), d)) {
                childAt = null;
            }
            if (childAt != null) {
                arrayList.add(childAt);
            }
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final List<View> f() {
        LinearLayout linearLayout = (LinearLayout) a(R.id.connectedGamesView);
        kotlin.g.c b2 = kotlin.g.d.b(0, linearLayout.getChildCount());
        ArrayList arrayList = new ArrayList();
        Iterator it = b2.iterator();
        while (it.hasNext()) {
            View childAt = linearLayout.getChildAt(((ah) it).a());
            j.a((Object) childAt, "it");
            if (!j.a(childAt.getTag(), e)) {
                childAt = null;
            }
            if (childAt != null) {
                arrayList.add(childAt);
            }
        }
        return arrayList;
    }

    public final void g() {
        View view = new View(getContext());
        view.setTag(d);
        view.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.gray95));
        view.setLayoutParams(new ViewGroup.LayoutParams(-1, kotlin.e.a.a(b.b.a.b.a(1))));
        ((LinearLayout) a(R.id.connectedGamesView)).addView(view);
    }

    public final void onAttach(Context context) {
        super.onAttach(context);
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f().b(this.f585b);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_profile_connected_games, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    public final void onDetach() {
        super.onDetach();
        SupercellId.INSTANCE.getSharedServices$supercellId_release().f().c(this.f585b);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.LinearLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.ProgressBar, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        j.b(view, "view");
        LinearLayout linearLayout = (LinearLayout) a(R.id.connectedGamesView);
        j.a((Object) linearLayout, "connectedGamesView");
        linearLayout.setVisibility(4);
        ProgressBar progressBar = (ProgressBar) a(R.id.progressBar);
        j.a((Object) progressBar, "progressBar");
        progressBar.setVisibility(0);
        this.f585b.invoke(SupercellId.INSTANCE.getSharedServices$supercellId_release().f().f1179a);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.p.a(java.lang.Iterable, int):int
     arg types: [java.util.List<b.b.a.h.b>, int]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.p.a(java.lang.Iterable, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
     arg types: [java.util.List<b.b.a.h.n>, b.b.a.i.r1.c$f]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.LinearLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void a(List<b.b.a.h.b> list, List<n> list2) {
        View view;
        if (getView() != null) {
            List<View> e2 = e();
            List<View> f2 = f();
            LinkedHashMap linkedHashMap = new LinkedHashMap(kotlin.g.d.c(ai.a(kotlin.a.m.a((Iterable) list, 10)), 16));
            for (T next : list) {
                linkedHashMap.put(((b.b.a.h.b) next).f108a, next);
            }
            List a2 = kotlin.a.m.a((Iterable) list2, (Comparator) new f(linkedHashMap));
            int i = 0;
            for (Object next2 : a2) {
                int i2 = i + 1;
                if (i < 0) {
                    kotlin.a.m.a();
                }
                n nVar = (n) next2;
                if (i > 0) {
                    int i3 = i - 1;
                    if (i3 < 0 || i3 > kotlin.a.m.a((List) e2)) {
                        g();
                    } else {
                        e2.get(i3);
                    }
                }
                if (i < 0 || i > kotlin.a.m.a((List) f2)) {
                    LinearLayout linearLayout = (LinearLayout) a(R.id.connectedGamesView);
                    View inflate = LayoutInflater.from(linearLayout.getContext()).inflate(R.layout.fragment_profile_list_item_game, (ViewGroup) ((LinearLayout) a(R.id.connectedGamesView)), false);
                    inflate.setTag(e);
                    linearLayout.addView(inflate);
                    j.a((Object) inflate, "newSystemRow()");
                    view = inflate;
                } else {
                    view = f2.get(i);
                }
                View view2 = view;
                int size = a2.size() - 1;
                int i4 = i == 0 ? i == size ? R.drawable.list_button : R.drawable.list_button_top : i == size ? R.drawable.list_button_bottom : R.drawable.list_button_middle;
                j.a((Object) view2, "view");
                a(view2, nVar, (b.b.a.h.b) linkedHashMap.get(nVar.f135a), i4);
                i = i2;
            }
            for (View removeView : kotlin.a.m.c(e2, Math.max(0, a2.size() - 1))) {
                ((LinearLayout) a(R.id.connectedGamesView)).removeView(removeView);
            }
            for (View removeView2 : kotlin.a.m.c(f2, a2.size())) {
                ((LinearLayout) a(R.id.connectedGamesView)).removeView(removeView2);
            }
        }
    }
}
