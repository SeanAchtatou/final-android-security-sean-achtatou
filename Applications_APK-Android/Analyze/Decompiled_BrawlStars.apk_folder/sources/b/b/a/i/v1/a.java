package b.b.a.i.v1;

import b.b.a.h.g;
import b.b.a.h.j;
import b.b.a.j.r0;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.supercell.id.R;

public final class a implements r0 {

    /* renamed from: a  reason: collision with root package name */
    public final int f741a = R.layout.fragment_public_profile_list_item_friend;

    /* renamed from: b  reason: collision with root package name */
    public final String f742b;
    public final String c;
    public final String d;
    public final g e;
    public final j f;
    public final int g;

    public a(String str, String str2, String str3, g gVar, j jVar, int i) {
        kotlin.d.b.j.b(str, "scid");
        kotlin.d.b.j.b(jVar, "relationship");
        this.f742b = str;
        this.c = str2;
        this.d = str3;
        this.e = gVar;
        this.f = jVar;
        this.g = i;
    }

    public static /* synthetic */ a a(a aVar, String str, String str2, String str3, g gVar, j jVar, int i, int i2) {
        if ((i2 & 1) != 0) {
            str = aVar.f742b;
        }
        String str4 = str;
        if ((i2 & 2) != 0) {
            str2 = aVar.c;
        }
        String str5 = str2;
        if ((i2 & 4) != 0) {
            str3 = aVar.d;
        }
        String str6 = str3;
        if ((i2 & 8) != 0) {
            gVar = aVar.e;
        }
        g gVar2 = gVar;
        if ((i2 & 16) != 0) {
            jVar = aVar.f;
        }
        j jVar2 = jVar;
        if ((i2 & 32) != 0) {
            i = aVar.g;
        }
        return aVar.a(str4, str5, str6, gVar2, jVar2, i);
    }

    public final int a() {
        return this.f741a;
    }

    public final a a(String str, String str2, String str3, g gVar, j jVar, int i) {
        kotlin.d.b.j.b(str, "scid");
        kotlin.d.b.j.b(jVar, "relationship");
        return new a(str, str2, str3, gVar, jVar, i);
    }

    public final boolean a(r0 r0Var) {
        kotlin.d.b.j.b(r0Var, FacebookRequestErrorClassification.KEY_OTHER);
        return (r0Var instanceof a) && kotlin.d.b.j.a(((a) r0Var).f742b, this.f742b);
    }

    public final boolean b(r0 r0Var) {
        kotlin.d.b.j.b(r0Var, FacebookRequestErrorClassification.KEY_OTHER);
        if (!(r0Var instanceof a)) {
            return false;
        }
        a aVar = (a) r0Var;
        return kotlin.d.b.j.a(this.c, aVar.c) && kotlin.d.b.j.a(this.f.getClass(), aVar.f.getClass()) && this.g == aVar.g;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof a) {
                a aVar = (a) obj;
                if (kotlin.d.b.j.a((Object) this.f742b, (Object) aVar.f742b) && kotlin.d.b.j.a((Object) this.c, (Object) aVar.c) && kotlin.d.b.j.a((Object) this.d, (Object) aVar.d) && kotlin.d.b.j.a(this.e, aVar.e) && kotlin.d.b.j.a(this.f, aVar.f)) {
                    if (this.g == aVar.g) {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        String str = this.f742b;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.c;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.d;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        g gVar = this.e;
        int hashCode4 = (hashCode3 + (gVar != null ? gVar.hashCode() : 0)) * 31;
        j jVar = this.f;
        if (jVar != null) {
            i = jVar.hashCode();
        }
        return ((hashCode4 + i) * 31) + this.g;
    }

    public final String toString() {
        StringBuilder a2 = b.a.a.a.a.a("FriendRow(scid=");
        a2.append(this.f742b);
        a2.append(", name=");
        a2.append(this.c);
        a2.append(", avatarUrl=");
        a2.append(this.d);
        a2.append(", presence=");
        a2.append(this.e);
        a2.append(", relationship=");
        a2.append(this.f);
        a2.append(", mutualFriends=");
        a2.append(this.g);
        a2.append(")");
        return a2.toString();
    }
}
