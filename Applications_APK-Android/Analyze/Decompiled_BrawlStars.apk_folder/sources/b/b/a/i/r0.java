package b.b.a.i;

import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import b.b.a.i.b;
import com.supercell.id.R;
import com.supercell.id.ui.MainActivity;
import java.util.HashMap;
import kotlin.d.b.g;
import kotlin.d.b.j;

public final class r0 extends g {
    public HashMap m;

    public static final class a extends b.a {
        public static final C0054a CREATOR = new C0054a(null);
        public final boolean e;
        public final Class<? extends g> f = r0.class;

        /* renamed from: b.b.a.i.r0$a$a  reason: collision with other inner class name */
        public static final class C0054a implements Parcelable.Creator<a> {
            public /* synthetic */ C0054a(g gVar) {
            }

            public final Object createFromParcel(Parcel parcel) {
                j.b(parcel, "parcel");
                return new a();
            }

            public final Object[] newArray(int i) {
                return new a[i];
            }
        }

        public final Class<? extends g> a() {
            return this.f;
        }

        public final int b(Resources resources, int i, int i2, int i3) {
            j.b(resources, "resources");
            float f2 = (float) ((i - i2) - i3);
            float a2 = f2 - b.b.a.b.a(410);
            float a3 = b.b.a.b.a(70);
            float f3 = f2 / 3.0f;
            if (Float.compare(a2, a3) < 0) {
                f3 = a3;
            } else if (Float.compare(a2, f3) <= 0) {
                f3 = a2;
            }
            return i2 + kotlin.e.a.a(f3);
        }

        public final boolean d(Resources resources) {
            j.b(resources, "resources");
            return true;
        }

        public final int describeContents() {
            return 0;
        }

        public final Class<? extends g> e(Resources resources) {
            j.b(resources, "resources");
            return b.class;
        }

        public final boolean e() {
            return this.e;
        }

        public final void writeToParcel(Parcel parcel, int i) {
        }
    }

    public static final class b extends g {
        public HashMap m;

        public final void a() {
            HashMap hashMap = this.m;
            if (hashMap != null) {
                hashMap.clear();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
            j.b(layoutInflater, "inflater");
            return layoutInflater.inflate(R.layout.fragment_maintenance_mode_top_area, viewGroup, false);
        }

        public final /* synthetic */ void onDestroyView() {
            super.onDestroyView();
            a();
        }
    }

    public final class c implements View.OnClickListener {
        public c() {
        }

        public final void onClick(View view) {
            MainActivity a2 = b.b.a.b.a((Fragment) r0.this);
            if (a2 != null) {
                a2.e();
            }
        }
    }

    public final void a() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_maintenance_mode, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    public final void onViewCreated(View view, Bundle bundle) {
        j.b(view, "view");
        super.onViewCreated(view, bundle);
        int i = R.id.button;
        if (this.m == null) {
            this.m = new HashMap();
        }
        View view2 = (View) this.m.get(Integer.valueOf(i));
        if (view2 == null) {
            View view3 = getView();
            if (view3 == null) {
                view2 = null;
            } else {
                view2 = view3.findViewById(i);
                this.m.put(Integer.valueOf(i), view2);
            }
        }
        ((Button) view2).setOnClickListener(new c());
    }
}
