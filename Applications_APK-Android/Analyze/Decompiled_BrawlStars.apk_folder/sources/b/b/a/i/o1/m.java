package b.b.a.i.o1;

import b.b.a.i.e;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import java.lang.ref.WeakReference;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.d.b.k;

public final class m extends k implements b<Boolean, kotlin.m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f454a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ String f455b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m(WeakReference weakReference, String str) {
        super(1);
        this.f454a = weakReference;
        this.f455b = str;
    }

    public final Object invoke(Object obj) {
        boolean booleanValue = ((Boolean) obj).booleanValue();
        o oVar = (o) this.f454a.get();
        if (oVar != null) {
            String str = this.f455b;
            j.b(str, "value");
            p h = oVar.h();
            if (h != null) {
                h.p = str;
            }
            p h2 = oVar.h();
            if (h2 != null) {
                h2.q = booleanValue;
            }
            SupercellId.INSTANCE.clearPendingLogin$supercellId_release();
            if (booleanValue || SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getHasGameAccountToken()) {
                p h3 = oVar.h();
                if (h3 != null) {
                    h3.k();
                }
            } else {
                MainActivity a2 = b.b.a.b.a(oVar);
                if (a2 != null) {
                    a2.a("binding_not_found", (b<? super e, kotlin.m>) null);
                }
            }
        }
        return kotlin.m.f5330a;
    }
}
