package b.b.a.i.s1;

import b.b.a.h.m;
import com.supercell.id.SupercellId;
import kotlin.d.a.a;
import kotlin.d.b.k;
import nl.komponents.kovenant.bw;

public final class r extends k implements a<bw<? extends m.b, ? extends Exception>> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ String f700a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ boolean f701b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public r(String str, boolean z) {
        super(0);
        this.f700a = str;
        this.f701b = z;
    }

    public final Object invoke() {
        return SupercellId.INSTANCE.getSharedServices$supercellId_release().g.a(this.f700a, this.f701b);
    }
}
