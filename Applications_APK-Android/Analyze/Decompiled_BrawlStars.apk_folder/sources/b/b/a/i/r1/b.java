package b.b.a.i.r1;

import android.widget.LinearLayout;
import b.b.a.i.r1.c;
import b.b.a.j.i0;
import com.supercell.id.R;
import com.supercell.id.view.ProgressBar;
import kotlin.d.a.a;
import kotlin.d.b.k;
import kotlin.m;

public final class b extends k implements a<m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ c.a f583a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ i0 f584b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b(c.a aVar, i0 i0Var) {
        super(0);
        this.f583a = aVar;
        this.f584b = i0Var;
    }

    public final Object invoke() {
        i0 i0Var = this.f584b;
        if (i0Var == null) {
            LinearLayout linearLayout = (LinearLayout) c.this.a(R.id.connectedGamesView);
            if (linearLayout != null) {
                linearLayout.setVisibility(4);
            }
            ProgressBar progressBar = (ProgressBar) c.this.a(R.id.progressBar);
            if (progressBar != null) {
                progressBar.setVisibility(0);
            }
        } else {
            c.this.a(i0Var.a().j, this.f584b.a().i);
            LinearLayout linearLayout2 = (LinearLayout) c.this.a(R.id.connectedGamesView);
            if (linearLayout2 != null) {
                linearLayout2.setVisibility(0);
            }
            ProgressBar progressBar2 = (ProgressBar) c.this.a(R.id.progressBar);
            if (progressBar2 != null) {
                progressBar2.setVisibility(4);
            }
        }
        return m.f5330a;
    }
}
