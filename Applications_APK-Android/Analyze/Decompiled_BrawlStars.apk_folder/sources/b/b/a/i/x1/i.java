package b.b.a.i.x1;

import android.support.v7.widget.ActivityChooserView;
import android.util.Log;
import b.b.a.j.e0;
import b.b.a.j.q0;
import com.supercell.id.IdAccount;
import com.supercell.id.SupercellId;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import kotlin.a.ai;
import kotlin.a.an;
import kotlin.d.b.g;
import kotlin.d.b.h;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.d.b.t;
import kotlin.m;
import nl.komponents.kovenant.bb;
import org.json.JSONObject;

public final class i {
    public static final a f = new a(null);

    /* renamed from: a  reason: collision with root package name */
    public final String f934a;

    /* renamed from: b  reason: collision with root package name */
    public int f935b = -1;
    public Date c;
    public final Set<String> d = an.a((Object[]) new String[]{"supercell_logo_black.png", "supercell_id_logo_black.png", "tutorial_1.png", "tutorial_2.png", "tutorial_3.png", "tutorial_4.png", "generic_button_01.mp3", "supercell_id_logo_white.png", "supercell_logo_white.png"});
    public final Set<String> e = an.a((Object[]) new String[]{"supercell_id_logo_white.png", "loader_icon_1.png", "loader_icon_2.png", "loader_icon_3.png", "loader_icon_3_alt.png", "generic_button_01.mp3", "AppIcon", "tab_icon_face_blue.png", "tab_icon_face_red.png", "tab_icon_face_disabled.png", "tab_icon_star.png", "tab_icon_sword.png", "tab_icon_star_disabled.png", "tab_icon_sword_disabled.png", "AccountIcon.png"});

    public static final class a {

        /* renamed from: b.b.a.i.x1.i$a$a  reason: collision with other inner class name */
        public final /* synthetic */ class C0094a extends h implements kotlin.d.a.b<InputStream, byte[]> {

            /* renamed from: a  reason: collision with root package name */
            public static final C0094a f936a = new C0094a();

            public C0094a() {
                super(1);
            }

            public final String getName() {
                return "readBytes";
            }

            public final kotlin.h.d getOwner() {
                return t.a(kotlin.io.a.class, "supercellId_release");
            }

            public final String getSignature() {
                return "readBytes(Ljava/io/InputStream;)[B";
            }

            public final Object invoke(Object obj) {
                InputStream inputStream = (InputStream) obj;
                j.b(inputStream, "p1");
                return kotlin.io.a.a(inputStream);
            }
        }

        public final class b extends k implements kotlin.d.a.b<byte[], m> {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ kotlin.d.a.b f937a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(kotlin.d.a.b bVar) {
                super(1);
                this.f937a = bVar;
            }

            public final Object invoke(Object obj) {
                byte[] bArr = (byte[]) obj;
                j.b(bArr, "it");
                this.f937a.invoke(bArr);
                return m.f5330a;
            }
        }

        public final class c extends k implements kotlin.d.a.b<Exception, m> {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ kotlin.d.a.b f938a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(kotlin.d.a.b bVar) {
                super(1);
                this.f938a = bVar;
            }

            public final Object invoke(Object obj) {
                Exception exc = (Exception) obj;
                j.b(exc, "it");
                i.f.getClass().getSimpleName();
                StringBuilder a2 = b.a.a.a.a.a("Asset fetch error: ");
                a2.append(Log.getStackTraceString(exc));
                a2.toString();
                this.f938a.invoke(null);
                return m.f5330a;
            }
        }

        public /* synthetic */ a(g gVar) {
        }

        public final void a(String str, kotlin.d.a.b<? super byte[], m> bVar) {
            e0.f1030b.a(str, false, C0094a.f936a).a(new b(bVar)).b(new c(bVar));
        }
    }

    public final class b extends k implements kotlin.d.a.a<m> {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ String f940b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(String str) {
            super(0);
            this.f940b = str;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [java.io.InputStream, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX WARNING: Code restructure failed: missing block: B:10:0x006a, code lost:
            r2 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x006b, code lost:
            kotlin.io.b.a(r0, r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:0x006e, code lost:
            throw r2;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invoke() {
            /*
                r3 = this;
                r0 = 10000(0x2710, float:1.4013E-41)
                android.net.TrafficStats.setThreadStatsTag(r0)
                java.net.URL r0 = new java.net.URL
                java.lang.String r1 = r3.f940b
                r0.<init>(r1)
                java.net.URLConnection r0 = r0.openConnection()
                if (r0 == 0) goto L_0x006f
                javax.net.ssl.HttpsURLConnection r0 = (javax.net.ssl.HttpsURLConnection) r0
                r1 = 0
                r0.setUseCaches(r1)
                java.io.InputStream r0 = r0.getInputStream()
                java.lang.String r1 = "connection.inputStream"
                kotlin.d.b.j.a(r0, r1)
                java.nio.charset.Charset r1 = kotlin.j.d.f5307a
                java.io.InputStreamReader r2 = new java.io.InputStreamReader
                r2.<init>(r0, r1)
                java.io.BufferedReader r0 = new java.io.BufferedReader
                r1 = 8192(0x2000, float:1.14794E-41)
                r0.<init>(r2, r1)
                r1 = 0
                java.lang.String r2 = kotlin.io.m.a(r0)     // Catch:{ all -> 0x0068 }
                kotlin.io.b.a(r0, r1)
                org.json.JSONObject r0 = new org.json.JSONObject
                r0.<init>(r2)
                b.b.a.i.x1.i r1 = b.b.a.i.x1.i.this
                java.lang.Class r1 = r1.getClass()
                r1.getSimpleName()
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "Response "
                r1.append(r2)
                r1.append(r0)
                r1.toString()
                com.supercell.id.SupercellId r1 = com.supercell.id.SupercellId.INSTANCE
                b.b.a.j.x r1 = r1.getSharedServices$supercellId_release()
                b.b.a.i.x1.f r1 = r1.j
                r1.a(r0)
                b.b.a.i.x1.i r1 = b.b.a.i.x1.i.this
                r1.a(r0)
                kotlin.m r0 = kotlin.m.f5330a
                return r0
            L_0x0068:
                r1 = move-exception
                throw r1     // Catch:{ all -> 0x006a }
            L_0x006a:
                r2 = move-exception
                kotlin.io.b.a(r0, r1)
                throw r2
            L_0x006f:
                kotlin.TypeCastException r0 = new kotlin.TypeCastException
                java.lang.String r1 = "null cannot be cast to non-null type javax.net.ssl.HttpsURLConnection"
                r0.<init>(r1)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: b.b.a.i.x1.i.b.invoke():java.lang.Object");
        }
    }

    public final class c extends k implements kotlin.d.a.b<m, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ kotlin.d.a.b f941a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(kotlin.d.a.b bVar) {
            super(1);
            this.f941a = bVar;
        }

        public final Object invoke(Object obj) {
            j.b((m) obj, "it");
            this.f941a.invoke(true);
            return m.f5330a;
        }
    }

    public final class d extends k implements kotlin.d.a.b<Exception, m> {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ kotlin.d.a.b f943b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(kotlin.d.a.b bVar) {
            super(1);
            this.f943b = bVar;
        }

        public final Object invoke(Object obj) {
            Exception exc = (Exception) obj;
            j.b(exc, "it");
            i.this.getClass().getSimpleName();
            StringBuilder a2 = b.a.a.a.a.a("Error ");
            a2.append(Log.getStackTraceString(exc));
            a2.toString();
            i.this.c = null;
            this.f943b.invoke(false);
            return m.f5330a;
        }
    }

    public final class e extends k implements kotlin.d.a.b<byte[], m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ String f944a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ String f945b;
        public final /* synthetic */ i c;
        public final /* synthetic */ kotlin.d.a.b d;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(String str, String str2, i iVar, kotlin.d.a.b bVar) {
            super(1);
            this.f944a = str;
            this.f945b = str2;
            this.c = iVar;
            this.d = bVar;
        }

        public final Object invoke(Object obj) {
            byte[] bArr = (byte[]) obj;
            if (bArr == null) {
                this.c.getClass().getSimpleName();
                StringBuilder a2 = b.a.a.a.a.a("Data loading error: ");
                a2.append(this.f944a);
                a2.toString();
            } else {
                SupercellId.INSTANCE.getSharedServices$supercellId_release().j.a(bArr, this.f945b);
            }
            i iVar = this.c;
            iVar.f935b = iVar.f935b - 1;
            if (iVar.f935b <= 0) {
                this.c.f935b = -1;
                this.d.invoke(true);
            }
            return m.f5330a;
        }
    }

    public i(String str) {
        j.b(str, "url");
        this.f934a = kotlin.j.t.b(str, '/');
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Calendar, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void a(kotlin.d.a.b<? super Boolean, m> bVar) {
        j.b(bVar, "callback");
        Calendar instance = Calendar.getInstance();
        j.a((Object) instance, "Calendar.getInstance()");
        Date time = instance.getTime();
        Date date = this.c;
        if (date != null) {
            if (date == null) {
                j.a();
            }
            if (time.before(new Date(date.getTime() + 300000))) {
                bVar.invoke(false);
                return;
            }
        }
        this.c = time;
        String a2 = b.a.a.a.a.a(new StringBuilder(), this.f934a, "/AssetManifest.json");
        "GET " + a2;
        bb.f5389a.a(new c(bVar)).b(new d(bVar));
    }

    public final void a(JSONObject jSONObject) {
        JSONObject optJSONObject = jSONObject.optJSONObject("conf");
        if (optJSONObject != null) {
            SupercellId.INSTANCE.setRemoteConfiguration$supercellId_release(q0.l.b(optJSONObject));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
     arg types: [java.util.List<java.lang.String>, b.b.a.i.x1.i$f]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T> */
    public final void b(kotlin.d.a.b<? super Boolean, m> bVar) {
        Set<String> set;
        j.b(bVar, "callback");
        if (this.f935b > 0) {
            bVar.invoke(false);
            return;
        }
        IdAccount idAccount = SupercellId.INSTANCE.getSharedServices$supercellId_release().i;
        Set b2 = an.b(an.a("Localizations"), SupercellId.INSTANCE.getSharedServices$supercellId_release().j.e.a());
        if (idAccount == null || !idAccount.getCanShowProfile$supercellId_release()) {
            Set<String> set2 = this.d;
            String game = SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getGame();
            j.b(set2, "$this$plus");
            LinkedHashSet linkedHashSet = new LinkedHashSet(ai.a(set2.size() + 1));
            linkedHashSet.addAll(set2);
            linkedHashSet.add(game);
            set = linkedHashSet;
        } else {
            set = this.e;
        }
        List<String> a2 = kotlin.a.m.a((Iterable) SupercellId.INSTANCE.getSharedServices$supercellId_release().j.d(), (Comparator) new f(an.b(b2, set)));
        if (a2.isEmpty()) {
            bVar.invoke(true);
            return;
        }
        this.f935b = a2.size();
        for (String str : a2) {
            String str2 = this.f934a + '/' + kotlin.j.t.a(str, '/');
            "Start downloading " + str2;
            f.a(str2, new e(str2, str, this, bVar));
        }
    }

    public final class f<T> implements Comparator<String> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ Set f946a;

        public f(Set set) {
            this.f946a = set;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.j.ac.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean
         arg types: [java.lang.String, java.lang.String, int]
         candidates:
          kotlin.j.ab.a(java.lang.String, java.lang.String, boolean):boolean
          kotlin.j.y.a(java.lang.Appendable, java.lang.Object, kotlin.d.a.b):void
          kotlin.j.ac.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean */
        /* renamed from: a */
        public final int compare(String str, String str2) {
            Iterator it = this.f946a.iterator();
            int i = 0;
            while (true) {
                if (!it.hasNext()) {
                    i = -1;
                    break;
                }
                Object next = it.next();
                if (i < 0) {
                    kotlin.a.m.a();
                }
                j.a((Object) str, "o1");
                if (kotlin.j.t.a((CharSequence) str, (CharSequence) ((String) next), false)) {
                    break;
                }
                i++;
            }
            int i2 = ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
            if (i == -1) {
                i = ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
            }
            Iterator it2 = this.f946a.iterator();
            int i3 = 0;
            while (true) {
                if (!it2.hasNext()) {
                    i3 = -1;
                    break;
                }
                Object next2 = it2.next();
                if (i3 < 0) {
                    kotlin.a.m.a();
                }
                j.a((Object) str2, "o2");
                if (kotlin.j.t.a((CharSequence) str2, (CharSequence) ((String) next2), false)) {
                    break;
                }
                i3++;
            }
            if (i3 != -1) {
                i2 = i3;
            }
            return i - i2;
        }
    }
}
