package b.b.a.i.y1;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.net.UrlQuerySanitizer;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import b.b.a.i.a1;
import b.b.a.i.b;
import b.b.a.i.g;
import b.b.a.i.s;
import b.b.a.i.t;
import b.b.a.i.v1.j;
import b.b.a.i.x0;
import b.b.a.j.q1;
import com.facebook.internal.NativeProtocol;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.google.zxing.p;
import com.journeyapps.barcodescanner.BarcodeView;
import com.journeyapps.barcodescanner.x;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import kotlin.a.ad;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class a extends g {
    public static final b r = new b(null);
    public boolean m;
    public boolean n;
    public boolean o;
    public final d p = new d();
    public HashMap q;

    /* renamed from: b.b.a.i.y1.a$a  reason: collision with other inner class name */
    public static final class C0095a extends b.a {
        public static final C0096a CREATOR = new C0096a(null);
        public final Set<Integer> e = ad.f5212a;
        public final boolean f = true;
        public final boolean g = true;
        public final Class<? extends g> h = a.class;

        /* renamed from: b.b.a.i.y1.a$a$a  reason: collision with other inner class name */
        public static final class C0096a implements Parcelable.Creator<C0095a> {
            public /* synthetic */ C0096a(kotlin.d.b.g gVar) {
            }

            public final Object createFromParcel(Parcel parcel) {
                j.b(parcel, "parcel");
                return new C0095a();
            }

            public final Object[] newArray(int i) {
                return new C0095a[i];
            }
        }

        public final Class<? extends g> a() {
            return this.h;
        }

        public final Class<? extends x0> a(Resources resources) {
            j.b(resources, "resources");
            return c.class;
        }

        public final int b(Resources resources, int i, int i2, int i3) {
            j.b(resources, "resources");
            return 0;
        }

        public final int c(Resources resources, int i, int i2, int i3) {
            j.b(resources, "resources");
            return 0;
        }

        public final Set<Integer> c() {
            return this.e;
        }

        public final boolean c(Resources resources) {
            j.b(resources, "resources");
            return !b.b.a.b.b(resources);
        }

        public final int describeContents() {
            return 0;
        }

        public final Class<? extends g> e(Resources resources) {
            j.b(resources, "resources");
            return a1.class;
        }

        public final boolean e() {
            return this.g;
        }

        public final boolean f() {
            return this.f;
        }

        public final void writeToParcel(Parcel parcel, int i) {
        }
    }

    public static final class b {
        public /* synthetic */ b(kotlin.d.b.g gVar) {
        }

        public final String a(String str) {
            j.b(str, "universalLink");
            return new UrlQuerySanitizer(str).getValue("p");
        }
    }

    public static final class c extends x0 {
        public HashMap r;

        /* renamed from: b.b.a.i.y1.a$c$a  reason: collision with other inner class name */
        public final class C0097a extends k implements kotlin.d.a.a<m> {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ View f962a;

            /* renamed from: b  reason: collision with root package name */
            public final /* synthetic */ float f963b;
            public final /* synthetic */ c c;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0097a(View view, float f, c cVar) {
                super(0);
                this.f962a = view;
                this.f963b = f;
                this.c = cVar;
            }

            public final Object invoke() {
                if (this.c.isAdded()) {
                    this.f962a.animate().scaleX(this.f963b).scaleY(1.0f).setStartDelay(300).setDuration(300).setInterpolator(b.b.a.f.a.f62a).start();
                }
                return m.f5330a;
            }
        }

        public final void a() {
            HashMap hashMap = this.r;
            if (hashMap != null) {
                hashMap.clear();
            }
        }

        public final void a(View view, g.a aVar, boolean z) {
            j.b(view, "view");
            j.b(aVar, "animation");
            super.a(view, aVar, z);
            View b2 = b();
            if (b2 != null) {
                b2.setScaleX(0.0f);
                b2.setScaleY(0.0f);
                q1.a(b2, new C0097a(b2, (float) b2.getResources().getInteger(R.integer.locale_mirror_flip), this));
            }
        }

        public final View b() {
            int i = R.id.nav_area_back_button;
            if (this.r == null) {
                this.r = new HashMap();
            }
            View view = (View) this.r.get(Integer.valueOf(i));
            if (view == null) {
                View view2 = getView();
                if (view2 == null) {
                    view = null;
                } else {
                    view = view2.findViewById(i);
                    this.r.put(Integer.valueOf(i), view);
                }
            }
            return (ImageButton) view;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
            j.b(layoutInflater, "inflater");
            return layoutInflater.inflate(R.layout.fragment_scan_code_nav_area, viewGroup, false);
        }

        public final /* synthetic */ void onDestroyView() {
            super.onDestroyView();
            a();
        }

        public final void onViewCreated(View view, Bundle bundle) {
            j.b(view, "view");
            a(kotlin.a.m.b(b()));
            super.onViewCreated(view, bundle);
        }
    }

    public final class e extends k implements kotlin.d.a.c<t, s, m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ MainActivity f966a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ a f967b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(MainActivity mainActivity, a aVar) {
            super(2);
            this.f966a = mainActivity;
            this.f967b = aVar;
        }

        public final Object invoke(Object obj, Object obj2) {
            s sVar = (s) obj2;
            j.b((t) obj, "decisionDialogFragment");
            j.b(sVar, "decision");
            int i = b.f969a[sVar.ordinal()];
            if (i == 1) {
                this.f966a.d();
            } else if (i == 2) {
                MainActivity mainActivity = this.f966a;
                Intent intent = new Intent();
                intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
                intent.setData(Uri.fromParts("package", mainActivity.getPackageName(), null));
                mainActivity.startActivity(intent);
                this.f967b.m = false;
            }
            return m.f5330a;
        }
    }

    public final class f extends k implements kotlin.d.a.b<b.b.a.i.e, m> {
        public f() {
            super(1);
        }

        public final Object invoke(Object obj) {
            j.b((b.b.a.i.e) obj, "it");
            MainActivity a2 = b.b.a.b.a((Fragment) a.this);
            if (a2 != null) {
                a2.d();
            }
            return m.f5330a;
        }
    }

    public static final /* synthetic */ void a(a aVar) {
        BarcodeView barcodeView = (BarcodeView) aVar.a(R.id.camera_view);
        if (barcodeView != null) {
            barcodeView.a(aVar.p);
        }
    }

    public final View a(int i) {
        if (this.q == null) {
            this.q = new HashMap();
        }
        View view = (View) this.q.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.q.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public final void a() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_scan_code, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    public final void onPause() {
        if (this.o) {
            this.o = false;
            ((BarcodeView) a(R.id.camera_view)).c();
        }
        super.onPause();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.Context, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onResume() {
        super.onResume();
        Context context = getContext();
        if (context != null) {
            j.a((Object) context, "context ?: return");
            if (!b.b.a.b.a(context)) {
                MainActivity a2 = b.b.a.b.a((Fragment) this);
                if (a2 != null) {
                    a2.a(MessengerShareContentUtility.TEMPLATE_GENERIC_TYPE, new f());
                }
            } else if (ContextCompat.checkSelfPermission(context, "android.permission.CAMERA") != 0) {
                if (!this.m) {
                    this.m = true;
                    FragmentActivity activity = getActivity();
                    this.n = activity != null ? !ActivityCompat.shouldShowRequestPermissionRationale(activity, "android.permission.CAMERA") : false;
                    requestPermissions(new String[]{"android.permission.CAMERA"}, 100);
                }
            } else if (!this.o) {
                this.o = true;
                ((BarcodeView) a(R.id.camera_view)).d();
            }
            SupercellId.INSTANCE.getSharedServices$supercellId_release().f.a("Scan QR Code");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.journeyapps.barcodescanner.BarcodeView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        j.b(view, "view");
        super.onViewCreated(view, bundle);
        BarcodeView barcodeView = (BarcodeView) a(R.id.camera_view);
        j.a((Object) barcodeView, "camera_view");
        barcodeView.setDecoderFactory(new x(kotlin.a.m.a(com.google.zxing.a.QR_CODE)));
        BarcodeView barcodeView2 = (BarcodeView) a(R.id.camera_view);
        if (barcodeView2 != null) {
            barcodeView2.a(this.p);
        }
    }

    public final class d implements com.journeyapps.barcodescanner.a {

        /* renamed from: b.b.a.i.y1.a$d$a  reason: collision with other inner class name */
        public final class C0098a extends k implements kotlin.d.a.b<b.b.a.i.e, m> {
            public C0098a() {
                super(1);
            }

            public final Object invoke(Object obj) {
                j.b((b.b.a.i.e) obj, "it");
                a.a(a.this);
                return m.f5330a;
            }
        }

        public d() {
        }

        public final void possibleResultPoints(List<p> list) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void barcodeResult(com.journeyapps.barcodescanner.b bVar) {
            if (bVar != null) {
                b bVar2 = a.r;
                String b2 = bVar.b();
                j.a((Object) b2, "it.text");
                String a2 = bVar2.a(b2);
                if (a2 != null) {
                    MainActivity a3 = b.b.a.b.a((Fragment) a.this);
                    if (a3 != null) {
                        j.a aVar = new j.a(a2, null, null, null, null, null, true);
                        kotlin.d.b.j.b(aVar, "entry");
                        b.b.a.i.b bVar3 = a3.d;
                        if (bVar3 == null) {
                            kotlin.d.b.j.a("backStack");
                        }
                        bVar3.d(aVar);
                        return;
                    }
                    return;
                }
                MainActivity a4 = b.b.a.b.a((Fragment) a.this);
                if (a4 != null) {
                    a4.a("scan_code", new C0098a());
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    public final void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        kotlin.d.b.j.b(strArr, NativeProtocol.RESULT_ARGS_PERMISSIONS);
        kotlin.d.b.j.b(iArr, "grantResults");
        if (i == 100) {
            kotlin.d.b.j.b(strArr, "$this$firstOrNull");
            if (kotlin.d.b.j.a((Object) (strArr.length == 0 ? null : strArr[0]), (Object) "android.permission.CAMERA")) {
                kotlin.d.b.j.b(iArr, "$this$firstOrNull");
                Integer valueOf = iArr.length == 0 ? null : Integer.valueOf(iArr[0]);
                if (valueOf == null || valueOf.intValue() != 0) {
                    MainActivity a2 = b.b.a.b.a((Fragment) this);
                    if (a2 == null) {
                        return;
                    }
                    if (this.n) {
                        t a3 = t.g.a("account_scan_code_camera_denied_heading", "account_scan_code_camera_denied_ok", "account_scan_code_camera_denied_cancel", kotlin.k.a("game", a2.getApplicationInfo().loadLabel(a2.getPackageManager()).toString()));
                        a3.e = new e(a2, this);
                        a2.a(a3, "popupDialog");
                        return;
                    }
                    a2.d();
                } else if (!this.o) {
                    this.o = true;
                    ((BarcodeView) a(R.id.camera_view)).d();
                }
            }
        }
    }
}
