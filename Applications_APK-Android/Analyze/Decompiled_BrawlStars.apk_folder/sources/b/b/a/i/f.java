package b.b.a.i;

import android.animation.ValueAnimator;
import android.view.View;
import kotlin.d.b.j;

public final class f implements ValueAnimator.AnimatorUpdateListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ View f242a;

    public f(View view) {
        this.f242a = view;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.animation.ValueAnimator, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        View view = this.f242a;
        j.a((Object) valueAnimator, "animator");
        view.setElevation((1.0f - valueAnimator.getAnimatedFraction()) * 8.0f);
    }
}
