package b.b.a.i;

import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.ViewPropertyAnimator;
import b.b.a.b;
import com.supercell.id.R;
import com.supercell.id.ui.MainActivity;
import kotlin.d.a.a;
import kotlin.d.b.k;
import kotlin.m;

public final class o extends k implements a<m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ View f403a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ g f404b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public o(View view, g gVar) {
        super(0);
        this.f403a = view;
        this.f404b = gVar;
    }

    public final Object invoke() {
        View a2;
        if (this.f404b.isAdded()) {
            MainActivity a3 = b.a((Fragment) this.f404b);
            boolean z = false;
            if (!(a3 == null || (a2 = a3.a(R.id.bottom_area_dimmer)) == null)) {
                a2.setVisibility(0);
                a2.bringToFront();
                a2.setAlpha(0.0f);
                a2.animate().setStartDelay(0).setDuration(350).setInterpolator(b.b.a.f.a.f62a).alpha(1.0f);
            }
            this.f403a.bringToFront();
            if (ViewCompat.getLayoutDirection(this.f403a) == 1) {
                z = true;
            }
            float f = z ? -1.0f : 1.0f;
            View view = this.f403a;
            view.setTranslationX(f * ((float) view.getWidth()));
            this.f403a.setAlpha(1.0f);
            if (Build.VERSION.SDK_INT >= 21) {
                this.f403a.setElevation(0.0f);
            }
            ViewPropertyAnimator listener = this.f403a.animate().setStartDelay(0).setDuration(350).setInterpolator(b.b.a.f.a.f62a).translationX(0.0f).setListener(new m(this));
            if (Build.VERSION.SDK_INT >= 21) {
                listener.setUpdateListener(new n(this));
            }
            listener.start();
        }
        return m.f5330a;
    }
}
