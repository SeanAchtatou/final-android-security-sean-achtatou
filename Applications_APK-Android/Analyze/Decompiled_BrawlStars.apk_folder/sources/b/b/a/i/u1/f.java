package b.b.a.i.u1;

import android.widget.FrameLayout;
import android.widget.LinearLayout;
import com.facebook.internal.ServerProtocol;
import com.supercell.id.R;
import com.supercell.id.view.ExpandableFrameLayout;
import kotlin.d.a.c;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class f extends k implements c<Float, ExpandableFrameLayout.b, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ ExpandableFrameLayout f736a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ LinearLayout f737b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public f(ExpandableFrameLayout expandableFrameLayout, LinearLayout linearLayout) {
        super(2);
        this.f736a = expandableFrameLayout;
        this.f737b = linearLayout;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.FrameLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    public final Object invoke(Object obj, Object obj2) {
        float floatValue = ((Number) obj).floatValue();
        ExpandableFrameLayout.b bVar = (ExpandableFrameLayout.b) obj2;
        j.b(bVar, ServerProtocol.DIALOG_PARAM_STATE);
        FrameLayout frameLayout = (FrameLayout) this.f736a.a(R.id.profileAccountView);
        j.a((Object) frameLayout, "rowView.profileAccountView");
        frameLayout.setAlpha(Math.max(0.0f, (floatValue * 2.0f) - 1.0f));
        if (bVar == ExpandableFrameLayout.b.COLLAPSED) {
            this.f737b.removeView(this.f736a);
        }
        return m.f5330a;
    }
}
