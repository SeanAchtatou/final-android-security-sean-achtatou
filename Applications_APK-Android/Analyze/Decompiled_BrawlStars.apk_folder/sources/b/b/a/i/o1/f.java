package b.b.a.i.o1;

import com.facebook.internal.ServerProtocol;
import com.supercell.id.SupercellId;
import java.lang.ref.WeakReference;
import kotlin.d.a.b;
import kotlin.d.b.k;
import kotlin.m;

public final class f extends k implements b<Boolean, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f418a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ String f419b;
    public final /* synthetic */ boolean c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public f(WeakReference weakReference, String str, boolean z) {
        super(1);
        this.f418a = weakReference;
        this.f419b = str;
        this.c = z;
    }

    public final Object invoke(Object obj) {
        ((Boolean) obj).booleanValue();
        h hVar = (h) this.f418a.get();
        if (hVar != null) {
            SupercellId.INSTANCE.setPendingLoginWithEmail$supercellId_release(this.f419b, this.c);
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Remember me", "Selection", this.c ? ServerProtocol.DIALOG_RETURN_SCOPES_TRUE : "false", null, false, 24);
            hVar.b(this.f419b);
            hVar.a(this.c);
            p h = hVar.h();
            if (h != null) {
                h.k();
            }
        }
        return m.f5330a;
    }
}
