package b.b.a.i.k1;

import b.a.a.a.a;
import b.b.a.j.r0;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.supercell.id.R;
import kotlin.d.b.j;

public final class b implements r0 {

    /* renamed from: a  reason: collision with root package name */
    public final int f348a = R.layout.fragment_ingame_invite_to_play_list_item;

    /* renamed from: b  reason: collision with root package name */
    public final String f349b;
    public final String c;
    public final String d;
    public final boolean e;

    public b(String str, String str2, String str3, boolean z) {
        j.b(str, "scid");
        this.f349b = str;
        this.c = str2;
        this.d = str3;
        this.e = z;
    }

    public static /* synthetic */ b a(b bVar, String str, String str2, String str3, boolean z, int i) {
        if ((i & 1) != 0) {
            str = bVar.f349b;
        }
        if ((i & 2) != 0) {
            str2 = bVar.c;
        }
        if ((i & 4) != 0) {
            str3 = bVar.d;
        }
        if ((i & 8) != 0) {
            z = bVar.e;
        }
        return bVar.a(str, str2, str3, z);
    }

    public final int a() {
        return this.f348a;
    }

    public final b a(String str, String str2, String str3, boolean z) {
        j.b(str, "scid");
        return new b(str, str2, str3, z);
    }

    public final boolean a(r0 r0Var) {
        j.b(r0Var, FacebookRequestErrorClassification.KEY_OTHER);
        return (r0Var instanceof b) && j.a(((b) r0Var).f349b, this.f349b);
    }

    public final boolean b(r0 r0Var) {
        j.b(r0Var, FacebookRequestErrorClassification.KEY_OTHER);
        if (!(r0Var instanceof b)) {
            return false;
        }
        b bVar = (b) r0Var;
        return j.a(this.c, bVar.c) && j.a(this.d, bVar.d) && this.e == bVar.e;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof b) {
                b bVar = (b) obj;
                if (j.a((Object) this.f349b, (Object) bVar.f349b) && j.a((Object) this.c, (Object) bVar.c) && j.a((Object) this.d, (Object) bVar.d)) {
                    if (this.e == bVar.e) {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        String str = this.f349b;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.c;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.d;
        if (str3 != null) {
            i = str3.hashCode();
        }
        int i2 = (hashCode2 + i) * 31;
        boolean z = this.e;
        if (z) {
            z = true;
        }
        return i2 + (z ? 1 : 0);
    }

    public final String toString() {
        StringBuilder a2 = a.a("FriendRow(scid=");
        a2.append(this.f349b);
        a2.append(", nickname=");
        a2.append(this.c);
        a2.append(", avatarUrl=");
        a2.append(this.d);
        a2.append(", inviteSent=");
        a2.append(this.e);
        a2.append(")");
        return a2.toString();
    }
}
