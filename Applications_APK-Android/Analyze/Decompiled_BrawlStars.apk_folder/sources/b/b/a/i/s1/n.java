package b.b.a.i.s1;

import b.b.a.j.f0;
import java.lang.ref.WeakReference;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class n extends k implements b<Exception, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f683a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public n(WeakReference weakReference) {
        super(1);
        this.f683a = weakReference;
    }

    public final Object invoke(Object obj) {
        Exception exc = (Exception) obj;
        j.b(exc, "it");
        p pVar = (p) this.f683a.get();
        if (pVar != null) {
            pVar.c = f0.e.a(exc);
        }
        return m.f5330a;
    }
}
