package b.b.a.i;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.view.View;
import b.b.a.b;
import com.supercell.id.R;
import com.supercell.id.ui.MainActivity;

public final class m extends AnimatorListenerAdapter {

    /* renamed from: a  reason: collision with root package name */
    public boolean f374a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ o f375b;

    public m(o oVar) {
        this.f375b = oVar;
    }

    public final void onAnimationCancel(Animator animator) {
        this.f374a = true;
    }

    public final void onAnimationEnd(Animator animator) {
        View a2;
        if (!this.f374a) {
            MainActivity a3 = b.a((Fragment) this.f375b.f404b);
            if (!(a3 == null || (a2 = a3.a(R.id.bottom_area_dimmer)) == null)) {
                a2.setVisibility(4);
            }
            if (Build.VERSION.SDK_INT >= 21) {
                this.f375b.f403a.setElevation(0.0f);
            }
        }
    }
}
