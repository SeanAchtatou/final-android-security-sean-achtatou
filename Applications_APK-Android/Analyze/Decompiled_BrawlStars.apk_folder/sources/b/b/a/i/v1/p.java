package b.b.a.i.v1;

import android.support.v4.app.Fragment;
import b.b.a.h.j;
import b.b.a.i.e;
import b.b.a.j.y0;
import com.supercell.id.ui.MainActivity;
import java.lang.ref.WeakReference;
import kotlin.d.a.b;
import kotlin.d.b.k;
import kotlin.m;
import nl.komponents.kovenant.bb;
import nl.komponents.kovenant.bw;

public final class p extends k implements b<Exception, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f808a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ j f809b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public p(WeakReference weakReference, j jVar) {
        super(1);
        this.f808a = weakReference;
        this.f809b = jVar;
    }

    public final Object invoke(Object obj) {
        Object obj2 = this.f808a.get();
        if (obj2 != null) {
            Exception exc = (Exception) obj;
            j jVar = (j) obj2;
            MainActivity a2 = b.b.a.b.a((Fragment) jVar);
            if (a2 != null) {
                a2.a(exc, (b<? super e, m>) null);
            }
            y0<h> y0Var = jVar.p;
            bw.a aVar = bw.d;
            h hVar = jVar.q;
            y0Var.a(bb.f5389a);
        }
        return m.f5330a;
    }
}
