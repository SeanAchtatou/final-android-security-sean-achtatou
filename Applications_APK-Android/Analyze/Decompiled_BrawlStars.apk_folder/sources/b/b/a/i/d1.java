package b.b.a.i;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import b.b.a.e.a;
import b.b.a.j.h0;
import b.b.a.j.p0;
import b.b.a.j.q1;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.view.FastScroll;
import com.supercell.id.view.TouchInterceptingFrameLayout;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;
import kotlin.a.ab;
import kotlin.d.b.s;
import kotlin.d.b.t;

public final class d1 extends AppCompatDialogFragment {
    public static final a c = new a(null);

    /* renamed from: a  reason: collision with root package name */
    public f f202a;

    /* renamed from: b  reason: collision with root package name */
    public HashMap f203b;

    public static final class a {
        public /* synthetic */ a(kotlin.d.b.g gVar) {
        }

        public final d1 a(String str, String str2, int i, int i2) {
            kotlin.d.b.j.b(str2, "selectedRegion");
            d1 d1Var = new d1();
            Bundle bundle = new Bundle();
            bundle.putString("currentRegion", str);
            bundle.putString("selectedRegion", str2);
            bundle.putInt("width", i);
            bundle.putInt("height", i2);
            d1Var.setArguments(bundle);
            return d1Var;
        }
    }

    public final class b extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements b.b.a.k.b {
        public static final /* synthetic */ kotlin.h.h[] f;

        /* renamed from: a  reason: collision with root package name */
        public final kotlin.d f204a = kotlin.e.a(c.f210a);

        /* renamed from: b  reason: collision with root package name */
        public final kotlin.d f205b;
        public String c;
        public final String d;
        public final /* synthetic */ d1 e;

        public final class a extends kotlin.d.b.k implements kotlin.d.a.c<Drawable, b.b.a.i.x1.c, kotlin.m> {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ Context f206a;

            /* renamed from: b  reason: collision with root package name */
            public final /* synthetic */ b f207b;
            public final /* synthetic */ d c;
            public final /* synthetic */ e.d d;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(Context context, b bVar, d dVar, e.d dVar2) {
                super(2);
                this.f206a = context;
                this.f207b = bVar;
                this.c = dVar;
                this.d = dVar2;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
             arg types: [android.content.res.Resources, java.lang.String]
             candidates:
              kotlin.d.b.j.a(int, int):int
              kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
              kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
              kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
             arg types: [android.widget.ImageView, java.lang.String]
             candidates:
              kotlin.d.b.j.a(int, int):int
              kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
              kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
              kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
            public final Object invoke(Object obj, Object obj2) {
                Drawable drawable = (Drawable) obj;
                kotlin.d.b.j.b(drawable, "drawable");
                kotlin.d.b.j.b((b.b.a.i.x1.c) obj2, "<anonymous parameter 1>");
                ImageView a2 = this.c.a();
                b.b.a.j.l lVar = b.b.a.j.l.f1088a;
                String str = this.d.f217a.f1120b;
                Resources resources = this.f207b.e.getResources();
                kotlin.d.b.j.a((Object) resources, "resources");
                a2.setImageDrawable(lVar.a(str, drawable, resources));
                ImageView a3 = this.c.a();
                kotlin.d.b.j.a((Object) a3, "holder.flag");
                b.b.a.b.a(a3, ContextCompat.getColor(this.f206a, R.color.blackTranslucent11), b.b.a.b.a(3), b.b.a.b.a(2), b.b.a.b.a(3), null, 16);
                return kotlin.m.f5330a;
            }
        }

        /* renamed from: b.b.a.i.d1$b$b  reason: collision with other inner class name */
        public final class C0018b extends kotlin.d.b.k implements kotlin.d.a.a<List<? extends kotlin.h<? extends e.c, ? extends List<? extends e.d>>>> {

            /* renamed from: b  reason: collision with root package name */
            public final /* synthetic */ String f209b;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0018b(String str) {
                super(0);
                this.f209b = str;
            }

            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: b.b.a.j.p0} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: b.b.a.j.p0} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v9, resolved type: b.b.a.j.p0} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v19, resolved type: java.lang.Object} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v5, resolved type: b.b.a.j.p0} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v11, resolved type: b.b.a.j.p0} */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
             arg types: [java.lang.String, java.lang.String]
             candidates:
              kotlin.d.b.j.a(int, int):int
              kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
              kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
              kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
            /* JADX WARNING: Multi-variable type inference failed */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.util.List<kotlin.h<b.b.a.i.d1.e.c, java.util.List<b.b.a.i.d1.e.d>>> invoke() {
                /*
                    r8 = this;
                    java.lang.String r0 = r8.f209b
                    r1 = 0
                    if (r0 == 0) goto L_0x002d
                    b.b.a.i.d1$b r2 = b.b.a.i.d1.b.this
                    kotlin.d r2 = r2.f204a
                    kotlin.h.h[] r3 = b.b.a.i.d1.b.f
                    java.lang.Object r2 = r2.a()
                    java.util.List r2 = (java.util.List) r2
                    java.util.Iterator r2 = r2.iterator()
                L_0x0015:
                    boolean r3 = r2.hasNext()
                    if (r3 == 0) goto L_0x002b
                    java.lang.Object r3 = r2.next()
                    r4 = r3
                    b.b.a.j.p0 r4 = (b.b.a.j.p0) r4
                    java.lang.String r4 = r4.f1120b
                    boolean r4 = kotlin.d.b.j.a(r4, r0)
                    if (r4 == 0) goto L_0x0015
                    r1 = r3
                L_0x002b:
                    b.b.a.j.p0 r1 = (b.b.a.j.p0) r1
                L_0x002d:
                    if (r1 == 0) goto L_0x005a
                    kotlin.h r0 = new kotlin.h
                    b.b.a.i.d1$e$a r2 = new b.b.a.i.d1$e$a
                    com.supercell.id.SupercellId r3 = com.supercell.id.SupercellId.INSTANCE
                    b.b.a.j.x r3 = r3.getSharedServices$supercellId_release()
                    b.b.a.i.x1.f r3 = r3.j
                    java.lang.String r4 = "region_selection_current_location"
                    java.lang.String r3 = r3.b(r4)
                    if (r3 == 0) goto L_0x0044
                    goto L_0x0046
                L_0x0044:
                    java.lang.String r3 = ""
                L_0x0046:
                    r2.<init>(r3)
                    b.b.a.i.d1$e$b r3 = new b.b.a.i.d1$e$b
                    r3.<init>(r1)
                    java.util.List r1 = kotlin.a.m.a(r3)
                    r0.<init>(r2, r1)
                    java.util.List r0 = kotlin.a.m.a(r0)
                    goto L_0x005e
                L_0x005a:
                    kotlin.a.ab r0 = kotlin.a.ab.f5210a
                    java.util.List r0 = (java.util.List) r0
                L_0x005e:
                    b.b.a.i.d1$b r1 = b.b.a.i.d1.b.this
                    kotlin.d r1 = r1.f204a
                    kotlin.h.h[] r2 = b.b.a.i.d1.b.f
                    java.lang.Object r1 = r1.a()
                    java.util.List r1 = (java.util.List) r1
                    java.util.LinkedHashMap r2 = new java.util.LinkedHashMap
                    r2.<init>()
                    java.util.Iterator r1 = r1.iterator()
                L_0x0073:
                    boolean r3 = r1.hasNext()
                    if (r3 == 0) goto L_0x0098
                    java.lang.Object r3 = r1.next()
                    r4 = r3
                    b.b.a.j.p0 r4 = (b.b.a.j.p0) r4
                    java.lang.String r4 = r4.a()
                    java.lang.Object r5 = r2.get(r4)
                    if (r5 != 0) goto L_0x0092
                    java.util.ArrayList r5 = new java.util.ArrayList
                    r5.<init>()
                    r2.put(r4, r5)
                L_0x0092:
                    java.util.List r5 = (java.util.List) r5
                    r5.add(r3)
                    goto L_0x0073
                L_0x0098:
                    java.util.ArrayList r1 = new java.util.ArrayList
                    int r3 = r2.size()
                    r1.<init>(r3)
                    java.util.Set r2 = r2.entrySet()
                    java.util.Iterator r2 = r2.iterator()
                L_0x00a9:
                    boolean r3 = r2.hasNext()
                    if (r3 == 0) goto L_0x00f3
                    java.lang.Object r3 = r2.next()
                    java.util.Map$Entry r3 = (java.util.Map.Entry) r3
                    b.b.a.i.d1$e$c r4 = new b.b.a.i.d1$e$c
                    java.lang.Object r5 = r3.getKey()
                    java.lang.String r5 = (java.lang.String) r5
                    r4.<init>(r5)
                    java.lang.Object r3 = r3.getValue()
                    java.lang.Iterable r3 = (java.lang.Iterable) r3
                    java.util.ArrayList r5 = new java.util.ArrayList
                    r6 = 10
                    int r6 = kotlin.a.m.a(r3, r6)
                    r5.<init>(r6)
                    java.util.Iterator r3 = r3.iterator()
                L_0x00d5:
                    boolean r6 = r3.hasNext()
                    if (r6 == 0) goto L_0x00ea
                    java.lang.Object r6 = r3.next()
                    b.b.a.j.p0 r6 = (b.b.a.j.p0) r6
                    b.b.a.i.d1$e$d r7 = new b.b.a.i.d1$e$d
                    r7.<init>(r6)
                    r5.add(r7)
                    goto L_0x00d5
                L_0x00ea:
                    kotlin.h r3 = new kotlin.h
                    r3.<init>(r4, r5)
                    r1.add(r3)
                    goto L_0x00a9
                L_0x00f3:
                    java.util.List r0 = kotlin.a.m.d(r0, r1)
                    return r0
                */
                throw new UnsupportedOperationException("Method not decompiled: b.b.a.i.d1.b.C0018b.invoke():java.util.List");
            }
        }

        public final class c extends kotlin.d.b.k implements kotlin.d.a.a<List<? extends p0>> {

            /* renamed from: a  reason: collision with root package name */
            public static final c f210a = new c();

            public final class a<T> implements Comparator<T> {

                /* renamed from: a  reason: collision with root package name */
                public final /* synthetic */ Comparator f211a;

                public a(Comparator comparator) {
                    this.f211a = comparator;
                }

                public final int compare(T t, T t2) {
                    return this.f211a.compare(((p0) t).f1119a, ((p0) t2).f1119a);
                }
            }

            public c() {
                super(0);
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: kotlin.a.p.a(java.lang.Iterable, int):int
             arg types: [java.util.List<java.lang.String>, int]
             candidates:
              kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
              kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
              kotlin.a.w.a(java.util.List, int):T
              kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
              kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
              kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
              kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
              kotlin.a.s.a(java.util.List, java.util.Comparator):void
              kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
              kotlin.a.p.a(java.lang.Iterable, int):int */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
             arg types: [java.lang.String, java.lang.String]
             candidates:
              kotlin.d.b.j.a(int, int):int
              kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
              kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
              kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
             arg types: [java.util.ArrayList, b.b.a.i.d1$b$c$a]
             candidates:
              kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
              kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
              kotlin.a.w.a(java.util.List, int):T
              kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
              kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
              kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
              kotlin.a.s.a(java.util.List, java.util.Comparator):void
              kotlin.a.p.a(java.lang.Iterable, int):int
              kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
              kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T> */
            public final List<p0> invoke() {
                List<String> a2 = h0.f1042a.a();
                ArrayList arrayList = new ArrayList(kotlin.a.m.a((Iterable) a2, 10));
                for (String str : a2) {
                    String displayCountry = new Locale("", str).getDisplayCountry(SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getLocale());
                    kotlin.d.b.j.a((Object) displayCountry, "Locale(\"\", it).getDispla…ces.configuration.locale)");
                    arrayList.add(new p0(displayCountry, str, com.google.i18n.phonenumbers.g.a().a(str)));
                }
                return kotlin.a.m.a((Iterable) arrayList, (Comparator) new a(SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getCollator()));
            }
        }

        static {
            Class<b> cls = b.class;
            f = new kotlin.h.h[]{t.a(new kotlin.d.b.r(t.a(cls), "supportedRegions", "getSupportedRegions()Ljava/util/List;")), t.a(new kotlin.d.b.r(t.a(cls), "regions", "getRegions()Ljava/util/List;"))};
        }

        public b(d1 d1Var, String str, String str2) {
            kotlin.d.b.j.b(str2, "selectedRegion");
            this.e = d1Var;
            this.d = str2;
            this.f205b = kotlin.e.a(new C0018b(str));
            this.c = "";
        }

        public final e a(int i) {
            return a().get(i);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
         arg types: [java.util.ArrayList, java.util.List]
         candidates:
          kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
          kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
          kotlin.a.w.a(java.util.List, int):T
          kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
          kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
          kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
          kotlin.a.s.a(java.util.List, java.util.Comparator):void
          kotlin.a.p.a(java.lang.Iterable, int):int
          kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
          kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.j.ac.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean
         arg types: [java.lang.String, java.lang.String, int]
         candidates:
          kotlin.j.ab.a(java.lang.String, java.lang.String, boolean):boolean
          kotlin.j.y.a(java.lang.Appendable, java.lang.Object, kotlin.d.a.b):void
          kotlin.j.ac.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean */
        public final List<e> a() {
            List list;
            if (this.c.length() == 0) {
                ArrayList arrayList = new ArrayList();
                for (kotlin.h hVar : (List) this.f205b.a()) {
                    kotlin.a.m.a((Collection) arrayList, (Iterable) kotlin.a.m.d(kotlin.a.m.a((Object) hVar.f5262a), hVar.f5263b));
                }
                return arrayList;
            }
            ArrayList arrayList2 = new ArrayList();
            for (kotlin.h hVar2 : (List) this.f205b.a()) {
                if (!(hVar2.f5262a instanceof e.a)) {
                    list = new ArrayList();
                    for (Object next : hVar2.f5263b) {
                        if (kotlin.j.t.a((CharSequence) ((e.d) next).f217a.f1119a, (CharSequence) this.c, true)) {
                            list.add(next);
                        }
                    }
                    if (!list.isEmpty()) {
                        kotlin.a.m.a((Collection) arrayList2, (Iterable) list);
                    }
                }
                list = ab.f5210a;
                kotlin.a.m.a((Collection) arrayList2, (Iterable) list);
            }
            return arrayList2;
        }

        public final int getItemCount() {
            return a().size();
        }

        public final int getItemViewType(int i) {
            e eVar = a().get(i);
            if (!(eVar instanceof e.a)) {
                if (!(eVar instanceof e.b)) {
                    if (!(eVar instanceof e.c)) {
                        if (!(eVar instanceof e.d)) {
                            throw new NoWhenBranchMatchedException();
                        }
                    }
                }
                return 1;
            }
            return 0;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.TextView, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.text.SpannableStringBuilder, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.widget.ImageView, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
            kotlin.d.b.j.b(viewHolder, "holder");
            int itemViewType = viewHolder.getItemViewType();
            if (itemViewType == 0) {
                e eVar = a().get(i);
                if (eVar != null) {
                    TextView textView = ((c) viewHolder).f212a;
                    kotlin.d.b.j.a((Object) textView, "holder.title");
                    textView.setText(((e.c) eVar).f216a);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type com.supercell.id.ui.RegionListDialogFragment.RegionListItem.RegionHeaderItem");
            } else if (itemViewType == 1) {
                e eVar2 = a().get(i);
                if (eVar2 != null) {
                    e.d dVar = (e.d) eVar2;
                    d dVar2 = (d) viewHolder;
                    Context context = this.e.getContext();
                    if (context != null) {
                        TextView textView2 = dVar2.f213a;
                        kotlin.d.b.j.a((Object) textView2, "holder.nameAndCountryCode");
                        SpannableStringBuilder append = b.b.a.b.a(new SpannableStringBuilder(), dVar.f217a.f1119a, new ForegroundColorSpan(ContextCompat.getColor(context, R.color.black)), 33).append((CharSequence) "   ");
                        kotlin.d.b.j.a((Object) append, "SpannableStringBuilder()…           .append(\"   \")");
                        StringBuilder a2 = b.a.a.a.a.a("‪(+");
                        a2.append(dVar.f217a.c);
                        a2.append(")‬");
                        textView2.setText(b.b.a.b.a(append, a2.toString(), new ForegroundColorSpan(ContextCompat.getColor(context, R.color.gray67)), 33));
                        SupercellId.INSTANCE.getSharedServices$supercellId_release().j.a("regionFlags.png", new a(context, this, dVar2, dVar));
                    }
                    ImageView imageView = dVar2.f214b;
                    kotlin.d.b.j.a((Object) imageView, "holder.selected");
                    imageView.setVisibility(kotlin.d.b.j.a(dVar.f217a.f1120b, this.d) ? 0 : 4);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type com.supercell.id.ui.RegionListDialogFragment.RegionListItem.RegionItem");
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.LayoutInflater, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            kotlin.d.b.j.b(viewGroup, "parent");
            if (i == 0) {
                LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
                kotlin.d.b.j.a((Object) from, "LayoutInflater.from(parent.context)");
                return new c(from, viewGroup);
            } else if (i == 1) {
                d1 d1Var = this.e;
                LayoutInflater from2 = LayoutInflater.from(viewGroup.getContext());
                kotlin.d.b.j.a((Object) from2, "LayoutInflater.from(parent.context)");
                return new d(d1Var, from2, viewGroup);
            } else {
                throw new IllegalArgumentException("Unknown view type " + i);
            }
        }
    }

    public static final class c extends RecyclerView.ViewHolder {

        /* renamed from: a  reason: collision with root package name */
        public final TextView f212a;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(LayoutInflater layoutInflater, ViewGroup viewGroup) {
            super(layoutInflater.inflate(R.layout.fragment_region_list_dialog_header, viewGroup, false));
            kotlin.d.b.j.b(layoutInflater, "inflater");
            kotlin.d.b.j.b(viewGroup, "parent");
            View view = this.itemView;
            kotlin.d.b.j.a((Object) view, "itemView");
            this.f212a = (TextView) view.findViewById(R.id.region_header_title);
        }
    }

    public final class d extends RecyclerView.ViewHolder {

        /* renamed from: a  reason: collision with root package name */
        public final TextView f213a;

        /* renamed from: b  reason: collision with root package name */
        public final ImageView f214b;
        public final ImageView c;
        public final /* synthetic */ d1 d;

        public final class a implements View.OnClickListener {
            public a() {
            }

            public final void onClick(View view) {
                SupercellId.INSTANCE.getSharedServices$supercellId_release().l.a(a.C0004a.BUTTON_01);
                d1 d1Var = d.this.d;
                f fVar = d1Var.f202a;
                if (fVar != null) {
                    b b2 = d1.b(d1Var);
                    e a2 = b2 != null ? b2.a(d.this.getAdapterPosition()) : null;
                    if (!(a2 instanceof e.d)) {
                        a2 = null;
                    }
                    e.d dVar = (e.d) a2;
                    if (dVar != null) {
                        fVar.a(dVar.f217a.f1120b);
                    }
                    d.this.d.a();
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.view.View, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(d1 d1Var, LayoutInflater layoutInflater, ViewGroup viewGroup) {
            super(layoutInflater.inflate(R.layout.fragment_region_list_dialog_item, viewGroup, false));
            kotlin.d.b.j.b(layoutInflater, "inflater");
            kotlin.d.b.j.b(viewGroup, "parent");
            this.d = d1Var;
            View view = this.itemView;
            kotlin.d.b.j.a((Object) view, "itemView");
            this.f213a = (TextView) view.findViewById(R.id.region_name_and_country_code);
            View view2 = this.itemView;
            kotlin.d.b.j.a((Object) view2, "itemView");
            this.f214b = (ImageView) view2.findViewById(R.id.region_selected);
            View view3 = this.itemView;
            kotlin.d.b.j.a((Object) view3, "itemView");
            this.c = (ImageView) view3.findViewById(R.id.region_flag);
            this.itemView.setOnClickListener(new a());
            View view4 = this.itemView;
            kotlin.d.b.j.a((Object) view4, "itemView");
            view4.setSoundEffectsEnabled(false);
        }

        public final ImageView a() {
            return this.c;
        }
    }

    public static abstract class e {

        public static final class a extends c {
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(String str) {
                super(str);
                kotlin.d.b.j.b(str, "title");
            }
        }

        public static final class b extends d {
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(p0 p0Var) {
                super(p0Var);
                kotlin.d.b.j.b(p0Var, "region");
            }
        }

        public static class c extends e {

            /* renamed from: a  reason: collision with root package name */
            public final String f216a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(String str) {
                super(null);
                kotlin.d.b.j.b(str, "title");
                this.f216a = str;
            }
        }

        public static class d extends e {

            /* renamed from: a  reason: collision with root package name */
            public final p0 f217a;

            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public d(p0 p0Var) {
                super(null);
                kotlin.d.b.j.b(p0Var, "region");
                this.f217a = p0Var;
            }
        }

        public e() {
        }

        public /* synthetic */ e(kotlin.d.b.g gVar) {
        }
    }

    public interface f {
        void a(String str);
    }

    public final class g extends AnimatorListenerAdapter {
        public g() {
        }

        public final void onAnimationEnd(Animator animator) {
            d1.this.dismissAllowingStateLoss();
        }
    }

    public final class h extends kotlin.d.b.k implements kotlin.d.a.a<kotlin.m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ LinearLayout f219a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ d1 f220b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(LinearLayout linearLayout, d1 d1Var) {
            super(0);
            this.f219a = linearLayout;
            this.f220b = d1Var;
        }

        public final Object invoke() {
            if (this.f220b.isAdded()) {
                ViewPropertyAnimator interpolator = this.f219a.animate().setDuration(150).setInterpolator(b.b.a.f.a.c);
                boolean z = true;
                if (ViewCompat.getLayoutDirection(this.f219a) != 1) {
                    z = false;
                }
                float width = (float) this.f219a.getWidth();
                if (z) {
                    width = -width;
                }
                interpolator.translationX(width).start();
            }
            return kotlin.m.f5330a;
        }
    }

    public final class i extends kotlin.d.b.k implements kotlin.d.a.a<kotlin.m> {
        public i() {
            super(0);
        }

        public final Object invoke() {
            d1.super.dismiss();
            return kotlin.m.f5330a;
        }
    }

    public final class j extends kotlin.d.b.k implements kotlin.d.a.a<kotlin.m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ View f222a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ d1 f223b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(View view, d1 d1Var) {
            super(0);
            this.f222a = view;
            this.f223b = d1Var;
        }

        public final Object invoke() {
            if (this.f223b.isAdded()) {
                this.f222a.animate().setDuration(300).setInterpolator(b.b.a.f.a.c).alpha(1.0f).start();
            }
            return kotlin.m.f5330a;
        }
    }

    public final class k extends kotlin.d.b.k implements kotlin.d.a.a<kotlin.m> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ LinearLayout f224a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ d1 f225b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(LinearLayout linearLayout, d1 d1Var) {
            super(0);
            this.f224a = linearLayout;
            this.f225b = d1Var;
        }

        public final Object invoke() {
            if (this.f225b.isAdded()) {
                LinearLayout linearLayout = this.f224a;
                boolean z = true;
                if (ViewCompat.getLayoutDirection(linearLayout) != 1) {
                    z = false;
                }
                float width = (float) this.f224a.getWidth();
                if (z) {
                    width = -width;
                }
                linearLayout.setTranslationX(width);
                this.f224a.setAlpha(1.0f);
                this.f224a.animate().setDuration(300).setInterpolator(b.b.a.f.a.c).translationX(0.0f).start();
            }
            return kotlin.m.f5330a;
        }
    }

    public final class l extends Dialog {
        public l(Context context, int i) {
            super(context, i);
        }

        public final void onBackPressed() {
            SupercellId.INSTANCE.getSharedServices$supercellId_release().l.a(a.C0004a.CANCEL_BUTTON_01);
            d1.this.a();
        }
    }

    public final class m implements DialogInterface.OnShowListener {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ BottomSheetDialog f227a;

        public m(BottomSheetDialog bottomSheetDialog) {
            this.f227a = bottomSheetDialog;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [android.support.design.widget.BottomSheetBehavior, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final void onShow(DialogInterface dialogInterface) {
            FrameLayout frameLayout = (FrameLayout) this.f227a.findViewById(android.support.design.R.id.design_bottom_sheet);
            if (frameLayout != null) {
                BottomSheetBehavior from = BottomSheetBehavior.from(frameLayout);
                kotlin.d.b.j.a((Object) from, "BottomSheetBehavior.from(frameLayout)");
                from.setState(3);
                BottomSheetBehavior from2 = BottomSheetBehavior.from(frameLayout);
                kotlin.d.b.j.a((Object) from2, "BottomSheetBehavior.from(frameLayout)");
                from2.setSkipCollapsed(true);
                BottomSheetBehavior from3 = BottomSheetBehavior.from(frameLayout);
                kotlin.d.b.j.a((Object) from3, "BottomSheetBehavior.from(frameLayout)");
                from3.setHideable(true);
            }
        }
    }

    public final class n implements View.OnClickListener {
        public n() {
        }

        public final void onClick(View view) {
            SupercellId.INSTANCE.getSharedServices$supercellId_release().l.a(a.C0004a.CANCEL_BUTTON_01);
            d1.this.a();
        }
    }

    public final class o extends kotlin.d.b.k implements kotlin.d.a.c<TouchInterceptingFrameLayout, MotionEvent, Boolean> {
        public o() {
            super(2);
        }

        public final Object invoke(Object obj, Object obj2) {
            kotlin.d.b.j.b((TouchInterceptingFrameLayout) obj, "<anonymous parameter 0>");
            d1.c(d1.this);
            return false;
        }
    }

    public final class p extends kotlin.d.b.k implements kotlin.d.a.a<kotlin.m> {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ Drawable f231b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public p(Drawable drawable) {
            super(0);
            this.f231b = drawable;
        }

        public final Object invoke() {
            TextViewCompat.setCompoundDrawablesRelative((EditText) d1.this.a(R.id.searchField), this.f231b, null, null, null);
            return kotlin.m.f5330a;
        }
    }

    public final class q implements TextWatcher {
        public q() {
        }

        public final void afterTextChanged(Editable editable) {
            String valueOf = String.valueOf(editable);
            b b2 = d1.b(d1.this);
            if (b2 != null) {
                kotlin.d.b.j.b(valueOf, "value");
                b2.c = valueOf;
                b2.notifyDataSetChanged();
            }
        }

        public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    public final class r implements View.OnFocusChangeListener {
        public r() {
        }

        public final void onFocusChange(View view, boolean z) {
            if (!z) {
                d1.c(d1.this);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v7.widget.RecyclerView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final /* synthetic */ b b(d1 d1Var) {
        RecyclerView recyclerView = (RecyclerView) d1Var.a(R.id.list);
        kotlin.d.b.j.a((Object) recyclerView, "list");
        RecyclerView.Adapter adapter = recyclerView.getAdapter();
        if (!(adapter instanceof b)) {
            adapter = null;
        }
        return (b) adapter;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final /* synthetic */ void c(d1 d1Var) {
        View view = d1Var.getView();
        if (view != null) {
            kotlin.d.b.j.a((Object) view, "view ?: return");
            Context context = view.getContext();
            if (context != null) {
                Object systemService = context.getSystemService("input_method");
                if (systemService != null) {
                    ((InputMethodManager) systemService).hideSoftInputFromWindow(view.getWindowToken(), 0);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type android.view.inputmethod.InputMethodManager");
            }
        }
    }

    public final View a(int i2) {
        if (this.f203b == null) {
            this.f203b = new HashMap();
        }
        View view = (View) this.f203b.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i2);
        this.f203b.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }

    public final void a() {
        View a2 = a(R.id.dimmer);
        if (a2 != null) {
            a2.animate().setDuration(150).setInterpolator(b.b.a.f.a.c).alpha(0.0f).setListener(new g()).start();
        } else {
            new i().invoke();
        }
        LinearLayout linearLayout = (LinearLayout) a(R.id.container);
        if (linearLayout != null) {
            q1.a(linearLayout, new h(linearLayout, this));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.app.Dialog, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onActivityCreated(Bundle bundle) {
        ViewGroup.LayoutParams layoutParams;
        FrameLayout frameLayout;
        ViewGroup.LayoutParams layoutParams2;
        super.onActivityCreated(bundle);
        if (getDialog() instanceof BottomSheetDialog) {
            Bundle arguments = getArguments();
            if (arguments != null) {
                int i2 = arguments.getInt("height");
                View view = getView();
                if (!(view == null || (layoutParams2 = view.getLayoutParams()) == null)) {
                    layoutParams2.height = i2;
                }
            }
            FrameLayout frameLayout2 = (FrameLayout) getDialog().findViewById(android.support.design.R.id.container);
            if (frameLayout2 != null) {
                frameLayout2.setFitsSystemWindows(false);
            }
            CoordinatorLayout coordinatorLayout = (CoordinatorLayout) getDialog().findViewById(android.support.design.R.id.coordinator);
            if (coordinatorLayout != null) {
                coordinatorLayout.setFitsSystemWindows(false);
            }
            Resources resources = getResources();
            kotlin.d.b.j.a((Object) resources, "resources");
            if (b.b.a.b.c(resources) && (frameLayout = (FrameLayout) getDialog().findViewById(android.support.design.R.id.design_bottom_sheet)) != null) {
                Bundle arguments2 = getArguments();
                if (arguments2 != null) {
                    int i3 = arguments2.getInt("width");
                    ViewGroup.LayoutParams layoutParams3 = frameLayout.getLayoutParams();
                    if (layoutParams3 != null) {
                        layoutParams3.width = i3;
                    }
                }
                ViewGroup.LayoutParams layoutParams4 = frameLayout.getLayoutParams();
                if (!(layoutParams4 instanceof CoordinatorLayout.LayoutParams)) {
                    layoutParams4 = null;
                }
                CoordinatorLayout.LayoutParams layoutParams5 = (CoordinatorLayout.LayoutParams) layoutParams4;
                if (layoutParams5 != null) {
                    layoutParams5.gravity = 53;
                }
            }
        } else {
            Bundle arguments3 = getArguments();
            if (arguments3 != null) {
                int i4 = arguments3.getInt("width");
                int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.region_code_dialog_max_width);
                LinearLayout linearLayout = (LinearLayout) a(R.id.container);
                if (!(linearLayout == null || (layoutParams = linearLayout.getLayoutParams()) == null)) {
                    layoutParams.width = Math.min(i4, dimensionPixelSize);
                }
            }
            View a2 = a(R.id.dimmer);
            if (a2 != null) {
                q1.a(a2, new j(a2, this));
            }
            LinearLayout linearLayout2 = (LinearLayout) a(R.id.container);
            if (linearLayout2 != null) {
                q1.a(linearLayout2, new k(linearLayout2, this));
            }
        }
        Dialog dialog = getDialog();
        kotlin.d.b.j.a((Object) dialog, "dialog");
        Window window = dialog.getWindow();
        if (window != null) {
            if (Build.VERSION.SDK_INT >= 21) {
                window.clearFlags(67108864);
            }
            FragmentActivity activity = getActivity();
            if (activity != null && b.b.a.b.b(activity)) {
                window.addFlags(1056);
            }
        }
    }

    public final void onAttach(Context context) {
        kotlin.d.b.j.b(context, "context");
        super.onAttach(context);
        Fragment parentFragment = getParentFragment();
        this.f202a = parentFragment != null ? (f) parentFragment : (f) context;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        SupercellId.INSTANCE.getSharedServices$supercellId_release().l.a(a.C0004a.CANCEL_BUTTON_01);
        super.onCancel(dialogInterface);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final Dialog onCreateDialog(Bundle bundle) {
        Resources resources = getResources();
        kotlin.d.b.j.a((Object) resources, "resources");
        if (b.b.a.b.b(resources)) {
            setStyle(1, R.style.SupercellIdTheme);
            Context context = getContext();
            if (context == null) {
                kotlin.d.b.j.a();
            }
            return new l(context, getTheme());
        }
        setStyle(1, R.style.SupercellIdBottomSheetDialogTheme);
        Context context2 = getContext();
        if (context2 == null) {
            kotlin.d.b.j.a();
        }
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(context2, getTheme());
        bottomSheetDialog.setOnShowListener(new m(bottomSheetDialog));
        return bottomSheetDialog;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kotlin.d.b.j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_region_list_dialog, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        HashMap hashMap = this.f203b;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public final void onDetach() {
        this.f202a = null;
        super.onDetach();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v7.widget.RecyclerView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.EditText, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onViewCreated(View view, Bundle bundle) {
        String str;
        kotlin.d.b.j.b(view, "view");
        View a2 = a(R.id.dimmer);
        if (a2 != null) {
            a2.setOnClickListener(new n());
        }
        View a3 = a(R.id.dimmer);
        if (a3 != null) {
            a3.setSoundEffectsEnabled(false);
        }
        ((TouchInterceptingFrameLayout) a(R.id.touchInterceptor)).setTouchInterceptor(new o());
        Bundle arguments = getArguments();
        String string = arguments != null ? arguments.getString("currentRegion") : null;
        Bundle arguments2 = getArguments();
        if (arguments2 == null || (str = arguments2.getString("selectedRegion")) == null) {
            str = "us";
        }
        b bVar = new b(this, string, str);
        RecyclerView recyclerView = (RecyclerView) a(R.id.list);
        kotlin.d.b.j.a((Object) recyclerView, "list");
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        RecyclerView recyclerView2 = (RecyclerView) a(R.id.list);
        kotlin.d.b.j.a((Object) recyclerView2, "list");
        recyclerView2.setAdapter(bVar);
        ((FastScroll) a(R.id.fastscroll)).setRecyclerView((RecyclerView) a(R.id.list));
        View a4 = a(R.id.region_header_title_sticky);
        if (a4 != null) {
            s.b bVar2 = new s.b();
            bVar2.f5252a = -1;
            ((RecyclerView) a(R.id.list)).addOnScrollListener(new e1((TextView) a4, bVar, bVar2));
            Drawable drawable = AppCompatResources.getDrawable(view.getContext(), R.drawable.magnifying_glass);
            if (drawable != null) {
                drawable.setBounds(new Rect(0, 0, kotlin.e.a.a(b.b.a.b.a(16)), kotlin.e.a.a(b.b.a.b.a(16))));
            }
            EditText editText = (EditText) a(R.id.searchField);
            kotlin.d.b.j.a((Object) editText, "searchField");
            q1.a(editText, new p(drawable));
            ((EditText) a(R.id.searchField)).addTextChangedListener(new q());
            ((EditText) a(R.id.searchField)).setOnFocusChangeListener(new r());
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
    }
}
