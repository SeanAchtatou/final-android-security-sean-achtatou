package b.b.a.i.g1;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.content.res.AppCompatResources;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.view.View;
import android.widget.TextView;
import b.b.a.e.a;
import b.b.a.i.g1.a;
import b.b.a.i.w1.n;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class b extends k implements kotlin.d.a.b<String, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ a.f f275a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ String f276b;

    public final class a extends ClickableSpan {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ b f277a;

        public a(TextView textView, b bVar, String str) {
            this.f277a = bVar;
        }

        public final void onClick(View view) {
            j.b(view, "widget");
            SupercellId.INSTANCE.getSharedServices$supercellId_release().l.a(a.C0004a.BUTTON_01);
            MainActivity a2 = b.b.a.b.a((Fragment) a.this);
            if (a2 != null) {
                a2.a(new n.a(null));
            }
        }

        public final void updateDrawState(TextPaint textPaint) {
            j.b(textPaint, "ds");
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b(a.f fVar, String str) {
        super(1);
        this.f275a = fVar;
        this.f276b = str;
    }

    public final /* synthetic */ Object invoke(Object obj) {
        a((String) obj);
        return m.f5330a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.graphics.drawable.Drawable, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.graphics.Bitmap, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public final void a(String str) {
        String str2 = str;
        j.b(str2, "buttonText");
        TextView textView = (TextView) this.f275a.f273b.get();
        if (textView != null) {
            SpannableStringBuilder append = new SpannableStringBuilder().append((CharSequence) this.f276b).append((CharSequence) "  ");
            j.b(str2, "$this$replace");
            String replace = str2.replace(' ', 160);
            j.a((Object) replace, "(this as java.lang.Strin…replace(oldChar, newChar)");
            SpannableStringBuilder append2 = append.append((CharSequence) replace).append((CharSequence) "  ");
            append2.setSpan(new ForegroundColorSpan(ResourcesCompat.getColor(textView.getResources(), R.color.text_blue, null)), (append2.length() - 2) - str.length(), append2.length(), 33);
            Context context = textView.getContext();
            if (context == null) {
                j.a();
            }
            j.b(context, "context");
            Drawable drawable = AppCompatResources.getDrawable(context, R.drawable.register_arrow);
            if (drawable == null) {
                j.a();
            }
            j.a((Object) drawable, "AppCompatResources.getDr…rawable.register_arrow)!!");
            if (Build.VERSION.SDK_INT < 21) {
                drawable = DrawableCompat.wrap(drawable).mutate();
                j.a((Object) drawable, "DrawableCompat.wrap(drawable).mutate()");
            }
            Bitmap createBitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            float f = SupercellId.INSTANCE.getSharedServices$supercellId_release().e.isRTL() ? -1.0f : 1.0f;
            Matrix matrix = new Matrix();
            matrix.postScale(f, 1.0f);
            j.a((Object) createBitmap, "bitmap");
            Bitmap createBitmap2 = Bitmap.createBitmap(createBitmap, 0, 0, createBitmap.getWidth(), createBitmap.getHeight(), matrix, true);
            j.a((Object) createBitmap2, "Bitmap.createBitmap(bitm…map.height, matrix, true)");
            Context context2 = textView.getContext();
            if (context2 == null) {
                j.a();
            }
            append2.setSpan(new ImageSpan(context2, createBitmap2, 0), append2.length() - 1, append2.length(), 33);
            append2.setSpan(new a(textView, this, str2), (append2.length() - 2) - str.length(), append2.length(), 33);
            textView.setText(append2);
        }
    }
}
