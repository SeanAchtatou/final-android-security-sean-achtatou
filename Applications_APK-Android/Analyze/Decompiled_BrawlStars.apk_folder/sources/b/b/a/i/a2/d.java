package b.b.a.i.a2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import b.b.a.i.f1;
import com.supercell.id.R;
import java.util.HashMap;
import kotlin.d.b.g;
import kotlin.d.b.j;

public final class d extends f1 {
    public static final a c = new a(null);

    /* renamed from: b  reason: collision with root package name */
    public HashMap f177b;

    public static final class a {
        public /* synthetic */ a(g gVar) {
        }

        public final d a(String str) {
            j.b(str, "imageKey");
            d dVar = new d();
            Bundle arguments = dVar.getArguments();
            if (arguments == null) {
                arguments = new Bundle();
            }
            arguments.putString("imageKey", str);
            dVar.setArguments(arguments);
            return dVar;
        }
    }

    public final void a() {
        HashMap hashMap = this.f177b;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        j.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_tutorial_image_page, viewGroup, false);
    }

    public final /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.i.x1.j.a(android.widget.ImageView, java.lang.String, boolean, int):void
     arg types: [android.widget.ImageView, java.lang.String, int, int]
     candidates:
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, java.lang.Object, int):void
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, kotlin.d.a.b, int):void
      b.b.a.i.x1.j.a(android.widget.ImageView, java.lang.String, boolean, int):void */
    public final void onViewCreated(View view, Bundle bundle) {
        j.b(view, "view");
        super.onViewCreated(view, bundle);
        int i = R.id.image;
        if (this.f177b == null) {
            this.f177b = new HashMap();
        }
        View view2 = (View) this.f177b.get(Integer.valueOf(i));
        if (view2 == null) {
            View view3 = getView();
            if (view3 == null) {
                view2 = null;
            } else {
                view2 = view3.findViewById(i);
                this.f177b.put(Integer.valueOf(i), view2);
            }
        }
        ImageView imageView = (ImageView) view2;
        if (imageView != null) {
            Bundle arguments = getArguments();
            if (arguments == null) {
                j.a();
            }
            String string = arguments.getString("imageKey");
            if (string == null) {
                j.a();
            }
            b.b.a.i.x1.j.a(imageView, string, false, 2);
        }
    }
}
