package b.b.a.i.o1;

import com.facebook.internal.ServerProtocol;
import com.supercell.id.SupercellId;
import java.lang.ref.WeakReference;
import kotlin.d.a.b;
import kotlin.d.b.k;
import kotlin.m;

public final class i extends k implements b<Boolean, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f429a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ String f430b;
    public final /* synthetic */ boolean c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public i(WeakReference weakReference, String str, boolean z) {
        super(1);
        this.f429a = weakReference;
        this.f430b = str;
        this.c = z;
    }

    public final Object invoke(Object obj) {
        ((Boolean) obj).booleanValue();
        k kVar = (k) this.f429a.get();
        if (kVar != null) {
            SupercellId.INSTANCE.setPendingLoginWithEmail$supercellId_release(this.f430b, this.c);
            b.b.a.c.b.a(SupercellId.INSTANCE.getSharedServices$supercellId_release().f, "Remember me", "Selection", this.c ? ServerProtocol.DIALOG_RETURN_SCOPES_TRUE : "false", null, false, 24);
            kVar.b(this.f430b);
            kVar.a(this.c);
            p h = kVar.h();
            if (h != null) {
                h.k();
            }
        }
        return m.f5330a;
    }
}
