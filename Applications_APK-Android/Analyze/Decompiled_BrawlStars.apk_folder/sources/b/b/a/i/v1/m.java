package b.b.a.i.v1;

import android.support.v4.app.Fragment;
import b.b.a.h.j;
import b.b.a.i.e;
import b.b.a.j.y0;
import com.supercell.id.ui.MainActivity;
import java.lang.ref.WeakReference;
import kotlin.d.a.b;
import kotlin.d.b.k;
import nl.komponents.kovenant.bb;
import nl.komponents.kovenant.bw;

public final class m extends k implements b<Exception, kotlin.m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f804a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ j f805b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m(WeakReference weakReference, j jVar) {
        super(1);
        this.f804a = weakReference;
        this.f805b = jVar;
    }

    public final Object invoke(Object obj) {
        Object obj2 = this.f804a.get();
        if (obj2 != null) {
            Exception exc = (Exception) obj;
            j jVar = (j) obj2;
            MainActivity a2 = b.b.a.b.a((Fragment) jVar);
            if (a2 != null) {
                a2.a(exc, (b<? super e, kotlin.m>) null);
            }
            y0<h> y0Var = jVar.p;
            bw.a aVar = bw.d;
            h hVar = jVar.q;
            y0Var.a(bb.f5389a);
        }
        return kotlin.m.f5330a;
    }
}
