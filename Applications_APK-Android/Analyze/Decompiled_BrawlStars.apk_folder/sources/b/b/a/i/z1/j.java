package b.b.a.i.z1;

import b.b.a.j.f0;
import java.lang.ref.WeakReference;
import kotlin.d.a.b;
import kotlin.d.b.k;
import kotlin.m;

public final class j extends k implements b<Exception, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f1000a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public j(WeakReference weakReference) {
        super(1);
        this.f1000a = weakReference;
    }

    public final Object invoke(Object obj) {
        Exception exc = (Exception) obj;
        kotlin.d.b.j.b(exc, "it");
        l lVar = (l) this.f1000a.get();
        if (lVar != null) {
            lVar.c = f0.e.a(exc);
        }
        return m.f5330a;
    }
}
