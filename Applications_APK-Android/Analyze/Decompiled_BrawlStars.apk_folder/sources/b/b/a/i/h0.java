package b.b.a.i;

import android.animation.ValueAnimator;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.supercell.id.R;
import kotlin.d.b.j;
import kotlin.e.a;

public final class h0 implements ValueAnimator.AnimatorUpdateListener {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ ValueAnimator f280a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ m0 f281b;
    public final /* synthetic */ int c;
    public final /* synthetic */ int d;

    public h0(ValueAnimator valueAnimator, m0 m0Var, int i, int i2) {
        this.f280a = valueAnimator;
        this.f281b = m0Var;
        this.c = i;
        this.d = i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.FrameLayout, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        FrameLayout frameLayout = (FrameLayout) this.f281b.f376a.a(R.id.content);
        j.a((Object) frameLayout, "content");
        ViewGroup.LayoutParams layoutParams = frameLayout.getLayoutParams();
        int i = this.c;
        layoutParams.width = a.a((((float) (this.d - i)) * this.f280a.getAnimatedFraction()) + ((float) i));
        ((FrameLayout) this.f281b.f376a.a(R.id.content)).requestLayout();
    }
}
