package b.b.a.i.j1;

import android.graphics.drawable.Drawable;
import kotlin.d.a.b;
import kotlin.d.a.c;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.m;

public final class d extends k implements c<Drawable, b.b.a.i.x1.c, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ b f336a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d(b bVar) {
        super(2);
        this.f336a = bVar;
    }

    public final Object invoke(Object obj, Object obj2) {
        Drawable drawable = (Drawable) obj;
        j.b(drawable, "drawable");
        j.b((b.b.a.i.x1.c) obj2, "<anonymous parameter 1>");
        this.f336a.invoke(drawable);
        return m.f5330a;
    }
}
