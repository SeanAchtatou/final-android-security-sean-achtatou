package b.b.a.i.q1;

import b.b.a.h.l;
import b.b.a.i.e;
import com.supercell.id.ui.MainActivity;
import java.lang.ref.WeakReference;
import java.util.List;
import kotlin.d.a.b;
import kotlin.d.b.k;

public final class m extends k implements b<l, kotlin.m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ WeakReference f571a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m(WeakReference weakReference) {
        super(1);
        this.f571a = weakReference;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.a(java.util.List, int):T
     arg types: [java.util.List<java.lang.String>, int]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.w.a(java.util.List, int):T */
    public final void invoke(l lVar) {
        MainActivity a2;
        Object obj = this.f571a.get();
        if (obj != null) {
            l lVar2 = lVar;
            l lVar3 = (l) obj;
            a f = lVar3.f();
            if (f != null) {
                f.k();
            }
            String str = (String) kotlin.a.m.a((List) lVar2.f130b, 0);
            if (str != null && (a2 = b.b.a.b.a(lVar3)) != null) {
                a2.a(str, (b<? super e, kotlin.m>) null);
            }
        }
    }
}
