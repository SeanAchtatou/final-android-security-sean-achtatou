package b.b.a.i;

import android.animation.ValueAnimator;
import android.view.View;
import kotlin.d.a.a;
import kotlin.d.b.k;
import kotlin.m;

public final class u0 extends k implements a<m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ View f726a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ x0 f727b;
    public final /* synthetic */ boolean c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public u0(View view, x0 x0Var, boolean z) {
        super(0);
        this.f726a = view;
        this.f727b = x0Var;
        this.c = z;
    }

    public final Object invoke() {
        if (this.f727b.isAdded()) {
            if (this.c) {
                this.f726a.bringToFront();
            }
            x0 x0Var = this.f727b;
            ValueAnimator ofFloat = ValueAnimator.ofFloat(x0Var.o, 1.0f);
            ofFloat.setStartDelay(175);
            ofFloat.setDuration(175L);
            ofFloat.setInterpolator(b.b.a.f.a.h);
            ofFloat.addUpdateListener(new t0(ofFloat, this));
            ofFloat.start();
            x0Var.n = ofFloat;
        }
        return m.f5330a;
    }
}
