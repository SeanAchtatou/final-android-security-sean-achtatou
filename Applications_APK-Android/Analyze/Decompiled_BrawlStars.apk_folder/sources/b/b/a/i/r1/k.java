package b.b.a.i.r1;

import android.graphics.drawable.BitmapDrawable;
import b.a.a.a.a;
import b.b.a.i.x1.f;
import b.b.a.j.r0;
import com.supercell.id.SupercellId;
import java.lang.ref.WeakReference;
import kotlin.d.a.b;
import kotlin.d.b.j;
import kotlin.m;

public final class k extends kotlin.d.b.k implements b<BitmapDrawable, m> {

    /* renamed from: a  reason: collision with root package name */
    public final /* synthetic */ String f626a;

    /* renamed from: b  reason: collision with root package name */
    public final /* synthetic */ WeakReference f627b;
    public final /* synthetic */ r0 c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k(String str, WeakReference weakReference, r0 r0Var) {
        super(1);
        this.f626a = str;
        this.f627b = weakReference;
        this.c = r0Var;
    }

    public final Object invoke(Object obj) {
        BitmapDrawable bitmapDrawable = (BitmapDrawable) obj;
        j.b(bitmapDrawable, "drawable");
        f fVar = SupercellId.INSTANCE.getSharedServices$supercellId_release().j;
        StringBuilder a2 = a.a("game_name_");
        a2.append(this.f626a);
        fVar.a(a2.toString(), new j(this, bitmapDrawable));
        return m.f5330a;
    }
}
