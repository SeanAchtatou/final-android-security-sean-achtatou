package b.b.a;

import com.supercell.id.SupercellId;
import kotlin.d.a.b;
import kotlin.d.b.k;
import kotlin.m;

public final class a extends k implements b<Boolean, m> {

    /* renamed from: a  reason: collision with root package name */
    public static final a f15a = new a();

    public a() {
        super(1);
    }

    public final Object invoke(Object obj) {
        boolean booleanValue = ((Boolean) obj).booleanValue();
        SupercellId.INSTANCE.getClass().getName();
        StringBuilder sb = new StringBuilder();
        sb.append("Remote asset update ");
        sb.append(booleanValue ? "OK" : "FAILED");
        sb.toString();
        return m.f5330a;
    }
}
