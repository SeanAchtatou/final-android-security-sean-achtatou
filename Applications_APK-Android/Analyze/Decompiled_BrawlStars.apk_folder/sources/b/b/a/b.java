package b.b.a;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.view.animation.PathInterpolatorCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import b.b.a.g.g;
import b.b.a.h.n;
import b.b.a.i.a2.c;
import b.b.a.i.b;
import b.b.a.i.g;
import b.b.a.i.g1.a;
import b.b.a.i.o1.p;
import b.b.a.i.s1.h;
import b.b.a.i.s1.i;
import b.b.a.i.u1.f;
import b.b.a.i.y;
import b.b.a.j.c1;
import b.b.a.j.e1;
import b.b.a.j.f1;
import b.b.a.j.h1;
import b.b.a.j.i1;
import b.b.a.j.j1;
import b.b.a.j.k;
import b.b.a.j.m0;
import b.b.a.j.m1;
import b.b.a.j.n0;
import b.b.a.j.o0;
import b.b.a.j.q1;
import b.b.a.j.r0;
import com.facebook.internal.NativeProtocol;
import com.supercell.id.IdAccount;
import com.supercell.id.R;
import com.supercell.id.SupercellId;
import com.supercell.id.ui.MainActivity;
import com.supercell.id.view.EdgeAntialiasingImageView;
import com.supercell.id.view.ExpandableFrameLayout;
import com.supercell.id.view.SubPageTabLayout;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import kotlin.TypeCastException;
import kotlin.a.ah;
import kotlin.a.ai;
import kotlin.a.m;
import kotlin.d.b.j;
import kotlin.d.b.v;
import kotlin.e.a;
import kotlin.g.c;
import kotlin.g.d;
import kotlin.h;
import nl.komponents.kovenant.ao;
import nl.komponents.kovenant.ap;
import nl.komponents.kovenant.bb;
import nl.komponents.kovenant.bc;
import nl.komponents.kovenant.bw;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    public static float f16a;

    public static final float a(int i) {
        return ((float) i) * f16a;
    }

    public static final int a(int i, float f) {
        return Color.argb(a.a(f * 255.0f), Color.red(i), Color.green(i), Color.blue(i));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final int a(RecyclerView recyclerView, int i) {
        j.b(recyclerView, "$this$estimatedScrollY");
        RecyclerView.Adapter adapter = recyclerView.getAdapter();
        if ((adapter != null ? adapter.getItemCount() : 0) == 0) {
            return 0;
        }
        RecyclerView.ViewHolder findViewHolderForLayoutPosition = recyclerView.findViewHolderForLayoutPosition(0);
        if (findViewHolderForLayoutPosition == null) {
            return recyclerView.getPaddingTop() + i;
        }
        View view = findViewHolderForLayoutPosition.itemView;
        j.a((Object) view, "firstRowViewHolder.itemView");
        return Math.max(0, recyclerView.getPaddingTop() + (-view.getTop()));
    }

    public static final int a(String str, String str2) {
        boolean z = !(str == null || str.length() == 0);
        boolean z2 = !(str2 == null || str2.length() == 0);
        if (!z || z2) {
            return (z || !z2) ? 0 : 1;
        }
        return -1;
    }

    public static final Animator a(ImageView imageView, int i) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(imageView, "translationY", a(-i));
        ofFloat.setInterpolator(PathInterpolatorCompat.create(0.35f, 0.1f, 0.35f, 1.0f));
        ofFloat.setDuration(120L);
        ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(imageView, "translationY", 0.0f);
        ofFloat2.setInterpolator(PathInterpolatorCompat.create(0.85f, 0.0f, 0.5f, 1.0f));
        ofFloat2.setDuration(80L);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playSequentially(ofFloat, ofFloat2);
        return animatorSet;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static final <T extends android.support.v4.app.Fragment> T a(T r1, java.lang.String r2, android.os.Parcelable r3) {
        /*
            java.lang.String r0 = "$this$addArgument"
            kotlin.d.b.j.b(r1, r0)
            java.lang.String r0 = "key"
            kotlin.d.b.j.b(r2, r0)
            java.lang.String r0 = "value"
            kotlin.d.b.j.b(r3, r0)
            android.os.Bundle r0 = r1.getArguments()
            if (r0 == 0) goto L_0x0016
            goto L_0x001b
        L_0x0016:
            android.os.Bundle r0 = new android.os.Bundle
            r0.<init>()
        L_0x001b:
            r0.putParcelable(r2, r3)
            r1.setArguments(r0)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.b.a(android.support.v4.app.Fragment, java.lang.String, android.os.Parcelable):android.support.v4.app.Fragment");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static final <T extends android.support.v4.app.Fragment> T a(T r1, java.lang.String r2, java.lang.String r3) {
        /*
            java.lang.String r0 = "$this$addArgument"
            kotlin.d.b.j.b(r1, r0)
            java.lang.String r0 = "key"
            kotlin.d.b.j.b(r2, r0)
            java.lang.String r0 = "value"
            kotlin.d.b.j.b(r3, r0)
            android.os.Bundle r0 = r1.getArguments()
            if (r0 == 0) goto L_0x0016
            goto L_0x001b
        L_0x0016:
            android.os.Bundle r0 = new android.os.Bundle
            r0.<init>()
        L_0x001b:
            r0.putString(r2, r3)
            r1.setArguments(r0)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.b.a(android.support.v4.app.Fragment, java.lang.String, java.lang.String):android.support.v4.app.Fragment");
    }

    public static final SpannableStringBuilder a(SpannableStringBuilder spannableStringBuilder, CharSequence charSequence, Object obj, int i) {
        j.b(spannableStringBuilder, "$this$appendText");
        j.b(charSequence, "text");
        j.b(obj, "what");
        int length = spannableStringBuilder.length();
        spannableStringBuilder.append(charSequence);
        spannableStringBuilder.setSpan(obj, length, spannableStringBuilder.length(), i);
        return spannableStringBuilder;
    }

    public static final <T extends b.a> T a(g gVar) {
        j.b(gVar, "$this$backStackEntry");
        Bundle arguments = gVar.getArguments();
        if (arguments != null) {
            return (b.a) arguments.getParcelable("backStackEntry");
        }
        return null;
    }

    public static final MainActivity a(Fragment fragment) {
        j.b(fragment, "$this$mainActivity");
        FragmentActivity activity = fragment.getActivity();
        if (!(activity instanceof MainActivity)) {
            activity = null;
        }
        return (MainActivity) activity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [byte[], java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.StringBuilder, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final String a(byte[] bArr) {
        j.b(bArr, "$this$md5");
        byte[] digest = MessageDigest.getInstance("MD5").digest(bArr);
        j.a((Object) digest, "md5Bytes");
        StringBuilder sb = new StringBuilder();
        for (byte valueOf : digest) {
            v vVar = v.f5255a;
            String format = String.format("%02x", Arrays.copyOf(new Object[]{Byte.valueOf(valueOf)}, 1));
            j.a((Object) format, "java.lang.String.format(format, *args)");
            sb.append(format);
            j.a((Object) sb, "acc.append(String.format(\"%02x\", byte))");
        }
        String sb2 = sb.toString();
        j.a((Object) sb2, "md5Bytes.fold(StringBuil…, byte))\n    }.toString()");
        return sb2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
     arg types: [java.util.ArrayList<T>, T]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.util.List, int):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T> */
    public static final <T> ArrayList<T> a(ArrayList<T> arrayList, T t) {
        j.b(arrayList, "$this$plus");
        return (ArrayList) m.a((Collection) arrayList, (Object) t);
    }

    public static final List<View> a(ViewGroup viewGroup) {
        c b2 = d.b(0, viewGroup.getChildCount());
        ArrayList arrayList = new ArrayList();
        Iterator it = b2.iterator();
        while (it.hasNext()) {
            View childAt = viewGroup.getChildAt(((ah) it).a());
            if (childAt != null) {
                arrayList.add(childAt);
            }
        }
        ArrayList arrayList2 = new ArrayList();
        for (Object next : arrayList) {
            if (((View) next).getId() != R.id.topAreaTabBarViewBackground) {
                arrayList2.add(next);
            }
        }
        return arrayList2;
    }

    public static final <K, V> Map<K, V> a(h... hVarArr) {
        j.b(hVarArr, "pairs");
        ArrayList arrayList = new ArrayList();
        for (h hVar : hVarArr) {
            if (hVar != null) {
                arrayList.add(hVar);
            }
        }
        return ai.a(arrayList);
    }

    public static final /* synthetic */ kotlin.m a(Fragment fragment, g.a aVar, boolean z) {
        if (!(fragment instanceof g)) {
            fragment = null;
        }
        g gVar = (g) fragment;
        if (gVar == null) {
            return null;
        }
        gVar.a(aVar, z);
        return kotlin.m.f5330a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.Window, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final void a(Activity activity) {
        j.b(activity, "$this$hideKeyboard");
        Object systemService = activity.getSystemService("input_method");
        if (systemService != null) {
            Window window = activity.getWindow();
            j.a((Object) window, "window");
            View decorView = window.getDecorView();
            j.a((Object) decorView, "window.decorView");
            View rootView = decorView.getRootView();
            j.a((Object) rootView, "window.decorView.rootView");
            ((InputMethodManager) systemService).hideSoftInputFromWindow(rootView.getWindowToken(), 0);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.view.inputmethod.InputMethodManager");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.EdgeAntialiasingImageView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.i.x1.j.a(android.widget.ImageView, java.lang.String, boolean, int):void
     arg types: [com.supercell.id.view.EdgeAntialiasingImageView, java.lang.String, int, int]
     candidates:
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, java.lang.Object, int):void
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, kotlin.d.a.b, int):void
      b.b.a.i.x1.j.a(android.widget.ImageView, java.lang.String, boolean, int):void */
    public static final void a(Context context, List<? extends View> list, int i, boolean z) {
        if (context != null) {
            int i2 = 0;
            for (T next : list) {
                int i3 = i2 + 1;
                if (i2 < 0) {
                    m.a();
                }
                View view = (View) next;
                view.setActivated(i2 == i);
                String str = null;
                if (i2 == i) {
                    String str2 = i2 != 0 ? i2 != 1 ? null : "tab_icon_id.png" : "tab_icon_star_blue.png";
                    if (str2 != null) {
                        EdgeAntialiasingImageView edgeAntialiasingImageView = (EdgeAntialiasingImageView) view.findViewById(R.id.tab_icon_left);
                        j.a((Object) edgeAntialiasingImageView, "view.tab_icon_left");
                        b.b.a.i.x1.j.a((ImageView) edgeAntialiasingImageView, str2, false, 2);
                    }
                    if (i2 == 0) {
                        str = "tab_icon_face_red.png";
                    } else if (i2 == 1) {
                        str = "tab_icon_gear.png";
                    }
                    if (str != null) {
                        EdgeAntialiasingImageView edgeAntialiasingImageView2 = (EdgeAntialiasingImageView) view.findViewById(R.id.tab_icon_right);
                        j.a((Object) edgeAntialiasingImageView2, "view.tab_icon_right");
                        b.b.a.i.x1.j.a((ImageView) edgeAntialiasingImageView2, str, false, 2);
                    }
                    ((RelativeLayout) view.findViewById(R.id.tab_icon)).setBackgroundResource(R.drawable.tab_icon_shadows);
                    view.setAlpha(1.0f);
                    if (z) {
                        EdgeAntialiasingImageView edgeAntialiasingImageView3 = (EdgeAntialiasingImageView) view.findViewById(R.id.tab_icon_left);
                        j.a((Object) edgeAntialiasingImageView3, "view.tab_icon_left");
                        EdgeAntialiasingImageView edgeAntialiasingImageView4 = (EdgeAntialiasingImageView) view.findViewById(R.id.tab_icon_right);
                        j.a((Object) edgeAntialiasingImageView4, "view.tab_icon_right");
                        a(edgeAntialiasingImageView3, edgeAntialiasingImageView4, 0, 4);
                    }
                } else {
                    String str3 = i2 != 0 ? i2 != 1 ? null : "tab_icon_id_disabled.png" : "tab_icon_star_blue_disabled.png";
                    if (str3 != null) {
                        EdgeAntialiasingImageView edgeAntialiasingImageView5 = (EdgeAntialiasingImageView) view.findViewById(R.id.tab_icon_left);
                        j.a((Object) edgeAntialiasingImageView5, "view.tab_icon_left");
                        b.b.a.i.x1.j.a((ImageView) edgeAntialiasingImageView5, str3, false, 2);
                    }
                    String str4 = i2 != 0 ? i2 != 1 ? null : "tab_icon_gear_disabled.png" : "tab_icon_face_disabled.png";
                    if (str4 != null) {
                        EdgeAntialiasingImageView edgeAntialiasingImageView6 = (EdgeAntialiasingImageView) view.findViewById(R.id.tab_icon_right);
                        j.a((Object) edgeAntialiasingImageView6, "view.tab_icon_right");
                        b.b.a.i.x1.j.a((ImageView) edgeAntialiasingImageView6, str4, false, 2);
                    }
                    ViewCompat.setBackground((RelativeLayout) view.findViewById(R.id.tab_icon), null);
                    view.setAlpha(0.3f);
                }
                i2 = i3;
            }
        }
    }

    public static /* synthetic */ void a(Context context, List list, int i, boolean z, int i2) {
        if ((i2 & 8) != 0) {
            z = false;
        }
        a(context, list, i, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final void a(Context context, List<? extends View> list, ViewPager viewPager, kotlin.d.a.c<? super View, ? super Integer, kotlin.m> cVar) {
        if (viewPager != null) {
            viewPager.addOnPageChangeListener(new b.b.a.i.s1.j(context, list));
        }
        int i = 0;
        for (T next : list) {
            int i2 = i + 1;
            if (i < 0) {
                m.a();
            }
            View view = (View) next;
            String str = i != 0 ? i != 1 ? null : "account_settings_tab" : "account_games_tab";
            if (str != null) {
                TextView textView = (TextView) view.findViewById(R.id.tab_title);
                j.a((Object) textView, "view.tab_title");
                b.b.a.i.x1.j.a(textView, str, (kotlin.d.a.b) null, 2);
            }
            view.setOnTouchListener(new i(view, i, cVar));
            i = i2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.EdgeAntialiasingImageView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final void a(Context context, List<? extends View> list, List<c1> list2, int i, boolean z) {
        j.b(list, "tabViews");
        j.b(list2, "tabData");
        if (context != null) {
            int i2 = 0;
            for (T next : list) {
                int i3 = i2 + 1;
                if (i2 < 0) {
                    m.a();
                }
                View view = (View) next;
                view.setActivated(i2 == i);
                if (i2 == i && z) {
                    EdgeAntialiasingImageView edgeAntialiasingImageView = (EdgeAntialiasingImageView) view.findViewById(R.id.tab_icon_left);
                    j.a((Object) edgeAntialiasingImageView, "view.tab_icon_left");
                    EdgeAntialiasingImageView edgeAntialiasingImageView2 = (EdgeAntialiasingImageView) view.findViewById(R.id.tab_icon_right);
                    j.a((Object) edgeAntialiasingImageView2, "view.tab_icon_right");
                    a(edgeAntialiasingImageView, edgeAntialiasingImageView2, 0, 4);
                }
                i2 = i3;
            }
            a(list, list2);
        }
    }

    public static final void a(Context context, List<? extends View> list, kotlin.d.a.a<? extends List<c1>> aVar, ViewPager viewPager, kotlin.d.a.c<? super View, ? super Integer, kotlin.m> cVar) {
        j.b(list, "tabViews");
        j.b(aVar, "tabData");
        if (context != null) {
            if (viewPager != null) {
                viewPager.addOnPageChangeListener(new i1(context, list, aVar));
            }
            int i = 0;
            for (T next : list) {
                int i2 = i + 1;
                if (i < 0) {
                    m.a();
                }
                View view = (View) next;
                view.setOnTouchListener(new h1(view, i, cVar));
                i = i2;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.graphics.drawable.Drawable, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final void a(View view, int i, float f, float f2, float f3, g.b bVar) {
        j.b(view, "$this$addDropShadow");
        j.b(bVar, "shape");
        float f4 = f * 1.4f;
        b.b.a.g.g gVar = b.b.a.g.g.f80b;
        Resources resources = view.getResources();
        j.a((Object) resources, "resources");
        Drawable a2 = gVar.a(resources, i, f4, f3, bVar);
        if (a2 != null && !(view.getBackground() instanceof b.b.a.g.b)) {
            float f5 = f4 / 2.0f;
            int a3 = a.a(f5);
            int i2 = b.b.a.g.c.f70a[bVar.ordinal()];
            int a4 = (i2 == 1 || i2 == 2) ? a.a(f5 - f2) : 0;
            int i3 = b.b.a.g.c.f71b[bVar.ordinal()];
            int a5 = (i3 == 1 || i3 == 2) ? a.a(f5 + f2) : 0;
            Drawable background = view.getBackground();
            j.a((Object) background, "this.background");
            ViewCompat.setBackground(view, new b.b.a.g.b(background, a2, a3, a3, a4, a5));
        }
    }

    public static /* synthetic */ void a(View view, int i, float f, float f2, float f3, g.b bVar, int i2) {
        int i3 = (i2 & 1) != 0 ? 335544320 : i;
        if ((i2 & 2) != 0) {
            f = b.b.a.g.b.f.a();
        }
        float f4 = f;
        if ((i2 & 4) != 0) {
            f2 = b.b.a.g.b.f.c();
        }
        float f5 = f2;
        if ((i2 & 8) != 0) {
            f3 = b.b.a.g.b.f.b();
        }
        float f6 = f3;
        if ((i2 & 16) != 0) {
            bVar = g.b.FULL;
        }
        a(view, i3, f4, f5, f6, bVar);
    }

    public static final void a(View view, String str, h<String, String>... hVarArr) {
        j.b(str, "titleKey");
        j.b(hVarArr, "replacements");
        if (!(view instanceof SubPageTabLayout)) {
            view = null;
        }
        SubPageTabLayout subPageTabLayout = (SubPageTabLayout) view;
        if (subPageTabLayout != null) {
            subPageTabLayout.setGetTitleKey(new e1(str, hVarArr));
            subPageTabLayout.setGetTitleReplacements(new f1(str, hVarArr));
            subPageTabLayout.a();
        }
    }

    public static final void a(View view, boolean z, boolean z2, int i, int i2) {
        int i3;
        g.b bVar;
        float f;
        float f2;
        float f3;
        int i4;
        if (view != null) {
            if (!z || !z2) {
                if (z) {
                    view.setBackgroundResource(R.drawable.list_container_top_pressable);
                    bVar = g.b.TOP;
                } else if (z2) {
                    view.setBackgroundResource(R.drawable.list_container_bottom_pressable);
                    bVar = g.b.BOTTOM;
                } else {
                    view.setBackgroundResource(R.drawable.list_container_middle_pressable);
                    bVar = g.b.MIDDLE;
                }
                i4 = 0;
                f3 = 0.0f;
                f2 = 0.0f;
                f = 0.0f;
                i3 = 15;
            } else {
                view.setBackgroundResource(R.drawable.list_container_pressable);
                i4 = 0;
                f3 = 0.0f;
                f2 = 0.0f;
                f = 0.0f;
                bVar = null;
                i3 = 31;
            }
            a(view, i4, f3, f2, f, bVar, i3);
            ViewGroup.MarginLayoutParams d = q1.d(view);
            if (d != null) {
                if (!z) {
                    i = 0;
                }
                d.topMargin = i;
            }
            ViewGroup.MarginLayoutParams d2 = q1.d(view);
            if (d2 != null) {
                if (!z2) {
                    i2 = 0;
                }
                d2.bottomMargin = i2;
            }
            view.setPadding(0, 0, 0, z2 ? view.getResources().getDimensionPixelSize(R.dimen.floating_element_thickness) : 0);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final void a(MainActivity mainActivity, View view) {
        j.b(mainActivity, "$this$showMyCodeInfoDialogPopup");
        j.b(view, "view");
        y.a aVar = y.h;
        Rect b2 = q1.b(view);
        int i = R.layout.my_code_info_dialog_content;
        Resources resources = mainActivity.getResources();
        j.a((Object) resources, "resources");
        mainActivity.a(aVar.a(b2, i, b(resources)), "popupDialog");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.supercell.id.view.ExpandableFrameLayout.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.supercell.id.view.ExpandableFrameLayout.a(com.supercell.id.view.ExpandableFrameLayout, float):void
      com.supercell.id.view.ExpandableFrameLayout.a(boolean, boolean):void */
    public static final void a(MainActivity mainActivity, IdAccount idAccount) {
        Object obj;
        j.b(mainActivity, "$this$forgetAccount");
        j.b(idAccount, "account");
        LinearLayout linearLayout = (LinearLayout) mainActivity.findViewById(R.id.profileList);
        if (linearLayout == null) {
            return;
        }
        if (linearLayout.getChildCount() > 1) {
            c b2 = d.b(0, linearLayout.getChildCount());
            ArrayList arrayList = new ArrayList();
            Iterator it = b2.iterator();
            while (it.hasNext()) {
                View childAt = linearLayout.getChildAt(((ah) it).a());
                if (childAt != null) {
                    arrayList.add(childAt);
                }
            }
            Iterator it2 = arrayList.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it2.next();
                if (j.a(((View) obj).getTag(R.id.profileAccountView), idAccount)) {
                    break;
                }
            }
            View view = (View) obj;
            if (view != null) {
                if (!(view instanceof ExpandableFrameLayout)) {
                    view = null;
                }
                ExpandableFrameLayout expandableFrameLayout = (ExpandableFrameLayout) view;
                if (expandableFrameLayout != null) {
                    expandableFrameLayout.setOnStateChangeListener(new f(expandableFrameLayout, linearLayout));
                    expandableFrameLayout.a(false, true);
                    return;
                }
                return;
            }
            return;
        }
        mainActivity.a(new p.a(null, null, null, 7));
    }

    public static final void a(kotlin.d.a.a<kotlin.m> aVar) {
        j.b(aVar, NativeProtocol.WEB_DIALOG_ACTION);
        Looper mainLooper = Looper.getMainLooper();
        if (j.a(Looper.myLooper(), mainLooper)) {
            aVar.invoke();
        } else {
            new Handler(mainLooper).post(new m1(aVar));
        }
    }

    public static final boolean a(Context context) {
        String[] strArr;
        j.b(context, "$this$canShowScanCode");
        if (!context.getPackageManager().hasSystemFeature("android.hardware.camera")) {
            return false;
        }
        if (Build.VERSION.SDK_INT < 23) {
            return context.getPackageManager().checkPermission("android.permission.CAMERA", context.getPackageName()) == 0;
        }
        PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 4096);
        if (packageInfo == null || (strArr = packageInfo.requestedPermissions) == null || !kotlin.a.g.b(strArr, "android.permission.CAMERA")) {
            return false;
        }
    }

    public static final boolean a(Resources resources) {
        j.b(resources, "$this$isLandscape");
        return resources.getConfiguration().orientation == 2;
    }

    public static final boolean a(Layout layout) {
        boolean z;
        Iterator it = d.b(0, layout.getLineCount() - 1).iterator();
        while (it.hasNext()) {
            if (layout.getText().charAt(layout.getLineEnd(((ah) it).a()) - 1) != ' ') {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                return true;
            }
        }
        return false;
    }

    public static final boolean a(List<? extends r0> list, int i) {
        j.b(list, "$this$roundBottomCorners");
        return i == list.size() - 1 || !(list.get(i + 1) instanceof k);
    }

    public static final float b(Layout layout) {
        c b2 = d.b(0, layout.getLineCount());
        ArrayList arrayList = new ArrayList(m.a(b2, 10));
        Iterator it = b2.iterator();
        while (it.hasNext()) {
            arrayList.add(Float.valueOf(layout.getLineWidth(((ah) it).a())));
        }
        Float l = m.l(arrayList);
        if (l != null) {
            return l.floatValue();
        }
        return 0.0f;
    }

    public static final void b(RecyclerView recyclerView, int i) {
        j.b(recyclerView, "$this$scrollCenterToPosition");
        q1.a(recyclerView, new o0(recyclerView, i));
    }

    public static final void b(MainActivity mainActivity) {
        int i;
        List<b.a> a2 = mainActivity.a();
        ListIterator<b.a> listIterator = a2.listIterator(a2.size());
        while (true) {
            if (listIterator.hasPrevious()) {
                if (listIterator.previous() instanceof h.a) {
                    i = listIterator.nextIndex();
                    break;
                }
            } else {
                i = -1;
                break;
            }
        }
        if (i > -1) {
            Object[] array = m.d(mainActivity.a(), i + 1).toArray(new b.a[0]);
            if (array != null) {
                b.a[] aVarArr = (b.a[]) array;
                mainActivity.a((b.a[]) Arrays.copyOf(aVarArr, aVarArr.length));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.Window, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final boolean b(Activity activity) {
        j.b(activity, "$this$isFullscreen");
        Window window = activity.getWindow();
        j.a((Object) window, "window");
        int i = window.getAttributes().flags;
        return (i | 1024) == i;
    }

    public static final /* synthetic */ boolean b(String str) {
        try {
            new URL(str);
            return true;
        } catch (MalformedURLException unused) {
            return false;
        }
    }

    public static final boolean b(List<? extends r0> list, int i) {
        j.b(list, "$this$roundTopCorners");
        return i == 0 || !(list.get(i - 1) instanceof k);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0015, code lost:
        if (r1 != null) goto L_0x001d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void c(com.supercell.id.ui.MainActivity r3) {
        /*
            r0 = 1
            b.b.a.i.b$a[] r0 = new b.b.a.i.b.a[r0]
            java.util.List r1 = r3.a()
            java.lang.Object r1 = kotlin.a.m.d(r1)
            b.b.a.i.b$a r1 = (b.b.a.i.b.a) r1
            if (r1 == 0) goto L_0x0018
            boolean r2 = r1 instanceof b.b.a.i.p1.i.a
            if (r2 == 0) goto L_0x0014
            goto L_0x0015
        L_0x0014:
            r1 = 0
        L_0x0015:
            if (r1 == 0) goto L_0x0018
            goto L_0x001d
        L_0x0018:
            b.b.a.i.p1.i$a r1 = new b.b.a.i.p1.i$a
            r1.<init>()
        L_0x001d:
            r2 = 0
            r0[r2] = r1
            r3.a(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.b.c(com.supercell.id.ui.MainActivity):void");
    }

    public static final boolean c(Resources resources) {
        j.b(resources, "$this$isSortOfATablet");
        return resources.getBoolean(R.bool.isSortOfATablet);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0015, code lost:
        if (r1 != null) goto L_0x001d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void d(com.supercell.id.ui.MainActivity r3) {
        /*
            r0 = 1
            b.b.a.i.b$a[] r0 = new b.b.a.i.b.a[r0]
            java.util.List r1 = r3.a()
            java.lang.Object r1 = kotlin.a.m.d(r1)
            b.b.a.i.b$a r1 = (b.b.a.i.b.a) r1
            if (r1 == 0) goto L_0x0018
            boolean r2 = r1 instanceof b.b.a.i.r1.n.a
            if (r2 == 0) goto L_0x0014
            goto L_0x0015
        L_0x0014:
            r1 = 0
        L_0x0015:
            if (r1 == 0) goto L_0x0018
            goto L_0x001d
        L_0x0018:
            b.b.a.i.r1.n$a r1 = new b.b.a.i.r1.n$a
            r1.<init>()
        L_0x001d:
            r2 = 0
            r0[r2] = r1
            r3.a(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.b.d(com.supercell.id.ui.MainActivity):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0015, code lost:
        if (r1 != null) goto L_0x001d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void e(com.supercell.id.ui.MainActivity r3) {
        /*
            r0 = 1
            b.b.a.i.b$a[] r0 = new b.b.a.i.b.a[r0]
            java.util.List r1 = r3.a()
            java.lang.Object r1 = kotlin.a.m.d(r1)
            b.b.a.i.b$a r1 = (b.b.a.i.b.a) r1
            if (r1 == 0) goto L_0x0018
            boolean r2 = r1 instanceof b.b.a.i.z1.b.a
            if (r2 == 0) goto L_0x0014
            goto L_0x0015
        L_0x0014:
            r1 = 0
        L_0x0015:
            if (r1 == 0) goto L_0x0018
            goto L_0x001d
        L_0x0018:
            b.b.a.i.z1.b$a r1 = new b.b.a.i.z1.b$a
            r1.<init>()
        L_0x001d:
            r2 = 0
            r0[r2] = r1
            r3.a(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.b.e(com.supercell.id.ui.MainActivity):void");
    }

    public static final /* synthetic */ bw a(Fragment fragment, g.b bVar, boolean z) {
        if (!(fragment instanceof b.b.a.i.g)) {
            fragment = null;
        }
        b.b.a.i.g gVar = (b.b.a.i.g) fragment;
        if (gVar != null) {
            return gVar.a(bVar, z);
        }
        return null;
    }

    public static final String b(n nVar) {
        j.b(nVar, "$this$comparableName");
        b.b.a.i.x1.f fVar = SupercellId.INSTANCE.getSharedServices$supercellId_release().j;
        StringBuilder a2 = b.a.a.a.a.a("game_name_");
        a2.append(nVar.f135a);
        String b2 = fVar.b(a2.toString());
        return b2 != null ? b2 : "";
    }

    public static final /* synthetic */ void a(MainActivity mainActivity) {
        SupercellId.INSTANCE.setTutorialComplete$supercellId_release();
        if (mainActivity.b() == 1 && SupercellId.INSTANCE.getSharedServices$supercellId_release().i == null) {
            mainActivity.a(new a.C0020a());
            return;
        }
        mainActivity.d();
    }

    public static final /* synthetic */ bw a(b.b.a.i.x1.f fVar, String str) {
        ap a2 = bb.f5389a;
        fVar.a(str, new b.b.a.g.n(a2, str));
        return a2.i();
    }

    public static final boolean b(b.b.a.i.g gVar) {
        c.a aVar = (c.a) a(gVar);
        if (aVar != null) {
            return aVar.g;
        }
        return false;
    }

    public static final /* synthetic */ boolean a(String str) {
        try {
            new URL(str);
            return true;
        } catch (MalformedURLException unused) {
            return false;
        }
    }

    public static final boolean b(Resources resources) {
        j.b(resources, "$this$isMobileLandscape");
        if (a(resources)) {
            j.b(resources, "$this$isSortOfATablet");
            if (!resources.getBoolean(R.bool.isSortOfATablet)) {
                return true;
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.Context, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.text.SpannableStringBuilder, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final /* synthetic */ void a(TextView textView, String str, String str2, BitmapDrawable bitmapDrawable) {
        SpannableStringBuilder spannableStringBuilder;
        kotlin.h[] hVarArr = new kotlin.h[2];
        SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder();
        if (str == null) {
            str = "";
        }
        Context context = textView.getContext();
        j.a((Object) context, "context");
        hVarArr[0] = kotlin.k.a("player", a(spannableStringBuilder2, str, kotlin.k.a(b.b.a.j.p.a(context, "fonts/SupercellTextAndroid_ACorp_Bd.ttf"), 33), kotlin.k.a(new ForegroundColorSpan(ContextCompat.getColor(textView.getContext(), R.color.black)), 33)));
        SpannableStringBuilder spannableStringBuilder3 = new SpannableStringBuilder();
        if (bitmapDrawable == null || (spannableStringBuilder = a(new SpannableStringBuilder(), " ", new b.b.a.j.h(bitmapDrawable), 33).append((CharSequence) " ")) == null) {
            spannableStringBuilder = "";
        }
        SpannableStringBuilder append = spannableStringBuilder3.append((CharSequence) spannableStringBuilder);
        j.a((Object) append, "SpannableStringBuilder()…                 } ?: \"\")");
        if (str2 == null) {
            str2 = "";
        }
        Context context2 = textView.getContext();
        j.a((Object) context2, "context");
        hVarArr[1] = kotlin.k.a("game", a(append, str2, kotlin.k.a(b.b.a.j.p.a(context2, "fonts/SupercellTextAndroid_ACorp_Bd.ttf"), 33), kotlin.k.a(new ForegroundColorSpan(ContextCompat.getColor(textView.getContext(), R.color.black)), 33)));
        b.b.a.i.x1.j.a(textView, "ingame_notification_message", hVarArr);
    }

    public static final void b(MainActivity mainActivity, View view) {
        j.b(mainActivity, "$this$showRememberMeInfoDialogPopup");
        j.b(view, "view");
        mainActivity.a(y.h.a(q1.b(view), R.layout.remember_me_info_dialog_content, false), "popupDialog");
    }

    public static final void a(TextView textView, String str, Rect rect) {
        j.b(textView, "$this$fetchCompoundDrawableStart");
        j.b(str, "assetName");
        SupercellId.INSTANCE.getSharedServices$supercellId_release().j.a(str, new j1(new WeakReference(textView), rect));
    }

    public static final String a(n nVar) {
        j.b(nVar, "$this$analyticsName");
        b.b.a.i.x1.f fVar = SupercellId.INSTANCE.getSharedServices$supercellId_release().j;
        StringBuilder a2 = b.a.a.a.a.a("game_name_");
        a2.append(nVar.f135a);
        String a3 = fVar.a(a2.toString());
        return a3 != null ? a3 : nVar.f135a;
    }

    public static final String a(b.b.a.h.b bVar) {
        j.b(bVar, "$this$comparableName");
        b.b.a.i.x1.f fVar = SupercellId.INSTANCE.getSharedServices$supercellId_release().j;
        StringBuilder a2 = b.a.a.a.a.a("game_name_");
        a2.append(bVar.f108a);
        String b2 = fVar.b(a2.toString());
        return b2 != null ? b2 : "";
    }

    public static /* synthetic */ void a(ImageView imageView, ImageView imageView2, int i, int i2) {
        if ((i2 & 4) != 0) {
            i = 13;
        }
        j.b(imageView, "left");
        j.b(imageView2, "right");
        AnimatorSet animatorSet = new AnimatorSet();
        Animator a2 = a(imageView2, i);
        a2.setStartDelay(40);
        animatorSet.playTogether(a(imageView, i), a2);
        animatorSet.setStartDelay(20);
        animatorSet.start();
    }

    public static /* synthetic */ bw a(bw bwVar, ao aoVar, kotlin.d.a.b bVar, int i) {
        if ((i & 1) != 0) {
            aoVar = bwVar.h();
        }
        j.b(bwVar, "$this$mapFail");
        j.b(aoVar, "context");
        j.b(bVar, "transform");
        if (bwVar.d()) {
            if (bwVar.f()) {
                bw.a aVar = bw.d;
                return bw.a.a(bwVar.a(), aoVar);
            } else if (bwVar.e()) {
                try {
                    bw.a aVar2 = bw.d;
                    return bw.a.a(bVar.invoke(bwVar.b()), aoVar);
                } catch (Exception e) {
                    bw.a aVar3 = bw.d;
                    return bw.a.b(e, aoVar);
                }
            }
        }
        ap a2 = bc.a(aoVar);
        bwVar.a(new m0(a2));
        bwVar.b(new n0(a2, bVar));
        return a2.i();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.a(java.util.List, int):T
     arg types: [java.util.List<b.b.a.j.c1>, int]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.w.a(java.util.List, int):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [com.supercell.id.view.EdgeAntialiasingImageView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: b.b.a.i.x1.j.a(android.widget.ImageView, java.lang.String, boolean, int):void
     arg types: [com.supercell.id.view.EdgeAntialiasingImageView, java.lang.String, int, int]
     candidates:
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, java.lang.Object, int):void
      b.b.a.i.x1.j.a(android.widget.TextView, java.lang.String, kotlin.d.a.b, int):void
      b.b.a.i.x1.j.a(android.widget.ImageView, java.lang.String, boolean, int):void */
    public static final void a(List<? extends View> list, List<c1> list2) {
        int size = list2.size();
        int i = 0;
        for (T next : list) {
            int i2 = i + 1;
            if (i < 0) {
                m.a();
            }
            View view = (View) next;
            if (i < size) {
                String str = null;
                if (view.isActivated()) {
                    c1 c1Var = (c1) m.a((List) list2, i);
                    String str2 = c1Var != null ? c1Var.f1022b : null;
                    if (str2 != null) {
                        EdgeAntialiasingImageView edgeAntialiasingImageView = (EdgeAntialiasingImageView) view.findViewById(R.id.tab_icon_left);
                        j.a((Object) edgeAntialiasingImageView, "view.tab_icon_left");
                        b.b.a.i.x1.j.a((ImageView) edgeAntialiasingImageView, str2, false, 2);
                    }
                    c1 c1Var2 = (c1) m.a((List) list2, i);
                    if (c1Var2 != null) {
                        str = c1Var2.c;
                    }
                    if (str != null) {
                        EdgeAntialiasingImageView edgeAntialiasingImageView2 = (EdgeAntialiasingImageView) view.findViewById(R.id.tab_icon_right);
                        j.a((Object) edgeAntialiasingImageView2, "view.tab_icon_right");
                        b.b.a.i.x1.j.a((ImageView) edgeAntialiasingImageView2, str, false, 2);
                    }
                    ((RelativeLayout) view.findViewById(R.id.tab_icon)).setBackgroundResource(R.drawable.tab_icon_shadows);
                    view.setAlpha(1.0f);
                } else {
                    c1 c1Var3 = (c1) m.a((List) list2, i);
                    String str3 = c1Var3 != null ? c1Var3.d : null;
                    if (str3 != null) {
                        EdgeAntialiasingImageView edgeAntialiasingImageView3 = (EdgeAntialiasingImageView) view.findViewById(R.id.tab_icon_left);
                        j.a((Object) edgeAntialiasingImageView3, "view.tab_icon_left");
                        b.b.a.i.x1.j.a((ImageView) edgeAntialiasingImageView3, str3, false, 2);
                    }
                    c1 c1Var4 = (c1) m.a((List) list2, i);
                    String str4 = c1Var4 != null ? c1Var4.e : null;
                    if (str4 != null) {
                        EdgeAntialiasingImageView edgeAntialiasingImageView4 = (EdgeAntialiasingImageView) view.findViewById(R.id.tab_icon_right);
                        j.a((Object) edgeAntialiasingImageView4, "view.tab_icon_right");
                        b.b.a.i.x1.j.a((ImageView) edgeAntialiasingImageView4, str4, false, 2);
                    }
                    ViewCompat.setBackground((RelativeLayout) view.findViewById(R.id.tab_icon), null);
                    view.setAlpha(0.3f);
                }
            }
            i = i2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.a.w.a(java.util.List, int):T
     arg types: [java.util.List<b.b.a.j.c1>, int]
     candidates:
      kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
      kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
      kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
      kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
      kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
      kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
      kotlin.a.s.a(java.util.List, java.util.Comparator):void
      kotlin.a.p.a(java.lang.Iterable, int):int
      kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
      kotlin.a.w.a(java.util.List, int):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.content.res.Resources, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static final void a(Context context, List<? extends View> list, List<c1> list2) {
        String str;
        j.b(list, "tabViews");
        j.b(list2, "tabData");
        if (context != null) {
            int size = list2.size();
            int i = 0;
            for (T next : list) {
                int i2 = i + 1;
                if (i < 0) {
                    m.a();
                }
                View view = (View) next;
                if (i < size) {
                    view.setVisibility(0);
                    c1 c1Var = (c1) m.a((List) list2, i);
                    if (!(c1Var == null || (str = c1Var.f1021a) == null)) {
                        TextView textView = (TextView) view.findViewById(R.id.tab_title);
                        j.a((Object) textView, "view.tab_title");
                        b.b.a.i.x1.j.a(textView, str, (kotlin.d.a.b) null, 2);
                    }
                    Resources resources = context.getResources();
                    j.a((Object) resources, "context.resources");
                    if (!b(resources)) {
                        view.setBackgroundResource(size == 1 ? R.drawable.tab_button_single : i == 0 ? R.drawable.tab_button_start : i == size + -1 ? R.drawable.tab_button_end : R.drawable.tab_button_middle);
                    }
                } else {
                    view.setVisibility(8);
                }
                i = i2;
            }
            a(list, list2);
        }
    }

    public static final <T> ArrayList<T> a(ArrayList<T> arrayList, int i) {
        j.b(arrayList, "$this$dropLast");
        j.b(arrayList, "$this$dropLast");
        if (i >= 0) {
            List d = m.d(arrayList, d.c(arrayList.size() - i, 0));
            ArrayList<T> arrayList2 = (ArrayList) (!(d instanceof ArrayList) ? null : d);
            return arrayList2 != null ? arrayList2 : new ArrayList<>(d);
        }
        throw new IllegalArgumentException(("Requested element count " + i + " is less than zero.").toString());
    }

    public static final <K, V> Map<K, V> a(Map<K, ? extends V> map, Map<K, ? extends V> map2) {
        j.b(map, "$this$plusOptional");
        if (map2 == null) {
            return map;
        }
        j.b(map, "$this$plus");
        j.b(map2, "map");
        LinkedHashMap linkedHashMap = new LinkedHashMap(map);
        linkedHashMap.putAll(map2);
        return linkedHashMap;
    }

    public static final <K, V> Map<K, V> b(kotlin.h<? extends K, ? extends V>... hVarArr) {
        j.b(hVarArr, "pairs");
        ArrayList arrayList = new ArrayList();
        for (kotlin.h<? extends K, ? extends V> hVar : hVarArr) {
            B b2 = hVar.f5263b;
            kotlin.h a2 = b2 != null ? kotlin.k.a(hVar.f5262a, b2) : null;
            if (a2 != null) {
                arrayList.add(a2);
            }
        }
        return ai.a(arrayList);
    }

    public static final SpannableStringBuilder a(SpannableStringBuilder spannableStringBuilder, CharSequence charSequence, kotlin.h<? extends Object, Integer>... hVarArr) {
        j.b(spannableStringBuilder, "$this$appendText");
        j.b(charSequence, "text");
        j.b(hVarArr, "what");
        int length = spannableStringBuilder.length();
        spannableStringBuilder.append(charSequence);
        for (kotlin.h<? extends Object, Integer> hVar : hVarArr) {
            spannableStringBuilder.setSpan(hVar.f5262a, length, spannableStringBuilder.length(), ((Number) hVar.f5263b).intValue());
        }
        return spannableStringBuilder;
    }
}
