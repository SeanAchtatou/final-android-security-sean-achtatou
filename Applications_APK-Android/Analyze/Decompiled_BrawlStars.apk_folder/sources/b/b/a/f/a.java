package b.b.a.f;

import android.support.v4.view.animation.PathInterpolatorCompat;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import kotlin.d.b.j;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    public static final Interpolator f62a;

    /* renamed from: b  reason: collision with root package name */
    public static final Interpolator f63b;
    public static final Interpolator c;
    public static final Interpolator d;
    public static final Interpolator e;
    public static final Interpolator f;
    public static final Interpolator g;
    public static final LinearInterpolator h = new LinearInterpolator();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.animation.Interpolator, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    static {
        Interpolator create = PathInterpolatorCompat.create(0.42f, 0.0f, 0.58f, 1.0f);
        j.a((Object) create, "PathInterpolatorCompat.create(.42f, 0f, .58f, 1f)");
        f62a = create;
        Interpolator create2 = PathInterpolatorCompat.create(0.7f, 0.0f, 0.3f, 1.0f);
        j.a((Object) create2, "PathInterpolatorCompat.create(.7f, 0f, .3f, 1f)");
        f63b = create2;
        Interpolator create3 = PathInterpolatorCompat.create(0.0f, 0.0f, 0.2f, 1.0f);
        j.a((Object) create3, "PathInterpolatorCompat.create(0f, 0f, .2f, 1f)");
        c = create3;
        Interpolator create4 = PathInterpolatorCompat.create(0.6f, 0.62f, 0.0f, 1.0f);
        j.a((Object) create4, "PathInterpolatorCompat.create(.6f, .62f, 0f, 1f)");
        d = create4;
        Interpolator create5 = PathInterpolatorCompat.create(0.0f, 1.0f, 0.0f, 1.0f);
        j.a((Object) create5, "PathInterpolatorCompat.create(0f, 1f, 0f, 1f)");
        e = create5;
        Interpolator create6 = PathInterpolatorCompat.create(0.6f, 3.5f, 0.55f, 1.0f);
        j.a((Object) create6, "PathInterpolatorCompat.create(.6f, 3.5f, .55f, 1f)");
        f = create6;
        Interpolator create7 = PathInterpolatorCompat.create(0.6f, 1.7f, 0.55f, 1.0f);
        j.a((Object) create7, "PathInterpolatorCompat.create(.6f, 1.7f, .55f, 1f)");
        g = create7;
    }

    public static final Interpolator a() {
        return e;
    }
}
