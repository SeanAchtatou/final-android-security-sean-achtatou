package b.b.a.h;

import kotlin.d.b.g;
import kotlin.d.b.j;

public final class c {
    public static final a g = new a(null);

    /* renamed from: a  reason: collision with root package name */
    public final String f110a;

    /* renamed from: b  reason: collision with root package name */
    public final String f111b;
    public final String c;
    public final g d;
    public final j e;
    public final int f;

    public static final class a {
        public a() {
        }

        public /* synthetic */ a(g gVar) {
        }

        /* JADX WARNING: Removed duplicated region for block: B:15:0x004f  */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0019 A[SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.util.List<b.b.a.h.c> a(org.json.JSONArray r7) {
            /*
                r6 = this;
                java.lang.String r0 = "JSON Parse error "
                java.lang.String r1 = "data"
                kotlin.d.b.j.b(r7, r1)
                int r1 = r7.length()
                r2 = 0
                kotlin.g.c r1 = kotlin.g.d.b(r2, r1)
                java.util.ArrayList r2 = new java.util.ArrayList
                r2.<init>()
                java.util.Iterator r1 = r1.iterator()
            L_0x0019:
                boolean r3 = r1.hasNext()
                if (r3 == 0) goto L_0x0053
                r3 = r1
                kotlin.a.ah r3 = (kotlin.a.ah) r3
                int r3 = r3.a()
                r4 = 0
                org.json.JSONObject r3 = r7.optJSONObject(r3)     // Catch:{ JSONException -> 0x003e, ParseException -> 0x0034 }
                if (r3 == 0) goto L_0x004d
                b.b.a.h.c r5 = new b.b.a.h.c     // Catch:{ JSONException -> 0x003e, ParseException -> 0x0034 }
                r5.<init>(r3)     // Catch:{ JSONException -> 0x003e, ParseException -> 0x0034 }
                r4 = r5
                goto L_0x004d
            L_0x0034:
                r3 = move-exception
                java.lang.StringBuilder r5 = b.a.a.a.a.a(r0)
                java.lang.String r3 = r3.getLocalizedMessage()
                goto L_0x0047
            L_0x003e:
                r3 = move-exception
                java.lang.StringBuilder r5 = b.a.a.a.a.a(r0)
                java.lang.String r3 = r3.getLocalizedMessage()
            L_0x0047:
                r5.append(r3)
                r5.toString()
            L_0x004d:
                if (r4 == 0) goto L_0x0019
                r2.add(r4)
                goto L_0x0019
            L_0x0053:
                return r2
            */
            throw new UnsupportedOperationException("Method not decompiled: b.b.a.h.c.a.a(org.json.JSONArray):java.util.List");
        }
    }

    public c(String str, String str2, String str3, g gVar, j jVar, int i) {
        j.b(str, "scid");
        j.b(jVar, "relationship");
        this.f110a = str;
        this.f111b = str2;
        this.c = str3;
        this.d = gVar;
        this.e = jVar;
        this.f = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public c(org.json.JSONObject r9) {
        /*
            r8 = this;
            java.lang.String r0 = "jsonObject"
            kotlin.d.b.j.b(r9, r0)
            java.lang.String r0 = "scid"
            java.lang.String r2 = r9.getString(r0)
            java.lang.String r0 = "jsonObject.getString(\"scid\")"
            kotlin.d.b.j.a(r2, r0)
            java.lang.String r0 = "name"
            java.lang.Object r0 = r9.opt(r0)
            r1 = 0
            if (r0 == 0) goto L_0x0021
            java.lang.Object r3 = org.json.JSONObject.NULL
            boolean r3 = kotlin.d.b.j.a(r0, r3)
            if (r3 == 0) goto L_0x0022
        L_0x0021:
            r0 = r1
        L_0x0022:
            if (r0 == 0) goto L_0x002c
            boolean r3 = r0 instanceof java.lang.String
            if (r3 == 0) goto L_0x002c
            java.lang.String r0 = (java.lang.String) r0
            r3 = r0
            goto L_0x002d
        L_0x002c:
            r3 = r1
        L_0x002d:
            java.lang.String r0 = "avatarImage"
            java.lang.Object r0 = r9.opt(r0)
            if (r0 == 0) goto L_0x003d
            java.lang.Object r4 = org.json.JSONObject.NULL
            boolean r4 = kotlin.d.b.j.a(r0, r4)
            if (r4 == 0) goto L_0x003e
        L_0x003d:
            r0 = r1
        L_0x003e:
            if (r0 == 0) goto L_0x0048
            boolean r4 = r0 instanceof java.lang.String
            if (r4 == 0) goto L_0x0048
            java.lang.String r0 = (java.lang.String) r0
            r4 = r0
            goto L_0x0049
        L_0x0048:
            r4 = r1
        L_0x0049:
            java.lang.String r0 = "presence"
            org.json.JSONObject r0 = r9.optJSONObject(r0)
            if (r0 == 0) goto L_0x0056
            b.b.a.h.g r1 = new b.b.a.h.g
            r1.<init>(r0)
        L_0x0056:
            r5 = r1
            java.lang.String r0 = "relationship"
            org.json.JSONObject r0 = r9.optJSONObject(r0)
            if (r0 == 0) goto L_0x0068
            b.b.a.h.j$c r1 = b.b.a.h.j.f124a
            b.b.a.h.j r0 = r1.a(r0)
            if (r0 == 0) goto L_0x0068
            goto L_0x006a
        L_0x0068:
            b.b.a.h.j$d r0 = b.b.a.h.j.d.f126b
        L_0x006a:
            r6 = r0
            r0 = 0
            java.lang.String r1 = "mutualFriends"
            int r7 = r9.optInt(r1, r0)
            r1 = r8
            r1.<init>(r2, r3, r4, r5, r6, r7)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.h.c.<init>(org.json.JSONObject):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof c) {
                c cVar = (c) obj;
                if (j.a((Object) this.f110a, (Object) cVar.f110a) && j.a((Object) this.f111b, (Object) cVar.f111b) && j.a((Object) this.c, (Object) cVar.c) && j.a(this.d, cVar.d) && j.a(this.e, cVar.e)) {
                    if (this.f == cVar.f) {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        String str = this.f110a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.f111b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        g gVar = this.d;
        int hashCode4 = (hashCode3 + (gVar != null ? gVar.hashCode() : 0)) * 31;
        j jVar = this.e;
        if (jVar != null) {
            i = jVar.hashCode();
        }
        return ((hashCode4 + i) * 31) + this.f;
    }

    public final String toString() {
        StringBuilder a2 = b.a.a.a.a.a("IdFriendInfo(scid=");
        a2.append(this.f110a);
        a2.append(", name=");
        a2.append(this.f111b);
        a2.append(", avatarImage=");
        a2.append(this.c);
        a2.append(", presence=");
        a2.append(this.d);
        a2.append(", relationship=");
        a2.append(this.e);
        a2.append(", mutualFriends=");
        a2.append(this.f);
        a2.append(")");
        return a2.toString();
    }
}
