package b.b.a.h;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.os.ParcelCompat;
import b.b.a.j.a0;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.facebook.share.internal.ShareConstants;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import kotlin.d.b.j;
import org.json.JSONArray;
import org.json.JSONObject;

public final class h implements a0 {
    public static final Parcelable.Creator<h> CREATOR = new a();

    /* renamed from: a  reason: collision with root package name */
    public final String f120a;

    /* renamed from: b  reason: collision with root package name */
    public final String f121b;
    public final Date c;
    public final String d;
    public final Date e;
    public final String f;
    public final String g;
    public final boolean h;
    public final List<n> i;
    public final List<b> j;

    public final class a implements Parcelable.Creator<h> {
        public final h createFromParcel(Parcel parcel) {
            j.b(parcel, ShareConstants.FEED_SOURCE_PARAM);
            j.b(parcel, "parcel");
            String readString = parcel.readString();
            if (readString == null) {
                j.a();
            }
            String readString2 = parcel.readString();
            Date date = new Date(parcel.readLong());
            String readString3 = parcel.readString();
            Date date2 = new Date(parcel.readLong());
            String readString4 = parcel.readString();
            String readString5 = parcel.readString();
            if (readString5 == null) {
                j.a();
            }
            boolean readBoolean = ParcelCompat.readBoolean(parcel);
            ArrayList createTypedArrayList = parcel.createTypedArrayList(n.CREATOR);
            if (createTypedArrayList == null) {
                j.a();
            }
            ArrayList createTypedArrayList2 = parcel.createTypedArrayList(b.CREATOR);
            if (createTypedArrayList2 == null) {
                j.a();
            }
            return new h(readString, readString2, date, readString3, date2, readString4, readString5, readBoolean, createTypedArrayList, createTypedArrayList2);
        }

        public final h[] newArray(int i) {
            return new h[i];
        }
    }

    public h(String str, String str2, Date date, String str3, Date date2, String str4, String str5, boolean z, List<n> list, List<b> list2) {
        j.b(str, "scid");
        j.b(date, "nameChangeAllowed");
        j.b(date2, "avatarChangeAllowed");
        j.b(str5, "universalLink");
        j.b(list, "availableSystems");
        j.b(list2, "connectedSystems");
        this.f120a = str;
        this.f121b = str2;
        this.c = date;
        this.d = str3;
        this.e = date2;
        this.f = str4;
        this.g = str5;
        this.h = z;
        this.i = list;
        this.j = list2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00c9  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x00ec  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public h(org.json.JSONObject r13) {
        /*
            r12 = this;
            java.lang.String r0 = "data"
            kotlin.d.b.j.b(r13, r0)
            java.lang.String r0 = "scid"
            java.lang.String r2 = r13.getString(r0)
            java.lang.String r0 = "data.getString(\"scid\")"
            kotlin.d.b.j.a(r2, r0)
            java.lang.String r0 = "name"
            org.json.JSONObject r1 = r13.optJSONObject(r0)
            r3 = 0
            if (r1 == 0) goto L_0x0032
            java.lang.Object r1 = r1.opt(r0)
            if (r1 == 0) goto L_0x0027
            java.lang.Object r4 = org.json.JSONObject.NULL
            boolean r4 = kotlin.d.b.j.a(r1, r4)
            if (r4 == 0) goto L_0x0028
        L_0x0027:
            r1 = r3
        L_0x0028:
            if (r1 == 0) goto L_0x0032
            boolean r4 = r1 instanceof java.lang.String
            if (r4 == 0) goto L_0x0032
            java.lang.String r1 = (java.lang.String) r1
            r4 = r1
            goto L_0x0033
        L_0x0032:
            r4 = r3
        L_0x0033:
            org.json.JSONObject r0 = r13.optJSONObject(r0)
            java.lang.String r1 = "changeAllowed"
            if (r0 == 0) goto L_0x0070
            java.lang.Object r0 = r0.opt(r1)
            if (r0 == 0) goto L_0x0049
            java.lang.Object r5 = org.json.JSONObject.NULL
            boolean r5 = kotlin.d.b.j.a(r0, r5)
            if (r5 == 0) goto L_0x004a
        L_0x0049:
            r0 = r3
        L_0x004a:
            if (r0 == 0) goto L_0x0063
            boolean r5 = r0 instanceof java.lang.Integer
            if (r5 == 0) goto L_0x005c
            java.lang.Number r0 = (java.lang.Number) r0
            int r0 = r0.intValue()
            long r5 = (long) r0
            java.lang.Long r0 = java.lang.Long.valueOf(r5)
            goto L_0x0064
        L_0x005c:
            boolean r5 = r0 instanceof java.lang.Long
            if (r5 == 0) goto L_0x0063
            java.lang.Long r0 = (java.lang.Long) r0
            goto L_0x0064
        L_0x0063:
            r0 = r3
        L_0x0064:
            if (r0 == 0) goto L_0x0070
            long r5 = r0.longValue()
            java.util.Date r0 = new java.util.Date
            r0.<init>(r5)
            goto L_0x0075
        L_0x0070:
            java.util.Date r0 = new java.util.Date
            r0.<init>()
        L_0x0075:
            java.lang.String r5 = "avatar"
            org.json.JSONObject r6 = r13.optJSONObject(r5)
            if (r6 == 0) goto L_0x0097
            java.lang.String r7 = "image"
            java.lang.Object r6 = r6.opt(r7)
            if (r6 == 0) goto L_0x008d
            java.lang.Object r7 = org.json.JSONObject.NULL
            boolean r7 = kotlin.d.b.j.a(r6, r7)
            if (r7 == 0) goto L_0x008e
        L_0x008d:
            r6 = r3
        L_0x008e:
            if (r6 == 0) goto L_0x0097
            boolean r7 = r6 instanceof java.lang.String
            if (r7 == 0) goto L_0x0097
            java.lang.String r6 = (java.lang.String) r6
            goto L_0x0098
        L_0x0097:
            r6 = r3
        L_0x0098:
            org.json.JSONObject r5 = r13.optJSONObject(r5)
            if (r5 == 0) goto L_0x00d3
            java.lang.Object r1 = r5.opt(r1)
            if (r1 == 0) goto L_0x00ac
            java.lang.Object r5 = org.json.JSONObject.NULL
            boolean r5 = kotlin.d.b.j.a(r1, r5)
            if (r5 == 0) goto L_0x00ad
        L_0x00ac:
            r1 = r3
        L_0x00ad:
            if (r1 == 0) goto L_0x00c6
            boolean r5 = r1 instanceof java.lang.Integer
            if (r5 == 0) goto L_0x00bf
            java.lang.Number r1 = (java.lang.Number) r1
            int r1 = r1.intValue()
            long r7 = (long) r1
            java.lang.Long r1 = java.lang.Long.valueOf(r7)
            goto L_0x00c7
        L_0x00bf:
            boolean r5 = r1 instanceof java.lang.Long
            if (r5 == 0) goto L_0x00c6
            java.lang.Long r1 = (java.lang.Long) r1
            goto L_0x00c7
        L_0x00c6:
            r1 = r3
        L_0x00c7:
            if (r1 == 0) goto L_0x00d3
            long r7 = r1.longValue()
            java.util.Date r1 = new java.util.Date
            r1.<init>(r7)
            goto L_0x00d8
        L_0x00d3:
            java.util.Date r1 = new java.util.Date
            r1.<init>()
        L_0x00d8:
            r7 = r1
            java.lang.String r1 = "qrCodeURL"
            java.lang.Object r1 = r13.opt(r1)
            if (r1 == 0) goto L_0x00e9
            java.lang.Object r5 = org.json.JSONObject.NULL
            boolean r5 = kotlin.d.b.j.a(r1, r5)
            if (r5 == 0) goto L_0x00ea
        L_0x00e9:
            r1 = r3
        L_0x00ea:
            if (r1 == 0) goto L_0x00f4
            boolean r5 = r1 instanceof java.lang.String
            if (r5 == 0) goto L_0x00f4
            java.lang.String r1 = (java.lang.String) r1
            r8 = r1
            goto L_0x00f5
        L_0x00f4:
            r8 = r3
        L_0x00f5:
            java.lang.String r1 = "universalLink"
            java.lang.String r9 = r13.getString(r1)
            java.lang.String r1 = "data.getString(\"universalLink\")"
            kotlin.d.b.j.a(r9, r1)
            java.lang.String r1 = "forcedOfflineStatus"
            boolean r10 = r13.getBoolean(r1)
            java.lang.String r1 = "availableSystems"
            org.json.JSONArray r1 = r13.getJSONArray(r1)
            b.b.a.h.n$b r3 = b.b.a.h.n.d
            java.util.List r11 = r3.a(r1)
            java.lang.String r1 = "connectedSystems"
            org.json.JSONArray r13 = r13.getJSONArray(r1)
            b.b.a.h.b$b r1 = b.b.a.h.b.e
            java.util.List r13 = r1.a(r13)
            r1 = r12
            r3 = r4
            r4 = r0
            r5 = r6
            r6 = r7
            r7 = r8
            r8 = r9
            r9 = r10
            r10 = r11
            r11 = r13
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.h.h.<init>(org.json.JSONObject):void");
    }

    public static /* synthetic */ h a(h hVar, String str, String str2, Date date, String str3, Date date2, String str4, String str5, boolean z, List list, List list2, int i2) {
        h hVar2 = hVar;
        int i3 = i2;
        return hVar.a((i3 & 1) != 0 ? hVar2.f120a : str, (i3 & 2) != 0 ? hVar2.f121b : str2, (i3 & 4) != 0 ? hVar2.c : date, (i3 & 8) != 0 ? hVar2.d : str3, (i3 & 16) != 0 ? hVar2.e : date2, (i3 & 32) != 0 ? hVar2.f : str4, (i3 & 64) != 0 ? hVar2.g : str5, (i3 & 128) != 0 ? hVar2.h : z, (i3 & 256) != 0 ? hVar2.i : list, (i3 & 512) != 0 ? hVar2.j : list2);
    }

    public final h a(String str, String str2, Date date, String str3, Date date2, String str4, String str5, boolean z, List<n> list, List<b> list2) {
        j.b(str, "scid");
        j.b(date, "nameChangeAllowed");
        Date date3 = date2;
        j.b(date3, "avatarChangeAllowed");
        String str6 = str5;
        j.b(str6, "universalLink");
        List<n> list3 = list;
        j.b(list3, "availableSystems");
        List<b> list4 = list2;
        j.b(list4, "connectedSystems");
        return new h(str, str2, date, str3, date3, str4, str6, z, list3, list4);
    }

    public final boolean a() {
        return new Date().compareTo(this.c) >= 0;
    }

    public final boolean b() {
        String str = this.f121b;
        if (!(str == null || str.length() == 0)) {
            String str2 = this.d;
            return str2 == null || str2.length() == 0;
        }
    }

    public final JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("scid", this.f120a);
        String str = this.f121b;
        if (str != null) {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("name", str);
            jSONObject2.put("changeAllowed", this.c.getTime());
            jSONObject.put("name", jSONObject2);
        }
        String str2 = this.d;
        if (str2 != null) {
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put(MessengerShareContentUtility.MEDIA_IMAGE, str2);
            jSONObject3.put("changeAllowed", this.e.getTime());
            jSONObject.put("avatar", jSONObject3);
        }
        jSONObject.putOpt("qrCodeURL", this.f);
        jSONObject.put("universalLink", this.g);
        jSONObject.put("forcedOfflineStatus", this.h);
        JSONArray jSONArray = new JSONArray();
        for (n b2 : this.i) {
            jSONArray.put(b2.b());
        }
        jSONObject.put("availableSystems", jSONArray);
        JSONArray jSONArray2 = new JSONArray();
        for (b a2 : this.j) {
            jSONArray2.put(a2.a());
        }
        jSONObject.put("connectedSystems", jSONArray2);
        return jSONObject;
    }

    public final int describeContents() {
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof h) {
                h hVar = (h) obj;
                if (j.a((Object) this.f120a, (Object) hVar.f120a) && j.a((Object) this.f121b, (Object) hVar.f121b) && j.a(this.c, hVar.c) && j.a((Object) this.d, (Object) hVar.d) && j.a(this.e, hVar.e) && j.a((Object) this.f, (Object) hVar.f) && j.a((Object) this.g, (Object) hVar.g)) {
                    if (!(this.h == hVar.h) || !j.a(this.i, hVar.i) || !j.a(this.j, hVar.j)) {
                        return false;
                    }
                }
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        String str = this.f120a;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.f121b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        Date date = this.c;
        int hashCode3 = (hashCode2 + (date != null ? date.hashCode() : 0)) * 31;
        String str3 = this.d;
        int hashCode4 = (hashCode3 + (str3 != null ? str3.hashCode() : 0)) * 31;
        Date date2 = this.e;
        int hashCode5 = (hashCode4 + (date2 != null ? date2.hashCode() : 0)) * 31;
        String str4 = this.f;
        int hashCode6 = (hashCode5 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.g;
        int hashCode7 = (hashCode6 + (str5 != null ? str5.hashCode() : 0)) * 31;
        boolean z = this.h;
        if (z) {
            z = true;
        }
        int i3 = (hashCode7 + (z ? 1 : 0)) * 31;
        List<n> list = this.i;
        int hashCode8 = (i3 + (list != null ? list.hashCode() : 0)) * 31;
        List<b> list2 = this.j;
        if (list2 != null) {
            i2 = list2.hashCode();
        }
        return hashCode8 + i2;
    }

    public final String toString() {
        StringBuilder a2 = b.a.a.a.a.a("IdProfile(scid=");
        a2.append(this.f120a);
        a2.append(", name=");
        a2.append(this.f121b);
        a2.append(", nameChangeAllowed=");
        a2.append(this.c);
        a2.append(", avatarImage=");
        a2.append(this.d);
        a2.append(", avatarChangeAllowed=");
        a2.append(this.e);
        a2.append(", qrCodeURL=");
        a2.append(this.f);
        a2.append(", universalLink=");
        a2.append(this.g);
        a2.append(", forcedOfflineStatus=");
        a2.append(this.h);
        a2.append(", availableSystems=");
        a2.append(this.i);
        a2.append(", connectedSystems=");
        a2.append(this.j);
        a2.append(")");
        return a2.toString();
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        j.b(parcel, "dest");
        parcel.writeString(this.f120a);
        parcel.writeString(this.f121b);
        parcel.writeLong(this.c.getTime());
        parcel.writeString(this.d);
        parcel.writeLong(this.e.getTime());
        parcel.writeString(this.f);
        parcel.writeString(this.g);
        ParcelCompat.writeBoolean(parcel, this.h);
        parcel.writeTypedList(this.i);
        parcel.writeTypedList(this.j);
    }
}
