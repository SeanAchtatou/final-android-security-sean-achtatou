package b.b.a.h;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.NotificationCompat;
import b.b.a.j.a0;
import com.facebook.share.internal.ShareConstants;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.a.ah;
import kotlin.a.m;
import kotlin.d.b.g;
import kotlin.d.b.j;
import kotlin.g.c;
import kotlin.g.d;
import org.json.JSONArray;
import org.json.JSONObject;

public final class b implements a0 {
    public static final Parcelable.Creator<b> CREATOR = new a();
    public static final C0007b e = new C0007b(null);

    /* renamed from: a  reason: collision with root package name */
    public final String f108a;

    /* renamed from: b  reason: collision with root package name */
    public final String f109b;
    public final List<String> c;
    public final String d;

    public final class a implements Parcelable.Creator<b> {
        public final b createFromParcel(Parcel parcel) {
            j.b(parcel, ShareConstants.FEED_SOURCE_PARAM);
            j.b(parcel, "parcel");
            String readString = parcel.readString();
            if (readString == null) {
                j.a();
            }
            return new b(readString, parcel.readString(), parcel.createStringArrayList(), parcel.readString());
        }

        public final b[] newArray(int i) {
            return new b[i];
        }
    }

    /* renamed from: b.b.a.h.b$b  reason: collision with other inner class name */
    public static final class C0007b {
        public /* synthetic */ C0007b(g gVar) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [org.json.JSONObject, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final List<b> a(JSONArray jSONArray) {
            j.b(jSONArray, ShareConstants.WEB_DIALOG_PARAM_DATA);
            c b2 = d.b(0, jSONArray.length());
            ArrayList arrayList = new ArrayList(m.a(b2, 10));
            Iterator it = b2.iterator();
            while (it.hasNext()) {
                JSONObject jSONObject = jSONArray.getJSONObject(((ah) it).a());
                j.a((Object) jSONObject, "it");
                arrayList.add(new b(jSONObject));
            }
            return arrayList;
        }
    }

    public b(String str, String str2, List<String> list, String str3) {
        j.b(str, "game");
        this.f108a = str;
        this.f109b = str2;
        this.c = list;
        this.d = str3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public b(JSONObject jSONObject) {
        ArrayList arrayList;
        j.b(jSONObject, "jsonObject");
        String string = jSONObject.getString("system");
        j.a((Object) string, "jsonObject.getString(\"system\")");
        Object opt = jSONObject.opt("username");
        String str = null;
        opt = (opt == null || j.a(opt, JSONObject.NULL)) ? null : opt;
        String str2 = (opt == null || !(opt instanceof String)) ? null : (String) opt;
        JSONArray optJSONArray = jSONObject.optJSONArray(NotificationCompat.CATEGORY_PROGRESS);
        if (optJSONArray != null) {
            c b2 = d.b(0, optJSONArray.length());
            arrayList = new ArrayList(m.a(b2, 10));
            Iterator it = b2.iterator();
            while (it.hasNext()) {
                Object opt2 = optJSONArray.opt(((ah) it).a());
                opt2 = (opt2 == null || j.a(opt2, JSONObject.NULL)) ? null : opt2;
                String str3 = (opt2 == null || !(opt2 instanceof String)) ? null : (String) opt2;
                if (str3 == null) {
                    str3 = "";
                }
                arrayList.add(str3);
            }
        } else {
            arrayList = null;
        }
        Object opt3 = jSONObject.opt("deeplink");
        opt3 = (opt3 == null || j.a(opt3, JSONObject.NULL)) ? null : opt3;
        if (opt3 != null && (opt3 instanceof String)) {
            str = (String) opt3;
        }
        j.b(string, "game");
        this.f108a = string;
        this.f109b = str2;
        this.c = arrayList;
        this.d = str;
    }

    public final JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("system", this.f108a);
        String str = this.f109b;
        if (str != null) {
            jSONObject.put("username", str);
        }
        if (this.c != null) {
            JSONArray jSONArray = new JSONArray();
            for (String put : this.c) {
                jSONArray.put(put);
            }
            jSONObject.put(NotificationCompat.CATEGORY_PROGRESS, jSONArray);
        }
        String str2 = this.d;
        if (str2 != null) {
            jSONObject.put("deeplink", str2);
        }
        return jSONObject;
    }

    public final int describeContents() {
        return 0;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        return j.a(this.f108a, bVar.f108a) && j.a(this.f109b, bVar.f109b) && j.a(this.c, bVar.c) && j.a(this.d, bVar.d);
    }

    public final int hashCode() {
        String str = this.f108a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.f109b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        List<String> list = this.c;
        int hashCode3 = (hashCode2 + (list != null ? list.hashCode() : 0)) * 31;
        String str3 = this.d;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return hashCode3 + i;
    }

    public final String toString() {
        StringBuilder a2 = b.a.a.a.a.a("IdConnectedSystem(game=");
        a2.append(this.f108a);
        a2.append(", username=");
        a2.append(this.f109b);
        a2.append(", progress=");
        a2.append(this.c);
        a2.append(", linkUrl=");
        return b.a.a.a.a.a(a2, this.d, ")");
    }

    public final void writeToParcel(Parcel parcel, int i) {
        j.b(parcel, "dest");
        parcel.writeString(this.f108a);
        parcel.writeString(this.f109b);
        parcel.writeStringList(this.c);
        parcel.writeString(this.d);
    }
}
