package b.b.a.h;

import android.os.Parcel;
import android.os.Parcelable;
import b.b.a.j.a0;
import com.facebook.appevents.codeless.internal.Constants;
import com.facebook.share.internal.ShareConstants;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.a.ah;
import kotlin.d.b.g;
import kotlin.d.b.j;
import kotlin.g.c;
import kotlin.g.d;
import org.json.JSONArray;
import org.json.JSONObject;

public final class n implements a0 {
    public static final Parcelable.Creator<n> CREATOR = new a();
    public static final b d = new b(null);

    /* renamed from: a  reason: collision with root package name */
    public final String f135a;

    /* renamed from: b  reason: collision with root package name */
    public final String f136b;
    public final String c;

    public final class a implements Parcelable.Creator<n> {
        public final n createFromParcel(Parcel parcel) {
            j.b(parcel, ShareConstants.FEED_SOURCE_PARAM);
            j.b(parcel, "parcel");
            String readString = parcel.readString();
            if (readString == null) {
                j.a();
            }
            return new n(readString, parcel.readString(), parcel.readString());
        }

        public final n[] newArray(int i) {
            return new n[i];
        }
    }

    public static final class b {
        public /* synthetic */ b(g gVar) {
        }

        public final List<n> a(JSONArray jSONArray) {
            j.b(jSONArray, ShareConstants.WEB_DIALOG_PARAM_DATA);
            c b2 = d.b(0, jSONArray.length());
            ArrayList arrayList = new ArrayList();
            Iterator it = b2.iterator();
            while (it.hasNext()) {
                JSONObject optJSONObject = jSONArray.optJSONObject(((ah) it).a());
                n nVar = optJSONObject != null ? new n(optJSONObject) : null;
                if (nVar != null) {
                    arrayList.add(nVar);
                }
            }
            return arrayList;
        }
    }

    public n(String str, String str2, String str3) {
        j.b(str, "name");
        this.f135a = str;
        this.f136b = str2;
        this.c = str3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public n(JSONObject jSONObject) {
        String str;
        Object opt;
        j.b(jSONObject, ShareConstants.WEB_DIALOG_PARAM_DATA);
        String string = jSONObject.getString("system");
        j.a((Object) string, "data.getString(\"system\")");
        JSONObject optJSONObject = jSONObject.optJSONObject("appStoreLinks");
        String str2 = null;
        if (optJSONObject != null) {
            Object opt2 = optJSONObject.opt(Constants.PLATFORM);
            opt2 = (opt2 == null || j.a(opt2, JSONObject.NULL)) ? null : opt2;
            if (opt2 != null && (opt2 instanceof String)) {
                str = (String) opt2;
                opt = jSONObject.opt("deeplink");
                opt = (opt == null || j.a(opt, JSONObject.NULL)) ? null : opt;
                if (opt != null && (opt instanceof String)) {
                    str2 = (String) opt;
                }
                j.b(string, "name");
                this.f135a = string;
                this.f136b = str;
                this.c = str2;
            }
        }
        str = null;
        opt = jSONObject.opt("deeplink");
        str2 = (String) opt;
        j.b(string, "name");
        this.f135a = string;
        this.f136b = str;
        this.c = str2;
    }

    public final String a() {
        return this.f136b;
    }

    public final JSONObject b() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("system", this.f135a);
        String str = this.f136b;
        if (str != null) {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put(Constants.PLATFORM, str);
            jSONObject.put("appStoreLinks", jSONObject2);
        }
        jSONObject.putOpt("deeplink", this.c);
        return jSONObject;
    }

    public final int describeContents() {
        return 0;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof n)) {
            return false;
        }
        n nVar = (n) obj;
        return j.a(this.f135a, nVar.f135a) && j.a(this.f136b, nVar.f136b) && j.a(this.c, nVar.c);
    }

    public final int hashCode() {
        String str = this.f135a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.f136b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return hashCode2 + i;
    }

    public final String toString() {
        StringBuilder a2 = b.a.a.a.a.a("IdSystem(name=");
        a2.append(this.f135a);
        a2.append(", appStoreLink=");
        a2.append(this.f136b);
        a2.append(", deepLink=");
        return b.a.a.a.a.a(a2, this.c, ")");
    }

    public final void writeToParcel(Parcel parcel, int i) {
        j.b(parcel, "dest");
        parcel.writeString(this.f135a);
        parcel.writeString(this.f136b);
        parcel.writeString(this.c);
    }
}
