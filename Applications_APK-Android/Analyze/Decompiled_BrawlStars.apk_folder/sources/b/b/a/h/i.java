package b.b.a.h;

import b.a.a.a.a;
import b.b.a.h.j;
import com.facebook.share.internal.ShareConstants;
import java.util.List;
import kotlin.d.b.j;
import org.json.JSONObject;

public final class i {

    /* renamed from: a  reason: collision with root package name */
    public final String f122a;

    /* renamed from: b  reason: collision with root package name */
    public final String f123b;
    public final String c;
    public final String d;
    public final String e;
    public final j f;
    public final List<n> g;
    public final List<b> h;
    public final g i;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public i(JSONObject jSONObject) {
        j.b(jSONObject, ShareConstants.WEB_DIALOG_PARAM_DATA);
        String string = jSONObject.getString("scid");
        j.a((Object) string, "data.getString(\"scid\")");
        Object opt = jSONObject.opt("name");
        g gVar = null;
        opt = (opt == null || j.a(opt, JSONObject.NULL)) ? null : opt;
        String str = (opt == null || !(opt instanceof String)) ? null : (String) opt;
        Object opt2 = jSONObject.opt("avatarImage");
        opt2 = (opt2 == null || j.a(opt2, JSONObject.NULL)) ? null : opt2;
        String str2 = (opt2 == null || !(opt2 instanceof String)) ? null : (String) opt2;
        Object opt3 = jSONObject.opt("qrCodeURL");
        opt3 = (opt3 == null || j.a(opt3, JSONObject.NULL)) ? null : opt3;
        String str3 = (opt3 == null || !(opt3 instanceof String)) ? null : (String) opt3;
        Object opt4 = jSONObject.opt("universalLink");
        opt4 = (opt4 == null || j.a(opt4, JSONObject.NULL)) ? null : opt4;
        String str4 = (opt4 == null || !(opt4 instanceof String)) ? null : (String) opt4;
        JSONObject optJSONObject = jSONObject.optJSONObject("relationship");
        j jVar = (optJSONObject == null || (jVar = j.f124a.a(optJSONObject)) == null) ? j.d.f126b : jVar;
        List<n> a2 = n.d.a(jSONObject.getJSONArray("availableSystems"));
        List<b> a3 = b.e.a(jSONObject.getJSONArray("connectedSystems"));
        JSONObject optJSONObject2 = jSONObject.optJSONObject("presence");
        gVar = optJSONObject2 != null ? new g(optJSONObject2) : gVar;
        kotlin.d.b.j.b(string, "scid");
        kotlin.d.b.j.b(jVar, "relationship");
        kotlin.d.b.j.b(a2, "availableSystems");
        kotlin.d.b.j.b(a3, "connectedSystems");
        this.f122a = string;
        this.f123b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = jVar;
        this.g = a2;
        this.h = a3;
        this.i = gVar;
    }

    public final List<n> a() {
        return this.g;
    }

    public final String b() {
        return this.d;
    }

    public final String c() {
        return this.e;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof i)) {
            return false;
        }
        i iVar = (i) obj;
        return kotlin.d.b.j.a(this.f122a, iVar.f122a) && kotlin.d.b.j.a(this.f123b, iVar.f123b) && kotlin.d.b.j.a(this.c, iVar.c) && kotlin.d.b.j.a(this.d, iVar.d) && kotlin.d.b.j.a(this.e, iVar.e) && kotlin.d.b.j.a(this.f, iVar.f) && kotlin.d.b.j.a(this.g, iVar.g) && kotlin.d.b.j.a(this.h, iVar.h) && kotlin.d.b.j.a(this.i, iVar.i);
    }

    public final int hashCode() {
        String str = this.f122a;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.f123b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.d;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.e;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        j jVar = this.f;
        int hashCode6 = (hashCode5 + (jVar != null ? jVar.hashCode() : 0)) * 31;
        List<n> list = this.g;
        int hashCode7 = (hashCode6 + (list != null ? list.hashCode() : 0)) * 31;
        List<b> list2 = this.h;
        int hashCode8 = (hashCode7 + (list2 != null ? list2.hashCode() : 0)) * 31;
        g gVar = this.i;
        if (gVar != null) {
            i2 = gVar.hashCode();
        }
        return hashCode8 + i2;
    }

    public final String toString() {
        StringBuilder a2 = a.a("IdPublicProfile(scid=");
        a2.append(this.f122a);
        a2.append(", name=");
        a2.append(this.f123b);
        a2.append(", avatarImage=");
        a2.append(this.c);
        a2.append(", qrCodeUrl=");
        a2.append(this.d);
        a2.append(", universalLink=");
        a2.append(this.e);
        a2.append(", relationship=");
        a2.append(this.f);
        a2.append(", availableSystems=");
        a2.append(this.g);
        a2.append(", connectedSystems=");
        a2.append(this.h);
        a2.append(", presence=");
        a2.append(this.i);
        a2.append(")");
        return a2.toString();
    }
}
