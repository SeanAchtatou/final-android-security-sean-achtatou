package b.b.a.h;

import b.b.a.h.j;
import com.facebook.internal.NativeProtocol;
import com.facebook.share.internal.ShareConstants;
import java.util.ArrayList;
import java.util.List;
import kotlin.d.b.g;
import kotlin.d.b.j;
import org.json.JSONArray;

public final class d {
    public static final a d = new a(null);

    /* renamed from: a  reason: collision with root package name */
    public final List<c> f112a;

    /* renamed from: b  reason: collision with root package name */
    public final List<c> f113b;
    public final List<c> c;

    public static final class a {
        public a() {
        }

        public /* synthetic */ a(g gVar) {
        }

        public final d a(JSONArray jSONArray) {
            j.b(jSONArray, ShareConstants.WEB_DIALOG_PARAM_DATA);
            List<c> a2 = c.g.a(jSONArray);
            ArrayList arrayList = new ArrayList();
            for (T next : a2) {
                if (((c) next).e instanceof j.a.C0008a) {
                    arrayList.add(next);
                }
            }
            ArrayList arrayList2 = new ArrayList();
            for (T next2 : a2) {
                if (((c) next2).e instanceof j.a.c) {
                    arrayList2.add(next2);
                }
            }
            ArrayList arrayList3 = new ArrayList();
            for (T next3 : a2) {
                if (((c) next3).e instanceof j.a.b) {
                    arrayList3.add(next3);
                }
            }
            return new d(arrayList, arrayList2, arrayList3);
        }
    }

    public d(List<c> list, List<c> list2, List<c> list3) {
        kotlin.d.b.j.b(list, NativeProtocol.AUDIENCE_FRIENDS);
        kotlin.d.b.j.b(list2, "sentInvites");
        kotlin.d.b.j.b(list3, "receivedInvites");
        this.f112a = list;
        this.f113b = list2;
        this.c = list3;
    }

    public final d a(List<c> list, List<c> list2, List<c> list3) {
        kotlin.d.b.j.b(list, NativeProtocol.AUDIENCE_FRIENDS);
        kotlin.d.b.j.b(list2, "sentInvites");
        kotlin.d.b.j.b(list3, "receivedInvites");
        return new d(list, list2, list3);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof d)) {
            return false;
        }
        d dVar = (d) obj;
        return kotlin.d.b.j.a(this.f112a, dVar.f112a) && kotlin.d.b.j.a(this.f113b, dVar.f113b) && kotlin.d.b.j.a(this.c, dVar.c);
    }

    public final int hashCode() {
        List<c> list = this.f112a;
        int i = 0;
        int hashCode = (list != null ? list.hashCode() : 0) * 31;
        List<c> list2 = this.f113b;
        int hashCode2 = (hashCode + (list2 != null ? list2.hashCode() : 0)) * 31;
        List<c> list3 = this.c;
        if (list3 != null) {
            i = list3.hashCode();
        }
        return hashCode2 + i;
    }

    public final String toString() {
        StringBuilder a2 = b.a.a.a.a.a("IdFriends(friends=");
        a2.append(this.f112a);
        a2.append(", sentInvites=");
        a2.append(this.f113b);
        a2.append(", receivedInvites=");
        a2.append(this.c);
        a2.append(")");
        return a2.toString();
    }
}
