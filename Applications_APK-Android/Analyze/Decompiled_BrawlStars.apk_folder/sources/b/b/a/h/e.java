package b.b.a.h;

import android.os.Parcel;
import android.os.Parcelable;
import b.b.a.j.a0;
import com.facebook.share.internal.ShareConstants;
import java.util.ArrayList;
import java.util.List;
import kotlin.d.b.j;
import org.json.JSONArray;
import org.json.JSONObject;

public final class e implements a0 {
    public static final Parcelable.Creator<e> CREATOR = new a();

    /* renamed from: a  reason: collision with root package name */
    public final List<n> f114a;

    /* renamed from: b  reason: collision with root package name */
    public final List<String> f115b;

    public final class a implements Parcelable.Creator<e> {
        public final e createFromParcel(Parcel parcel) {
            j.b(parcel, ShareConstants.FEED_SOURCE_PARAM);
            j.b(parcel, "parcel");
            ArrayList createTypedArrayList = parcel.createTypedArrayList(n.CREATOR);
            if (createTypedArrayList == null) {
                j.a();
            }
            ArrayList<String> createStringArrayList = parcel.createStringArrayList();
            if (createStringArrayList == null) {
                j.a();
            }
            return new e(createTypedArrayList, createStringArrayList);
        }

        public final e[] newArray(int i) {
            return new e[i];
        }
    }

    public e(List<n> list, List<String> list2) {
        j.b(list, "availableSystems");
        j.b(list2, "connectedSystems");
        this.f114a = list;
        this.f115b = list2;
    }

    public final JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        for (n b2 : this.f114a) {
            jSONArray.put(b2.b());
        }
        jSONObject.put("availableSystems", jSONArray);
        JSONArray jSONArray2 = new JSONArray();
        for (String put : this.f115b) {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("system", put);
            jSONArray2.put(jSONObject2);
        }
        jSONObject.put("connectedSystems", jSONArray2);
        return jSONObject;
    }

    public final int describeContents() {
        return 0;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof e)) {
            return false;
        }
        e eVar = (e) obj;
        return j.a(this.f114a, eVar.f114a) && j.a(this.f115b, eVar.f115b);
    }

    public final int hashCode() {
        List<n> list = this.f114a;
        int i = 0;
        int hashCode = (list != null ? list.hashCode() : 0) * 31;
        List<String> list2 = this.f115b;
        if (list2 != null) {
            i = list2.hashCode();
        }
        return hashCode + i;
    }

    public final String toString() {
        StringBuilder a2 = b.a.a.a.a.a("IdInfo(availableSystems=");
        a2.append(this.f114a);
        a2.append(", connectedSystems=");
        a2.append(this.f115b);
        a2.append(")");
        return a2.toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        j.b(parcel, "dest");
        parcel.writeTypedList(this.f114a);
        parcel.writeStringList(this.f115b);
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x005e A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public e(org.json.JSONObject r9) {
        /*
            r8 = this;
            java.lang.String r0 = "data"
            kotlin.d.b.j.b(r9, r0)
            java.lang.String r0 = "availableSystems"
            org.json.JSONArray r1 = r9.optJSONArray(r0)
            r2 = 0
            r3 = 0
            if (r1 == 0) goto L_0x0040
            int r4 = r1.length()
            kotlin.g.c r4 = kotlin.g.d.b(r2, r4)
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            java.util.Iterator r4 = r4.iterator()
        L_0x0020:
            boolean r6 = r4.hasNext()
            if (r6 == 0) goto L_0x0045
            r6 = r4
            kotlin.a.ah r6 = (kotlin.a.ah) r6
            int r6 = r6.a()
            org.json.JSONObject r6 = r1.optJSONObject(r6)
            if (r6 == 0) goto L_0x0039
            b.b.a.h.n r7 = new b.b.a.h.n
            r7.<init>(r6)
            goto L_0x003a
        L_0x0039:
            r7 = r3
        L_0x003a:
            if (r7 == 0) goto L_0x0020
            r5.add(r7)
            goto L_0x0020
        L_0x0040:
            kotlin.a.ab r1 = kotlin.a.ab.f5210a
            r5 = r1
            java.util.List r5 = (java.util.List) r5
        L_0x0045:
            java.lang.String r1 = "connectedSystems"
            org.json.JSONArray r9 = r9.optJSONArray(r1)
            if (r9 == 0) goto L_0x0092
            int r4 = r9.length()
            kotlin.g.c r2 = kotlin.g.d.b(r2, r4)
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            java.util.Iterator r2 = r2.iterator()
        L_0x005e:
            boolean r6 = r2.hasNext()
            if (r6 == 0) goto L_0x0097
            r6 = r2
            kotlin.a.ah r6 = (kotlin.a.ah) r6
            int r6 = r6.a()
            org.json.JSONObject r6 = r9.optJSONObject(r6)
            if (r6 == 0) goto L_0x008b
            java.lang.String r7 = "system"
            java.lang.Object r6 = r6.opt(r7)
            if (r6 == 0) goto L_0x0081
            java.lang.Object r7 = org.json.JSONObject.NULL
            boolean r7 = kotlin.d.b.j.a(r6, r7)
            if (r7 == 0) goto L_0x0082
        L_0x0081:
            r6 = r3
        L_0x0082:
            if (r6 == 0) goto L_0x008b
            boolean r7 = r6 instanceof java.lang.String
            if (r7 == 0) goto L_0x008b
            java.lang.String r6 = (java.lang.String) r6
            goto L_0x008c
        L_0x008b:
            r6 = r3
        L_0x008c:
            if (r6 == 0) goto L_0x005e
            r4.add(r6)
            goto L_0x005e
        L_0x0092:
            kotlin.a.ab r9 = kotlin.a.ab.f5210a
            r4 = r9
            java.util.List r4 = (java.util.List) r4
        L_0x0097:
            kotlin.d.b.j.b(r5, r0)
            kotlin.d.b.j.b(r4, r1)
            r8.<init>()
            r8.f114a = r5
            r8.f115b = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.h.e.<init>(org.json.JSONObject):void");
    }
}
