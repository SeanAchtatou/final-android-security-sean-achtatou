package b.b.a.h;

import android.os.Parcel;
import android.os.Parcelable;
import b.b.a.j.a0;
import com.facebook.share.internal.ShareConstants;
import kotlin.d.b.j;
import org.json.JSONObject;

public final class g implements a0 {
    public static final Parcelable.Creator<g> CREATOR = new a();

    /* renamed from: a  reason: collision with root package name */
    public final String f118a;

    /* renamed from: b  reason: collision with root package name */
    public final String f119b;

    public final class a implements Parcelable.Creator<g> {
        public final g createFromParcel(Parcel parcel) {
            j.b(parcel, ShareConstants.FEED_SOURCE_PARAM);
            j.b(parcel, "parcel");
            String readString = parcel.readString();
            if (readString == null) {
                j.a();
            }
            return new g(readString, parcel.readString());
        }

        public final g[] newArray(int i) {
            return new g[i];
        }
    }

    public g(String str, String str2) {
        j.b(str, "system");
        this.f118a = str;
        this.f119b = str2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public g(JSONObject jSONObject) {
        j.b(jSONObject, "jsonObject");
        String string = jSONObject.getString("system");
        j.a((Object) string, "jsonObject.getString(\"system\")");
        Object opt = jSONObject.opt("username");
        String str = null;
        opt = (opt == null || j.a(opt, JSONObject.NULL)) ? null : opt;
        if (opt != null && (opt instanceof String)) {
            str = (String) opt;
        }
        j.b(string, "system");
        this.f118a = string;
        this.f119b = str;
    }

    public final int describeContents() {
        return 0;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof g)) {
            return false;
        }
        g gVar = (g) obj;
        return j.a(this.f118a, gVar.f118a) && j.a(this.f119b, gVar.f119b);
    }

    public final int hashCode() {
        String str = this.f118a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.f119b;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode + i;
    }

    public final String toString() {
        StringBuilder a2 = b.a.a.a.a.a("IdPresenceStatus(system=");
        a2.append(this.f118a);
        a2.append(", username=");
        return b.a.a.a.a.a(a2, this.f119b, ")");
    }

    public final void writeToParcel(Parcel parcel, int i) {
        j.b(parcel, "dest");
        parcel.writeString(this.f118a);
        parcel.writeString(this.f119b);
    }
}
