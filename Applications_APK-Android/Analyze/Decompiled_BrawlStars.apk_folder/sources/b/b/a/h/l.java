package b.b.a.h;

import b.a.a.a.a;
import java.util.List;
import kotlin.d.b.j;

public final class l {

    /* renamed from: a  reason: collision with root package name */
    public final h f129a;

    /* renamed from: b  reason: collision with root package name */
    public final List<String> f130b;

    public l(h hVar, List<String> list) {
        j.b(hVar, "profile");
        j.b(list, "warnings");
        this.f129a = hVar;
        this.f130b = list;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof l)) {
            return false;
        }
        l lVar = (l) obj;
        return j.a(this.f129a, lVar.f129a) && j.a(this.f130b, lVar.f130b);
    }

    public final int hashCode() {
        h hVar = this.f129a;
        int i = 0;
        int hashCode = (hVar != null ? hVar.hashCode() : 0) * 31;
        List<String> list = this.f130b;
        if (list != null) {
            i = list.hashCode();
        }
        return hashCode + i;
    }

    public final String toString() {
        StringBuilder a2 = a.a("IdSetProfileResponse(profile=");
        a2.append(this.f129a);
        a2.append(", warnings=");
        a2.append(this.f130b);
        a2.append(")");
        return a2.toString();
    }
}
