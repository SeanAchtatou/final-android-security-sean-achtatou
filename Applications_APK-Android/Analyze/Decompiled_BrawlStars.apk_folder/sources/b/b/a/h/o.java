package b.b.a.h;

import kotlin.d.b.g;
import kotlin.d.b.j;
import org.json.JSONObject;

public abstract class o {

    /* renamed from: b  reason: collision with root package name */
    public static final b f137b = new b(null);

    /* renamed from: a  reason: collision with root package name */
    public final String f138a;

    public static final class a extends o {
        public final String c;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public a(org.json.JSONObject r5) {
            /*
                r4 = this;
                java.lang.String r0 = "jsonObject"
                kotlin.d.b.j.b(r5, r0)
                java.lang.String r0 = "scid"
                java.lang.String r1 = r5.getString(r0)
                java.lang.String r2 = "jsonObject.getString(\"scid\")"
                kotlin.d.b.j.a(r1, r2)
                java.lang.String r2 = "bindToken"
                java.lang.String r5 = r5.getString(r2)
                java.lang.String r3 = "jsonObject.getString(\"bindToken\")"
                kotlin.d.b.j.a(r5, r3)
                kotlin.d.b.j.b(r1, r0)
                kotlin.d.b.j.b(r5, r2)
                r0 = 0
                r4.<init>(r1, r0)
                r4.c = r5
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: b.b.a.h.o.a.<init>(org.json.JSONObject):void");
        }

        public final String a() {
            return this.c;
        }
    }

    public static final class b {
        public b() {
        }

        public /* synthetic */ b(g gVar) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final o a(JSONObject jSONObject) {
            j.b(jSONObject, "jsonObject");
            if (jSONObject.has("bindToken")) {
                return new a(jSONObject);
            }
            String string = jSONObject.getString("scid");
            j.a((Object) string, "jsonObject.getString(\"scid\")");
            return new c(string);
        }
    }

    public static final class c extends o {
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(String str) {
            super(str, null);
            j.b(str, "scid");
        }
    }

    public /* synthetic */ o(String str, g gVar) {
        this.f138a = str;
    }
}
