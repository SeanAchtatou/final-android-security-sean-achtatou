package b.b.a.h;

import android.os.Parcel;
import android.os.Parcelable;
import b.b.a.j.a0;
import java.util.Date;
import java.util.NoSuchElementException;
import kotlin.d.b.g;

public abstract class j implements a0 {
    public static final Parcelable.Creator<j> CREATOR = new b();

    /* renamed from: a  reason: collision with root package name */
    public static final c f124a = new c(null);

    public static abstract class a extends j {

        /* renamed from: b  reason: collision with root package name */
        public final Date f125b;

        /* renamed from: b.b.a.h.j$a$a  reason: collision with other inner class name */
        public static final class C0008a extends a {
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0008a(Date date) {
                super(date, null);
                kotlin.d.b.j.b(date, "timestamp");
            }
        }

        public static final class b extends a {
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(Date date) {
                super(date, null);
                kotlin.d.b.j.b(date, "timestamp");
            }
        }

        public static final class c extends a {
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(Date date) {
                super(date, null);
                kotlin.d.b.j.b(date, "timestamp");
            }
        }

        public /* synthetic */ a(Date date, g gVar) {
            super(null);
            this.f125b = date;
        }

        public final Date a() {
            return this.f125b;
        }
    }

    public final class b implements Parcelable.Creator<j> {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [java.util.Locale, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x006f  */
        /* JADX WARNING: Removed duplicated region for block: B:7:0x0025  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object createFromParcel(android.os.Parcel r4) {
            /*
                r3 = this;
                java.lang.String r0 = "parcel"
                kotlin.d.b.j.b(r4, r0)
                java.lang.String r0 = r4.readString()
                if (r0 == 0) goto L_0x0022
                java.util.Locale r1 = java.util.Locale.ENGLISH
                java.lang.String r2 = "Locale.ENGLISH"
                kotlin.d.b.j.a(r1, r2)
                java.lang.String r0 = r0.toUpperCase(r1)
                java.lang.String r1 = "(this as java.lang.String).toUpperCase(locale)"
                kotlin.d.b.j.a(r0, r1)
                if (r0 == 0) goto L_0x0022
                b.b.a.h.p r0 = b.b.a.h.p.valueOf(r0)
                goto L_0x0023
            L_0x0022:
                r0 = 0
            L_0x0023:
                if (r0 == 0) goto L_0x006f
                int[] r1 = b.b.a.h.k.f127a
                int r0 = r0.ordinal()
                r0 = r1[r0]
                r1 = 1
                if (r0 == r1) goto L_0x006c
                r1 = 2
                if (r0 == r1) goto L_0x005d
                r1 = 3
                if (r0 == r1) goto L_0x004e
                r1 = 4
                if (r0 != r1) goto L_0x0048
                long r0 = r4.readLong()
                java.util.Date r4 = new java.util.Date
                r4.<init>(r0)
                b.b.a.h.j$a$a r0 = new b.b.a.h.j$a$a
                r0.<init>(r4)
                goto L_0x006e
            L_0x0048:
                kotlin.NoWhenBranchMatchedException r4 = new kotlin.NoWhenBranchMatchedException
                r4.<init>()
                throw r4
            L_0x004e:
                long r0 = r4.readLong()
                java.util.Date r4 = new java.util.Date
                r4.<init>(r0)
                b.b.a.h.j$a$b r0 = new b.b.a.h.j$a$b
                r0.<init>(r4)
                goto L_0x006e
            L_0x005d:
                long r0 = r4.readLong()
                java.util.Date r4 = new java.util.Date
                r4.<init>(r0)
                b.b.a.h.j$a$c r0 = new b.b.a.h.j$a$c
                r0.<init>(r4)
                goto L_0x006e
            L_0x006c:
                b.b.a.h.j$d r0 = b.b.a.h.j.d.f126b
            L_0x006e:
                return r0
            L_0x006f:
                android.os.ParcelFormatException r4 = new android.os.ParcelFormatException
                java.lang.String r0 = "Invalid parcel"
                r4.<init>(r0)
                throw r4
            */
            throw new UnsupportedOperationException("Method not decompiled: b.b.a.h.j.b.createFromParcel(android.os.Parcel):java.lang.Object");
        }

        public final Object[] newArray(int i) {
            return new j[i];
        }
    }

    public static final class c {
        public /* synthetic */ c(g gVar) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [java.util.Locale, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0033  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0051  */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x00a7  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final b.b.a.h.j a(org.json.JSONObject r5) {
            /*
                r4 = this;
                java.lang.String r0 = "jsonObject"
                kotlin.d.b.j.b(r5, r0)
                java.lang.String r0 = "timestamp"
                java.lang.Object r0 = r5.opt(r0)
                r1 = 0
                if (r0 == 0) goto L_0x0016
                java.lang.Object r2 = org.json.JSONObject.NULL
                boolean r2 = kotlin.d.b.j.a(r0, r2)
                if (r2 == 0) goto L_0x0017
            L_0x0016:
                r0 = r1
            L_0x0017:
                if (r0 == 0) goto L_0x0030
                boolean r2 = r0 instanceof java.lang.Integer
                if (r2 == 0) goto L_0x0029
                java.lang.Number r0 = (java.lang.Number) r0
                int r0 = r0.intValue()
                long r2 = (long) r0
                java.lang.Long r0 = java.lang.Long.valueOf(r2)
                goto L_0x0031
            L_0x0029:
                boolean r2 = r0 instanceof java.lang.Long
                if (r2 == 0) goto L_0x0030
                java.lang.Long r0 = (java.lang.Long) r0
                goto L_0x0031
            L_0x0030:
                r0 = r1
            L_0x0031:
                if (r0 == 0) goto L_0x003d
                long r0 = r0.longValue()
                java.util.Date r2 = new java.util.Date
                r2.<init>(r0)
                r1 = r2
            L_0x003d:
                java.lang.String r0 = "status"
                java.lang.String r5 = r5.getString(r0)
                java.lang.String r0 = "jsonObject.getString(\"status\")"
                kotlin.d.b.j.a(r5, r0)
                java.util.Locale r0 = java.util.Locale.ENGLISH
                java.lang.String r2 = "Locale.ENGLISH"
                kotlin.d.b.j.a(r0, r2)
                if (r5 == 0) goto L_0x00a7
                java.lang.String r5 = r5.toUpperCase(r0)
                java.lang.String r0 = "(this as java.lang.String).toUpperCase(locale)"
                kotlin.d.b.j.a(r5, r0)
                b.b.a.h.p r5 = b.b.a.h.p.valueOf(r5)
                int[] r0 = b.b.a.h.k.f128b
                int r5 = r5.ordinal()
                r5 = r0[r5]
                r0 = 1
                if (r5 == r0) goto L_0x00a4
                r0 = 2
                java.lang.String r2 = "Invalid JSON"
                if (r5 == r0) goto L_0x0096
                r0 = 3
                if (r5 == r0) goto L_0x0088
                r0 = 4
                if (r5 != r0) goto L_0x0082
                if (r1 == 0) goto L_0x007c
                b.b.a.h.j$a$a r5 = new b.b.a.h.j$a$a
                r5.<init>(r1)
                goto L_0x00a6
            L_0x007c:
                org.json.JSONException r5 = new org.json.JSONException
                r5.<init>(r2)
                throw r5
            L_0x0082:
                kotlin.NoWhenBranchMatchedException r5 = new kotlin.NoWhenBranchMatchedException
                r5.<init>()
                throw r5
            L_0x0088:
                if (r1 == 0) goto L_0x0090
                b.b.a.h.j$a$b r5 = new b.b.a.h.j$a$b
                r5.<init>(r1)
                goto L_0x00a6
            L_0x0090:
                org.json.JSONException r5 = new org.json.JSONException
                r5.<init>(r2)
                throw r5
            L_0x0096:
                if (r1 == 0) goto L_0x009e
                b.b.a.h.j$a$c r5 = new b.b.a.h.j$a$c
                r5.<init>(r1)
                goto L_0x00a6
            L_0x009e:
                org.json.JSONException r5 = new org.json.JSONException
                r5.<init>(r2)
                throw r5
            L_0x00a4:
                b.b.a.h.j$d r5 = b.b.a.h.j.d.f126b
            L_0x00a6:
                return r5
            L_0x00a7:
                kotlin.TypeCastException r5 = new kotlin.TypeCastException
                java.lang.String r0 = "null cannot be cast to non-null type java.lang.String"
                r5.<init>(r0)
                throw r5
            */
            throw new UnsupportedOperationException("Method not decompiled: b.b.a.h.j.c.a(org.json.JSONObject):b.b.a.h.j");
        }
    }

    public static final class d extends j {

        /* renamed from: b  reason: collision with root package name */
        public static final d f126b = new d();

        public d() {
            super(null);
        }
    }

    public j() {
    }

    public /* synthetic */ j(g gVar) {
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        kotlin.d.b.j.b(parcel, "dest");
        for (p pVar : p.values()) {
            if (kotlin.d.b.j.a(pVar.f140a, getClass())) {
                parcel.writeString(pVar.name());
                if (this instanceof a) {
                    parcel.writeLong(((a) this).f125b.getTime());
                    return;
                }
                return;
            }
        }
        throw new NoSuchElementException("Array contains no element matching the predicate.");
    }
}
