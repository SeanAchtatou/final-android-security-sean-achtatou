package b.b.a.h;

import java.util.Set;
import kotlin.a.ad;
import kotlin.d.b.j;
import org.json.JSONArray;
import org.json.JSONObject;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    public final Set<String> f107a;

    public a() {
        this(null, 1);
    }

    public a(Set<String> set) {
        j.b(set, "seenInGameFriends");
        this.f107a = set;
    }

    public final JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        for (String put : this.f107a) {
            jSONArray.put(put);
        }
        jSONObject.put("seenInGameFriends", jSONArray);
        return jSONObject;
    }

    public final boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof a) && j.a(this.f107a, ((a) obj).f107a);
        }
        return true;
    }

    public final int hashCode() {
        Set<String> set = this.f107a;
        if (set != null) {
            return set.hashCode();
        }
        return 0;
    }

    public final String toString() {
        StringBuilder a2 = b.a.a.a.a.a("ClientState(seenInGameFriends=");
        a2.append(this.f107a);
        a2.append(")");
        return a2.toString();
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ a(Set set, int i) {
        this((i & 1) != 0 ? ad.f5212a : set);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x004f, code lost:
        if (r6 != null) goto L_0x0056;
     */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public a(org.json.JSONObject r6) {
        /*
            r5 = this;
            java.lang.String r0 = "data"
            kotlin.d.b.j.b(r6, r0)
            java.lang.String r0 = "seenInGameFriends"
            org.json.JSONArray r6 = r6.optJSONArray(r0)
            if (r6 == 0) goto L_0x0052
            int r0 = r6.length()
            r1 = 0
            kotlin.g.c r0 = kotlin.g.d.b(r1, r0)
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.util.Iterator r0 = r0.iterator()
        L_0x001f:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L_0x004b
            r2 = r0
            kotlin.a.ah r2 = (kotlin.a.ah) r2
            int r2 = r2.a()
            java.lang.Object r2 = r6.opt(r2)
            r3 = 0
            if (r2 == 0) goto L_0x003b
            java.lang.Object r4 = org.json.JSONObject.NULL
            boolean r4 = kotlin.d.b.j.a(r2, r4)
            if (r4 == 0) goto L_0x003c
        L_0x003b:
            r2 = r3
        L_0x003c:
            if (r2 == 0) goto L_0x0045
            boolean r4 = r2 instanceof java.lang.String
            if (r4 == 0) goto L_0x0045
            r3 = r2
            java.lang.String r3 = (java.lang.String) r3
        L_0x0045:
            if (r3 == 0) goto L_0x001f
            r1.add(r3)
            goto L_0x001f
        L_0x004b:
            java.util.Set r6 = kotlin.a.m.i(r1)
            if (r6 == 0) goto L_0x0052
            goto L_0x0056
        L_0x0052:
            kotlin.a.ad r6 = kotlin.a.ad.f5212a
            java.util.Set r6 = (java.util.Set) r6
        L_0x0056:
            r5.<init>(r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: b.b.a.h.a.<init>(org.json.JSONObject):void");
    }
}
