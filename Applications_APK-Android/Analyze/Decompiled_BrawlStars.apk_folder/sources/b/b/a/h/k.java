package b.b.a.h;

public final /* synthetic */ class k {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ int[] f127a = new int[p.values().length];

    /* renamed from: b  reason: collision with root package name */
    public static final /* synthetic */ int[] f128b = new int[p.values().length];

    static {
        f127a[p.STRANGER.ordinal()] = 1;
        f127a[p.REQUEST_SENT.ordinal()] = 2;
        f127a[p.REQUEST_RECEIVED.ordinal()] = 3;
        f127a[p.FRIEND.ordinal()] = 4;
        f128b[p.STRANGER.ordinal()] = 1;
        f128b[p.REQUEST_SENT.ordinal()] = 2;
        f128b[p.REQUEST_RECEIVED.ordinal()] = 3;
        f128b[p.FRIEND.ordinal()] = 4;
    }
}
