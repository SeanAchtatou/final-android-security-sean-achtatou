package b.b.a.h;

import b.b.a.h.j;
import java.util.Map;
import kotlin.d.b.g;
import kotlin.d.b.j;
import org.json.JSONObject;

public final class f {
    public static final a f = new a(null);

    /* renamed from: a  reason: collision with root package name */
    public final String f116a;

    /* renamed from: b  reason: collision with root package name */
    public final String f117b;
    public final String c;
    public final j d;
    public final String e;

    public static final class a {
        public /* synthetic */ a(g gVar) {
        }

        /* JADX WARNING: Removed duplicated region for block: B:19:0x005f  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x001e A[SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.util.List<b.b.a.h.f> a(org.json.JSONArray r8, java.util.Map<java.lang.String, java.lang.String> r9) {
            /*
                r7 = this;
                java.lang.String r0 = "JSON Parse error "
                java.lang.String r1 = "data"
                kotlin.d.b.j.b(r8, r1)
                java.lang.String r1 = "supercellIdToUserName"
                kotlin.d.b.j.b(r9, r1)
                int r1 = r8.length()
                r2 = 0
                kotlin.g.c r1 = kotlin.g.d.b(r2, r1)
                java.util.ArrayList r2 = new java.util.ArrayList
                r2.<init>()
                java.util.Iterator r1 = r1.iterator()
            L_0x001e:
                boolean r3 = r1.hasNext()
                if (r3 == 0) goto L_0x0063
                r3 = r1
                kotlin.a.ah r3 = (kotlin.a.ah) r3
                int r3 = r3.a()
                r4 = 0
                org.json.JSONObject r3 = r8.optJSONObject(r3)     // Catch:{ JSONException -> 0x004e, ParseException -> 0x0044 }
                if (r3 == 0) goto L_0x005d
                b.b.a.h.f r5 = new b.b.a.h.f     // Catch:{ JSONException -> 0x004e, ParseException -> 0x0044 }
                r5.<init>(r3, r9)     // Catch:{ JSONException -> 0x004e, ParseException -> 0x0044 }
                b.b.a.h.j r3 = r5.d     // Catch:{ JSONException -> 0x004e, ParseException -> 0x0044 }
                boolean r6 = r3 instanceof b.b.a.h.j.d
                if (r6 == 0) goto L_0x003e
                goto L_0x0042
            L_0x003e:
                boolean r3 = r3 instanceof b.b.a.h.j.a.b
                if (r3 == 0) goto L_0x005d
            L_0x0042:
                r4 = r5
                goto L_0x005d
            L_0x0044:
                r3 = move-exception
                java.lang.StringBuilder r5 = b.a.a.a.a.a(r0)
                java.lang.String r3 = r3.getLocalizedMessage()
                goto L_0x0057
            L_0x004e:
                r3 = move-exception
                java.lang.StringBuilder r5 = b.a.a.a.a.a(r0)
                java.lang.String r3 = r3.getLocalizedMessage()
            L_0x0057:
                r5.append(r3)
                r5.toString()
            L_0x005d:
                if (r4 == 0) goto L_0x001e
                r2.add(r4)
                goto L_0x001e
            L_0x0063:
                return r2
            */
            throw new UnsupportedOperationException("Method not decompiled: b.b.a.h.f.a.a(org.json.JSONArray, java.util.Map):java.util.List");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public f(JSONObject jSONObject, Map<String, String> map) {
        j.b(jSONObject, "jsonObject");
        j.b(map, "supercellIdToUserName");
        String string = jSONObject.getString("scid");
        j.a((Object) string, "jsonObject.getString(\"scid\")");
        Object opt = jSONObject.opt("name");
        String str = null;
        opt = (opt == null || j.a(opt, JSONObject.NULL)) ? null : opt;
        String str2 = (opt == null || !(opt instanceof String)) ? null : (String) opt;
        Object opt2 = jSONObject.opt("avatarImage");
        opt2 = (opt2 == null || j.a(opt2, JSONObject.NULL)) ? null : opt2;
        if (opt2 != null && (opt2 instanceof String)) {
            str = (String) opt2;
        }
        JSONObject optJSONObject = jSONObject.optJSONObject("relationship");
        j jVar = (optJSONObject == null || (jVar = j.f124a.a(optJSONObject)) == null) ? j.d.f126b : jVar;
        String str3 = map.get(jSONObject.getString("scid"));
        str3 = str3 == null ? "" : str3;
        kotlin.d.b.j.b(string, "scid");
        kotlin.d.b.j.b(jVar, "relationship");
        kotlin.d.b.j.b(str3, "username");
        this.f116a = string;
        this.f117b = str2;
        this.c = str;
        this.d = jVar;
        this.e = str3;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof f)) {
            return false;
        }
        f fVar = (f) obj;
        return kotlin.d.b.j.a(this.f116a, fVar.f116a) && kotlin.d.b.j.a(this.f117b, fVar.f117b) && kotlin.d.b.j.a(this.c, fVar.c) && kotlin.d.b.j.a(this.d, fVar.d) && kotlin.d.b.j.a(this.e, fVar.e);
    }

    public final int hashCode() {
        String str = this.f116a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.f117b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        j jVar = this.d;
        int hashCode4 = (hashCode3 + (jVar != null ? jVar.hashCode() : 0)) * 31;
        String str4 = this.e;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return hashCode4 + i;
    }

    public final String toString() {
        StringBuilder a2 = b.a.a.a.a.a("IdIngameFriendInfo(scid=");
        a2.append(this.f116a);
        a2.append(", name=");
        a2.append(this.f117b);
        a2.append(", avatarImage=");
        a2.append(this.c);
        a2.append(", relationship=");
        a2.append(this.d);
        a2.append(", username=");
        return b.a.a.a.a.a(a2, this.e, ")");
    }
}
