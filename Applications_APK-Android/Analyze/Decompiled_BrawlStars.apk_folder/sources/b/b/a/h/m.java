package b.b.a.h;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.a.ah;
import kotlin.d.b.g;
import kotlin.d.b.j;
import kotlin.g.c;
import kotlin.g.d;
import org.json.JSONArray;
import org.json.JSONObject;

public final class m {
    public static final a c = new a(null);

    /* renamed from: a  reason: collision with root package name */
    public final boolean f131a;

    /* renamed from: b  reason: collision with root package name */
    public final List<b> f132b;

    public static final class a {
        public a() {
        }

        public /* synthetic */ a(g gVar) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.a.p.a(java.lang.Iterable, int):int
         arg types: [java.util.ArrayList, int]
         candidates:
          kotlin.a.w.a(java.lang.Iterable, java.lang.Object):int
          kotlin.a.w.a(java.util.Collection, kotlin.f.d):T
          kotlin.a.w.a(java.util.List, int):T
          kotlin.a.w.a(java.lang.Iterable, java.util.Collection):C
          kotlin.a.w.a(java.lang.Iterable, java.util.Comparator):java.util.List<T>
          kotlin.a.w.a(java.util.Collection, java.lang.Object):java.util.List<T>
          kotlin.a.t.a(java.util.Collection, java.lang.Iterable):boolean
          kotlin.a.s.a(java.util.List, java.util.Comparator):void
          kotlin.a.p.a(java.lang.Iterable, java.lang.Iterable):java.util.Collection<T>
          kotlin.a.p.a(java.lang.Iterable, int):int */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [org.json.JSONObject, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final m a(JSONObject jSONObject) {
            j.b(jSONObject, "json");
            JSONArray jSONArray = jSONObject.getJSONArray("marketing_scope_consents");
            boolean z = jSONObject.getBoolean("email_marketing_permission");
            c b2 = d.b(0, jSONArray.length());
            ArrayList<JSONObject> arrayList = new ArrayList<>(kotlin.a.m.a(b2, 10));
            Iterator it = b2.iterator();
            while (it.hasNext()) {
                arrayList.add(jSONArray.getJSONObject(((ah) it).a()));
            }
            ArrayList arrayList2 = new ArrayList(kotlin.a.m.a((Iterable) arrayList, 10));
            for (JSONObject jSONObject2 : arrayList) {
                b.a aVar = b.d;
                j.a((Object) jSONObject2, "it");
                arrayList2.add(aVar.a(jSONObject2));
            }
            return new m(z, arrayList2);
        }
    }

    public static final class b {
        public static final a d = new a(null);

        /* renamed from: a  reason: collision with root package name */
        public final String f133a;

        /* renamed from: b  reason: collision with root package name */
        public final String f134b;
        public final boolean c;

        public static final class a {
            public a() {
            }

            public /* synthetic */ a(g gVar) {
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
             arg types: [java.lang.String, java.lang.String]
             candidates:
              kotlin.d.b.j.a(int, int):int
              kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
              kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
              kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
            public final b a(JSONObject jSONObject) {
                j.b(jSONObject, "json");
                String string = jSONObject.getString("scope_id");
                j.a((Object) string, "json.getString(\"scope_id\")");
                String string2 = jSONObject.getString("scope_localized_name");
                j.a((Object) string2, "json.getString(\"scope_localized_name\")");
                return new b(string, string2, jSONObject.getBoolean("scope_consent"));
            }
        }

        public b(String str, String str2, boolean z) {
            j.b(str, "id");
            j.b(str2, "localizedName");
            this.f133a = str;
            this.f134b = str2;
            this.c = z;
        }

        public static /* synthetic */ b a(b bVar, String str, String str2, boolean z, int i) {
            if ((i & 1) != 0) {
                str = bVar.f133a;
            }
            if ((i & 2) != 0) {
                str2 = bVar.f134b;
            }
            if ((i & 4) != 0) {
                z = bVar.c;
            }
            return bVar.a(str, str2, z);
        }

        public final b a(String str, String str2, boolean z) {
            j.b(str, "id");
            j.b(str2, "localizedName");
            return new b(str, str2, z);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
         arg types: [java.lang.String, java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
        public final boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof b) {
                    b bVar = (b) obj;
                    if (j.a((Object) this.f133a, (Object) bVar.f133a) && j.a((Object) this.f134b, (Object) bVar.f134b)) {
                        if (this.c == bVar.c) {
                            return true;
                        }
                    }
                }
                return false;
            }
            return true;
        }

        public final int hashCode() {
            String str = this.f133a;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.f134b;
            if (str2 != null) {
                i = str2.hashCode();
            }
            int i2 = (hashCode + i) * 31;
            boolean z = this.c;
            if (z) {
                z = true;
            }
            return i2 + (z ? 1 : 0);
        }

        public final String toString() {
            StringBuilder a2 = b.a.a.a.a.a("Scope(id=");
            a2.append(this.f133a);
            a2.append(", localizedName=");
            a2.append(this.f134b);
            a2.append(", hasConsent=");
            a2.append(this.c);
            a2.append(")");
            return a2.toString();
        }
    }

    public m(boolean z, List<b> list) {
        j.b(list, "marketingScopeConsents");
        this.f131a = z;
        this.f132b = list;
    }

    public final m a(boolean z, List<b> list) {
        j.b(list, "marketingScopeConsents");
        return new m(z, list);
    }

    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof m) {
                m mVar = (m) obj;
                if (!(this.f131a == mVar.f131a) || !j.a(this.f132b, mVar.f132b)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        boolean z = this.f131a;
        if (z) {
            z = true;
        }
        int i = (z ? 1 : 0) * true;
        List<b> list = this.f132b;
        return i + (list != null ? list.hashCode() : 0);
    }

    public final String toString() {
        StringBuilder a2 = b.a.a.a.a.a("IdSubscriptions(hasEmailMarketingPermission=");
        a2.append(this.f131a);
        a2.append(", marketingScopeConsents=");
        a2.append(this.f132b);
        a2.append(")");
        return a2.toString();
    }
}
