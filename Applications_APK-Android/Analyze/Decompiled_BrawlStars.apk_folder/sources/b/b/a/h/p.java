package b.b.a.h;

import b.b.a.h.j;

public enum p {
    STRANGER(j.d.class),
    REQUEST_SENT(j.a.c.class),
    REQUEST_RECEIVED(j.a.b.class),
    FRIEND(j.a.C0008a.class);
    

    /* renamed from: a  reason: collision with root package name */
    public final Class<? extends j> f140a;

    /* access modifiers changed from: public */
    p(Class<? extends j> cls) {
        this.f140a = cls;
    }
}
