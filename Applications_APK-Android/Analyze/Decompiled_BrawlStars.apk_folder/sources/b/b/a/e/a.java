package b.b.a.e;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.SoundPool;
import android.os.Build;
import b.b.a.i.x1.d;
import com.supercell.id.SupercellId;
import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.d.b.g;
import kotlin.d.b.j;
import kotlin.d.b.k;
import kotlin.j.t;
import kotlin.m;
import nl.komponents.kovenant.bb;
import nl.komponents.kovenant.bw;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    public final Map<C0004a, Integer> f56a = new LinkedHashMap();

    /* renamed from: b  reason: collision with root package name */
    public SoundPool f57b = b();

    /* renamed from: b.b.a.e.a$a  reason: collision with other inner class name */
    public enum C0004a {
        BUTTON_01("generic_button_01"),
        CANCEL_BUTTON_01("cancel_btn_01"),
        TAB_SWITCH("switching_tabs_jump_02");
        
        public static final C0004a[] f = values();
        public static final C0005a g = new C0005a(null);

        /* renamed from: a  reason: collision with root package name */
        public final String f59a;

        /* renamed from: b.b.a.e.a$a$a  reason: collision with other inner class name */
        public static final class C0005a {
            public C0005a() {
            }

            public /* synthetic */ C0005a(g gVar) {
            }

            public final C0004a a(int i) {
                if (i >= 0) {
                    C0004a[] aVarArr = C0004a.f;
                    if (i < aVarArr.length) {
                        return aVarArr[i];
                    }
                }
                C0005a.class.getSimpleName();
                "Audio effect index " + i + " out of bounds";
                return C0004a.f[0];
            }
        }

        /* access modifiers changed from: public */
        C0004a(String str) {
            this.f59a = str;
        }
    }

    public final class b extends k implements kotlin.d.a.a<m> {

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ Context f61b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Context context) {
            super(0);
            this.f61b = context;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String[], java.lang.String]
         candidates:
          kotlin.d.b.j.a(int, int):int
          kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
          kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
          kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
        public final Object invoke() {
            File a2;
            if (SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getSfxEnabled() && (a2 = d.f911b.a(this.f61b)) != null && a2.isDirectory()) {
                String[] list = a2.list();
                j.a((Object) list, "audioDirectory.list()");
                for (String file : list) {
                    a.this.a(new File(a2, file));
                }
            }
            return m.f5330a;
        }
    }

    public final bw<m, Exception> a(Context context) {
        j.b(context, "context");
        return bb.f5389a;
    }

    public final void a() {
        synchronized (this) {
            this.f56a.clear();
            this.f57b.release();
            this.f57b = b();
            m mVar = m.f5330a;
        }
    }

    public final void a(C0004a aVar) {
        j.b(aVar, "effect");
        if (SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getSfxEnabled()) {
            synchronized (this) {
                Integer num = this.f56a.get(aVar);
                if (num != null) {
                    this.f57b.play(num.intValue(), 0.25f, 0.25f, 1, 0, 1.0f);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.media.SoundPool, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public final SoundPool b() {
        if (Build.VERSION.SDK_INT < 21) {
            return new SoundPool(1, 3, 0);
        }
        SoundPool build = new SoundPool.Builder().setMaxStreams(1).setAudioAttributes(new AudioAttributes.Builder().setContentType(4).setUsage(14).build()).build();
        j.a((Object) build, "SoundPool.Builder()\n    …                 .build()");
        return build;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean */
    public final void a(File file) {
        C0004a aVar;
        j.b(file, "file");
        if (SupercellId.INSTANCE.getSharedServices$supercellId_release().e.getSfxEnabled()) {
            C0004a[] values = C0004a.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    aVar = null;
                    break;
                }
                aVar = values[i];
                j.b(file, "$this$nameWithoutExtension");
                String name = file.getName();
                j.a((Object) name, "name");
                j.b(name, "$this$substringBeforeLast");
                j.b(".", "delimiter");
                j.b(name, "missingDelimiterValue");
                int b2 = t.b(name, ".", 0, false, 6);
                if (b2 != -1) {
                    name = name.substring(0, b2);
                    j.a((Object) name, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                }
                if (j.a((Object) name, (Object) aVar.f59a)) {
                    break;
                }
                i++;
            }
            if (aVar != null) {
                synchronized (this) {
                    if (this.f56a.get(aVar) == null) {
                        int load = this.f57b.load(file.getAbsolutePath(), 1);
                        if (load != 0) {
                            this.f56a.put(aVar, Integer.valueOf(load));
                            m mVar = m.f5330a;
                        }
                    }
                }
            }
        }
    }
}
