package b.a.a.a;

import android.support.v7.util.DiffUtil;
import b.b.a.j.t0;
import java.util.List;
import kotlin.d.b.j;

/* compiled from: outline */
public class a {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kotlin.d.b.j.a(java.lang.Object, java.lang.String):void
     arg types: [android.support.v7.util.DiffUtil$DiffResult, java.lang.String]
     candidates:
      kotlin.d.b.j.a(int, int):int
      kotlin.d.b.j.a(java.lang.Throwable, java.lang.String):T
      kotlin.d.b.j.a(java.lang.Object, java.lang.Object):boolean
      kotlin.d.b.j.a(java.lang.Object, java.lang.String):void */
    public static DiffUtil.DiffResult a(t0.a aVar, List list, List list2, String str) {
        DiffUtil.DiffResult calculateDiff = DiffUtil.calculateDiff(aVar.a(list, list2));
        j.a((Object) calculateDiff, str);
        return calculateDiff;
    }

    public static String a(StringBuilder sb, String str, String str2) {
        sb.append(str);
        sb.append(str2);
        return sb.toString();
    }

    public static StringBuilder a(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        return sb;
    }
}
