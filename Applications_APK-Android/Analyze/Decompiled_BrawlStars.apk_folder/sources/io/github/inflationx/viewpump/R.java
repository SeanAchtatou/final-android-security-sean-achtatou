package io.github.inflationx.viewpump;

public final class R {
    private R() {
    }

    public static final class id {
        public static final int viewpump_tag_id = 2131231492;

        private id() {
        }
    }
}
