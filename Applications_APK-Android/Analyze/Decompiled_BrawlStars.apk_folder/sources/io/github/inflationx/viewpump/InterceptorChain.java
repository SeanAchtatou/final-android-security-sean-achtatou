package io.github.inflationx.viewpump;

import io.github.inflationx.viewpump.Interceptor;
import java.util.List;

class InterceptorChain implements Interceptor.Chain {
    private final int index;
    private final List<Interceptor> interceptors;
    private final InflateRequest request;

    InterceptorChain(List<Interceptor> list, int i, InflateRequest inflateRequest) {
        this.interceptors = list;
        this.index = i;
        this.request = inflateRequest;
    }

    public InflateRequest request() {
        return this.request;
    }

    public InflateResult proceed(InflateRequest inflateRequest) {
        if (this.index < this.interceptors.size()) {
            InterceptorChain interceptorChain = new InterceptorChain(this.interceptors, this.index + 1, inflateRequest);
            Interceptor interceptor = this.interceptors.get(this.index);
            InflateResult intercept = interceptor.intercept(interceptorChain);
            if (intercept != null) {
                return intercept;
            }
            throw new NullPointerException("interceptor " + interceptor + " returned null");
        }
        throw new AssertionError("no interceptors added to the chain");
    }
}
