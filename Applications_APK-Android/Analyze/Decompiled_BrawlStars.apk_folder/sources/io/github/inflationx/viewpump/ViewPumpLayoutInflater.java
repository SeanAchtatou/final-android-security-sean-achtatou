package io.github.inflationx.viewpump;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import org.xmlpull.v1.XmlPullParser;

class ViewPumpLayoutInflater extends LayoutInflater implements ViewPumpActivityFactory {
    /* access modifiers changed from: private */
    public static final String[] sClassPrefixList = {"android.widget.", "android.webkit."};
    private Field mConstructorArgs = null;
    private boolean mSetPrivateFactory = false;
    private FallbackViewCreator nameAndAttrsViewCreator = new NameAndAttrsViewCreator(this);
    private FallbackViewCreator parentAndNameAndAttrsViewCreator = new ParentAndNameAndAttrsViewCreator(this);

    protected ViewPumpLayoutInflater(Context context) {
        super(context);
        setUpLayoutFactories(false);
    }

    protected ViewPumpLayoutInflater(LayoutInflater layoutInflater, Context context, boolean z) {
        super(layoutInflater, context);
        setUpLayoutFactories(z);
    }

    public LayoutInflater cloneInContext(Context context) {
        return new ViewPumpLayoutInflater(this, context, true);
    }

    public View inflate(XmlPullParser xmlPullParser, ViewGroup viewGroup, boolean z) {
        setPrivateFactoryInternal();
        return super.inflate(xmlPullParser, viewGroup, z);
    }

    private void setUpLayoutFactories(boolean z) {
        if (!z) {
            if (getFactory2() != null && !(getFactory2() instanceof WrapperFactory2)) {
                setFactory2(getFactory2());
            }
            if (getFactory() != null && !(getFactory() instanceof WrapperFactory)) {
                setFactory(getFactory());
            }
        }
    }

    public void setFactory(LayoutInflater.Factory factory) {
        if (!(factory instanceof WrapperFactory)) {
            super.setFactory(new WrapperFactory(factory));
        } else {
            super.setFactory(factory);
        }
    }

    public void setFactory2(LayoutInflater.Factory2 factory2) {
        if (!(factory2 instanceof WrapperFactory2)) {
            super.setFactory2(new WrapperFactory2(factory2));
        } else {
            super.setFactory2(factory2);
        }
    }

    private void setPrivateFactoryInternal() {
        if (this.mSetPrivateFactory || !ViewPump.get().isReflection()) {
            return;
        }
        if (!(getContext() instanceof LayoutInflater.Factory2)) {
            this.mSetPrivateFactory = true;
            return;
        }
        Method method = ReflectionUtils.getMethod(LayoutInflater.class, "setPrivateFactory");
        if (method != null) {
            ReflectionUtils.invokeMethod(this, method, new PrivateWrapperFactory2((LayoutInflater.Factory2) getContext(), this));
        }
        this.mSetPrivateFactory = true;
    }

    public View onActivityCreateView(View view, View view2, String str, Context context, AttributeSet attributeSet) {
        return ViewPump.get().inflate(InflateRequest.builder().name(str).context(context).attrs(attributeSet).parent(view).fallbackViewCreator(new ActivityViewCreator(this, view2)).build()).view();
    }

    /* access modifiers changed from: protected */
    public View onCreateView(View view, String str, AttributeSet attributeSet) throws ClassNotFoundException {
        return ViewPump.get().inflate(InflateRequest.builder().name(str).context(getContext()).attrs(attributeSet).parent(view).fallbackViewCreator(this.parentAndNameAndAttrsViewCreator).build()).view();
    }

    /* access modifiers changed from: protected */
    public View onCreateView(String str, AttributeSet attributeSet) throws ClassNotFoundException {
        return ViewPump.get().inflate(InflateRequest.builder().name(str).context(getContext()).attrs(attributeSet).fallbackViewCreator(this.nameAndAttrsViewCreator).build()).view();
    }

    /* access modifiers changed from: private */
    public View createCustomViewInternal(View view, View view2, String str, Context context, AttributeSet attributeSet) {
        if (!ViewPump.get().isCustomViewCreation()) {
            return view2;
        }
        if (view2 == null && str.indexOf(46) > -1) {
            if (this.mConstructorArgs == null) {
                this.mConstructorArgs = ReflectionUtils.getField(LayoutInflater.class, "mConstructorArgs");
            }
            Object[] objArr = (Object[]) ReflectionUtils.getValue(this.mConstructorArgs, this);
            Object obj = objArr[0];
            objArr[0] = context;
            ReflectionUtils.setValue(this.mConstructorArgs, this, objArr);
            try {
                view2 = createView(str, null, attributeSet);
                objArr[0] = obj;
            } catch (ClassNotFoundException unused) {
                objArr[0] = obj;
            } catch (Throwable th) {
                objArr[0] = obj;
                ReflectionUtils.setValue(this.mConstructorArgs, this, objArr);
                throw th;
            }
            ReflectionUtils.setValue(this.mConstructorArgs, this, objArr);
        }
        return view2;
    }

    /* access modifiers changed from: private */
    public View superOnCreateView(View view, String str, AttributeSet attributeSet) {
        try {
            return super.onCreateView(view, str, attributeSet);
        } catch (ClassNotFoundException unused) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public View superOnCreateView(String str, AttributeSet attributeSet) {
        try {
            return super.onCreateView(str, attributeSet);
        } catch (ClassNotFoundException unused) {
            return null;
        }
    }

    static class ActivityViewCreator implements FallbackViewCreator {
        private final ViewPumpLayoutInflater inflater;
        private final View view;

        public ActivityViewCreator(ViewPumpLayoutInflater viewPumpLayoutInflater, View view2) {
            this.inflater = viewPumpLayoutInflater;
            this.view = view2;
        }

        public View onCreateView(View view2, String str, Context context, AttributeSet attributeSet) {
            return this.inflater.createCustomViewInternal(view2, this.view, str, context, attributeSet);
        }
    }

    static class ParentAndNameAndAttrsViewCreator implements FallbackViewCreator {
        private final ViewPumpLayoutInflater inflater;

        public ParentAndNameAndAttrsViewCreator(ViewPumpLayoutInflater viewPumpLayoutInflater) {
            this.inflater = viewPumpLayoutInflater;
        }

        public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
            return this.inflater.superOnCreateView(view, str, attributeSet);
        }
    }

    static class NameAndAttrsViewCreator implements FallbackViewCreator {
        private final ViewPumpLayoutInflater inflater;

        public NameAndAttrsViewCreator(ViewPumpLayoutInflater viewPumpLayoutInflater) {
            this.inflater = viewPumpLayoutInflater;
        }

        public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
            String[] access$200 = ViewPumpLayoutInflater.sClassPrefixList;
            int length = access$200.length;
            View view2 = null;
            int i = 0;
            while (i < length) {
                try {
                    view2 = this.inflater.createView(str, access$200[i], attributeSet);
                    if (view2 != null) {
                        break;
                    }
                    i++;
                } catch (ClassNotFoundException unused) {
                }
            }
            return view2 == null ? this.inflater.superOnCreateView(str, attributeSet) : view2;
        }
    }

    static class WrapperFactory implements LayoutInflater.Factory {
        private final FallbackViewCreator mViewCreator;

        public WrapperFactory(LayoutInflater.Factory factory) {
            this.mViewCreator = new WrapperFactoryViewCreator(factory);
        }

        public View onCreateView(String str, Context context, AttributeSet attributeSet) {
            return ViewPump.get().inflate(InflateRequest.builder().name(str).context(context).attrs(attributeSet).fallbackViewCreator(this.mViewCreator).build()).view();
        }
    }

    static class WrapperFactoryViewCreator implements FallbackViewCreator {
        protected final LayoutInflater.Factory mFactory;

        public WrapperFactoryViewCreator(LayoutInflater.Factory factory) {
            this.mFactory = factory;
        }

        public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
            return this.mFactory.onCreateView(str, context, attributeSet);
        }
    }

    static class WrapperFactory2 implements LayoutInflater.Factory2 {
        protected final LayoutInflater.Factory2 mFactory2;
        private final WrapperFactory2ViewCreator mViewCreator;

        public WrapperFactory2(LayoutInflater.Factory2 factory2) {
            this.mFactory2 = factory2;
            this.mViewCreator = new WrapperFactory2ViewCreator(factory2);
        }

        public View onCreateView(String str, Context context, AttributeSet attributeSet) {
            return onCreateView(null, str, context, attributeSet);
        }

        public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
            return ViewPump.get().inflate(InflateRequest.builder().name(str).context(context).attrs(attributeSet).parent(view).fallbackViewCreator(this.mViewCreator).build()).view();
        }
    }

    static class WrapperFactory2ViewCreator implements FallbackViewCreator {
        protected final LayoutInflater.Factory2 mFactory2;

        public WrapperFactory2ViewCreator(LayoutInflater.Factory2 factory2) {
            this.mFactory2 = factory2;
        }

        public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
            return this.mFactory2.onCreateView(view, str, context, attributeSet);
        }
    }

    static class PrivateWrapperFactory2 extends WrapperFactory2 {
        private final PrivateWrapperFactory2ViewCreator mViewCreator;

        public PrivateWrapperFactory2(LayoutInflater.Factory2 factory2, ViewPumpLayoutInflater viewPumpLayoutInflater) {
            super(factory2);
            this.mViewCreator = new PrivateWrapperFactory2ViewCreator(factory2, viewPumpLayoutInflater);
        }

        public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
            return ViewPump.get().inflate(InflateRequest.builder().name(str).context(context).attrs(attributeSet).parent(view).fallbackViewCreator(this.mViewCreator).build()).view();
        }
    }

    static class PrivateWrapperFactory2ViewCreator extends WrapperFactory2ViewCreator implements FallbackViewCreator {
        private final ViewPumpLayoutInflater mInflater;

        public PrivateWrapperFactory2ViewCreator(LayoutInflater.Factory2 factory2, ViewPumpLayoutInflater viewPumpLayoutInflater) {
            super(factory2);
            this.mInflater = viewPumpLayoutInflater;
        }

        public View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
            return this.mInflater.createCustomViewInternal(view, this.mFactory2.onCreateView(view, str, context, attributeSet), str, context, attributeSet);
        }
    }
}
