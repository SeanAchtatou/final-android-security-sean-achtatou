package io.github.inflationx.viewpump;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

public class InflateRequest {
    /* access modifiers changed from: private */
    public final AttributeSet attrs;
    /* access modifiers changed from: private */
    public final Context context;
    /* access modifiers changed from: private */
    public final FallbackViewCreator fallbackViewCreator;
    /* access modifiers changed from: private */
    public final String name;
    /* access modifiers changed from: private */
    public final View parent;

    private InflateRequest(Builder builder) {
        this.name = builder.name;
        this.context = builder.context;
        this.attrs = builder.attrs;
        this.parent = builder.parent;
        this.fallbackViewCreator = builder.fallbackViewCreator;
    }

    public String name() {
        return this.name;
    }

    public Context context() {
        return this.context;
    }

    public AttributeSet attrs() {
        return this.attrs;
    }

    public View parent() {
        return this.parent;
    }

    public FallbackViewCreator fallbackViewCreator() {
        return this.fallbackViewCreator;
    }

    public static Builder builder() {
        return new Builder();
    }

    public Builder toBuilder() {
        return new Builder();
    }

    public String toString() {
        return "InflateRequest{name='" + this.name + '\'' + ", context=" + this.context + ", attrs=" + this.attrs + ", parent=" + this.parent + ", fallbackViewCreator=" + this.fallbackViewCreator + '}';
    }

    public static final class Builder {
        /* access modifiers changed from: private */
        public AttributeSet attrs;
        /* access modifiers changed from: private */
        public Context context;
        /* access modifiers changed from: private */
        public FallbackViewCreator fallbackViewCreator;
        /* access modifiers changed from: private */
        public String name;
        /* access modifiers changed from: private */
        public View parent;

        private Builder() {
        }

        private Builder(InflateRequest inflateRequest) {
            this.name = inflateRequest.name;
            this.context = inflateRequest.context;
            this.attrs = inflateRequest.attrs;
            this.parent = inflateRequest.parent;
            this.fallbackViewCreator = inflateRequest.fallbackViewCreator;
        }

        public final Builder name(String str) {
            this.name = str;
            return this;
        }

        public final Builder context(Context context2) {
            this.context = context2;
            return this;
        }

        public final Builder attrs(AttributeSet attributeSet) {
            this.attrs = attributeSet;
            return this;
        }

        public final Builder parent(View view) {
            this.parent = view;
            return this;
        }

        public final Builder fallbackViewCreator(FallbackViewCreator fallbackViewCreator2) {
            this.fallbackViewCreator = fallbackViewCreator2;
            return this;
        }

        public final InflateRequest build() {
            if (this.name == null) {
                throw new IllegalStateException("name == null");
            } else if (this.context == null) {
                throw new IllegalStateException("context == null");
            } else if (this.fallbackViewCreator != null) {
                return new InflateRequest(this);
            } else {
                throw new IllegalStateException("fallbackViewCreator == null");
            }
        }
    }
}
