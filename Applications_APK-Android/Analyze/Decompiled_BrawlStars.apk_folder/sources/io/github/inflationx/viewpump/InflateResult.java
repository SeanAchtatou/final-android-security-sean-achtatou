package io.github.inflationx.viewpump;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

public class InflateResult {
    /* access modifiers changed from: private */
    public final AttributeSet attrs;
    /* access modifiers changed from: private */
    public final Context context;
    /* access modifiers changed from: private */
    public final String name;
    /* access modifiers changed from: private */
    public final View view;

    private InflateResult(Builder builder) {
        this.view = builder.view;
        this.name = builder.name;
        this.context = builder.context;
        this.attrs = builder.attrs;
    }

    public View view() {
        return this.view;
    }

    public String name() {
        return this.name;
    }

    public Context context() {
        return this.context;
    }

    public AttributeSet attrs() {
        return this.attrs;
    }

    public static Builder builder() {
        return new Builder();
    }

    public Builder toBuilder() {
        return new Builder();
    }

    public String toString() {
        return "InflateResult{view=" + this.view + ", name=" + this.name + ", context=" + this.context + ", attrs=" + this.attrs + '}';
    }

    public static final class Builder {
        /* access modifiers changed from: private */
        public AttributeSet attrs;
        /* access modifiers changed from: private */
        public Context context;
        /* access modifiers changed from: private */
        public String name;
        /* access modifiers changed from: private */
        public View view;

        private Builder() {
        }

        private Builder(InflateResult inflateResult) {
            this.view = inflateResult.view;
            this.name = inflateResult.name;
            this.context = inflateResult.context;
            this.attrs = inflateResult.attrs;
        }

        public final Builder view(View view2) {
            this.view = view2;
            return this;
        }

        public final Builder name(String str) {
            this.name = str;
            return this;
        }

        public final Builder context(Context context2) {
            this.context = context2;
            return this;
        }

        public final Builder attrs(AttributeSet attributeSet) {
            this.attrs = attributeSet;
            return this;
        }

        public final InflateResult build() {
            String str = this.name;
            if (str == null) {
                throw new IllegalStateException("name == null");
            } else if (this.context != null) {
                View view2 = this.view;
                if (view2 == null || str.equals(view2.getClass().getName())) {
                    return new InflateResult(this);
                }
                throw new IllegalStateException("name (" + this.name + ") must be the view's fully qualified name (" + this.view.getClass().getName() + ")");
            } else {
                throw new IllegalStateException("context == null");
            }
        }
    }
}
