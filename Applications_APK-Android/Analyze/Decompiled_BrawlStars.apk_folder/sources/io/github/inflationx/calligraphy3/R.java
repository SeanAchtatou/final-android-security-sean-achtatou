package io.github.inflationx.calligraphy3;

public final class R {
    private R() {
    }

    public static final class attr {
        public static final int fontPath = 2130903285;

        private attr() {
        }
    }

    public static final class id {
        public static final int calligraphy_tag_id = 2131230839;
        public static final int viewpump_tag_id = 2131231492;

        private id() {
        }
    }
}
