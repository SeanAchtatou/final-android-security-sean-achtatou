package com.google.i18n.phonenumbers;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.List;

/* compiled from: Phonemetadata */
public final class i {

    /* compiled from: Phonemetadata */
    public static class a implements Externalizable {
        private static final long serialVersionUID = 1;

        /* renamed from: a  reason: collision with root package name */
        String f2868a = "";

        /* renamed from: b  reason: collision with root package name */
        String f2869b = "";
        String c = "";
        String d = "";
        private boolean e;
        private boolean f;
        private List<String> g = new ArrayList();
        private boolean h;
        private boolean i;
        private boolean j = false;
        private boolean k;

        public final int a() {
            return this.g.size();
        }

        public final String a(int i2) {
            return this.g.get(i2);
        }

        public void writeExternal(ObjectOutput objectOutput) throws IOException {
            objectOutput.writeUTF(this.f2868a);
            objectOutput.writeUTF(this.f2869b);
            int a2 = a();
            objectOutput.writeInt(a2);
            for (int i2 = 0; i2 < a2; i2++) {
                objectOutput.writeUTF(this.g.get(i2));
            }
            objectOutput.writeBoolean(this.h);
            if (this.h) {
                objectOutput.writeUTF(this.c);
            }
            objectOutput.writeBoolean(this.k);
            if (this.k) {
                objectOutput.writeUTF(this.d);
            }
            objectOutput.writeBoolean(this.j);
        }

        public void readExternal(ObjectInput objectInput) throws IOException {
            String readUTF = objectInput.readUTF();
            this.e = true;
            this.f2868a = readUTF;
            String readUTF2 = objectInput.readUTF();
            this.f = true;
            this.f2869b = readUTF2;
            int readInt = objectInput.readInt();
            for (int i2 = 0; i2 < readInt; i2++) {
                this.g.add(objectInput.readUTF());
            }
            if (objectInput.readBoolean()) {
                String readUTF3 = objectInput.readUTF();
                this.h = true;
                this.c = readUTF3;
            }
            if (objectInput.readBoolean()) {
                String readUTF4 = objectInput.readUTF();
                this.k = true;
                this.d = readUTF4;
            }
            boolean readBoolean = objectInput.readBoolean();
            this.i = true;
            this.j = readBoolean;
        }
    }

    /* compiled from: Phonemetadata */
    public static class d implements Externalizable {
        private static final long serialVersionUID = 1;

        /* renamed from: a  reason: collision with root package name */
        public String f2873a = "";

        /* renamed from: b  reason: collision with root package name */
        List<Integer> f2874b = new ArrayList();
        List<Integer> c = new ArrayList();
        private boolean d;
        private boolean e;
        private String f = "";

        public final int a() {
            return this.f2874b.size();
        }

        public void writeExternal(ObjectOutput objectOutput) throws IOException {
            objectOutput.writeBoolean(this.d);
            if (this.d) {
                objectOutput.writeUTF(this.f2873a);
            }
            int a2 = a();
            objectOutput.writeInt(a2);
            for (int i = 0; i < a2; i++) {
                objectOutput.writeInt(this.f2874b.get(i).intValue());
            }
            int size = this.c.size();
            objectOutput.writeInt(size);
            for (int i2 = 0; i2 < size; i2++) {
                objectOutput.writeInt(this.c.get(i2).intValue());
            }
            objectOutput.writeBoolean(this.e);
            if (this.e) {
                objectOutput.writeUTF(this.f);
            }
        }

        public void readExternal(ObjectInput objectInput) throws IOException {
            if (objectInput.readBoolean()) {
                String readUTF = objectInput.readUTF();
                this.d = true;
                this.f2873a = readUTF;
            }
            int readInt = objectInput.readInt();
            for (int i = 0; i < readInt; i++) {
                this.f2874b.add(Integer.valueOf(objectInput.readInt()));
            }
            int readInt2 = objectInput.readInt();
            for (int i2 = 0; i2 < readInt2; i2++) {
                this.c.add(Integer.valueOf(objectInput.readInt()));
            }
            if (objectInput.readBoolean()) {
                String readUTF2 = objectInput.readUTF();
                this.e = true;
                this.f = readUTF2;
            }
        }
    }

    /* compiled from: Phonemetadata */
    public static class b implements Externalizable {
        private static final long serialVersionUID = 1;
        private boolean A;
        private boolean B;
        private boolean C;
        private boolean D;
        private boolean E;
        private boolean F;
        private boolean G;
        private d H = null;
        private boolean I;
        private boolean J;
        private d K = null;
        private boolean L;
        private d M = null;
        private boolean N;
        private d O = null;
        private boolean P;
        private d Q = null;
        private boolean R;
        private d S = null;
        private boolean T;
        private String U = "";
        private boolean V;
        private boolean W;
        private boolean X;
        private String Y = "";
        private boolean Z;

        /* renamed from: a  reason: collision with root package name */
        d f2870a = null;
        private String aa = "";
        private boolean ab;
        private boolean ac;
        private boolean ad;
        private boolean ae;
        private boolean af = false;
        private boolean ag;
        private boolean ah = false;
        private boolean ai;
        private boolean aj = false;

        /* renamed from: b  reason: collision with root package name */
        d f2871b = null;
        d c = null;
        d d = null;
        d e = null;
        d f = null;
        d g = null;
        d h = null;
        d i = null;
        d j = null;
        d k = null;
        int l = 0;
        String m = "";
        boolean n;
        String o = "";
        String p = "";
        String q = "";
        boolean r = false;
        List<a> s = new ArrayList();
        List<a> t = new ArrayList();
        boolean u;
        String v = "";
        private boolean w;
        private boolean x;
        private boolean y;
        private boolean z;

        public void writeExternal(ObjectOutput objectOutput) throws IOException {
            objectOutput.writeBoolean(this.w);
            if (this.w) {
                this.f2870a.writeExternal(objectOutput);
            }
            objectOutput.writeBoolean(this.x);
            if (this.x) {
                this.f2871b.writeExternal(objectOutput);
            }
            objectOutput.writeBoolean(this.y);
            if (this.y) {
                this.c.writeExternal(objectOutput);
            }
            objectOutput.writeBoolean(this.z);
            if (this.z) {
                this.d.writeExternal(objectOutput);
            }
            objectOutput.writeBoolean(this.A);
            if (this.A) {
                this.e.writeExternal(objectOutput);
            }
            objectOutput.writeBoolean(this.B);
            if (this.B) {
                this.f.writeExternal(objectOutput);
            }
            objectOutput.writeBoolean(this.C);
            if (this.C) {
                this.g.writeExternal(objectOutput);
            }
            objectOutput.writeBoolean(this.D);
            if (this.D) {
                this.h.writeExternal(objectOutput);
            }
            objectOutput.writeBoolean(this.E);
            if (this.E) {
                this.i.writeExternal(objectOutput);
            }
            objectOutput.writeBoolean(this.F);
            if (this.F) {
                this.j.writeExternal(objectOutput);
            }
            objectOutput.writeBoolean(this.G);
            if (this.G) {
                this.H.writeExternal(objectOutput);
            }
            objectOutput.writeBoolean(this.I);
            if (this.I) {
                this.k.writeExternal(objectOutput);
            }
            objectOutput.writeBoolean(this.J);
            if (this.J) {
                this.K.writeExternal(objectOutput);
            }
            objectOutput.writeBoolean(this.L);
            if (this.L) {
                this.M.writeExternal(objectOutput);
            }
            objectOutput.writeBoolean(this.N);
            if (this.N) {
                this.O.writeExternal(objectOutput);
            }
            objectOutput.writeBoolean(this.P);
            if (this.P) {
                this.Q.writeExternal(objectOutput);
            }
            objectOutput.writeBoolean(this.R);
            if (this.R) {
                this.S.writeExternal(objectOutput);
            }
            objectOutput.writeUTF(this.U);
            objectOutput.writeInt(this.l);
            objectOutput.writeUTF(this.m);
            objectOutput.writeBoolean(this.X);
            if (this.X) {
                objectOutput.writeUTF(this.Y);
            }
            objectOutput.writeBoolean(this.Z);
            if (this.Z) {
                objectOutput.writeUTF(this.aa);
            }
            objectOutput.writeBoolean(this.n);
            if (this.n) {
                objectOutput.writeUTF(this.o);
            }
            objectOutput.writeBoolean(this.ab);
            if (this.ab) {
                objectOutput.writeUTF(this.p);
            }
            objectOutput.writeBoolean(this.ac);
            if (this.ac) {
                objectOutput.writeUTF(this.q);
            }
            objectOutput.writeBoolean(this.r);
            int size = this.s.size();
            objectOutput.writeInt(size);
            for (int i2 = 0; i2 < size; i2++) {
                this.s.get(i2).writeExternal(objectOutput);
            }
            int size2 = this.t.size();
            objectOutput.writeInt(size2);
            for (int i3 = 0; i3 < size2; i3++) {
                this.t.get(i3).writeExternal(objectOutput);
            }
            objectOutput.writeBoolean(this.af);
            objectOutput.writeBoolean(this.u);
            if (this.u) {
                objectOutput.writeUTF(this.v);
            }
            objectOutput.writeBoolean(this.ah);
            objectOutput.writeBoolean(this.aj);
        }

        public void readExternal(ObjectInput objectInput) throws IOException {
            if (objectInput.readBoolean()) {
                d dVar = new d();
                dVar.readExternal(objectInput);
                this.w = true;
                this.f2870a = dVar;
            }
            if (objectInput.readBoolean()) {
                d dVar2 = new d();
                dVar2.readExternal(objectInput);
                this.x = true;
                this.f2871b = dVar2;
            }
            if (objectInput.readBoolean()) {
                d dVar3 = new d();
                dVar3.readExternal(objectInput);
                this.y = true;
                this.c = dVar3;
            }
            if (objectInput.readBoolean()) {
                d dVar4 = new d();
                dVar4.readExternal(objectInput);
                this.z = true;
                this.d = dVar4;
            }
            if (objectInput.readBoolean()) {
                d dVar5 = new d();
                dVar5.readExternal(objectInput);
                this.A = true;
                this.e = dVar5;
            }
            if (objectInput.readBoolean()) {
                d dVar6 = new d();
                dVar6.readExternal(objectInput);
                this.B = true;
                this.f = dVar6;
            }
            if (objectInput.readBoolean()) {
                d dVar7 = new d();
                dVar7.readExternal(objectInput);
                this.C = true;
                this.g = dVar7;
            }
            if (objectInput.readBoolean()) {
                d dVar8 = new d();
                dVar8.readExternal(objectInput);
                this.D = true;
                this.h = dVar8;
            }
            if (objectInput.readBoolean()) {
                d dVar9 = new d();
                dVar9.readExternal(objectInput);
                this.E = true;
                this.i = dVar9;
            }
            if (objectInput.readBoolean()) {
                d dVar10 = new d();
                dVar10.readExternal(objectInput);
                this.F = true;
                this.j = dVar10;
            }
            if (objectInput.readBoolean()) {
                d dVar11 = new d();
                dVar11.readExternal(objectInput);
                this.G = true;
                this.H = dVar11;
            }
            if (objectInput.readBoolean()) {
                d dVar12 = new d();
                dVar12.readExternal(objectInput);
                this.I = true;
                this.k = dVar12;
            }
            if (objectInput.readBoolean()) {
                d dVar13 = new d();
                dVar13.readExternal(objectInput);
                this.J = true;
                this.K = dVar13;
            }
            if (objectInput.readBoolean()) {
                d dVar14 = new d();
                dVar14.readExternal(objectInput);
                this.L = true;
                this.M = dVar14;
            }
            if (objectInput.readBoolean()) {
                d dVar15 = new d();
                dVar15.readExternal(objectInput);
                this.N = true;
                this.O = dVar15;
            }
            if (objectInput.readBoolean()) {
                d dVar16 = new d();
                dVar16.readExternal(objectInput);
                this.P = true;
                this.Q = dVar16;
            }
            if (objectInput.readBoolean()) {
                d dVar17 = new d();
                dVar17.readExternal(objectInput);
                this.R = true;
                this.S = dVar17;
            }
            String readUTF = objectInput.readUTF();
            this.T = true;
            this.U = readUTF;
            int readInt = objectInput.readInt();
            this.V = true;
            this.l = readInt;
            String readUTF2 = objectInput.readUTF();
            this.W = true;
            this.m = readUTF2;
            if (objectInput.readBoolean()) {
                String readUTF3 = objectInput.readUTF();
                this.X = true;
                this.Y = readUTF3;
            }
            if (objectInput.readBoolean()) {
                String readUTF4 = objectInput.readUTF();
                this.Z = true;
                this.aa = readUTF4;
            }
            if (objectInput.readBoolean()) {
                String readUTF5 = objectInput.readUTF();
                this.n = true;
                this.o = readUTF5;
            }
            if (objectInput.readBoolean()) {
                String readUTF6 = objectInput.readUTF();
                this.ab = true;
                this.p = readUTF6;
            }
            if (objectInput.readBoolean()) {
                String readUTF7 = objectInput.readUTF();
                this.ac = true;
                this.q = readUTF7;
            }
            boolean readBoolean = objectInput.readBoolean();
            this.ad = true;
            this.r = readBoolean;
            int readInt2 = objectInput.readInt();
            for (int i2 = 0; i2 < readInt2; i2++) {
                a aVar = new a();
                aVar.readExternal(objectInput);
                this.s.add(aVar);
            }
            int readInt3 = objectInput.readInt();
            for (int i3 = 0; i3 < readInt3; i3++) {
                a aVar2 = new a();
                aVar2.readExternal(objectInput);
                this.t.add(aVar2);
            }
            boolean readBoolean2 = objectInput.readBoolean();
            this.ae = true;
            this.af = readBoolean2;
            if (objectInput.readBoolean()) {
                String readUTF8 = objectInput.readUTF();
                this.u = true;
                this.v = readUTF8;
            }
            boolean readBoolean3 = objectInput.readBoolean();
            this.ag = true;
            this.ah = readBoolean3;
            boolean readBoolean4 = objectInput.readBoolean();
            this.ai = true;
            this.aj = readBoolean4;
        }
    }

    /* compiled from: Phonemetadata */
    public static class c implements Externalizable {
        private static final long serialVersionUID = 1;

        /* renamed from: a  reason: collision with root package name */
        List<b> f2872a = new ArrayList();

        public void readExternal(ObjectInput objectInput) throws IOException {
            int readInt = objectInput.readInt();
            for (int i = 0; i < readInt; i++) {
                b bVar = new b();
                bVar.readExternal(objectInput);
                this.f2872a.add(bVar);
            }
        }

        public void writeExternal(ObjectOutput objectOutput) throws IOException {
            int size = this.f2872a.size();
            objectOutput.writeInt(size);
            for (int i = 0; i < size; i++) {
                this.f2872a.get(i).writeExternal(objectOutput);
            }
        }
    }
}
