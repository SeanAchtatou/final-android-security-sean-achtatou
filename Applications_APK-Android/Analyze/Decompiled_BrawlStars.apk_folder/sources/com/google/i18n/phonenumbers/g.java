package com.google.i18n.phonenumbers;

import com.facebook.appevents.AppEventsConstants;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.i;
import com.google.i18n.phonenumbers.j;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: PhoneNumberUtil */
public class g {
    private static g A = null;

    /* renamed from: a  reason: collision with root package name */
    static final Pattern f2858a = Pattern.compile("[+＋]+");

    /* renamed from: b  reason: collision with root package name */
    static final Pattern f2859b = Pattern.compile("[\\\\/] *x");
    static final Pattern c = Pattern.compile("[[\\P{N}&&\\P{L}]&&[^#]]+$");
    static final String d = b("xｘ#＃~～");
    static final Pattern e = Pattern.compile("(\\D+)");
    private static final Logger g = Logger.getLogger(g.class.getName());
    private static final Map<Integer, String> h;
    private static final Set<Integer> i;
    private static final Set<Integer> j;
    private static final Map<Character, Character> k;
    private static final Map<Character, Character> l;
    private static final Map<Character, Character> m;
    private static final Map<Character, Character> n;
    private static final Pattern o = Pattern.compile("[\\d]+(?:[~⁓∼～][\\d]+)?");
    private static final String p = (Arrays.toString(l.keySet().toArray()).replaceAll("[, \\[\\]]", "") + Arrays.toString(l.keySet().toArray()).toLowerCase().replaceAll("[, \\[\\]]", ""));
    private static final Pattern q = Pattern.compile("[-x‐-―−ー－-／  ­​⁠　()（）［］.\\[\\]/~⁓∼～]+");
    private static final Pattern r = Pattern.compile("(\\p{Nd})");
    private static final Pattern s = Pattern.compile("[+＋\\p{Nd}]");
    private static final Pattern t = Pattern.compile("(?:.*?[A-Za-z]){3}.*");
    private static final String u = ("\\p{Nd}{2}|[+＋]*+(?:[-x‐-―−ー－-／  ­​⁠　()（）［］.\\[\\]/~⁓∼～*]*\\p{Nd}){3,}[-x‐-―−ー－-／  ­​⁠　()（）［］.\\[\\]/~⁓∼～*" + p + "\\p{Nd}" + "]*");
    private static final String v;
    private static final Pattern w = Pattern.compile("(?:" + v + ")$", 66);
    private static final Pattern x = Pattern.compile(u + "(?:" + v + ")?", 66);
    private static final Pattern y = Pattern.compile("(\\$\\d)");
    private static final Pattern z = Pattern.compile("\\(?\\$1\\)?");
    private final e B;
    private final Map<Integer, List<String>> C;
    private final com.google.i18n.phonenumbers.a.a D = new com.google.i18n.phonenumbers.a.b();
    private final Set<String> E = new HashSet(35);
    private final com.google.i18n.phonenumbers.a.c F = new com.google.i18n.phonenumbers.a.c(100);
    private final Set<Integer> G = new HashSet();
    public final Set<String> f = new HashSet(320);

    /* compiled from: PhoneNumberUtil */
    public enum a {
        E164,
        INTERNATIONAL,
        NATIONAL,
        RFC3966
    }

    /* compiled from: PhoneNumberUtil */
    public enum b {
        FIXED_LINE,
        MOBILE,
        FIXED_LINE_OR_MOBILE,
        TOLL_FREE,
        PREMIUM_RATE,
        SHARED_COST,
        VOIP,
        PERSONAL_NUMBER,
        PAGER,
        UAN,
        VOICEMAIL,
        UNKNOWN
    }

    /* compiled from: PhoneNumberUtil */
    public enum c {
        IS_POSSIBLE,
        IS_POSSIBLE_LOCAL_ONLY,
        INVALID_COUNTRY_CODE,
        TOO_SHORT,
        INVALID_LENGTH,
        TOO_LONG
    }

    static {
        HashMap hashMap = new HashMap();
        hashMap.put(52, AppEventsConstants.EVENT_PARAM_VALUE_YES);
        hashMap.put(54, "9");
        h = Collections.unmodifiableMap(hashMap);
        HashSet hashSet = new HashSet();
        hashSet.add(86);
        i = Collections.unmodifiableSet(hashSet);
        HashSet hashSet2 = new HashSet();
        hashSet2.add(52);
        hashSet2.add(54);
        hashSet2.add(55);
        hashSet2.add(62);
        hashSet2.addAll(hashSet);
        j = Collections.unmodifiableSet(hashSet2);
        HashMap hashMap2 = new HashMap();
        hashMap2.put('0', '0');
        hashMap2.put('1', '1');
        hashMap2.put('2', '2');
        hashMap2.put('3', '3');
        hashMap2.put('4', '4');
        hashMap2.put('5', '5');
        hashMap2.put('6', '6');
        hashMap2.put('7', '7');
        hashMap2.put('8', '8');
        hashMap2.put('9', '9');
        HashMap hashMap3 = new HashMap(40);
        hashMap3.put('A', '2');
        hashMap3.put('B', '2');
        hashMap3.put('C', '2');
        hashMap3.put('D', '3');
        hashMap3.put('E', '3');
        hashMap3.put('F', '3');
        hashMap3.put('G', '4');
        hashMap3.put('H', '4');
        hashMap3.put('I', '4');
        hashMap3.put('J', '5');
        hashMap3.put('K', '5');
        hashMap3.put('L', '5');
        hashMap3.put('M', '6');
        hashMap3.put('N', '6');
        hashMap3.put('O', '6');
        hashMap3.put('P', '7');
        hashMap3.put('Q', '7');
        hashMap3.put('R', '7');
        hashMap3.put('S', '7');
        hashMap3.put('T', '8');
        hashMap3.put('U', '8');
        hashMap3.put('V', '8');
        hashMap3.put('W', '9');
        hashMap3.put('X', '9');
        hashMap3.put('Y', '9');
        hashMap3.put('Z', '9');
        l = Collections.unmodifiableMap(hashMap3);
        HashMap hashMap4 = new HashMap(100);
        hashMap4.putAll(l);
        hashMap4.putAll(hashMap2);
        m = Collections.unmodifiableMap(hashMap4);
        HashMap hashMap5 = new HashMap();
        hashMap5.putAll(hashMap2);
        hashMap5.put('+', '+');
        hashMap5.put('*', '*');
        hashMap5.put('#', '#');
        k = Collections.unmodifiableMap(hashMap5);
        HashMap hashMap6 = new HashMap();
        for (Character charValue : l.keySet()) {
            char charValue2 = charValue.charValue();
            hashMap6.put(Character.valueOf(Character.toLowerCase(charValue2)), Character.valueOf(charValue2));
            hashMap6.put(Character.valueOf(charValue2), Character.valueOf(charValue2));
        }
        hashMap6.putAll(hashMap2);
        hashMap6.put('-', '-');
        hashMap6.put(65293, '-');
        hashMap6.put(8208, '-');
        hashMap6.put(8209, '-');
        hashMap6.put(8210, '-');
        hashMap6.put(8211, '-');
        hashMap6.put(8212, '-');
        hashMap6.put(8213, '-');
        hashMap6.put(8722, '-');
        hashMap6.put('/', '/');
        hashMap6.put(65295, '/');
        hashMap6.put(' ', ' ');
        hashMap6.put(12288, ' ');
        hashMap6.put(8288, ' ');
        hashMap6.put('.', '.');
        hashMap6.put(65294, '.');
        n = Collections.unmodifiableMap(hashMap6);
        StringBuilder sb = new StringBuilder();
        sb.append(",;");
        sb.append("xｘ#＃~～");
        v = b(sb.toString());
    }

    private static String b(String str) {
        return ";ext=(\\p{Nd}{1,7})|[  \\t,]*(?:e?xt(?:ensi(?:ó?|ó))?n?|ｅ?ｘｔｎ?|доб|[" + str + "]|int|anexo|ｉｎｔ)[:\\.．]?[  \\t,-]*" + "(\\p{Nd}{1,7})" + "#?|[- ]+(" + "\\p{Nd}" + "{1,5})#";
    }

    private g(e eVar, Map<Integer, List<String>> map) {
        this.B = eVar;
        this.C = map;
        for (Map.Entry next : map.entrySet()) {
            List list = (List) next.getValue();
            if (list.size() != 1 || !"001".equals(list.get(0))) {
                this.f.addAll(list);
            } else {
                this.G.add(next.getKey());
            }
        }
        if (this.f.remove("001")) {
            g.log(Level.WARNING, "invalid metadata (country calling code was mapped to the non-geo entity as well as specific region(s))");
        }
        this.E.addAll(map.get(1));
    }

    private static CharSequence a(CharSequence charSequence) {
        Matcher matcher = s.matcher(charSequence);
        if (!matcher.find()) {
            return "";
        }
        CharSequence subSequence = charSequence.subSequence(matcher.start(), charSequence.length());
        Matcher matcher2 = c.matcher(subSequence);
        if (matcher2.find()) {
            subSequence = subSequence.subSequence(0, matcher2.start());
        }
        Matcher matcher3 = f2859b.matcher(subSequence);
        return matcher3.find() ? subSequence.subSequence(0, matcher3.start()) : subSequence;
    }

    private static boolean b(CharSequence charSequence) {
        if (charSequence.length() < 2) {
            return false;
        }
        return x.matcher(charSequence).matches();
    }

    private static StringBuilder a(StringBuilder sb) {
        if (t.matcher(sb).matches()) {
            int length = sb.length();
            Map<Character, Character> map = m;
            StringBuilder sb2 = new StringBuilder(sb.length());
            for (int i2 = 0; i2 < sb.length(); i2++) {
                Character ch = map.get(Character.valueOf(Character.toUpperCase(sb.charAt(i2))));
                if (ch != null) {
                    sb2.append(ch);
                }
            }
            sb.replace(0, length, sb2.toString());
        } else {
            sb.replace(0, sb.length(), c(sb));
        }
        return sb;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.i18n.phonenumbers.g.a(java.lang.CharSequence, boolean):java.lang.StringBuilder
     arg types: [java.lang.CharSequence, int]
     candidates:
      com.google.i18n.phonenumbers.g.a(java.lang.StringBuilder, java.lang.StringBuilder):int
      com.google.i18n.phonenumbers.g.a(java.lang.String, com.google.i18n.phonenumbers.i$b):com.google.i18n.phonenumbers.g$b
      com.google.i18n.phonenumbers.g.a(java.lang.CharSequence, com.google.i18n.phonenumbers.i$b):com.google.i18n.phonenumbers.g$c
      com.google.i18n.phonenumbers.g.a(java.util.List<com.google.i18n.phonenumbers.i$a>, java.lang.String):com.google.i18n.phonenumbers.i$a
      com.google.i18n.phonenumbers.g.a(int, java.lang.String):com.google.i18n.phonenumbers.i$b
      com.google.i18n.phonenumbers.g.a(com.google.i18n.phonenumbers.i$b, com.google.i18n.phonenumbers.g$b):com.google.i18n.phonenumbers.i$d
      com.google.i18n.phonenumbers.g.a(com.google.i18n.phonenumbers.j$a, java.util.List<java.lang.String>):java.lang.String
      com.google.i18n.phonenumbers.g.a(java.lang.CharSequence, com.google.i18n.phonenumbers.j$a):void
      com.google.i18n.phonenumbers.g.a(java.lang.String, java.lang.StringBuilder):void
      com.google.i18n.phonenumbers.g.a(com.google.i18n.phonenumbers.j$a, java.lang.String):boolean
      com.google.i18n.phonenumbers.g.a(java.lang.String, com.google.i18n.phonenumbers.i$d):boolean
      com.google.i18n.phonenumbers.g.a(java.lang.CharSequence, java.lang.String):com.google.i18n.phonenumbers.j$a
      com.google.i18n.phonenumbers.g.a(com.google.i18n.phonenumbers.j$a, com.google.i18n.phonenumbers.g$a):java.lang.String
      com.google.i18n.phonenumbers.g.a(java.lang.CharSequence, boolean):java.lang.StringBuilder */
    private static String c(CharSequence charSequence) {
        return a(charSequence, false).toString();
    }

    private static StringBuilder a(CharSequence charSequence, boolean z2) {
        StringBuilder sb = new StringBuilder(charSequence.length());
        for (int i2 = 0; i2 < charSequence.length(); i2++) {
            int digit = Character.digit(charSequence.charAt(i2), 10);
            if (digit != -1) {
                sb.append(digit);
            }
        }
        return sb;
    }

    private static synchronized void a(g gVar) {
        synchronized (g.class) {
            A = gVar;
        }
    }

    private static boolean a(i.d dVar) {
        return (dVar.a() == 1 && dVar.f2874b.get(0).intValue() == -1) ? false : true;
    }

    public static synchronized g a() {
        g gVar;
        synchronized (g.class) {
            if (A == null) {
                b bVar = c.f2854a;
                if (bVar != null) {
                    a(new g(new f(bVar), a.a()));
                } else {
                    throw new IllegalArgumentException("metadataLoader could not be null.");
                }
            }
            gVar = A;
        }
        return gVar;
    }

    private boolean c(String str) {
        return str != null && this.f.contains(str);
    }

    private boolean a(int i2) {
        return this.C.containsKey(Integer.valueOf(i2));
    }

    private void a(j.a aVar, a aVar2, StringBuilder sb) {
        sb.setLength(0);
        int i2 = aVar.f2875a;
        String a2 = a(aVar);
        if (aVar2 == a.E164) {
            sb.append(a2);
            a(i2, a.E164, sb);
        } else if (!a(i2)) {
            sb.append(a2);
        } else {
            i.b a3 = a(i2, c(i2));
            sb.append(a(a2, a3, aVar2));
            a(aVar, a3, aVar2, sb);
            a(i2, aVar2, sb);
        }
    }

    private i.b a(int i2, String str) {
        if ("001".equals(str)) {
            return b(i2);
        }
        return d(str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Arrays.fill(char[], char):void}
     arg types: [char[], int]
     candidates:
      ClspMth{java.util.Arrays.fill(double[], double):void}
      ClspMth{java.util.Arrays.fill(byte[], byte):void}
      ClspMth{java.util.Arrays.fill(long[], long):void}
      ClspMth{java.util.Arrays.fill(boolean[], boolean):void}
      ClspMth{java.util.Arrays.fill(short[], short):void}
      ClspMth{java.util.Arrays.fill(java.lang.Object[], java.lang.Object):void}
      ClspMth{java.util.Arrays.fill(int[], int):void}
      ClspMth{java.util.Arrays.fill(float[], float):void}
      ClspMth{java.util.Arrays.fill(char[], char):void} */
    public static String a(j.a aVar) {
        StringBuilder sb = new StringBuilder();
        if (aVar.f && aVar.h > 0) {
            char[] cArr = new char[aVar.h];
            Arrays.fill(cArr, '0');
            sb.append(new String(cArr));
        }
        sb.append(aVar.f2876b);
        return sb.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuilder.insert(int, char):java.lang.StringBuilder}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.CharSequence):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.Object):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, float):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.String):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, long):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, char[]):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, int):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, boolean):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, double):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, char):java.lang.StringBuilder} */
    private static void a(int i2, a aVar, StringBuilder sb) {
        int i3 = h.f2867b[aVar.ordinal()];
        if (i3 == 1) {
            sb.insert(0, i2).insert(0, '+');
        } else if (i3 == 2) {
            sb.insert(0, " ").insert(0, i2).insert(0, '+');
        } else if (i3 == 3) {
            sb.insert(0, "-").insert(0, i2).insert(0, '+').insert(0, "tel:");
        }
    }

    private String a(String str, i.b bVar, a aVar) {
        return a(str, bVar, aVar, (CharSequence) null);
    }

    private i.a a(List<i.a> list, String str) {
        for (i.a next : list) {
            int a2 = next.a();
            if ((a2 == 0 || this.F.a(next.a(a2 - 1)).matcher(str).lookingAt()) && this.F.a(next.f2868a).matcher(str).matches()) {
                return next;
            }
        }
        return null;
    }

    private static i.d a(i.b bVar, b bVar2) {
        switch (h.c[bVar2.ordinal()]) {
            case 1:
                return bVar.e;
            case 2:
                return bVar.d;
            case 3:
                return bVar.c;
            case 4:
            case 5:
                return bVar.f2871b;
            case 6:
                return bVar.f;
            case 7:
                return bVar.h;
            case 8:
                return bVar.g;
            case 9:
                return bVar.i;
            case 10:
                return bVar.j;
            case 11:
                return bVar.k;
            default:
                return bVar.f2870a;
        }
    }

    private i.b d(String str) {
        if (!c(str)) {
            return null;
        }
        return this.B.a(str);
    }

    private i.b b(int i2) {
        if (!this.C.containsKey(Integer.valueOf(i2))) {
            return null;
        }
        return this.B.a(i2);
    }

    private boolean a(String str, i.d dVar) {
        int length = str.length();
        List<Integer> list = dVar.f2874b;
        if (list.size() <= 0 || list.contains(Integer.valueOf(length))) {
            return this.D.a(str, dVar, false);
        }
        return false;
    }

    public final boolean b(j.a aVar) {
        return a(aVar, c(aVar));
    }

    private String a(j.a aVar, List<String> list) {
        String a2 = a(aVar);
        for (String next : list) {
            i.b d2 = d(next);
            if (d2.u) {
                if (this.F.a(d2.v).matcher(a2).lookingAt()) {
                    return next;
                }
            } else if (a(a2, d2) != b.UNKNOWN) {
                return next;
            }
        }
        return null;
    }

    private String c(int i2) {
        List list = this.C.get(Integer.valueOf(i2));
        if (list == null) {
            return "ZZ";
        }
        return (String) list.get(0);
    }

    public final int a(String str) {
        if (c(str)) {
            return e(str);
        }
        Logger logger = g;
        Level level = Level.WARNING;
        StringBuilder sb = new StringBuilder();
        sb.append("Invalid or missing region code (");
        if (str == null) {
            str = "null";
        }
        sb.append(str);
        sb.append(") provided.");
        logger.log(level, sb.toString());
        return 0;
    }

    private int e(String str) {
        i.b d2 = d(str);
        if (d2 != null) {
            return d2.l;
        }
        throw new IllegalArgumentException("Invalid region code: " + str);
    }

    private c a(CharSequence charSequence, i.b bVar) {
        return a(charSequence, bVar, b.UNKNOWN);
    }

    private c a(CharSequence charSequence, i.b bVar, b bVar2) {
        List<Integer> list;
        List<Integer> list2;
        List<Integer> list3;
        List<Integer> list4;
        while (true) {
            i.d a2 = a(bVar, bVar2);
            if (a2.f2874b.isEmpty()) {
                list = bVar.f2870a.f2874b;
            } else {
                list = a2.f2874b;
            }
            list2 = a2.c;
            if (bVar2 != b.FIXED_LINE_OR_MOBILE) {
                break;
            } else if (!a(a(bVar, b.FIXED_LINE))) {
                bVar2 = b.MOBILE;
            } else {
                i.d a3 = a(bVar, b.MOBILE);
                if (a(a3)) {
                    list3 = new ArrayList<>(list);
                    if (a3.f2874b.size() == 0) {
                        list4 = bVar.f2870a.f2874b;
                    } else {
                        list4 = a3.f2874b;
                    }
                    list3.addAll(list4);
                    Collections.sort(list3);
                    if (list2.isEmpty()) {
                        list2 = a3.c;
                    } else {
                        ArrayList arrayList = new ArrayList(list2);
                        arrayList.addAll(a3.c);
                        Collections.sort(arrayList);
                        list2 = arrayList;
                    }
                }
            }
        }
        list3 = list;
        if (((Integer) list3.get(0)).intValue() == -1) {
            return c.INVALID_LENGTH;
        }
        int length = charSequence.length();
        if (list2.contains(Integer.valueOf(length))) {
            return c.IS_POSSIBLE_LOCAL_ONLY;
        }
        int intValue = ((Integer) list3.get(0)).intValue();
        if (intValue == length) {
            return c.IS_POSSIBLE;
        }
        if (intValue > length) {
            return c.TOO_SHORT;
        }
        if (((Integer) list3.get(list3.size() - 1)).intValue() < length) {
            return c.TOO_LONG;
        }
        return list3.subList(1, list3.size()).contains(Integer.valueOf(length)) ? c.IS_POSSIBLE : c.INVALID_LENGTH;
    }

    private int a(StringBuilder sb, StringBuilder sb2) {
        if (!(sb.length() == 0 || sb.charAt(0) == '0')) {
            int length = sb.length();
            int i2 = 1;
            while (i2 <= 3 && i2 <= length) {
                int parseInt = Integer.parseInt(sb.substring(0, i2));
                if (this.C.containsKey(Integer.valueOf(parseInt))) {
                    sb2.append(sb.substring(i2));
                    return parseInt;
                }
                i2++;
            }
        }
        return 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0079  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int a(java.lang.CharSequence r6, com.google.i18n.phonenumbers.i.b r7, java.lang.StringBuilder r8, boolean r9, com.google.i18n.phonenumbers.j.a r10) throws com.google.i18n.phonenumbers.NumberParseException {
        /*
            r5 = this;
            int r0 = r6.length()
            r1 = 0
            if (r0 != 0) goto L_0x0008
            return r1
        L_0x0008:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r6)
            if (r7 == 0) goto L_0x0012
            java.lang.String r6 = r7.m
            goto L_0x0014
        L_0x0012:
            java.lang.String r6 = "NonMatch"
        L_0x0014:
            int r2 = r0.length()
            if (r2 != 0) goto L_0x001d
            com.google.i18n.phonenumbers.j$a$a r6 = com.google.i18n.phonenumbers.j.a.C0127a.FROM_DEFAULT_COUNTRY
            goto L_0x007b
        L_0x001d:
            java.util.regex.Pattern r2 = com.google.i18n.phonenumbers.g.f2858a
            java.util.regex.Matcher r2 = r2.matcher(r0)
            boolean r3 = r2.lookingAt()
            if (r3 == 0) goto L_0x0036
            int r6 = r2.end()
            r0.delete(r1, r6)
            a(r0)
            com.google.i18n.phonenumbers.j$a$a r6 = com.google.i18n.phonenumbers.j.a.C0127a.FROM_NUMBER_WITH_PLUS_SIGN
            goto L_0x007b
        L_0x0036:
            com.google.i18n.phonenumbers.a.c r2 = r5.F
            java.util.regex.Pattern r6 = r2.a(r6)
            a(r0)
            java.util.regex.Matcher r6 = r6.matcher(r0)
            boolean r2 = r6.lookingAt()
            r3 = 1
            if (r2 == 0) goto L_0x0073
            int r6 = r6.end()
            java.util.regex.Pattern r2 = com.google.i18n.phonenumbers.g.r
            java.lang.String r4 = r0.substring(r6)
            java.util.regex.Matcher r2 = r2.matcher(r4)
            boolean r4 = r2.find()
            if (r4 == 0) goto L_0x006f
            java.lang.String r2 = r2.group(r3)
            java.lang.String r2 = c(r2)
            java.lang.String r4 = "0"
            boolean r2 = r2.equals(r4)
            if (r2 == 0) goto L_0x006f
            goto L_0x0073
        L_0x006f:
            r0.delete(r1, r6)
            goto L_0x0074
        L_0x0073:
            r3 = 0
        L_0x0074:
            if (r3 == 0) goto L_0x0079
            com.google.i18n.phonenumbers.j$a$a r6 = com.google.i18n.phonenumbers.j.a.C0127a.FROM_NUMBER_WITH_IDD
            goto L_0x007b
        L_0x0079:
            com.google.i18n.phonenumbers.j$a$a r6 = com.google.i18n.phonenumbers.j.a.C0127a.FROM_DEFAULT_COUNTRY
        L_0x007b:
            if (r9 == 0) goto L_0x0080
            r10.a(r6)
        L_0x0080:
            com.google.i18n.phonenumbers.j$a$a r2 = com.google.i18n.phonenumbers.j.a.C0127a.FROM_DEFAULT_COUNTRY
            if (r6 == r2) goto L_0x00a9
            int r6 = r0.length()
            r7 = 2
            if (r6 <= r7) goto L_0x009f
            int r6 = r5.a(r0, r8)
            if (r6 == 0) goto L_0x0095
            r10.a(r6)
            return r6
        L_0x0095:
            com.google.i18n.phonenumbers.NumberParseException r6 = new com.google.i18n.phonenumbers.NumberParseException
            com.google.i18n.phonenumbers.NumberParseException$a r7 = com.google.i18n.phonenumbers.NumberParseException.a.INVALID_COUNTRY_CODE
            java.lang.String r8 = "Country calling code supplied was not recognised."
            r6.<init>(r7, r8)
            throw r6
        L_0x009f:
            com.google.i18n.phonenumbers.NumberParseException r6 = new com.google.i18n.phonenumbers.NumberParseException
            com.google.i18n.phonenumbers.NumberParseException$a r7 = com.google.i18n.phonenumbers.NumberParseException.a.TOO_SHORT_AFTER_IDD
            java.lang.String r8 = "Phone number had an IDD, but after this was not long enough to be a viable phone number."
            r6.<init>(r7, r8)
            throw r6
        L_0x00a9:
            if (r7 == 0) goto L_0x00f4
            int r6 = r7.l
            java.lang.String r2 = java.lang.String.valueOf(r6)
            java.lang.String r3 = r0.toString()
            boolean r4 = r3.startsWith(r2)
            if (r4 == 0) goto L_0x00f4
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            int r2 = r2.length()
            java.lang.String r2 = r3.substring(r2)
            r4.<init>(r2)
            com.google.i18n.phonenumbers.i$d r2 = r7.f2870a
            r3 = 0
            r5.a(r4, r7, r3)
            com.google.i18n.phonenumbers.a.a r3 = r5.D
            boolean r3 = r3.a(r0, r2, r1)
            if (r3 != 0) goto L_0x00de
            com.google.i18n.phonenumbers.a.a r3 = r5.D
            boolean r2 = r3.a(r4, r2, r1)
            if (r2 != 0) goto L_0x00e6
        L_0x00de:
            com.google.i18n.phonenumbers.g$c r7 = r5.a(r0, r7)
            com.google.i18n.phonenumbers.g$c r0 = com.google.i18n.phonenumbers.g.c.TOO_LONG
            if (r7 != r0) goto L_0x00f4
        L_0x00e6:
            r8.append(r4)
            if (r9 == 0) goto L_0x00f0
            com.google.i18n.phonenumbers.j$a$a r7 = com.google.i18n.phonenumbers.j.a.C0127a.FROM_NUMBER_WITHOUT_PLUS_SIGN
            r10.a(r7)
        L_0x00f0:
            r10.a(r6)
            return r6
        L_0x00f4:
            r10.a(r1)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.i18n.phonenumbers.g.a(java.lang.CharSequence, com.google.i18n.phonenumbers.i$b, java.lang.StringBuilder, boolean, com.google.i18n.phonenumbers.j$a):int");
    }

    private boolean a(StringBuilder sb, i.b bVar, StringBuilder sb2) {
        int length = sb.length();
        String str = bVar.p;
        if (!(length == 0 || str.length() == 0)) {
            Matcher matcher = this.F.a(str).matcher(sb);
            if (matcher.lookingAt()) {
                i.d dVar = bVar.f2870a;
                boolean a2 = this.D.a(sb, dVar, false);
                int groupCount = matcher.groupCount();
                String str2 = bVar.q;
                if (str2 != null && str2.length() != 0 && matcher.group(groupCount) != null) {
                    StringBuilder sb3 = new StringBuilder(sb);
                    sb3.replace(0, length, matcher.replaceFirst(str2));
                    if (a2 && !this.D.a(sb3.toString(), dVar, false)) {
                        return false;
                    }
                    if (sb2 != null && groupCount > 1) {
                        sb2.append(matcher.group(1));
                    }
                    sb.replace(0, sb.length(), sb3.toString());
                    return true;
                } else if (a2 && !this.D.a(sb.substring(matcher.end()), dVar, false)) {
                    return false;
                } else {
                    if (!(sb2 == null || groupCount <= 0 || matcher.group(groupCount) == null)) {
                        sb2.append(matcher.group(1));
                    }
                    sb.delete(0, matcher.end());
                    return true;
                }
            }
        }
        return false;
    }

    private static String b(StringBuilder sb) {
        Matcher matcher = w.matcher(sb);
        if (!matcher.find() || !b((CharSequence) sb.substring(0, matcher.start()))) {
            return "";
        }
        int groupCount = matcher.groupCount();
        for (int i2 = 1; i2 <= groupCount; i2++) {
            if (matcher.group(i2) != null) {
                String group = matcher.group(i2);
                sb.delete(matcher.start(), sb.length());
                return group;
            }
        }
        return "";
    }

    private boolean c(CharSequence charSequence, String str) {
        if (!c(str)) {
            return charSequence.length() != 0 && f2858a.matcher(charSequence).lookingAt();
        }
        return true;
    }

    public final j.a a(CharSequence charSequence, String str) throws NumberParseException {
        j.a aVar = new j.a();
        a(charSequence, str, aVar);
        return aVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.i18n.phonenumbers.g.a(java.lang.CharSequence, java.lang.String, boolean, boolean, com.google.i18n.phonenumbers.j$a):void
     arg types: [java.lang.CharSequence, java.lang.String, int, int, com.google.i18n.phonenumbers.j$a]
     candidates:
      com.google.i18n.phonenumbers.g.a(java.lang.CharSequence, com.google.i18n.phonenumbers.i$b, java.lang.StringBuilder, boolean, com.google.i18n.phonenumbers.j$a):int
      com.google.i18n.phonenumbers.g.a(java.lang.CharSequence, java.lang.String, boolean, boolean, com.google.i18n.phonenumbers.j$a):void */
    private void a(CharSequence charSequence, String str, j.a aVar) throws NumberParseException {
        a(charSequence, str, false, true, aVar);
    }

    public final j.a b(CharSequence charSequence, String str) throws NumberParseException {
        j.a aVar = new j.a();
        b(charSequence, str, aVar);
        return aVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.i18n.phonenumbers.g.a(java.lang.CharSequence, java.lang.String, boolean, boolean, com.google.i18n.phonenumbers.j$a):void
     arg types: [java.lang.CharSequence, java.lang.String, int, int, com.google.i18n.phonenumbers.j$a]
     candidates:
      com.google.i18n.phonenumbers.g.a(java.lang.CharSequence, com.google.i18n.phonenumbers.i$b, java.lang.StringBuilder, boolean, com.google.i18n.phonenumbers.j$a):int
      com.google.i18n.phonenumbers.g.a(java.lang.CharSequence, java.lang.String, boolean, boolean, com.google.i18n.phonenumbers.j$a):void */
    private void b(CharSequence charSequence, String str, j.a aVar) throws NumberParseException {
        a(charSequence, str, true, true, aVar);
    }

    private static void a(CharSequence charSequence, j.a aVar) {
        if (charSequence.length() > 1 && charSequence.charAt(0) == '0') {
            aVar.e = true;
            aVar.f = true;
            int i2 = 1;
            while (i2 < charSequence.length() - 1 && charSequence.charAt(i2) == '0') {
                i2++;
            }
            if (i2 != 1) {
                aVar.g = true;
                aVar.h = i2;
            }
        }
    }

    private void a(CharSequence charSequence, String str, boolean z2, boolean z3, j.a aVar) throws NumberParseException {
        int i2;
        if (charSequence == null) {
            throw new NumberParseException(NumberParseException.a.NOT_A_NUMBER, "The phone number supplied was null.");
        } else if (charSequence.length() <= 250) {
            StringBuilder sb = new StringBuilder();
            String charSequence2 = charSequence.toString();
            a(charSequence2, sb);
            if (!b((CharSequence) sb)) {
                throw new NumberParseException(NumberParseException.a.NOT_A_NUMBER, "The string supplied did not seem to be a phone number.");
            } else if (c(sb, str)) {
                if (z2) {
                    aVar.b(charSequence2);
                }
                String b2 = b(sb);
                if (b2.length() > 0) {
                    aVar.a(b2);
                }
                i.b d2 = d(str);
                StringBuilder sb2 = new StringBuilder();
                try {
                    i2 = a(sb, d2, sb2, z2, aVar);
                } catch (NumberParseException e2) {
                    Matcher matcher = f2858a.matcher(sb);
                    if (e2.f2845a != NumberParseException.a.INVALID_COUNTRY_CODE || !matcher.lookingAt()) {
                        throw new NumberParseException(e2.f2845a, e2.getMessage());
                    }
                    i2 = a(sb.substring(matcher.end()), d2, sb2, z2, aVar);
                    if (i2 == 0) {
                        throw new NumberParseException(NumberParseException.a.INVALID_COUNTRY_CODE, "Could not interpret numbers after plus-sign.");
                    }
                }
                if (i2 != 0) {
                    String c2 = c(i2);
                    if (!c2.equals(str)) {
                        d2 = a(i2, c2);
                    }
                } else {
                    sb2.append((CharSequence) a(sb));
                    if (str != null) {
                        aVar.a(d2.l);
                    } else if (z2) {
                        aVar.a();
                    }
                }
                if (sb2.length() >= 2) {
                    if (d2 != null) {
                        StringBuilder sb3 = new StringBuilder();
                        StringBuilder sb4 = new StringBuilder(sb2);
                        a(sb4, d2, sb3);
                        c a2 = a(sb4, d2);
                        if (!(a2 == c.TOO_SHORT || a2 == c.IS_POSSIBLE_LOCAL_ONLY || a2 == c.INVALID_LENGTH)) {
                            if (z2 && sb3.length() > 0) {
                                aVar.c(sb3.toString());
                            }
                            sb2 = sb4;
                        }
                    }
                    int length = sb2.length();
                    if (length < 2) {
                        throw new NumberParseException(NumberParseException.a.TOO_SHORT_NSN, "The string supplied is too short to be a phone number.");
                    } else if (length <= 17) {
                        a(sb2, aVar);
                        aVar.a(Long.parseLong(sb2.toString()));
                    } else {
                        throw new NumberParseException(NumberParseException.a.TOO_LONG, "The string supplied is too long to be a phone number.");
                    }
                } else {
                    throw new NumberParseException(NumberParseException.a.TOO_SHORT_NSN, "The string supplied is too short to be a phone number.");
                }
            } else {
                throw new NumberParseException(NumberParseException.a.INVALID_COUNTRY_CODE, "Missing or invalid default region.");
            }
        } else {
            throw new NumberParseException(NumberParseException.a.TOO_LONG, "The string supplied was too long to parse.");
        }
    }

    private static void a(String str, StringBuilder sb) {
        int indexOf = str.indexOf(";phone-context=");
        if (indexOf >= 0) {
            int i2 = indexOf + 15;
            if (i2 < str.length() - 1 && str.charAt(i2) == '+') {
                int indexOf2 = str.indexOf(59, i2);
                if (indexOf2 > 0) {
                    sb.append(str.substring(i2, indexOf2));
                } else {
                    sb.append(str.substring(i2));
                }
            }
            int indexOf3 = str.indexOf("tel:");
            sb.append(str.substring(indexOf3 >= 0 ? indexOf3 + 4 : 0, indexOf));
        } else {
            sb.append(a((CharSequence) str));
        }
        int indexOf4 = sb.indexOf(";isub=");
        if (indexOf4 > 0) {
            sb.delete(indexOf4, sb.length());
        }
    }

    public final String a(j.a aVar, a aVar2) {
        if (aVar.f2876b == 0 && aVar.i) {
            String str = aVar.j;
            if (str.length() > 0) {
                return str;
            }
        }
        StringBuilder sb = new StringBuilder(20);
        a(aVar, aVar2, sb);
        return sb.toString();
    }

    private String a(String str, i.b bVar, a aVar, CharSequence charSequence) {
        List<i.a> list;
        if (bVar.t.size() == 0 || aVar == a.NATIONAL) {
            list = bVar.s;
        } else {
            list = bVar.t;
        }
        i.a a2 = a(list, str);
        return a2 == null ? str : a(str, a2, aVar, (CharSequence) null);
    }

    private String a(String str, i.a aVar, a aVar2, CharSequence charSequence) {
        String str2;
        String str3 = aVar.f2869b;
        Matcher matcher = this.F.a(aVar.f2868a).matcher(str);
        if (aVar2 != a.NATIONAL || charSequence == null || charSequence.length() <= 0 || aVar.d.length() <= 0) {
            String str4 = aVar.c;
            if (aVar2 != a.NATIONAL || str4 == null || str4.length() <= 0) {
                str2 = matcher.replaceAll(str3);
            } else {
                str2 = matcher.replaceAll(y.matcher(str3).replaceFirst(str4));
            }
        } else {
            str2 = matcher.replaceAll(y.matcher(str3).replaceFirst(aVar.d.replace("$CC", charSequence)));
        }
        if (aVar2 != a.RFC3966) {
            return str2;
        }
        Matcher matcher2 = q.matcher(str2);
        if (matcher2.lookingAt()) {
            str2 = matcher2.replaceFirst("");
        }
        return matcher2.reset(str2).replaceAll("-");
    }

    private static void a(j.a aVar, i.b bVar, a aVar2, StringBuilder sb) {
        if (aVar.c && aVar.d.length() > 0) {
            if (aVar2 == a.RFC3966) {
                sb.append(";ext=");
                sb.append(aVar.d);
            } else if (bVar.n) {
                sb.append(bVar.o);
                sb.append(aVar.d);
            } else {
                sb.append(" ext. ");
                sb.append(aVar.d);
            }
        }
    }

    private b a(String str, i.b bVar) {
        if (!a(str, bVar.f2870a)) {
            return b.UNKNOWN;
        }
        if (a(str, bVar.e)) {
            return b.PREMIUM_RATE;
        }
        if (a(str, bVar.d)) {
            return b.TOLL_FREE;
        }
        if (a(str, bVar.f)) {
            return b.SHARED_COST;
        }
        if (a(str, bVar.h)) {
            return b.VOIP;
        }
        if (a(str, bVar.g)) {
            return b.PERSONAL_NUMBER;
        }
        if (a(str, bVar.i)) {
            return b.PAGER;
        }
        if (a(str, bVar.j)) {
            return b.UAN;
        }
        if (a(str, bVar.k)) {
            return b.VOICEMAIL;
        }
        if (a(str, bVar.f2871b)) {
            if (bVar.r) {
                return b.FIXED_LINE_OR_MOBILE;
            }
            if (a(str, bVar.c)) {
                return b.FIXED_LINE_OR_MOBILE;
            }
            return b.FIXED_LINE;
        } else if (bVar.r || !a(str, bVar.c)) {
            return b.UNKNOWN;
        } else {
            return b.MOBILE;
        }
    }

    private boolean a(j.a aVar, String str) {
        int i2 = aVar.f2875a;
        i.b a2 = a(i2, str);
        if (a2 == null) {
            return false;
        }
        if (("001".equals(str) || i2 == e(str)) && a(a(aVar), a2) != b.UNKNOWN) {
            return true;
        }
        return false;
    }

    public final String c(j.a aVar) {
        int i2 = aVar.f2875a;
        List list = this.C.get(Integer.valueOf(i2));
        if (list == null) {
            Logger logger = g;
            Level level = Level.INFO;
            logger.log(level, "Missing/invalid country_code (" + i2 + ")");
            return null;
        } else if (list.size() == 1) {
            return (String) list.get(0);
        } else {
            return a(aVar, list);
        }
    }
}
