package com.google.i18n.phonenumbers;

public class NumberParseException extends Exception {

    /* renamed from: a  reason: collision with root package name */
    a f2845a;

    /* renamed from: b  reason: collision with root package name */
    private String f2846b;

    public enum a {
        INVALID_COUNTRY_CODE,
        NOT_A_NUMBER,
        TOO_SHORT_AFTER_IDD,
        TOO_SHORT_NSN,
        TOO_LONG
    }

    public NumberParseException(a aVar, String str) {
        super(str);
        this.f2846b = str;
        this.f2845a = aVar;
    }

    public String toString() {
        return "Error type: " + this.f2845a + ". " + this.f2846b;
    }
}
