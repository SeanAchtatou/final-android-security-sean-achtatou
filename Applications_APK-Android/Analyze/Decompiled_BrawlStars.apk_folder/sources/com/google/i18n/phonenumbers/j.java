package com.google.i18n.phonenumbers;

import java.io.Serializable;

/* compiled from: Phonenumber */
public final class j {

    /* compiled from: Phonenumber */
    public static class a implements Serializable {
        private static final long serialVersionUID = 1;

        /* renamed from: a  reason: collision with root package name */
        public int f2875a = 0;

        /* renamed from: b  reason: collision with root package name */
        long f2876b = 0;
        boolean c;
        String d = "";
        boolean e;
        boolean f = false;
        boolean g;
        int h = 1;
        boolean i;
        public String j = "";
        private boolean k;
        private boolean l;
        private boolean m;
        private C0127a n = C0127a.UNSPECIFIED;
        private boolean o;
        private String p = "";

        /* renamed from: com.google.i18n.phonenumbers.j$a$a  reason: collision with other inner class name */
        /* compiled from: Phonenumber */
        public enum C0127a {
            FROM_NUMBER_WITH_PLUS_SIGN,
            FROM_NUMBER_WITH_IDD,
            FROM_NUMBER_WITHOUT_PLUS_SIGN,
            FROM_DEFAULT_COUNTRY,
            UNSPECIFIED
        }

        public final a a(int i2) {
            this.k = true;
            this.f2875a = i2;
            return this;
        }

        public final a a(long j2) {
            this.l = true;
            this.f2876b = j2;
            return this;
        }

        public final a a(String str) {
            if (str != null) {
                this.c = true;
                this.d = str;
                return this;
            }
            throw new NullPointerException();
        }

        public final a b(String str) {
            if (str != null) {
                this.i = true;
                this.j = str;
                return this;
            }
            throw new NullPointerException();
        }

        public final a a(C0127a aVar) {
            if (aVar != null) {
                this.m = true;
                this.n = aVar;
                return this;
            }
            throw new NullPointerException();
        }

        public final a a() {
            this.m = false;
            this.n = C0127a.UNSPECIFIED;
            return this;
        }

        public final a c(String str) {
            if (str != null) {
                this.o = true;
                this.p = str;
                return this;
            }
            throw new NullPointerException();
        }

        public boolean equals(Object obj) {
            if (obj instanceof a) {
                a aVar = (a) obj;
                if (aVar != null && (this == aVar || (this.f2875a == aVar.f2875a && this.f2876b == aVar.f2876b && this.d.equals(aVar.d) && this.f == aVar.f && this.h == aVar.h && this.j.equals(aVar.j) && this.n == aVar.n && this.p.equals(aVar.p) && this.o == aVar.o))) {
                    return true;
                }
            }
            return false;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("Country Code: ");
            sb.append(this.f2875a);
            sb.append(" National Number: ");
            sb.append(this.f2876b);
            if (this.e && this.f) {
                sb.append(" Leading Zero(s): true");
            }
            if (this.g) {
                sb.append(" Number of leading zeros: ");
                sb.append(this.h);
            }
            if (this.c) {
                sb.append(" Extension: ");
                sb.append(this.d);
            }
            if (this.m) {
                sb.append(" Country Code Source: ");
                sb.append(this.n);
            }
            if (this.o) {
                sb.append(" Preferred Domestic Carrier Code: ");
                sb.append(this.p);
            }
            return sb.toString();
        }

        public int hashCode() {
            int i2 = 1231;
            int hashCode = (((((((((((((((this.f2875a + 2173) * 53) + Long.valueOf(this.f2876b).hashCode()) * 53) + this.d.hashCode()) * 53) + (this.f ? 1231 : 1237)) * 53) + this.h) * 53) + this.j.hashCode()) * 53) + this.n.hashCode()) * 53) + this.p.hashCode()) * 53;
            if (!this.o) {
                i2 = 1237;
            }
            return hashCode + i2;
        }
    }
}
