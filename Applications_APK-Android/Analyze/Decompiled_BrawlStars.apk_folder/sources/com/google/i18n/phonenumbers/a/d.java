package com.google.i18n.phonenumbers.a;

import com.google.i18n.phonenumbers.a.c;
import java.util.LinkedHashMap;
import java.util.Map;

/* compiled from: RegexCache */
class d extends LinkedHashMap<K, V> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c.a f2853a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    d(c.a aVar, int i, float f, boolean z) {
        super(i, 0.75f, true);
        this.f2853a = aVar;
    }

    /* access modifiers changed from: protected */
    public boolean removeEldestEntry(Map.Entry<K, V> entry) {
        return size() > this.f2853a.f2851a;
    }
}
