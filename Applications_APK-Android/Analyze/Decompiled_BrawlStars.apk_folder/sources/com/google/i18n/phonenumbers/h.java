package com.google.i18n.phonenumbers;

import com.google.i18n.phonenumbers.g;
import com.google.i18n.phonenumbers.j;

/* compiled from: PhoneNumberUtil */
/* synthetic */ class h {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f2866a = new int[j.a.C0127a.values().length];

    /* renamed from: b  reason: collision with root package name */
    static final /* synthetic */ int[] f2867b = new int[g.a.values().length];
    static final /* synthetic */ int[] c = new int[g.b.values().length];

    /* JADX WARNING: Can't wrap try/catch for region: R(41:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|31|32|33|34|35|36|(2:37|38)|39|41|42|43|44|45|46|47|48|50) */
    /* JADX WARNING: Can't wrap try/catch for region: R(42:0|(2:1|2)|3|5|6|7|(2:9|10)|11|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|31|32|33|34|35|36|(2:37|38)|39|41|42|43|44|45|46|47|48|50) */
    /* JADX WARNING: Can't wrap try/catch for region: R(43:0|(2:1|2)|3|5|6|7|(2:9|10)|11|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|31|32|33|34|35|36|37|38|39|41|42|43|44|45|46|47|48|50) */
    /* JADX WARNING: Can't wrap try/catch for region: R(44:0|1|2|3|5|6|7|(2:9|10)|11|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|31|32|33|34|35|36|37|38|39|41|42|43|44|45|46|47|48|50) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0035 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0040 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x004b */
    /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0056 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0062 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x006e */
    /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x007a */
    /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x0099 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:35:0x00a3 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:37:0x00ad */
    /* JADX WARNING: Missing exception handler attribute for start block: B:43:0x00ca */
    /* JADX WARNING: Missing exception handler attribute for start block: B:45:0x00d4 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:47:0x00de */
    static {
        /*
            com.google.i18n.phonenumbers.g$b[] r0 = com.google.i18n.phonenumbers.g.b.values()
            int r0 = r0.length
            int[] r0 = new int[r0]
            com.google.i18n.phonenumbers.h.c = r0
            r0 = 1
            int[] r1 = com.google.i18n.phonenumbers.h.c     // Catch:{ NoSuchFieldError -> 0x0014 }
            com.google.i18n.phonenumbers.g$b r2 = com.google.i18n.phonenumbers.g.b.PREMIUM_RATE     // Catch:{ NoSuchFieldError -> 0x0014 }
            int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
            r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
        L_0x0014:
            r1 = 2
            int[] r2 = com.google.i18n.phonenumbers.h.c     // Catch:{ NoSuchFieldError -> 0x001f }
            com.google.i18n.phonenumbers.g$b r3 = com.google.i18n.phonenumbers.g.b.TOLL_FREE     // Catch:{ NoSuchFieldError -> 0x001f }
            int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
            r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
        L_0x001f:
            r2 = 3
            int[] r3 = com.google.i18n.phonenumbers.h.c     // Catch:{ NoSuchFieldError -> 0x002a }
            com.google.i18n.phonenumbers.g$b r4 = com.google.i18n.phonenumbers.g.b.MOBILE     // Catch:{ NoSuchFieldError -> 0x002a }
            int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
            r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
        L_0x002a:
            r3 = 4
            int[] r4 = com.google.i18n.phonenumbers.h.c     // Catch:{ NoSuchFieldError -> 0x0035 }
            com.google.i18n.phonenumbers.g$b r5 = com.google.i18n.phonenumbers.g.b.FIXED_LINE     // Catch:{ NoSuchFieldError -> 0x0035 }
            int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
            r4[r5] = r3     // Catch:{ NoSuchFieldError -> 0x0035 }
        L_0x0035:
            int[] r4 = com.google.i18n.phonenumbers.h.c     // Catch:{ NoSuchFieldError -> 0x0040 }
            com.google.i18n.phonenumbers.g$b r5 = com.google.i18n.phonenumbers.g.b.FIXED_LINE_OR_MOBILE     // Catch:{ NoSuchFieldError -> 0x0040 }
            int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
            r6 = 5
            r4[r5] = r6     // Catch:{ NoSuchFieldError -> 0x0040 }
        L_0x0040:
            int[] r4 = com.google.i18n.phonenumbers.h.c     // Catch:{ NoSuchFieldError -> 0x004b }
            com.google.i18n.phonenumbers.g$b r5 = com.google.i18n.phonenumbers.g.b.SHARED_COST     // Catch:{ NoSuchFieldError -> 0x004b }
            int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
            r6 = 6
            r4[r5] = r6     // Catch:{ NoSuchFieldError -> 0x004b }
        L_0x004b:
            int[] r4 = com.google.i18n.phonenumbers.h.c     // Catch:{ NoSuchFieldError -> 0x0056 }
            com.google.i18n.phonenumbers.g$b r5 = com.google.i18n.phonenumbers.g.b.VOIP     // Catch:{ NoSuchFieldError -> 0x0056 }
            int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0056 }
            r6 = 7
            r4[r5] = r6     // Catch:{ NoSuchFieldError -> 0x0056 }
        L_0x0056:
            int[] r4 = com.google.i18n.phonenumbers.h.c     // Catch:{ NoSuchFieldError -> 0x0062 }
            com.google.i18n.phonenumbers.g$b r5 = com.google.i18n.phonenumbers.g.b.PERSONAL_NUMBER     // Catch:{ NoSuchFieldError -> 0x0062 }
            int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0062 }
            r6 = 8
            r4[r5] = r6     // Catch:{ NoSuchFieldError -> 0x0062 }
        L_0x0062:
            int[] r4 = com.google.i18n.phonenumbers.h.c     // Catch:{ NoSuchFieldError -> 0x006e }
            com.google.i18n.phonenumbers.g$b r5 = com.google.i18n.phonenumbers.g.b.PAGER     // Catch:{ NoSuchFieldError -> 0x006e }
            int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x006e }
            r6 = 9
            r4[r5] = r6     // Catch:{ NoSuchFieldError -> 0x006e }
        L_0x006e:
            int[] r4 = com.google.i18n.phonenumbers.h.c     // Catch:{ NoSuchFieldError -> 0x007a }
            com.google.i18n.phonenumbers.g$b r5 = com.google.i18n.phonenumbers.g.b.UAN     // Catch:{ NoSuchFieldError -> 0x007a }
            int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x007a }
            r6 = 10
            r4[r5] = r6     // Catch:{ NoSuchFieldError -> 0x007a }
        L_0x007a:
            int[] r4 = com.google.i18n.phonenumbers.h.c     // Catch:{ NoSuchFieldError -> 0x0086 }
            com.google.i18n.phonenumbers.g$b r5 = com.google.i18n.phonenumbers.g.b.VOICEMAIL     // Catch:{ NoSuchFieldError -> 0x0086 }
            int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0086 }
            r6 = 11
            r4[r5] = r6     // Catch:{ NoSuchFieldError -> 0x0086 }
        L_0x0086:
            com.google.i18n.phonenumbers.g$a[] r4 = com.google.i18n.phonenumbers.g.a.values()
            int r4 = r4.length
            int[] r4 = new int[r4]
            com.google.i18n.phonenumbers.h.f2867b = r4
            int[] r4 = com.google.i18n.phonenumbers.h.f2867b     // Catch:{ NoSuchFieldError -> 0x0099 }
            com.google.i18n.phonenumbers.g$a r5 = com.google.i18n.phonenumbers.g.a.E164     // Catch:{ NoSuchFieldError -> 0x0099 }
            int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0099 }
            r4[r5] = r0     // Catch:{ NoSuchFieldError -> 0x0099 }
        L_0x0099:
            int[] r4 = com.google.i18n.phonenumbers.h.f2867b     // Catch:{ NoSuchFieldError -> 0x00a3 }
            com.google.i18n.phonenumbers.g$a r5 = com.google.i18n.phonenumbers.g.a.INTERNATIONAL     // Catch:{ NoSuchFieldError -> 0x00a3 }
            int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x00a3 }
            r4[r5] = r1     // Catch:{ NoSuchFieldError -> 0x00a3 }
        L_0x00a3:
            int[] r4 = com.google.i18n.phonenumbers.h.f2867b     // Catch:{ NoSuchFieldError -> 0x00ad }
            com.google.i18n.phonenumbers.g$a r5 = com.google.i18n.phonenumbers.g.a.RFC3966     // Catch:{ NoSuchFieldError -> 0x00ad }
            int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x00ad }
            r4[r5] = r2     // Catch:{ NoSuchFieldError -> 0x00ad }
        L_0x00ad:
            int[] r4 = com.google.i18n.phonenumbers.h.f2867b     // Catch:{ NoSuchFieldError -> 0x00b7 }
            com.google.i18n.phonenumbers.g$a r5 = com.google.i18n.phonenumbers.g.a.NATIONAL     // Catch:{ NoSuchFieldError -> 0x00b7 }
            int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x00b7 }
            r4[r5] = r3     // Catch:{ NoSuchFieldError -> 0x00b7 }
        L_0x00b7:
            com.google.i18n.phonenumbers.j$a$a[] r4 = com.google.i18n.phonenumbers.j.a.C0127a.values()
            int r4 = r4.length
            int[] r4 = new int[r4]
            com.google.i18n.phonenumbers.h.f2866a = r4
            int[] r4 = com.google.i18n.phonenumbers.h.f2866a     // Catch:{ NoSuchFieldError -> 0x00ca }
            com.google.i18n.phonenumbers.j$a$a r5 = com.google.i18n.phonenumbers.j.a.C0127a.FROM_NUMBER_WITH_PLUS_SIGN     // Catch:{ NoSuchFieldError -> 0x00ca }
            int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x00ca }
            r4[r5] = r0     // Catch:{ NoSuchFieldError -> 0x00ca }
        L_0x00ca:
            int[] r0 = com.google.i18n.phonenumbers.h.f2866a     // Catch:{ NoSuchFieldError -> 0x00d4 }
            com.google.i18n.phonenumbers.j$a$a r4 = com.google.i18n.phonenumbers.j.a.C0127a.FROM_NUMBER_WITH_IDD     // Catch:{ NoSuchFieldError -> 0x00d4 }
            int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x00d4 }
            r0[r4] = r1     // Catch:{ NoSuchFieldError -> 0x00d4 }
        L_0x00d4:
            int[] r0 = com.google.i18n.phonenumbers.h.f2866a     // Catch:{ NoSuchFieldError -> 0x00de }
            com.google.i18n.phonenumbers.j$a$a r1 = com.google.i18n.phonenumbers.j.a.C0127a.FROM_NUMBER_WITHOUT_PLUS_SIGN     // Catch:{ NoSuchFieldError -> 0x00de }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00de }
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00de }
        L_0x00de:
            int[] r0 = com.google.i18n.phonenumbers.h.f2866a     // Catch:{ NoSuchFieldError -> 0x00e8 }
            com.google.i18n.phonenumbers.j$a$a r1 = com.google.i18n.phonenumbers.j.a.C0127a.FROM_DEFAULT_COUNTRY     // Catch:{ NoSuchFieldError -> 0x00e8 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00e8 }
            r0[r1] = r3     // Catch:{ NoSuchFieldError -> 0x00e8 }
        L_0x00e8:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.i18n.phonenumbers.h.<clinit>():void");
    }
}
