package com.google.i18n.phonenumbers;

import com.google.i18n.phonenumbers.i;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: MultiFileMetadataSourceImpl */
final class f implements e {

    /* renamed from: a  reason: collision with root package name */
    private final String f2856a;

    /* renamed from: b  reason: collision with root package name */
    private final b f2857b;
    private final ConcurrentHashMap<String, i.b> c;
    private final ConcurrentHashMap<Integer, i.b> d;

    private f(String str, b bVar) {
        this.c = new ConcurrentHashMap<>();
        this.d = new ConcurrentHashMap<>();
        this.f2856a = str;
        this.f2857b = bVar;
    }

    f(b bVar) {
        this("/com/google/i18n/phonenumbers/data/PhoneNumberMetadataProto", bVar);
    }

    public final i.b a(String str) {
        return c.a(str, this.c, this.f2856a, this.f2857b);
    }

    public final i.b a(int i) {
        List list = a.a().get(Integer.valueOf(i));
        boolean z = false;
        if (list.size() == 1 && "001".equals(list.get(0))) {
            z = true;
        }
        if (!z) {
            return null;
        }
        return c.a(Integer.valueOf(i), this.d, this.f2856a, this.f2857b);
    }
}
