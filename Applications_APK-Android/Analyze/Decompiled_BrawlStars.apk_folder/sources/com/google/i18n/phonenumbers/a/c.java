package com.google.i18n.phonenumbers.a;

import java.util.LinkedHashMap;
import java.util.regex.Pattern;

/* compiled from: RegexCache */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private a<String, Pattern> f2850a = new a<>(100);

    public c(int i) {
    }

    public final Pattern a(String str) {
        Pattern a2 = this.f2850a.a(str);
        if (a2 != null) {
            return a2;
        }
        Pattern compile = Pattern.compile(str);
        this.f2850a.a(str, compile);
        return compile;
    }

    /* compiled from: RegexCache */
    static class a<K, V> {

        /* renamed from: a  reason: collision with root package name */
        int f2851a;

        /* renamed from: b  reason: collision with root package name */
        private LinkedHashMap<K, V> f2852b;

        public a(int i) {
            this.f2851a = i;
            this.f2852b = new d(this, ((i * 4) / 3) + 1, 0.75f, true);
        }

        public final synchronized V a(K k) {
            return this.f2852b.get(k);
        }

        public final synchronized void a(K k, V v) {
            this.f2852b.put(k, v);
        }
    }
}
