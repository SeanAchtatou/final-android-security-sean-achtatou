package com.google.i18n.phonenumbers.a;

import com.google.i18n.phonenumbers.i;
import java.util.regex.Matcher;

/* compiled from: RegexBasedMatcher */
public final class b implements a {

    /* renamed from: a  reason: collision with root package name */
    private final c f2849a = new c(100);

    public final boolean a(CharSequence charSequence, i.d dVar, boolean z) {
        String str = dVar.f2873a;
        if (str.length() == 0) {
            return false;
        }
        Matcher matcher = this.f2849a.a(str).matcher(charSequence);
        if (matcher.lookingAt() && matcher.matches()) {
            return true;
        }
        return false;
    }
}
