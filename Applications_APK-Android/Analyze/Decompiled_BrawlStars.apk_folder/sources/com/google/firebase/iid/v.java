package com.google.firebase.iid;

import android.os.Looper;
import android.os.Message;
import com.google.android.gms.internal.a.d;

final class v extends d {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ u f2821a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    v(u uVar, Looper looper) {
        super(looper);
        this.f2821a = uVar;
    }

    public final void handleMessage(Message message) {
        u.a(this.f2821a, message);
    }
}
