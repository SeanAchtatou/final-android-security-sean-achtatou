package com.google.firebase.iid;

import com.google.firebase.a.d;
import com.google.firebase.b;
import com.google.firebase.components.e;
import com.google.firebase.components.f;
import java.util.Arrays;
import java.util.List;

public final class Registrar implements e {

    static class a implements com.google.firebase.iid.a.a {

        /* renamed from: a  reason: collision with root package name */
        private final FirebaseInstanceId f2764a;

        public a(FirebaseInstanceId firebaseInstanceId) {
            this.f2764a = firebaseInstanceId;
        }
    }

    public final List<com.google.firebase.components.a<?>> getComponents() {
        return Arrays.asList(com.google.firebase.components.a.a(FirebaseInstanceId.class).a(f.a(b.class)).a(f.a(d.class)).a(p.f2813a).a(1).a(), com.google.firebase.components.a.a(com.google.firebase.iid.a.a.class).a(f.a(FirebaseInstanceId.class)).a(q.f2814a).a());
    }
}
