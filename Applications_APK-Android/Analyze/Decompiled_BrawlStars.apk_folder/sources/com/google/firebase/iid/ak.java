package com.google.firebase.iid;

import java.util.concurrent.Executor;

final /* synthetic */ class ak implements Executor {

    /* renamed from: a  reason: collision with root package name */
    static final Executor f2781a = new ak();

    private ak() {
    }

    public final void execute(Runnable runnable) {
        runnable.run();
    }
}
