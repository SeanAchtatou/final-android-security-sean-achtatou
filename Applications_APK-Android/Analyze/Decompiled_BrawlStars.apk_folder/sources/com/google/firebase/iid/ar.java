package com.google.firebase.iid;

import android.os.Bundle;
import com.google.android.gms.tasks.h;
import java.io.IOException;

final /* synthetic */ class ar implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final aq f2791a;

    /* renamed from: b  reason: collision with root package name */
    private final Bundle f2792b;
    private final h c;

    ar(aq aqVar, Bundle bundle, h hVar) {
        this.f2791a = aqVar;
        this.f2792b = bundle;
        this.c = hVar;
    }

    public final void run() {
        aq aqVar = this.f2791a;
        Bundle bundle = this.f2792b;
        h hVar = this.c;
        try {
            hVar.a((Boolean) aqVar.f2789a.a(bundle));
        } catch (IOException e) {
            hVar.a(e);
        }
    }
}
