package com.google.firebase.iid;

import java.util.concurrent.ThreadFactory;

final /* synthetic */ class aj implements ThreadFactory {

    /* renamed from: a  reason: collision with root package name */
    static final ThreadFactory f2780a = new aj();

    private aj() {
    }

    public final Thread newThread(Runnable runnable) {
        return ai.a(runnable);
    }
}
