package com.google.firebase.iid;

public final class zzal extends Exception {

    /* renamed from: a  reason: collision with root package name */
    final int f2830a;

    public zzal(int i, String str) {
        super(str);
        this.f2830a = i;
    }
}
