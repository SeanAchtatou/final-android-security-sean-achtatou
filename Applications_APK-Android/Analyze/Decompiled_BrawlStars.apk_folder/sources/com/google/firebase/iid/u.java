package com.google.firebase.iid;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcelable;
import android.support.v4.util.SimpleArrayMap;
import android.util.Log;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.tasks.g;
import com.google.android.gms.tasks.h;
import com.google.android.gms.tasks.j;
import com.google.firebase.iid.zzl;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class u {

    /* renamed from: a  reason: collision with root package name */
    private static int f2819a;

    /* renamed from: b  reason: collision with root package name */
    private static PendingIntent f2820b;
    private final SimpleArrayMap<String, h<Bundle>> c = new SimpleArrayMap<>();
    private final Context d;
    private final o e;
    private Messenger f;
    private Messenger g;
    private zzl h;

    public u(Context context, o oVar) {
        this.d = context;
        this.e = oVar;
        this.f = new Messenger(new v(this, Looper.getMainLooper()));
    }

    private static synchronized void a(Context context, Intent intent) {
        synchronized (u.class) {
            if (f2820b == null) {
                Intent intent2 = new Intent();
                intent2.setPackage("com.google.example.invalidpackage");
                f2820b = PendingIntent.getBroadcast(context, 0, intent2, 0);
            }
            intent.putExtra("app", f2820b);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0023, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void a(java.lang.String r3, android.os.Bundle r4) {
        /*
            r2 = this;
            android.support.v4.util.SimpleArrayMap<java.lang.String, com.google.android.gms.tasks.h<android.os.Bundle>> r0 = r2.c
            monitor-enter(r0)
            android.support.v4.util.SimpleArrayMap<java.lang.String, com.google.android.gms.tasks.h<android.os.Bundle>> r1 = r2.c     // Catch:{ all -> 0x0029 }
            java.lang.Object r1 = r1.remove(r3)     // Catch:{ all -> 0x0029 }
            com.google.android.gms.tasks.h r1 = (com.google.android.gms.tasks.h) r1     // Catch:{ all -> 0x0029 }
            if (r1 != 0) goto L_0x0024
            java.lang.String r4 = "Missing callback for "
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ all -> 0x0029 }
            int r1 = r3.length()     // Catch:{ all -> 0x0029 }
            if (r1 == 0) goto L_0x001d
            r4.concat(r3)     // Catch:{ all -> 0x0029 }
            goto L_0x0022
        L_0x001d:
            java.lang.String r3 = new java.lang.String     // Catch:{ all -> 0x0029 }
            r3.<init>(r4)     // Catch:{ all -> 0x0029 }
        L_0x0022:
            monitor-exit(r0)     // Catch:{ all -> 0x0029 }
            return
        L_0x0024:
            r1.a(r4)     // Catch:{ all -> 0x0029 }
            monitor-exit(r0)     // Catch:{ all -> 0x0029 }
            return
        L_0x0029:
            r3 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0029 }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.iid.u.a(java.lang.String, android.os.Bundle):void");
    }

    /* access modifiers changed from: package-private */
    public final Bundle a(Bundle bundle) throws IOException {
        Object obj;
        if (this.e.d() < 12000000) {
            return b(bundle);
        }
        e a2 = e.a(this.d);
        g a3 = a2.a(new n(a2.a(), bundle));
        try {
            l.c("Must not be called on the main application thread");
            l.a(a3, "Task must not be null");
            if (a3.a()) {
                obj = j.a(a3);
            } else {
                j.a aVar = new j.a((byte) 0);
                j.a(a3, aVar);
                aVar.f2682a.await();
                obj = j.a(a3);
            }
            return (Bundle) obj;
        } catch (InterruptedException | ExecutionException e2) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                String valueOf = String.valueOf(e2);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 22);
                sb.append("Error making request: ");
                sb.append(valueOf);
                sb.toString();
            }
            if (!(e2.getCause() instanceof zzal) || ((zzal) e2.getCause()).f2830a != 4) {
                return null;
            }
            return b(bundle);
        }
    }

    private final Bundle b(Bundle bundle) throws IOException {
        Bundle c2 = c(bundle);
        if (c2 == null || !c2.containsKey("google.messenger")) {
            return c2;
        }
        Bundle c3 = c(bundle);
        if (c3 == null || !c3.containsKey("google.messenger")) {
            return c3;
        }
        return null;
    }

    private static synchronized String a() {
        String num;
        synchronized (u.class) {
            int i = f2819a;
            f2819a = i + 1;
            num = Integer.toString(i);
        }
        return num;
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x00d8 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final android.os.Bundle c(android.os.Bundle r7) throws java.io.IOException {
        /*
            r6 = this;
            java.lang.String r0 = a()
            com.google.android.gms.tasks.h r1 = new com.google.android.gms.tasks.h
            r1.<init>()
            android.support.v4.util.SimpleArrayMap<java.lang.String, com.google.android.gms.tasks.h<android.os.Bundle>> r2 = r6.c
            monitor-enter(r2)
            android.support.v4.util.SimpleArrayMap<java.lang.String, com.google.android.gms.tasks.h<android.os.Bundle>> r3 = r6.c     // Catch:{ all -> 0x0108 }
            r3.put(r0, r1)     // Catch:{ all -> 0x0108 }
            monitor-exit(r2)     // Catch:{ all -> 0x0108 }
            com.google.firebase.iid.o r2 = r6.e
            int r2 = r2.a()
            if (r2 == 0) goto L_0x0100
            android.content.Intent r2 = new android.content.Intent
            r2.<init>()
            java.lang.String r3 = "com.google.android.gms"
            r2.setPackage(r3)
            com.google.firebase.iid.o r3 = r6.e
            int r3 = r3.a()
            r4 = 2
            if (r3 != r4) goto L_0x0033
            java.lang.String r3 = "com.google.iid.TOKEN_REQUEST"
            r2.setAction(r3)
            goto L_0x0038
        L_0x0033:
            java.lang.String r3 = "com.google.android.c2dm.intent.REGISTER"
            r2.setAction(r3)
        L_0x0038:
            r2.putExtras(r7)
            android.content.Context r7 = r6.d
            a(r7, r2)
            java.lang.String r7 = java.lang.String.valueOf(r0)
            int r7 = r7.length()
            int r7 = r7 + 5
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r7)
            java.lang.String r7 = "|ID|"
            r3.append(r7)
            r3.append(r0)
            java.lang.String r7 = "|"
            r3.append(r7)
            java.lang.String r7 = r3.toString()
            java.lang.String r3 = "kid"
            r2.putExtra(r3, r7)
            r7 = 3
            java.lang.String r3 = "FirebaseInstanceId"
            boolean r7 = android.util.Log.isLoggable(r3, r7)
            if (r7 == 0) goto L_0x0090
            android.os.Bundle r7 = r2.getExtras()
            java.lang.String r7 = java.lang.String.valueOf(r7)
            java.lang.String r3 = java.lang.String.valueOf(r7)
            int r3 = r3.length()
            int r3 = r3 + 8
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>(r3)
            java.lang.String r3 = "Sending "
            r5.append(r3)
            r5.append(r7)
            r5.toString()
        L_0x0090:
            android.os.Messenger r7 = r6.f
            java.lang.String r3 = "google.messenger"
            r2.putExtra(r3, r7)
            android.os.Messenger r7 = r6.g
            if (r7 != 0) goto L_0x009f
            com.google.firebase.iid.zzl r7 = r6.h
            if (r7 == 0) goto L_0x00b6
        L_0x009f:
            android.os.Message r7 = android.os.Message.obtain()
            r7.obj = r2
            android.os.Messenger r3 = r6.g     // Catch:{ RemoteException -> 0x00b5 }
            if (r3 == 0) goto L_0x00af
            android.os.Messenger r3 = r6.g     // Catch:{ RemoteException -> 0x00b5 }
            r3.send(r7)     // Catch:{ RemoteException -> 0x00b5 }
            goto L_0x00c9
        L_0x00af:
            com.google.firebase.iid.zzl r3 = r6.h     // Catch:{ RemoteException -> 0x00b5 }
            r3.a(r7)     // Catch:{ RemoteException -> 0x00b5 }
            goto L_0x00c9
        L_0x00b5:
        L_0x00b6:
            com.google.firebase.iid.o r7 = r6.e
            int r7 = r7.a()
            if (r7 != r4) goto L_0x00c4
            android.content.Context r7 = r6.d
            r7.sendBroadcast(r2)
            goto L_0x00c9
        L_0x00c4:
            android.content.Context r7 = r6.d
            r7.startService(r2)
        L_0x00c9:
            com.google.android.gms.tasks.ab<TResult> r7 = r1.f2678a     // Catch:{ InterruptedException | TimeoutException -> 0x00eb, ExecutionException -> 0x00e4 }
            r1 = 30000(0x7530, double:1.4822E-319)
            java.util.concurrent.TimeUnit r3 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ InterruptedException | TimeoutException -> 0x00eb, ExecutionException -> 0x00e4 }
            java.lang.Object r7 = com.google.android.gms.tasks.j.a(r7, r1, r3)     // Catch:{ InterruptedException | TimeoutException -> 0x00eb, ExecutionException -> 0x00e4 }
            android.os.Bundle r7 = (android.os.Bundle) r7     // Catch:{ InterruptedException | TimeoutException -> 0x00eb, ExecutionException -> 0x00e4 }
            android.support.v4.util.SimpleArrayMap<java.lang.String, com.google.android.gms.tasks.h<android.os.Bundle>> r1 = r6.c
            monitor-enter(r1)
            android.support.v4.util.SimpleArrayMap<java.lang.String, com.google.android.gms.tasks.h<android.os.Bundle>> r2 = r6.c     // Catch:{ all -> 0x00df }
            r2.remove(r0)     // Catch:{ all -> 0x00df }
            monitor-exit(r1)     // Catch:{ all -> 0x00df }
            return r7
        L_0x00df:
            r7 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00df }
            throw r7
        L_0x00e2:
            r7 = move-exception
            goto L_0x00f3
        L_0x00e4:
            r7 = move-exception
            java.io.IOException r1 = new java.io.IOException     // Catch:{ all -> 0x00e2 }
            r1.<init>(r7)     // Catch:{ all -> 0x00e2 }
            throw r1     // Catch:{ all -> 0x00e2 }
        L_0x00eb:
            java.io.IOException r7 = new java.io.IOException     // Catch:{ all -> 0x00e2 }
            java.lang.String r1 = "TIMEOUT"
            r7.<init>(r1)     // Catch:{ all -> 0x00e2 }
            throw r7     // Catch:{ all -> 0x00e2 }
        L_0x00f3:
            android.support.v4.util.SimpleArrayMap<java.lang.String, com.google.android.gms.tasks.h<android.os.Bundle>> r1 = r6.c
            monitor-enter(r1)
            android.support.v4.util.SimpleArrayMap<java.lang.String, com.google.android.gms.tasks.h<android.os.Bundle>> r2 = r6.c     // Catch:{ all -> 0x00fd }
            r2.remove(r0)     // Catch:{ all -> 0x00fd }
            monitor-exit(r1)     // Catch:{ all -> 0x00fd }
            throw r7
        L_0x00fd:
            r7 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00fd }
            throw r7
        L_0x0100:
            java.io.IOException r7 = new java.io.IOException
            java.lang.String r0 = "MISSING_INSTANCEID_SERVICE"
            r7.<init>(r0)
            throw r7
        L_0x0108:
            r7 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0108 }
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.iid.u.c(android.os.Bundle):android.os.Bundle");
    }

    static /* synthetic */ void a(u uVar, Message message) {
        if (message != null && (message.obj instanceof Intent)) {
            Intent intent = (Intent) message.obj;
            intent.setExtrasClassLoader(new zzl.a());
            if (intent.hasExtra("google.messenger")) {
                Parcelable parcelableExtra = intent.getParcelableExtra("google.messenger");
                if (parcelableExtra instanceof zzl) {
                    uVar.h = (zzl) parcelableExtra;
                }
                if (parcelableExtra instanceof Messenger) {
                    uVar.g = (Messenger) parcelableExtra;
                }
            }
            Intent intent2 = (Intent) message.obj;
            String action = intent2.getAction();
            if ("com.google.android.c2dm.intent.REGISTRATION".equals(action)) {
                String stringExtra = intent2.getStringExtra("registration_id");
                if (stringExtra == null) {
                    stringExtra = intent2.getStringExtra("unregistered");
                }
                if (stringExtra == null) {
                    String stringExtra2 = intent2.getStringExtra("error");
                    if (stringExtra2 == null) {
                        String valueOf = String.valueOf(intent2.getExtras());
                        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 49);
                        sb.append("Unexpected response, no error or registration id ");
                        sb.append(valueOf);
                        sb.toString();
                        return;
                    }
                    if (Log.isLoggable("FirebaseInstanceId", 3)) {
                        String valueOf2 = String.valueOf(stringExtra2);
                        if (valueOf2.length() != 0) {
                            "Received InstanceID error ".concat(valueOf2);
                        } else {
                            new String("Received InstanceID error ");
                        }
                    }
                    if (stringExtra2.startsWith("|")) {
                        String[] split = stringExtra2.split("\\|");
                        if (split.length <= 2 || !"ID".equals(split[1])) {
                            String valueOf3 = String.valueOf(stringExtra2);
                            if (valueOf3.length() != 0) {
                                "Unexpected structured response ".concat(valueOf3);
                            } else {
                                new String("Unexpected structured response ");
                            }
                        } else {
                            String str = split[2];
                            String str2 = split[3];
                            if (str2.startsWith(":")) {
                                str2 = str2.substring(1);
                            }
                            uVar.a(str, intent2.putExtra("error", str2).getExtras());
                        }
                    } else {
                        synchronized (uVar.c) {
                            for (int i = 0; i < uVar.c.size(); i++) {
                                uVar.a(uVar.c.keyAt(i), intent2.getExtras());
                            }
                        }
                    }
                } else {
                    Matcher matcher = Pattern.compile("\\|ID\\|([^|]+)\\|:?+(.*)").matcher(stringExtra);
                    if (matcher.matches()) {
                        String group = matcher.group(1);
                        String group2 = matcher.group(2);
                        Bundle extras = intent2.getExtras();
                        extras.putString("registration_id", group2);
                        uVar.a(group, extras);
                    } else if (Log.isLoggable("FirebaseInstanceId", 3)) {
                        String valueOf4 = String.valueOf(stringExtra);
                        if (valueOf4.length() != 0) {
                            "Unexpected response string: ".concat(valueOf4);
                        } else {
                            new String("Unexpected response string: ");
                        }
                    }
                }
            } else if (Log.isLoggable("FirebaseInstanceId", 3)) {
                String valueOf5 = String.valueOf(action);
                if (valueOf5.length() != 0) {
                    "Unexpected response action: ".concat(valueOf5);
                } else {
                    new String("Unexpected response action: ");
                }
            }
        }
    }
}
