package com.google.firebase.iid;

import android.content.Intent;

final class ac implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Intent f2768a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ Intent f2769b;
    private final /* synthetic */ zzb c;

    ac(zzb zzb, Intent intent, Intent intent2) {
        this.c = zzb;
        this.f2768a = intent;
        this.f2769b = intent2;
    }

    public final void run() {
        this.c.b(this.f2768a);
        this.c.d(this.f2769b);
    }
}
