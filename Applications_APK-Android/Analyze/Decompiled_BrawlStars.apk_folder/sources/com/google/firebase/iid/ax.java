package com.google.firebase.iid;

import android.util.Base64;
import java.security.KeyPair;
import java.util.Arrays;

final class ax {

    /* renamed from: a  reason: collision with root package name */
    final KeyPair f2796a;

    /* renamed from: b  reason: collision with root package name */
    final long f2797b;

    ax(KeyPair keyPair, long j) {
        this.f2796a = keyPair;
        this.f2797b = j;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof ax)) {
            return false;
        }
        ax axVar = (ax) obj;
        if (this.f2797b != axVar.f2797b || !this.f2796a.getPublic().equals(axVar.f2796a.getPublic()) || !this.f2796a.getPrivate().equals(axVar.f2796a.getPrivate())) {
            return false;
        }
        return true;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.f2796a.getPublic(), this.f2796a.getPrivate(), Long.valueOf(this.f2797b)});
    }

    /* access modifiers changed from: package-private */
    public final String a() {
        return Base64.encodeToString(this.f2796a.getPublic().getEncoded(), 11);
    }

    /* access modifiers changed from: package-private */
    public final String b() {
        return Base64.encodeToString(this.f2796a.getPrivate().getEncoded(), 11);
    }
}
