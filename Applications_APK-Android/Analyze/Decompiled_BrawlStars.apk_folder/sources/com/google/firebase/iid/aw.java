package com.google.firebase.iid;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import com.google.android.gms.internal.a.f;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Properties;

final class aw {
    aw() {
    }

    /* access modifiers changed from: package-private */
    public final ax a(Context context, String str) {
        ax axVar = new ax(c.a(), System.currentTimeMillis());
        ax a2 = a(context, str, axVar, true);
        if (a2 != null && !a2.equals(axVar)) {
            return a2;
        }
        a(context, str, axVar);
        return axVar;
    }

    /* access modifiers changed from: package-private */
    public final ax b(Context context, String str) throws d {
        try {
            ax c = c(context, str);
            if (c != null) {
                a(context, str, c);
                return c;
            }
            e = null;
            try {
                ax a2 = a(context.getSharedPreferences("com.google.android.gms.appid", 0), str);
                if (a2 != null) {
                    a(context, str, a2, false);
                    return a2;
                }
            } catch (d e) {
                e = e;
            }
            if (e == null) {
                return null;
            }
            throw e;
        } catch (d e2) {
            e = e2;
        }
    }

    private static KeyPair a(String str, String str2) throws d {
        try {
            byte[] decode = Base64.decode(str, 8);
            byte[] decode2 = Base64.decode(str2, 8);
            try {
                KeyFactory instance = KeyFactory.getInstance("RSA");
                return new KeyPair(instance.generatePublic(new X509EncodedKeySpec(decode)), instance.generatePrivate(new PKCS8EncodedKeySpec(decode2)));
            } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
                String valueOf = String.valueOf(e);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 19);
                sb.append("Invalid key stored ");
                sb.append(valueOf);
                sb.toString();
                throw new d(e);
            }
        } catch (IllegalArgumentException e2) {
            throw new d(e2);
        }
    }

    private final ax c(Context context, String str) throws d {
        File d = d(context, str);
        if (!d.exists()) {
            return null;
        }
        try {
            return a(d);
        } catch (d | IOException e) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                String valueOf = String.valueOf(e);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 40);
                sb.append("Failed to read key from file, retrying: ");
                sb.append(valueOf);
                sb.toString();
            }
            try {
                return a(d);
            } catch (IOException e2) {
                String valueOf2 = String.valueOf(e2);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 45);
                sb2.append("IID file exists, but failed to read from it: ");
                sb2.append(valueOf2);
                sb2.toString();
                throw new d(e2);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0093, code lost:
        r9 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0094, code lost:
        if (r6 != null) goto L_0x0096;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        a(r8, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0099, code lost:
        throw r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x009c, code lost:
        r8 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        a(r6, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00a0, code lost:
        throw r8;
     */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:16:0x0050=Splitter:B:16:0x0050, B:29:0x008d=Splitter:B:29:0x008d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.google.firebase.iid.ax a(android.content.Context r6, java.lang.String r7, com.google.firebase.iid.ax r8, boolean r9) {
        /*
            java.util.Properties r0 = new java.util.Properties
            r0.<init>()
            java.lang.String r1 = r8.a()
            java.lang.String r2 = "pub"
            r0.setProperty(r2, r1)
            java.lang.String r1 = r8.b()
            java.lang.String r2 = "pri"
            r0.setProperty(r2, r1)
            long r1 = r8.f2797b
            java.lang.String r1 = java.lang.String.valueOf(r1)
            java.lang.String r2 = "cre"
            r0.setProperty(r2, r1)
            java.io.File r6 = d(r6, r7)
            r7 = 0
            r6.createNewFile()     // Catch:{ IOException -> 0x00a1 }
            java.io.RandomAccessFile r1 = new java.io.RandomAccessFile     // Catch:{ IOException -> 0x00a1 }
            java.lang.String r2 = "rw"
            r1.<init>(r6, r2)     // Catch:{ IOException -> 0x00a1 }
            java.nio.channels.FileChannel r6 = r1.getChannel()     // Catch:{ all -> 0x009a }
            r6.lock()     // Catch:{ all -> 0x0091 }
            r2 = 0
            if (r9 == 0) goto L_0x007e
            long r4 = r6.size()     // Catch:{ all -> 0x0091 }
            int r9 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r9 <= 0) goto L_0x007e
            r6.position(r2)     // Catch:{ IOException -> 0x0056, d -> 0x0054 }
            com.google.firebase.iid.ax r8 = a(r6)     // Catch:{ IOException -> 0x0056, d -> 0x0054 }
            if (r6 == 0) goto L_0x0050
            a(r7, r6)     // Catch:{ all -> 0x009a }
        L_0x0050:
            a(r7, r1)     // Catch:{ IOException -> 0x00a1 }
            return r8
        L_0x0054:
            r9 = move-exception
            goto L_0x0057
        L_0x0056:
            r9 = move-exception
        L_0x0057:
            java.lang.String r4 = "FirebaseInstanceId"
            r5 = 3
            boolean r4 = android.util.Log.isLoggable(r4, r5)     // Catch:{ all -> 0x0091 }
            if (r4 == 0) goto L_0x007e
            java.lang.String r9 = java.lang.String.valueOf(r9)     // Catch:{ all -> 0x0091 }
            java.lang.String r4 = java.lang.String.valueOf(r9)     // Catch:{ all -> 0x0091 }
            int r4 = r4.length()     // Catch:{ all -> 0x0091 }
            int r4 = r4 + 64
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0091 }
            r5.<init>(r4)     // Catch:{ all -> 0x0091 }
            java.lang.String r4 = "Tried reading key pair before writing new one, but failed with: "
            r5.append(r4)     // Catch:{ all -> 0x0091 }
            r5.append(r9)     // Catch:{ all -> 0x0091 }
            r5.toString()     // Catch:{ all -> 0x0091 }
        L_0x007e:
            r6.position(r2)     // Catch:{ all -> 0x0091 }
            java.io.OutputStream r9 = java.nio.channels.Channels.newOutputStream(r6)     // Catch:{ all -> 0x0091 }
            r0.store(r9, r7)     // Catch:{ all -> 0x0091 }
            if (r6 == 0) goto L_0x008d
            a(r7, r6)     // Catch:{ all -> 0x009a }
        L_0x008d:
            a(r7, r1)     // Catch:{ IOException -> 0x00a1 }
            return r8
        L_0x0091:
            r8 = move-exception
            throw r8     // Catch:{ all -> 0x0093 }
        L_0x0093:
            r9 = move-exception
            if (r6 == 0) goto L_0x0099
            a(r8, r6)     // Catch:{ all -> 0x009a }
        L_0x0099:
            throw r9     // Catch:{ all -> 0x009a }
        L_0x009a:
            r6 = move-exception
            throw r6     // Catch:{ all -> 0x009c }
        L_0x009c:
            r8 = move-exception
            a(r6, r1)     // Catch:{ IOException -> 0x00a1 }
            throw r8     // Catch:{ IOException -> 0x00a1 }
        L_0x00a1:
            r6 = move-exception
            java.lang.String r6 = java.lang.String.valueOf(r6)
            java.lang.String r8 = java.lang.String.valueOf(r6)
            int r8 = r8.length()
            int r8 = r8 + 21
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>(r8)
            java.lang.String r8 = "Failed to write key: "
            r9.append(r8)
            r9.append(r6)
            r9.toString()
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.iid.aw.a(android.content.Context, java.lang.String, com.google.firebase.iid.ax, boolean):com.google.firebase.iid.ax");
    }

    static File a(Context context) {
        File noBackupFilesDir = ContextCompat.getNoBackupFilesDir(context);
        if (noBackupFilesDir == null || !noBackupFilesDir.isDirectory()) {
            return context.getFilesDir();
        }
        return noBackupFilesDir;
    }

    private static File d(Context context, String str) {
        String str2;
        if (TextUtils.isEmpty(str)) {
            str2 = "com.google.InstanceId.properties";
        } else {
            try {
                String encodeToString = Base64.encodeToString(str.getBytes("UTF-8"), 11);
                StringBuilder sb = new StringBuilder(String.valueOf(encodeToString).length() + 33);
                sb.append("com.google.InstanceId_");
                sb.append(encodeToString);
                sb.append(".properties");
                str2 = sb.toString();
            } catch (UnsupportedEncodingException e) {
                throw new AssertionError(e);
            }
        }
        return new File(a(context), str2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0025, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0026, code lost:
        if (r7 != null) goto L_0x0028;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        a(r1, r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x002b, code lost:
        throw r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x002e, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x002f, code lost:
        a(r7, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0032, code lost:
        throw r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.google.firebase.iid.ax a(java.io.File r7) throws com.google.firebase.iid.d, java.io.IOException {
        /*
            java.io.FileInputStream r0 = new java.io.FileInputStream
            r0.<init>(r7)
            java.nio.channels.FileChannel r7 = r0.getChannel()     // Catch:{ all -> 0x002c }
            r2 = 0
            r4 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r6 = 1
            r1 = r7
            r1.lock(r2, r4, r6)     // Catch:{ all -> 0x0023 }
            com.google.firebase.iid.ax r1 = a(r7)     // Catch:{ all -> 0x0023 }
            r2 = 0
            if (r7 == 0) goto L_0x001f
            a(r2, r7)     // Catch:{ all -> 0x002c }
        L_0x001f:
            a(r2, r0)
            return r1
        L_0x0023:
            r1 = move-exception
            throw r1     // Catch:{ all -> 0x0025 }
        L_0x0025:
            r2 = move-exception
            if (r7 == 0) goto L_0x002b
            a(r1, r7)     // Catch:{ all -> 0x002c }
        L_0x002b:
            throw r2     // Catch:{ all -> 0x002c }
        L_0x002c:
            r7 = move-exception
            throw r7     // Catch:{ all -> 0x002e }
        L_0x002e:
            r1 = move-exception
            a(r7, r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.iid.aw.a(java.io.File):com.google.firebase.iid.ax");
    }

    private static ax a(FileChannel fileChannel) throws d, IOException {
        Properties properties = new Properties();
        properties.load(Channels.newInputStream(fileChannel));
        String property = properties.getProperty("pub");
        String property2 = properties.getProperty("pri");
        if (property == null || property2 == null) {
            throw new d("Invalid properties file");
        }
        try {
            return new ax(a(property, property2), Long.parseLong(properties.getProperty("cre")));
        } catch (NumberFormatException e) {
            throw new d(e);
        }
    }

    private static ax a(SharedPreferences sharedPreferences, String str) throws d {
        String string = sharedPreferences.getString(x.a(str, "|P|"), null);
        String string2 = sharedPreferences.getString(x.a(str, "|K|"), null);
        if (string == null || string2 == null) {
            return null;
        }
        return new ax(a(string, string2), b(sharedPreferences, str));
    }

    private static void a(Context context, String str, ax axVar) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("com.google.android.gms.appid", 0);
        try {
            if (axVar.equals(a(sharedPreferences, str))) {
                return;
            }
        } catch (d unused) {
        }
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(x.a(str, "|P|"), axVar.a());
        edit.putString(x.a(str, "|K|"), axVar.b());
        edit.putString(x.a(str, "cre"), String.valueOf(axVar.f2797b));
        edit.commit();
    }

    private static long b(SharedPreferences sharedPreferences, String str) {
        String string = sharedPreferences.getString(x.a(str, "cre"), null);
        if (string == null) {
            return 0;
        }
        try {
            return Long.parseLong(string);
        } catch (NumberFormatException unused) {
            return 0;
        }
    }

    private static /* synthetic */ void a(Throwable th, FileChannel fileChannel) {
        if (th != null) {
            try {
                fileChannel.close();
            } catch (Throwable th2) {
                f.a(th, th2);
            }
        } else {
            fileChannel.close();
        }
    }

    private static /* synthetic */ void a(Throwable th, RandomAccessFile randomAccessFile) {
        if (th != null) {
            try {
                randomAccessFile.close();
            } catch (Throwable th2) {
                f.a(th, th2);
            }
        } else {
            randomAccessFile.close();
        }
    }

    private static /* synthetic */ void a(Throwable th, FileInputStream fileInputStream) {
        if (th != null) {
            try {
                fileInputStream.close();
            } catch (Throwable th2) {
                f.a(th, th2);
            }
        } else {
            fileInputStream.close();
        }
    }
}
