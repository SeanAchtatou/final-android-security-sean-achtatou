package com.google.firebase.iid;

import com.google.firebase.components.c;
import com.google.firebase.components.d;
import com.google.firebase.iid.Registrar;

final /* synthetic */ class q implements d {

    /* renamed from: a  reason: collision with root package name */
    static final d f2814a = new q();

    private q() {
    }

    public final Object a(c cVar) {
        return new Registrar.a((FirebaseInstanceId) cVar.a(FirebaseInstanceId.class));
    }
}
