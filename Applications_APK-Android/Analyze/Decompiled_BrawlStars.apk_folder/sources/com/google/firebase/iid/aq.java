package com.google.firebase.iid;

import android.os.Build;
import android.os.Bundle;
import com.facebook.appevents.AppEventsConstants;
import com.google.android.gms.tasks.g;
import com.google.android.gms.tasks.h;
import com.google.firebase.b;
import java.io.IOException;
import java.util.concurrent.Executor;

final class aq implements b {

    /* renamed from: a  reason: collision with root package name */
    final u f2789a;

    /* renamed from: b  reason: collision with root package name */
    private final b f2790b;
    private final o c;
    private final Executor d;

    aq(b bVar, o oVar, Executor executor) {
        this(bVar, oVar, executor, new u(bVar.a(), oVar));
    }

    private aq(b bVar, o oVar, Executor executor, u uVar) {
        this.f2790b = bVar;
        this.c = oVar;
        this.f2789a = uVar;
        this.d = executor;
    }

    public final boolean a() {
        return this.c.a() != 0;
    }

    public final g<String> a(String str, String str2, String str3) {
        return b(a(str, str2, str3, new Bundle()));
    }

    public final g<Void> b(String str, String str2, String str3) {
        Bundle bundle = new Bundle();
        String valueOf = String.valueOf(str3);
        bundle.putString("gcm.topic", valueOf.length() != 0 ? "/topics/".concat(valueOf) : new String("/topics/"));
        String valueOf2 = String.valueOf(str3);
        return a(b(a(str, str2, valueOf2.length() != 0 ? "/topics/".concat(valueOf2) : new String("/topics/"), bundle)));
    }

    public final g<Void> c(String str, String str2, String str3) {
        Bundle bundle = new Bundle();
        String valueOf = String.valueOf(str3);
        bundle.putString("gcm.topic", valueOf.length() != 0 ? "/topics/".concat(valueOf) : new String("/topics/"));
        bundle.putString("delete", AppEventsConstants.EVENT_PARAM_VALUE_YES);
        String valueOf2 = String.valueOf(str3);
        return a(b(a(str, str2, valueOf2.length() != 0 ? "/topics/".concat(valueOf2) : new String("/topics/"), bundle)));
    }

    private final g<Bundle> a(String str, String str2, String str3, Bundle bundle) {
        bundle.putString("scope", str3);
        bundle.putString("sender", str2);
        bundle.putString("subtype", str2);
        bundle.putString("appid", str);
        bundle.putString("gmp_app_id", this.f2790b.b().f2730a);
        bundle.putString("gmsv", Integer.toString(this.c.d()));
        bundle.putString("osv", Integer.toString(Build.VERSION.SDK_INT));
        bundle.putString("app_ver", this.c.b());
        bundle.putString("app_ver_name", this.c.c());
        bundle.putString("cliv", "fiid-12451000");
        h hVar = new h();
        this.d.execute(new ar(this, bundle, hVar));
        return hVar.f2678a;
    }

    private final <T> g<Void> a(g<T> gVar) {
        return gVar.a(ai.a(), new as());
    }

    private final g<String> b(g<Bundle> gVar) {
        return gVar.a(this.d, new at(this));
    }

    static /* synthetic */ String a(Bundle bundle) throws IOException {
        if (bundle != null) {
            String string = bundle.getString("registration_id");
            if (string != null || (string = bundle.getString("unregistered")) != null) {
                return string;
            }
            String string2 = bundle.getString("error");
            if ("RST".equals(string2)) {
                throw new IOException("INSTANCE_ID_RESET");
            } else if (string2 != null) {
                throw new IOException(string2);
            } else {
                String valueOf = String.valueOf(bundle);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 21);
                sb.append("Unexpected response: ");
                sb.append(valueOf);
                sb.toString();
                new Throwable();
                throw new IOException("SERVICE_NOT_AVAILABLE");
            }
        } else {
            throw new IOException("SERVICE_NOT_AVAILABLE");
        }
    }
}
