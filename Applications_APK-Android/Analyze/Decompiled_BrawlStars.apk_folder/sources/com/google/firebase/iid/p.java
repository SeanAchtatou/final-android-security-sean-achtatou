package com.google.firebase.iid;

import com.google.firebase.b;
import com.google.firebase.components.c;
import com.google.firebase.components.d;

final /* synthetic */ class p implements d {

    /* renamed from: a  reason: collision with root package name */
    static final d f2813a = new p();

    private p() {
    }

    public final Object a(c cVar) {
        return new FirebaseInstanceId((b) cVar.a(b.class), (com.google.firebase.a.d) cVar.a(com.google.firebase.a.d.class));
    }
}
