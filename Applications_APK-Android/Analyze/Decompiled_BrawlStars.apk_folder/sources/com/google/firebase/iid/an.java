package com.google.firebase.iid;

import com.google.android.gms.tasks.g;

final /* synthetic */ class an implements t {

    /* renamed from: a  reason: collision with root package name */
    private final FirebaseInstanceId f2784a;

    /* renamed from: b  reason: collision with root package name */
    private final String f2785b;
    private final String c;
    private final String d;
    private final String e;

    an(FirebaseInstanceId firebaseInstanceId, String str, String str2, String str3, String str4) {
        this.f2784a = firebaseInstanceId;
        this.f2785b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
    }

    public final g a() {
        FirebaseInstanceId firebaseInstanceId = this.f2784a;
        String str = this.f2785b;
        String str2 = this.c;
        String str3 = this.d;
        String str4 = this.e;
        return firebaseInstanceId.e.a(str, str3, str4).a(firebaseInstanceId.f2759b, new ao(firebaseInstanceId, str3, str4, str));
    }
}
