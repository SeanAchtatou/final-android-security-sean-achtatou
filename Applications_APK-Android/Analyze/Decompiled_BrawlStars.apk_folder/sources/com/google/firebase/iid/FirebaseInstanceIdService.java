package com.google.firebase.iid;

import android.content.Intent;
import android.util.Log;

@Deprecated
public class FirebaseInstanceIdService extends zzb {
    /* access modifiers changed from: protected */
    public final Intent a(Intent intent) {
        return w.a().c.poll();
    }

    public final void b(Intent intent) {
        String stringExtra;
        if (!"com.google.firebase.iid.TOKEN_REFRESH".equals(intent.getAction()) && (stringExtra = intent.getStringExtra("CMD")) != null) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                String valueOf = String.valueOf(intent.getExtras());
                StringBuilder sb = new StringBuilder(String.valueOf(stringExtra).length() + 21 + String.valueOf(valueOf).length());
                sb.append("Received command: ");
                sb.append(stringExtra);
                sb.append(" - ");
                sb.append(valueOf);
                sb.toString();
            }
            if ("RST".equals(stringExtra) || "RST_FULL".equals(stringExtra)) {
                FirebaseInstanceId.a().g();
            } else if ("SYNC".equals(stringExtra)) {
                FirebaseInstanceId a2 = FirebaseInstanceId.a();
                FirebaseInstanceId.f2758a.c("");
                a2.c();
            }
        }
    }
}
