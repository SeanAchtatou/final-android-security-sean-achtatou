package com.google.firebase.iid;

import android.os.Build;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public class zzl implements Parcelable {
    public static final Parcelable.Creator<zzl> CREATOR = new al();

    /* renamed from: a  reason: collision with root package name */
    private Messenger f2833a;

    /* renamed from: b  reason: collision with root package name */
    private au f2834b;

    public static final class a extends ClassLoader {
        /* access modifiers changed from: protected */
        public final Class<?> loadClass(String str, boolean z) throws ClassNotFoundException {
            if (!"com.google.android.gms.iid.MessengerCompat".equals(str)) {
                return super.loadClass(str, z);
            }
            FirebaseInstanceId.f();
            return zzl.class;
        }
    }

    public zzl(IBinder iBinder) {
        if (Build.VERSION.SDK_INT >= 21) {
            this.f2833a = new Messenger(iBinder);
        } else {
            this.f2834b = new zzw(iBinder);
        }
    }

    public int describeContents() {
        return 0;
    }

    public final void a(Message message) throws RemoteException {
        Messenger messenger = this.f2833a;
        if (messenger != null) {
            messenger.send(message);
        } else {
            this.f2834b.a(message);
        }
    }

    private final IBinder a() {
        Messenger messenger = this.f2833a;
        return messenger != null ? messenger.getBinder() : this.f2834b.asBinder();
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        try {
            return a().equals(((zzl) obj).a());
        } catch (ClassCastException unused) {
            return false;
        }
    }

    public int hashCode() {
        return a().hashCode();
    }

    public void writeToParcel(Parcel parcel, int i) {
        Messenger messenger = this.f2833a;
        if (messenger != null) {
            parcel.writeStrongBinder(messenger.getBinder());
        } else {
            parcel.writeStrongBinder(this.f2834b.asBinder());
        }
    }
}
