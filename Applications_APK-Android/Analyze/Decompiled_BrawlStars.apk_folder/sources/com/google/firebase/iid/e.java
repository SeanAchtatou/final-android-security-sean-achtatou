package com.google.firebase.iid;

import android.content.Context;
import android.util.Log;
import com.google.android.gms.internal.a.b;
import com.google.android.gms.tasks.g;
import java.util.concurrent.ScheduledExecutorService;

public final class e {
    private static e c;

    /* renamed from: a  reason: collision with root package name */
    final Context f2798a;

    /* renamed from: b  reason: collision with root package name */
    final ScheduledExecutorService f2799b;
    private f d = new f(this, (byte) 0);
    private int e = 1;

    public static synchronized e a(Context context) {
        e eVar;
        synchronized (e.class) {
            if (c == null) {
                c = new e(context, b.a().b(new com.google.android.gms.common.util.a.b("MessengerIpcClient")));
            }
            eVar = c;
        }
        return eVar;
    }

    private e(Context context, ScheduledExecutorService scheduledExecutorService) {
        this.f2799b = scheduledExecutorService;
        this.f2798a = context.getApplicationContext();
    }

    public final synchronized <T> g<T> a(m mVar) {
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            String valueOf = String.valueOf(mVar);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 9);
            sb.append("Queueing ");
            sb.append(valueOf);
            sb.toString();
        }
        if (!this.d.a(mVar)) {
            this.d = new f(this, (byte) 0);
            this.d.a(mVar);
        }
        return mVar.f2810b.f2678a;
    }

    public final synchronized int a() {
        int i;
        i = this.e;
        this.e = i + 1;
        return i;
    }
}
