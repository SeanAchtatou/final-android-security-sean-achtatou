package com.google.firebase.iid;

import android.util.Pair;
import com.google.android.gms.tasks.a;
import com.google.android.gms.tasks.g;

final /* synthetic */ class s implements a {

    /* renamed from: a  reason: collision with root package name */
    private final r f2817a;

    /* renamed from: b  reason: collision with root package name */
    private final Pair f2818b;

    s(r rVar, Pair pair) {
        this.f2817a = rVar;
        this.f2818b = pair;
    }

    public final Object a(g gVar) {
        return this.f2817a.a(this.f2818b, gVar);
    }
}
