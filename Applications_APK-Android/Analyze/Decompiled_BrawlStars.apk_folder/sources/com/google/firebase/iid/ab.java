package com.google.firebase.iid;

import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import com.google.android.gms.tasks.h;
import java.io.IOException;
import java.util.Map;

final class ab {

    /* renamed from: a  reason: collision with root package name */
    private int f2766a = 0;

    /* renamed from: b  reason: collision with root package name */
    private final Map<Integer, h<Void>> f2767b = new ArrayMap();
    private final x c;

    ab(x xVar) {
        this.c = xVar;
    }

    /* access modifiers changed from: package-private */
    public final synchronized boolean a() {
        return b() != null;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0014, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0016, code lost:
        monitor-enter(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r2 = r4.f2767b.remove(java.lang.Integer.valueOf(r4.f2766a));
        a(r0);
        r4.f2766a++;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002d, code lost:
        monitor-exit(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002e, code lost:
        if (r2 == null) goto L_0x0000;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0030, code lost:
        r2.a((java.lang.Boolean) null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0012, code lost:
        if (a(r5, r0) != false) goto L_0x0016;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(com.google.firebase.iid.FirebaseInstanceId r5) {
        /*
            r4 = this;
        L_0x0000:
            monitor-enter(r4)
            java.lang.String r0 = r4.b()     // Catch:{ all -> 0x0038 }
            r1 = 1
            if (r0 != 0) goto L_0x000d
            com.google.firebase.iid.FirebaseInstanceId.f()     // Catch:{ all -> 0x0038 }
            monitor-exit(r4)     // Catch:{ all -> 0x0038 }
            return r1
        L_0x000d:
            monitor-exit(r4)     // Catch:{ all -> 0x0038 }
            boolean r2 = a(r5, r0)
            if (r2 != 0) goto L_0x0016
            r5 = 0
            return r5
        L_0x0016:
            monitor-enter(r4)
            java.util.Map<java.lang.Integer, com.google.android.gms.tasks.h<java.lang.Void>> r2 = r4.f2767b     // Catch:{ all -> 0x0035 }
            int r3 = r4.f2766a     // Catch:{ all -> 0x0035 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x0035 }
            java.lang.Object r2 = r2.remove(r3)     // Catch:{ all -> 0x0035 }
            com.google.android.gms.tasks.h r2 = (com.google.android.gms.tasks.h) r2     // Catch:{ all -> 0x0035 }
            r4.a(r0)     // Catch:{ all -> 0x0035 }
            int r0 = r4.f2766a     // Catch:{ all -> 0x0035 }
            int r0 = r0 + r1
            r4.f2766a = r0     // Catch:{ all -> 0x0035 }
            monitor-exit(r4)     // Catch:{ all -> 0x0035 }
            if (r2 == 0) goto L_0x0000
            r0 = 0
            r2.a(r0)
            goto L_0x0000
        L_0x0035:
            r5 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0035 }
            throw r5
        L_0x0038:
            r5 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0038 }
            goto L_0x003c
        L_0x003b:
            throw r5
        L_0x003c:
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.iid.ab.a(com.google.firebase.iid.FirebaseInstanceId):boolean");
    }

    private final String b() {
        String a2;
        synchronized (this.c) {
            a2 = this.c.a();
        }
        if (TextUtils.isEmpty(a2)) {
            return null;
        }
        String[] split = a2.split(",");
        if (split.length <= 1 || TextUtils.isEmpty(split[1])) {
            return null;
        }
        return split[1];
    }

    private final synchronized boolean a(String str) {
        synchronized (this.c) {
            String a2 = this.c.a();
            String valueOf = String.valueOf(str);
            if (!a2.startsWith(valueOf.length() != 0 ? ",".concat(valueOf) : new String(","))) {
                return false;
            }
            String valueOf2 = String.valueOf(str);
            this.c.a(a2.substring((valueOf2.length() != 0 ? ",".concat(valueOf2) : new String(",")).length()));
            return true;
        }
    }

    private static boolean a(FirebaseInstanceId firebaseInstanceId, String str) {
        String[] split = str.split("!");
        if (split.length == 2) {
            String str2 = split[0];
            String str3 = split[1];
            char c2 = 65535;
            try {
                int hashCode = str2.hashCode();
                if (hashCode != 83) {
                    if (hashCode == 85) {
                        if (str2.equals("U")) {
                            c2 = 1;
                        }
                    }
                } else if (str2.equals("S")) {
                    c2 = 0;
                }
                if (c2 == 0) {
                    y e = firebaseInstanceId.e();
                    if (!firebaseInstanceId.a(e)) {
                        firebaseInstanceId.a(firebaseInstanceId.e.b(FirebaseInstanceId.d(), e.f2827a, str3));
                        boolean f = FirebaseInstanceId.f();
                    } else {
                        throw new IOException("token not available");
                    }
                } else if (c2 == 1) {
                    y e2 = firebaseInstanceId.e();
                    if (!firebaseInstanceId.a(e2)) {
                        firebaseInstanceId.a(firebaseInstanceId.e.c(FirebaseInstanceId.d(), e2.f2827a, str3));
                        FirebaseInstanceId.f();
                    } else {
                        throw new IOException("token not available");
                    }
                }
            } catch (IOException e3) {
                String valueOf = String.valueOf(e3.getMessage());
                if (valueOf.length() != 0) {
                    "Topic sync failed: ".concat(valueOf);
                } else {
                    new String("Topic sync failed: ");
                }
                return false;
            }
        }
        return true;
    }
}
