package com.google.firebase.iid;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.util.Log;
import com.google.android.gms.tasks.g;
import com.google.android.gms.tasks.j;
import com.google.firebase.a.d;
import com.google.firebase.b;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class FirebaseInstanceId {

    /* renamed from: a  reason: collision with root package name */
    static x f2758a;
    private static final long g = TimeUnit.HOURS.toSeconds(8);
    private static ScheduledThreadPoolExecutor h;

    /* renamed from: b  reason: collision with root package name */
    final Executor f2759b;
    public final b c;
    final o d;
    b e;
    final r f;
    private final ab i;
    private boolean j;
    private final a k;

    public static FirebaseInstanceId a() {
        return getInstance(b.c());
    }

    public static FirebaseInstanceId getInstance(b bVar) {
        return (FirebaseInstanceId) bVar.a(FirebaseInstanceId.class);
    }

    FirebaseInstanceId(b bVar, d dVar) {
        this(bVar, new o(bVar.a()), ai.b(), ai.b(), dVar);
    }

    class a {

        /* renamed from: b  reason: collision with root package name */
        private final boolean f2761b = c();
        private final d c;
        private com.google.firebase.a.b<com.google.firebase.a> d;
        private Boolean e = b();

        a(d dVar) {
            this.c = dVar;
            if (this.e == null && this.f2761b) {
                this.d = new ap(this);
                dVar.a(com.google.firebase.a.class, this.d);
            }
        }

        /* access modifiers changed from: package-private */
        public final synchronized boolean a() {
            if (this.e == null) {
                return this.f2761b && FirebaseInstanceId.this.c.d();
            }
            return this.e.booleanValue();
        }

        private final Boolean b() {
            ApplicationInfo applicationInfo;
            Context a2 = FirebaseInstanceId.this.c.a();
            SharedPreferences sharedPreferences = a2.getSharedPreferences("com.google.firebase.messaging", 0);
            if (sharedPreferences.contains("auto_init")) {
                return Boolean.valueOf(sharedPreferences.getBoolean("auto_init", false));
            }
            try {
                PackageManager packageManager = a2.getPackageManager();
                if (packageManager == null || (applicationInfo = packageManager.getApplicationInfo(a2.getPackageName(), 128)) == null || applicationInfo.metaData == null || !applicationInfo.metaData.containsKey("firebase_messaging_auto_init_enabled")) {
                    return null;
                }
                return Boolean.valueOf(applicationInfo.metaData.getBoolean("firebase_messaging_auto_init_enabled"));
            } catch (PackageManager.NameNotFoundException unused) {
                return null;
            }
        }

        private final boolean c() {
            try {
                Class.forName("com.google.firebase.messaging.a");
                return true;
            } catch (ClassNotFoundException unused) {
                Context a2 = FirebaseInstanceId.this.c.a();
                Intent intent = new Intent("com.google.firebase.MESSAGING_EVENT");
                intent.setPackage(a2.getPackageName());
                ResolveInfo resolveService = a2.getPackageManager().resolveService(intent, 0);
                if (resolveService == null || resolveService.serviceInfo == null) {
                    return false;
                }
                return true;
            }
        }
    }

    private FirebaseInstanceId(b bVar, o oVar, Executor executor, Executor executor2, d dVar) {
        this.j = false;
        if (o.a(bVar) != null) {
            synchronized (FirebaseInstanceId.class) {
                if (f2758a == null) {
                    f2758a = new x(bVar.a());
                }
            }
            this.c = bVar;
            this.d = oVar;
            if (this.e == null) {
                b bVar2 = (b) bVar.a(b.class);
                if (bVar2 == null || !bVar2.a()) {
                    this.e = new aq(bVar, oVar, executor);
                } else {
                    this.e = bVar2;
                }
            }
            this.e = this.e;
            this.f2759b = executor2;
            this.i = new ab(f2758a);
            this.k = new a(dVar);
            this.f = new r(executor);
            if (this.k.a()) {
                b();
                return;
            }
            return;
        }
        throw new IllegalStateException("FirebaseInstanceId failed to initialize, FirebaseApp is missing project ID");
    }

    public final void b() {
        if (a(e()) || this.i.a()) {
            c();
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(boolean z) {
        this.j = z;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void c() {
        if (!this.j) {
            a(0);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* access modifiers changed from: package-private */
    public final synchronized void a(long j2) {
        a(new z(this, this.i, Math.min(Math.max(30L, j2 << 1), g)), j2);
        this.j = true;
    }

    static void a(Runnable runnable, long j2) {
        synchronized (FirebaseInstanceId.class) {
            if (h == null) {
                h = new ScheduledThreadPoolExecutor(1, new com.google.android.gms.common.util.a.b("FirebaseInstanceId"));
            }
            h.schedule(runnable, j2, TimeUnit.SECONDS);
        }
    }

    public static String d() {
        return o.a(f2758a.b("").f2796a);
    }

    public final g<a> a(String str, String str2) {
        return j.a((Object) null).b(this.f2759b, new am(this, str, a(str2)));
    }

    /* access modifiers changed from: package-private */
    public final y e() {
        return b(o.a(this.c), "*");
    }

    static y b(String str, String str2) {
        return f2758a.a("", str, str2);
    }

    /* access modifiers changed from: package-private */
    public final <T> T a(g<T> gVar) throws IOException {
        try {
            return j.a(gVar, 30000, TimeUnit.MILLISECONDS);
        } catch (ExecutionException e2) {
            Throwable cause = e2.getCause();
            if (cause instanceof IOException) {
                if ("INSTANCE_ID_RESET".equals(cause.getMessage())) {
                    g();
                }
                throw ((IOException) cause);
            } else if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else {
                throw new IOException(e2);
            }
        } catch (InterruptedException | TimeoutException unused) {
            throw new IOException("SERVICE_NOT_AVAILABLE");
        }
    }

    static boolean f() {
        if (!Log.isLoggable("FirebaseInstanceId", 3)) {
            return Build.VERSION.SDK_INT == 23 && Log.isLoggable("FirebaseInstanceId", 3);
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void g() {
        f2758a.b();
        if (this.k.a()) {
            c();
        }
    }

    private static String a(String str) {
        return (str.isEmpty() || str.equalsIgnoreCase("fcm") || str.equalsIgnoreCase("gcm")) ? "*" : str;
    }

    /* access modifiers changed from: package-private */
    public final boolean a(y yVar) {
        return yVar == null || yVar.b(this.d.b());
    }
}
