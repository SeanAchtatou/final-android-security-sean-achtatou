package com.google.firebase.iid;

import android.os.Bundle;

public final class l extends m<Void> {
    public l(int i, Bundle bundle) {
        super(i, 2, bundle);
    }

    /* access modifiers changed from: package-private */
    public final boolean a() {
        return true;
    }

    /* access modifiers changed from: package-private */
    public final void a(Bundle bundle) {
        if (bundle.getBoolean("ack", false)) {
            a((Object) null);
        } else {
            a(new zzal(4, "Invalid response to one way request"));
        }
    }
}
