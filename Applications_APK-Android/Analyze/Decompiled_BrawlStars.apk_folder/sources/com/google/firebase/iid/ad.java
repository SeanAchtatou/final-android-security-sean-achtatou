package com.google.firebase.iid;

import android.content.BroadcastReceiver;
import android.content.Intent;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

final class ad {

    /* renamed from: a  reason: collision with root package name */
    final Intent f2770a;

    /* renamed from: b  reason: collision with root package name */
    private final BroadcastReceiver.PendingResult f2771b;
    private boolean c = false;
    private final ScheduledFuture<?> d;

    ad(Intent intent, BroadcastReceiver.PendingResult pendingResult, ScheduledExecutorService scheduledExecutorService) {
        this.f2770a = intent;
        this.f2771b = pendingResult;
        this.d = scheduledExecutorService.schedule(new ae(this, intent), 9000, TimeUnit.MILLISECONDS);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a() {
        if (!this.c) {
            this.f2771b.finish();
            this.d.cancel(false);
            this.c = true;
        }
    }
}
