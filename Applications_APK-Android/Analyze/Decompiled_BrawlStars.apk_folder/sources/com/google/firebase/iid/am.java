package com.google.firebase.iid;

import com.google.android.gms.tasks.a;
import com.google.android.gms.tasks.g;
import com.google.android.gms.tasks.j;

final /* synthetic */ class am implements a {

    /* renamed from: a  reason: collision with root package name */
    private final FirebaseInstanceId f2782a;

    /* renamed from: b  reason: collision with root package name */
    private final String f2783b;
    private final String c;

    am(FirebaseInstanceId firebaseInstanceId, String str, String str2) {
        this.f2782a = firebaseInstanceId;
        this.f2783b = str;
        this.c = str2;
    }

    public final Object a(g gVar) {
        FirebaseInstanceId firebaseInstanceId = this.f2782a;
        String str = this.f2783b;
        String str2 = this.c;
        String d = FirebaseInstanceId.d();
        y b2 = FirebaseInstanceId.b(str, str2);
        if (!firebaseInstanceId.a(b2)) {
            return j.a(new av(d, b2.f2827a));
        }
        return firebaseInstanceId.f.a(str, str2, new an(firebaseInstanceId, d, y.a(b2), str, str2));
    }
}
