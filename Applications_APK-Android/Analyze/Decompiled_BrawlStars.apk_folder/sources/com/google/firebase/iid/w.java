package com.google.firebase.iid;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.util.SimpleArrayMap;
import android.util.Log;
import java.util.ArrayDeque;
import java.util.Queue;

public final class w {
    private static w e;

    /* renamed from: a  reason: collision with root package name */
    Boolean f2822a = null;

    /* renamed from: b  reason: collision with root package name */
    Boolean f2823b = null;
    final Queue<Intent> c = new ArrayDeque();
    public final Queue<Intent> d = new ArrayDeque();
    private final SimpleArrayMap<String, String> f = new SimpleArrayMap<>();

    public static synchronized w a() {
        w wVar;
        synchronized (w.class) {
            if (e == null) {
                e = new w();
            }
            wVar = e;
        }
        return wVar;
    }

    private w() {
    }

    public static PendingIntent a(Context context, int i, Intent intent) {
        return PendingIntent.getBroadcast(context, i, a(context, "com.google.firebase.MESSAGING_EVENT", intent), 1073741824);
    }

    static Intent a(Context context, String str, Intent intent) {
        Intent intent2 = new Intent(context, FirebaseInstanceIdReceiver.class);
        intent2.setAction(str);
        intent2.putExtra("wrapped_intent", intent);
        return intent2;
    }

    public final int b(Context context, String str, Intent intent) {
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            String valueOf = String.valueOf(str);
            if (valueOf.length() != 0) {
                "Starting service: ".concat(valueOf);
            } else {
                new String("Starting service: ");
            }
        }
        char c2 = 65535;
        int hashCode = str.hashCode();
        if (hashCode != -842411455) {
            if (hashCode == 41532704 && str.equals("com.google.firebase.MESSAGING_EVENT")) {
                c2 = 1;
            }
        } else if (str.equals("com.google.firebase.INSTANCE_ID_EVENT")) {
            c2 = 0;
        }
        if (c2 == 0) {
            this.c.offer(intent);
        } else if (c2 != 1) {
            String valueOf2 = String.valueOf(str);
            if (valueOf2.length() != 0) {
                "Unknown service action: ".concat(valueOf2);
                return 500;
            }
            new String("Unknown service action: ");
            return 500;
        } else {
            this.d.offer(intent);
        }
        Intent intent2 = new Intent(str);
        intent2.setPackage(context.getPackageName());
        return a(context, intent2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x00c9 A[Catch:{ SecurityException -> 0x00fb, IllegalStateException -> 0x00d9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00ce A[Catch:{ SecurityException -> 0x00fb, IllegalStateException -> 0x00d9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00d4 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00d7 A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final int a(android.content.Context r5, android.content.Intent r6) {
        /*
            r4 = this;
            android.support.v4.util.SimpleArrayMap<java.lang.String, java.lang.String> r0 = r4.f
            monitor-enter(r0)
            android.support.v4.util.SimpleArrayMap<java.lang.String, java.lang.String> r1 = r4.f     // Catch:{ all -> 0x00fe }
            java.lang.String r2 = r6.getAction()     // Catch:{ all -> 0x00fe }
            java.lang.Object r1 = r1.get(r2)     // Catch:{ all -> 0x00fe }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x00fe }
            monitor-exit(r0)     // Catch:{ all -> 0x00fe }
            if (r1 != 0) goto L_0x009e
            android.content.pm.PackageManager r0 = r5.getPackageManager()
            r1 = 0
            android.content.pm.ResolveInfo r0 = r0.resolveService(r6, r1)
            if (r0 == 0) goto L_0x00c3
            android.content.pm.ServiceInfo r1 = r0.serviceInfo
            if (r1 != 0) goto L_0x0023
            goto L_0x00c3
        L_0x0023:
            android.content.pm.ServiceInfo r0 = r0.serviceInfo
            java.lang.String r1 = r5.getPackageName()
            java.lang.String r2 = r0.packageName
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x006e
            java.lang.String r1 = r0.name
            if (r1 != 0) goto L_0x0036
            goto L_0x006e
        L_0x0036:
            java.lang.String r0 = r0.name
            java.lang.String r1 = "."
            boolean r1 = r0.startsWith(r1)
            if (r1 == 0) goto L_0x005c
            java.lang.String r1 = r5.getPackageName()
            java.lang.String r1 = java.lang.String.valueOf(r1)
            java.lang.String r0 = java.lang.String.valueOf(r0)
            int r2 = r0.length()
            if (r2 == 0) goto L_0x0057
            java.lang.String r0 = r1.concat(r0)
            goto L_0x005c
        L_0x0057:
            java.lang.String r0 = new java.lang.String
            r0.<init>(r1)
        L_0x005c:
            r1 = r0
            android.support.v4.util.SimpleArrayMap<java.lang.String, java.lang.String> r2 = r4.f
            monitor-enter(r2)
            android.support.v4.util.SimpleArrayMap<java.lang.String, java.lang.String> r0 = r4.f     // Catch:{ all -> 0x006b }
            java.lang.String r3 = r6.getAction()     // Catch:{ all -> 0x006b }
            r0.put(r3, r1)     // Catch:{ all -> 0x006b }
            monitor-exit(r2)     // Catch:{ all -> 0x006b }
            goto L_0x009e
        L_0x006b:
            r5 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x006b }
            throw r5
        L_0x006e:
            java.lang.String r1 = r0.packageName
            java.lang.String r0 = r0.name
            java.lang.String r2 = java.lang.String.valueOf(r1)
            int r2 = r2.length()
            int r2 = r2 + 94
            java.lang.String r3 = java.lang.String.valueOf(r0)
            int r3 = r3.length()
            int r2 = r2 + r3
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r2)
            java.lang.String r2 = "Error resolving target intent service, skipping classname enforcement. Resolved service was: "
            r3.append(r2)
            r3.append(r1)
            java.lang.String r1 = "/"
            r3.append(r1)
            r3.append(r0)
            r3.toString()
            goto L_0x00c3
        L_0x009e:
            r0 = 3
            java.lang.String r2 = "FirebaseInstanceId"
            boolean r0 = android.util.Log.isLoggable(r2, r0)
            if (r0 == 0) goto L_0x00bc
            java.lang.String r0 = "Restricting intent to a specific service: "
            java.lang.String r2 = java.lang.String.valueOf(r1)
            int r3 = r2.length()
            if (r3 == 0) goto L_0x00b7
            r0.concat(r2)
            goto L_0x00bc
        L_0x00b7:
            java.lang.String r2 = new java.lang.String
            r2.<init>(r0)
        L_0x00bc:
            java.lang.String r0 = r5.getPackageName()
            r6.setClassName(r0, r1)
        L_0x00c3:
            boolean r0 = r4.a(r5)     // Catch:{ SecurityException -> 0x00fb, IllegalStateException -> 0x00d9 }
            if (r0 == 0) goto L_0x00ce
            android.content.ComponentName r5 = android.support.v4.content.WakefulBroadcastReceiver.startWakefulService(r5, r6)     // Catch:{ SecurityException -> 0x00fb, IllegalStateException -> 0x00d9 }
            goto L_0x00d2
        L_0x00ce:
            android.content.ComponentName r5 = r5.startService(r6)     // Catch:{ SecurityException -> 0x00fb, IllegalStateException -> 0x00d9 }
        L_0x00d2:
            if (r5 != 0) goto L_0x00d7
            r5 = 404(0x194, float:5.66E-43)
            return r5
        L_0x00d7:
            r5 = -1
            return r5
        L_0x00d9:
            r5 = move-exception
            java.lang.String r5 = java.lang.String.valueOf(r5)
            java.lang.String r6 = java.lang.String.valueOf(r5)
            int r6 = r6.length()
            int r6 = r6 + 45
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r6)
            java.lang.String r6 = "Failed to start service while in background: "
            r0.append(r6)
            r0.append(r5)
            r0.toString()
            r5 = 402(0x192, float:5.63E-43)
            return r5
        L_0x00fb:
            r5 = 401(0x191, float:5.62E-43)
            return r5
        L_0x00fe:
            r5 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00fe }
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.iid.w.a(android.content.Context, android.content.Intent):int");
    }

    /* access modifiers changed from: package-private */
    public final boolean a(Context context) {
        if (this.f2822a == null) {
            this.f2822a = Boolean.valueOf(context.checkCallingOrSelfPermission("android.permission.WAKE_LOCK") == 0);
        }
        this.f2822a.booleanValue();
        return this.f2822a.booleanValue();
    }
}
