package com.google.firebase.iid;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import com.google.android.gms.common.util.a.b;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;

public final class ah implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    private final Context f2777a;

    /* renamed from: b  reason: collision with root package name */
    private final Intent f2778b;
    private final ScheduledExecutorService c;
    private final Queue<ad> d;
    private af e;
    private boolean f;

    public ah(Context context, String str) {
        this(context, str, new ScheduledThreadPoolExecutor(0, new b("Firebase-FirebaseInstanceIdServiceConnection")));
    }

    private ah(Context context, String str, ScheduledExecutorService scheduledExecutorService) {
        this.d = new ArrayDeque();
        this.f = false;
        this.f2777a = context.getApplicationContext();
        this.f2778b = new Intent(str).setPackage(this.f2777a.getPackageName());
        this.c = scheduledExecutorService;
    }

    public final synchronized void a(Intent intent, BroadcastReceiver.PendingResult pendingResult) {
        this.d.add(new ad(intent, pendingResult, this.c));
        a();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x008e, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x0088 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final synchronized void a() {
        /*
            r5 = this;
            monitor-enter(r5)
        L_0x0001:
            java.util.Queue<com.google.firebase.iid.ad> r0 = r5.d     // Catch:{ all -> 0x0091 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0091 }
            if (r0 != 0) goto L_0x008f
            com.google.firebase.iid.af r0 = r5.e     // Catch:{ all -> 0x0091 }
            if (r0 == 0) goto L_0x004c
            com.google.firebase.iid.af r0 = r5.e     // Catch:{ all -> 0x0091 }
            boolean r0 = r0.isBinderAlive()     // Catch:{ all -> 0x0091 }
            if (r0 == 0) goto L_0x004c
            java.util.Queue<com.google.firebase.iid.ad> r0 = r5.d     // Catch:{ all -> 0x0091 }
            java.lang.Object r0 = r0.poll()     // Catch:{ all -> 0x0091 }
            com.google.firebase.iid.ad r0 = (com.google.firebase.iid.ad) r0     // Catch:{ all -> 0x0091 }
            com.google.firebase.iid.af r1 = r5.e     // Catch:{ all -> 0x0091 }
            int r2 = android.os.Binder.getCallingUid()     // Catch:{ all -> 0x0091 }
            int r3 = android.os.Process.myUid()     // Catch:{ all -> 0x0091 }
            if (r2 != r3) goto L_0x0044
            com.google.firebase.iid.zzb r2 = r1.f2774a     // Catch:{ all -> 0x0091 }
            android.content.Intent r3 = r0.f2770a     // Catch:{ all -> 0x0091 }
            boolean r2 = r2.c(r3)     // Catch:{ all -> 0x0091 }
            if (r2 == 0) goto L_0x0037
            r0.a()     // Catch:{ all -> 0x0091 }
            goto L_0x0001
        L_0x0037:
            com.google.firebase.iid.zzb r2 = r1.f2774a     // Catch:{ all -> 0x0091 }
            java.util.concurrent.ExecutorService r2 = r2.f2831a     // Catch:{ all -> 0x0091 }
            com.google.firebase.iid.ag r3 = new com.google.firebase.iid.ag     // Catch:{ all -> 0x0091 }
            r3.<init>(r1, r0)     // Catch:{ all -> 0x0091 }
            r2.execute(r3)     // Catch:{ all -> 0x0091 }
            goto L_0x0001
        L_0x0044:
            java.lang.SecurityException r0 = new java.lang.SecurityException     // Catch:{ all -> 0x0091 }
            java.lang.String r1 = "Binding only allowed within app"
            r0.<init>(r1)     // Catch:{ all -> 0x0091 }
            throw r0     // Catch:{ all -> 0x0091 }
        L_0x004c:
            java.lang.String r0 = "EnhancedIntentService"
            r1 = 3
            boolean r0 = android.util.Log.isLoggable(r0, r1)     // Catch:{ all -> 0x0091 }
            r1 = 0
            r2 = 1
            if (r0 == 0) goto L_0x0070
            boolean r0 = r5.f     // Catch:{ all -> 0x0091 }
            if (r0 != 0) goto L_0x005d
            r0 = 1
            goto L_0x005e
        L_0x005d:
            r0 = 0
        L_0x005e:
            r3 = 39
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0091 }
            r4.<init>(r3)     // Catch:{ all -> 0x0091 }
            java.lang.String r3 = "binder is dead. start connection? "
            r4.append(r3)     // Catch:{ all -> 0x0091 }
            r4.append(r0)     // Catch:{ all -> 0x0091 }
            r4.toString()     // Catch:{ all -> 0x0091 }
        L_0x0070:
            boolean r0 = r5.f     // Catch:{ all -> 0x0091 }
            if (r0 != 0) goto L_0x008d
            r5.f = r2     // Catch:{ all -> 0x0091 }
            com.google.android.gms.common.stats.a r0 = com.google.android.gms.common.stats.a.a()     // Catch:{ SecurityException -> 0x0088 }
            android.content.Context r2 = r5.f2777a     // Catch:{ SecurityException -> 0x0088 }
            android.content.Intent r3 = r5.f2778b     // Catch:{ SecurityException -> 0x0088 }
            r4 = 65
            boolean r0 = r0.b(r2, r3, r5, r4)     // Catch:{ SecurityException -> 0x0088 }
            if (r0 == 0) goto L_0x0088
            monitor-exit(r5)
            return
        L_0x0088:
            r5.f = r1     // Catch:{ all -> 0x0091 }
            r5.b()     // Catch:{ all -> 0x0091 }
        L_0x008d:
            monitor-exit(r5)
            return
        L_0x008f:
            monitor-exit(r5)
            return
        L_0x0091:
            r0 = move-exception
            monitor-exit(r5)
            goto L_0x0095
        L_0x0094:
            throw r0
        L_0x0095:
            goto L_0x0094
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.iid.ah.a():void");
    }

    private final void b() {
        while (!this.d.isEmpty()) {
            this.d.poll().a();
        }
    }

    public final synchronized void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            String valueOf = String.valueOf(componentName);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 20);
            sb.append("onServiceConnected: ");
            sb.append(valueOf);
            sb.toString();
        }
        this.f = false;
        if (!(iBinder instanceof af)) {
            String valueOf2 = String.valueOf(iBinder);
            StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 28);
            sb2.append("Invalid service connection: ");
            sb2.append(valueOf2);
            sb2.toString();
            b();
            return;
        }
        this.e = (af) iBinder;
        a();
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            String valueOf = String.valueOf(componentName);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 23);
            sb.append("onServiceDisconnected: ");
            sb.append(valueOf);
            sb.toString();
        }
        a();
    }
}
