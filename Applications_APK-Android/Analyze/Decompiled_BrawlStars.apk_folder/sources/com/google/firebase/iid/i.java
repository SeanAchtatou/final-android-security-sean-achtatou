package com.google.firebase.iid;

final /* synthetic */ class i implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final f f2804a;

    i(f fVar) {
        this.f2804a = fVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003e, code lost:
        if (android.util.Log.isLoggable("MessengerIpcClient", 3) == false) goto L_0x005e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0040, code lost:
        r3 = java.lang.String.valueOf(r1);
        r5 = new java.lang.StringBuilder(java.lang.String.valueOf(r3).length() + 8);
        r5.append("Sending ");
        r5.append(r3);
        r5.toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x005e, code lost:
        r3 = r0.f.f2798a;
        r4 = r0.f2801b;
        r5 = android.os.Message.obtain();
        r5.what = r1.c;
        r5.arg1 = r1.f2809a;
        r5.replyTo = r4;
        r4 = new android.os.Bundle();
        r4.putBoolean("oneWay", r1.a());
        r4.putString("pkg", r3.getPackageName());
        r4.putBundle(com.facebook.share.internal.ShareConstants.WEB_DIALOG_PARAM_DATA, r1.d);
        r5.setData(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r1 = r0.c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0097, code lost:
        if (r1.f2807a == null) goto L_0x00a0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0099, code lost:
        r1.f2807a.send(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00a2, code lost:
        if (r1.f2808b == null) goto L_0x00ab;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00a4, code lost:
        r1.f2808b.a(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00b2, code lost:
        throw new java.lang.IllegalStateException("Both messengers are null");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00b3, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00b4, code lost:
        r0.a(2, r1.getMessage());
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r8 = this;
            com.google.firebase.iid.f r0 = r8.f2804a
        L_0x0002:
            monitor-enter(r0)
            int r1 = r0.f2800a     // Catch:{ all -> 0x00bd }
            r2 = 2
            if (r1 == r2) goto L_0x000a
            monitor-exit(r0)     // Catch:{ all -> 0x00bd }
            return
        L_0x000a:
            java.util.Queue<com.google.firebase.iid.m<?>> r1 = r0.d     // Catch:{ all -> 0x00bd }
            boolean r1 = r1.isEmpty()     // Catch:{ all -> 0x00bd }
            if (r1 == 0) goto L_0x0017
            r0.a()     // Catch:{ all -> 0x00bd }
            monitor-exit(r0)     // Catch:{ all -> 0x00bd }
            return
        L_0x0017:
            java.util.Queue<com.google.firebase.iid.m<?>> r1 = r0.d     // Catch:{ all -> 0x00bd }
            java.lang.Object r1 = r1.poll()     // Catch:{ all -> 0x00bd }
            com.google.firebase.iid.m r1 = (com.google.firebase.iid.m) r1     // Catch:{ all -> 0x00bd }
            android.util.SparseArray<com.google.firebase.iid.m<?>> r3 = r0.e     // Catch:{ all -> 0x00bd }
            int r4 = r1.f2809a     // Catch:{ all -> 0x00bd }
            r3.put(r4, r1)     // Catch:{ all -> 0x00bd }
            com.google.firebase.iid.e r3 = r0.f     // Catch:{ all -> 0x00bd }
            java.util.concurrent.ScheduledExecutorService r3 = r3.f2799b     // Catch:{ all -> 0x00bd }
            com.google.firebase.iid.j r4 = new com.google.firebase.iid.j     // Catch:{ all -> 0x00bd }
            r4.<init>(r0, r1)     // Catch:{ all -> 0x00bd }
            r5 = 30
            java.util.concurrent.TimeUnit r7 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ all -> 0x00bd }
            r3.schedule(r4, r5, r7)     // Catch:{ all -> 0x00bd }
            monitor-exit(r0)     // Catch:{ all -> 0x00bd }
            r3 = 3
            java.lang.String r4 = "MessengerIpcClient"
            boolean r3 = android.util.Log.isLoggable(r4, r3)
            if (r3 == 0) goto L_0x005e
            java.lang.String r3 = java.lang.String.valueOf(r1)
            java.lang.String r4 = java.lang.String.valueOf(r3)
            int r4 = r4.length()
            int r4 = r4 + 8
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>(r4)
            java.lang.String r4 = "Sending "
            r5.append(r4)
            r5.append(r3)
            r5.toString()
        L_0x005e:
            com.google.firebase.iid.e r3 = r0.f
            android.content.Context r3 = r3.f2798a
            android.os.Messenger r4 = r0.f2801b
            android.os.Message r5 = android.os.Message.obtain()
            int r6 = r1.c
            r5.what = r6
            int r6 = r1.f2809a
            r5.arg1 = r6
            r5.replyTo = r4
            android.os.Bundle r4 = new android.os.Bundle
            r4.<init>()
            boolean r6 = r1.a()
            java.lang.String r7 = "oneWay"
            r4.putBoolean(r7, r6)
            java.lang.String r3 = r3.getPackageName()
            java.lang.String r6 = "pkg"
            r4.putString(r6, r3)
            android.os.Bundle r1 = r1.d
            java.lang.String r3 = "data"
            r4.putBundle(r3, r1)
            r5.setData(r4)
            com.google.firebase.iid.k r1 = r0.c     // Catch:{ RemoteException -> 0x00b3 }
            android.os.Messenger r3 = r1.f2807a     // Catch:{ RemoteException -> 0x00b3 }
            if (r3 == 0) goto L_0x00a0
            android.os.Messenger r1 = r1.f2807a     // Catch:{ RemoteException -> 0x00b3 }
            r1.send(r5)     // Catch:{ RemoteException -> 0x00b3 }
            goto L_0x0002
        L_0x00a0:
            com.google.firebase.iid.zzl r3 = r1.f2808b     // Catch:{ RemoteException -> 0x00b3 }
            if (r3 == 0) goto L_0x00ab
            com.google.firebase.iid.zzl r1 = r1.f2808b     // Catch:{ RemoteException -> 0x00b3 }
            r1.a(r5)     // Catch:{ RemoteException -> 0x00b3 }
            goto L_0x0002
        L_0x00ab:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ RemoteException -> 0x00b3 }
            java.lang.String r3 = "Both messengers are null"
            r1.<init>(r3)     // Catch:{ RemoteException -> 0x00b3 }
            throw r1     // Catch:{ RemoteException -> 0x00b3 }
        L_0x00b3:
            r1 = move-exception
            java.lang.String r1 = r1.getMessage()
            r0.a(r2, r1)
            goto L_0x0002
        L_0x00bd:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00bd }
            goto L_0x00c1
        L_0x00c0:
            throw r1
        L_0x00c1:
            goto L_0x00c0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.iid.i.run():void");
    }
}
