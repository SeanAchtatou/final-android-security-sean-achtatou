package com.google.firebase.iid;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import javax.annotation.Nullable;

final class aa extends BroadcastReceiver {
    @Nullable

    /* renamed from: a  reason: collision with root package name */
    z f2765a;

    public aa(z zVar) {
        this.f2765a = zVar;
    }

    public final void onReceive(Context context, Intent intent) {
        z zVar = this.f2765a;
        if (zVar != null && zVar.b()) {
            FirebaseInstanceId.f();
            FirebaseInstanceId.a(this.f2765a, 0);
            this.f2765a.a().unregisterReceiver(this);
            this.f2765a = null;
        }
    }
}
