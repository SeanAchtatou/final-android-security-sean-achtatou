package com.google.firebase.iid;

import android.os.Handler;
import android.os.Message;

final /* synthetic */ class g implements Handler.Callback {

    /* renamed from: a  reason: collision with root package name */
    private final f f2802a;

    g(f fVar) {
        this.f2802a = fVar;
    }

    public final boolean handleMessage(Message message) {
        return this.f2802a.a(message);
    }
}
