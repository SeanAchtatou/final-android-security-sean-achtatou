package com.google.firebase.iid;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.content.WakefulBroadcastReceiver;
import com.google.android.gms.internal.a.a;
import com.google.android.gms.internal.a.b;
import java.util.concurrent.ExecutorService;

public abstract class zzb extends Service {

    /* renamed from: a  reason: collision with root package name */
    final ExecutorService f2831a;

    /* renamed from: b  reason: collision with root package name */
    private Binder f2832b;
    private final Object c;
    private int d;
    private int e;

    public zzb() {
        a a2 = b.a();
        String valueOf = String.valueOf(getClass().getSimpleName());
        this.f2831a = a2.a(new com.google.android.gms.common.util.a.b(valueOf.length() != 0 ? "Firebase-".concat(valueOf) : new String("Firebase-")));
        this.c = new Object();
        this.e = 0;
    }

    /* access modifiers changed from: protected */
    public Intent a(Intent intent) {
        return intent;
    }

    public abstract void b(Intent intent);

    public boolean c(Intent intent) {
        return false;
    }

    public final synchronized IBinder onBind(Intent intent) {
        if (this.f2832b == null) {
            this.f2832b = new af(this);
        }
        return this.f2832b;
    }

    public final int onStartCommand(Intent intent, int i, int i2) {
        synchronized (this.c) {
            this.d = i2;
            this.e++;
        }
        Intent a2 = a(intent);
        if (a2 == null) {
            d(intent);
            return 2;
        } else if (c(a2)) {
            d(intent);
            return 2;
        } else {
            this.f2831a.execute(new ac(this, a2, intent));
            return 3;
        }
    }

    /* access modifiers changed from: private */
    public final void d(Intent intent) {
        if (intent != null) {
            WakefulBroadcastReceiver.completeWakefulIntent(intent);
        }
        synchronized (this.c) {
            this.e--;
            if (this.e == 0) {
                stopSelfResult(this.d);
            }
        }
    }
}
