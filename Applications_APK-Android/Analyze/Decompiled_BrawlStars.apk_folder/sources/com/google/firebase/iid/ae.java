package com.google.firebase.iid;

import android.content.Intent;

final /* synthetic */ class ae implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final ad f2772a;

    /* renamed from: b  reason: collision with root package name */
    private final Intent f2773b;

    ae(ad adVar, Intent intent) {
        this.f2772a = adVar;
        this.f2773b = intent;
    }

    public final void run() {
        ad adVar = this.f2772a;
        String action = this.f2773b.getAction();
        StringBuilder sb = new StringBuilder(String.valueOf(action).length() + 61);
        sb.append("Service took too long to process intent: ");
        sb.append(action);
        sb.append(" App may get closed.");
        sb.toString();
        adVar.a();
    }
}
