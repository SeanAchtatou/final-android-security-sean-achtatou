package com.google.firebase.iid;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.Map;

final class x {

    /* renamed from: a  reason: collision with root package name */
    private final SharedPreferences f2824a;

    /* renamed from: b  reason: collision with root package name */
    private final Context f2825b;
    private final aw c;
    private final Map<String, ax> d;

    public x(Context context) {
        this(context, new aw());
    }

    private x(Context context, aw awVar) {
        this.d = new ArrayMap();
        this.f2825b = context;
        this.f2824a = context.getSharedPreferences("com.google.android.gms.appid", 0);
        this.c = awVar;
        File file = new File(ContextCompat.getNoBackupFilesDir(this.f2825b), "com.google.android.gms.appid-no-backup");
        if (!file.exists()) {
            try {
                if (file.createNewFile() && !c()) {
                    b();
                    FirebaseInstanceId.a().g();
                }
            } catch (IOException e) {
                if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    String valueOf = String.valueOf(e.getMessage());
                    if (valueOf.length() != 0) {
                        "Error creating file in no backup dir: ".concat(valueOf);
                    } else {
                        new String("Error creating file in no backup dir: ");
                    }
                }
            }
        }
    }

    public final synchronized String a() {
        return this.f2824a.getString("topic_operaion_queue", "");
    }

    public final synchronized void a(String str) {
        this.f2824a.edit().putString("topic_operaion_queue", str).apply();
    }

    private final synchronized boolean c() {
        return this.f2824a.getAll().isEmpty();
    }

    private static String b(String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 4 + String.valueOf(str2).length() + String.valueOf(str3).length());
        sb.append(str);
        sb.append("|T|");
        sb.append(str2);
        sb.append("|");
        sb.append(str3);
        return sb.toString();
    }

    static String a(String str, String str2) {
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 3 + String.valueOf(str2).length());
        sb.append(str);
        sb.append("|S|");
        sb.append(str2);
        return sb.toString();
    }

    public final synchronized void b() {
        this.d.clear();
        for (File file : aw.a(this.f2825b).listFiles()) {
            if (file.getName().startsWith("com.google.InstanceId")) {
                file.delete();
            }
        }
        this.f2824a.edit().clear().commit();
    }

    public final synchronized y a(String str, String str2, String str3) {
        return y.a(this.f2824a.getString(b(str, str2, str3), null));
    }

    public final synchronized void a(String str, String str2, String str3, String str4, String str5) {
        String a2 = y.a(str4, str5, System.currentTimeMillis());
        if (a2 != null) {
            SharedPreferences.Editor edit = this.f2824a.edit();
            edit.putString(b(str, str2, str3), a2);
            edit.commit();
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:11|12) */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        com.google.firebase.iid.FirebaseInstanceId.a().g();
        r2 = r3.c.a(r3.f2825b, r4);
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x001d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized com.google.firebase.iid.ax b(java.lang.String r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            java.util.Map<java.lang.String, com.google.firebase.iid.ax> r0 = r3.d     // Catch:{ all -> 0x0033 }
            java.lang.Object r0 = r0.get(r4)     // Catch:{ all -> 0x0033 }
            com.google.firebase.iid.ax r0 = (com.google.firebase.iid.ax) r0     // Catch:{ all -> 0x0033 }
            if (r0 == 0) goto L_0x000d
            monitor-exit(r3)
            return r0
        L_0x000d:
            com.google.firebase.iid.aw r0 = r3.c     // Catch:{ d -> 0x001d }
            android.content.Context r1 = r3.f2825b     // Catch:{ d -> 0x001d }
            com.google.firebase.iid.ax r2 = r0.b(r1, r4)     // Catch:{ d -> 0x001d }
            if (r2 == 0) goto L_0x0018
            goto L_0x002c
        L_0x0018:
            com.google.firebase.iid.ax r2 = r0.a(r1, r4)     // Catch:{ d -> 0x001d }
            goto L_0x002c
        L_0x001d:
            com.google.firebase.iid.FirebaseInstanceId r0 = com.google.firebase.iid.FirebaseInstanceId.a()     // Catch:{ all -> 0x0033 }
            r0.g()     // Catch:{ all -> 0x0033 }
            com.google.firebase.iid.aw r0 = r3.c     // Catch:{ all -> 0x0033 }
            android.content.Context r1 = r3.f2825b     // Catch:{ all -> 0x0033 }
            com.google.firebase.iid.ax r2 = r0.a(r1, r4)     // Catch:{ all -> 0x0033 }
        L_0x002c:
            java.util.Map<java.lang.String, com.google.firebase.iid.ax> r0 = r3.d     // Catch:{ all -> 0x0033 }
            r0.put(r4, r2)     // Catch:{ all -> 0x0033 }
            monitor-exit(r3)
            return r2
        L_0x0033:
            r4 = move-exception
            monitor-exit(r3)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.iid.x.b(java.lang.String):com.google.firebase.iid.ax");
    }

    public final synchronized void c(String str) {
        String concat = String.valueOf(str).concat("|T|");
        SharedPreferences.Editor edit = this.f2824a.edit();
        for (String next : this.f2824a.getAll().keySet()) {
            if (next.startsWith(concat)) {
                edit.remove(next);
            }
        }
        edit.commit();
    }
}
