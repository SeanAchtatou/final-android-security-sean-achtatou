package com.google.firebase.iid;

import com.google.android.gms.tasks.f;
import com.google.android.gms.tasks.g;
import com.google.android.gms.tasks.j;

final /* synthetic */ class ao implements f {

    /* renamed from: a  reason: collision with root package name */
    private final FirebaseInstanceId f2786a;

    /* renamed from: b  reason: collision with root package name */
    private final String f2787b;
    private final String c;
    private final String d;

    ao(FirebaseInstanceId firebaseInstanceId, String str, String str2, String str3) {
        this.f2786a = firebaseInstanceId;
        this.f2787b = str;
        this.c = str2;
        this.d = str3;
    }

    public final g a(Object obj) {
        FirebaseInstanceId firebaseInstanceId = this.f2786a;
        String str = this.f2787b;
        String str2 = this.c;
        String str3 = this.d;
        String str4 = (String) obj;
        FirebaseInstanceId.f2758a.a("", str, str2, str4, firebaseInstanceId.d.b());
        return j.a(new av(str3, str4));
    }
}
