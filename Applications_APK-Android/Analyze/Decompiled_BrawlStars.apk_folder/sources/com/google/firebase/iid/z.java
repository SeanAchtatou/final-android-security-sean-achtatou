package com.google.firebase.iid;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Looper;
import android.os.PowerManager;
import java.io.IOException;

final class z implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final long f2828a;

    /* renamed from: b  reason: collision with root package name */
    private final PowerManager.WakeLock f2829b = ((PowerManager) a().getSystemService("power")).newWakeLock(1, "fiid-sync");
    private final FirebaseInstanceId c;
    private final ab d;

    z(FirebaseInstanceId firebaseInstanceId, ab abVar, long j) {
        this.c = firebaseInstanceId;
        this.d = abVar;
        this.f2828a = j;
        this.f2829b.setReferenceCounted(false);
    }

    public final void run() {
        boolean a2;
        try {
            if (w.a().a(a())) {
                this.f2829b.acquire();
            }
            boolean z = true;
            this.c.a(true);
            if (!this.c.e.a()) {
                this.c.a(false);
                if (!a2) {
                    return;
                }
                return;
            }
            w a3 = w.a();
            Context a4 = a();
            if (a3.f2823b == null) {
                if (a4.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != 0) {
                    z = false;
                }
                a3.f2823b = Boolean.valueOf(z);
            }
            a3.f2822a.booleanValue();
            if (!a3.f2823b.booleanValue() || b()) {
                if (!c() || !this.d.a(this.c)) {
                    this.c.a(this.f2828a);
                } else {
                    this.c.a(false);
                }
                if (w.a().a(a())) {
                    this.f2829b.release();
                    return;
                }
                return;
            }
            aa aaVar = new aa(this);
            FirebaseInstanceId.f();
            aaVar.f2765a.a().registerReceiver(aaVar, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            if (w.a().a(a())) {
                this.f2829b.release();
            }
        } finally {
            if (w.a().a(a())) {
                this.f2829b.release();
            }
        }
    }

    private final boolean c() {
        y e = this.c.e();
        if (!this.c.a(e)) {
            return true;
        }
        try {
            FirebaseInstanceId firebaseInstanceId = this.c;
            String a2 = o.a(firebaseInstanceId.c);
            if (Looper.getMainLooper() != Looper.myLooper()) {
                String a3 = ((a) firebaseInstanceId.a(firebaseInstanceId.a(a2, "*"))).a();
                if (a3 == null) {
                    return false;
                }
                if (e == null || (e != null && !a3.equals(e.f2827a))) {
                    Context a4 = a();
                    Intent intent = new Intent("com.google.firebase.messaging.NEW_TOKEN");
                    intent.putExtra("token", a3);
                    a4.sendBroadcast(w.a(a4, "com.google.firebase.MESSAGING_EVENT", intent));
                    a4.sendBroadcast(w.a(a4, "com.google.firebase.INSTANCE_ID_EVENT", new Intent("com.google.firebase.iid.TOKEN_REFRESH")));
                }
                return true;
            }
            throw new IOException("MAIN_THREAD");
        } catch (IOException | SecurityException e2) {
            String valueOf = String.valueOf(e2.getMessage());
            if (valueOf.length() != 0) {
                "Token retrieval failed: ".concat(valueOf);
            } else {
                new String("Token retrieval failed: ");
            }
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public final Context a() {
        return this.c.c.a();
    }

    /* access modifiers changed from: package-private */
    public final boolean b() {
        ConnectivityManager connectivityManager = (ConnectivityManager) a().getSystemService("connectivity");
        NetworkInfo activeNetworkInfo = connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
