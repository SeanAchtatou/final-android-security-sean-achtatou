package com.google.firebase.components;

import com.google.firebase.b.a;

/* compiled from: com.google.firebase:firebase-common@@16.0.2 */
abstract class g implements c {
    g() {
    }

    public <T> T a(Class cls) {
        a b2 = b(cls);
        if (b2 == null) {
            return null;
        }
        return b2.a();
    }
}
