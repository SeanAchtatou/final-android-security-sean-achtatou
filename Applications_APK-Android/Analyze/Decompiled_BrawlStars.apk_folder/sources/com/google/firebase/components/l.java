package com.google.firebase.components;

import java.util.HashSet;
import java.util.Set;

/* compiled from: com.google.firebase:firebase-common@@16.0.2 */
final class l {

    /* renamed from: a  reason: collision with root package name */
    final a<?> f2744a;

    /* renamed from: b  reason: collision with root package name */
    final Set<l> f2745b = new HashSet();
    final Set<l> c = new HashSet();

    l(a<?> aVar) {
        this.f2744a = aVar;
    }

    /* access modifiers changed from: package-private */
    public final boolean a() {
        return this.c.isEmpty();
    }
}
