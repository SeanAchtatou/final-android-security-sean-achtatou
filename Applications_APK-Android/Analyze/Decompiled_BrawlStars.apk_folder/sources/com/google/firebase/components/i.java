package com.google.firebase.components;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: com.google.firebase:firebase-common@@16.0.2 */
public final class i implements j<Context> {
    private i() {
    }

    public /* synthetic */ i(byte b2) {
        this();
    }

    public final /* synthetic */ List a(Object obj) {
        Bundle a2 = a((Context) obj);
        if (a2 == null) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        for (String next : a2.keySet()) {
            if ("com.google.firebase.components.ComponentRegistrar".equals(a2.get(next)) && next.startsWith("com.google.firebase.components:")) {
                arrayList.add(next.substring(31));
            }
        }
        return arrayList;
    }

    private static Bundle a(Context context) {
        ServiceInfo serviceInfo;
        try {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager == null || (serviceInfo = packageManager.getServiceInfo(new ComponentName(context, ComponentDiscoveryService.class), 128)) == null) {
                return null;
            }
            return serviceInfo.metaData;
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }
}
