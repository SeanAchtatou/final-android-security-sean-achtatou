package com.google.firebase.components;

import com.google.android.gms.common.internal.l;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* compiled from: com.google.firebase:firebase-common@@16.0.2 */
public final class a<T> {

    /* renamed from: a  reason: collision with root package name */
    final Set<Class<? super T>> f2733a;

    /* renamed from: b  reason: collision with root package name */
    final Set<f> f2734b;
    final d<T> c;
    final Set<Class<?>> d;
    private final int e;

    /* synthetic */ a(Set set, Set set2, int i, d dVar, Set set3, byte b2) {
        this(set, set2, i, dVar, set3);
    }

    private a(Set<Class<? super T>> set, Set<f> set2, int i, d<T> dVar, Set<Class<?>> set3) {
        this.f2733a = Collections.unmodifiableSet(set);
        this.f2734b = Collections.unmodifiableSet(set2);
        this.e = i;
        this.c = dVar;
        this.d = Collections.unmodifiableSet(set3);
    }

    public final boolean a() {
        return this.e == 1;
    }

    public final boolean b() {
        return this.e == 2;
    }

    public final String toString() {
        return "Component<" + Arrays.toString(this.f2733a.toArray()) + ">{" + this.e + ", deps=" + Arrays.toString(this.f2734b.toArray()) + "}";
    }

    public static <T> C0126a<T> a(Class<T> cls) {
        return new C0126a<>(cls, new Class[0], (byte) 0);
    }

    private static <T> C0126a<T> a(Class<T> cls, Class<? super T>... clsArr) {
        return new C0126a<>(cls, clsArr, (byte) 0);
    }

    @SafeVarargs
    public static <T> a<T> a(T t, Class<T> cls, Class<? super T>... clsArr) {
        return a(cls, clsArr).a(h.a((Object) t)).a();
    }

    /* renamed from: com.google.firebase.components.a$a  reason: collision with other inner class name */
    /* compiled from: com.google.firebase:firebase-common@@16.0.2 */
    public static class C0126a<T> {

        /* renamed from: a  reason: collision with root package name */
        private final Set<Class<? super T>> f2735a;

        /* renamed from: b  reason: collision with root package name */
        private final Set<f> f2736b;
        private int c;
        private d<T> d;
        private Set<Class<?>> e;

        /* synthetic */ C0126a(Class cls, Class[] clsArr, byte b2) {
            this(cls, clsArr);
        }

        private C0126a(Class<T> cls, Class<? super T>... clsArr) {
            this.f2735a = new HashSet();
            this.f2736b = new HashSet();
            this.c = 0;
            this.e = new HashSet();
            l.a(cls, "Null interface");
            this.f2735a.add(cls);
            for (Class<? super T> a2 : clsArr) {
                l.a(a2, "Null interface");
            }
            Collections.addAll(this.f2735a, clsArr);
        }

        public final C0126a<T> a(f fVar) {
            l.a(fVar, "Null dependency");
            l.b(!this.f2735a.contains(fVar.f2739a), "Components are not allowed to depend on interfaces they themselves provide.");
            this.f2736b.add(fVar);
            return this;
        }

        public C0126a<T> a(int i) {
            l.a(this.c == 0, "Instantiation type has already been set.");
            this.c = i;
            return this;
        }

        public final C0126a<T> a(d dVar) {
            this.d = (d) l.a(dVar, "Null factory");
            return this;
        }

        public final a<T> a() {
            l.a(this.d != null, "Missing required property: factory.");
            return new a(new HashSet(this.f2735a), new HashSet(this.f2736b), this.c, this.d, this.e, (byte) 0);
        }
    }
}
