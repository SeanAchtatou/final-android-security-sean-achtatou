package com.google.firebase.components;

import com.google.firebase.a.c;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* compiled from: com.google.firebase:firebase-common@@16.0.2 */
final class q extends g {

    /* renamed from: a  reason: collision with root package name */
    private final Set<Class<?>> f2754a;

    /* renamed from: b  reason: collision with root package name */
    private final Set<Class<?>> f2755b;
    private final Set<Class<?>> c;
    private final c d;

    q(a<?> aVar, c cVar) {
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet();
        for (f next : aVar.f2734b) {
            if (next.a()) {
                hashSet.add(next.f2739a);
            } else {
                hashSet2.add(next.f2739a);
            }
        }
        if (!aVar.d.isEmpty()) {
            hashSet.add(c.class);
        }
        this.f2754a = Collections.unmodifiableSet(hashSet);
        this.f2755b = Collections.unmodifiableSet(hashSet2);
        this.c = aVar.d;
        this.d = cVar;
    }

    public final <T> T a(Class<T> cls) {
        if (this.f2754a.contains(cls)) {
            T a2 = this.d.a(cls);
            if (!cls.equals(c.class)) {
                return a2;
            }
            return new a(this.c, (c) a2);
        }
        throw new IllegalArgumentException(String.format("Requesting %s is not allowed.", cls));
    }

    public final <T> com.google.firebase.b.a<T> b(Class<T> cls) {
        if (this.f2755b.contains(cls)) {
            return this.d.b(cls);
        }
        throw new IllegalArgumentException(String.format("Requesting Provider<%s> is not allowed.", cls));
    }

    /* compiled from: com.google.firebase:firebase-common@@16.0.2 */
    static class a implements c {

        /* renamed from: a  reason: collision with root package name */
        private final Set<Class<?>> f2756a;

        /* renamed from: b  reason: collision with root package name */
        private final c f2757b;

        public a(Set<Class<?>> set, c cVar) {
            this.f2756a = set;
            this.f2757b = cVar;
        }
    }
}
