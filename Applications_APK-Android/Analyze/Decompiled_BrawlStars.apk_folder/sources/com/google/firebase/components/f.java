package com.google.firebase.components;

import com.google.android.gms.common.internal.l;

/* compiled from: com.google.firebase:firebase-common@@16.0.2 */
public final class f {

    /* renamed from: a  reason: collision with root package name */
    final Class<?> f2739a;

    /* renamed from: b  reason: collision with root package name */
    final int f2740b = 1;
    private final int c = 0;

    private f(Class<?> cls, int i, int i2) {
        this.f2739a = (Class) l.a(cls, "Null dependency anInterface.");
    }

    public static f a(Class<?> cls) {
        return new f(cls, 1, 0);
    }

    public final boolean a() {
        return this.c == 0;
    }

    public final boolean equals(Object obj) {
        if (obj instanceof f) {
            f fVar = (f) obj;
            if (this.f2739a == fVar.f2739a && this.f2740b == fVar.f2740b && this.c == fVar.c) {
                return true;
            }
            return false;
        }
        return false;
    }

    public final int hashCode() {
        return ((((this.f2739a.hashCode() ^ 1000003) * 1000003) ^ this.f2740b) * 1000003) ^ this.c;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("Dependency{anInterface=");
        sb.append(this.f2739a);
        sb.append(", required=");
        boolean z = false;
        sb.append(this.f2740b == 1);
        sb.append(", direct=");
        if (this.c == 0) {
            z = true;
        }
        sb.append(z);
        sb.append("}");
        return sb.toString();
    }
}
