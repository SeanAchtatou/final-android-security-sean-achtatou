package com.google.firebase.components;

import com.google.firebase.b.a;

/* compiled from: com.google.firebase:firebase-common@@16.0.2 */
final class o<T> implements a<T> {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f2750a = new Object();

    /* renamed from: b  reason: collision with root package name */
    private volatile Object f2751b = f2750a;
    private volatile a<T> c;

    o(d<T> dVar, c cVar) {
        this.c = new p(dVar, cVar);
    }

    public final T a() {
        T t = this.f2751b;
        if (t == f2750a) {
            synchronized (this) {
                t = this.f2751b;
                if (t == f2750a) {
                    t = this.c.a();
                    this.f2751b = t;
                    this.c = null;
                }
            }
        }
        return t;
    }
}
