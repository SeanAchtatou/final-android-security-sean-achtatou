package com.google.firebase.components;

import com.google.firebase.a.a;
import com.google.firebase.a.b;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-common@@16.0.2 */
final /* synthetic */ class n implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final Map.Entry f2748a;

    /* renamed from: b  reason: collision with root package name */
    private final a f2749b;

    private n(Map.Entry entry, a aVar) {
        this.f2748a = entry;
        this.f2749b = aVar;
    }

    public static Runnable a(Map.Entry entry, a aVar) {
        return new n(entry, aVar);
    }

    public final void run() {
        ((b) this.f2748a.getKey()).a(this.f2749b);
    }
}
