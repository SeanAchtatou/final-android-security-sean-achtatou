package com.google.firebase.components;

import java.util.Arrays;
import java.util.List;

/* compiled from: com.google.firebase:firebase-common@@16.0.2 */
public class DependencyCycleException extends DependencyException {

    /* renamed from: a  reason: collision with root package name */
    private final List<a<?>> f2732a;

    public DependencyCycleException(List<a<?>> list) {
        super("Dependency cycle detected: " + Arrays.toString(list.toArray()));
        this.f2732a = list;
    }
}
