package com.google.firebase.components;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* compiled from: com.google.firebase:firebase-common@@16.0.2 */
public class b<T> {

    /* renamed from: a  reason: collision with root package name */
    public final T f2737a;

    /* renamed from: b  reason: collision with root package name */
    public final j<T> f2738b;

    public b(T t, j<T> jVar) {
        this.f2737a = t;
        this.f2738b = jVar;
    }

    public static List<e> a(List<String> list) {
        ArrayList arrayList = new ArrayList();
        for (String next : list) {
            try {
                Class<?> cls = Class.forName(next);
                if (!e.class.isAssignableFrom(cls)) {
                    String.format("Class %s is not an instance of %s", next, "com.google.firebase.components.ComponentRegistrar");
                } else {
                    arrayList.add((e) cls.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]));
                }
            } catch (ClassNotFoundException unused) {
                String.format("Class %s is not an found.", next);
            } catch (IllegalAccessException unused2) {
                String.format("Could not instantiate %s.", next);
            } catch (InstantiationException unused3) {
                String.format("Could not instantiate %s.", next);
            } catch (NoSuchMethodException unused4) {
                String.format("Could not instantiate %s", next);
            } catch (InvocationTargetException unused5) {
                String.format("Could not instantiate %s", next);
            }
        }
        return arrayList;
    }

    static List<a<?>> b(List<a<?>> list) {
        l lVar;
        HashMap hashMap = new HashMap(list.size());
        for (a next : list) {
            l lVar2 = new l(next);
            Iterator<Class<? super T>> it = next.f2733a.iterator();
            while (true) {
                if (it.hasNext()) {
                    Class next2 = it.next();
                    if (hashMap.put(next2, lVar2) != null) {
                        throw new IllegalArgumentException(String.format("Multiple components provide %s.", next2));
                    }
                }
            }
        }
        for (l lVar3 : hashMap.values()) {
            for (f next3 : lVar3.f2744a.f2734b) {
                if (next3.a() && (lVar = (l) hashMap.get(next3.f2739a)) != null) {
                    lVar3.f2745b.add(lVar);
                    lVar.c.add(lVar3);
                }
            }
        }
        HashSet<l> hashSet = new HashSet<>(hashMap.values());
        Set<l> a2 = a(hashSet);
        ArrayList arrayList = new ArrayList();
        while (!a2.isEmpty()) {
            l next4 = a2.iterator().next();
            a2.remove(next4);
            arrayList.add(next4.f2744a);
            for (l next5 : next4.f2745b) {
                next5.c.remove(next4);
                if (next5.a()) {
                    a2.add(next5);
                }
            }
        }
        if (arrayList.size() == list.size()) {
            Collections.reverse(arrayList);
            return arrayList;
        }
        ArrayList arrayList2 = new ArrayList();
        for (l lVar4 : hashSet) {
            if (!lVar4.a() && !lVar4.f2745b.isEmpty()) {
                arrayList2.add(lVar4.f2744a);
            }
        }
        throw new DependencyCycleException(arrayList2);
    }

    private static Set<l> a(Set<l> set) {
        HashSet hashSet = new HashSet();
        for (l next : set) {
            if (next.a()) {
                hashSet.add(next);
            }
        }
        return hashSet;
    }
}
