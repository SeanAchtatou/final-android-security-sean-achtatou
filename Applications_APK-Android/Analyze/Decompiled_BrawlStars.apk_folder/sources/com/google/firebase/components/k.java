package com.google.firebase.components;

import com.google.android.gms.common.internal.l;
import com.google.firebase.a.c;
import com.google.firebase.a.d;
import com.google.firebase.b.a;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

/* compiled from: com.google.firebase:firebase-common@@16.0.2 */
public final class k extends g {

    /* renamed from: a  reason: collision with root package name */
    private final List<a<?>> f2742a;

    /* renamed from: b  reason: collision with root package name */
    private final Map<Class<?>, o<?>> f2743b = new HashMap();
    private final m c;

    public final /* bridge */ /* synthetic */ Object a(Class cls) {
        return super.a(cls);
    }

    public k(Executor executor, Iterable<e> iterable, a<?>... aVarArr) {
        this.c = new m(executor);
        ArrayList arrayList = new ArrayList();
        arrayList.add(a.a(this.c, m.class, d.class, c.class));
        for (e components : iterable) {
            arrayList.addAll(components.getComponents());
        }
        Collections.addAll(arrayList, aVarArr);
        this.f2742a = Collections.unmodifiableList(b.b(arrayList));
        for (a<?> a2 : this.f2742a) {
            a(a2);
        }
        a();
    }

    public final <T> a<T> b(Class<T> cls) {
        l.a(cls, "Null interface requested.");
        return this.f2743b.get(cls);
    }

    public final void a(boolean z) {
        for (a next : this.f2742a) {
            if (next.a() || (next.b() && z)) {
                super.a(next.f2733a.iterator().next());
            }
        }
        this.c.a();
    }

    private <T> void a(a aVar) {
        o oVar = new o(aVar.c, new q(aVar, this));
        for (Class<? super T> put : aVar.f2733a) {
            this.f2743b.put(put, oVar);
        }
    }

    private void a() {
        for (a next : this.f2742a) {
            Iterator<f> it = next.f2734b.iterator();
            while (true) {
                if (it.hasNext()) {
                    f next2 = it.next();
                    if ((next2.f2740b == 1) && !this.f2743b.containsKey(next2.f2739a)) {
                        throw new MissingDependencyException(String.format("Unsatisfied dependency for component %s: %s", next, next2.f2739a));
                    }
                }
            }
        }
    }
}
