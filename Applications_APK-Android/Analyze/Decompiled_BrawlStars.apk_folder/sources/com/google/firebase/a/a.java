package com.google.firebase.a;

/* compiled from: com.google.firebase:firebase-common@@16.0.2 */
public class a<T> {

    /* renamed from: a  reason: collision with root package name */
    public final Class<T> f2713a;

    /* renamed from: b  reason: collision with root package name */
    public final T f2714b;

    public String toString() {
        return String.format("Event{type: %s, payload: %s}", this.f2713a, this.f2714b);
    }
}
