package com.google.firebase;

import android.content.Context;
import android.text.TextUtils;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.internal.o;
import com.google.android.gms.common.util.q;

/* compiled from: com.google.firebase:firebase-common@@16.0.2 */
public final class c {

    /* renamed from: a  reason: collision with root package name */
    public final String f2730a;

    /* renamed from: b  reason: collision with root package name */
    public final String f2731b;
    private final String c;
    private final String d;
    private final String e;
    private final String f;
    private final String g;

    private c(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        l.a(!q.a(str), "ApplicationId must be set.");
        this.f2730a = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f2731b = str5;
        this.f = str6;
        this.g = str7;
    }

    public static c a(Context context) {
        o oVar = new o(context);
        String a2 = oVar.a("google_app_id");
        if (TextUtils.isEmpty(a2)) {
            return null;
        }
        return new c(a2, oVar.a("google_api_key"), oVar.a("firebase_database_url"), oVar.a("ga_trackingId"), oVar.a("gcm_defaultSenderId"), oVar.a("google_storage_bucket"), oVar.a("project_id"));
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof c)) {
            return false;
        }
        c cVar = (c) obj;
        if (!j.a(this.f2730a, cVar.f2730a) || !j.a(this.c, cVar.c) || !j.a(this.d, cVar.d) || !j.a(this.e, cVar.e) || !j.a(this.f2731b, cVar.f2731b) || !j.a(this.f, cVar.f) || !j.a(this.g, cVar.g)) {
            return false;
        }
        return true;
    }

    public final int hashCode() {
        return j.a(this.f2730a, this.c, this.d, this.e, this.f2731b, this.f, this.g);
    }

    public final String toString() {
        return j.a(this).a("applicationId", this.f2730a).a("apiKey", this.c).a("databaseUrl", this.d).a("gcmSenderId", this.f2731b).a("storageBucket", this.f).a("projectId", this.g).toString();
    }
}
