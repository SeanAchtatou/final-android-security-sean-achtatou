package com.google.firebase.messaging;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.AdaptiveIconDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;
import java.util.Arrays;
import java.util.MissingFormatArgumentException;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONArray;
import org.json.JSONException;

final class c {

    /* renamed from: a  reason: collision with root package name */
    private static final AtomicInteger f2843a = new AtomicInteger((int) SystemClock.elapsedRealtime());

    /* renamed from: b  reason: collision with root package name */
    private final Context f2844b;
    private Bundle c;

    public c(Context context) {
        this.f2844b = context.getApplicationContext();
    }

    static boolean a(Bundle bundle) {
        return AppEventsConstants.EVENT_PARAM_VALUE_YES.equals(a(bundle, "gcm.n.e")) || a(bundle, "gcm.n.icon") != null;
    }

    static String a(Bundle bundle, String str) {
        String string = bundle.getString(str);
        return string == null ? bundle.getString(str.replace("gcm.n.", "gcm.notification.")) : string;
    }

    static String b(Bundle bundle, String str) {
        String valueOf = String.valueOf(str);
        return a(bundle, "_loc_key".length() != 0 ? valueOf.concat("_loc_key") : new String(valueOf));
    }

    static Object[] c(Bundle bundle, String str) {
        String valueOf = String.valueOf(str);
        String a2 = a(bundle, "_loc_args".length() != 0 ? valueOf.concat("_loc_args") : new String(valueOf));
        if (TextUtils.isEmpty(a2)) {
            return null;
        }
        try {
            JSONArray jSONArray = new JSONArray(a2);
            Object[] objArr = new String[jSONArray.length()];
            for (int i = 0; i < objArr.length; i++) {
                objArr[i] = jSONArray.opt(i);
            }
            return objArr;
        } catch (JSONException unused) {
            String valueOf2 = String.valueOf(str);
            String substring = ("_loc_args".length() != 0 ? valueOf2.concat("_loc_args") : new String(valueOf2)).substring(6);
            StringBuilder sb = new StringBuilder(String.valueOf(substring).length() + 41 + String.valueOf(a2).length());
            sb.append("Malformed ");
            sb.append(substring);
            sb.append(": ");
            sb.append(a2);
            sb.append("  Default value will be used.");
            sb.toString();
            return null;
        }
    }

    static Uri b(Bundle bundle) {
        String a2 = a(bundle, "gcm.n.link_android");
        if (TextUtils.isEmpty(a2)) {
            a2 = a(bundle, "gcm.n.link");
        }
        if (!TextUtils.isEmpty(a2)) {
            return Uri.parse(a2);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x02ee  */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x02ff  */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x0308  */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x030d  */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x0312  */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x0317  */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x0334  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x011d  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x011f  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0183  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0197  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x01c3  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x01c5  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x020d  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x020f  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x021b  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x024d  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x02e5  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean c(android.os.Bundle r15) {
        /*
            r14 = this;
            java.lang.String r0 = "gcm.n.noui"
            java.lang.String r0 = a(r15, r0)
            java.lang.String r1 = "1"
            boolean r0 = r1.equals(r0)
            r2 = 1
            if (r0 == 0) goto L_0x0010
            return r2
        L_0x0010:
            android.content.Context r0 = r14.f2844b
            java.lang.String r3 = "keyguard"
            java.lang.Object r0 = r0.getSystemService(r3)
            android.app.KeyguardManager r0 = (android.app.KeyguardManager) r0
            boolean r0 = r0.inKeyguardRestrictedInputMode()
            r3 = 0
            if (r0 != 0) goto L_0x005c
            boolean r0 = com.google.android.gms.common.util.o.e()
            if (r0 != 0) goto L_0x002c
            r4 = 10
            android.os.SystemClock.sleep(r4)
        L_0x002c:
            int r0 = android.os.Process.myPid()
            android.content.Context r4 = r14.f2844b
            java.lang.String r5 = "activity"
            java.lang.Object r4 = r4.getSystemService(r5)
            android.app.ActivityManager r4 = (android.app.ActivityManager) r4
            java.util.List r4 = r4.getRunningAppProcesses()
            if (r4 == 0) goto L_0x005c
            java.util.Iterator r4 = r4.iterator()
        L_0x0044:
            boolean r5 = r4.hasNext()
            if (r5 == 0) goto L_0x005c
            java.lang.Object r5 = r4.next()
            android.app.ActivityManager$RunningAppProcessInfo r5 = (android.app.ActivityManager.RunningAppProcessInfo) r5
            int r6 = r5.pid
            if (r6 != r0) goto L_0x0044
            int r0 = r5.importance
            r4 = 100
            if (r0 != r4) goto L_0x005c
            r0 = 1
            goto L_0x005d
        L_0x005c:
            r0 = 0
        L_0x005d:
            if (r0 == 0) goto L_0x0060
            return r3
        L_0x0060:
            java.lang.String r0 = "gcm.n.title"
            java.lang.String r0 = r14.d(r15, r0)
            boolean r4 = android.text.TextUtils.isEmpty(r0)
            if (r4 == 0) goto L_0x007c
            android.content.Context r0 = r14.f2844b
            android.content.pm.ApplicationInfo r0 = r0.getApplicationInfo()
            android.content.Context r4 = r14.f2844b
            android.content.pm.PackageManager r4 = r4.getPackageManager()
            java.lang.CharSequence r0 = r0.loadLabel(r4)
        L_0x007c:
            java.lang.String r4 = "gcm.n.body"
            java.lang.String r4 = r14.d(r15, r4)
            java.lang.String r5 = "gcm.n.icon"
            java.lang.String r5 = a(r15, r5)
            boolean r6 = android.text.TextUtils.isEmpty(r5)
            if (r6 != 0) goto L_0x00dd
            android.content.Context r6 = r14.f2844b
            android.content.res.Resources r6 = r6.getResources()
            android.content.Context r7 = r14.f2844b
            java.lang.String r7 = r7.getPackageName()
            java.lang.String r8 = "drawable"
            int r7 = r6.getIdentifier(r5, r8, r7)
            if (r7 == 0) goto L_0x00a9
            boolean r8 = r14.a(r7)
            if (r8 == 0) goto L_0x00a9
            goto L_0x0108
        L_0x00a9:
            android.content.Context r7 = r14.f2844b
            java.lang.String r7 = r7.getPackageName()
            java.lang.String r8 = "mipmap"
            int r7 = r6.getIdentifier(r5, r8, r7)
            if (r7 == 0) goto L_0x00be
            boolean r6 = r14.a(r7)
            if (r6 == 0) goto L_0x00be
            goto L_0x0108
        L_0x00be:
            java.lang.String r6 = java.lang.String.valueOf(r5)
            int r6 = r6.length()
            int r6 = r6 + 61
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>(r6)
            java.lang.String r6 = "Icon resource "
            r7.append(r6)
            r7.append(r5)
            java.lang.String r5 = " not found. Notification will use default icon."
            r7.append(r5)
            r7.toString()
        L_0x00dd:
            android.os.Bundle r5 = r14.a()
            java.lang.String r6 = "com.google.firebase.messaging.default_notification_icon"
            int r5 = r5.getInt(r6, r3)
            if (r5 == 0) goto L_0x00ef
            boolean r6 = r14.a(r5)
            if (r6 != 0) goto L_0x00f7
        L_0x00ef:
            android.content.Context r5 = r14.f2844b
            android.content.pm.ApplicationInfo r5 = r5.getApplicationInfo()
            int r5 = r5.icon
        L_0x00f7:
            if (r5 == 0) goto L_0x0102
            boolean r6 = r14.a(r5)
            if (r6 != 0) goto L_0x0100
            goto L_0x0102
        L_0x0100:
            r7 = r5
            goto L_0x0108
        L_0x0102:
            r5 = 17301651(0x1080093, float:2.4979667E-38)
            r7 = 17301651(0x1080093, float:2.4979667E-38)
        L_0x0108:
            java.lang.String r5 = "gcm.n.color"
            java.lang.String r5 = a(r15, r5)
            java.lang.Integer r5 = r14.a(r5)
            java.lang.String r6 = d(r15)
            boolean r8 = android.text.TextUtils.isEmpty(r6)
            r9 = 0
            if (r8 == 0) goto L_0x011f
            r6 = r9
            goto L_0x0177
        L_0x011f:
            java.lang.String r8 = "default"
            boolean r8 = r8.equals(r6)
            if (r8 != 0) goto L_0x0172
            android.content.Context r8 = r14.f2844b
            android.content.res.Resources r8 = r8.getResources()
            android.content.Context r10 = r14.f2844b
            java.lang.String r10 = r10.getPackageName()
            java.lang.String r11 = "raw"
            int r8 = r8.getIdentifier(r6, r11, r10)
            if (r8 == 0) goto L_0x0172
            android.content.Context r8 = r14.f2844b
            java.lang.String r8 = r8.getPackageName()
            java.lang.String r10 = java.lang.String.valueOf(r8)
            int r10 = r10.length()
            int r10 = r10 + 24
            java.lang.String r11 = java.lang.String.valueOf(r6)
            int r11 = r11.length()
            int r10 = r10 + r11
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>(r10)
            java.lang.String r10 = "android.resource://"
            r11.append(r10)
            r11.append(r8)
            java.lang.String r8 = "/raw/"
            r11.append(r8)
            r11.append(r6)
            java.lang.String r6 = r11.toString()
            android.net.Uri r6 = android.net.Uri.parse(r6)
            goto L_0x0177
        L_0x0172:
            r6 = 2
            android.net.Uri r6 = android.media.RingtoneManager.getDefaultUri(r6)
        L_0x0177:
            java.lang.String r8 = "gcm.n.click_action"
            java.lang.String r8 = a(r15, r8)
            boolean r10 = android.text.TextUtils.isEmpty(r8)
            if (r10 != 0) goto L_0x0197
            android.content.Intent r10 = new android.content.Intent
            r10.<init>(r8)
            android.content.Context r8 = r14.f2844b
            java.lang.String r8 = r8.getPackageName()
            r10.setPackage(r8)
            r8 = 268435456(0x10000000, float:2.5243549E-29)
            r10.setFlags(r8)
            goto L_0x01c1
        L_0x0197:
            android.net.Uri r8 = b(r15)
            if (r8 == 0) goto L_0x01b1
            android.content.Intent r10 = new android.content.Intent
            java.lang.String r11 = "android.intent.action.VIEW"
            r10.<init>(r11)
            android.content.Context r11 = r14.f2844b
            java.lang.String r11 = r11.getPackageName()
            r10.setPackage(r11)
            r10.setData(r8)
            goto L_0x01c1
        L_0x01b1:
            android.content.Context r8 = r14.f2844b
            android.content.pm.PackageManager r8 = r8.getPackageManager()
            android.content.Context r10 = r14.f2844b
            java.lang.String r10 = r10.getPackageName()
            android.content.Intent r10 = r8.getLaunchIntentForPackage(r10)
        L_0x01c1:
            if (r10 != 0) goto L_0x01c5
            r8 = r9
            goto L_0x020b
        L_0x01c5:
            r8 = 67108864(0x4000000, float:1.5046328E-36)
            r10.addFlags(r8)
            android.os.Bundle r8 = new android.os.Bundle
            r8.<init>(r15)
            com.google.firebase.messaging.FirebaseMessagingService.a(r8)
            r10.putExtras(r8)
            java.util.Set r8 = r8.keySet()
            java.util.Iterator r8 = r8.iterator()
        L_0x01dd:
            boolean r11 = r8.hasNext()
            if (r11 == 0) goto L_0x01fd
            java.lang.Object r11 = r8.next()
            java.lang.String r11 = (java.lang.String) r11
            java.lang.String r12 = "gcm.n."
            boolean r12 = r11.startsWith(r12)
            if (r12 != 0) goto L_0x01f9
            java.lang.String r12 = "gcm.notification."
            boolean r12 = r11.startsWith(r12)
            if (r12 == 0) goto L_0x01dd
        L_0x01f9:
            r10.removeExtra(r11)
            goto L_0x01dd
        L_0x01fd:
            android.content.Context r8 = r14.f2844b
            java.util.concurrent.atomic.AtomicInteger r11 = com.google.firebase.messaging.c.f2843a
            int r11 = r11.incrementAndGet()
            r12 = 1073741824(0x40000000, float:2.0)
            android.app.PendingIntent r8 = android.app.PendingIntent.getActivity(r8, r11, r10, r12)
        L_0x020b:
            if (r15 != 0) goto L_0x020f
            r1 = 0
            goto L_0x0219
        L_0x020f:
            java.lang.String r10 = "google.c.a.e"
            java.lang.String r10 = r15.getString(r10)
            boolean r1 = r1.equals(r10)
        L_0x0219:
            if (r1 == 0) goto L_0x024d
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r10 = "com.google.firebase.messaging.NOTIFICATION_OPEN"
            r1.<init>(r10)
            a(r1, r15)
            java.lang.String r10 = "pending_intent"
            r1.putExtra(r10, r8)
            android.content.Context r8 = r14.f2844b
            java.util.concurrent.atomic.AtomicInteger r10 = com.google.firebase.messaging.c.f2843a
            int r10 = r10.incrementAndGet()
            android.app.PendingIntent r8 = com.google.firebase.iid.w.a(r8, r10, r1)
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r10 = "com.google.firebase.messaging.NOTIFICATION_DISMISS"
            r1.<init>(r10)
            a(r1, r15)
            android.content.Context r10 = r14.f2844b
            java.util.concurrent.atomic.AtomicInteger r11 = com.google.firebase.messaging.c.f2843a
            int r11 = r11.incrementAndGet()
            android.app.PendingIntent r1 = com.google.firebase.iid.w.a(r10, r11, r1)
            goto L_0x024e
        L_0x024d:
            r1 = r9
        L_0x024e:
            java.lang.String r10 = "gcm.n.android_channel_id"
            java.lang.String r10 = a(r15, r10)
            boolean r11 = com.google.android.gms.common.util.o.g()
            java.lang.String r12 = "fcm_fallback_notification_channel"
            if (r11 == 0) goto L_0x02cf
            android.content.Context r11 = r14.f2844b
            android.content.pm.ApplicationInfo r11 = r11.getApplicationInfo()
            int r11 = r11.targetSdkVersion
            r13 = 26
            if (r11 >= r13) goto L_0x0269
            goto L_0x02cf
        L_0x0269:
            android.content.Context r9 = r14.f2844b
            java.lang.Class<android.app.NotificationManager> r11 = android.app.NotificationManager.class
            java.lang.Object r9 = r9.getSystemService(r11)
            android.app.NotificationManager r9 = (android.app.NotificationManager) r9
            boolean r11 = android.text.TextUtils.isEmpty(r10)
            if (r11 != 0) goto L_0x029f
            android.app.NotificationChannel r11 = r9.getNotificationChannel(r10)
            if (r11 == 0) goto L_0x0280
            goto L_0x02d0
        L_0x0280:
            java.lang.String r11 = java.lang.String.valueOf(r10)
            int r11 = r11.length()
            int r11 = r11 + 122
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>(r11)
            java.lang.String r11 = "Notification Channel requested ("
            r13.append(r11)
            r13.append(r10)
            java.lang.String r10 = ") has not been created by the app. Manifest configuration, or default, value will be used."
            r13.append(r10)
            r13.toString()
        L_0x029f:
            android.os.Bundle r10 = r14.a()
            java.lang.String r11 = "com.google.firebase.messaging.default_notification_channel_id"
            java.lang.String r10 = r10.getString(r11)
            boolean r11 = android.text.TextUtils.isEmpty(r10)
            if (r11 != 0) goto L_0x02b6
            android.app.NotificationChannel r11 = r9.getNotificationChannel(r10)
            if (r11 == 0) goto L_0x02b6
            goto L_0x02d0
        L_0x02b6:
            android.app.NotificationChannel r10 = r9.getNotificationChannel(r12)
            if (r10 != 0) goto L_0x02cd
            android.app.NotificationChannel r10 = new android.app.NotificationChannel
            android.content.Context r11 = r14.f2844b
            int r13 = com.google.firebase.messaging.R.string.fcm_fallback_notification_channel_label
            java.lang.String r11 = r11.getString(r13)
            r13 = 3
            r10.<init>(r12, r11, r13)
            r9.createNotificationChannel(r10)
        L_0x02cd:
            r10 = r12
            goto L_0x02d0
        L_0x02cf:
            r10 = r9
        L_0x02d0:
            android.support.v4.app.NotificationCompat$Builder r9 = new android.support.v4.app.NotificationCompat$Builder
            android.content.Context r11 = r14.f2844b
            r9.<init>(r11)
            android.support.v4.app.NotificationCompat$Builder r9 = r9.setAutoCancel(r2)
            android.support.v4.app.NotificationCompat$Builder r7 = r9.setSmallIcon(r7)
            boolean r9 = android.text.TextUtils.isEmpty(r0)
            if (r9 != 0) goto L_0x02e8
            r7.setContentTitle(r0)
        L_0x02e8:
            boolean r0 = android.text.TextUtils.isEmpty(r4)
            if (r0 != 0) goto L_0x02fd
            r7.setContentText(r4)
            android.support.v4.app.NotificationCompat$BigTextStyle r0 = new android.support.v4.app.NotificationCompat$BigTextStyle
            r0.<init>()
            android.support.v4.app.NotificationCompat$BigTextStyle r0 = r0.bigText(r4)
            r7.setStyle(r0)
        L_0x02fd:
            if (r5 == 0) goto L_0x0306
            int r0 = r5.intValue()
            r7.setColor(r0)
        L_0x0306:
            if (r6 == 0) goto L_0x030b
            r7.setSound(r6)
        L_0x030b:
            if (r8 == 0) goto L_0x0310
            r7.setContentIntent(r8)
        L_0x0310:
            if (r1 == 0) goto L_0x0315
            r7.setDeleteIntent(r1)
        L_0x0315:
            if (r10 == 0) goto L_0x031a
            r7.setChannelId(r10)
        L_0x031a:
            android.app.Notification r0 = r7.build()
            java.lang.String r1 = "gcm.n.tag"
            java.lang.String r15 = a(r15, r1)
            android.content.Context r1 = r14.f2844b
            java.lang.String r4 = "notification"
            java.lang.Object r1 = r1.getSystemService(r4)
            android.app.NotificationManager r1 = (android.app.NotificationManager) r1
            boolean r4 = android.text.TextUtils.isEmpty(r15)
            if (r4 == 0) goto L_0x034b
            long r4 = android.os.SystemClock.uptimeMillis()
            r15 = 37
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>(r15)
            java.lang.String r15 = "FCM-Notification:"
            r6.append(r15)
            r6.append(r4)
            java.lang.String r15 = r6.toString()
        L_0x034b:
            r1.notify(r15, r3, r0)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.messaging.c.c(android.os.Bundle):boolean");
    }

    private final String d(Bundle bundle, String str) {
        String a2 = a(bundle, str);
        if (!TextUtils.isEmpty(a2)) {
            return a2;
        }
        String b2 = b(bundle, str);
        if (TextUtils.isEmpty(b2)) {
            return null;
        }
        Resources resources = this.f2844b.getResources();
        int identifier = resources.getIdentifier(b2, "string", this.f2844b.getPackageName());
        if (identifier == 0) {
            String valueOf = String.valueOf(str);
            String substring = ("_loc_key".length() != 0 ? valueOf.concat("_loc_key") : new String(valueOf)).substring(6);
            StringBuilder sb = new StringBuilder(String.valueOf(substring).length() + 49 + String.valueOf(b2).length());
            sb.append(substring);
            sb.append(" resource not found: ");
            sb.append(b2);
            sb.append(" Default value will be used.");
            sb.toString();
            return null;
        }
        Object[] c2 = c(bundle, str);
        if (c2 == null) {
            return resources.getString(identifier);
        }
        try {
            return resources.getString(identifier, c2);
        } catch (MissingFormatArgumentException unused) {
            String arrays = Arrays.toString(c2);
            StringBuilder sb2 = new StringBuilder(String.valueOf(b2).length() + 58 + String.valueOf(arrays).length());
            sb2.append("Missing format argument for ");
            sb2.append(b2);
            sb2.append(": ");
            sb2.append(arrays);
            sb2.append(" Default value will be used.");
            sb2.toString();
            return null;
        }
    }

    private final boolean a(int i) {
        if (Build.VERSION.SDK_INT != 26) {
            return true;
        }
        try {
            if (!(this.f2844b.getResources().getDrawable(i, null) instanceof AdaptiveIconDrawable)) {
                return true;
            }
            StringBuilder sb = new StringBuilder(77);
            sb.append("Adaptive icons cannot be used in notifications. Ignoring icon id: ");
            sb.append(i);
            sb.toString();
            return false;
        } catch (Resources.NotFoundException unused) {
            return false;
        }
    }

    private final Integer a(String str) {
        if (Build.VERSION.SDK_INT < 21) {
            return null;
        }
        if (!TextUtils.isEmpty(str)) {
            try {
                return Integer.valueOf(Color.parseColor(str));
            } catch (IllegalArgumentException unused) {
                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 54);
                sb.append("Color ");
                sb.append(str);
                sb.append(" not valid. Notification will use default color.");
                sb.toString();
            }
        }
        int i = a().getInt("com.google.firebase.messaging.default_notification_color", 0);
        if (i != 0) {
            try {
                return Integer.valueOf(ContextCompat.getColor(this.f2844b, i));
            } catch (Resources.NotFoundException unused2) {
            }
        }
        return null;
    }

    static String d(Bundle bundle) {
        String a2 = a(bundle, "gcm.n.sound2");
        return TextUtils.isEmpty(a2) ? a(bundle, "gcm.n.sound") : a2;
    }

    private static void a(Intent intent, Bundle bundle) {
        for (String next : bundle.keySet()) {
            if (next.startsWith("google.c.a.") || next.equals("from")) {
                intent.putExtra(next, bundle.getString(next));
            }
        }
    }

    private final Bundle a() {
        Bundle bundle = this.c;
        if (bundle != null) {
            return bundle;
        }
        ApplicationInfo applicationInfo = null;
        try {
            applicationInfo = this.f2844b.getPackageManager().getApplicationInfo(this.f2844b.getPackageName(), 128);
        } catch (PackageManager.NameNotFoundException unused) {
        }
        if (applicationInfo == null || applicationInfo.metaData == null) {
            return Bundle.EMPTY;
        }
        this.c = applicationInfo.metaData;
        return this.c;
    }
}
