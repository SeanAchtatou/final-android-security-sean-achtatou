package com.google.firebase.messaging;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.util.ArrayMap;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.Map;

public final class RemoteMessage extends AbstractSafeParcelable {
    public static final Parcelable.Creator<RemoteMessage> CREATOR = new d();

    /* renamed from: a  reason: collision with root package name */
    public Bundle f2837a;

    /* renamed from: b  reason: collision with root package name */
    private Map<String, String> f2838b;
    private a c;

    public RemoteMessage(Bundle bundle) {
        this.f2837a = bundle;
    }

    public final Map<String, String> a() {
        if (this.f2838b == null) {
            Bundle bundle = this.f2837a;
            ArrayMap arrayMap = new ArrayMap();
            for (String next : bundle.keySet()) {
                Object obj = bundle.get(next);
                if (obj instanceof String) {
                    String str = (String) obj;
                    if (!next.startsWith("google.") && !next.startsWith("gcm.") && !next.equals("from") && !next.equals("message_type") && !next.equals("collapse_key")) {
                        arrayMap.put(next, str);
                    }
                }
            }
            this.f2838b = arrayMap;
        }
        return this.f2838b;
    }

    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public final String f2839a;

        /* renamed from: b  reason: collision with root package name */
        public final String f2840b;
        public final String c;
        public final String d;
        private final String e;
        private final String[] f;
        private final String g;
        private final String[] h;
        private final String i;
        private final String j;
        private final String k;
        private final String l;
        private final Uri m;

        private a(Bundle bundle) {
            this.f2839a = c.a(bundle, "gcm.n.title");
            this.e = c.b(bundle, "gcm.n.title");
            this.f = a(bundle, "gcm.n.title");
            this.f2840b = c.a(bundle, "gcm.n.body");
            this.g = c.b(bundle, "gcm.n.body");
            this.h = a(bundle, "gcm.n.body");
            this.i = c.a(bundle, "gcm.n.icon");
            this.c = c.d(bundle);
            this.j = c.a(bundle, "gcm.n.tag");
            this.d = c.a(bundle, "gcm.n.color");
            this.k = c.a(bundle, "gcm.n.click_action");
            this.l = c.a(bundle, "gcm.n.android_channel_id");
            this.m = c.b(bundle);
        }

        private static String[] a(Bundle bundle, String str) {
            Object[] c2 = c.c(bundle, str);
            if (c2 == null) {
                return null;
            }
            String[] strArr = new String[c2.length];
            for (int i2 = 0; i2 < c2.length; i2++) {
                strArr[i2] = String.valueOf(c2[i2]);
            }
            return strArr;
        }

        /* synthetic */ a(Bundle bundle, byte b2) {
            this(bundle);
        }
    }

    public final a b() {
        if (this.c == null && c.a(this.f2837a)) {
            this.c = new a(this.f2837a, (byte) 0);
        }
        return this.c;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
     arg types: [android.os.Parcel, int, android.os.Bundle, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = com.google.android.gms.common.internal.safeparcel.a.a(parcel, 20293);
        com.google.android.gms.common.internal.safeparcel.a.a(parcel, 2, this.f2837a, false);
        com.google.android.gms.common.internal.safeparcel.a.b(parcel, a2);
    }
}
