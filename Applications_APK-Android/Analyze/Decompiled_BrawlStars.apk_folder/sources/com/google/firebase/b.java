package com.google.firebase;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import com.google.android.gms.common.api.internal.b;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.util.p;
import com.google.firebase.components.e;
import com.google.firebase.components.i;
import com.google.firebase.components.k;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: com.google.firebase:firebase-common@@16.0.2 */
public class b {

    /* renamed from: a  reason: collision with root package name */
    static final Map<String, b> f2724a = new ArrayMap();

    /* renamed from: b  reason: collision with root package name */
    private static final List<String> f2725b = Arrays.asList("com.google.firebase.auth.FirebaseAuth", "com.google.firebase.iid.FirebaseInstanceId");
    private static final List<String> c = Collections.singletonList("com.google.firebase.crash.FirebaseCrash");
    private static final List<String> d = Arrays.asList("com.google.android.gms.measurement.AppMeasurement");
    private static final List<String> e = Arrays.asList(new String[0]);
    private static final Set<String> f = Collections.emptySet();
    /* access modifiers changed from: private */
    public static final Object g = new Object();
    private static final Executor h = new c((byte) 0);
    private final Context i;
    private final String j;
    private final c k;
    private final k l;
    private final SharedPreferences m;
    private final com.google.firebase.a.c n;
    /* access modifiers changed from: private */
    public final AtomicBoolean o = new AtomicBoolean(false);
    private final AtomicBoolean p = new AtomicBoolean();
    private final AtomicBoolean q;
    private final List<Object> r = new CopyOnWriteArrayList();
    private final List<Object> s = new CopyOnWriteArrayList();
    private final List<Object> t = new CopyOnWriteArrayList();
    private a u;

    @Deprecated
    /* compiled from: com.google.firebase:firebase-common@@16.0.2 */
    public interface a {
    }

    public final Context a() {
        i();
        return this.i;
    }

    private String g() {
        i();
        return this.j;
    }

    public final c b() {
        i();
        return this.k;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof b)) {
            return false;
        }
        return this.j.equals(((b) obj).g());
    }

    public int hashCode() {
        return this.j.hashCode();
    }

    public String toString() {
        return j.a(this).a("name", this.j).a("options", this.k).toString();
    }

    public static b c() {
        b bVar;
        synchronized (g) {
            bVar = f2724a.get("[DEFAULT]");
            if (bVar == null) {
                throw new IllegalStateException("Default FirebaseApp is not initialized in this process " + p.a() + ". Make sure to call FirebaseApp.initializeApp(Context) first.");
            }
        }
        return bVar;
    }

    public static b a(Context context) {
        synchronized (g) {
            if (f2724a.containsKey("[DEFAULT]")) {
                b c2 = c();
                return c2;
            }
            c a2 = c.a(context);
            if (a2 == null) {
                return null;
            }
            b a3 = a(context, a2, "[DEFAULT]");
            return a3;
        }
    }

    private static b a(Context context, c cVar, String str) {
        b bVar;
        C0125b.a(context);
        String trim = str.trim();
        if (context.getApplicationContext() != null) {
            context = context.getApplicationContext();
        }
        synchronized (g) {
            boolean z = !f2724a.containsKey(trim);
            l.a(z, "FirebaseApp name " + trim + " already exists!");
            l.a(context, "Application context cannot be null.");
            bVar = new b(context, trim, cVar);
            f2724a.put(trim, bVar);
        }
        bVar.j();
        return bVar;
    }

    public final <T> T a(Class<T> cls) {
        i();
        return this.l.a(cls);
    }

    public final boolean d() {
        i();
        return this.q.get();
    }

    private b(Context context, String str, c cVar) {
        this.i = (Context) l.a(context);
        this.j = l.a(str);
        this.k = (c) l.a(cVar);
        this.u = new com.google.firebase.internal.a();
        this.m = context.getSharedPreferences("com.google.firebase.common.prefs", 0);
        this.q = new AtomicBoolean(h());
        com.google.firebase.components.b bVar = new com.google.firebase.components.b(context, new i((byte) 0));
        List<e> a2 = com.google.firebase.components.b.a(bVar.f2738b.a(bVar.f2737a));
        this.l = new k(h, a2, com.google.firebase.components.a.a(context, Context.class, new Class[0]), com.google.firebase.components.a.a(this, b.class, new Class[0]), com.google.firebase.components.a.a(cVar, c.class, new Class[0]));
        this.n = (com.google.firebase.a.c) this.l.a(com.google.firebase.a.c.class);
    }

    private boolean h() {
        ApplicationInfo applicationInfo;
        if (this.m.contains("firebase_data_collection_default_enabled")) {
            return this.m.getBoolean("firebase_data_collection_default_enabled", true);
        }
        try {
            PackageManager packageManager = this.i.getPackageManager();
            if (!(packageManager == null || (applicationInfo = packageManager.getApplicationInfo(this.i.getPackageName(), 128)) == null || applicationInfo.metaData == null || !applicationInfo.metaData.containsKey("firebase_data_collection_default_enabled"))) {
                return applicationInfo.metaData.getBoolean("firebase_data_collection_default_enabled");
            }
        } catch (PackageManager.NameNotFoundException unused) {
        }
        return true;
    }

    private void i() {
        l.a(!this.p.get(), "FirebaseApp was deleted");
    }

    public final boolean e() {
        return "[DEFAULT]".equals(g());
    }

    /* access modifiers changed from: private */
    public void j() {
        Class<b> cls = b.class;
        boolean isDeviceProtectedStorage = ContextCompat.isDeviceProtectedStorage(this.i);
        if (isDeviceProtectedStorage) {
            d.a(this.i);
        } else {
            this.l.a(e());
        }
        a(cls, this, f2725b, isDeviceProtectedStorage);
        if (e()) {
            a(cls, this, c, isDeviceProtectedStorage);
            a(Context.class, this.i, d, isDeviceProtectedStorage);
        }
    }

    private static <T> void a(Class<T> cls, T t2, Iterable<String> iterable, boolean z) {
        for (String next : iterable) {
            if (z) {
                try {
                    if (!e.contains(next)) {
                    }
                } catch (ClassNotFoundException unused) {
                    if (!f.contains(next)) {
                        next + " is not linked. Skipping initialization.";
                    } else {
                        throw new IllegalStateException(next + " is missing, but is required. Check if it has been removed by Proguard.");
                    }
                } catch (NoSuchMethodException unused2) {
                    throw new IllegalStateException(next + "#getInstance has been removed by Proguard. Add keep rule to prevent it.");
                } catch (InvocationTargetException e2) {
                    Log.wtf("FirebaseApp", "Firebase API initialization failure.", e2);
                } catch (IllegalAccessException e3) {
                    Log.wtf("FirebaseApp", "Failed to initialize " + next, e3);
                }
            }
            Method method = Class.forName(next).getMethod("getInstance", cls);
            int modifiers = method.getModifiers();
            if (Modifier.isPublic(modifiers) && Modifier.isStatic(modifiers)) {
                method.invoke(null, t2);
            }
        }
    }

    /* compiled from: com.google.firebase:firebase-common@@16.0.2 */
    static class d extends BroadcastReceiver {

        /* renamed from: a  reason: collision with root package name */
        private static AtomicReference<d> f2728a = new AtomicReference<>();

        /* renamed from: b  reason: collision with root package name */
        private final Context f2729b;

        private d(Context context) {
            this.f2729b = context;
        }

        public final void onReceive(Context context, Intent intent) {
            synchronized (b.g) {
                for (b a2 : b.f2724a.values()) {
                    a2.j();
                }
            }
            this.f2729b.unregisterReceiver(this);
        }

        static /* synthetic */ void a(Context context) {
            if (f2728a.get() == null) {
                d dVar = new d(context);
                if (f2728a.compareAndSet(null, dVar)) {
                    context.registerReceiver(dVar, new IntentFilter("android.intent.action.USER_UNLOCKED"));
                }
            }
        }
    }

    /* renamed from: com.google.firebase.b$b  reason: collision with other inner class name */
    /* compiled from: com.google.firebase:firebase-common@@16.0.2 */
    static class C0125b implements b.a {

        /* renamed from: a  reason: collision with root package name */
        private static AtomicReference<C0125b> f2726a = new AtomicReference<>();

        private C0125b() {
        }

        public final void a(boolean z) {
            synchronized (b.g) {
                Iterator it = new ArrayList(b.f2724a.values()).iterator();
                while (it.hasNext()) {
                    b bVar = (b) it.next();
                    if (bVar.o.get()) {
                        b.a(bVar, z);
                    }
                }
            }
        }

        static /* synthetic */ void a(Context context) {
            if (context.getApplicationContext() instanceof Application) {
                Application application = (Application) context.getApplicationContext();
                if (f2726a.get() == null) {
                    C0125b bVar = new C0125b();
                    if (f2726a.compareAndSet(null, bVar)) {
                        com.google.android.gms.common.api.internal.b.a(application);
                        com.google.android.gms.common.api.internal.b.a().a(bVar);
                    }
                }
            }
        }
    }

    /* compiled from: com.google.firebase:firebase-common@@16.0.2 */
    static class c implements Executor {

        /* renamed from: a  reason: collision with root package name */
        private static final Handler f2727a = new Handler(Looper.getMainLooper());

        private c() {
        }

        /* synthetic */ c(byte b2) {
            this();
        }

        public final void execute(Runnable runnable) {
            f2727a.post(runnable);
        }
    }

    static /* synthetic */ void a(b bVar, boolean z) {
        Iterator<Object> it = bVar.s.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }
}
