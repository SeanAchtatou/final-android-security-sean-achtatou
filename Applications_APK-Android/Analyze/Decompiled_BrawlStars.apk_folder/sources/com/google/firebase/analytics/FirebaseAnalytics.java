package com.google.firebase.analytics;

import android.app.Activity;
import android.content.Context;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.measurement.internal.ar;
import com.google.android.gms.measurement.internal.cg;
import com.google.android.gms.measurement.internal.ch;
import com.google.android.gms.measurement.internal.ed;
import com.google.android.gms.measurement.internal.ej;
import com.google.android.gms.measurement.internal.j;
import com.google.firebase.iid.FirebaseInstanceId;

public final class FirebaseAnalytics {

    /* renamed from: a  reason: collision with root package name */
    private static volatile FirebaseAnalytics f2715a;

    /* renamed from: b  reason: collision with root package name */
    private final ar f2716b;
    private final Object c = new Object();

    public static FirebaseAnalytics getInstance(Context context) {
        if (f2715a == null) {
            synchronized (FirebaseAnalytics.class) {
                if (f2715a == null) {
                    f2715a = new FirebaseAnalytics(ar.a(context, (j) null));
                }
            }
        }
        return f2715a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.ch.a(android.app.Activity, com.google.android.gms.measurement.internal.cg, boolean):void
     arg types: [android.app.Activity, com.google.android.gms.measurement.internal.cg, int]
     candidates:
      com.google.android.gms.measurement.internal.ch.a(com.google.android.gms.measurement.internal.cg, android.os.Bundle, boolean):void
      com.google.android.gms.measurement.internal.ch.a(com.google.android.gms.measurement.internal.ch, com.google.android.gms.measurement.internal.cg, boolean):void
      com.google.android.gms.measurement.internal.ch.a(android.app.Activity, com.google.android.gms.measurement.internal.cg, boolean):void */
    public final void setCurrentScreen(Activity activity, String str, String str2) {
        if (!ej.a()) {
            this.f2716b.q().f.a("setCurrentScreen must be called from the main thread");
            return;
        }
        ch h = this.f2716b.h();
        if (h.f2464b == null) {
            h.q().f.a("setCurrentScreen cannot be called while no activity active");
        } else if (h.d.get(activity) == null) {
            h.q().f.a("setCurrentScreen must be called with an activity in the activity lifecycle");
        } else {
            if (str2 == null) {
                str2 = ch.a(activity.getClass().getCanonicalName());
            }
            boolean equals = h.f2464b.f2462b.equals(str2);
            boolean c2 = ed.c(h.f2464b.f2461a, str);
            if (equals && c2) {
                h.q().h.a("setCurrentScreen cannot be called with the same class and name");
            } else if (str != null && (str.length() <= 0 || str.length() > 100)) {
                h.q().f.a("Invalid screen name length in setCurrentScreen. Length", Integer.valueOf(str.length()));
            } else if (str2 == null || (str2.length() > 0 && str2.length() <= 100)) {
                h.q().k.a("Setting current screen to name, class", str == null ? "null" : str, str2);
                cg cgVar = new cg(str, str2, h.o().f());
                h.d.put(activity, cgVar);
                h.a(activity, cgVar, true);
            } else {
                h.q().f.a("Invalid class name length in setCurrentScreen. Length", Integer.valueOf(str2.length()));
            }
        }
    }

    private FirebaseAnalytics(ar arVar) {
        l.a(arVar);
        this.f2716b = arVar;
    }

    public final String getFirebaseInstanceId() {
        FirebaseInstanceId.a().b();
        return FirebaseInstanceId.d();
    }
}
