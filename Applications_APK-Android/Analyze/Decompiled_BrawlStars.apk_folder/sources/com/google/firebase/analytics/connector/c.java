package com.google.firebase.analytics.connector;

import java.util.concurrent.Executor;

final /* synthetic */ class c implements Executor {

    /* renamed from: a  reason: collision with root package name */
    static final Executor f2719a = new c();

    private c() {
    }

    public final void execute(Runnable runnable) {
        runnable.run();
    }
}
