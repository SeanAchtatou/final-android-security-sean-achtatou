package com.google.firebase.analytics.connector.internal;

import android.content.Context;
import com.google.firebase.analytics.connector.b;
import com.google.firebase.components.c;
import com.google.firebase.components.d;

final /* synthetic */ class a implements d {

    /* renamed from: a  reason: collision with root package name */
    static final d f2721a = new a();

    private a() {
    }

    public final Object a(c cVar) {
        return b.a((com.google.firebase.b) cVar.a(com.google.firebase.b.class), (Context) cVar.a(Context.class), (com.google.firebase.a.d) cVar.a(com.google.firebase.a.d.class));
    }
}
