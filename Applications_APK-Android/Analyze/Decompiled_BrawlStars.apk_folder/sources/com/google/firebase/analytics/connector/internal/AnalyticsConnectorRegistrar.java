package com.google.firebase.analytics.connector.internal;

import android.content.Context;
import com.google.firebase.a.d;
import com.google.firebase.b;
import com.google.firebase.components.a;
import com.google.firebase.components.e;
import com.google.firebase.components.f;
import java.util.Collections;
import java.util.List;

public class AnalyticsConnectorRegistrar implements e {
    public List<a<?>> getComponents() {
        return Collections.singletonList(a.a(com.google.firebase.analytics.connector.a.class).a(f.a(b.class)).a(f.a(Context.class)).a(f.a(d.class)).a(a.f2721a).a(2).a());
    }
}
