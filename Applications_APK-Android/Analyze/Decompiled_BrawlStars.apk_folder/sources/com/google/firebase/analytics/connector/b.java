package com.google.firebase.analytics.connector;

import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.measurement.AppMeasurement;
import com.google.android.gms.measurement.internal.ar;
import com.google.android.gms.measurement.internal.j;
import com.google.firebase.a;
import com.google.firebase.a.d;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class b implements a {

    /* renamed from: b  reason: collision with root package name */
    private static volatile a f2717b;

    /* renamed from: a  reason: collision with root package name */
    final Map<String, Object> f2718a = new ConcurrentHashMap();
    private final AppMeasurement c;

    private b(AppMeasurement appMeasurement) {
        l.a(appMeasurement);
        this.c = appMeasurement;
    }

    public static a a(com.google.firebase.b bVar, Context context, d dVar) {
        l.a(bVar);
        l.a(context);
        l.a(dVar);
        l.a(context.getApplicationContext());
        if (f2717b == null) {
            synchronized (b.class) {
                if (f2717b == null) {
                    Bundle bundle = new Bundle(1);
                    if (bVar.e()) {
                        dVar.a(a.class, c.f2719a, d.f2720a);
                        bundle.putBoolean("dataCollectionDefaultEnabled", bVar.d());
                    }
                    f2717b = new b(ar.a(context, new j(0, 0, true, null, null, null, bundle)).h);
                }
            }
        }
        return f2717b;
    }

    public final void a(String str, String str2, Bundle bundle) {
        if (com.google.firebase.analytics.connector.internal.b.a(str) && com.google.firebase.analytics.connector.internal.b.a(str2, bundle) && com.google.firebase.analytics.connector.internal.b.a(str, str2, bundle)) {
            this.c.logEventInternal(str, str2, bundle);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.bv.a(java.lang.String, java.lang.String, java.lang.Object, boolean):void
     arg types: [java.lang.String, java.lang.String, java.lang.Object, int]
     candidates:
      com.google.android.gms.measurement.internal.bv.a(java.lang.String, java.lang.String, long, java.lang.Object):void
      com.google.android.gms.measurement.internal.bv.a(java.lang.String, java.lang.String, java.lang.String, boolean):java.util.Map<java.lang.String, java.lang.Object>
      com.google.android.gms.measurement.internal.bv.a(java.lang.String, java.lang.String, long, android.os.Bundle):void
      com.google.android.gms.measurement.internal.bv.a(java.lang.String, java.lang.String, java.lang.Object, long):void
      com.google.android.gms.measurement.internal.bv.a(java.lang.String, java.lang.String, java.lang.String, android.os.Bundle):void
      com.google.android.gms.measurement.internal.bv.a(java.lang.String, java.lang.String, java.lang.Object, boolean):void */
    public final void a(String str, String str2, Object obj) {
        if (com.google.firebase.analytics.connector.internal.b.a(str) && com.google.firebase.analytics.connector.internal.b.a(str, str2)) {
            AppMeasurement appMeasurement = this.c;
            l.a(str);
            appMeasurement.f2353a.d().a(str, str2, obj, true);
        }
    }

    static final /* synthetic */ void a(com.google.firebase.a.a aVar) {
        boolean z = ((a) aVar.f2714b).f2712a;
        synchronized (b.class) {
            ((b) f2717b).c.f2353a.d().a(z);
        }
    }
}
