package com.google.zxing;

/* compiled from: LuminanceSource */
public abstract class h {

    /* renamed from: a  reason: collision with root package name */
    public final int f3144a;

    /* renamed from: b  reason: collision with root package name */
    public final int f3145b;

    public abstract byte[] a();

    public abstract byte[] a(int i, byte[] bArr);

    public boolean b() {
        return false;
    }

    protected h(int i, int i2) {
        this.f3144a = i;
        this.f3145b = i2;
    }

    public h c() {
        return new g(this);
    }

    public h d() {
        throw new UnsupportedOperationException("This luminance source does not support rotation by 90 degrees.");
    }

    public final String toString() {
        int i = this.f3144a;
        StringBuilder sb = new StringBuilder(this.f3145b * (i + 1));
        byte[] bArr = new byte[i];
        for (int i2 = 0; i2 < this.f3145b; i2++) {
            bArr = a(i2, bArr);
            for (int i3 = 0; i3 < this.f3144a; i3++) {
                byte b2 = bArr[i3] & 255;
                sb.append(b2 < 64 ? '#' : b2 < 128 ? '+' : b2 < 192 ? '.' : ' ');
            }
            sb.append(10);
        }
        return sb.toString();
    }
}
