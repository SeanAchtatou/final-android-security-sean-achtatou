package com.google.zxing;

public final class ChecksumException extends ReaderException {
    private static final ChecksumException c;

    static {
        ChecksumException checksumException = new ChecksumException();
        c = checksumException;
        checksumException.setStackTrace(f2880b);
    }

    private ChecksumException() {
    }

    public static ChecksumException a() {
        return f2879a ? new ChecksumException() : c;
    }
}
