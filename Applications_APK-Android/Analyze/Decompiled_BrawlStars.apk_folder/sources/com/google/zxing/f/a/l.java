package com.google.zxing.f.a;

import com.google.zxing.FormatException;
import com.google.zxing.common.c;
import com.google.zxing.common.e;
import com.google.zxing.d;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

/* compiled from: DecodedBitStreamParser */
final class l {

    /* renamed from: a  reason: collision with root package name */
    private static final char[] f3108a = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ $%*+-./:".toCharArray();

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    static e a(byte[] bArr, s sVar, o oVar, Map<d, ?> map) throws FormatException {
        q a2;
        q qVar;
        String str;
        int i;
        int a3;
        int i2;
        s sVar2 = sVar;
        c cVar = new c(bArr);
        StringBuilder sb = new StringBuilder(50);
        int i3 = 1;
        ArrayList arrayList = new ArrayList(1);
        com.google.zxing.common.d dVar = null;
        boolean z = false;
        int i4 = -1;
        int i5 = -1;
        while (true) {
            try {
                if (cVar.a() < 4) {
                    a2 = q.TERMINATOR;
                } else {
                    a2 = q.a(cVar.a(4));
                }
                q qVar2 = a2;
                switch (qVar2) {
                    case TERMINATOR:
                        qVar = qVar2;
                        break;
                    case FNC1_FIRST_POSITION:
                    case FNC1_SECOND_POSITION:
                        qVar = qVar2;
                        z = true;
                        break;
                    case STRUCTURED_APPEND:
                        if (cVar.a() >= 16) {
                            i4 = cVar.a(8);
                            i5 = cVar.a(8);
                            qVar = qVar2;
                            break;
                        } else {
                            throw FormatException.a();
                        }
                    case ECI:
                        int a4 = cVar.a(8);
                        if ((a4 & 128) == 0) {
                            i = a4 & 127;
                        } else {
                            if ((a4 & 192) == 128) {
                                a3 = cVar.a(8);
                                i2 = (a4 & 63) << 8;
                            } else if ((a4 & 224) == 192) {
                                a3 = cVar.a(16);
                                i2 = (a4 & 31) << 16;
                            } else {
                                throw FormatException.a();
                            }
                            i = i2 | a3;
                        }
                        dVar = com.google.zxing.common.d.a(i);
                        if (dVar != null) {
                            qVar = qVar2;
                            break;
                        } else {
                            throw FormatException.a();
                        }
                    case HANZI:
                        int a5 = cVar.a(4);
                        int a6 = cVar.a(qVar2.a(sVar2));
                        if (a5 == i3) {
                            a(cVar, sb, a6);
                        }
                        qVar = qVar2;
                        break;
                    default:
                        int a7 = cVar.a(qVar2.a(sVar2));
                        int i6 = m.f3109a[qVar2.ordinal()];
                        if (i6 != i3) {
                            if (i6 != 2) {
                                if (i6 == 3) {
                                    qVar = qVar2;
                                    a(cVar, sb, a7, dVar, arrayList, map);
                                    break;
                                } else if (i6 == 4) {
                                    b(cVar, sb, a7);
                                    qVar = qVar2;
                                    break;
                                } else {
                                    throw FormatException.a();
                                }
                            } else {
                                qVar = qVar2;
                                a(cVar, sb, a7, z);
                                break;
                            }
                        } else {
                            qVar = qVar2;
                            c(cVar, sb, a7);
                            break;
                        }
                }
                if (qVar == q.TERMINATOR) {
                    String sb2 = sb.toString();
                    ArrayList arrayList2 = arrayList.isEmpty() ? null : arrayList;
                    if (oVar == null) {
                        str = null;
                    } else {
                        str = oVar.toString();
                    }
                    return new e(bArr, sb2, arrayList2, str, i4, i5);
                }
                i3 = 1;
            } catch (IllegalArgumentException unused) {
                throw FormatException.a();
            }
        }
    }

    private static void a(c cVar, StringBuilder sb, int i) throws FormatException {
        if (i * 13 <= cVar.a()) {
            byte[] bArr = new byte[(i * 2)];
            int i2 = 0;
            while (i > 0) {
                int a2 = cVar.a(13);
                int i3 = (a2 % 96) | ((a2 / 96) << 8);
                int i4 = i3 + (i3 < 959 ? 41377 : 42657);
                bArr[i2] = (byte) (i4 >> 8);
                bArr[i2 + 1] = (byte) i4;
                i2 += 2;
                i--;
            }
            try {
                sb.append(new String(bArr, "GB2312"));
            } catch (UnsupportedEncodingException unused) {
                throw FormatException.a();
            }
        } else {
            throw FormatException.a();
        }
    }

    private static void b(c cVar, StringBuilder sb, int i) throws FormatException {
        if (i * 13 <= cVar.a()) {
            byte[] bArr = new byte[(i * 2)];
            int i2 = 0;
            while (i > 0) {
                int a2 = cVar.a(13);
                int i3 = (a2 % 192) | ((a2 / 192) << 8);
                int i4 = i3 + (i3 < 7936 ? 33088 : 49472);
                bArr[i2] = (byte) (i4 >> 8);
                bArr[i2 + 1] = (byte) i4;
                i2 += 2;
                i--;
            }
            try {
                sb.append(new String(bArr, "SJIS"));
            } catch (UnsupportedEncodingException unused) {
                throw FormatException.a();
            }
        } else {
            throw FormatException.a();
        }
    }

    private static void a(c cVar, StringBuilder sb, int i, com.google.zxing.common.d dVar, Collection<byte[]> collection, Map<d, ?> map) throws FormatException {
        String str;
        if ((i << 3) <= cVar.a()) {
            byte[] bArr = new byte[i];
            for (int i2 = 0; i2 < i; i2++) {
                bArr[i2] = (byte) cVar.a(8);
            }
            if (dVar == null) {
                str = com.google.zxing.common.l.a(bArr, map);
            } else {
                str = dVar.name();
            }
            try {
                sb.append(new String(bArr, str));
                collection.add(bArr);
            } catch (UnsupportedEncodingException unused) {
                throw FormatException.a();
            }
        } else {
            throw FormatException.a();
        }
    }

    private static char a(int i) throws FormatException {
        char[] cArr = f3108a;
        if (i < cArr.length) {
            return cArr[i];
        }
        throw FormatException.a();
    }

    private static void a(c cVar, StringBuilder sb, int i, boolean z) throws FormatException {
        while (i > 1) {
            if (cVar.a() >= 11) {
                int a2 = cVar.a(11);
                sb.append(a(a2 / 45));
                sb.append(a(a2 % 45));
                i -= 2;
            } else {
                throw FormatException.a();
            }
        }
        if (i == 1) {
            if (cVar.a() >= 6) {
                sb.append(a(cVar.a(6)));
            } else {
                throw FormatException.a();
            }
        }
        if (z) {
            for (int length = sb.length(); length < sb.length(); length++) {
                if (sb.charAt(length) == '%') {
                    if (length < sb.length() - 1) {
                        int i2 = length + 1;
                        if (sb.charAt(i2) == '%') {
                            sb.deleteCharAt(i2);
                        }
                    }
                    sb.setCharAt(length, 29);
                }
            }
        }
    }

    private static void c(c cVar, StringBuilder sb, int i) throws FormatException {
        while (i >= 3) {
            if (cVar.a() >= 10) {
                int a2 = cVar.a(10);
                if (a2 < 1000) {
                    sb.append(a(a2 / 100));
                    sb.append(a((a2 / 10) % 10));
                    sb.append(a(a2 % 10));
                    i -= 3;
                } else {
                    throw FormatException.a();
                }
            } else {
                throw FormatException.a();
            }
        }
        if (i == 2) {
            if (cVar.a() >= 7) {
                int a3 = cVar.a(7);
                if (a3 < 100) {
                    sb.append(a(a3 / 10));
                    sb.append(a(a3 % 10));
                    return;
                }
                throw FormatException.a();
            }
            throw FormatException.a();
        } else if (i != 1) {
        } else {
            if (cVar.a() >= 4) {
                int a4 = cVar.a(4);
                if (a4 < 10) {
                    sb.append(a(a4));
                    return;
                }
                throw FormatException.a();
            }
            throw FormatException.a();
        }
    }
}
