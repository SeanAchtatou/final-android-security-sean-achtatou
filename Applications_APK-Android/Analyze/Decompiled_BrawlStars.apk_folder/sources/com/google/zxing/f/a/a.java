package com.google.zxing.f.a;

import com.google.zxing.FormatException;
import com.google.zxing.common.b;

/* compiled from: BitMatrixParser */
final class a {

    /* renamed from: a  reason: collision with root package name */
    final b f3102a;

    /* renamed from: b  reason: collision with root package name */
    s f3103b;
    p c;
    boolean d;

    a(b bVar) throws FormatException {
        int i = bVar.f2969b;
        if (i < 21 || (i & 3) != 1) {
            throw FormatException.a();
        }
        this.f3102a = bVar;
    }

    /* access modifiers changed from: package-private */
    public final p a() throws FormatException {
        p pVar = this.c;
        if (pVar != null) {
            return pVar;
        }
        int i = 0;
        int i2 = 0;
        for (int i3 = 0; i3 < 6; i3++) {
            i2 = a(i3, 8, i2);
        }
        int a2 = a(8, 7, a(8, 8, a(7, 8, i2)));
        for (int i4 = 5; i4 >= 0; i4--) {
            a2 = a(8, i4, a2);
        }
        int i5 = this.f3102a.f2969b;
        int i6 = i5 - 7;
        for (int i7 = i5 - 1; i7 >= i6; i7--) {
            i = a(8, i7, i);
        }
        for (int i8 = i5 - 8; i8 < i5; i8++) {
            i = a(i8, 8, i);
        }
        this.c = p.b(a2, i);
        p pVar2 = this.c;
        if (pVar2 != null) {
            return pVar2;
        }
        throw FormatException.a();
    }

    /* access modifiers changed from: package-private */
    public final s b() throws FormatException {
        s sVar = this.f3103b;
        if (sVar != null) {
            return sVar;
        }
        int i = this.f3102a.f2969b;
        int i2 = (i - 17) / 4;
        if (i2 <= 6) {
            return s.b(i2);
        }
        int i3 = i - 11;
        int i4 = 0;
        int i5 = 0;
        for (int i6 = 5; i6 >= 0; i6--) {
            for (int i7 = i - 9; i7 >= i3; i7--) {
                i5 = a(i7, i6, i5);
            }
        }
        s c2 = s.c(i5);
        if (c2 == null || c2.a() != i) {
            for (int i8 = 5; i8 >= 0; i8--) {
                for (int i9 = i - 9; i9 >= i3; i9--) {
                    i4 = a(i8, i9, i4);
                }
            }
            s c3 = s.c(i4);
            if (c3 == null || c3.a() != i) {
                throw FormatException.a();
            }
            this.f3103b = c3;
            return c3;
        }
        this.f3103b = c2;
        return c2;
    }

    private int a(int i, int i2, int i3) {
        return this.d ? this.f3102a.a(i2, i) : this.f3102a.a(i, i2) ? (i3 << 1) | 1 : i3 << 1;
    }

    /* access modifiers changed from: package-private */
    public final byte[] c() throws FormatException {
        int i;
        p a2 = a();
        s b2 = b();
        c cVar = c.values()[a2.f3114b];
        int i2 = this.f3102a.f2969b;
        cVar.a(this.f3102a, i2);
        int a3 = b2.a();
        b bVar = new b(a3);
        bVar.a(0, 0, 9, 9);
        int i3 = a3 - 8;
        bVar.a(i3, 0, 8, 9);
        bVar.a(0, i3, 9, 8);
        int length = b2.f3119b.length;
        int i4 = 0;
        while (true) {
            i = 2;
            if (i4 >= length) {
                break;
            }
            int i5 = b2.f3119b[i4] - 2;
            for (int i6 = 0; i6 < length; i6++) {
                if (!((i4 == 0 && (i6 == 0 || i6 == length - 1)) || (i4 == length - 1 && i6 == 0))) {
                    bVar.a(b2.f3119b[i6] - 2, i5, 5, 5);
                }
            }
            i4++;
        }
        int i7 = a3 - 17;
        int i8 = 6;
        bVar.a(6, 9, 1, i7);
        bVar.a(9, 6, i7, 1);
        if (b2.f3118a > 6) {
            int i9 = a3 - 11;
            bVar.a(i9, 0, 3, 6);
            bVar.a(0, i9, 6, 3);
        }
        byte[] bArr = new byte[b2.c];
        int i10 = i2 - 1;
        int i11 = i10;
        int i12 = 0;
        boolean z = true;
        int i13 = 0;
        int i14 = 0;
        while (i11 > 0) {
            if (i11 == i8) {
                i11--;
            }
            int i15 = i14;
            int i16 = i13;
            int i17 = i12;
            int i18 = 0;
            while (i18 < i2) {
                int i19 = z ? i10 - i18 : i18;
                int i20 = i15;
                int i21 = i16;
                int i22 = i17;
                int i23 = 0;
                while (i23 < i) {
                    int i24 = i11 - i23;
                    if (!bVar.a(i24, i19)) {
                        i21++;
                        int i25 = i20 << 1;
                        int i26 = this.f3102a.a(i24, i19) ? i25 | 1 : i25;
                        if (i21 == 8) {
                            bArr[i22] = (byte) i26;
                            i22++;
                            i21 = 0;
                            i20 = 0;
                        } else {
                            i20 = i26;
                        }
                    }
                    i23++;
                    i = 2;
                }
                i18++;
                i17 = i22;
                i16 = i21;
                i15 = i20;
                i = 2;
            }
            z = !z;
            i11 -= 2;
            i12 = i17;
            i13 = i16;
            i14 = i15;
            i8 = 6;
            i = 2;
        }
        if (i12 == b2.c) {
            return bArr;
        }
        throw FormatException.a();
    }
}
