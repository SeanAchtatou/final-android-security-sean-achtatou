package com.google.zxing.f.a;

import com.google.zxing.f.a.s;

/* compiled from: DataBlock */
final class b {

    /* renamed from: a  reason: collision with root package name */
    final int f3104a;

    /* renamed from: b  reason: collision with root package name */
    final byte[] f3105b;

    private b(int i, byte[] bArr) {
        this.f3104a = i;
        this.f3105b = bArr;
    }

    static b[] a(byte[] bArr, s sVar, o oVar) {
        if (bArr.length == sVar.c) {
            s.b a2 = sVar.a(oVar);
            s.a[] aVarArr = a2.f3123b;
            int i = 0;
            for (s.a aVar : aVarArr) {
                i += aVar.f3120a;
            }
            b[] bVarArr = new b[i];
            int length = aVarArr.length;
            int i2 = 0;
            int i3 = 0;
            while (i2 < length) {
                s.a aVar2 = aVarArr[i2];
                int i4 = i3;
                int i5 = 0;
                while (i5 < aVar2.f3120a) {
                    int i6 = aVar2.f3121b;
                    bVarArr[i4] = new b(i6, new byte[(a2.f3122a + i6)]);
                    i5++;
                    i4++;
                }
                i2++;
                i3 = i4;
            }
            int length2 = bVarArr[0].f3105b.length;
            int length3 = bVarArr.length - 1;
            while (length3 >= 0 && bVarArr[length3].f3105b.length != length2) {
                length3--;
            }
            int i7 = length3 + 1;
            int i8 = length2 - a2.f3122a;
            int i9 = 0;
            int i10 = 0;
            while (i9 < i8) {
                int i11 = i10;
                int i12 = 0;
                while (i12 < i3) {
                    bVarArr[i12].f3105b[i9] = bArr[i11];
                    i12++;
                    i11++;
                }
                i9++;
                i10 = i11;
            }
            int i13 = i7;
            while (i13 < i3) {
                bVarArr[i13].f3105b[i8] = bArr[i10];
                i13++;
                i10++;
            }
            int length4 = bVarArr[0].f3105b.length;
            while (i8 < length4) {
                int i14 = i10;
                int i15 = 0;
                while (i15 < i3) {
                    bVarArr[i15].f3105b[i15 < i7 ? i8 : i8 + 1] = bArr[i14];
                    i15++;
                    i14++;
                }
                i8++;
                i10 = i14;
            }
            return bVarArr;
        }
        throw new IllegalArgumentException();
    }
}
