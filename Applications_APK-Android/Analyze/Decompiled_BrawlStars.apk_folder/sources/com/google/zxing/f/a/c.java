package com.google.zxing.f.a;

import com.google.zxing.common.b;

/* compiled from: DataMask */
enum c {
    DATA_MASK_000,
    DATA_MASK_001,
    DATA_MASK_010,
    DATA_MASK_011,
    DATA_MASK_100,
    DATA_MASK_101,
    DATA_MASK_110,
    DATA_MASK_111;

    /* access modifiers changed from: package-private */
    public abstract boolean a(int i2, int i3);

    /* access modifiers changed from: package-private */
    public final void a(b bVar, int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            for (int i4 = 0; i4 < i2; i4++) {
                if (a(i3, i4)) {
                    bVar.c(i4, i3);
                }
            }
        }
    }
}
