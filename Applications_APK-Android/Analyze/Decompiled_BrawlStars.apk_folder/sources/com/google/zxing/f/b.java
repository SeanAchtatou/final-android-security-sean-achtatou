package com.google.zxing.f;

import com.google.zxing.WriterException;
import com.google.zxing.a;
import com.google.zxing.f;
import com.google.zxing.f.a.o;
import com.google.zxing.f.c.c;
import com.google.zxing.f.c.g;
import com.google.zxing.r;
import java.util.Map;

/* compiled from: QRCodeWriter */
public final class b implements r {
    public final com.google.zxing.common.b a(String str, a aVar, int i, int i2, Map<f, ?> map) throws WriterException {
        if (str.isEmpty()) {
            throw new IllegalArgumentException("Found empty contents");
        } else if (aVar != a.QR_CODE) {
            throw new IllegalArgumentException("Can only encode QR_CODE, but got " + aVar);
        } else if (i < 0 || i2 < 0) {
            throw new IllegalArgumentException("Requested dimensions are too small: " + i + 'x' + i2);
        } else {
            o oVar = o.f3111a;
            int i3 = 4;
            if (map != null) {
                if (map.containsKey(f.ERROR_CORRECTION)) {
                    oVar = o.valueOf(map.get(f.ERROR_CORRECTION).toString());
                }
                if (map.containsKey(f.MARGIN)) {
                    i3 = Integer.parseInt(map.get(f.MARGIN).toString());
                }
            }
            return a(c.a(str, oVar, map), i, i2, i3);
        }
    }

    private static com.google.zxing.common.b a(g gVar, int i, int i2, int i3) {
        com.google.zxing.f.c.b bVar = gVar.e;
        if (bVar != null) {
            int i4 = bVar.f3137b;
            int i5 = bVar.c;
            int i6 = i3 << 1;
            int i7 = i4 + i6;
            int i8 = i6 + i5;
            int max = Math.max(i, i7);
            int max2 = Math.max(i2, i8);
            int min = Math.min(max / i7, max2 / i8);
            int i9 = (max - (i4 * min)) / 2;
            int i10 = (max2 - (i5 * min)) / 2;
            com.google.zxing.common.b bVar2 = new com.google.zxing.common.b(max, max2);
            int i11 = 0;
            while (i11 < i5) {
                int i12 = i9;
                int i13 = 0;
                while (i13 < i4) {
                    if (bVar.a(i13, i11) == 1) {
                        bVar2.a(i12, i10, min, min);
                    }
                    i13++;
                    i12 += min;
                }
                i11++;
                i10 += min;
            }
            return bVar2;
        }
        throw new IllegalStateException();
    }
}
