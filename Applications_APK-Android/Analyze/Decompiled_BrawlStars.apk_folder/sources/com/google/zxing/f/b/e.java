package com.google.zxing.f.b;

import com.google.zxing.NotFoundException;
import com.google.zxing.d;
import com.google.zxing.p;
import com.google.zxing.q;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: FinderPatternFinder */
public class e {

    /* renamed from: a  reason: collision with root package name */
    private final com.google.zxing.common.b f3128a;

    /* renamed from: b  reason: collision with root package name */
    private final List<d> f3129b = new ArrayList();
    private boolean c;
    private final int[] d = new int[5];
    private final q e;

    public e(com.google.zxing.common.b bVar, q qVar) {
        this.f3128a = bVar;
        this.e = qVar;
    }

    /* access modifiers changed from: package-private */
    public final f a(Map<d, ?> map) throws NotFoundException {
        int i;
        boolean z = map != null && map.containsKey(d.TRY_HARDER);
        int i2 = this.f3128a.f2969b;
        int i3 = this.f3128a.f2968a;
        int i4 = (i2 * 3) / 388;
        if (i4 < 3 || z) {
            i4 = 3;
        }
        int[] iArr = new int[5];
        int i5 = i4 - 1;
        int i6 = i4;
        boolean z2 = false;
        while (i5 < i2 && !z2) {
            b(iArr);
            boolean z3 = z2;
            int i7 = i6;
            int i8 = 0;
            int i9 = 0;
            while (i8 < i3) {
                if (this.f3128a.a(i8, i5)) {
                    if ((i9 & 1) == 1) {
                        i9++;
                    }
                    iArr[i9] = iArr[i9] + 1;
                } else if ((i9 & 1) != 0) {
                    iArr[i9] = iArr[i9] + 1;
                } else if (i9 == 4) {
                    if (!a(iArr)) {
                        c(iArr);
                    } else if (a(iArr, i5, i8)) {
                        if (this.c) {
                            z3 = b();
                        } else {
                            if (this.f3129b.size() > 1) {
                                d dVar = null;
                                Iterator<d> it = this.f3129b.iterator();
                                while (true) {
                                    if (!it.hasNext()) {
                                        break;
                                    }
                                    d next = it.next();
                                    if (next.d >= 2) {
                                        if (dVar != null) {
                                            this.c = true;
                                            i = ((int) (Math.abs(dVar.f3153a - next.f3153a) - Math.abs(dVar.f3154b - next.f3154b))) / 2;
                                            break;
                                        }
                                        dVar = next;
                                    }
                                }
                            }
                            i = 0;
                            if (i > iArr[2]) {
                                i5 += (i - iArr[2]) - 2;
                                i8 = i3 - 1;
                            }
                        }
                        b(iArr);
                        i9 = 0;
                        i7 = 2;
                    } else {
                        c(iArr);
                    }
                    i9 = 3;
                } else {
                    i9++;
                    iArr[i9] = iArr[i9] + 1;
                }
                i8++;
            }
            if (!a(iArr) || !a(iArr, i5, i3)) {
                i6 = i7;
            } else {
                int i10 = iArr[0];
                if (this.c) {
                    i6 = i10;
                    z2 = b();
                    i5 += i6;
                } else {
                    i6 = i10;
                }
            }
            z2 = z3;
            i5 += i6;
        }
        int size = this.f3129b.size();
        if (size >= 3) {
            float f = 0.0f;
            if (size > 3) {
                float f2 = 0.0f;
                float f3 = 0.0f;
                for (d dVar2 : this.f3129b) {
                    float f4 = dVar2.c;
                    f2 += f4;
                    f3 += f4 * f4;
                }
                float f5 = (float) size;
                float f6 = f2 / f5;
                float sqrt = (float) Math.sqrt((double) ((f3 / f5) - (f6 * f6)));
                Collections.sort(this.f3129b, new b(f6, (byte) 0));
                float max = Math.max(0.2f * f6, sqrt);
                int i11 = 0;
                while (i11 < this.f3129b.size() && this.f3129b.size() > 3) {
                    if (Math.abs(this.f3129b.get(i11).c - f6) > max) {
                        this.f3129b.remove(i11);
                        i11--;
                    }
                    i11++;
                }
            }
            if (this.f3129b.size() > 3) {
                for (d dVar3 : this.f3129b) {
                    f += dVar3.c;
                }
                Collections.sort(this.f3129b, new a(f / ((float) this.f3129b.size()), (byte) 0));
                List<d> list = this.f3129b;
                list.subList(3, list.size()).clear();
            }
            d[] dVarArr = {this.f3129b.get(0), this.f3129b.get(1), this.f3129b.get(2)};
            p.a(dVarArr);
            return new f(dVarArr);
        }
        throw NotFoundException.a();
    }

    private static float a(int[] iArr, int i) {
        return ((float) ((i - iArr[4]) - iArr[3])) - (((float) iArr[2]) / 2.0f);
    }

    private static boolean a(int[] iArr) {
        int i = 0;
        for (int i2 = 0; i2 < 5; i2++) {
            int i3 = iArr[i2];
            if (i3 == 0) {
                return false;
            }
            i += i3;
        }
        if (i < 7) {
            return false;
        }
        float f = ((float) i) / 7.0f;
        float f2 = f / 2.0f;
        if (Math.abs(f - ((float) iArr[0])) >= f2 || Math.abs(f - ((float) iArr[1])) >= f2 || Math.abs((f * 3.0f) - ((float) iArr[2])) >= 3.0f * f2 || Math.abs(f - ((float) iArr[3])) >= f2 || Math.abs(f - ((float) iArr[4])) >= f2) {
            return false;
        }
        return true;
    }

    private int[] a() {
        b(this.d);
        return this.d;
    }

    private static void b(int[] iArr) {
        for (int i = 0; i < iArr.length; i++) {
            iArr[i] = 0;
        }
    }

    private static void c(int[] iArr) {
        iArr[0] = iArr[2];
        iArr[1] = iArr[3];
        iArr[2] = iArr[4];
        iArr[3] = 1;
        iArr[4] = 0;
    }

    private boolean a(int i, int i2) {
        int i3;
        int i4;
        int i5;
        int[] a2 = a();
        int i6 = 0;
        while (i >= i6 && i2 >= i6 && this.f3128a.a(i2 - i6, i - i6)) {
            a2[2] = a2[2] + 1;
            i6++;
        }
        if (a2[2] == 0) {
            return false;
        }
        while (i >= i6 && i2 >= i6 && !this.f3128a.a(i2 - i6, i - i6)) {
            a2[1] = a2[1] + 1;
            i6++;
        }
        if (a2[1] == 0) {
            return false;
        }
        while (i >= i6 && i2 >= i6 && this.f3128a.a(i2 - i6, i - i6)) {
            a2[0] = a2[0] + 1;
            i6++;
        }
        if (a2[0] == 0) {
            return false;
        }
        int i7 = this.f3128a.f2969b;
        int i8 = this.f3128a.f2968a;
        int i9 = 1;
        while (true) {
            int i10 = i + i9;
            if (i10 < i7 && (i5 = i2 + i9) < i8 && this.f3128a.a(i5, i10)) {
                a2[2] = a2[2] + 1;
                i9++;
            }
        }
        while (true) {
            int i11 = i + i9;
            if (i11 < i7 && (i4 = i2 + i9) < i8 && !this.f3128a.a(i4, i11)) {
                a2[3] = a2[3] + 1;
                i9++;
            }
        }
        if (a2[3] == 0) {
            return false;
        }
        while (true) {
            int i12 = i + i9;
            if (i12 < i7 && (i3 = i2 + i9) < i8 && this.f3128a.a(i3, i12)) {
                a2[4] = a2[4] + 1;
                i9++;
            }
        }
        if (a2[4] == 0) {
            return false;
        }
        int i13 = 0;
        for (int i14 = 0; i14 < 5; i14++) {
            int i15 = a2[i14];
            if (i15 == 0) {
                return false;
            }
            i13 += i15;
        }
        if (i13 < 7) {
            return false;
        }
        float f = ((float) i13) / 7.0f;
        float f2 = f / 1.333f;
        if (Math.abs(f - ((float) a2[0])) >= f2 || Math.abs(f - ((float) a2[1])) >= f2 || Math.abs((f * 3.0f) - ((float) a2[2])) >= 3.0f * f2 || Math.abs(f - ((float) a2[3])) >= f2 || Math.abs(f - ((float) a2[4])) >= f2) {
            return false;
        }
        return true;
    }

    private float a(int i, int i2, int i3, int i4) {
        com.google.zxing.common.b bVar = this.f3128a;
        int i5 = bVar.f2969b;
        int[] a2 = a();
        int i6 = i;
        while (i6 >= 0 && bVar.a(i2, i6)) {
            a2[2] = a2[2] + 1;
            i6--;
        }
        if (i6 < 0) {
            return Float.NaN;
        }
        while (i6 >= 0 && !bVar.a(i2, i6) && a2[1] <= i3) {
            a2[1] = a2[1] + 1;
            i6--;
        }
        if (i6 >= 0 && a2[1] <= i3) {
            while (i6 >= 0 && bVar.a(i2, i6) && a2[0] <= i3) {
                a2[0] = a2[0] + 1;
                i6--;
            }
            if (a2[0] > i3) {
                return Float.NaN;
            }
            int i7 = i + 1;
            while (i7 < i5 && bVar.a(i2, i7)) {
                a2[2] = a2[2] + 1;
                i7++;
            }
            if (i7 == i5) {
                return Float.NaN;
            }
            while (i7 < i5 && !bVar.a(i2, i7) && a2[3] < i3) {
                a2[3] = a2[3] + 1;
                i7++;
            }
            if (i7 != i5 && a2[3] < i3) {
                while (i7 < i5 && bVar.a(i2, i7) && a2[4] < i3) {
                    a2[4] = a2[4] + 1;
                    i7++;
                }
                if (a2[4] < i3 && Math.abs(((((a2[0] + a2[1]) + a2[2]) + a2[3]) + a2[4]) - i4) * 5 < i4 * 2 && a(a2)) {
                    return a(a2, i7);
                }
            }
        }
        return Float.NaN;
    }

    private float b(int i, int i2, int i3, int i4) {
        com.google.zxing.common.b bVar = this.f3128a;
        int i5 = bVar.f2968a;
        int[] a2 = a();
        int i6 = i;
        while (i6 >= 0 && bVar.a(i6, i2)) {
            a2[2] = a2[2] + 1;
            i6--;
        }
        if (i6 < 0) {
            return Float.NaN;
        }
        while (i6 >= 0 && !bVar.a(i6, i2) && a2[1] <= i3) {
            a2[1] = a2[1] + 1;
            i6--;
        }
        if (i6 >= 0 && a2[1] <= i3) {
            while (i6 >= 0 && bVar.a(i6, i2) && a2[0] <= i3) {
                a2[0] = a2[0] + 1;
                i6--;
            }
            if (a2[0] > i3) {
                return Float.NaN;
            }
            int i7 = i + 1;
            while (i7 < i5 && bVar.a(i7, i2)) {
                a2[2] = a2[2] + 1;
                i7++;
            }
            if (i7 == i5) {
                return Float.NaN;
            }
            while (i7 < i5 && !bVar.a(i7, i2) && a2[3] < i3) {
                a2[3] = a2[3] + 1;
                i7++;
            }
            if (i7 != i5 && a2[3] < i3) {
                while (i7 < i5 && bVar.a(i7, i2) && a2[4] < i3) {
                    a2[4] = a2[4] + 1;
                    i7++;
                }
                if (a2[4] < i3 && Math.abs(((((a2[0] + a2[1]) + a2[2]) + a2[3]) + a2[4]) - i4) * 5 < i4 && a(a2)) {
                    return a(a2, i7);
                }
            }
        }
        return Float.NaN;
    }

    private boolean a(int[] iArr, int i, int i2) {
        boolean z = false;
        int i3 = iArr[0] + iArr[1] + iArr[2] + iArr[3] + iArr[4];
        int a2 = (int) a(iArr, i2);
        float a3 = a(i, a2, iArr[2], i3);
        if (!Float.isNaN(a3)) {
            int i4 = (int) a3;
            float b2 = b(a2, i4, iArr[2], i3);
            if (!Float.isNaN(b2) && a(i4, (int) b2)) {
                float f = ((float) i3) / 7.0f;
                int i5 = 0;
                while (true) {
                    if (i5 >= this.f3129b.size()) {
                        break;
                    }
                    d dVar = this.f3129b.get(i5);
                    if (dVar.a(f, a3, b2)) {
                        this.f3129b.set(i5, dVar.b(a3, b2, f));
                        z = true;
                        break;
                    }
                    i5++;
                }
                if (!z) {
                    d dVar2 = new d(b2, a3, f);
                    this.f3129b.add(dVar2);
                    q qVar = this.e;
                    if (qVar != null) {
                        qVar.a(dVar2);
                    }
                }
                return true;
            }
        }
        return false;
    }

    private boolean b() {
        int size = this.f3129b.size();
        float f = 0.0f;
        int i = 0;
        float f2 = 0.0f;
        for (d next : this.f3129b) {
            if (next.d >= 2) {
                i++;
                f2 += next.c;
            }
        }
        if (i < 3) {
            return false;
        }
        float f3 = f2 / ((float) size);
        for (d dVar : this.f3129b) {
            f += Math.abs(dVar.c - f3);
        }
        if (f <= f2 * 0.05f) {
            return true;
        }
        return false;
    }

    /* compiled from: FinderPatternFinder */
    static final class b implements Serializable, Comparator<d> {

        /* renamed from: a  reason: collision with root package name */
        private final float f3131a;

        /* synthetic */ b(float f, byte b2) {
            this(f);
        }

        public final /* synthetic */ int compare(Object obj, Object obj2) {
            return Float.compare(Math.abs(((d) obj2).c - this.f3131a), Math.abs(((d) obj).c - this.f3131a));
        }

        private b(float f) {
            this.f3131a = f;
        }
    }

    /* compiled from: FinderPatternFinder */
    static final class a implements Serializable, Comparator<d> {

        /* renamed from: a  reason: collision with root package name */
        private final float f3130a;

        /* synthetic */ a(float f, byte b2) {
            this(f);
        }

        public final /* synthetic */ int compare(Object obj, Object obj2) {
            d dVar = (d) obj;
            d dVar2 = (d) obj2;
            int compare = Integer.compare(dVar2.d, dVar.d);
            return compare == 0 ? Float.compare(Math.abs(dVar.c - this.f3130a), Math.abs(dVar2.c - this.f3130a)) : compare;
        }

        private a(float f) {
            this.f3130a = f;
        }
    }
}
