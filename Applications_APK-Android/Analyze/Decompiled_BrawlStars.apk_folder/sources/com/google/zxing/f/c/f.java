package com.google.zxing.f.c;

import com.google.zxing.WriterException;
import com.google.zxing.common.a;
import com.google.zxing.f.a.o;
import com.google.zxing.f.a.s;
import java.util.Arrays;

/* compiled from: MatrixUtil */
final class f {

    /* renamed from: a  reason: collision with root package name */
    private static final int[][] f3140a = {new int[]{1, 1, 1, 1, 1, 1, 1}, new int[]{1, 0, 0, 0, 0, 0, 1}, new int[]{1, 0, 1, 1, 1, 0, 1}, new int[]{1, 0, 1, 1, 1, 0, 1}, new int[]{1, 0, 1, 1, 1, 0, 1}, new int[]{1, 0, 0, 0, 0, 0, 1}, new int[]{1, 1, 1, 1, 1, 1, 1}};

    /* renamed from: b  reason: collision with root package name */
    private static final int[][] f3141b = {new int[]{1, 1, 1, 1, 1}, new int[]{1, 0, 0, 0, 1}, new int[]{1, 0, 1, 0, 1}, new int[]{1, 0, 0, 0, 1}, new int[]{1, 1, 1, 1, 1}};
    private static final int[][] c = {new int[]{-1, -1, -1, -1, -1, -1, -1}, new int[]{6, 18, -1, -1, -1, -1, -1}, new int[]{6, 22, -1, -1, -1, -1, -1}, new int[]{6, 26, -1, -1, -1, -1, -1}, new int[]{6, 30, -1, -1, -1, -1, -1}, new int[]{6, 34, -1, -1, -1, -1, -1}, new int[]{6, 22, 38, -1, -1, -1, -1}, new int[]{6, 24, 42, -1, -1, -1, -1}, new int[]{6, 26, 46, -1, -1, -1, -1}, new int[]{6, 28, 50, -1, -1, -1, -1}, new int[]{6, 30, 54, -1, -1, -1, -1}, new int[]{6, 32, 58, -1, -1, -1, -1}, new int[]{6, 34, 62, -1, -1, -1, -1}, new int[]{6, 26, 46, 66, -1, -1, -1}, new int[]{6, 26, 48, 70, -1, -1, -1}, new int[]{6, 26, 50, 74, -1, -1, -1}, new int[]{6, 30, 54, 78, -1, -1, -1}, new int[]{6, 30, 56, 82, -1, -1, -1}, new int[]{6, 30, 58, 86, -1, -1, -1}, new int[]{6, 34, 62, 90, -1, -1, -1}, new int[]{6, 28, 50, 72, 94, -1, -1}, new int[]{6, 26, 50, 74, 98, -1, -1}, new int[]{6, 30, 54, 78, 102, -1, -1}, new int[]{6, 28, 54, 80, 106, -1, -1}, new int[]{6, 32, 58, 84, 110, -1, -1}, new int[]{6, 30, 58, 86, 114, -1, -1}, new int[]{6, 34, 62, 90, 118, -1, -1}, new int[]{6, 26, 50, 74, 98, 122, -1}, new int[]{6, 30, 54, 78, 102, 126, -1}, new int[]{6, 26, 52, 78, 104, 130, -1}, new int[]{6, 30, 56, 82, 108, 134, -1}, new int[]{6, 34, 60, 86, 112, 138, -1}, new int[]{6, 30, 58, 86, 114, 142, -1}, new int[]{6, 34, 62, 90, 118, 146, -1}, new int[]{6, 30, 54, 78, 102, 126, 150}, new int[]{6, 24, 50, 76, 102, 128, 154}, new int[]{6, 28, 54, 80, 106, 132, 158}, new int[]{6, 32, 58, 84, 110, 136, 162}, new int[]{6, 26, 54, 82, 110, 138, 166}, new int[]{6, 30, 58, 86, 114, 142, 170}};
    private static final int[][] d = {new int[]{8, 0}, new int[]{8, 1}, new int[]{8, 2}, new int[]{8, 3}, new int[]{8, 4}, new int[]{8, 5}, new int[]{8, 7}, new int[]{8, 8}, new int[]{7, 8}, new int[]{5, 8}, new int[]{4, 8}, new int[]{3, 8}, new int[]{2, 8}, new int[]{1, 8}, new int[]{0, 8}};

    private static boolean a(int i) {
        return i == -1;
    }

    private static void a(int i, int i2, b bVar) throws WriterException {
        int i3 = 0;
        while (i3 < 8) {
            int i4 = i + i3;
            if (a(bVar.a(i4, i2))) {
                bVar.a(i4, i2, 0);
                i3++;
            } else {
                throw new WriterException();
            }
        }
    }

    private static void b(int i, int i2, b bVar) throws WriterException {
        int i3 = 0;
        while (i3 < 7) {
            int i4 = i2 + i3;
            if (a(bVar.a(i, i4))) {
                bVar.a(i, i4, 0);
                i3++;
            } else {
                throw new WriterException();
            }
        }
    }

    private static void c(int i, int i2, b bVar) {
        for (int i3 = 0; i3 < 7; i3++) {
            int[] iArr = f3140a[i3];
            for (int i4 = 0; i4 < 7; i4++) {
                bVar.a(i + i4, i2 + i3, iArr[i4]);
            }
        }
    }

    static void a(a aVar, o oVar, s sVar, int i, b bVar) throws WriterException {
        s sVar2 = sVar;
        int i2 = i;
        b bVar2 = bVar;
        int i3 = 0;
        for (byte[] fill : bVar2.f3136a) {
            Arrays.fill(fill, (byte) -1);
        }
        int length = f3140a[0].length;
        c(0, 0, bVar2);
        c(bVar2.f3137b - length, 0, bVar2);
        c(0, bVar2.f3137b - length, bVar2);
        a(0, 7, bVar2);
        a(bVar2.f3137b - 8, 7, bVar2);
        a(0, bVar2.f3137b - 8, bVar2);
        b(7, 0, bVar2);
        b((bVar2.c - 7) - 1, 0, bVar2);
        b(7, bVar2.c - 7, bVar2);
        if (bVar2.a(8, bVar2.c - 8) != 0) {
            bVar2.a(8, bVar2.c - 8, 1);
            int i4 = 5;
            if (sVar2.f3118a >= 2) {
                int[] iArr = c[sVar2.f3118a - 1];
                int length2 = iArr.length;
                int i5 = 0;
                while (i5 < length2) {
                    int i6 = iArr[i5];
                    if (i6 >= 0) {
                        int length3 = iArr.length;
                        int i7 = 0;
                        while (i7 < length3) {
                            int i8 = iArr[i7];
                            if (i8 >= 0 && a(bVar2.a(i8, i6))) {
                                int i9 = i8 - 2;
                                int i10 = i6 - 2;
                                int i11 = 0;
                                while (i11 < i4) {
                                    int[] iArr2 = f3141b[i11];
                                    while (i3 < i4) {
                                        bVar2.a(i9 + i3, i10 + i11, iArr2[i3]);
                                        i3++;
                                        i4 = 5;
                                    }
                                    i11++;
                                    i3 = 0;
                                    i4 = 5;
                                }
                            }
                            i7++;
                            i3 = 0;
                            i4 = 5;
                        }
                    }
                    i5++;
                    i3 = 0;
                    i4 = 5;
                }
            }
            int i12 = 8;
            while (i12 < bVar2.f3137b - 8) {
                int i13 = i12 + 1;
                int i14 = i13 % 2;
                if (a(bVar2.a(i12, 6))) {
                    bVar2.a(i12, 6, i14);
                }
                if (a(bVar2.a(6, i12))) {
                    bVar2.a(6, i12, i14);
                }
                i12 = i13;
            }
            a aVar2 = new a();
            if (i2 >= 0 && i2 < 8) {
                int i15 = (oVar.e << 3) | i2;
                aVar2.a(i15, 5);
                aVar2.a(a(i15, 1335), 10);
                a aVar3 = new a();
                aVar3.a(21522, 15);
                if (aVar2.f2965b == aVar3.f2965b) {
                    for (int i16 = 0; i16 < aVar2.f2964a.length; i16++) {
                        int[] iArr3 = aVar2.f2964a;
                        iArr3[i16] = iArr3[i16] ^ aVar3.f2964a[i16];
                    }
                    if (aVar2.f2965b == 15) {
                        for (int i17 = 0; i17 < aVar2.f2965b; i17++) {
                            boolean a2 = aVar2.a((aVar2.f2965b - 1) - i17);
                            int[] iArr4 = d[i17];
                            bVar2.a(iArr4[0], iArr4[1], a2);
                            if (i17 < 8) {
                                bVar2.a((bVar2.f3137b - i17) - 1, 8, a2);
                            } else {
                                bVar2.a(8, (bVar2.c - 7) + (i17 - 8), a2);
                            }
                        }
                        a(sVar2, bVar2);
                        a(aVar, i2, bVar2);
                        return;
                    }
                    throw new WriterException("should not happen but we got: " + aVar2.f2965b);
                }
                throw new IllegalArgumentException("Sizes don't match");
            }
            throw new WriterException("Invalid mask pattern");
        }
        throw new WriterException();
    }

    private static void a(s sVar, b bVar) throws WriterException {
        if (sVar.f3118a >= 7) {
            a aVar = new a();
            aVar.a(sVar.f3118a, 6);
            aVar.a(a(sVar.f3118a, 7973), 12);
            if (aVar.f2965b == 18) {
                int i = 0;
                int i2 = 17;
                while (i < 6) {
                    int i3 = i2;
                    for (int i4 = 0; i4 < 3; i4++) {
                        boolean a2 = aVar.a(i3);
                        i3--;
                        bVar.a(i, (bVar.c - 11) + i4, a2);
                        bVar.a((bVar.c - 11) + i4, i, a2);
                    }
                    i++;
                    i2 = i3;
                }
                return;
            }
            throw new WriterException("should not happen but we got: " + aVar.f2965b);
        }
    }

    private static void a(a aVar, int i, b bVar) throws WriterException {
        boolean z;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6 = bVar.f3137b - 1;
        int i7 = bVar.c - 1;
        int i8 = 0;
        int i9 = -1;
        while (i6 > 0) {
            if (i6 == 6) {
                i6--;
            }
            while (i7 >= 0 && i7 < bVar.c) {
                int i10 = i8;
                for (int i11 = 0; i11 < 2; i11++) {
                    int i12 = i6 - i11;
                    if (a(bVar.a(i12, i7))) {
                        if (i10 < aVar.f2965b) {
                            z = aVar.a(i10);
                            i10++;
                        } else {
                            z = false;
                        }
                        if (i != -1) {
                            switch (i) {
                                case 0:
                                    i3 = i7 + i12;
                                    i2 = i3 & 1;
                                    break;
                                case 1:
                                    i2 = i7 & 1;
                                    break;
                                case 2:
                                    i2 = i12 % 3;
                                    break;
                                case 3:
                                    i2 = (i7 + i12) % 3;
                                    break;
                                case 4:
                                    i5 = i7 / 2;
                                    i4 = i12 / 3;
                                    i3 = i5 + i4;
                                    i2 = i3 & 1;
                                    break;
                                case 5:
                                    int i13 = i7 * i12;
                                    i2 = (i13 % 3) + (i13 & 1);
                                    break;
                                case 6:
                                    int i14 = i7 * i12;
                                    i2 = ((i14 & 1) + (i14 % 3)) & 1;
                                    break;
                                case 7:
                                    i5 = (i7 * i12) % 3;
                                    i4 = (i7 + i12) & 1;
                                    i3 = i5 + i4;
                                    i2 = i3 & 1;
                                    break;
                                default:
                                    throw new IllegalArgumentException("Invalid mask pattern: " + i);
                            }
                            if (i2 == 0) {
                                z = !z;
                            }
                        }
                        bVar.a(i12, i7, z);
                    }
                }
                i7 += i9;
                i8 = i10;
            }
            i9 = -i9;
            i7 += i9;
            i6 -= 2;
        }
        if (i8 != aVar.f2965b) {
            throw new WriterException("Not all bits consumed: " + i8 + '/' + aVar.f2965b);
        }
    }

    private static int a(int i, int i2) {
        if (i2 != 0) {
            int numberOfLeadingZeros = 32 - Integer.numberOfLeadingZeros(i2);
            int i3 = i << (numberOfLeadingZeros - 1);
            while (32 - Integer.numberOfLeadingZeros(i3) >= numberOfLeadingZeros) {
                i3 ^= i2 << ((32 - Integer.numberOfLeadingZeros(i3)) - numberOfLeadingZeros);
            }
            return i3;
        }
        throw new IllegalArgumentException("0 polynomial");
    }
}
