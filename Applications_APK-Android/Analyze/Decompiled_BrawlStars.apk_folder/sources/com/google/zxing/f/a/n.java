package com.google.zxing.f.a;

import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.common.e;
import com.google.zxing.common.reedsolomon.ReedSolomonException;
import com.google.zxing.common.reedsolomon.a;
import com.google.zxing.common.reedsolomon.c;
import com.google.zxing.d;
import java.util.Map;

/* compiled from: Decoder */
public final class n {

    /* renamed from: a  reason: collision with root package name */
    private final c f3110a = new c(a.e);

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0015 A[Catch:{ ChecksumException | FormatException -> 0x006f }] */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0016 A[Catch:{ ChecksumException | FormatException -> 0x006f }] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x003d A[Catch:{ ChecksumException | FormatException -> 0x006f }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.zxing.common.e a(com.google.zxing.common.b r9, java.util.Map<com.google.zxing.d, ?> r10) throws com.google.zxing.FormatException, com.google.zxing.ChecksumException {
        /*
            r8 = this;
            com.google.zxing.f.a.a r0 = new com.google.zxing.f.a.a
            r0.<init>(r9)
            r9 = 0
            com.google.zxing.common.e r9 = r8.a(r0, r10)     // Catch:{ FormatException -> 0x000f, ChecksumException -> 0x000b }
            return r9
        L_0x000b:
            r1 = move-exception
            r2 = r1
            r1 = r9
            goto L_0x0011
        L_0x000f:
            r1 = move-exception
            r2 = r9
        L_0x0011:
            com.google.zxing.f.a.p r3 = r0.c     // Catch:{ FormatException -> 0x0071, ChecksumException -> 0x006f }
            if (r3 != 0) goto L_0x0016
            goto L_0x0029
        L_0x0016:
            com.google.zxing.f.a.c[] r3 = com.google.zxing.f.a.c.values()     // Catch:{ FormatException -> 0x0071, ChecksumException -> 0x006f }
            com.google.zxing.f.a.p r4 = r0.c     // Catch:{ FormatException -> 0x0071, ChecksumException -> 0x006f }
            byte r4 = r4.f3114b     // Catch:{ FormatException -> 0x0071, ChecksumException -> 0x006f }
            r3 = r3[r4]     // Catch:{ FormatException -> 0x0071, ChecksumException -> 0x006f }
            com.google.zxing.common.b r4 = r0.f3102a     // Catch:{ FormatException -> 0x0071, ChecksumException -> 0x006f }
            int r4 = r4.f2969b     // Catch:{ FormatException -> 0x0071, ChecksumException -> 0x006f }
            com.google.zxing.common.b r5 = r0.f3102a     // Catch:{ FormatException -> 0x0071, ChecksumException -> 0x006f }
            r3.a(r5, r4)     // Catch:{ FormatException -> 0x0071, ChecksumException -> 0x006f }
        L_0x0029:
            r0.f3103b = r9     // Catch:{ FormatException -> 0x0071, ChecksumException -> 0x006f }
            r0.c = r9     // Catch:{ FormatException -> 0x0071, ChecksumException -> 0x006f }
            r9 = 1
            r0.d = r9     // Catch:{ FormatException -> 0x0071, ChecksumException -> 0x006f }
            r0.b()     // Catch:{ FormatException -> 0x0071, ChecksumException -> 0x006f }
            r0.a()     // Catch:{ FormatException -> 0x0071, ChecksumException -> 0x006f }
            r3 = 0
        L_0x0037:
            com.google.zxing.common.b r4 = r0.f3102a     // Catch:{ FormatException -> 0x0071, ChecksumException -> 0x006f }
            int r4 = r4.f2968a     // Catch:{ FormatException -> 0x0071, ChecksumException -> 0x006f }
            if (r3 >= r4) goto L_0x0063
            int r4 = r3 + 1
            r5 = r4
        L_0x0040:
            com.google.zxing.common.b r6 = r0.f3102a     // Catch:{ FormatException -> 0x0071, ChecksumException -> 0x006f }
            int r6 = r6.f2969b     // Catch:{ FormatException -> 0x0071, ChecksumException -> 0x006f }
            if (r5 >= r6) goto L_0x0061
            com.google.zxing.common.b r6 = r0.f3102a     // Catch:{ FormatException -> 0x0071, ChecksumException -> 0x006f }
            boolean r6 = r6.a(r3, r5)     // Catch:{ FormatException -> 0x0071, ChecksumException -> 0x006f }
            com.google.zxing.common.b r7 = r0.f3102a     // Catch:{ FormatException -> 0x0071, ChecksumException -> 0x006f }
            boolean r7 = r7.a(r5, r3)     // Catch:{ FormatException -> 0x0071, ChecksumException -> 0x006f }
            if (r6 == r7) goto L_0x005e
            com.google.zxing.common.b r6 = r0.f3102a     // Catch:{ FormatException -> 0x0071, ChecksumException -> 0x006f }
            r6.c(r5, r3)     // Catch:{ FormatException -> 0x0071, ChecksumException -> 0x006f }
            com.google.zxing.common.b r6 = r0.f3102a     // Catch:{ FormatException -> 0x0071, ChecksumException -> 0x006f }
            r6.c(r3, r5)     // Catch:{ FormatException -> 0x0071, ChecksumException -> 0x006f }
        L_0x005e:
            int r5 = r5 + 1
            goto L_0x0040
        L_0x0061:
            r3 = r4
            goto L_0x0037
        L_0x0063:
            com.google.zxing.common.e r10 = r8.a(r0, r10)     // Catch:{ FormatException -> 0x0071, ChecksumException -> 0x006f }
            com.google.zxing.f.a.r r0 = new com.google.zxing.f.a.r     // Catch:{ FormatException -> 0x0071, ChecksumException -> 0x006f }
            r0.<init>(r9)     // Catch:{ FormatException -> 0x0071, ChecksumException -> 0x006f }
            r10.h = r0     // Catch:{ FormatException -> 0x0071, ChecksumException -> 0x006f }
            return r10
        L_0x006f:
            r9 = move-exception
            goto L_0x0072
        L_0x0071:
            r9 = move-exception
        L_0x0072:
            if (r1 != 0) goto L_0x0078
            if (r2 == 0) goto L_0x0077
            throw r2
        L_0x0077:
            throw r9
        L_0x0078:
            goto L_0x007a
        L_0x0079:
            throw r1
        L_0x007a:
            goto L_0x0079
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.f.a.n.a(com.google.zxing.common.b, java.util.Map):com.google.zxing.common.e");
    }

    private e a(a aVar, Map<d, ?> map) throws FormatException, ChecksumException {
        s b2 = aVar.b();
        o oVar = aVar.a().f3113a;
        b[] a2 = b.a(aVar.c(), b2, oVar);
        int i = 0;
        for (b bVar : a2) {
            i += bVar.f3104a;
        }
        byte[] bArr = new byte[i];
        int length = a2.length;
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            b bVar2 = a2[i2];
            byte[] bArr2 = bVar2.f3105b;
            int i4 = bVar2.f3104a;
            a(bArr2, i4);
            int i5 = i3;
            int i6 = 0;
            while (i6 < i4) {
                bArr[i5] = bArr2[i6];
                i6++;
                i5++;
            }
            i2++;
            i3 = i5;
        }
        return l.a(bArr, b2, oVar, map);
    }

    private void a(byte[] bArr, int i) throws ChecksumException {
        int length = bArr.length;
        int[] iArr = new int[length];
        for (int i2 = 0; i2 < length; i2++) {
            iArr[i2] = bArr[i2] & 255;
        }
        try {
            this.f3110a.a(iArr, bArr.length - i);
            for (int i3 = 0; i3 < i; i3++) {
                bArr[i3] = (byte) iArr[i3];
            }
        } catch (ReedSolomonException unused) {
            throw ChecksumException.a();
        }
    }
}
