package com.google.zxing.f.b;

import com.google.zxing.p;

/* compiled from: AlignmentPattern */
public final class a extends p {
    private final float c;

    a(float f, float f2, float f3) {
        super(f, f2);
        this.c = f3;
    }

    /* access modifiers changed from: package-private */
    public final boolean a(float f, float f2, float f3) {
        if (Math.abs(f2 - this.f3154b) > f || Math.abs(f3 - this.f3153a) > f) {
            return false;
        }
        float abs = Math.abs(f - this.c);
        if (abs <= 1.0f || abs <= this.c) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public final a b(float f, float f2, float f3) {
        return new a((this.f3153a + f2) / 2.0f, (this.f3154b + f) / 2.0f, (this.c + f3) / 2.0f);
    }
}
