package com.google.zxing.f.c;

import android.support.v7.widget.ActivityChooserView;
import com.google.zxing.WriterException;
import com.google.zxing.common.a;
import com.google.zxing.common.d;
import com.google.zxing.f;
import com.google.zxing.f.a.o;
import com.google.zxing.f.a.q;
import com.google.zxing.f.a.s;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Map;

/* compiled from: Encoder */
public final class c {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f3138a = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 36, -1, -1, -1, 37, 38, -1, -1, -1, -1, 39, 40, -1, 41, 42, 43, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 44, -1, -1, -1, -1, -1, -1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, -1, -1, -1, -1, -1};

    public static g a(String str, o oVar, Map<f, ?> map) throws WriterException {
        String str2;
        q qVar;
        s sVar;
        int i;
        d a2;
        int i2 = 0;
        boolean z = map != null && map.containsKey(f.CHARACTER_SET);
        if (z) {
            str2 = map.get(f.CHARACTER_SET).toString();
        } else {
            str2 = "ISO-8859-1";
        }
        if (!"Shift_JIS".equals(str2) || !a(str)) {
            int i3 = 0;
            boolean z2 = false;
            boolean z3 = false;
            while (true) {
                if (i3 < str.length()) {
                    char charAt = str.charAt(i3);
                    if (charAt < '0' || charAt > '9') {
                        if (a(charAt) == -1) {
                            qVar = q.BYTE;
                            break;
                        }
                        z2 = true;
                    } else {
                        z3 = true;
                    }
                    i3++;
                } else if (z2) {
                    qVar = q.ALPHANUMERIC;
                } else if (z3) {
                    qVar = q.NUMERIC;
                } else {
                    qVar = q.BYTE;
                }
            }
        } else {
            qVar = q.KANJI;
        }
        a aVar = new a();
        if (qVar == q.BYTE && ((z || !"ISO-8859-1".equals(str2)) && (a2 = d.a(str2)) != null)) {
            aVar.a(q.ECI.k, 4);
            aVar.a(a2.B[0], 8);
        }
        if ((map != null && map.containsKey(f.GS1_FORMAT)) && Boolean.valueOf(map.get(f.GS1_FORMAT).toString()).booleanValue()) {
            a(q.FNC1_FIRST_POSITION, aVar);
        }
        a(qVar, aVar);
        a aVar2 = new a();
        int i4 = d.f3139a[qVar.ordinal()];
        if (i4 == 1) {
            int length = str.length();
            while (i2 < length) {
                int charAt2 = str.charAt(i2) - '0';
                int i5 = i2 + 2;
                if (i5 < length) {
                    aVar2.a((charAt2 * 100) + ((str.charAt(i2 + 1) - '0') * 10) + (str.charAt(i5) - '0'), 10);
                    i = i2 + 3;
                } else {
                    i = i2 + 1;
                    if (i < length) {
                        aVar2.a((charAt2 * 10) + (str.charAt(i) - '0'), 7);
                        i = i5;
                    } else {
                        aVar2.a(charAt2, 4);
                    }
                }
            }
        } else if (i4 == 2) {
            int length2 = str.length();
            while (i2 < length2) {
                int a3 = a(str.charAt(i2));
                if (a3 != -1) {
                    int i6 = i2 + 1;
                    if (i6 < length2) {
                        int a4 = a(str.charAt(i6));
                        if (a4 != -1) {
                            aVar2.a((a3 * 45) + a4, 11);
                            i2 += 2;
                        } else {
                            throw new WriterException();
                        }
                    } else {
                        aVar2.a(a3, 6);
                        i2 = i6;
                    }
                } else {
                    throw new WriterException();
                }
            }
        } else if (i4 == 3) {
            a(str, aVar2, str2);
        } else if (i4 == 4) {
            a(str, aVar2);
        } else {
            throw new WriterException("Invalid mode: " + qVar);
        }
        if (map == null || !map.containsKey(f.QR_VERSION)) {
            sVar = a(a(qVar, aVar, aVar2, a(a(qVar, aVar, aVar2, s.b(1)), oVar)), oVar);
        } else {
            sVar = s.b(Integer.parseInt(map.get(f.QR_VERSION).toString()));
            if (!a(a(qVar, aVar, aVar2, sVar), sVar, oVar)) {
                throw new WriterException("Data too big for requested version");
            }
        }
        a aVar3 = new a();
        aVar3.a(aVar);
        int a5 = qVar == q.BYTE ? aVar2.a() : str.length();
        int a6 = qVar.a(sVar);
        int i7 = 1 << a6;
        if (a5 < i7) {
            aVar3.a(a5, a6);
            aVar3.a(aVar2);
            s.b a7 = sVar.a(oVar);
            int b2 = sVar.c - a7.b();
            a(b2, aVar3);
            a a8 = a(aVar3, sVar.c, b2, a7.a());
            g gVar = new g();
            gVar.f3143b = oVar;
            gVar.f3142a = qVar;
            gVar.c = sVar;
            int a9 = sVar.a();
            b bVar = new b(a9, a9);
            int a10 = a(a8, oVar, sVar, bVar);
            gVar.d = a10;
            f.a(a8, oVar, sVar, a10, bVar);
            gVar.e = bVar;
            return gVar;
        }
        throw new WriterException(a5 + " is bigger than " + (i7 - 1));
    }

    private static int a(int i) {
        int[] iArr = f3138a;
        if (i < iArr.length) {
            return iArr[i];
        }
        return -1;
    }

    private static boolean a(String str) {
        try {
            byte[] bytes = str.getBytes("Shift_JIS");
            int length = bytes.length;
            if (length % 2 != 0) {
                return false;
            }
            for (int i = 0; i < length; i += 2) {
                byte b2 = bytes[i] & 255;
                if ((b2 < 129 || b2 > 159) && (b2 < 224 || b2 > 235)) {
                    return false;
                }
            }
            return true;
        } catch (UnsupportedEncodingException unused) {
            return false;
        }
    }

    private static int a(a aVar, o oVar, s sVar, b bVar) throws WriterException {
        b bVar2 = bVar;
        boolean z = false;
        int i = -1;
        int i2 = 0;
        int i3 = ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
        while (i2 < 8) {
            f.a(aVar, oVar, sVar, i2, bVar2);
            int a2 = e.a(bVar2, true) + e.a(bVar2, z);
            byte[][] bArr = bVar2.f3136a;
            int i4 = bVar2.f3137b;
            int i5 = bVar2.c;
            int i6 = 0;
            int i7 = 0;
            while (i6 < i5 - 1) {
                byte[] bArr2 = bArr[i6];
                int i8 = i7;
                int i9 = 0;
                while (i9 < i4 - 1) {
                    byte b2 = bArr2[i9];
                    int i10 = i9 + 1;
                    if (b2 == bArr2[i10]) {
                        int i11 = i6 + 1;
                        if (b2 == bArr[i11][i9] && b2 == bArr[i11][i10]) {
                            i8++;
                        }
                    }
                    i9 = i10;
                }
                i6++;
                i7 = i8;
            }
            int i12 = a2 + (i7 * 3);
            byte[][] bArr3 = bVar2.f3136a;
            int i13 = bVar2.f3137b;
            int i14 = bVar2.c;
            int i15 = 0;
            int i16 = 0;
            while (i15 < i14) {
                int i17 = i16;
                for (int i18 = 0; i18 < i13; i18++) {
                    byte[] bArr4 = bArr3[i15];
                    int i19 = i18 + 6;
                    if (i19 < i13 && bArr4[i18] == 1 && bArr4[i18 + 1] == 0 && bArr4[i18 + 2] == 1 && bArr4[i18 + 3] == 1 && bArr4[i18 + 4] == 1 && bArr4[i18 + 5] == 0 && bArr4[i19] == 1 && (e.a(bArr4, i18 - 4, i18) || e.a(bArr4, i18 + 7, i18 + 11))) {
                        i17++;
                    }
                    int i20 = i15 + 6;
                    if (i20 < i14 && bArr3[i15][i18] == 1 && bArr3[i15 + 1][i18] == 0 && bArr3[i15 + 2][i18] == 1 && bArr3[i15 + 3][i18] == 1 && bArr3[i15 + 4][i18] == 1 && bArr3[i15 + 5][i18] == 0 && bArr3[i20][i18] == 1 && (e.a(bArr3, i18, i15 - 4, i15) || e.a(bArr3, i18, i15 + 7, i15 + 11))) {
                        i17++;
                    }
                }
                i15++;
                i16 = i17;
            }
            int i21 = i12 + (i16 * 40);
            byte[][] bArr5 = bVar2.f3136a;
            int i22 = bVar2.f3137b;
            int i23 = bVar2.c;
            int i24 = 0;
            int i25 = 0;
            while (i24 < i23) {
                byte[] bArr6 = bArr5[i24];
                int i26 = i25;
                for (int i27 = 0; i27 < i22; i27++) {
                    if (bArr6[i27] == 1) {
                        i26++;
                    }
                }
                i24++;
                i25 = i26;
            }
            int i28 = bVar2.c * bVar2.f3137b;
            int abs = i21 + (((Math.abs((i25 << 1) - i28) * 10) / i28) * 10);
            if (abs < i3) {
                i = i2;
                i3 = abs;
            }
            i2++;
            z = false;
        }
        return i;
    }

    private static s a(int i, o oVar) throws WriterException {
        for (int i2 = 1; i2 <= 40; i2++) {
            s b2 = s.b(i2);
            if (a(i, b2, oVar)) {
                return b2;
            }
        }
        throw new WriterException("Data too big");
    }

    private static a a(a aVar, int i, int i2, int i3) throws WriterException {
        int i4;
        int i5 = i;
        int i6 = i2;
        int i7 = i3;
        if (aVar.a() == i6) {
            ArrayList<a> arrayList = new ArrayList<>(i7);
            int i8 = 0;
            int i9 = 0;
            int i10 = 0;
            int i11 = 0;
            while (i8 < i7) {
                int[] iArr = new int[1];
                int[] iArr2 = new int[1];
                if (i8 < i7) {
                    int i12 = i5 % i7;
                    int i13 = i7 - i12;
                    int i14 = i5 / i7;
                    int i15 = i14 + 1;
                    int i16 = i6 / i7;
                    int i17 = i16 + 1;
                    int i18 = i14 - i16;
                    int i19 = i15 - i17;
                    if (i18 != i19) {
                        throw new WriterException("EC bytes mismatch");
                    } else if (i7 != i13 + i12) {
                        throw new WriterException("RS blocks mismatch");
                    } else if (i5 == ((i16 + i18) * i13) + ((i17 + i19) * i12)) {
                        if (i8 < i13) {
                            i4 = 0;
                            iArr[0] = i16;
                            iArr2[0] = i18;
                        } else {
                            i4 = 0;
                            iArr[0] = i17;
                            iArr2[0] = i19;
                        }
                        int i20 = iArr[i4];
                        byte[] bArr = new byte[i20];
                        aVar.a(i9 << 3, bArr, i4, i20);
                        byte[] a2 = a(bArr, iArr2[i4]);
                        arrayList.add(new a(bArr, a2));
                        i10 = Math.max(i10, i20);
                        i11 = Math.max(i11, a2.length);
                        i9 += iArr[i4];
                        i8++;
                    } else {
                        throw new WriterException("Total bytes mismatch");
                    }
                } else {
                    throw new WriterException("Block ID too large");
                }
            }
            if (i6 == i9) {
                a aVar2 = new a();
                for (int i21 = 0; i21 < i10; i21++) {
                    for (a aVar3 : arrayList) {
                        byte[] bArr2 = aVar3.f3134a;
                        if (i21 < bArr2.length) {
                            aVar2.a(bArr2[i21], 8);
                        }
                    }
                }
                for (int i22 = 0; i22 < i11; i22++) {
                    for (a aVar4 : arrayList) {
                        byte[] bArr3 = aVar4.f3135b;
                        if (i22 < bArr3.length) {
                            aVar2.a(bArr3[i22], 8);
                        }
                    }
                }
                if (i5 == aVar2.a()) {
                    return aVar2;
                }
                throw new WriterException("Interleaving error: " + i5 + " and " + aVar2.a() + " differ.");
            }
            throw new WriterException("Data bytes does not match offset");
        }
        throw new WriterException("Number of bits and data bytes does not match");
    }

    private static byte[] a(byte[] bArr, int i) {
        int length = bArr.length;
        int[] iArr = new int[(length + i)];
        for (int i2 = 0; i2 < length; i2++) {
            iArr[i2] = bArr[i2] & 255;
        }
        new com.google.zxing.common.reedsolomon.d(com.google.zxing.common.reedsolomon.a.e).a(iArr, i);
        byte[] bArr2 = new byte[i];
        for (int i3 = 0; i3 < i; i3++) {
            bArr2[i3] = (byte) iArr[length + i3];
        }
        return bArr2;
    }

    private static void a(String str, a aVar, String str2) throws WriterException {
        try {
            for (byte a2 : str.getBytes(str2)) {
                aVar.a(a2, 8);
            }
        } catch (UnsupportedEncodingException e) {
            throw new WriterException(e);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0035 A[LOOP:0: B:4:0x0008->B:17:0x0035, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0044 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(java.lang.String r6, com.google.zxing.common.a r7) throws com.google.zxing.WriterException {
        /*
            java.lang.String r0 = "Shift_JIS"
            byte[] r6 = r6.getBytes(r0)     // Catch:{ UnsupportedEncodingException -> 0x004d }
            int r0 = r6.length
            r1 = 0
        L_0x0008:
            if (r1 >= r0) goto L_0x004c
            byte r2 = r6[r1]
            r2 = r2 & 255(0xff, float:3.57E-43)
            int r3 = r1 + 1
            byte r3 = r6[r3]
            r3 = r3 & 255(0xff, float:3.57E-43)
            int r2 = r2 << 8
            r2 = r2 | r3
            r3 = 33088(0x8140, float:4.6366E-41)
            r4 = -1
            if (r2 < r3) goto L_0x0024
            r5 = 40956(0x9ffc, float:5.7392E-41)
            if (r2 > r5) goto L_0x0024
        L_0x0022:
            int r2 = r2 - r3
            goto L_0x0033
        L_0x0024:
            r3 = 57408(0xe040, float:8.0446E-41)
            if (r2 < r3) goto L_0x0032
            r3 = 60351(0xebbf, float:8.457E-41)
            if (r2 > r3) goto L_0x0032
            r3 = 49472(0xc140, float:6.9325E-41)
            goto L_0x0022
        L_0x0032:
            r2 = -1
        L_0x0033:
            if (r2 == r4) goto L_0x0044
            int r3 = r2 >> 8
            int r3 = r3 * 192
            r2 = r2 & 255(0xff, float:3.57E-43)
            int r3 = r3 + r2
            r2 = 13
            r7.a(r3, r2)
            int r1 = r1 + 2
            goto L_0x0008
        L_0x0044:
            com.google.zxing.WriterException r6 = new com.google.zxing.WriterException
            java.lang.String r7 = "Invalid byte sequence"
            r6.<init>(r7)
            throw r6
        L_0x004c:
            return
        L_0x004d:
            r6 = move-exception
            com.google.zxing.WriterException r7 = new com.google.zxing.WriterException
            r7.<init>(r6)
            goto L_0x0055
        L_0x0054:
            throw r7
        L_0x0055:
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.f.c.c.a(java.lang.String, com.google.zxing.common.a):void");
    }

    private static int a(q qVar, a aVar, a aVar2, s sVar) {
        return aVar.f2965b + qVar.a(sVar) + aVar2.f2965b;
    }

    private static boolean a(int i, s sVar, o oVar) {
        return sVar.c - sVar.a(oVar).b() >= (i + 7) / 8;
    }

    private static void a(int i, a aVar) throws WriterException {
        int i2 = i << 3;
        if (aVar.f2965b <= i2) {
            for (int i3 = 0; i3 < 4 && aVar.f2965b < i2; i3++) {
                aVar.a(false);
            }
            int i4 = aVar.f2965b & 7;
            if (i4 > 0) {
                while (i4 < 8) {
                    aVar.a(false);
                    i4++;
                }
            }
            int a2 = i - aVar.a();
            for (int i5 = 0; i5 < a2; i5++) {
                aVar.a((i5 & 1) == 0 ? 236 : 17, 8);
            }
            if (aVar.f2965b != i2) {
                throw new WriterException("Bits size does not equal capacity");
            }
            return;
        }
        throw new WriterException("data bits cannot fit in the QR Code" + aVar.f2965b + " > " + i2);
    }

    private static void a(q qVar, a aVar) {
        aVar.a(qVar.k, 4);
    }
}
