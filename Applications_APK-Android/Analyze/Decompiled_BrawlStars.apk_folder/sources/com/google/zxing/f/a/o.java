package com.google.zxing.f.a;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* compiled from: ErrorCorrectionLevel */
public final class o extends Enum<o> {

    /* renamed from: a  reason: collision with root package name */
    public static final o f3111a = new o("L", 0, 1);

    /* renamed from: b  reason: collision with root package name */
    public static final o f3112b = new o("M", 1, 0);
    public static final o c = new o("Q", 2, 3);
    public static final o d = new o("H", 3, 2);
    private static final o[] f;
    private static final /* synthetic */ o[] g;
    public final int e;

    public static o valueOf(String str) {
        return (o) Enum.valueOf(o.class, str);
    }

    public static o[] values() {
        return (o[]) g.clone();
    }

    static {
        o oVar = f3111a;
        o oVar2 = f3112b;
        o oVar3 = c;
        o oVar4 = d;
        g = new o[]{oVar, oVar2, oVar3, oVar4};
        f = new o[]{oVar2, oVar, oVar4, oVar3};
    }

    private o(String str, int i, int i2) {
        this.e = i2;
    }

    public static o a(int i) {
        if (i >= 0) {
            o[] oVarArr = f;
            if (i < oVarArr.length) {
                return oVarArr[i];
            }
        }
        throw new IllegalArgumentException();
    }
}
