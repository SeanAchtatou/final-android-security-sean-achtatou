package com.google.zxing.f.c;

import android.support.v7.widget.helper.ItemTouchHelper;
import com.google.zxing.f.a.o;
import com.google.zxing.f.a.q;
import com.google.zxing.f.a.s;

/* compiled from: QRCode */
public final class g {

    /* renamed from: a  reason: collision with root package name */
    q f3142a;

    /* renamed from: b  reason: collision with root package name */
    o f3143b;
    s c;
    int d = -1;
    public b e;

    public final String toString() {
        StringBuilder sb = new StringBuilder((int) ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION);
        sb.append("<<\n");
        sb.append(" mode: ");
        sb.append(this.f3142a);
        sb.append("\n ecLevel: ");
        sb.append(this.f3143b);
        sb.append("\n version: ");
        sb.append(this.c);
        sb.append("\n maskPattern: ");
        sb.append(this.d);
        if (this.e == null) {
            sb.append("\n matrix: null\n");
        } else {
            sb.append("\n matrix:\n");
            sb.append(this.e);
        }
        sb.append(">>\n");
        return sb.toString();
    }
}
