package com.google.zxing.f.a;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* compiled from: DataMask */
final class h extends c {
    h(String str, int i) {
        super(str, 4, (byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final boolean a(int i, int i2) {
        return (((i / 2) + (i2 / 3)) & 1) == 0;
    }
}
