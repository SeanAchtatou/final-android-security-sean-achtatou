package com.google.zxing.f.c;

/* compiled from: MaskUtil */
final class e {
    static boolean a(byte[] bArr, int i, int i2) {
        int min = Math.min(i2, bArr.length);
        for (int max = Math.max(i, 0); max < min; max++) {
            if (bArr[max] == 1) {
                return false;
            }
        }
        return true;
    }

    static boolean a(byte[][] bArr, int i, int i2, int i3) {
        int min = Math.min(i3, bArr.length);
        for (int max = Math.max(i2, 0); max < min; max++) {
            if (bArr[max][i] == 1) {
                return false;
            }
        }
        return true;
    }

    static int a(b bVar, boolean z) {
        int i;
        int i2;
        if (z) {
            i = bVar.c;
        } else {
            i = bVar.f3137b;
        }
        if (z) {
            i2 = bVar.f3137b;
        } else {
            i2 = bVar.c;
        }
        byte[][] bArr = bVar.f3136a;
        int i3 = 0;
        for (int i4 = 0; i4 < i; i4++) {
            int i5 = i3;
            int i6 = 0;
            byte b2 = -1;
            for (int i7 = 0; i7 < i2; i7++) {
                byte b3 = z ? bArr[i4][i7] : bArr[i7][i4];
                if (b3 == b2) {
                    i6++;
                } else {
                    if (i6 >= 5) {
                        i5 += (i6 - 5) + 3;
                    }
                    i6 = 1;
                    b2 = b3;
                }
            }
            if (i6 >= 5) {
                i5 += (i6 - 5) + 3;
            }
            i3 = i5;
        }
        return i3;
    }
}
