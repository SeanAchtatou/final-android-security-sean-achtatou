package com.google.zxing.f.c;

import java.lang.reflect.Array;

/* compiled from: ByteMatrix */
public final class b {

    /* renamed from: a  reason: collision with root package name */
    final byte[][] f3136a;

    /* renamed from: b  reason: collision with root package name */
    public final int f3137b;
    public final int c;

    public b(int i, int i2) {
        this.f3136a = (byte[][]) Array.newInstance(byte.class, i2, i);
        this.f3137b = i;
        this.c = i2;
    }

    public final byte a(int i, int i2) {
        return this.f3136a[i2][i];
    }

    public final void a(int i, int i2, int i3) {
        this.f3136a[i2][i] = (byte) i3;
    }

    public final void a(int i, int i2, boolean z) {
        this.f3136a[i2][i] = z ? (byte) 1 : 0;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder((this.f3137b * 2 * this.c) + 2);
        for (int i = 0; i < this.c; i++) {
            byte[] bArr = this.f3136a[i];
            for (int i2 = 0; i2 < this.f3137b; i2++) {
                byte b2 = bArr[i2];
                if (b2 == 0) {
                    sb.append(" 0");
                } else if (b2 != 1) {
                    sb.append("  ");
                } else {
                    sb.append(" 1");
                }
            }
            sb.append(10);
        }
        return sb.toString();
    }
}
