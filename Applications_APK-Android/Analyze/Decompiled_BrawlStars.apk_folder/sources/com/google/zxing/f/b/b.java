package com.google.zxing.f.b;

import com.google.zxing.q;
import java.util.ArrayList;
import java.util.List;

/* compiled from: AlignmentPatternFinder */
final class b {

    /* renamed from: a  reason: collision with root package name */
    final com.google.zxing.common.b f3124a;

    /* renamed from: b  reason: collision with root package name */
    final List<a> f3125b = new ArrayList(5);
    final int c;
    final int d;
    final int e;
    final int f;
    private final float g;
    private final int[] h;
    private final q i;

    b(com.google.zxing.common.b bVar, int i2, int i3, int i4, int i5, float f2, q qVar) {
        this.f3124a = bVar;
        this.c = i2;
        this.d = i3;
        this.e = i4;
        this.f = i5;
        this.g = f2;
        this.h = new int[3];
        this.i = qVar;
    }

    private static float a(int[] iArr, int i2) {
        return ((float) (i2 - iArr[2])) - (((float) iArr[1]) / 2.0f);
    }

    /* access modifiers changed from: package-private */
    public boolean a(int[] iArr) {
        float f2 = this.g;
        float f3 = f2 / 2.0f;
        for (int i2 = 0; i2 < 3; i2++) {
            if (Math.abs(f2 - ((float) iArr[i2])) >= f3) {
                return false;
            }
        }
        return true;
    }

    private float a(int i2, int i3, int i4, int i5) {
        com.google.zxing.common.b bVar = this.f3124a;
        int i6 = bVar.f2969b;
        int[] iArr = this.h;
        iArr[0] = 0;
        iArr[1] = 0;
        iArr[2] = 0;
        int i7 = i2;
        while (i7 >= 0 && bVar.a(i3, i7) && iArr[1] <= i4) {
            iArr[1] = iArr[1] + 1;
            i7--;
        }
        if (i7 >= 0 && iArr[1] <= i4) {
            while (i7 >= 0 && !bVar.a(i3, i7) && iArr[0] <= i4) {
                iArr[0] = iArr[0] + 1;
                i7--;
            }
            if (iArr[0] > i4) {
                return Float.NaN;
            }
            int i8 = i2 + 1;
            while (i8 < i6 && bVar.a(i3, i8) && iArr[1] <= i4) {
                iArr[1] = iArr[1] + 1;
                i8++;
            }
            if (i8 != i6 && iArr[1] <= i4) {
                while (i8 < i6 && !bVar.a(i3, i8) && iArr[2] <= i4) {
                    iArr[2] = iArr[2] + 1;
                    i8++;
                }
                if (iArr[2] <= i4 && Math.abs(((iArr[0] + iArr[1]) + iArr[2]) - i5) * 5 < i5 * 2 && a(iArr)) {
                    return a(iArr, i8);
                }
            }
        }
        return Float.NaN;
    }

    /* access modifiers changed from: package-private */
    public a a(int[] iArr, int i2, int i3) {
        int i4 = iArr[0] + iArr[1] + iArr[2];
        float a2 = a(iArr, i3);
        float a3 = a(i2, (int) a2, iArr[1] * 2, i4);
        if (Float.isNaN(a3)) {
            return null;
        }
        float f2 = ((float) ((iArr[0] + iArr[1]) + iArr[2])) / 3.0f;
        for (a next : this.f3125b) {
            if (next.a(f2, a3, a2)) {
                return next.b(a3, a2, f2);
            }
        }
        a aVar = new a(a2, a3, f2);
        this.f3125b.add(aVar);
        q qVar = this.i;
        if (qVar == null) {
            return null;
        }
        qVar.a(aVar);
        return null;
    }
}
