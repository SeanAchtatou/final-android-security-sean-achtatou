package com.google.zxing.f.a;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* compiled from: DataMask */
final class k extends c {
    k(String str, int i) {
        super(str, 7, (byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final boolean a(int i, int i2) {
        return (((i + i2) + ((i * i2) % 3)) & 1) == 0;
    }
}
