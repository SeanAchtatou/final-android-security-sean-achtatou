package com.google.zxing.f;

import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.c;
import com.google.zxing.common.b;
import com.google.zxing.common.e;
import com.google.zxing.common.g;
import com.google.zxing.d;
import com.google.zxing.f.a.n;
import com.google.zxing.f.a.r;
import com.google.zxing.m;
import com.google.zxing.o;
import com.google.zxing.p;
import java.util.List;
import java.util.Map;

/* compiled from: QRCodeReader */
public class a implements m {

    /* renamed from: a  reason: collision with root package name */
    private static final p[] f3100a = new p[0];

    /* renamed from: b  reason: collision with root package name */
    private final n f3101b = new n();

    public final void a() {
    }

    public final com.google.zxing.n a(c cVar) throws NotFoundException, ChecksumException, FormatException {
        return a(cVar, null);
    }

    public final com.google.zxing.n a(c cVar, Map<d, ?> map) throws NotFoundException, ChecksumException, FormatException {
        p[] pVarArr;
        e eVar;
        boolean z = false;
        if (map == null || !map.containsKey(d.PURE_BARCODE)) {
            g a2 = new com.google.zxing.f.b.c(cVar.b()).a(map);
            e a3 = this.f3101b.a(a2.d, map);
            pVarArr = a2.e;
            eVar = a3;
        } else {
            b b2 = cVar.b();
            int[] b3 = b2.b();
            int[] c = b2.c();
            if (b3 == null || c == null) {
                throw NotFoundException.a();
            }
            int i = b2.f2969b;
            int i2 = b2.f2968a;
            int i3 = b3[0];
            int i4 = b3[1];
            boolean z2 = true;
            int i5 = 0;
            while (i3 < i2 && i4 < i) {
                if (z2 != b2.a(i3, i4)) {
                    i5++;
                    if (i5 == 5) {
                        break;
                    }
                    z2 = !z2;
                }
                i3++;
                i4++;
            }
            if (i3 == i2 || i4 == i) {
                throw NotFoundException.a();
            }
            float f = ((float) (i3 - b3[0])) / 7.0f;
            int i6 = b3[1];
            int i7 = c[1];
            int i8 = b3[0];
            int i9 = c[0];
            if (i8 >= i9 || i6 >= i7) {
                throw NotFoundException.a();
            }
            int i10 = i7 - i6;
            if (i10 == i9 - i8 || (i9 = i8 + i10) < b2.f2968a) {
                int round = Math.round(((float) ((i9 - i8) + 1)) / f);
                int round2 = Math.round(((float) (i10 + 1)) / f);
                if (round <= 0 || round2 <= 0) {
                    throw NotFoundException.a();
                } else if (round2 == round) {
                    int i11 = (int) (f / 2.0f);
                    int i12 = i6 + i11;
                    int i13 = i8 + i11;
                    int i14 = (((int) (((float) (round - 1)) * f)) + i13) - i9;
                    if (i14 > 0) {
                        if (i14 <= i11) {
                            i13 -= i14;
                        } else {
                            throw NotFoundException.a();
                        }
                    }
                    int i15 = (((int) (((float) (round2 - 1)) * f)) + i12) - i7;
                    if (i15 > 0) {
                        if (i15 <= i11) {
                            i12 -= i15;
                        } else {
                            throw NotFoundException.a();
                        }
                    }
                    b bVar = new b(round, round2);
                    for (int i16 = 0; i16 < round2; i16++) {
                        int i17 = ((int) (((float) i16) * f)) + i12;
                        for (int i18 = 0; i18 < round; i18++) {
                            if (b2.a(((int) (((float) i18) * f)) + i13, i17)) {
                                bVar.b(i18, i16);
                            }
                        }
                    }
                    eVar = this.f3101b.a(bVar, map);
                    pVarArr = f3100a;
                } else {
                    throw NotFoundException.a();
                }
            } else {
                throw NotFoundException.a();
            }
        }
        if ((eVar.h instanceof r) && ((r) eVar.h).f3117a && pVarArr != null && pVarArr.length >= 3) {
            p pVar = pVarArr[0];
            pVarArr[0] = pVarArr[2];
            pVarArr[2] = pVar;
        }
        com.google.zxing.n nVar = new com.google.zxing.n(eVar.c, eVar.f2974a, pVarArr, com.google.zxing.a.QR_CODE);
        List<byte[]> list = eVar.d;
        if (list != null) {
            nVar.a(o.BYTE_SEGMENTS, list);
        }
        String str = eVar.e;
        if (str != null) {
            nVar.a(o.ERROR_CORRECTION_LEVEL, str);
        }
        if (eVar.i >= 0 && eVar.j >= 0) {
            z = true;
        }
        if (z) {
            nVar.a(o.STRUCTURED_APPEND_SEQUENCE, Integer.valueOf(eVar.j));
            nVar.a(o.STRUCTURED_APPEND_PARITY, Integer.valueOf(eVar.i));
        }
        return nVar;
    }
}
