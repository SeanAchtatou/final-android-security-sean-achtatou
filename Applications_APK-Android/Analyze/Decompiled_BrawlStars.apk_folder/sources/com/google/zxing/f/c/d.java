package com.google.zxing.f.c;

import com.google.zxing.f.a.q;

/* compiled from: Encoder */
/* synthetic */ class d {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f3139a = new int[q.values().length];

    /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
    /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
    static {
        /*
            com.google.zxing.f.a.q[] r0 = com.google.zxing.f.a.q.values()
            int r0 = r0.length
            int[] r0 = new int[r0]
            com.google.zxing.f.c.d.f3139a = r0
            int[] r0 = com.google.zxing.f.c.d.f3139a     // Catch:{ NoSuchFieldError -> 0x0014 }
            com.google.zxing.f.a.q r1 = com.google.zxing.f.a.q.NUMERIC     // Catch:{ NoSuchFieldError -> 0x0014 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
            r2 = 1
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
        L_0x0014:
            int[] r0 = com.google.zxing.f.c.d.f3139a     // Catch:{ NoSuchFieldError -> 0x001f }
            com.google.zxing.f.a.q r1 = com.google.zxing.f.a.q.ALPHANUMERIC     // Catch:{ NoSuchFieldError -> 0x001f }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
            r2 = 2
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
        L_0x001f:
            int[] r0 = com.google.zxing.f.c.d.f3139a     // Catch:{ NoSuchFieldError -> 0x002a }
            com.google.zxing.f.a.q r1 = com.google.zxing.f.a.q.BYTE     // Catch:{ NoSuchFieldError -> 0x002a }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
            r2 = 3
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
        L_0x002a:
            int[] r0 = com.google.zxing.f.c.d.f3139a     // Catch:{ NoSuchFieldError -> 0x0035 }
            com.google.zxing.f.a.q r1 = com.google.zxing.f.a.q.KANJI     // Catch:{ NoSuchFieldError -> 0x0035 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
            r2 = 4
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
        L_0x0035:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.f.c.d.<clinit>():void");
    }
}
