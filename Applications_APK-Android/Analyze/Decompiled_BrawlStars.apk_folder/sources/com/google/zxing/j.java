package com.google.zxing;

import com.google.zxing.a.c;
import com.google.zxing.common.b;
import com.google.zxing.d.ab;
import com.google.zxing.d.d;
import com.google.zxing.d.f;
import com.google.zxing.d.h;
import com.google.zxing.d.l;
import com.google.zxing.d.o;
import com.google.zxing.d.u;
import java.util.Map;

/* compiled from: MultiFormatWriter */
public final class j implements r {
    public final b a(String str, a aVar, int i, int i2, Map<f, ?> map) throws WriterException {
        r rVar;
        switch (k.f3148a[aVar.ordinal()]) {
            case 1:
                rVar = new l();
                break;
            case 2:
                rVar = new ab();
                break;
            case 3:
                rVar = new com.google.zxing.d.j();
                break;
            case 4:
                rVar = new u();
                break;
            case 5:
                rVar = new com.google.zxing.f.b();
                break;
            case 6:
                rVar = new f();
                break;
            case 7:
                rVar = new h();
                break;
            case 8:
                rVar = new d();
                break;
            case 9:
                rVar = new o();
                break;
            case 10:
                rVar = new com.google.zxing.e.d();
                break;
            case 11:
                rVar = new com.google.zxing.d.b();
                break;
            case 12:
                rVar = new com.google.zxing.b.b();
                break;
            case 13:
                rVar = new c();
                break;
            default:
                throw new IllegalArgumentException("No encoder available for format " + aVar);
        }
        return rVar.a(str, aVar, i, i2, map);
    }
}
