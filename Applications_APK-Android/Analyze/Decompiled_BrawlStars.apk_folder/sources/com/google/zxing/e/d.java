package com.google.zxing.e;

import com.google.zxing.WriterException;
import com.google.zxing.a;
import com.google.zxing.common.b;
import com.google.zxing.e.c.c;
import com.google.zxing.e.c.e;
import com.google.zxing.f;
import com.google.zxing.r;
import java.lang.reflect.Array;
import java.nio.charset.Charset;
import java.util.Map;

/* compiled from: PDF417Writer */
public final class d implements r {
    public final b a(String str, a aVar, int i, int i2, Map<f, ?> map) throws WriterException {
        boolean z;
        if (aVar == a.PDF_417) {
            e eVar = new e();
            int i3 = 30;
            int i4 = 2;
            if (map != null) {
                if (map.containsKey(f.PDF417_COMPACT)) {
                    eVar.f3093b = Boolean.valueOf(map.get(f.PDF417_COMPACT).toString()).booleanValue();
                }
                if (map.containsKey(f.PDF417_COMPACTION)) {
                    eVar.c = c.valueOf(map.get(f.PDF417_COMPACTION).toString());
                }
                if (map.containsKey(f.PDF417_DIMENSIONS)) {
                    com.google.zxing.e.c.d dVar = (com.google.zxing.e.c.d) map.get(f.PDF417_DIMENSIONS);
                    int i5 = dVar.f3091b;
                    int i6 = dVar.f3090a;
                    int i7 = dVar.d;
                    int i8 = dVar.c;
                    eVar.f = i5;
                    eVar.e = i6;
                    eVar.g = i7;
                    eVar.h = i8;
                }
                if (map.containsKey(f.MARGIN)) {
                    i3 = Integer.parseInt(map.get(f.MARGIN).toString());
                }
                if (map.containsKey(f.ERROR_CORRECTION)) {
                    i4 = Integer.parseInt(map.get(f.ERROR_CORRECTION).toString());
                }
                if (map.containsKey(f.CHARACTER_SET)) {
                    eVar.d = Charset.forName(map.get(f.CHARACTER_SET).toString());
                }
            }
            eVar.a(str, i4);
            byte[][] a2 = eVar.f3092a.a(1, 4);
            if ((i2 > i) != (a2[0].length < a2.length)) {
                a2 = a(a2);
                z = true;
            } else {
                z = false;
            }
            int length = i / a2[0].length;
            int length2 = i2 / a2.length;
            if (length >= length2) {
                length = length2;
            }
            if (length <= 1) {
                return a(a2, i3);
            }
            byte[][] a3 = eVar.f3092a.a(length, length << 2);
            if (z) {
                a3 = a(a3);
            }
            return a(a3, i3);
        }
        throw new IllegalArgumentException("Can only encode PDF_417, but got " + aVar);
    }

    private static b a(byte[][] bArr, int i) {
        int i2 = i * 2;
        b bVar = new b(bArr[0].length + i2, bArr.length + i2);
        bVar.a();
        int i3 = (bVar.f2969b - i) - 1;
        int i4 = 0;
        while (i4 < bArr.length) {
            byte[] bArr2 = bArr[i4];
            for (int i5 = 0; i5 < bArr[0].length; i5++) {
                if (bArr2[i5] == 1) {
                    bVar.b(i5 + i, i3);
                }
            }
            i4++;
            i3--;
        }
        return bVar;
    }

    private static byte[][] a(byte[][] bArr) {
        byte[][] bArr2 = (byte[][]) Array.newInstance(byte.class, bArr[0].length, bArr.length);
        for (int i = 0; i < bArr.length; i++) {
            int length = (bArr.length - i) - 1;
            for (int i2 = 0; i2 < bArr[0].length; i2++) {
                bArr2[i2][length] = bArr[i][i2];
            }
        }
        return bArr2;
    }
}
