package com.google.zxing.e.a;

import com.google.zxing.e.a.e;

/* compiled from: DecodedBitStreamParser */
/* synthetic */ class f {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f3071a = new int[e.a.values().length];

    /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|(3:11|12|14)) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
    static {
        /*
            com.google.zxing.e.a.e$a[] r0 = com.google.zxing.e.a.e.a.values()
            int r0 = r0.length
            int[] r0 = new int[r0]
            com.google.zxing.e.a.f.f3071a = r0
            int[] r0 = com.google.zxing.e.a.f.f3071a     // Catch:{ NoSuchFieldError -> 0x0014 }
            com.google.zxing.e.a.e$a r1 = com.google.zxing.e.a.e.a.ALPHA     // Catch:{ NoSuchFieldError -> 0x0014 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
            r2 = 1
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
        L_0x0014:
            int[] r0 = com.google.zxing.e.a.f.f3071a     // Catch:{ NoSuchFieldError -> 0x001f }
            com.google.zxing.e.a.e$a r1 = com.google.zxing.e.a.e.a.LOWER     // Catch:{ NoSuchFieldError -> 0x001f }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
            r2 = 2
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
        L_0x001f:
            int[] r0 = com.google.zxing.e.a.f.f3071a     // Catch:{ NoSuchFieldError -> 0x002a }
            com.google.zxing.e.a.e$a r1 = com.google.zxing.e.a.e.a.MIXED     // Catch:{ NoSuchFieldError -> 0x002a }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
            r2 = 3
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
        L_0x002a:
            int[] r0 = com.google.zxing.e.a.f.f3071a     // Catch:{ NoSuchFieldError -> 0x0035 }
            com.google.zxing.e.a.e$a r1 = com.google.zxing.e.a.e.a.PUNCT     // Catch:{ NoSuchFieldError -> 0x0035 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
            r2 = 4
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
        L_0x0035:
            int[] r0 = com.google.zxing.e.a.f.f3071a     // Catch:{ NoSuchFieldError -> 0x0040 }
            com.google.zxing.e.a.e$a r1 = com.google.zxing.e.a.e.a.ALPHA_SHIFT     // Catch:{ NoSuchFieldError -> 0x0040 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
            r2 = 5
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
        L_0x0040:
            int[] r0 = com.google.zxing.e.a.f.f3071a     // Catch:{ NoSuchFieldError -> 0x004b }
            com.google.zxing.e.a.e$a r1 = com.google.zxing.e.a.e.a.PUNCT_SHIFT     // Catch:{ NoSuchFieldError -> 0x004b }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
            r2 = 6
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
        L_0x004b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.e.a.f.<clinit>():void");
    }
}
