package com.google.zxing.e.a.a;

/* compiled from: ModulusGF */
public final class b {

    /* renamed from: a  reason: collision with root package name */
    public static final b f3058a = new b(929, 3);

    /* renamed from: b  reason: collision with root package name */
    public final int[] f3059b = new int[929];
    public final int[] c = new int[929];
    public final c d;
    public final c e;
    final int f = 929;

    private b(int i, int i2) {
        int i3 = 1;
        for (int i4 = 0; i4 < 929; i4++) {
            this.f3059b[i4] = i3;
            i3 = (i3 * 3) % 929;
        }
        for (int i5 = 0; i5 < 928; i5++) {
            this.c[this.f3059b[i5]] = i5;
        }
        this.d = new c(this, new int[]{0});
        this.e = new c(this, new int[]{1});
    }

    public final c a(int i, int i2) {
        if (i < 0) {
            throw new IllegalArgumentException();
        } else if (i2 == 0) {
            return this.d;
        } else {
            int[] iArr = new int[(i + 1)];
            iArr[0] = i2;
            return new c(this, iArr);
        }
    }

    /* access modifiers changed from: package-private */
    public final int b(int i, int i2) {
        return (i + i2) % this.f;
    }

    public final int c(int i, int i2) {
        int i3 = this.f;
        return ((i + i3) - i2) % i3;
    }

    public final int a(int i) {
        if (i != 0) {
            return this.f3059b[(this.f - this.c[i]) - 1];
        }
        throw new ArithmeticException();
    }

    public final int d(int i, int i2) {
        if (i == 0 || i2 == 0) {
            return 0;
        }
        int[] iArr = this.f3059b;
        int[] iArr2 = this.c;
        return iArr[(iArr2[i] + iArr2[i2]) % (this.f - 1)];
    }
}
