package com.google.zxing.e;

import android.support.v7.widget.ActivityChooserView;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.c;
import com.google.zxing.common.e;
import com.google.zxing.d;
import com.google.zxing.e.a.k;
import com.google.zxing.e.b.a;
import com.google.zxing.m;
import com.google.zxing.n;
import com.google.zxing.o;
import com.google.zxing.p;
import java.util.ArrayList;
import java.util.Map;

/* compiled from: PDF417Reader */
public final class b implements m {
    public final void a() {
    }

    public final n a(c cVar) throws NotFoundException, FormatException, ChecksumException {
        return a(cVar, (Map<d, ?>) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.zxing.e.b.a.a(com.google.zxing.c, boolean):com.google.zxing.e.b.b
     arg types: [com.google.zxing.c, int]
     candidates:
      com.google.zxing.e.b.a.a(boolean, com.google.zxing.common.b):java.util.List<com.google.zxing.p[]>
      com.google.zxing.e.b.a.a(com.google.zxing.c, boolean):com.google.zxing.e.b.b */
    public final n a(c cVar, Map<d, ?> map) throws NotFoundException, FormatException, ChecksumException {
        ArrayList arrayList = new ArrayList();
        com.google.zxing.e.b.b a2 = a.a(cVar, false);
        for (p[] next : a2.f3081b) {
            e a3 = k.a(a2.f3080a, next[4], next[5], next[6], next[7], Math.min(Math.min(b(next[0], next[4]), (b(next[6], next[2]) * 17) / 18), Math.min(b(next[1], next[5]), (b(next[7], next[3]) * 17) / 18)), Math.max(Math.max(a(next[0], next[4]), (a(next[6], next[2]) * 17) / 18), Math.max(a(next[1], next[5]), (a(next[7], next[3]) * 17) / 18)));
            n nVar = new n(a3.c, a3.f2974a, next, com.google.zxing.a.PDF_417);
            nVar.a(o.ERROR_CORRECTION_LEVEL, a3.e);
            c cVar2 = (c) a3.h;
            if (cVar2 != null) {
                nVar.a(o.PDF417_EXTRA_METADATA, cVar2);
            }
            arrayList.add(nVar);
        }
        n[] nVarArr = (n[]) arrayList.toArray(new n[arrayList.size()]);
        if (nVarArr != null && nVarArr.length != 0 && nVarArr[0] != null) {
            return nVarArr[0];
        }
        throw NotFoundException.a();
    }

    private static int a(p pVar, p pVar2) {
        if (pVar == null || pVar2 == null) {
            return 0;
        }
        return (int) Math.abs(pVar.f3153a - pVar2.f3153a);
    }

    private static int b(p pVar, p pVar2) {
        return (pVar == null || pVar2 == null) ? ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED : (int) Math.abs(pVar.f3153a - pVar2.f3153a);
    }
}
