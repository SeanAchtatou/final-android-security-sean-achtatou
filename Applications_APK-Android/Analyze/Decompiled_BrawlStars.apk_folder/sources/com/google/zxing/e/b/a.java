package com.google.zxing.e.b;

import com.google.zxing.NotFoundException;
import com.google.zxing.c;
import com.google.zxing.common.b;
import com.google.zxing.p;
import java.util.Arrays;
import java.util.List;

/* compiled from: Detector */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f3078a = {0, 4, 1, 5};

    /* renamed from: b  reason: collision with root package name */
    private static final int[] f3079b = {6, 2, 7, 3};
    private static final int[] c = {8, 1, 1, 1, 1, 1, 1, 3};
    private static final int[] d = {7, 1, 1, 3, 1, 1, 1, 2, 1};

    public static b a(c cVar, boolean z) throws NotFoundException {
        b b2 = cVar.b();
        List<p[]> a2 = a(z, b2);
        if (a2.isEmpty()) {
            b2 = b2.clone();
            int i = b2.f2968a;
            int i2 = b2.f2969b;
            com.google.zxing.common.a aVar = new com.google.zxing.common.a(i);
            com.google.zxing.common.a aVar2 = new com.google.zxing.common.a(i);
            for (int i3 = 0; i3 < (i2 + 1) / 2; i3++) {
                aVar = b2.a(i3, aVar);
                int i4 = (i2 - 1) - i3;
                aVar2 = b2.a(i4, aVar2);
                aVar.c();
                aVar2.c();
                b2.b(i3, aVar2);
                b2.b(i4, aVar);
            }
            a2 = a(z, b2);
        }
        return new b(b2, a2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0025, code lost:
        if (r4.hasNext() == false) goto L_0x0049;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0027, code lost:
        r5 = (com.google.zxing.p[]) r4.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002f, code lost:
        if (r5[1] == null) goto L_0x003b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0031, code lost:
        r3 = (int) java.lang.Math.max((float) r3, r5[1].f3154b);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003d, code lost:
        if (r5[3] == null) goto L_0x0021;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003f, code lost:
        r3 = java.lang.Math.max(r3, (int) r5[3].f3154b);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001b, code lost:
        if (r5 == false) goto L_0x006e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001d, code lost:
        r4 = r0.iterator();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.util.List<com.google.zxing.p[]> a(boolean r8, com.google.zxing.common.b r9) {
        /*
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r1 = 1
            r2 = 0
            r3 = 0
        L_0x0008:
            r4 = 0
            r5 = 0
        L_0x000a:
            int r6 = r9.f2969b
            if (r3 >= r6) goto L_0x006e
            com.google.zxing.p[] r4 = a(r9, r3, r4)
            r6 = r4[r2]
            if (r6 != 0) goto L_0x004c
            r6 = 3
            r7 = r4[r6]
            if (r7 != 0) goto L_0x004c
            if (r5 == 0) goto L_0x006e
            java.util.Iterator r4 = r0.iterator()
        L_0x0021:
            boolean r5 = r4.hasNext()
            if (r5 == 0) goto L_0x0049
            java.lang.Object r5 = r4.next()
            com.google.zxing.p[] r5 = (com.google.zxing.p[]) r5
            r7 = r5[r1]
            if (r7 == 0) goto L_0x003b
            float r3 = (float) r3
            r7 = r5[r1]
            float r7 = r7.f3154b
            float r3 = java.lang.Math.max(r3, r7)
            int r3 = (int) r3
        L_0x003b:
            r7 = r5[r6]
            if (r7 == 0) goto L_0x0021
            r5 = r5[r6]
            float r5 = r5.f3154b
            int r5 = (int) r5
            int r3 = java.lang.Math.max(r3, r5)
            goto L_0x0021
        L_0x0049:
            int r3 = r3 + 5
            goto L_0x0008
        L_0x004c:
            r0.add(r4)
            if (r8 == 0) goto L_0x006e
            r3 = 2
            r5 = r4[r3]
            if (r5 == 0) goto L_0x0060
            r5 = r4[r3]
            float r5 = r5.f3153a
            int r5 = (int) r5
            r3 = r4[r3]
            float r3 = r3.f3154b
            goto L_0x006a
        L_0x0060:
            r3 = 4
            r5 = r4[r3]
            float r5 = r5.f3153a
            int r5 = (int) r5
            r3 = r4[r3]
            float r3 = r3.f3154b
        L_0x006a:
            int r3 = (int) r3
            r4 = r5
            r5 = 1
            goto L_0x000a
        L_0x006e:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.e.b.a.a(boolean, com.google.zxing.common.b):java.util.List");
    }

    private static void a(p[] pVarArr, p[] pVarArr2, int[] iArr) {
        for (int i = 0; i < iArr.length; i++) {
            pVarArr[iArr[i]] = pVarArr2[i];
        }
    }

    private static p[] a(b bVar, int i, int i2, int i3, int i4, int[] iArr) {
        boolean z;
        int i5;
        int[] iArr2;
        int i6 = i;
        p[] pVarArr = new p[4];
        int[] iArr3 = new int[iArr.length];
        int i7 = i3;
        while (true) {
            if (i7 >= i6) {
                z = false;
                break;
            }
            int[] a2 = a(bVar, i4, i7, i2, false, iArr, iArr3);
            if (a2 != null) {
                while (true) {
                    iArr2 = a2;
                    if (i7 <= 0) {
                        break;
                    }
                    i7--;
                    a2 = a(bVar, i4, i7, i2, false, iArr, iArr3);
                    if (a2 == null) {
                        i7++;
                        break;
                    }
                }
                float f = (float) i7;
                pVarArr[0] = new p((float) iArr2[0], f);
                pVarArr[1] = new p((float) iArr2[1], f);
                z = true;
            } else {
                i7 += 5;
            }
        }
        int i8 = i7 + 1;
        if (z) {
            int[] iArr4 = {(int) pVarArr[0].f3153a, (int) pVarArr[1].f3153a};
            int i9 = i8;
            int i10 = 0;
            while (true) {
                if (i9 >= i6) {
                    i5 = i10;
                    break;
                }
                i5 = i10;
                int[] a3 = a(bVar, iArr4[0], i9, i2, false, iArr, iArr3);
                if (a3 == null || Math.abs(iArr4[0] - a3[0]) >= 5 || Math.abs(iArr4[1] - a3[1]) >= 5) {
                    if (i5 > 25) {
                        break;
                    }
                    i10 = i5 + 1;
                } else {
                    iArr4 = a3;
                    i10 = 0;
                }
                i9++;
            }
            i8 = i9 - (i5 + 1);
            float f2 = (float) i8;
            pVarArr[2] = new p((float) iArr4[0], f2);
            pVarArr[3] = new p((float) iArr4[1], f2);
        }
        if (i8 - i7 < 10) {
            Arrays.fill(pVarArr, (Object) null);
        }
        return pVarArr;
    }

    private static int[] a(b bVar, int i, int i2, int i3, boolean z, int[] iArr, int[] iArr2) {
        Arrays.fill(iArr2, 0, iArr2.length, 0);
        int i4 = 0;
        while (bVar.a(i, i2) && i > 0) {
            int i5 = i4 + 1;
            if (i4 >= 3) {
                break;
            }
            i--;
            i4 = i5;
        }
        int length = iArr.length;
        int i6 = i;
        int i7 = 0;
        boolean z2 = false;
        while (i < i3) {
            if (bVar.a(i, i2) != z2) {
                iArr2[i7] = iArr2[i7] + 1;
            } else {
                if (i7 != length - 1) {
                    i7++;
                } else if (a(iArr2, iArr, 0.8f) < 0.42f) {
                    return new int[]{i6, i};
                } else {
                    i6 += iArr2[0] + iArr2[1];
                    int i8 = i7 - 1;
                    System.arraycopy(iArr2, 2, iArr2, 0, i8);
                    iArr2[i8] = 0;
                    iArr2[i7] = 0;
                    i7--;
                }
                iArr2[i7] = 1;
                z2 = !z2;
            }
            i++;
        }
        if (i7 != length - 1 || a(iArr2, iArr, 0.8f) >= 0.42f) {
            return null;
        }
        return new int[]{i6, i - 1};
    }

    private static float a(int[] iArr, int[] iArr2, float f) {
        int length = iArr.length;
        int i = 0;
        int i2 = 0;
        for (int i3 = 0; i3 < length; i3++) {
            i += iArr[i3];
            i2 += iArr2[i3];
        }
        if (i < i2) {
            return Float.POSITIVE_INFINITY;
        }
        float f2 = (float) i;
        float f3 = f2 / ((float) i2);
        float f4 = 0.8f * f3;
        float f5 = 0.0f;
        for (int i4 = 0; i4 < length; i4++) {
            int i5 = iArr[i4];
            float f6 = ((float) iArr2[i4]) * f3;
            float f7 = (float) i5;
            float f8 = f7 > f6 ? f7 - f6 : f6 - f7;
            if (f8 > f4) {
                return Float.POSITIVE_INFINITY;
            }
            f5 += f8;
        }
        return f5 / f2;
    }

    private static p[] a(b bVar, int i, int i2) {
        int i3 = bVar.f2969b;
        int i4 = bVar.f2968a;
        p[] pVarArr = new p[8];
        a(pVarArr, a(bVar, i3, i4, i, i2, c), f3078a);
        if (pVarArr[4] != null) {
            i2 = (int) pVarArr[4].f3153a;
            i = (int) pVarArr[4].f3154b;
        }
        a(pVarArr, a(bVar, i3, i4, i, i2, d), f3079b);
        return pVarArr;
    }
}
