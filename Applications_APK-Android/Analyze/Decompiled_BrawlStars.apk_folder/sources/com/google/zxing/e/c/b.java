package com.google.zxing.e.c;

/* compiled from: BarcodeRow */
final class b {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f3086a;

    /* renamed from: b  reason: collision with root package name */
    private int f3087b = 0;

    b(int i) {
        this.f3086a = new byte[i];
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            int i3 = this.f3087b;
            this.f3087b = i3 + 1;
            this.f3086a[i3] = z ? (byte) 1 : 0;
        }
    }

    /* access modifiers changed from: package-private */
    public final byte[] a(int i) {
        byte[] bArr = new byte[(this.f3086a.length * i)];
        for (int i2 = 0; i2 < bArr.length; i2++) {
            bArr[i2] = this.f3086a[i2 / i];
        }
        return bArr;
    }
}
