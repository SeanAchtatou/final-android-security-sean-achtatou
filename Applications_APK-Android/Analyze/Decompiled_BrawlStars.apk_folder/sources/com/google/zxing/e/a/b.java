package com.google.zxing.e.a;

import com.google.zxing.e.a;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* compiled from: BarcodeValue */
final class b {

    /* renamed from: a  reason: collision with root package name */
    private final Map<Integer, Integer> f3062a = new HashMap();

    b() {
    }

    /* access modifiers changed from: package-private */
    public final void a(int i) {
        Integer num = this.f3062a.get(Integer.valueOf(i));
        if (num == null) {
            num = 0;
        }
        this.f3062a.put(Integer.valueOf(i), Integer.valueOf(num.intValue() + 1));
    }

    /* access modifiers changed from: package-private */
    public final int[] a() {
        ArrayList arrayList = new ArrayList();
        int i = -1;
        for (Map.Entry next : this.f3062a.entrySet()) {
            if (((Integer) next.getValue()).intValue() > i) {
                i = ((Integer) next.getValue()).intValue();
                arrayList.clear();
                arrayList.add(next.getKey());
            } else if (((Integer) next.getValue()).intValue() == i) {
                arrayList.add(next.getKey());
            }
        }
        return a.a(arrayList);
    }
}
