package com.google.zxing.e.a;

import com.google.zxing.NotFoundException;
import com.google.zxing.common.b;
import com.google.zxing.p;

/* compiled from: BoundingBox */
final class c {

    /* renamed from: a  reason: collision with root package name */
    b f3063a;

    /* renamed from: b  reason: collision with root package name */
    p f3064b;
    p c;
    p d;
    p e;
    int f;
    int g;
    int h;
    int i;

    c(b bVar, p pVar, p pVar2, p pVar3, p pVar4) throws NotFoundException {
        if (!(pVar == null && pVar3 == null) && (!(pVar2 == null && pVar4 == null) && ((pVar == null || pVar2 != null) && (pVar3 == null || pVar4 != null)))) {
            a(bVar, pVar, pVar2, pVar3, pVar4);
            return;
        }
        throw NotFoundException.a();
    }

    c(c cVar) {
        a(cVar.f3063a, cVar.f3064b, cVar.c, cVar.d, cVar.e);
    }

    private void a(b bVar, p pVar, p pVar2, p pVar3, p pVar4) {
        this.f3063a = bVar;
        this.f3064b = pVar;
        this.c = pVar2;
        this.d = pVar3;
        this.e = pVar4;
        a();
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0029  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x004f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.zxing.e.a.c a(int r13, int r14, boolean r15) throws com.google.zxing.NotFoundException {
        /*
            r12 = this;
            com.google.zxing.p r0 = r12.f3064b
            com.google.zxing.p r1 = r12.c
            com.google.zxing.p r2 = r12.d
            com.google.zxing.p r3 = r12.e
            if (r13 <= 0) goto L_0x0025
            if (r15 == 0) goto L_0x000e
            r4 = r0
            goto L_0x000f
        L_0x000e:
            r4 = r2
        L_0x000f:
            float r5 = r4.f3154b
            int r5 = (int) r5
            int r5 = r5 - r13
            if (r5 >= 0) goto L_0x0016
            r5 = 0
        L_0x0016:
            com.google.zxing.p r13 = new com.google.zxing.p
            float r4 = r4.f3153a
            float r5 = (float) r5
            r13.<init>(r4, r5)
            if (r15 == 0) goto L_0x0022
            r8 = r13
            goto L_0x0026
        L_0x0022:
            r10 = r13
            r8 = r0
            goto L_0x0027
        L_0x0025:
            r8 = r0
        L_0x0026:
            r10 = r2
        L_0x0027:
            if (r14 <= 0) goto L_0x004f
            if (r15 == 0) goto L_0x002e
            com.google.zxing.p r13 = r12.c
            goto L_0x0030
        L_0x002e:
            com.google.zxing.p r13 = r12.e
        L_0x0030:
            float r0 = r13.f3154b
            int r0 = (int) r0
            int r0 = r0 + r14
            com.google.zxing.common.b r14 = r12.f3063a
            int r14 = r14.f2969b
            if (r0 < r14) goto L_0x0040
            com.google.zxing.common.b r14 = r12.f3063a
            int r14 = r14.f2969b
            int r0 = r14 + -1
        L_0x0040:
            com.google.zxing.p r14 = new com.google.zxing.p
            float r13 = r13.f3153a
            float r0 = (float) r0
            r14.<init>(r13, r0)
            if (r15 == 0) goto L_0x004c
            r9 = r14
            goto L_0x0050
        L_0x004c:
            r11 = r14
            r9 = r1
            goto L_0x0051
        L_0x004f:
            r9 = r1
        L_0x0050:
            r11 = r3
        L_0x0051:
            r12.a()
            com.google.zxing.e.a.c r13 = new com.google.zxing.e.a.c
            com.google.zxing.common.b r7 = r12.f3063a
            r6 = r13
            r6.<init>(r7, r8, r9, r10, r11)
            return r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.e.a.c.a(int, int, boolean):com.google.zxing.e.a.c");
    }

    private void a() {
        if (this.f3064b == null) {
            this.f3064b = new p(0.0f, this.d.f3154b);
            this.c = new p(0.0f, this.e.f3154b);
        } else if (this.d == null) {
            this.d = new p((float) (this.f3063a.f2968a - 1), this.f3064b.f3154b);
            this.e = new p((float) (this.f3063a.f2968a - 1), this.c.f3154b);
        }
        this.f = (int) Math.min(this.f3064b.f3153a, this.c.f3153a);
        this.g = (int) Math.max(this.d.f3153a, this.e.f3153a);
        this.h = (int) Math.min(this.f3064b.f3154b, this.d.f3154b);
        this.i = (int) Math.max(this.c.f3154b, this.e.f3154b);
    }
}
