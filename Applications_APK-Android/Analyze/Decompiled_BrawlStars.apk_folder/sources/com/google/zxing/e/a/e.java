package com.google.zxing.e.a;

import com.google.zxing.FormatException;
import com.google.zxing.e.c;
import java.math.BigInteger;
import java.util.Arrays;

/* compiled from: DecodedBitStreamParser */
final class e {

    /* renamed from: a  reason: collision with root package name */
    private static final char[] f3067a = ";<>@[\\]_`~!\r\t,:\n-.$/\"|*()?{}'".toCharArray();

    /* renamed from: b  reason: collision with root package name */
    private static final char[] f3068b = "0123456789&\r\t,:#-.$/+%*=^".toCharArray();
    private static final BigInteger[] c;

    /* compiled from: DecodedBitStreamParser */
    enum a {
        ALPHA,
        LOWER,
        MIXED,
        PUNCT,
        ALPHA_SHIFT,
        PUNCT_SHIFT
    }

    static {
        BigInteger[] bigIntegerArr = new BigInteger[16];
        c = bigIntegerArr;
        bigIntegerArr[0] = BigInteger.ONE;
        BigInteger valueOf = BigInteger.valueOf(900);
        c[1] = valueOf;
        int i = 2;
        while (true) {
            BigInteger[] bigIntegerArr2 = c;
            if (i < bigIntegerArr2.length) {
                bigIntegerArr2[i] = bigIntegerArr2[i - 1].multiply(valueOf);
                i++;
            } else {
                return;
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static com.google.zxing.common.e a(int[] r6, java.lang.String r7) throws com.google.zxing.FormatException {
        /*
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            int r1 = r6.length
            r2 = 1
            int r1 = r1 << r2
            r0.<init>(r1)
            java.nio.charset.Charset r1 = java.nio.charset.StandardCharsets.ISO_8859_1
            r2 = r6[r2]
            com.google.zxing.e.c r3 = new com.google.zxing.e.c
            r3.<init>()
            r4 = 2
        L_0x0012:
            r5 = 0
            r5 = r6[r5]
            if (r4 >= r5) goto L_0x006d
            r5 = 913(0x391, float:1.28E-42)
            if (r2 == r5) goto L_0x0058
            switch(r2) {
                case 900: goto L_0x0053;
                case 901: goto L_0x004e;
                case 902: goto L_0x0049;
                default: goto L_0x001e;
            }
        L_0x001e:
            switch(r2) {
                case 922: goto L_0x0044;
                case 923: goto L_0x0044;
                case 924: goto L_0x004e;
                case 925: goto L_0x0041;
                case 926: goto L_0x003e;
                case 927: goto L_0x002d;
                case 928: goto L_0x0028;
                default: goto L_0x0021;
            }
        L_0x0021:
            int r4 = r4 + -1
            int r2 = a(r6, r4, r0)
            goto L_0x0060
        L_0x0028:
            int r2 = a(r6, r4, r3)
            goto L_0x0060
        L_0x002d:
            int r2 = r4 + 1
            r1 = r6[r4]
            com.google.zxing.common.d r1 = com.google.zxing.common.d.a(r1)
            java.lang.String r1 = r1.name()
            java.nio.charset.Charset r1 = java.nio.charset.Charset.forName(r1)
            goto L_0x0060
        L_0x003e:
            int r2 = r4 + 2
            goto L_0x0060
        L_0x0041:
            int r2 = r4 + 1
            goto L_0x0060
        L_0x0044:
            com.google.zxing.FormatException r6 = com.google.zxing.FormatException.a()
            throw r6
        L_0x0049:
            int r2 = b(r6, r4, r0)
            goto L_0x0060
        L_0x004e:
            int r2 = a(r2, r6, r1, r4, r0)
            goto L_0x0060
        L_0x0053:
            int r2 = a(r6, r4, r0)
            goto L_0x0060
        L_0x0058:
            int r2 = r4 + 1
            r4 = r6[r4]
            char r4 = (char) r4
            r0.append(r4)
        L_0x0060:
            int r4 = r6.length
            if (r2 >= r4) goto L_0x0068
            int r4 = r2 + 1
            r2 = r6[r2]
            goto L_0x0012
        L_0x0068:
            com.google.zxing.FormatException r6 = com.google.zxing.FormatException.a()
            throw r6
        L_0x006d:
            int r6 = r0.length()
            if (r6 == 0) goto L_0x0080
            com.google.zxing.common.e r6 = new com.google.zxing.common.e
            java.lang.String r0 = r0.toString()
            r1 = 0
            r6.<init>(r1, r0, r1, r7)
            r6.h = r3
            return r6
        L_0x0080:
            com.google.zxing.FormatException r6 = com.google.zxing.FormatException.a()
            goto L_0x0086
        L_0x0085:
            throw r6
        L_0x0086:
            goto L_0x0085
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.e.a.e.a(int[], java.lang.String):com.google.zxing.common.e");
    }

    private static int a(int[] iArr, int i, c cVar) throws FormatException {
        if (i + 2 <= iArr[0]) {
            int[] iArr2 = new int[2];
            int i2 = i;
            int i3 = 0;
            while (i3 < 2) {
                iArr2[i3] = iArr[i2];
                i3++;
                i2++;
            }
            cVar.f3082a = Integer.parseInt(a(iArr2, 2));
            StringBuilder sb = new StringBuilder();
            int a2 = a(iArr, i2, sb);
            cVar.f3083b = sb.toString();
            int i4 = iArr[a2];
            if (i4 == 922) {
                cVar.d = true;
                return a2 + 1;
            } else if (i4 != 923) {
                return a2;
            } else {
                int i5 = a2 + 1;
                int[] iArr3 = new int[(iArr[0] - i5)];
                boolean z = false;
                int i6 = 0;
                while (i5 < iArr[0] && !z) {
                    int i7 = i5 + 1;
                    int i8 = iArr[i5];
                    if (i8 < 900) {
                        iArr3[i6] = i8;
                        i5 = i7;
                        i6++;
                    } else if (i8 == 922) {
                        cVar.d = true;
                        i5 = i7 + 1;
                        z = true;
                    } else {
                        throw FormatException.a();
                    }
                }
                cVar.c = Arrays.copyOf(iArr3, i6);
                return i5;
            }
        } else {
            throw FormatException.a();
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private static int a(int[] iArr, int i, StringBuilder sb) {
        a aVar;
        char c2;
        int i2;
        char c3;
        StringBuilder sb2 = sb;
        int[] iArr2 = new int[((iArr[0] - i) << 1)];
        int[] iArr3 = new int[((iArr[0] - i) << 1)];
        int i3 = i;
        boolean z = false;
        int i4 = 0;
        while (i3 < iArr[0] && !z) {
            int i5 = i3 + 1;
            int i6 = iArr[i3];
            if (i6 < 900) {
                iArr2[i4] = i6 / 30;
                iArr2[i4 + 1] = i6 % 30;
                i4 += 2;
            } else if (i6 != 913) {
                if (i6 != 928) {
                    switch (i6) {
                        case 900:
                            iArr2[i4] = 900;
                            i4++;
                            break;
                        default:
                            switch (i6) {
                            }
                        case 901:
                        case 902:
                            i3 = i5 - 1;
                            z = true;
                            break;
                    }
                }
                i3 = i5 - 1;
                z = true;
            } else {
                iArr2[i4] = 913;
                i3 = i5 + 1;
                iArr3[i4] = iArr[i5];
                i4++;
            }
            i3 = i5;
        }
        a aVar2 = a.ALPHA;
        a aVar3 = a.ALPHA;
        a aVar4 = aVar2;
        for (int i7 = 0; i7 < i4; i7++) {
            int i8 = iArr2[i7];
            char c4 = ' ';
            switch (f.f3071a[aVar4.ordinal()]) {
                case 1:
                    if (i8 >= 26) {
                        if (i8 == 900) {
                            aVar4 = a.ALPHA;
                        } else if (i8 != 913) {
                            switch (i8) {
                                case 27:
                                    aVar4 = a.LOWER;
                                    break;
                                case 28:
                                    aVar4 = a.MIXED;
                                    break;
                                case 29:
                                    aVar = a.PUNCT_SHIFT;
                                    c4 = 0;
                                    a aVar5 = aVar;
                                    aVar3 = aVar4;
                                    aVar4 = aVar5;
                                    break;
                            }
                        } else {
                            sb2.append((char) iArr3[i7]);
                        }
                        c4 = 0;
                        break;
                    } else {
                        i2 = i8 + 65;
                        c2 = (char) i2;
                        c4 = c2;
                        break;
                    }
                case 2:
                    if (i8 >= 26) {
                        if (i8 == 900) {
                            aVar4 = a.ALPHA;
                        } else if (i8 != 913) {
                            switch (i8) {
                                case 27:
                                    aVar = a.ALPHA_SHIFT;
                                    c4 = 0;
                                    a aVar52 = aVar;
                                    aVar3 = aVar4;
                                    aVar4 = aVar52;
                                    break;
                                case 28:
                                    aVar4 = a.MIXED;
                                    break;
                                case 29:
                                    aVar = a.PUNCT_SHIFT;
                                    c4 = 0;
                                    a aVar522 = aVar;
                                    aVar3 = aVar4;
                                    aVar4 = aVar522;
                                    break;
                            }
                        } else {
                            sb2.append((char) iArr3[i7]);
                        }
                        c4 = 0;
                        break;
                    } else {
                        i2 = i8 + 97;
                        c2 = (char) i2;
                        c4 = c2;
                        break;
                    }
                case 3:
                    if (i8 >= 25) {
                        if (i8 == 900) {
                            aVar4 = a.ALPHA;
                        } else if (i8 != 913) {
                            switch (i8) {
                                case 25:
                                    aVar4 = a.PUNCT;
                                    break;
                                case 27:
                                    aVar4 = a.LOWER;
                                    break;
                                case 28:
                                    aVar4 = a.ALPHA;
                                    break;
                                case 29:
                                    aVar = a.PUNCT_SHIFT;
                                    c4 = 0;
                                    a aVar5222 = aVar;
                                    aVar3 = aVar4;
                                    aVar4 = aVar5222;
                                    break;
                            }
                        } else {
                            sb2.append((char) iArr3[i7]);
                        }
                        c4 = 0;
                        break;
                    } else {
                        c2 = f3068b[i8];
                        c4 = c2;
                        break;
                    }
                    break;
                case 4:
                    if (i8 >= 29) {
                        if (i8 == 29) {
                            aVar4 = a.ALPHA;
                        } else if (i8 == 900) {
                            aVar4 = a.ALPHA;
                        } else if (i8 == 913) {
                            sb2.append((char) iArr3[i7]);
                        }
                        c4 = 0;
                        break;
                    } else {
                        c2 = f3067a[i8];
                        c4 = c2;
                        break;
                    }
                case 5:
                    if (i8 < 26) {
                        c3 = (char) (i8 + 65);
                        c4 = c3;
                        aVar4 = aVar3;
                        break;
                    } else {
                        if (i8 != 26) {
                            if (i8 == 900) {
                                aVar4 = a.ALPHA;
                                c4 = 0;
                                break;
                            }
                            aVar4 = aVar3;
                            c4 = 0;
                        }
                        aVar4 = aVar3;
                    }
                case 6:
                    if (i8 >= 29) {
                        if (i8 == 29) {
                            aVar4 = a.ALPHA;
                        } else if (i8 != 900) {
                            if (i8 == 913) {
                                sb2.append((char) iArr3[i7]);
                            }
                            aVar4 = aVar3;
                        } else {
                            aVar4 = a.ALPHA;
                        }
                        c4 = 0;
                        break;
                    } else {
                        c3 = f3067a[i8];
                        c4 = c3;
                        aVar4 = aVar3;
                        break;
                    }
                default:
                    c4 = 0;
                    break;
            }
            if (c4 != 0) {
                sb2.append(c4);
            }
        }
        return i3;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0046 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0021 A[ADDED_TO_REGION, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int a(int r16, int[] r17, java.nio.charset.Charset r18, int r19, java.lang.StringBuilder r20) {
        /*
            r0 = r16
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream
            r1.<init>()
            r2 = 901(0x385, float:1.263E-42)
            r3 = 900(0x384, double:4.447E-321)
            r5 = 928(0x3a0, float:1.3E-42)
            r6 = 900(0x384, float:1.261E-42)
            r7 = 6
            r10 = 0
            if (r0 == r2) goto L_0x0059
            r2 = 924(0x39c, float:1.295E-42)
            if (r0 == r2) goto L_0x001b
            r0 = r19
            goto L_0x00c9
        L_0x001b:
            r0 = r19
            r2 = 0
        L_0x001e:
            r12 = 0
            r13 = 0
        L_0x0021:
            r15 = r17[r10]
            if (r0 >= r15) goto L_0x00c9
            if (r2 != 0) goto L_0x00c9
            int r15 = r0 + 1
            r0 = r17[r0]
            if (r0 >= r6) goto L_0x0034
            int r12 = r12 + 1
            long r13 = r13 * r3
            long r8 = (long) r0
            long r13 = r13 + r8
            goto L_0x003c
        L_0x0034:
            if (r0 == r5) goto L_0x003e
            switch(r0) {
                case 900: goto L_0x003e;
                case 901: goto L_0x003e;
                case 902: goto L_0x003e;
                default: goto L_0x0039;
            }
        L_0x0039:
            switch(r0) {
                case 922: goto L_0x003e;
                case 923: goto L_0x003e;
                case 924: goto L_0x003e;
                default: goto L_0x003c;
            }
        L_0x003c:
            r0 = r15
            goto L_0x0042
        L_0x003e:
            int r15 = r15 + -1
            r0 = r15
            r2 = 1
        L_0x0042:
            int r8 = r12 % 5
            if (r8 != 0) goto L_0x0021
            if (r12 <= 0) goto L_0x0021
            r8 = 0
        L_0x0049:
            if (r8 >= r7) goto L_0x001e
            int r9 = 5 - r8
            int r9 = r9 * 8
            long r11 = r13 >> r9
            int r9 = (int) r11
            byte r9 = (byte) r9
            r1.write(r9)
            int r8 = r8 + 1
            goto L_0x0049
        L_0x0059:
            int[] r0 = new int[r7]
            int r2 = r19 + 1
            r8 = r17[r19]
            r9 = r8
            r8 = 0
        L_0x0061:
            r11 = 0
            r12 = 0
        L_0x0064:
            r14 = r17[r10]
            if (r2 >= r14) goto L_0x00b2
            if (r8 != 0) goto L_0x00b2
            int r14 = r11 + 1
            r0[r11] = r9
            long r12 = r12 * r3
            long r3 = (long) r9
            long r12 = r12 + r3
            int r3 = r2 + 1
            r9 = r17[r2]
            if (r9 == r5) goto L_0x00aa
            switch(r9) {
                case 900: goto L_0x00aa;
                case 901: goto L_0x00aa;
                case 902: goto L_0x00aa;
                default: goto L_0x007b;
            }
        L_0x007b:
            switch(r9) {
                case 922: goto L_0x00aa;
                case 923: goto L_0x00aa;
                case 924: goto L_0x00aa;
                default: goto L_0x007e;
            }
        L_0x007e:
            int r2 = r14 % 5
            if (r2 != 0) goto L_0x00a0
            if (r14 <= 0) goto L_0x00a0
            r2 = 0
        L_0x0085:
            if (r2 >= r7) goto L_0x009a
            int r4 = 5 - r2
            int r4 = r4 * 8
            r16 = r8
            long r7 = r12 >> r4
            int r4 = (int) r7
            byte r4 = (byte) r4
            r1.write(r4)
            int r2 = r2 + 1
            r7 = 6
            r8 = r16
            goto L_0x0085
        L_0x009a:
            r16 = r8
            r2 = r3
            r3 = 900(0x384, double:4.447E-321)
            goto L_0x0061
        L_0x00a0:
            r16 = r8
            r8 = r16
            r2 = r3
            r11 = r14
            r3 = 900(0x384, double:4.447E-321)
            r7 = 6
            goto L_0x0064
        L_0x00aa:
            int r2 = r3 + -1
            r11 = r14
            r3 = 900(0x384, double:4.447E-321)
            r7 = 6
            r8 = 1
            goto L_0x0064
        L_0x00b2:
            r3 = r17[r10]
            if (r2 != r3) goto L_0x00bd
            if (r9 >= r6) goto L_0x00bd
            int r3 = r11 + 1
            r0[r11] = r9
            r11 = r3
        L_0x00bd:
            if (r10 >= r11) goto L_0x00c8
            r3 = r0[r10]
            byte r3 = (byte) r3
            r1.write(r3)
            int r10 = r10 + 1
            goto L_0x00bd
        L_0x00c8:
            r0 = r2
        L_0x00c9:
            java.lang.String r2 = new java.lang.String
            byte[] r1 = r1.toByteArray()
            r3 = r18
            r2.<init>(r1, r3)
            r1 = r20
            r1.append(r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.e.a.e.a(int, int[], java.nio.charset.Charset, int, java.lang.StringBuilder):int");
    }

    private static int b(int[] iArr, int i, StringBuilder sb) throws FormatException {
        int[] iArr2 = new int[15];
        boolean z = false;
        int i2 = 0;
        while (i < iArr[0] && !z) {
            int i3 = i + 1;
            int i4 = iArr[i];
            if (i3 == iArr[0]) {
                z = true;
            }
            if (i4 < 900) {
                iArr2[i2] = i4;
                i2++;
            } else {
                if (!(i4 == 900 || i4 == 901 || i4 == 928)) {
                    switch (i4) {
                    }
                }
                i3--;
                z = true;
            }
            if ((i2 % 15 == 0 || i4 == 902 || z) && i2 > 0) {
                sb.append(a(iArr2, i2));
                i2 = 0;
            }
            i = i3;
        }
        return i;
    }

    private static String a(int[] iArr, int i) throws FormatException {
        BigInteger bigInteger = BigInteger.ZERO;
        for (int i2 = 0; i2 < i; i2++) {
            bigInteger = bigInteger.add(c[(i - i2) - 1].multiply(BigInteger.valueOf((long) iArr[i2])));
        }
        String bigInteger2 = bigInteger.toString();
        if (bigInteger2.charAt(0) == '1') {
            return bigInteger2.substring(1);
        }
        throw FormatException.a();
    }
}
