package com.google.zxing.e.a;

import com.google.zxing.NotFoundException;
import com.google.zxing.common.b;
import com.google.zxing.e.a.a.a;
import com.google.zxing.p;

/* compiled from: PDF417ScanningDecoder */
public final class k {

    /* renamed from: a  reason: collision with root package name */
    private static final a f3077a = new a();

    private static boolean a(int i, int i2, int i3) {
        return i2 + -2 <= i && i <= i3 + 2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.zxing.e.a.k.a(com.google.zxing.common.b, com.google.zxing.e.a.c, com.google.zxing.p, boolean, int, int):com.google.zxing.e.a.i
     arg types: [com.google.zxing.common.b, com.google.zxing.e.a.c, com.google.zxing.p, int, int, int]
     candidates:
      com.google.zxing.e.a.k.a(com.google.zxing.common.b, int, int, boolean, int, int):int[]
      com.google.zxing.e.a.k.a(com.google.zxing.common.b, com.google.zxing.e.a.c, com.google.zxing.p, boolean, int, int):com.google.zxing.e.a.i */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0064, code lost:
        if (r3.e != r4.e) goto L_0x0069;
     */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x01af  */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x01de  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:291:0x00c0 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00a0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.google.zxing.common.e a(com.google.zxing.common.b r22, com.google.zxing.p r23, com.google.zxing.p r24, com.google.zxing.p r25, com.google.zxing.p r26, int r27, int r28) throws com.google.zxing.NotFoundException, com.google.zxing.FormatException, com.google.zxing.ChecksumException {
        /*
            com.google.zxing.e.a.c r6 = new com.google.zxing.e.a.c
            r0 = r6
            r1 = r22
            r2 = r23
            r3 = r24
            r4 = r25
            r5 = r26
            r0.<init>(r1, r2, r3, r4, r5)
            r1 = 0
            r9 = r6
            r2 = 0
            r3 = 0
            r4 = 0
            r10 = 0
        L_0x0016:
            r11 = 2
            if (r2 >= r11) goto L_0x00c5
            if (r23 == 0) goto L_0x0029
            r6 = 1
            r3 = r22
            r4 = r9
            r5 = r23
            r7 = r27
            r8 = r28
            com.google.zxing.e.a.i r3 = a(r3, r4, r5, r6, r7, r8)
        L_0x0029:
            r12 = r3
            if (r25 == 0) goto L_0x003b
            r6 = 0
            r3 = r22
            r4 = r9
            r5 = r25
            r7 = r27
            r8 = r28
            com.google.zxing.e.a.i r3 = a(r3, r4, r5, r6, r7, r8)
            r10 = r3
        L_0x003b:
            if (r12 != 0) goto L_0x0042
            if (r10 != 0) goto L_0x0042
        L_0x003f:
            r4 = 0
            goto L_0x009e
        L_0x0042:
            if (r12 == 0) goto L_0x0067
            com.google.zxing.e.a.a r3 = r12.b()
            if (r3 != 0) goto L_0x004b
            goto L_0x0067
        L_0x004b:
            if (r10 == 0) goto L_0x006f
            com.google.zxing.e.a.a r4 = r10.b()
            if (r4 != 0) goto L_0x0054
            goto L_0x006f
        L_0x0054:
            int r5 = r3.f3055a
            int r6 = r4.f3055a
            if (r5 == r6) goto L_0x006f
            int r5 = r3.f3056b
            int r6 = r4.f3056b
            if (r5 == r6) goto L_0x006f
            int r5 = r3.e
            int r4 = r4.e
            if (r5 == r4) goto L_0x006f
            goto L_0x0069
        L_0x0067:
            if (r10 != 0) goto L_0x006b
        L_0x0069:
            r3 = 0
            goto L_0x006f
        L_0x006b:
            com.google.zxing.e.a.a r3 = r10.b()
        L_0x006f:
            if (r3 != 0) goto L_0x0072
            goto L_0x003f
        L_0x0072:
            com.google.zxing.e.a.c r4 = a(r12)
            com.google.zxing.e.a.c r5 = a(r10)
            if (r4 != 0) goto L_0x007e
            r4 = r5
            goto L_0x0098
        L_0x007e:
            if (r5 != 0) goto L_0x0081
            goto L_0x0098
        L_0x0081:
            com.google.zxing.e.a.c r6 = new com.google.zxing.e.a.c
            com.google.zxing.common.b r14 = r4.f3063a
            com.google.zxing.p r15 = r4.f3064b
            com.google.zxing.p r4 = r4.c
            com.google.zxing.p r7 = r5.d
            com.google.zxing.p r5 = r5.e
            r13 = r6
            r16 = r4
            r17 = r7
            r18 = r5
            r13.<init>(r14, r15, r16, r17, r18)
            r4 = r6
        L_0x0098:
            com.google.zxing.e.a.g r5 = new com.google.zxing.e.a.g
            r5.<init>(r3, r4)
            r4 = r5
        L_0x009e:
            if (r4 == 0) goto L_0x00c0
            if (r2 != 0) goto L_0x00bd
            com.google.zxing.e.a.c r3 = r4.f3073b
            if (r3 == 0) goto L_0x00bd
            com.google.zxing.e.a.c r3 = r4.f3073b
            int r3 = r3.h
            int r5 = r9.h
            if (r3 < r5) goto L_0x00b6
            com.google.zxing.e.a.c r3 = r4.f3073b
            int r3 = r3.i
            int r5 = r9.i
            if (r3 <= r5) goto L_0x00bd
        L_0x00b6:
            com.google.zxing.e.a.c r9 = r4.f3073b
            int r2 = r2 + 1
            r3 = r12
            goto L_0x0016
        L_0x00bd:
            r4.f3073b = r9
            goto L_0x00c6
        L_0x00c0:
            com.google.zxing.NotFoundException r0 = com.google.zxing.NotFoundException.a()
            throw r0
        L_0x00c5:
            r12 = r3
        L_0x00c6:
            int r2 = r4.c
            r3 = 1
            int r2 = r2 + r3
            com.google.zxing.e.a.h[] r5 = r4.f3072a
            r5[r1] = r12
            com.google.zxing.e.a.h[] r5 = r4.f3072a
            r5[r2] = r10
            if (r12 == 0) goto L_0x00d6
            r5 = 1
            goto L_0x00d7
        L_0x00d6:
            r5 = 0
        L_0x00d7:
            r7 = r27
            r8 = r28
            r6 = 1
        L_0x00dc:
            if (r6 > r2) goto L_0x01fc
            if (r5 == 0) goto L_0x00e2
            r10 = r6
            goto L_0x00e4
        L_0x00e2:
            int r10 = r2 - r6
        L_0x00e4:
            com.google.zxing.e.a.h[] r12 = r4.f3072a
            r12 = r12[r10]
            if (r12 != 0) goto L_0x01f1
            if (r10 == 0) goto L_0x00f5
            if (r10 != r2) goto L_0x00ef
            goto L_0x00f5
        L_0x00ef:
            com.google.zxing.e.a.h r12 = new com.google.zxing.e.a.h
            r12.<init>(r9)
            goto L_0x00ff
        L_0x00f5:
            com.google.zxing.e.a.i r12 = new com.google.zxing.e.a.i
            if (r10 != 0) goto L_0x00fb
            r13 = 1
            goto L_0x00fc
        L_0x00fb:
            r13 = 0
        L_0x00fc:
            r12.<init>(r9, r13)
        L_0x00ff:
            com.google.zxing.e.a.h[] r13 = r4.f3072a
            r13[r10] = r12
            int r13 = r9.h
            r14 = r8
            r8 = r7
            r7 = r13
            r13 = -1
        L_0x0109:
            int r0 = r9.i
            if (r7 > r0) goto L_0x01eb
            if (r5 == 0) goto L_0x0111
            r0 = 1
            goto L_0x0112
        L_0x0111:
            r0 = -1
        L_0x0112:
            int r3 = r10 - r0
            boolean r16 = a(r4, r3)
            if (r16 == 0) goto L_0x0123
            com.google.zxing.e.a.h[] r1 = r4.f3072a
            r1 = r1[r3]
            com.google.zxing.e.a.d r1 = r1.c(r7)
            goto L_0x0124
        L_0x0123:
            r1 = 0
        L_0x0124:
            if (r1 == 0) goto L_0x0131
            if (r5 == 0) goto L_0x012b
            int r0 = r1.f3066b
            goto L_0x012d
        L_0x012b:
            int r0 = r1.f3065a
        L_0x012d:
            r21 = r2
            goto L_0x01a2
        L_0x0131:
            com.google.zxing.e.a.h[] r1 = r4.f3072a
            r1 = r1[r10]
            com.google.zxing.e.a.d r1 = r1.a(r7)
            if (r1 == 0) goto L_0x0143
            if (r5 == 0) goto L_0x0140
            int r0 = r1.f3065a
            goto L_0x012d
        L_0x0140:
            int r0 = r1.f3066b
            goto L_0x012d
        L_0x0143:
            boolean r16 = a(r4, r3)
            if (r16 == 0) goto L_0x0151
            com.google.zxing.e.a.h[] r1 = r4.f3072a
            r1 = r1[r3]
            com.google.zxing.e.a.d r1 = r1.a(r7)
        L_0x0151:
            if (r1 == 0) goto L_0x015b
            if (r5 == 0) goto L_0x0158
            int r0 = r1.f3066b
            goto L_0x012d
        L_0x0158:
            int r0 = r1.f3065a
            goto L_0x012d
        L_0x015b:
            r1 = r10
            r3 = 0
        L_0x015d:
            int r1 = r1 - r0
            boolean r16 = a(r4, r1)
            if (r16 == 0) goto L_0x0195
            com.google.zxing.e.a.h[] r11 = r4.f3072a
            r11 = r11[r1]
            com.google.zxing.e.a.d[] r11 = r11.f3075b
            int r15 = r11.length
            r27 = r1
            r1 = 0
        L_0x016e:
            if (r1 >= r15) goto L_0x018d
            r21 = r2
            r2 = r11[r1]
            if (r2 == 0) goto L_0x0188
            if (r5 == 0) goto L_0x017b
            int r1 = r2.f3066b
            goto L_0x017d
        L_0x017b:
            int r1 = r2.f3065a
        L_0x017d:
            int r0 = r0 * r3
            int r3 = r2.f3066b
            int r2 = r2.f3065a
            int r3 = r3 - r2
            int r0 = r0 * r3
            int r0 = r0 + r1
            goto L_0x01a2
        L_0x0188:
            int r1 = r1 + 1
            r2 = r21
            goto L_0x016e
        L_0x018d:
            r21 = r2
            int r3 = r3 + 1
            r1 = r27
            r11 = 2
            goto L_0x015d
        L_0x0195:
            r21 = r2
            if (r5 == 0) goto L_0x019e
            com.google.zxing.e.a.c r0 = r4.f3073b
            int r0 = r0.f
            goto L_0x01a2
        L_0x019e:
            com.google.zxing.e.a.c r0 = r4.f3073b
            int r0 = r0.g
        L_0x01a2:
            if (r0 < 0) goto L_0x01ac
            int r1 = r9.g
            if (r0 <= r1) goto L_0x01a9
            goto L_0x01ac
        L_0x01a9:
            r1 = r0
            r0 = -1
            goto L_0x01b0
        L_0x01ac:
            r0 = -1
            if (r13 == r0) goto L_0x01de
            r1 = r13
        L_0x01b0:
            int r2 = r9.f
            int r15 = r9.g
            r3 = r13
            r13 = r22
            r11 = r14
            r14 = r2
            r16 = r5
            r17 = r1
            r18 = r7
            r19 = r8
            r20 = r11
            com.google.zxing.e.a.d r2 = a(r13, r14, r15, r16, r17, r18, r19, r20)
            if (r2 == 0) goto L_0x01e0
            r12.a(r7, r2)
            int r3 = r2.c()
            int r8 = java.lang.Math.min(r8, r3)
            int r2 = r2.c()
            int r14 = java.lang.Math.max(r11, r2)
            r13 = r1
            goto L_0x01e2
        L_0x01de:
            r3 = r13
            r11 = r14
        L_0x01e0:
            r13 = r3
            r14 = r11
        L_0x01e2:
            int r7 = r7 + 1
            r2 = r21
            r1 = 0
            r3 = 1
            r11 = 2
            goto L_0x0109
        L_0x01eb:
            r21 = r2
            r11 = r14
            r7 = r8
            r8 = r11
            goto L_0x01f3
        L_0x01f1:
            r21 = r2
        L_0x01f3:
            int r6 = r6 + 1
            r2 = r21
            r1 = 0
            r3 = 1
            r11 = 2
            goto L_0x00dc
        L_0x01fc:
            int r0 = r4.a()
            int r1 = r4.c
            r2 = 2
            int r1 = r1 + r2
            int[] r0 = new int[]{r0, r1}
            java.lang.Class<com.google.zxing.e.a.b> r1 = com.google.zxing.e.a.b.class
            java.lang.Object r0 = java.lang.reflect.Array.newInstance(r1, r0)
            com.google.zxing.e.a.b[][] r0 = (com.google.zxing.e.a.b[][]) r0
            r1 = 0
        L_0x0211:
            int r2 = r0.length
            if (r1 >= r2) goto L_0x0229
            r2 = 0
        L_0x0215:
            r3 = r0[r1]
            int r3 = r3.length
            if (r2 >= r3) goto L_0x0226
            r3 = r0[r1]
            com.google.zxing.e.a.b r5 = new com.google.zxing.e.a.b
            r5.<init>()
            r3[r2] = r5
            int r2 = r2 + 1
            goto L_0x0215
        L_0x0226:
            int r1 = r1 + 1
            goto L_0x0211
        L_0x0229:
            com.google.zxing.e.a.h[] r1 = r4.f3072a
            r2 = 0
            r1 = r1[r2]
            r4.a(r1)
            com.google.zxing.e.a.h[] r1 = r4.f3072a
            int r3 = r4.c
            r5 = 1
            int r3 = r3 + r5
            r1 = r1[r3]
            r4.a(r1)
            r1 = 928(0x3a0, float:1.3E-42)
            r3 = 928(0x3a0, float:1.3E-42)
        L_0x0240:
            com.google.zxing.e.a.h[] r6 = r4.f3072a
            r6 = r6[r2]
            if (r6 == 0) goto L_0x02a2
            com.google.zxing.e.a.h[] r6 = r4.f3072a
            int r7 = r4.c
            int r7 = r7 + r5
            r6 = r6[r7]
            if (r6 != 0) goto L_0x0250
            goto L_0x02a2
        L_0x0250:
            com.google.zxing.e.a.h[] r6 = r4.f3072a
            r6 = r6[r2]
            com.google.zxing.e.a.d[] r2 = r6.f3075b
            com.google.zxing.e.a.h[] r6 = r4.f3072a
            int r7 = r4.c
            int r7 = r7 + r5
            r5 = r6[r7]
            com.google.zxing.e.a.d[] r5 = r5.f3075b
            r6 = 0
        L_0x0260:
            int r7 = r2.length
            if (r6 >= r7) goto L_0x02a2
            r7 = r2[r6]
            if (r7 == 0) goto L_0x029e
            r7 = r5[r6]
            if (r7 == 0) goto L_0x029e
            r7 = r2[r6]
            int r7 = r7.e
            r8 = r5[r6]
            int r8 = r8.e
            if (r7 != r8) goto L_0x029e
            r7 = 1
        L_0x0276:
            int r8 = r4.c
            if (r7 > r8) goto L_0x029e
            com.google.zxing.e.a.h[] r8 = r4.f3072a
            r8 = r8[r7]
            com.google.zxing.e.a.d[] r8 = r8.f3075b
            r8 = r8[r6]
            if (r8 == 0) goto L_0x029a
            r9 = r2[r6]
            int r9 = r9.e
            r8.e = r9
            boolean r8 = r8.a()
            if (r8 != 0) goto L_0x029a
            com.google.zxing.e.a.h[] r8 = r4.f3072a
            r8 = r8[r7]
            com.google.zxing.e.a.d[] r8 = r8.f3075b
            r9 = 0
            r8[r6] = r9
            goto L_0x029b
        L_0x029a:
            r9 = 0
        L_0x029b:
            int r7 = r7 + 1
            goto L_0x0276
        L_0x029e:
            r9 = 0
            int r6 = r6 + 1
            goto L_0x0260
        L_0x02a2:
            r9 = 0
            com.google.zxing.e.a.h[] r2 = r4.f3072a
            r5 = 0
            r2 = r2[r5]
            if (r2 != 0) goto L_0x02ab
            goto L_0x02e8
        L_0x02ab:
            com.google.zxing.e.a.h[] r2 = r4.f3072a
            r2 = r2[r5]
            com.google.zxing.e.a.d[] r2 = r2.f3075b
            r5 = 0
            r6 = 0
        L_0x02b3:
            int r7 = r2.length
            if (r5 >= r7) goto L_0x02e7
            r7 = r2[r5]
            if (r7 == 0) goto L_0x02e4
            r7 = r2[r5]
            int r7 = r7.e
            r10 = r6
            r6 = 1
            r8 = 0
        L_0x02c1:
            int r11 = r4.c
            r12 = 1
            int r11 = r11 + r12
            if (r6 >= r11) goto L_0x02e3
            r11 = 2
            if (r8 >= r11) goto L_0x02e3
            com.google.zxing.e.a.h[] r11 = r4.f3072a
            r11 = r11[r6]
            com.google.zxing.e.a.d[] r11 = r11.f3075b
            r11 = r11[r5]
            if (r11 == 0) goto L_0x02e0
            int r8 = com.google.zxing.e.a.g.a(r7, r8, r11)
            boolean r11 = r11.a()
            if (r11 != 0) goto L_0x02e0
            int r10 = r10 + 1
        L_0x02e0:
            int r6 = r6 + 1
            goto L_0x02c1
        L_0x02e3:
            r6 = r10
        L_0x02e4:
            int r5 = r5 + 1
            goto L_0x02b3
        L_0x02e7:
            r5 = r6
        L_0x02e8:
            com.google.zxing.e.a.h[] r2 = r4.f3072a
            int r6 = r4.c
            r7 = 1
            int r6 = r6 + r7
            r2 = r2[r6]
            if (r2 != 0) goto L_0x02f4
            r8 = 0
            goto L_0x0330
        L_0x02f4:
            com.google.zxing.e.a.h[] r2 = r4.f3072a
            int r6 = r4.c
            int r6 = r6 + r7
            r2 = r2[r6]
            com.google.zxing.e.a.d[] r2 = r2.f3075b
            r6 = 0
            r8 = 0
        L_0x02ff:
            int r10 = r2.length
            if (r6 >= r10) goto L_0x0330
            r10 = r2[r6]
            if (r10 == 0) goto L_0x032c
            r10 = r2[r6]
            int r10 = r10.e
            int r11 = r4.c
            int r11 = r11 + r7
            r7 = 0
        L_0x030e:
            if (r11 <= 0) goto L_0x032c
            r12 = 2
            if (r7 >= r12) goto L_0x032c
            com.google.zxing.e.a.h[] r12 = r4.f3072a
            r12 = r12[r11]
            com.google.zxing.e.a.d[] r12 = r12.f3075b
            r12 = r12[r6]
            if (r12 == 0) goto L_0x0329
            int r7 = com.google.zxing.e.a.g.a(r10, r7, r12)
            boolean r12 = r12.a()
            if (r12 != 0) goto L_0x0329
            int r8 = r8 + 1
        L_0x0329:
            int r11 = r11 + -1
            goto L_0x030e
        L_0x032c:
            int r6 = r6 + 1
            r7 = 1
            goto L_0x02ff
        L_0x0330:
            int r2 = r5 + r8
            if (r2 != 0) goto L_0x0337
            r2 = 0
            goto L_0x03fd
        L_0x0337:
            r5 = 1
        L_0x0338:
            int r6 = r4.c
            r7 = 1
            int r6 = r6 + r7
            if (r5 >= r6) goto L_0x03fd
            com.google.zxing.e.a.h[] r6 = r4.f3072a
            r6 = r6[r5]
            com.google.zxing.e.a.d[] r6 = r6.f3075b
            r7 = 0
        L_0x0345:
            int r8 = r6.length
            if (r7 >= r8) goto L_0x03f9
            r8 = r6[r7]
            if (r8 == 0) goto L_0x03f5
            r8 = r6[r7]
            boolean r8 = r8.a()
            if (r8 != 0) goto L_0x03f5
            r8 = r6[r7]
            com.google.zxing.e.a.h[] r10 = r4.f3072a
            int r11 = r5 + -1
            r10 = r10[r11]
            com.google.zxing.e.a.d[] r10 = r10.f3075b
            com.google.zxing.e.a.h[] r11 = r4.f3072a
            int r12 = r5 + 1
            r11 = r11[r12]
            if (r11 == 0) goto L_0x036d
            com.google.zxing.e.a.h[] r11 = r4.f3072a
            r11 = r11[r12]
            com.google.zxing.e.a.d[] r11 = r11.f3075b
            goto L_0x036e
        L_0x036d:
            r11 = r10
        L_0x036e:
            r12 = 14
            com.google.zxing.e.a.d[] r13 = new com.google.zxing.e.a.d[r12]
            r14 = r10[r7]
            r15 = 2
            r13[r15] = r14
            r14 = 3
            r15 = r11[r7]
            r13[r14] = r15
            if (r7 <= 0) goto L_0x0390
            int r14 = r7 + -1
            r15 = r6[r14]
            r16 = 0
            r13[r16] = r15
            r15 = 4
            r16 = r10[r14]
            r13[r15] = r16
            r15 = 5
            r14 = r11[r14]
            r13[r15] = r14
        L_0x0390:
            r14 = 1
            if (r7 <= r14) goto L_0x03a7
            r14 = 8
            int r15 = r7 + -2
            r16 = r6[r15]
            r13[r14] = r16
            r14 = 10
            r16 = r10[r15]
            r13[r14] = r16
            r14 = 11
            r15 = r11[r15]
            r13[r14] = r15
        L_0x03a7:
            int r14 = r6.length
            r15 = 1
            int r14 = r14 - r15
            if (r7 >= r14) goto L_0x03bc
            int r14 = r7 + 1
            r16 = r6[r14]
            r13[r15] = r16
            r15 = 6
            r16 = r10[r14]
            r13[r15] = r16
            r15 = 7
            r14 = r11[r14]
            r13[r15] = r14
        L_0x03bc:
            int r14 = r6.length
            r15 = 2
            int r14 = r14 - r15
            if (r7 >= r14) goto L_0x03d5
            r14 = 9
            int r15 = r7 + 2
            r16 = r6[r15]
            r13[r14] = r16
            r14 = 12
            r10 = r10[r15]
            r13[r14] = r10
            r10 = 13
            r11 = r11[r15]
            r13[r10] = r11
        L_0x03d5:
            r10 = 0
        L_0x03d6:
            if (r10 >= r12) goto L_0x03f5
            r11 = r13[r10]
            if (r11 != 0) goto L_0x03de
        L_0x03dc:
            r11 = 0
            goto L_0x03ef
        L_0x03de:
            boolean r14 = r11.a()
            if (r14 == 0) goto L_0x03dc
            int r14 = r11.c
            int r15 = r8.c
            if (r14 != r15) goto L_0x03dc
            int r11 = r11.e
            r8.e = r11
            r11 = 1
        L_0x03ef:
            if (r11 == 0) goto L_0x03f2
            goto L_0x03f5
        L_0x03f2:
            int r10 = r10 + 1
            goto L_0x03d6
        L_0x03f5:
            int r7 = r7 + 1
            goto L_0x0345
        L_0x03f9:
            int r5 = r5 + 1
            goto L_0x0338
        L_0x03fd:
            if (r2 <= 0) goto L_0x0407
            if (r2 < r3) goto L_0x0402
            goto L_0x0407
        L_0x0402:
            r3 = r2
            r2 = 0
            r5 = 1
            goto L_0x0240
        L_0x0407:
            com.google.zxing.e.a.h[] r2 = r4.f3072a
            int r3 = r2.length
            r5 = 0
            r6 = 0
        L_0x040c:
            if (r5 >= r3) goto L_0x0434
            r7 = r2[r5]
            if (r7 == 0) goto L_0x042f
            com.google.zxing.e.a.d[] r7 = r7.f3075b
            int r8 = r7.length
            r9 = 0
        L_0x0416:
            if (r9 >= r8) goto L_0x042f
            r10 = r7[r9]
            if (r10 == 0) goto L_0x042c
            int r11 = r10.e
            if (r11 < 0) goto L_0x042c
            int r12 = r0.length
            if (r11 >= r12) goto L_0x042c
            r11 = r0[r11]
            r11 = r11[r6]
            int r10 = r10.d
            r11.a(r10)
        L_0x042c:
            int r9 = r9 + 1
            goto L_0x0416
        L_0x042f:
            int r6 = r6 + 1
            int r5 = r5 + 1
            goto L_0x040c
        L_0x0434:
            r5 = 0
            r2 = r0[r5]
            r3 = 1
            r2 = r2[r3]
            int[] r3 = r2.a()
            int r5 = r4.c
            int r6 = r4.a()
            int r5 = r5 * r6
            int r6 = r4.b()
            r7 = 2
            int r6 = r7 << r6
            int r5 = r5 - r6
            int r6 = r3.length
            if (r6 != 0) goto L_0x045e
            if (r5 <= 0) goto L_0x0459
            if (r5 > r1) goto L_0x0459
            r2.a(r5)
            goto L_0x0466
        L_0x0459:
            com.google.zxing.NotFoundException r0 = com.google.zxing.NotFoundException.a()
            throw r0
        L_0x045e:
            r1 = 0
            r3 = r3[r1]
            if (r3 == r5) goto L_0x0466
            r2.a(r5)
        L_0x0466:
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            int r2 = r4.a()
            int r3 = r4.c
            int r2 = r2 * r3
            int[] r2 = new int[r2]
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            r6 = 0
        L_0x0480:
            int r7 = r4.a()
            if (r6 >= r7) goto L_0x04c3
            r7 = 0
        L_0x0487:
            int r8 = r4.c
            if (r7 >= r8) goto L_0x04be
            r8 = r0[r6]
            int r9 = r7 + 1
            r8 = r8[r9]
            int[] r8 = r8.a()
            int r10 = r4.c
            int r10 = r10 * r6
            int r10 = r10 + r7
            int r7 = r8.length
            if (r7 != 0) goto L_0x04a7
            java.lang.Integer r7 = java.lang.Integer.valueOf(r10)
            r1.add(r7)
            r7 = 0
            r11 = 1
            goto L_0x04bc
        L_0x04a7:
            int r7 = r8.length
            r11 = 1
            if (r7 != r11) goto L_0x04b1
            r7 = 0
            r8 = r8[r7]
            r2[r10] = r8
            goto L_0x04bc
        L_0x04b1:
            r7 = 0
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)
            r5.add(r10)
            r3.add(r8)
        L_0x04bc:
            r7 = r9
            goto L_0x0487
        L_0x04be:
            r7 = 0
            r11 = 1
            int r6 = r6 + 1
            goto L_0x0480
        L_0x04c3:
            r7 = 0
            int r0 = r3.size()
            int[][] r0 = new int[r0][]
        L_0x04ca:
            int r6 = r0.length
            if (r7 >= r6) goto L_0x04d8
            java.lang.Object r6 = r3.get(r7)
            int[] r6 = (int[]) r6
            r0[r7] = r6
            int r7 = r7 + 1
            goto L_0x04ca
        L_0x04d8:
            int r3 = r4.b()
            int[] r1 = com.google.zxing.e.a.a(r1)
            int[] r4 = com.google.zxing.e.a.a(r5)
            com.google.zxing.common.e r0 = a(r3, r2, r1, r4, r0)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.e.a.k.a(com.google.zxing.common.b, com.google.zxing.p, com.google.zxing.p, com.google.zxing.p, com.google.zxing.p, int, int):com.google.zxing.common.e");
    }

    private static c a(i iVar) throws NotFoundException {
        int[] a2;
        if (iVar == null || (a2 = iVar.a()) == null) {
            return null;
        }
        int a3 = a(a2);
        int i = 0;
        int i2 = 0;
        for (int i3 : a2) {
            i2 += a3 - i3;
            if (i3 > 0) {
                break;
            }
        }
        d[] dVarArr = iVar.f3075b;
        int i4 = 0;
        while (i2 > 0 && dVarArr[i4] == null) {
            i2--;
            i4++;
        }
        for (int length = a2.length - 1; length >= 0; length--) {
            i += a3 - a2[length];
            if (a2[length] > 0) {
                break;
            }
        }
        int length2 = dVarArr.length - 1;
        while (i > 0 && dVarArr[length2] == null) {
            i--;
            length2--;
        }
        return iVar.f3074a.a(i2, i, iVar.c);
    }

    private static int a(int[] iArr) {
        int i = -1;
        for (int max : iArr) {
            i = Math.max(i, max);
        }
        return i;
    }

    private static i a(b bVar, c cVar, p pVar, boolean z, int i, int i2) {
        c cVar2 = cVar;
        p pVar2 = pVar;
        boolean z2 = z;
        i iVar = new i(cVar2, z2);
        int i3 = 0;
        while (i3 < 2) {
            int i4 = i3 == 0 ? 1 : -1;
            int i5 = (int) pVar2.f3153a;
            int i6 = (int) pVar2.f3154b;
            while (i6 <= cVar2.i && i6 >= cVar2.h) {
                d a2 = a(bVar, 0, bVar.f2968a, z, i5, i6, i, i2);
                if (a2 != null) {
                    iVar.a(i6, a2);
                    if (z2) {
                        i5 = a2.f3065a;
                    } else {
                        i5 = a2.f3066b;
                    }
                }
                i6 += i4;
            }
            i3++;
        }
        return iVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0220, code lost:
        r16 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0226, code lost:
        throw com.google.zxing.ChecksumException.a();
     */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x0235  */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x025d A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.google.zxing.common.e a(int r20, int[] r21, int[] r22, int[] r23, int[][] r24) throws com.google.zxing.FormatException, com.google.zxing.ChecksumException {
        /*
            r0 = r21
            r1 = r22
            r2 = r23
            int r3 = r2.length
            int[] r3 = new int[r3]
            r4 = 100
        L_0x000b:
            int r5 = r4 + -1
            if (r4 <= 0) goto L_0x0262
            r6 = 0
        L_0x0010:
            int r7 = r3.length
            if (r6 >= r7) goto L_0x0020
            r7 = r2[r6]
            r8 = r24[r6]
            r9 = r3[r6]
            r8 = r8[r9]
            r0[r7] = r8
            int r6 = r6 + 1
            goto L_0x0010
        L_0x0020:
            r6 = 1
            int r7 = r0.length     // Catch:{ ChecksumException -> 0x0230 }
            if (r7 == 0) goto L_0x0227
            int r7 = r20 + 1
            int r7 = r6 << r7
            if (r1 == 0) goto L_0x0031
            int r8 = r1.length     // Catch:{ ChecksumException -> 0x0230 }
            int r9 = r7 / 2
            int r9 = r9 + 3
            if (r8 > r9) goto L_0x0220
        L_0x0031:
            if (r7 < 0) goto L_0x0220
            r8 = 512(0x200, float:7.175E-43)
            if (r7 > r8) goto L_0x0220
            com.google.zxing.e.a.a.a r8 = com.google.zxing.e.a.k.f3077a     // Catch:{ ChecksumException -> 0x0230 }
            com.google.zxing.e.a.a.c r9 = new com.google.zxing.e.a.a.c     // Catch:{ ChecksumException -> 0x0230 }
            com.google.zxing.e.a.a.b r10 = r8.f3057a     // Catch:{ ChecksumException -> 0x0230 }
            r9.<init>(r10, r0)     // Catch:{ ChecksumException -> 0x0230 }
            int[] r10 = new int[r7]     // Catch:{ ChecksumException -> 0x0230 }
            r11 = r7
            r12 = 0
        L_0x0044:
            if (r11 <= 0) goto L_0x005a
            com.google.zxing.e.a.a.b r13 = r8.f3057a     // Catch:{ ChecksumException -> 0x0230 }
            int[] r13 = r13.f3059b     // Catch:{ ChecksumException -> 0x0230 }
            r13 = r13[r11]     // Catch:{ ChecksumException -> 0x0230 }
            int r13 = r9.b(r13)     // Catch:{ ChecksumException -> 0x0230 }
            int r14 = r7 - r11
            r10[r14] = r13     // Catch:{ ChecksumException -> 0x0230 }
            if (r13 == 0) goto L_0x0057
            r12 = 1
        L_0x0057:
            int r11 = r11 + -1
            goto L_0x0044
        L_0x005a:
            if (r12 != 0) goto L_0x0061
            r16 = r5
            r4 = 0
            goto L_0x01e1
        L_0x0061:
            com.google.zxing.e.a.a.b r9 = r8.f3057a     // Catch:{ ChecksumException -> 0x0230 }
            com.google.zxing.e.a.a.c r9 = r9.e     // Catch:{ ChecksumException -> 0x0230 }
            r11 = 2
            if (r1 == 0) goto L_0x0098
            int r12 = r1.length     // Catch:{ ChecksumException -> 0x0230 }
            r13 = r9
            r9 = 0
        L_0x006b:
            if (r9 >= r12) goto L_0x0098
            r14 = r1[r9]     // Catch:{ ChecksumException -> 0x0230 }
            com.google.zxing.e.a.a.b r15 = r8.f3057a     // Catch:{ ChecksumException -> 0x0230 }
            int r4 = r0.length     // Catch:{ ChecksumException -> 0x0230 }
            int r4 = r4 - r6
            int r4 = r4 - r14
            int[] r14 = r15.f3059b     // Catch:{ ChecksumException -> 0x0230 }
            r4 = r14[r4]     // Catch:{ ChecksumException -> 0x0230 }
            com.google.zxing.e.a.a.c r14 = new com.google.zxing.e.a.a.c     // Catch:{ ChecksumException -> 0x0230 }
            com.google.zxing.e.a.a.b r15 = r8.f3057a     // Catch:{ ChecksumException -> 0x0230 }
            int[] r6 = new int[r11]     // Catch:{ ChecksumException -> 0x0230 }
            com.google.zxing.e.a.a.b r11 = r8.f3057a     // Catch:{ ChecksumException -> 0x0230 }
            r2 = 0
            int r4 = r11.c(r2, r4)     // Catch:{ ChecksumException -> 0x0230 }
            r6[r2] = r4     // Catch:{ ChecksumException -> 0x0230 }
            r2 = 1
            r6[r2] = r2     // Catch:{ ChecksumException -> 0x0230 }
            r14.<init>(r15, r6)     // Catch:{ ChecksumException -> 0x0230 }
            com.google.zxing.e.a.a.c r13 = r13.c(r14)     // Catch:{ ChecksumException -> 0x0230 }
            int r9 = r9 + 1
            r2 = r23
            r6 = 1
            r11 = 2
            goto L_0x006b
        L_0x0098:
            com.google.zxing.e.a.a.c r2 = new com.google.zxing.e.a.a.c     // Catch:{ ChecksumException -> 0x0230 }
            com.google.zxing.e.a.a.b r4 = r8.f3057a     // Catch:{ ChecksumException -> 0x0230 }
            r2.<init>(r4, r10)     // Catch:{ ChecksumException -> 0x0230 }
            com.google.zxing.e.a.a.b r4 = r8.f3057a     // Catch:{ ChecksumException -> 0x0230 }
            r6 = 1
            com.google.zxing.e.a.a.c r4 = r4.a(r7, r6)     // Catch:{ ChecksumException -> 0x0230 }
            int[] r9 = r4.f3061b     // Catch:{ ChecksumException -> 0x0230 }
            int r9 = r9.length     // Catch:{ ChecksumException -> 0x0230 }
            int r9 = r9 - r6
            int[] r10 = r2.f3061b     // Catch:{ ChecksumException -> 0x0230 }
            int r10 = r10.length     // Catch:{ ChecksumException -> 0x0230 }
            int r10 = r10 - r6
            if (r9 >= r10) goto L_0x00b1
            goto L_0x00b6
        L_0x00b1:
            r18 = r4
            r4 = r2
            r2 = r18
        L_0x00b6:
            com.google.zxing.e.a.a.b r6 = r8.f3057a     // Catch:{ ChecksumException -> 0x0230 }
            com.google.zxing.e.a.a.c r6 = r6.d     // Catch:{ ChecksumException -> 0x0230 }
            com.google.zxing.e.a.a.b r9 = r8.f3057a     // Catch:{ ChecksumException -> 0x0230 }
            com.google.zxing.e.a.a.c r9 = r9.e     // Catch:{ ChecksumException -> 0x0230 }
            r18 = r4
            r4 = r2
            r2 = r18
            r19 = r9
            r9 = r6
            r6 = r19
        L_0x00c8:
            int[] r10 = r2.f3061b     // Catch:{ ChecksumException -> 0x0230 }
            int r10 = r10.length     // Catch:{ ChecksumException -> 0x0230 }
            r11 = 1
            int r10 = r10 - r11
            int r11 = r7 / 2
            if (r10 < r11) goto L_0x0186
            boolean r10 = r2.a()     // Catch:{ ChecksumException -> 0x0230 }
            if (r10 != 0) goto L_0x017f
            com.google.zxing.e.a.a.b r10 = r8.f3057a     // Catch:{ ChecksumException -> 0x0230 }
            com.google.zxing.e.a.a.c r10 = r10.d     // Catch:{ ChecksumException -> 0x0230 }
            int[] r11 = r2.f3061b     // Catch:{ ChecksumException -> 0x0230 }
            int r11 = r11.length     // Catch:{ ChecksumException -> 0x0230 }
            r12 = 1
            int r11 = r11 - r12
            int r11 = r2.a(r11)     // Catch:{ ChecksumException -> 0x0230 }
            com.google.zxing.e.a.a.b r12 = r8.f3057a     // Catch:{ ChecksumException -> 0x0230 }
            int r11 = r12.a(r11)     // Catch:{ ChecksumException -> 0x0230 }
        L_0x00ea:
            int[] r12 = r4.f3061b     // Catch:{ ChecksumException -> 0x0230 }
            int r12 = r12.length     // Catch:{ ChecksumException -> 0x0230 }
            r13 = 1
            int r12 = r12 - r13
            int[] r14 = r2.f3061b     // Catch:{ ChecksumException -> 0x0230 }
            int r14 = r14.length     // Catch:{ ChecksumException -> 0x0230 }
            int r14 = r14 - r13
            if (r12 < r14) goto L_0x0166
            boolean r12 = r4.a()     // Catch:{ ChecksumException -> 0x0230 }
            if (r12 != 0) goto L_0x0166
            int[] r12 = r4.f3061b     // Catch:{ ChecksumException -> 0x0230 }
            int r12 = r12.length     // Catch:{ ChecksumException -> 0x0230 }
            int r12 = r12 - r13
            int[] r14 = r2.f3061b     // Catch:{ ChecksumException -> 0x0230 }
            int r14 = r14.length     // Catch:{ ChecksumException -> 0x0230 }
            int r14 = r14 - r13
            int r12 = r12 - r14
            com.google.zxing.e.a.a.b r14 = r8.f3057a     // Catch:{ ChecksumException -> 0x0230 }
            int[] r15 = r4.f3061b     // Catch:{ ChecksumException -> 0x0230 }
            int r15 = r15.length     // Catch:{ ChecksumException -> 0x0230 }
            int r15 = r15 - r13
            int r13 = r4.a(r15)     // Catch:{ ChecksumException -> 0x0230 }
            int r13 = r14.d(r13, r11)     // Catch:{ ChecksumException -> 0x0230 }
            com.google.zxing.e.a.a.b r14 = r8.f3057a     // Catch:{ ChecksumException -> 0x0230 }
            com.google.zxing.e.a.a.c r14 = r14.a(r12, r13)     // Catch:{ ChecksumException -> 0x0230 }
            com.google.zxing.e.a.a.c r10 = r10.a(r14)     // Catch:{ ChecksumException -> 0x0230 }
            if (r12 < 0) goto L_0x015e
            if (r13 != 0) goto L_0x0129
            com.google.zxing.e.a.a.b r12 = r2.f3060a     // Catch:{ ChecksumException -> 0x0230 }
            com.google.zxing.e.a.a.c r12 = r12.d     // Catch:{ ChecksumException -> 0x0230 }
            r16 = r5
            r17 = r10
            goto L_0x0155
        L_0x0129:
            int[] r14 = r2.f3061b     // Catch:{ ChecksumException -> 0x0230 }
            int r14 = r14.length     // Catch:{ ChecksumException -> 0x0230 }
            int r12 = r12 + r14
            int[] r12 = new int[r12]     // Catch:{ ChecksumException -> 0x0230 }
            r15 = 0
        L_0x0130:
            if (r15 >= r14) goto L_0x0149
            r16 = r5
            com.google.zxing.e.a.a.b r5 = r2.f3060a     // Catch:{ ChecksumException -> 0x022e }
            r17 = r10
            int[] r10 = r2.f3061b     // Catch:{ ChecksumException -> 0x022e }
            r10 = r10[r15]     // Catch:{ ChecksumException -> 0x022e }
            int r5 = r5.d(r10, r13)     // Catch:{ ChecksumException -> 0x022e }
            r12[r15] = r5     // Catch:{ ChecksumException -> 0x022e }
            int r15 = r15 + 1
            r5 = r16
            r10 = r17
            goto L_0x0130
        L_0x0149:
            r16 = r5
            r17 = r10
            com.google.zxing.e.a.a.c r5 = new com.google.zxing.e.a.a.c     // Catch:{ ChecksumException -> 0x022e }
            com.google.zxing.e.a.a.b r10 = r2.f3060a     // Catch:{ ChecksumException -> 0x022e }
            r5.<init>(r10, r12)     // Catch:{ ChecksumException -> 0x022e }
            r12 = r5
        L_0x0155:
            com.google.zxing.e.a.a.c r4 = r4.b(r12)     // Catch:{ ChecksumException -> 0x022e }
            r5 = r16
            r10 = r17
            goto L_0x00ea
        L_0x015e:
            r16 = r5
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException     // Catch:{ ChecksumException -> 0x022e }
            r2.<init>()     // Catch:{ ChecksumException -> 0x022e }
            throw r2     // Catch:{ ChecksumException -> 0x022e }
        L_0x0166:
            r16 = r5
            com.google.zxing.e.a.a.c r5 = r10.c(r6)     // Catch:{ ChecksumException -> 0x022e }
            com.google.zxing.e.a.a.c r5 = r5.b(r9)     // Catch:{ ChecksumException -> 0x022e }
            com.google.zxing.e.a.a.c r5 = r5.b()     // Catch:{ ChecksumException -> 0x022e }
            r9 = r6
            r6 = r5
            r5 = r16
            r18 = r4
            r4 = r2
            r2 = r18
            goto L_0x00c8
        L_0x017f:
            r16 = r5
            com.google.zxing.ChecksumException r2 = com.google.zxing.ChecksumException.a()     // Catch:{ ChecksumException -> 0x022e }
            throw r2     // Catch:{ ChecksumException -> 0x022e }
        L_0x0186:
            r16 = r5
            r4 = 0
            int r5 = r6.a(r4)     // Catch:{ ChecksumException -> 0x022e }
            if (r5 == 0) goto L_0x021b
            com.google.zxing.e.a.a.b r4 = r8.f3057a     // Catch:{ ChecksumException -> 0x022e }
            int r4 = r4.a(r5)     // Catch:{ ChecksumException -> 0x022e }
            com.google.zxing.e.a.a.c r5 = r6.c(r4)     // Catch:{ ChecksumException -> 0x022e }
            com.google.zxing.e.a.a.c r2 = r2.c(r4)     // Catch:{ ChecksumException -> 0x022e }
            r4 = 2
            com.google.zxing.e.a.a.c[] r4 = new com.google.zxing.e.a.a.c[r4]     // Catch:{ ChecksumException -> 0x022e }
            r6 = 0
            r4[r6] = r5     // Catch:{ ChecksumException -> 0x022e }
            r5 = 1
            r4[r5] = r2     // Catch:{ ChecksumException -> 0x022e }
            r2 = r4[r6]     // Catch:{ ChecksumException -> 0x022e }
            r4 = r4[r5]     // Catch:{ ChecksumException -> 0x022e }
            int[] r5 = r8.a(r2)     // Catch:{ ChecksumException -> 0x022e }
            int[] r2 = r8.a(r4, r2, r5)     // Catch:{ ChecksumException -> 0x022e }
            r4 = 0
        L_0x01b3:
            int r6 = r5.length     // Catch:{ ChecksumException -> 0x022e }
            if (r4 >= r6) goto L_0x01e0
            int r6 = r0.length     // Catch:{ ChecksumException -> 0x022e }
            r9 = 1
            int r6 = r6 - r9
            com.google.zxing.e.a.a.b r9 = r8.f3057a     // Catch:{ ChecksumException -> 0x022e }
            r10 = r5[r4]     // Catch:{ ChecksumException -> 0x022e }
            if (r10 == 0) goto L_0x01da
            int[] r9 = r9.c     // Catch:{ ChecksumException -> 0x022e }
            r9 = r9[r10]     // Catch:{ ChecksumException -> 0x022e }
            int r6 = r6 - r9
            if (r6 < 0) goto L_0x01d5
            com.google.zxing.e.a.a.b r9 = r8.f3057a     // Catch:{ ChecksumException -> 0x022e }
            r10 = r0[r6]     // Catch:{ ChecksumException -> 0x022e }
            r11 = r2[r4]     // Catch:{ ChecksumException -> 0x022e }
            int r9 = r9.c(r10, r11)     // Catch:{ ChecksumException -> 0x022e }
            r0[r6] = r9     // Catch:{ ChecksumException -> 0x022e }
            int r4 = r4 + 1
            goto L_0x01b3
        L_0x01d5:
            com.google.zxing.ChecksumException r2 = com.google.zxing.ChecksumException.a()     // Catch:{ ChecksumException -> 0x022e }
            throw r2     // Catch:{ ChecksumException -> 0x022e }
        L_0x01da:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException     // Catch:{ ChecksumException -> 0x022e }
            r2.<init>()     // Catch:{ ChecksumException -> 0x022e }
            throw r2     // Catch:{ ChecksumException -> 0x022e }
        L_0x01e0:
            int r4 = r5.length     // Catch:{ ChecksumException -> 0x022e }
        L_0x01e1:
            int r2 = r0.length     // Catch:{ ChecksumException -> 0x022e }
            r5 = 4
            if (r2 < r5) goto L_0x0216
            r2 = 0
            r5 = r0[r2]     // Catch:{ ChecksumException -> 0x022e }
            int r2 = r0.length     // Catch:{ ChecksumException -> 0x022e }
            if (r5 > r2) goto L_0x0211
            if (r5 != 0) goto L_0x01fb
            int r2 = r0.length     // Catch:{ ChecksumException -> 0x022e }
            if (r7 >= r2) goto L_0x01f6
            int r2 = r0.length     // Catch:{ ChecksumException -> 0x022e }
            int r2 = r2 - r7
            r5 = 0
            r0[r5] = r2     // Catch:{ ChecksumException -> 0x022e }
            goto L_0x01fb
        L_0x01f6:
            com.google.zxing.FormatException r2 = com.google.zxing.FormatException.a()     // Catch:{ ChecksumException -> 0x022e }
            throw r2     // Catch:{ ChecksumException -> 0x022e }
        L_0x01fb:
            java.lang.String r2 = java.lang.String.valueOf(r20)     // Catch:{ ChecksumException -> 0x022e }
            com.google.zxing.common.e r2 = com.google.zxing.e.a.e.a(r0, r2)     // Catch:{ ChecksumException -> 0x022e }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ ChecksumException -> 0x022e }
            r2.f = r4     // Catch:{ ChecksumException -> 0x022e }
            int r4 = r1.length     // Catch:{ ChecksumException -> 0x022e }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ ChecksumException -> 0x022e }
            r2.g = r4     // Catch:{ ChecksumException -> 0x022e }
            return r2
        L_0x0211:
            com.google.zxing.FormatException r2 = com.google.zxing.FormatException.a()     // Catch:{ ChecksumException -> 0x022e }
            throw r2     // Catch:{ ChecksumException -> 0x022e }
        L_0x0216:
            com.google.zxing.FormatException r2 = com.google.zxing.FormatException.a()     // Catch:{ ChecksumException -> 0x022e }
            throw r2     // Catch:{ ChecksumException -> 0x022e }
        L_0x021b:
            com.google.zxing.ChecksumException r2 = com.google.zxing.ChecksumException.a()     // Catch:{ ChecksumException -> 0x022e }
            throw r2     // Catch:{ ChecksumException -> 0x022e }
        L_0x0220:
            r16 = r5
            com.google.zxing.ChecksumException r2 = com.google.zxing.ChecksumException.a()     // Catch:{ ChecksumException -> 0x022e }
            throw r2     // Catch:{ ChecksumException -> 0x022e }
        L_0x0227:
            r16 = r5
            com.google.zxing.FormatException r2 = com.google.zxing.FormatException.a()     // Catch:{ ChecksumException -> 0x022e }
            throw r2     // Catch:{ ChecksumException -> 0x022e }
        L_0x022e:
            goto L_0x0232
        L_0x0230:
            r16 = r5
        L_0x0232:
            int r2 = r3.length
            if (r2 == 0) goto L_0x025d
            r2 = 0
        L_0x0236:
            int r4 = r3.length
            if (r2 >= r4) goto L_0x0257
            r4 = r3[r2]
            r5 = r24[r2]
            int r5 = r5.length
            r6 = 1
            int r5 = r5 - r6
            if (r4 >= r5) goto L_0x0248
            r4 = r3[r2]
            int r4 = r4 + r6
            r3[r2] = r4
            goto L_0x0257
        L_0x0248:
            r4 = 0
            r3[r2] = r4
            int r5 = r3.length
            int r5 = r5 - r6
            if (r2 == r5) goto L_0x0252
            int r2 = r2 + 1
            goto L_0x0236
        L_0x0252:
            com.google.zxing.ChecksumException r0 = com.google.zxing.ChecksumException.a()
            throw r0
        L_0x0257:
            r2 = r23
            r4 = r16
            goto L_0x000b
        L_0x025d:
            com.google.zxing.ChecksumException r0 = com.google.zxing.ChecksumException.a()
            throw r0
        L_0x0262:
            com.google.zxing.ChecksumException r0 = com.google.zxing.ChecksumException.a()
            goto L_0x0268
        L_0x0267:
            throw r0
        L_0x0268:
            goto L_0x0267
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.e.a.k.a(int, int[], int[], int[], int[][]):com.google.zxing.common.e");
    }

    private static d a(b bVar, int i, int i2, boolean z, int i3, int i4, int i5, int i6) {
        int i7;
        int a2;
        int a3;
        int b2 = b(bVar, i, i2, z, i3, i4);
        int[] a4 = a(bVar, i, i2, z, b2, i4);
        if (a4 == null) {
            return null;
        }
        int a5 = com.google.zxing.common.a.a.a(a4);
        if (z) {
            int i8 = b2;
            b2 += a5;
            i7 = i8;
        } else {
            for (int i9 = 0; i9 < a4.length / 2; i9++) {
                int i10 = a4[i9];
                a4[i9] = a4[(a4.length - 1) - i9];
                a4[(a4.length - 1) - i9] = i10;
            }
            i7 = b2 - a5;
        }
        if (a(a5, i5, i6) && (a3 = com.google.zxing.e.a.a((a2 = j.a(a4)))) != -1) {
            return new d(i7, b2, b(a2), a3);
        }
        return null;
    }

    private static int[] a(b bVar, int i, int i2, boolean z, int i3, int i4) {
        int[] iArr = new int[8];
        int i5 = z ? 1 : -1;
        int i6 = 0;
        boolean z2 = z;
        while (true) {
            if (!z) {
                if (i3 < i) {
                    break;
                }
            } else if (i3 >= i2) {
                break;
            }
            if (i6 >= 8) {
                break;
            } else if (bVar.a(i3, i4) == z2) {
                iArr[i6] = iArr[i6] + 1;
                i3 += i5;
            } else {
                i6++;
                z2 = !z2;
            }
        }
        if (i6 != 8) {
            if (z) {
                i = i2;
            }
            if (!(i3 == i && i6 == 7)) {
                return null;
            }
        }
        return iArr;
    }

    private static int b(b bVar, int i, int i2, boolean z, int i3, int i4) {
        int i5 = z ? -1 : 1;
        boolean z2 = z;
        int i6 = i3;
        for (int i7 = 0; i7 < 2; i7++) {
            while (true) {
                if (!z2) {
                    if (i6 >= i2) {
                        continue;
                        break;
                    }
                } else if (i6 < i) {
                    continue;
                    break;
                }
                if (z2 != bVar.a(i6, i4)) {
                    continue;
                    break;
                } else if (Math.abs(i3 - i6) > 2) {
                    return i3;
                } else {
                    i6 += i5;
                }
            }
            i5 = -i5;
            z2 = !z2;
        }
        return i6;
    }

    private static int[] a(int i) {
        int[] iArr = new int[8];
        int i2 = 0;
        int i3 = 7;
        while (true) {
            int i4 = i & 1;
            if (i4 != i2) {
                i3--;
                if (i3 < 0) {
                    return iArr;
                }
                i2 = i4;
            }
            iArr[i3] = iArr[i3] + 1;
            i >>= 1;
        }
    }

    private static int b(int i) {
        return b(a(i));
    }

    private static int b(int[] iArr) {
        return ((((iArr[0] - iArr[2]) + iArr[4]) - iArr[6]) + 9) % 9;
    }

    private static boolean a(g gVar, int i) {
        return i >= 0 && i <= gVar.c + 1;
    }
}
