package com.google.zxing.e.b;

import com.google.zxing.p;
import java.util.List;

/* compiled from: PDF417DetectorResult */
public final class b {

    /* renamed from: a  reason: collision with root package name */
    public final com.google.zxing.common.b f3080a;

    /* renamed from: b  reason: collision with root package name */
    public final List<p[]> f3081b;

    public b(com.google.zxing.common.b bVar, List<p[]> list) {
        this.f3080a = bVar;
        this.f3081b = list;
    }
}
