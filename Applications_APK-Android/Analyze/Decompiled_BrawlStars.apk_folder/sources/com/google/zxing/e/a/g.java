package com.google.zxing.e.a;

/* compiled from: DetectionResult */
final class g {

    /* renamed from: a  reason: collision with root package name */
    final h[] f3072a = new h[(this.c + 2)];

    /* renamed from: b  reason: collision with root package name */
    c f3073b;
    final int c;
    private final a d;

    g(a aVar, c cVar) {
        this.d = aVar;
        this.c = aVar.f3055a;
        this.f3073b = cVar;
    }

    /* access modifiers changed from: package-private */
    public void a(h hVar) {
        if (hVar != null) {
            ((i) hVar).a(this.d);
        }
    }

    static int a(int i, int i2, d dVar) {
        if (dVar == null || dVar.a()) {
            return i2;
        }
        if (!dVar.a(i)) {
            return i2 + 1;
        }
        dVar.e = i;
        return 0;
    }

    /* access modifiers changed from: package-private */
    public final int a() {
        return this.d.e;
    }

    /* access modifiers changed from: package-private */
    public final int b() {
        return this.d.f3056b;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0078, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x007d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x007e, code lost:
        r1.addSuppressed(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0082, code lost:
        throw r2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String toString() {
        /*
            r10 = this;
            com.google.zxing.e.a.h[] r0 = r10.f3072a
            r1 = 0
            r2 = r0[r1]
            r3 = 1
            if (r2 != 0) goto L_0x000d
            int r2 = r10.c
            int r2 = r2 + r3
            r2 = r0[r2]
        L_0x000d:
            java.util.Formatter r0 = new java.util.Formatter
            r0.<init>()
            r4 = 0
        L_0x0013:
            com.google.zxing.e.a.d[] r5 = r2.f3075b     // Catch:{ all -> 0x0076 }
            int r5 = r5.length     // Catch:{ all -> 0x0076 }
            if (r4 >= r5) goto L_0x006e
            java.lang.String r5 = "CW %3d:"
            java.lang.Object[] r6 = new java.lang.Object[r3]     // Catch:{ all -> 0x0076 }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x0076 }
            r6[r1] = r7     // Catch:{ all -> 0x0076 }
            r0.format(r5, r6)     // Catch:{ all -> 0x0076 }
            r5 = 0
        L_0x0026:
            int r6 = r10.c     // Catch:{ all -> 0x0076 }
            r7 = 2
            int r6 = r6 + r7
            if (r5 >= r6) goto L_0x0064
            com.google.zxing.e.a.h[] r6 = r10.f3072a     // Catch:{ all -> 0x0076 }
            r6 = r6[r5]     // Catch:{ all -> 0x0076 }
            java.lang.String r8 = "    |   "
            if (r6 != 0) goto L_0x003a
            java.lang.Object[] r6 = new java.lang.Object[r1]     // Catch:{ all -> 0x0076 }
            r0.format(r8, r6)     // Catch:{ all -> 0x0076 }
            goto L_0x0061
        L_0x003a:
            com.google.zxing.e.a.h[] r6 = r10.f3072a     // Catch:{ all -> 0x0076 }
            r6 = r6[r5]     // Catch:{ all -> 0x0076 }
            com.google.zxing.e.a.d[] r6 = r6.f3075b     // Catch:{ all -> 0x0076 }
            r6 = r6[r4]     // Catch:{ all -> 0x0076 }
            if (r6 != 0) goto L_0x004a
            java.lang.Object[] r6 = new java.lang.Object[r1]     // Catch:{ all -> 0x0076 }
            r0.format(r8, r6)     // Catch:{ all -> 0x0076 }
            goto L_0x0061
        L_0x004a:
            java.lang.String r8 = " %3d|%3d"
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ all -> 0x0076 }
            int r9 = r6.e     // Catch:{ all -> 0x0076 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ all -> 0x0076 }
            r7[r1] = r9     // Catch:{ all -> 0x0076 }
            int r6 = r6.d     // Catch:{ all -> 0x0076 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x0076 }
            r7[r3] = r6     // Catch:{ all -> 0x0076 }
            r0.format(r8, r7)     // Catch:{ all -> 0x0076 }
        L_0x0061:
            int r5 = r5 + 1
            goto L_0x0026
        L_0x0064:
            java.lang.String r5 = "%n"
            java.lang.Object[] r6 = new java.lang.Object[r1]     // Catch:{ all -> 0x0076 }
            r0.format(r5, r6)     // Catch:{ all -> 0x0076 }
            int r4 = r4 + 1
            goto L_0x0013
        L_0x006e:
            java.lang.String r1 = r0.toString()     // Catch:{ all -> 0x0076 }
            r0.close()
            return r1
        L_0x0076:
            r1 = move-exception
            throw r1     // Catch:{ all -> 0x0078 }
        L_0x0078:
            r2 = move-exception
            r0.close()     // Catch:{ all -> 0x007d }
            goto L_0x0081
        L_0x007d:
            r0 = move-exception
            r1.addSuppressed(r0)
        L_0x0081:
            goto L_0x0083
        L_0x0082:
            throw r2
        L_0x0083:
            goto L_0x0082
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.e.a.g.toString():java.lang.String");
    }
}
