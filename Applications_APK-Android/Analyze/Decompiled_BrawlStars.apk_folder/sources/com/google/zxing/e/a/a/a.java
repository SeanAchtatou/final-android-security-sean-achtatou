package com.google.zxing.e.a.a;

import com.google.zxing.ChecksumException;

/* compiled from: ErrorCorrection */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    public final b f3057a = b.f3058a;

    public int[] a(c cVar) throws ChecksumException {
        int length = cVar.f3061b.length - 1;
        int[] iArr = new int[length];
        int i = 0;
        for (int i2 = 1; i2 < this.f3057a.f && i < length; i2++) {
            if (cVar.b(i2) == 0) {
                iArr[i] = this.f3057a.a(i2);
                i++;
            }
        }
        if (i == length) {
            return iArr;
        }
        throw ChecksumException.a();
    }

    public int[] a(c cVar, c cVar2, int[] iArr) {
        int length = cVar2.f3061b.length - 1;
        int[] iArr2 = new int[length];
        for (int i = 1; i <= length; i++) {
            iArr2[length - i] = this.f3057a.d(i, cVar2.a(i));
        }
        c cVar3 = new c(this.f3057a, iArr2);
        int length2 = iArr.length;
        int[] iArr3 = new int[length2];
        for (int i2 = 0; i2 < length2; i2++) {
            int a2 = this.f3057a.a(iArr[i2]);
            iArr3[i2] = this.f3057a.d(this.f3057a.c(0, cVar.b(a2)), this.f3057a.a(cVar3.b(a2)));
        }
        return iArr3;
    }
}
