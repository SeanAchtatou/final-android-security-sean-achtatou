package com.google.zxing.e.c;

import java.lang.reflect.Array;

/* compiled from: BarcodeMatrix */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    int f3084a;

    /* renamed from: b  reason: collision with root package name */
    private final b[] f3085b;
    private final int c;
    private final int d;

    a(int i, int i2) {
        this.f3085b = new b[i];
        int length = this.f3085b.length;
        for (int i3 = 0; i3 < length; i3++) {
            this.f3085b[i3] = new b(((i2 + 4) * 17) + 1);
        }
        this.d = i2 * 17;
        this.c = i;
        this.f3084a = -1;
    }

    /* access modifiers changed from: package-private */
    public final b a() {
        return this.f3085b[this.f3084a];
    }

    public final byte[][] a(int i, int i2) {
        byte[][] bArr = (byte[][]) Array.newInstance(byte.class, this.c * i2, this.d * i);
        int i3 = this.c * i2;
        for (int i4 = 0; i4 < i3; i4++) {
            bArr[(i3 - i4) - 1] = this.f3085b[i4 / i2].a(i);
        }
        return bArr;
    }
}
