package com.google.zxing.e.a;

import com.google.zxing.p;

/* compiled from: DetectionResultRowIndicatorColumn */
final class i extends h {
    final boolean c;

    i(c cVar, boolean z) {
        super(cVar);
        this.c = z;
    }

    /* access modifiers changed from: package-private */
    public final int[] a() {
        p pVar;
        p pVar2;
        int i;
        a b2 = b();
        if (b2 == null) {
            return null;
        }
        c cVar = this.f3074a;
        if (this.c) {
            pVar = cVar.f3064b;
        } else {
            pVar = cVar.d;
        }
        if (this.c) {
            pVar2 = cVar.c;
        } else {
            pVar2 = cVar.e;
        }
        int b3 = b((int) pVar2.f3154b);
        d[] dVarArr = this.f3075b;
        int i2 = -1;
        int i3 = 0;
        int i4 = 1;
        for (int b4 = b((int) pVar.f3154b); b4 < b3; b4++) {
            if (dVarArr[b4] != null) {
                d dVar = dVarArr[b4];
                dVar.b();
                int i5 = dVar.e - i2;
                if (i5 == 0) {
                    i3++;
                } else {
                    if (i5 == 1) {
                        i4 = Math.max(i4, i3);
                        i2 = dVar.e;
                    } else if (dVar.e >= b2.e) {
                        dVarArr[b4] = null;
                    } else {
                        i2 = dVar.e;
                    }
                    i3 = 1;
                }
            }
        }
        int[] iArr = new int[b2.e];
        for (d dVar2 : this.f3075b) {
            if (dVar2 != null && (i = dVar2.e) < iArr.length) {
                iArr[i] = iArr[i] + 1;
            }
        }
        return iArr;
    }

    private void a(d[] dVarArr, a aVar) {
        for (int i = 0; i < dVarArr.length; i++) {
            d dVar = dVarArr[i];
            if (dVarArr[i] != null) {
                int i2 = dVar.d % 30;
                int i3 = dVar.e;
                if (i3 > aVar.e) {
                    dVarArr[i] = null;
                } else {
                    if (!this.c) {
                        i3 += 2;
                    }
                    int i4 = i3 % 3;
                    if (i4 != 0) {
                        if (i4 != 1) {
                            if (i4 == 2 && i2 + 1 != aVar.f3055a) {
                                dVarArr[i] = null;
                            }
                        } else if (i2 / 3 != aVar.f3056b || i2 % 3 != aVar.d) {
                            dVarArr[i] = null;
                        }
                    } else if ((i2 * 3) + 1 != aVar.c) {
                        dVarArr[i] = null;
                    }
                }
            }
        }
    }

    public final String toString() {
        return "IsLeft: " + this.c + 10 + super.toString();
    }

    private void c() {
        for (d dVar : this.f3075b) {
            if (dVar != null) {
                dVar.b();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(a aVar) {
        p pVar;
        p pVar2;
        d[] dVarArr = this.f3075b;
        c();
        a(dVarArr, aVar);
        c cVar = this.f3074a;
        if (this.c) {
            pVar = cVar.f3064b;
        } else {
            pVar = cVar.d;
        }
        if (this.c) {
            pVar2 = cVar.c;
        } else {
            pVar2 = cVar.e;
        }
        int b2 = b((int) pVar.f3154b);
        int b3 = b((int) pVar2.f3154b);
        int i = -1;
        int i2 = 0;
        int i3 = 1;
        while (b2 < b3) {
            if (dVarArr[b2] != null) {
                d dVar = dVarArr[b2];
                int i4 = dVar.e - i;
                if (i4 == 0) {
                    i2++;
                } else {
                    if (i4 == 1) {
                        i3 = Math.max(i3, i2);
                        i = dVar.e;
                    } else if (i4 < 0 || dVar.e >= aVar.e || i4 > b2) {
                        dVarArr[b2] = null;
                    } else {
                        if (i3 > 2) {
                            i4 *= i3 - 2;
                        }
                        boolean z = i4 >= b2;
                        for (int i5 = 1; i5 <= i4 && !z; i5++) {
                            z = dVarArr[b2 - i5] != null;
                        }
                        if (z) {
                            dVarArr[b2] = null;
                        } else {
                            i = dVar.e;
                        }
                    }
                    i2 = 1;
                }
            }
            b2++;
        }
    }

    /* access modifiers changed from: package-private */
    public final a b() {
        d[] dVarArr = this.f3075b;
        b bVar = new b();
        b bVar2 = new b();
        b bVar3 = new b();
        b bVar4 = new b();
        for (d dVar : dVarArr) {
            if (dVar != null) {
                dVar.b();
                int i = dVar.d % 30;
                int i2 = dVar.e;
                if (!this.c) {
                    i2 += 2;
                }
                int i3 = i2 % 3;
                if (i3 == 0) {
                    bVar2.a((i * 3) + 1);
                } else if (i3 == 1) {
                    bVar4.a(i / 3);
                    bVar3.a(i % 3);
                } else if (i3 == 2) {
                    bVar.a(i + 1);
                }
            }
        }
        if (bVar.a().length == 0 || bVar2.a().length == 0 || bVar3.a().length == 0 || bVar4.a().length == 0 || bVar.a()[0] <= 0 || bVar2.a()[0] + bVar3.a()[0] < 3 || bVar2.a()[0] + bVar3.a()[0] > 90) {
            return null;
        }
        a aVar = new a(bVar.a()[0], bVar2.a()[0], bVar3.a()[0], bVar4.a()[0]);
        a(dVarArr, aVar);
        return aVar;
    }
}
