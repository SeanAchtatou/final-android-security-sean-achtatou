package com.google.zxing.c;

import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.c.a.c;
import com.google.zxing.common.b;
import com.google.zxing.common.e;
import com.google.zxing.d;
import com.google.zxing.m;
import com.google.zxing.n;
import com.google.zxing.o;
import com.google.zxing.p;
import java.util.Map;

/* compiled from: MaxiCodeReader */
public final class a implements m {

    /* renamed from: a  reason: collision with root package name */
    private static final p[] f2939a = new p[0];

    /* renamed from: b  reason: collision with root package name */
    private final c f2940b = new c();

    public final void a() {
    }

    public final n a(com.google.zxing.c cVar) throws NotFoundException, ChecksumException, FormatException {
        return a(cVar, null);
    }

    public final n a(com.google.zxing.c cVar, Map<d, ?> map) throws NotFoundException, ChecksumException, FormatException {
        byte[] bArr;
        Map<d, ?> map2 = map;
        if (map2 == null || !map2.containsKey(d.PURE_BARCODE)) {
            throw NotFoundException.a();
        }
        b b2 = cVar.b();
        int i = b2.f2968a;
        int i2 = -1;
        int i3 = b2.f2969b;
        int i4 = -1;
        int i5 = i;
        int i6 = 0;
        while (i6 < b2.f2969b) {
            int i7 = i2;
            int i8 = i5;
            for (int i9 = 0; i9 < b2.c; i9++) {
                int i10 = b2.d[(b2.c * i6) + i9];
                if (i10 != 0) {
                    if (i6 < i3) {
                        i3 = i6;
                    }
                    if (i6 > i4) {
                        i4 = i6;
                    }
                    int i11 = i9 << 5;
                    int i12 = 31;
                    if (i11 < i8) {
                        int i13 = 0;
                        while ((i10 << (31 - i13)) == 0) {
                            i13++;
                        }
                        int i14 = i13 + i11;
                        if (i14 < i8) {
                            i8 = i14;
                        }
                    }
                    if (i11 + 31 > i7) {
                        while ((i10 >>> i12) == 0) {
                            i12--;
                        }
                        int i15 = i11 + i12;
                        if (i15 > i7) {
                            i7 = i15;
                        }
                    }
                }
            }
            i6++;
            i5 = i8;
            i2 = i7;
        }
        int[] iArr = (i2 < i5 || i4 < i3) ? null : new int[]{i5, i3, (i2 - i5) + 1, (i4 - i3) + 1};
        if (iArr != null) {
            int i16 = iArr[0];
            int i17 = iArr[1];
            int i18 = iArr[2];
            int i19 = iArr[3];
            b bVar = new b(30, 33);
            int i20 = 0;
            for (int i21 = 33; i20 < i21; i21 = 33) {
                int i22 = (((i20 * i19) + (i19 / 2)) / i21) + i17;
                for (int i23 = 0; i23 < 30; i23++) {
                    if (b2.a(i16 + ((((i23 * i18) + (i18 / 2)) + (((i20 & 1) * i18) / 2)) / 30), i22)) {
                        bVar.b(i23, i20);
                    }
                }
                i20++;
            }
            c cVar2 = this.f2940b;
            com.google.zxing.c.a.a aVar = new com.google.zxing.c.a.a(bVar);
            byte[] bArr2 = new byte[144];
            int i24 = aVar.f2942b.f2969b;
            int i25 = aVar.f2942b.f2968a;
            for (int i26 = 0; i26 < i24; i26++) {
                int[] iArr2 = com.google.zxing.c.a.a.f2941a[i26];
                for (int i27 = 0; i27 < i25; i27++) {
                    int i28 = iArr2[i27];
                    if (i28 >= 0 && aVar.f2942b.a(i27, i26)) {
                        int i29 = i28 / 6;
                        bArr2[i29] = (byte) (bArr2[i29] | ((byte) (1 << (5 - (i28 % 6)))));
                    }
                }
            }
            cVar2.a(bArr2, 0, 10, 10, 0);
            byte b3 = bArr2[0] & 15;
            if (b3 == 2 || b3 == 3 || b3 == 4) {
                c cVar3 = cVar2;
                byte[] bArr3 = bArr2;
                cVar3.a(bArr3, 20, 84, 40, 1);
                cVar3.a(bArr3, 20, 84, 40, 2);
                bArr = new byte[94];
            } else if (b3 == 5) {
                c cVar4 = cVar2;
                byte[] bArr4 = bArr2;
                cVar4.a(bArr4, 20, 68, 56, 1);
                cVar4.a(bArr4, 20, 68, 56, 2);
                bArr = new byte[78];
            } else {
                throw FormatException.a();
            }
            System.arraycopy(bArr2, 0, bArr, 0, 10);
            System.arraycopy(bArr2, 20, bArr, 10, bArr.length - 10);
            e a2 = com.google.zxing.c.a.b.a(bArr, b3);
            n nVar = new n(a2.c, a2.f2974a, f2939a, com.google.zxing.a.MAXICODE);
            String str = a2.e;
            if (str != null) {
                nVar.a(o.ERROR_CORRECTION_LEVEL, str);
            }
            return nVar;
        }
        throw NotFoundException.a();
    }
}
