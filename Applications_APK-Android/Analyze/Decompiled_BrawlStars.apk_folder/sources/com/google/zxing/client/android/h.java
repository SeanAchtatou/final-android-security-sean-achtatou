package com.google.zxing.client.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;

/* compiled from: InactivityTimer */
public final class h {
    private static final String d = h.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    public final Context f2959a;

    /* renamed from: b  reason: collision with root package name */
    public final BroadcastReceiver f2960b;
    public boolean c = false;
    /* access modifiers changed from: private */
    public Handler e;
    private Runnable f;
    private boolean g;

    public h(Context context, Runnable runnable) {
        this.f2959a = context;
        this.f = runnable;
        this.f2960b = new a(this, (byte) 0);
        this.e = new Handler();
    }

    public final void a() {
        c();
        if (this.g) {
            this.e.postDelayed(this.f, 300000);
        }
    }

    public final void b() {
        c();
        if (this.c) {
            this.f2959a.unregisterReceiver(this.f2960b);
            this.c = false;
        }
    }

    private void c() {
        this.e.removeCallbacksAndMessages(null);
    }

    /* compiled from: InactivityTimer */
    final class a extends BroadcastReceiver {
        private a() {
        }

        /* synthetic */ a(h hVar, byte b2) {
            this();
        }

        public final void onReceive(Context context, Intent intent) {
            if ("android.intent.action.BATTERY_CHANGED".equals(intent.getAction())) {
                h.this.e.post(new i(this, intent.getIntExtra("plugged", -1) <= 0));
            }
        }
    }

    static /* synthetic */ void a(h hVar, boolean z) {
        hVar.g = z;
        if (hVar.c) {
            hVar.a();
        }
    }
}
