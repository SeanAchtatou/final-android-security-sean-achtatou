package com.google.zxing.client.android;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Vibrator;
import java.io.IOException;

/* compiled from: BeepManager */
public final class c {
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static final String f2951b = c.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    public boolean f2952a = true;
    private final Context c;
    private boolean d = false;

    public c(Activity activity) {
        activity.setVolumeControlStream(3);
        this.c = activity.getApplicationContext();
    }

    public final synchronized void a() {
        Vibrator vibrator;
        if (this.f2952a) {
            c();
        }
        if (this.d && (vibrator = (Vibrator) this.c.getSystemService("vibrator")) != null) {
            vibrator.vibrate(200);
        }
    }

    private MediaPlayer c() {
        AssetFileDescriptor openRawResourceFd;
        MediaPlayer mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(3);
        mediaPlayer.setOnCompletionListener(new d(this));
        mediaPlayer.setOnErrorListener(new e(this));
        try {
            openRawResourceFd = this.c.getResources().openRawResourceFd(R.raw.zxing_beep);
            mediaPlayer.setDataSource(openRawResourceFd.getFileDescriptor(), openRawResourceFd.getStartOffset(), openRawResourceFd.getLength());
            openRawResourceFd.close();
            mediaPlayer.setVolume(0.1f, 0.1f);
            mediaPlayer.prepare();
            mediaPlayer.start();
            return mediaPlayer;
        } catch (IOException unused) {
            mediaPlayer.release();
            return null;
        } catch (Throwable th) {
            openRawResourceFd.close();
            throw th;
        }
    }
}
