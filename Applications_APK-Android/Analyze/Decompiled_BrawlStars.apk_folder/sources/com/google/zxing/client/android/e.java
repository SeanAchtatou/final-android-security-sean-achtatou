package com.google.zxing.client.android;

import android.media.MediaPlayer;

/* compiled from: BeepManager */
class e implements MediaPlayer.OnErrorListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f2954a;

    e(c cVar) {
        this.f2954a = cVar;
    }

    public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        String unused = c.f2951b;
        "Failed to beep " + i + ", " + i2;
        mediaPlayer.stop();
        mediaPlayer.release();
        return true;
    }
}
