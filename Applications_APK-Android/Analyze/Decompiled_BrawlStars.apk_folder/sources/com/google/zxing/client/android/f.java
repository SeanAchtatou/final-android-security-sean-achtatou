package com.google.zxing.client.android;

import android.content.Intent;
import com.google.zxing.a;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

/* compiled from: DecodeFormatManager */
public final class f {

    /* renamed from: a  reason: collision with root package name */
    static final Set<a> f2955a = EnumSet.of(a.UPC_A, a.UPC_E, a.EAN_13, a.EAN_8, a.RSS_14, a.RSS_EXPANDED);

    /* renamed from: b  reason: collision with root package name */
    static final Set<a> f2956b = EnumSet.of(a.CODE_39, a.CODE_93, a.CODE_128, a.ITF, a.CODABAR);
    static final Set<a> c = EnumSet.of(a.QR_CODE);
    static final Set<a> d = EnumSet.of(a.DATA_MATRIX);
    static final Set<a> e = EnumSet.of(a.AZTEC);
    static final Set<a> f = EnumSet.of(a.PDF_417);
    private static final Pattern g = Pattern.compile(",");
    private static final Set<a> h = EnumSet.copyOf(f2955a);
    private static final Map<String, Set<a>> i = new HashMap();

    static {
        h.addAll(f2956b);
        i.put("ONE_D_MODE", h);
        i.put("PRODUCT_MODE", f2955a);
        i.put("QR_CODE_MODE", c);
        i.put("DATA_MATRIX_MODE", d);
        i.put("AZTEC_MODE", e);
        i.put("PDF417_MODE", f);
    }

    public static Set<a> a(Intent intent) {
        String stringExtra = intent.getStringExtra("SCAN_FORMATS");
        return a(stringExtra != null ? Arrays.asList(g.split(stringExtra)) : null, intent.getStringExtra("SCAN_MODE"));
    }

    private static Set<a> a(Iterable<String> iterable, String str) {
        if (iterable != null) {
            EnumSet noneOf = EnumSet.noneOf(a.class);
            try {
                for (String valueOf : iterable) {
                    noneOf.add(a.valueOf(valueOf));
                }
                return noneOf;
            } catch (IllegalArgumentException unused) {
            }
        }
        if (str != null) {
            return i.get(str);
        }
        return null;
    }
}
