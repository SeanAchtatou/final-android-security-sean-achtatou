package com.google.zxing.client.android;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.os.Handler;
import com.journeyapps.barcodescanner.a.m;
import com.journeyapps.barcodescanner.a.n;

/* compiled from: AmbientLightManager */
public final class a implements SensorEventListener {

    /* renamed from: a  reason: collision with root package name */
    public n f2945a;

    /* renamed from: b  reason: collision with root package name */
    public Sensor f2946b;
    public Context c;
    /* access modifiers changed from: private */
    public m d;
    private Handler e = new Handler();

    public final void onAccuracyChanged(Sensor sensor, int i) {
    }

    public a(Context context, m mVar, n nVar) {
        this.c = context;
        this.d = mVar;
        this.f2945a = nVar;
    }

    private void a(boolean z) {
        this.e.post(new b(this, z));
    }

    public final void onSensorChanged(SensorEvent sensorEvent) {
        float f = sensorEvent.values[0];
        if (this.d == null) {
            return;
        }
        if (f <= 45.0f) {
            a(true);
        } else if (f >= 450.0f) {
            a(false);
        }
    }
}
