package com.google.zxing.client.android;

import android.media.MediaPlayer;

/* compiled from: BeepManager */
class d implements MediaPlayer.OnCompletionListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f2953a;

    d(c cVar) {
        this.f2953a = cVar;
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        mediaPlayer.stop();
        mediaPlayer.release();
    }
}
