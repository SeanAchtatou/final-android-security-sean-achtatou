package com.google.zxing.client.android;

import android.content.Intent;
import android.os.Bundle;
import com.google.zxing.d;
import java.util.EnumMap;
import java.util.Map;
import java.util.regex.Pattern;

/* compiled from: DecodeHintManager */
public final class g {

    /* renamed from: a  reason: collision with root package name */
    private static final String f2957a = g.class.getSimpleName();

    /* renamed from: b  reason: collision with root package name */
    private static final Pattern f2958b = Pattern.compile(",");

    private g() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.google.zxing.d, java.lang.Boolean]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.google.zxing.d, java.lang.Object]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    public static Map<d, Object> a(Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras == null || extras.isEmpty()) {
            return null;
        }
        EnumMap enumMap = new EnumMap(d.class);
        for (d dVar : d.values()) {
            if (!(dVar == d.CHARACTER_SET || dVar == d.NEED_RESULT_POINT_CALLBACK || dVar == d.POSSIBLE_FORMATS)) {
                String name = dVar.name();
                if (extras.containsKey(name)) {
                    if (dVar.l.equals(Void.class)) {
                        enumMap.put((Object) dVar, (Object) Boolean.TRUE);
                    } else {
                        Object obj = extras.get(name);
                        if (dVar.l.isInstance(obj)) {
                            enumMap.put((Object) dVar, obj);
                        } else {
                            "Ignoring hint " + dVar + " because it is not assignable from " + obj;
                        }
                    }
                }
            }
        }
        "Hints from the Intent: " + enumMap;
        return enumMap;
    }
}
