package com.google.zxing.client.android.a;

import android.graphics.Rect;
import android.hardware.Camera;
import com.journeyapps.barcodescanner.a.n;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

/* compiled from: CameraConfigurationUtils */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static final Pattern f2947a = Pattern.compile(";");

    public static void a(Camera.Parameters parameters, n.a aVar, boolean z) {
        String str;
        List<String> supportedFocusModes = parameters.getSupportedFocusModes();
        if (z || aVar == n.a.AUTO) {
            str = a("focus mode", supportedFocusModes, "auto");
        } else if (aVar == n.a.CONTINUOUS) {
            str = a("focus mode", supportedFocusModes, "continuous-picture", "continuous-video", "auto");
        } else if (aVar == n.a.INFINITY) {
            str = a("focus mode", supportedFocusModes, "infinity");
        } else if (aVar == n.a.MACRO) {
            str = a("focus mode", supportedFocusModes, "macro");
        } else {
            str = null;
        }
        if (!z && str == null) {
            str = a("focus mode", supportedFocusModes, "macro", "edof");
        }
        if (str == null) {
            return;
        }
        if (str.equals(parameters.getFocusMode())) {
            "Focus mode already set to " + str;
            return;
        }
        parameters.setFocusMode(str);
    }

    public static void a(Camera.Parameters parameters, boolean z) {
        String str;
        List<String> supportedFlashModes = parameters.getSupportedFlashModes();
        if (z) {
            str = a("flash mode", supportedFlashModes, "torch", "on");
        } else {
            str = a("flash mode", supportedFlashModes, "off");
        }
        if (str == null) {
            return;
        }
        if (str.equals(parameters.getFlashMode())) {
            "Flash mode already set to " + str;
            return;
        }
        "Setting flash mode to " + str;
        parameters.setFlashMode(str);
    }

    public static void b(Camera.Parameters parameters, boolean z) {
        int minExposureCompensation = parameters.getMinExposureCompensation();
        int maxExposureCompensation = parameters.getMaxExposureCompensation();
        float exposureCompensationStep = parameters.getExposureCompensationStep();
        if (minExposureCompensation != 0 || maxExposureCompensation != 0) {
            float f = 0.0f;
            if (exposureCompensationStep > 0.0f) {
                if (!z) {
                    f = 1.5f;
                }
                int round = Math.round(f / exposureCompensationStep);
                float f2 = exposureCompensationStep * ((float) round);
                int max = Math.max(Math.min(round, maxExposureCompensation), minExposureCompensation);
                if (parameters.getExposureCompensation() == max) {
                    "Exposure compensation already set to " + max + " / " + f2;
                    return;
                }
                "Setting exposure compensation to " + max + " / " + f2;
                parameters.setExposureCompensation(max);
            }
        }
    }

    public static void b(Camera.Parameters parameters) {
        if (parameters.getMaxNumFocusAreas() > 0) {
            "Old focus areas: " + a((Iterable<Camera.Area>) parameters.getFocusAreas());
            List<Camera.Area> a2 = a(400);
            "Setting focus area to : " + a((Iterable<Camera.Area>) a2);
            parameters.setFocusAreas(a2);
        }
    }

    public static void c(Camera.Parameters parameters) {
        if (parameters.getMaxNumMeteringAreas() > 0) {
            "Old metering areas: " + parameters.getMeteringAreas();
            List<Camera.Area> a2 = a(400);
            "Setting metering area to : " + a((Iterable<Camera.Area>) a2);
            parameters.setMeteringAreas(a2);
        }
    }

    private static List<Camera.Area> a(int i) {
        return Collections.singletonList(new Camera.Area(new Rect(-400, -400, 400, 400), 1));
    }

    public static void d(Camera.Parameters parameters) {
        if (parameters.isVideoStabilizationSupported() && !parameters.getVideoStabilization()) {
            parameters.setVideoStabilization(true);
        }
    }

    public static void e(Camera.Parameters parameters) {
        String a2;
        if (!"barcode".equals(parameters.getSceneMode()) && (a2 = a("scene mode", parameters.getSupportedSceneModes(), "barcode")) != null) {
            parameters.setSceneMode(a2);
        }
    }

    public static void f(Camera.Parameters parameters) {
        String a2;
        if (!"negative".equals(parameters.getColorEffect()) && (a2 = a("color effect", parameters.getSupportedColorEffects(), "negative")) != null) {
            parameters.setColorEffect(a2);
        }
    }

    private static String a(String str, Collection<String> collection, String... strArr) {
        "Requesting " + str + " value from among: " + Arrays.toString(strArr);
        "Supported " + str + " values: " + collection;
        if (collection == null) {
            return null;
        }
        for (String str2 : strArr) {
            if (collection.contains(str2)) {
                "Can set " + str + " to: " + str2;
                return str2;
            }
        }
        return null;
    }

    private static String a(Collection<int[]> collection) {
        if (collection == null || collection.isEmpty()) {
            return "[]";
        }
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        Iterator<int[]> it = collection.iterator();
        while (it.hasNext()) {
            sb.append(Arrays.toString(it.next()));
            if (it.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(']');
        return sb.toString();
    }

    private static String a(Iterable<Camera.Area> iterable) {
        if (iterable == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (Camera.Area next : iterable) {
            sb.append(next.rect);
            sb.append(':');
            sb.append(next.weight);
            sb.append(' ');
        }
        return sb.toString();
    }

    public static void a(Camera.Parameters parameters) {
        List<int[]> supportedPreviewFpsRange = parameters.getSupportedPreviewFpsRange();
        "Supported FPS ranges: " + a((Collection<int[]>) supportedPreviewFpsRange);
        if (supportedPreviewFpsRange != null && !supportedPreviewFpsRange.isEmpty()) {
            int[] iArr = null;
            Iterator<int[]> it = supportedPreviewFpsRange.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                int[] next = it.next();
                int i = next[0];
                int i2 = next[1];
                if (i >= 10000 && i2 <= 20000) {
                    iArr = next;
                    break;
                }
            }
            if (iArr != null) {
                int[] iArr2 = new int[2];
                parameters.getPreviewFpsRange(iArr2);
                if (Arrays.equals(iArr2, iArr)) {
                    "FPS range already set to " + Arrays.toString(iArr);
                    return;
                }
                "Setting FPS range to " + Arrays.toString(iArr);
                parameters.setPreviewFpsRange(iArr[0], iArr[1]);
            }
        }
    }
}
