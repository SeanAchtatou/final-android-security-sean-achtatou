package com.google.zxing.client.android;

public final class R {

    public static final class attr {
        public static final int zxing_framing_rect_height = 2130903674;
        public static final int zxing_framing_rect_width = 2130903675;
        public static final int zxing_possible_result_points = 2130903676;
        public static final int zxing_preview_scaling_strategy = 2130903677;
        public static final int zxing_result_view = 2130903678;
        public static final int zxing_scanner_layout = 2130903679;
        public static final int zxing_use_texture_view = 2130903680;
        public static final int zxing_viewfinder_laser = 2130903681;
        public static final int zxing_viewfinder_mask = 2130903682;

        private attr() {
        }
    }

    public static final class color {
        public static final int zxing_custom_possible_result_points = 2131034449;
        public static final int zxing_custom_result_view = 2131034450;
        public static final int zxing_custom_viewfinder_laser = 2131034451;
        public static final int zxing_custom_viewfinder_mask = 2131034452;
        public static final int zxing_possible_result_points = 2131034453;
        public static final int zxing_result_view = 2131034454;
        public static final int zxing_status_text = 2131034455;
        public static final int zxing_transparent = 2131034456;
        public static final int zxing_viewfinder_laser = 2131034457;
        public static final int zxing_viewfinder_mask = 2131034458;

        private color() {
        }
    }

    public static final class id {
        public static final int centerCrop = 2131230845;
        public static final int fitCenter = 2131230971;
        public static final int fitXY = 2131230972;
        public static final int zxing_back_button = 2131231499;
        public static final int zxing_barcode_scanner = 2131231500;
        public static final int zxing_barcode_surface = 2131231501;
        public static final int zxing_camera_closed = 2131231502;
        public static final int zxing_camera_error = 2131231503;
        public static final int zxing_decode = 2131231504;
        public static final int zxing_decode_failed = 2131231505;
        public static final int zxing_decode_succeeded = 2131231506;
        public static final int zxing_possible_result_points = 2131231507;
        public static final int zxing_preview_failed = 2131231508;
        public static final int zxing_prewiew_size_ready = 2131231509;
        public static final int zxing_status_view = 2131231510;
        public static final int zxing_viewfinder_view = 2131231511;

        private id() {
        }
    }

    public static final class layout {
        public static final int zxing_barcode_scanner = 2131427599;
        public static final int zxing_capture = 2131427600;

        private layout() {
        }
    }

    public static final class raw {
        public static final int zxing_beep = 2131623936;

        private raw() {
        }
    }

    public static final class string {
        public static final int zxing_app_name = 2131689738;
        public static final int zxing_button_ok = 2131689739;
        public static final int zxing_msg_camera_framework_bug = 2131689740;
        public static final int zxing_msg_default_status = 2131689741;

        private string() {
        }
    }

    public static final class style {
        public static final int zxing_CaptureTheme = 2131755602;

        private style() {
        }
    }

    public static final class styleable {
        public static final int[] zxing_camera_preview = {com.supercell.brawlstars.R.attr.zxing_framing_rect_height, com.supercell.brawlstars.R.attr.zxing_framing_rect_width, com.supercell.brawlstars.R.attr.zxing_preview_scaling_strategy, com.supercell.brawlstars.R.attr.zxing_use_texture_view};
        public static final int zxing_camera_preview_zxing_framing_rect_height = 0;
        public static final int zxing_camera_preview_zxing_framing_rect_width = 1;
        public static final int zxing_camera_preview_zxing_preview_scaling_strategy = 2;
        public static final int zxing_camera_preview_zxing_use_texture_view = 3;
        public static final int[] zxing_finder = {com.supercell.brawlstars.R.attr.zxing_possible_result_points, com.supercell.brawlstars.R.attr.zxing_result_view, com.supercell.brawlstars.R.attr.zxing_viewfinder_laser, com.supercell.brawlstars.R.attr.zxing_viewfinder_mask};
        public static final int zxing_finder_zxing_possible_result_points = 0;
        public static final int zxing_finder_zxing_result_view = 1;
        public static final int zxing_finder_zxing_viewfinder_laser = 2;
        public static final int zxing_finder_zxing_viewfinder_mask = 3;
        public static final int[] zxing_view = {com.supercell.brawlstars.R.attr.zxing_scanner_layout};
        public static final int zxing_view_zxing_scanner_layout = 0;

        private styleable() {
        }
    }
}
