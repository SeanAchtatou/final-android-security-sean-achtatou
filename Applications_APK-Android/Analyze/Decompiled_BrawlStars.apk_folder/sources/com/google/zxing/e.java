package com.google.zxing;

/* compiled from: Dimension */
public final class e {

    /* renamed from: a  reason: collision with root package name */
    public final int f3051a;

    /* renamed from: b  reason: collision with root package name */
    public final int f3052b;

    public final boolean equals(Object obj) {
        if (obj instanceof e) {
            e eVar = (e) obj;
            if (this.f3051a == eVar.f3051a && this.f3052b == eVar.f3052b) {
                return true;
            }
            return false;
        }
        return false;
    }

    public final int hashCode() {
        return (this.f3051a * 32713) + this.f3052b;
    }

    public final String toString() {
        return this.f3051a + "x" + this.f3052b;
    }
}
