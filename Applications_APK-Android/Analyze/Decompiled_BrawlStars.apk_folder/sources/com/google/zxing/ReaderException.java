package com.google.zxing;

public abstract class ReaderException extends Exception {

    /* renamed from: a  reason: collision with root package name */
    protected static final boolean f2879a = (System.getProperty("surefire.test.class.path") != null);

    /* renamed from: b  reason: collision with root package name */
    protected static final StackTraceElement[] f2880b = new StackTraceElement[0];

    ReaderException() {
    }

    ReaderException(Throwable th) {
        super(th);
    }

    public final synchronized Throwable fillInStackTrace() {
        return null;
    }
}
