package com.google.zxing.d;

import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.common.a;
import com.google.zxing.d;
import com.google.zxing.p;
import java.util.Map;

/* compiled from: ITFReader */
public final class n extends r {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f3035a = {6, 8, 10, 12, 14};
    private static final int[] c = {1, 1, 1, 1};
    private static final int[][] d = {new int[]{1, 1, 2}, new int[]{1, 1, 3}};
    private static final int[][] e = {new int[]{1, 1, 2, 2, 1}, new int[]{2, 1, 1, 1, 2}, new int[]{1, 2, 1, 1, 2}, new int[]{2, 2, 1, 1, 1}, new int[]{1, 1, 2, 1, 2}, new int[]{2, 1, 2, 1, 1}, new int[]{1, 2, 2, 1, 1}, new int[]{1, 1, 1, 2, 2}, new int[]{2, 1, 1, 2, 1}, new int[]{1, 2, 1, 2, 1}, new int[]{1, 1, 3, 3, 1}, new int[]{3, 1, 1, 1, 3}, new int[]{1, 3, 1, 1, 3}, new int[]{3, 3, 1, 1, 1}, new int[]{1, 1, 3, 1, 3}, new int[]{3, 1, 3, 1, 1}, new int[]{1, 3, 3, 1, 1}, new int[]{1, 1, 1, 3, 3}, new int[]{3, 1, 1, 3, 1}, new int[]{1, 3, 1, 3, 1}};

    /* renamed from: b  reason: collision with root package name */
    private int f3036b = -1;

    private static void a(a aVar, int i, int i2, StringBuilder sb) throws NotFoundException {
        int[] iArr = new int[10];
        int[] iArr2 = new int[5];
        int[] iArr3 = new int[5];
        while (i < i2) {
            a(aVar, i, iArr);
            for (int i3 = 0; i3 < 5; i3++) {
                int i4 = i3 * 2;
                iArr2[i3] = iArr[i4];
                iArr3[i3] = iArr[i4 + 1];
            }
            sb.append((char) (a(iArr2) + 48));
            sb.append((char) (a(iArr3) + 48));
            for (int i5 = 0; i5 < 10; i5++) {
                i += iArr[i5];
            }
        }
    }

    private void a(a aVar, int i) throws NotFoundException {
        int i2 = this.f3036b * 10;
        if (i2 >= i) {
            i2 = i;
        }
        int i3 = i - 1;
        while (i2 > 0 && i3 >= 0 && !aVar.a(i3)) {
            i2--;
            i3--;
        }
        if (i2 != 0) {
            throw NotFoundException.a();
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:6|7) */
    /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
        r0 = c(r7, r0, com.google.zxing.d.n.d[1]);
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:6:0x0012 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int[] b(com.google.zxing.common.a r7) throws com.google.zxing.NotFoundException {
        /*
            r6 = this;
            r7.c()
            int r0 = a(r7)     // Catch:{ all -> 0x0031 }
            r1 = 1
            r2 = 0
            int[][] r3 = com.google.zxing.d.n.d     // Catch:{ NotFoundException -> 0x0012 }
            r3 = r3[r2]     // Catch:{ NotFoundException -> 0x0012 }
            int[] r0 = c(r7, r0, r3)     // Catch:{ NotFoundException -> 0x0012 }
            goto L_0x001a
        L_0x0012:
            int[][] r3 = com.google.zxing.d.n.d     // Catch:{ all -> 0x0031 }
            r3 = r3[r1]     // Catch:{ all -> 0x0031 }
            int[] r0 = c(r7, r0, r3)     // Catch:{ all -> 0x0031 }
        L_0x001a:
            r3 = r0[r2]     // Catch:{ all -> 0x0031 }
            r6.a(r7, r3)     // Catch:{ all -> 0x0031 }
            r3 = r0[r2]     // Catch:{ all -> 0x0031 }
            int r4 = r7.f2965b     // Catch:{ all -> 0x0031 }
            r5 = r0[r1]     // Catch:{ all -> 0x0031 }
            int r4 = r4 - r5
            r0[r2] = r4     // Catch:{ all -> 0x0031 }
            int r2 = r7.f2965b     // Catch:{ all -> 0x0031 }
            int r2 = r2 - r3
            r0[r1] = r2     // Catch:{ all -> 0x0031 }
            r7.c()
            return r0
        L_0x0031:
            r0 = move-exception
            r7.c()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.d.n.b(com.google.zxing.common.a):int[]");
    }

    private static int[] c(a aVar, int i, int[] iArr) throws NotFoundException {
        int length = iArr.length;
        int[] iArr2 = new int[length];
        int i2 = aVar.f2965b;
        int i3 = i;
        boolean z = false;
        int i4 = 0;
        while (i < i2) {
            if (aVar.a(i) != z) {
                iArr2[i4] = iArr2[i4] + 1;
            } else {
                if (i4 != length - 1) {
                    i4++;
                } else if (a(iArr2, iArr, 0.5f) < 0.38f) {
                    return new int[]{i3, i};
                } else {
                    i3 += iArr2[0] + iArr2[1];
                    int i5 = i4 - 1;
                    System.arraycopy(iArr2, 2, iArr2, 0, i5);
                    iArr2[i5] = 0;
                    iArr2[i4] = 0;
                    i4--;
                }
                iArr2[i4] = 1;
                z = !z;
            }
            i++;
        }
        throw NotFoundException.a();
    }

    private static int a(int[] iArr) throws NotFoundException {
        int length = e.length;
        int i = -1;
        float f = 0.38f;
        for (int i2 = 0; i2 < length; i2++) {
            float a2 = a(iArr, e[i2], 0.5f);
            if (a2 < f) {
                i = i2;
                f = a2;
            } else if (a2 == f) {
                i = -1;
            }
        }
        if (i >= 0) {
            return i % 10;
        }
        throw NotFoundException.a();
    }

    public final com.google.zxing.n a(int i, a aVar, Map<d, ?> map) throws FormatException, NotFoundException {
        boolean z;
        int[] c2 = c(aVar, a(aVar), c);
        this.f3036b = (c2[1] - c2[0]) / 4;
        a(aVar, c2[0]);
        int[] b2 = b(aVar);
        StringBuilder sb = new StringBuilder(20);
        a(aVar, c2[1], b2[0], sb);
        String sb2 = sb.toString();
        int[] iArr = map != null ? (int[]) map.get(d.ALLOWED_LENGTHS) : null;
        if (iArr == null) {
            iArr = f3035a;
        }
        int length = sb2.length();
        int length2 = iArr.length;
        int i2 = 0;
        int i3 = 0;
        while (true) {
            if (i2 >= length2) {
                z = false;
                break;
            }
            int i4 = iArr[i2];
            if (length == i4) {
                z = true;
                break;
            }
            if (i4 > i3) {
                i3 = i4;
            }
            i2++;
        }
        if (!z && length > i3) {
            z = true;
        }
        if (z) {
            float f = (float) i;
            return new com.google.zxing.n(sb2, null, new p[]{new p((float) c2[1], f), new p((float) b2[0], f)}, com.google.zxing.a.ITF);
        }
        throw FormatException.a();
    }

    private static int a(a aVar) throws NotFoundException {
        int i = aVar.f2965b;
        int c2 = aVar.c(0);
        if (c2 != i) {
            return c2;
        }
        throw NotFoundException.a();
    }
}
