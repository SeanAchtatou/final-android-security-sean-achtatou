package com.google.zxing.d.a.a.a;

import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.common.a;

/* compiled from: AbstractExpandedDecoder */
public abstract class j {

    /* renamed from: a  reason: collision with root package name */
    final a f2996a;

    /* renamed from: b  reason: collision with root package name */
    final s f2997b;

    public abstract String a() throws NotFoundException, FormatException;

    j(a aVar) {
        this.f2996a = aVar;
        this.f2997b = new s(aVar);
    }

    public static j a(a aVar) {
        if (aVar.a(1)) {
            return new g(aVar);
        }
        if (!aVar.a(2)) {
            return new k(aVar);
        }
        int a2 = s.a(aVar, 1, 4);
        if (a2 == 4) {
            return new a(aVar);
        }
        if (a2 == 5) {
            return new b(aVar);
        }
        int a3 = s.a(aVar, 1, 5);
        if (a3 == 12) {
            return new c(aVar);
        }
        if (a3 == 13) {
            return new d(aVar);
        }
        switch (s.a(aVar, 1, 7)) {
            case 56:
                return new e(aVar, "310", "11");
            case 57:
                return new e(aVar, "320", "11");
            case 58:
                return new e(aVar, "310", "13");
            case 59:
                return new e(aVar, "320", "13");
            case 60:
                return new e(aVar, "310", "15");
            case 61:
                return new e(aVar, "320", "15");
            case 62:
                return new e(aVar, "310", "17");
            case 63:
                return new e(aVar, "320", "17");
            default:
                throw new IllegalStateException("unknown decoder: " + aVar);
        }
    }
}
