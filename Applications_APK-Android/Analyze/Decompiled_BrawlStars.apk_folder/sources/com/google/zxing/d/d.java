package com.google.zxing.d;

import com.google.zxing.WriterException;
import com.google.zxing.common.b;
import com.google.zxing.f;
import java.util.ArrayList;
import java.util.Map;

/* compiled from: Code128Writer */
public final class d extends s {

    /* compiled from: Code128Writer */
    enum a {
        UNCODABLE,
        ONE_DIGIT,
        TWO_DIGITS,
        FNC_1
    }

    public final b a(String str, com.google.zxing.a aVar, int i, int i2, Map<f, ?> map) throws WriterException {
        if (aVar == com.google.zxing.a.CODE_128) {
            return super.a(str, aVar, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode CODE_128, but got " + aVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.zxing.d.s.a(boolean[], int, int[], boolean):int
     arg types: [boolean[], int, int[], int]
     candidates:
      com.google.zxing.d.s.a(boolean[], int, int, int):com.google.zxing.common.b
      com.google.zxing.d.s.a(boolean[], int, int[], boolean):int */
    public final boolean[] a(String str) {
        int i;
        int length = str.length();
        if (length <= 0 || length > 80) {
            throw new IllegalArgumentException("Contents length should be between 1 and 80 characters, but got " + length);
        }
        int i2 = 0;
        for (int i3 = 0; i3 < length; i3++) {
            char charAt = str.charAt(i3);
            switch (charAt) {
                case 241:
                case 242:
                case 243:
                case 244:
                    break;
                default:
                    if (charAt <= 127) {
                        break;
                    } else {
                        throw new IllegalArgumentException("Bad character in input: " + charAt);
                    }
            }
        }
        ArrayList<int[]> arrayList = new ArrayList<>();
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        int i7 = 1;
        while (i4 < length) {
            int a2 = a(str, i4, i6);
            int i8 = 100;
            if (a2 == i6) {
                switch (str.charAt(i4)) {
                    case 241:
                        i8 = 102;
                        break;
                    case 242:
                        i8 = 97;
                        break;
                    case 243:
                        i8 = 96;
                        break;
                    case 244:
                        if (i6 == 101) {
                            i8 = 101;
                            break;
                        }
                        break;
                    default:
                        if (i6 != 100) {
                            if (i6 == 101) {
                                i8 = str.charAt(i4) - ' ';
                                if (i8 < 0) {
                                    i8 += 96;
                                    break;
                                }
                            } else {
                                i8 = Integer.parseInt(str.substring(i4, i4 + 2));
                                i4++;
                                break;
                            }
                        } else {
                            i8 = str.charAt(i4) - ' ';
                            break;
                        }
                        break;
                }
                i4++;
            } else {
                i = i6 == 0 ? a2 != 100 ? a2 != 101 ? 105 : 103 : 104 : a2;
                i6 = a2;
            }
            arrayList.add(c.f3024a[i]);
            i5 += i * i7;
            if (i4 != 0) {
                i7++;
            }
        }
        arrayList.add(c.f3024a[i5 % 103]);
        arrayList.add(c.f3024a[106]);
        int i9 = 0;
        for (int[] iArr : arrayList) {
            int i10 = i9;
            for (int i11 : (int[]) r13.next()) {
                i10 += i11;
            }
            i9 = i10;
        }
        boolean[] zArr = new boolean[i9];
        for (int[] a3 : arrayList) {
            i2 += a(zArr, i2, a3, true);
        }
        return zArr;
    }

    private static a a(CharSequence charSequence, int i) {
        int length = charSequence.length();
        if (i >= length) {
            return a.UNCODABLE;
        }
        char charAt = charSequence.charAt(i);
        if (charAt == 241) {
            return a.FNC_1;
        }
        if (charAt < '0' || charAt > '9') {
            return a.UNCODABLE;
        }
        int i2 = i + 1;
        if (i2 >= length) {
            return a.ONE_DIGIT;
        }
        char charAt2 = charSequence.charAt(i2);
        if (charAt2 < '0' || charAt2 > '9') {
            return a.ONE_DIGIT;
        }
        return a.TWO_DIGITS;
    }

    private static int a(CharSequence charSequence, int i, int i2) {
        a a2;
        a a3;
        char charAt;
        a a4 = a(charSequence, i);
        if (a4 == a.ONE_DIGIT) {
            return 100;
        }
        if (a4 == a.UNCODABLE) {
            if (i >= charSequence.length() || ((charAt = charSequence.charAt(i)) >= ' ' && (i2 != 101 || charAt >= '`'))) {
                return 100;
            }
            return 101;
        } else if (i2 == 99) {
            return 99;
        } else {
            if (i2 != 100) {
                if (a4 == a.FNC_1) {
                    a4 = a(charSequence, i + 1);
                }
                if (a4 == a.TWO_DIGITS) {
                    return 99;
                }
                return 100;
            } else if (a4 == a.FNC_1 || (a2 = a(charSequence, i + 2)) == a.UNCODABLE || a2 == a.ONE_DIGIT) {
                return 100;
            } else {
                if (a2 != a.FNC_1) {
                    int i3 = i + 4;
                    while (true) {
                        a3 = a(charSequence, i3);
                        if (a3 != a.TWO_DIGITS) {
                            break;
                        }
                        i3 += 2;
                    }
                    if (a3 == a.ONE_DIGIT) {
                        return 100;
                    }
                    return 99;
                } else if (a(charSequence, i + 3) == a.TWO_DIGITS) {
                    return 99;
                } else {
                    return 100;
                }
            }
        }
    }
}
