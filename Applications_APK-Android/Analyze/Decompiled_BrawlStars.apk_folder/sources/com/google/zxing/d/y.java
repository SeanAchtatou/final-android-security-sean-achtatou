package com.google.zxing.d;

import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.ReaderException;
import com.google.zxing.common.a;
import com.google.zxing.d;
import com.google.zxing.n;
import com.google.zxing.o;
import com.google.zxing.p;
import com.google.zxing.q;
import java.util.Arrays;
import java.util.Map;

/* compiled from: UPCEANReader */
public abstract class y extends r {

    /* renamed from: b  reason: collision with root package name */
    static final int[] f3049b = {1, 1, 1};
    static final int[] c = {1, 1, 1, 1, 1};
    static final int[] d = {1, 1, 1, 1, 1, 1};
    static final int[][] e = {new int[]{3, 2, 1, 1}, new int[]{2, 2, 2, 1}, new int[]{2, 1, 2, 2}, new int[]{1, 4, 1, 1}, new int[]{1, 1, 3, 2}, new int[]{1, 2, 3, 1}, new int[]{1, 1, 1, 4}, new int[]{1, 3, 1, 2}, new int[]{1, 2, 1, 3}, new int[]{3, 1, 1, 2}};
    static final int[][] f = new int[20][];

    /* renamed from: a  reason: collision with root package name */
    private final StringBuilder f3050a = new StringBuilder(20);
    private final x g = new x();
    private final m h = new m();

    /* access modifiers changed from: protected */
    public abstract int a(a aVar, int[] iArr, StringBuilder sb) throws NotFoundException;

    /* access modifiers changed from: package-private */
    public abstract com.google.zxing.a b();

    static {
        System.arraycopy(e, 0, f, 0, 10);
        for (int i = 10; i < 20; i++) {
            int[] iArr = e[i - 10];
            int[] iArr2 = new int[iArr.length];
            for (int i2 = 0; i2 < iArr.length; i2++) {
                iArr2[i2] = iArr[(iArr.length - i2) - 1];
            }
            f[i] = iArr2;
        }
    }

    protected y() {
    }

    static int[] a(a aVar) throws NotFoundException {
        int[] iArr = new int[f3049b.length];
        int[] iArr2 = null;
        boolean z = false;
        int i = 0;
        while (!z) {
            Arrays.fill(iArr, 0, f3049b.length, 0);
            iArr2 = a(aVar, i, false, f3049b, iArr);
            int i2 = iArr2[0];
            int i3 = iArr2[1];
            int i4 = i2 - (i3 - i2);
            if (i4 >= 0) {
                z = aVar.a(i4, i2, false);
            }
            i = i3;
        }
        return iArr2;
    }

    public n a(int i, a aVar, Map<d, ?> map) throws NotFoundException, ChecksumException, FormatException {
        return a(i, aVar, a(aVar), map);
    }

    public n a(int i, a aVar, int[] iArr, Map<d, ?> map) throws NotFoundException, ChecksumException, FormatException {
        q qVar;
        int i2;
        int[] iArr2;
        boolean z;
        String str = null;
        if (map == null) {
            qVar = null;
        } else {
            qVar = (q) map.get(d.NEED_RESULT_POINT_CALLBACK);
        }
        if (qVar != null) {
            qVar.a(new p(((float) (iArr[0] + iArr[1])) / 2.0f, (float) i));
        }
        StringBuilder sb = this.f3050a;
        sb.setLength(0);
        int a2 = a(aVar, iArr, sb);
        if (qVar != null) {
            qVar.a(new p((float) a2, (float) i));
        }
        int[] a3 = a(aVar, a2);
        if (qVar != null) {
            qVar.a(new p(((float) (a3[0] + a3[1])) / 2.0f, (float) i));
        }
        int i3 = a3[1];
        int i4 = (i3 - a3[0]) + i3;
        if (i4 >= aVar.f2965b || !aVar.a(i3, i4, false)) {
            throw NotFoundException.a();
        }
        String sb2 = sb.toString();
        if (sb2.length() < 8) {
            throw FormatException.a();
        } else if (a(sb2)) {
            com.google.zxing.a b2 = b();
            float f2 = (float) i;
            n nVar = new n(sb2, null, new p[]{new p(((float) (iArr[1] + iArr[0])) / 2.0f, f2), new p(((float) (a3[1] + a3[0])) / 2.0f, f2)}, b2);
            try {
                n a4 = this.g.a(i, aVar, a3[1]);
                nVar.a(o.UPC_EAN_EXTENSION, a4.f3149a);
                nVar.a(a4.e);
                p[] pVarArr = a4.c;
                p[] pVarArr2 = nVar.c;
                if (pVarArr2 == null) {
                    nVar.c = pVarArr;
                } else if (pVarArr != null && pVarArr.length > 0) {
                    p[] pVarArr3 = new p[(pVarArr2.length + pVarArr.length)];
                    System.arraycopy(pVarArr2, 0, pVarArr3, 0, pVarArr2.length);
                    System.arraycopy(pVarArr, 0, pVarArr3, pVarArr2.length, pVarArr.length);
                    nVar.c = pVarArr3;
                }
                i2 = a4.f3149a.length();
            } catch (ReaderException unused) {
                i2 = 0;
            }
            if (map == null) {
                iArr2 = null;
            } else {
                iArr2 = (int[]) map.get(d.ALLOWED_EAN_EXTENSIONS);
            }
            if (iArr2 != null) {
                int length = iArr2.length;
                int i5 = 0;
                while (true) {
                    if (i5 >= length) {
                        z = false;
                        break;
                    } else if (i2 == iArr2[i5]) {
                        z = true;
                        break;
                    } else {
                        i5++;
                    }
                }
                if (!z) {
                    throw NotFoundException.a();
                }
            }
            if (b2 == com.google.zxing.a.EAN_13 || b2 == com.google.zxing.a.UPC_A) {
                m mVar = this.h;
                mVar.a();
                int parseInt = Integer.parseInt(sb2.substring(0, 3));
                int size = mVar.f3033a.size();
                int i6 = 0;
                while (true) {
                    if (i6 < size) {
                        int[] iArr3 = mVar.f3033a.get(i6);
                        int i7 = iArr3[0];
                        if (parseInt < i7) {
                            break;
                        }
                        if (iArr3.length != 1) {
                            i7 = iArr3[1];
                        }
                        if (parseInt <= i7) {
                            str = mVar.f3034b.get(i6);
                            break;
                        }
                        i6++;
                    } else {
                        break;
                    }
                }
                if (str != null) {
                    nVar.a(o.POSSIBLE_COUNTRY, str);
                }
            }
            return nVar;
        } else {
            throw ChecksumException.a();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(String str) throws FormatException {
        return a((CharSequence) str);
    }

    static boolean a(CharSequence charSequence) throws FormatException {
        int length = charSequence.length();
        if (length == 0) {
            return false;
        }
        int i = length - 1;
        return b(charSequence.subSequence(0, i)) == Character.digit(charSequence.charAt(i), 10);
    }

    static int b(CharSequence charSequence) throws FormatException {
        int length = charSequence.length();
        int i = 0;
        for (int i2 = length - 1; i2 >= 0; i2 -= 2) {
            int charAt = charSequence.charAt(i2) - '0';
            if (charAt < 0 || charAt > 9) {
                throw FormatException.a();
            }
            i += charAt;
        }
        int i3 = i * 3;
        for (int i4 = length - 2; i4 >= 0; i4 -= 2) {
            int charAt2 = charSequence.charAt(i4) - '0';
            if (charAt2 < 0 || charAt2 > 9) {
                throw FormatException.a();
            }
            i3 += charAt2;
        }
        return (1000 - i3) % 10;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.zxing.d.y.a(com.google.zxing.common.a, int, boolean, int[]):int[]
     arg types: [com.google.zxing.common.a, int, int, int[]]
     candidates:
      com.google.zxing.d.y.a(com.google.zxing.common.a, int[], int, int[][]):int
      com.google.zxing.d.y.a(int, com.google.zxing.common.a, int[], java.util.Map<com.google.zxing.d, ?>):com.google.zxing.n
      com.google.zxing.d.y.a(com.google.zxing.common.a, int, boolean, int[]):int[] */
    /* access modifiers changed from: package-private */
    public int[] a(a aVar, int i) throws NotFoundException {
        return a(aVar, i, false, f3049b);
    }

    static int[] a(a aVar, int i, boolean z, int[] iArr) throws NotFoundException {
        return a(aVar, i, z, iArr, new int[iArr.length]);
    }

    static int a(a aVar, int[] iArr, int i, int[][] iArr2) throws NotFoundException {
        a(aVar, i, iArr);
        int length = iArr2.length;
        float f2 = 0.48f;
        int i2 = -1;
        for (int i3 = 0; i3 < length; i3++) {
            float a2 = a(iArr, iArr2[i3], 0.7f);
            if (a2 < f2) {
                i2 = i3;
                f2 = a2;
            }
        }
        if (i2 >= 0) {
            return i2;
        }
        throw NotFoundException.a();
    }

    private static int[] a(a aVar, int i, boolean z, int[] iArr, int[] iArr2) throws NotFoundException {
        int i2 = aVar.f2965b;
        int d2 = z ? aVar.d(i) : aVar.c(i);
        int length = iArr.length;
        int i3 = d2;
        int i4 = 0;
        while (d2 < i2) {
            if (aVar.a(d2) != z) {
                iArr2[i4] = iArr2[i4] + 1;
            } else {
                if (i4 != length - 1) {
                    i4++;
                } else if (a(iArr2, iArr, 0.7f) < 0.48f) {
                    return new int[]{i3, d2};
                } else {
                    i3 += iArr2[0] + iArr2[1];
                    int i5 = i4 - 1;
                    System.arraycopy(iArr2, 2, iArr2, 0, i5);
                    iArr2[i5] = 0;
                    iArr2[i4] = 0;
                    i4--;
                }
                iArr2[i4] = 1;
                z = !z;
            }
            d2++;
        }
        throw NotFoundException.a();
    }
}
