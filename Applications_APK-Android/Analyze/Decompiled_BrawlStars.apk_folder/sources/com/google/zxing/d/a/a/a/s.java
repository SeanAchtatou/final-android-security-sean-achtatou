package com.google.zxing.d.a.a.a;

import android.support.v7.widget.helper.ItemTouchHelper;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.common.a;
import com.google.zxing.d.a.a.a.m;

/* compiled from: GeneralAppIdDecoder */
final class s {

    /* renamed from: a  reason: collision with root package name */
    private final a f3011a;

    /* renamed from: b  reason: collision with root package name */
    private final m f3012b = new m();
    private final StringBuilder c = new StringBuilder();

    s(a aVar) {
        this.f3011a = aVar;
    }

    /* access modifiers changed from: package-private */
    public final String a(StringBuilder sb, int i) throws NotFoundException, FormatException {
        String str;
        String str2 = null;
        while (true) {
            o a2 = a(i, str2);
            String a3 = r.a(a2.f3005a);
            if (a3 != null) {
                sb.append(a3);
            }
            if (a2.c) {
                str = String.valueOf(a2.f3006b);
            } else {
                str = null;
            }
            if (i == a2.d) {
                return sb.toString();
            }
            i = a2.d;
            str2 = str;
        }
    }

    private boolean a(int i) {
        if (i + 7 <= this.f3011a.f2965b) {
            int i2 = i;
            while (true) {
                int i3 = i + 3;
                if (i2 >= i3) {
                    return this.f3011a.a(i3);
                }
                if (this.f3011a.a(i2)) {
                    return true;
                }
                i2++;
            }
        } else if (i + 4 <= this.f3011a.f2965b) {
            return true;
        } else {
            return false;
        }
    }

    private p b(int i) throws FormatException {
        int i2 = i + 7;
        if (i2 > this.f3011a.f2965b) {
            int a2 = a(i, 4);
            if (a2 == 0) {
                return new p(this.f3011a.f2965b, 10, 10);
            }
            return new p(this.f3011a.f2965b, a2 - 1, 10);
        }
        int a3 = a(i, 7) - 8;
        return new p(i2, a3 / 11, a3 % 11);
    }

    /* access modifiers changed from: package-private */
    public final int a(int i, int i2) {
        return a(this.f3011a, i, i2);
    }

    static int a(a aVar, int i, int i2) {
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            if (aVar.a(i + i4)) {
                i3 |= 1 << ((i2 - i4) - 1);
            }
        }
        return i3;
    }

    /* access modifiers changed from: package-private */
    public final o a(int i, String str) throws FormatException {
        this.c.setLength(0);
        if (str != null) {
            this.c.append(str);
        }
        this.f3012b.f3000a = i;
        o a2 = a();
        if (a2 == null || !a2.c) {
            return new o(this.f3012b.f3000a, this.c.toString());
        }
        return new o(this.f3012b.f3000a, this.c.toString(), a2.f3006b);
    }

    private o a() throws FormatException {
        boolean z;
        l lVar;
        do {
            int i = this.f3012b.f3000a;
            if (this.f3012b.a()) {
                lVar = d();
                z = lVar.f2999b;
            } else if (this.f3012b.b()) {
                lVar = c();
                z = lVar.f2999b;
            } else {
                lVar = b();
                z = lVar.f2999b;
            }
            if (!(i != this.f3012b.f3000a) && !z) {
                break;
            }
        } while (!z);
        return lVar.f2998a;
    }

    private l b() throws FormatException {
        o oVar;
        while (a(this.f3012b.f3000a)) {
            p b2 = b(this.f3012b.f3000a);
            this.f3012b.f3000a = b2.d;
            if (b2.a()) {
                if (b2.b()) {
                    oVar = new o(this.f3012b.f3000a, this.c.toString());
                } else {
                    oVar = new o(this.f3012b.f3000a, this.c.toString(), b2.f3008b);
                }
                return new l(oVar, true);
            }
            this.c.append(b2.f3007a);
            if (b2.b()) {
                return new l(new o(this.f3012b.f3000a, this.c.toString()), true);
            }
            this.c.append(b2.f3008b);
        }
        if (i(this.f3012b.f3000a)) {
            this.f3012b.f3001b = m.a.ALPHA;
            this.f3012b.a(4);
        }
        return new l(false);
    }

    private l c() throws FormatException {
        while (c(this.f3012b.f3000a)) {
            n d = d(this.f3012b.f3000a);
            this.f3012b.f3000a = d.d;
            if (d.a()) {
                return new l(new o(this.f3012b.f3000a, this.c.toString()), true);
            }
            this.c.append(d.f3004a);
        }
        if (h(this.f3012b.f3000a)) {
            this.f3012b.a(3);
            this.f3012b.f3001b = m.a.NUMERIC;
        } else if (g(this.f3012b.f3000a)) {
            if (this.f3012b.f3000a + 5 < this.f3011a.f2965b) {
                this.f3012b.a(5);
            } else {
                this.f3012b.f3000a = this.f3011a.f2965b;
            }
            this.f3012b.f3001b = m.a.ALPHA;
        }
        return new l(false);
    }

    private l d() {
        while (e(this.f3012b.f3000a)) {
            n f = f(this.f3012b.f3000a);
            this.f3012b.f3000a = f.d;
            if (f.a()) {
                return new l(new o(this.f3012b.f3000a, this.c.toString()), true);
            }
            this.c.append(f.f3004a);
        }
        if (h(this.f3012b.f3000a)) {
            this.f3012b.a(3);
            this.f3012b.f3001b = m.a.NUMERIC;
        } else if (g(this.f3012b.f3000a)) {
            if (this.f3012b.f3000a + 5 < this.f3011a.f2965b) {
                this.f3012b.a(5);
            } else {
                this.f3012b.f3000a = this.f3011a.f2965b;
            }
            this.f3012b.f3001b = m.a.ISO_IEC_646;
        }
        return new l(false);
    }

    private boolean c(int i) {
        int a2;
        if (i + 5 > this.f3011a.f2965b) {
            return false;
        }
        int a3 = a(i, 5);
        if (a3 >= 5 && a3 < 16) {
            return true;
        }
        if (i + 7 > this.f3011a.f2965b) {
            return false;
        }
        int a4 = a(i, 7);
        if (a4 < 64 || a4 >= 116) {
            return i + 8 <= this.f3011a.f2965b && (a2 = a(i, 8)) >= 232 && a2 < 253;
        }
        return true;
    }

    private n d(int i) throws FormatException {
        char c2;
        int a2 = a(i, 5);
        if (a2 == 15) {
            return new n(i + 5, '$');
        }
        if (a2 >= 5 && a2 < 15) {
            return new n(i + 5, (char) ((a2 + 48) - 5));
        }
        int a3 = a(i, 7);
        if (a3 >= 64 && a3 < 90) {
            return new n(i + 7, (char) (a3 + 1));
        }
        if (a3 >= 90 && a3 < 116) {
            return new n(i + 7, (char) (a3 + 7));
        }
        switch (a(i, 8)) {
            case 232:
                c2 = '!';
                break;
            case 233:
                c2 = '\"';
                break;
            case 234:
                c2 = '%';
                break;
            case 235:
                c2 = '&';
                break;
            case 236:
                c2 = '\'';
                break;
            case 237:
                c2 = '(';
                break;
            case 238:
                c2 = ')';
                break;
            case 239:
                c2 = '*';
                break;
            case 240:
                c2 = '+';
                break;
            case 241:
                c2 = ',';
                break;
            case 242:
                c2 = '-';
                break;
            case 243:
                c2 = '.';
                break;
            case 244:
                c2 = '/';
                break;
            case 245:
                c2 = ':';
                break;
            case 246:
                c2 = ';';
                break;
            case 247:
                c2 = '<';
                break;
            case 248:
                c2 = '=';
                break;
            case 249:
                c2 = '>';
                break;
            case ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION:
                c2 = '?';
                break;
            case 251:
                c2 = '_';
                break;
            case 252:
                c2 = ' ';
                break;
            default:
                throw FormatException.a();
        }
        return new n(i + 8, c2);
    }

    private boolean e(int i) {
        int a2;
        if (i + 5 > this.f3011a.f2965b) {
            return false;
        }
        int a3 = a(i, 5);
        if (a3 < 5 || a3 >= 16) {
            return i + 6 <= this.f3011a.f2965b && (a2 = a(i, 6)) >= 16 && a2 < 63;
        }
        return true;
    }

    private n f(int i) {
        char c2;
        int a2 = a(i, 5);
        if (a2 == 15) {
            return new n(i + 5, '$');
        }
        if (a2 >= 5 && a2 < 15) {
            return new n(i + 5, (char) ((a2 + 48) - 5));
        }
        int a3 = a(i, 6);
        if (a3 >= 32 && a3 < 58) {
            return new n(i + 6, (char) (a3 + 33));
        }
        switch (a3) {
            case 58:
                c2 = '*';
                break;
            case 59:
                c2 = ',';
                break;
            case 60:
                c2 = '-';
                break;
            case 61:
                c2 = '.';
                break;
            case 62:
                c2 = '/';
                break;
            default:
                throw new IllegalStateException("Decoding invalid alphanumeric value: " + a3);
        }
        return new n(i + 6, c2);
    }

    private boolean g(int i) {
        int i2;
        if (i + 1 > this.f3011a.f2965b) {
            return false;
        }
        int i3 = 0;
        while (i3 < 5 && (i2 = i3 + i) < this.f3011a.f2965b) {
            if (i3 == 2) {
                if (!this.f3011a.a(i + 2)) {
                    return false;
                }
            } else if (this.f3011a.a(i2)) {
                return false;
            }
            i3++;
        }
        return true;
    }

    private boolean h(int i) {
        int i2 = i + 3;
        if (i2 > this.f3011a.f2965b) {
            return false;
        }
        while (i < i2) {
            if (this.f3011a.a(i)) {
                return false;
            }
            i++;
        }
        return true;
    }

    private boolean i(int i) {
        int i2;
        if (i + 1 > this.f3011a.f2965b) {
            return false;
        }
        int i3 = 0;
        while (i3 < 4 && (i2 = i3 + i) < this.f3011a.f2965b) {
            if (this.f3011a.a(i2)) {
                return false;
            }
            i3++;
        }
        return true;
    }
}
