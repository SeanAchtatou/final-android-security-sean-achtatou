package com.google.zxing.d;

import com.google.zxing.WriterException;
import com.google.zxing.a;
import com.google.zxing.common.b;
import java.util.Map;

/* compiled from: Code39Writer */
public final class f extends s {
    public final b a(String str, a aVar, int i, int i2, Map<com.google.zxing.f, ?> map) throws WriterException {
        if (aVar == a.CODE_39) {
            return super.a(str, aVar, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode CODE_39, but got " + aVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.zxing.d.s.a(boolean[], int, int[], boolean):int
     arg types: [boolean[], int, int[], int]
     candidates:
      com.google.zxing.d.s.a(boolean[], int, int, int):com.google.zxing.common.b
      com.google.zxing.d.s.a(boolean[], int, int[], boolean):int */
    public final boolean[] a(String str) {
        int length = str.length();
        if (length <= 80) {
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                } else if ("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%".indexOf(str.charAt(i)) < 0) {
                    str = b(str);
                    length = str.length();
                    if (length > 80) {
                        throw new IllegalArgumentException("Requested contents should be less than 80 digits long, but got " + length + " (extended full ASCII mode)");
                    }
                } else {
                    i++;
                }
            }
            int[] iArr = new int[9];
            int i2 = length + 25;
            int i3 = 0;
            while (i3 < length) {
                a(e.f3027a["0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%".indexOf(str.charAt(i3))], iArr);
                int i4 = i2;
                for (int i5 = 0; i5 < 9; i5++) {
                    i4 += iArr[i5];
                }
                i3++;
                i2 = i4;
            }
            boolean[] zArr = new boolean[i2];
            a(148, iArr);
            int a2 = a(zArr, 0, iArr, true);
            int[] iArr2 = {1};
            int a3 = a2 + a(zArr, a2, iArr2, false);
            for (int i6 = 0; i6 < length; i6++) {
                a(e.f3027a["0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%".indexOf(str.charAt(i6))], iArr);
                int a4 = a3 + a(zArr, a3, iArr, true);
                a3 = a4 + a(zArr, a4, iArr2, false);
            }
            a(148, iArr);
            a(zArr, a3, iArr, true);
            return zArr;
        }
        throw new IllegalArgumentException("Requested contents should be less than 80 digits long, but got " + length);
    }

    private static void a(int i, int[] iArr) {
        for (int i2 = 0; i2 < 9; i2++) {
            int i3 = 1;
            if (((1 << (8 - i2)) & i) != 0) {
                i3 = 2;
            }
            iArr[i2] = i3;
        }
    }

    private static String b(String str) {
        int length = str.length();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (charAt != 0) {
                if (charAt != ' ') {
                    if (charAt == '@') {
                        sb.append("%V");
                    } else if (charAt == '`') {
                        sb.append("%W");
                    } else if (!(charAt == '-' || charAt == '.')) {
                        if (charAt > 0 && charAt < 27) {
                            sb.append('$');
                            sb.append((char) ((charAt - 1) + 65));
                        } else if (charAt > 26 && charAt < ' ') {
                            sb.append('%');
                            sb.append((char) ((charAt - 27) + 65));
                        } else if ((charAt > ' ' && charAt < '-') || charAt == '/' || charAt == ':') {
                            sb.append('/');
                            sb.append((char) ((charAt - '!') + 65));
                        } else if (charAt > '/' && charAt < ':') {
                            sb.append((char) ((charAt - '0') + 48));
                        } else if (charAt > ':' && charAt < '@') {
                            sb.append('%');
                            sb.append((char) ((charAt - ';') + 70));
                        } else if (charAt > '@' && charAt < '[') {
                            sb.append((char) ((charAt - 'A') + 65));
                        } else if (charAt > 'Z' && charAt < '`') {
                            sb.append('%');
                            sb.append((char) ((charAt - '[') + 75));
                        } else if (charAt > '`' && charAt < '{') {
                            sb.append('+');
                            sb.append((char) ((charAt - 'a') + 65));
                        } else if (charAt <= 'z' || charAt >= 128) {
                            throw new IllegalArgumentException("Requested content contains a non-encodable character: '" + str.charAt(i) + "'");
                        } else {
                            sb.append('%');
                            sb.append((char) ((charAt - '{') + 80));
                        }
                    }
                }
                sb.append(charAt);
            } else {
                sb.append("%U");
            }
        }
        return sb.toString();
    }
}
