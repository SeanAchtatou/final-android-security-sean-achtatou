package com.google.zxing.d;

import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.c;
import com.google.zxing.common.a;
import com.google.zxing.d;
import com.google.zxing.n;
import java.util.Map;

/* compiled from: UPCAReader */
public final class t extends y {

    /* renamed from: a  reason: collision with root package name */
    private final y f3041a = new i();

    public final n a(int i, a aVar, int[] iArr, Map<d, ?> map) throws NotFoundException, FormatException, ChecksumException {
        return a(this.f3041a.a(i, aVar, iArr, map));
    }

    public final n a(int i, a aVar, Map<d, ?> map) throws NotFoundException, FormatException, ChecksumException {
        return a(this.f3041a.a(i, aVar, map));
    }

    public final n a(c cVar) throws NotFoundException, FormatException {
        return a(this.f3041a.a(cVar));
    }

    public final n a(c cVar, Map<d, ?> map) throws NotFoundException, FormatException {
        return a(this.f3041a.a(cVar, map));
    }

    /* access modifiers changed from: package-private */
    public final com.google.zxing.a b() {
        return com.google.zxing.a.UPC_A;
    }

    /* access modifiers changed from: protected */
    public final int a(a aVar, int[] iArr, StringBuilder sb) throws NotFoundException {
        return this.f3041a.a(aVar, iArr, sb);
    }

    private static n a(n nVar) throws FormatException {
        String str = nVar.f3149a;
        if (str.charAt(0) == '0') {
            return new n(str.substring(1), null, nVar.c, com.google.zxing.a.UPC_A);
        }
        throw FormatException.a();
    }
}
