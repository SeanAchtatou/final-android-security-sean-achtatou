package com.google.zxing.d.a.a.a;

import com.google.zxing.FormatException;

/* compiled from: DecodedNumeric */
final class p extends q {

    /* renamed from: a  reason: collision with root package name */
    final int f3007a;

    /* renamed from: b  reason: collision with root package name */
    final int f3008b;

    p(int i, int i2, int i3) throws FormatException {
        super(i);
        if (i2 < 0 || i2 > 10 || i3 < 0 || i3 > 10) {
            throw FormatException.a();
        }
        this.f3007a = i2;
        this.f3008b = i3;
    }

    /* access modifiers changed from: package-private */
    public final boolean a() {
        return this.f3007a == 10;
    }

    /* access modifiers changed from: package-private */
    public final boolean b() {
        return this.f3008b == 10;
    }
}
