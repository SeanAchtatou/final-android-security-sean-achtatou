package com.google.zxing.d.a;

import com.google.zxing.NotFoundException;
import com.google.zxing.common.a;
import com.google.zxing.d;
import com.google.zxing.n;
import com.google.zxing.p;
import com.google.zxing.q;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: RSS14Reader */
public final class e extends a {
    private static final int[] g = {1, 10, 34, 70, 126};
    private static final int[] h = {4, 20, 48, 81};
    private static final int[] i = {0, 161, 961, 2015, 2715};
    private static final int[] j = {0, 336, 1036, 1516};
    private static final int[] k = {8, 6, 4, 3, 1};
    private static final int[] l = {2, 4, 6, 8};
    private static final int[][] m = {new int[]{3, 8, 2, 1}, new int[]{3, 5, 5, 1}, new int[]{3, 3, 7, 1}, new int[]{3, 1, 9, 1}, new int[]{2, 7, 4, 1}, new int[]{2, 5, 6, 1}, new int[]{2, 3, 8, 1}, new int[]{1, 5, 7, 1}, new int[]{1, 3, 9, 1}};
    private final List<d> n = new ArrayList();
    private final List<d> o = new ArrayList();

    public final n a(int i2, a aVar, Map<d, ?> map) throws NotFoundException {
        a(this.n, a(aVar, false, i2, map));
        aVar.c();
        a(this.o, a(aVar, true, i2, map));
        aVar.c();
        for (d next : this.n) {
            if (next.d > 1) {
                for (d next2 : this.o) {
                    if (next2.d > 1) {
                        int i3 = (next.f3018b + (next2.f3018b * 16)) % 79;
                        int i4 = (next.c.f3019a * 9) + next2.c.f3019a;
                        if (i4 > 72) {
                            i4--;
                        }
                        if (i4 > 8) {
                            i4--;
                        }
                        if (i3 == i4) {
                            String valueOf = String.valueOf((((long) next.f3017a) * 4537077) + ((long) next2.f3017a));
                            StringBuilder sb = new StringBuilder(14);
                            for (int length = 13 - valueOf.length(); length > 0; length--) {
                                sb.append('0');
                            }
                            sb.append(valueOf);
                            int i5 = 0;
                            for (int i6 = 0; i6 < 13; i6++) {
                                int charAt = sb.charAt(i6) - '0';
                                if ((i6 & 1) == 0) {
                                    charAt *= 3;
                                }
                                i5 += charAt;
                            }
                            int i7 = 10 - (i5 % 10);
                            if (i7 == 10) {
                                i7 = 0;
                            }
                            sb.append(i7);
                            p[] pVarArr = next.c.c;
                            p[] pVarArr2 = next2.c.c;
                            return new n(sb.toString(), null, new p[]{pVarArr[0], pVarArr[1], pVarArr2[0], pVarArr2[1]}, com.google.zxing.a.RSS_14);
                        }
                    }
                }
                continue;
            }
        }
        throw NotFoundException.a();
    }

    private static void a(Collection<d> collection, d dVar) {
        if (dVar != null) {
            boolean z = false;
            Iterator<d> it = collection.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                d next = it.next();
                if (next.f3017a == dVar.f3017a) {
                    next.a();
                    z = true;
                    break;
                }
            }
            if (!z) {
                collection.add(dVar);
            }
        }
    }

    public final void a() {
        this.n.clear();
        this.o.clear();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.zxing.d.a.e.a(com.google.zxing.common.a, com.google.zxing.d.a.c, boolean):com.google.zxing.d.a.b
     arg types: [com.google.zxing.common.a, com.google.zxing.d.a.c, int]
     candidates:
      com.google.zxing.d.a.e.a(int, com.google.zxing.common.a, java.util.Map<com.google.zxing.d, ?>):com.google.zxing.n
      com.google.zxing.d.r.a(int[], int[], float):float
      com.google.zxing.d.r.a(com.google.zxing.common.a, int, int[]):void
      com.google.zxing.d.r.a(int, com.google.zxing.common.a, java.util.Map<com.google.zxing.d, ?>):com.google.zxing.n
      com.google.zxing.d.a.e.a(com.google.zxing.common.a, com.google.zxing.d.a.c, boolean):com.google.zxing.d.a.b */
    private d a(a aVar, boolean z, int i2, Map<d, ?> map) {
        int i3;
        int i4;
        q qVar;
        a aVar2 = aVar;
        boolean z2 = z;
        Map<d, ?> map2 = map;
        try {
            int[] iArr = this.f2994a;
            iArr[0] = 0;
            iArr[1] = 0;
            iArr[2] = 0;
            iArr[3] = 0;
            int i5 = aVar2.f2965b;
            int i6 = 0;
            boolean z3 = false;
            while (i6 < i5) {
                z3 = !aVar2.a(i6);
                if (z2 == z3) {
                    break;
                }
                i6++;
            }
            int i7 = i6;
            int i8 = 0;
            while (i6 < i5) {
                if (aVar2.a(i6) != z3) {
                    iArr[i8] = iArr[i8] + 1;
                    int i9 = i2;
                } else {
                    if (i8 != 3) {
                        int i10 = i2;
                        i8++;
                    } else if (a(iArr)) {
                        int[] iArr2 = {i7, i6};
                        boolean a2 = aVar2.a(iArr2[0]);
                        int i11 = iArr2[0] - 1;
                        while (i11 >= 0 && a2 != aVar2.a(i11)) {
                            i11--;
                        }
                        int i12 = i11 + 1;
                        int[] iArr3 = this.f2994a;
                        System.arraycopy(iArr3, 0, iArr3, 1, iArr3.length - 1);
                        iArr3[0] = iArr2[0] - i12;
                        int a3 = a(iArr3, m);
                        int i13 = iArr2[1];
                        if (z2) {
                            i4 = (aVar2.f2965b - 1) - i12;
                            i3 = (aVar2.f2965b - 1) - i13;
                        } else {
                            i3 = i13;
                            i4 = i12;
                        }
                        c cVar = new c(a3, new int[]{i12, iArr2[1]}, i4, i3, i2);
                        if (map2 == null) {
                            qVar = null;
                        } else {
                            qVar = (q) map2.get(d.NEED_RESULT_POINT_CALLBACK);
                        }
                        if (qVar != null) {
                            float f = ((float) (iArr2[0] + iArr2[1])) / 2.0f;
                            if (z2) {
                                f = ((float) (aVar2.f2965b - 1)) - f;
                            }
                            qVar.a(new p(f, (float) i2));
                        }
                        b a4 = a(aVar2, cVar, true);
                        b a5 = a(aVar2, cVar, false);
                        return new d((a4.f3017a * 1597) + a5.f3017a, a4.f3018b + (a5.f3018b * 4), cVar);
                    } else {
                        int i14 = i2;
                        i7 += iArr[0] + iArr[1];
                        iArr[0] = iArr[2];
                        iArr[1] = iArr[3];
                        iArr[2] = 0;
                        iArr[3] = 0;
                        i8--;
                    }
                    iArr[i8] = 1;
                    z3 = !z3;
                }
                i6++;
            }
            throw NotFoundException.a();
        } catch (NotFoundException unused) {
            return null;
        }
    }

    private b a(a aVar, c cVar, boolean z) throws NotFoundException {
        int[] iArr = this.f2995b;
        for (int i2 = 0; i2 < iArr.length; i2++) {
            iArr[i2] = 0;
        }
        if (z) {
            b(aVar, cVar.f3020b[0], iArr);
        } else {
            a(aVar, cVar.f3020b[1] + 1, iArr);
            int i3 = 0;
            for (int length = iArr.length - 1; i3 < length; length--) {
                int i4 = iArr[i3];
                iArr[i3] = iArr[length];
                iArr[length] = i4;
                i3++;
            }
        }
        int i5 = z ? 16 : 15;
        float a2 = ((float) com.google.zxing.common.a.a.a(iArr)) / ((float) i5);
        int[] iArr2 = this.e;
        int[] iArr3 = this.f;
        float[] fArr = this.c;
        float[] fArr2 = this.d;
        for (int i6 = 0; i6 < iArr.length; i6++) {
            float f = ((float) iArr[i6]) / a2;
            int i7 = (int) (0.5f + f);
            if (i7 <= 0) {
                i7 = 1;
            } else if (i7 > 8) {
                i7 = 8;
            }
            int i8 = i6 / 2;
            if ((i6 & 1) == 0) {
                iArr2[i8] = i7;
                fArr[i8] = f - ((float) i7);
            } else {
                iArr3[i8] = i7;
                fArr2[i8] = f - ((float) i7);
            }
        }
        a(z, i5);
        int i9 = 0;
        int i10 = 0;
        for (int length2 = iArr2.length - 1; length2 >= 0; length2--) {
            i9 = (i9 * 9) + iArr2[length2];
            i10 += iArr2[length2];
        }
        int i11 = 0;
        int i12 = 0;
        for (int length3 = iArr3.length - 1; length3 >= 0; length3--) {
            i11 = (i11 * 9) + iArr3[length3];
            i12 += iArr3[length3];
        }
        int i13 = i9 + (i11 * 3);
        if (z) {
            if ((i10 & 1) != 0 || i10 > 12 || i10 < 4) {
                throw NotFoundException.a();
            }
            int i14 = (12 - i10) / 2;
            int i15 = k[i14];
            return new b((f.a(iArr2, i15, false) * g[i14]) + f.a(iArr3, 9 - i15, true) + i[i14], i13);
        } else if ((i12 & 1) != 0 || i12 > 10 || i12 < 4) {
            throw NotFoundException.a();
        } else {
            int i16 = (10 - i12) / 2;
            int i17 = l[i16];
            return new b((f.a(iArr3, 9 - i17, false) * h[i16]) + f.a(iArr2, i17, true) + j[i16], i13);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0025, code lost:
        if (r1 < 4) goto L_0x003f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x003d, code lost:
        if (r1 < 4) goto L_0x003f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x003f, code lost:
        r2 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0041, code lost:
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0042, code lost:
        r7 = false;
     */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0094  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:84:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(boolean r10, int r11) throws com.google.zxing.NotFoundException {
        /*
            r9 = this;
            int[] r0 = r9.e
            int r0 = com.google.zxing.common.a.a.a(r0)
            int[] r1 = r9.f
            int r1 = com.google.zxing.common.a.a.a(r1)
            r2 = 4
            r3 = 0
            r4 = 1
            if (r10 == 0) goto L_0x0028
            r5 = 12
            if (r0 <= r5) goto L_0x0018
            r6 = 0
            r7 = 1
            goto L_0x001e
        L_0x0018:
            if (r0 >= r2) goto L_0x001c
            r6 = 1
            goto L_0x001d
        L_0x001c:
            r6 = 0
        L_0x001d:
            r7 = 0
        L_0x001e:
            if (r1 <= r5) goto L_0x0023
            r5 = r6
            r6 = r7
            goto L_0x003a
        L_0x0023:
            r5 = r6
            r6 = r7
            if (r1 >= r2) goto L_0x0041
            goto L_0x003f
        L_0x0028:
            r5 = 11
            if (r0 <= r5) goto L_0x002f
            r5 = 0
            r6 = 1
            goto L_0x0036
        L_0x002f:
            r5 = 5
            if (r0 >= r5) goto L_0x0034
            r5 = 1
            goto L_0x0035
        L_0x0034:
            r5 = 0
        L_0x0035:
            r6 = 0
        L_0x0036:
            r7 = 10
            if (r1 <= r7) goto L_0x003d
        L_0x003a:
            r2 = 0
            r7 = 1
            goto L_0x0043
        L_0x003d:
            if (r1 >= r2) goto L_0x0041
        L_0x003f:
            r2 = 1
            goto L_0x0042
        L_0x0041:
            r2 = 0
        L_0x0042:
            r7 = 0
        L_0x0043:
            int r8 = r0 + r1
            int r8 = r8 - r11
            r11 = r0 & 1
            if (r11 != r10) goto L_0x004c
            r10 = 1
            goto L_0x004d
        L_0x004c:
            r10 = 0
        L_0x004d:
            r11 = r1 & 1
            if (r11 != r4) goto L_0x0052
            r3 = 1
        L_0x0052:
            if (r8 != r4) goto L_0x0068
            if (r10 == 0) goto L_0x005f
            if (r3 != 0) goto L_0x005a
        L_0x0058:
            r6 = 1
            goto L_0x0092
        L_0x005a:
            com.google.zxing.NotFoundException r10 = com.google.zxing.NotFoundException.a()
            throw r10
        L_0x005f:
            if (r3 == 0) goto L_0x0063
        L_0x0061:
            r7 = 1
            goto L_0x0092
        L_0x0063:
            com.google.zxing.NotFoundException r10 = com.google.zxing.NotFoundException.a()
            throw r10
        L_0x0068:
            r11 = -1
            if (r8 != r11) goto L_0x007f
            if (r10 == 0) goto L_0x0076
            if (r3 != 0) goto L_0x0071
            r5 = 1
            goto L_0x0092
        L_0x0071:
            com.google.zxing.NotFoundException r10 = com.google.zxing.NotFoundException.a()
            throw r10
        L_0x0076:
            if (r3 == 0) goto L_0x007a
            r2 = 1
            goto L_0x0092
        L_0x007a:
            com.google.zxing.NotFoundException r10 = com.google.zxing.NotFoundException.a()
            throw r10
        L_0x007f:
            if (r8 != 0) goto L_0x00cc
            if (r10 == 0) goto L_0x0090
            if (r3 == 0) goto L_0x008b
            if (r0 >= r1) goto L_0x0089
            r5 = 1
            goto L_0x0061
        L_0x0089:
            r2 = 1
            goto L_0x0058
        L_0x008b:
            com.google.zxing.NotFoundException r10 = com.google.zxing.NotFoundException.a()
            throw r10
        L_0x0090:
            if (r3 != 0) goto L_0x00c7
        L_0x0092:
            if (r5 == 0) goto L_0x00a3
            if (r6 != 0) goto L_0x009e
            int[] r10 = r9.e
            float[] r11 = r9.c
            a(r10, r11)
            goto L_0x00a3
        L_0x009e:
            com.google.zxing.NotFoundException r10 = com.google.zxing.NotFoundException.a()
            throw r10
        L_0x00a3:
            if (r6 == 0) goto L_0x00ac
            int[] r10 = r9.e
            float[] r11 = r9.c
            b(r10, r11)
        L_0x00ac:
            if (r2 == 0) goto L_0x00bd
            if (r7 != 0) goto L_0x00b8
            int[] r10 = r9.f
            float[] r11 = r9.c
            a(r10, r11)
            goto L_0x00bd
        L_0x00b8:
            com.google.zxing.NotFoundException r10 = com.google.zxing.NotFoundException.a()
            throw r10
        L_0x00bd:
            if (r7 == 0) goto L_0x00c6
            int[] r10 = r9.f
            float[] r11 = r9.d
            b(r10, r11)
        L_0x00c6:
            return
        L_0x00c7:
            com.google.zxing.NotFoundException r10 = com.google.zxing.NotFoundException.a()
            throw r10
        L_0x00cc:
            com.google.zxing.NotFoundException r10 = com.google.zxing.NotFoundException.a()
            goto L_0x00d2
        L_0x00d1:
            throw r10
        L_0x00d2:
            goto L_0x00d1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.d.a.e.a(boolean, int):void");
    }
}
