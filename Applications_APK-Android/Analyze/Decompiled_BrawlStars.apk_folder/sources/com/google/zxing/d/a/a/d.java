package com.google.zxing.d.a.a;

import android.support.v7.widget.helper.ItemTouchHelper;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.d.a.a;
import com.google.zxing.d.a.a.a.j;
import com.google.zxing.d.a.b;
import com.google.zxing.d.a.c;
import com.google.zxing.d.a.f;
import com.google.zxing.n;
import com.google.zxing.p;
import com.supercell.id.view.AvatarView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: RSSExpandedReader */
public final class d extends a {
    private static final int[] g = {7, 5, 4, 3, 1};
    private static final int[] h = {4, 20, 52, 104, 204};
    private static final int[] i = {0, 348, 1388, 2948, 3988};
    private static final int[][] j = {new int[]{1, 8, 4, 1}, new int[]{3, 6, 4, 1}, new int[]{3, 4, 6, 1}, new int[]{3, 2, 8, 1}, new int[]{2, 6, 5, 1}, new int[]{2, 2, 9, 1}};
    private static final int[][] k = {new int[]{1, 3, 9, 27, 81, 32, 96, 77}, new int[]{20, 60, 180, 118, 143, 7, 21, 63}, new int[]{189, 145, 13, 39, 117, 140, 209, 205}, new int[]{193, 157, 49, 147, 19, 57, 171, 91}, new int[]{62, 186, 136, 197, 169, 85, 44, 132}, new int[]{185, 133, 188, 142, 4, 12, 36, 108}, new int[]{113, 128, 173, 97, 80, 29, 87, 50}, new int[]{150, 28, 84, 41, 123, 158, 52, 156}, new int[]{46, 138, 203, 187, 139, 206, 196, 166}, new int[]{76, 17, 51, 153, 37, 111, 122, 155}, new int[]{43, 129, 176, 106, 107, 110, 119, 146}, new int[]{16, 48, 144, 10, 30, 90, 59, 177}, new int[]{109, 116, 137, ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION, 178, 112, 125, 164}, new int[]{70, 210, 208, 202, 184, 130, 179, 115}, new int[]{134, 191, 151, 31, 93, 68, 204, FacebookRequestErrorClassification.EC_INVALID_TOKEN}, new int[]{148, 22, 66, 198, 172, 94, 71, 2}, new int[]{6, 18, 54, 162, 64, 192, 154, 40}, new int[]{120, 149, 25, 75, 14, 42, 126, 167}, new int[]{79, 26, 78, 23, 69, 207, 199, 175}, new int[]{103, 98, 83, 38, 114, 131, 182, 124}, new int[]{161, 61, 183, 127, 170, 88, 53, 159}, new int[]{55, 165, 73, 8, 24, 72, 5, 15}, new int[]{45, 135, 194, AvatarView.INTRINSIC_POINT_SIZE, 58, 174, 100, 89}};
    private static final int[][] l = {new int[]{0, 0}, new int[]{0, 1, 1}, new int[]{0, 2, 1, 3}, new int[]{0, 4, 1, 3, 2}, new int[]{0, 4, 1, 3, 3, 5}, new int[]{0, 4, 1, 3, 4, 5, 5}, new int[]{0, 0, 1, 1, 2, 2, 3, 3}, new int[]{0, 0, 1, 1, 2, 2, 3, 4, 4}, new int[]{0, 0, 1, 1, 2, 2, 3, 4, 5, 5}, new int[]{0, 0, 1, 1, 2, 3, 3, 4, 4, 5, 5}};
    private final List<b> m = new ArrayList(11);
    private final List<c> n = new ArrayList();
    private final int[] o = new int[2];
    private boolean p;

    public final n a(int i2, com.google.zxing.common.a aVar, Map<com.google.zxing.d, ?> map) throws NotFoundException, FormatException {
        this.m.clear();
        this.p = false;
        try {
            return a(a(i2, aVar));
        } catch (NotFoundException unused) {
            this.m.clear();
            this.p = true;
            return a(a(i2, aVar));
        }
    }

    public final void a() {
        this.m.clear();
        this.n.clear();
    }

    private List<b> a(int i2, com.google.zxing.common.a aVar) throws NotFoundException {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        while (true) {
            try {
                this.m.add(a(aVar, this.m, i2));
            } catch (NotFoundException e) {
                if (this.m.isEmpty()) {
                    throw e;
                } else if (b()) {
                    return this.m;
                } else {
                    boolean z7 = !this.n.isEmpty();
                    int i3 = 0;
                    boolean z8 = false;
                    while (true) {
                        if (i3 >= this.n.size()) {
                            z = false;
                            break;
                        }
                        c cVar = this.n.get(i3);
                        if (cVar.f3016b > i2) {
                            z = cVar.a(this.m);
                            break;
                        }
                        z8 = cVar.a(this.m);
                        i3++;
                    }
                    if (!z && !z8) {
                        List<b> list = this.m;
                        Iterator<T> it = this.n.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                z2 = false;
                                break;
                            }
                            c cVar2 = (c) it.next();
                            Iterator<T> it2 = list.iterator();
                            while (true) {
                                if (!it2.hasNext()) {
                                    z5 = true;
                                    continue;
                                    break;
                                }
                                b bVar = (b) it2.next();
                                Iterator<b> it3 = cVar2.f3015a.iterator();
                                while (true) {
                                    if (it3.hasNext()) {
                                        if (bVar.equals(it3.next())) {
                                            z6 = true;
                                            continue;
                                            break;
                                        }
                                    } else {
                                        z6 = false;
                                        continue;
                                        break;
                                    }
                                }
                                if (!z6) {
                                    z5 = false;
                                    continue;
                                    break;
                                }
                            }
                            if (z5) {
                                z2 = true;
                                break;
                            }
                        }
                        if (!z2) {
                            this.n.add(i3, new c(this.m, i2, false));
                            List<b> list2 = this.m;
                            Iterator<c> it4 = this.n.iterator();
                            while (it4.hasNext()) {
                                c next = it4.next();
                                if (next.f3015a.size() != list2.size()) {
                                    Iterator<b> it5 = next.f3015a.iterator();
                                    while (true) {
                                        if (!it5.hasNext()) {
                                            z3 = true;
                                            break;
                                        }
                                        b next2 = it5.next();
                                        Iterator<b> it6 = list2.iterator();
                                        while (true) {
                                            if (it6.hasNext()) {
                                                if (next2.equals(it6.next())) {
                                                    z4 = true;
                                                    continue;
                                                    break;
                                                }
                                            } else {
                                                z4 = false;
                                                continue;
                                                break;
                                            }
                                        }
                                        if (!z4) {
                                            z3 = false;
                                            break;
                                        }
                                    }
                                    if (z3) {
                                        it4.remove();
                                    }
                                }
                            }
                        }
                    }
                    if (z7) {
                        List<b> a2 = a(false);
                        if (a2 != null) {
                            return a2;
                        }
                        List<b> a3 = a(true);
                        if (a3 != null) {
                            return a3;
                        }
                    }
                    throw NotFoundException.a();
                }
            }
        }
    }

    private List<b> a(boolean z) {
        List<b> list = null;
        if (this.n.size() > 25) {
            this.n.clear();
            return null;
        }
        this.m.clear();
        if (z) {
            Collections.reverse(this.n);
        }
        try {
            list = a(new ArrayList(), 0);
        } catch (NotFoundException unused) {
        }
        if (z) {
            Collections.reverse(this.n);
        }
        return list;
    }

    private List<b> a(List<c> list, int i2) throws NotFoundException {
        boolean z;
        while (i2 < this.n.size()) {
            c cVar = this.n.get(i2);
            this.m.clear();
            for (c cVar2 : list) {
                this.m.addAll(cVar2.f3015a);
            }
            this.m.addAll(cVar.f3015a);
            List<b> list2 = this.m;
            int[][] iArr = l;
            int length = iArr.length;
            boolean z2 = false;
            int i3 = 0;
            while (true) {
                if (i3 >= length) {
                    break;
                }
                int[] iArr2 = iArr[i3];
                if (list2.size() <= iArr2.length) {
                    int i4 = 0;
                    while (true) {
                        if (i4 >= list2.size()) {
                            z = true;
                            break;
                        } else if (list2.get(i4).c.f3019a != iArr2[i4]) {
                            z = false;
                            break;
                        } else {
                            i4++;
                        }
                    }
                    if (z) {
                        z2 = true;
                        break;
                    }
                }
                i3++;
            }
            if (!z2) {
                i2++;
            } else if (b()) {
                return this.m;
            } else {
                ArrayList arrayList = new ArrayList();
                arrayList.addAll(list);
                arrayList.add(cVar);
                try {
                    return a(arrayList, i2 + 1);
                } catch (NotFoundException unused) {
                }
            }
        }
        throw NotFoundException.a();
    }

    private static n a(List<b> list) throws NotFoundException, FormatException {
        String a2 = j.a(a.a(list)).a();
        p[] pVarArr = list.get(0).c.c;
        p[] pVarArr2 = list.get(list.size() - 1).c.c;
        return new n(a2, null, new p[]{pVarArr[0], pVarArr[1], pVarArr2[0], pVarArr2[1]}, com.google.zxing.a.RSS_EXPANDED);
    }

    private boolean b() {
        b bVar = this.m.get(0);
        b bVar2 = bVar.f3013a;
        b bVar3 = bVar.f3014b;
        if (bVar3 == null) {
            return false;
        }
        int i2 = bVar3.f3018b;
        int i3 = 2;
        for (int i4 = 1; i4 < this.m.size(); i4++) {
            b bVar4 = this.m.get(i4);
            i2 += bVar4.f3013a.f3018b;
            i3++;
            b bVar5 = bVar4.f3014b;
            if (bVar5 != null) {
                i2 += bVar5.f3018b;
                i3++;
            }
        }
        if (((i3 - 4) * 211) + (i2 % 211) == bVar2.f3017a) {
            return true;
        }
        return false;
    }

    private b a(com.google.zxing.common.a aVar, List<b> list, int i2) throws NotFoundException {
        b bVar;
        com.google.zxing.common.a aVar2 = aVar;
        List<b> list2 = list;
        int i3 = 2;
        boolean z = list.size() % 2 == 0;
        if (this.p) {
            z = !z;
        }
        int i4 = -1;
        boolean z2 = true;
        while (true) {
            int[] iArr = this.f2994a;
            iArr[0] = 0;
            iArr[1] = 0;
            iArr[i3] = 0;
            iArr[3] = 0;
            int i5 = aVar2.f2965b;
            int i6 = i4 >= 0 ? i4 : list.isEmpty() ? 0 : list2.get(list.size() - 1).c.f3020b[1];
            boolean z3 = list.size() % i3 != 0;
            if (this.p) {
                z3 = !z3;
            }
            boolean z4 = false;
            while (i6 < i5) {
                z4 = !aVar2.a(i6);
                if (!z4) {
                    break;
                }
                i6++;
            }
            int i7 = i6;
            int i8 = 0;
            while (i6 < i5) {
                if (aVar2.a(i6) != z4) {
                    iArr[i8] = iArr[i8] + 1;
                } else {
                    if (i8 == 3) {
                        if (z3) {
                            b(iArr);
                        }
                        if (a(iArr)) {
                            int[] iArr2 = this.o;
                            iArr2[0] = i7;
                            iArr2[1] = i6;
                            c a2 = a(aVar2, i2, z);
                            if (a2 == null) {
                                int i9 = this.o[0];
                                if (aVar2.a(i9)) {
                                    i4 = aVar2.c(aVar2.d(i9));
                                } else {
                                    i4 = aVar2.d(aVar2.c(i9));
                                }
                            } else {
                                z2 = false;
                            }
                            if (!z2) {
                                b a3 = a(aVar2, a2, z, true);
                                if (!list.isEmpty()) {
                                    if (list2.get(list.size() - 1).f3014b == null) {
                                        throw NotFoundException.a();
                                    }
                                }
                                try {
                                    bVar = a(aVar2, a2, z, false);
                                } catch (NotFoundException unused) {
                                    bVar = null;
                                }
                                return new b(a3, bVar, a2, true);
                            }
                            i3 = 2;
                        } else {
                            if (z3) {
                                b(iArr);
                            }
                            i7 += iArr[0] + iArr[1];
                            iArr[0] = iArr[2];
                            iArr[1] = iArr[3];
                            iArr[2] = 0;
                            iArr[3] = 0;
                            i8--;
                        }
                    } else {
                        i8++;
                    }
                    iArr[i8] = 1;
                    z4 = !z4;
                }
                i6++;
            }
            throw NotFoundException.a();
        }
    }

    private static void b(int[] iArr) {
        int length = iArr.length;
        for (int i2 = 0; i2 < length / 2; i2++) {
            int i3 = iArr[i2];
            int i4 = (length - i2) - 1;
            iArr[i2] = iArr[i4];
            iArr[i4] = i3;
        }
    }

    private c a(com.google.zxing.common.a aVar, int i2, boolean z) {
        int i3;
        int i4;
        int i5;
        if (z) {
            int i6 = this.o[0] - 1;
            while (i6 >= 0 && !aVar.a(i6)) {
                i6--;
            }
            int i7 = i6 + 1;
            int[] iArr = this.o;
            i4 = iArr[1];
            i5 = i7;
            i3 = iArr[0] - i7;
        } else {
            int[] iArr2 = this.o;
            int i8 = iArr2[0];
            int d = aVar.d(iArr2[1] + 1);
            i3 = d - this.o[1];
            i4 = d;
            i5 = i8;
        }
        int[] iArr3 = this.f2994a;
        System.arraycopy(iArr3, 0, iArr3, 1, iArr3.length - 1);
        iArr3[0] = i3;
        try {
            return new c(a(iArr3, j), new int[]{i5, i4}, i5, i4, i2);
        } catch (NotFoundException unused) {
            return null;
        }
    }

    private b a(com.google.zxing.common.a aVar, c cVar, boolean z, boolean z2) throws NotFoundException {
        com.google.zxing.common.a aVar2 = aVar;
        c cVar2 = cVar;
        int[] iArr = this.f2995b;
        for (int i2 = 0; i2 < iArr.length; i2++) {
            iArr[i2] = 0;
        }
        if (z2) {
            b(aVar2, cVar2.f3020b[0], iArr);
        } else {
            a(aVar2, cVar2.f3020b[1], iArr);
            int i3 = 0;
            for (int length = iArr.length - 1; i3 < length; length--) {
                int i4 = iArr[i3];
                iArr[i3] = iArr[length];
                iArr[length] = i4;
                i3++;
            }
        }
        float a2 = ((float) com.google.zxing.common.a.a.a(iArr)) / 17.0f;
        float f = ((float) (cVar2.f3020b[1] - cVar2.f3020b[0])) / 15.0f;
        if (Math.abs(a2 - f) / f <= 0.3f) {
            int[] iArr2 = this.e;
            int[] iArr3 = this.f;
            float[] fArr = this.c;
            float[] fArr2 = this.d;
            for (int i5 = 0; i5 < iArr.length; i5++) {
                float f2 = (((float) iArr[i5]) * 1.0f) / a2;
                int i6 = (int) (0.5f + f2);
                if (i6 <= 0) {
                    if (f2 >= 0.3f) {
                        i6 = 1;
                    } else {
                        throw NotFoundException.a();
                    }
                } else if (i6 > 8) {
                    if (f2 <= 8.7f) {
                        i6 = 8;
                    } else {
                        throw NotFoundException.a();
                    }
                }
                int i7 = i5 / 2;
                if ((i5 & 1) == 0) {
                    iArr2[i7] = i6;
                    fArr[i7] = f2 - ((float) i6);
                } else {
                    iArr3[i7] = i6;
                    fArr2[i7] = f2 - ((float) i6);
                }
            }
            a(17);
            int i8 = (((cVar2.f3019a * 4) + (z ? 0 : 2)) + (z2 ^ true ? 1 : 0)) - 1;
            int i9 = 0;
            int i10 = 0;
            for (int length2 = iArr2.length - 1; length2 >= 0; length2--) {
                if (a(cVar, z, z2)) {
                    i9 += iArr2[length2] * k[i8][length2 * 2];
                }
                i10 += iArr2[length2];
            }
            int i11 = 0;
            for (int length3 = iArr3.length - 1; length3 >= 0; length3--) {
                if (a(cVar, z, z2)) {
                    i11 += iArr3[length3] * k[i8][(length3 * 2) + 1];
                }
            }
            int i12 = i9 + i11;
            if ((i10 & 1) != 0 || i10 > 13 || i10 < 4) {
                throw NotFoundException.a();
            }
            int i13 = (13 - i10) / 2;
            int i14 = g[i13];
            return new b((f.a(iArr2, i14, true) * h[i13]) + f.a(iArr3, 9 - i14, false) + i[i13], i12);
        }
        throw NotFoundException.a();
    }

    private static boolean a(c cVar, boolean z, boolean z2) {
        return cVar.f3019a != 0 || !z || !z2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:51:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x008a  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0093  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:69:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(int r11) throws com.google.zxing.NotFoundException {
        /*
            r10 = this;
            int[] r11 = r10.e
            int r11 = com.google.zxing.common.a.a.a(r11)
            int[] r0 = r10.f
            int r0 = com.google.zxing.common.a.a.a(r0)
            r1 = 4
            r2 = 13
            r3 = 0
            r4 = 1
            if (r11 <= r2) goto L_0x0016
            r5 = 0
            r6 = 1
            goto L_0x001c
        L_0x0016:
            if (r11 >= r1) goto L_0x001a
            r5 = 1
            goto L_0x001b
        L_0x001a:
            r5 = 0
        L_0x001b:
            r6 = 0
        L_0x001c:
            if (r0 <= r2) goto L_0x0021
            r1 = 0
            r2 = 1
            goto L_0x0027
        L_0x0021:
            if (r0 >= r1) goto L_0x0025
            r1 = 1
            goto L_0x0026
        L_0x0025:
            r1 = 0
        L_0x0026:
            r2 = 0
        L_0x0027:
            int r7 = r11 + r0
            int r7 = r7 + -17
            r8 = r11 & 1
            if (r8 != r4) goto L_0x0031
            r8 = 1
            goto L_0x0032
        L_0x0031:
            r8 = 0
        L_0x0032:
            r9 = r0 & 1
            if (r9 != 0) goto L_0x0037
            r3 = 1
        L_0x0037:
            if (r7 != r4) goto L_0x004d
            if (r8 == 0) goto L_0x0044
            if (r3 != 0) goto L_0x003f
        L_0x003d:
            r6 = 1
            goto L_0x0077
        L_0x003f:
            com.google.zxing.NotFoundException r11 = com.google.zxing.NotFoundException.a()
            throw r11
        L_0x0044:
            if (r3 == 0) goto L_0x0048
            r2 = 1
            goto L_0x0077
        L_0x0048:
            com.google.zxing.NotFoundException r11 = com.google.zxing.NotFoundException.a()
            throw r11
        L_0x004d:
            r9 = -1
            if (r7 != r9) goto L_0x0064
            if (r8 == 0) goto L_0x005b
            if (r3 != 0) goto L_0x0056
        L_0x0054:
            r5 = 1
            goto L_0x0077
        L_0x0056:
            com.google.zxing.NotFoundException r11 = com.google.zxing.NotFoundException.a()
            throw r11
        L_0x005b:
            if (r3 == 0) goto L_0x005f
            r1 = 1
            goto L_0x0077
        L_0x005f:
            com.google.zxing.NotFoundException r11 = com.google.zxing.NotFoundException.a()
            throw r11
        L_0x0064:
            if (r7 != 0) goto L_0x00b1
            if (r8 == 0) goto L_0x0075
            if (r3 == 0) goto L_0x0070
            if (r11 >= r0) goto L_0x006e
            r2 = 1
            goto L_0x0054
        L_0x006e:
            r1 = 1
            goto L_0x003d
        L_0x0070:
            com.google.zxing.NotFoundException r11 = com.google.zxing.NotFoundException.a()
            throw r11
        L_0x0075:
            if (r3 != 0) goto L_0x00ac
        L_0x0077:
            if (r5 == 0) goto L_0x0088
            if (r6 != 0) goto L_0x0083
            int[] r11 = r10.e
            float[] r0 = r10.c
            a(r11, r0)
            goto L_0x0088
        L_0x0083:
            com.google.zxing.NotFoundException r11 = com.google.zxing.NotFoundException.a()
            throw r11
        L_0x0088:
            if (r6 == 0) goto L_0x0091
            int[] r11 = r10.e
            float[] r0 = r10.c
            b(r11, r0)
        L_0x0091:
            if (r1 == 0) goto L_0x00a2
            if (r2 != 0) goto L_0x009d
            int[] r11 = r10.f
            float[] r0 = r10.c
            a(r11, r0)
            goto L_0x00a2
        L_0x009d:
            com.google.zxing.NotFoundException r11 = com.google.zxing.NotFoundException.a()
            throw r11
        L_0x00a2:
            if (r2 == 0) goto L_0x00ab
            int[] r11 = r10.f
            float[] r0 = r10.d
            b(r11, r0)
        L_0x00ab:
            return
        L_0x00ac:
            com.google.zxing.NotFoundException r11 = com.google.zxing.NotFoundException.a()
            throw r11
        L_0x00b1:
            com.google.zxing.NotFoundException r11 = com.google.zxing.NotFoundException.a()
            goto L_0x00b7
        L_0x00b6:
            throw r11
        L_0x00b7:
            goto L_0x00b6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.d.a.a.d.a(int):void");
    }
}
