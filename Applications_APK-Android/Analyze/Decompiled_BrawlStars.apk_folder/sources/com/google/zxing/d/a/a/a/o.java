package com.google.zxing.d.a.a.a;

/* compiled from: DecodedInformation */
final class o extends q {

    /* renamed from: a  reason: collision with root package name */
    final String f3005a;

    /* renamed from: b  reason: collision with root package name */
    final int f3006b;
    final boolean c;

    o(int i, String str) {
        super(i);
        this.f3005a = str;
        this.c = false;
        this.f3006b = 0;
    }

    o(int i, String str, int i2) {
        super(i);
        this.c = true;
        this.f3006b = i2;
        this.f3005a = str;
    }
}
