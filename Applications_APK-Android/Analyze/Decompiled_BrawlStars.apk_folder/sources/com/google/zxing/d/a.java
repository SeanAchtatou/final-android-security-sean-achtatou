package com.google.zxing.d;

import android.support.v7.widget.ActivityChooserView;
import com.google.zxing.NotFoundException;
import com.google.zxing.d;
import com.google.zxing.n;
import com.google.zxing.p;
import java.util.Arrays;
import java.util.Map;

/* compiled from: CodaBarReader */
public final class a extends r {

    /* renamed from: a  reason: collision with root package name */
    static final char[] f2992a = "0123456789-$:/.+ABCD".toCharArray();

    /* renamed from: b  reason: collision with root package name */
    static final int[] f2993b = {3, 6, 9, 96, 18, 66, 33, 36, 48, 72, 12, 24, 69, 81, 84, 21, 26, 41, 11, 14};
    private static final char[] c = {'A', 'B', 'C', 'D'};
    private final StringBuilder d = new StringBuilder(20);
    private int[] e = new int[80];
    private int f = 0;

    public final n a(int i, com.google.zxing.common.a aVar, Map<d, ?> map) throws NotFoundException {
        com.google.zxing.common.a aVar2 = aVar;
        Map<d, ?> map2 = map;
        Arrays.fill(this.e, 0);
        this.f = 0;
        int d2 = aVar2.d(0);
        int i2 = aVar2.f2965b;
        if (d2 < i2) {
            int i3 = 0;
            boolean z = true;
            while (d2 < i2) {
                if (aVar2.a(d2) != z) {
                    i3++;
                } else {
                    a(i3);
                    z = !z;
                    i3 = 1;
                }
                d2++;
            }
            a(i3);
            int i4 = 1;
            while (i4 < this.f) {
                int b2 = b(i4);
                if (b2 != -1 && a(c, f2992a[b2])) {
                    int i5 = 0;
                    for (int i6 = i4; i6 < i4 + 7; i6++) {
                        i5 += this.e[i6];
                    }
                    if (i4 == 1 || this.e[i4 - 1] >= i5 / 2) {
                        this.d.setLength(0);
                        int i7 = i4;
                        while (true) {
                            int b3 = b(i7);
                            if (b3 != -1) {
                                this.d.append((char) b3);
                                i7 += 8;
                                if ((this.d.length() <= 1 || !a(c, f2992a[b3])) && i7 < this.f) {
                                }
                            } else {
                                throw NotFoundException.a();
                            }
                        }
                        int i8 = i7 - 1;
                        int i9 = this.e[i8];
                        int i10 = 0;
                        for (int i11 = -8; i11 < -1; i11++) {
                            i10 += this.e[i7 + i11];
                        }
                        if (i7 >= this.f || i9 >= i10 / 2) {
                            int[] iArr = {0, 0, 0, 0};
                            int[] iArr2 = {0, 0, 0, 0};
                            int length = this.d.length() - 1;
                            int i12 = i4;
                            int i13 = 0;
                            while (true) {
                                int i14 = f2993b[this.d.charAt(i13)];
                                for (int i15 = 6; i15 >= 0; i15--) {
                                    int i16 = (i15 & 1) + ((i14 & 1) << 1);
                                    iArr[i16] = iArr[i16] + this.e[i12 + i15];
                                    iArr2[i16] = iArr2[i16] + 1;
                                    i14 >>= 1;
                                }
                                if (i13 >= length) {
                                    break;
                                }
                                i12 += 8;
                                i13++;
                            }
                            float[] fArr = new float[4];
                            float[] fArr2 = new float[4];
                            int i17 = 0;
                            for (int i18 = 2; i17 < i18; i18 = 2) {
                                fArr2[i17] = 0.0f;
                                int i19 = i17 + 2;
                                fArr2[i19] = ((((float) iArr[i17]) / ((float) iArr2[i17])) + (((float) iArr[i19]) / ((float) iArr2[i19]))) / 2.0f;
                                fArr[i17] = fArr2[i19];
                                fArr[i19] = ((((float) iArr[i19]) * 2.0f) + 1.5f) / ((float) iArr2[i19]);
                                i17++;
                            }
                            int i20 = i4;
                            int i21 = 0;
                            loop8:
                            while (true) {
                                int i22 = f2993b[this.d.charAt(i21)];
                                int i23 = 6;
                                while (i23 >= 0) {
                                    int i24 = (i23 & 1) + ((i22 & 1) << 1);
                                    float f2 = (float) this.e[i20 + i23];
                                    if (f2 >= fArr2[i24] && f2 <= fArr[i24]) {
                                        i22 >>= 1;
                                        i23--;
                                    }
                                }
                                if (i21 < length) {
                                    i20 += 8;
                                    i21++;
                                } else {
                                    for (int i25 = 0; i25 < this.d.length(); i25++) {
                                        StringBuilder sb = this.d;
                                        sb.setCharAt(i25, f2992a[sb.charAt(i25)]);
                                    }
                                    if (a(c, this.d.charAt(0))) {
                                        StringBuilder sb2 = this.d;
                                        if (!a(c, sb2.charAt(sb2.length() - 1))) {
                                            throw NotFoundException.a();
                                        } else if (this.d.length() > 3) {
                                            if (map2 == null || !map2.containsKey(d.RETURN_CODABAR_START_END)) {
                                                StringBuilder sb3 = this.d;
                                                sb3.deleteCharAt(sb3.length() - 1);
                                                this.d.deleteCharAt(0);
                                            }
                                            int i26 = 0;
                                            for (int i27 = 0; i27 < i4; i27++) {
                                                i26 += this.e[i27];
                                            }
                                            float f3 = (float) i26;
                                            while (i4 < i8) {
                                                i26 += this.e[i4];
                                                i4++;
                                            }
                                            float f4 = (float) i;
                                            return new n(this.d.toString(), null, new p[]{new p(f3, f4), new p((float) i26, f4)}, com.google.zxing.a.CODABAR);
                                        } else {
                                            throw NotFoundException.a();
                                        }
                                    } else {
                                        throw NotFoundException.a();
                                    }
                                }
                            }
                            throw NotFoundException.a();
                        }
                        throw NotFoundException.a();
                    }
                }
                i4 += 2;
            }
            throw NotFoundException.a();
        }
        throw NotFoundException.a();
    }

    private void a(int i) {
        int[] iArr = this.e;
        int i2 = this.f;
        iArr[i2] = i;
        this.f = i2 + 1;
        int i3 = this.f;
        if (i3 >= iArr.length) {
            int[] iArr2 = new int[(i3 << 1)];
            System.arraycopy(iArr, 0, iArr2, 0, i3);
            this.e = iArr2;
        }
    }

    static boolean a(char[] cArr, char c2) {
        if (cArr != null) {
            for (char c3 : cArr) {
                if (c3 == c2) {
                    return true;
                }
            }
        }
        return false;
    }

    private int b(int i) {
        int i2 = i + 7;
        if (i2 >= this.f) {
            return -1;
        }
        int[] iArr = this.e;
        int i3 = ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
        int i4 = 0;
        int i5 = ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
        int i6 = 0;
        for (int i7 = i; i7 < i2; i7 += 2) {
            int i8 = iArr[i7];
            if (i8 < i5) {
                i5 = i8;
            }
            if (i8 > i6) {
                i6 = i8;
            }
        }
        int i9 = (i5 + i6) / 2;
        int i10 = 0;
        for (int i11 = i + 1; i11 < i2; i11 += 2) {
            int i12 = iArr[i11];
            if (i12 < i3) {
                i3 = i12;
            }
            if (i12 > i10) {
                i10 = i12;
            }
        }
        int i13 = (i3 + i10) / 2;
        int i14 = 128;
        int i15 = 0;
        for (int i16 = 0; i16 < 7; i16++) {
            i14 >>= 1;
            if (iArr[i + i16] > ((i16 & 1) == 0 ? i9 : i13)) {
                i15 |= i14;
            }
        }
        while (true) {
            int[] iArr2 = f2993b;
            if (i4 >= iArr2.length) {
                return -1;
            }
            if (iArr2[i4] == i15) {
                return i4;
            }
            i4++;
        }
    }
}
