package com.google.zxing.d.a.a.a;

import com.google.zxing.common.a;

/* compiled from: AI01320xDecoder */
final class b extends f {
    /* access modifiers changed from: protected */
    public final int a(int i) {
        return i < 10000 ? i : i - 10000;
    }

    b(a aVar) {
        super(aVar);
    }

    /* access modifiers changed from: protected */
    public final void a(StringBuilder sb, int i) {
        if (i < 10000) {
            sb.append("(3202)");
        } else {
            sb.append("(3203)");
        }
    }
}
