package com.google.zxing.d.a.a;

import java.util.List;

/* compiled from: BitArrayBuilder */
final class a {
    static com.google.zxing.common.a a(List<b> list) {
        int size = (list.size() << 1) - 1;
        if (list.get(list.size() - 1).f3014b == null) {
            size--;
        }
        com.google.zxing.common.a aVar = new com.google.zxing.common.a(size * 12);
        int i = list.get(0).f3014b.f3017a;
        int i2 = 0;
        for (int i3 = 11; i3 >= 0; i3--) {
            if (((1 << i3) & i) != 0) {
                aVar.b(i2);
            }
            i2++;
        }
        for (int i4 = 1; i4 < list.size(); i4++) {
            b bVar = list.get(i4);
            int i5 = bVar.f3013a.f3017a;
            int i6 = i2;
            for (int i7 = 11; i7 >= 0; i7--) {
                if (((1 << i7) & i5) != 0) {
                    aVar.b(i6);
                }
                i6++;
            }
            if (bVar.f3014b != null) {
                int i8 = bVar.f3014b.f3017a;
                for (int i9 = 11; i9 >= 0; i9--) {
                    if (((1 << i9) & i8) != 0) {
                        aVar.b(i6);
                    }
                    i6++;
                }
            }
            i2 = i6;
        }
        return aVar;
    }
}
