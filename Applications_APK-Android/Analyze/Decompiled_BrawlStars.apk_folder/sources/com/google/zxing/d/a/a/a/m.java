package com.google.zxing.d.a.a.a;

/* compiled from: CurrentParsingState */
final class m {

    /* renamed from: a  reason: collision with root package name */
    int f3000a = 0;

    /* renamed from: b  reason: collision with root package name */
    a f3001b = a.NUMERIC;

    /* compiled from: CurrentParsingState */
    enum a {
        NUMERIC,
        ALPHA,
        ISO_IEC_646
    }

    m() {
    }

    /* access modifiers changed from: package-private */
    public final void a(int i) {
        this.f3000a += i;
    }

    /* access modifiers changed from: package-private */
    public final boolean a() {
        return this.f3001b == a.ALPHA;
    }

    /* access modifiers changed from: package-private */
    public final boolean b() {
        return this.f3001b == a.ISO_IEC_646;
    }
}
