package com.google.zxing.d;

import com.google.zxing.FormatException;
import com.google.zxing.WriterException;
import com.google.zxing.a;
import com.google.zxing.common.b;
import com.google.zxing.f;
import java.util.Map;

/* compiled from: EAN13Writer */
public final class j extends z {
    public final b a(String str, a aVar, int i, int i2, Map<f, ?> map) throws WriterException {
        if (aVar == a.EAN_13) {
            return super.a(str, aVar, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode EAN_13, but got " + aVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.zxing.d.s.a(boolean[], int, int[], boolean):int
     arg types: [boolean[], int, int[], int]
     candidates:
      com.google.zxing.d.s.a(boolean[], int, int, int):com.google.zxing.common.b
      com.google.zxing.d.s.a(boolean[], int, int[], boolean):int */
    public final boolean[] a(String str) {
        int length = str.length();
        if (length == 12) {
            try {
                str = str + y.b(str);
            } catch (FormatException e) {
                throw new IllegalArgumentException(e);
            }
        } else if (length == 13) {
            try {
                if (!y.a((CharSequence) str)) {
                    throw new IllegalArgumentException("Contents do not pass checksum");
                }
            } catch (FormatException unused) {
                throw new IllegalArgumentException("Illegal contents");
            }
        } else {
            throw new IllegalArgumentException("Requested contents should be 12 or 13 digits long, but got " + length);
        }
        int i = i.f3031a[Character.digit(str.charAt(0), 10)];
        boolean[] zArr = new boolean[95];
        int a2 = a(zArr, 0, y.f3049b, true) + 0;
        for (int i2 = 1; i2 <= 6; i2++) {
            int digit = Character.digit(str.charAt(i2), 10);
            if (((i >> (6 - i2)) & 1) == 1) {
                digit += 10;
            }
            a2 += a(zArr, a2, y.f[digit], false);
        }
        int a3 = a2 + a(zArr, a2, y.c, false);
        for (int i3 = 7; i3 <= 12; i3++) {
            a3 += a(zArr, a3, y.e[Character.digit(str.charAt(i3), 10)], true);
        }
        a(zArr, a3, y.f3049b, true);
        return zArr;
    }
}
