package com.google.zxing.d.a.a;

import java.util.ArrayList;
import java.util.List;

/* compiled from: ExpandedRow */
final class c {

    /* renamed from: a  reason: collision with root package name */
    final List<b> f3015a;

    /* renamed from: b  reason: collision with root package name */
    final int f3016b;
    private final boolean c;

    c(List<b> list, int i, boolean z) {
        this.f3015a = new ArrayList(list);
        this.f3016b = i;
        this.c = z;
    }

    /* access modifiers changed from: package-private */
    public final boolean a(List<b> list) {
        return this.f3015a.equals(list);
    }

    public final String toString() {
        return "{ " + this.f3015a + " }";
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof c)) {
            return false;
        }
        c cVar = (c) obj;
        if (!this.f3015a.equals(cVar.f3015a) || this.c != cVar.c) {
            return false;
        }
        return true;
    }

    public final int hashCode() {
        return this.f3015a.hashCode() ^ Boolean.valueOf(this.c).hashCode();
    }
}
