package com.google.zxing.d.a.a.a;

import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.common.a;

/* compiled from: AI01393xDecoder */
final class d extends h {
    d(a aVar) {
        super(aVar);
    }

    public final String a() throws NotFoundException, FormatException {
        if (this.f2996a.f2965b >= 48) {
            StringBuilder sb = new StringBuilder();
            b(sb, 8);
            int a2 = this.f2997b.a(48, 2);
            sb.append("(393");
            sb.append(a2);
            sb.append(')');
            int a3 = this.f2997b.a(50, 10);
            if (a3 / 100 == 0) {
                sb.append('0');
            }
            if (a3 / 10 == 0) {
                sb.append('0');
            }
            sb.append(a3);
            sb.append(this.f2997b.a(60, (String) null).f3005a);
            return sb.toString();
        }
        throw NotFoundException.a();
    }
}
