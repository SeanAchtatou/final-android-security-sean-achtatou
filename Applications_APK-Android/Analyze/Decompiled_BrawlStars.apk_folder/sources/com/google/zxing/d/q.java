package com.google.zxing.d;

import com.google.zxing.NotFoundException;
import com.google.zxing.ReaderException;
import com.google.zxing.a;
import com.google.zxing.d;
import com.google.zxing.n;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

/* compiled from: MultiFormatUPCEANReader */
public final class q extends r {

    /* renamed from: a  reason: collision with root package name */
    private final y[] f3040a;

    public q(Map<d, ?> map) {
        Collection collection;
        if (map == null) {
            collection = null;
        } else {
            collection = (Collection) map.get(d.POSSIBLE_FORMATS);
        }
        ArrayList arrayList = new ArrayList();
        if (collection != null) {
            if (collection.contains(a.EAN_13)) {
                arrayList.add(new i());
            } else if (collection.contains(a.UPC_A)) {
                arrayList.add(new t());
            }
            if (collection.contains(a.EAN_8)) {
                arrayList.add(new k());
            }
            if (collection.contains(a.UPC_E)) {
                arrayList.add(new aa());
            }
        }
        if (arrayList.isEmpty()) {
            arrayList.add(new i());
            arrayList.add(new k());
            arrayList.add(new aa());
        }
        this.f3040a = (y[]) arrayList.toArray(new y[arrayList.size()]);
    }

    public final n a(int i, com.google.zxing.common.a aVar, Map<d, ?> map) throws NotFoundException {
        Collection collection;
        boolean z;
        int[] a2 = y.a(aVar);
        y[] yVarArr = this.f3040a;
        int i2 = 0;
        while (i2 < yVarArr.length) {
            try {
                n a3 = yVarArr[i2].a(i, aVar, a2, map);
                boolean z2 = a3.d == a.EAN_13 && a3.f3149a.charAt(0) == '0';
                if (map == null) {
                    collection = null;
                } else {
                    collection = (Collection) map.get(d.POSSIBLE_FORMATS);
                }
                if (collection != null) {
                    if (!collection.contains(a.UPC_A)) {
                        z = false;
                        if (z2 || !z) {
                            return a3;
                        }
                        n nVar = new n(a3.f3149a.substring(1), a3.f3150b, a3.c, a.UPC_A);
                        nVar.a(a3.e);
                        return nVar;
                    }
                }
                z = true;
                if (z2) {
                }
                return a3;
            } catch (ReaderException unused) {
                i2++;
            }
        }
        throw NotFoundException.a();
    }

    public final void a() {
        for (y a2 : this.f3040a) {
            a2.a();
        }
    }
}
