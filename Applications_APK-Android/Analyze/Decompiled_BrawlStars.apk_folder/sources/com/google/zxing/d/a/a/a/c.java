package com.google.zxing.d.a.a.a;

import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.common.a;

/* compiled from: AI01392xDecoder */
final class c extends h {
    c(a aVar) {
        super(aVar);
    }

    public final String a() throws NotFoundException, FormatException {
        if (this.f2996a.f2965b >= 48) {
            StringBuilder sb = new StringBuilder();
            b(sb, 8);
            int a2 = this.f2997b.a(48, 2);
            sb.append("(392");
            sb.append(a2);
            sb.append(')');
            sb.append(this.f2997b.a(50, (String) null).f3005a);
            return sb.toString();
        }
        throw NotFoundException.a();
    }
}
