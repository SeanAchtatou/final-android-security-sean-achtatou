package com.google.zxing.d;

import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.ReaderException;
import com.google.zxing.c;
import com.google.zxing.common.a;
import com.google.zxing.d;
import com.google.zxing.m;
import com.google.zxing.n;
import com.google.zxing.o;
import com.google.zxing.p;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.Map;

/* compiled from: OneDReader */
public abstract class r implements m {
    public abstract n a(int i, a aVar, Map<d, ?> map) throws NotFoundException, ChecksumException, FormatException;

    public void a() {
    }

    public n a(c cVar) throws NotFoundException, FormatException {
        return a(cVar, null);
    }

    public n a(c cVar, Map<d, ?> map) throws NotFoundException, FormatException {
        try {
            return b(cVar, map);
        } catch (NotFoundException e) {
            if (!(map != null && map.containsKey(d.TRY_HARDER)) || !cVar.f2937a.f2904a.b()) {
                throw e;
            }
            c cVar2 = new c(cVar.f2937a.a(cVar.f2937a.f2904a.d()));
            n b2 = b(cVar2, map);
            Map<o, Object> map2 = b2.e;
            int i = 270;
            if (map2 != null && map2.containsKey(o.ORIENTATION)) {
                i = (((Integer) map2.get(o.ORIENTATION)).intValue() + 270) % 360;
            }
            b2.a(o.ORIENTATION, Integer.valueOf(i));
            p[] pVarArr = b2.c;
            if (pVarArr != null) {
                int a2 = cVar2.a();
                for (int i2 = 0; i2 < pVarArr.length; i2++) {
                    pVarArr[i2] = new p((((float) a2) - pVarArr[i2].f3154b) - 1.0f, pVarArr[i2].f3153a);
                }
            }
            return b2;
        }
    }

    protected static void a(a aVar, int i, int[] iArr) throws NotFoundException {
        int length = iArr.length;
        int i2 = 0;
        Arrays.fill(iArr, 0, length, 0);
        int i3 = aVar.f2965b;
        if (i < i3) {
            boolean z = !aVar.a(i);
            while (i < i3) {
                if (aVar.a(i) == z) {
                    i2++;
                    if (i2 == length) {
                        break;
                    }
                    iArr[i2] = 1;
                    z = !z;
                } else {
                    iArr[i2] = iArr[i2] + 1;
                }
                i++;
            }
            if (i2 == length) {
                return;
            }
            if (i2 != length - 1 || i != i3) {
                throw NotFoundException.a();
            }
            return;
        }
        throw NotFoundException.a();
    }

    protected static void b(a aVar, int i, int[] iArr) throws NotFoundException {
        int length = iArr.length;
        boolean a2 = aVar.a(i);
        while (i > 0 && length >= 0) {
            i--;
            if (aVar.a(i) != a2) {
                length--;
                a2 = !a2;
            }
        }
        if (length < 0) {
            a(aVar, i + 1, iArr);
            return;
        }
        throw NotFoundException.a();
    }

    public static float a(int[] iArr, int[] iArr2, float f) {
        int length = iArr.length;
        int i = 0;
        int i2 = 0;
        for (int i3 = 0; i3 < length; i3++) {
            i += iArr[i3];
            i2 += iArr2[i3];
        }
        if (i < i2) {
            return Float.POSITIVE_INFINITY;
        }
        float f2 = (float) i;
        float f3 = f2 / ((float) i2);
        float f4 = f * f3;
        float f5 = 0.0f;
        for (int i4 = 0; i4 < length; i4++) {
            int i5 = iArr[i4];
            float f6 = ((float) iArr2[i4]) * f3;
            float f7 = (float) i5;
            float f8 = f7 > f6 ? f7 - f6 : f6 - f7;
            if (f8 > f4) {
                return Float.POSITIVE_INFINITY;
            }
            f5 += f8;
        }
        return f5 / f2;
    }

    private n b(c cVar, Map<d, ?> map) throws NotFoundException {
        int i;
        a aVar;
        int i2;
        c cVar2 = cVar;
        Map<d, ?> map2 = map;
        int i3 = cVar2.f2937a.f2904a.f3144a;
        int a2 = cVar.a();
        a aVar2 = new a(i3);
        int i4 = 1;
        boolean z = map2 != null && map2.containsKey(d.TRY_HARDER);
        int max = Math.max(1, a2 >> (z ? 8 : 5));
        int i5 = z ? a2 : 15;
        int i6 = a2 / 2;
        EnumMap enumMap = map2;
        int i7 = 0;
        while (i7 < i5) {
            int i8 = i7 + 1;
            int i9 = i8 / 2;
            if (!((i7 & 1) == 0)) {
                i9 = -i9;
            }
            int i10 = (i9 * max) + i6;
            if (i10 < 0 || i10 >= a2) {
                break;
            }
            try {
                a a3 = cVar2.f2937a.a(i10, aVar2);
                int i11 = 0;
                while (i11 < 2) {
                    if (i11 == i4) {
                        a3.c();
                        if (enumMap != null && enumMap.containsKey(d.NEED_RESULT_POINT_CALLBACK)) {
                            EnumMap enumMap2 = new EnumMap(d.class);
                            enumMap2.putAll(enumMap);
                            enumMap2.remove(d.NEED_RESULT_POINT_CALLBACK);
                            enumMap = enumMap2;
                        }
                    }
                    try {
                        n a4 = a(i10, a3, enumMap);
                        if (i11 == i4) {
                            a4.a(o.ORIENTATION, 180);
                            p[] pVarArr = a4.c;
                            if (pVarArr != null) {
                                float f = (float) i3;
                                aVar = a3;
                                try {
                                    i2 = i3;
                                } catch (ReaderException unused) {
                                    i2 = i3;
                                    i11++;
                                    a3 = aVar;
                                    i3 = i2;
                                    i4 = 1;
                                }
                                try {
                                    pVarArr[0] = new p((f - pVarArr[0].f3153a) - 1.0f, pVarArr[0].f3154b);
                                    try {
                                        pVarArr[1] = new p((f - pVarArr[1].f3153a) - 1.0f, pVarArr[1].f3154b);
                                    } catch (ReaderException unused2) {
                                        continue;
                                    }
                                } catch (ReaderException unused3) {
                                    i11++;
                                    a3 = aVar;
                                    i3 = i2;
                                    i4 = 1;
                                }
                            }
                        }
                        return a4;
                    } catch (ReaderException unused4) {
                        aVar = a3;
                        i2 = i3;
                        i11++;
                        a3 = aVar;
                        i3 = i2;
                        i4 = 1;
                    }
                }
                i = i3;
                aVar2 = a3;
            } catch (NotFoundException unused5) {
                i = i3;
            }
            cVar2 = cVar;
            i7 = i8;
            i3 = i;
            i4 = 1;
        }
        throw NotFoundException.a();
    }
}
