package com.google.zxing.d.a.a.a;

import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.common.a;

/* compiled from: AI01AndOtherAIs */
final class g extends h {
    g(a aVar) {
        super(aVar);
    }

    public final String a() throws NotFoundException, FormatException {
        StringBuilder sb = new StringBuilder();
        sb.append("(01)");
        int length = sb.length();
        sb.append(this.f2997b.a(4, 4));
        a(sb, 8, length);
        return this.f2997b.a(sb, 48);
    }
}
