package com.google.zxing.d;

import com.google.zxing.NotFoundException;
import com.google.zxing.common.a;
import com.google.zxing.n;
import com.google.zxing.o;
import com.google.zxing.p;
import java.util.EnumMap;

/* compiled from: UPCEANExtension2Support */
final class v {

    /* renamed from: a  reason: collision with root package name */
    private final int[] f3043a = new int[4];

    /* renamed from: b  reason: collision with root package name */
    private final StringBuilder f3044b = new StringBuilder();

    v() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.google.zxing.o, java.lang.Integer]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    /* access modifiers changed from: package-private */
    public final n a(int i, a aVar, int[] iArr) throws NotFoundException {
        EnumMap enumMap;
        a aVar2 = aVar;
        StringBuilder sb = this.f3044b;
        sb.setLength(0);
        int[] iArr2 = this.f3043a;
        iArr2[0] = 0;
        iArr2[1] = 0;
        iArr2[2] = 0;
        iArr2[3] = 0;
        int i2 = aVar2.f2965b;
        int i3 = iArr[1];
        int i4 = 0;
        int i5 = 0;
        while (i4 < 2 && i3 < i2) {
            int a2 = y.a(aVar2, iArr2, i3, y.f);
            sb.append((char) ((a2 % 10) + 48));
            int i6 = i3;
            for (int i7 : iArr2) {
                i6 += i7;
            }
            if (a2 >= 10) {
                i5 = (1 << (1 - i4)) | i5;
            }
            i3 = i4 != 1 ? aVar2.d(aVar2.c(i6)) : i6;
            i4++;
        }
        if (sb.length() != 2) {
            throw NotFoundException.a();
        } else if (Integer.parseInt(sb.toString()) % 4 == i5) {
            String sb2 = sb.toString();
            if (sb2.length() != 2) {
                enumMap = null;
            } else {
                enumMap = new EnumMap(o.class);
                enumMap.put((Object) o.ISSUE_NUMBER, (Object) Integer.valueOf(sb2));
            }
            float f = (float) i;
            n nVar = new n(sb2, null, new p[]{new p(((float) (iArr[0] + iArr[1])) / 2.0f, f), new p((float) i3, f)}, com.google.zxing.a.UPC_EAN_EXTENSION);
            if (enumMap != null) {
                nVar.a(enumMap);
            }
            return nVar;
        } else {
            throw NotFoundException.a();
        }
    }
}
