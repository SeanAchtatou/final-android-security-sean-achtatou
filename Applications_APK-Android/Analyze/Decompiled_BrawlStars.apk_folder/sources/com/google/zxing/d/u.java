package com.google.zxing.d;

import com.facebook.appevents.AppEventsConstants;
import com.google.zxing.WriterException;
import com.google.zxing.a;
import com.google.zxing.common.b;
import com.google.zxing.f;
import com.google.zxing.r;
import java.util.Map;

/* compiled from: UPCAWriter */
public final class u implements r {

    /* renamed from: a  reason: collision with root package name */
    private final j f3042a = new j();

    public final b a(String str, a aVar, int i, int i2, Map<f, ?> map) throws WriterException {
        if (aVar == a.UPC_A) {
            j jVar = this.f3042a;
            return jVar.a(AppEventsConstants.EVENT_PARAM_VALUE_NO + str, a.EAN_13, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode UPC-A, but got " + aVar);
    }
}
