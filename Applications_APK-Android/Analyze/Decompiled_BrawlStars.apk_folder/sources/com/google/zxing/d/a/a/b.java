package com.google.zxing.d.a.a;

import com.google.zxing.d.a.c;

/* compiled from: ExpandedPair */
final class b {

    /* renamed from: a  reason: collision with root package name */
    final com.google.zxing.d.a.b f3013a;

    /* renamed from: b  reason: collision with root package name */
    final com.google.zxing.d.a.b f3014b;
    final c c;
    private final boolean d = true;

    b(com.google.zxing.d.a.b bVar, com.google.zxing.d.a.b bVar2, c cVar, boolean z) {
        this.f3013a = bVar;
        this.f3014b = bVar2;
        this.c = cVar;
    }

    public final String toString() {
        Object obj;
        StringBuilder sb = new StringBuilder("[ ");
        sb.append(this.f3013a);
        sb.append(" , ");
        sb.append(this.f3014b);
        sb.append(" : ");
        c cVar = this.c;
        if (cVar == null) {
            obj = "null";
        } else {
            obj = Integer.valueOf(cVar.f3019a);
        }
        sb.append(obj);
        sb.append(" ]");
        return sb.toString();
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        if (!a(this.f3013a, bVar.f3013a) || !a(this.f3014b, bVar.f3014b) || !a(this.c, bVar.c)) {
            return false;
        }
        return true;
    }

    private static boolean a(Object obj, Object obj2) {
        if (obj == null) {
            return obj2 == null;
        }
        return obj.equals(obj2);
    }

    public final int hashCode() {
        return (a(this.f3013a) ^ a(this.f3014b)) ^ a(this.c);
    }

    private static int a(Object obj) {
        if (obj == null) {
            return 0;
        }
        return obj.hashCode();
    }
}
