package com.google.zxing.d;

import android.support.v7.widget.ActivityChooserView;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.common.a;
import com.google.zxing.d;
import com.google.zxing.n;
import com.google.zxing.p;
import java.util.Arrays;
import java.util.Map;

/* compiled from: Code39Reader */
public final class e extends r {

    /* renamed from: a  reason: collision with root package name */
    static final int[] f3027a = {52, 289, 97, 352, 49, 304, 112, 37, 292, 100, 265, 73, 328, 25, 280, 88, 13, 268, 76, 28, 259, 67, 322, 19, 274, 82, 7, 262, 70, 22, 385, 193, 448, 145, 400, 208, 133, 388, 196, 168, 162, 138, 42};

    /* renamed from: b  reason: collision with root package name */
    private final boolean f3028b;
    private final boolean c;
    private final StringBuilder d;
    private final int[] e;

    public e() {
        this(false);
    }

    public e(boolean z) {
        this(z, false);
    }

    private e(boolean z, boolean z2) {
        this.f3028b = z;
        this.c = false;
        this.d = new StringBuilder(20);
        this.e = new int[9];
    }

    public final n a(int i, a aVar, Map<d, ?> map) throws NotFoundException, ChecksumException, FormatException {
        char c2;
        char c3;
        String str;
        char c4;
        int i2;
        a aVar2 = aVar;
        int[] iArr = this.e;
        Arrays.fill(iArr, 0);
        StringBuilder sb = this.d;
        sb.setLength(0);
        int i3 = aVar2.f2965b;
        int c5 = aVar2.c(0);
        int length = iArr.length;
        int i4 = c5;
        boolean z = false;
        int i5 = 0;
        while (c5 < i3) {
            if (aVar2.a(c5) != z) {
                iArr[i5] = iArr[i5] + 1;
            } else {
                if (i5 != length - 1) {
                    i5++;
                } else if (a(iArr) != 148 || !aVar2.a(Math.max(0, i4 - ((c5 - i4) / 2)), i4, false)) {
                    i4 += iArr[0] + iArr[1];
                    int i6 = i5 - 1;
                    System.arraycopy(iArr, 2, iArr, 0, i6);
                    iArr[i6] = 0;
                    iArr[i5] = 0;
                    i5--;
                } else {
                    int[] iArr2 = {i4, c5};
                    int c6 = aVar2.c(iArr2[1]);
                    int i7 = aVar2.f2965b;
                    while (true) {
                        a(aVar2, c6, iArr);
                        int a2 = a(iArr);
                        if (a2 >= 0) {
                            int i8 = 0;
                            while (true) {
                                int[] iArr3 = f3027a;
                                if (i8 < iArr3.length) {
                                    if (iArr3[i8] == a2) {
                                        c2 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%".charAt(i8);
                                        break;
                                    }
                                    i8++;
                                } else if (a2 == 148) {
                                    c2 = '*';
                                } else {
                                    throw NotFoundException.a();
                                }
                            }
                            sb.append(c2);
                            int i9 = c6;
                            for (int i10 : iArr) {
                                i9 += i10;
                            }
                            int c7 = aVar2.c(i9);
                            if (c2 == '*') {
                                sb.setLength(sb.length() - 1);
                                int i11 = 0;
                                for (int i12 : iArr) {
                                    i11 += i12;
                                }
                                int i13 = (c7 - c6) - i11;
                                if (c7 == i7 || (i13 << 1) >= i11) {
                                    if (this.f3028b) {
                                        int length2 = sb.length() - 1;
                                        int i14 = 0;
                                        for (int i15 = 0; i15 < length2; i15++) {
                                            i14 += "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%".indexOf(this.d.charAt(i15));
                                        }
                                        if (sb.charAt(length2) == "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%".charAt(i14 % 43)) {
                                            sb.setLength(length2);
                                        } else {
                                            throw ChecksumException.a();
                                        }
                                    }
                                    if (sb.length() != 0) {
                                        if (this.c) {
                                            int length3 = sb.length();
                                            StringBuilder sb2 = new StringBuilder(length3);
                                            int i16 = 0;
                                            while (i16 < length3) {
                                                char charAt = sb.charAt(i16);
                                                if (charAt == '+' || charAt == '$' || charAt == '%' || charAt == '/') {
                                                    i16++;
                                                    char charAt2 = sb.charAt(i16);
                                                    if (charAt != '$') {
                                                        if (charAt != '%') {
                                                            if (charAt != '+') {
                                                                if (charAt == '/') {
                                                                    if (charAt2 >= 'A' && charAt2 <= 'O') {
                                                                        i2 = charAt2 - ' ';
                                                                    } else if (charAt2 == 'Z') {
                                                                        c4 = ':';
                                                                        sb2.append(c4);
                                                                    } else {
                                                                        throw FormatException.a();
                                                                    }
                                                                }
                                                            } else if (charAt2 < 'A' || charAt2 > 'Z') {
                                                                throw FormatException.a();
                                                            } else {
                                                                i2 = charAt2 + ' ';
                                                            }
                                                        } else if (charAt2 >= 'A' && charAt2 <= 'E') {
                                                            i2 = charAt2 - '&';
                                                        } else if (charAt2 >= 'F' && charAt2 <= 'J') {
                                                            i2 = charAt2 - 11;
                                                        } else if (charAt2 >= 'K' && charAt2 <= 'O') {
                                                            i2 = charAt2 + 16;
                                                        } else if (charAt2 >= 'P' && charAt2 <= 'T') {
                                                            i2 = charAt2 + '+';
                                                        } else if (charAt2 != 'U') {
                                                            if (charAt2 == 'V') {
                                                                c4 = '@';
                                                            } else if (charAt2 == 'W') {
                                                                c4 = '`';
                                                            } else if (charAt2 == 'X' || charAt2 == 'Y' || charAt2 == 'Z') {
                                                                c4 = 127;
                                                            } else {
                                                                throw FormatException.a();
                                                            }
                                                            sb2.append(c4);
                                                        }
                                                        c4 = 0;
                                                        sb2.append(c4);
                                                    } else if (charAt2 < 'A' || charAt2 > 'Z') {
                                                        throw FormatException.a();
                                                    } else {
                                                        i2 = charAt2 - '@';
                                                    }
                                                    c4 = (char) i2;
                                                    sb2.append(c4);
                                                } else {
                                                    sb2.append(charAt);
                                                }
                                                i16++;
                                            }
                                            c3 = 1;
                                            str = sb2.toString();
                                        } else {
                                            c3 = 1;
                                            str = sb.toString();
                                        }
                                        float f = (float) i;
                                        return new n(str, null, new p[]{new p(((float) (iArr2[c3] + iArr2[0])) / 2.0f, f), new p(((float) c6) + (((float) i11) / 2.0f), f)}, com.google.zxing.a.CODE_39);
                                    }
                                    throw NotFoundException.a();
                                }
                                throw NotFoundException.a();
                            }
                            c6 = c7;
                        } else {
                            throw NotFoundException.a();
                        }
                    }
                }
                iArr[i5] = 1;
                z = !z;
            }
            c5++;
        }
        throw NotFoundException.a();
    }

    private static int a(int[] iArr) {
        int length = iArr.length;
        int i = 0;
        while (true) {
            int i2 = ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
            for (int i3 : iArr) {
                if (i3 < i2 && i3 > i) {
                    i2 = i3;
                }
            }
            int i4 = 0;
            int i5 = 0;
            int i6 = 0;
            for (int i7 = 0; i7 < length; i7++) {
                int i8 = iArr[i7];
                if (i8 > i2) {
                    i5 |= 1 << ((length - 1) - i7);
                    i4++;
                    i6 += i8;
                }
            }
            if (i4 == 3) {
                for (int i9 = 0; i9 < length && i4 > 0; i9++) {
                    int i10 = iArr[i9];
                    if (i10 > i2) {
                        i4--;
                        if ((i10 << 1) >= i6) {
                            return -1;
                        }
                    }
                }
                return i5;
            } else if (i4 <= 3) {
                return -1;
            } else {
                i = i2;
            }
        }
    }
}
