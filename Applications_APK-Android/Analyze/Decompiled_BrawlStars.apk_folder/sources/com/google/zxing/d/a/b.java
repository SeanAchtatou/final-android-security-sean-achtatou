package com.google.zxing.d.a;

/* compiled from: DataCharacter */
public class b {

    /* renamed from: a  reason: collision with root package name */
    public final int f3017a;

    /* renamed from: b  reason: collision with root package name */
    public final int f3018b;

    public b(int i, int i2) {
        this.f3017a = i;
        this.f3018b = i2;
    }

    public final String toString() {
        return this.f3017a + "(" + this.f3018b + ')';
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        if (this.f3017a == bVar.f3017a && this.f3018b == bVar.f3018b) {
            return true;
        }
        return false;
    }

    public final int hashCode() {
        return this.f3017a ^ this.f3018b;
    }
}
