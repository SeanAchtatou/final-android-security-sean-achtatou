package com.google.zxing.d.a;

import com.google.zxing.p;

/* compiled from: FinderPattern */
public final class c {

    /* renamed from: a  reason: collision with root package name */
    public final int f3019a;

    /* renamed from: b  reason: collision with root package name */
    public final int[] f3020b;
    public final p[] c;

    public c(int i, int[] iArr, int i2, int i3, int i4) {
        this.f3019a = i;
        this.f3020b = iArr;
        float f = (float) i4;
        this.c = new p[]{new p((float) i2, f), new p((float) i3, f)};
    }

    public final boolean equals(Object obj) {
        if ((obj instanceof c) && this.f3019a == ((c) obj).f3019a) {
            return true;
        }
        return false;
    }

    public final int hashCode() {
        return this.f3019a;
    }
}
