package com.google.zxing;

import com.google.zxing.common.a;

/* compiled from: Binarizer */
public abstract class b {

    /* renamed from: a  reason: collision with root package name */
    public final h f2904a;

    public abstract b a(h hVar);

    public abstract a a(int i, a aVar) throws NotFoundException;

    public abstract com.google.zxing.common.b a() throws NotFoundException;

    protected b(h hVar) {
        this.f2904a = hVar;
    }
}
