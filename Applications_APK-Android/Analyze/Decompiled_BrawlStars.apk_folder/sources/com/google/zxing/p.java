package com.google.zxing;

import com.google.zxing.common.a.a;

/* compiled from: ResultPoint */
public class p {

    /* renamed from: a  reason: collision with root package name */
    public final float f3153a;

    /* renamed from: b  reason: collision with root package name */
    public final float f3154b;

    public p(float f, float f2) {
        this.f3153a = f;
        this.f3154b = f2;
    }

    public final boolean equals(Object obj) {
        if (obj instanceof p) {
            p pVar = (p) obj;
            if (this.f3153a == pVar.f3153a && this.f3154b == pVar.f3154b) {
                return true;
            }
            return false;
        }
        return false;
    }

    public final int hashCode() {
        return (Float.floatToIntBits(this.f3153a) * 31) + Float.floatToIntBits(this.f3154b);
    }

    public final String toString() {
        return "(" + this.f3153a + ',' + this.f3154b + ')';
    }

    public static void a(p[] pVarArr) {
        p pVar;
        p pVar2;
        p pVar3;
        float a2 = a(pVarArr[0], pVarArr[1]);
        float a3 = a(pVarArr[1], pVarArr[2]);
        float a4 = a(pVarArr[0], pVarArr[2]);
        if (a3 >= a2 && a3 >= a4) {
            pVar3 = pVarArr[0];
            pVar2 = pVarArr[1];
            pVar = pVarArr[2];
        } else if (a4 < a3 || a4 < a2) {
            pVar3 = pVarArr[2];
            pVar2 = pVarArr[0];
            pVar = pVarArr[1];
        } else {
            pVar3 = pVarArr[1];
            pVar2 = pVarArr[0];
            pVar = pVarArr[2];
        }
        float f = pVar3.f3153a;
        float f2 = pVar3.f3154b;
        if (((pVar.f3153a - f) * (pVar2.f3154b - f2)) - ((pVar.f3154b - f2) * (pVar2.f3153a - f)) < 0.0f) {
            p pVar4 = pVar;
            pVar = pVar2;
            pVar2 = pVar4;
        }
        pVarArr[0] = pVar2;
        pVarArr[1] = pVar3;
        pVarArr[2] = pVar;
    }

    public static float a(p pVar, p pVar2) {
        return a.a(pVar.f3153a, pVar.f3154b, pVar2.f3153a, pVar2.f3154b);
    }
}
