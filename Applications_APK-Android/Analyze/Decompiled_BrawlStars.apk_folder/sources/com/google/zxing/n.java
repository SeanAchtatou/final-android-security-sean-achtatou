package com.google.zxing;

import java.util.EnumMap;
import java.util.Map;

/* compiled from: Result */
public final class n {

    /* renamed from: a  reason: collision with root package name */
    public final String f3149a;

    /* renamed from: b  reason: collision with root package name */
    public final byte[] f3150b;
    public p[] c;
    public final a d;
    public Map<o, Object> e;
    private final int f;
    private final long g;

    public n(String str, byte[] bArr, p[] pVarArr, a aVar) {
        this(str, bArr, pVarArr, aVar, System.currentTimeMillis());
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    private n(String str, byte[] bArr, p[] pVarArr, a aVar, long j) {
        this(str, bArr, bArr == null ? 0 : bArr.length * 8, pVarArr, aVar, j);
    }

    public n(String str, byte[] bArr, int i, p[] pVarArr, a aVar, long j) {
        this.f3149a = str;
        this.f3150b = bArr;
        this.f = i;
        this.c = pVarArr;
        this.d = aVar;
        this.e = null;
        this.g = j;
    }

    public final void a(o oVar, Object obj) {
        if (this.e == null) {
            this.e = new EnumMap(o.class);
        }
        this.e.put(oVar, obj);
    }

    public final void a(Map<o, Object> map) {
        if (map != null) {
            Map<o, Object> map2 = this.e;
            if (map2 == null) {
                this.e = map;
            } else {
                map2.putAll(map);
            }
        }
    }

    public final String toString() {
        return this.f3149a;
    }
}
