package com.google.zxing;

import com.google.zxing.a.b;
import com.google.zxing.d.p;
import com.google.zxing.f.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

/* compiled from: MultiFormatReader */
public final class i implements m {

    /* renamed from: a  reason: collision with root package name */
    public m[] f3146a;

    /* renamed from: b  reason: collision with root package name */
    private Map<d, ?> f3147b;

    public final n a(c cVar) throws NotFoundException {
        a((Map<d, ?>) null);
        return b(cVar);
    }

    public final n a(c cVar, Map<d, ?> map) throws NotFoundException {
        a(map);
        return b(cVar);
    }

    public final void a(Map<d, ?> map) {
        Collection collection;
        this.f3147b = map;
        boolean z = true;
        boolean z2 = map != null && map.containsKey(d.TRY_HARDER);
        if (map == null) {
            collection = null;
        } else {
            collection = (Collection) map.get(d.POSSIBLE_FORMATS);
        }
        ArrayList arrayList = new ArrayList();
        if (collection != null) {
            if (!collection.contains(a.UPC_A) && !collection.contains(a.UPC_E) && !collection.contains(a.EAN_13) && !collection.contains(a.EAN_8) && !collection.contains(a.CODABAR) && !collection.contains(a.CODE_39) && !collection.contains(a.CODE_93) && !collection.contains(a.CODE_128) && !collection.contains(a.ITF) && !collection.contains(a.RSS_14) && !collection.contains(a.RSS_EXPANDED)) {
                z = false;
            }
            if (z && !z2) {
                arrayList.add(new p(map));
            }
            if (collection.contains(a.QR_CODE)) {
                arrayList.add(new a());
            }
            if (collection.contains(a.DATA_MATRIX)) {
                arrayList.add(new com.google.zxing.b.a());
            }
            if (collection.contains(a.AZTEC)) {
                arrayList.add(new b());
            }
            if (collection.contains(a.PDF_417)) {
                arrayList.add(new com.google.zxing.e.b());
            }
            if (collection.contains(a.MAXICODE)) {
                arrayList.add(new com.google.zxing.c.a());
            }
            if (z && z2) {
                arrayList.add(new p(map));
            }
        }
        if (arrayList.isEmpty()) {
            if (!z2) {
                arrayList.add(new p(map));
            }
            arrayList.add(new a());
            arrayList.add(new com.google.zxing.b.a());
            arrayList.add(new b());
            arrayList.add(new com.google.zxing.e.b());
            arrayList.add(new com.google.zxing.c.a());
            if (z2) {
                arrayList.add(new p(map));
            }
        }
        this.f3146a = (m[]) arrayList.toArray(new m[arrayList.size()]);
    }

    public final void a() {
        m[] mVarArr = this.f3146a;
        if (mVarArr != null) {
            for (m a2 : mVarArr) {
                a2.a();
            }
        }
    }

    public n b(c cVar) throws NotFoundException {
        m[] mVarArr = this.f3146a;
        if (mVarArr != null) {
            int length = mVarArr.length;
            int i = 0;
            while (i < length) {
                try {
                    return mVarArr[i].a(cVar, this.f3147b);
                } catch (ReaderException unused) {
                    i++;
                }
            }
        }
        throw NotFoundException.a();
    }
}
