package com.google.zxing.a.c;

import com.google.zxing.common.a;

/* compiled from: BinaryShiftToken */
final class b extends h {
    private final short c;
    private final short d;

    b(h hVar, int i, int i2) {
        super(hVar);
        this.c = (short) i;
        this.d = (short) i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(int, int):int}
     arg types: [short, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int} */
    public final void a(a aVar, byte[] bArr) {
        int i = 0;
        while (true) {
            short s = this.d;
            if (i < s) {
                if (i == 0 || (i == 31 && s <= 62)) {
                    aVar.a(31, 5);
                    short s2 = this.d;
                    if (s2 > 62) {
                        aVar.a(s2 - 31, 16);
                    } else if (i == 0) {
                        aVar.a(Math.min((int) s2, 31), 5);
                    } else {
                        aVar.a(s2 - 31, 5);
                    }
                }
                aVar.a(bArr[this.c + i], 8);
                i++;
            } else {
                return;
            }
        }
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("<");
        sb.append((int) this.c);
        sb.append("::");
        sb.append((this.c + this.d) - 1);
        sb.append('>');
        return sb.toString();
    }
}
