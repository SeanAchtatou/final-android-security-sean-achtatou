package com.google.zxing.a.c;

import com.google.zxing.common.a;
import java.util.LinkedList;

/* compiled from: State */
final class g {

    /* renamed from: a  reason: collision with root package name */
    static final g f2900a = new g(h.f2902a, 0, 0, 0);

    /* renamed from: b  reason: collision with root package name */
    final int f2901b;
    final int c;
    final int d;
    private final h e;

    private g(h hVar, int i, int i2, int i3) {
        this.e = hVar;
        this.f2901b = i;
        this.c = i2;
        this.d = i3;
    }

    /* access modifiers changed from: package-private */
    public final g a(int i, int i2) {
        int i3 = this.d;
        h hVar = this.e;
        if (i != this.f2901b) {
            int i4 = d.f2898b[this.f2901b][i];
            int i5 = 65535 & i4;
            int i6 = i4 >> 16;
            hVar = hVar.a(i5, i6);
            i3 += i6;
        }
        int i7 = i == 2 ? 4 : 5;
        return new g(hVar.a(i2, i7), i, 0, i3 + i7);
    }

    /* access modifiers changed from: package-private */
    public final g b(int i, int i2) {
        h hVar = this.e;
        int i3 = this.f2901b == 2 ? 4 : 5;
        return new g(hVar.a(d.c[this.f2901b][i], i3).a(i2, 5), this.f2901b, 0, this.d + i3 + 5);
    }

    /* access modifiers changed from: package-private */
    public final g a(int i) {
        h hVar = this.e;
        int i2 = this.f2901b;
        int i3 = this.d;
        if (i2 == 4 || i2 == 2) {
            int i4 = d.f2898b[i2][0];
            int i5 = 65535 & i4;
            int i6 = i4 >> 16;
            hVar = hVar.a(i5, i6);
            i3 += i6;
            i2 = 0;
        }
        int i7 = this.c;
        g gVar = new g(hVar, i2, this.c + 1, i3 + ((i7 == 0 || i7 == 31) ? 18 : i7 == 62 ? 9 : 8));
        return gVar.c == 2078 ? gVar.b(i + 1) : gVar;
    }

    /* access modifiers changed from: package-private */
    public final g b(int i) {
        int i2 = this.c;
        if (i2 == 0) {
            return this;
        }
        return new g(this.e.b(i - i2, i2), this.f2901b, 0, this.d);
    }

    /* access modifiers changed from: package-private */
    public final boolean a(g gVar) {
        int i;
        int i2 = this.d + (d.f2898b[this.f2901b][gVar.f2901b] >> 16);
        int i3 = gVar.c;
        if (i3 > 0 && ((i = this.c) == 0 || i > i3)) {
            i2 += 10;
        }
        return i2 <= gVar.d;
    }

    /* access modifiers changed from: package-private */
    public final a a(byte[] bArr) {
        LinkedList<h> linkedList = new LinkedList<>();
        for (h hVar = b(bArr.length).e; hVar != null; hVar = hVar.f2903b) {
            linkedList.addFirst(hVar);
        }
        a aVar = new a();
        for (h a2 : linkedList) {
            a2.a(aVar, bArr);
        }
        return aVar;
    }

    public final String toString() {
        return String.format("%s bits=%d bytes=%d", d.f2897a[this.f2901b], Integer.valueOf(this.d), Integer.valueOf(this.c));
    }
}
