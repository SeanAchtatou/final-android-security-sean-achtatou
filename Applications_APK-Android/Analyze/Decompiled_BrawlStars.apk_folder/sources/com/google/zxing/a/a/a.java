package com.google.zxing.a.a;

import com.facebook.appevents.AppEventsConstants;
import com.google.zxing.FormatException;
import com.google.zxing.common.b;
import com.google.zxing.common.e;
import com.google.zxing.common.reedsolomon.ReedSolomonException;
import com.google.zxing.common.reedsolomon.c;
import java.util.Arrays;

/* compiled from: Decoder */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static final String[] f2885a = {"CTRL_PS", " ", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "CTRL_LL", "CTRL_ML", "CTRL_DL", "CTRL_BS"};

    /* renamed from: b  reason: collision with root package name */
    private static final String[] f2886b = {"CTRL_PS", " ", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "CTRL_US", "CTRL_ML", "CTRL_DL", "CTRL_BS"};
    private static final String[] c = {"CTRL_PS", " ", "\u0001", "\u0002", "\u0003", "\u0004", "\u0005", "\u0006", "\u0007", "\b", "\t", "\n", "\u000b", "\f", "\r", "\u001b", "\u001c", "\u001d", "\u001e", "\u001f", "@", "\\", "^", "_", "`", "|", "~", "", "CTRL_LL", "CTRL_UL", "CTRL_PL", "CTRL_BS"};
    private static final String[] d = {"", "\r", "\r\n", ". ", ", ", ": ", "!", "\"", "#", "$", "%", "&", "'", "(", ")", "*", "+", ",", "-", ".", "/", ":", ";", "<", "=", ">", "?", "[", "]", "{", "}", "CTRL_UL"};
    private static final String[] e = {"CTRL_PS", " ", AppEventsConstants.EVENT_PARAM_VALUE_NO, AppEventsConstants.EVENT_PARAM_VALUE_YES, "2", "3", "4", "5", "6", "7", "8", "9", ",", ".", "CTRL_UL", "CTRL_US"};
    private com.google.zxing.a.a f;

    /* renamed from: com.google.zxing.a.a.a$a  reason: collision with other inner class name */
    /* compiled from: Decoder */
    enum C0128a {
        UPPER,
        LOWER,
        MIXED,
        DIGIT,
        PUNCT,
        BINARY
    }

    private static int a(int i, boolean z) {
        return ((z ? 88 : 112) + (i << 4)) * i;
    }

    public final e a(com.google.zxing.a.a aVar) throws FormatException {
        this.f = aVar;
        boolean[] b2 = b(a(aVar.d));
        e eVar = new e(c(b2), a(b2), null, null);
        eVar.f2975b = b2.length;
        return eVar;
    }

    private static String a(boolean[] zArr) {
        int length = zArr.length;
        C0128a aVar = C0128a.UPPER;
        C0128a aVar2 = C0128a.UPPER;
        StringBuilder sb = new StringBuilder(20);
        C0128a aVar3 = aVar;
        int i = 0;
        while (i < length) {
            if (aVar2 == C0128a.BINARY) {
                if (length - i < 5) {
                    break;
                }
                int a2 = a(zArr, i, 5);
                int i2 = i + 5;
                if (a2 == 0) {
                    if (length - i2 < 11) {
                        break;
                    }
                    a2 = a(zArr, i2, 11) + 31;
                    i2 += 11;
                }
                int i3 = i2;
                int i4 = 0;
                while (true) {
                    if (i4 >= a2) {
                        i = i3;
                        break;
                    } else if (length - i3 < 8) {
                        i = length;
                        break;
                    } else {
                        sb.append((char) a(zArr, i3, 8));
                        i3 += 8;
                        i4++;
                    }
                }
            } else {
                int i5 = aVar2 == C0128a.DIGIT ? 4 : 5;
                if (length - i < i5) {
                    break;
                }
                int a3 = a(zArr, i, i5);
                i += i5;
                String a4 = a(aVar2, a3);
                if (a4.startsWith("CTRL_")) {
                    aVar3 = a(a4.charAt(5));
                    if (a4.charAt(6) != 'L') {
                        C0128a aVar4 = aVar3;
                        aVar3 = aVar2;
                        aVar2 = aVar4;
                    }
                } else {
                    sb.append(a4);
                }
            }
            aVar2 = aVar3;
        }
        return sb.toString();
    }

    private static C0128a a(char c2) {
        if (c2 == 'B') {
            return C0128a.BINARY;
        }
        if (c2 == 'D') {
            return C0128a.DIGIT;
        }
        if (c2 == 'P') {
            return C0128a.PUNCT;
        }
        if (c2 == 'L') {
            return C0128a.LOWER;
        }
        if (c2 != 'M') {
            return C0128a.UPPER;
        }
        return C0128a.MIXED;
    }

    private static String a(C0128a aVar, int i) {
        int i2 = b.f2889a[aVar.ordinal()];
        if (i2 == 1) {
            return f2885a[i];
        }
        if (i2 == 2) {
            return f2886b[i];
        }
        if (i2 == 3) {
            return c[i];
        }
        if (i2 == 4) {
            return d[i];
        }
        if (i2 == 5) {
            return e[i];
        }
        throw new IllegalStateException("Bad table");
    }

    private boolean[] b(boolean[] zArr) throws FormatException {
        com.google.zxing.common.reedsolomon.a aVar;
        int i = 8;
        if (this.f.c <= 2) {
            i = 6;
            aVar = com.google.zxing.common.reedsolomon.a.c;
        } else if (this.f.c <= 8) {
            aVar = com.google.zxing.common.reedsolomon.a.g;
        } else if (this.f.c <= 22) {
            i = 10;
            aVar = com.google.zxing.common.reedsolomon.a.f2984b;
        } else {
            i = 12;
            aVar = com.google.zxing.common.reedsolomon.a.f2983a;
        }
        int i2 = this.f.f2884b;
        int length = zArr.length / i;
        if (length >= i2) {
            int[] iArr = new int[length];
            int length2 = zArr.length % i;
            int i3 = 0;
            while (i3 < length) {
                iArr[i3] = a(zArr, length2, i);
                i3++;
                length2 += i;
            }
            try {
                new c(aVar).a(iArr, length - i2);
                int i4 = (1 << i) - 1;
                int i5 = 0;
                for (int i6 = 0; i6 < i2; i6++) {
                    int i7 = iArr[i6];
                    if (i7 == 0 || i7 == i4) {
                        throw FormatException.a();
                    }
                    if (i7 == 1 || i7 == i4 - 1) {
                        i5++;
                    }
                }
                boolean[] zArr2 = new boolean[((i2 * i) - i5)];
                int i8 = 0;
                for (int i9 = 0; i9 < i2; i9++) {
                    int i10 = iArr[i9];
                    if (i10 == 1 || i10 == i4 - 1) {
                        Arrays.fill(zArr2, i8, (i8 + i) - 1, i10 > 1);
                        i8 += i - 1;
                    } else {
                        int i11 = i - 1;
                        while (i11 >= 0) {
                            int i12 = i8 + 1;
                            zArr2[i8] = ((1 << i11) & i10) != 0;
                            i11--;
                            i8 = i12;
                        }
                    }
                }
                return zArr2;
            } catch (ReedSolomonException e2) {
                throw FormatException.a(e2);
            }
        } else {
            throw FormatException.a();
        }
    }

    private boolean[] a(b bVar) {
        b bVar2 = bVar;
        boolean z = this.f.f2883a;
        int i = this.f.c;
        int i2 = (z ? 11 : 14) + (i << 2);
        int[] iArr = new int[i2];
        boolean[] zArr = new boolean[a(i, z)];
        int i3 = 2;
        if (z) {
            for (int i4 = 0; i4 < iArr.length; i4++) {
                iArr[i4] = i4;
            }
        } else {
            int i5 = i2 / 2;
            int i6 = ((i2 + 1) + (((i5 - 1) / 15) * 2)) / 2;
            for (int i7 = 0; i7 < i5; i7++) {
                int i8 = (i7 / 15) + i7;
                iArr[(i5 - i7) - 1] = (i6 - i8) - 1;
                iArr[i5 + i7] = i8 + i6 + 1;
            }
        }
        int i9 = 0;
        int i10 = 0;
        while (i9 < i) {
            int i11 = ((i - i9) << i3) + (z ? 9 : 12);
            int i12 = i9 << 1;
            int i13 = (i2 - 1) - i12;
            int i14 = 0;
            while (i14 < i11) {
                int i15 = i14 << 1;
                int i16 = 0;
                while (i16 < i3) {
                    int i17 = i12 + i16;
                    int i18 = i12 + i14;
                    zArr[i10 + i15 + i16] = bVar2.a(iArr[i17], iArr[i18]);
                    int i19 = iArr[i18];
                    int i20 = i13 - i16;
                    zArr[(i11 * 2) + i10 + i15 + i16] = bVar2.a(i19, iArr[i20]);
                    int i21 = i13 - i14;
                    zArr[(i11 * 4) + i10 + i15 + i16] = bVar2.a(iArr[i20], iArr[i21]);
                    zArr[(i11 * 6) + i10 + i15 + i16] = bVar2.a(iArr[i21], iArr[i17]);
                    i16++;
                    i = i;
                    z = z;
                    i3 = 2;
                }
                i14++;
                i3 = 2;
            }
            i10 += i11 << 3;
            i9++;
            i3 = 2;
        }
        return zArr;
    }

    private static int a(boolean[] zArr, int i, int i2) {
        int i3 = 0;
        for (int i4 = i; i4 < i + i2; i4++) {
            i3 <<= 1;
            if (zArr[i4]) {
                i3 |= 1;
            }
        }
        return i3;
    }

    private static byte a(boolean[] zArr, int i) {
        int a2;
        int length = zArr.length - i;
        if (length >= 8) {
            a2 = a(zArr, i, 8);
        } else {
            a2 = a(zArr, i, length) << (8 - length);
        }
        return (byte) a2;
    }

    private static byte[] c(boolean[] zArr) {
        byte[] bArr = new byte[((zArr.length + 7) / 8)];
        for (int i = 0; i < bArr.length; i++) {
            bArr[i] = a(zArr, i << 3);
        }
        return bArr;
    }
}
