package com.google.zxing.a.c;

import com.google.zxing.common.a;

/* compiled from: SimpleToken */
final class f extends h {
    private final short c;
    private final short d;

    f(h hVar, int i, int i2) {
        super(hVar);
        this.c = (short) i;
        this.d = (short) i2;
    }

    /* access modifiers changed from: package-private */
    public final void a(a aVar, byte[] bArr) {
        aVar.a(this.c, this.d);
    }

    public final String toString() {
        short s = this.c;
        short s2 = this.d;
        short s3 = (s & ((1 << s2) - 1)) | (1 << s2);
        return "<" + Integer.toBinaryString(s3 | (1 << this.d)).substring(1) + '>';
    }
}
