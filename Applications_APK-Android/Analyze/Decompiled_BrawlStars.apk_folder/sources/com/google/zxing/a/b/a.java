package com.google.zxing.a.b;

import com.google.zxing.NotFoundException;
import com.google.zxing.common.b;
import com.google.zxing.common.i;
import com.google.zxing.common.reedsolomon.ReedSolomonException;
import com.google.zxing.common.reedsolomon.c;
import com.google.zxing.p;

/* compiled from: Detector */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f2890a = {3808, 476, 2107, 1799};

    /* renamed from: b  reason: collision with root package name */
    private final b f2891b;
    private boolean c;
    private int d;
    private int e;
    private int f;
    private int g;

    public a(b bVar) {
        this.f2891b = bVar;
    }

    public final com.google.zxing.a.a a(boolean z) throws NotFoundException {
        p[] a2 = a(a());
        if (z) {
            p pVar = a2[0];
            a2[0] = a2[2];
            a2[2] = pVar;
        }
        a(a2);
        b bVar = this.f2891b;
        int i = this.g;
        return new com.google.zxing.a.a(a(bVar, a2[i % 4], a2[(i + 1) % 4], a2[(i + 2) % 4], a2[(i + 3) % 4]), b(a2), this.c, this.e, this.d);
    }

    private void a(p[] pVarArr) throws NotFoundException {
        long j;
        long j2;
        if (!a(pVarArr[0]) || !a(pVarArr[1]) || !a(pVarArr[2]) || !a(pVarArr[3])) {
            throw NotFoundException.a();
        }
        int i = this.f * 2;
        int[] iArr = {a(pVarArr[0], pVarArr[1], i), a(pVarArr[1], pVarArr[2], i), a(pVarArr[2], pVarArr[3], i), a(pVarArr[3], pVarArr[0], i)};
        this.g = a(iArr, i);
        long j3 = 0;
        for (int i2 = 0; i2 < 4; i2++) {
            int i3 = iArr[(this.g + i2) % 4];
            if (this.c) {
                j2 = j3 << 7;
                j = (long) ((i3 >> 1) & 127);
            } else {
                j2 = j3 << 10;
                j = (long) (((i3 >> 2) & 992) + ((i3 >> 1) & 31));
            }
            j3 = j2 + j;
        }
        int a2 = a(j3, this.c);
        if (this.c) {
            this.d = (a2 >> 6) + 1;
            this.e = (a2 & 63) + 1;
            return;
        }
        this.d = (a2 >> 11) + 1;
        this.e = (a2 & 2047) + 1;
    }

    private static int a(int[] iArr, int i) throws NotFoundException {
        int i2 = 0;
        for (int i3 = 0; i3 < 4; i3++) {
            int i4 = iArr[i3];
            i2 = (i2 << 3) + ((i4 >> (i - 2)) << 1) + (i4 & 1);
        }
        int i5 = ((i2 & 1) << 11) + (i2 >> 1);
        for (int i6 = 0; i6 < 4; i6++) {
            if (Integer.bitCount(f2890a[i6] ^ i5) <= 2) {
                return i6;
            }
        }
        throw NotFoundException.a();
    }

    private static int a(long j, boolean z) throws NotFoundException {
        int i;
        int i2;
        if (z) {
            i = 7;
            i2 = 2;
        } else {
            i = 10;
            i2 = 4;
        }
        int i3 = i - i2;
        int[] iArr = new int[i];
        for (int i4 = i - 1; i4 >= 0; i4--) {
            iArr[i4] = ((int) j) & 15;
            j >>= 4;
        }
        try {
            new c(com.google.zxing.common.reedsolomon.a.d).a(iArr, i3);
            int i5 = 0;
            for (int i6 = 0; i6 < i2; i6++) {
                i5 = (i5 << 4) + iArr[i6];
            }
            return i5;
        } catch (ReedSolomonException unused) {
            throw NotFoundException.a();
        }
    }

    private p[] a(C0129a aVar) throws NotFoundException {
        this.f = 1;
        C0129a aVar2 = aVar;
        C0129a aVar3 = aVar2;
        C0129a aVar4 = aVar3;
        C0129a aVar5 = aVar4;
        boolean z = true;
        while (this.f < 9) {
            C0129a a2 = a(aVar2, z, 1, -1);
            C0129a a3 = a(aVar3, z, 1, 1);
            C0129a a4 = a(aVar4, z, -1, 1);
            C0129a a5 = a(aVar5, z, -1, -1);
            if (this.f > 2) {
                double b2 = (double) ((b(a5, a2) * ((float) this.f)) / (b(aVar5, aVar2) * ((float) (this.f + 2))));
                if (b2 < 0.75d || b2 > 1.25d || !a(a2, a3, a4, a5)) {
                    break;
                }
            }
            z = !z;
            this.f++;
            aVar5 = a5;
            aVar2 = a2;
            aVar3 = a3;
            aVar4 = a4;
        }
        int i = this.f;
        if (i == 5 || i == 7) {
            this.c = this.f == 5;
            p[] pVarArr = {new p(((float) aVar2.f2892a) + 0.5f, ((float) aVar2.f2893b) - 0.5f), new p(((float) aVar3.f2892a) + 0.5f, ((float) aVar3.f2893b) + 0.5f), new p(((float) aVar4.f2892a) - 0.5f, ((float) aVar4.f2893b) + 0.5f), new p(((float) aVar5.f2892a) - 0.5f, ((float) aVar5.f2893b) - 0.5f)};
            int i2 = this.f;
            return a(pVarArr, (float) ((i2 * 2) - 3), (float) (i2 * 2));
        }
        throw NotFoundException.a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.zxing.a.b.a.a(com.google.zxing.a.b.a$a, boolean, int, int):com.google.zxing.a.b.a$a
     arg types: [com.google.zxing.a.b.a$a, int, int, int]
     candidates:
      com.google.zxing.a.b.a.a(com.google.zxing.a.b.a$a, com.google.zxing.a.b.a$a, com.google.zxing.a.b.a$a, com.google.zxing.a.b.a$a):boolean
      com.google.zxing.a.b.a.a(com.google.zxing.a.b.a$a, boolean, int, int):com.google.zxing.a.b.a$a */
    private C0129a a() {
        p pVar;
        p pVar2;
        p pVar3;
        p pVar4;
        p pVar5;
        p pVar6;
        p pVar7;
        p pVar8;
        try {
            p[] a2 = new com.google.zxing.common.a.b(this.f2891b).a();
            pVar3 = a2[0];
            pVar2 = a2[1];
            pVar = a2[2];
            pVar4 = a2[3];
        } catch (NotFoundException unused) {
            int i = this.f2891b.f2968a / 2;
            int i2 = this.f2891b.f2969b / 2;
            int i3 = i + 7;
            int i4 = i2 - 7;
            p a3 = a(new C0129a(i3, i4), false, 1, -1).a();
            int i5 = i2 + 7;
            p a4 = a(new C0129a(i3, i5), false, 1, 1).a();
            int i6 = i - 7;
            p a5 = a(new C0129a(i6, i5), false, -1, 1).a();
            pVar4 = a(new C0129a(i6, i4), false, -1, -1).a();
            p pVar9 = a4;
            pVar = a5;
            pVar3 = a3;
            pVar2 = pVar9;
        }
        int a6 = com.google.zxing.common.a.a.a((((pVar3.f3153a + pVar4.f3153a) + pVar2.f3153a) + pVar.f3153a) / 4.0f);
        int a7 = com.google.zxing.common.a.a.a((((pVar3.f3154b + pVar4.f3154b) + pVar2.f3154b) + pVar.f3154b) / 4.0f);
        try {
            p[] a8 = new com.google.zxing.common.a.b(this.f2891b, 15, a6, a7).a();
            pVar6 = a8[0];
            pVar5 = a8[1];
            pVar7 = a8[2];
            pVar8 = a8[3];
        } catch (NotFoundException unused2) {
            int i7 = a6 + 7;
            int i8 = a7 - 7;
            pVar6 = a(new C0129a(i7, i8), false, 1, -1).a();
            int i9 = a7 + 7;
            pVar5 = a(new C0129a(i7, i9), false, 1, 1).a();
            int i10 = a6 - 7;
            pVar7 = a(new C0129a(i10, i9), false, -1, 1).a();
            pVar8 = a(new C0129a(i10, i8), false, -1, -1).a();
        }
        return new C0129a(com.google.zxing.common.a.a.a((((pVar6.f3153a + pVar8.f3153a) + pVar5.f3153a) + pVar7.f3153a) / 4.0f), com.google.zxing.common.a.a.a((((pVar6.f3154b + pVar8.f3154b) + pVar5.f3154b) + pVar7.f3154b) / 4.0f));
    }

    private p[] b(p[] pVarArr) {
        return a(pVarArr, (float) (this.f * 2), (float) b());
    }

    private b a(b bVar, p pVar, p pVar2, p pVar3, p pVar4) throws NotFoundException {
        p pVar5 = pVar;
        p pVar6 = pVar2;
        p pVar7 = pVar3;
        p pVar8 = pVar4;
        i a2 = i.a();
        int b2 = b();
        int i = b2;
        int i2 = b2;
        float f2 = ((float) b2) / 2.0f;
        int i3 = this.f;
        float f3 = f2 - ((float) i3);
        float f4 = f2 + ((float) i3);
        return a2.a(bVar, i2, i, f3, f3, f4, f3, f4, f4, f3, f4, pVar5.f3153a, pVar5.f3154b, pVar6.f3153a, pVar6.f3154b, pVar7.f3153a, pVar7.f3154b, pVar8.f3153a, pVar8.f3154b);
    }

    private int a(p pVar, p pVar2, int i) {
        float a2 = a(pVar, pVar2);
        float f2 = a2 / ((float) i);
        float f3 = pVar.f3153a;
        float f4 = pVar.f3154b;
        float f5 = ((pVar2.f3153a - pVar.f3153a) * f2) / a2;
        float f6 = (f2 * (pVar2.f3154b - pVar.f3154b)) / a2;
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3++) {
            float f7 = (float) i3;
            if (this.f2891b.a(com.google.zxing.common.a.a.a((f7 * f5) + f3), com.google.zxing.common.a.a.a((f7 * f6) + f4))) {
                i2 |= 1 << ((i - i3) - 1);
            }
        }
        return i2;
    }

    private boolean a(C0129a aVar, C0129a aVar2, C0129a aVar3, C0129a aVar4) {
        C0129a aVar5 = new C0129a(aVar.f2892a - 3, aVar.f2893b + 3);
        C0129a aVar6 = new C0129a(aVar2.f2892a - 3, aVar2.f2893b - 3);
        C0129a aVar7 = new C0129a(aVar3.f2892a + 3, aVar3.f2893b - 3);
        C0129a aVar8 = new C0129a(aVar4.f2892a + 3, aVar4.f2893b + 3);
        int a2 = a(aVar8, aVar5);
        if (a2 != 0 && a(aVar5, aVar6) == a2 && a(aVar6, aVar7) == a2 && a(aVar7, aVar8) == a2) {
            return true;
        }
        return false;
    }

    private int a(C0129a aVar, C0129a aVar2) {
        float b2 = b(aVar, aVar2);
        float f2 = ((float) (aVar2.f2892a - aVar.f2892a)) / b2;
        float f3 = ((float) (aVar2.f2893b - aVar.f2893b)) / b2;
        boolean a2 = this.f2891b.a(aVar.f2892a, aVar.f2893b);
        int ceil = (int) Math.ceil((double) b2);
        boolean z = false;
        float f4 = (float) aVar.f2892a;
        float f5 = (float) aVar.f2893b;
        int i = 0;
        for (int i2 = 0; i2 < ceil; i2++) {
            f4 += f2;
            f5 += f3;
            if (this.f2891b.a(com.google.zxing.common.a.a.a(f4), com.google.zxing.common.a.a.a(f5)) != a2) {
                i++;
            }
        }
        float f6 = ((float) i) / b2;
        if (f6 > 0.1f && f6 < 0.9f) {
            return 0;
        }
        if (f6 <= 0.1f) {
            z = true;
        }
        return z == a2 ? 1 : -1;
    }

    private static p[] a(p[] pVarArr, float f2, float f3) {
        float f4 = f3 / (f2 * 2.0f);
        float f5 = pVarArr[0].f3153a - pVarArr[2].f3153a;
        float f6 = pVarArr[0].f3154b - pVarArr[2].f3154b;
        float f7 = (pVarArr[0].f3153a + pVarArr[2].f3153a) / 2.0f;
        float f8 = (pVarArr[0].f3154b + pVarArr[2].f3154b) / 2.0f;
        float f9 = f5 * f4;
        float f10 = f6 * f4;
        p pVar = new p(f7 + f9, f8 + f10);
        p pVar2 = new p(f7 - f9, f8 - f10);
        float f11 = pVarArr[1].f3153a - pVarArr[3].f3153a;
        float f12 = pVarArr[1].f3154b - pVarArr[3].f3154b;
        float f13 = (pVarArr[1].f3153a + pVarArr[3].f3153a) / 2.0f;
        float f14 = (pVarArr[1].f3154b + pVarArr[3].f3154b) / 2.0f;
        float f15 = f11 * f4;
        float f16 = f4 * f12;
        return new p[]{pVar, new p(f13 + f15, f14 + f16), pVar2, new p(f13 - f15, f14 - f16)};
    }

    private boolean a(int i, int i2) {
        return i >= 0 && i < this.f2891b.f2968a && i2 > 0 && i2 < this.f2891b.f2969b;
    }

    private int b() {
        if (this.c) {
            return (this.d * 4) + 11;
        }
        int i = this.d;
        if (i <= 4) {
            return (i * 4) + 15;
        }
        return (i * 4) + ((((i - 4) / 8) + 1) * 2) + 15;
    }

    /* renamed from: com.google.zxing.a.b.a$a  reason: collision with other inner class name */
    /* compiled from: Detector */
    static final class C0129a {

        /* renamed from: a  reason: collision with root package name */
        final int f2892a;

        /* renamed from: b  reason: collision with root package name */
        final int f2893b;

        /* access modifiers changed from: package-private */
        public final p a() {
            return new p((float) this.f2892a, (float) this.f2893b);
        }

        C0129a(int i, int i2) {
            this.f2892a = i;
            this.f2893b = i2;
        }

        public final String toString() {
            return "<" + this.f2892a + ' ' + this.f2893b + '>';
        }
    }

    private C0129a a(C0129a aVar, boolean z, int i, int i2) {
        int i3 = aVar.f2892a + i;
        int i4 = aVar.f2893b;
        while (true) {
            i4 += i2;
            if (!a(i3, i4) || this.f2891b.a(i3, i4) != z) {
                int i5 = i3 - i;
                int i6 = i4 - i2;
            } else {
                i3 += i;
            }
        }
        int i52 = i3 - i;
        int i62 = i4 - i2;
        while (a(i52, i62) && this.f2891b.a(i52, i62) == z) {
            i52 += i;
        }
        int i7 = i52 - i;
        while (a(i7, i62) && this.f2891b.a(i7, i62) == z) {
            i62 += i2;
        }
        return new C0129a(i7, i62 - i2);
    }

    private boolean a(p pVar) {
        return a(com.google.zxing.common.a.a.a(pVar.f3153a), com.google.zxing.common.a.a.a(pVar.f3154b));
    }

    private static float b(C0129a aVar, C0129a aVar2) {
        return com.google.zxing.common.a.a.a(aVar.f2892a, aVar.f2893b, aVar2.f2892a, aVar2.f2893b);
    }

    private static float a(p pVar, p pVar2) {
        return com.google.zxing.common.a.a.a(pVar.f3153a, pVar.f3154b, pVar2.f3153a, pVar2.f3154b);
    }
}
