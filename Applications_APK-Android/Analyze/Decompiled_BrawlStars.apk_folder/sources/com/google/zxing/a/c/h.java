package com.google.zxing.a.c;

import com.google.zxing.common.a;

/* compiled from: Token */
abstract class h {

    /* renamed from: a  reason: collision with root package name */
    static final h f2902a = new f(null, 0, 0);

    /* renamed from: b  reason: collision with root package name */
    final h f2903b;

    /* access modifiers changed from: package-private */
    public abstract void a(a aVar, byte[] bArr);

    h(h hVar) {
        this.f2903b = hVar;
    }

    /* access modifiers changed from: package-private */
    public final h a(int i, int i2) {
        return new f(this, i, i2);
    }

    /* access modifiers changed from: package-private */
    public final h b(int i, int i2) {
        return new b(this, i, i2);
    }
}
