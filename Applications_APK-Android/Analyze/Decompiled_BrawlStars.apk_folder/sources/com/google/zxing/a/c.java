package com.google.zxing.a;

import com.google.zxing.a;
import com.google.zxing.common.b;
import com.google.zxing.f;
import com.google.zxing.r;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Map;

/* compiled from: AztecWriter */
public final class c implements r {
    public final b a(String str, a aVar, int i, int i2, Map<f, ?> map) {
        Charset charset = StandardCharsets.ISO_8859_1;
        int i3 = 33;
        int i4 = 0;
        if (map != null) {
            if (map.containsKey(f.CHARACTER_SET)) {
                charset = Charset.forName(map.get(f.CHARACTER_SET).toString());
            }
            if (map.containsKey(f.ERROR_CORRECTION)) {
                i3 = Integer.parseInt(map.get(f.ERROR_CORRECTION).toString());
            }
            if (map.containsKey(f.AZTEC_LAYERS)) {
                i4 = Integer.parseInt(map.get(f.AZTEC_LAYERS).toString());
            }
        }
        if (aVar == a.AZTEC) {
            return a(com.google.zxing.a.c.c.a(str.getBytes(charset), i3, i4), i, i2);
        }
        throw new IllegalArgumentException("Can only encode AZTEC, but got " + aVar);
    }

    private static b a(com.google.zxing.a.c.a aVar, int i, int i2) {
        b bVar = aVar.e;
        if (bVar != null) {
            int i3 = bVar.f2968a;
            int i4 = bVar.f2969b;
            int max = Math.max(i, i3);
            int max2 = Math.max(i2, i4);
            int min = Math.min(max / i3, max2 / i4);
            int i5 = (max - (i3 * min)) / 2;
            int i6 = (max2 - (i4 * min)) / 2;
            b bVar2 = new b(max, max2);
            int i7 = 0;
            while (i7 < i4) {
                int i8 = i5;
                int i9 = 0;
                while (i9 < i3) {
                    if (bVar.a(i9, i7)) {
                        bVar2.a(i8, i6, min, min);
                    }
                    i9++;
                    i8 += min;
                }
                i7++;
                i6 += min;
            }
            return bVar2;
        }
        throw new IllegalStateException();
    }
}
