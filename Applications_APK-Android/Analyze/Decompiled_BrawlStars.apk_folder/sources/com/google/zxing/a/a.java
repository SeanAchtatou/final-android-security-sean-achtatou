package com.google.zxing.a;

import com.google.zxing.common.b;
import com.google.zxing.common.g;
import com.google.zxing.p;

/* compiled from: AztecDetectorResult */
public final class a extends g {

    /* renamed from: a  reason: collision with root package name */
    public final boolean f2883a;

    /* renamed from: b  reason: collision with root package name */
    public final int f2884b;
    public final int c;

    public a(b bVar, p[] pVarArr, boolean z, int i, int i2) {
        super(bVar, pVarArr);
        this.f2883a = z;
        this.f2884b = i;
        this.c = i2;
    }
}
