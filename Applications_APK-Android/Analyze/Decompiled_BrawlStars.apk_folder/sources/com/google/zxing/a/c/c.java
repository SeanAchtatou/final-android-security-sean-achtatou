package com.google.zxing.a.c;

import com.google.zxing.common.a;
import com.google.zxing.common.b;
import com.google.zxing.common.reedsolomon.d;
import java.util.Collections;
import java.util.LinkedList;

/* compiled from: Encoder */
public final class c {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f2896a = {4, 6, 6, 8, 8, 8, 8, 8, 8, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12};

    private static int a(int i, boolean z) {
        return ((z ? 88 : 112) + (i << 4)) * i;
    }

    public static a a(byte[] bArr, int i, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        boolean z;
        a aVar;
        int i8;
        int i9;
        a aVar2;
        int i10;
        int i11;
        d dVar = new d(bArr);
        Iterable<g> singletonList = Collections.singletonList(g.f2900a);
        int i12 = 0;
        while (true) {
            i3 = 5;
            i4 = 3;
            int i13 = 4;
            i5 = 32;
            i6 = 2;
            if (i12 >= dVar.d.length) {
                break;
            }
            int i14 = i12 + 1;
            byte b2 = i14 < dVar.d.length ? dVar.d[i14] : 0;
            byte b3 = dVar.d[i12];
            if (b3 == 13) {
                if (b2 != 10) {
                    i6 = 0;
                }
                i3 = i6;
            } else if (b3 == 44) {
                if (b2 != 32) {
                    i13 = 0;
                }
                i3 = i13;
            } else if (b3 == 46) {
                if (b2 != 32) {
                    i4 = 0;
                }
                i3 = i4;
            } else if (!(b3 == 58 && b2 == 32)) {
                i3 = 0;
            }
            if (i3 > 0) {
                singletonList = d.a(singletonList, i12, i3);
                i12 = i14;
            } else {
                LinkedList linkedList = new LinkedList();
                for (g a2 : singletonList) {
                    dVar.a(a2, i12, linkedList);
                }
                singletonList = d.a(linkedList);
            }
            i12++;
        }
        a a3 = ((g) Collections.min(singletonList, new e(dVar))).a(dVar.d);
        int i15 = 11;
        int i16 = ((a3.f2965b * i) / 100) + 11;
        int i17 = a3.f2965b + i16;
        if (i2 != 0) {
            boolean z2 = i2 < 0;
            int abs = Math.abs(i2);
            if (z2) {
                i5 = 4;
            }
            if (abs <= i5) {
                int a4 = a(abs, z2);
                int i18 = f2896a[abs];
                int i19 = a4 - (a4 % i18);
                a a5 = a(a3, i18);
                if (a5.f2965b + i16 > i19) {
                    throw new IllegalArgumentException("Data to large for user specified layer");
                } else if (!z2 || a5.f2965b <= (i18 << 6)) {
                    aVar = a5;
                    z = z2;
                    i7 = abs;
                    i9 = a4;
                    i8 = i18;
                } else {
                    throw new IllegalArgumentException("Data to large for user specified layer");
                }
            } else {
                throw new IllegalArgumentException(String.format("Illegal value %s for layers", Integer.valueOf(i2)));
            }
        } else {
            aVar = null;
            int i20 = 0;
            int i21 = 0;
            while (i20 <= i5) {
                z = i20 <= i4;
                i7 = z ? i20 + 1 : i20;
                i9 = a(i7, z);
                if (i17 <= i9) {
                    int[] iArr = f2896a;
                    if (i21 != iArr[i7]) {
                        i8 = iArr[i7];
                        aVar = a(a3, i8);
                    } else {
                        i8 = i21;
                    }
                    int i22 = i9 - (i9 % i8);
                    if ((z && aVar.f2965b > (i8 << 6)) || aVar.f2965b + i16 > i22) {
                        i21 = i8;
                    }
                }
                i20++;
                i3 = 5;
                i4 = 3;
                i5 = 32;
                i6 = 2;
            }
            throw new IllegalArgumentException("Data too large for an Aztec code");
        }
        a a6 = a(aVar, i9, i8);
        int i23 = aVar.f2965b / i8;
        a aVar3 = new a();
        if (z) {
            aVar3.a(i7 - 1, i6);
            aVar3.a(i23 - 1, 6);
            aVar2 = a(aVar3, 28, 4);
        } else {
            aVar3.a(i7 - 1, i3);
            aVar3.a(i23 - 1, 11);
            aVar2 = a(aVar3, 40, 4);
        }
        if (!z) {
            i15 = 14;
        }
        int i24 = i15 + (i7 << 2);
        int[] iArr2 = new int[i24];
        if (z) {
            for (int i25 = 0; i25 < iArr2.length; i25++) {
                iArr2[i25] = i25;
            }
            i10 = i24;
        } else {
            int i26 = i24 / 2;
            i10 = i24 + 1 + (((i26 - 1) / 15) * 2);
            int i27 = i10 / 2;
            for (int i28 = 0; i28 < i26; i28++) {
                int i29 = (i28 / 15) + i28;
                iArr2[(i26 - i28) - 1] = (i27 - i29) - 1;
                iArr2[i26 + i28] = i29 + i27 + 1;
            }
        }
        b bVar = new b(i10);
        int i30 = 0;
        int i31 = 0;
        while (i30 < i7) {
            int i32 = ((i7 - i30) << i6) + (z ? 9 : 12);
            int i33 = 0;
            while (i33 < i32) {
                int i34 = i33 << 1;
                int i35 = 0;
                while (i35 < i6) {
                    if (a6.a(i31 + i34 + i35)) {
                        int i36 = i30 << 1;
                        i11 = i23;
                        bVar.b(iArr2[i36 + i35], iArr2[i36 + i33]);
                    } else {
                        i11 = i23;
                    }
                    if (a6.a((i32 << 1) + i31 + i34 + i35)) {
                        int i37 = i30 << 1;
                        bVar.b(iArr2[i37 + i33], iArr2[((i24 - 1) - i37) - i35]);
                    }
                    if (a6.a((i32 << 2) + i31 + i34 + i35)) {
                        int i38 = (i24 - 1) - (i30 << 1);
                        bVar.b(iArr2[i38 - i35], iArr2[i38 - i33]);
                    }
                    if (a6.a((i32 * 6) + i31 + i34 + i35)) {
                        int i39 = i30 << 1;
                        bVar.b(iArr2[((i24 - 1) - i39) - i33], iArr2[i39 + i35]);
                    }
                    i35++;
                    i23 = i11;
                    i6 = 2;
                }
                i33++;
                i6 = 2;
            }
            i31 += i32 << 3;
            i30++;
            i23 = i23;
            i6 = 2;
        }
        int i40 = i23;
        a(bVar, z, i10, aVar2);
        if (z) {
            a(bVar, i10 / 2, 5);
        } else {
            int i41 = i10 / 2;
            a(bVar, i41, 7);
            int i42 = 0;
            int i43 = 0;
            while (i42 < (i24 / 2) - 1) {
                for (int i44 = i41 & 1; i44 < i10; i44 += 2) {
                    int i45 = i41 - i43;
                    bVar.b(i45, i44);
                    int i46 = i41 + i43;
                    bVar.b(i46, i44);
                    bVar.b(i44, i45);
                    bVar.b(i44, i46);
                }
                i42 += 15;
                i43 += 16;
            }
        }
        a aVar4 = new a();
        aVar4.f2894a = z;
        aVar4.f2895b = i10;
        aVar4.c = i7;
        aVar4.d = i40;
        aVar4.e = bVar;
        return aVar4;
    }

    private static void a(b bVar, int i, int i2) {
        for (int i3 = 0; i3 < i2; i3 += 2) {
            int i4 = i - i3;
            int i5 = i4;
            while (true) {
                int i6 = i + i3;
                if (i5 > i6) {
                    break;
                }
                bVar.b(i5, i4);
                bVar.b(i5, i6);
                bVar.b(i4, i5);
                bVar.b(i6, i5);
                i5++;
            }
        }
        int i7 = i - i2;
        bVar.b(i7, i7);
        int i8 = i7 + 1;
        bVar.b(i8, i7);
        bVar.b(i7, i8);
        int i9 = i + i2;
        bVar.b(i9, i7);
        bVar.b(i9, i8);
        bVar.b(i9, i9 - 1);
    }

    private static void a(b bVar, boolean z, int i, a aVar) {
        int i2 = i / 2;
        int i3 = 0;
        if (z) {
            while (i3 < 7) {
                int i4 = (i2 - 3) + i3;
                if (aVar.a(i3)) {
                    bVar.b(i4, i2 - 5);
                }
                if (aVar.a(i3 + 7)) {
                    bVar.b(i2 + 5, i4);
                }
                if (aVar.a(20 - i3)) {
                    bVar.b(i4, i2 + 5);
                }
                if (aVar.a(27 - i3)) {
                    bVar.b(i2 - 5, i4);
                }
                i3++;
            }
            return;
        }
        while (i3 < 10) {
            int i5 = (i2 - 5) + i3 + (i3 / 5);
            if (aVar.a(i3)) {
                bVar.b(i5, i2 - 7);
            }
            if (aVar.a(i3 + 10)) {
                bVar.b(i2 + 7, i5);
            }
            if (aVar.a(29 - i3)) {
                bVar.b(i5, i2 + 7);
            }
            if (aVar.a(39 - i3)) {
                bVar.b(i2 - 7, i5);
            }
            i3++;
        }
    }

    private static int[] b(a aVar, int i, int i2) {
        int[] iArr = new int[i2];
        int i3 = aVar.f2965b / i;
        for (int i4 = 0; i4 < i3; i4++) {
            int i5 = 0;
            for (int i6 = 0; i6 < i; i6++) {
                i5 |= aVar.a((i4 * i) + i6) ? 1 << ((i - i6) - 1) : 0;
            }
            iArr[i4] = i5;
        }
        return iArr;
    }

    private static com.google.zxing.common.reedsolomon.a a(int i) {
        if (i == 4) {
            return com.google.zxing.common.reedsolomon.a.d;
        }
        if (i == 6) {
            return com.google.zxing.common.reedsolomon.a.c;
        }
        if (i == 8) {
            return com.google.zxing.common.reedsolomon.a.g;
        }
        if (i == 10) {
            return com.google.zxing.common.reedsolomon.a.f2984b;
        }
        if (i == 12) {
            return com.google.zxing.common.reedsolomon.a.f2983a;
        }
        throw new IllegalArgumentException("Unsupported word size " + i);
    }

    private static a a(a aVar, int i) {
        a aVar2 = new a();
        int i2 = aVar.f2965b;
        int i3 = (1 << i) - 2;
        int i4 = 0;
        while (i4 < i2) {
            int i5 = 0;
            for (int i6 = 0; i6 < i; i6++) {
                int i7 = i4 + i6;
                if (i7 >= i2 || aVar.a(i7)) {
                    i5 |= 1 << ((i - 1) - i6);
                }
            }
            int i8 = i5 & i3;
            if (i8 == i3) {
                aVar2.a(i8, i);
            } else if (i8 == 0) {
                aVar2.a(i5 | 1, i);
            } else {
                aVar2.a(i5, i);
                i4 += i;
            }
            i4--;
            i4 += i;
        }
        return aVar2;
    }

    private static a a(a aVar, int i, int i2) {
        d dVar = new d(a(i2));
        int i3 = i / i2;
        int[] b2 = b(aVar, i2, i3);
        dVar.a(b2, i3 - (aVar.f2965b / i2));
        a aVar2 = new a();
        aVar2.a(0, i % i2);
        for (int a2 : b2) {
            aVar2.a(a2, i2);
        }
        return aVar2;
    }
}
