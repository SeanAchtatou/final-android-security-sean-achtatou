package com.google.zxing.b;

import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.b.a.e;
import com.google.zxing.b.b.a;
import com.google.zxing.c;
import com.google.zxing.common.b;
import com.google.zxing.common.g;
import com.google.zxing.d;
import com.google.zxing.m;
import com.google.zxing.n;
import com.google.zxing.o;
import com.google.zxing.p;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: DataMatrixReader */
public final class a implements m {

    /* renamed from: a  reason: collision with root package name */
    private static final p[] f2905a = new p[0];

    /* renamed from: b  reason: collision with root package name */
    private final e f2906b = new e();

    public final void a() {
    }

    public final n a(c cVar) throws NotFoundException, ChecksumException, FormatException {
        return a(cVar, null);
    }

    public final n a(c cVar, Map<d, ?> map) throws NotFoundException, ChecksumException, FormatException {
        p[] pVarArr;
        com.google.zxing.common.e eVar;
        p pVar;
        b bVar;
        Map<d, ?> map2 = map;
        if (map2 == null || !map2.containsKey(d.PURE_BARCODE)) {
            com.google.zxing.b.b.a aVar = new com.google.zxing.b.b.a(cVar.b());
            p[] a2 = aVar.f2924b.a();
            p pVar2 = a2[0];
            p pVar3 = a2[1];
            p pVar4 = a2[2];
            p pVar5 = a2[3];
            ArrayList arrayList = new ArrayList(4);
            arrayList.add(aVar.b(pVar2, pVar3));
            arrayList.add(aVar.b(pVar2, pVar4));
            arrayList.add(aVar.b(pVar3, pVar5));
            arrayList.add(aVar.b(pVar4, pVar5));
            Collections.sort(arrayList, new a.b((byte) 0));
            a.C0130a aVar2 = (a.C0130a) arrayList.get(0);
            a.C0130a aVar3 = (a.C0130a) arrayList.get(1);
            HashMap hashMap = new HashMap();
            com.google.zxing.b.b.a.a(hashMap, aVar2.f2925a);
            com.google.zxing.b.b.a.a(hashMap, aVar2.f2926b);
            com.google.zxing.b.b.a.a(hashMap, aVar3.f2925a);
            com.google.zxing.b.b.a.a(hashMap, aVar3.f2926b);
            p pVar6 = null;
            p pVar7 = null;
            p pVar8 = null;
            for (Map.Entry entry : hashMap.entrySet()) {
                p pVar9 = (p) entry.getKey();
                if (((Integer) entry.getValue()).intValue() == 2) {
                    pVar7 = pVar9;
                } else if (pVar6 == null) {
                    pVar6 = pVar9;
                } else {
                    pVar8 = pVar9;
                }
            }
            if (pVar6 == null || pVar7 == null || pVar8 == null) {
                throw NotFoundException.a();
            }
            p[] pVarArr2 = {pVar6, pVar7, pVar8};
            p.a(pVarArr2);
            p pVar10 = pVarArr2[0];
            p pVar11 = pVarArr2[1];
            p pVar12 = pVarArr2[2];
            if (!hashMap.containsKey(pVar2)) {
                pVar5 = pVar2;
            } else if (!hashMap.containsKey(pVar3)) {
                pVar5 = pVar3;
            } else if (!hashMap.containsKey(pVar4)) {
                pVar5 = pVar4;
            }
            int i = aVar.b(pVar12, pVar5).c;
            int i2 = aVar.b(pVar10, pVar5).c;
            if ((i & 1) == 1) {
                i++;
            }
            int i3 = i + 2;
            if ((i2 & 1) == 1) {
                i2++;
            }
            int i4 = i2 + 2;
            if (i3 * 4 >= i4 * 7 || i4 * 4 >= i3 * 7) {
                float a3 = ((float) com.google.zxing.b.b.a.a(pVar11, pVar10)) / ((float) i3);
                float a4 = (float) com.google.zxing.b.b.a.a(pVar12, pVar5);
                p pVar13 = new p(pVar5.f3153a + (((pVar5.f3153a - pVar12.f3153a) / a4) * a3), pVar5.f3154b + (a3 * ((pVar5.f3154b - pVar12.f3154b) / a4)));
                float a5 = ((float) com.google.zxing.b.b.a.a(pVar11, pVar12)) / ((float) i4);
                float a6 = (float) com.google.zxing.b.b.a.a(pVar10, pVar5);
                pVar = new p(pVar5.f3153a + (((pVar5.f3153a - pVar10.f3153a) / a6) * a5), pVar5.f3154b + (a5 * ((pVar5.f3154b - pVar10.f3154b) / a6)));
                if (!aVar.a(pVar13)) {
                    if (!aVar.a(pVar)) {
                        pVar = null;
                    }
                } else if (!aVar.a(pVar) || Math.abs(i3 - aVar.b(pVar12, pVar13).c) + Math.abs(i4 - aVar.b(pVar10, pVar13).c) <= Math.abs(i3 - aVar.b(pVar12, pVar).c) + Math.abs(i4 - aVar.b(pVar10, pVar).c)) {
                    pVar = pVar13;
                }
                if (pVar == null) {
                    pVar = pVar5;
                }
                int i5 = aVar.b(pVar12, pVar).c;
                int i6 = aVar.b(pVar10, pVar).c;
                if ((i5 & 1) == 1) {
                    i5++;
                }
                int i7 = i5;
                if ((i6 & 1) == 1) {
                    i6++;
                }
                bVar = com.google.zxing.b.b.a.a(aVar.f2923a, pVar12, pVar11, pVar10, pVar, i7, i6);
            } else {
                float min = (float) Math.min(i4, i3);
                float a7 = ((float) com.google.zxing.b.b.a.a(pVar11, pVar10)) / min;
                float a8 = (float) com.google.zxing.b.b.a.a(pVar12, pVar5);
                p pVar14 = new p(pVar5.f3153a + (((pVar5.f3153a - pVar12.f3153a) / a8) * a7), pVar5.f3154b + (a7 * ((pVar5.f3154b - pVar12.f3154b) / a8)));
                float a9 = ((float) com.google.zxing.b.b.a.a(pVar11, pVar12)) / min;
                float a10 = (float) com.google.zxing.b.b.a.a(pVar10, pVar5);
                p pVar15 = new p(pVar5.f3153a + (((pVar5.f3153a - pVar10.f3153a) / a10) * a9), pVar5.f3154b + (a9 * ((pVar5.f3154b - pVar10.f3154b) / a10)));
                if (!aVar.a(pVar14)) {
                    if (!aVar.a(pVar15)) {
                        pVar15 = null;
                    }
                } else if (!aVar.a(pVar15) || Math.abs(aVar.b(pVar12, pVar14).c - aVar.b(pVar10, pVar14).c) <= Math.abs(aVar.b(pVar12, pVar15).c - aVar.b(pVar10, pVar15).c)) {
                    pVar15 = pVar14;
                }
                if (pVar15 != null) {
                    pVar5 = pVar15;
                }
                int max = Math.max(aVar.b(pVar12, pVar5).c, aVar.b(pVar10, pVar5).c) + 1;
                if ((max & 1) == 1) {
                    max++;
                }
                int i8 = max;
                bVar = com.google.zxing.b.b.a.a(aVar.f2923a, pVar12, pVar11, pVar10, pVar5, i8, i8);
                pVar = pVar5;
            }
            g gVar = new g(bVar, new p[]{pVar12, pVar11, pVar10, pVar});
            eVar = this.f2906b.a(gVar.d);
            pVarArr = gVar.e;
        } else {
            b b2 = cVar.b();
            int[] b3 = b2.b();
            int[] c = b2.c();
            if (b3 == null || c == null) {
                throw NotFoundException.a();
            }
            int i9 = b2.f2968a;
            int i10 = b3[0];
            int i11 = b3[1];
            while (i10 < i9 && b2.a(i10, i11)) {
                i10++;
            }
            if (i10 != i9) {
                int i12 = i10 - b3[0];
                if (i12 != 0) {
                    int i13 = b3[1];
                    int i14 = c[1];
                    int i15 = b3[0];
                    int i16 = ((c[0] - i15) + 1) / i12;
                    int i17 = ((i14 - i13) + 1) / i12;
                    if (i16 <= 0 || i17 <= 0) {
                        throw NotFoundException.a();
                    }
                    int i18 = i12 / 2;
                    int i19 = i13 + i18;
                    int i20 = i15 + i18;
                    b bVar2 = new b(i16, i17);
                    for (int i21 = 0; i21 < i17; i21++) {
                        int i22 = (i21 * i12) + i19;
                        for (int i23 = 0; i23 < i16; i23++) {
                            if (b2.a((i23 * i12) + i20, i22)) {
                                bVar2.b(i23, i21);
                            }
                        }
                    }
                    eVar = this.f2906b.a(bVar2);
                    pVarArr = f2905a;
                } else {
                    throw NotFoundException.a();
                }
            } else {
                throw NotFoundException.a();
            }
        }
        n nVar = new n(eVar.c, eVar.f2974a, pVarArr, com.google.zxing.a.DATA_MATRIX);
        List<byte[]> list = eVar.d;
        if (list != null) {
            nVar.a(o.BYTE_SEGMENTS, list);
        }
        String str = eVar.e;
        if (str != null) {
            nVar.a(o.ERROR_CORRECTION_LEVEL, str);
        }
        return nVar;
    }
}
