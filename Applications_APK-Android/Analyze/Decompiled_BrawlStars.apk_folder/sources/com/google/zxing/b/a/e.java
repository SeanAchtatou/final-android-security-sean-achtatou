package com.google.zxing.b.a;

import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.common.b;
import com.google.zxing.common.reedsolomon.ReedSolomonException;
import com.google.zxing.common.reedsolomon.a;
import com.google.zxing.common.reedsolomon.c;

/* compiled from: Decoder */
public final class e {

    /* renamed from: a  reason: collision with root package name */
    private final c f2916a = new c(a.f);

    public final com.google.zxing.common.e a(b bVar) throws FormatException, ChecksumException {
        a aVar = new a(bVar);
        b[] a2 = b.a(aVar.a(), aVar.f2907a);
        int i = 0;
        for (b bVar2 : a2) {
            i += bVar2.f2909a;
        }
        byte[] bArr = new byte[i];
        int length = a2.length;
        for (int i2 = 0; i2 < length; i2++) {
            b bVar3 = a2[i2];
            byte[] bArr2 = bVar3.f2910b;
            int i3 = bVar3.f2909a;
            a(bArr2, i3);
            for (int i4 = 0; i4 < i3; i4++) {
                bArr[(i4 * length) + i2] = bArr2[i4];
            }
        }
        return c.a(bArr);
    }

    private void a(byte[] bArr, int i) throws ChecksumException {
        int length = bArr.length;
        int[] iArr = new int[length];
        for (int i2 = 0; i2 < length; i2++) {
            iArr[i2] = bArr[i2] & 255;
        }
        try {
            this.f2916a.a(iArr, bArr.length - i);
            for (int i3 = 0; i3 < i; i3++) {
                bArr[i3] = (byte) iArr[i3];
            }
        } catch (ReedSolomonException unused) {
            throw ChecksumException.a();
        }
    }
}
