package com.google.zxing.b.a;

import com.google.zxing.b.a.f;

/* compiled from: DataBlock */
final class b {

    /* renamed from: a  reason: collision with root package name */
    final int f2909a;

    /* renamed from: b  reason: collision with root package name */
    final byte[] f2910b;

    private b(int i, byte[] bArr) {
        this.f2909a = i;
        this.f2910b = bArr;
    }

    static b[] a(byte[] bArr, f fVar) {
        f.b bVar = fVar.f;
        f.a[] aVarArr = bVar.f2922b;
        int i = 0;
        for (f.a aVar : aVarArr) {
            i += aVar.f2919a;
        }
        b[] bVarArr = new b[i];
        int length = aVarArr.length;
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            f.a aVar2 = aVarArr[i2];
            int i4 = i3;
            int i5 = 0;
            while (i5 < aVar2.f2919a) {
                int i6 = aVar2.f2920b;
                bVarArr[i4] = new b(i6, new byte[(bVar.f2921a + i6)]);
                i5++;
                i4++;
            }
            i2++;
            i3 = i4;
        }
        int length2 = bVarArr[0].f2910b.length - bVar.f2921a;
        int i7 = length2 - 1;
        int i8 = 0;
        int i9 = 0;
        while (i8 < i7) {
            int i10 = i9;
            int i11 = 0;
            while (i11 < i3) {
                bVarArr[i11].f2910b[i8] = bArr[i10];
                i11++;
                i10++;
            }
            i8++;
            i9 = i10;
        }
        boolean z = fVar.f2917a == 24;
        int i12 = z ? 8 : i3;
        int i13 = i9;
        int i14 = 0;
        while (i14 < i12) {
            bVarArr[i14].f2910b[i7] = bArr[i13];
            i14++;
            i13++;
        }
        int length3 = bVarArr[0].f2910b.length;
        while (length2 < length3) {
            int i15 = 0;
            while (i15 < i3) {
                int i16 = z ? (i15 + 8) % i3 : i15;
                bVarArr[i16].f2910b[(!z || i16 <= 7) ? length2 : length2 - 1] = bArr[i13];
                i15++;
                i13++;
            }
            length2++;
        }
        if (i13 == bArr.length) {
            return bVarArr;
        }
        throw new IllegalArgumentException();
    }
}
