package com.google.zxing.b.a;

import android.support.v7.widget.helper.ItemTouchHelper;
import com.google.zxing.FormatException;
import com.google.zxing.common.e;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;

/* compiled from: DecodedBitStreamParser */
final class c {

    /* renamed from: a  reason: collision with root package name */
    private static final char[] f2911a = {'*', '*', '*', ' ', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

    /* renamed from: b  reason: collision with root package name */
    private static final char[] f2912b = {'!', '\"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', ':', ';', '<', '=', '>', '?', '@', '[', '\\', ']', '^', '_'};
    private static final char[] c = {'*', '*', '*', ' ', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
    private static final char[] d = f2912b;
    private static final char[] e = {'`', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '{', '|', '}', '~', 127};

    /* compiled from: DecodedBitStreamParser */
    enum a {
        PAD_ENCODE,
        ASCII_ENCODE,
        C40_ENCODE,
        TEXT_ENCODE,
        ANSIX12_ENCODE,
        EDIFACT_ENCODE,
        BASE256_ENCODE
    }

    static e a(byte[] bArr) throws FormatException {
        com.google.zxing.common.c cVar = new com.google.zxing.common.c(bArr);
        StringBuilder sb = new StringBuilder(100);
        StringBuilder sb2 = new StringBuilder(0);
        ArrayList arrayList = new ArrayList(1);
        a aVar = a.ASCII_ENCODE;
        do {
            if (aVar == a.ASCII_ENCODE) {
                aVar = a(cVar, sb, sb2);
            } else {
                int i = d.f2915a[aVar.ordinal()];
                if (i == 1) {
                    a(cVar, sb);
                } else if (i == 2) {
                    b(cVar, sb);
                } else if (i == 3) {
                    c(cVar, sb);
                } else if (i == 4) {
                    d(cVar, sb);
                } else if (i == 5) {
                    a(cVar, sb, arrayList);
                } else {
                    throw FormatException.a();
                }
                aVar = a.ASCII_ENCODE;
            }
            if (aVar == a.PAD_ENCODE) {
                break;
            }
        } while (cVar.a() > 0);
        if (sb2.length() > 0) {
            sb.append((CharSequence) sb2);
        }
        String sb3 = sb.toString();
        if (arrayList.isEmpty()) {
            arrayList = null;
        }
        return new e(bArr, sb3, arrayList, null);
    }

    private static a a(com.google.zxing.common.c cVar, StringBuilder sb, StringBuilder sb2) throws FormatException {
        boolean z = false;
        do {
            int a2 = cVar.a(8);
            if (a2 == 0) {
                throw FormatException.a();
            } else if (a2 <= 128) {
                if (z) {
                    a2 += 128;
                }
                sb.append((char) (a2 - 1));
                return a.ASCII_ENCODE;
            } else if (a2 == 129) {
                return a.PAD_ENCODE;
            } else {
                if (a2 <= 229) {
                    int i = a2 - 130;
                    if (i < 10) {
                        sb.append('0');
                    }
                    sb.append(i);
                } else {
                    switch (a2) {
                        case 230:
                            return a.C40_ENCODE;
                        case 231:
                            return a.BASE256_ENCODE;
                        case 232:
                            sb.append(29);
                            break;
                        case 233:
                        case 234:
                        case 241:
                            break;
                        case 235:
                            z = true;
                            break;
                        case 236:
                            sb.append("[)>\u001e05\u001d");
                            sb2.insert(0, "\u001e\u0004");
                            break;
                        case 237:
                            sb.append("[)>\u001e06\u001d");
                            sb2.insert(0, "\u001e\u0004");
                            break;
                        case 238:
                            return a.ANSIX12_ENCODE;
                        case 239:
                            return a.TEXT_ENCODE;
                        case 240:
                            return a.EDIFACT_ENCODE;
                        default:
                            if (a2 >= 242 && !(a2 == 254 && cVar.a() == 0)) {
                                throw FormatException.a();
                            }
                    }
                }
            }
        } while (cVar.a() > 0);
        return a.ASCII_ENCODE;
    }

    private static void a(com.google.zxing.common.c cVar, StringBuilder sb) throws FormatException {
        int a2;
        int[] iArr = new int[3];
        boolean z = false;
        int i = 0;
        while (cVar.a() != 8 && (a2 = cVar.a(8)) != 254) {
            a(a2, cVar.a(8), iArr);
            boolean z2 = z;
            for (int i2 = 0; i2 < 3; i2++) {
                int i3 = iArr[i2];
                if (i != 0) {
                    if (i != 1) {
                        if (i == 2) {
                            char[] cArr = f2912b;
                            if (i3 < cArr.length) {
                                char c2 = cArr[i3];
                                if (z2) {
                                    sb.append((char) (c2 + 128));
                                } else {
                                    sb.append(c2);
                                }
                            } else if (i3 == 27) {
                                sb.append(29);
                            } else if (i3 == 30) {
                                z2 = true;
                            } else {
                                throw FormatException.a();
                            }
                            i = 0;
                        } else if (i != 3) {
                            throw FormatException.a();
                        } else if (z2) {
                            sb.append((char) (i3 + 224));
                        } else {
                            sb.append((char) (i3 + 96));
                            i = 0;
                        }
                    } else if (z2) {
                        sb.append((char) (i3 + 128));
                    } else {
                        sb.append((char) i3);
                        i = 0;
                    }
                    z2 = false;
                    i = 0;
                } else if (i3 < 3) {
                    i = i3 + 1;
                } else {
                    char[] cArr2 = f2911a;
                    if (i3 < cArr2.length) {
                        char c3 = cArr2[i3];
                        if (z2) {
                            sb.append((char) (c3 + 128));
                            z2 = false;
                        } else {
                            sb.append(c3);
                        }
                    } else {
                        throw FormatException.a();
                    }
                }
            }
            if (cVar.a() > 0) {
                z = z2;
            } else {
                return;
            }
        }
    }

    private static void b(com.google.zxing.common.c cVar, StringBuilder sb) throws FormatException {
        int a2;
        int[] iArr = new int[3];
        boolean z = false;
        int i = 0;
        while (cVar.a() != 8 && (a2 = cVar.a(8)) != 254) {
            a(a2, cVar.a(8), iArr);
            boolean z2 = z;
            for (int i2 = 0; i2 < 3; i2++) {
                int i3 = iArr[i2];
                if (i != 0) {
                    if (i != 1) {
                        if (i == 2) {
                            char[] cArr = d;
                            if (i3 < cArr.length) {
                                char c2 = cArr[i3];
                                if (z2) {
                                    sb.append((char) (c2 + 128));
                                } else {
                                    sb.append(c2);
                                }
                            } else if (i3 == 27) {
                                sb.append(29);
                            } else if (i3 == 30) {
                                z2 = true;
                            } else {
                                throw FormatException.a();
                            }
                            i = 0;
                        } else if (i == 3) {
                            char[] cArr2 = e;
                            if (i3 < cArr2.length) {
                                char c3 = cArr2[i3];
                                if (z2) {
                                    sb.append((char) (c3 + 128));
                                } else {
                                    sb.append(c3);
                                    i = 0;
                                }
                            } else {
                                throw FormatException.a();
                            }
                        } else {
                            throw FormatException.a();
                        }
                    } else if (z2) {
                        sb.append((char) (i3 + 128));
                    } else {
                        sb.append((char) i3);
                        i = 0;
                    }
                    z2 = false;
                    i = 0;
                } else if (i3 < 3) {
                    i = i3 + 1;
                } else {
                    char[] cArr3 = c;
                    if (i3 < cArr3.length) {
                        char c4 = cArr3[i3];
                        if (z2) {
                            sb.append((char) (c4 + 128));
                            z2 = false;
                        } else {
                            sb.append(c4);
                        }
                    } else {
                        throw FormatException.a();
                    }
                }
            }
            if (cVar.a() > 0) {
                z = z2;
            } else {
                return;
            }
        }
    }

    private static void c(com.google.zxing.common.c cVar, StringBuilder sb) throws FormatException {
        int a2;
        int[] iArr = new int[3];
        while (cVar.a() != 8 && (a2 = cVar.a(8)) != 254) {
            a(a2, cVar.a(8), iArr);
            for (int i = 0; i < 3; i++) {
                int i2 = iArr[i];
                if (i2 == 0) {
                    sb.append(13);
                } else if (i2 == 1) {
                    sb.append('*');
                } else if (i2 == 2) {
                    sb.append('>');
                } else if (i2 == 3) {
                    sb.append(' ');
                } else if (i2 < 14) {
                    sb.append((char) (i2 + 44));
                } else if (i2 < 40) {
                    sb.append((char) (i2 + 51));
                } else {
                    throw FormatException.a();
                }
            }
            if (cVar.a() <= 0) {
                return;
            }
        }
    }

    private static void a(int i, int i2, int[] iArr) {
        int i3 = ((i << 8) + i2) - 1;
        int i4 = i3 / 1600;
        iArr[0] = i4;
        int i5 = i3 - (i4 * 1600);
        int i6 = i5 / 40;
        iArr[1] = i6;
        iArr[2] = i5 - (i6 * 40);
    }

    private static void d(com.google.zxing.common.c cVar, StringBuilder sb) {
        while (cVar.a() > 16) {
            for (int i = 0; i < 4; i++) {
                int a2 = cVar.a(6);
                if (a2 == 31) {
                    int i2 = 8 - cVar.f2971b;
                    if (i2 != 8) {
                        cVar.a(i2);
                        return;
                    }
                    return;
                }
                if ((a2 & 32) == 0) {
                    a2 |= 64;
                }
                sb.append((char) a2);
            }
            if (cVar.a() <= 0) {
                return;
            }
        }
    }

    private static int a(int i, int i2) {
        int i3 = i - (((i2 * 149) % 255) + 1);
        return i3 >= 0 ? i3 : i3 + 256;
    }

    private static void a(com.google.zxing.common.c cVar, StringBuilder sb, Collection<byte[]> collection) throws FormatException {
        int i = cVar.f2970a + 1;
        int i2 = i + 1;
        int a2 = a(cVar.a(8), i);
        if (a2 == 0) {
            a2 = cVar.a() / 8;
        } else if (a2 >= 250) {
            a2 = ((a2 - 249) * ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION) + a(cVar.a(8), i2);
            i2++;
        }
        if (a2 >= 0) {
            byte[] bArr = new byte[a2];
            int i3 = 0;
            while (i3 < a2) {
                if (cVar.a() >= 8) {
                    bArr[i3] = (byte) a(cVar.a(8), i2);
                    i3++;
                    i2++;
                } else {
                    throw FormatException.a();
                }
            }
            collection.add(bArr);
            try {
                sb.append(new String(bArr, "ISO8859_1"));
            } catch (UnsupportedEncodingException e2) {
                throw new IllegalStateException("Platform does not support required encoding: " + e2);
            }
        } else {
            throw FormatException.a();
        }
    }
}
