package com.google.zxing.b.a;

import com.google.zxing.FormatException;

/* compiled from: Version */
public final class f {
    private static final f[] h = {new f(1, 10, 10, 8, 8, new b(5, new a(1, 3, (byte) 0), (byte) 0)), new f(2, 12, 12, 10, 10, new b(7, new a(1, 5, (byte) 0), (byte) 0)), new f(3, 14, 14, 12, 12, new b(10, new a(1, 8, (byte) 0), (byte) 0)), new f(4, 16, 16, 14, 14, new b(12, new a(1, 12, (byte) 0), (byte) 0)), new f(5, 18, 18, 16, 16, new b(14, new a(1, 18, (byte) 0), (byte) 0)), new f(6, 20, 20, 18, 18, new b(18, new a(1, 22, (byte) 0), (byte) 0)), new f(7, 22, 22, 20, 20, new b(20, new a(1, 30, (byte) 0), (byte) 0)), new f(8, 24, 24, 22, 22, new b(24, new a(1, 36, (byte) 0), (byte) 0)), new f(9, 26, 26, 24, 24, new b(28, new a(1, 44, (byte) 0), (byte) 0)), new f(10, 32, 32, 14, 14, new b(36, new a(1, 62, (byte) 0), (byte) 0)), new f(11, 36, 36, 16, 16, new b(42, new a(1, 86, (byte) 0), (byte) 0)), new f(12, 40, 40, 18, 18, new b(48, new a(1, 114, (byte) 0), (byte) 0)), new f(13, 44, 44, 20, 20, new b(56, new a(1, 144, (byte) 0), (byte) 0)), new f(14, 48, 48, 22, 22, new b(68, new a(1, 174, (byte) 0), (byte) 0)), new f(15, 52, 52, 24, 24, new b(42, new a(2, 102, (byte) 0), (byte) 0)), new f(16, 64, 64, 14, 14, new b(56, new a(2, 140, (byte) 0), (byte) 0)), new f(17, 72, 72, 16, 16, new b(36, new a(4, 92, (byte) 0), (byte) 0)), new f(18, 80, 80, 18, 18, new b(48, new a(4, 114, (byte) 0), (byte) 0)), new f(19, 88, 88, 20, 20, new b(56, new a(4, 144, (byte) 0), (byte) 0)), new f(20, 96, 96, 22, 22, new b(68, new a(4, 174, (byte) 0), (byte) 0)), new f(21, 104, 104, 24, 24, new b(56, new a(6, 136, (byte) 0), (byte) 0)), new f(22, 120, 120, 18, 18, new b(68, new a(6, 175, (byte) 0), (byte) 0)), new f(23, 132, 132, 20, 20, new b(62, new a(8, 163, (byte) 0), (byte) 0)), new f(24, 144, 144, 22, 22, new b(62, new a(8, 156, (byte) 0), new a(2, 155, (byte) 0), (byte) 0)), new f(25, 8, 18, 6, 16, new b(7, new a(1, 5, (byte) 0), (byte) 0)), new f(26, 8, 32, 6, 14, new b(11, new a(1, 10, (byte) 0), (byte) 0)), new f(27, 12, 26, 10, 24, new b(14, new a(1, 16, (byte) 0), (byte) 0)), new f(28, 12, 36, 10, 16, new b(18, new a(1, 22, (byte) 0), (byte) 0)), new f(29, 16, 36, 14, 16, new b(24, new a(1, 32, (byte) 0), (byte) 0)), new f(30, 16, 48, 14, 22, new b(28, new a(1, 49, (byte) 0), (byte) 0))};

    /* renamed from: a  reason: collision with root package name */
    final int f2917a;

    /* renamed from: b  reason: collision with root package name */
    final int f2918b;
    final int c;
    final int d;
    final int e;
    final b f;
    final int g;

    private f(int i, int i2, int i3, int i4, int i5, b bVar) {
        this.f2917a = i;
        this.f2918b = i2;
        this.c = i3;
        this.d = i4;
        this.e = i5;
        this.f = bVar;
        int i6 = bVar.f2921a;
        int i7 = 0;
        for (a aVar : bVar.f2922b) {
            i7 += aVar.f2919a * (aVar.f2920b + i6);
        }
        this.g = i7;
    }

    public static f a(int i, int i2) throws FormatException {
        if ((i & 1) == 0 && (i2 & 1) == 0) {
            for (f fVar : h) {
                if (fVar.f2918b == i && fVar.c == i2) {
                    return fVar;
                }
            }
            throw FormatException.a();
        }
        throw FormatException.a();
    }

    /* compiled from: Version */
    static final class b {

        /* renamed from: a  reason: collision with root package name */
        final int f2921a;

        /* renamed from: b  reason: collision with root package name */
        final a[] f2922b;

        /* synthetic */ b(int i, a aVar, byte b2) {
            this(i, aVar);
        }

        /* synthetic */ b(int i, a aVar, a aVar2, byte b2) {
            this(62, aVar, aVar2);
        }

        private b(int i, a aVar) {
            this.f2921a = i;
            this.f2922b = new a[]{aVar};
        }

        private b(int i, a aVar, a aVar2) {
            this.f2921a = i;
            this.f2922b = new a[]{aVar, aVar2};
        }
    }

    /* compiled from: Version */
    static final class a {

        /* renamed from: a  reason: collision with root package name */
        final int f2919a;

        /* renamed from: b  reason: collision with root package name */
        final int f2920b;

        /* synthetic */ a(int i, int i2, byte b2) {
            this(i, i2);
        }

        private a(int i, int i2) {
            this.f2919a = i;
            this.f2920b = i2;
        }
    }

    public final String toString() {
        return String.valueOf(this.f2917a);
    }
}
