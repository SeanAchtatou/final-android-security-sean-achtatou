package com.google.zxing.b.c;

import java.util.Arrays;

/* compiled from: DefaultPlacement */
public class e {

    /* renamed from: a  reason: collision with root package name */
    public final int f2927a;

    /* renamed from: b  reason: collision with root package name */
    public final byte[] f2928b;
    private final CharSequence c;
    private final int d;

    public e(CharSequence charSequence, int i, int i2) {
        this.c = charSequence;
        this.f2927a = i;
        this.d = i2;
        this.f2928b = new byte[(i * i2)];
        Arrays.fill(this.f2928b, (byte) -1);
    }

    private void a(int i, int i2, boolean z) {
        this.f2928b[(i2 * this.f2927a) + i] = z ? (byte) 1 : 0;
    }

    private boolean a(int i, int i2) {
        return this.f2928b[(i2 * this.f2927a) + i] >= 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.zxing.b.c.e.a(int, int, boolean):void
     arg types: [int, int, int]
     candidates:
      com.google.zxing.b.c.e.a(int, int, int):void
      com.google.zxing.b.c.e.a(int, int, boolean):void */
    public final void a() {
        int i;
        int i2;
        int i3 = 4;
        int i4 = 0;
        int i5 = 0;
        while (true) {
            int i6 = this.d;
            if (i3 == i6 && i4 == 0) {
                a(i6 - 1, 0, i5, 1);
                a(this.d - 1, 1, i5, 2);
                a(this.d - 1, 2, i5, 3);
                a(0, this.f2927a - 2, i5, 4);
                a(0, this.f2927a - 1, i5, 5);
                a(1, this.f2927a - 1, i5, 6);
                a(2, this.f2927a - 1, i5, 7);
                a(3, this.f2927a - 1, i5, 8);
                i5++;
            }
            int i7 = this.d;
            if (i3 == i7 - 2 && i4 == 0 && this.f2927a % 4 != 0) {
                a(i7 - 3, 0, i5, 1);
                a(this.d - 2, 0, i5, 2);
                a(this.d - 1, 0, i5, 3);
                a(0, this.f2927a - 4, i5, 4);
                a(0, this.f2927a - 3, i5, 5);
                a(0, this.f2927a - 2, i5, 6);
                a(0, this.f2927a - 1, i5, 7);
                a(1, this.f2927a - 1, i5, 8);
                i5++;
            }
            int i8 = this.d;
            if (i3 == i8 - 2 && i4 == 0 && this.f2927a % 8 == 4) {
                a(i8 - 3, 0, i5, 1);
                a(this.d - 2, 0, i5, 2);
                a(this.d - 1, 0, i5, 3);
                a(0, this.f2927a - 2, i5, 4);
                a(0, this.f2927a - 1, i5, 5);
                a(1, this.f2927a - 1, i5, 6);
                a(2, this.f2927a - 1, i5, 7);
                a(3, this.f2927a - 1, i5, 8);
                i5++;
            }
            int i9 = this.d;
            if (i3 == i9 + 4 && i4 == 2 && this.f2927a % 8 == 0) {
                a(i9 - 1, 0, i5, 1);
                a(this.d - 1, this.f2927a - 1, i5, 2);
                a(0, this.f2927a - 3, i5, 3);
                a(0, this.f2927a - 2, i5, 4);
                a(0, this.f2927a - 1, i5, 5);
                a(1, this.f2927a - 3, i5, 6);
                a(1, this.f2927a - 2, i5, 7);
                a(1, this.f2927a - 1, i5, 8);
                i5++;
            }
            do {
                if (i3 < this.d && i4 >= 0 && !a(i4, i3)) {
                    a(i3, i4, i5);
                    i5++;
                }
                i3 -= 2;
                i4 += 2;
                if (i3 < 0) {
                    break;
                }
            } while (i4 < this.f2927a);
            int i10 = i3 + 1;
            int i11 = i4 + 3;
            do {
                if (i10 >= 0 && i11 < this.f2927a && !a(i11, i10)) {
                    a(i10, i11, i5);
                    i5++;
                }
                i10 += 2;
                i11 -= 2;
                if (i10 >= this.d) {
                    break;
                }
            } while (i11 >= 0);
            i3 = i10 + 3;
            i4 = i11 + 1;
            i = this.d;
            if (i3 >= i && i4 >= (i2 = this.f2927a)) {
                break;
            }
        }
        if (!a(i2 - 1, i - 1)) {
            a(this.f2927a - 1, this.d - 1, true);
            a(this.f2927a - 2, this.d - 2, true);
        }
    }

    private void a(int i, int i2, int i3, int i4) {
        if (i < 0) {
            int i5 = this.d;
            i += i5;
            i2 += 4 - ((i5 + 4) % 8);
        }
        if (i2 < 0) {
            int i6 = this.f2927a;
            i2 += i6;
            i += 4 - ((i6 + 4) % 8);
        }
        boolean z = true;
        if ((this.c.charAt(i3) & (1 << (8 - i4))) == 0) {
            z = false;
        }
        a(i2, i, z);
    }

    private void a(int i, int i2, int i3) {
        int i4 = i - 2;
        int i5 = i2 - 2;
        a(i4, i5, i3, 1);
        int i6 = i2 - 1;
        a(i4, i6, i3, 2);
        int i7 = i - 1;
        a(i7, i5, i3, 3);
        a(i7, i6, i3, 4);
        a(i7, i2, i3, 5);
        a(i, i5, i3, 6);
        a(i, i6, i3, 7);
        a(i, i2, i3, 8);
    }
}
