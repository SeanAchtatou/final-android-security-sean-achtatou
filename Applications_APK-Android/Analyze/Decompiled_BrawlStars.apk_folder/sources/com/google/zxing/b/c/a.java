package com.google.zxing.b.c;

/* compiled from: ASCIIEncoder */
final class a implements g {
    a() {
    }

    public final void a(h hVar) {
        int i;
        String str = hVar.f2929a;
        int i2 = hVar.f;
        int length = str.length();
        if (i2 < length) {
            char charAt = str.charAt(i2);
            i = 0;
            while (j.a(charAt) && i2 < length) {
                i++;
                i2++;
                if (i2 < length) {
                    charAt = str.charAt(i2);
                }
            }
        } else {
            i = 0;
        }
        if (i >= 2) {
            char charAt2 = hVar.f2929a.charAt(hVar.f);
            char charAt3 = hVar.f2929a.charAt(hVar.f + 1);
            if (!j.a(charAt2) || !j.a(charAt3)) {
                throw new IllegalArgumentException("not digits: " + charAt2 + charAt3);
            }
            hVar.a((char) (((charAt2 - '0') * 10) + (charAt3 - '0') + 130));
            hVar.f += 2;
            return;
        }
        char a2 = hVar.a();
        int a3 = j.a(hVar.f2929a, hVar.f, 0);
        if (a3 != 0) {
            if (a3 == 1) {
                hVar.a(230);
                hVar.g = 1;
            } else if (a3 == 2) {
                hVar.a(239);
                hVar.g = 2;
            } else if (a3 == 3) {
                hVar.a(238);
                hVar.g = 3;
            } else if (a3 == 4) {
                hVar.a(240);
                hVar.g = 4;
            } else if (a3 == 5) {
                hVar.a(231);
                hVar.g = 5;
            } else {
                throw new IllegalStateException("Illegal mode: " + a3);
            }
        } else if (j.b(a2)) {
            hVar.a(235);
            hVar.a((char) ((a2 - 128) + 1));
            hVar.f++;
        } else {
            hVar.a((char) (a2 + 1));
            hVar.f++;
        }
    }
}
