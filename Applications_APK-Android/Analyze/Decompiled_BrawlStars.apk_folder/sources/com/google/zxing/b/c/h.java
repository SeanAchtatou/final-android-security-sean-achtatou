package com.google.zxing.b.c;

import com.google.zxing.e;
import java.nio.charset.StandardCharsets;

/* compiled from: EncoderContext */
final class h {

    /* renamed from: a  reason: collision with root package name */
    final String f2929a;

    /* renamed from: b  reason: collision with root package name */
    l f2930b;
    e c;
    e d;
    final StringBuilder e;
    int f;
    int g;
    k h;
    int i;

    h(String str) {
        byte[] bytes = str.getBytes(StandardCharsets.ISO_8859_1);
        StringBuilder sb = new StringBuilder(bytes.length);
        int length = bytes.length;
        int i2 = 0;
        while (i2 < length) {
            char c2 = (char) (bytes[i2] & 255);
            if (c2 != '?' || str.charAt(i2) == '?') {
                sb.append(c2);
                i2++;
            } else {
                throw new IllegalArgumentException("Message contains characters outside ISO-8859-1 encoding.");
            }
        }
        this.f2929a = sb.toString();
        this.f2930b = l.FORCE_NONE;
        this.e = new StringBuilder(str.length());
        this.g = -1;
    }

    public final char a() {
        return this.f2929a.charAt(this.f);
    }

    public final void a(String str) {
        this.e.append(str);
    }

    public final void a(char c2) {
        this.e.append(c2);
    }

    public final boolean b() {
        return this.f < e();
    }

    private int e() {
        return this.f2929a.length() - this.i;
    }

    public final int c() {
        return e() - this.f;
    }

    public final void a(int i2) {
        k kVar = this.h;
        if (kVar == null || i2 > kVar.f2934b) {
            this.h = k.a(i2, this.f2930b, this.c, this.d, true);
        }
    }

    public final void d() {
        a(this.e.length());
    }
}
