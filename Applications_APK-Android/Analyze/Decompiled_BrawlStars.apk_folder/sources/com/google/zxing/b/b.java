package com.google.zxing.b;

import com.google.zxing.a;
import com.google.zxing.b.c.i;
import com.google.zxing.b.c.j;
import com.google.zxing.b.c.k;
import com.google.zxing.b.c.l;
import com.google.zxing.e;
import com.google.zxing.f;
import com.google.zxing.r;
import java.util.Map;

/* compiled from: DataMatrixWriter */
public final class b implements r {
    public final com.google.zxing.common.b a(String str, a aVar, int i, int i2, Map<f, ?> map) {
        e eVar;
        if (str.isEmpty()) {
            throw new IllegalArgumentException("Found empty contents");
        } else if (aVar != a.DATA_MATRIX) {
            throw new IllegalArgumentException("Can only encode DATA_MATRIX, but got " + aVar);
        } else if (i < 0 || i2 < 0) {
            throw new IllegalArgumentException("Requested dimensions can't be negative: " + i + 'x' + i2);
        } else {
            l lVar = l.FORCE_NONE;
            e eVar2 = null;
            if (map != null) {
                l lVar2 = (l) map.get(f.DATA_MATRIX_SHAPE);
                if (lVar2 != null) {
                    lVar = lVar2;
                }
                eVar = (e) map.get(f.MIN_SIZE);
                if (eVar == null) {
                    eVar = null;
                }
                e eVar3 = (e) map.get(f.MAX_SIZE);
                if (eVar3 != null) {
                    eVar2 = eVar3;
                }
            } else {
                eVar = null;
            }
            String a2 = j.a(str, lVar, eVar, eVar2);
            k a3 = k.a(a2.length(), lVar, eVar, eVar2, true);
            com.google.zxing.b.c.e eVar4 = new com.google.zxing.b.c.e(i.a(a2, a3), a3.b(), a3.c());
            eVar4.a();
            return a(eVar4, a3, i, i2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.zxing.f.c.b.a(int, int, boolean):void
     arg types: [int, int, int]
     candidates:
      com.google.zxing.f.c.b.a(int, int, int):void
      com.google.zxing.f.c.b.a(int, int, boolean):void */
    private static com.google.zxing.common.b a(com.google.zxing.b.c.e eVar, k kVar, int i, int i2) {
        com.google.zxing.common.b bVar;
        int b2 = kVar.b();
        int c = kVar.c();
        com.google.zxing.f.c.b bVar2 = new com.google.zxing.f.c.b(kVar.d(), kVar.e());
        int i3 = 0;
        for (int i4 = 0; i4 < c; i4++) {
            if (i4 % kVar.e == 0) {
                int i5 = 0;
                for (int i6 = 0; i6 < kVar.d(); i6++) {
                    bVar2.a(i5, i3, i6 % 2 == 0);
                    i5++;
                }
                i3++;
            }
            int i7 = 0;
            for (int i8 = 0; i8 < b2; i8++) {
                if (i8 % kVar.d == 0) {
                    bVar2.a(i7, i3, true);
                    i7++;
                }
                bVar2.a(i7, i3, eVar.f2928b[(eVar.f2927a * i4) + i8] == 1);
                i7++;
                if (i8 % kVar.d == kVar.d - 1) {
                    bVar2.a(i7, i3, i4 % 2 == 0);
                    i7++;
                }
            }
            i3++;
            if (i4 % kVar.e == kVar.e - 1) {
                int i9 = 0;
                for (int i10 = 0; i10 < kVar.d(); i10++) {
                    bVar2.a(i9, i3, true);
                    i9++;
                }
                i3++;
            }
        }
        int i11 = bVar2.f3137b;
        int i12 = bVar2.c;
        int max = Math.max(i, i11);
        int max2 = Math.max(i2, i12);
        int min = Math.min(max / i11, max2 / i12);
        int i13 = (max - (i11 * min)) / 2;
        int i14 = (max2 - (i12 * min)) / 2;
        if (i2 < i12 || i < i11) {
            bVar = new com.google.zxing.common.b(i11, i12);
            i13 = 0;
            i14 = 0;
        } else {
            bVar = new com.google.zxing.common.b(i, i2);
        }
        bVar.a();
        int i15 = 0;
        while (i15 < i12) {
            int i16 = i13;
            int i17 = 0;
            while (i17 < i11) {
                if (bVar2.a(i17, i15) == 1) {
                    bVar.a(i16, i14, min, min);
                }
                i17++;
                i16 += min;
            }
            i15++;
            i14 += min;
        }
        return bVar;
    }
}
