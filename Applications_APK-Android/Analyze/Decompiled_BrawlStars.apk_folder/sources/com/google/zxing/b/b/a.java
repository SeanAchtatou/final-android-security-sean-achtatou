package com.google.zxing.b.b;

import com.google.zxing.NotFoundException;
import com.google.zxing.common.i;
import com.google.zxing.p;
import java.io.Serializable;
import java.util.Comparator;
import java.util.Map;

/* compiled from: Detector */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    public final com.google.zxing.common.b f2923a;

    /* renamed from: b  reason: collision with root package name */
    public final com.google.zxing.common.a.b f2924b;

    public a(com.google.zxing.common.b bVar) throws NotFoundException {
        this.f2923a = bVar;
        this.f2924b = new com.google.zxing.common.a.b(bVar);
    }

    public static int a(p pVar, p pVar2) {
        return com.google.zxing.common.a.a.a(p.a(pVar, pVar2));
    }

    public static void a(Map<p, Integer> map, p pVar) {
        Integer num = map.get(pVar);
        int i = 1;
        if (num != null) {
            i = 1 + num.intValue();
        }
        map.put(pVar, Integer.valueOf(i));
    }

    public static com.google.zxing.common.b a(com.google.zxing.common.b bVar, p pVar, p pVar2, p pVar3, p pVar4, int i, int i2) throws NotFoundException {
        p pVar5 = pVar;
        p pVar6 = pVar2;
        p pVar7 = pVar3;
        p pVar8 = pVar4;
        float f = ((float) i) - 0.5f;
        float f2 = ((float) i2) - 0.5f;
        return i.a().a(bVar, i, i2, 0.5f, 0.5f, f, 0.5f, f, f2, 0.5f, f2, pVar5.f3153a, pVar5.f3154b, pVar8.f3153a, pVar8.f3154b, pVar7.f3153a, pVar7.f3154b, pVar6.f3153a, pVar6.f3154b);
    }

    /* renamed from: com.google.zxing.b.b.a$a  reason: collision with other inner class name */
    /* compiled from: Detector */
    public static final class C0130a {

        /* renamed from: a  reason: collision with root package name */
        public final p f2925a;

        /* renamed from: b  reason: collision with root package name */
        public final p f2926b;
        public final int c;

        /* synthetic */ C0130a(p pVar, p pVar2, int i, byte b2) {
            this(pVar, pVar2, i);
        }

        private C0130a(p pVar, p pVar2, int i) {
            this.f2925a = pVar;
            this.f2926b = pVar2;
            this.c = i;
        }

        public final String toString() {
            return this.f2925a + "/" + this.f2926b + '/' + this.c;
        }
    }

    /* compiled from: Detector */
    public static final class b implements Serializable, Comparator<C0130a> {
        private b() {
        }

        public /* synthetic */ b(byte b2) {
            this();
        }

        public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
            return ((C0130a) obj).c - ((C0130a) obj2).c;
        }
    }

    public boolean a(p pVar) {
        return pVar.f3153a >= 0.0f && pVar.f3153a < ((float) this.f2923a.f2968a) && pVar.f3154b > 0.0f && pVar.f3154b < ((float) this.f2923a.f2969b);
    }

    public C0130a b(p pVar, p pVar2) {
        a aVar = this;
        p pVar3 = pVar;
        p pVar4 = pVar2;
        int i = (int) pVar3.f3153a;
        int i2 = (int) pVar3.f3154b;
        int i3 = (int) pVar4.f3153a;
        int i4 = (int) pVar4.f3154b;
        boolean z = Math.abs(i4 - i2) > Math.abs(i3 - i);
        if (z) {
            int i5 = i2;
            i2 = i;
            i = i5;
            int i6 = i4;
            i4 = i3;
            i3 = i6;
        }
        int abs = Math.abs(i3 - i);
        int abs2 = Math.abs(i4 - i2);
        int i7 = (-abs) / 2;
        int i8 = -1;
        int i9 = i2 < i4 ? 1 : -1;
        if (i < i3) {
            i8 = 1;
        }
        boolean a2 = aVar.f2923a.a(z ? i2 : i, z ? i : i2);
        int i10 = 0;
        while (i != i3) {
            int i11 = i3;
            boolean a3 = aVar.f2923a.a(z ? i2 : i, z ? i : i2);
            if (a3 != a2) {
                i10++;
                a2 = a3;
            }
            i7 += abs2;
            if (i7 > 0) {
                if (i2 == i4) {
                    break;
                }
                i2 += i9;
                i7 -= abs;
            }
            i += i8;
            aVar = this;
            i3 = i11;
        }
        return new C0130a(pVar3, pVar4, i10, (byte) 0);
    }
}
