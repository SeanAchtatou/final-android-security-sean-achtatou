package com.google.zxing.b.c;

/* compiled from: X12Encoder */
final class n extends c {
    public final int a() {
        return 3;
    }

    n() {
    }

    public final void a(h hVar) {
        StringBuilder sb = new StringBuilder();
        while (true) {
            if (!hVar.b()) {
                break;
            }
            char a2 = hVar.a();
            hVar.f++;
            a(a2, sb);
            if (sb.length() % 3 == 0) {
                a(hVar, sb);
                if (j.a(hVar.f2929a, hVar.f, 3) != 3) {
                    hVar.g = 0;
                    break;
                }
            }
        }
        b(hVar, sb);
    }

    /* access modifiers changed from: package-private */
    public final int a(char c, StringBuilder sb) {
        if (c == 13) {
            sb.append(0);
        } else if (c == ' ') {
            sb.append(3);
        } else if (c == '*') {
            sb.append(1);
        } else if (c == '>') {
            sb.append(2);
        } else if (c >= '0' && c <= '9') {
            sb.append((char) ((c - '0') + 4));
        } else if (c < 'A' || c > 'Z') {
            j.c(c);
        } else {
            sb.append((char) ((c - 'A') + 14));
        }
        return 1;
    }

    /* access modifiers changed from: package-private */
    public final void b(h hVar, StringBuilder sb) {
        hVar.d();
        int length = hVar.h.f2934b - hVar.e.length();
        hVar.f -= sb.length();
        if (hVar.c() > 1 || length > 1 || hVar.c() != length) {
            hVar.a(254);
        }
        if (hVar.g < 0) {
            hVar.g = 0;
        }
    }
}
