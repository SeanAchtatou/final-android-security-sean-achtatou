package com.google.zxing.b.c;

/* compiled from: EdifactEncoder */
final class f implements g {
    f() {
    }

    public final void a(h hVar) {
        boolean z;
        StringBuilder sb = new StringBuilder();
        while (true) {
            z = true;
            if (!hVar.b()) {
                break;
            }
            char a2 = hVar.a();
            if (a2 >= ' ' && a2 <= '?') {
                sb.append(a2);
            } else if (a2 < '@' || a2 > '^') {
                j.c(a2);
            } else {
                sb.append((char) (a2 - '@'));
            }
            hVar.f++;
            if (sb.length() >= 4) {
                hVar.a(a(sb, 0));
                sb.delete(0, 4);
                if (j.a(hVar.f2929a, hVar.f, 4) != 4) {
                    hVar.g = 0;
                    break;
                }
            }
        }
        sb.append(31);
        try {
            int length = sb.length();
            if (length != 0) {
                if (length == 1) {
                    hVar.d();
                    int length2 = hVar.h.f2934b - hVar.e.length();
                    if (hVar.c() <= length2 && length2 <= 2) {
                        hVar.g = 0;
                        return;
                    }
                }
                if (length <= 4) {
                    int i = length - 1;
                    String a3 = a(sb, 0);
                    if (!(!hVar.b()) || i > 2) {
                        z = false;
                    }
                    if (i <= 2) {
                        hVar.a(hVar.e.length() + i);
                        if (hVar.h.f2934b - hVar.e.length() >= 3) {
                            hVar.a(hVar.e.length() + a3.length());
                            z = false;
                        }
                    }
                    if (z) {
                        hVar.h = null;
                        hVar.f -= i;
                    } else {
                        hVar.a(a3);
                    }
                    hVar.g = 0;
                    return;
                }
                throw new IllegalStateException("Count must not exceed 4");
            }
        } finally {
            hVar.g = 0;
        }
    }

    private static String a(CharSequence charSequence, int i) {
        char c = 0;
        int length = charSequence.length() - 0;
        if (length != 0) {
            char charAt = charSequence.charAt(0);
            char charAt2 = length >= 2 ? charSequence.charAt(1) : 0;
            char charAt3 = length >= 3 ? charSequence.charAt(2) : 0;
            if (length >= 4) {
                c = charSequence.charAt(3);
            }
            int i2 = (charAt << 18) + (charAt2 << 12) + (charAt3 << 6) + c;
            char c2 = (char) ((i2 >> 8) & 255);
            char c3 = (char) (i2 & 255);
            StringBuilder sb = new StringBuilder(3);
            sb.append((char) ((i2 >> 16) & 255));
            if (length >= 2) {
                sb.append(c2);
            }
            if (length >= 3) {
                sb.append(c3);
            }
            return sb.toString();
        }
        throw new IllegalStateException("StringBuilder must not be empty");
    }
}
