package com.google.zxing;

public final class NotFoundException extends ReaderException {
    private static final NotFoundException c;

    static {
        NotFoundException notFoundException = new NotFoundException();
        c = notFoundException;
        notFoundException.setStackTrace(f2880b);
    }

    private NotFoundException() {
    }

    public static NotFoundException a() {
        return c;
    }
}
