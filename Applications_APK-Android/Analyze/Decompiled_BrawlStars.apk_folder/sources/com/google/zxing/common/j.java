package com.google.zxing.common;

import com.google.zxing.NotFoundException;
import com.google.zxing.b;
import com.google.zxing.h;
import java.lang.reflect.Array;

/* compiled from: HybridBinarizer */
public final class j extends h {

    /* renamed from: b  reason: collision with root package name */
    private b f2978b;

    private static int a(int i, int i2, int i3) {
        if (i < 2) {
            return 2;
        }
        return i > i3 ? i3 : i;
    }

    public j(h hVar) {
        super(hVar);
    }

    public final b a() throws NotFoundException {
        j jVar;
        b bVar = this.f2978b;
        if (bVar != null) {
            return bVar;
        }
        h hVar = this.f2904a;
        int i = hVar.f3144a;
        int i2 = hVar.f3145b;
        if (i < 40 || i2 < 40) {
            jVar = this;
            jVar.f2978b = super.a();
        } else {
            byte[] a2 = hVar.a();
            int i3 = i >> 3;
            if ((i & 7) != 0) {
                i3++;
            }
            int i4 = i2 >> 3;
            if ((i2 & 7) != 0) {
                i4++;
            }
            int i5 = i2 - 8;
            int i6 = i - 8;
            int[][] iArr = (int[][]) Array.newInstance(int.class, i4, i3);
            int i7 = 0;
            while (true) {
                int i8 = 8;
                if (i7 >= i4) {
                    break;
                }
                int i9 = i7 << 3;
                if (i9 > i5) {
                    i9 = i5;
                }
                int i10 = 0;
                while (i10 < i3) {
                    int i11 = i10 << 3;
                    if (i11 > i6) {
                        i11 = i6;
                    }
                    int i12 = (i9 * i) + i11;
                    int i13 = 0;
                    byte b2 = 255;
                    int i14 = 0;
                    byte b3 = 0;
                    while (i13 < i8) {
                        int i15 = i9;
                        byte b4 = b3;
                        byte b5 = b2;
                        int i16 = 0;
                        while (i16 < i8) {
                            byte b6 = a2[i12 + i16] & 255;
                            i14 += b6;
                            if (b6 < b5) {
                                b5 = b6;
                            }
                            if (b6 > b4) {
                                b4 = b6;
                            }
                            i16++;
                            i8 = 8;
                        }
                        if (b4 - b5 <= 24) {
                            i13++;
                            i12 += i;
                            i8 = 8;
                            b3 = b4;
                            b2 = b5;
                            i9 = i15;
                        }
                        while (true) {
                            i13++;
                            i12 += i;
                            if (i13 >= 8) {
                                break;
                            }
                            int i17 = 0;
                            for (int i18 = 8; i17 < i18; i18 = 8) {
                                i14 += a2[i12 + i17] & 255;
                                i17++;
                            }
                        }
                        i13++;
                        i12 += i;
                        i8 = 8;
                        b3 = b4;
                        b2 = b5;
                        i9 = i15;
                    }
                    int i19 = i9;
                    int i20 = i14 >> 6;
                    if (b3 - b2 <= 24) {
                        i20 = b2 / 2;
                        if (i7 > 0 && i10 > 0) {
                            int i21 = i7 - 1;
                            int i22 = i10 - 1;
                            int i23 = ((iArr[i21][i10] + (iArr[i7][i22] * 2)) + iArr[i21][i22]) / 4;
                            if (b2 < i23) {
                                i20 = i23;
                            }
                        }
                    }
                    iArr[i7][i10] = i20;
                    i10++;
                    i8 = 8;
                    i9 = i19;
                }
                i7++;
            }
            b bVar2 = new b(i, i2);
            for (int i24 = 0; i24 < i4; i24++) {
                int i25 = i24 << 3;
                if (i25 > i5) {
                    i25 = i5;
                }
                int i26 = 2;
                int a3 = a(i24, 2, i4 - 3);
                int i27 = 0;
                while (i27 < i3) {
                    int i28 = i27 << 3;
                    if (i28 > i6) {
                        i28 = i6;
                    }
                    int a4 = a(i27, i26, i3 - 3);
                    int i29 = -2;
                    int i30 = 0;
                    while (i29 <= i26) {
                        int[] iArr2 = iArr[a3 + i29];
                        i30 += iArr2[a4 - 2] + iArr2[a4 - 1] + iArr2[a4] + iArr2[a4 + 1] + iArr2[a4 + 2];
                        i29++;
                        i26 = 2;
                    }
                    int i31 = i30 / 25;
                    int i32 = i3;
                    int i33 = (i25 * i) + i28;
                    int i34 = 8;
                    int i35 = 0;
                    while (i35 < i34) {
                        int i36 = i4;
                        int i37 = 0;
                        while (i37 < i34) {
                            byte[] bArr = a2;
                            if ((a2[i33 + i37] & 255) <= i31) {
                                bVar2.b(i28 + i37, i25 + i35);
                            }
                            i37++;
                            a2 = bArr;
                            i34 = 8;
                        }
                        i35++;
                        i33 += i;
                        i4 = i36;
                        i34 = 8;
                    }
                    i27++;
                    i3 = i32;
                    i26 = 2;
                }
            }
            jVar = this;
            jVar.f2978b = bVar2;
        }
        return jVar.f2978b;
    }

    public final b a(h hVar) {
        return new j(hVar);
    }
}
