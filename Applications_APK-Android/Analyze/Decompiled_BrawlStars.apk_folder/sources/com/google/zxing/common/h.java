package com.google.zxing.common;

import com.google.zxing.NotFoundException;
import com.google.zxing.b;

/* compiled from: GlobalHistogramBinarizer */
public class h extends b {

    /* renamed from: b  reason: collision with root package name */
    private static final byte[] f2976b = new byte[0];
    private byte[] c = f2976b;
    private final int[] d = new int[32];

    public h(com.google.zxing.h hVar) {
        super(hVar);
    }

    public b a(com.google.zxing.h hVar) {
        return new h(hVar);
    }

    private void a(int i) {
        if (this.c.length < i) {
            this.c = new byte[i];
        }
        for (int i2 = 0; i2 < 32; i2++) {
            this.d[i2] = 0;
        }
    }

    private static int a(int[] iArr) throws NotFoundException {
        int length = iArr.length;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        for (int i4 = 0; i4 < length; i4++) {
            if (iArr[i4] > i) {
                i = iArr[i4];
                i3 = i4;
            }
            if (iArr[i4] > i2) {
                i2 = iArr[i4];
            }
        }
        int i5 = 0;
        int i6 = 0;
        for (int i7 = 0; i7 < length; i7++) {
            int i8 = i7 - i3;
            int i9 = iArr[i7] * i8 * i8;
            if (i9 > i6) {
                i5 = i7;
                i6 = i9;
            }
        }
        if (i3 > i5) {
            int i10 = i3;
            i3 = i5;
            i5 = i10;
        }
        if (i5 - i3 > length / 16) {
            int i11 = i5 - 1;
            int i12 = i11;
            int i13 = -1;
            while (i11 > i3) {
                int i14 = i11 - i3;
                int i15 = i14 * i14 * (i5 - i11) * (i2 - iArr[i11]);
                if (i15 > i13) {
                    i12 = i11;
                    i13 = i15;
                }
                i11--;
            }
            return i12 << 3;
        }
        throw NotFoundException.a();
    }

    public final a a(int i, a aVar) throws NotFoundException {
        com.google.zxing.h hVar = this.f2904a;
        int i2 = hVar.f3144a;
        if (aVar == null || aVar.f2965b < i2) {
            aVar = new a(i2);
        } else {
            aVar.b();
        }
        a(i2);
        byte[] a2 = hVar.a(i, this.c);
        int[] iArr = this.d;
        for (int i3 = 0; i3 < i2; i3++) {
            int i4 = (a2[i3] & 255) >> 3;
            iArr[i4] = iArr[i4] + 1;
        }
        int a3 = a(iArr);
        if (i2 < 3) {
            for (int i5 = 0; i5 < i2; i5++) {
                if ((a2[i5] & 255) < a3) {
                    aVar.b(i5);
                }
            }
        } else {
            byte b2 = a2[1] & 255;
            byte b3 = a2[0] & 255;
            int i6 = 1;
            while (i6 < i2 - 1) {
                int i7 = i6 + 1;
                byte b4 = a2[i7] & 255;
                if ((((b2 << 2) - b3) - b4) / 2 < a3) {
                    aVar.b(i6);
                }
                b3 = b2;
                i6 = i7;
                b2 = b4;
            }
        }
        return aVar;
    }

    public b a() throws NotFoundException {
        com.google.zxing.h hVar = this.f2904a;
        int i = hVar.f3144a;
        int i2 = hVar.f3145b;
        b bVar = new b(i, i2);
        a(i);
        int[] iArr = this.d;
        for (int i3 = 1; i3 < 5; i3++) {
            byte[] a2 = hVar.a((i2 * i3) / 5, this.c);
            int i4 = (i << 2) / 5;
            for (int i5 = i / 5; i5 < i4; i5++) {
                int i6 = (a2[i5] & 255) >> 3;
                iArr[i6] = iArr[i6] + 1;
            }
        }
        int a3 = a(iArr);
        byte[] a4 = hVar.a();
        for (int i7 = 0; i7 < i2; i7++) {
            int i8 = i7 * i;
            for (int i9 = 0; i9 < i; i9++) {
                if ((a4[i8 + i9] & 255) < a3) {
                    bVar.b(i9, i7);
                }
            }
        }
        return bVar;
    }
}
