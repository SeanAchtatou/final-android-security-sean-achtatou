package com.google.zxing.common;

/* compiled from: BitSource */
public final class c {

    /* renamed from: a  reason: collision with root package name */
    public int f2970a;

    /* renamed from: b  reason: collision with root package name */
    public int f2971b;
    private final byte[] c;

    public c(byte[] bArr) {
        this.c = bArr;
    }

    public final int a(int i) {
        byte b2;
        if (i <= 0 || i > 32 || i > a()) {
            throw new IllegalArgumentException(String.valueOf(i));
        }
        int i2 = this.f2971b;
        if (i2 > 0) {
            int i3 = 8 - i2;
            int i4 = i < i3 ? i : i3;
            int i5 = i3 - i4;
            byte[] bArr = this.c;
            int i6 = this.f2970a;
            b2 = (((255 >> (8 - i4)) << i5) & bArr[i6]) >> i5;
            i -= i4;
            this.f2971b += i4;
            if (this.f2971b == 8) {
                this.f2971b = 0;
                this.f2970a = i6 + 1;
            }
        } else {
            b2 = 0;
        }
        if (i <= 0) {
            return b2;
        }
        while (i >= 8) {
            byte[] bArr2 = this.c;
            int i7 = this.f2970a;
            b2 = (b2 << 8) | (bArr2[i7] & 255);
            this.f2970a = i7 + 1;
            i -= 8;
        }
        if (i <= 0) {
            return b2;
        }
        int i8 = 8 - i;
        int i9 = (b2 << i) | ((((255 >> i8) << i8) & this.c[this.f2970a]) >> i8);
        this.f2971b += i;
        return i9;
    }

    public final int a() {
        return ((this.c.length - this.f2970a) * 8) - this.f2971b;
    }
}
