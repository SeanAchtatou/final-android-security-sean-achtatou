package com.google.zxing.common.reedsolomon;

/* compiled from: ReedSolomonDecoder */
public final class c {

    /* renamed from: a  reason: collision with root package name */
    private final a f2987a;

    public c(a aVar) {
        this.f2987a = aVar;
    }

    public final void a(int[] iArr, int i) throws ReedSolomonException {
        b bVar = new b(this.f2987a, iArr);
        int[] iArr2 = new int[i];
        int i2 = 0;
        boolean z = true;
        for (int i3 = 0; i3 < i; i3++) {
            a aVar = this.f2987a;
            int b2 = bVar.b(aVar.i[aVar.m + i3]);
            iArr2[(i - 1) - i3] = b2;
            if (b2 != 0) {
                z = false;
            }
        }
        if (!z) {
            b[] a2 = a(this.f2987a.a(i, 1), new b(this.f2987a, iArr2), i);
            b bVar2 = a2[0];
            b bVar3 = a2[1];
            int[] a3 = a(bVar2);
            int[] a4 = a(bVar3, a3);
            while (i2 < a3.length) {
                int length = (iArr.length - 1) - this.f2987a.a(a3[i2]);
                if (length >= 0) {
                    iArr[length] = a.b(iArr[length], a4[i2]);
                    i2++;
                } else {
                    throw new ReedSolomonException("Bad error location");
                }
            }
        }
    }

    private int[] a(b bVar, int[] iArr) {
        int length = iArr.length;
        int[] iArr2 = new int[length];
        for (int i = 0; i < length; i++) {
            int b2 = this.f2987a.b(iArr[i]);
            int i2 = 1;
            for (int i3 = 0; i3 < length; i3++) {
                if (i != i3) {
                    int c = this.f2987a.c(iArr[i3], b2);
                    i2 = this.f2987a.c(i2, (c & 1) == 0 ? c | 1 : c & -2);
                }
            }
            iArr2[i] = this.f2987a.c(bVar.b(b2), this.f2987a.b(i2));
            if (this.f2987a.m != 0) {
                iArr2[i] = this.f2987a.c(iArr2[i], b2);
            }
        }
        return iArr2;
    }

    private b[] a(b bVar, b bVar2, int i) throws ReedSolomonException {
        if (bVar.f2985a.length - 1 < bVar2.f2985a.length - 1) {
            b bVar3 = bVar2;
            bVar2 = bVar;
            bVar = bVar3;
        }
        b bVar4 = this.f2987a.j;
        b bVar5 = this.f2987a.k;
        do {
            b bVar6 = r12;
            r12 = bVar;
            bVar = bVar6;
            b bVar7 = bVar5;
            b bVar8 = bVar4;
            bVar4 = bVar7;
            if (bVar.f2985a.length - 1 < i / 2) {
                int a2 = bVar4.a(0);
                if (a2 != 0) {
                    int b2 = this.f2987a.b(a2);
                    return new b[]{bVar4.c(b2), bVar.c(b2)};
                }
                throw new ReedSolomonException("sigmaTilde(0) was zero");
            } else if (!bVar.a()) {
                b bVar9 = this.f2987a.j;
                int b3 = this.f2987a.b(bVar.a(bVar.f2985a.length - 1));
                while (r12.f2985a.length - 1 >= bVar.f2985a.length - 1 && !r12.a()) {
                    int length = (r12.f2985a.length - 1) - (bVar.f2985a.length - 1);
                    int c = this.f2987a.c(r12.a(r12.f2985a.length - 1), b3);
                    bVar9 = bVar9.a(this.f2987a.a(length, c));
                    r12 = r12.a(bVar.a(length, c));
                }
                bVar5 = bVar9.b(bVar4).a(bVar8);
            } else {
                throw new ReedSolomonException("r_{i-1} was zero");
            }
        } while (r12.f2985a.length - 1 < bVar.f2985a.length - 1);
        throw new IllegalStateException("Division algorithm failed to reduce polynomial?");
    }

    private int[] a(b bVar) throws ReedSolomonException {
        int length = bVar.f2985a.length - 1;
        int i = 0;
        if (length == 1) {
            return new int[]{bVar.a(1)};
        }
        int[] iArr = new int[length];
        for (int i2 = 1; i2 < this.f2987a.l && i < length; i2++) {
            if (bVar.b(i2) == 0) {
                iArr[i] = this.f2987a.b(i2);
                i++;
            }
        }
        if (i == length) {
            return iArr;
        }
        throw new ReedSolomonException("Error locator degree does not match number of roots");
    }
}
