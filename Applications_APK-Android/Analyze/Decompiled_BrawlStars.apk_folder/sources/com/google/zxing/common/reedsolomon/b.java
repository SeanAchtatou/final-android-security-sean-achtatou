package com.google.zxing.common.reedsolomon;

/* compiled from: GenericGFPoly */
final class b {

    /* renamed from: a  reason: collision with root package name */
    final int[] f2985a;

    /* renamed from: b  reason: collision with root package name */
    private final a f2986b;

    b(a aVar, int[] iArr) {
        if (iArr.length != 0) {
            this.f2986b = aVar;
            int length = iArr.length;
            if (length <= 1 || iArr[0] != 0) {
                this.f2985a = iArr;
                return;
            }
            int i = 1;
            while (i < length && iArr[i] == 0) {
                i++;
            }
            if (i == length) {
                this.f2985a = new int[]{0};
                return;
            }
            this.f2985a = new int[(length - i)];
            int[] iArr2 = this.f2985a;
            System.arraycopy(iArr, i, iArr2, 0, iArr2.length);
            return;
        }
        throw new IllegalArgumentException();
    }

    /* access modifiers changed from: package-private */
    public final boolean a() {
        return this.f2985a[0] == 0;
    }

    /* access modifiers changed from: package-private */
    public final int a(int i) {
        int[] iArr = this.f2985a;
        return iArr[(iArr.length - 1) - i];
    }

    /* access modifiers changed from: package-private */
    public final int b(int i) {
        if (i == 0) {
            return a(0);
        }
        if (i == 1) {
            int i2 = 0;
            for (int b2 : this.f2985a) {
                i2 = a.b(i2, b2);
            }
            return i2;
        }
        int[] iArr = this.f2985a;
        int i3 = iArr[0];
        int length = iArr.length;
        for (int i4 = 1; i4 < length; i4++) {
            i3 = a.b(this.f2986b.c(i, i3), this.f2985a[i4]);
        }
        return i3;
    }

    /* access modifiers changed from: package-private */
    public final b a(b bVar) {
        if (!this.f2986b.equals(bVar.f2986b)) {
            throw new IllegalArgumentException("GenericGFPolys do not have same GenericGF field");
        } else if (a()) {
            return bVar;
        } else {
            if (bVar.a()) {
                return this;
            }
            int[] iArr = this.f2985a;
            int[] iArr2 = bVar.f2985a;
            if (iArr.length > iArr2.length) {
                int[] iArr3 = iArr;
                iArr = iArr2;
                iArr2 = iArr3;
            }
            int[] iArr4 = new int[iArr2.length];
            int length = iArr2.length - iArr.length;
            System.arraycopy(iArr2, 0, iArr4, 0, length);
            for (int i = length; i < iArr2.length; i++) {
                iArr4[i] = a.b(iArr[i - length], iArr2[i]);
            }
            return new b(this.f2986b, iArr4);
        }
    }

    /* access modifiers changed from: package-private */
    public final b b(b bVar) {
        if (!this.f2986b.equals(bVar.f2986b)) {
            throw new IllegalArgumentException("GenericGFPolys do not have same GenericGF field");
        } else if (a() || bVar.a()) {
            return this.f2986b.j;
        } else {
            int[] iArr = this.f2985a;
            int length = iArr.length;
            int[] iArr2 = bVar.f2985a;
            int length2 = iArr2.length;
            int[] iArr3 = new int[((length + length2) - 1)];
            for (int i = 0; i < length; i++) {
                int i2 = iArr[i];
                for (int i3 = 0; i3 < length2; i3++) {
                    int i4 = i + i3;
                    iArr3[i4] = a.b(iArr3[i4], this.f2986b.c(i2, iArr2[i3]));
                }
            }
            return new b(this.f2986b, iArr3);
        }
    }

    /* access modifiers changed from: package-private */
    public final b c(int i) {
        if (i == 0) {
            return this.f2986b.j;
        }
        if (i == 1) {
            return this;
        }
        int length = this.f2985a.length;
        int[] iArr = new int[length];
        for (int i2 = 0; i2 < length; i2++) {
            iArr[i2] = this.f2986b.c(this.f2985a[i2], i);
        }
        return new b(this.f2986b, iArr);
    }

    /* access modifiers changed from: package-private */
    public final b a(int i, int i2) {
        if (i < 0) {
            throw new IllegalArgumentException();
        } else if (i2 == 0) {
            return this.f2986b.j;
        } else {
            int length = this.f2985a.length;
            int[] iArr = new int[(i + length)];
            for (int i3 = 0; i3 < length; i3++) {
                iArr[i3] = this.f2986b.c(this.f2985a[i3], i2);
            }
            return new b(this.f2986b, iArr);
        }
    }

    /* access modifiers changed from: package-private */
    public final b[] c(b bVar) {
        if (!this.f2986b.equals(bVar.f2986b)) {
            throw new IllegalArgumentException("GenericGFPolys do not have same GenericGF field");
        } else if (!bVar.a()) {
            b bVar2 = this.f2986b.j;
            int b2 = this.f2986b.b(bVar.a(bVar.f2985a.length - 1));
            b bVar3 = bVar2;
            b bVar4 = this;
            while (bVar4.f2985a.length - 1 >= bVar.f2985a.length - 1 && !bVar4.a()) {
                int[] iArr = bVar4.f2985a;
                int length = (iArr.length - 1) - (bVar.f2985a.length - 1);
                int c = this.f2986b.c(bVar4.a(iArr.length - 1), b2);
                b a2 = bVar.a(length, c);
                bVar3 = bVar3.a(this.f2986b.a(length, c));
                bVar4 = bVar4.a(a2);
            }
            return new b[]{bVar3, bVar4};
        } else {
            throw new IllegalArgumentException("Divide by 0");
        }
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder((this.f2985a.length - 1) * 8);
        for (int length = this.f2985a.length - 1; length >= 0; length--) {
            int a2 = a(length);
            if (a2 != 0) {
                if (a2 < 0) {
                    sb.append(" - ");
                    a2 = -a2;
                } else if (sb.length() > 0) {
                    sb.append(" + ");
                }
                if (length == 0 || a2 != 1) {
                    int a3 = this.f2986b.a(a2);
                    if (a3 == 0) {
                        sb.append('1');
                    } else if (a3 == 1) {
                        sb.append('a');
                    } else {
                        sb.append("a^");
                        sb.append(a3);
                    }
                }
                if (length != 0) {
                    if (length == 1) {
                        sb.append('x');
                    } else {
                        sb.append("x^");
                        sb.append(length);
                    }
                }
            }
        }
        return sb.toString();
    }
}
