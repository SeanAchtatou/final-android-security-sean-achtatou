package com.google.zxing.common.reedsolomon;

import java.util.ArrayList;
import java.util.List;

/* compiled from: ReedSolomonEncoder */
public final class d {

    /* renamed from: a  reason: collision with root package name */
    private final a f2988a;

    /* renamed from: b  reason: collision with root package name */
    private final List<b> f2989b = new ArrayList();

    public d(a aVar) {
        this.f2988a = aVar;
        this.f2989b.add(new b(aVar, new int[]{1}));
    }

    private b a(int i) {
        if (i >= this.f2989b.size()) {
            List<b> list = this.f2989b;
            b bVar = list.get(list.size() - 1);
            for (int size = this.f2989b.size(); size <= i; size++) {
                a aVar = this.f2988a;
                bVar = bVar.b(new b(aVar, new int[]{1, aVar.i[(size - 1) + aVar.m]}));
                this.f2989b.add(bVar);
            }
        }
        return this.f2989b.get(i);
    }

    public final void a(int[] iArr, int i) {
        if (i != 0) {
            int length = iArr.length - i;
            if (length > 0) {
                b a2 = a(i);
                int[] iArr2 = new int[length];
                System.arraycopy(iArr, 0, iArr2, 0, length);
                int[] iArr3 = new b(this.f2988a, iArr2).a(i, 1).c(a2)[1].f2985a;
                int length2 = i - iArr3.length;
                for (int i2 = 0; i2 < length2; i2++) {
                    iArr[length + i2] = 0;
                }
                System.arraycopy(iArr3, 0, iArr, length + length2, iArr3.length);
                return;
            }
            throw new IllegalArgumentException("No data bytes provided");
        }
        throw new IllegalArgumentException("No error correction bytes");
    }
}
