package com.google.zxing.common;

import java.util.Arrays;

/* compiled from: BitArray */
public final class a implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    public int[] f2964a;

    /* renamed from: b  reason: collision with root package name */
    public int f2965b;

    public a() {
        this.f2965b = 0;
        this.f2964a = new int[1];
    }

    public a(int i) {
        this.f2965b = i;
        this.f2964a = f(i);
    }

    private a(int[] iArr, int i) {
        this.f2964a = iArr;
        this.f2965b = i;
    }

    public final int a() {
        return (this.f2965b + 7) / 8;
    }

    private void e(int i) {
        if (i > (this.f2964a.length << 5)) {
            int[] f = f(i);
            int[] iArr = this.f2964a;
            System.arraycopy(iArr, 0, f, 0, iArr.length);
            this.f2964a = f;
        }
    }

    public final boolean a(int i) {
        return ((1 << (i & 31)) & this.f2964a[i / 32]) != 0;
    }

    public final void b(int i) {
        int[] iArr = this.f2964a;
        int i2 = i / 32;
        iArr[i2] = (1 << (i & 31)) | iArr[i2];
    }

    public final int c(int i) {
        int i2 = this.f2965b;
        if (i >= i2) {
            return i2;
        }
        int i3 = i / 32;
        int i4 = (((1 << (i & 31)) - 1) ^ -1) & this.f2964a[i3];
        while (i4 == 0) {
            i3++;
            int[] iArr = this.f2964a;
            if (i3 == iArr.length) {
                return this.f2965b;
            }
            i4 = iArr[i3];
        }
        int numberOfTrailingZeros = (i3 << 5) + Integer.numberOfTrailingZeros(i4);
        int i5 = this.f2965b;
        return numberOfTrailingZeros > i5 ? i5 : numberOfTrailingZeros;
    }

    public final int d(int i) {
        int i2 = this.f2965b;
        if (i >= i2) {
            return i2;
        }
        int i3 = i / 32;
        int i4 = (((1 << (i & 31)) - 1) ^ -1) & (this.f2964a[i3] ^ -1);
        while (i4 == 0) {
            i3++;
            int[] iArr = this.f2964a;
            if (i3 == iArr.length) {
                return this.f2965b;
            }
            i4 = iArr[i3] ^ -1;
        }
        int numberOfTrailingZeros = (i3 << 5) + Integer.numberOfTrailingZeros(i4);
        int i5 = this.f2965b;
        return numberOfTrailingZeros > i5 ? i5 : numberOfTrailingZeros;
    }

    public final void b() {
        int length = this.f2964a.length;
        for (int i = 0; i < length; i++) {
            this.f2964a[i] = 0;
        }
    }

    public final boolean a(int i, int i2, boolean z) {
        if (i2 < i || i < 0 || i2 > this.f2965b) {
            throw new IllegalArgumentException();
        } else if (i2 == i) {
            return true;
        } else {
            int i3 = i2 - 1;
            int i4 = i / 32;
            int i5 = i3 / 32;
            int i6 = i4;
            while (i6 <= i5) {
                int i7 = 31;
                int i8 = i6 > i4 ? 0 : i & 31;
                if (i6 >= i5) {
                    i7 = 31 & i3;
                }
                if ((((2 << i7) - (1 << i8)) & this.f2964a[i6]) != 0) {
                    return false;
                }
                i6++;
            }
            return true;
        }
    }

    public final void a(boolean z) {
        e(this.f2965b + 1);
        if (z) {
            int[] iArr = this.f2964a;
            int i = this.f2965b;
            int i2 = i / 32;
            iArr[i2] = (1 << (i & 31)) | iArr[i2];
        }
        this.f2965b++;
    }

    public final void a(int i, int i2) {
        if (i2 < 0 || i2 > 32) {
            throw new IllegalArgumentException("Num bits must be between 0 and 32");
        }
        e(this.f2965b + i2);
        while (i2 > 0) {
            boolean z = true;
            if (((i >> (i2 - 1)) & 1) != 1) {
                z = false;
            }
            a(z);
            i2--;
        }
    }

    public final void a(a aVar) {
        int i = aVar.f2965b;
        e(this.f2965b + i);
        for (int i2 = 0; i2 < i; i2++) {
            a(aVar.a(i2));
        }
    }

    public final void a(int i, byte[] bArr, int i2, int i3) {
        int i4 = i;
        int i5 = 0;
        while (i5 < i3) {
            int i6 = i4;
            int i7 = 0;
            for (int i8 = 0; i8 < 8; i8++) {
                if (a(i6)) {
                    i7 |= 1 << (7 - i8);
                }
                i6++;
            }
            bArr[i5 + 0] = (byte) i7;
            i5++;
            i4 = i6;
        }
    }

    public final void c() {
        int[] iArr = new int[this.f2964a.length];
        int i = (this.f2965b - 1) / 32;
        int i2 = i + 1;
        for (int i3 = 0; i3 < i2; i3++) {
            long j = (long) this.f2964a[i3];
            long j2 = ((j & 1431655765) << 1) | ((j >> 1) & 1431655765);
            long j3 = ((j2 & 858993459) << 2) | ((j2 >> 2) & 858993459);
            long j4 = ((j3 & 252645135) << 4) | ((j3 >> 4) & 252645135);
            long j5 = ((j4 & 16711935) << 8) | ((j4 >> 8) & 16711935);
            iArr[i - i3] = (int) (((j5 & 65535) << 16) | ((j5 >> 16) & 65535));
        }
        int i4 = this.f2965b;
        int i5 = i2 << 5;
        if (i4 != i5) {
            int i6 = i5 - i4;
            int i7 = iArr[0] >>> i6;
            for (int i8 = 1; i8 < i2; i8++) {
                int i9 = iArr[i8];
                iArr[i8 - 1] = i7 | (i9 << (32 - i6));
                i7 = i9 >>> i6;
            }
            iArr[i2 - 1] = i7;
        }
        this.f2964a = iArr;
    }

    private static int[] f(int i) {
        return new int[((i + 31) / 32)];
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof a)) {
            return false;
        }
        a aVar = (a) obj;
        if (this.f2965b != aVar.f2965b || !Arrays.equals(this.f2964a, aVar.f2964a)) {
            return false;
        }
        return true;
    }

    public final int hashCode() {
        return (this.f2965b * 31) + Arrays.hashCode(this.f2964a);
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder(this.f2965b);
        for (int i = 0; i < this.f2965b; i++) {
            if ((i & 7) == 0) {
                sb.append(' ');
            }
            sb.append(a(i) ? 'X' : '.');
        }
        return sb.toString();
    }

    public final /* synthetic */ Object clone() throws CloneNotSupportedException {
        return new a((int[]) this.f2964a.clone(), this.f2965b);
    }
}
