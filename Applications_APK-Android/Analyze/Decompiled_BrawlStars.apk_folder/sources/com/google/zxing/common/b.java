package com.google.zxing.common;

import java.util.Arrays;

/* compiled from: BitMatrix */
public final class b implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    public final int f2968a;

    /* renamed from: b  reason: collision with root package name */
    public final int f2969b;
    public final int c;
    public final int[] d;

    public b(int i) {
        this(i, i);
    }

    public b(int i, int i2) {
        if (i <= 0 || i2 <= 0) {
            throw new IllegalArgumentException("Both dimensions must be greater than 0");
        }
        this.f2968a = i;
        this.f2969b = i2;
        this.c = (i + 31) / 32;
        this.d = new int[(this.c * i2)];
    }

    private b(int i, int i2, int i3, int[] iArr) {
        this.f2968a = i;
        this.f2969b = i2;
        this.c = i3;
        this.d = iArr;
    }

    public final boolean a(int i, int i2) {
        return ((this.d[(i2 * this.c) + (i / 32)] >>> (i & 31)) & 1) != 0;
    }

    public final void b(int i, int i2) {
        int i3 = (i2 * this.c) + (i / 32);
        int[] iArr = this.d;
        iArr[i3] = (1 << (i & 31)) | iArr[i3];
    }

    public final void c(int i, int i2) {
        int i3 = (i2 * this.c) + (i / 32);
        int[] iArr = this.d;
        iArr[i3] = (1 << (i & 31)) ^ iArr[i3];
    }

    public final void a() {
        int length = this.d.length;
        for (int i = 0; i < length; i++) {
            this.d[i] = 0;
        }
    }

    public final void a(int i, int i2, int i3, int i4) {
        if (i2 < 0 || i < 0) {
            throw new IllegalArgumentException("Left and top must be nonnegative");
        } else if (i4 <= 0 || i3 <= 0) {
            throw new IllegalArgumentException("Height and width must be at least 1");
        } else {
            int i5 = i3 + i;
            int i6 = i4 + i2;
            if (i6 > this.f2969b || i5 > this.f2968a) {
                throw new IllegalArgumentException("The region must fit inside the matrix");
            }
            while (i2 < i6) {
                int i7 = this.c * i2;
                for (int i8 = i; i8 < i5; i8++) {
                    int[] iArr = this.d;
                    int i9 = (i8 / 32) + i7;
                    iArr[i9] = iArr[i9] | (1 << (i8 & 31));
                }
                i2++;
            }
        }
    }

    public final int[] b() {
        int i = 0;
        while (true) {
            int[] iArr = this.d;
            if (i >= iArr.length || iArr[i] != 0) {
                int[] iArr2 = this.d;
            } else {
                i++;
            }
        }
        int[] iArr22 = this.d;
        if (i == iArr22.length) {
            return null;
        }
        int i2 = this.c;
        int i3 = i / i2;
        int i4 = (i % i2) << 5;
        int i5 = iArr22[i];
        int i6 = 0;
        while ((i5 << (31 - i6)) == 0) {
            i6++;
        }
        return new int[]{i4 + i6, i3};
    }

    public final int[] c() {
        int length = this.d.length - 1;
        while (length >= 0 && this.d[length] == 0) {
            length--;
        }
        if (length < 0) {
            return null;
        }
        int i = this.c;
        int i2 = length / i;
        int i3 = (length % i) << 5;
        int i4 = 31;
        while ((this.d[length] >>> i4) == 0) {
            i4--;
        }
        return new int[]{i3 + i4, i2};
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        if (this.f2968a == bVar.f2968a && this.f2969b == bVar.f2969b && this.c == bVar.c && Arrays.equals(this.d, bVar.d)) {
            return true;
        }
        return false;
    }

    public final int hashCode() {
        int i = this.f2968a;
        return (((((((i * 31) + i) * 31) + this.f2969b) * 31) + this.c) * 31) + Arrays.hashCode(this.d);
    }

    /* renamed from: d */
    public final b clone() {
        return new b(this.f2968a, this.f2969b, this.c, (int[]) this.d.clone());
    }

    public final a a(int i, a aVar) {
        if (aVar == null || aVar.f2965b < this.f2968a) {
            aVar = new a(this.f2968a);
        } else {
            aVar.b();
        }
        int i2 = i * this.c;
        for (int i3 = 0; i3 < this.c; i3++) {
            aVar.f2964a[(i3 << 5) / 32] = this.d[i2 + i3];
        }
        return aVar;
    }

    public final void b(int i, a aVar) {
        int[] iArr = aVar.f2964a;
        int[] iArr2 = this.d;
        int i2 = this.c;
        System.arraycopy(iArr, 0, iArr2, i * i2, i2);
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder(this.f2969b * (this.f2968a + 1));
        for (int i = 0; i < this.f2969b; i++) {
            for (int i2 = 0; i2 < this.f2968a; i2++) {
                sb.append(a(i2, i) ? "X " : "  ");
            }
            sb.append("\n");
        }
        return sb.toString();
    }
}
