package com.google.zxing.common;

/* compiled from: PerspectiveTransform */
public final class k {

    /* renamed from: a  reason: collision with root package name */
    final float f2979a;

    /* renamed from: b  reason: collision with root package name */
    final float f2980b;
    final float c;
    final float d;
    final float e;
    final float f;
    final float g;
    final float h;
    final float i;

    private k(float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10) {
        this.f2979a = f2;
        this.f2980b = f5;
        this.c = f8;
        this.d = f3;
        this.e = f6;
        this.f = f9;
        this.g = f4;
        this.h = f7;
        this.i = f10;
    }

    public static k a(float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, float f12, float f13, float f14, float f15, float f16, float f17) {
        return a(f10, f11, f12, f13, f14, f15, f16, f17).a(b(f2, f3, f4, f5, f6, f7, f8, f9));
    }

    private static k a(float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9) {
        float f10 = ((f2 - f4) + f6) - f8;
        float f11 = ((f3 - f5) + f7) - f9;
        if (f10 == 0.0f && f11 == 0.0f) {
            return new k(f4 - f2, f6 - f4, f2, f5 - f3, f7 - f5, f3, 0.0f, 0.0f, 1.0f);
        }
        float f12 = f4 - f6;
        float f13 = f8 - f6;
        float f14 = f5 - f7;
        float f15 = f9 - f7;
        float f16 = (f12 * f15) - (f13 * f14);
        float f17 = ((f15 * f10) - (f13 * f11)) / f16;
        float f18 = ((f12 * f11) - (f10 * f14)) / f16;
        return new k((f17 * f4) + (f4 - f2), (f18 * f8) + (f8 - f2), f2, (f5 - f3) + (f17 * f5), (f9 - f3) + (f18 * f9), f3, f17, f18, 1.0f);
    }

    private static k b(float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9) {
        return a(f2, f3, f4, f5, f6, f7, f8, f9).a();
    }

    private k a() {
        float f2 = this.e;
        float f3 = this.i;
        float f4 = this.f;
        float f5 = this.h;
        float f6 = (f2 * f3) - (f4 * f5);
        float f7 = this.g;
        float f8 = this.d;
        float f9 = (f4 * f7) - (f8 * f3);
        float f10 = (f8 * f5) - (f2 * f7);
        float f11 = this.c;
        float f12 = this.f2980b;
        float f13 = (f11 * f5) - (f12 * f3);
        float f14 = this.f2979a;
        return new k(f6, f9, f10, f13, (f3 * f14) - (f11 * f7), (f7 * f12) - (f5 * f14), (f12 * f4) - (f11 * f2), (f11 * f8) - (f4 * f14), (f14 * f2) - (f12 * f8));
    }

    private k a(k kVar) {
        k kVar2 = kVar;
        float f2 = this.f2979a;
        float f3 = kVar2.f2979a;
        float f4 = this.d;
        float f5 = kVar2.f2980b;
        float f6 = this.g;
        float f7 = kVar2.c;
        float f8 = (f2 * f3) + (f4 * f5) + (f6 * f7);
        float f9 = kVar2.d;
        float f10 = kVar2.e;
        float f11 = kVar2.f;
        float f12 = (f2 * f9) + (f4 * f10) + (f6 * f11);
        float f13 = kVar2.g;
        float f14 = kVar2.h;
        float f15 = kVar2.i;
        float f16 = (f2 * f13) + (f4 * f14) + (f6 * f15);
        float f17 = this.f2980b;
        float f18 = f16;
        float f19 = this.e;
        float f20 = f12;
        float f21 = this.h;
        float f22 = (f17 * f3) + (f19 * f5) + (f21 * f7);
        float f23 = (f21 * f15) + (f17 * f13) + (f19 * f14);
        float f24 = this.c;
        float f25 = this.f;
        float f26 = (f3 * f24) + (f5 * f25);
        float f27 = this.i;
        float f28 = (f24 * f13) + (f25 * f14) + (f27 * f15);
        return new k(f8, f20, f18, f22, (f17 * f9) + (f19 * f10) + (f21 * f11), f23, (f7 * f27) + f26, (f9 * f24) + (f10 * f25) + (f11 * f27), f28);
    }
}
