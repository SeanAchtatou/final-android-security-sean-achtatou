package com.google.zxing.common.a;

import com.google.zxing.NotFoundException;
import com.google.zxing.p;

/* compiled from: WhiteRectangleDetector */
public final class b {

    /* renamed from: a  reason: collision with root package name */
    private final com.google.zxing.common.b f2966a;

    /* renamed from: b  reason: collision with root package name */
    private final int f2967b;
    private final int c;
    private final int d;
    private final int e;
    private final int f;
    private final int g;

    public b(com.google.zxing.common.b bVar, int i, int i2, int i3) throws NotFoundException {
        this.f2966a = bVar;
        this.f2967b = bVar.f2969b;
        this.c = bVar.f2968a;
        int i4 = i / 2;
        this.d = i2 - i4;
        this.e = i2 + i4;
        this.g = i3 - i4;
        this.f = i3 + i4;
        if (this.g < 0 || this.d < 0 || this.f >= this.f2967b || this.e >= this.c) {
            throw NotFoundException.a();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.zxing.common.a.b.a(int, int, int, boolean):boolean
     arg types: [int, int, int, int]
     candidates:
      com.google.zxing.common.a.b.a(float, float, float, float):com.google.zxing.p
      com.google.zxing.common.a.b.a(com.google.zxing.p, com.google.zxing.p, com.google.zxing.p, com.google.zxing.p):com.google.zxing.p[]
      com.google.zxing.common.a.b.a(int, int, int, boolean):boolean */
    public final p[] a() throws NotFoundException {
        int i = this.d;
        int i2 = this.e;
        int i3 = this.g;
        int i4 = this.f;
        boolean z = false;
        int i5 = 1;
        int i6 = i;
        boolean z2 = true;
        boolean z3 = false;
        boolean z4 = false;
        boolean z5 = false;
        boolean z6 = false;
        boolean z7 = false;
        while (true) {
            if (!z2) {
                break;
            }
            boolean z8 = true;
            boolean z9 = false;
            while (true) {
                if ((z8 || !z3) && i2 < this.c) {
                    z8 = a(i3, i4, i2, false);
                    if (z8) {
                        i2++;
                        z3 = true;
                        z9 = true;
                    } else if (!z3) {
                        i2++;
                    }
                }
            }
            if (i2 >= this.c) {
                break;
            }
            boolean z10 = true;
            while (true) {
                if ((z10 || !z4) && i4 < this.f2967b) {
                    z10 = a(i6, i2, i4, true);
                    if (z10) {
                        i4++;
                        z4 = true;
                        z9 = true;
                    } else if (!z4) {
                        i4++;
                    }
                }
            }
            if (i4 >= this.f2967b) {
                break;
            }
            boolean z11 = true;
            while (true) {
                if ((z11 || !z5) && i6 >= 0) {
                    z11 = a(i3, i4, i6, false);
                    if (z11) {
                        i6--;
                        z5 = true;
                        z9 = true;
                    } else if (!z5) {
                        i6--;
                    }
                }
            }
            if (i6 < 0) {
                break;
            }
            boolean z12 = true;
            while (true) {
                if ((z12 || !z7) && i3 >= 0) {
                    z12 = a(i6, i2, i3, true);
                    if (z12) {
                        i3--;
                        z7 = true;
                        z9 = true;
                    } else if (!z7) {
                        i3--;
                    }
                }
            }
            if (i3 < 0) {
                break;
            }
            if (z9) {
                z6 = true;
            }
            z2 = z9;
        }
        z = true;
        if (z || !z6) {
            throw NotFoundException.a();
        }
        int i7 = i2 - i6;
        p pVar = null;
        p pVar2 = null;
        int i8 = 1;
        while (pVar2 == null && i8 < i7) {
            pVar2 = a((float) i6, (float) (i4 - i8), (float) (i6 + i8), (float) i4);
            i8++;
        }
        if (pVar2 != null) {
            p pVar3 = null;
            int i9 = 1;
            while (pVar3 == null && i9 < i7) {
                pVar3 = a((float) i6, (float) (i3 + i9), (float) (i6 + i9), (float) i3);
                i9++;
            }
            if (pVar3 != null) {
                p pVar4 = null;
                int i10 = 1;
                while (pVar4 == null && i10 < i7) {
                    pVar4 = a((float) i2, (float) (i3 + i10), (float) (i2 - i10), (float) i3);
                    i10++;
                }
                if (pVar4 != null) {
                    while (pVar == null && i5 < i7) {
                        pVar = a((float) i2, (float) (i4 - i5), (float) (i2 - i5), (float) i4);
                        i5++;
                    }
                    if (pVar != null) {
                        return a(pVar, pVar2, pVar4, pVar3);
                    }
                    throw NotFoundException.a();
                }
                throw NotFoundException.a();
            }
            throw NotFoundException.a();
        }
        throw NotFoundException.a();
    }

    private p a(float f2, float f3, float f4, float f5) {
        int a2 = a.a(a.a(f2, f3, f4, f5));
        float f6 = (float) a2;
        float f7 = (f4 - f2) / f6;
        float f8 = (f5 - f3) / f6;
        for (int i = 0; i < a2; i++) {
            float f9 = (float) i;
            int a3 = a.a((f9 * f7) + f2);
            int a4 = a.a((f9 * f8) + f3);
            if (this.f2966a.a(a3, a4)) {
                return new p((float) a3, (float) a4);
            }
        }
        return null;
    }

    private boolean a(int i, int i2, int i3, boolean z) {
        if (z) {
            while (i <= i2) {
                if (this.f2966a.a(i, i3)) {
                    return true;
                }
                i++;
            }
            return false;
        }
        while (i <= i2) {
            if (this.f2966a.a(i3, i)) {
                return true;
            }
            i++;
        }
        return false;
    }

    public b(com.google.zxing.common.b bVar) throws NotFoundException {
        this(bVar, 10, bVar.f2968a / 2, bVar.f2969b / 2);
    }

    private p[] a(p pVar, p pVar2, p pVar3, p pVar4) {
        float f2 = pVar.f3153a;
        float f3 = pVar.f3154b;
        float f4 = pVar2.f3153a;
        float f5 = pVar2.f3154b;
        float f6 = pVar3.f3153a;
        float f7 = pVar3.f3154b;
        float f8 = pVar4.f3153a;
        float f9 = pVar4.f3154b;
        if (f2 < ((float) this.c) / 2.0f) {
            return new p[]{new p(f8 - 1.0f, f9 + 1.0f), new p(f4 + 1.0f, f5 + 1.0f), new p(f6 - 1.0f, f7 - 1.0f), new p(f2 + 1.0f, f3 - 1.0f)};
        }
        return new p[]{new p(f8 + 1.0f, f9 + 1.0f), new p(f4 + 1.0f, f5 - 1.0f), new p(f6 - 1.0f, f7 + 1.0f), new p(f2 - 1.0f, f3 - 1.0f)};
    }
}
