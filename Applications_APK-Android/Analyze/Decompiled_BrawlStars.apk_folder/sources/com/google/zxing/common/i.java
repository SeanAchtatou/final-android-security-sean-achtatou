package com.google.zxing.common;

import com.google.zxing.NotFoundException;

/* compiled from: GridSampler */
public abstract class i {

    /* renamed from: a  reason: collision with root package name */
    private static i f2977a = new f();

    public abstract b a(b bVar, int i, int i2, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, float f12, float f13, float f14, float f15, float f16) throws NotFoundException;

    public abstract b a(b bVar, int i, int i2, k kVar) throws NotFoundException;

    public static i a() {
        return f2977a;
    }
}
