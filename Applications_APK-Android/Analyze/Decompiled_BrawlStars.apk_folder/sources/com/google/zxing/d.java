package com.google.zxing;

import java.util.List;

/* compiled from: DecodeHintType */
public enum d {
    OTHER(Object.class),
    PURE_BARCODE(Void.class),
    POSSIBLE_FORMATS(List.class),
    TRY_HARDER(Void.class),
    CHARACTER_SET(String.class),
    ALLOWED_LENGTHS(r0),
    ASSUME_CODE_39_CHECK_DIGIT(Void.class),
    ASSUME_GS1(Void.class),
    RETURN_CODABAR_START_END(Void.class),
    NEED_RESULT_POINT_CALLBACK(q.class),
    ALLOWED_EAN_EXTENSIONS(r0);
    
    public final Class<?> l;

    static {
        Class<int[]> cls = int[].class;
    }

    private d(Class<?> cls) {
        this.l = cls;
    }
}
