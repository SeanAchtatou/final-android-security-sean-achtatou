package com.google.zxing;

public final class FormatException extends ReaderException {
    private static final FormatException c;

    static {
        FormatException formatException = new FormatException();
        c = formatException;
        formatException.setStackTrace(f2880b);
    }

    private FormatException() {
    }

    private FormatException(Throwable th) {
        super(th);
    }

    public static FormatException a() {
        return f2879a ? new FormatException() : c;
    }

    public static FormatException a(Throwable th) {
        return f2879a ? new FormatException(th) : c;
    }
}
