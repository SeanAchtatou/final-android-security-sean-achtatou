package com.google.zxing;

import com.google.zxing.common.b;

/* compiled from: BinaryBitmap */
public final class c {

    /* renamed from: a  reason: collision with root package name */
    public final b f2937a;

    /* renamed from: b  reason: collision with root package name */
    private b f2938b;

    public c(b bVar) {
        if (bVar != null) {
            this.f2937a = bVar;
            return;
        }
        throw new IllegalArgumentException("Binarizer must be non-null.");
    }

    public final int a() {
        return this.f2937a.f2904a.f3145b;
    }

    public final b b() throws NotFoundException {
        if (this.f2938b == null) {
            this.f2938b = this.f2937a.a();
        }
        return this.f2938b;
    }

    public final String toString() {
        try {
            return b().toString();
        } catch (NotFoundException unused) {
            return "";
        }
    }
}
