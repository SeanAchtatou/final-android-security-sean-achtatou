package com.google.zxing;

/* compiled from: PlanarYUVLuminanceSource */
public final class l extends h {
    private final byte[] c;
    private final int d;
    private final int e;
    private final int f;
    private final int g;

    public l(byte[] bArr, int i, int i2, int i3, int i4, int i5, int i6, boolean z) {
        super(i5, i6);
        if (i5 + i3 > i || i6 + i4 > i2) {
            throw new IllegalArgumentException("Crop rectangle does not fit within image data.");
        }
        this.c = bArr;
        this.d = i;
        this.e = i2;
        this.f = i3;
        this.g = i4;
    }

    public final byte[] a(int i, byte[] bArr) {
        if (i < 0 || i >= this.f3145b) {
            throw new IllegalArgumentException("Requested row is outside the image: " + i);
        }
        int i2 = this.f3144a;
        if (bArr == null || bArr.length < i2) {
            bArr = new byte[i2];
        }
        System.arraycopy(this.c, ((i + this.g) * this.d) + this.f, bArr, 0, i2);
        return bArr;
    }

    public final byte[] a() {
        int i = this.f3144a;
        int i2 = this.f3145b;
        if (i == this.d && i2 == this.e) {
            return this.c;
        }
        int i3 = i * i2;
        byte[] bArr = new byte[i3];
        int i4 = this.g;
        int i5 = this.d;
        int i6 = (i4 * i5) + this.f;
        if (i == i5) {
            System.arraycopy(this.c, i6, bArr, 0, i3);
            return bArr;
        }
        for (int i7 = 0; i7 < i2; i7++) {
            System.arraycopy(this.c, i6, bArr, i7 * i, i);
            i6 += this.d;
        }
        return bArr;
    }
}
