package com.google.zxing;

/* compiled from: InvertedLuminanceSource */
public final class g extends h {
    private final h c;

    public final byte[] a(int i, byte[] bArr) {
        byte[] a2 = this.c.a(i, bArr);
        int i2 = this.f3144a;
        for (int i3 = 0; i3 < i2; i3++) {
            a2[i3] = (byte) (255 - (a2[i3] & 255));
        }
        return a2;
    }

    public final byte[] a() {
        byte[] a2 = this.c.a();
        int i = this.f3144a * this.f3145b;
        byte[] bArr = new byte[i];
        for (int i2 = 0; i2 < i; i2++) {
            bArr[i2] = (byte) (255 - (a2[i2] & 255));
        }
        return bArr;
    }

    public final boolean b() {
        return this.c.b();
    }

    public final h c() {
        return this.c;
    }

    public final h d() {
        return new g(this.c.d());
    }

    public g(h hVar) {
        super(hVar.f3144a, hVar.f3145b);
        this.c = hVar;
    }
}
