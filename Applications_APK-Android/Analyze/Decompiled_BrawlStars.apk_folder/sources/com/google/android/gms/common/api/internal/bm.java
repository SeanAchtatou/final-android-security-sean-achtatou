package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.g;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

public final class bm {

    /* renamed from: a  reason: collision with root package name */
    public static final Status f1427a = new Status(8, "The connection to Google Play services was lost");

    /* renamed from: b  reason: collision with root package name */
    static final BasePendingResult<?>[] f1428b = new BasePendingResult[0];
    final Set<BasePendingResult<?>> c = Collections.synchronizedSet(Collections.newSetFromMap(new WeakHashMap()));
    private final bo d = new bn(this);
    private final Map<a.c<?>, a.f> e;

    public bm(Map<a.c<?>, a.f> map) {
        this.e = map;
    }

    /* access modifiers changed from: package-private */
    public final void a(BasePendingResult<? extends g> basePendingResult) {
        this.c.add(basePendingResult);
        basePendingResult.a(this.d);
    }

    public final void a() {
        for (BasePendingResult basePendingResult : (BasePendingResult[]) this.c.toArray(f1428b)) {
            basePendingResult.a((bo) null);
            basePendingResult.d();
            if (basePendingResult.f()) {
                this.c.remove(basePendingResult);
            }
        }
    }
}
