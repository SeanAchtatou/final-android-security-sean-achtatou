package com.google.android.gms.games.multiplayer.realtime;

import android.os.Bundle;
import android.os.Parcelable;
import com.google.android.gms.common.data.e;
import com.google.android.gms.games.multiplayer.c;

public interface Room extends Parcelable, e<Room>, c {
    String b();

    String c();

    long d();

    int e();

    String f();

    int g();

    Bundle h();

    int j();
}
