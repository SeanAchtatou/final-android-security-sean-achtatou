package com.google.android.gms.games.multiplayer.turnbased;

import android.os.Bundle;
import android.os.Parcelable;
import com.google.android.gms.common.data.e;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.multiplayer.c;

public interface TurnBasedMatch extends Parcelable, e<TurnBasedMatch>, c {
    public static final int[] MATCH_TURN_STATUS_ALL = {0, 1, 2, 3};

    Game b();

    String c();

    String d();

    long e();

    int f();

    int g();

    String h();

    int j();

    String k();

    long l();

    String m();

    byte[] n();

    int o();

    String p();

    byte[] q();

    int r();

    Bundle s();

    int t();

    boolean u();

    String v();
}
