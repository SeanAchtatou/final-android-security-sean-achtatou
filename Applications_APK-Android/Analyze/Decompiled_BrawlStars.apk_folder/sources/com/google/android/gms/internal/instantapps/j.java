package com.google.android.gms.internal.instantapps;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import com.google.android.gms.common.g;

final class j {
    static boolean a(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo("com.google.android.gms", 64);
            if (g.a(context).a(packageInfo)) {
                return true;
            }
            String valueOf = String.valueOf(packageInfo.packageName);
            if (valueOf.length() != 0) {
                "Incorrect signature for package ".concat(valueOf);
            } else {
                new String("Incorrect signature for package ");
            }
            return false;
        } catch (PackageManager.NameNotFoundException unused) {
            return false;
        }
    }
}
