package com.google.android.gms.internal.measurement;

import com.google.android.gms.common.internal.l;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

final class bn {

    /* renamed from: a  reason: collision with root package name */
    int f2083a;

    /* renamed from: b  reason: collision with root package name */
    ByteArrayOutputStream f2084b = new ByteArrayOutputStream();
    private final /* synthetic */ bm c;

    public bn(bm bmVar) {
        this.c = bmVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.bm.a(com.google.android.gms.internal.measurement.bh, boolean):java.lang.String
     arg types: [com.google.android.gms.internal.measurement.bh, int]
     candidates:
      com.google.android.gms.internal.measurement.bm.a(java.net.URL, byte[]):int
      com.google.android.gms.internal.measurement.bm.a(com.google.android.gms.internal.measurement.bh, java.lang.String):java.net.URL
      com.google.android.gms.internal.measurement.q.a(java.lang.String, java.lang.Object):void
      com.google.android.gms.internal.measurement.bm.a(com.google.android.gms.internal.measurement.bh, boolean):java.lang.String */
    public final boolean a(bh bhVar) {
        l.a(bhVar);
        if (this.f2083a + 1 > au.g()) {
            return false;
        }
        String a2 = this.c.a(bhVar, false);
        if (a2 == null) {
            this.c.c.a().a(bhVar, "Error formatting hit");
            return true;
        }
        byte[] bytes = a2.getBytes();
        int length = bytes.length;
        if (length > au.c()) {
            this.c.c.a().a(bhVar, "Hit size exceeds the maximum size limit");
            return true;
        }
        if (this.f2084b.size() > 0) {
            length++;
        }
        if (this.f2084b.size() + length > ((Integer) bc.t.f2067a).intValue()) {
            return false;
        }
        try {
            if (this.f2084b.size() > 0) {
                this.f2084b.write(bm.d);
            }
            this.f2084b.write(bytes);
            this.f2083a++;
            return true;
        } catch (IOException e) {
            this.c.e("Failed to write payload when batching hits", e);
            return true;
        }
    }
}
