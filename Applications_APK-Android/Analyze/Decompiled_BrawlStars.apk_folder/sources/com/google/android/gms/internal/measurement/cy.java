package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class cy extends iz<cy> {
    private static volatile cy[] c;

    /* renamed from: a  reason: collision with root package name */
    public Integer f2145a = null;

    /* renamed from: b  reason: collision with root package name */
    public long[] f2146b = jh.f2316b;

    public static cy[] a() {
        if (c == null) {
            synchronized (jd.f2312b) {
                if (c == null) {
                    c = new cy[0];
                }
            }
        }
        return c;
    }

    public cy() {
        this.L = null;
        this.M = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof cy)) {
            return false;
        }
        cy cyVar = (cy) obj;
        Integer num = this.f2145a;
        if (num == null) {
            if (cyVar.f2145a != null) {
                return false;
            }
        } else if (!num.equals(cyVar.f2145a)) {
            return false;
        }
        if (!jd.a(this.f2146b, cyVar.f2146b)) {
            return false;
        }
        if (this.L == null || this.L.a()) {
            return cyVar.L == null || cyVar.L.a();
        }
        return this.L.equals(cyVar.L);
    }

    public final int hashCode() {
        int hashCode = (getClass().getName().hashCode() + 527) * 31;
        Integer num = this.f2145a;
        int i = 0;
        int hashCode2 = (((hashCode + (num == null ? 0 : num.hashCode())) * 31) + jd.a(this.f2146b)) * 31;
        if (this.L != null && !this.L.a()) {
            i = this.L.hashCode();
        }
        return hashCode2 + i;
    }

    public final void a(iy iyVar) throws IOException {
        Integer num = this.f2145a;
        if (num != null) {
            iyVar.a(1, num.intValue());
        }
        long[] jArr = this.f2146b;
        if (jArr != null && jArr.length > 0) {
            int i = 0;
            while (true) {
                long[] jArr2 = this.f2146b;
                if (i >= jArr2.length) {
                    break;
                }
                iyVar.b(2, jArr2[i]);
                i++;
            }
        }
        super.a(iyVar);
    }

    /* access modifiers changed from: protected */
    public final int b() {
        int b2 = super.b();
        Integer num = this.f2145a;
        if (num != null) {
            b2 += iy.b(1, num.intValue());
        }
        long[] jArr = this.f2146b;
        if (jArr == null || jArr.length <= 0) {
            return b2;
        }
        int i = 0;
        int i2 = 0;
        while (true) {
            long[] jArr2 = this.f2146b;
            if (i >= jArr2.length) {
                return b2 + i2 + (jArr2.length * 1);
            }
            i2 += iy.a(jArr2[i]);
            i++;
        }
    }

    public final /* synthetic */ je a(iw iwVar) throws IOException {
        while (true) {
            int a2 = iwVar.a();
            if (a2 == 0) {
                return this;
            }
            if (a2 == 8) {
                this.f2145a = Integer.valueOf(iwVar.d());
            } else if (a2 == 16) {
                int a3 = jh.a(iwVar, 16);
                long[] jArr = this.f2146b;
                int length = jArr == null ? 0 : jArr.length;
                long[] jArr2 = new long[(a3 + length)];
                if (length != 0) {
                    System.arraycopy(this.f2146b, 0, jArr2, 0, length);
                }
                while (length < jArr2.length - 1) {
                    jArr2[length] = iwVar.e();
                    iwVar.a();
                    length++;
                }
                jArr2[length] = iwVar.e();
                this.f2146b = jArr2;
            } else if (a2 == 18) {
                int c2 = iwVar.c(iwVar.d());
                int i = iwVar.i();
                int i2 = 0;
                while (iwVar.h() > 0) {
                    iwVar.e();
                    i2++;
                }
                iwVar.e(i);
                long[] jArr3 = this.f2146b;
                int length2 = jArr3 == null ? 0 : jArr3.length;
                long[] jArr4 = new long[(i2 + length2)];
                if (length2 != 0) {
                    System.arraycopy(this.f2146b, 0, jArr4, 0, length2);
                }
                while (length2 < jArr4.length) {
                    jArr4[length2] = iwVar.e();
                    length2++;
                }
                this.f2146b = jArr4;
                iwVar.d(c2);
            } else if (!super.a(iwVar, a2)) {
                return this;
            }
        }
    }
}
