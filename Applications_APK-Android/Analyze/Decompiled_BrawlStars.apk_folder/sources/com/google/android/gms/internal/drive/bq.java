package com.google.android.gms.internal.drive;

import com.google.android.gms.drive.metadata.a;
import com.google.android.gms.drive.metadata.internal.b;
import com.google.android.gms.drive.metadata.internal.g;

public final class bq {

    /* renamed from: a  reason: collision with root package name */
    public static final a<Integer> f1899a = new g("contentAvailability");

    /* renamed from: b  reason: collision with root package name */
    public static final a<Boolean> f1900b = new b("isPinnable", 4300000);
}
