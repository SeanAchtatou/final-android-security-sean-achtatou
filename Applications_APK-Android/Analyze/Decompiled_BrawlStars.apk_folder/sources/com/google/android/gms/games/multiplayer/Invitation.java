package com.google.android.gms.games.multiplayer;

import android.os.Parcelable;
import com.google.android.gms.common.data.e;
import com.google.android.gms.games.Game;

public interface Invitation extends Parcelable, e<Invitation>, c {
    Game b();

    String c();

    Participant d();

    long e();

    int f();

    int g();

    int h();
}
