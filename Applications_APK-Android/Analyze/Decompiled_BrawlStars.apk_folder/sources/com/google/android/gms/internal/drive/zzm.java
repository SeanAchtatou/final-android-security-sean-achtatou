package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public final class zzm extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzm> CREATOR = new cb();

    /* renamed from: a  reason: collision with root package name */
    private final DriveId f1979a;

    /* renamed from: b  reason: collision with root package name */
    private final MetadataBundle f1980b;
    private final Contents c;
    private final boolean d;
    private final String e;
    private final int f;
    private final int g;
    private final boolean h;
    private final boolean i;

    zzm(DriveId driveId, MetadataBundle metadataBundle, Contents contents, boolean z, String str, int i2, int i3, boolean z2, boolean z3) {
        this.f1979a = driveId;
        this.f1980b = metadataBundle;
        this.c = contents;
        this.d = z;
        this.e = str;
        this.f = i2;
        this.g = i3;
        this.h = z2;
        this.i = z3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.DriveId, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.metadata.internal.MetadataBundle, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.Contents, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void */
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = a.a(parcel, 20293);
        a.a(parcel, 2, (Parcelable) this.f1979a, i2, false);
        a.a(parcel, 3, (Parcelable) this.f1980b, i2, false);
        a.a(parcel, 4, (Parcelable) this.c, i2, false);
        a.a(parcel, 5, this.d);
        a.a(parcel, 6, this.e, false);
        a.b(parcel, 7, this.f);
        a.b(parcel, 8, this.g);
        a.a(parcel, 9, this.h);
        a.a(parcel, 10, this.i);
        a.b(parcel, a2);
    }
}
