package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class eb implements Parcelable.Creator<zzfv> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zzfv[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int a2 = SafeParcelReader.a(parcel);
        String str = null;
        Long l = null;
        Float f = null;
        String str2 = null;
        String str3 = null;
        Double d = null;
        long j = 0;
        int i = 0;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    i = SafeParcelReader.d(parcel2, readInt);
                    break;
                case 2:
                    str = SafeParcelReader.l(parcel2, readInt);
                    break;
                case 3:
                    j = SafeParcelReader.f(parcel2, readInt);
                    break;
                case 4:
                    l = SafeParcelReader.g(parcel2, readInt);
                    break;
                case 5:
                    int a3 = SafeParcelReader.a(parcel2, readInt);
                    if (a3 != 0) {
                        SafeParcelReader.a(parcel2, a3, 4);
                        f = Float.valueOf(parcel.readFloat());
                        break;
                    } else {
                        f = null;
                        break;
                    }
                case 6:
                    str2 = SafeParcelReader.l(parcel2, readInt);
                    break;
                case 7:
                    str3 = SafeParcelReader.l(parcel2, readInt);
                    break;
                case 8:
                    int a4 = SafeParcelReader.a(parcel2, readInt);
                    if (a4 != 0) {
                        SafeParcelReader.a(parcel2, a4, 8);
                        d = Double.valueOf(parcel.readDouble());
                        break;
                    } else {
                        d = null;
                        break;
                    }
                default:
                    SafeParcelReader.b(parcel2, readInt);
                    break;
            }
        }
        SafeParcelReader.x(parcel2, a2);
        return new zzfv(i, str, j, l, f, str2, str3, d);
    }
}
