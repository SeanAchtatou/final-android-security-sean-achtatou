package com.google.android.gms.measurement.internal;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import com.google.android.gms.common.c.b;
import com.google.android.gms.common.c.c;
import com.google.android.gms.common.stats.a;
import com.google.android.gms.internal.measurement.zzu;
import java.util.List;

public final class ae {

    /* renamed from: a  reason: collision with root package name */
    final ar f2368a;

    ae(ar arVar) {
        this.f2368a = arVar;
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        if (str == null || str.isEmpty()) {
            this.f2368a.q().i.a("Install Referrer Reporter was called with invalid app package name");
            return;
        }
        this.f2368a.p().c();
        if (!a()) {
            this.f2368a.q().i.a("Install Referrer Reporter is not available");
            return;
        }
        this.f2368a.q().i.a("Install Referrer Reporter is initializing");
        af afVar = new af(this, str);
        this.f2368a.p().c();
        Intent intent = new Intent("com.google.android.finsky.BIND_GET_INSTALL_REFERRER_SERVICE");
        intent.setComponent(new ComponentName("com.android.vending", "com.google.android.finsky.externalreferrer.GetInstallReferrerService"));
        PackageManager packageManager = this.f2368a.m().getPackageManager();
        if (packageManager == null) {
            this.f2368a.q().f.a("Failed to obtain Package Manager to verify binding conditions");
            return;
        }
        List<ResolveInfo> queryIntentServices = packageManager.queryIntentServices(intent, 0);
        if (queryIntentServices == null || queryIntentServices.isEmpty()) {
            this.f2368a.q().i.a("Play Service for fetching Install Referrer is unavailable on device");
            return;
        }
        ResolveInfo resolveInfo = queryIntentServices.get(0);
        if (resolveInfo.serviceInfo != null) {
            String str2 = resolveInfo.serviceInfo.packageName;
            if (resolveInfo.serviceInfo.name == null || !"com.android.vending".equals(str2) || !a()) {
                this.f2368a.q().i.a("Play Store missing or incompatible. Version 8.3.73 or later required");
                return;
            }
            try {
                this.f2368a.q().i.a("Install Referrer Service is", a.a().b(this.f2368a.m(), new Intent(intent), afVar, 1) ? "available" : "not available");
            } catch (Exception e) {
                this.f2368a.q().c.a("Exception occurred while binding to Install Referrer Service", e.getMessage());
            }
        }
    }

    private final boolean a() {
        try {
            b a2 = c.a(this.f2368a.m());
            if (a2 == null) {
                this.f2368a.q().i.a("Failed to retrieve Package Manager to check Play Store compatibility");
                return false;
            } else if (a2.b("com.android.vending", 128).versionCode >= 80837300) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            this.f2368a.q().i.a("Failed to retrieve Play Store version", e);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public final Bundle a(String str, zzu zzu) {
        this.f2368a.p().c();
        if (zzu == null) {
            this.f2368a.q().f.a("Attempting to use Install Referrer Service while it is not initialized");
            return null;
        }
        Bundle bundle = new Bundle();
        bundle.putString("package_name", str);
        try {
            Bundle a2 = zzu.a(bundle);
            if (a2 != null) {
                return a2;
            }
            this.f2368a.q().c.a("Install Referrer Service returned a null response");
            return null;
        } catch (Exception e) {
            this.f2368a.q().c.a("Exception occurred while retrieving the Install Referrer", e.getMessage());
            return null;
        }
    }
}
