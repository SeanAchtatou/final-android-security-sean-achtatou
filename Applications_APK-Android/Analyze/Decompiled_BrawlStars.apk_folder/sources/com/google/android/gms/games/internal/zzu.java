package com.google.android.gms.games.internal;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzu extends IInterface {
    void a() throws RemoteException;

    void a(int i, String str) throws RemoteException;

    void b(int i, String str) throws RemoteException;
}
