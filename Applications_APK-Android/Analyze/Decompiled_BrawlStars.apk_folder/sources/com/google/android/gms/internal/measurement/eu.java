package com.google.android.gms.internal.measurement;

final class eu implements ep {
    private eu() {
    }

    public final byte[] a(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        return bArr2;
    }

    /* synthetic */ eu(byte b2) {
        this();
    }
}
