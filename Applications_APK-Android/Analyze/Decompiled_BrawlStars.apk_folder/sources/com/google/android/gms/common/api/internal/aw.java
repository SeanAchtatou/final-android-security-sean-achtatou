package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.internal.d;
import com.google.android.gms.common.internal.IAccountAccessor;
import java.util.Collections;

final class aw implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ConnectionResult f1409a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ d.c f1410b;

    aw(d.c cVar, ConnectionResult connectionResult) {
        this.f1410b = cVar;
        this.f1409a = connectionResult;
    }

    public final void run() {
        if (this.f1409a.b()) {
            d.c cVar = this.f1410b;
            cVar.c = true;
            if (cVar.f1472a.d()) {
                this.f1410b.a();
                return;
            }
            try {
                this.f1410b.f1472a.a((IAccountAccessor) null, Collections.emptySet());
            } catch (SecurityException unused) {
                ((d.a) d.this.f.get(this.f1410b.f1473b)).onConnectionFailed(new ConnectionResult(10));
            }
        } else {
            ((d.a) d.this.f.get(this.f1410b.f1473b)).onConnectionFailed(this.f1409a);
        }
    }
}
