package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.a;

public final class zzo extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzo> CREATOR = new ek();

    /* renamed from: a  reason: collision with root package name */
    public String f2603a;

    /* renamed from: b  reason: collision with root package name */
    public String f2604b;
    public zzfv c;
    public long d;
    public boolean e;
    public String f;
    public zzag g;
    public long h;
    public zzag i;
    public long j;
    public zzag k;

    zzo(zzo zzo) {
        l.a(zzo);
        this.f2603a = zzo.f2603a;
        this.f2604b = zzo.f2604b;
        this.c = zzo.c;
        this.d = zzo.d;
        this.e = zzo.e;
        this.f = zzo.f;
        this.g = zzo.g;
        this.h = zzo.h;
        this.i = zzo.i;
        this.j = zzo.j;
        this.k = zzo.k;
    }

    zzo(String str, String str2, zzfv zzfv, long j2, boolean z, String str3, zzag zzag, long j3, zzag zzag2, long j4, zzag zzag3) {
        this.f2603a = str;
        this.f2604b = str2;
        this.c = zzfv;
        this.d = j2;
        this.e = z;
        this.f = str3;
        this.g = zzag;
        this.h = j3;
        this.i = zzag2;
        this.j = j4;
        this.k = zzag3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.measurement.internal.zzfv, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.measurement.internal.zzag, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = a.a(parcel, 20293);
        a.a(parcel, 2, this.f2603a, false);
        a.a(parcel, 3, this.f2604b, false);
        a.a(parcel, 4, (Parcelable) this.c, i2, false);
        a.a(parcel, 5, this.d);
        a.a(parcel, 6, this.e);
        a.a(parcel, 7, this.f, false);
        a.a(parcel, 8, (Parcelable) this.g, i2, false);
        a.a(parcel, 9, this.h);
        a.a(parcel, 10, (Parcelable) this.i, i2, false);
        a.a(parcel, 11, this.j);
        a.a(parcel, 12, (Parcelable) this.k, i2, false);
        a.b(parcel, a2);
    }
}
