package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.nio.charset.Charset;

class et extends es {
    protected final byte[] c;

    et(byte[] bArr) {
        if (bArr != null) {
            this.c = bArr;
            return;
        }
        throw new NullPointerException();
    }

    /* access modifiers changed from: protected */
    public int d() {
        return 0;
    }

    public byte a(int i) {
        return this.c[i];
    }

    /* access modifiers changed from: package-private */
    public byte b(int i) {
        return this.c[i];
    }

    public int a() {
        return this.c.length;
    }

    public final ej c(int i) {
        int a2 = a(0, i, a());
        if (a2 == 0) {
            return ej.f2184a;
        }
        return new eo(this.c, d(), a2);
    }

    /* access modifiers changed from: package-private */
    public final void a(ei eiVar) throws IOException {
        eiVar.a(this.c, d(), a());
    }

    /* access modifiers changed from: protected */
    public final String a(Charset charset) {
        return new String(this.c, d(), a(), charset);
    }

    public final boolean c() {
        int d = d();
        return ij.a(this.c, d, a() + d);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ej) || a() != ((ej) obj).a()) {
            return false;
        }
        if (a() == 0) {
            return true;
        }
        if (!(obj instanceof et)) {
            return obj.equals(this);
        }
        et etVar = (et) obj;
        int i = this.f2185b;
        int i2 = etVar.f2185b;
        if (i == 0 || i2 == 0 || i == i2) {
            return a(etVar, a());
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public final boolean a(ej ejVar, int i) {
        if (i > ejVar.a()) {
            int a2 = a();
            StringBuilder sb = new StringBuilder(40);
            sb.append("Length too large: ");
            sb.append(i);
            sb.append(a2);
            throw new IllegalArgumentException(sb.toString());
        } else if (i > ejVar.a()) {
            int a3 = ejVar.a();
            StringBuilder sb2 = new StringBuilder(59);
            sb2.append("Ran off end of other: 0, ");
            sb2.append(i);
            sb2.append(", ");
            sb2.append(a3);
            throw new IllegalArgumentException(sb2.toString());
        } else if (!(ejVar instanceof et)) {
            return ejVar.c(i).equals(c(i));
        } else {
            et etVar = (et) ejVar;
            byte[] bArr = this.c;
            byte[] bArr2 = etVar.c;
            int d = d() + i;
            int d2 = d();
            int d3 = etVar.d();
            while (d2 < d) {
                if (bArr[d2] != bArr2[d3]) {
                    return false;
                }
                d2++;
                d3++;
            }
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public final int a(int i, int i2) {
        return fs.a(i, this.c, d(), i2);
    }
}
