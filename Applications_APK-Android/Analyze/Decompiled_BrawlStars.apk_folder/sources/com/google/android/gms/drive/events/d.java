package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class d implements Parcelable.Creator<zzb> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zzb[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        zze zze = null;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            if ((65535 & readInt) != 3) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                zze = (zze) SafeParcelReader.a(parcel, readInt, zze.CREATOR);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new zzb(zze);
    }
}
