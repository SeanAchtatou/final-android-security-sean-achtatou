package com.google.android.gms.internal.measurement;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.analytics.n;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.util.k;
import java.io.Closeable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class ad extends r implements Closeable {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f2037a = String.format("CREATE TABLE IF NOT EXISTS %s ( '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '%s' INTEGER NOT NULL, '%s' TEXT NOT NULL, '%s' TEXT NOT NULL, '%s' INTEGER);", "hits2", "hit_id", "hit_time", "hit_url", "hit_string", "hit_app_id");

    /* renamed from: b  reason: collision with root package name */
    private static final String f2038b = String.format("SELECT MAX(%s) FROM %s WHERE 1;", "hit_time", "hits2");
    private final ae d;
    private final bw e = new bw(f());
    /* access modifiers changed from: private */
    public final bw f = new bw(f());

    ad(t tVar) {
        super(tVar);
        this.d = new ae(this, tVar.f2332a, "google_analytics_v4.db");
    }

    static /* synthetic */ String s() {
        return "google_analytics_v4.db";
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    public final void b() {
        m();
        r().beginTransaction();
    }

    public final void c() {
        m();
        r().setTransactionSuccessful();
    }

    public final void d() {
        m();
        r().endTransaction();
    }

    public final void a(bh bhVar) {
        String str;
        l.a(bhVar);
        n.b();
        m();
        l.a(bhVar);
        Uri.Builder builder = new Uri.Builder();
        for (Map.Entry next : bhVar.f2073a.entrySet()) {
            String str2 = (String) next.getKey();
            if (!"ht".equals(str2) && !"qt".equals(str2) && !"AppUID".equals(str2)) {
                builder.appendQueryParameter(str2, (String) next.getValue());
            }
        }
        String encodedQuery = builder.build().getEncodedQuery();
        if (encodedQuery == null) {
            encodedQuery = "";
        }
        if (encodedQuery.length() > 8192) {
            this.c.a().a(bhVar, "Hit length exceeds the maximum allowed size");
            return;
        }
        int intValue = ((Integer) bc.c.f2067a).intValue();
        long u = u();
        if (u > ((long) (intValue - 1))) {
            List<Long> c = c((u - ((long) intValue)) + 1);
            d("Store full, deleting hits to make room, count", Integer.valueOf(c.size()));
            a(c);
        }
        SQLiteDatabase r = r();
        ContentValues contentValues = new ContentValues();
        contentValues.put("hit_string", encodedQuery);
        contentValues.put("hit_time", Long.valueOf(bhVar.d));
        contentValues.put("hit_app_id", Integer.valueOf(bhVar.e));
        if (bhVar.f) {
            str = au.h();
        } else {
            str = au.i();
        }
        contentValues.put("hit_url", str);
        try {
            long insert = r.insert("hits2", null, contentValues);
            if (insert == -1) {
                f("Failed to insert a hit (got -1)");
            } else {
                b("Hit saved to database. db-id, hit", Long.valueOf(insert), bhVar);
            }
        } catch (SQLiteException e2) {
            e("Error storing a hit", e2);
        }
    }

    private final long u() {
        n.b();
        m();
        return a("SELECT COUNT(*) FROM hits2");
    }

    /* access modifiers changed from: package-private */
    public final boolean e() {
        return u() == 0;
    }

    private final List<Long> c(long j) {
        n.b();
        m();
        if (j <= 0) {
            return Collections.emptyList();
        }
        SQLiteDatabase r = r();
        ArrayList arrayList = new ArrayList();
        Cursor cursor = null;
        try {
            Cursor query = r.query("hits2", new String[]{"hit_id"}, null, null, null, null, String.format("%s ASC", "hit_id"), Long.toString(j));
            if (query.moveToFirst()) {
                do {
                    arrayList.add(Long.valueOf(query.getLong(0)));
                } while (query.moveToNext());
            }
            if (query != null) {
                query.close();
            }
        } catch (SQLiteException e2) {
            d("Error selecting hit ids", e2);
            if (cursor != null) {
                cursor.close();
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        return arrayList;
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x00af  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.google.android.gms.internal.measurement.bh> a(long r23) {
        /*
            r22 = this;
            r10 = r22
            java.lang.String r0 = "hit_id"
            r11 = 1
            r12 = 0
            r1 = 0
            int r3 = (r23 > r1 ? 1 : (r23 == r1 ? 0 : -1))
            if (r3 < 0) goto L_0x000e
            r1 = 1
            goto L_0x000f
        L_0x000e:
            r1 = 0
        L_0x000f:
            com.google.android.gms.common.internal.l.b(r1)
            com.google.android.gms.analytics.n.b()
            r22.m()
            android.database.sqlite.SQLiteDatabase r13 = r22.r()
            r1 = 0
            java.lang.String r14 = "hits2"
            r2 = 5
            java.lang.String[] r15 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x00a6 }
            r15[r12] = r0     // Catch:{ SQLiteException -> 0x00a6 }
            java.lang.String r2 = "hit_time"
            r15[r11] = r2     // Catch:{ SQLiteException -> 0x00a6 }
            java.lang.String r2 = "hit_string"
            r9 = 2
            r15[r9] = r2     // Catch:{ SQLiteException -> 0x00a6 }
            java.lang.String r2 = "hit_url"
            r7 = 3
            r15[r7] = r2     // Catch:{ SQLiteException -> 0x00a6 }
            java.lang.String r2 = "hit_app_id"
            r8 = 4
            r15[r8] = r2     // Catch:{ SQLiteException -> 0x00a6 }
            r16 = 0
            r17 = 0
            r18 = 0
            r19 = 0
            java.lang.String r2 = "%s ASC"
            java.lang.Object[] r3 = new java.lang.Object[r11]     // Catch:{ SQLiteException -> 0x00a6 }
            r3[r12] = r0     // Catch:{ SQLiteException -> 0x00a6 }
            java.lang.String r20 = java.lang.String.format(r2, r3)     // Catch:{ SQLiteException -> 0x00a6 }
            java.lang.String r21 = java.lang.Long.toString(r23)     // Catch:{ SQLiteException -> 0x00a6 }
            android.database.Cursor r13 = r13.query(r14, r15, r16, r17, r18, r19, r20, r21)     // Catch:{ SQLiteException -> 0x00a6 }
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009e }
            r0.<init>()     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009e }
            boolean r1 = r13.moveToFirst()     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009e }
            if (r1 == 0) goto L_0x0098
        L_0x005c:
            long r14 = r13.getLong(r12)     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009e }
            long r4 = r13.getLong(r11)     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009e }
            java.lang.String r1 = r13.getString(r9)     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009e }
            java.lang.String r2 = r13.getString(r7)     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009e }
            int r16 = r13.getInt(r8)     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009e }
            java.util.Map r3 = r10.g(r1)     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009e }
            boolean r6 = com.google.android.gms.internal.measurement.bx.e(r2)     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009e }
            com.google.android.gms.internal.measurement.bh r2 = new com.google.android.gms.internal.measurement.bh     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009e }
            r1 = r2
            r11 = r2
            r2 = r22
            r18 = 4
            r19 = 3
            r7 = r14
            r14 = 2
            r9 = r16
            r1.<init>(r2, r3, r4, r6, r7, r9)     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009e }
            r0.add(r11)     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009e }
            boolean r1 = r13.moveToNext()     // Catch:{ SQLiteException -> 0x00a1, all -> 0x009e }
            if (r1 != 0) goto L_0x0093
            goto L_0x0098
        L_0x0093:
            r7 = 3
            r8 = 4
            r9 = 2
            r11 = 1
            goto L_0x005c
        L_0x0098:
            if (r13 == 0) goto L_0x009d
            r13.close()
        L_0x009d:
            return r0
        L_0x009e:
            r0 = move-exception
            r1 = r13
            goto L_0x00ad
        L_0x00a1:
            r0 = move-exception
            r1 = r13
            goto L_0x00a7
        L_0x00a4:
            r0 = move-exception
            goto L_0x00ad
        L_0x00a6:
            r0 = move-exception
        L_0x00a7:
            java.lang.String r2 = "Error loading hits from the database"
            r10.e(r2, r0)     // Catch:{ all -> 0x00a4 }
            throw r0     // Catch:{ all -> 0x00a4 }
        L_0x00ad:
            if (r1 == 0) goto L_0x00b2
            r1.close()
        L_0x00b2:
            goto L_0x00b4
        L_0x00b3:
            throw r0
        L_0x00b4:
            goto L_0x00b3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.ad.a(long):java.util.List");
    }

    public final void a(List<Long> list) {
        l.a(list);
        n.b();
        m();
        if (!list.isEmpty()) {
            StringBuilder sb = new StringBuilder("hit_id");
            sb.append(" in (");
            for (int i = 0; i < list.size(); i++) {
                Long l = list.get(i);
                if (l == null || l.longValue() == 0) {
                    throw new SQLiteException("Invalid hit id");
                }
                if (i > 0) {
                    sb.append(",");
                }
                sb.append(l);
            }
            sb.append(")");
            String sb2 = sb.toString();
            try {
                SQLiteDatabase r = r();
                a("Deleting dispatched hits. count", Integer.valueOf(list.size()));
                int delete = r.delete("hits2", sb2, null);
                if (delete != list.size()) {
                    b("Deleted fewer hits then expected", Integer.valueOf(list.size()), Integer.valueOf(delete), sb2);
                }
            } catch (SQLiteException e2) {
                e("Error deleting hits", e2);
                throw e2;
            }
        }
    }

    public final void b(long j) {
        n.b();
        m();
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(Long.valueOf(j));
        a("Deleting hit, id", Long.valueOf(j));
        a((List<Long>) arrayList);
    }

    public final int o() {
        n.b();
        m();
        if (!this.e.a(86400000)) {
            return 0;
        }
        this.e.a();
        b("Deleting stale hits (if any)");
        int delete = r().delete("hits2", "hit_time < ?", new String[]{Long.toString(f().a() - 2592000000L)});
        a("Deleted stale hits, count", Integer.valueOf(delete));
        return delete;
    }

    public final long p() {
        n.b();
        m();
        return a(f2038b, (String[]) null);
    }

    public final long a(long j, String str, String str2) {
        l.a(str);
        l.a(str2);
        m();
        n.b();
        return a("SELECT hits_count FROM properties WHERE app_uid=? AND cid=? AND tid=?", new String[]{String.valueOf(j), str, str2});
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x00c7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.google.android.gms.internal.measurement.w> q() {
        /*
            r23 = this;
            r1 = r23
            r23.m()
            com.google.android.gms.analytics.n.b()
            android.database.sqlite.SQLiteDatabase r2 = r23.r()
            r0 = 5
            java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x00ba, all -> 0x00b6 }
            java.lang.String r0 = "cid"
            r12 = 0
            r4[r12] = r0     // Catch:{ SQLiteException -> 0x00ba, all -> 0x00b6 }
            java.lang.String r0 = "tid"
            r13 = 1
            r4[r13] = r0     // Catch:{ SQLiteException -> 0x00ba, all -> 0x00b6 }
            java.lang.String r0 = "adid"
            r14 = 2
            r4[r14] = r0     // Catch:{ SQLiteException -> 0x00ba, all -> 0x00b6 }
            java.lang.String r0 = "hits_count"
            r15 = 3
            r4[r15] = r0     // Catch:{ SQLiteException -> 0x00ba, all -> 0x00b6 }
            java.lang.String r0 = "params"
            r10 = 4
            r4[r10] = r0     // Catch:{ SQLiteException -> 0x00ba, all -> 0x00b6 }
            com.google.android.gms.internal.measurement.bd<java.lang.Integer> r0 = com.google.android.gms.internal.measurement.bc.d     // Catch:{ SQLiteException -> 0x00ba, all -> 0x00b6 }
            V r0 = r0.f2067a     // Catch:{ SQLiteException -> 0x00ba, all -> 0x00b6 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ SQLiteException -> 0x00ba, all -> 0x00b6 }
            int r0 = r0.intValue()     // Catch:{ SQLiteException -> 0x00ba, all -> 0x00b6 }
            java.lang.String r16 = java.lang.String.valueOf(r0)     // Catch:{ SQLiteException -> 0x00ba, all -> 0x00b6 }
            java.lang.String r5 = "app_uid=?"
            java.lang.String[] r6 = new java.lang.String[r13]     // Catch:{ SQLiteException -> 0x00ba, all -> 0x00b6 }
            java.lang.String r3 = "0"
            r6[r12] = r3     // Catch:{ SQLiteException -> 0x00ba, all -> 0x00b6 }
            java.lang.String r3 = "properties"
            r7 = 0
            r8 = 0
            r9 = 0
            r11 = 4
            r10 = r16
            android.database.Cursor r2 = r2.query(r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ SQLiteException -> 0x00ba, all -> 0x00b6 }
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ SQLiteException -> 0x00b3, all -> 0x00af }
            r3.<init>()     // Catch:{ SQLiteException -> 0x00b3, all -> 0x00af }
            boolean r4 = r2.moveToFirst()     // Catch:{ SQLiteException -> 0x00b3, all -> 0x00af }
            if (r4 == 0) goto L_0x009e
        L_0x0055:
            java.lang.String r4 = r2.getString(r12)     // Catch:{ SQLiteException -> 0x00b3, all -> 0x00af }
            java.lang.String r5 = r2.getString(r13)     // Catch:{ SQLiteException -> 0x00b3, all -> 0x00af }
            int r6 = r2.getInt(r14)     // Catch:{ SQLiteException -> 0x00b3, all -> 0x00af }
            if (r6 == 0) goto L_0x0066
            r19 = 1
            goto L_0x0068
        L_0x0066:
            r19 = 0
        L_0x0068:
            int r6 = r2.getInt(r15)     // Catch:{ SQLiteException -> 0x00b3, all -> 0x00af }
            long r6 = (long) r6     // Catch:{ SQLiteException -> 0x00b3, all -> 0x00af }
            java.lang.String r8 = r2.getString(r11)     // Catch:{ SQLiteException -> 0x00b3, all -> 0x00af }
            java.util.Map r22 = r1.h(r8)     // Catch:{ SQLiteException -> 0x00b3, all -> 0x00af }
            boolean r8 = android.text.TextUtils.isEmpty(r4)     // Catch:{ SQLiteException -> 0x00b3, all -> 0x00af }
            if (r8 != 0) goto L_0x0093
            boolean r8 = android.text.TextUtils.isEmpty(r5)     // Catch:{ SQLiteException -> 0x00b3, all -> 0x00af }
            if (r8 == 0) goto L_0x0082
            goto L_0x0093
        L_0x0082:
            com.google.android.gms.internal.measurement.w r8 = new com.google.android.gms.internal.measurement.w     // Catch:{ SQLiteException -> 0x00b3, all -> 0x00af }
            r16 = r8
            r17 = r4
            r18 = r5
            r20 = r6
            r16.<init>(r17, r18, r19, r20, r22)     // Catch:{ SQLiteException -> 0x00b3, all -> 0x00af }
            r3.add(r8)     // Catch:{ SQLiteException -> 0x00b3, all -> 0x00af }
            goto L_0x0098
        L_0x0093:
            java.lang.String r6 = "Read property with empty client id or tracker id"
            r1.c(r6, r4, r5)     // Catch:{ SQLiteException -> 0x00b3, all -> 0x00af }
        L_0x0098:
            boolean r4 = r2.moveToNext()     // Catch:{ SQLiteException -> 0x00b3, all -> 0x00af }
            if (r4 != 0) goto L_0x0055
        L_0x009e:
            int r4 = r3.size()     // Catch:{ SQLiteException -> 0x00b3, all -> 0x00af }
            if (r4 < r0) goto L_0x00a9
            java.lang.String r0 = "Sending hits to too many properties. Campaign report might be incorrect"
            r1.e(r0)     // Catch:{ SQLiteException -> 0x00b3, all -> 0x00af }
        L_0x00a9:
            if (r2 == 0) goto L_0x00ae
            r2.close()
        L_0x00ae:
            return r3
        L_0x00af:
            r0 = move-exception
            r17 = r2
            goto L_0x00c5
        L_0x00b3:
            r0 = move-exception
            r11 = r2
            goto L_0x00bc
        L_0x00b6:
            r0 = move-exception
            r17 = 0
            goto L_0x00c5
        L_0x00ba:
            r0 = move-exception
            r11 = 0
        L_0x00bc:
            java.lang.String r2 = "Error loading hits from the database"
            r1.e(r2, r0)     // Catch:{ all -> 0x00c2 }
            throw r0     // Catch:{ all -> 0x00c2 }
        L_0x00c2:
            r0 = move-exception
            r17 = r11
        L_0x00c5:
            if (r17 == 0) goto L_0x00ca
            r17.close()
        L_0x00ca:
            goto L_0x00cc
        L_0x00cb:
            throw r0
        L_0x00cc:
            goto L_0x00cb
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.ad.q():java.util.List");
    }

    public final void close() {
        try {
            this.d.close();
        } catch (SQLiteException e2) {
            e("Sql error closing database", e2);
        } catch (IllegalStateException e3) {
            e("Error closing database", e3);
        }
    }

    private final long a(String str) {
        Cursor cursor = null;
        try {
            cursor = r().rawQuery(str, null);
            if (cursor.moveToFirst()) {
                long j = cursor.getLong(0);
                if (cursor != null) {
                    cursor.close();
                }
                return j;
            }
            throw new SQLiteException("Database returned empty set");
        } catch (SQLiteException e2) {
            d("Database error", str, e2);
            throw e2;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    private final long a(String str, String[] strArr) {
        Cursor cursor = null;
        try {
            Cursor rawQuery = r().rawQuery(str, strArr);
            if (rawQuery.moveToFirst()) {
                long j = rawQuery.getLong(0);
                if (rawQuery != null) {
                    rawQuery.close();
                }
                return j;
            } else if (rawQuery == null) {
                return 0;
            } else {
                rawQuery.close();
                return 0;
            }
        } catch (SQLiteException e2) {
            d("Database error", str, e2);
            throw e2;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    private final Map<String, String> g(String str) {
        if (TextUtils.isEmpty(str)) {
            return new HashMap(0);
        }
        try {
            if (!str.startsWith("?")) {
                String valueOf = String.valueOf(str);
                str = valueOf.length() != 0 ? "?".concat(valueOf) : new String("?");
            }
            return k.a(new URI(str), "UTF-8");
        } catch (URISyntaxException e2) {
            e("Error parsing hit parameters", e2);
            return new HashMap(0);
        }
    }

    private final Map<String, String> h(String str) {
        if (TextUtils.isEmpty(str)) {
            return new HashMap(0);
        }
        try {
            String valueOf = String.valueOf(str);
            return k.a(new URI(valueOf.length() != 0 ? "?".concat(valueOf) : new String("?")), "UTF-8");
        } catch (URISyntaxException e2) {
            e("Error parsing property parameters", e2);
            return new HashMap(0);
        }
    }

    /* access modifiers changed from: package-private */
    public final SQLiteDatabase r() {
        try {
            return this.d.getWritableDatabase();
        } catch (SQLiteException e2) {
            d("Error opening database", e2);
            throw e2;
        }
    }
}
