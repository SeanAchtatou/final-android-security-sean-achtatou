package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.IStatusCallback;
import com.google.android.gms.common.api.internal.c;

public class StatusCallback extends IStatusCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    private final c.b<Status> f1382a;

    public final void a(Status status) {
        this.f1382a.a((Object) status);
    }
}
