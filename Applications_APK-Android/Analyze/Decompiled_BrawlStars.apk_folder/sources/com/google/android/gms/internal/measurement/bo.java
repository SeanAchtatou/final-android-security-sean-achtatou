package com.google.android.gms.internal.measurement;

import android.content.SharedPreferences;
import android.text.TextUtils;
import com.google.android.gms.analytics.n;

public final class bo extends r {

    /* renamed from: a  reason: collision with root package name */
    SharedPreferences f2085a;

    /* renamed from: b  reason: collision with root package name */
    final bp f2086b = new bp(this, "monitoring", ((Long) bc.D.f2067a).longValue(), (byte) 0);
    private long d;
    private long e = -1;

    protected bo(t tVar) {
        super(tVar);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2085a = g().getSharedPreferences("com.google.android.gms.analytics.prefs", 0);
    }

    public final long b() {
        n.b();
        m();
        if (this.d == 0) {
            long j = this.f2085a.getLong("first_run", 0);
            if (j != 0) {
                this.d = j;
            } else {
                long a2 = f().a();
                SharedPreferences.Editor edit = this.f2085a.edit();
                edit.putLong("first_run", a2);
                if (!edit.commit()) {
                    e("Failed to commit first run time");
                }
                this.d = a2;
            }
        }
        return this.d;
    }

    public final bw c() {
        return new bw(f(), b());
    }

    public final long d() {
        n.b();
        m();
        if (this.e == -1) {
            this.e = this.f2085a.getLong("last_dispatch", 0);
        }
        return this.e;
    }

    public final void e() {
        n.b();
        m();
        long a2 = f().a();
        SharedPreferences.Editor edit = this.f2085a.edit();
        edit.putLong("last_dispatch", a2);
        edit.apply();
        this.e = a2;
    }

    public final String o() {
        n.b();
        m();
        String string = this.f2085a.getString("installation_campaign", null);
        if (TextUtils.isEmpty(string)) {
            return null;
        }
        return string;
    }

    public final void a(String str) {
        n.b();
        m();
        SharedPreferences.Editor edit = this.f2085a.edit();
        if (TextUtils.isEmpty(str)) {
            edit.remove("installation_campaign");
        } else {
            edit.putString("installation_campaign", str);
        }
        if (!edit.commit()) {
            e("Failed to commit campaign data");
        }
    }
}
