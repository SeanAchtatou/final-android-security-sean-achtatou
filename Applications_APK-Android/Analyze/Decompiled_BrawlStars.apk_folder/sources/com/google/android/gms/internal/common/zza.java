package com.google.android.gms.internal.common;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public class zza implements IInterface {

    /* renamed from: a  reason: collision with root package name */
    protected final IBinder f1885a;

    /* renamed from: b  reason: collision with root package name */
    private final String f1886b;

    protected zza(IBinder iBinder, String str) {
        this.f1885a = iBinder;
        this.f1886b = str;
    }

    public IBinder asBinder() {
        return this.f1885a;
    }

    /* access modifiers changed from: protected */
    public final Parcel s() {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(this.f1886b);
        return obtain;
    }

    /* access modifiers changed from: protected */
    public final Parcel a(int i, Parcel parcel) throws RemoteException {
        parcel = Parcel.obtain();
        try {
            this.f1885a.transact(i, parcel, parcel, 0);
            parcel.readException();
            return parcel;
        } catch (RuntimeException e) {
            throw e;
        } finally {
            parcel.recycle();
        }
    }

    /* access modifiers changed from: protected */
    public final void b(int i, Parcel parcel) throws RemoteException {
        Parcel obtain = Parcel.obtain();
        try {
            this.f1885a.transact(i, parcel, obtain, 0);
            obtain.readException();
        } finally {
            parcel.recycle();
            obtain.recycle();
        }
    }
}
