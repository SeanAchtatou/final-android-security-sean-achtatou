package com.google.android.gms.tasks;

import com.google.android.gms.common.internal.l;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public final class j {

    interface b extends b, d, e<Object> {
    }

    public static <TResult> g<TResult> a(TResult tresult) {
        ab abVar = new ab();
        abVar.a((Object) tresult);
        return abVar;
    }

    public static final class a implements b {

        /* renamed from: a  reason: collision with root package name */
        public final CountDownLatch f2682a;

        private a() {
            this.f2682a = new CountDownLatch(1);
        }

        public final void a(Object obj) {
            this.f2682a.countDown();
        }

        public final void a(Exception exc) {
            this.f2682a.countDown();
        }

        public final void g_() {
            this.f2682a.countDown();
        }

        public /* synthetic */ a(byte b2) {
            this();
        }
    }

    public static <TResult> TResult a(g<TResult> gVar) throws ExecutionException {
        if (gVar.b()) {
            return gVar.d();
        }
        if (gVar.c()) {
            throw new CancellationException("Task is already canceled");
        }
        throw new ExecutionException(gVar.e());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.e):com.google.android.gms.tasks.g<TResult>
     arg types: [java.util.concurrent.Executor, com.google.android.gms.tasks.j$b]
     candidates:
      com.google.android.gms.tasks.g.a(android.app.Activity, com.google.android.gms.tasks.e):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.a):com.google.android.gms.tasks.g<TContinuationResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.b):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.c):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.d):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.f):com.google.android.gms.tasks.g<TContinuationResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.e):com.google.android.gms.tasks.g<TResult> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.d):com.google.android.gms.tasks.g<TResult>
     arg types: [java.util.concurrent.Executor, com.google.android.gms.tasks.j$b]
     candidates:
      com.google.android.gms.tasks.g.a(android.app.Activity, com.google.android.gms.tasks.e):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.a):com.google.android.gms.tasks.g<TContinuationResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.b):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.c):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.e):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.f):com.google.android.gms.tasks.g<TContinuationResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.d):com.google.android.gms.tasks.g<TResult> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.b):com.google.android.gms.tasks.g<TResult>
     arg types: [java.util.concurrent.Executor, com.google.android.gms.tasks.j$b]
     candidates:
      com.google.android.gms.tasks.g.a(android.app.Activity, com.google.android.gms.tasks.e):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.a):com.google.android.gms.tasks.g<TContinuationResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.c):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.d):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.e):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.f):com.google.android.gms.tasks.g<TContinuationResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.b):com.google.android.gms.tasks.g<TResult> */
    public static void a(g<?> gVar, b bVar) {
        gVar.a(i.f2680b, (e) bVar);
        gVar.a(i.f2680b, (d) bVar);
        gVar.a(i.f2680b, (b) bVar);
    }

    public static <TResult> TResult a(g<TResult> gVar, long j, TimeUnit timeUnit) throws ExecutionException, InterruptedException, TimeoutException {
        l.c("Must not be called on the main application thread");
        l.a(gVar, "Task must not be null");
        l.a(timeUnit, "TimeUnit must not be null");
        if (gVar.a()) {
            return a((g) gVar);
        }
        a aVar = new a((byte) 0);
        a(gVar, aVar);
        if (aVar.f2682a.await(j, timeUnit)) {
            return a((g) gVar);
        }
        throw new TimeoutException("Timed out waiting for Task");
    }
}
