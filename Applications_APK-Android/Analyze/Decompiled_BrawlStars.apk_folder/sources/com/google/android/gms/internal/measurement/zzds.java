package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import android.os.Parcel;
import android.os.RemoteException;

public final class zzds extends zzq implements zzdq {
    public final void a(Bundle bundle) throws RemoteException {
        Parcel a2 = a();
        dk.a(a2, bundle);
        b(1, a2);
    }
}
