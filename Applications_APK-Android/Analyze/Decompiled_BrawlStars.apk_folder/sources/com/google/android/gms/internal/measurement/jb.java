package com.google.android.gms.internal.measurement;

public final class jb implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    static final jc f2307a = new jc();

    /* renamed from: b  reason: collision with root package name */
    int[] f2308b;
    jc[] c;
    int d;
    private boolean e;

    jb() {
        this(10);
    }

    private jb(int i) {
        this.e = false;
        int a2 = a(i);
        this.f2308b = new int[a2];
        this.c = new jc[a2];
        this.d = 0;
    }

    public final boolean a() {
        return this.d == 0;
    }

    public final boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof jb)) {
            return false;
        }
        jb jbVar = (jb) obj;
        int i = this.d;
        if (i != jbVar.d) {
            return false;
        }
        int[] iArr = this.f2308b;
        int[] iArr2 = jbVar.f2308b;
        int i2 = 0;
        while (true) {
            if (i2 >= i) {
                z = true;
                break;
            } else if (iArr[i2] != iArr2[i2]) {
                z = false;
                break;
            } else {
                i2++;
            }
        }
        if (z) {
            jc[] jcVarArr = this.c;
            jc[] jcVarArr2 = jbVar.c;
            int i3 = this.d;
            int i4 = 0;
            while (true) {
                if (i4 >= i3) {
                    z2 = true;
                    break;
                } else if (!jcVarArr[i4].equals(jcVarArr2[i4])) {
                    z2 = false;
                    break;
                } else {
                    i4++;
                }
            }
            if (z2) {
                return true;
            }
        }
        return false;
    }

    public final int hashCode() {
        int i = 17;
        for (int i2 = 0; i2 < this.d; i2++) {
            i = (((i * 31) + this.f2308b[i2]) * 31) + this.c[i2].hashCode();
        }
        return i;
    }

    static int a(int i) {
        int i2 = i << 2;
        int i3 = 4;
        while (true) {
            if (i3 >= 32) {
                break;
            }
            int i4 = (1 << i3) - 12;
            if (i2 <= i4) {
                i2 = i4;
                break;
            }
            i3++;
        }
        return i2 / 4;
    }

    /* access modifiers changed from: package-private */
    public final int b(int i) {
        int i2 = this.d - 1;
        int i3 = 0;
        while (i3 <= i2) {
            int i4 = (i3 + i2) >>> 1;
            int i5 = this.f2308b[i4];
            if (i5 < i) {
                i3 = i4 + 1;
            } else if (i5 <= i) {
                return i4;
            } else {
                i2 = i4 - 1;
            }
        }
        return i3 ^ -1;
    }

    public final /* synthetic */ Object clone() throws CloneNotSupportedException {
        int i = this.d;
        jb jbVar = new jb(i);
        System.arraycopy(this.f2308b, 0, jbVar.f2308b, 0, i);
        for (int i2 = 0; i2 < i; i2++) {
            jc[] jcVarArr = this.c;
            if (jcVarArr[i2] != null) {
                jbVar.c[i2] = (jc) jcVarArr[i2].clone();
            }
        }
        jbVar.d = i;
        return jbVar;
    }
}
