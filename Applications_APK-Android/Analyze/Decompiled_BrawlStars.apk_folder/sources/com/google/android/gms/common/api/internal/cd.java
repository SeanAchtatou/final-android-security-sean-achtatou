package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;

final class cd implements ba {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ cc f1453a;

    private cd(cc ccVar) {
        this.f1453a = ccVar;
    }

    public final void a(Bundle bundle) {
        this.f1453a.g.lock();
        try {
            cc ccVar = this.f1453a;
            if (ccVar.c == null) {
                ccVar.c = bundle;
            } else if (bundle != null) {
                ccVar.c.putAll(bundle);
            }
            this.f1453a.d = ConnectionResult.f1352a;
            cc.a(this.f1453a);
        } finally {
            this.f1453a.g.unlock();
        }
    }

    public final void a(ConnectionResult connectionResult) {
        this.f1453a.g.lock();
        try {
            this.f1453a.d = connectionResult;
            cc.a(this.f1453a);
        } finally {
            this.f1453a.g.unlock();
        }
    }

    public final void a(int i, boolean z) {
        this.f1453a.g.lock();
        try {
            if (!this.f1453a.f && this.f1453a.e != null) {
                if (this.f1453a.e.b()) {
                    this.f1453a.f = true;
                    this.f1453a.f1452b.onConnectionSuspended(i);
                    this.f1453a.g.unlock();
                    return;
                }
            }
            this.f1453a.f = false;
            cc.a(this.f1453a, i, z);
        } finally {
            this.f1453a.g.unlock();
        }
    }

    /* synthetic */ cd(cc ccVar, byte b2) {
        this(ccVar);
    }
}
