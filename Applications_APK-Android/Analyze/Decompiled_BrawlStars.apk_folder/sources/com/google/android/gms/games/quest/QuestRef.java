package com.google.android.gms.games.quest;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.d;
import com.google.android.gms.games.Game;
import java.util.ArrayList;
import java.util.List;

public final class QuestRef extends d implements Quest {
    private final Game c;
    private final int d;

    public final /* synthetic */ Object a() {
        return new QuestEntity(this);
    }

    public final String b() {
        return e("external_quest_id");
    }

    public final String c() {
        return e("quest_name");
    }

    public final String d() {
        return e("quest_description");
    }

    public final int describeContents() {
        return 0;
    }

    public final Uri e() {
        return h("quest_banner_image_uri");
    }

    public final boolean equals(Object obj) {
        return QuestEntity.a(this, obj);
    }

    public final Uri f() {
        return h("quest_icon_image_uri");
    }

    public final List<Milestone> g() {
        ArrayList arrayList = new ArrayList(this.d);
        for (int i = 0; i < this.d; i++) {
            arrayList.add(new zzb(this.f1532a, this.f1533b + i));
        }
        return arrayList;
    }

    public final String getBannerImageUrl() {
        return e("quest_banner_image_url");
    }

    public final String getIconImageUrl() {
        return e("quest_icon_image_url");
    }

    public final Game h() {
        return this.c;
    }

    public final int hashCode() {
        return QuestEntity.a(this);
    }

    public final int i() {
        return c("quest_state");
    }

    public final int j() {
        return c("quest_type");
    }

    public final long k() {
        return b("accepted_ts");
    }

    public final long l() {
        return b("quest_end_ts");
    }

    public final long m() {
        return b("quest_last_updated_ts");
    }

    public final long n() {
        return b("notification_ts");
    }

    public final long o() {
        return b("quest_start_ts");
    }

    public final String toString() {
        return QuestEntity.b(this);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        ((QuestEntity) ((Quest) a())).writeToParcel(parcel, i);
    }
}
