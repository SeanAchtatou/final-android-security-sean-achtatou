package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class cs extends iz<cs> {
    private static volatile cs[] c;

    /* renamed from: a  reason: collision with root package name */
    public Integer f2134a = null;

    /* renamed from: b  reason: collision with root package name */
    public Long f2135b = null;

    public static cs[] a() {
        if (c == null) {
            synchronized (jd.f2312b) {
                if (c == null) {
                    c = new cs[0];
                }
            }
        }
        return c;
    }

    public cs() {
        this.L = null;
        this.M = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof cs)) {
            return false;
        }
        cs csVar = (cs) obj;
        Integer num = this.f2134a;
        if (num == null) {
            if (csVar.f2134a != null) {
                return false;
            }
        } else if (!num.equals(csVar.f2134a)) {
            return false;
        }
        Long l = this.f2135b;
        if (l == null) {
            if (csVar.f2135b != null) {
                return false;
            }
        } else if (!l.equals(csVar.f2135b)) {
            return false;
        }
        if (this.L == null || this.L.a()) {
            return csVar.L == null || csVar.L.a();
        }
        return this.L.equals(csVar.L);
    }

    public final int hashCode() {
        int hashCode = (getClass().getName().hashCode() + 527) * 31;
        Integer num = this.f2134a;
        int i = 0;
        int hashCode2 = (hashCode + (num == null ? 0 : num.hashCode())) * 31;
        Long l = this.f2135b;
        int hashCode3 = (hashCode2 + (l == null ? 0 : l.hashCode())) * 31;
        if (this.L != null && !this.L.a()) {
            i = this.L.hashCode();
        }
        return hashCode3 + i;
    }

    public final void a(iy iyVar) throws IOException {
        Integer num = this.f2134a;
        if (num != null) {
            iyVar.a(1, num.intValue());
        }
        Long l = this.f2135b;
        if (l != null) {
            iyVar.b(2, l.longValue());
        }
        super.a(iyVar);
    }

    /* access modifiers changed from: protected */
    public final int b() {
        int b2 = super.b();
        Integer num = this.f2134a;
        if (num != null) {
            b2 += iy.b(1, num.intValue());
        }
        Long l = this.f2135b;
        return l != null ? b2 + iy.c(2, l.longValue()) : b2;
    }

    public final /* synthetic */ je a(iw iwVar) throws IOException {
        while (true) {
            int a2 = iwVar.a();
            if (a2 == 0) {
                return this;
            }
            if (a2 == 8) {
                this.f2134a = Integer.valueOf(iwVar.d());
            } else if (a2 == 16) {
                this.f2135b = Long.valueOf(iwVar.e());
            } else if (!super.a(iwVar, a2)) {
                return this;
            }
        }
    }
}
