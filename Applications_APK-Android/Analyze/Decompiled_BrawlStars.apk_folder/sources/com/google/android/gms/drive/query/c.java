package com.google.android.gms.drive.query;

import com.google.android.gms.drive.metadata.a;
import com.google.android.gms.drive.query.internal.g;
import com.google.android.gms.drive.query.internal.zzx;
import java.util.Iterator;
import java.util.List;

public final class c implements g<String> {
    public final /* synthetic */ Object a() {
        return "all()";
    }

    public final /* synthetic */ Object a(a aVar) {
        return String.format("fieldOnly(%s)", aVar.a());
    }

    public final /* synthetic */ Object a(a aVar, Object obj) {
        return String.format("has(%s,%s)", aVar.a(), obj);
    }

    public final /* synthetic */ Object a(Object obj) {
        return String.format("not(%s)", (String) obj);
    }

    public final /* synthetic */ Object a(String str) {
        return String.format("fullTextSearch(%s)", str);
    }

    public final /* synthetic */ Object b() {
        return "ownedByMe()";
    }

    public final /* synthetic */ Object a(zzx zzx, List list) {
        StringBuilder sb = new StringBuilder(String.valueOf(zzx.i).concat("("));
        Iterator it = list.iterator();
        String str = "";
        while (it.hasNext()) {
            sb.append(str);
            sb.append((String) it.next());
            str = ",";
        }
        sb.append(")");
        return sb.toString();
    }

    public final /* synthetic */ Object a(com.google.android.gms.drive.metadata.g gVar, Object obj) {
        return String.format("contains(%s,%s)", gVar.f1731b, obj);
    }

    public final /* synthetic */ Object a(zzx zzx, a aVar, Object obj) {
        return String.format("cmp(%s,%s,%s)", zzx.i, aVar.a(), obj);
    }
}
