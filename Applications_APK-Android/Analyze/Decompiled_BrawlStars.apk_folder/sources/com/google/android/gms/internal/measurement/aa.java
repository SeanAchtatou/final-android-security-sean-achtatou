package com.google.android.gms.internal.measurement;

import android.content.ComponentName;
import android.content.ServiceConnection;
import com.google.android.gms.common.internal.l;

public final class aa implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ y f2031a;

    /* renamed from: b  reason: collision with root package name */
    private volatile zzcl f2032b;
    private volatile boolean c;

    protected aa(y yVar) {
        this.f2031a = yVar;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:9|10|11|12|13|(1:15)) */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0071, code lost:
        return r0;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x005a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.measurement.zzcl a() {
        /*
            r6 = this;
            com.google.android.gms.analytics.n.b()
            android.content.Intent r0 = new android.content.Intent
            java.lang.String r1 = "com.google.android.gms.analytics.service.START"
            r0.<init>(r1)
            android.content.ComponentName r1 = new android.content.ComponentName
            java.lang.String r2 = "com.google.android.gms"
            java.lang.String r3 = "com.google.android.gms.analytics.service.AnalyticsService"
            r1.<init>(r2, r3)
            r0.setComponent(r1)
            com.google.android.gms.internal.measurement.y r1 = r6.f2031a
            android.content.Context r1 = r1.g()
            java.lang.String r2 = r1.getPackageName()
            java.lang.String r3 = "app_package_name"
            r0.putExtra(r3, r2)
            com.google.android.gms.common.stats.a r2 = com.google.android.gms.common.stats.a.a()
            monitor-enter(r6)
            r3 = 0
            r6.f2032b = r3     // Catch:{ all -> 0x0072 }
            r4 = 1
            r6.c = r4     // Catch:{ all -> 0x0072 }
            com.google.android.gms.internal.measurement.y r4 = r6.f2031a     // Catch:{ all -> 0x0072 }
            com.google.android.gms.internal.measurement.aa r4 = r4.f2341a     // Catch:{ all -> 0x0072 }
            r5 = 129(0x81, float:1.81E-43)
            boolean r0 = r2.b(r1, r0, r4, r5)     // Catch:{ all -> 0x0072 }
            com.google.android.gms.internal.measurement.y r1 = r6.f2031a     // Catch:{ all -> 0x0072 }
            java.lang.String r2 = "Bind to service requested"
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x0072 }
            r1.a(r2, r4)     // Catch:{ all -> 0x0072 }
            r1 = 0
            if (r0 != 0) goto L_0x004c
            r6.c = r1     // Catch:{ all -> 0x0072 }
            monitor-exit(r6)     // Catch:{ all -> 0x0072 }
            return r3
        L_0x004c:
            com.google.android.gms.internal.measurement.bd<java.lang.Long> r0 = com.google.android.gms.internal.measurement.bc.B     // Catch:{ InterruptedException -> 0x005a }
            V r0 = r0.f2067a     // Catch:{ InterruptedException -> 0x005a }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ InterruptedException -> 0x005a }
            long r4 = r0.longValue()     // Catch:{ InterruptedException -> 0x005a }
            r6.wait(r4)     // Catch:{ InterruptedException -> 0x005a }
            goto L_0x0061
        L_0x005a:
            com.google.android.gms.internal.measurement.y r0 = r6.f2031a     // Catch:{ all -> 0x0072 }
            java.lang.String r2 = "Wait for service connect was interrupted"
            r0.e(r2)     // Catch:{ all -> 0x0072 }
        L_0x0061:
            r6.c = r1     // Catch:{ all -> 0x0072 }
            com.google.android.gms.internal.measurement.zzcl r0 = r6.f2032b     // Catch:{ all -> 0x0072 }
            r6.f2032b = r3     // Catch:{ all -> 0x0072 }
            if (r0 != 0) goto L_0x0070
            com.google.android.gms.internal.measurement.y r1 = r6.f2031a     // Catch:{ all -> 0x0072 }
            java.lang.String r2 = "Successfully bound to service but never got onServiceConnected callback"
            r1.f(r2)     // Catch:{ all -> 0x0072 }
        L_0x0070:
            monitor-exit(r6)     // Catch:{ all -> 0x0072 }
            return r0
        L_0x0072:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x0072 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.aa.a():com.google.android.gms.internal.measurement.zzcl");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:23|24) */
    /* JADX WARNING: Can't wrap try/catch for region: R(9:10|11|12|(2:(1:15)(3:16|(1:18)(1:19)|20)|21)(1:22)|(2:26|27)(3:28|29|(1:31)(1:32))|33|34|35|36) */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        r2.f2031a.f("Service connect failed to get IAnalyticsService");
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x004a */
    /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x0082 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onServiceConnected(android.content.ComponentName r3, android.os.IBinder r4) {
        /*
            r2 = this;
            java.lang.String r3 = "AnalyticsServiceConnection.onServiceConnected"
            com.google.android.gms.common.internal.l.b(r3)
            monitor-enter(r2)
            if (r4 != 0) goto L_0x0017
            com.google.android.gms.internal.measurement.y r3 = r2.f2031a     // Catch:{ all -> 0x0014 }
            java.lang.String r4 = "Service connected with null binder"
            r3.f(r4)     // Catch:{ all -> 0x0014 }
            r2.notifyAll()     // Catch:{ all -> 0x008b }
            monitor-exit(r2)     // Catch:{ all -> 0x008b }
            return
        L_0x0014:
            r3 = move-exception
            goto L_0x0087
        L_0x0017:
            r3 = 0
            java.lang.String r0 = r4.getInterfaceDescriptor()     // Catch:{ RemoteException -> 0x004a }
            java.lang.String r1 = "com.google.android.gms.analytics.internal.IAnalyticsService"
            boolean r1 = r1.equals(r0)     // Catch:{ RemoteException -> 0x004a }
            if (r1 == 0) goto L_0x0042
            if (r4 != 0) goto L_0x0027
            goto L_0x003a
        L_0x0027:
            java.lang.String r0 = "com.google.android.gms.analytics.internal.IAnalyticsService"
            android.os.IInterface r0 = r4.queryLocalInterface(r0)     // Catch:{ RemoteException -> 0x004a }
            boolean r1 = r0 instanceof com.google.android.gms.internal.measurement.zzcl     // Catch:{ RemoteException -> 0x004a }
            if (r1 == 0) goto L_0x0034
            com.google.android.gms.internal.measurement.zzcl r0 = (com.google.android.gms.internal.measurement.zzcl) r0     // Catch:{ RemoteException -> 0x004a }
            goto L_0x0039
        L_0x0034:
            com.google.android.gms.internal.measurement.zzcm r0 = new com.google.android.gms.internal.measurement.zzcm     // Catch:{ RemoteException -> 0x004a }
            r0.<init>(r4)     // Catch:{ RemoteException -> 0x004a }
        L_0x0039:
            r3 = r0
        L_0x003a:
            com.google.android.gms.internal.measurement.y r4 = r2.f2031a     // Catch:{ RemoteException -> 0x004a }
            java.lang.String r0 = "Bound to IAnalyticsService interface"
            r4.b(r0)     // Catch:{ RemoteException -> 0x004a }
            goto L_0x0051
        L_0x0042:
            com.google.android.gms.internal.measurement.y r4 = r2.f2031a     // Catch:{ RemoteException -> 0x004a }
            java.lang.String r1 = "Got binder with a wrong descriptor"
            r4.e(r1, r0)     // Catch:{ RemoteException -> 0x004a }
            goto L_0x0051
        L_0x004a:
            com.google.android.gms.internal.measurement.y r4 = r2.f2031a     // Catch:{ all -> 0x0014 }
            java.lang.String r0 = "Service connect failed to get IAnalyticsService"
            r4.f(r0)     // Catch:{ all -> 0x0014 }
        L_0x0051:
            if (r3 != 0) goto L_0x0064
            com.google.android.gms.common.stats.a.a()     // Catch:{ IllegalArgumentException -> 0x0082 }
            com.google.android.gms.internal.measurement.y r3 = r2.f2031a     // Catch:{ IllegalArgumentException -> 0x0082 }
            android.content.Context r3 = r3.g()     // Catch:{ IllegalArgumentException -> 0x0082 }
            com.google.android.gms.internal.measurement.y r4 = r2.f2031a     // Catch:{ IllegalArgumentException -> 0x0082 }
            com.google.android.gms.internal.measurement.aa r4 = r4.f2341a     // Catch:{ IllegalArgumentException -> 0x0082 }
            com.google.android.gms.common.stats.a.a(r3, r4)     // Catch:{ IllegalArgumentException -> 0x0082 }
            goto L_0x0082
        L_0x0064:
            boolean r4 = r2.c     // Catch:{ all -> 0x0014 }
            if (r4 != 0) goto L_0x0080
            com.google.android.gms.internal.measurement.y r4 = r2.f2031a     // Catch:{ all -> 0x0014 }
            java.lang.String r0 = "onServiceConnected received after the timeout limit"
            r4.e(r0)     // Catch:{ all -> 0x0014 }
            com.google.android.gms.internal.measurement.y r4 = r2.f2031a     // Catch:{ all -> 0x0014 }
            com.google.android.gms.internal.measurement.t r4 = r4.c     // Catch:{ all -> 0x0014 }
            com.google.android.gms.analytics.n r4 = r4.b()     // Catch:{ all -> 0x0014 }
            com.google.android.gms.internal.measurement.ab r0 = new com.google.android.gms.internal.measurement.ab     // Catch:{ all -> 0x0014 }
            r0.<init>(r2, r3)     // Catch:{ all -> 0x0014 }
            r4.a(r0)     // Catch:{ all -> 0x0014 }
            goto L_0x0082
        L_0x0080:
            r2.f2032b = r3     // Catch:{ all -> 0x0014 }
        L_0x0082:
            r2.notifyAll()     // Catch:{ all -> 0x008b }
            monitor-exit(r2)     // Catch:{ all -> 0x008b }
            return
        L_0x0087:
            r2.notifyAll()     // Catch:{ all -> 0x008b }
            throw r3     // Catch:{ all -> 0x008b }
        L_0x008b:
            r3 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x008b }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.aa.onServiceConnected(android.content.ComponentName, android.os.IBinder):void");
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        l.b("AnalyticsServiceConnection.onServiceDisconnected");
        this.f2031a.c.b().a(new ac(this, componentName));
    }
}
