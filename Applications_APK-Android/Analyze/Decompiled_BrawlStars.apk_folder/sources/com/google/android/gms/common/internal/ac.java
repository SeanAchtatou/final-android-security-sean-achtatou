package com.google.android.gms.common.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.google.android.gms.common.internal.g;
import com.google.android.gms.common.stats.a;
import com.google.android.gms.internal.common.c;
import java.util.HashMap;

final class ac extends g implements Handler.Callback {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final HashMap<g.a, ad> f1585a = new HashMap<>();
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final Context f1586b;
    /* access modifiers changed from: private */
    public final Handler c;
    /* access modifiers changed from: private */
    public final a d;
    private final long e;
    /* access modifiers changed from: private */
    public final long f;

    ac(Context context) {
        this.f1586b = context.getApplicationContext();
        this.c = new c(context.getMainLooper(), this);
        this.d = a.a();
        this.e = 5000;
        this.f = 300000;
    }

    /* access modifiers changed from: protected */
    public final boolean a(g.a aVar, ServiceConnection serviceConnection, String str) {
        boolean z;
        l.a(serviceConnection, "ServiceConnection must not be null");
        synchronized (this.f1585a) {
            ad adVar = this.f1585a.get(aVar);
            if (adVar == null) {
                adVar = new ad(this, aVar);
                adVar.a(serviceConnection);
                adVar.a(str);
                this.f1585a.put(aVar, adVar);
            } else {
                this.c.removeMessages(0, aVar);
                if (!adVar.b(serviceConnection)) {
                    adVar.a(serviceConnection);
                    int i = adVar.f1588b;
                    if (i == 1) {
                        serviceConnection.onServiceConnected(adVar.f, adVar.d);
                    } else if (i == 2) {
                        adVar.a(str);
                    }
                } else {
                    String valueOf = String.valueOf(aVar);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 81);
                    sb.append("Trying to bind a GmsServiceConnection that was already connected before.  config=");
                    sb.append(valueOf);
                    throw new IllegalStateException(sb.toString());
                }
            }
            z = adVar.c;
        }
        return z;
    }

    /* access modifiers changed from: protected */
    public final void b(g.a aVar, ServiceConnection serviceConnection, String str) {
        l.a(serviceConnection, "ServiceConnection must not be null");
        synchronized (this.f1585a) {
            ad adVar = this.f1585a.get(aVar);
            if (adVar == null) {
                String valueOf = String.valueOf(aVar);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 50);
                sb.append("Nonexistent connection status for service config: ");
                sb.append(valueOf);
                throw new IllegalStateException(sb.toString());
            } else if (adVar.b(serviceConnection)) {
                a aVar2 = adVar.g.d;
                Context context = adVar.g.f1586b;
                adVar.f1587a.remove(serviceConnection);
                if (adVar.a()) {
                    this.c.sendMessageDelayed(this.c.obtainMessage(0, aVar), this.e);
                }
            } else {
                String valueOf2 = String.valueOf(aVar);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 76);
                sb2.append("Trying to unbind a GmsServiceConnection  that was not bound before.  config=");
                sb2.append(valueOf2);
                throw new IllegalStateException(sb2.toString());
            }
        }
    }

    public final boolean handleMessage(Message message) {
        int i = message.what;
        if (i == 0) {
            synchronized (this.f1585a) {
                g.a aVar = (g.a) message.obj;
                ad adVar = this.f1585a.get(aVar);
                if (adVar != null && adVar.a()) {
                    if (adVar.c) {
                        adVar.g.c.removeMessages(1, adVar.e);
                        a aVar2 = adVar.g.d;
                        a.a(adVar.g.f1586b, adVar);
                        adVar.c = false;
                        adVar.f1588b = 2;
                    }
                    this.f1585a.remove(aVar);
                }
            }
            return true;
        } else if (i != 1) {
            return false;
        } else {
            synchronized (this.f1585a) {
                g.a aVar3 = (g.a) message.obj;
                ad adVar2 = this.f1585a.get(aVar3);
                if (adVar2 != null && adVar2.f1588b == 3) {
                    String valueOf = String.valueOf(aVar3);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 47);
                    sb.append("Timeout waiting for ServiceConnection callback ");
                    sb.append(valueOf);
                    Log.wtf("GmsClientSupervisor", sb.toString(), new Exception());
                    ComponentName componentName = adVar2.f;
                    if (componentName == null) {
                        componentName = aVar3.f1606b;
                    }
                    if (componentName == null) {
                        componentName = new ComponentName(aVar3.f1605a, "unknown");
                    }
                    adVar2.onServiceDisconnected(componentName);
                }
            }
            return true;
        }
    }
}
