package com.google.android.gms.common.internal;

public final class af {

    /* renamed from: a  reason: collision with root package name */
    final String f1591a;

    /* renamed from: b  reason: collision with root package name */
    final String f1592b;
    final int c = 129;
    private final boolean d;

    public af(String str, String str2, boolean z) {
        this.f1592b = str;
        this.f1591a = str2;
        this.d = z;
    }
}
