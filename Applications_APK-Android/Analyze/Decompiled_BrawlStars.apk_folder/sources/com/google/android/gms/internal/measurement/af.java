package com.google.android.gms.internal.measurement;

public final class af extends r {

    /* renamed from: a  reason: collision with root package name */
    private final hy f2040a = new hy();

    af(t tVar) {
        super(tVar);
    }

    public final hy b() {
        m();
        return this.f2040a;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.c.b().a().a(this.f2040a);
        by e = this.c.e();
        String c = e.c();
        if (c != null) {
            this.f2040a.f2277a = c;
        }
        String b2 = e.b();
        if (b2 != null) {
            this.f2040a.f2278b = b2;
        }
    }
}
