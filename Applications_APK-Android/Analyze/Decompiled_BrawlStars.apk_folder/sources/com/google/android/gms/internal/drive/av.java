package com.google.android.gms.internal.drive;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.metadata.internal.b;
import java.util.Collection;

final class av extends b {
    av(String str, Collection collection, Collection collection2) {
        super(str, collection, collection2);
    }

    public final Boolean a_(DataHolder dataHolder, int i, int i2) {
        return Boolean.valueOf(dataHolder.b("trashed", i, i2) == 2);
    }

    public final /* synthetic */ Object c(DataHolder dataHolder, int i, int i2) {
        return c(dataHolder, i, i2);
    }
}
