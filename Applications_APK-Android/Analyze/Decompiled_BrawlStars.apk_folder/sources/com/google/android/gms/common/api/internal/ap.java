package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.util.a.c;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class ap {

    /* renamed from: a  reason: collision with root package name */
    private static final ExecutorService f1401a = Executors.newFixedThreadPool(2, new c("GAC_Executor"));

    public static ExecutorService a() {
        return f1401a;
    }
}
