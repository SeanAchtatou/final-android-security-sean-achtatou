package com.google.android.gms.instantapps;

import android.content.Context;
import com.google.android.gms.common.api.a;
import com.google.android.gms.internal.instantapps.e;
import com.google.android.gms.internal.instantapps.t;

public final class a {
    @Deprecated

    /* renamed from: a  reason: collision with root package name */
    public static final com.google.android.gms.common.api.a<Object> f1860a = new com.google.android.gms.common.api.a<>("InstantApps.API", c, f1861b);

    /* renamed from: b  reason: collision with root package name */
    private static final a.g<com.google.android.gms.internal.instantapps.a> f1861b = new a.g<>();
    private static final a.C0111a<com.google.android.gms.internal.instantapps.a, Object> c = new e();
    @Deprecated
    private static final f d = new t();

    public static b a(Context context) {
        return e.a(context);
    }
}
