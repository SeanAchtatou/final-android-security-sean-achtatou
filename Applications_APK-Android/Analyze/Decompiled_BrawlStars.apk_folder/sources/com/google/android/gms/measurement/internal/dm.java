package com.google.android.gms.measurement.internal;

final class dm extends et {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ dj f2515a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    dm(dj djVar, bo boVar) {
        super(boVar);
        this.f2515a = djVar;
    }

    public final void a() {
        dj djVar = this.f2515a;
        djVar.c();
        djVar.q().k.a("Current session is expired, remove the session number and Id");
        if (djVar.s().h(djVar.f().v())) {
            djVar.e().a("auto", "_sid", (Object) null, djVar.l().a());
        }
        if (djVar.s().i(djVar.f().v())) {
            djVar.e().a("auto", "_sno", (Object) null, djVar.l().a());
        }
    }
}
