package com.google.android.gms.internal.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.d;
import com.google.android.gms.common.api.internal.c;
import com.google.android.gms.games.achievement.a;
import com.google.android.gms.games.internal.zze;

final class v extends w {
    private final /* synthetic */ String d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    v(String str, d dVar, String str2) {
        super(str, dVar);
        this.d = str2;
    }

    public final /* synthetic */ void b(a.b bVar) throws RemoteException {
        ((zze) bVar).a((c.b<a.C0119a>) null, this.d);
    }
}
