package com.google.android.gms.common.api.internal;

import android.os.Message;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.e;
import com.google.android.gms.common.api.g;
import com.google.android.gms.internal.base.d;

final class bl extends d {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ bj f1426a;

    public final void handleMessage(Message message) {
        int i = message.what;
        if (i == 0) {
            e<R> eVar = (e) message.obj;
            synchronized (this.f1426a.e) {
                if (eVar == null) {
                    this.f1426a.f1423b.a(new Status(13, "Transform returned null"));
                } else if (eVar instanceof bf) {
                    this.f1426a.f1423b.a(((bf) eVar).f1418a);
                } else {
                    bj<? extends g> bjVar = this.f1426a.f1423b;
                    synchronized (bjVar.e) {
                        bjVar.d = eVar;
                        if (bjVar.f1422a != null || bjVar.c != null) {
                            com.google.android.gms.common.api.d dVar = bjVar.g.get();
                            if (!(bjVar.i || bjVar.f1422a == null || dVar == null)) {
                                dVar.a(bjVar);
                                bjVar.i = true;
                            }
                            if (bjVar.f != null) {
                                bjVar.b(bjVar.f);
                            } else if (bjVar.d != null) {
                                bjVar.d.a(bjVar);
                            }
                        }
                    }
                }
            }
        } else if (i != 1) {
            int i2 = message.what;
            StringBuilder sb = new StringBuilder(70);
            sb.append("TransformationResultHandler received unknown message type: ");
            sb.append(i2);
            sb.toString();
        } else {
            RuntimeException runtimeException = (RuntimeException) message.obj;
            String valueOf = String.valueOf(runtimeException.getMessage());
            if (valueOf.length() != 0) {
                "Runtime exception on the transformation worker thread: ".concat(valueOf);
            } else {
                new String("Runtime exception on the transformation worker thread: ");
            }
            throw runtimeException;
        }
    }
}
