package com.google.android.gms.measurement.internal;

final class ck implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ cg f2467a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ ch f2468b;

    ck(ch chVar, cg cgVar) {
        this.f2468b = chVar;
        this.f2467a = cgVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.ch.a(com.google.android.gms.measurement.internal.ch, com.google.android.gms.measurement.internal.cg, boolean):void
     arg types: [com.google.android.gms.measurement.internal.ch, com.google.android.gms.measurement.internal.cg, int]
     candidates:
      com.google.android.gms.measurement.internal.ch.a(com.google.android.gms.measurement.internal.cg, android.os.Bundle, boolean):void
      com.google.android.gms.measurement.internal.ch.a(android.app.Activity, com.google.android.gms.measurement.internal.cg, boolean):void
      com.google.android.gms.measurement.internal.ch.a(com.google.android.gms.measurement.internal.ch, com.google.android.gms.measurement.internal.cg, boolean):void */
    public final void run() {
        ch.a(this.f2468b, this.f2467a, false);
        ch chVar = this.f2468b;
        chVar.f2463a = null;
        chVar.g().a((cg) null);
    }
}
