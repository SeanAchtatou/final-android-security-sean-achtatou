package com.google.android.gms.common;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class i implements Parcelable.Creator<ConnectionResult> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new ConnectionResult[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        int i = 0;
        PendingIntent pendingIntent = null;
        String str = null;
        int i2 = 0;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            int i3 = 65535 & readInt;
            if (i3 == 1) {
                i = SafeParcelReader.d(parcel, readInt);
            } else if (i3 == 2) {
                i2 = SafeParcelReader.d(parcel, readInt);
            } else if (i3 == 3) {
                pendingIntent = (PendingIntent) SafeParcelReader.a(parcel, readInt, PendingIntent.CREATOR);
            } else if (i3 != 4) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                str = SafeParcelReader.l(parcel, readInt);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new ConnectionResult(i, i2, pendingIntent, str);
    }
}
