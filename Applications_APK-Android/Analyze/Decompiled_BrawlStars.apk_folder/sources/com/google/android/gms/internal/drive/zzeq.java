package com.google.android.gms.internal.drive;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;

public interface zzeq extends IInterface {
    void a() throws RemoteException;

    void a(Status status) throws RemoteException;

    void a(zzfb zzfb) throws RemoteException;

    void a(zzfd zzfd) throws RemoteException;

    void a(zzfh zzfh) throws RemoteException;

    void a(zzfn zzfn) throws RemoteException;

    void a(zzfp zzfp) throws RemoteException;

    void a(zzfs zzfs) throws RemoteException;

    void a(zzfu zzfu) throws RemoteException;
}
