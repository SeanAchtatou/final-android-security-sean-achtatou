package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.util.ArraySet;
import android.text.TextUtils;
import com.facebook.share.internal.ShareConstants;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import java.util.Collections;
import java.util.Set;
import java.util.regex.Pattern;

public class DriveSpace extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Parcelable.Creator<DriveSpace> CREATOR = new j();

    /* renamed from: a  reason: collision with root package name */
    public static final DriveSpace f1698a = new DriveSpace("DRIVE");

    /* renamed from: b  reason: collision with root package name */
    public static final DriveSpace f1699b = new DriveSpace("APP_DATA_FOLDER");
    public static final DriveSpace c = new DriveSpace(ShareConstants.PHOTOS);
    private static final Set<DriveSpace> d;
    private static final String e = TextUtils.join(",", d.toArray());
    private static final Pattern f = Pattern.compile("[A-Z0-9_]*");
    private final String g;

    DriveSpace(String str) {
        this.g = (String) l.a((Object) str);
    }

    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != DriveSpace.class) {
            return false;
        }
        return this.g.equals(((DriveSpace) obj).g);
    }

    public int hashCode() {
        return this.g.hashCode() ^ 1247068382;
    }

    public String toString() {
        return this.g;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void */
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.a(parcel, 2, this.g, false);
        a.b(parcel, a2);
    }

    static {
        DriveSpace driveSpace = f1698a;
        DriveSpace driveSpace2 = f1699b;
        DriveSpace driveSpace3 = c;
        ArraySet arraySet = new ArraySet(3);
        arraySet.add(driveSpace);
        arraySet.add(driveSpace2);
        arraySet.add(driveSpace3);
        d = Collections.unmodifiableSet(arraySet);
    }
}
