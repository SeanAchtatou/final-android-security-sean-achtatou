package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import android.util.Pair;
import com.google.android.gms.common.internal.h;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.drive.events.DriveEvent;
import com.google.android.gms.drive.events.i;
import java.util.List;

public final class zzee extends zzet {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final h f1926a = new h("EventCallback", "");

    /* renamed from: b  reason: collision with root package name */
    private final int f1927b;
    private final i c;
    private final l d;
    private final List<Integer> e;

    public final void a(zzfj zzfj) throws RemoteException {
        DriveEvent a2 = zzfj.a();
        l.a(this.f1927b == a2.a());
        l.a(this.e.contains(Integer.valueOf(a2.a())));
        l lVar = this.d;
        lVar.sendMessage(lVar.obtainMessage(1, new Pair(this.c, a2)));
    }
}
