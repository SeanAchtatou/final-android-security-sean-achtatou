package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.drive.DriveId;
import java.util.Arrays;

public final class zzh extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzh> CREATOR = new bf();

    /* renamed from: a  reason: collision with root package name */
    final int f1974a;

    /* renamed from: b  reason: collision with root package name */
    final DriveId f1975b;
    final int c;
    final long d;
    final long e;

    public zzh(int i, DriveId driveId, int i2, long j, long j2) {
        this.f1974a = i;
        this.f1975b = driveId;
        this.c = i2;
        this.d = j;
        this.e = j2;
    }

    public final boolean equals(Object obj) {
        if (obj != null && obj.getClass() == getClass()) {
            if (obj == this) {
                return true;
            }
            zzh zzh = (zzh) obj;
            return this.f1974a == zzh.f1974a && j.a(this.f1975b, zzh.f1975b) && this.c == zzh.c && this.d == zzh.d && this.e == zzh.e;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.DriveId, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.b(parcel, 2, this.f1974a);
        a.a(parcel, 3, (Parcelable) this.f1975b, i, false);
        a.b(parcel, 4, this.c);
        a.a(parcel, 5, this.d);
        a.a(parcel, 6, this.e);
        a.b(parcel, a2);
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.f1974a), this.f1975b, Integer.valueOf(this.c), Long.valueOf(this.d), Long.valueOf(this.e)});
    }
}
