package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.ce;

final class ch implements fv {

    /* renamed from: a  reason: collision with root package name */
    static final fv f2113a = new ch();

    private ch() {
    }

    public final boolean a(int i) {
        return ce.b.C0121b.a(i) != null;
    }
}
