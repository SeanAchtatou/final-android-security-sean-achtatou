package com.google.android.gms.measurement.internal;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;
import com.facebook.appevents.codeless.internal.Constants;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.internal.measurement.ce;
import com.google.android.gms.internal.measurement.ct;
import com.google.android.gms.internal.measurement.cu;
import com.google.android.gms.internal.measurement.cv;
import com.google.android.gms.internal.measurement.cw;
import com.google.android.gms.internal.measurement.cz;
import com.google.android.gms.internal.measurement.fq;
import com.google.android.gms.internal.measurement.iy;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

final class ce extends dt {
    public ce(du duVar) {
        super(duVar);
    }

    /* access modifiers changed from: protected */
    public final boolean d() {
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.ed.a(android.os.Bundle, java.lang.String, java.lang.Object):void
     arg types: [android.os.Bundle, java.lang.String, long]
     candidates:
      com.google.android.gms.measurement.internal.ed.a(int, java.lang.Object, boolean):java.lang.Object
      com.google.android.gms.measurement.internal.ed.a(java.lang.String, int, boolean):java.lang.String
      com.google.android.gms.measurement.internal.ed.a(java.lang.String, int, java.lang.String):boolean
      com.google.android.gms.measurement.internal.ed.a(java.lang.String, java.lang.String[], java.lang.String):boolean
      com.google.android.gms.measurement.internal.ed.a(android.os.Bundle, java.lang.String, java.lang.Object):void */
    public final byte[] a(zzag zzag, String str) {
        Integer num;
        cw cwVar;
        Bundle bundle;
        ef efVar;
        cv cvVar;
        d dVar;
        long j;
        ec ecVar;
        zzag zzag2 = zzag;
        String str2 = str;
        c();
        this.r.s();
        l.a(zzag);
        l.a(str);
        if (!s().c(str2, h.at)) {
            q().j.a("Generating ScionPayload disabled. packageName", str2);
            return new byte[0];
        } else if ("_iap".equals(zzag2.f2595a) || "_iapx".equals(zzag2.f2595a)) {
            cv cvVar2 = new cv();
            h().e();
            try {
                ef b2 = h().b(str2);
                if (b2 == null) {
                    q().j.a("Log and bundle not available. package_name", str2);
                    return new byte[0];
                } else if (!b2.n()) {
                    q().j.a("Log and bundle disabled. package_name", str2);
                    byte[] bArr = new byte[0];
                    h().v();
                    return bArr;
                } else {
                    cw cwVar2 = new cw();
                    cvVar2.f2140a = new cw[]{cwVar2};
                    cwVar2.f2141a = 1;
                    cwVar2.i = Constants.PLATFORM;
                    cwVar2.o = b2.a();
                    cwVar2.n = b2.k();
                    cwVar2.p = b2.i();
                    long j2 = b2.j();
                    if (j2 == -2147483648L) {
                        num = null;
                    } else {
                        num = Integer.valueOf((int) j2);
                    }
                    cwVar2.C = num;
                    cwVar2.q = Long.valueOf(b2.l());
                    cwVar2.y = b2.c();
                    if (TextUtils.isEmpty(cwVar2.y)) {
                        cwVar2.I = b2.d();
                    }
                    cwVar2.v = Long.valueOf(b2.m());
                    if (this.r.r() && el.j() && s().c(cwVar2.o)) {
                        cwVar2.G = null;
                    }
                    Pair<String, Boolean> a2 = r().a(b2.a());
                    if (b2.u() && !TextUtils.isEmpty((CharSequence) a2.first)) {
                        Object obj = a2.first;
                        Long.toString(zzag2.d);
                        cwVar2.s = e();
                        cwVar2.t = (Boolean) a2.second;
                    }
                    k().w();
                    cwVar2.k = Build.MODEL;
                    k().w();
                    cwVar2.j = Build.VERSION.RELEASE;
                    cwVar2.m = Integer.valueOf((int) k().e_());
                    cwVar2.l = k().f();
                    try {
                        b2.b();
                        Long.toString(zzag2.d);
                        cwVar2.u = e();
                        cwVar2.B = b2.f();
                        String str3 = cwVar2.o;
                        List<ec> a3 = h().a(str3);
                        if (s().d(str2)) {
                            Iterator<ec> it = a3.iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    ecVar = null;
                                    break;
                                }
                                ecVar = it.next();
                                if ("_lte".equals(ecVar.c)) {
                                    break;
                                }
                            }
                            if (ecVar == null || ecVar.e == null) {
                                ec ecVar2 = new ec(str3, "auto", "_lte", l().a(), 0L);
                                a3.add(ecVar2);
                                h().a(ecVar2);
                            }
                        }
                        cz[] czVarArr = new cz[a3.size()];
                        for (int i = 0; i < a3.size(); i++) {
                            cz czVar = new cz();
                            czVarArr[i] = czVar;
                            czVar.f2148b = a3.get(i).c;
                            czVar.f2147a = Long.valueOf(a3.get(i).d);
                            f().a(czVar, a3.get(i).e);
                        }
                        cwVar2.c = czVarArr;
                        Bundle a4 = zzag2.f2596b.a();
                        a4.putLong("_c", 1);
                        q().j.a("Marking in-app purchase as real-time");
                        a4.putLong("_r", 1);
                        a4.putString("_o", zzag2.c);
                        if (o().f(cwVar2.o)) {
                            o().a(a4, "_dbg", (Object) 1L);
                            o().a(a4, "_r", (Object) 1L);
                        }
                        d a5 = h().a(str2, zzag2.f2595a);
                        if (a5 == null) {
                            bundle = a4;
                            cwVar = cwVar2;
                            cvVar = cvVar2;
                            efVar = b2;
                            dVar = new d(str, zzag2.f2595a, 0, 0, zzag2.d, 0, null, null, null, null);
                            j = 0;
                        } else {
                            bundle = a4;
                            cwVar = cwVar2;
                            cvVar = cvVar2;
                            efVar = b2;
                            j = a5.e;
                            dVar = a5.a(zzag2.d);
                        }
                        h().a(dVar);
                        c cVar = new c(this.r, zzag2.c, str, zzag2.f2595a, zzag2.d, j, bundle);
                        ct ctVar = new ct();
                        int i2 = 0;
                        cw cwVar3 = cwVar;
                        cwVar3.f2142b = new ct[]{ctVar};
                        ctVar.c = Long.valueOf(cVar.c);
                        ctVar.f2137b = cVar.f2453b;
                        ctVar.d = Long.valueOf(cVar.d);
                        ctVar.f2136a = new cu[cVar.e.f2594a.size()];
                        Iterator<String> it2 = cVar.e.iterator();
                        while (it2.hasNext()) {
                            String next = it2.next();
                            cu cuVar = new cu();
                            ctVar.f2136a[i2] = cuVar;
                            cuVar.f2138a = next;
                            f().a(cuVar, cVar.e.a(next));
                            i2++;
                        }
                        cwVar3.J = (ce.b) ((fq) ce.b.a().a((ce.a) ((fq) ce.a.a().a(dVar.c).a(zzag2.f2595a).d())).d());
                        cwVar3.A = g().a(efVar.a(), (ct[]) null, cwVar3.c);
                        cwVar3.e = ctVar.c;
                        cwVar3.f = ctVar.c;
                        long h = efVar.h();
                        cwVar3.h = h != 0 ? Long.valueOf(h) : null;
                        long g = efVar.g();
                        if (g != 0) {
                            h = g;
                        }
                        cwVar3.g = h != 0 ? Long.valueOf(h) : null;
                        efVar.r();
                        cwVar3.w = Integer.valueOf((int) efVar.o());
                        cwVar3.r = 14710L;
                        cwVar3.d = Long.valueOf(l().a());
                        cwVar3.z = Boolean.TRUE;
                        ef efVar2 = efVar;
                        efVar2.a(cwVar3.e.longValue());
                        efVar2.b(cwVar3.f.longValue());
                        h().a(efVar2);
                        h().u();
                        h().v();
                        try {
                            byte[] bArr2 = new byte[cvVar.e()];
                            iy a6 = iy.a(bArr2, bArr2.length);
                            cvVar.a(a6);
                            a6.a();
                            return f().b(bArr2);
                        } catch (IOException e) {
                            q().c.a("Data loss. Failed to bundle and serialize. appId", o.a(str), e);
                            return null;
                        }
                    } catch (SecurityException e2) {
                        q().j.a("app instance id encryption failed", e2.getMessage());
                        byte[] bArr3 = new byte[0];
                        h().v();
                        return bArr3;
                    }
                }
            } catch (SecurityException e3) {
                q().j.a("Resettable device id encryption failed", e3.getMessage());
                return new byte[0];
            } finally {
                h().v();
            }
        } else {
            q().j.a("Generating a payload for this event is not available. package_name, event_name", str2, zzag2.f2595a);
            return null;
        }
    }

    private static String e() {
        throw new SecurityException("This implementation should not be used.");
    }
}
