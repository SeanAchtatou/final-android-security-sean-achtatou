package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.a.d;
import com.google.android.gms.common.internal.j;

public final class bs<O extends a.d> {

    /* renamed from: a  reason: collision with root package name */
    public final a<O> f1433a;

    /* renamed from: b  reason: collision with root package name */
    private final boolean f1434b = true;
    private final int c;
    private final O d;

    public bs(a<O> aVar) {
        this.f1433a = aVar;
        this.d = null;
        this.c = System.identityHashCode(this);
    }

    public final int hashCode() {
        return this.c;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof bs)) {
            return false;
        }
        bs bsVar = (bs) obj;
        return !this.f1434b && !bsVar.f1434b && j.a(this.f1433a, bsVar.f1433a) && j.a(this.d, bsVar.d);
    }
}
