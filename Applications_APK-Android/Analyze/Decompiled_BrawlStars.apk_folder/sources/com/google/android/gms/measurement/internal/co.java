package com.google.android.gms.measurement.internal;

import android.os.RemoteException;

final class co implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zzk f2474a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ cl f2475b;

    co(cl clVar, zzk zzk) {
        this.f2475b = clVar;
        this.f2474a = zzk;
    }

    public final void run() {
        zzaj zzaj = this.f2475b.f2470b;
        if (zzaj == null) {
            this.f2475b.q().c.a("Discarding data. Failed to send app launch");
            return;
        }
        try {
            zzaj.a(this.f2474a);
            this.f2475b.a(zzaj, null, this.f2474a);
            this.f2475b.y();
        } catch (RemoteException e) {
            this.f2475b.q().c.a("Failed to send app launch to the service", e);
        }
    }
}
