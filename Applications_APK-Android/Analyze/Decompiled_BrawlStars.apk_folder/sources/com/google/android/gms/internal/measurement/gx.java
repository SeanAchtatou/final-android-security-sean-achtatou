package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.fq;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import sun.misc.Unsafe;

final class gx<T> implements hj<T> {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f2250a = new int[0];

    /* renamed from: b  reason: collision with root package name */
    private static final Unsafe f2251b = ih.c();
    private final int[] c;
    private final Object[] d;
    private final int e;
    private final int f;
    private final gt g;
    private final boolean h;
    private final boolean i;
    private final boolean j;
    private final boolean k;
    private final int[] l;
    private final int m;
    private final int n;
    private final ha o;
    private final ge p;
    private final ib<?, ?> q;
    private final fe<?> r;
    private final go s;

    private gx(int[] iArr, Object[] objArr, int i2, int i3, gt gtVar, boolean z, int[] iArr2, int i4, int i5, ha haVar, ge geVar, ib<?, ?> ibVar, fe<?> feVar, go goVar) {
        this.c = iArr;
        this.d = objArr;
        this.e = i2;
        this.f = i3;
        this.i = gtVar instanceof fq;
        this.j = z;
        this.h = feVar != null && feVar.a(gtVar);
        this.k = false;
        this.l = iArr2;
        this.m = i4;
        this.n = i5;
        this.o = haVar;
        this.p = geVar;
        this.q = ibVar;
        this.r = feVar;
        this.g = gtVar;
        this.s = goVar;
    }

    private static boolean e(int i2) {
        return (i2 & 536870912) != 0;
    }

    static <T> gx<T> a(gr grVar, ha haVar, ge geVar, ib<?, ?> ibVar, fe<?> feVar, go goVar) {
        int i2;
        int i3;
        char c2;
        int[] iArr;
        char c3;
        char c4;
        int i4;
        char c5;
        char c6;
        int i5;
        int i6;
        int i7;
        hh hhVar;
        char c7;
        String str;
        int i8;
        int i9;
        int i10;
        int i11;
        Field field;
        char charAt;
        int i12;
        char c8;
        Field field2;
        Field field3;
        int i13;
        char charAt2;
        int i14;
        char charAt3;
        int i15;
        char charAt4;
        int i16;
        char c9;
        int i17;
        int i18;
        int i19;
        int i20;
        int i21;
        char charAt5;
        int i22;
        char charAt6;
        int i23;
        char charAt7;
        int i24;
        char charAt8;
        char charAt9;
        char charAt10;
        char charAt11;
        char charAt12;
        char charAt13;
        char charAt14;
        gr grVar2 = grVar;
        if (grVar2 instanceof hh) {
            hh hhVar2 = (hh) grVar2;
            char c10 = 0;
            boolean z = hhVar2.a() == fq.e.i;
            String str2 = hhVar2.f2262b;
            int length = str2.length();
            char charAt15 = str2.charAt(0);
            if (charAt15 >= 55296) {
                char c11 = charAt15 & 8191;
                int i25 = 1;
                int i26 = 13;
                while (true) {
                    i2 = i25 + 1;
                    charAt14 = str2.charAt(i25);
                    if (charAt14 < 55296) {
                        break;
                    }
                    c11 |= (charAt14 & 8191) << i26;
                    i26 += 13;
                    i25 = i2;
                }
                charAt15 = (charAt14 << i26) | c11;
            } else {
                i2 = 1;
            }
            int i27 = i2 + 1;
            char charAt16 = str2.charAt(i2);
            if (charAt16 >= 55296) {
                char c12 = charAt16 & 8191;
                int i28 = 13;
                while (true) {
                    i3 = i27 + 1;
                    charAt13 = str2.charAt(i27);
                    if (charAt13 < 55296) {
                        break;
                    }
                    c12 |= (charAt13 & 8191) << i28;
                    i28 += 13;
                    i27 = i3;
                }
                charAt16 = c12 | (charAt13 << i28);
            } else {
                i3 = i27;
            }
            if (charAt16 == 0) {
                iArr = f2250a;
                c6 = 0;
                c5 = 0;
                i4 = 0;
                c4 = 0;
                c3 = 0;
                c2 = 0;
            } else {
                int i29 = i3 + 1;
                char charAt17 = str2.charAt(i3);
                if (charAt17 >= 55296) {
                    char c13 = charAt17 & 8191;
                    int i30 = 13;
                    while (true) {
                        i16 = i29 + 1;
                        charAt12 = str2.charAt(i29);
                        if (charAt12 < 55296) {
                            break;
                        }
                        c13 |= (charAt12 & 8191) << i30;
                        i30 += 13;
                        i29 = i16;
                    }
                    c9 = (charAt12 << i30) | c13;
                } else {
                    i16 = i29;
                    c9 = charAt17;
                }
                int i31 = i16 + 1;
                char charAt18 = str2.charAt(i16);
                if (charAt18 >= 55296) {
                    char c14 = charAt18 & 8191;
                    int i32 = 13;
                    while (true) {
                        i17 = i31 + 1;
                        charAt11 = str2.charAt(i31);
                        if (charAt11 < 55296) {
                            break;
                        }
                        c14 |= (charAt11 & 8191) << i32;
                        i32 += 13;
                        i31 = i17;
                    }
                    charAt18 = c14 | (charAt11 << i32);
                } else {
                    i17 = i31;
                }
                int i33 = i17 + 1;
                char charAt19 = str2.charAt(i17);
                if (charAt19 >= 55296) {
                    char c15 = charAt19 & 8191;
                    int i34 = 13;
                    while (true) {
                        i18 = i33 + 1;
                        charAt10 = str2.charAt(i33);
                        if (charAt10 < 55296) {
                            break;
                        }
                        c15 |= (charAt10 & 8191) << i34;
                        i34 += 13;
                        i33 = i18;
                    }
                    charAt19 = (charAt10 << i34) | c15;
                } else {
                    i18 = i33;
                }
                int i35 = i18 + 1;
                char charAt20 = str2.charAt(i18);
                if (charAt20 >= 55296) {
                    char c16 = charAt20 & 8191;
                    int i36 = 13;
                    while (true) {
                        i19 = i35 + 1;
                        charAt9 = str2.charAt(i35);
                        if (charAt9 < 55296) {
                            break;
                        }
                        c16 |= (charAt9 & 8191) << i36;
                        i36 += 13;
                        i35 = i19;
                    }
                    charAt20 = (charAt9 << i36) | c16;
                } else {
                    i19 = i35;
                }
                int i37 = i19 + 1;
                c3 = str2.charAt(i19);
                if (c3 >= 55296) {
                    char c17 = c3 & 8191;
                    int i38 = 13;
                    while (true) {
                        i24 = i37 + 1;
                        charAt8 = str2.charAt(i37);
                        if (charAt8 < 55296) {
                            break;
                        }
                        c17 |= (charAt8 & 8191) << i38;
                        i38 += 13;
                        i37 = i24;
                    }
                    c3 = (charAt8 << i38) | c17;
                    i37 = i24;
                }
                int i39 = i37 + 1;
                char charAt21 = str2.charAt(i37);
                if (charAt21 >= 55296) {
                    char c18 = charAt21 & 8191;
                    int i40 = 13;
                    while (true) {
                        i23 = i39 + 1;
                        charAt7 = str2.charAt(i39);
                        if (charAt7 < 55296) {
                            break;
                        }
                        c18 |= (charAt7 & 8191) << i40;
                        i40 += 13;
                        i39 = i23;
                    }
                    charAt21 = c18 | (charAt7 << i40);
                    i39 = i23;
                }
                int i41 = i39 + 1;
                char charAt22 = str2.charAt(i39);
                if (charAt22 >= 55296) {
                    int i42 = 13;
                    int i43 = i41;
                    char c19 = charAt22 & 8191;
                    int i44 = i43;
                    while (true) {
                        i22 = i44 + 1;
                        charAt6 = str2.charAt(i44);
                        if (charAt6 < 55296) {
                            break;
                        }
                        c19 |= (charAt6 & 8191) << i42;
                        i42 += 13;
                        i44 = i22;
                    }
                    charAt22 = c19 | (charAt6 << i42);
                    i20 = i22;
                } else {
                    i20 = i41;
                }
                int i45 = i20 + 1;
                char charAt23 = str2.charAt(i20);
                if (charAt23 >= 55296) {
                    int i46 = 13;
                    int i47 = i45;
                    char c20 = charAt23 & 8191;
                    int i48 = i47;
                    while (true) {
                        i21 = i48 + 1;
                        charAt5 = str2.charAt(i48);
                        if (charAt5 < 55296) {
                            break;
                        }
                        c20 |= (charAt5 & 8191) << i46;
                        i46 += 13;
                        i48 = i21;
                    }
                    charAt23 = c20 | (charAt5 << i46);
                    i45 = i21;
                }
                iArr = new int[(charAt23 + charAt21 + charAt22)];
                i4 = (c9 << 1) + charAt18;
                char c21 = charAt20;
                c4 = charAt23;
                c10 = charAt21;
                c5 = c21;
                int i49 = i45;
                c2 = c9;
                c6 = charAt19;
                i3 = i49;
            }
            Unsafe unsafe = f2251b;
            Object[] objArr = hhVar2.c;
            Class<?> cls = hhVar2.f2261a.getClass();
            int i50 = i4;
            int[] iArr2 = new int[(c3 * 3)];
            Object[] objArr2 = new Object[(c3 << 1)];
            int i51 = c10 + c4;
            int i52 = i51;
            char c22 = c4;
            int i53 = 0;
            int i54 = 0;
            while (i3 < length) {
                int i55 = i3 + 1;
                int charAt24 = str2.charAt(i3);
                int i56 = length;
                char c23 = 55296;
                if (charAt24 >= 55296) {
                    int i57 = 13;
                    int i58 = i55;
                    int i59 = charAt24 & 8191;
                    int i60 = i58;
                    while (true) {
                        i15 = i60 + 1;
                        charAt4 = str2.charAt(i60);
                        if (charAt4 < c23) {
                            break;
                        }
                        i59 |= (charAt4 & 8191) << i57;
                        i57 += 13;
                        i60 = i15;
                        c23 = 55296;
                    }
                    charAt24 = i59 | (charAt4 << i57);
                    i5 = i15;
                } else {
                    i5 = i55;
                }
                int i61 = i5 + 1;
                char charAt25 = str2.charAt(i5);
                int i62 = i51;
                char c24 = 55296;
                if (charAt25 >= 55296) {
                    int i63 = 13;
                    int i64 = i61;
                    char c25 = charAt25 & 8191;
                    int i65 = i64;
                    while (true) {
                        i14 = i65 + 1;
                        charAt3 = str2.charAt(i65);
                        if (charAt3 < c24) {
                            break;
                        }
                        c25 |= (charAt3 & 8191) << i63;
                        i63 += 13;
                        i65 = i14;
                        c24 = 55296;
                    }
                    charAt25 = c25 | (charAt3 << i63);
                    i6 = i14;
                } else {
                    i6 = i61;
                }
                char c26 = c4;
                char c27 = charAt25 & 255;
                boolean z2 = z;
                if ((charAt25 & 1024) != 0) {
                    iArr[i53] = i54;
                    i53++;
                }
                char c28 = c5;
                if (c27 >= '3') {
                    int i66 = i6 + 1;
                    char charAt26 = str2.charAt(i6);
                    char c29 = 55296;
                    if (charAt26 >= 55296) {
                        char c30 = charAt26 & 8191;
                        int i67 = 13;
                        while (true) {
                            i13 = i66 + 1;
                            charAt2 = str2.charAt(i66);
                            if (charAt2 < c29) {
                                break;
                            }
                            c30 |= (charAt2 & 8191) << i67;
                            i67 += 13;
                            i66 = i13;
                            c29 = 55296;
                        }
                        charAt26 = c30 | (charAt2 << i67);
                        i66 = i13;
                    }
                    int i68 = c27 - '3';
                    int i69 = i66;
                    if (i68 == 9 || i68 == 17) {
                        c8 = 1;
                        objArr2[((i54 / 3) << 1) + 1] = objArr[i50];
                        i50++;
                    } else {
                        if (i68 == 12 && (charAt15 & 1) == 1) {
                            objArr2[((i54 / 3) << 1) + 1] = objArr[i50];
                            i50++;
                        }
                        c8 = 1;
                    }
                    int i70 = charAt26 << c8;
                    Object obj = objArr[i70];
                    if (obj instanceof Field) {
                        field2 = (Field) obj;
                    } else {
                        field2 = a(cls, (String) obj);
                        objArr[i70] = field2;
                    }
                    char c31 = c6;
                    int objectFieldOffset = (int) unsafe.objectFieldOffset(field2);
                    int i71 = i70 + 1;
                    Object obj2 = objArr[i71];
                    int i72 = objectFieldOffset;
                    if (obj2 instanceof Field) {
                        field3 = (Field) obj2;
                    } else {
                        field3 = a(cls, (String) obj2);
                        objArr[i71] = field3;
                    }
                    int objectFieldOffset2 = (int) unsafe.objectFieldOffset(field3);
                    i9 = i50;
                    i8 = i72;
                    i7 = i69;
                    hhVar = hhVar2;
                    c7 = c31;
                    str = str2;
                    i11 = objectFieldOffset2;
                    i10 = 0;
                } else {
                    char c32 = c6;
                    i9 = i50 + 1;
                    Field a2 = a(cls, (String) objArr[i50]);
                    c7 = c32;
                    if (c27 == 9 || c27 == 17) {
                        hhVar = hhVar2;
                        objArr2[((i54 / 3) << 1) + 1] = a2.getType();
                    } else {
                        if (c27 == 27 || c27 == '1') {
                            hhVar = hhVar2;
                            i12 = i9 + 1;
                            objArr2[((i54 / 3) << 1) + 1] = objArr[i9];
                        } else if (c27 == 12 || c27 == 30 || c27 == ',') {
                            hhVar = hhVar2;
                            if ((charAt15 & 1) == 1) {
                                i12 = i9 + 1;
                                objArr2[((i54 / 3) << 1) + 1] = objArr[i9];
                            }
                        } else if (c27 == '2') {
                            int i73 = c22 + 1;
                            iArr[c22] = i54;
                            int i74 = (i54 / 3) << 1;
                            int i75 = i9 + 1;
                            objArr2[i74] = objArr[i9];
                            if ((charAt25 & 2048) != 0) {
                                i9 = i75 + 1;
                                objArr2[i74 + 1] = objArr[i75];
                                hhVar = hhVar2;
                                c22 = i73;
                            } else {
                                c22 = i73;
                                i9 = i75;
                                hhVar = hhVar2;
                            }
                        } else {
                            hhVar = hhVar2;
                        }
                        str = str2;
                        i9 = i12;
                        i8 = (int) unsafe.objectFieldOffset(a2);
                        if ((charAt15 & 1) == 1 || c27 > 17) {
                            i7 = i6;
                            i11 = 0;
                            i10 = 0;
                        } else {
                            int i76 = i6 + 1;
                            String str3 = str;
                            char charAt27 = str3.charAt(i6);
                            if (charAt27 >= 55296) {
                                char c33 = charAt27 & 8191;
                                int i77 = 13;
                                while (true) {
                                    i7 = i76 + 1;
                                    charAt = str3.charAt(i76);
                                    if (charAt < 55296) {
                                        break;
                                    }
                                    c33 |= (charAt & 8191) << i77;
                                    i77 += 13;
                                    i76 = i7;
                                }
                                charAt27 = c33 | (charAt << i77);
                            } else {
                                i7 = i76;
                            }
                            int i78 = (c2 << 1) + (charAt27 / ' ');
                            Object obj3 = objArr[i78];
                            if (obj3 instanceof Field) {
                                field = (Field) obj3;
                            } else {
                                field = a(cls, (String) obj3);
                                objArr[i78] = field;
                            }
                            str = str3;
                            i11 = (int) unsafe.objectFieldOffset(field);
                            i10 = charAt27 % ' ';
                        }
                        if (c27 >= 18 && c27 <= '1') {
                            iArr[i52] = i8;
                            i52++;
                        }
                    }
                    str = str2;
                    i8 = (int) unsafe.objectFieldOffset(a2);
                    if ((charAt15 & 1) == 1) {
                    }
                    i7 = i6;
                    i11 = 0;
                    i10 = 0;
                    iArr[i52] = i8;
                    i52++;
                }
                int i79 = i54 + 1;
                iArr2[i54] = charAt24;
                int i80 = i79 + 1;
                Class<?> cls2 = cls;
                iArr2[i79] = ((charAt25 & 256) != 0 ? 268435456 : 0) | ((charAt25 & 512) != 0 ? 536870912 : 0) | (c27 << 20) | i8;
                i54 = i80 + 1;
                iArr2[i80] = (i10 << 20) | i11;
                str2 = str;
                cls = cls2;
                c4 = c26;
                length = i56;
                i51 = i62;
                z = z2;
                hhVar2 = hhVar;
                i3 = i7;
                c5 = c28;
                char c34 = c7;
                i50 = i9;
                c6 = c34;
            }
            return new gx(iArr2, objArr2, c6, c5, hhVar2.f2261a, z, iArr, c4, i51, haVar, geVar, ibVar, feVar, goVar);
        }
        hw hwVar = (hw) grVar2;
        throw new NoSuchMethodError();
    }

    private static Field a(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException unused) {
            Field[] declaredFields = cls.getDeclaredFields();
            for (Field field : declaredFields) {
                if (str.equals(field.getName())) {
                    return field;
                }
            }
            String name = cls.getName();
            String arrays = Arrays.toString(declaredFields);
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 40 + String.valueOf(name).length() + String.valueOf(arrays).length());
            sb.append("Field ");
            sb.append(str);
            sb.append(" for ");
            sb.append(name);
            sb.append(" not found. Known fields are ");
            sb.append(arrays);
            throw new RuntimeException(sb.toString());
        }
    }

    public final T a() {
        return this.o.a(this.g);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x006a, code lost:
        if (com.google.android.gms.internal.measurement.hl.a(com.google.android.gms.internal.measurement.ih.f(r10, r6), com.google.android.gms.internal.measurement.ih.f(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x007e, code lost:
        if (com.google.android.gms.internal.measurement.ih.b(r10, r6) == com.google.android.gms.internal.measurement.ih.b(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0090, code lost:
        if (com.google.android.gms.internal.measurement.ih.a(r10, r6) == com.google.android.gms.internal.measurement.ih.a(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00a4, code lost:
        if (com.google.android.gms.internal.measurement.ih.b(r10, r6) == com.google.android.gms.internal.measurement.ih.b(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00b6, code lost:
        if (com.google.android.gms.internal.measurement.ih.a(r10, r6) == com.google.android.gms.internal.measurement.ih.a(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00c8, code lost:
        if (com.google.android.gms.internal.measurement.ih.a(r10, r6) == com.google.android.gms.internal.measurement.ih.a(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00da, code lost:
        if (com.google.android.gms.internal.measurement.ih.a(r10, r6) == com.google.android.gms.internal.measurement.ih.a(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00f0, code lost:
        if (com.google.android.gms.internal.measurement.hl.a(com.google.android.gms.internal.measurement.ih.f(r10, r6), com.google.android.gms.internal.measurement.ih.f(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0106, code lost:
        if (com.google.android.gms.internal.measurement.hl.a(com.google.android.gms.internal.measurement.ih.f(r10, r6), com.google.android.gms.internal.measurement.ih.f(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x011c, code lost:
        if (com.google.android.gms.internal.measurement.hl.a(com.google.android.gms.internal.measurement.ih.f(r10, r6), com.google.android.gms.internal.measurement.ih.f(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x012e, code lost:
        if (com.google.android.gms.internal.measurement.ih.c(r10, r6) == com.google.android.gms.internal.measurement.ih.c(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0140, code lost:
        if (com.google.android.gms.internal.measurement.ih.a(r10, r6) == com.google.android.gms.internal.measurement.ih.a(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0154, code lost:
        if (com.google.android.gms.internal.measurement.ih.b(r10, r6) == com.google.android.gms.internal.measurement.ih.b(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0165, code lost:
        if (com.google.android.gms.internal.measurement.ih.a(r10, r6) == com.google.android.gms.internal.measurement.ih.a(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0178, code lost:
        if (com.google.android.gms.internal.measurement.ih.b(r10, r6) == com.google.android.gms.internal.measurement.ih.b(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x018b, code lost:
        if (com.google.android.gms.internal.measurement.ih.b(r10, r6) == com.google.android.gms.internal.measurement.ih.b(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x01a4, code lost:
        if (java.lang.Float.floatToIntBits(com.google.android.gms.internal.measurement.ih.d(r10, r6)) == java.lang.Float.floatToIntBits(com.google.android.gms.internal.measurement.ih.d(r11, r6))) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01bf, code lost:
        if (java.lang.Double.doubleToLongBits(com.google.android.gms.internal.measurement.ih.e(r10, r6)) == java.lang.Double.doubleToLongBits(com.google.android.gms.internal.measurement.ih.e(r11, r6))) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0038, code lost:
        if (com.google.android.gms.internal.measurement.hl.a(com.google.android.gms.internal.measurement.ih.f(r10, r6), com.google.android.gms.internal.measurement.ih.f(r11, r6)) != false) goto L_0x01c2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(T r10, T r11) {
        /*
            r9 = this;
            int[] r0 = r9.c
            int r0 = r0.length
            r1 = 0
            r2 = 0
        L_0x0005:
            r3 = 1
            if (r2 >= r0) goto L_0x01c9
            int r4 = r9.c(r2)
            r5 = 1048575(0xfffff, float:1.469367E-39)
            r6 = r4 & r5
            long r6 = (long) r6
            r8 = 267386880(0xff00000, float:2.3665827E-29)
            r4 = r4 & r8
            int r4 = r4 >>> 20
            switch(r4) {
                case 0: goto L_0x01a7;
                case 1: goto L_0x018e;
                case 2: goto L_0x017b;
                case 3: goto L_0x0168;
                case 4: goto L_0x0157;
                case 5: goto L_0x0144;
                case 6: goto L_0x0132;
                case 7: goto L_0x0120;
                case 8: goto L_0x010a;
                case 9: goto L_0x00f4;
                case 10: goto L_0x00de;
                case 11: goto L_0x00cc;
                case 12: goto L_0x00ba;
                case 13: goto L_0x00a8;
                case 14: goto L_0x0094;
                case 15: goto L_0x0082;
                case 16: goto L_0x006e;
                case 17: goto L_0x0058;
                case 18: goto L_0x004a;
                case 19: goto L_0x004a;
                case 20: goto L_0x004a;
                case 21: goto L_0x004a;
                case 22: goto L_0x004a;
                case 23: goto L_0x004a;
                case 24: goto L_0x004a;
                case 25: goto L_0x004a;
                case 26: goto L_0x004a;
                case 27: goto L_0x004a;
                case 28: goto L_0x004a;
                case 29: goto L_0x004a;
                case 30: goto L_0x004a;
                case 31: goto L_0x004a;
                case 32: goto L_0x004a;
                case 33: goto L_0x004a;
                case 34: goto L_0x004a;
                case 35: goto L_0x004a;
                case 36: goto L_0x004a;
                case 37: goto L_0x004a;
                case 38: goto L_0x004a;
                case 39: goto L_0x004a;
                case 40: goto L_0x004a;
                case 41: goto L_0x004a;
                case 42: goto L_0x004a;
                case 43: goto L_0x004a;
                case 44: goto L_0x004a;
                case 45: goto L_0x004a;
                case 46: goto L_0x004a;
                case 47: goto L_0x004a;
                case 48: goto L_0x004a;
                case 49: goto L_0x004a;
                case 50: goto L_0x003c;
                case 51: goto L_0x001c;
                case 52: goto L_0x001c;
                case 53: goto L_0x001c;
                case 54: goto L_0x001c;
                case 55: goto L_0x001c;
                case 56: goto L_0x001c;
                case 57: goto L_0x001c;
                case 58: goto L_0x001c;
                case 59: goto L_0x001c;
                case 60: goto L_0x001c;
                case 61: goto L_0x001c;
                case 62: goto L_0x001c;
                case 63: goto L_0x001c;
                case 64: goto L_0x001c;
                case 65: goto L_0x001c;
                case 66: goto L_0x001c;
                case 67: goto L_0x001c;
                case 68: goto L_0x001c;
                default: goto L_0x001a;
            }
        L_0x001a:
            goto L_0x01c2
        L_0x001c:
            int r4 = r9.d(r2)
            r4 = r4 & r5
            long r4 = (long) r4
            int r8 = com.google.android.gms.internal.measurement.ih.a(r10, r4)
            int r4 = com.google.android.gms.internal.measurement.ih.a(r11, r4)
            if (r8 != r4) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.measurement.ih.f(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.measurement.ih.f(r11, r6)
            boolean r4 = com.google.android.gms.internal.measurement.hl.a(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x003c:
            java.lang.Object r3 = com.google.android.gms.internal.measurement.ih.f(r10, r6)
            java.lang.Object r4 = com.google.android.gms.internal.measurement.ih.f(r11, r6)
            boolean r3 = com.google.android.gms.internal.measurement.hl.a(r3, r4)
            goto L_0x01c2
        L_0x004a:
            java.lang.Object r3 = com.google.android.gms.internal.measurement.ih.f(r10, r6)
            java.lang.Object r4 = com.google.android.gms.internal.measurement.ih.f(r11, r6)
            boolean r3 = com.google.android.gms.internal.measurement.hl.a(r3, r4)
            goto L_0x01c2
        L_0x0058:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.measurement.ih.f(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.measurement.ih.f(r11, r6)
            boolean r4 = com.google.android.gms.internal.measurement.hl.a(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x006e:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.measurement.ih.b(r10, r6)
            long r6 = com.google.android.gms.internal.measurement.ih.b(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x0082:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.measurement.ih.a(r10, r6)
            int r5 = com.google.android.gms.internal.measurement.ih.a(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0094:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.measurement.ih.b(r10, r6)
            long r6 = com.google.android.gms.internal.measurement.ih.b(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x00a8:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.measurement.ih.a(r10, r6)
            int r5 = com.google.android.gms.internal.measurement.ih.a(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x00ba:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.measurement.ih.a(r10, r6)
            int r5 = com.google.android.gms.internal.measurement.ih.a(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x00cc:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.measurement.ih.a(r10, r6)
            int r5 = com.google.android.gms.internal.measurement.ih.a(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x00de:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.measurement.ih.f(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.measurement.ih.f(r11, r6)
            boolean r4 = com.google.android.gms.internal.measurement.hl.a(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x00f4:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.measurement.ih.f(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.measurement.ih.f(r11, r6)
            boolean r4 = com.google.android.gms.internal.measurement.hl.a(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x010a:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.measurement.ih.f(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.measurement.ih.f(r11, r6)
            boolean r4 = com.google.android.gms.internal.measurement.hl.a(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x0120:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            boolean r4 = com.google.android.gms.internal.measurement.ih.c(r10, r6)
            boolean r5 = com.google.android.gms.internal.measurement.ih.c(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0132:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.measurement.ih.a(r10, r6)
            int r5 = com.google.android.gms.internal.measurement.ih.a(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0144:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.measurement.ih.b(r10, r6)
            long r6 = com.google.android.gms.internal.measurement.ih.b(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x0157:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.measurement.ih.a(r10, r6)
            int r5 = com.google.android.gms.internal.measurement.ih.a(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0168:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.measurement.ih.b(r10, r6)
            long r6 = com.google.android.gms.internal.measurement.ih.b(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x017b:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.measurement.ih.b(r10, r6)
            long r6 = com.google.android.gms.internal.measurement.ih.b(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x018e:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            float r4 = com.google.android.gms.internal.measurement.ih.d(r10, r6)
            int r4 = java.lang.Float.floatToIntBits(r4)
            float r5 = com.google.android.gms.internal.measurement.ih.d(r11, r6)
            int r5 = java.lang.Float.floatToIntBits(r5)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x01a7:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            double r4 = com.google.android.gms.internal.measurement.ih.e(r10, r6)
            long r4 = java.lang.Double.doubleToLongBits(r4)
            double r6 = com.google.android.gms.internal.measurement.ih.e(r11, r6)
            long r6 = java.lang.Double.doubleToLongBits(r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
        L_0x01c1:
            r3 = 0
        L_0x01c2:
            if (r3 != 0) goto L_0x01c5
            return r1
        L_0x01c5:
            int r2 = r2 + 3
            goto L_0x0005
        L_0x01c9:
            com.google.android.gms.internal.measurement.ib<?, ?> r0 = r9.q
            java.lang.Object r0 = r0.b(r10)
            com.google.android.gms.internal.measurement.ib<?, ?> r2 = r9.q
            java.lang.Object r2 = r2.b(r11)
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x01dc
            return r1
        L_0x01dc:
            boolean r0 = r9.h
            if (r0 == 0) goto L_0x01f1
            com.google.android.gms.internal.measurement.fe<?> r0 = r9.r
            com.google.android.gms.internal.measurement.fh r10 = r0.a(r10)
            com.google.android.gms.internal.measurement.fe<?> r0 = r9.r
            com.google.android.gms.internal.measurement.fh r11 = r0.a(r11)
            boolean r10 = r10.equals(r11)
            return r10
        L_0x01f1:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.gx.a(java.lang.Object, java.lang.Object):boolean");
    }

    public final int a(T t) {
        int i2;
        int i3;
        int length = this.c.length;
        int i4 = 0;
        for (int i5 = 0; i5 < length; i5 += 3) {
            int c2 = c(i5);
            int i6 = this.c[i5];
            long j2 = (long) (1048575 & c2);
            int i7 = 37;
            switch ((c2 & 267386880) >>> 20) {
                case 0:
                    i3 = i4 * 53;
                    i2 = fs.a(Double.doubleToLongBits(ih.e(t, j2)));
                    i4 = i3 + i2;
                    break;
                case 1:
                    i3 = i4 * 53;
                    i2 = Float.floatToIntBits(ih.d(t, j2));
                    i4 = i3 + i2;
                    break;
                case 2:
                    i3 = i4 * 53;
                    i2 = fs.a(ih.b(t, j2));
                    i4 = i3 + i2;
                    break;
                case 3:
                    i3 = i4 * 53;
                    i2 = fs.a(ih.b(t, j2));
                    i4 = i3 + i2;
                    break;
                case 4:
                    i3 = i4 * 53;
                    i2 = ih.a(t, j2);
                    i4 = i3 + i2;
                    break;
                case 5:
                    i3 = i4 * 53;
                    i2 = fs.a(ih.b(t, j2));
                    i4 = i3 + i2;
                    break;
                case 6:
                    i3 = i4 * 53;
                    i2 = ih.a(t, j2);
                    i4 = i3 + i2;
                    break;
                case 7:
                    i3 = i4 * 53;
                    i2 = fs.a(ih.c(t, j2));
                    i4 = i3 + i2;
                    break;
                case 8:
                    i3 = i4 * 53;
                    i2 = ((String) ih.f(t, j2)).hashCode();
                    i4 = i3 + i2;
                    break;
                case 9:
                    Object f2 = ih.f(t, j2);
                    if (f2 != null) {
                        i7 = f2.hashCode();
                    }
                    i4 = (i4 * 53) + i7;
                    break;
                case 10:
                    i3 = i4 * 53;
                    i2 = ih.f(t, j2).hashCode();
                    i4 = i3 + i2;
                    break;
                case 11:
                    i3 = i4 * 53;
                    i2 = ih.a(t, j2);
                    i4 = i3 + i2;
                    break;
                case 12:
                    i3 = i4 * 53;
                    i2 = ih.a(t, j2);
                    i4 = i3 + i2;
                    break;
                case 13:
                    i3 = i4 * 53;
                    i2 = ih.a(t, j2);
                    i4 = i3 + i2;
                    break;
                case 14:
                    i3 = i4 * 53;
                    i2 = fs.a(ih.b(t, j2));
                    i4 = i3 + i2;
                    break;
                case 15:
                    i3 = i4 * 53;
                    i2 = ih.a(t, j2);
                    i4 = i3 + i2;
                    break;
                case 16:
                    i3 = i4 * 53;
                    i2 = fs.a(ih.b(t, j2));
                    i4 = i3 + i2;
                    break;
                case 17:
                    Object f3 = ih.f(t, j2);
                    if (f3 != null) {
                        i7 = f3.hashCode();
                    }
                    i4 = (i4 * 53) + i7;
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    i3 = i4 * 53;
                    i2 = ih.f(t, j2).hashCode();
                    i4 = i3 + i2;
                    break;
                case 50:
                    i3 = i4 * 53;
                    i2 = ih.f(t, j2).hashCode();
                    i4 = i3 + i2;
                    break;
                case 51:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = fs.a(Double.doubleToLongBits(b(t, j2)));
                        i4 = i3 + i2;
                        break;
                    }
                case 52:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = Float.floatToIntBits(c(t, j2));
                        i4 = i3 + i2;
                        break;
                    }
                case 53:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = fs.a(e(t, j2));
                        i4 = i3 + i2;
                        break;
                    }
                case 54:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = fs.a(e(t, j2));
                        i4 = i3 + i2;
                        break;
                    }
                case 55:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = d(t, j2);
                        i4 = i3 + i2;
                        break;
                    }
                case 56:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = fs.a(e(t, j2));
                        i4 = i3 + i2;
                        break;
                    }
                case 57:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = d(t, j2);
                        i4 = i3 + i2;
                        break;
                    }
                case 58:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = fs.a(f(t, j2));
                        i4 = i3 + i2;
                        break;
                    }
                case 59:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = ((String) ih.f(t, j2)).hashCode();
                        i4 = i3 + i2;
                        break;
                    }
                case 60:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = ih.f(t, j2).hashCode();
                        i4 = i3 + i2;
                        break;
                    }
                case 61:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = ih.f(t, j2).hashCode();
                        i4 = i3 + i2;
                        break;
                    }
                case 62:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = d(t, j2);
                        i4 = i3 + i2;
                        break;
                    }
                case 63:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = d(t, j2);
                        i4 = i3 + i2;
                        break;
                    }
                case 64:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = d(t, j2);
                        i4 = i3 + i2;
                        break;
                    }
                case 65:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = fs.a(e(t, j2));
                        i4 = i3 + i2;
                        break;
                    }
                case 66:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = d(t, j2);
                        i4 = i3 + i2;
                        break;
                    }
                case 67:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = fs.a(e(t, j2));
                        i4 = i3 + i2;
                        break;
                    }
                case 68:
                    if (!a(t, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = ih.f(t, j2).hashCode();
                        i4 = i3 + i2;
                        break;
                    }
            }
        }
        int hashCode = (i4 * 53) + this.q.b(t).hashCode();
        return this.h ? (hashCode * 53) + this.r.a((Object) t).hashCode() : hashCode;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.gx.a(java.lang.Object, int):boolean
     arg types: [T, int]
     candidates:
      com.google.android.gms.internal.measurement.gx.a(com.google.android.gms.internal.measurement.ib, java.lang.Object):int
      com.google.android.gms.internal.measurement.gx.a(java.lang.Class<?>, java.lang.String):java.lang.reflect.Field
      com.google.android.gms.internal.measurement.gx.a(java.lang.Object, long):java.util.List<E>
      com.google.android.gms.internal.measurement.gx.a(java.lang.Object, com.google.android.gms.internal.measurement.iv):void
      com.google.android.gms.internal.measurement.gx.a(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.hj.a(java.lang.Object, com.google.android.gms.internal.measurement.iv):void
      com.google.android.gms.internal.measurement.hj.a(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.gx.a(java.lang.Object, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, long):void
     arg types: [T, long, long]
     candidates:
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, float):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, int):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.measurement.ih.a(byte[], long, byte):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, long):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.gx.b(java.lang.Object, int):void
     arg types: [T, int]
     candidates:
      com.google.android.gms.internal.measurement.gx.b(java.lang.Object, long):double
      com.google.android.gms.internal.measurement.gx.b(java.lang.Object, com.google.android.gms.internal.measurement.iv):void
      com.google.android.gms.internal.measurement.gx.b(java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.hj.b(java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.gx.b(java.lang.Object, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, int):void
     arg types: [T, long, int]
     candidates:
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, float):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, long):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.measurement.ih.a(byte[], long, byte):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, boolean):void
     arg types: [T, long, boolean]
     candidates:
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, float):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, int):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, long):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.ih.a(byte[], long, byte):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, float):void
     arg types: [T, long, float]
     candidates:
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, int):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, long):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.measurement.ih.a(byte[], long, byte):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, float):void */
    public final void b(T t, T t2) {
        if (t2 != null) {
            for (int i2 = 0; i2 < this.c.length; i2 += 3) {
                int c2 = c(i2);
                long j2 = (long) (1048575 & c2);
                int i3 = this.c[i2];
                switch ((c2 & 267386880) >>> 20) {
                    case 0:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            ih.a(t, j2, ih.e(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 1:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            ih.a((Object) t, j2, ih.d(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 2:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            ih.a((Object) t, j2, ih.b(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 3:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            ih.a((Object) t, j2, ih.b(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 4:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            ih.a((Object) t, j2, ih.a(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 5:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            ih.a((Object) t, j2, ih.b(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 6:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            ih.a((Object) t, j2, ih.a(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 7:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            ih.a((Object) t, j2, ih.c(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 8:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            ih.a(t, j2, ih.f(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 9:
                        a(t, t2, i2);
                        break;
                    case 10:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            ih.a(t, j2, ih.f(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 11:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            ih.a((Object) t, j2, ih.a(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 12:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            ih.a((Object) t, j2, ih.a(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 13:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            ih.a((Object) t, j2, ih.a(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 14:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            ih.a((Object) t, j2, ih.b(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 15:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            ih.a((Object) t, j2, ih.a(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 16:
                        if (!a((Object) t2, i2)) {
                            break;
                        } else {
                            ih.a((Object) t, j2, ih.b(t2, j2));
                            b((Object) t, i2);
                            break;
                        }
                    case 17:
                        a(t, t2, i2);
                        break;
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 30:
                    case 31:
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 45:
                    case 46:
                    case 47:
                    case 48:
                    case 49:
                        this.p.a(t, t2, j2);
                        break;
                    case 50:
                        hl.a(this.s, t, t2, j2);
                        break;
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                    case 58:
                    case 59:
                        if (!a(t2, i3, i2)) {
                            break;
                        } else {
                            ih.a(t, j2, ih.f(t2, j2));
                            b(t, i3, i2);
                            break;
                        }
                    case 60:
                        b(t, t2, i2);
                        break;
                    case 61:
                    case 62:
                    case 63:
                    case 64:
                    case 65:
                    case 66:
                    case 67:
                        if (!a(t2, i3, i2)) {
                            break;
                        } else {
                            ih.a(t, j2, ih.f(t2, j2));
                            b(t, i3, i2);
                            break;
                        }
                    case 68:
                        b(t, t2, i2);
                        break;
                }
            }
            if (!this.j) {
                hl.a(this.q, t, t2);
                if (this.h) {
                    hl.a(this.r, t, t2);
                    return;
                }
                return;
            }
            return;
        }
        throw new NullPointerException();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.gx.a(java.lang.Object, int):boolean
     arg types: [T, int]
     candidates:
      com.google.android.gms.internal.measurement.gx.a(com.google.android.gms.internal.measurement.ib, java.lang.Object):int
      com.google.android.gms.internal.measurement.gx.a(java.lang.Class<?>, java.lang.String):java.lang.reflect.Field
      com.google.android.gms.internal.measurement.gx.a(java.lang.Object, long):java.util.List<E>
      com.google.android.gms.internal.measurement.gx.a(java.lang.Object, com.google.android.gms.internal.measurement.iv):void
      com.google.android.gms.internal.measurement.gx.a(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.hj.a(java.lang.Object, com.google.android.gms.internal.measurement.iv):void
      com.google.android.gms.internal.measurement.hj.a(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.gx.a(java.lang.Object, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.gx.b(java.lang.Object, int):void
     arg types: [T, int]
     candidates:
      com.google.android.gms.internal.measurement.gx.b(java.lang.Object, long):double
      com.google.android.gms.internal.measurement.gx.b(java.lang.Object, com.google.android.gms.internal.measurement.iv):void
      com.google.android.gms.internal.measurement.gx.b(java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.hj.b(java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.gx.b(java.lang.Object, int):void */
    private final void a(T t, T t2, int i2) {
        long c2 = (long) (c(i2) & 1048575);
        if (a((Object) t2, i2)) {
            Object f2 = ih.f(t, c2);
            Object f3 = ih.f(t2, c2);
            if (f2 != null && f3 != null) {
                ih.a(t, c2, fs.a(f2, f3));
                b((Object) t, i2);
            } else if (f3 != null) {
                ih.a(t, c2, f3);
                b((Object) t, i2);
            }
        }
    }

    private final void b(T t, T t2, int i2) {
        int c2 = c(i2);
        int i3 = this.c[i2];
        long j2 = (long) (c2 & 1048575);
        if (a(t2, i3, i2)) {
            Object f2 = ih.f(t, j2);
            Object f3 = ih.f(t2, j2);
            if (f2 != null && f3 != null) {
                ih.a(t, j2, fs.a(f2, f3));
                b(t, i3, i2);
            } else if (f3 != null) {
                ih.a(t, j2, f3);
                b(t, i3, i2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.gx.a(java.lang.Object, int):boolean
     arg types: [T, int]
     candidates:
      com.google.android.gms.internal.measurement.gx.a(com.google.android.gms.internal.measurement.ib, java.lang.Object):int
      com.google.android.gms.internal.measurement.gx.a(java.lang.Class<?>, java.lang.String):java.lang.reflect.Field
      com.google.android.gms.internal.measurement.gx.a(java.lang.Object, long):java.util.List<E>
      com.google.android.gms.internal.measurement.gx.a(java.lang.Object, com.google.android.gms.internal.measurement.iv):void
      com.google.android.gms.internal.measurement.gx.a(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.hj.a(java.lang.Object, com.google.android.gms.internal.measurement.iv):void
      com.google.android.gms.internal.measurement.hj.a(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.gx.a(java.lang.Object, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.gx.a(com.google.android.gms.internal.measurement.ib, java.lang.Object):int
     arg types: [com.google.android.gms.internal.measurement.ib<?, ?>, T]
     candidates:
      com.google.android.gms.internal.measurement.gx.a(java.lang.Class<?>, java.lang.String):java.lang.reflect.Field
      com.google.android.gms.internal.measurement.gx.a(java.lang.Object, long):java.util.List<E>
      com.google.android.gms.internal.measurement.gx.a(java.lang.Object, int):boolean
      com.google.android.gms.internal.measurement.gx.a(java.lang.Object, com.google.android.gms.internal.measurement.iv):void
      com.google.android.gms.internal.measurement.gx.a(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.hj.a(java.lang.Object, com.google.android.gms.internal.measurement.iv):void
      com.google.android.gms.internal.measurement.hj.a(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.gx.a(com.google.android.gms.internal.measurement.ib, java.lang.Object):int */
    public final int b(T t) {
        int i2;
        int i3;
        int j2;
        int i4;
        int i5;
        int i6;
        int j3;
        int i7;
        int i8;
        int i9;
        T t2 = t;
        int i10 = 267386880;
        int i11 = 1048575;
        if (this.j) {
            Unsafe unsafe = f2251b;
            int i12 = 0;
            for (int i13 = 0; i13 < this.c.length; i13 += 3) {
                int c2 = c(i13);
                int i14 = (c2 & 267386880) >>> 20;
                int i15 = this.c[i13];
                long j4 = (long) (c2 & 1048575);
                int i16 = (i14 < fk.DOUBLE_LIST_PACKED.c || i14 > fk.SINT64_LIST_PACKED.c) ? 0 : this.c[i13 + 2] & 1048575;
                switch (i14) {
                    case 0:
                        if (a((Object) t2, i13)) {
                            j3 = zztv.j(i15);
                            break;
                        } else {
                            continue;
                        }
                    case 1:
                        if (a((Object) t2, i13)) {
                            j3 = zztv.i(i15);
                            break;
                        } else {
                            continue;
                        }
                    case 2:
                        if (a((Object) t2, i13)) {
                            j3 = zztv.d(i15, ih.b(t2, j4));
                            break;
                        } else {
                            continue;
                        }
                    case 3:
                        if (a((Object) t2, i13)) {
                            j3 = zztv.e(i15, ih.b(t2, j4));
                            break;
                        } else {
                            continue;
                        }
                    case 4:
                        if (a((Object) t2, i13)) {
                            j3 = zztv.f(i15, ih.a(t2, j4));
                            break;
                        } else {
                            continue;
                        }
                    case 5:
                        if (a((Object) t2, i13)) {
                            j3 = zztv.g(i15);
                            break;
                        } else {
                            continue;
                        }
                    case 6:
                        if (a((Object) t2, i13)) {
                            j3 = zztv.e(i15);
                            break;
                        } else {
                            continue;
                        }
                    case 7:
                        if (a((Object) t2, i13)) {
                            j3 = zztv.k(i15);
                            break;
                        } else {
                            continue;
                        }
                    case 8:
                        if (a((Object) t2, i13)) {
                            Object f2 = ih.f(t2, j4);
                            if (!(f2 instanceof ej)) {
                                j3 = zztv.b(i15, (String) f2);
                                break;
                            } else {
                                j3 = zztv.c(i15, (ej) f2);
                                break;
                            }
                        } else {
                            continue;
                        }
                    case 9:
                        if (a((Object) t2, i13)) {
                            j3 = hl.a(i15, ih.f(t2, j4), a(i13));
                            break;
                        } else {
                            continue;
                        }
                    case 10:
                        if (a((Object) t2, i13)) {
                            j3 = zztv.c(i15, (ej) ih.f(t2, j4));
                            break;
                        } else {
                            continue;
                        }
                    case 11:
                        if (a((Object) t2, i13)) {
                            j3 = zztv.g(i15, ih.a(t2, j4));
                            break;
                        } else {
                            continue;
                        }
                    case 12:
                        if (a((Object) t2, i13)) {
                            j3 = zztv.i(i15, ih.a(t2, j4));
                            break;
                        } else {
                            continue;
                        }
                    case 13:
                        if (a((Object) t2, i13)) {
                            j3 = zztv.f(i15);
                            break;
                        } else {
                            continue;
                        }
                    case 14:
                        if (a((Object) t2, i13)) {
                            j3 = zztv.h(i15);
                            break;
                        } else {
                            continue;
                        }
                    case 15:
                        if (a((Object) t2, i13)) {
                            j3 = zztv.h(i15, ih.a(t2, j4));
                            break;
                        } else {
                            continue;
                        }
                    case 16:
                        if (a((Object) t2, i13)) {
                            j3 = zztv.f(i15, ih.b(t2, j4));
                            break;
                        } else {
                            continue;
                        }
                    case 17:
                        if (a((Object) t2, i13)) {
                            j3 = zztv.c(i15, (gt) ih.f(t2, j4), a(i13));
                            break;
                        } else {
                            continue;
                        }
                    case 18:
                        j3 = hl.i(i15, a(t2, j4));
                        break;
                    case 19:
                        j3 = hl.h(i15, a(t2, j4));
                        break;
                    case 20:
                        j3 = hl.a(i15, a(t2, j4));
                        break;
                    case 21:
                        j3 = hl.b(i15, a(t2, j4));
                        break;
                    case 22:
                        j3 = hl.e(i15, a(t2, j4));
                        break;
                    case 23:
                        j3 = hl.i(i15, a(t2, j4));
                        break;
                    case 24:
                        j3 = hl.h(i15, a(t2, j4));
                        break;
                    case 25:
                        j3 = hl.j(i15, a(t2, j4));
                        break;
                    case 26:
                        j3 = hl.k(i15, a(t2, j4));
                        break;
                    case 27:
                        j3 = hl.a(i15, (List<?>) a(t2, j4), a(i13));
                        break;
                    case 28:
                        j3 = hl.l(i15, a(t2, j4));
                        break;
                    case 29:
                        j3 = hl.f(i15, a(t2, j4));
                        break;
                    case 30:
                        j3 = hl.d(i15, a(t2, j4));
                        break;
                    case 31:
                        j3 = hl.h(i15, a(t2, j4));
                        break;
                    case 32:
                        j3 = hl.i(i15, a(t2, j4));
                        break;
                    case 33:
                        j3 = hl.g(i15, a(t2, j4));
                        break;
                    case 34:
                        j3 = hl.c(i15, a(t2, j4));
                        break;
                    case 35:
                        i8 = hl.i((List) unsafe.getObject(t2, j4));
                        if (i8 > 0) {
                            if (this.k) {
                                unsafe.putInt(t2, (long) i16, i8);
                            }
                            i9 = zztv.l(i15);
                            i7 = zztv.n(i8);
                            j3 = i9 + i7 + i8;
                            break;
                        } else {
                            continue;
                        }
                    case 36:
                        i8 = hl.h((List) unsafe.getObject(t2, j4));
                        if (i8 > 0) {
                            if (this.k) {
                                unsafe.putInt(t2, (long) i16, i8);
                            }
                            i9 = zztv.l(i15);
                            i7 = zztv.n(i8);
                            j3 = i9 + i7 + i8;
                            break;
                        } else {
                            continue;
                        }
                    case 37:
                        i8 = hl.a((List) unsafe.getObject(t2, j4));
                        if (i8 > 0) {
                            if (this.k) {
                                unsafe.putInt(t2, (long) i16, i8);
                            }
                            i9 = zztv.l(i15);
                            i7 = zztv.n(i8);
                            j3 = i9 + i7 + i8;
                            break;
                        } else {
                            continue;
                        }
                    case 38:
                        i8 = hl.b((List) unsafe.getObject(t2, j4));
                        if (i8 > 0) {
                            if (this.k) {
                                unsafe.putInt(t2, (long) i16, i8);
                            }
                            i9 = zztv.l(i15);
                            i7 = zztv.n(i8);
                            j3 = i9 + i7 + i8;
                            break;
                        } else {
                            continue;
                        }
                    case 39:
                        i8 = hl.e((List) unsafe.getObject(t2, j4));
                        if (i8 > 0) {
                            if (this.k) {
                                unsafe.putInt(t2, (long) i16, i8);
                            }
                            i9 = zztv.l(i15);
                            i7 = zztv.n(i8);
                            j3 = i9 + i7 + i8;
                            break;
                        } else {
                            continue;
                        }
                    case 40:
                        i8 = hl.i((List) unsafe.getObject(t2, j4));
                        if (i8 > 0) {
                            if (this.k) {
                                unsafe.putInt(t2, (long) i16, i8);
                            }
                            i9 = zztv.l(i15);
                            i7 = zztv.n(i8);
                            j3 = i9 + i7 + i8;
                            break;
                        } else {
                            continue;
                        }
                    case 41:
                        i8 = hl.h((List) unsafe.getObject(t2, j4));
                        if (i8 > 0) {
                            if (this.k) {
                                unsafe.putInt(t2, (long) i16, i8);
                            }
                            i9 = zztv.l(i15);
                            i7 = zztv.n(i8);
                            j3 = i9 + i7 + i8;
                            break;
                        } else {
                            continue;
                        }
                    case 42:
                        i8 = hl.j((List) unsafe.getObject(t2, j4));
                        if (i8 > 0) {
                            if (this.k) {
                                unsafe.putInt(t2, (long) i16, i8);
                            }
                            i9 = zztv.l(i15);
                            i7 = zztv.n(i8);
                            j3 = i9 + i7 + i8;
                            break;
                        } else {
                            continue;
                        }
                    case 43:
                        i8 = hl.f((List) unsafe.getObject(t2, j4));
                        if (i8 > 0) {
                            if (this.k) {
                                unsafe.putInt(t2, (long) i16, i8);
                            }
                            i9 = zztv.l(i15);
                            i7 = zztv.n(i8);
                            j3 = i9 + i7 + i8;
                            break;
                        } else {
                            continue;
                        }
                    case 44:
                        i8 = hl.d((List) unsafe.getObject(t2, j4));
                        if (i8 > 0) {
                            if (this.k) {
                                unsafe.putInt(t2, (long) i16, i8);
                            }
                            i9 = zztv.l(i15);
                            i7 = zztv.n(i8);
                            j3 = i9 + i7 + i8;
                            break;
                        } else {
                            continue;
                        }
                    case 45:
                        i8 = hl.h((List) unsafe.getObject(t2, j4));
                        if (i8 > 0) {
                            if (this.k) {
                                unsafe.putInt(t2, (long) i16, i8);
                            }
                            i9 = zztv.l(i15);
                            i7 = zztv.n(i8);
                            j3 = i9 + i7 + i8;
                            break;
                        } else {
                            continue;
                        }
                    case 46:
                        i8 = hl.i((List) unsafe.getObject(t2, j4));
                        if (i8 > 0) {
                            if (this.k) {
                                unsafe.putInt(t2, (long) i16, i8);
                            }
                            i9 = zztv.l(i15);
                            i7 = zztv.n(i8);
                            j3 = i9 + i7 + i8;
                            break;
                        } else {
                            continue;
                        }
                    case 47:
                        i8 = hl.g((List) unsafe.getObject(t2, j4));
                        if (i8 > 0) {
                            if (this.k) {
                                unsafe.putInt(t2, (long) i16, i8);
                            }
                            i9 = zztv.l(i15);
                            i7 = zztv.n(i8);
                            j3 = i9 + i7 + i8;
                            break;
                        } else {
                            continue;
                        }
                    case 48:
                        i8 = hl.c((List) unsafe.getObject(t2, j4));
                        if (i8 > 0) {
                            if (this.k) {
                                unsafe.putInt(t2, (long) i16, i8);
                            }
                            i9 = zztv.l(i15);
                            i7 = zztv.n(i8);
                            j3 = i9 + i7 + i8;
                            break;
                        } else {
                            continue;
                        }
                    case 49:
                        j3 = hl.b(i15, a(t2, j4), a(i13));
                        break;
                    case 50:
                        j3 = this.s.e(ih.f(t2, j4));
                        break;
                    case 51:
                        if (a(t2, i15, i13)) {
                            j3 = zztv.j(i15);
                            break;
                        } else {
                            continue;
                        }
                    case 52:
                        if (a(t2, i15, i13)) {
                            j3 = zztv.i(i15);
                            break;
                        } else {
                            continue;
                        }
                    case 53:
                        if (a(t2, i15, i13)) {
                            j3 = zztv.d(i15, e(t2, j4));
                            break;
                        } else {
                            continue;
                        }
                    case 54:
                        if (a(t2, i15, i13)) {
                            j3 = zztv.e(i15, e(t2, j4));
                            break;
                        } else {
                            continue;
                        }
                    case 55:
                        if (a(t2, i15, i13)) {
                            j3 = zztv.f(i15, d(t2, j4));
                            break;
                        } else {
                            continue;
                        }
                    case 56:
                        if (a(t2, i15, i13)) {
                            j3 = zztv.g(i15);
                            break;
                        } else {
                            continue;
                        }
                    case 57:
                        if (a(t2, i15, i13)) {
                            j3 = zztv.e(i15);
                            break;
                        } else {
                            continue;
                        }
                    case 58:
                        if (a(t2, i15, i13)) {
                            j3 = zztv.k(i15);
                            break;
                        } else {
                            continue;
                        }
                    case 59:
                        if (a(t2, i15, i13)) {
                            Object f3 = ih.f(t2, j4);
                            if (!(f3 instanceof ej)) {
                                j3 = zztv.b(i15, (String) f3);
                                break;
                            } else {
                                j3 = zztv.c(i15, (ej) f3);
                                break;
                            }
                        } else {
                            continue;
                        }
                    case 60:
                        if (a(t2, i15, i13)) {
                            j3 = hl.a(i15, ih.f(t2, j4), a(i13));
                            break;
                        } else {
                            continue;
                        }
                    case 61:
                        if (a(t2, i15, i13)) {
                            j3 = zztv.c(i15, (ej) ih.f(t2, j4));
                            break;
                        } else {
                            continue;
                        }
                    case 62:
                        if (a(t2, i15, i13)) {
                            j3 = zztv.g(i15, d(t2, j4));
                            break;
                        } else {
                            continue;
                        }
                    case 63:
                        if (a(t2, i15, i13)) {
                            j3 = zztv.i(i15, d(t2, j4));
                            break;
                        } else {
                            continue;
                        }
                    case 64:
                        if (a(t2, i15, i13)) {
                            j3 = zztv.f(i15);
                            break;
                        } else {
                            continue;
                        }
                    case 65:
                        if (a(t2, i15, i13)) {
                            j3 = zztv.h(i15);
                            break;
                        } else {
                            continue;
                        }
                    case 66:
                        if (a(t2, i15, i13)) {
                            j3 = zztv.h(i15, d(t2, j4));
                            break;
                        } else {
                            continue;
                        }
                    case 67:
                        if (a(t2, i15, i13)) {
                            j3 = zztv.f(i15, e(t2, j4));
                            break;
                        } else {
                            continue;
                        }
                    case 68:
                        if (a(t2, i15, i13)) {
                            j3 = zztv.c(i15, (gt) ih.f(t2, j4), a(i13));
                            break;
                        } else {
                            continue;
                        }
                    default:
                }
                i12 += j3;
            }
            return i12 + a((ib) this.q, (Object) t2);
        }
        Unsafe unsafe2 = f2251b;
        int i17 = 0;
        int i18 = 0;
        int i19 = -1;
        int i20 = 0;
        while (i17 < this.c.length) {
            int c3 = c(i17);
            int[] iArr = this.c;
            int i21 = iArr[i17];
            int i22 = (c3 & i10) >>> 20;
            if (i22 <= 17) {
                i3 = iArr[i17 + 2];
                int i23 = i3 & i11;
                i2 = 1 << (i3 >>> 20);
                if (i23 != i19) {
                    i20 = unsafe2.getInt(t2, (long) i23);
                } else {
                    i23 = i19;
                }
                i19 = i23;
            } else {
                i3 = (!this.k || i22 < fk.DOUBLE_LIST_PACKED.c || i22 > fk.SINT64_LIST_PACKED.c) ? 0 : this.c[i17 + 2] & i11;
                i2 = 0;
            }
            long j5 = (long) (c3 & i11);
            switch (i22) {
                case 0:
                    if ((i20 & i2) == 0) {
                        break;
                    } else {
                        j2 = zztv.j(i21);
                        i18 += j2;
                        break;
                    }
                case 1:
                    if ((i20 & i2) == 0) {
                        break;
                    } else {
                        j2 = zztv.i(i21);
                        i18 += j2;
                        break;
                    }
                case 2:
                    if ((i20 & i2) == 0) {
                        break;
                    } else {
                        j2 = zztv.d(i21, unsafe2.getLong(t2, j5));
                        i18 += j2;
                        break;
                    }
                case 3:
                    if ((i20 & i2) == 0) {
                        break;
                    } else {
                        j2 = zztv.e(i21, unsafe2.getLong(t2, j5));
                        i18 += j2;
                        break;
                    }
                case 4:
                    if ((i20 & i2) == 0) {
                        break;
                    } else {
                        j2 = zztv.f(i21, unsafe2.getInt(t2, j5));
                        i18 += j2;
                        break;
                    }
                case 5:
                    if ((i20 & i2) == 0) {
                        break;
                    } else {
                        j2 = zztv.g(i21);
                        i18 += j2;
                        break;
                    }
                case 6:
                    if ((i20 & i2) == 0) {
                        break;
                    } else {
                        j2 = zztv.e(i21);
                        i18 += j2;
                        break;
                    }
                case 7:
                    if ((i20 & i2) == 0) {
                        break;
                    } else {
                        j2 = zztv.k(i21);
                        i18 += j2;
                        break;
                    }
                case 8:
                    if ((i20 & i2) == 0) {
                        break;
                    } else {
                        Object object = unsafe2.getObject(t2, j5);
                        j2 = object instanceof ej ? zztv.c(i21, (ej) object) : zztv.b(i21, (String) object);
                        i18 += j2;
                        break;
                    }
                case 9:
                    if ((i20 & i2) == 0) {
                        break;
                    } else {
                        j2 = hl.a(i21, unsafe2.getObject(t2, j5), a(i17));
                        i18 += j2;
                        break;
                    }
                case 10:
                    if ((i20 & i2) == 0) {
                        break;
                    } else {
                        j2 = zztv.c(i21, (ej) unsafe2.getObject(t2, j5));
                        i18 += j2;
                        break;
                    }
                case 11:
                    if ((i20 & i2) == 0) {
                        break;
                    } else {
                        j2 = zztv.g(i21, unsafe2.getInt(t2, j5));
                        i18 += j2;
                        break;
                    }
                case 12:
                    if ((i20 & i2) == 0) {
                        break;
                    } else {
                        j2 = zztv.i(i21, unsafe2.getInt(t2, j5));
                        i18 += j2;
                        break;
                    }
                case 13:
                    if ((i20 & i2) == 0) {
                        break;
                    } else {
                        j2 = zztv.f(i21);
                        i18 += j2;
                        break;
                    }
                case 14:
                    if ((i20 & i2) == 0) {
                        break;
                    } else {
                        j2 = zztv.h(i21);
                        i18 += j2;
                        break;
                    }
                case 15:
                    if ((i20 & i2) == 0) {
                        break;
                    } else {
                        j2 = zztv.h(i21, unsafe2.getInt(t2, j5));
                        i18 += j2;
                        break;
                    }
                case 16:
                    if ((i20 & i2) == 0) {
                        break;
                    } else {
                        j2 = zztv.f(i21, unsafe2.getLong(t2, j5));
                        i18 += j2;
                        break;
                    }
                case 17:
                    if ((i20 & i2) == 0) {
                        break;
                    } else {
                        j2 = zztv.c(i21, (gt) unsafe2.getObject(t2, j5), a(i17));
                        i18 += j2;
                        break;
                    }
                case 18:
                    j2 = hl.i(i21, (List) unsafe2.getObject(t2, j5));
                    i18 += j2;
                    break;
                case 19:
                    j2 = hl.h(i21, (List) unsafe2.getObject(t2, j5));
                    i18 += j2;
                    break;
                case 20:
                    j2 = hl.a(i21, (List) unsafe2.getObject(t2, j5));
                    i18 += j2;
                    break;
                case 21:
                    j2 = hl.b(i21, (List) unsafe2.getObject(t2, j5));
                    i18 += j2;
                    break;
                case 22:
                    j2 = hl.e(i21, (List) unsafe2.getObject(t2, j5));
                    i18 += j2;
                    break;
                case 23:
                    j2 = hl.i(i21, (List) unsafe2.getObject(t2, j5));
                    i18 += j2;
                    break;
                case 24:
                    j2 = hl.h(i21, (List) unsafe2.getObject(t2, j5));
                    i18 += j2;
                    break;
                case 25:
                    j2 = hl.j(i21, (List) unsafe2.getObject(t2, j5));
                    i18 += j2;
                    break;
                case 26:
                    j2 = hl.k(i21, (List) unsafe2.getObject(t2, j5));
                    i18 += j2;
                    break;
                case 27:
                    j2 = hl.a(i21, (List<?>) ((List) unsafe2.getObject(t2, j5)), a(i17));
                    i18 += j2;
                    break;
                case 28:
                    j2 = hl.l(i21, (List) unsafe2.getObject(t2, j5));
                    i18 += j2;
                    break;
                case 29:
                    j2 = hl.f(i21, (List) unsafe2.getObject(t2, j5));
                    i18 += j2;
                    break;
                case 30:
                    j2 = hl.d(i21, (List) unsafe2.getObject(t2, j5));
                    i18 += j2;
                    break;
                case 31:
                    j2 = hl.h(i21, (List) unsafe2.getObject(t2, j5));
                    i18 += j2;
                    break;
                case 32:
                    j2 = hl.i(i21, (List) unsafe2.getObject(t2, j5));
                    i18 += j2;
                    break;
                case 33:
                    j2 = hl.g(i21, (List) unsafe2.getObject(t2, j5));
                    i18 += j2;
                    break;
                case 34:
                    j2 = hl.c(i21, (List) unsafe2.getObject(t2, j5));
                    i18 += j2;
                    break;
                case 35:
                    i6 = hl.i((List) unsafe2.getObject(t2, j5));
                    if (i6 > 0) {
                        if (this.k) {
                            unsafe2.putInt(t2, (long) i3, i6);
                        }
                        i5 = zztv.l(i21);
                        i4 = zztv.n(i6);
                        i18 += i5 + i4 + i6;
                        break;
                    } else {
                        break;
                    }
                case 36:
                    i6 = hl.h((List) unsafe2.getObject(t2, j5));
                    if (i6 > 0) {
                        if (this.k) {
                            unsafe2.putInt(t2, (long) i3, i6);
                        }
                        i5 = zztv.l(i21);
                        i4 = zztv.n(i6);
                        i18 += i5 + i4 + i6;
                        break;
                    } else {
                        break;
                    }
                case 37:
                    i6 = hl.a((List) unsafe2.getObject(t2, j5));
                    if (i6 > 0) {
                        if (this.k) {
                            unsafe2.putInt(t2, (long) i3, i6);
                        }
                        i5 = zztv.l(i21);
                        i4 = zztv.n(i6);
                        i18 += i5 + i4 + i6;
                        break;
                    } else {
                        break;
                    }
                case 38:
                    i6 = hl.b((List) unsafe2.getObject(t2, j5));
                    if (i6 > 0) {
                        if (this.k) {
                            unsafe2.putInt(t2, (long) i3, i6);
                        }
                        i5 = zztv.l(i21);
                        i4 = zztv.n(i6);
                        i18 += i5 + i4 + i6;
                        break;
                    } else {
                        break;
                    }
                case 39:
                    i6 = hl.e((List) unsafe2.getObject(t2, j5));
                    if (i6 > 0) {
                        if (this.k) {
                            unsafe2.putInt(t2, (long) i3, i6);
                        }
                        i5 = zztv.l(i21);
                        i4 = zztv.n(i6);
                        i18 += i5 + i4 + i6;
                        break;
                    } else {
                        break;
                    }
                case 40:
                    i6 = hl.i((List) unsafe2.getObject(t2, j5));
                    if (i6 > 0) {
                        if (this.k) {
                            unsafe2.putInt(t2, (long) i3, i6);
                        }
                        i5 = zztv.l(i21);
                        i4 = zztv.n(i6);
                        i18 += i5 + i4 + i6;
                        break;
                    } else {
                        break;
                    }
                case 41:
                    i6 = hl.h((List) unsafe2.getObject(t2, j5));
                    if (i6 > 0) {
                        if (this.k) {
                            unsafe2.putInt(t2, (long) i3, i6);
                        }
                        i5 = zztv.l(i21);
                        i4 = zztv.n(i6);
                        i18 += i5 + i4 + i6;
                        break;
                    } else {
                        break;
                    }
                case 42:
                    i6 = hl.j((List) unsafe2.getObject(t2, j5));
                    if (i6 > 0) {
                        if (this.k) {
                            unsafe2.putInt(t2, (long) i3, i6);
                        }
                        i5 = zztv.l(i21);
                        i4 = zztv.n(i6);
                        i18 += i5 + i4 + i6;
                        break;
                    } else {
                        break;
                    }
                case 43:
                    i6 = hl.f((List) unsafe2.getObject(t2, j5));
                    if (i6 > 0) {
                        if (this.k) {
                            unsafe2.putInt(t2, (long) i3, i6);
                        }
                        i5 = zztv.l(i21);
                        i4 = zztv.n(i6);
                        i18 += i5 + i4 + i6;
                        break;
                    } else {
                        break;
                    }
                case 44:
                    i6 = hl.d((List) unsafe2.getObject(t2, j5));
                    if (i6 > 0) {
                        if (this.k) {
                            unsafe2.putInt(t2, (long) i3, i6);
                        }
                        i5 = zztv.l(i21);
                        i4 = zztv.n(i6);
                        i18 += i5 + i4 + i6;
                        break;
                    } else {
                        break;
                    }
                case 45:
                    i6 = hl.h((List) unsafe2.getObject(t2, j5));
                    if (i6 > 0) {
                        if (this.k) {
                            unsafe2.putInt(t2, (long) i3, i6);
                        }
                        i5 = zztv.l(i21);
                        i4 = zztv.n(i6);
                        i18 += i5 + i4 + i6;
                        break;
                    } else {
                        break;
                    }
                case 46:
                    i6 = hl.i((List) unsafe2.getObject(t2, j5));
                    if (i6 > 0) {
                        if (this.k) {
                            unsafe2.putInt(t2, (long) i3, i6);
                        }
                        i5 = zztv.l(i21);
                        i4 = zztv.n(i6);
                        i18 += i5 + i4 + i6;
                        break;
                    } else {
                        break;
                    }
                case 47:
                    i6 = hl.g((List) unsafe2.getObject(t2, j5));
                    if (i6 > 0) {
                        if (this.k) {
                            unsafe2.putInt(t2, (long) i3, i6);
                        }
                        i5 = zztv.l(i21);
                        i4 = zztv.n(i6);
                        i18 += i5 + i4 + i6;
                        break;
                    } else {
                        break;
                    }
                case 48:
                    i6 = hl.c((List) unsafe2.getObject(t2, j5));
                    if (i6 > 0) {
                        if (this.k) {
                            unsafe2.putInt(t2, (long) i3, i6);
                        }
                        i5 = zztv.l(i21);
                        i4 = zztv.n(i6);
                        i18 += i5 + i4 + i6;
                        break;
                    } else {
                        break;
                    }
                case 49:
                    j2 = hl.b(i21, (List) unsafe2.getObject(t2, j5), a(i17));
                    i18 += j2;
                    break;
                case 50:
                    j2 = this.s.e(unsafe2.getObject(t2, j5));
                    i18 += j2;
                    break;
                case 51:
                    if (!a(t2, i21, i17)) {
                        break;
                    } else {
                        j2 = zztv.j(i21);
                        i18 += j2;
                        break;
                    }
                case 52:
                    if (!a(t2, i21, i17)) {
                        break;
                    } else {
                        j2 = zztv.i(i21);
                        i18 += j2;
                        break;
                    }
                case 53:
                    if (!a(t2, i21, i17)) {
                        break;
                    } else {
                        j2 = zztv.d(i21, e(t2, j5));
                        i18 += j2;
                        break;
                    }
                case 54:
                    if (!a(t2, i21, i17)) {
                        break;
                    } else {
                        j2 = zztv.e(i21, e(t2, j5));
                        i18 += j2;
                        break;
                    }
                case 55:
                    if (!a(t2, i21, i17)) {
                        break;
                    } else {
                        j2 = zztv.f(i21, d(t2, j5));
                        i18 += j2;
                        break;
                    }
                case 56:
                    if (!a(t2, i21, i17)) {
                        break;
                    } else {
                        j2 = zztv.g(i21);
                        i18 += j2;
                        break;
                    }
                case 57:
                    if (!a(t2, i21, i17)) {
                        break;
                    } else {
                        j2 = zztv.e(i21);
                        i18 += j2;
                        break;
                    }
                case 58:
                    if (!a(t2, i21, i17)) {
                        break;
                    } else {
                        j2 = zztv.k(i21);
                        i18 += j2;
                        break;
                    }
                case 59:
                    if (!a(t2, i21, i17)) {
                        break;
                    } else {
                        Object object2 = unsafe2.getObject(t2, j5);
                        if (object2 instanceof ej) {
                            j2 = zztv.c(i21, (ej) object2);
                        } else {
                            j2 = zztv.b(i21, (String) object2);
                        }
                        i18 += j2;
                        break;
                    }
                case 60:
                    if (!a(t2, i21, i17)) {
                        break;
                    } else {
                        j2 = hl.a(i21, unsafe2.getObject(t2, j5), a(i17));
                        i18 += j2;
                        break;
                    }
                case 61:
                    if (!a(t2, i21, i17)) {
                        break;
                    } else {
                        j2 = zztv.c(i21, (ej) unsafe2.getObject(t2, j5));
                        i18 += j2;
                        break;
                    }
                case 62:
                    if (!a(t2, i21, i17)) {
                        break;
                    } else {
                        j2 = zztv.g(i21, d(t2, j5));
                        i18 += j2;
                        break;
                    }
                case 63:
                    if (!a(t2, i21, i17)) {
                        break;
                    } else {
                        j2 = zztv.i(i21, d(t2, j5));
                        i18 += j2;
                        break;
                    }
                case 64:
                    if (!a(t2, i21, i17)) {
                        break;
                    } else {
                        j2 = zztv.f(i21);
                        i18 += j2;
                        break;
                    }
                case 65:
                    if (!a(t2, i21, i17)) {
                        break;
                    } else {
                        j2 = zztv.h(i21);
                        i18 += j2;
                        break;
                    }
                case 66:
                    if (!a(t2, i21, i17)) {
                        break;
                    } else {
                        j2 = zztv.h(i21, d(t2, j5));
                        i18 += j2;
                        break;
                    }
                case 67:
                    if (!a(t2, i21, i17)) {
                        break;
                    } else {
                        j2 = zztv.f(i21, e(t2, j5));
                        i18 += j2;
                        break;
                    }
                case 68:
                    if (!a(t2, i21, i17)) {
                        break;
                    } else {
                        j2 = zztv.c(i21, (gt) unsafe2.getObject(t2, j5), a(i17));
                        i18 += j2;
                        break;
                    }
            }
            i17 += 3;
            i10 = 267386880;
            i11 = 1048575;
        }
        int a2 = i18 + a((ib) this.q, (Object) t2);
        if (!this.h) {
            return a2;
        }
        fh<?> a3 = this.r.a((Object) t2);
        int i24 = 0;
        for (int i25 = 0; i25 < a3.f2206a.b(); i25++) {
            Map.Entry<FieldDescriptorType, Object> b2 = a3.f2206a.b(i25);
            i24 += fh.a((fj) b2.getKey(), b2.getValue());
        }
        for (Map.Entry next : a3.f2206a.c()) {
            i24 += fh.a((fj) next.getKey(), next.getValue());
        }
        return a2 + i24;
    }

    private static <UT, UB> int a(ib<UT, UB> ibVar, T t) {
        return ibVar.f(ibVar.b(t));
    }

    private static <E> List<E> a(Object obj, long j2) {
        return (List) ih.f(obj, j2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.hl.b(int, java.util.List<java.lang.Float>, com.google.android.gms.internal.measurement.iv, boolean):void
     arg types: [int, java.util.List, com.google.android.gms.internal.measurement.iv, int]
     candidates:
      com.google.android.gms.internal.measurement.hl.b(int, java.util.List<?>, com.google.android.gms.internal.measurement.iv, com.google.android.gms.internal.measurement.hj):void
      com.google.android.gms.internal.measurement.hl.b(int, java.util.List<java.lang.Float>, com.google.android.gms.internal.measurement.iv, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.hl.a(int, java.util.List<java.lang.Double>, com.google.android.gms.internal.measurement.iv, boolean):void
     arg types: [int, java.util.List, com.google.android.gms.internal.measurement.iv, int]
     candidates:
      com.google.android.gms.internal.measurement.hl.a(int, int, java.lang.Object, com.google.android.gms.internal.measurement.ib):UB
      com.google.android.gms.internal.measurement.hl.a(int, java.util.List<?>, com.google.android.gms.internal.measurement.iv, com.google.android.gms.internal.measurement.hj):void
      com.google.android.gms.internal.measurement.hl.a(com.google.android.gms.internal.measurement.go, java.lang.Object, java.lang.Object, long):void
      com.google.android.gms.internal.measurement.hl.a(int, java.util.List<java.lang.Double>, com.google.android.gms.internal.measurement.iv, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.gx.a(java.lang.Object, int):boolean
     arg types: [T, int]
     candidates:
      com.google.android.gms.internal.measurement.gx.a(com.google.android.gms.internal.measurement.ib, java.lang.Object):int
      com.google.android.gms.internal.measurement.gx.a(java.lang.Class<?>, java.lang.String):java.lang.reflect.Field
      com.google.android.gms.internal.measurement.gx.a(java.lang.Object, long):java.util.List<E>
      com.google.android.gms.internal.measurement.gx.a(java.lang.Object, com.google.android.gms.internal.measurement.iv):void
      com.google.android.gms.internal.measurement.gx.a(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.hj.a(java.lang.Object, com.google.android.gms.internal.measurement.iv):void
      com.google.android.gms.internal.measurement.hj.a(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.gx.a(java.lang.Object, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.gx.b(java.lang.Object, com.google.android.gms.internal.measurement.iv):void
     arg types: [T, com.google.android.gms.internal.measurement.iv]
     candidates:
      com.google.android.gms.internal.measurement.gx.b(java.lang.Object, long):double
      com.google.android.gms.internal.measurement.gx.b(java.lang.Object, int):void
      com.google.android.gms.internal.measurement.gx.b(java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.hj.b(java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.gx.b(java.lang.Object, com.google.android.gms.internal.measurement.iv):void */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:167:0x052e  */
    /* JADX WARNING: Removed duplicated region for block: B:182:0x056e  */
    /* JADX WARNING: Removed duplicated region for block: B:335:0x0a46  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(T r14, com.google.android.gms.internal.measurement.iv r15) throws java.io.IOException {
        /*
            r13 = this;
            int r0 = r15.a()
            int r1 = com.google.android.gms.internal.measurement.fq.e.k
            r2 = 267386880(0xff00000, float:2.3665827E-29)
            r3 = 0
            r4 = 1
            r5 = 0
            r6 = 1048575(0xfffff, float:1.469367E-39)
            if (r0 != r1) goto L_0x0544
            com.google.android.gms.internal.measurement.ib<?, ?> r0 = r13.q
            a(r0, r14, r15)
            boolean r0 = r13.h
            if (r0 == 0) goto L_0x004d
            com.google.android.gms.internal.measurement.fe<?> r0 = r13.r
            com.google.android.gms.internal.measurement.fh r0 = r0.a(r14)
            com.google.android.gms.internal.measurement.hm<FieldDescriptorType, java.lang.Object> r1 = r0.f2206a
            boolean r1 = r1.isEmpty()
            if (r1 != 0) goto L_0x004d
            boolean r1 = r0.c
            if (r1 == 0) goto L_0x003c
            com.google.android.gms.internal.measurement.ga r1 = new com.google.android.gms.internal.measurement.ga
            com.google.android.gms.internal.measurement.hm<FieldDescriptorType, java.lang.Object> r0 = r0.f2206a
            java.util.Set r0 = r0.d()
            java.util.Iterator r0 = r0.iterator()
            r1.<init>(r0)
            r0 = r1
            goto L_0x0046
        L_0x003c:
            com.google.android.gms.internal.measurement.hm<FieldDescriptorType, java.lang.Object> r0 = r0.f2206a
            java.util.Set r0 = r0.d()
            java.util.Iterator r0 = r0.iterator()
        L_0x0046:
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            goto L_0x004f
        L_0x004d:
            r0 = r3
            r1 = r0
        L_0x004f:
            int[] r7 = r13.c
            int r7 = r7.length
            int r7 = r7 + -3
        L_0x0054:
            if (r7 < 0) goto L_0x052c
            int r8 = r13.c(r7)
            int[] r9 = r13.c
            r9 = r9[r7]
        L_0x005e:
            if (r1 == 0) goto L_0x007c
            com.google.android.gms.internal.measurement.fe<?> r10 = r13.r
            int r10 = r10.a(r1)
            if (r10 <= r9) goto L_0x007c
            com.google.android.gms.internal.measurement.fe<?> r10 = r13.r
            r10.b(r1)
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x007a
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            goto L_0x005e
        L_0x007a:
            r1 = r3
            goto L_0x005e
        L_0x007c:
            r10 = r8 & r2
            int r10 = r10 >>> 20
            switch(r10) {
                case 0: goto L_0x0519;
                case 1: goto L_0x0509;
                case 2: goto L_0x04f9;
                case 3: goto L_0x04e9;
                case 4: goto L_0x04d9;
                case 5: goto L_0x04c9;
                case 6: goto L_0x04b9;
                case 7: goto L_0x04a8;
                case 8: goto L_0x0497;
                case 9: goto L_0x0482;
                case 10: goto L_0x046f;
                case 11: goto L_0x045e;
                case 12: goto L_0x044d;
                case 13: goto L_0x043c;
                case 14: goto L_0x042b;
                case 15: goto L_0x041a;
                case 16: goto L_0x0409;
                case 17: goto L_0x03f4;
                case 18: goto L_0x03e3;
                case 19: goto L_0x03d2;
                case 20: goto L_0x03c1;
                case 21: goto L_0x03b0;
                case 22: goto L_0x039f;
                case 23: goto L_0x038e;
                case 24: goto L_0x037d;
                case 25: goto L_0x036c;
                case 26: goto L_0x035b;
                case 27: goto L_0x0346;
                case 28: goto L_0x0335;
                case 29: goto L_0x0324;
                case 30: goto L_0x0313;
                case 31: goto L_0x0302;
                case 32: goto L_0x02f1;
                case 33: goto L_0x02e0;
                case 34: goto L_0x02cf;
                case 35: goto L_0x02be;
                case 36: goto L_0x02ad;
                case 37: goto L_0x029c;
                case 38: goto L_0x028b;
                case 39: goto L_0x027a;
                case 40: goto L_0x0269;
                case 41: goto L_0x0258;
                case 42: goto L_0x0247;
                case 43: goto L_0x0236;
                case 44: goto L_0x0225;
                case 45: goto L_0x0214;
                case 46: goto L_0x0203;
                case 47: goto L_0x01f2;
                case 48: goto L_0x01e1;
                case 49: goto L_0x01cc;
                case 50: goto L_0x01c1;
                case 51: goto L_0x01b0;
                case 52: goto L_0x019f;
                case 53: goto L_0x018e;
                case 54: goto L_0x017d;
                case 55: goto L_0x016c;
                case 56: goto L_0x015b;
                case 57: goto L_0x014a;
                case 58: goto L_0x0139;
                case 59: goto L_0x0128;
                case 60: goto L_0x0113;
                case 61: goto L_0x0100;
                case 62: goto L_0x00ef;
                case 63: goto L_0x00de;
                case 64: goto L_0x00cd;
                case 65: goto L_0x00bc;
                case 66: goto L_0x00ab;
                case 67: goto L_0x009a;
                case 68: goto L_0x0085;
                default: goto L_0x0083;
            }
        L_0x0083:
            goto L_0x0528
        L_0x0085:
            boolean r10 = r13.a(r14, r9, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            com.google.android.gms.internal.measurement.hj r10 = r13.a(r7)
            r15.b(r9, r8, r10)
            goto L_0x0528
        L_0x009a:
            boolean r10 = r13.a(r14, r9, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = e(r14, r10)
            r15.e(r9, r10)
            goto L_0x0528
        L_0x00ab:
            boolean r10 = r13.a(r14, r9, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = d(r14, r10)
            r15.f(r9, r8)
            goto L_0x0528
        L_0x00bc:
            boolean r10 = r13.a(r14, r9, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = e(r14, r10)
            r15.b(r9, r10)
            goto L_0x0528
        L_0x00cd:
            boolean r10 = r13.a(r14, r9, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = d(r14, r10)
            r15.a(r9, r8)
            goto L_0x0528
        L_0x00de:
            boolean r10 = r13.a(r14, r9, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = d(r14, r10)
            r15.b(r9, r8)
            goto L_0x0528
        L_0x00ef:
            boolean r10 = r13.a(r14, r9, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = d(r14, r10)
            r15.e(r9, r8)
            goto L_0x0528
        L_0x0100:
            boolean r10 = r13.a(r14, r9, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            com.google.android.gms.internal.measurement.ej r8 = (com.google.android.gms.internal.measurement.ej) r8
            r15.a(r9, r8)
            goto L_0x0528
        L_0x0113:
            boolean r10 = r13.a(r14, r9, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            com.google.android.gms.internal.measurement.hj r10 = r13.a(r7)
            r15.a(r9, r8, r10)
            goto L_0x0528
        L_0x0128:
            boolean r10 = r13.a(r14, r9, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            a(r9, r8, r15)
            goto L_0x0528
        L_0x0139:
            boolean r10 = r13.a(r14, r9, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            boolean r8 = f(r14, r10)
            r15.a(r9, r8)
            goto L_0x0528
        L_0x014a:
            boolean r10 = r13.a(r14, r9, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = d(r14, r10)
            r15.d(r9, r8)
            goto L_0x0528
        L_0x015b:
            boolean r10 = r13.a(r14, r9, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = e(r14, r10)
            r15.d(r9, r10)
            goto L_0x0528
        L_0x016c:
            boolean r10 = r13.a(r14, r9, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = d(r14, r10)
            r15.c(r9, r8)
            goto L_0x0528
        L_0x017d:
            boolean r10 = r13.a(r14, r9, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = e(r14, r10)
            r15.c(r9, r10)
            goto L_0x0528
        L_0x018e:
            boolean r10 = r13.a(r14, r9, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = e(r14, r10)
            r15.a(r9, r10)
            goto L_0x0528
        L_0x019f:
            boolean r10 = r13.a(r14, r9, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            float r8 = c(r14, r10)
            r15.a(r9, r8)
            goto L_0x0528
        L_0x01b0:
            boolean r10 = r13.a(r14, r9, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            double r10 = b(r14, r10)
            r15.a(r9, r10)
            goto L_0x0528
        L_0x01c1:
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            r13.a(r15, r9, r8, r7)
            goto L_0x0528
        L_0x01cc:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hj r10 = r13.a(r7)
            com.google.android.gms.internal.measurement.hl.b(r9, r8, r15, r10)
            goto L_0x0528
        L_0x01e1:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hl.e(r9, r8, r15, r4)
            goto L_0x0528
        L_0x01f2:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hl.j(r9, r8, r15, r4)
            goto L_0x0528
        L_0x0203:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hl.g(r9, r8, r15, r4)
            goto L_0x0528
        L_0x0214:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hl.l(r9, r8, r15, r4)
            goto L_0x0528
        L_0x0225:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hl.m(r9, r8, r15, r4)
            goto L_0x0528
        L_0x0236:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hl.i(r9, r8, r15, r4)
            goto L_0x0528
        L_0x0247:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hl.n(r9, r8, r15, r4)
            goto L_0x0528
        L_0x0258:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hl.k(r9, r8, r15, r4)
            goto L_0x0528
        L_0x0269:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hl.f(r9, r8, r15, r4)
            goto L_0x0528
        L_0x027a:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hl.h(r9, r8, r15, r4)
            goto L_0x0528
        L_0x028b:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hl.d(r9, r8, r15, r4)
            goto L_0x0528
        L_0x029c:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hl.c(r9, r8, r15, r4)
            goto L_0x0528
        L_0x02ad:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hl.b(r9, r8, r15, r4)
            goto L_0x0528
        L_0x02be:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hl.a(r9, r8, r15, r4)
            goto L_0x0528
        L_0x02cf:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hl.e(r9, r8, r15, r5)
            goto L_0x0528
        L_0x02e0:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hl.j(r9, r8, r15, r5)
            goto L_0x0528
        L_0x02f1:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hl.g(r9, r8, r15, r5)
            goto L_0x0528
        L_0x0302:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hl.l(r9, r8, r15, r5)
            goto L_0x0528
        L_0x0313:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hl.m(r9, r8, r15, r5)
            goto L_0x0528
        L_0x0324:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hl.i(r9, r8, r15, r5)
            goto L_0x0528
        L_0x0335:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hl.b(r9, r8, r15)
            goto L_0x0528
        L_0x0346:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hj r10 = r13.a(r7)
            com.google.android.gms.internal.measurement.hl.a(r9, r8, r15, r10)
            goto L_0x0528
        L_0x035b:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hl.a(r9, r8, r15)
            goto L_0x0528
        L_0x036c:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hl.n(r9, r8, r15, r5)
            goto L_0x0528
        L_0x037d:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hl.k(r9, r8, r15, r5)
            goto L_0x0528
        L_0x038e:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hl.f(r9, r8, r15, r5)
            goto L_0x0528
        L_0x039f:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hl.h(r9, r8, r15, r5)
            goto L_0x0528
        L_0x03b0:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hl.d(r9, r8, r15, r5)
            goto L_0x0528
        L_0x03c1:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hl.c(r9, r8, r15, r5)
            goto L_0x0528
        L_0x03d2:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hl.b(r9, r8, r15, r5)
            goto L_0x0528
        L_0x03e3:
            int[] r9 = r13.c
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.hl.a(r9, r8, r15, r5)
            goto L_0x0528
        L_0x03f4:
            boolean r10 = r13.a(r14, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            com.google.android.gms.internal.measurement.hj r10 = r13.a(r7)
            r15.b(r9, r8, r10)
            goto L_0x0528
        L_0x0409:
            boolean r10 = r13.a(r14, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = com.google.android.gms.internal.measurement.ih.b(r14, r10)
            r15.e(r9, r10)
            goto L_0x0528
        L_0x041a:
            boolean r10 = r13.a(r14, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.measurement.ih.a(r14, r10)
            r15.f(r9, r8)
            goto L_0x0528
        L_0x042b:
            boolean r10 = r13.a(r14, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = com.google.android.gms.internal.measurement.ih.b(r14, r10)
            r15.b(r9, r10)
            goto L_0x0528
        L_0x043c:
            boolean r10 = r13.a(r14, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.measurement.ih.a(r14, r10)
            r15.a(r9, r8)
            goto L_0x0528
        L_0x044d:
            boolean r10 = r13.a(r14, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.measurement.ih.a(r14, r10)
            r15.b(r9, r8)
            goto L_0x0528
        L_0x045e:
            boolean r10 = r13.a(r14, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.measurement.ih.a(r14, r10)
            r15.e(r9, r8)
            goto L_0x0528
        L_0x046f:
            boolean r10 = r13.a(r14, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            com.google.android.gms.internal.measurement.ej r8 = (com.google.android.gms.internal.measurement.ej) r8
            r15.a(r9, r8)
            goto L_0x0528
        L_0x0482:
            boolean r10 = r13.a(r14, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            com.google.android.gms.internal.measurement.hj r10 = r13.a(r7)
            r15.a(r9, r8, r10)
            goto L_0x0528
        L_0x0497:
            boolean r10 = r13.a(r14, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r14, r10)
            a(r9, r8, r15)
            goto L_0x0528
        L_0x04a8:
            boolean r10 = r13.a(r14, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            boolean r8 = com.google.android.gms.internal.measurement.ih.c(r14, r10)
            r15.a(r9, r8)
            goto L_0x0528
        L_0x04b9:
            boolean r10 = r13.a(r14, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.measurement.ih.a(r14, r10)
            r15.d(r9, r8)
            goto L_0x0528
        L_0x04c9:
            boolean r10 = r13.a(r14, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = com.google.android.gms.internal.measurement.ih.b(r14, r10)
            r15.d(r9, r10)
            goto L_0x0528
        L_0x04d9:
            boolean r10 = r13.a(r14, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            int r8 = com.google.android.gms.internal.measurement.ih.a(r14, r10)
            r15.c(r9, r8)
            goto L_0x0528
        L_0x04e9:
            boolean r10 = r13.a(r14, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = com.google.android.gms.internal.measurement.ih.b(r14, r10)
            r15.c(r9, r10)
            goto L_0x0528
        L_0x04f9:
            boolean r10 = r13.a(r14, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            long r10 = com.google.android.gms.internal.measurement.ih.b(r14, r10)
            r15.a(r9, r10)
            goto L_0x0528
        L_0x0509:
            boolean r10 = r13.a(r14, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            float r8 = com.google.android.gms.internal.measurement.ih.d(r14, r10)
            r15.a(r9, r8)
            goto L_0x0528
        L_0x0519:
            boolean r10 = r13.a(r14, r7)
            if (r10 == 0) goto L_0x0528
            r8 = r8 & r6
            long r10 = (long) r8
            double r10 = com.google.android.gms.internal.measurement.ih.e(r14, r10)
            r15.a(r9, r10)
        L_0x0528:
            int r7 = r7 + -3
            goto L_0x0054
        L_0x052c:
            if (r1 == 0) goto L_0x0543
            com.google.android.gms.internal.measurement.fe<?> r14 = r13.r
            r14.b(r1)
            boolean r14 = r0.hasNext()
            if (r14 == 0) goto L_0x0541
            java.lang.Object r14 = r0.next()
            java.util.Map$Entry r14 = (java.util.Map.Entry) r14
            r1 = r14
            goto L_0x052c
        L_0x0541:
            r1 = r3
            goto L_0x052c
        L_0x0543:
            return
        L_0x0544:
            boolean r0 = r13.j
            if (r0 == 0) goto L_0x0a61
            boolean r0 = r13.h
            if (r0 == 0) goto L_0x0565
            com.google.android.gms.internal.measurement.fe<?> r0 = r13.r
            com.google.android.gms.internal.measurement.fh r0 = r0.a(r14)
            com.google.android.gms.internal.measurement.hm<FieldDescriptorType, java.lang.Object> r1 = r0.f2206a
            boolean r1 = r1.isEmpty()
            if (r1 != 0) goto L_0x0565
            java.util.Iterator r0 = r0.c()
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            goto L_0x0567
        L_0x0565:
            r0 = r3
            r1 = r0
        L_0x0567:
            int[] r7 = r13.c
            int r7 = r7.length
            r8 = r1
            r1 = 0
        L_0x056c:
            if (r1 >= r7) goto L_0x0a44
            int r9 = r13.c(r1)
            int[] r10 = r13.c
            r10 = r10[r1]
        L_0x0576:
            if (r8 == 0) goto L_0x0594
            com.google.android.gms.internal.measurement.fe<?> r11 = r13.r
            int r11 = r11.a(r8)
            if (r11 > r10) goto L_0x0594
            com.google.android.gms.internal.measurement.fe<?> r11 = r13.r
            r11.b(r8)
            boolean r8 = r0.hasNext()
            if (r8 == 0) goto L_0x0592
            java.lang.Object r8 = r0.next()
            java.util.Map$Entry r8 = (java.util.Map.Entry) r8
            goto L_0x0576
        L_0x0592:
            r8 = r3
            goto L_0x0576
        L_0x0594:
            r11 = r9 & r2
            int r11 = r11 >>> 20
            switch(r11) {
                case 0: goto L_0x0a31;
                case 1: goto L_0x0a21;
                case 2: goto L_0x0a11;
                case 3: goto L_0x0a01;
                case 4: goto L_0x09f1;
                case 5: goto L_0x09e1;
                case 6: goto L_0x09d1;
                case 7: goto L_0x09c0;
                case 8: goto L_0x09af;
                case 9: goto L_0x099a;
                case 10: goto L_0x0987;
                case 11: goto L_0x0976;
                case 12: goto L_0x0965;
                case 13: goto L_0x0954;
                case 14: goto L_0x0943;
                case 15: goto L_0x0932;
                case 16: goto L_0x0921;
                case 17: goto L_0x090c;
                case 18: goto L_0x08fb;
                case 19: goto L_0x08ea;
                case 20: goto L_0x08d9;
                case 21: goto L_0x08c8;
                case 22: goto L_0x08b7;
                case 23: goto L_0x08a6;
                case 24: goto L_0x0895;
                case 25: goto L_0x0884;
                case 26: goto L_0x0873;
                case 27: goto L_0x085e;
                case 28: goto L_0x084d;
                case 29: goto L_0x083c;
                case 30: goto L_0x082b;
                case 31: goto L_0x081a;
                case 32: goto L_0x0809;
                case 33: goto L_0x07f8;
                case 34: goto L_0x07e7;
                case 35: goto L_0x07d6;
                case 36: goto L_0x07c5;
                case 37: goto L_0x07b4;
                case 38: goto L_0x07a3;
                case 39: goto L_0x0792;
                case 40: goto L_0x0781;
                case 41: goto L_0x0770;
                case 42: goto L_0x075f;
                case 43: goto L_0x074e;
                case 44: goto L_0x073d;
                case 45: goto L_0x072c;
                case 46: goto L_0x071b;
                case 47: goto L_0x070a;
                case 48: goto L_0x06f9;
                case 49: goto L_0x06e4;
                case 50: goto L_0x06d9;
                case 51: goto L_0x06c8;
                case 52: goto L_0x06b7;
                case 53: goto L_0x06a6;
                case 54: goto L_0x0695;
                case 55: goto L_0x0684;
                case 56: goto L_0x0673;
                case 57: goto L_0x0662;
                case 58: goto L_0x0651;
                case 59: goto L_0x0640;
                case 60: goto L_0x062b;
                case 61: goto L_0x0618;
                case 62: goto L_0x0607;
                case 63: goto L_0x05f6;
                case 64: goto L_0x05e5;
                case 65: goto L_0x05d4;
                case 66: goto L_0x05c3;
                case 67: goto L_0x05b2;
                case 68: goto L_0x059d;
                default: goto L_0x059b;
            }
        L_0x059b:
            goto L_0x0a40
        L_0x059d:
            boolean r11 = r13.a(r14, r10, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            com.google.android.gms.internal.measurement.hj r11 = r13.a(r1)
            r15.b(r10, r9, r11)
            goto L_0x0a40
        L_0x05b2:
            boolean r11 = r13.a(r14, r10, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = e(r14, r11)
            r15.e(r10, r11)
            goto L_0x0a40
        L_0x05c3:
            boolean r11 = r13.a(r14, r10, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = d(r14, r11)
            r15.f(r10, r9)
            goto L_0x0a40
        L_0x05d4:
            boolean r11 = r13.a(r14, r10, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = e(r14, r11)
            r15.b(r10, r11)
            goto L_0x0a40
        L_0x05e5:
            boolean r11 = r13.a(r14, r10, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = d(r14, r11)
            r15.a(r10, r9)
            goto L_0x0a40
        L_0x05f6:
            boolean r11 = r13.a(r14, r10, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = d(r14, r11)
            r15.b(r10, r9)
            goto L_0x0a40
        L_0x0607:
            boolean r11 = r13.a(r14, r10, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = d(r14, r11)
            r15.e(r10, r9)
            goto L_0x0a40
        L_0x0618:
            boolean r11 = r13.a(r14, r10, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            com.google.android.gms.internal.measurement.ej r9 = (com.google.android.gms.internal.measurement.ej) r9
            r15.a(r10, r9)
            goto L_0x0a40
        L_0x062b:
            boolean r11 = r13.a(r14, r10, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            com.google.android.gms.internal.measurement.hj r11 = r13.a(r1)
            r15.a(r10, r9, r11)
            goto L_0x0a40
        L_0x0640:
            boolean r11 = r13.a(r14, r10, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            a(r10, r9, r15)
            goto L_0x0a40
        L_0x0651:
            boolean r11 = r13.a(r14, r10, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            boolean r9 = f(r14, r11)
            r15.a(r10, r9)
            goto L_0x0a40
        L_0x0662:
            boolean r11 = r13.a(r14, r10, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = d(r14, r11)
            r15.d(r10, r9)
            goto L_0x0a40
        L_0x0673:
            boolean r11 = r13.a(r14, r10, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = e(r14, r11)
            r15.d(r10, r11)
            goto L_0x0a40
        L_0x0684:
            boolean r11 = r13.a(r14, r10, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = d(r14, r11)
            r15.c(r10, r9)
            goto L_0x0a40
        L_0x0695:
            boolean r11 = r13.a(r14, r10, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = e(r14, r11)
            r15.c(r10, r11)
            goto L_0x0a40
        L_0x06a6:
            boolean r11 = r13.a(r14, r10, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = e(r14, r11)
            r15.a(r10, r11)
            goto L_0x0a40
        L_0x06b7:
            boolean r11 = r13.a(r14, r10, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            float r9 = c(r14, r11)
            r15.a(r10, r9)
            goto L_0x0a40
        L_0x06c8:
            boolean r11 = r13.a(r14, r10, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            double r11 = b(r14, r11)
            r15.a(r10, r11)
            goto L_0x0a40
        L_0x06d9:
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            r13.a(r15, r10, r9, r1)
            goto L_0x0a40
        L_0x06e4:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hj r11 = r13.a(r1)
            com.google.android.gms.internal.measurement.hl.b(r10, r9, r15, r11)
            goto L_0x0a40
        L_0x06f9:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.e(r10, r9, r15, r4)
            goto L_0x0a40
        L_0x070a:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.j(r10, r9, r15, r4)
            goto L_0x0a40
        L_0x071b:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.g(r10, r9, r15, r4)
            goto L_0x0a40
        L_0x072c:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.l(r10, r9, r15, r4)
            goto L_0x0a40
        L_0x073d:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.m(r10, r9, r15, r4)
            goto L_0x0a40
        L_0x074e:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.i(r10, r9, r15, r4)
            goto L_0x0a40
        L_0x075f:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.n(r10, r9, r15, r4)
            goto L_0x0a40
        L_0x0770:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.k(r10, r9, r15, r4)
            goto L_0x0a40
        L_0x0781:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.f(r10, r9, r15, r4)
            goto L_0x0a40
        L_0x0792:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.h(r10, r9, r15, r4)
            goto L_0x0a40
        L_0x07a3:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.d(r10, r9, r15, r4)
            goto L_0x0a40
        L_0x07b4:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.c(r10, r9, r15, r4)
            goto L_0x0a40
        L_0x07c5:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.b(r10, r9, r15, r4)
            goto L_0x0a40
        L_0x07d6:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.a(r10, r9, r15, r4)
            goto L_0x0a40
        L_0x07e7:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.e(r10, r9, r15, r5)
            goto L_0x0a40
        L_0x07f8:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.j(r10, r9, r15, r5)
            goto L_0x0a40
        L_0x0809:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.g(r10, r9, r15, r5)
            goto L_0x0a40
        L_0x081a:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.l(r10, r9, r15, r5)
            goto L_0x0a40
        L_0x082b:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.m(r10, r9, r15, r5)
            goto L_0x0a40
        L_0x083c:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.i(r10, r9, r15, r5)
            goto L_0x0a40
        L_0x084d:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.b(r10, r9, r15)
            goto L_0x0a40
        L_0x085e:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hj r11 = r13.a(r1)
            com.google.android.gms.internal.measurement.hl.a(r10, r9, r15, r11)
            goto L_0x0a40
        L_0x0873:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.a(r10, r9, r15)
            goto L_0x0a40
        L_0x0884:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.n(r10, r9, r15, r5)
            goto L_0x0a40
        L_0x0895:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.k(r10, r9, r15, r5)
            goto L_0x0a40
        L_0x08a6:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.f(r10, r9, r15, r5)
            goto L_0x0a40
        L_0x08b7:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.h(r10, r9, r15, r5)
            goto L_0x0a40
        L_0x08c8:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.d(r10, r9, r15, r5)
            goto L_0x0a40
        L_0x08d9:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.c(r10, r9, r15, r5)
            goto L_0x0a40
        L_0x08ea:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.b(r10, r9, r15, r5)
            goto L_0x0a40
        L_0x08fb:
            int[] r10 = r13.c
            r10 = r10[r1]
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.a(r10, r9, r15, r5)
            goto L_0x0a40
        L_0x090c:
            boolean r11 = r13.a(r14, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            com.google.android.gms.internal.measurement.hj r11 = r13.a(r1)
            r15.b(r10, r9, r11)
            goto L_0x0a40
        L_0x0921:
            boolean r11 = r13.a(r14, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = com.google.android.gms.internal.measurement.ih.b(r14, r11)
            r15.e(r10, r11)
            goto L_0x0a40
        L_0x0932:
            boolean r11 = r13.a(r14, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.measurement.ih.a(r14, r11)
            r15.f(r10, r9)
            goto L_0x0a40
        L_0x0943:
            boolean r11 = r13.a(r14, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = com.google.android.gms.internal.measurement.ih.b(r14, r11)
            r15.b(r10, r11)
            goto L_0x0a40
        L_0x0954:
            boolean r11 = r13.a(r14, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.measurement.ih.a(r14, r11)
            r15.a(r10, r9)
            goto L_0x0a40
        L_0x0965:
            boolean r11 = r13.a(r14, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.measurement.ih.a(r14, r11)
            r15.b(r10, r9)
            goto L_0x0a40
        L_0x0976:
            boolean r11 = r13.a(r14, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.measurement.ih.a(r14, r11)
            r15.e(r10, r9)
            goto L_0x0a40
        L_0x0987:
            boolean r11 = r13.a(r14, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            com.google.android.gms.internal.measurement.ej r9 = (com.google.android.gms.internal.measurement.ej) r9
            r15.a(r10, r9)
            goto L_0x0a40
        L_0x099a:
            boolean r11 = r13.a(r14, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            com.google.android.gms.internal.measurement.hj r11 = r13.a(r1)
            r15.a(r10, r9, r11)
            goto L_0x0a40
        L_0x09af:
            boolean r11 = r13.a(r14, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            java.lang.Object r9 = com.google.android.gms.internal.measurement.ih.f(r14, r11)
            a(r10, r9, r15)
            goto L_0x0a40
        L_0x09c0:
            boolean r11 = r13.a(r14, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            boolean r9 = com.google.android.gms.internal.measurement.ih.c(r14, r11)
            r15.a(r10, r9)
            goto L_0x0a40
        L_0x09d1:
            boolean r11 = r13.a(r14, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.measurement.ih.a(r14, r11)
            r15.d(r10, r9)
            goto L_0x0a40
        L_0x09e1:
            boolean r11 = r13.a(r14, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = com.google.android.gms.internal.measurement.ih.b(r14, r11)
            r15.d(r10, r11)
            goto L_0x0a40
        L_0x09f1:
            boolean r11 = r13.a(r14, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            int r9 = com.google.android.gms.internal.measurement.ih.a(r14, r11)
            r15.c(r10, r9)
            goto L_0x0a40
        L_0x0a01:
            boolean r11 = r13.a(r14, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = com.google.android.gms.internal.measurement.ih.b(r14, r11)
            r15.c(r10, r11)
            goto L_0x0a40
        L_0x0a11:
            boolean r11 = r13.a(r14, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            long r11 = com.google.android.gms.internal.measurement.ih.b(r14, r11)
            r15.a(r10, r11)
            goto L_0x0a40
        L_0x0a21:
            boolean r11 = r13.a(r14, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            float r9 = com.google.android.gms.internal.measurement.ih.d(r14, r11)
            r15.a(r10, r9)
            goto L_0x0a40
        L_0x0a31:
            boolean r11 = r13.a(r14, r1)
            if (r11 == 0) goto L_0x0a40
            r9 = r9 & r6
            long r11 = (long) r9
            double r11 = com.google.android.gms.internal.measurement.ih.e(r14, r11)
            r15.a(r10, r11)
        L_0x0a40:
            int r1 = r1 + 3
            goto L_0x056c
        L_0x0a44:
            if (r8 == 0) goto L_0x0a5b
            com.google.android.gms.internal.measurement.fe<?> r1 = r13.r
            r1.b(r8)
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0a59
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            r8 = r1
            goto L_0x0a44
        L_0x0a59:
            r8 = r3
            goto L_0x0a44
        L_0x0a5b:
            com.google.android.gms.internal.measurement.ib<?, ?> r0 = r13.q
            a(r0, r14, r15)
            return
        L_0x0a61:
            r13.b(r14, r15)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.gx.a(java.lang.Object, com.google.android.gms.internal.measurement.iv):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.hl.b(int, java.util.List<java.lang.Float>, com.google.android.gms.internal.measurement.iv, boolean):void
     arg types: [int, java.util.List, com.google.android.gms.internal.measurement.iv, int]
     candidates:
      com.google.android.gms.internal.measurement.hl.b(int, java.util.List<?>, com.google.android.gms.internal.measurement.iv, com.google.android.gms.internal.measurement.hj):void
      com.google.android.gms.internal.measurement.hl.b(int, java.util.List<java.lang.Float>, com.google.android.gms.internal.measurement.iv, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.hl.a(int, java.util.List<java.lang.Double>, com.google.android.gms.internal.measurement.iv, boolean):void
     arg types: [int, java.util.List, com.google.android.gms.internal.measurement.iv, int]
     candidates:
      com.google.android.gms.internal.measurement.hl.a(int, int, java.lang.Object, com.google.android.gms.internal.measurement.ib):UB
      com.google.android.gms.internal.measurement.hl.a(int, java.util.List<?>, com.google.android.gms.internal.measurement.iv, com.google.android.gms.internal.measurement.hj):void
      com.google.android.gms.internal.measurement.hl.a(com.google.android.gms.internal.measurement.go, java.lang.Object, java.lang.Object, long):void
      com.google.android.gms.internal.measurement.hl.a(int, java.util.List<java.lang.Double>, com.google.android.gms.internal.measurement.iv, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:172:0x04b3  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0030  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void b(T r19, com.google.android.gms.internal.measurement.iv r20) throws java.io.IOException {
        /*
            r18 = this;
            r0 = r18
            r1 = r19
            r2 = r20
            boolean r3 = r0.h
            if (r3 == 0) goto L_0x0023
            com.google.android.gms.internal.measurement.fe<?> r3 = r0.r
            com.google.android.gms.internal.measurement.fh r3 = r3.a(r1)
            com.google.android.gms.internal.measurement.hm<FieldDescriptorType, java.lang.Object> r5 = r3.f2206a
            boolean r5 = r5.isEmpty()
            if (r5 != 0) goto L_0x0023
            java.util.Iterator r3 = r3.c()
            java.lang.Object r5 = r3.next()
            java.util.Map$Entry r5 = (java.util.Map.Entry) r5
            goto L_0x0025
        L_0x0023:
            r3 = 0
            r5 = 0
        L_0x0025:
            r6 = -1
            int[] r7 = r0.c
            int r7 = r7.length
            sun.misc.Unsafe r8 = com.google.android.gms.internal.measurement.gx.f2251b
            r10 = r5
            r5 = 0
            r11 = 0
        L_0x002e:
            if (r5 >= r7) goto L_0x04ad
            int r12 = r0.c(r5)
            int[] r13 = r0.c
            r14 = r13[r5]
            r15 = 267386880(0xff00000, float:2.3665827E-29)
            r15 = r15 & r12
            int r15 = r15 >>> 20
            boolean r4 = r0.j
            r16 = 1048575(0xfffff, float:1.469367E-39)
            if (r4 != 0) goto L_0x0062
            r4 = 17
            if (r15 > r4) goto L_0x0062
            int r4 = r5 + 2
            r4 = r13[r4]
            r13 = r4 & r16
            r17 = r10
            if (r13 == r6) goto L_0x0058
            long r9 = (long) r13
            int r11 = r8.getInt(r1, r9)
            goto L_0x0059
        L_0x0058:
            r13 = r6
        L_0x0059:
            int r4 = r4 >>> 20
            r6 = 1
            int r9 = r6 << r4
            r6 = r13
            r10 = r17
            goto L_0x0067
        L_0x0062:
            r17 = r10
            r10 = r17
            r9 = 0
        L_0x0067:
            if (r10 == 0) goto L_0x0086
            com.google.android.gms.internal.measurement.fe<?> r4 = r0.r
            int r4 = r4.a(r10)
            if (r4 > r14) goto L_0x0086
            com.google.android.gms.internal.measurement.fe<?> r4 = r0.r
            r4.b(r10)
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x0084
            java.lang.Object r4 = r3.next()
            java.util.Map$Entry r4 = (java.util.Map.Entry) r4
            r10 = r4
            goto L_0x0067
        L_0x0084:
            r10 = 0
            goto L_0x0067
        L_0x0086:
            r4 = r12 & r16
            long r12 = (long) r4
            switch(r15) {
                case 0: goto L_0x049d;
                case 1: goto L_0x0490;
                case 2: goto L_0x0483;
                case 3: goto L_0x0476;
                case 4: goto L_0x0469;
                case 5: goto L_0x045c;
                case 6: goto L_0x044f;
                case 7: goto L_0x0442;
                case 8: goto L_0x0434;
                case 9: goto L_0x0422;
                case 10: goto L_0x0412;
                case 11: goto L_0x0404;
                case 12: goto L_0x03f6;
                case 13: goto L_0x03e8;
                case 14: goto L_0x03da;
                case 15: goto L_0x03cc;
                case 16: goto L_0x03be;
                case 17: goto L_0x03ac;
                case 18: goto L_0x039c;
                case 19: goto L_0x038c;
                case 20: goto L_0x037c;
                case 21: goto L_0x036c;
                case 22: goto L_0x035c;
                case 23: goto L_0x034c;
                case 24: goto L_0x033c;
                case 25: goto L_0x032c;
                case 26: goto L_0x031d;
                case 27: goto L_0x030a;
                case 28: goto L_0x02fb;
                case 29: goto L_0x02eb;
                case 30: goto L_0x02db;
                case 31: goto L_0x02cb;
                case 32: goto L_0x02bb;
                case 33: goto L_0x02ab;
                case 34: goto L_0x029b;
                case 35: goto L_0x028b;
                case 36: goto L_0x027b;
                case 37: goto L_0x026b;
                case 38: goto L_0x025b;
                case 39: goto L_0x024b;
                case 40: goto L_0x023b;
                case 41: goto L_0x022b;
                case 42: goto L_0x021b;
                case 43: goto L_0x020b;
                case 44: goto L_0x01fb;
                case 45: goto L_0x01eb;
                case 46: goto L_0x01db;
                case 47: goto L_0x01cb;
                case 48: goto L_0x01bb;
                case 49: goto L_0x01a8;
                case 50: goto L_0x019f;
                case 51: goto L_0x0190;
                case 52: goto L_0x0181;
                case 53: goto L_0x0172;
                case 54: goto L_0x0163;
                case 55: goto L_0x0154;
                case 56: goto L_0x0145;
                case 57: goto L_0x0136;
                case 58: goto L_0x0127;
                case 59: goto L_0x0118;
                case 60: goto L_0x0105;
                case 61: goto L_0x00f5;
                case 62: goto L_0x00e7;
                case 63: goto L_0x00d9;
                case 64: goto L_0x00cb;
                case 65: goto L_0x00bd;
                case 66: goto L_0x00af;
                case 67: goto L_0x00a1;
                case 68: goto L_0x008f;
                default: goto L_0x008c;
            }
        L_0x008c:
            r15 = 0
            goto L_0x04a9
        L_0x008f:
            boolean r4 = r0.a(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            java.lang.Object r4 = r8.getObject(r1, r12)
            com.google.android.gms.internal.measurement.hj r9 = r0.a(r5)
            r2.b(r14, r4, r9)
            goto L_0x008c
        L_0x00a1:
            boolean r4 = r0.a(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            long r12 = e(r1, r12)
            r2.e(r14, r12)
            goto L_0x008c
        L_0x00af:
            boolean r4 = r0.a(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            int r4 = d(r1, r12)
            r2.f(r14, r4)
            goto L_0x008c
        L_0x00bd:
            boolean r4 = r0.a(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            long r12 = e(r1, r12)
            r2.b(r14, r12)
            goto L_0x008c
        L_0x00cb:
            boolean r4 = r0.a(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            int r4 = d(r1, r12)
            r2.a(r14, r4)
            goto L_0x008c
        L_0x00d9:
            boolean r4 = r0.a(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            int r4 = d(r1, r12)
            r2.b(r14, r4)
            goto L_0x008c
        L_0x00e7:
            boolean r4 = r0.a(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            int r4 = d(r1, r12)
            r2.e(r14, r4)
            goto L_0x008c
        L_0x00f5:
            boolean r4 = r0.a(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            java.lang.Object r4 = r8.getObject(r1, r12)
            com.google.android.gms.internal.measurement.ej r4 = (com.google.android.gms.internal.measurement.ej) r4
            r2.a(r14, r4)
            goto L_0x008c
        L_0x0105:
            boolean r4 = r0.a(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            java.lang.Object r4 = r8.getObject(r1, r12)
            com.google.android.gms.internal.measurement.hj r9 = r0.a(r5)
            r2.a(r14, r4, r9)
            goto L_0x008c
        L_0x0118:
            boolean r4 = r0.a(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            java.lang.Object r4 = r8.getObject(r1, r12)
            a(r14, r4, r2)
            goto L_0x008c
        L_0x0127:
            boolean r4 = r0.a(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            boolean r4 = f(r1, r12)
            r2.a(r14, r4)
            goto L_0x008c
        L_0x0136:
            boolean r4 = r0.a(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            int r4 = d(r1, r12)
            r2.d(r14, r4)
            goto L_0x008c
        L_0x0145:
            boolean r4 = r0.a(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            long r12 = e(r1, r12)
            r2.d(r14, r12)
            goto L_0x008c
        L_0x0154:
            boolean r4 = r0.a(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            int r4 = d(r1, r12)
            r2.c(r14, r4)
            goto L_0x008c
        L_0x0163:
            boolean r4 = r0.a(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            long r12 = e(r1, r12)
            r2.c(r14, r12)
            goto L_0x008c
        L_0x0172:
            boolean r4 = r0.a(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            long r12 = e(r1, r12)
            r2.a(r14, r12)
            goto L_0x008c
        L_0x0181:
            boolean r4 = r0.a(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            float r4 = c(r1, r12)
            r2.a(r14, r4)
            goto L_0x008c
        L_0x0190:
            boolean r4 = r0.a(r1, r14, r5)
            if (r4 == 0) goto L_0x008c
            double r12 = b(r1, r12)
            r2.a(r14, r12)
            goto L_0x008c
        L_0x019f:
            java.lang.Object r4 = r8.getObject(r1, r12)
            r0.a(r2, r14, r4, r5)
            goto L_0x008c
        L_0x01a8:
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hj r12 = r0.a(r5)
            com.google.android.gms.internal.measurement.hl.b(r4, r9, r2, r12)
            goto L_0x008c
        L_0x01bb:
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            r14 = 1
            com.google.android.gms.internal.measurement.hl.e(r4, r9, r2, r14)
            goto L_0x008c
        L_0x01cb:
            r14 = 1
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.j(r4, r9, r2, r14)
            goto L_0x008c
        L_0x01db:
            r14 = 1
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.g(r4, r9, r2, r14)
            goto L_0x008c
        L_0x01eb:
            r14 = 1
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.l(r4, r9, r2, r14)
            goto L_0x008c
        L_0x01fb:
            r14 = 1
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.m(r4, r9, r2, r14)
            goto L_0x008c
        L_0x020b:
            r14 = 1
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.i(r4, r9, r2, r14)
            goto L_0x008c
        L_0x021b:
            r14 = 1
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.n(r4, r9, r2, r14)
            goto L_0x008c
        L_0x022b:
            r14 = 1
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.k(r4, r9, r2, r14)
            goto L_0x008c
        L_0x023b:
            r14 = 1
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.f(r4, r9, r2, r14)
            goto L_0x008c
        L_0x024b:
            r14 = 1
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.h(r4, r9, r2, r14)
            goto L_0x008c
        L_0x025b:
            r14 = 1
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.d(r4, r9, r2, r14)
            goto L_0x008c
        L_0x026b:
            r14 = 1
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.c(r4, r9, r2, r14)
            goto L_0x008c
        L_0x027b:
            r14 = 1
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.b(r4, r9, r2, r14)
            goto L_0x008c
        L_0x028b:
            r14 = 1
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.a(r4, r9, r2, r14)
            goto L_0x008c
        L_0x029b:
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            r14 = 0
            com.google.android.gms.internal.measurement.hl.e(r4, r9, r2, r14)
            goto L_0x008c
        L_0x02ab:
            r14 = 0
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.j(r4, r9, r2, r14)
            goto L_0x008c
        L_0x02bb:
            r14 = 0
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.g(r4, r9, r2, r14)
            goto L_0x008c
        L_0x02cb:
            r14 = 0
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.l(r4, r9, r2, r14)
            goto L_0x008c
        L_0x02db:
            r14 = 0
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.m(r4, r9, r2, r14)
            goto L_0x008c
        L_0x02eb:
            r14 = 0
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.i(r4, r9, r2, r14)
            goto L_0x008c
        L_0x02fb:
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.b(r4, r9, r2)
            goto L_0x008c
        L_0x030a:
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hj r12 = r0.a(r5)
            com.google.android.gms.internal.measurement.hl.a(r4, r9, r2, r12)
            goto L_0x008c
        L_0x031d:
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.a(r4, r9, r2)
            goto L_0x008c
        L_0x032c:
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            r15 = 0
            com.google.android.gms.internal.measurement.hl.n(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x033c:
            r15 = 0
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.k(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x034c:
            r15 = 0
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.f(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x035c:
            r15 = 0
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.h(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x036c:
            r15 = 0
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.d(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x037c:
            r15 = 0
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.c(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x038c:
            r15 = 0
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.b(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x039c:
            r15 = 0
            int[] r4 = r0.c
            r4 = r4[r5]
            java.lang.Object r9 = r8.getObject(r1, r12)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.hl.a(r4, r9, r2, r15)
            goto L_0x04a9
        L_0x03ac:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            java.lang.Object r4 = r8.getObject(r1, r12)
            com.google.android.gms.internal.measurement.hj r9 = r0.a(r5)
            r2.b(r14, r4, r9)
            goto L_0x04a9
        L_0x03be:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            long r12 = r8.getLong(r1, r12)
            r2.e(r14, r12)
            goto L_0x04a9
        L_0x03cc:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            int r4 = r8.getInt(r1, r12)
            r2.f(r14, r4)
            goto L_0x04a9
        L_0x03da:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            long r12 = r8.getLong(r1, r12)
            r2.b(r14, r12)
            goto L_0x04a9
        L_0x03e8:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            int r4 = r8.getInt(r1, r12)
            r2.a(r14, r4)
            goto L_0x04a9
        L_0x03f6:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            int r4 = r8.getInt(r1, r12)
            r2.b(r14, r4)
            goto L_0x04a9
        L_0x0404:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            int r4 = r8.getInt(r1, r12)
            r2.e(r14, r4)
            goto L_0x04a9
        L_0x0412:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            java.lang.Object r4 = r8.getObject(r1, r12)
            com.google.android.gms.internal.measurement.ej r4 = (com.google.android.gms.internal.measurement.ej) r4
            r2.a(r14, r4)
            goto L_0x04a9
        L_0x0422:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            java.lang.Object r4 = r8.getObject(r1, r12)
            com.google.android.gms.internal.measurement.hj r9 = r0.a(r5)
            r2.a(r14, r4, r9)
            goto L_0x04a9
        L_0x0434:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            java.lang.Object r4 = r8.getObject(r1, r12)
            a(r14, r4, r2)
            goto L_0x04a9
        L_0x0442:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            boolean r4 = com.google.android.gms.internal.measurement.ih.c(r1, r12)
            r2.a(r14, r4)
            goto L_0x04a9
        L_0x044f:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            int r4 = r8.getInt(r1, r12)
            r2.d(r14, r4)
            goto L_0x04a9
        L_0x045c:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            long r12 = r8.getLong(r1, r12)
            r2.d(r14, r12)
            goto L_0x04a9
        L_0x0469:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            int r4 = r8.getInt(r1, r12)
            r2.c(r14, r4)
            goto L_0x04a9
        L_0x0476:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            long r12 = r8.getLong(r1, r12)
            r2.c(r14, r12)
            goto L_0x04a9
        L_0x0483:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            long r12 = r8.getLong(r1, r12)
            r2.a(r14, r12)
            goto L_0x04a9
        L_0x0490:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            float r4 = com.google.android.gms.internal.measurement.ih.d(r1, r12)
            r2.a(r14, r4)
            goto L_0x04a9
        L_0x049d:
            r15 = 0
            r4 = r11 & r9
            if (r4 == 0) goto L_0x04a9
            double r12 = com.google.android.gms.internal.measurement.ih.e(r1, r12)
            r2.a(r14, r12)
        L_0x04a9:
            int r5 = r5 + 3
            goto L_0x002e
        L_0x04ad:
            r17 = r10
            r4 = r17
        L_0x04b1:
            if (r4 == 0) goto L_0x04c7
            com.google.android.gms.internal.measurement.fe<?> r5 = r0.r
            r5.b(r4)
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x04c5
            java.lang.Object r4 = r3.next()
            java.util.Map$Entry r4 = (java.util.Map.Entry) r4
            goto L_0x04b1
        L_0x04c5:
            r4 = 0
            goto L_0x04b1
        L_0x04c7:
            com.google.android.gms.internal.measurement.ib<?, ?> r3 = r0.q
            a(r3, r1, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.gx.b(java.lang.Object, com.google.android.gms.internal.measurement.iv):void");
    }

    private final <K, V> void a(iv ivVar, int i2, Object obj, int i3) throws IOException {
        if (obj != null) {
            ivVar.a(i2, this.s.b(), this.s.b(obj));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.ib.a(java.lang.Object, com.google.android.gms.internal.measurement.iv):void
     arg types: [UT, com.google.android.gms.internal.measurement.iv]
     candidates:
      com.google.android.gms.internal.measurement.ib.a(java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.ib.a(java.lang.Object, com.google.android.gms.internal.measurement.hi):boolean
      com.google.android.gms.internal.measurement.ib.a(java.lang.Object, com.google.android.gms.internal.measurement.iv):void */
    private static <UT, UB> void a(ib<UT, UB> ibVar, T t, iv ivVar) throws IOException {
        ibVar.a((Object) ibVar.b(t), ivVar);
    }

    /*  JADX ERROR: StackOverflow in pass: MarkFinallyVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    public final void a(T r13, com.google.android.gms.internal.measurement.hi r14, com.google.android.gms.internal.measurement.fd r15) throws java.io.IOException {
        /*
            r12 = this;
            if (r15 == 0) goto L_0x05cf
            com.google.android.gms.internal.measurement.ib<?, ?> r0 = r12.q
            com.google.android.gms.internal.measurement.fe<?> r1 = r12.r
            r2 = 0
            r3 = r2
            r4 = r3
        L_0x0009:
            int r5 = r14.a()     // Catch:{ all -> 0x05b7 }
            int r6 = r12.e     // Catch:{ all -> 0x05b7 }
            r7 = -1
            if (r5 < r6) goto L_0x0036
            int r6 = r12.f     // Catch:{ all -> 0x05b7 }
            if (r5 > r6) goto L_0x0036
            r6 = 0
            int[] r8 = r12.c     // Catch:{ all -> 0x05b7 }
            int r8 = r8.length     // Catch:{ all -> 0x05b7 }
            int r8 = r8 / 3
            int r8 = r8 + -1
        L_0x001e:
            if (r6 > r8) goto L_0x0036
            int r9 = r8 + r6
            int r9 = r9 >>> 1
            int r10 = r9 * 3
            int[] r11 = r12.c     // Catch:{ all -> 0x05b7 }
            r11 = r11[r10]     // Catch:{ all -> 0x05b7 }
            if (r5 != r11) goto L_0x002e
            r7 = r10
            goto L_0x0036
        L_0x002e:
            if (r5 >= r11) goto L_0x0033
            int r8 = r9 + -1
            goto L_0x001e
        L_0x0033:
            int r6 = r9 + 1
            goto L_0x001e
        L_0x0036:
            if (r7 >= 0) goto L_0x0090
            r6 = 2147483647(0x7fffffff, float:NaN)
            if (r5 != r6) goto L_0x0054
            int r14 = r12.m
        L_0x003f:
            int r15 = r12.n
            if (r14 >= r15) goto L_0x004e
            int[] r15 = r12.l
            r15 = r15[r14]
            java.lang.Object r3 = r12.a(r13, r15, r3, r0)
            int r14 = r14 + 1
            goto L_0x003f
        L_0x004e:
            if (r3 == 0) goto L_0x0053
            r0.b(r13, r3)
        L_0x0053:
            return
        L_0x0054:
            boolean r6 = r12.h     // Catch:{ all -> 0x05b7 }
            if (r6 != 0) goto L_0x005a
            r5 = r2
            goto L_0x0060
        L_0x005a:
            com.google.android.gms.internal.measurement.gt r6 = r12.g     // Catch:{ all -> 0x05b7 }
            java.lang.Object r5 = r1.a(r15, r6, r5)     // Catch:{ all -> 0x05b7 }
        L_0x0060:
            if (r5 == 0) goto L_0x006d
            if (r4 != 0) goto L_0x0068
            com.google.android.gms.internal.measurement.fh r4 = r1.b(r13)     // Catch:{ all -> 0x05b7 }
        L_0x0068:
            java.lang.Object r3 = r1.a()     // Catch:{ all -> 0x05b7 }
            goto L_0x0009
        L_0x006d:
            if (r3 != 0) goto L_0x0073
            java.lang.Object r3 = r0.c(r13)     // Catch:{ all -> 0x05b7 }
        L_0x0073:
            boolean r5 = r0.a(r3, r14)     // Catch:{ all -> 0x05b7 }
            if (r5 != 0) goto L_0x0009
            int r14 = r12.m
        L_0x007b:
            int r15 = r12.n
            if (r14 >= r15) goto L_0x008a
            int[] r15 = r12.l
            r15 = r15[r14]
            java.lang.Object r3 = r12.a(r13, r15, r3, r0)
            int r14 = r14 + 1
            goto L_0x007b
        L_0x008a:
            if (r3 == 0) goto L_0x008f
            r0.b(r13, r3)
        L_0x008f:
            return
        L_0x0090:
            int r6 = r12.c(r7)     // Catch:{ all -> 0x05b7 }
            r8 = 267386880(0xff00000, float:2.3665827E-29)
            r8 = r8 & r6
            int r8 = r8 >>> 20
            r9 = 1048575(0xfffff, float:1.469367E-39)
            switch(r8) {
                case 0: goto L_0x0568;
                case 1: goto L_0x0559;
                case 2: goto L_0x054a;
                case 3: goto L_0x053b;
                case 4: goto L_0x052c;
                case 5: goto L_0x051d;
                case 6: goto L_0x050e;
                case 7: goto L_0x04ff;
                case 8: goto L_0x04f7;
                case 9: goto L_0x04c6;
                case 10: goto L_0x04b7;
                case 11: goto L_0x04a8;
                case 12: goto L_0x0486;
                case 13: goto L_0x0477;
                case 14: goto L_0x0468;
                case 15: goto L_0x0459;
                case 16: goto L_0x044a;
                case 17: goto L_0x0419;
                case 18: goto L_0x040c;
                case 19: goto L_0x03ff;
                case 20: goto L_0x03f2;
                case 21: goto L_0x03e5;
                case 22: goto L_0x03d8;
                case 23: goto L_0x03cb;
                case 24: goto L_0x03be;
                case 25: goto L_0x03b1;
                case 26: goto L_0x0391;
                case 27: goto L_0x0380;
                case 28: goto L_0x0373;
                case 29: goto L_0x0366;
                case 30: goto L_0x0351;
                case 31: goto L_0x0344;
                case 32: goto L_0x0337;
                case 33: goto L_0x032a;
                case 34: goto L_0x031d;
                case 35: goto L_0x0310;
                case 36: goto L_0x0303;
                case 37: goto L_0x02f6;
                case 38: goto L_0x02e9;
                case 39: goto L_0x02dc;
                case 40: goto L_0x02cf;
                case 41: goto L_0x02c2;
                case 42: goto L_0x02b5;
                case 43: goto L_0x02a8;
                case 44: goto L_0x0293;
                case 45: goto L_0x0286;
                case 46: goto L_0x0279;
                case 47: goto L_0x026c;
                case 48: goto L_0x025f;
                case 49: goto L_0x024d;
                case 50: goto L_0x020f;
                case 51: goto L_0x01fd;
                case 52: goto L_0x01eb;
                case 53: goto L_0x01d9;
                case 54: goto L_0x01c7;
                case 55: goto L_0x01b5;
                case 56: goto L_0x01a3;
                case 57: goto L_0x0191;
                case 58: goto L_0x017f;
                case 59: goto L_0x0177;
                case 60: goto L_0x0146;
                case 61: goto L_0x0138;
                case 62: goto L_0x0126;
                case 63: goto L_0x0101;
                case 64: goto L_0x00ef;
                case 65: goto L_0x00dd;
                case 66: goto L_0x00cb;
                case 67: goto L_0x00b9;
                case 68: goto L_0x00a7;
                default: goto L_0x009f;
            }
        L_0x009f:
            if (r3 != 0) goto L_0x0577
            java.lang.Object r3 = r0.a()     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0577
        L_0x00a7:
            r6 = r6 & r9
            long r8 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.hj r6 = r12.a(r7)     // Catch:{ zzuw -> 0x0594 }
            java.lang.Object r6 = r14.b(r6, r15)     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r8, r6)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r5, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x00b9:
            r6 = r6 & r9
            long r8 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            long r10 = r14.t()     // Catch:{ zzuw -> 0x0594 }
            java.lang.Long r6 = java.lang.Long.valueOf(r10)     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r8, r6)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r5, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x00cb:
            r6 = r6 & r9
            long r8 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            int r6 = r14.s()     // Catch:{ zzuw -> 0x0594 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r8, r6)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r5, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x00dd:
            r6 = r6 & r9
            long r8 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            long r10 = r14.r()     // Catch:{ zzuw -> 0x0594 }
            java.lang.Long r6 = java.lang.Long.valueOf(r10)     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r8, r6)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r5, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x00ef:
            r6 = r6 & r9
            long r8 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            int r6 = r14.q()     // Catch:{ zzuw -> 0x0594 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r8, r6)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r5, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x0101:
            int r8 = r14.p()     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.fv r10 = r12.b(r7)     // Catch:{ zzuw -> 0x0594 }
            if (r10 == 0) goto L_0x0118
            boolean r10 = r10.a(r8)     // Catch:{ zzuw -> 0x0594 }
            if (r10 == 0) goto L_0x0112
            goto L_0x0118
        L_0x0112:
            java.lang.Object r3 = com.google.android.gms.internal.measurement.hl.a(r5, r8, r3, r0)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x0118:
            r6 = r6 & r9
            long r9 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r8)     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r9, r6)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r5, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x0126:
            r6 = r6 & r9
            long r8 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            int r6 = r14.o()     // Catch:{ zzuw -> 0x0594 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r8, r6)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r5, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x0138:
            r6 = r6 & r9
            long r8 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ej r6 = r14.n()     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r8, r6)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r5, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x0146:
            boolean r8 = r12.a(r13, r5, r7)     // Catch:{ zzuw -> 0x0594 }
            if (r8 == 0) goto L_0x0162
            r6 = r6 & r9
            long r8 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.lang.Object r6 = com.google.android.gms.internal.measurement.ih.f(r13, r8)     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.hj r10 = r12.a(r7)     // Catch:{ zzuw -> 0x0594 }
            java.lang.Object r10 = r14.a(r10, r15)     // Catch:{ zzuw -> 0x0594 }
            java.lang.Object r6 = com.google.android.gms.internal.measurement.fs.a(r6, r10)     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r8, r6)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0172
        L_0x0162:
            r6 = r6 & r9
            long r8 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.hj r6 = r12.a(r7)     // Catch:{ zzuw -> 0x0594 }
            java.lang.Object r6 = r14.a(r6, r15)     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r8, r6)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r7)     // Catch:{ zzuw -> 0x0594 }
        L_0x0172:
            r12.b(r13, r5, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x0177:
            r12.a(r13, r6, r14)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r5, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x017f:
            r6 = r6 & r9
            long r8 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            boolean r6 = r14.k()     // Catch:{ zzuw -> 0x0594 }
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r6)     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r8, r6)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r5, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x0191:
            r6 = r6 & r9
            long r8 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            int r6 = r14.j()     // Catch:{ zzuw -> 0x0594 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r8, r6)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r5, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x01a3:
            r6 = r6 & r9
            long r8 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            long r10 = r14.i()     // Catch:{ zzuw -> 0x0594 }
            java.lang.Long r6 = java.lang.Long.valueOf(r10)     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r8, r6)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r5, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x01b5:
            r6 = r6 & r9
            long r8 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            int r6 = r14.h()     // Catch:{ zzuw -> 0x0594 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r8, r6)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r5, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x01c7:
            r6 = r6 & r9
            long r8 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            long r10 = r14.f()     // Catch:{ zzuw -> 0x0594 }
            java.lang.Long r6 = java.lang.Long.valueOf(r10)     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r8, r6)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r5, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x01d9:
            r6 = r6 & r9
            long r8 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            long r10 = r14.g()     // Catch:{ zzuw -> 0x0594 }
            java.lang.Long r6 = java.lang.Long.valueOf(r10)     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r8, r6)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r5, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x01eb:
            r6 = r6 & r9
            long r8 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            float r6 = r14.e()     // Catch:{ zzuw -> 0x0594 }
            java.lang.Float r6 = java.lang.Float.valueOf(r6)     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r8, r6)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r5, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x01fd:
            r6 = r6 & r9
            long r8 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            double r10 = r14.d()     // Catch:{ zzuw -> 0x0594 }
            java.lang.Double r6 = java.lang.Double.valueOf(r10)     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r8, r6)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r5, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x020f:
            int r5 = r12.c(r7)     // Catch:{ zzuw -> 0x0594 }
            r5 = r5 & r9
            long r5 = (long) r5     // Catch:{ zzuw -> 0x0594 }
            java.lang.Object r7 = com.google.android.gms.internal.measurement.ih.f(r13, r5)     // Catch:{ zzuw -> 0x0594 }
            if (r7 != 0) goto L_0x0225
            com.google.android.gms.internal.measurement.go r7 = r12.s     // Catch:{ zzuw -> 0x0594 }
            java.lang.Object r7 = r7.a()     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r5, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x023c
        L_0x0225:
            com.google.android.gms.internal.measurement.go r8 = r12.s     // Catch:{ zzuw -> 0x0594 }
            boolean r8 = r8.c(r7)     // Catch:{ zzuw -> 0x0594 }
            if (r8 == 0) goto L_0x023c
            com.google.android.gms.internal.measurement.go r8 = r12.s     // Catch:{ zzuw -> 0x0594 }
            java.lang.Object r8 = r8.a()     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.go r9 = r12.s     // Catch:{ zzuw -> 0x0594 }
            r9.a(r8, r7)     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r5, r8)     // Catch:{ zzuw -> 0x0594 }
            r7 = r8
        L_0x023c:
            com.google.android.gms.internal.measurement.go r5 = r12.s     // Catch:{ zzuw -> 0x0594 }
            java.util.Map r5 = r5.a(r7)     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.go r6 = r12.s     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.gm r6 = r6.b()     // Catch:{ zzuw -> 0x0594 }
            r14.a(r5, r6, r15)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x024d:
            r5 = r6 & r9
            long r5 = (long) r5     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.hj r7 = r12.a(r7)     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ge r8 = r12.p     // Catch:{ zzuw -> 0x0594 }
            java.util.List r5 = r8.a(r13, r5)     // Catch:{ zzuw -> 0x0594 }
            r14.b(r5, r7, r15)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x025f:
            com.google.android.gms.internal.measurement.ge r5 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r6 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r5 = r5.a(r13, r6)     // Catch:{ zzuw -> 0x0594 }
            r14.q(r5)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x026c:
            com.google.android.gms.internal.measurement.ge r5 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r6 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r5 = r5.a(r13, r6)     // Catch:{ zzuw -> 0x0594 }
            r14.p(r5)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x0279:
            com.google.android.gms.internal.measurement.ge r5 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r6 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r5 = r5.a(r13, r6)     // Catch:{ zzuw -> 0x0594 }
            r14.o(r5)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x0286:
            com.google.android.gms.internal.measurement.ge r5 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r6 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r5 = r5.a(r13, r6)     // Catch:{ zzuw -> 0x0594 }
            r14.n(r5)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x0293:
            com.google.android.gms.internal.measurement.ge r8 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r9 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r6 = r8.a(r13, r9)     // Catch:{ zzuw -> 0x0594 }
            r14.m(r6)     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.fv r7 = r12.b(r7)     // Catch:{ zzuw -> 0x0594 }
            java.lang.Object r3 = com.google.android.gms.internal.measurement.hl.a(r5, r6, r7, r3, r0)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x02a8:
            com.google.android.gms.internal.measurement.ge r5 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r6 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r5 = r5.a(r13, r6)     // Catch:{ zzuw -> 0x0594 }
            r14.l(r5)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x02b5:
            com.google.android.gms.internal.measurement.ge r5 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r6 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r5 = r5.a(r13, r6)     // Catch:{ zzuw -> 0x0594 }
            r14.h(r5)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x02c2:
            com.google.android.gms.internal.measurement.ge r5 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r6 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r5 = r5.a(r13, r6)     // Catch:{ zzuw -> 0x0594 }
            r14.g(r5)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x02cf:
            com.google.android.gms.internal.measurement.ge r5 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r6 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r5 = r5.a(r13, r6)     // Catch:{ zzuw -> 0x0594 }
            r14.f(r5)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x02dc:
            com.google.android.gms.internal.measurement.ge r5 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r6 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r5 = r5.a(r13, r6)     // Catch:{ zzuw -> 0x0594 }
            r14.e(r5)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x02e9:
            com.google.android.gms.internal.measurement.ge r5 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r6 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r5 = r5.a(r13, r6)     // Catch:{ zzuw -> 0x0594 }
            r14.c(r5)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x02f6:
            com.google.android.gms.internal.measurement.ge r5 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r6 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r5 = r5.a(r13, r6)     // Catch:{ zzuw -> 0x0594 }
            r14.d(r5)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x0303:
            com.google.android.gms.internal.measurement.ge r5 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r6 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r5 = r5.a(r13, r6)     // Catch:{ zzuw -> 0x0594 }
            r14.b(r5)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x0310:
            com.google.android.gms.internal.measurement.ge r5 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r6 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r5 = r5.a(r13, r6)     // Catch:{ zzuw -> 0x0594 }
            r14.a(r5)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x031d:
            com.google.android.gms.internal.measurement.ge r5 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r6 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r5 = r5.a(r13, r6)     // Catch:{ zzuw -> 0x0594 }
            r14.q(r5)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x032a:
            com.google.android.gms.internal.measurement.ge r5 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r6 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r5 = r5.a(r13, r6)     // Catch:{ zzuw -> 0x0594 }
            r14.p(r5)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x0337:
            com.google.android.gms.internal.measurement.ge r5 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r6 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r5 = r5.a(r13, r6)     // Catch:{ zzuw -> 0x0594 }
            r14.o(r5)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x0344:
            com.google.android.gms.internal.measurement.ge r5 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r6 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r5 = r5.a(r13, r6)     // Catch:{ zzuw -> 0x0594 }
            r14.n(r5)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x0351:
            com.google.android.gms.internal.measurement.ge r8 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r9 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r6 = r8.a(r13, r9)     // Catch:{ zzuw -> 0x0594 }
            r14.m(r6)     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.fv r7 = r12.b(r7)     // Catch:{ zzuw -> 0x0594 }
            java.lang.Object r3 = com.google.android.gms.internal.measurement.hl.a(r5, r6, r7, r3, r0)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x0366:
            com.google.android.gms.internal.measurement.ge r5 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r6 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r5 = r5.a(r13, r6)     // Catch:{ zzuw -> 0x0594 }
            r14.l(r5)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x0373:
            com.google.android.gms.internal.measurement.ge r5 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r6 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r5 = r5.a(r13, r6)     // Catch:{ zzuw -> 0x0594 }
            r14.k(r5)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x0380:
            com.google.android.gms.internal.measurement.hj r5 = r12.a(r7)     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r6 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ge r8 = r12.p     // Catch:{ zzuw -> 0x0594 }
            java.util.List r6 = r8.a(r13, r6)     // Catch:{ zzuw -> 0x0594 }
            r14.a(r6, r5, r15)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x0391:
            boolean r5 = e(r6)     // Catch:{ zzuw -> 0x0594 }
            if (r5 == 0) goto L_0x03a4
            com.google.android.gms.internal.measurement.ge r5 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r6 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r5 = r5.a(r13, r6)     // Catch:{ zzuw -> 0x0594 }
            r14.j(r5)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x03a4:
            com.google.android.gms.internal.measurement.ge r5 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r6 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r5 = r5.a(r13, r6)     // Catch:{ zzuw -> 0x0594 }
            r14.i(r5)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x03b1:
            com.google.android.gms.internal.measurement.ge r5 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r6 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r5 = r5.a(r13, r6)     // Catch:{ zzuw -> 0x0594 }
            r14.h(r5)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x03be:
            com.google.android.gms.internal.measurement.ge r5 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r6 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r5 = r5.a(r13, r6)     // Catch:{ zzuw -> 0x0594 }
            r14.g(r5)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x03cb:
            com.google.android.gms.internal.measurement.ge r5 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r6 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r5 = r5.a(r13, r6)     // Catch:{ zzuw -> 0x0594 }
            r14.f(r5)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x03d8:
            com.google.android.gms.internal.measurement.ge r5 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r6 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r5 = r5.a(r13, r6)     // Catch:{ zzuw -> 0x0594 }
            r14.e(r5)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x03e5:
            com.google.android.gms.internal.measurement.ge r5 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r6 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r5 = r5.a(r13, r6)     // Catch:{ zzuw -> 0x0594 }
            r14.c(r5)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x03f2:
            com.google.android.gms.internal.measurement.ge r5 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r6 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r5 = r5.a(r13, r6)     // Catch:{ zzuw -> 0x0594 }
            r14.d(r5)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x03ff:
            com.google.android.gms.internal.measurement.ge r5 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r6 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r5 = r5.a(r13, r6)     // Catch:{ zzuw -> 0x0594 }
            r14.b(r5)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x040c:
            com.google.android.gms.internal.measurement.ge r5 = r12.p     // Catch:{ zzuw -> 0x0594 }
            r6 = r6 & r9
            long r6 = (long) r6     // Catch:{ zzuw -> 0x0594 }
            java.util.List r5 = r5.a(r13, r6)     // Catch:{ zzuw -> 0x0594 }
            r14.a(r5)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x0419:
            boolean r5 = r12.a(r13, r7)     // Catch:{ zzuw -> 0x0594 }
            if (r5 == 0) goto L_0x0437
            r5 = r6 & r9
            long r5 = (long) r5     // Catch:{ zzuw -> 0x0594 }
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r13, r5)     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.hj r7 = r12.a(r7)     // Catch:{ zzuw -> 0x0594 }
            java.lang.Object r7 = r14.b(r7, r15)     // Catch:{ zzuw -> 0x0594 }
            java.lang.Object r7 = com.google.android.gms.internal.measurement.fs.a(r8, r7)     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r5, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x0437:
            r5 = r6 & r9
            long r5 = (long) r5     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.hj r8 = r12.a(r7)     // Catch:{ zzuw -> 0x0594 }
            java.lang.Object r8 = r14.b(r8, r15)     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r5, r8)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x044a:
            r5 = r6 & r9
            long r5 = (long) r5     // Catch:{ zzuw -> 0x0594 }
            long r8 = r14.t()     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r5, r8)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x0459:
            r5 = r6 & r9
            long r5 = (long) r5     // Catch:{ zzuw -> 0x0594 }
            int r8 = r14.s()     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r5, r8)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x0468:
            r5 = r6 & r9
            long r5 = (long) r5     // Catch:{ zzuw -> 0x0594 }
            long r8 = r14.r()     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r5, r8)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x0477:
            r5 = r6 & r9
            long r5 = (long) r5     // Catch:{ zzuw -> 0x0594 }
            int r8 = r14.q()     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r5, r8)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x0486:
            int r8 = r14.p()     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.fv r10 = r12.b(r7)     // Catch:{ zzuw -> 0x0594 }
            if (r10 == 0) goto L_0x049d
            boolean r10 = r10.a(r8)     // Catch:{ zzuw -> 0x0594 }
            if (r10 == 0) goto L_0x0497
            goto L_0x049d
        L_0x0497:
            java.lang.Object r3 = com.google.android.gms.internal.measurement.hl.a(r5, r8, r3, r0)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x049d:
            r5 = r6 & r9
            long r5 = (long) r5     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r5, r8)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x04a8:
            r5 = r6 & r9
            long r5 = (long) r5     // Catch:{ zzuw -> 0x0594 }
            int r8 = r14.o()     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r5, r8)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x04b7:
            r5 = r6 & r9
            long r5 = (long) r5     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ej r8 = r14.n()     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r5, r8)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x04c6:
            boolean r5 = r12.a(r13, r7)     // Catch:{ zzuw -> 0x0594 }
            if (r5 == 0) goto L_0x04e4
            r5 = r6 & r9
            long r5 = (long) r5     // Catch:{ zzuw -> 0x0594 }
            java.lang.Object r8 = com.google.android.gms.internal.measurement.ih.f(r13, r5)     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.hj r7 = r12.a(r7)     // Catch:{ zzuw -> 0x0594 }
            java.lang.Object r7 = r14.a(r7, r15)     // Catch:{ zzuw -> 0x0594 }
            java.lang.Object r7 = com.google.android.gms.internal.measurement.fs.a(r8, r7)     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r5, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x04e4:
            r5 = r6 & r9
            long r5 = (long) r5     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.hj r8 = r12.a(r7)     // Catch:{ zzuw -> 0x0594 }
            java.lang.Object r8 = r14.a(r8, r15)     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r5, r8)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x04f7:
            r12.a(r13, r6, r14)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x04ff:
            r5 = r6 & r9
            long r5 = (long) r5     // Catch:{ zzuw -> 0x0594 }
            boolean r8 = r14.k()     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r5, r8)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x050e:
            r5 = r6 & r9
            long r5 = (long) r5     // Catch:{ zzuw -> 0x0594 }
            int r8 = r14.j()     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r5, r8)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x051d:
            r5 = r6 & r9
            long r5 = (long) r5     // Catch:{ zzuw -> 0x0594 }
            long r8 = r14.i()     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r5, r8)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x052c:
            r5 = r6 & r9
            long r5 = (long) r5     // Catch:{ zzuw -> 0x0594 }
            int r8 = r14.h()     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r5, r8)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x053b:
            r5 = r6 & r9
            long r5 = (long) r5     // Catch:{ zzuw -> 0x0594 }
            long r8 = r14.f()     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r5, r8)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x054a:
            r5 = r6 & r9
            long r5 = (long) r5     // Catch:{ zzuw -> 0x0594 }
            long r8 = r14.g()     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r5, r8)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x0559:
            r5 = r6 & r9
            long r5 = (long) r5     // Catch:{ zzuw -> 0x0594 }
            float r8 = r14.e()     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r5, r8)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x0568:
            r5 = r6 & r9
            long r5 = (long) r5     // Catch:{ zzuw -> 0x0594 }
            double r8 = r14.d()     // Catch:{ zzuw -> 0x0594 }
            com.google.android.gms.internal.measurement.ih.a(r13, r5, r8)     // Catch:{ zzuw -> 0x0594 }
            r12.b(r13, r7)     // Catch:{ zzuw -> 0x0594 }
            goto L_0x0009
        L_0x0577:
            boolean r5 = r0.a(r3, r14)     // Catch:{ zzuw -> 0x0594 }
            if (r5 != 0) goto L_0x0009
            int r14 = r12.m
        L_0x057f:
            int r15 = r12.n
            if (r14 >= r15) goto L_0x058e
            int[] r15 = r12.l
            r15 = r15[r14]
            java.lang.Object r3 = r12.a(r13, r15, r3, r0)
            int r14 = r14 + 1
            goto L_0x057f
        L_0x058e:
            if (r3 == 0) goto L_0x0593
            r0.b(r13, r3)
        L_0x0593:
            return
        L_0x0594:
            if (r3 != 0) goto L_0x059a
            java.lang.Object r3 = r0.c(r13)     // Catch:{ all -> 0x05b7 }
        L_0x059a:
            boolean r5 = r0.a(r3, r14)     // Catch:{ all -> 0x05b7 }
            if (r5 != 0) goto L_0x0009
            int r14 = r12.m
        L_0x05a2:
            int r15 = r12.n
            if (r14 >= r15) goto L_0x05b1
            int[] r15 = r12.l
            r15 = r15[r14]
            java.lang.Object r3 = r12.a(r13, r15, r3, r0)
            int r14 = r14 + 1
            goto L_0x05a2
        L_0x05b1:
            if (r3 == 0) goto L_0x05b6
            r0.b(r13, r3)
        L_0x05b6:
            return
        L_0x05b7:
            r14 = move-exception
            int r15 = r12.m
        L_0x05ba:
            int r1 = r12.n
            if (r15 >= r1) goto L_0x05c9
            int[] r1 = r12.l
            r1 = r1[r15]
            java.lang.Object r3 = r12.a(r13, r1, r3, r0)
            int r15 = r15 + 1
            goto L_0x05ba
        L_0x05c9:
            if (r3 == 0) goto L_0x05ce
            r0.b(r13, r3)
        L_0x05ce:
            throw r14
        L_0x05cf:
            java.lang.NullPointerException r13 = new java.lang.NullPointerException
            r13.<init>()
            goto L_0x05d6
        L_0x05d5:
            throw r13
        L_0x05d6:
            goto L_0x05d5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.gx.a(java.lang.Object, com.google.android.gms.internal.measurement.hi, com.google.android.gms.internal.measurement.fd):void");
    }

    private final hj a(int i2) {
        int i3 = (i2 / 3) << 1;
        hj hjVar = (hj) this.d[i3];
        if (hjVar != null) {
            return hjVar;
        }
        hj a2 = hf.a().a((Class) this.d[i3 + 1]);
        this.d[i3] = a2;
        return a2;
    }

    private final fv b(int i2) {
        return (fv) this.d[((i2 / 3) << 1) + 1];
    }

    public final void c(T t) {
        int i2;
        int i3 = this.m;
        while (true) {
            i2 = this.n;
            if (i3 >= i2) {
                break;
            }
            long c2 = (long) (c(this.l[i3]) & 1048575);
            Object f2 = ih.f(t, c2);
            if (f2 != null) {
                ih.a(t, c2, this.s.d(f2));
            }
            i3++;
        }
        int length = this.l.length;
        while (i2 < length) {
            this.p.b(t, (long) this.l[i2]);
            i2++;
        }
        this.q.d(t);
        if (this.h) {
            this.r.c(t);
        }
    }

    private final <UT, UB> UB a(Object obj, int i2, UB ub, ib<UT, UB> ibVar) {
        fv b2;
        int i3 = this.c[i2];
        Object f2 = ih.f(obj, (long) (c(i2) & 1048575));
        if (f2 == null || (b2 = b(i2)) == null) {
            return ub;
        }
        return a(i2, i3, this.s.a(f2), b2, ub, ibVar);
    }

    private final <K, V, UT, UB> UB a(int i2, int i3, Map map, fv fvVar, Object obj, ib ibVar) {
        gm<?, ?> b2 = this.s.b();
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            if (!fvVar.a(((Integer) entry.getValue()).intValue())) {
                if (obj == null) {
                    obj = ibVar.a();
                }
                er d2 = ej.d(gl.a(b2, entry.getKey(), entry.getValue()));
                try {
                    gl.a(d2.f2188a, b2, entry.getKey(), entry.getValue());
                    ibVar.a(obj, i3, d2.a());
                    it.remove();
                } catch (IOException e2) {
                    throw new RuntimeException(e2);
                }
            }
        }
        return obj;
    }

    public final boolean d(T t) {
        int i2;
        int i3 = 0;
        int i4 = -1;
        int i5 = 0;
        while (true) {
            boolean z = true;
            if (i3 >= this.m) {
                return !this.h || this.r.a(t).d();
            }
            int i6 = this.l[i3];
            int i7 = this.c[i6];
            int c2 = c(i6);
            if (!this.j) {
                int i8 = this.c[i6 + 2];
                int i9 = i8 & 1048575;
                i2 = 1 << (i8 >>> 20);
                if (i9 != i4) {
                    i5 = f2251b.getInt(t, (long) i9);
                    i4 = i9;
                }
            } else {
                i2 = 0;
            }
            if (((268435456 & c2) != 0) && !a(t, i6, i5, i2)) {
                return false;
            }
            int i10 = (267386880 & c2) >>> 20;
            if (i10 != 9 && i10 != 17) {
                if (i10 != 27) {
                    if (i10 == 60 || i10 == 68) {
                        if (a(t, i7, i6) && !a(t, c2, a(i6))) {
                            return false;
                        }
                    } else if (i10 != 49) {
                        if (i10 != 50) {
                            continue;
                        } else {
                            Map<?, ?> b2 = this.s.b(ih.f(t, (long) (c2 & 1048575)));
                            if (!b2.isEmpty() && this.s.b().c.s == iu.MESSAGE) {
                                hj hjVar = null;
                                Iterator<?> it = b2.values().iterator();
                                while (true) {
                                    if (!it.hasNext()) {
                                        break;
                                    }
                                    Object next = it.next();
                                    if (hjVar == null) {
                                        hjVar = hf.a().a((Class) next.getClass());
                                    }
                                    if (!hjVar.d(next)) {
                                        z = false;
                                        break;
                                    }
                                }
                            }
                            if (!z) {
                                return false;
                            }
                        }
                    }
                }
                List list = (List) ih.f(t, (long) (c2 & 1048575));
                if (!list.isEmpty()) {
                    hj a2 = a(i6);
                    int i11 = 0;
                    while (true) {
                        if (i11 >= list.size()) {
                            break;
                        } else if (!a2.d(list.get(i11))) {
                            z = false;
                            break;
                        } else {
                            i11++;
                        }
                    }
                }
                if (!z) {
                    return false;
                }
            } else if (a(t, i6, i5, i2) && !a(t, c2, a(i6))) {
                return false;
            }
            i3++;
        }
    }

    private static boolean a(Object obj, int i2, hj hjVar) {
        return hjVar.d(ih.f(obj, (long) (i2 & 1048575)));
    }

    private static void a(int i2, Object obj, iv ivVar) throws IOException {
        if (obj instanceof String) {
            ivVar.a(i2, (String) obj);
        } else {
            ivVar.a(i2, (ej) obj);
        }
    }

    private final void a(Object obj, int i2, hi hiVar) throws IOException {
        if (e(i2)) {
            ih.a(obj, (long) (i2 & 1048575), hiVar.m());
        } else if (this.i) {
            ih.a(obj, (long) (i2 & 1048575), hiVar.l());
        } else {
            ih.a(obj, (long) (i2 & 1048575), hiVar.n());
        }
    }

    private final int c(int i2) {
        return this.c[i2 + 1];
    }

    private final int d(int i2) {
        return this.c[i2 + 2];
    }

    private static <T> double b(T t, long j2) {
        return ((Double) ih.f(t, j2)).doubleValue();
    }

    private static <T> float c(T t, long j2) {
        return ((Float) ih.f(t, j2)).floatValue();
    }

    private static <T> int d(T t, long j2) {
        return ((Integer) ih.f(t, j2)).intValue();
    }

    private static <T> long e(T t, long j2) {
        return ((Long) ih.f(t, j2)).longValue();
    }

    private static <T> boolean f(T t, long j2) {
        return ((Boolean) ih.f(t, j2)).booleanValue();
    }

    private final boolean c(T t, T t2, int i2) {
        return a(t, i2) == a(t2, i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.gx.a(java.lang.Object, int):boolean
     arg types: [T, int]
     candidates:
      com.google.android.gms.internal.measurement.gx.a(com.google.android.gms.internal.measurement.ib, java.lang.Object):int
      com.google.android.gms.internal.measurement.gx.a(java.lang.Class<?>, java.lang.String):java.lang.reflect.Field
      com.google.android.gms.internal.measurement.gx.a(java.lang.Object, long):java.util.List<E>
      com.google.android.gms.internal.measurement.gx.a(java.lang.Object, com.google.android.gms.internal.measurement.iv):void
      com.google.android.gms.internal.measurement.gx.a(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.hj.a(java.lang.Object, com.google.android.gms.internal.measurement.iv):void
      com.google.android.gms.internal.measurement.hj.a(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.gx.a(java.lang.Object, int):boolean */
    private final boolean a(T t, int i2, int i3, int i4) {
        if (this.j) {
            return a((Object) t, i2);
        }
        return (i3 & i4) != 0;
    }

    private final boolean a(T t, int i2) {
        if (this.j) {
            int c2 = c(i2);
            long j2 = (long) (c2 & 1048575);
            switch ((c2 & 267386880) >>> 20) {
                case 0:
                    return ih.e(t, j2) != 0.0d;
                case 1:
                    return ih.d(t, j2) != 0.0f;
                case 2:
                    return ih.b(t, j2) != 0;
                case 3:
                    return ih.b(t, j2) != 0;
                case 4:
                    return ih.a(t, j2) != 0;
                case 5:
                    return ih.b(t, j2) != 0;
                case 6:
                    return ih.a(t, j2) != 0;
                case 7:
                    return ih.c(t, j2);
                case 8:
                    Object f2 = ih.f(t, j2);
                    if (f2 instanceof String) {
                        return !((String) f2).isEmpty();
                    }
                    if (f2 instanceof ej) {
                        return !ej.f2184a.equals(f2);
                    }
                    throw new IllegalArgumentException();
                case 9:
                    return ih.f(t, j2) != null;
                case 10:
                    return !ej.f2184a.equals(ih.f(t, j2));
                case 11:
                    return ih.a(t, j2) != 0;
                case 12:
                    return ih.a(t, j2) != 0;
                case 13:
                    return ih.a(t, j2) != 0;
                case 14:
                    return ih.b(t, j2) != 0;
                case 15:
                    return ih.a(t, j2) != 0;
                case 16:
                    return ih.b(t, j2) != 0;
                case 17:
                    return ih.f(t, j2) != null;
                default:
                    throw new IllegalArgumentException();
            }
        } else {
            int d2 = d(i2);
            return (ih.a(t, (long) (d2 & 1048575)) & (1 << (d2 >>> 20))) != 0;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, int):void
     arg types: [T, long, int]
     candidates:
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, float):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, long):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.measurement.ih.a(byte[], long, byte):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, int):void */
    private final void b(T t, int i2) {
        if (!this.j) {
            int d2 = d(i2);
            long j2 = (long) (d2 & 1048575);
            ih.a((Object) t, j2, ih.a(t, j2) | (1 << (d2 >>> 20)));
        }
    }

    private final boolean a(T t, int i2, int i3) {
        return ih.a(t, (long) (d(i3) & 1048575)) == i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, int):void
     arg types: [T, long, int]
     candidates:
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, float):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, long):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.measurement.ih.a(byte[], long, byte):void
      com.google.android.gms.internal.measurement.ih.a(java.lang.Object, long, int):void */
    private final void b(T t, int i2, int i3) {
        ih.a((Object) t, (long) (d(i3) & 1048575), i2);
    }
}
