package com.google.android.gms.internal.measurement;

import java.io.IOException;

public class zzuv extends IOException {

    /* renamed from: a  reason: collision with root package name */
    gt f2351a = null;

    public zzuv(String str) {
        super(str);
    }

    static zzuv a() {
        return new zzuv("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either that the input has been truncated or that an embedded message misreported its own length.");
    }

    static zzuv b() {
        return new zzuv("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
    }

    static zzuv c() {
        return new zzuv("CodedInputStream encountered a malformed varint.");
    }

    static zzuv d() {
        return new zzuv("Protocol message end-group tag did not match expected tag.");
    }

    static zzuw e() {
        return new zzuw("Protocol message tag had invalid wire type.");
    }

    static zzuv f() {
        return new zzuv("Protocol message had too many levels of nesting.  May be malicious.  Use CodedInputStream.setRecursionLimit() to increase the depth limit.");
    }

    static zzuv g() {
        return new zzuv("Failed to parse the message.");
    }

    static zzuv h() {
        return new zzuv("Protocol message had invalid UTF-8.");
    }
}
