package com.google.android.gms.games.multiplayer.turnbased;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.internal.e;
import com.google.android.gms.games.internal.zzd;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.ParticipantEntity;
import java.util.ArrayList;
import java.util.Arrays;

public final class TurnBasedMatchEntity extends zzd implements TurnBasedMatch {
    public static final Parcelable.Creator<TurnBasedMatchEntity> CREATOR = new b();

    /* renamed from: a  reason: collision with root package name */
    private final GameEntity f1833a;

    /* renamed from: b  reason: collision with root package name */
    private final String f1834b;
    private final String c;
    private final long d;
    private final String e;
    private final long f;
    private final String g;
    private final int h;
    private final int i;
    private final int j;
    private final byte[] k;
    private final ArrayList<ParticipantEntity> l;
    private final String m;
    private final byte[] n;
    private final int o;
    private final Bundle p;
    private final int q;
    private final boolean r;
    private final String s;
    private final String t;

    TurnBasedMatchEntity(GameEntity gameEntity, String str, String str2, long j2, String str3, long j3, String str4, int i2, int i3, int i4, byte[] bArr, ArrayList<ParticipantEntity> arrayList, String str5, byte[] bArr2, int i5, Bundle bundle, int i6, boolean z, String str6, String str7) {
        this.f1833a = gameEntity;
        this.f1834b = str;
        this.c = str2;
        this.d = j2;
        this.e = str3;
        this.f = j3;
        this.g = str4;
        this.h = i2;
        this.q = i6;
        this.i = i3;
        this.j = i4;
        this.k = bArr;
        this.l = arrayList;
        this.m = str5;
        this.n = bArr2;
        this.o = i5;
        this.p = bundle;
        this.r = z;
        this.s = str6;
        this.t = str7;
    }

    public TurnBasedMatchEntity(TurnBasedMatch turnBasedMatch) {
        this.f1833a = new GameEntity(turnBasedMatch.b());
        this.f1834b = turnBasedMatch.c();
        this.c = turnBasedMatch.d();
        this.d = turnBasedMatch.e();
        this.e = turnBasedMatch.k();
        this.f = turnBasedMatch.l();
        this.g = turnBasedMatch.m();
        this.h = turnBasedMatch.f();
        this.q = turnBasedMatch.g();
        this.i = turnBasedMatch.j();
        this.j = turnBasedMatch.o();
        this.m = turnBasedMatch.p();
        this.o = turnBasedMatch.r();
        this.p = turnBasedMatch.s();
        this.r = turnBasedMatch.u();
        this.s = turnBasedMatch.h();
        this.t = turnBasedMatch.v();
        byte[] n2 = turnBasedMatch.n();
        if (n2 == null) {
            this.k = null;
        } else {
            this.k = new byte[n2.length];
            System.arraycopy(n2, 0, this.k, 0, n2.length);
        }
        byte[] q2 = turnBasedMatch.q();
        if (q2 == null) {
            this.n = null;
        } else {
            this.n = new byte[q2.length];
            System.arraycopy(q2, 0, this.n, 0, q2.length);
        }
        ArrayList<Participant> i2 = turnBasedMatch.i();
        int size = i2.size();
        this.l = new ArrayList<>(size);
        for (int i3 = 0; i3 < size; i3++) {
            this.l.add((ParticipantEntity) i2.get(i3).a());
        }
    }

    static boolean a(TurnBasedMatch turnBasedMatch, Object obj) {
        if (!(obj instanceof TurnBasedMatch)) {
            return false;
        }
        if (turnBasedMatch == obj) {
            return true;
        }
        TurnBasedMatch turnBasedMatch2 = (TurnBasedMatch) obj;
        return j.a(turnBasedMatch2.b(), turnBasedMatch.b()) && j.a(turnBasedMatch2.c(), turnBasedMatch.c()) && j.a(turnBasedMatch2.d(), turnBasedMatch.d()) && j.a(Long.valueOf(turnBasedMatch2.e()), Long.valueOf(turnBasedMatch.e())) && j.a(turnBasedMatch2.k(), turnBasedMatch.k()) && j.a(Long.valueOf(turnBasedMatch2.l()), Long.valueOf(turnBasedMatch.l())) && j.a(turnBasedMatch2.m(), turnBasedMatch.m()) && j.a(Integer.valueOf(turnBasedMatch2.f()), Integer.valueOf(turnBasedMatch.f())) && j.a(Integer.valueOf(turnBasedMatch2.g()), Integer.valueOf(turnBasedMatch.g())) && j.a(turnBasedMatch2.h(), turnBasedMatch.h()) && j.a(Integer.valueOf(turnBasedMatch2.j()), Integer.valueOf(turnBasedMatch.j())) && j.a(Integer.valueOf(turnBasedMatch2.o()), Integer.valueOf(turnBasedMatch.o())) && j.a(turnBasedMatch2.i(), turnBasedMatch.i()) && j.a(turnBasedMatch2.p(), turnBasedMatch.p()) && j.a(Integer.valueOf(turnBasedMatch2.r()), Integer.valueOf(turnBasedMatch.r())) && e.a(turnBasedMatch2.s(), turnBasedMatch.s()) && j.a(Integer.valueOf(turnBasedMatch2.t()), Integer.valueOf(turnBasedMatch.t())) && j.a(Boolean.valueOf(turnBasedMatch2.u()), Boolean.valueOf(turnBasedMatch.u()));
    }

    static String b(TurnBasedMatch turnBasedMatch) {
        return j.a(turnBasedMatch).a("Game", turnBasedMatch.b()).a("MatchId", turnBasedMatch.c()).a("CreatorId", turnBasedMatch.d()).a("CreationTimestamp", Long.valueOf(turnBasedMatch.e())).a("LastUpdaterId", turnBasedMatch.k()).a("LastUpdatedTimestamp", Long.valueOf(turnBasedMatch.l())).a("PendingParticipantId", turnBasedMatch.m()).a("MatchStatus", Integer.valueOf(turnBasedMatch.f())).a("TurnStatus", Integer.valueOf(turnBasedMatch.g())).a("Description", turnBasedMatch.h()).a("Variant", Integer.valueOf(turnBasedMatch.j())).a("Data", turnBasedMatch.n()).a("Version", Integer.valueOf(turnBasedMatch.o())).a("Participants", turnBasedMatch.i()).a("RematchId", turnBasedMatch.p()).a("PreviousData", turnBasedMatch.q()).a("MatchNumber", Integer.valueOf(turnBasedMatch.r())).a("AutoMatchCriteria", turnBasedMatch.s()).a("AvailableAutoMatchSlots", Integer.valueOf(turnBasedMatch.t())).a("LocallyModified", Boolean.valueOf(turnBasedMatch.u())).a("DescriptionParticipantId", turnBasedMatch.v()).toString();
    }

    public final /* bridge */ /* synthetic */ Object a() {
        return this;
    }

    public final Game b() {
        return this.f1833a;
    }

    public final String c() {
        return this.f1834b;
    }

    public final String d() {
        return this.c;
    }

    public final long e() {
        return this.d;
    }

    public final boolean equals(Object obj) {
        return a(this, obj);
    }

    public final int f() {
        return this.h;
    }

    public final int g() {
        return this.q;
    }

    public final String h() {
        return this.s;
    }

    public final int hashCode() {
        return a(this);
    }

    public final ArrayList<Participant> i() {
        return new ArrayList<>(this.l);
    }

    public final int j() {
        return this.i;
    }

    public final String k() {
        return this.e;
    }

    public final long l() {
        return this.f;
    }

    public final String m() {
        return this.g;
    }

    public final byte[] n() {
        return this.k;
    }

    public final int o() {
        return this.j;
    }

    public final String p() {
        return this.m;
    }

    public final byte[] q() {
        return this.n;
    }

    public final int r() {
        return this.o;
    }

    public final Bundle s() {
        return this.p;
    }

    public final int t() {
        Bundle bundle = this.p;
        if (bundle == null) {
            return 0;
        }
        return bundle.getInt("max_automatch_players");
    }

    public final String toString() {
        return b(this);
    }

    public final boolean u() {
        return this.r;
    }

    public final String v() {
        return this.t;
    }

    static int a(TurnBasedMatch turnBasedMatch) {
        return Arrays.hashCode(new Object[]{turnBasedMatch.b(), turnBasedMatch.c(), turnBasedMatch.d(), Long.valueOf(turnBasedMatch.e()), turnBasedMatch.k(), Long.valueOf(turnBasedMatch.l()), turnBasedMatch.m(), Integer.valueOf(turnBasedMatch.f()), Integer.valueOf(turnBasedMatch.g()), turnBasedMatch.h(), Integer.valueOf(turnBasedMatch.j()), Integer.valueOf(turnBasedMatch.o()), turnBasedMatch.i(), turnBasedMatch.p(), Integer.valueOf(turnBasedMatch.r()), Integer.valueOf(e.a(turnBasedMatch.s())), Integer.valueOf(turnBasedMatch.t()), Boolean.valueOf(turnBasedMatch.u())});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.GameEntity, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
     arg types: [android.os.Parcel, int, byte[], int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
     arg types: [android.os.Parcel, int, android.os.Bundle, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void */
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = a.a(parcel, 20293);
        a.a(parcel, 1, (Parcelable) this.f1833a, i2, false);
        a.a(parcel, 2, this.f1834b, false);
        a.a(parcel, 3, this.c, false);
        a.a(parcel, 4, this.d);
        a.a(parcel, 5, this.e, false);
        a.a(parcel, 6, this.f);
        a.a(parcel, 7, this.g, false);
        a.b(parcel, 8, this.h);
        a.b(parcel, 10, this.i);
        a.b(parcel, 11, this.j);
        a.a(parcel, 12, this.k, false);
        a.b(parcel, 13, i(), false);
        a.a(parcel, 14, this.m, false);
        a.a(parcel, 15, this.n, false);
        a.b(parcel, 16, this.o);
        a.a(parcel, 17, this.p, false);
        a.b(parcel, 18, this.q);
        a.a(parcel, 19, this.r);
        a.a(parcel, 20, this.s, false);
        a.a(parcel, 21, this.t, false);
        a.b(parcel, a2);
    }
}
