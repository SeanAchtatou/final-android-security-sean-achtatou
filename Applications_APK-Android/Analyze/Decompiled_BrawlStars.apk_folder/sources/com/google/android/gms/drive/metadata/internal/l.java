package com.google.android.gms.drive.metadata.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import java.util.ArrayList;

public final class l implements Parcelable.Creator<ParentDriveIdSet> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new ParentDriveIdSet[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        ArrayList arrayList = null;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            if ((65535 & readInt) != 2) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                arrayList = SafeParcelReader.c(parcel, readInt, zzq.CREATOR);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new ParentDriveIdSet(arrayList);
    }
}
