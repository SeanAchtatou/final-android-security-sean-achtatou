package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;

public final class zzz extends zza {
    public static final Parcelable.Creator<zzz> CREATOR = new a();

    public final <F> F a(g<F> gVar) {
        return gVar.b();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        a.b(parcel, a.a(parcel, 20293));
    }
}
