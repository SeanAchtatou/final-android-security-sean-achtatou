package com.google.android.gms.common.data;

import android.net.Uri;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.common.internal.l;
import java.util.Arrays;

public class d {

    /* renamed from: a  reason: collision with root package name */
    protected final DataHolder f1532a;

    /* renamed from: b  reason: collision with root package name */
    protected int f1533b;
    private int c;

    public d(DataHolder dataHolder, int i) {
        this.f1532a = (DataHolder) l.a(dataHolder);
        l.a(i >= 0 && i < this.f1532a.f);
        this.f1533b = i;
        this.c = this.f1532a.a(this.f1533b);
    }

    /* access modifiers changed from: protected */
    public final int c_() {
        return this.f1533b;
    }

    public final boolean a_(String str) {
        return this.f1532a.a(str);
    }

    /* access modifiers changed from: protected */
    public final long b(String str) {
        return this.f1532a.a(str, this.f1533b, this.c);
    }

    /* access modifiers changed from: protected */
    public final int c(String str) {
        return this.f1532a.b(str, this.f1533b, this.c);
    }

    /* access modifiers changed from: protected */
    public final boolean d(String str) {
        return this.f1532a.d(str, this.f1533b, this.c);
    }

    /* access modifiers changed from: protected */
    public final String e(String str) {
        return this.f1532a.c(str, this.f1533b, this.c);
    }

    /* access modifiers changed from: protected */
    public final float f(String str) {
        DataHolder dataHolder = this.f1532a;
        int i = this.f1533b;
        int i2 = this.c;
        dataHolder.a(str, i);
        return dataHolder.c[i2].getFloat(i, dataHolder.f1526b.getInt(str));
    }

    /* access modifiers changed from: protected */
    public final byte[] g(String str) {
        DataHolder dataHolder = this.f1532a;
        int i = this.f1533b;
        int i2 = this.c;
        dataHolder.a(str, i);
        return dataHolder.c[i2].getBlob(i, dataHolder.f1526b.getInt(str));
    }

    /* access modifiers changed from: protected */
    public final Uri h(String str) {
        String c2 = this.f1532a.c(str, this.f1533b, this.c);
        if (c2 == null) {
            return null;
        }
        return Uri.parse(c2);
    }

    /* access modifiers changed from: protected */
    public final boolean i(String str) {
        return this.f1532a.e(str, this.f1533b, this.c);
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.f1533b), Integer.valueOf(this.c), this.f1532a});
    }

    public boolean equals(Object obj) {
        if (obj instanceof d) {
            d dVar = (d) obj;
            if (!j.a(Integer.valueOf(dVar.f1533b), Integer.valueOf(this.f1533b)) || !j.a(Integer.valueOf(dVar.c), Integer.valueOf(this.c)) || dVar.f1532a != this.f1532a) {
                return false;
            }
            return true;
        }
        return false;
    }
}
