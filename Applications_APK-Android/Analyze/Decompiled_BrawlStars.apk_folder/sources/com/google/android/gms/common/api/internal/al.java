package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.g;
import com.google.android.gms.common.api.internal.c;

public interface al {
    <A extends a.b, T extends c.a<? extends g, A>> T a(c.a aVar);

    void a();

    void a(int i);

    void a(Bundle bundle);

    void a(ConnectionResult connectionResult, a<?> aVar, boolean z);

    boolean b();

    void c();
}
