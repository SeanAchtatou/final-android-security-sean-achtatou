package com.google.android.gms.drive.metadata.internal;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.UserMetadata;
import java.util.Arrays;
import java.util.Collections;

public final class r extends k<UserMetadata> {
    public r(String str) {
        super(str, Arrays.asList(a(str, "permissionId"), a(str, "displayName"), a(str, "picture"), a(str, "isAuthenticatedUser"), a(str, "emailAddress")), Collections.emptyList(), 6000000);
    }

    private static String a(String str, String str2) {
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 1 + String.valueOf(str2).length());
        sb.append(str);
        sb.append(".");
        sb.append(str2);
        return sb.toString();
    }

    public final boolean b(DataHolder dataHolder, int i, int i2) {
        return dataHolder.a(a("permissionId")) && !dataHolder.e(a("permissionId"), i, i2);
    }

    public final /* synthetic */ Object c(DataHolder dataHolder, int i, int i2) {
        String c = dataHolder.c(a("permissionId"), i, i2);
        if (c == null) {
            return null;
        }
        String c2 = dataHolder.c(a("displayName"), i, i2);
        String c3 = dataHolder.c(a("picture"), i, i2);
        Boolean valueOf = Boolean.valueOf(dataHolder.d(a("isAuthenticatedUser"), i, i2));
        return new UserMetadata(c, c2, c3, valueOf.booleanValue(), dataHolder.c(a("emailAddress"), i, i2));
    }

    private final String a(String str) {
        return a(this.f1731b, str);
    }
}
