package com.google.android.gms.measurement.internal;

import java.util.concurrent.atomic.AtomicReference;

final class cb implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ AtomicReference f2456a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f2457b;
    private final /* synthetic */ String c;
    private final /* synthetic */ String d;
    private final /* synthetic */ boolean e;
    private final /* synthetic */ bv f;

    cb(bv bvVar, AtomicReference atomicReference, String str, String str2, String str3, boolean z) {
        this.f = bvVar;
        this.f2456a = atomicReference;
        this.f2457b = str;
        this.c = str2;
        this.d = str3;
        this.e = z;
    }

    public final void run() {
        this.f.r.i().a(this.f2456a, this.f2457b, this.c, this.d, this.e);
    }
}
