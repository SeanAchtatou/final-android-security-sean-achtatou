package com.google.android.gms.measurement.internal;

import android.content.SharedPreferences;
import com.google.android.gms.common.internal.l;

public final class ac {

    /* renamed from: a  reason: collision with root package name */
    final String f2364a;

    /* renamed from: b  reason: collision with root package name */
    final String f2365b;
    final long c;
    final /* synthetic */ z d;
    private final String e;

    private ac(z zVar, String str, long j) {
        this.d = zVar;
        l.a(str);
        l.b(j > 0);
        this.e = String.valueOf(str).concat(":start");
        this.f2364a = String.valueOf(str).concat(":count");
        this.f2365b = String.valueOf(str).concat(":value");
        this.c = j;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.d.c();
        long a2 = this.d.l().a();
        SharedPreferences.Editor edit = this.d.f().edit();
        edit.remove(this.f2364a);
        edit.remove(this.f2365b);
        edit.putLong(this.e, a2);
        edit.apply();
    }

    /* access modifiers changed from: package-private */
    public final long b() {
        return this.d.f().getLong(this.e, 0);
    }

    /* synthetic */ ac(z zVar, String str, long j, byte b2) {
        this(zVar, str, j);
    }
}
