package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class cj extends iz<cj> {
    private static volatile cj[] g;

    /* renamed from: a  reason: collision with root package name */
    public Integer f2116a = null;

    /* renamed from: b  reason: collision with root package name */
    public String f2117b = null;
    public ck[] c = ck.a();
    public cl d = null;
    public Boolean e = null;
    public Boolean f = null;
    private Boolean h = null;

    public static cj[] a() {
        if (g == null) {
            synchronized (jd.f2312b) {
                if (g == null) {
                    g = new cj[0];
                }
            }
        }
        return g;
    }

    public cj() {
        this.L = null;
        this.M = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof cj)) {
            return false;
        }
        cj cjVar = (cj) obj;
        Integer num = this.f2116a;
        if (num == null) {
            if (cjVar.f2116a != null) {
                return false;
            }
        } else if (!num.equals(cjVar.f2116a)) {
            return false;
        }
        String str = this.f2117b;
        if (str == null) {
            if (cjVar.f2117b != null) {
                return false;
            }
        } else if (!str.equals(cjVar.f2117b)) {
            return false;
        }
        if (!jd.a(this.c, cjVar.c)) {
            return false;
        }
        Boolean bool = this.h;
        if (bool == null) {
            if (cjVar.h != null) {
                return false;
            }
        } else if (!bool.equals(cjVar.h)) {
            return false;
        }
        cl clVar = this.d;
        if (clVar == null) {
            if (cjVar.d != null) {
                return false;
            }
        } else if (!clVar.equals(cjVar.d)) {
            return false;
        }
        Boolean bool2 = this.e;
        if (bool2 == null) {
            if (cjVar.e != null) {
                return false;
            }
        } else if (!bool2.equals(cjVar.e)) {
            return false;
        }
        Boolean bool3 = this.f;
        if (bool3 == null) {
            if (cjVar.f != null) {
                return false;
            }
        } else if (!bool3.equals(cjVar.f)) {
            return false;
        }
        if (this.L == null || this.L.a()) {
            return cjVar.L == null || cjVar.L.a();
        }
        return this.L.equals(cjVar.L);
    }

    public final int hashCode() {
        int i;
        int hashCode = (getClass().getName().hashCode() + 527) * 31;
        Integer num = this.f2116a;
        int i2 = 0;
        int hashCode2 = (hashCode + (num == null ? 0 : num.hashCode())) * 31;
        String str = this.f2117b;
        int hashCode3 = (((hashCode2 + (str == null ? 0 : str.hashCode())) * 31) + jd.a(this.c)) * 31;
        Boolean bool = this.h;
        int hashCode4 = hashCode3 + (bool == null ? 0 : bool.hashCode());
        cl clVar = this.d;
        int i3 = hashCode4 * 31;
        if (clVar == null) {
            i = 0;
        } else {
            i = clVar.hashCode();
        }
        int i4 = (i3 + i) * 31;
        Boolean bool2 = this.e;
        int hashCode5 = (i4 + (bool2 == null ? 0 : bool2.hashCode())) * 31;
        Boolean bool3 = this.f;
        int hashCode6 = (hashCode5 + (bool3 == null ? 0 : bool3.hashCode())) * 31;
        if (this.L != null && !this.L.a()) {
            i2 = this.L.hashCode();
        }
        return hashCode6 + i2;
    }

    public final void a(iy iyVar) throws IOException {
        Integer num = this.f2116a;
        if (num != null) {
            iyVar.a(1, num.intValue());
        }
        String str = this.f2117b;
        if (str != null) {
            iyVar.a(2, str);
        }
        ck[] ckVarArr = this.c;
        if (ckVarArr != null && ckVarArr.length > 0) {
            int i = 0;
            while (true) {
                ck[] ckVarArr2 = this.c;
                if (i >= ckVarArr2.length) {
                    break;
                }
                ck ckVar = ckVarArr2[i];
                if (ckVar != null) {
                    iyVar.a(3, ckVar);
                }
                i++;
            }
        }
        Boolean bool = this.h;
        if (bool != null) {
            iyVar.a(4, bool.booleanValue());
        }
        cl clVar = this.d;
        if (clVar != null) {
            iyVar.a(5, clVar);
        }
        Boolean bool2 = this.e;
        if (bool2 != null) {
            iyVar.a(6, bool2.booleanValue());
        }
        Boolean bool3 = this.f;
        if (bool3 != null) {
            iyVar.a(7, bool3.booleanValue());
        }
        super.a(iyVar);
    }

    /* access modifiers changed from: protected */
    public final int b() {
        int b2 = super.b();
        Integer num = this.f2116a;
        if (num != null) {
            b2 += iy.b(1, num.intValue());
        }
        String str = this.f2117b;
        if (str != null) {
            b2 += iy.b(2, str);
        }
        ck[] ckVarArr = this.c;
        if (ckVarArr != null && ckVarArr.length > 0) {
            int i = 0;
            while (true) {
                ck[] ckVarArr2 = this.c;
                if (i >= ckVarArr2.length) {
                    break;
                }
                ck ckVar = ckVarArr2[i];
                if (ckVar != null) {
                    b2 += iy.b(3, ckVar);
                }
                i++;
            }
        }
        Boolean bool = this.h;
        if (bool != null) {
            bool.booleanValue();
            b2 += iy.c(32) + 1;
        }
        cl clVar = this.d;
        if (clVar != null) {
            b2 += iy.b(5, clVar);
        }
        Boolean bool2 = this.e;
        if (bool2 != null) {
            bool2.booleanValue();
            b2 += iy.c(48) + 1;
        }
        Boolean bool3 = this.f;
        if (bool3 == null) {
            return b2;
        }
        bool3.booleanValue();
        return b2 + iy.c(56) + 1;
    }

    public final /* synthetic */ je a(iw iwVar) throws IOException {
        while (true) {
            int a2 = iwVar.a();
            if (a2 == 0) {
                return this;
            }
            if (a2 == 8) {
                this.f2116a = Integer.valueOf(iwVar.d());
            } else if (a2 == 18) {
                this.f2117b = iwVar.c();
            } else if (a2 == 26) {
                int a3 = jh.a(iwVar, 26);
                ck[] ckVarArr = this.c;
                int length = ckVarArr == null ? 0 : ckVarArr.length;
                ck[] ckVarArr2 = new ck[(a3 + length)];
                if (length != 0) {
                    System.arraycopy(this.c, 0, ckVarArr2, 0, length);
                }
                while (length < ckVarArr2.length - 1) {
                    ckVarArr2[length] = new ck();
                    iwVar.a(ckVarArr2[length]);
                    iwVar.a();
                    length++;
                }
                ckVarArr2[length] = new ck();
                iwVar.a(ckVarArr2[length]);
                this.c = ckVarArr2;
            } else if (a2 == 32) {
                this.h = Boolean.valueOf(iwVar.b());
            } else if (a2 == 42) {
                if (this.d == null) {
                    this.d = new cl();
                }
                iwVar.a(this.d);
            } else if (a2 == 48) {
                this.e = Boolean.valueOf(iwVar.b());
            } else if (a2 == 56) {
                this.f = Boolean.valueOf(iwVar.b());
            } else if (!super.a(iwVar, a2)) {
                return this;
            }
        }
    }
}
