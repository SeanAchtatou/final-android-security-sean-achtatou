package com.google.android.gms.internal.ads_identifier;

import android.os.Parcel;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final ClassLoader f1874a = a.class.getClassLoader();

    private a() {
    }

    public static boolean a(Parcel parcel) {
        return parcel.readInt() != 0;
    }

    public static void b(Parcel parcel) {
        parcel.writeInt(1);
    }
}
