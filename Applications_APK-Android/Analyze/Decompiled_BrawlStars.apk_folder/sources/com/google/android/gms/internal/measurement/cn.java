package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class cn extends iz<cn> {

    /* renamed from: a  reason: collision with root package name */
    public Integer f2124a = null;

    /* renamed from: b  reason: collision with root package name */
    public String f2125b = null;
    public Boolean c = null;
    public String[] d = jh.c;

    public cn() {
        this.L = null;
        this.M = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof cn)) {
            return false;
        }
        cn cnVar = (cn) obj;
        Integer num = this.f2124a;
        if (num == null) {
            if (cnVar.f2124a != null) {
                return false;
            }
        } else if (!num.equals(cnVar.f2124a)) {
            return false;
        }
        String str = this.f2125b;
        if (str == null) {
            if (cnVar.f2125b != null) {
                return false;
            }
        } else if (!str.equals(cnVar.f2125b)) {
            return false;
        }
        Boolean bool = this.c;
        if (bool == null) {
            if (cnVar.c != null) {
                return false;
            }
        } else if (!bool.equals(cnVar.c)) {
            return false;
        }
        if (!jd.a(this.d, cnVar.d)) {
            return false;
        }
        if (this.L == null || this.L.a()) {
            return cnVar.L == null || cnVar.L.a();
        }
        return this.L.equals(cnVar.L);
    }

    public final int hashCode() {
        int hashCode = (getClass().getName().hashCode() + 527) * 31;
        Integer num = this.f2124a;
        int i = 0;
        int intValue = (hashCode + (num == null ? 0 : num.intValue())) * 31;
        String str = this.f2125b;
        int hashCode2 = (intValue + (str == null ? 0 : str.hashCode())) * 31;
        Boolean bool = this.c;
        int hashCode3 = (((hashCode2 + (bool == null ? 0 : bool.hashCode())) * 31) + jd.a(this.d)) * 31;
        if (this.L != null && !this.L.a()) {
            i = this.L.hashCode();
        }
        return hashCode3 + i;
    }

    public final void a(iy iyVar) throws IOException {
        Integer num = this.f2124a;
        if (num != null) {
            iyVar.a(1, num.intValue());
        }
        String str = this.f2125b;
        if (str != null) {
            iyVar.a(2, str);
        }
        Boolean bool = this.c;
        if (bool != null) {
            iyVar.a(3, bool.booleanValue());
        }
        String[] strArr = this.d;
        if (strArr != null && strArr.length > 0) {
            int i = 0;
            while (true) {
                String[] strArr2 = this.d;
                if (i >= strArr2.length) {
                    break;
                }
                String str2 = strArr2[i];
                if (str2 != null) {
                    iyVar.a(4, str2);
                }
                i++;
            }
        }
        super.a(iyVar);
    }

    /* access modifiers changed from: protected */
    public final int b() {
        int b2 = super.b();
        Integer num = this.f2124a;
        if (num != null) {
            b2 += iy.b(1, num.intValue());
        }
        String str = this.f2125b;
        if (str != null) {
            b2 += iy.b(2, str);
        }
        Boolean bool = this.c;
        if (bool != null) {
            bool.booleanValue();
            b2 += iy.c(24) + 1;
        }
        String[] strArr = this.d;
        if (strArr == null || strArr.length <= 0) {
            return b2;
        }
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (true) {
            String[] strArr2 = this.d;
            if (i >= strArr2.length) {
                return b2 + i2 + (i3 * 1);
            }
            String str2 = strArr2[i];
            if (str2 != null) {
                i3++;
                i2 += iy.a(str2);
            }
            i++;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final cn a(iw iwVar) throws IOException {
        int d2;
        while (true) {
            int a2 = iwVar.a();
            if (a2 == 0) {
                return this;
            }
            if (a2 == 8) {
                try {
                    d2 = iwVar.d();
                    if (d2 < 0 || d2 > 6) {
                        StringBuilder sb = new StringBuilder(41);
                        sb.append(d2);
                        sb.append(" is not a valid enum MatchType");
                    } else {
                        this.f2124a = Integer.valueOf(d2);
                    }
                } catch (IllegalArgumentException unused) {
                    iwVar.e(iwVar.i());
                    a(iwVar, a2);
                }
            } else if (a2 == 18) {
                this.f2125b = iwVar.c();
            } else if (a2 == 24) {
                this.c = Boolean.valueOf(iwVar.b());
            } else if (a2 == 34) {
                int a3 = jh.a(iwVar, 34);
                String[] strArr = this.d;
                int length = strArr == null ? 0 : strArr.length;
                String[] strArr2 = new String[(a3 + length)];
                if (length != 0) {
                    System.arraycopy(this.d, 0, strArr2, 0, length);
                }
                while (length < strArr2.length - 1) {
                    strArr2[length] = iwVar.c();
                    iwVar.a();
                    length++;
                }
                strArr2[length] = iwVar.c();
                this.d = strArr2;
            } else if (!super.a(iwVar, a2)) {
                return this;
            }
        }
        StringBuilder sb2 = new StringBuilder(41);
        sb2.append(d2);
        sb2.append(" is not a valid enum MatchType");
        throw new IllegalArgumentException(sb2.toString());
    }
}
