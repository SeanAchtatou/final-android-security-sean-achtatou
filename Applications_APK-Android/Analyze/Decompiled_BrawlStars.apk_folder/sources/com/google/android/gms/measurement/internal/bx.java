package com.google.android.gms.measurement.internal;

final class bx implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ String f2446a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f2447b;
    private final /* synthetic */ Object c;
    private final /* synthetic */ long d;
    private final /* synthetic */ bv e;

    bx(bv bvVar, String str, String str2, Object obj, long j) {
        this.e = bvVar;
        this.f2446a = str;
        this.f2447b = str2;
        this.c = obj;
        this.d = j;
    }

    public final void run() {
        this.e.a(this.f2446a, this.f2447b, this.c, this.d);
    }
}
