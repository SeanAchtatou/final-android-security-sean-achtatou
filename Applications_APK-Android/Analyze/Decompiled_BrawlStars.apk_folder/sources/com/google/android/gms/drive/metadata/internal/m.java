package com.google.android.gms.drive.metadata.internal;

import android.os.Bundle;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.metadata.b;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

public final class m extends j<DriveId> implements b<DriveId> {

    /* renamed from: a  reason: collision with root package name */
    public static final f f1740a = new n();

    public m() {
        super("parents", Collections.emptySet(), Arrays.asList("parentsExtra", "dbInstanceId", "parentsExtraHolder"), 4100000);
    }

    public final /* synthetic */ Object b(Bundle bundle) {
        return b(bundle);
    }

    public final /* synthetic */ Object c(DataHolder dataHolder, int i, int i2) {
        return c(dataHolder, i, i2);
    }

    /* access modifiers changed from: protected */
    public final Collection<DriveId> c(Bundle bundle) {
        Collection c = super.b(bundle);
        if (c == null) {
            return null;
        }
        return new HashSet(c);
    }

    public final Collection<DriveId> b_(DataHolder dataHolder, int i, int i2) {
        String str;
        DataHolder dataHolder2 = dataHolder;
        Bundle bundle = dataHolder2.d;
        ArrayList parcelableArrayList = bundle.getParcelableArrayList("parentsExtra");
        if (parcelableArrayList == null) {
            if (bundle.getParcelable("parentsExtraHolder") != null) {
                synchronized (dataHolder) {
                    DataHolder dataHolder3 = (DataHolder) dataHolder2.d.getParcelable("parentsExtraHolder");
                    if (dataHolder3 != null) {
                        try {
                            int i3 = dataHolder2.f;
                            ArrayList arrayList = new ArrayList(i3);
                            HashMap hashMap = new HashMap(i3);
                            for (int i4 = 0; i4 < i3; i4++) {
                                int a2 = dataHolder2.a(i4);
                                ParentDriveIdSet parentDriveIdSet = new ParentDriveIdSet();
                                arrayList.add(parentDriveIdSet);
                                hashMap.put(Long.valueOf(dataHolder2.a("sqlId", i4, a2)), parentDriveIdSet);
                            }
                            Bundle bundle2 = dataHolder3.d;
                            String string = bundle2.getString("childSqlIdColumn");
                            String string2 = bundle2.getString("parentSqlIdColumn");
                            String string3 = bundle2.getString("parentResIdColumn");
                            int i5 = dataHolder3.f;
                            for (int i6 = 0; i6 < i5; i6++) {
                                int a3 = dataHolder3.a(i6);
                                ((ParentDriveIdSet) hashMap.get(Long.valueOf(dataHolder3.a(string, i6, a3)))).f1737a.add(new zzq(dataHolder3.c(string3, i6, a3), dataHolder3.a(string2, i6, a3), 1));
                            }
                            dataHolder2.d.putParcelableArrayList("parentsExtra", arrayList);
                        } finally {
                            dataHolder3.close();
                            str = "parentsExtraHolder";
                            dataHolder2.d.remove(str);
                        }
                    }
                }
                parcelableArrayList = bundle.getParcelableArrayList("parentsExtra");
            }
            if (parcelableArrayList == null) {
                return null;
            }
        }
        long j = bundle.getLong("dbInstanceId");
        HashSet hashSet = new HashSet();
        for (zzq next : ((ParentDriveIdSet) parcelableArrayList.get(i)).f1737a) {
            hashSet.add(new DriveId(next.f1743a, next.f1744b, j, next.c));
        }
        return hashSet;
    }

    static /* synthetic */ void a(DataHolder dataHolder) {
        Bundle bundle = dataHolder.d;
        if (bundle != null) {
            synchronized (dataHolder) {
                DataHolder dataHolder2 = (DataHolder) bundle.getParcelable("parentsExtraHolder");
                if (dataHolder2 != null) {
                    dataHolder2.close();
                    bundle.remove("parentsExtraHolder");
                }
            }
        }
    }
}
