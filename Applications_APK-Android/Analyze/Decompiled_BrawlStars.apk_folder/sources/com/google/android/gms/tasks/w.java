package com.google.android.gms.tasks;

import java.util.concurrent.Executor;

final class w<TResult, TContinuationResult> implements b, d, e<TContinuationResult>, y<TResult> {

    /* renamed from: a  reason: collision with root package name */
    final f<TResult, TContinuationResult> f2706a;

    /* renamed from: b  reason: collision with root package name */
    private final Executor f2707b;
    private final ab<TContinuationResult> c;

    public w(Executor executor, f<TResult, TContinuationResult> fVar, ab<TContinuationResult> abVar) {
        this.f2707b = executor;
        this.f2706a = fVar;
        this.c = abVar;
    }

    public final void a(g<TResult> gVar) {
        this.f2707b.execute(new x(this, gVar));
    }

    public final void a() {
        throw new UnsupportedOperationException();
    }

    public final void a(TContinuationResult tcontinuationresult) {
        this.c.a((Object) tcontinuationresult);
    }

    public final void a(Exception exc) {
        this.c.a(exc);
    }

    public final void g_() {
        this.c.f();
    }
}
