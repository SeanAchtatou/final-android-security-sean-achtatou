package com.google.android.gms.internal.measurement;

import android.content.ComponentName;

final class ac implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ComponentName f2035a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ aa f2036b;

    ac(aa aaVar, ComponentName componentName) {
        this.f2036b = aaVar;
        this.f2035a = componentName;
    }

    public final void run() {
        y.a(this.f2036b.f2031a, this.f2035a);
    }
}
