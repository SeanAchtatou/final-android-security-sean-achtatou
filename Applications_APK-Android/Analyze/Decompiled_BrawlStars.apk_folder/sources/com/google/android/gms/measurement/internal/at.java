package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.l;

final class at implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zzk f2391a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ zzby f2392b;

    at(zzby zzby, zzk zzk) {
        this.f2392b = zzby;
        this.f2391a = zzk;
    }

    public final void run() {
        this.f2392b.f2597a.k();
        du a2 = this.f2392b.f2597a;
        zzk zzk = this.f2391a;
        a2.g();
        a2.h();
        l.a(zzk.f2601a);
        a2.c(zzk);
    }
}
