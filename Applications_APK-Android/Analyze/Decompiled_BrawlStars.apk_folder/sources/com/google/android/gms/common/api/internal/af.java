package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.g;
import com.google.android.gms.common.api.internal.c;
import java.util.Collections;

public final class af implements al {

    /* renamed from: a  reason: collision with root package name */
    private final am f1390a;

    public af(am amVar) {
        this.f1390a = amVar;
    }

    public final void a(int i) {
    }

    public final void a(Bundle bundle) {
    }

    public final void a(ConnectionResult connectionResult, a<?> aVar, boolean z) {
    }

    public final boolean b() {
        return true;
    }

    public final void a() {
        for (a.f a2 : this.f1390a.f.values()) {
            a2.a();
        }
        this.f1390a.m.c = Collections.emptySet();
    }

    public final <A extends a.b, T extends c.a<? extends g, A>> T a(T t) {
        throw new IllegalStateException("GoogleApiClient is not connected yet.");
    }

    public final void c() {
        am amVar = this.f1390a;
        amVar.f1397a.lock();
        try {
            amVar.k = new u(amVar, amVar.h, amVar.i, amVar.d, amVar.j, amVar.f1397a, amVar.c);
            amVar.k.a();
            amVar.f1398b.signalAll();
        } finally {
            amVar.f1397a.unlock();
        }
    }
}
