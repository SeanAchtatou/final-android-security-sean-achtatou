package com.google.android.gms.measurement.internal;

import android.os.RemoteException;

final class cr implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zzk f2479a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ cl f2480b;

    cr(cl clVar, zzk zzk) {
        this.f2480b = clVar;
        this.f2479a = zzk;
    }

    public final void run() {
        zzaj zzaj = this.f2480b.f2470b;
        if (zzaj == null) {
            this.f2480b.q().c.a("Failed to send measurementEnabled to service");
            return;
        }
        try {
            zzaj.b(this.f2479a);
            this.f2480b.y();
        } catch (RemoteException e) {
            this.f2480b.q().c.a("Failed to send measurementEnabled to the service", e);
        }
    }
}
