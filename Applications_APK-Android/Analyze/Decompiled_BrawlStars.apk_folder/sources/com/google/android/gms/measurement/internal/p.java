package com.google.android.gms.measurement.internal;

import android.content.SharedPreferences;

final class p implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ int f2576a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f2577b;
    private final /* synthetic */ Object c;
    private final /* synthetic */ Object d;
    private final /* synthetic */ Object e;
    private final /* synthetic */ o f;

    p(o oVar, int i, String str, Object obj, Object obj2, Object obj3) {
        this.f = oVar;
        this.f2576a = i;
        this.f2577b = str;
        this.c = obj;
        this.d = obj2;
        this.e = obj3;
    }

    public final void run() {
        z b2 = this.f.r.b();
        if (!b2.v()) {
            this.f.a(6, "Persisted config not initialized. Not logging error/warn");
            return;
        }
        if (this.f.f2574a == 0) {
            if (this.f.s().e()) {
                this.f.f2574a = 'C';
            } else {
                this.f.f2574a = 'c';
            }
        }
        if (this.f.f2575b < 0) {
            this.f.f2575b = 14710;
        }
        char charAt = "01VDIWEA?".charAt(this.f2576a);
        char c2 = this.f.f2574a;
        long j = this.f.f2575b;
        String a2 = o.a(true, this.f2577b, this.c, this.d, this.e);
        StringBuilder sb = new StringBuilder(String.valueOf(a2).length() + 24);
        sb.append("2");
        sb.append(charAt);
        sb.append(c2);
        sb.append(j);
        sb.append(":");
        sb.append(a2);
        String sb2 = sb.toString();
        boolean z = false;
        if (sb2.length() > 1024) {
            sb2 = this.f2577b.substring(0, 1024);
        }
        ac acVar = b2.c;
        acVar.d.c();
        if (acVar.b() == 0) {
            acVar.a();
        }
        if (sb2 == null) {
            sb2 = "";
        }
        long j2 = acVar.d.f().getLong(acVar.f2364a, 0);
        if (j2 <= 0) {
            SharedPreferences.Editor edit = acVar.d.f().edit();
            edit.putString(acVar.f2365b, sb2);
            edit.putLong(acVar.f2364a, 1);
            edit.apply();
            return;
        }
        long j3 = j2 + 1;
        if ((acVar.d.o().g().nextLong() & Long.MAX_VALUE) < Long.MAX_VALUE / j3) {
            z = true;
        }
        SharedPreferences.Editor edit2 = acVar.d.f().edit();
        if (z) {
            edit2.putString(acVar.f2365b, sb2);
        }
        edit2.putLong(acVar.f2364a, j3);
        edit2.apply();
    }
}
