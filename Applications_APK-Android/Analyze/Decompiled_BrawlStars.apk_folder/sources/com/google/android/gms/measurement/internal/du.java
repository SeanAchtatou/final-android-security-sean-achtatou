package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import com.facebook.appevents.codeless.internal.Constants;
import com.facebook.internal.AnalyticsEvents;
import com.google.android.gms.common.c.c;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.util.e;
import com.google.android.gms.internal.measurement.cp;
import com.google.android.gms.internal.measurement.ct;
import com.google.android.gms.internal.measurement.cu;
import com.google.android.gms.internal.measurement.cw;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class du implements bo {
    private static volatile du f;

    /* renamed from: a  reason: collision with root package name */
    ce f2527a;

    /* renamed from: b  reason: collision with root package name */
    final ar f2528b;
    List<Runnable> c;
    int d;
    int e;
    private al g;
    private s h;
    private eo i;
    private x j;
    private dq k;
    private ei l;
    private final ea m;
    private boolean n;
    private boolean o;
    private boolean p;
    private long q;
    private boolean r;
    private boolean s;
    private boolean t;
    private FileLock u;
    private FileChannel v;
    private List<Long> w;
    private List<Long> x;
    private long y;

    class a implements eq {

        /* renamed from: a  reason: collision with root package name */
        cw f2529a;

        /* renamed from: b  reason: collision with root package name */
        List<Long> f2530b;
        List<ct> c;
        private long d;

        private a() {
        }

        public final void a(cw cwVar) {
            l.a(cwVar);
            this.f2529a = cwVar;
        }

        public final boolean a(long j, ct ctVar) {
            l.a(ctVar);
            if (this.c == null) {
                this.c = new ArrayList();
            }
            if (this.f2530b == null) {
                this.f2530b = new ArrayList();
            }
            if (this.c.size() > 0 && a(this.c.get(0)) != a(ctVar)) {
                return false;
            }
            long e2 = this.d + ((long) ctVar.e());
            if (e2 >= ((long) Math.max(0, h.q.a().intValue()))) {
                return false;
            }
            this.d = e2;
            this.c.add(ctVar);
            this.f2530b.add(Long.valueOf(j));
            if (this.c.size() >= Math.max(1, h.r.a().intValue())) {
                return false;
            }
            return true;
        }

        private static long a(ct ctVar) {
            return ((ctVar.c.longValue() / 1000) / 60) / 60;
        }

        /* synthetic */ a(du duVar, byte b2) {
            this();
        }
    }

    public static du a(Context context) {
        l.a(context);
        l.a(context.getApplicationContext());
        if (f == null) {
            synchronized (du.class) {
                if (f == null) {
                    f = new du(new dz(context));
                }
            }
        }
        return f;
    }

    private du(dz dzVar) {
        this(dzVar, (byte) 0);
    }

    private du(dz dzVar, byte b2) {
        this.n = false;
        l.a(dzVar);
        this.f2528b = ar.a(dzVar.f2538a, (j) null);
        this.y = -1;
        ea eaVar = new ea(this);
        eaVar.t();
        this.m = eaVar;
        s sVar = new s(this);
        sVar.t();
        this.h = sVar;
        al alVar = new al(this);
        alVar.t();
        this.g = alVar;
        this.f2528b.p().a(new dv(this, dzVar));
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2528b.p().c();
        d().z();
        if (this.f2528b.b().d.a() == 0) {
            this.f2528b.b().d.a(this.f2528b.l().a());
        }
        j();
    }

    public final el b() {
        return this.f2528b.e;
    }

    public final o q() {
        return this.f2528b.q();
    }

    public final am p() {
        return this.f2528b.p();
    }

    private final al n() {
        a(this.g);
        return this.g;
    }

    public final s c() {
        a(this.h);
        return this.h;
    }

    public final eo d() {
        a(this.i);
        return this.i;
    }

    private final x o() {
        x xVar = this.j;
        if (xVar != null) {
            return xVar;
        }
        throw new IllegalStateException("Network broadcast receiver not created");
    }

    private final dq r() {
        a(this.k);
        return this.k;
    }

    public final ei e() {
        a(this.l);
        return this.l;
    }

    public final ea f() {
        a(this.m);
        return this.m;
    }

    public final Context m() {
        return this.f2528b.m();
    }

    public final e l() {
        return this.f2528b.l();
    }

    /* access modifiers changed from: package-private */
    public final void g() {
        this.f2528b.p().c();
    }

    /* access modifiers changed from: package-private */
    public final void h() {
        if (!this.n) {
            throw new IllegalStateException("UploadController is not initialized");
        }
    }

    static void a(dt dtVar) {
        if (dtVar == null) {
            throw new IllegalStateException("Upload Component not created");
        } else if (!dtVar.i()) {
            String valueOf = String.valueOf(dtVar.getClass());
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("Component not initialized: ");
            sb.append(valueOf);
            throw new IllegalStateException(sb.toString());
        }
    }

    private final long s() {
        long a2 = this.f2528b.l().a();
        z b2 = this.f2528b.b();
        b2.w();
        b2.c();
        long a3 = b2.h.a();
        if (a3 == 0) {
            a3 = 1 + ((long) b2.o().g().nextInt(86400000));
            b2.h.a(a3);
        }
        return ((((a2 + a3) / 1000) / 60) / 60) / 24;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzk.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, ?[OBJECT, ARRAY], boolean, int, java.lang.String, long, int, int, boolean, boolean, int, java.lang.String]
     candidates:
      com.google.android.gms.measurement.internal.zzk.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, long, long, java.lang.String, boolean, boolean, long, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String):void
      com.google.android.gms.measurement.internal.zzk.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String):void */
    /* access modifiers changed from: package-private */
    public final void a(zzag zzag, String str) {
        zzag zzag2 = zzag;
        String str2 = str;
        ef b2 = d().b(str2);
        if (b2 == null || TextUtils.isEmpty(b2.i())) {
            this.f2528b.q().j.a("No app data available; dropping event", str2);
            return;
        }
        Boolean b3 = b(b2);
        if (b3 == null) {
            if (!"_ui".equals(zzag2.f2595a)) {
                this.f2528b.q().f.a("Could not find package. appId", o.a(str));
            }
        } else if (!b3.booleanValue()) {
            this.f2528b.q().c.a("App version does not match; dropping event. appId", o.a(str));
            return;
        }
        zzk zzk = r2;
        ef efVar = b2;
        zzk zzk2 = new zzk(str, b2.c(), b2.i(), b2.j(), b2.k(), b2.l(), b2.m(), (String) null, b2.n(), false, efVar.f(), efVar.t(), 0L, 0, efVar.u(), efVar.v(), false, efVar.d());
        a(zzag2, zzk);
    }

    /* access modifiers changed from: package-private */
    public final void a(zzag zzag, zzk zzk) {
        List<zzo> list;
        List<zzo> list2;
        List<zzo> list3;
        zzag zzag2 = zzag;
        zzk zzk2 = zzk;
        l.a(zzk);
        l.a(zzk2.f2601a);
        g();
        h();
        String str = zzk2.f2601a;
        long j2 = zzag2.d;
        if (f().a(zzag2, zzk2)) {
            if (!zzk2.h) {
                c(zzk2);
                return;
            }
            d().e();
            try {
                eo d2 = d();
                l.a(str);
                d2.c();
                d2.j();
                if (j2 < 0) {
                    d2.q().f.a("Invalid time querying timed out conditional properties", o.a(str), Long.valueOf(j2));
                    list = Collections.emptyList();
                } else {
                    list = d2.a("active=0 and app_id=? and abs(? - creation_timestamp) > trigger_timeout", new String[]{str, String.valueOf(j2)});
                }
                for (zzo zzo : list) {
                    if (zzo != null) {
                        this.f2528b.q().j.a("User property timed out", zzo.f2603a, this.f2528b.f().c(zzo.c.f2599a), zzo.c.a());
                        if (zzo.g != null) {
                            b(new zzag(zzo.g, j2), zzk2);
                        }
                        d().e(str, zzo.c.f2599a);
                    }
                }
                eo d3 = d();
                l.a(str);
                d3.c();
                d3.j();
                if (j2 < 0) {
                    d3.q().f.a("Invalid time querying expired conditional properties", o.a(str), Long.valueOf(j2));
                    list2 = Collections.emptyList();
                } else {
                    list2 = d3.a("active<>0 and app_id=? and abs(? - triggered_timestamp) > time_to_live", new String[]{str, String.valueOf(j2)});
                }
                ArrayList arrayList = new ArrayList(list2.size());
                for (zzo zzo2 : list2) {
                    if (zzo2 != null) {
                        this.f2528b.q().j.a("User property expired", zzo2.f2603a, this.f2528b.f().c(zzo2.c.f2599a), zzo2.c.a());
                        d().b(str, zzo2.c.f2599a);
                        if (zzo2.k != null) {
                            arrayList.add(zzo2.k);
                        }
                        d().e(str, zzo2.c.f2599a);
                    }
                }
                ArrayList arrayList2 = arrayList;
                int size = arrayList2.size();
                int i2 = 0;
                while (i2 < size) {
                    Object obj = arrayList2.get(i2);
                    i2++;
                    b(new zzag((zzag) obj, j2), zzk2);
                }
                eo d4 = d();
                String str2 = zzag2.f2595a;
                l.a(str);
                l.a(str2);
                d4.c();
                d4.j();
                if (j2 < 0) {
                    d4.q().f.a("Invalid time querying triggered conditional properties", o.a(str), d4.n().a(str2), Long.valueOf(j2));
                    list3 = Collections.emptyList();
                } else {
                    list3 = d4.a("active=0 and app_id=? and trigger_event_name=? and abs(? - creation_timestamp) <= trigger_timeout", new String[]{str, str2, String.valueOf(j2)});
                }
                ArrayList arrayList3 = new ArrayList(list3.size());
                for (zzo zzo3 : list3) {
                    if (zzo3 != null) {
                        zzfv zzfv = zzo3.c;
                        ec ecVar = r4;
                        ec ecVar2 = new ec(zzo3.f2603a, zzo3.f2604b, zzfv.f2599a, j2, zzfv.a());
                        if (d().a(ecVar)) {
                            this.f2528b.q().j.a("User property triggered", zzo3.f2603a, this.f2528b.f().c(ecVar.c), ecVar.e);
                        } else {
                            this.f2528b.q().c.a("Too many active user properties, ignoring", o.a(zzo3.f2603a), this.f2528b.f().c(ecVar.c), ecVar.e);
                        }
                        if (zzo3.i != null) {
                            arrayList3.add(zzo3.i);
                        }
                        zzo3.c = new zzfv(ecVar);
                        zzo3.e = true;
                        d().a(zzo3);
                    }
                }
                b(zzag, zzk);
                ArrayList arrayList4 = arrayList3;
                int size2 = arrayList4.size();
                int i3 = 0;
                while (i3 < size2) {
                    Object obj2 = arrayList4.get(i3);
                    i3++;
                    b(new zzag((zzag) obj2, j2), zzk2);
                }
                d().u();
            } finally {
                d().v();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.ed.a(android.os.Bundle, java.lang.String, java.lang.Object):void
     arg types: [android.os.Bundle, java.lang.String, long]
     candidates:
      com.google.android.gms.measurement.internal.ed.a(int, java.lang.Object, boolean):java.lang.Object
      com.google.android.gms.measurement.internal.ed.a(java.lang.String, int, boolean):java.lang.String
      com.google.android.gms.measurement.internal.ed.a(java.lang.String, int, java.lang.String):boolean
      com.google.android.gms.measurement.internal.ed.a(java.lang.String, java.lang.String[], java.lang.String):boolean
      com.google.android.gms.measurement.internal.ed.a(android.os.Bundle, java.lang.String, java.lang.Object):void */
    /* JADX WARNING: Code restructure failed: missing block: B:194:0x072c, code lost:
        r2 = true;
     */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0258 A[Catch:{ SQLiteException -> 0x0227, all -> 0x07d7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x028e A[Catch:{ SQLiteException -> 0x0227, all -> 0x07d7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x02db A[Catch:{ SQLiteException -> 0x0227, all -> 0x07d7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x0306  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void b(com.google.android.gms.measurement.internal.zzag r27, com.google.android.gms.measurement.internal.zzk r28) {
        /*
            r26 = this;
            r1 = r26
            r2 = r27
            r3 = r28
            java.lang.String r4 = "_sno"
            com.google.android.gms.common.internal.l.a(r28)
            java.lang.String r0 = r3.f2601a
            com.google.android.gms.common.internal.l.a(r0)
            long r5 = java.lang.System.nanoTime()
            r26.g()
            r26.h()
            java.lang.String r15 = r3.f2601a
            com.google.android.gms.measurement.internal.ea r0 = r26.f()
            boolean r0 = r0.a(r2, r3)
            if (r0 != 0) goto L_0x0027
            return
        L_0x0027:
            boolean r0 = r3.h
            if (r0 != 0) goto L_0x002f
            r1.c(r3)
            return
        L_0x002f:
            com.google.android.gms.measurement.internal.al r0 = r26.n()
            java.lang.String r7 = r2.f2595a
            boolean r0 = r0.b(r15, r7)
            java.lang.String r14 = "_err"
            r13 = 0
            r22 = 1
            if (r0 == 0) goto L_0x00d5
            com.google.android.gms.measurement.internal.ar r0 = r1.f2528b
            com.google.android.gms.measurement.internal.o r0 = r0.q()
            com.google.android.gms.measurement.internal.q r0 = r0.f
            java.lang.Object r3 = com.google.android.gms.measurement.internal.o.a(r15)
            com.google.android.gms.measurement.internal.ar r4 = r1.f2528b
            com.google.android.gms.measurement.internal.m r4 = r4.f()
            java.lang.String r5 = r2.f2595a
            java.lang.String r4 = r4.a(r5)
            java.lang.String r5 = "Dropping blacklisted event. appId"
            r0.a(r5, r3, r4)
            com.google.android.gms.measurement.internal.al r0 = r26.n()
            boolean r0 = r0.f(r15)
            if (r0 != 0) goto L_0x0071
            com.google.android.gms.measurement.internal.al r0 = r26.n()
            boolean r0 = r0.g(r15)
            if (r0 == 0) goto L_0x0072
        L_0x0071:
            r13 = 1
        L_0x0072:
            if (r13 != 0) goto L_0x008d
            java.lang.String r0 = r2.f2595a
            boolean r0 = r14.equals(r0)
            if (r0 != 0) goto L_0x008d
            com.google.android.gms.measurement.internal.ar r0 = r1.f2528b
            com.google.android.gms.measurement.internal.ed r7 = r0.e()
            r9 = 11
            java.lang.String r11 = r2.f2595a
            r12 = 0
            java.lang.String r10 = "_ev"
            r8 = r15
            r7.a(r8, r9, r10, r11, r12)
        L_0x008d:
            if (r13 == 0) goto L_0x00d4
            com.google.android.gms.measurement.internal.eo r0 = r26.d()
            com.google.android.gms.measurement.internal.ef r0 = r0.b(r15)
            if (r0 == 0) goto L_0x00d4
            long r2 = r0.q()
            long r4 = r0.p()
            long r2 = java.lang.Math.max(r2, r4)
            com.google.android.gms.measurement.internal.ar r4 = r1.f2528b
            com.google.android.gms.common.util.e r4 = r4.l()
            long r4 = r4.a()
            long r4 = r4 - r2
            long r2 = java.lang.Math.abs(r4)
            com.google.android.gms.measurement.internal.h$a<java.lang.Long> r4 = com.google.android.gms.measurement.internal.h.H
            java.lang.Object r4 = r4.a()
            java.lang.Long r4 = (java.lang.Long) r4
            long r4 = r4.longValue()
            int r6 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r6 <= 0) goto L_0x00d4
            com.google.android.gms.measurement.internal.ar r2 = r1.f2528b
            com.google.android.gms.measurement.internal.o r2 = r2.q()
            com.google.android.gms.measurement.internal.q r2 = r2.j
            java.lang.String r3 = "Fetching config for blacklisted app"
            r2.a(r3)
            r1.a(r0)
        L_0x00d4:
            return
        L_0x00d5:
            com.google.android.gms.measurement.internal.ar r0 = r1.f2528b
            com.google.android.gms.measurement.internal.o r0 = r0.q()
            r11 = 2
            boolean r0 = r0.a(r11)
            if (r0 == 0) goto L_0x00f9
            com.google.android.gms.measurement.internal.ar r0 = r1.f2528b
            com.google.android.gms.measurement.internal.o r0 = r0.q()
            com.google.android.gms.measurement.internal.q r0 = r0.k
            com.google.android.gms.measurement.internal.ar r7 = r1.f2528b
            com.google.android.gms.measurement.internal.m r7 = r7.f()
            java.lang.String r7 = r7.a(r2)
            java.lang.String r8 = "Logging event"
            r0.a(r8, r7)
        L_0x00f9:
            com.google.android.gms.measurement.internal.eo r0 = r26.d()
            r0.e()
            r1.c(r3)     // Catch:{ all -> 0x07d7 }
            java.lang.String r0 = "_iap"
            java.lang.String r7 = r2.f2595a     // Catch:{ all -> 0x07d7 }
            boolean r0 = r0.equals(r7)     // Catch:{ all -> 0x07d7 }
            java.lang.String r7 = "ecommerce_purchase"
            if (r0 != 0) goto L_0x011e
            java.lang.String r0 = r2.f2595a     // Catch:{ all -> 0x07d7 }
            boolean r0 = r7.equals(r0)     // Catch:{ all -> 0x07d7 }
            if (r0 == 0) goto L_0x0118
            goto L_0x011e
        L_0x0118:
            r23 = r5
            r5 = 2
            r6 = 0
            goto L_0x029d
        L_0x011e:
            com.google.android.gms.measurement.internal.zzad r0 = r2.f2596b     // Catch:{ all -> 0x07d7 }
            java.lang.String r8 = "currency"
            java.lang.String r0 = r0.d(r8)     // Catch:{ all -> 0x07d7 }
            java.lang.String r8 = r2.f2595a     // Catch:{ all -> 0x07d7 }
            boolean r7 = r7.equals(r8)     // Catch:{ all -> 0x07d7 }
            java.lang.String r8 = "value"
            if (r7 == 0) goto L_0x0183
            com.google.android.gms.measurement.internal.zzad r7 = r2.f2596b     // Catch:{ all -> 0x07d7 }
            java.lang.Double r7 = r7.c(r8)     // Catch:{ all -> 0x07d7 }
            double r9 = r7.doubleValue()     // Catch:{ all -> 0x07d7 }
            r16 = 4696837146684686336(0x412e848000000000, double:1000000.0)
            double r9 = r9 * r16
            r18 = 0
            int r7 = (r9 > r18 ? 1 : (r9 == r18 ? 0 : -1))
            if (r7 != 0) goto L_0x0157
            com.google.android.gms.measurement.internal.zzad r7 = r2.f2596b     // Catch:{ all -> 0x07d7 }
            java.lang.Long r7 = r7.b(r8)     // Catch:{ all -> 0x07d7 }
            long r7 = r7.longValue()     // Catch:{ all -> 0x07d7 }
            double r7 = (double) r7
            java.lang.Double.isNaN(r7)
            double r9 = r7 * r16
        L_0x0157:
            r7 = 4890909195324358656(0x43e0000000000000, double:9.223372036854776E18)
            int r12 = (r9 > r7 ? 1 : (r9 == r7 ? 0 : -1))
            if (r12 > 0) goto L_0x0168
            r7 = -4332462841530417152(0xc3e0000000000000, double:-9.223372036854776E18)
            int r12 = (r9 > r7 ? 1 : (r9 == r7 ? 0 : -1))
            if (r12 < 0) goto L_0x0168
            long r7 = java.lang.Math.round(r9)     // Catch:{ all -> 0x07d7 }
            goto L_0x018d
        L_0x0168:
            com.google.android.gms.measurement.internal.ar r0 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.o r0 = r0.q()     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.q r0 = r0.f     // Catch:{ all -> 0x07d7 }
            java.lang.String r7 = "Data lost. Currency value is too big. appId"
            java.lang.Object r8 = com.google.android.gms.measurement.internal.o.a(r15)     // Catch:{ all -> 0x07d7 }
            java.lang.Double r9 = java.lang.Double.valueOf(r9)     // Catch:{ all -> 0x07d7 }
            r0.a(r7, r8, r9)     // Catch:{ all -> 0x07d7 }
            r23 = r5
            r5 = 2
            r6 = 0
            goto L_0x028c
        L_0x0183:
            com.google.android.gms.measurement.internal.zzad r7 = r2.f2596b     // Catch:{ all -> 0x07d7 }
            java.lang.Long r7 = r7.b(r8)     // Catch:{ all -> 0x07d7 }
            long r7 = r7.longValue()     // Catch:{ all -> 0x07d7 }
        L_0x018d:
            boolean r9 = android.text.TextUtils.isEmpty(r0)     // Catch:{ all -> 0x07d7 }
            if (r9 != 0) goto L_0x0287
            java.util.Locale r9 = java.util.Locale.US     // Catch:{ all -> 0x07d7 }
            java.lang.String r0 = r0.toUpperCase(r9)     // Catch:{ all -> 0x07d7 }
            java.lang.String r9 = "[A-Z]{3}"
            boolean r9 = r0.matches(r9)     // Catch:{ all -> 0x07d7 }
            if (r9 == 0) goto L_0x0287
            java.lang.String r9 = "_ltv_"
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x07d7 }
            int r10 = r0.length()     // Catch:{ all -> 0x07d7 }
            if (r10 == 0) goto L_0x01b2
            java.lang.String r0 = r9.concat(r0)     // Catch:{ all -> 0x07d7 }
            goto L_0x01b7
        L_0x01b2:
            java.lang.String r0 = new java.lang.String     // Catch:{ all -> 0x07d7 }
            r0.<init>(r9)     // Catch:{ all -> 0x07d7 }
        L_0x01b7:
            r10 = r0
            com.google.android.gms.measurement.internal.eo r0 = r26.d()     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ec r0 = r0.c(r15, r10)     // Catch:{ all -> 0x07d7 }
            if (r0 == 0) goto L_0x01f3
            java.lang.Object r9 = r0.e     // Catch:{ all -> 0x07d7 }
            boolean r9 = r9 instanceof java.lang.Long     // Catch:{ all -> 0x07d7 }
            if (r9 != 0) goto L_0x01c9
            goto L_0x01f3
        L_0x01c9:
            java.lang.Object r0 = r0.e     // Catch:{ all -> 0x07d7 }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ all -> 0x07d7 }
            long r16 = r0.longValue()     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ec r0 = new com.google.android.gms.measurement.internal.ec     // Catch:{ all -> 0x07d7 }
            java.lang.String r9 = r2.c     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ar r12 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.common.util.e r12 = r12.l()     // Catch:{ all -> 0x07d7 }
            long r18 = r12.a()     // Catch:{ all -> 0x07d7 }
            long r16 = r16 + r7
            java.lang.Long r16 = java.lang.Long.valueOf(r16)     // Catch:{ all -> 0x07d7 }
            r7 = r0
            r8 = r15
            r23 = r5
            r5 = 2
            r11 = r18
            r6 = 0
            r13 = r16
            r7.<init>(r8, r9, r10, r11, r13)     // Catch:{ all -> 0x07d7 }
            goto L_0x024e
        L_0x01f3:
            r23 = r5
            r5 = 2
            r6 = 0
            com.google.android.gms.measurement.internal.eo r9 = r26.d()     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ar r0 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.el r0 = r0.e     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.h$a<java.lang.Integer> r11 = com.google.android.gms.measurement.internal.h.M     // Catch:{ all -> 0x07d7 }
            int r0 = r0.b(r15, r11)     // Catch:{ all -> 0x07d7 }
            int r0 = r0 + -1
            com.google.android.gms.common.internal.l.a(r15)     // Catch:{ all -> 0x07d7 }
            r9.c()     // Catch:{ all -> 0x07d7 }
            r9.j()     // Catch:{ all -> 0x07d7 }
            android.database.sqlite.SQLiteDatabase r11 = r9.w()     // Catch:{ SQLiteException -> 0x0227 }
            java.lang.String r12 = "delete from user_attributes where app_id=? and name in (select name from user_attributes where app_id=? and name like '_ltv_%' order by set_timestamp desc limit ?,10);"
            r13 = 3
            java.lang.String[] r13 = new java.lang.String[r13]     // Catch:{ SQLiteException -> 0x0227 }
            r13[r6] = r15     // Catch:{ SQLiteException -> 0x0227 }
            r13[r22] = r15     // Catch:{ SQLiteException -> 0x0227 }
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ SQLiteException -> 0x0227 }
            r13[r5] = r0     // Catch:{ SQLiteException -> 0x0227 }
            r11.execSQL(r12, r13)     // Catch:{ SQLiteException -> 0x0227 }
            goto L_0x0237
        L_0x0227:
            r0 = move-exception
            com.google.android.gms.measurement.internal.o r9 = r9.q()     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.q r9 = r9.c     // Catch:{ all -> 0x07d7 }
            java.lang.String r11 = "Error pruning currencies. appId"
            java.lang.Object r12 = com.google.android.gms.measurement.internal.o.a(r15)     // Catch:{ all -> 0x07d7 }
            r9.a(r11, r12, r0)     // Catch:{ all -> 0x07d7 }
        L_0x0237:
            com.google.android.gms.measurement.internal.ec r0 = new com.google.android.gms.measurement.internal.ec     // Catch:{ all -> 0x07d7 }
            java.lang.String r9 = r2.c     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ar r11 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.common.util.e r11 = r11.l()     // Catch:{ all -> 0x07d7 }
            long r11 = r11.a()     // Catch:{ all -> 0x07d7 }
            java.lang.Long r13 = java.lang.Long.valueOf(r7)     // Catch:{ all -> 0x07d7 }
            r7 = r0
            r8 = r15
            r7.<init>(r8, r9, r10, r11, r13)     // Catch:{ all -> 0x07d7 }
        L_0x024e:
            com.google.android.gms.measurement.internal.eo r7 = r26.d()     // Catch:{ all -> 0x07d7 }
            boolean r7 = r7.a(r0)     // Catch:{ all -> 0x07d7 }
            if (r7 != 0) goto L_0x028b
            com.google.android.gms.measurement.internal.ar r7 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.o r7 = r7.q()     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.q r7 = r7.c     // Catch:{ all -> 0x07d7 }
            java.lang.String r8 = "Too many unique user properties are set. Ignoring user property. appId"
            java.lang.Object r9 = com.google.android.gms.measurement.internal.o.a(r15)     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ar r10 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.m r10 = r10.f()     // Catch:{ all -> 0x07d7 }
            java.lang.String r11 = r0.c     // Catch:{ all -> 0x07d7 }
            java.lang.String r10 = r10.c(r11)     // Catch:{ all -> 0x07d7 }
            java.lang.Object r0 = r0.e     // Catch:{ all -> 0x07d7 }
            r7.a(r8, r9, r10, r0)     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ar r0 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ed r7 = r0.e()     // Catch:{ all -> 0x07d7 }
            r9 = 9
            r10 = 0
            r11 = 0
            r12 = 0
            r8 = r15
            r7.a(r8, r9, r10, r11, r12)     // Catch:{ all -> 0x07d7 }
            goto L_0x028b
        L_0x0287:
            r23 = r5
            r5 = 2
            r6 = 0
        L_0x028b:
            r13 = 1
        L_0x028c:
            if (r13 != 0) goto L_0x029d
            com.google.android.gms.measurement.internal.eo r0 = r26.d()     // Catch:{ all -> 0x07d7 }
            r0.u()     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.eo r0 = r26.d()
            r0.v()
            return
        L_0x029d:
            java.lang.String r0 = r2.f2595a     // Catch:{ all -> 0x07d7 }
            boolean r0 = com.google.android.gms.measurement.internal.ed.a(r0)     // Catch:{ all -> 0x07d7 }
            java.lang.String r7 = r2.f2595a     // Catch:{ all -> 0x07d7 }
            boolean r16 = r14.equals(r7)     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.eo r7 = r26.d()     // Catch:{ all -> 0x07d7 }
            long r8 = r26.s()     // Catch:{ all -> 0x07d7 }
            r11 = 1
            r13 = 0
            r17 = 0
            r10 = r15
            r12 = r0
            r14 = r16
            r18 = r15
            r15 = r17
            com.google.android.gms.measurement.internal.ep r7 = r7.a(r8, r10, r11, r12, r13, r14, r15)     // Catch:{ all -> 0x07d7 }
            long r8 = r7.f2555b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.h$a<java.lang.Integer> r10 = com.google.android.gms.measurement.internal.h.s     // Catch:{ all -> 0x07d7 }
            java.lang.Object r10 = r10.a()     // Catch:{ all -> 0x07d7 }
            java.lang.Integer r10 = (java.lang.Integer) r10     // Catch:{ all -> 0x07d7 }
            int r10 = r10.intValue()     // Catch:{ all -> 0x07d7 }
            long r10 = (long) r10     // Catch:{ all -> 0x07d7 }
            long r8 = r8 - r10
            r10 = 1000(0x3e8, double:4.94E-321)
            r12 = 1
            r14 = 0
            int r17 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r17 <= 0) goto L_0x0306
            long r8 = r8 % r10
            int r0 = (r8 > r12 ? 1 : (r8 == r12 ? 0 : -1))
            if (r0 != 0) goto L_0x02f7
            com.google.android.gms.measurement.internal.ar r0 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.o r0 = r0.q()     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.q r0 = r0.c     // Catch:{ all -> 0x07d7 }
            java.lang.String r2 = "Data loss. Too many events logged. appId, count"
            java.lang.Object r3 = com.google.android.gms.measurement.internal.o.a(r18)     // Catch:{ all -> 0x07d7 }
            long r4 = r7.f2555b     // Catch:{ all -> 0x07d7 }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ all -> 0x07d7 }
            r0.a(r2, r3, r4)     // Catch:{ all -> 0x07d7 }
        L_0x02f7:
            com.google.android.gms.measurement.internal.eo r0 = r26.d()     // Catch:{ all -> 0x07d7 }
            r0.u()     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.eo r0 = r26.d()
            r0.v()
            return
        L_0x0306:
            if (r0 == 0) goto L_0x0360
            long r8 = r7.f2554a     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.h$a<java.lang.Integer> r17 = com.google.android.gms.measurement.internal.h.u     // Catch:{ all -> 0x07d7 }
            java.lang.Object r17 = r17.a()     // Catch:{ all -> 0x07d7 }
            java.lang.Integer r17 = (java.lang.Integer) r17     // Catch:{ all -> 0x07d7 }
            int r5 = r17.intValue()     // Catch:{ all -> 0x07d7 }
            r17 = r7
            long r6 = (long) r5     // Catch:{ all -> 0x07d7 }
            long r8 = r8 - r6
            int r5 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r5 <= 0) goto L_0x035d
            long r8 = r8 % r10
            int r0 = (r8 > r12 ? 1 : (r8 == r12 ? 0 : -1))
            if (r0 != 0) goto L_0x033c
            com.google.android.gms.measurement.internal.ar r0 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.o r0 = r0.q()     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.q r0 = r0.c     // Catch:{ all -> 0x07d7 }
            java.lang.String r3 = "Data loss. Too many public events logged. appId, count"
            java.lang.Object r4 = com.google.android.gms.measurement.internal.o.a(r18)     // Catch:{ all -> 0x07d7 }
            r5 = r17
            long r5 = r5.f2554a     // Catch:{ all -> 0x07d7 }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ all -> 0x07d7 }
            r0.a(r3, r4, r5)     // Catch:{ all -> 0x07d7 }
        L_0x033c:
            com.google.android.gms.measurement.internal.ar r0 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ed r7 = r0.e()     // Catch:{ all -> 0x07d7 }
            r9 = 16
            java.lang.String r10 = "_ev"
            java.lang.String r11 = r2.f2595a     // Catch:{ all -> 0x07d7 }
            r12 = 0
            r8 = r18
            r7.a(r8, r9, r10, r11, r12)     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.eo r0 = r26.d()     // Catch:{ all -> 0x07d7 }
            r0.u()     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.eo r0 = r26.d()
            r0.v()
            return
        L_0x035d:
            r5 = r17
            goto L_0x0361
        L_0x0360:
            r5 = r7
        L_0x0361:
            if (r16 == 0) goto L_0x03ad
            long r6 = r5.d     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ar r8 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.el r8 = r8.e     // Catch:{ all -> 0x07d7 }
            java.lang.String r9 = r3.f2601a     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.h$a<java.lang.Integer> r10 = com.google.android.gms.measurement.internal.h.t     // Catch:{ all -> 0x07d7 }
            int r8 = r8.b(r9, r10)     // Catch:{ all -> 0x07d7 }
            r9 = 1000000(0xf4240, float:1.401298E-39)
            int r8 = java.lang.Math.min(r9, r8)     // Catch:{ all -> 0x07d7 }
            r11 = 0
            int r8 = java.lang.Math.max(r11, r8)     // Catch:{ all -> 0x07d7 }
            long r8 = (long) r8     // Catch:{ all -> 0x07d7 }
            long r6 = r6 - r8
            int r8 = (r6 > r14 ? 1 : (r6 == r14 ? 0 : -1))
            if (r8 <= 0) goto L_0x03ae
            int r0 = (r6 > r12 ? 1 : (r6 == r12 ? 0 : -1))
            if (r0 != 0) goto L_0x039e
            com.google.android.gms.measurement.internal.ar r0 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.o r0 = r0.q()     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.q r0 = r0.c     // Catch:{ all -> 0x07d7 }
            java.lang.String r2 = "Too many error events logged. appId, count"
            java.lang.Object r3 = com.google.android.gms.measurement.internal.o.a(r18)     // Catch:{ all -> 0x07d7 }
            long r4 = r5.d     // Catch:{ all -> 0x07d7 }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ all -> 0x07d7 }
            r0.a(r2, r3, r4)     // Catch:{ all -> 0x07d7 }
        L_0x039e:
            com.google.android.gms.measurement.internal.eo r0 = r26.d()     // Catch:{ all -> 0x07d7 }
            r0.u()     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.eo r0 = r26.d()
            r0.v()
            return
        L_0x03ad:
            r11 = 0
        L_0x03ae:
            com.google.android.gms.measurement.internal.zzad r5 = r2.f2596b     // Catch:{ all -> 0x07d7 }
            android.os.Bundle r5 = r5.a()     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ar r6 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ed r6 = r6.e()     // Catch:{ all -> 0x07d7 }
            java.lang.String r7 = "_o"
            java.lang.String r8 = r2.c     // Catch:{ all -> 0x07d7 }
            r6.a(r5, r7, r8)     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ar r6 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ed r6 = r6.e()     // Catch:{ all -> 0x07d7 }
            r10 = r18
            boolean r6 = r6.f(r10)     // Catch:{ all -> 0x07d7 }
            java.lang.String r9 = "_r"
            if (r6 == 0) goto L_0x03ed
            com.google.android.gms.measurement.internal.ar r6 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ed r6 = r6.e()     // Catch:{ all -> 0x07d7 }
            java.lang.String r7 = "_dbg"
            java.lang.Long r8 = java.lang.Long.valueOf(r12)     // Catch:{ all -> 0x07d7 }
            r6.a(r5, r7, r8)     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ar r6 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ed r6 = r6.e()     // Catch:{ all -> 0x07d7 }
            java.lang.Long r7 = java.lang.Long.valueOf(r12)     // Catch:{ all -> 0x07d7 }
            r6.a(r5, r9, r7)     // Catch:{ all -> 0x07d7 }
        L_0x03ed:
            com.google.android.gms.measurement.internal.ar r6 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.el r6 = r6.e     // Catch:{ all -> 0x07d7 }
            java.lang.String r7 = r3.f2601a     // Catch:{ all -> 0x07d7 }
            boolean r6 = r6.i(r7)     // Catch:{ all -> 0x07d7 }
            if (r6 == 0) goto L_0x0420
            java.lang.String r6 = "_s"
            java.lang.String r7 = r2.f2595a     // Catch:{ all -> 0x07d7 }
            boolean r6 = r6.equals(r7)     // Catch:{ all -> 0x07d7 }
            if (r6 == 0) goto L_0x0420
            com.google.android.gms.measurement.internal.eo r6 = r26.d()     // Catch:{ all -> 0x07d7 }
            java.lang.String r7 = r3.f2601a     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ec r6 = r6.c(r7, r4)     // Catch:{ all -> 0x07d7 }
            if (r6 == 0) goto L_0x0420
            java.lang.Object r7 = r6.e     // Catch:{ all -> 0x07d7 }
            boolean r7 = r7 instanceof java.lang.Long     // Catch:{ all -> 0x07d7 }
            if (r7 == 0) goto L_0x0420
            com.google.android.gms.measurement.internal.ar r7 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ed r7 = r7.e()     // Catch:{ all -> 0x07d7 }
            java.lang.Object r6 = r6.e     // Catch:{ all -> 0x07d7 }
            r7.a(r5, r4, r6)     // Catch:{ all -> 0x07d7 }
        L_0x0420:
            com.google.android.gms.measurement.internal.eo r4 = r26.d()     // Catch:{ all -> 0x07d7 }
            long r6 = r4.c(r10)     // Catch:{ all -> 0x07d7 }
            int r4 = (r6 > r14 ? 1 : (r6 == r14 ? 0 : -1))
            if (r4 <= 0) goto L_0x0441
            com.google.android.gms.measurement.internal.ar r4 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.o r4 = r4.q()     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.q r4 = r4.f     // Catch:{ all -> 0x07d7 }
            java.lang.String r8 = "Data lost. Too many events stored on disk, deleted. appId"
            java.lang.Object r12 = com.google.android.gms.measurement.internal.o.a(r10)     // Catch:{ all -> 0x07d7 }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x07d7 }
            r4.a(r8, r12, r6)     // Catch:{ all -> 0x07d7 }
        L_0x0441:
            com.google.android.gms.measurement.internal.c r4 = new com.google.android.gms.measurement.internal.c     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ar r8 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            java.lang.String r6 = r2.c     // Catch:{ all -> 0x07d7 }
            java.lang.String r12 = r2.f2595a     // Catch:{ all -> 0x07d7 }
            long r14 = r2.d     // Catch:{ all -> 0x07d7 }
            r18 = 0
            r7 = r4
            r2 = r9
            r9 = r6
            r6 = r10
            r25 = 0
            r11 = r12
            r12 = r14
            r14 = r18
            r16 = r5
            r7.<init>(r8, r9, r10, r11, r12, r14, r16)     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.eo r5 = r26.d()     // Catch:{ all -> 0x07d7 }
            java.lang.String r7 = r4.f2453b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.d r5 = r5.a(r6, r7)     // Catch:{ all -> 0x07d7 }
            if (r5 != 0) goto L_0x04cc
            com.google.android.gms.measurement.internal.eo r5 = r26.d()     // Catch:{ all -> 0x07d7 }
            long r7 = r5.f(r6)     // Catch:{ all -> 0x07d7 }
            r9 = 500(0x1f4, double:2.47E-321)
            int r5 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r5 < 0) goto L_0x04b2
            if (r0 == 0) goto L_0x04b2
            com.google.android.gms.measurement.internal.ar r0 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.o r0 = r0.q()     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.q r0 = r0.c     // Catch:{ all -> 0x07d7 }
            java.lang.String r2 = "Too many event names used, ignoring event. appId, name, supported count"
            java.lang.Object r3 = com.google.android.gms.measurement.internal.o.a(r6)     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ar r5 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.m r5 = r5.f()     // Catch:{ all -> 0x07d7 }
            java.lang.String r4 = r4.f2453b     // Catch:{ all -> 0x07d7 }
            java.lang.String r4 = r5.a(r4)     // Catch:{ all -> 0x07d7 }
            r5 = 500(0x1f4, float:7.0E-43)
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x07d7 }
            r0.a(r2, r3, r4, r5)     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ar r0 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ed r7 = r0.e()     // Catch:{ all -> 0x07d7 }
            r9 = 8
            r10 = 0
            r11 = 0
            r12 = 0
            r8 = r6
            r7.a(r8, r9, r10, r11, r12)     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.eo r0 = r26.d()
            r0.v()
            return
        L_0x04b2:
            com.google.android.gms.measurement.internal.d r0 = new com.google.android.gms.measurement.internal.d     // Catch:{ all -> 0x07d7 }
            java.lang.String r9 = r4.f2453b     // Catch:{ all -> 0x07d7 }
            r10 = 0
            r12 = 0
            long r14 = r4.c     // Catch:{ all -> 0x07d7 }
            r16 = 0
            r18 = 0
            r19 = 0
            r20 = 0
            r21 = 0
            r7 = r0
            r8 = r6
            r7.<init>(r8, r9, r10, r12, r14, r16, r18, r19, r20, r21)     // Catch:{ all -> 0x07d7 }
            goto L_0x04da
        L_0x04cc:
            com.google.android.gms.measurement.internal.ar r0 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            long r6 = r5.e     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.c r4 = r4.a(r0, r6)     // Catch:{ all -> 0x07d7 }
            long r6 = r4.c     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.d r0 = r5.a(r6)     // Catch:{ all -> 0x07d7 }
        L_0x04da:
            com.google.android.gms.measurement.internal.eo r5 = r26.d()     // Catch:{ all -> 0x07d7 }
            r5.a(r0)     // Catch:{ all -> 0x07d7 }
            r26.g()     // Catch:{ all -> 0x07d7 }
            r26.h()     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.common.internal.l.a(r4)     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.common.internal.l.a(r28)     // Catch:{ all -> 0x07d7 }
            java.lang.String r0 = r4.f2452a     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.common.internal.l.a(r0)     // Catch:{ all -> 0x07d7 }
            java.lang.String r0 = r4.f2452a     // Catch:{ all -> 0x07d7 }
            java.lang.String r5 = r3.f2601a     // Catch:{ all -> 0x07d7 }
            boolean r0 = r0.equals(r5)     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.common.internal.l.b(r0)     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.internal.measurement.cw r5 = new com.google.android.gms.internal.measurement.cw     // Catch:{ all -> 0x07d7 }
            r5.<init>()     // Catch:{ all -> 0x07d7 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r22)     // Catch:{ all -> 0x07d7 }
            r5.f2141a = r0     // Catch:{ all -> 0x07d7 }
            java.lang.String r0 = "android"
            r5.i = r0     // Catch:{ all -> 0x07d7 }
            java.lang.String r0 = r3.f2601a     // Catch:{ all -> 0x07d7 }
            r5.o = r0     // Catch:{ all -> 0x07d7 }
            java.lang.String r0 = r3.d     // Catch:{ all -> 0x07d7 }
            r5.n = r0     // Catch:{ all -> 0x07d7 }
            java.lang.String r0 = r3.c     // Catch:{ all -> 0x07d7 }
            r5.p = r0     // Catch:{ all -> 0x07d7 }
            long r6 = r3.j     // Catch:{ all -> 0x07d7 }
            r8 = -2147483648(0xffffffff80000000, double:NaN)
            r0 = 0
            int r10 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r10 != 0) goto L_0x0524
            r6 = r0
            goto L_0x052b
        L_0x0524:
            long r6 = r3.j     // Catch:{ all -> 0x07d7 }
            int r7 = (int) r6     // Catch:{ all -> 0x07d7 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r7)     // Catch:{ all -> 0x07d7 }
        L_0x052b:
            r5.C = r6     // Catch:{ all -> 0x07d7 }
            long r6 = r3.e     // Catch:{ all -> 0x07d7 }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x07d7 }
            r5.q = r6     // Catch:{ all -> 0x07d7 }
            java.lang.String r6 = r3.f2602b     // Catch:{ all -> 0x07d7 }
            r5.y = r6     // Catch:{ all -> 0x07d7 }
            java.lang.String r6 = r3.r     // Catch:{ all -> 0x07d7 }
            r5.I = r6     // Catch:{ all -> 0x07d7 }
            long r6 = r3.f     // Catch:{ all -> 0x07d7 }
            r8 = 0
            int r10 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r10 != 0) goto L_0x0547
            r6 = r0
            goto L_0x054d
        L_0x0547:
            long r6 = r3.f     // Catch:{ all -> 0x07d7 }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x07d7 }
        L_0x054d:
            r5.v = r6     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ar r6 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.el r6 = r6.e     // Catch:{ all -> 0x07d7 }
            java.lang.String r7 = r3.f2601a     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.h$a<java.lang.Boolean> r10 = com.google.android.gms.measurement.internal.h.as     // Catch:{ all -> 0x07d7 }
            boolean r6 = r6.c(r7, r10)     // Catch:{ all -> 0x07d7 }
            if (r6 == 0) goto L_0x0567
            com.google.android.gms.measurement.internal.ea r6 = r26.f()     // Catch:{ all -> 0x07d7 }
            int[] r6 = r6.e()     // Catch:{ all -> 0x07d7 }
            r5.K = r6     // Catch:{ all -> 0x07d7 }
        L_0x0567:
            com.google.android.gms.measurement.internal.ar r6 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.z r6 = r6.b()     // Catch:{ all -> 0x07d7 }
            java.lang.String r7 = r3.f2601a     // Catch:{ all -> 0x07d7 }
            android.util.Pair r6 = r6.a(r7)     // Catch:{ all -> 0x07d7 }
            java.lang.Object r7 = r6.first     // Catch:{ all -> 0x07d7 }
            java.lang.CharSequence r7 = (java.lang.CharSequence) r7     // Catch:{ all -> 0x07d7 }
            boolean r7 = android.text.TextUtils.isEmpty(r7)     // Catch:{ all -> 0x07d7 }
            if (r7 != 0) goto L_0x058e
            boolean r7 = r3.o     // Catch:{ all -> 0x07d7 }
            if (r7 == 0) goto L_0x05e7
            java.lang.Object r7 = r6.first     // Catch:{ all -> 0x07d7 }
            java.lang.String r7 = (java.lang.String) r7     // Catch:{ all -> 0x07d7 }
            r5.s = r7     // Catch:{ all -> 0x07d7 }
            java.lang.Object r6 = r6.second     // Catch:{ all -> 0x07d7 }
            java.lang.Boolean r6 = (java.lang.Boolean) r6     // Catch:{ all -> 0x07d7 }
            r5.t = r6     // Catch:{ all -> 0x07d7 }
            goto L_0x05e7
        L_0x058e:
            com.google.android.gms.measurement.internal.ar r6 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.b r6 = r6.j()     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ar r7 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            android.content.Context r7 = r7.m()     // Catch:{ all -> 0x07d7 }
            boolean r6 = r6.a(r7)     // Catch:{ all -> 0x07d7 }
            if (r6 != 0) goto L_0x05e7
            boolean r6 = r3.p     // Catch:{ all -> 0x07d7 }
            if (r6 == 0) goto L_0x05e7
            com.google.android.gms.measurement.internal.ar r6 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            android.content.Context r6 = r6.m()     // Catch:{ all -> 0x07d7 }
            android.content.ContentResolver r6 = r6.getContentResolver()     // Catch:{ all -> 0x07d7 }
            java.lang.String r7 = "android_id"
            java.lang.String r6 = android.provider.Settings.Secure.getString(r6, r7)     // Catch:{ all -> 0x07d7 }
            if (r6 != 0) goto L_0x05cc
            com.google.android.gms.measurement.internal.ar r6 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.o r6 = r6.q()     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.q r6 = r6.f     // Catch:{ all -> 0x07d7 }
            java.lang.String r7 = "null secure ID. appId"
            java.lang.String r10 = r5.o     // Catch:{ all -> 0x07d7 }
            java.lang.Object r10 = com.google.android.gms.measurement.internal.o.a(r10)     // Catch:{ all -> 0x07d7 }
            r6.a(r7, r10)     // Catch:{ all -> 0x07d7 }
            java.lang.String r6 = "null"
            goto L_0x05e5
        L_0x05cc:
            boolean r7 = r6.isEmpty()     // Catch:{ all -> 0x07d7 }
            if (r7 == 0) goto L_0x05e5
            com.google.android.gms.measurement.internal.ar r7 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.o r7 = r7.q()     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.q r7 = r7.f     // Catch:{ all -> 0x07d7 }
            java.lang.String r10 = "empty secure ID. appId"
            java.lang.String r11 = r5.o     // Catch:{ all -> 0x07d7 }
            java.lang.Object r11 = com.google.android.gms.measurement.internal.o.a(r11)     // Catch:{ all -> 0x07d7 }
            r7.a(r10, r11)     // Catch:{ all -> 0x07d7 }
        L_0x05e5:
            r5.D = r6     // Catch:{ all -> 0x07d7 }
        L_0x05e7:
            com.google.android.gms.measurement.internal.ar r6 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.b r6 = r6.j()     // Catch:{ all -> 0x07d7 }
            r6.w()     // Catch:{ all -> 0x07d7 }
            java.lang.String r6 = android.os.Build.MODEL     // Catch:{ all -> 0x07d7 }
            r5.k = r6     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ar r6 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.b r6 = r6.j()     // Catch:{ all -> 0x07d7 }
            r6.w()     // Catch:{ all -> 0x07d7 }
            java.lang.String r6 = android.os.Build.VERSION.RELEASE     // Catch:{ all -> 0x07d7 }
            r5.j = r6     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ar r6 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.b r6 = r6.j()     // Catch:{ all -> 0x07d7 }
            long r6 = r6.e_()     // Catch:{ all -> 0x07d7 }
            int r7 = (int) r6     // Catch:{ all -> 0x07d7 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r7)     // Catch:{ all -> 0x07d7 }
            r5.m = r6     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ar r6 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.b r6 = r6.j()     // Catch:{ all -> 0x07d7 }
            java.lang.String r6 = r6.f()     // Catch:{ all -> 0x07d7 }
            r5.l = r6     // Catch:{ all -> 0x07d7 }
            r5.r = r0     // Catch:{ all -> 0x07d7 }
            r5.d = r0     // Catch:{ all -> 0x07d7 }
            r5.e = r0     // Catch:{ all -> 0x07d7 }
            r5.f = r0     // Catch:{ all -> 0x07d7 }
            long r6 = r3.l     // Catch:{ all -> 0x07d7 }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x07d7 }
            r5.F = r6     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ar r6 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            boolean r6 = r6.r()     // Catch:{ all -> 0x07d7 }
            if (r6 == 0) goto L_0x063e
            boolean r6 = com.google.android.gms.measurement.internal.el.j()     // Catch:{ all -> 0x07d7 }
            if (r6 == 0) goto L_0x063e
            r5.G = r0     // Catch:{ all -> 0x07d7 }
        L_0x063e:
            com.google.android.gms.measurement.internal.eo r0 = r26.d()     // Catch:{ all -> 0x07d7 }
            java.lang.String r6 = r3.f2601a     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ef r0 = r0.b(r6)     // Catch:{ all -> 0x07d7 }
            if (r0 != 0) goto L_0x06ac
            com.google.android.gms.measurement.internal.ef r0 = new com.google.android.gms.measurement.internal.ef     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ar r6 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            java.lang.String r7 = r3.f2601a     // Catch:{ all -> 0x07d7 }
            r0.<init>(r6, r7)     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ar r6 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ed r6 = r6.e()     // Catch:{ all -> 0x07d7 }
            java.lang.String r6 = r6.i()     // Catch:{ all -> 0x07d7 }
            r0.a(r6)     // Catch:{ all -> 0x07d7 }
            java.lang.String r6 = r3.k     // Catch:{ all -> 0x07d7 }
            r0.e(r6)     // Catch:{ all -> 0x07d7 }
            java.lang.String r6 = r3.f2602b     // Catch:{ all -> 0x07d7 }
            r0.b(r6)     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ar r6 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.z r6 = r6.b()     // Catch:{ all -> 0x07d7 }
            java.lang.String r7 = r3.f2601a     // Catch:{ all -> 0x07d7 }
            java.lang.String r6 = r6.b(r7)     // Catch:{ all -> 0x07d7 }
            r0.d(r6)     // Catch:{ all -> 0x07d7 }
            r0.f(r8)     // Catch:{ all -> 0x07d7 }
            r0.a(r8)     // Catch:{ all -> 0x07d7 }
            r0.b(r8)     // Catch:{ all -> 0x07d7 }
            java.lang.String r6 = r3.c     // Catch:{ all -> 0x07d7 }
            r0.f(r6)     // Catch:{ all -> 0x07d7 }
            long r6 = r3.j     // Catch:{ all -> 0x07d7 }
            r0.c(r6)     // Catch:{ all -> 0x07d7 }
            java.lang.String r6 = r3.d     // Catch:{ all -> 0x07d7 }
            r0.g(r6)     // Catch:{ all -> 0x07d7 }
            long r6 = r3.e     // Catch:{ all -> 0x07d7 }
            r0.d(r6)     // Catch:{ all -> 0x07d7 }
            long r6 = r3.f     // Catch:{ all -> 0x07d7 }
            r0.e(r6)     // Catch:{ all -> 0x07d7 }
            boolean r6 = r3.h     // Catch:{ all -> 0x07d7 }
            r0.a(r6)     // Catch:{ all -> 0x07d7 }
            long r6 = r3.l     // Catch:{ all -> 0x07d7 }
            r0.i(r6)     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.eo r6 = r26.d()     // Catch:{ all -> 0x07d7 }
            r6.a(r0)     // Catch:{ all -> 0x07d7 }
        L_0x06ac:
            java.lang.String r6 = r0.b()     // Catch:{ all -> 0x07d7 }
            r5.u = r6     // Catch:{ all -> 0x07d7 }
            java.lang.String r0 = r0.f()     // Catch:{ all -> 0x07d7 }
            r5.B = r0     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.eo r0 = r26.d()     // Catch:{ all -> 0x07d7 }
            java.lang.String r3 = r3.f2601a     // Catch:{ all -> 0x07d7 }
            java.util.List r0 = r0.a(r3)     // Catch:{ all -> 0x07d7 }
            int r3 = r0.size()     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.internal.measurement.cz[] r3 = new com.google.android.gms.internal.measurement.cz[r3]     // Catch:{ all -> 0x07d7 }
            r5.c = r3     // Catch:{ all -> 0x07d7 }
            r3 = 0
        L_0x06cb:
            int r6 = r0.size()     // Catch:{ all -> 0x07d7 }
            if (r3 >= r6) goto L_0x0704
            com.google.android.gms.internal.measurement.cz r6 = new com.google.android.gms.internal.measurement.cz     // Catch:{ all -> 0x07d7 }
            r6.<init>()     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.internal.measurement.cz[] r7 = r5.c     // Catch:{ all -> 0x07d7 }
            r7[r3] = r6     // Catch:{ all -> 0x07d7 }
            java.lang.Object r7 = r0.get(r3)     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ec r7 = (com.google.android.gms.measurement.internal.ec) r7     // Catch:{ all -> 0x07d7 }
            java.lang.String r7 = r7.c     // Catch:{ all -> 0x07d7 }
            r6.f2148b = r7     // Catch:{ all -> 0x07d7 }
            java.lang.Object r7 = r0.get(r3)     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ec r7 = (com.google.android.gms.measurement.internal.ec) r7     // Catch:{ all -> 0x07d7 }
            long r10 = r7.d     // Catch:{ all -> 0x07d7 }
            java.lang.Long r7 = java.lang.Long.valueOf(r10)     // Catch:{ all -> 0x07d7 }
            r6.f2147a = r7     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ea r7 = r26.f()     // Catch:{ all -> 0x07d7 }
            java.lang.Object r10 = r0.get(r3)     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ec r10 = (com.google.android.gms.measurement.internal.ec) r10     // Catch:{ all -> 0x07d7 }
            java.lang.Object r10 = r10.e     // Catch:{ all -> 0x07d7 }
            r7.a(r6, r10)     // Catch:{ all -> 0x07d7 }
            int r3 = r3 + 1
            goto L_0x06cb
        L_0x0704:
            com.google.android.gms.measurement.internal.eo r0 = r26.d()     // Catch:{ IOException -> 0x076e }
            long r5 = r0.a(r5)     // Catch:{ IOException -> 0x076e }
            com.google.android.gms.measurement.internal.eo r0 = r26.d()     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.zzad r3 = r4.e     // Catch:{ all -> 0x07d7 }
            if (r3 == 0) goto L_0x0764
            com.google.android.gms.measurement.internal.zzad r3 = r4.e     // Catch:{ all -> 0x07d7 }
            java.util.Iterator r3 = r3.iterator()     // Catch:{ all -> 0x07d7 }
        L_0x071a:
            boolean r7 = r3.hasNext()     // Catch:{ all -> 0x07d7 }
            if (r7 == 0) goto L_0x072e
            java.lang.Object r7 = r3.next()     // Catch:{ all -> 0x07d7 }
            java.lang.String r7 = (java.lang.String) r7     // Catch:{ all -> 0x07d7 }
            boolean r7 = r2.equals(r7)     // Catch:{ all -> 0x07d7 }
            if (r7 == 0) goto L_0x071a
        L_0x072c:
            r2 = 1
            goto L_0x0765
        L_0x072e:
            com.google.android.gms.measurement.internal.al r2 = r26.n()     // Catch:{ all -> 0x07d7 }
            java.lang.String r3 = r4.f2452a     // Catch:{ all -> 0x07d7 }
            java.lang.String r7 = r4.f2453b     // Catch:{ all -> 0x07d7 }
            boolean r2 = r2.c(r3, r7)     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.eo r10 = r26.d()     // Catch:{ all -> 0x07d7 }
            long r11 = r26.s()     // Catch:{ all -> 0x07d7 }
            java.lang.String r13 = r4.f2452a     // Catch:{ all -> 0x07d7 }
            r14 = 0
            r15 = 0
            r16 = 0
            r17 = 0
            r18 = 0
            com.google.android.gms.measurement.internal.ep r3 = r10.a(r11, r13, r14, r15, r16, r17, r18)     // Catch:{ all -> 0x07d7 }
            if (r2 == 0) goto L_0x0764
            long r2 = r3.e     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ar r7 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.el r7 = r7.e     // Catch:{ all -> 0x07d7 }
            java.lang.String r10 = r4.f2452a     // Catch:{ all -> 0x07d7 }
            int r7 = r7.a(r10)     // Catch:{ all -> 0x07d7 }
            long r10 = (long) r7     // Catch:{ all -> 0x07d7 }
            int r7 = (r2 > r10 ? 1 : (r2 == r10 ? 0 : -1))
            if (r7 >= 0) goto L_0x0764
            goto L_0x072c
        L_0x0764:
            r2 = 0
        L_0x0765:
            boolean r0 = r0.a(r4, r5, r2)     // Catch:{ all -> 0x07d7 }
            if (r0 == 0) goto L_0x0782
            r1.q = r8     // Catch:{ all -> 0x07d7 }
            goto L_0x0782
        L_0x076e:
            r0 = move-exception
            com.google.android.gms.measurement.internal.ar r2 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.o r2 = r2.q()     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.q r2 = r2.c     // Catch:{ all -> 0x07d7 }
            java.lang.String r3 = "Data loss. Failed to insert raw event metadata. appId"
            java.lang.String r5 = r5.o     // Catch:{ all -> 0x07d7 }
            java.lang.Object r5 = com.google.android.gms.measurement.internal.o.a(r5)     // Catch:{ all -> 0x07d7 }
            r2.a(r3, r5, r0)     // Catch:{ all -> 0x07d7 }
        L_0x0782:
            com.google.android.gms.measurement.internal.eo r0 = r26.d()     // Catch:{ all -> 0x07d7 }
            r0.u()     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.ar r0 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.o r0 = r0.q()     // Catch:{ all -> 0x07d7 }
            r2 = 2
            boolean r0 = r0.a(r2)     // Catch:{ all -> 0x07d7 }
            if (r0 == 0) goto L_0x07ad
            com.google.android.gms.measurement.internal.ar r0 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.o r0 = r0.q()     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.q r0 = r0.k     // Catch:{ all -> 0x07d7 }
            java.lang.String r2 = "Event recorded"
            com.google.android.gms.measurement.internal.ar r3 = r1.f2528b     // Catch:{ all -> 0x07d7 }
            com.google.android.gms.measurement.internal.m r3 = r3.f()     // Catch:{ all -> 0x07d7 }
            java.lang.String r3 = r3.a(r4)     // Catch:{ all -> 0x07d7 }
            r0.a(r2, r3)     // Catch:{ all -> 0x07d7 }
        L_0x07ad:
            com.google.android.gms.measurement.internal.eo r0 = r26.d()
            r0.v()
            r26.j()
            com.google.android.gms.measurement.internal.ar r0 = r1.f2528b
            com.google.android.gms.measurement.internal.o r0 = r0.q()
            com.google.android.gms.measurement.internal.q r0 = r0.k
            long r2 = java.lang.System.nanoTime()
            long r2 = r2 - r23
            r4 = 500000(0x7a120, double:2.47033E-318)
            long r2 = r2 + r4
            r4 = 1000000(0xf4240, double:4.940656E-318)
            long r2 = r2 / r4
            java.lang.Long r2 = java.lang.Long.valueOf(r2)
            java.lang.String r3 = "Background event processing time, ms"
            r0.a(r3, r2)
            return
        L_0x07d7:
            r0 = move-exception
            com.google.android.gms.measurement.internal.eo r2 = r26.d()
            r2.v()
            goto L_0x07e1
        L_0x07e0:
            throw r0
        L_0x07e1:
            goto L_0x07e0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.du.b(com.google.android.gms.measurement.internal.zzag, com.google.android.gms.measurement.internal.zzk):void");
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Can't wrap try/catch for region: R(2:92|93) */
    /* JADX WARNING: Code restructure failed: missing block: B:93:?, code lost:
        r14.f2528b.q().c.a("Failed to parse upload URL. Not uploading. appId", com.google.android.gms.measurement.internal.o.a(r4), r12);
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:92:0x0277 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void i() {
        /*
            r14 = this;
            r14.g()
            r14.h()
            r0 = 1
            r14.t = r0
            r1 = 0
            com.google.android.gms.measurement.internal.ar r2 = r14.f2528b     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.measurement.internal.cl r2 = r2.i()     // Catch:{ all -> 0x02b1 }
            java.lang.Boolean r2 = r2.c     // Catch:{ all -> 0x02b1 }
            if (r2 != 0) goto L_0x0027
            com.google.android.gms.measurement.internal.ar r0 = r14.f2528b     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.measurement.internal.o r0 = r0.q()     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.measurement.internal.q r0 = r0.f     // Catch:{ all -> 0x02b1 }
            java.lang.String r2 = "Upload data called on the client side before use of service was decided"
            r0.a(r2)     // Catch:{ all -> 0x02b1 }
            r14.t = r1
            r14.u()
            return
        L_0x0027:
            boolean r2 = r2.booleanValue()     // Catch:{ all -> 0x02b1 }
            if (r2 == 0) goto L_0x0040
            com.google.android.gms.measurement.internal.ar r0 = r14.f2528b     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.measurement.internal.o r0 = r0.q()     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.measurement.internal.q r0 = r0.c     // Catch:{ all -> 0x02b1 }
            java.lang.String r2 = "Upload called in the client side when service should be used"
            r0.a(r2)     // Catch:{ all -> 0x02b1 }
            r14.t = r1
            r14.u()
            return
        L_0x0040:
            long r2 = r14.q     // Catch:{ all -> 0x02b1 }
            r4 = 0
            int r6 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r6 <= 0) goto L_0x0051
            r14.j()     // Catch:{ all -> 0x02b1 }
            r14.t = r1
            r14.u()
            return
        L_0x0051:
            r14.g()     // Catch:{ all -> 0x02b1 }
            java.util.List<java.lang.Long> r2 = r14.w     // Catch:{ all -> 0x02b1 }
            if (r2 == 0) goto L_0x005a
            r2 = 1
            goto L_0x005b
        L_0x005a:
            r2 = 0
        L_0x005b:
            if (r2 == 0) goto L_0x0070
            com.google.android.gms.measurement.internal.ar r0 = r14.f2528b     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.measurement.internal.o r0 = r0.q()     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.measurement.internal.q r0 = r0.k     // Catch:{ all -> 0x02b1 }
            java.lang.String r2 = "Uploading requested multiple times"
            r0.a(r2)     // Catch:{ all -> 0x02b1 }
            r14.t = r1
            r14.u()
            return
        L_0x0070:
            com.google.android.gms.measurement.internal.s r2 = r14.c()     // Catch:{ all -> 0x02b1 }
            boolean r2 = r2.e()     // Catch:{ all -> 0x02b1 }
            if (r2 != 0) goto L_0x0090
            com.google.android.gms.measurement.internal.ar r0 = r14.f2528b     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.measurement.internal.o r0 = r0.q()     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.measurement.internal.q r0 = r0.k     // Catch:{ all -> 0x02b1 }
            java.lang.String r2 = "Network not connected, ignoring upload request"
            r0.a(r2)     // Catch:{ all -> 0x02b1 }
            r14.j()     // Catch:{ all -> 0x02b1 }
            r14.t = r1
            r14.u()
            return
        L_0x0090:
            com.google.android.gms.measurement.internal.ar r2 = r14.f2528b     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.common.util.e r2 = r2.l()     // Catch:{ all -> 0x02b1 }
            long r2 = r2.a()     // Catch:{ all -> 0x02b1 }
            long r6 = com.google.android.gms.measurement.internal.el.h()     // Catch:{ all -> 0x02b1 }
            long r6 = r2 - r6
            r14.a(r6)     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.measurement.internal.ar r6 = r14.f2528b     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.measurement.internal.z r6 = r6.b()     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.measurement.internal.ab r6 = r6.d     // Catch:{ all -> 0x02b1 }
            long r6 = r6.a()     // Catch:{ all -> 0x02b1 }
            int r8 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r8 == 0) goto L_0x00ca
            com.google.android.gms.measurement.internal.ar r4 = r14.f2528b     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.measurement.internal.o r4 = r4.q()     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.measurement.internal.q r4 = r4.j     // Catch:{ all -> 0x02b1 }
            java.lang.String r5 = "Uploading events. Elapsed time since last upload attempt (ms)"
            long r6 = r2 - r6
            long r6 = java.lang.Math.abs(r6)     // Catch:{ all -> 0x02b1 }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x02b1 }
            r4.a(r5, r6)     // Catch:{ all -> 0x02b1 }
        L_0x00ca:
            com.google.android.gms.measurement.internal.eo r4 = r14.d()     // Catch:{ all -> 0x02b1 }
            java.lang.String r4 = r4.x()     // Catch:{ all -> 0x02b1 }
            boolean r5 = android.text.TextUtils.isEmpty(r4)     // Catch:{ all -> 0x02b1 }
            r6 = -1
            if (r5 != 0) goto L_0x0289
            long r8 = r14.y     // Catch:{ all -> 0x02b1 }
            int r5 = (r8 > r6 ? 1 : (r8 == r6 ? 0 : -1))
            if (r5 != 0) goto L_0x00ea
            com.google.android.gms.measurement.internal.eo r5 = r14.d()     // Catch:{ all -> 0x02b1 }
            long r5 = r5.E()     // Catch:{ all -> 0x02b1 }
            r14.y = r5     // Catch:{ all -> 0x02b1 }
        L_0x00ea:
            com.google.android.gms.measurement.internal.ar r5 = r14.f2528b     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.measurement.internal.el r5 = r5.e     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.measurement.internal.h$a<java.lang.Integer> r6 = com.google.android.gms.measurement.internal.h.o     // Catch:{ all -> 0x02b1 }
            int r5 = r5.b(r4, r6)     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.measurement.internal.ar r6 = r14.f2528b     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.measurement.internal.el r6 = r6.e     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.measurement.internal.h$a<java.lang.Integer> r7 = com.google.android.gms.measurement.internal.h.p     // Catch:{ all -> 0x02b1 }
            int r6 = r6.b(r4, r7)     // Catch:{ all -> 0x02b1 }
            int r6 = java.lang.Math.max(r1, r6)     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.measurement.internal.eo r7 = r14.d()     // Catch:{ all -> 0x02b1 }
            java.util.List r5 = r7.a(r4, r5, r6)     // Catch:{ all -> 0x02b1 }
            boolean r6 = r5.isEmpty()     // Catch:{ all -> 0x02b1 }
            if (r6 != 0) goto L_0x02ab
            java.util.Iterator r6 = r5.iterator()     // Catch:{ all -> 0x02b1 }
        L_0x0114:
            boolean r7 = r6.hasNext()     // Catch:{ all -> 0x02b1 }
            r8 = 0
            if (r7 == 0) goto L_0x0130
            java.lang.Object r7 = r6.next()     // Catch:{ all -> 0x02b1 }
            android.util.Pair r7 = (android.util.Pair) r7     // Catch:{ all -> 0x02b1 }
            java.lang.Object r7 = r7.first     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.internal.measurement.cw r7 = (com.google.android.gms.internal.measurement.cw) r7     // Catch:{ all -> 0x02b1 }
            java.lang.String r9 = r7.s     // Catch:{ all -> 0x02b1 }
            boolean r9 = android.text.TextUtils.isEmpty(r9)     // Catch:{ all -> 0x02b1 }
            if (r9 != 0) goto L_0x0114
            java.lang.String r6 = r7.s     // Catch:{ all -> 0x02b1 }
            goto L_0x0131
        L_0x0130:
            r6 = r8
        L_0x0131:
            if (r6 == 0) goto L_0x015c
            r7 = 0
        L_0x0134:
            int r9 = r5.size()     // Catch:{ all -> 0x02b1 }
            if (r7 >= r9) goto L_0x015c
            java.lang.Object r9 = r5.get(r7)     // Catch:{ all -> 0x02b1 }
            android.util.Pair r9 = (android.util.Pair) r9     // Catch:{ all -> 0x02b1 }
            java.lang.Object r9 = r9.first     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.internal.measurement.cw r9 = (com.google.android.gms.internal.measurement.cw) r9     // Catch:{ all -> 0x02b1 }
            java.lang.String r10 = r9.s     // Catch:{ all -> 0x02b1 }
            boolean r10 = android.text.TextUtils.isEmpty(r10)     // Catch:{ all -> 0x02b1 }
            if (r10 != 0) goto L_0x0159
            java.lang.String r9 = r9.s     // Catch:{ all -> 0x02b1 }
            boolean r9 = r9.equals(r6)     // Catch:{ all -> 0x02b1 }
            if (r9 != 0) goto L_0x0159
            java.util.List r5 = r5.subList(r1, r7)     // Catch:{ all -> 0x02b1 }
            goto L_0x015c
        L_0x0159:
            int r7 = r7 + 1
            goto L_0x0134
        L_0x015c:
            com.google.android.gms.internal.measurement.cv r6 = new com.google.android.gms.internal.measurement.cv     // Catch:{ all -> 0x02b1 }
            r6.<init>()     // Catch:{ all -> 0x02b1 }
            int r7 = r5.size()     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.internal.measurement.cw[] r7 = new com.google.android.gms.internal.measurement.cw[r7]     // Catch:{ all -> 0x02b1 }
            r6.f2140a = r7     // Catch:{ all -> 0x02b1 }
            java.util.ArrayList r7 = new java.util.ArrayList     // Catch:{ all -> 0x02b1 }
            int r9 = r5.size()     // Catch:{ all -> 0x02b1 }
            r7.<init>(r9)     // Catch:{ all -> 0x02b1 }
            boolean r9 = com.google.android.gms.measurement.internal.el.j()     // Catch:{ all -> 0x02b1 }
            if (r9 == 0) goto L_0x0184
            com.google.android.gms.measurement.internal.ar r9 = r14.f2528b     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.measurement.internal.el r9 = r9.e     // Catch:{ all -> 0x02b1 }
            boolean r9 = r9.c(r4)     // Catch:{ all -> 0x02b1 }
            if (r9 == 0) goto L_0x0184
            r9 = 1
            goto L_0x0185
        L_0x0184:
            r9 = 0
        L_0x0185:
            r10 = 0
        L_0x0186:
            com.google.android.gms.internal.measurement.cw[] r11 = r6.f2140a     // Catch:{ all -> 0x02b1 }
            int r11 = r11.length     // Catch:{ all -> 0x02b1 }
            if (r10 >= r11) goto L_0x01d1
            com.google.android.gms.internal.measurement.cw[] r11 = r6.f2140a     // Catch:{ all -> 0x02b1 }
            java.lang.Object r12 = r5.get(r10)     // Catch:{ all -> 0x02b1 }
            android.util.Pair r12 = (android.util.Pair) r12     // Catch:{ all -> 0x02b1 }
            java.lang.Object r12 = r12.first     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.internal.measurement.cw r12 = (com.google.android.gms.internal.measurement.cw) r12     // Catch:{ all -> 0x02b1 }
            r11[r10] = r12     // Catch:{ all -> 0x02b1 }
            java.lang.Object r11 = r5.get(r10)     // Catch:{ all -> 0x02b1 }
            android.util.Pair r11 = (android.util.Pair) r11     // Catch:{ all -> 0x02b1 }
            java.lang.Object r11 = r11.second     // Catch:{ all -> 0x02b1 }
            java.lang.Long r11 = (java.lang.Long) r11     // Catch:{ all -> 0x02b1 }
            r7.add(r11)     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.internal.measurement.cw[] r11 = r6.f2140a     // Catch:{ all -> 0x02b1 }
            r11 = r11[r10]     // Catch:{ all -> 0x02b1 }
            r12 = 14710(0x3976, double:7.2677E-320)
            java.lang.Long r12 = java.lang.Long.valueOf(r12)     // Catch:{ all -> 0x02b1 }
            r11.r = r12     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.internal.measurement.cw[] r11 = r6.f2140a     // Catch:{ all -> 0x02b1 }
            r11 = r11[r10]     // Catch:{ all -> 0x02b1 }
            java.lang.Long r12 = java.lang.Long.valueOf(r2)     // Catch:{ all -> 0x02b1 }
            r11.d = r12     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.internal.measurement.cw[] r11 = r6.f2140a     // Catch:{ all -> 0x02b1 }
            r11 = r11[r10]     // Catch:{ all -> 0x02b1 }
            java.lang.Boolean r12 = java.lang.Boolean.valueOf(r1)     // Catch:{ all -> 0x02b1 }
            r11.z = r12     // Catch:{ all -> 0x02b1 }
            if (r9 != 0) goto L_0x01ce
            com.google.android.gms.internal.measurement.cw[] r11 = r6.f2140a     // Catch:{ all -> 0x02b1 }
            r11 = r11[r10]     // Catch:{ all -> 0x02b1 }
            r11.G = r8     // Catch:{ all -> 0x02b1 }
        L_0x01ce:
            int r10 = r10 + 1
            goto L_0x0186
        L_0x01d1:
            com.google.android.gms.measurement.internal.ar r5 = r14.f2528b     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.measurement.internal.o r5 = r5.q()     // Catch:{ all -> 0x02b1 }
            r9 = 2
            boolean r5 = r5.a(r9)     // Catch:{ all -> 0x02b1 }
            if (r5 == 0) goto L_0x01e6
            com.google.android.gms.measurement.internal.ea r5 = r14.f()     // Catch:{ all -> 0x02b1 }
            java.lang.String r8 = r5.b(r6)     // Catch:{ all -> 0x02b1 }
        L_0x01e6:
            com.google.android.gms.measurement.internal.ea r5 = r14.f()     // Catch:{ all -> 0x02b1 }
            byte[] r9 = r5.a(r6)     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.measurement.internal.h$a<java.lang.String> r5 = com.google.android.gms.measurement.internal.h.y     // Catch:{ all -> 0x02b1 }
            java.lang.Object r5 = r5.a()     // Catch:{ all -> 0x02b1 }
            r12 = r5
            java.lang.String r12 = (java.lang.String) r12     // Catch:{ all -> 0x02b1 }
            java.net.URL r10 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0277 }
            r10.<init>(r12)     // Catch:{ MalformedURLException -> 0x0277 }
            boolean r5 = r7.isEmpty()     // Catch:{ MalformedURLException -> 0x0277 }
            if (r5 != 0) goto L_0x0204
            r5 = 1
            goto L_0x0205
        L_0x0204:
            r5 = 0
        L_0x0205:
            com.google.android.gms.common.internal.l.b(r5)     // Catch:{ MalformedURLException -> 0x0277 }
            java.util.List<java.lang.Long> r5 = r14.w     // Catch:{ MalformedURLException -> 0x0277 }
            if (r5 == 0) goto L_0x021a
            com.google.android.gms.measurement.internal.ar r5 = r14.f2528b     // Catch:{ MalformedURLException -> 0x0277 }
            com.google.android.gms.measurement.internal.o r5 = r5.q()     // Catch:{ MalformedURLException -> 0x0277 }
            com.google.android.gms.measurement.internal.q r5 = r5.c     // Catch:{ MalformedURLException -> 0x0277 }
            java.lang.String r7 = "Set uploading progress before finishing the previous upload"
            r5.a(r7)     // Catch:{ MalformedURLException -> 0x0277 }
            goto L_0x0221
        L_0x021a:
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch:{ MalformedURLException -> 0x0277 }
            r5.<init>(r7)     // Catch:{ MalformedURLException -> 0x0277 }
            r14.w = r5     // Catch:{ MalformedURLException -> 0x0277 }
        L_0x0221:
            com.google.android.gms.measurement.internal.ar r5 = r14.f2528b     // Catch:{ MalformedURLException -> 0x0277 }
            com.google.android.gms.measurement.internal.z r5 = r5.b()     // Catch:{ MalformedURLException -> 0x0277 }
            com.google.android.gms.measurement.internal.ab r5 = r5.e     // Catch:{ MalformedURLException -> 0x0277 }
            r5.a(r2)     // Catch:{ MalformedURLException -> 0x0277 }
            java.lang.String r2 = "?"
            com.google.android.gms.internal.measurement.cw[] r3 = r6.f2140a     // Catch:{ MalformedURLException -> 0x0277 }
            int r3 = r3.length     // Catch:{ MalformedURLException -> 0x0277 }
            if (r3 <= 0) goto L_0x0239
            com.google.android.gms.internal.measurement.cw[] r2 = r6.f2140a     // Catch:{ MalformedURLException -> 0x0277 }
            r2 = r2[r1]     // Catch:{ MalformedURLException -> 0x0277 }
            java.lang.String r2 = r2.o     // Catch:{ MalformedURLException -> 0x0277 }
        L_0x0239:
            com.google.android.gms.measurement.internal.ar r3 = r14.f2528b     // Catch:{ MalformedURLException -> 0x0277 }
            com.google.android.gms.measurement.internal.o r3 = r3.q()     // Catch:{ MalformedURLException -> 0x0277 }
            com.google.android.gms.measurement.internal.q r3 = r3.k     // Catch:{ MalformedURLException -> 0x0277 }
            java.lang.String r5 = "Uploading data. app, uncompressed size, data"
            int r6 = r9.length     // Catch:{ MalformedURLException -> 0x0277 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ MalformedURLException -> 0x0277 }
            r3.a(r5, r2, r6, r8)     // Catch:{ MalformedURLException -> 0x0277 }
            r14.s = r0     // Catch:{ MalformedURLException -> 0x0277 }
            com.google.android.gms.measurement.internal.s r6 = r14.c()     // Catch:{ MalformedURLException -> 0x0277 }
            com.google.android.gms.measurement.internal.dw r11 = new com.google.android.gms.measurement.internal.dw     // Catch:{ MalformedURLException -> 0x0277 }
            r11.<init>(r14, r4)     // Catch:{ MalformedURLException -> 0x0277 }
            r6.c()     // Catch:{ MalformedURLException -> 0x0277 }
            r6.j()     // Catch:{ MalformedURLException -> 0x0277 }
            com.google.android.gms.common.internal.l.a(r10)     // Catch:{ MalformedURLException -> 0x0277 }
            com.google.android.gms.common.internal.l.a(r9)     // Catch:{ MalformedURLException -> 0x0277 }
            com.google.android.gms.common.internal.l.a(r11)     // Catch:{ MalformedURLException -> 0x0277 }
            com.google.android.gms.measurement.internal.am r0 = r6.p()     // Catch:{ MalformedURLException -> 0x0277 }
            com.google.android.gms.measurement.internal.w r2 = new com.google.android.gms.measurement.internal.w     // Catch:{ MalformedURLException -> 0x0277 }
            r3 = 0
            r5 = r2
            r7 = r4
            r8 = r10
            r10 = r3
            r5.<init>(r6, r7, r8, r9, r10, r11)     // Catch:{ MalformedURLException -> 0x0277 }
            r0.b(r2)     // Catch:{ MalformedURLException -> 0x0277 }
            goto L_0x02ab
        L_0x0277:
            com.google.android.gms.measurement.internal.ar r0 = r14.f2528b     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.measurement.internal.o r0 = r0.q()     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.measurement.internal.q r0 = r0.c     // Catch:{ all -> 0x02b1 }
            java.lang.String r2 = "Failed to parse upload URL. Not uploading. appId"
            java.lang.Object r3 = com.google.android.gms.measurement.internal.o.a(r4)     // Catch:{ all -> 0x02b1 }
            r0.a(r2, r3, r12)     // Catch:{ all -> 0x02b1 }
            goto L_0x02ab
        L_0x0289:
            r14.y = r6     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.measurement.internal.eo r0 = r14.d()     // Catch:{ all -> 0x02b1 }
            long r4 = com.google.android.gms.measurement.internal.el.h()     // Catch:{ all -> 0x02b1 }
            long r2 = r2 - r4
            java.lang.String r0 = r0.a(r2)     // Catch:{ all -> 0x02b1 }
            boolean r2 = android.text.TextUtils.isEmpty(r0)     // Catch:{ all -> 0x02b1 }
            if (r2 != 0) goto L_0x02ab
            com.google.android.gms.measurement.internal.eo r2 = r14.d()     // Catch:{ all -> 0x02b1 }
            com.google.android.gms.measurement.internal.ef r0 = r2.b(r0)     // Catch:{ all -> 0x02b1 }
            if (r0 == 0) goto L_0x02ab
            r14.a(r0)     // Catch:{ all -> 0x02b1 }
        L_0x02ab:
            r14.t = r1
            r14.u()
            return
        L_0x02b1:
            r0 = move-exception
            r14.t = r1
            r14.u()
            goto L_0x02b9
        L_0x02b8:
            throw r0
        L_0x02b9:
            goto L_0x02b8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.du.i():void");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:47:0x00e1 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:24:0x0081 */
    /* JADX WARN: Type inference failed for: r12v0 */
    /* JADX WARN: Type inference failed for: r12v108, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r12v109 */
    /* JADX WARN: Type inference failed for: r12v110 */
    /* JADX WARN: Type inference failed for: r12v114, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r12v121 */
    /* JADX WARN: Type inference failed for: r12v123, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r12v124 */
    /* JADX WARN: Type inference failed for: r12v126 */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0042 A[ExcHandler: all (th java.lang.Throwable), PHI: r12 
      PHI: (r12v121 ?) = (r12v114 ?), (r12v114 ?), (r12v114 ?), (r12v123 ?), (r12v123 ?), (r12v123 ?), (r12v123 ?), (r12v0 ?), (r12v0 ?) binds: [B:47:0x00e1, B:53:0x00ee, B:54:?, B:24:0x0081, B:30:0x008e, B:32:0x0092, B:33:?, B:9:0x0033, B:10:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:9:0x0033] */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x0262 A[SYNTHETIC, Splitter:B:128:0x0262] */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x0269 A[Catch:{ IOException -> 0x0229, all -> 0x0d5f }] */
    /* JADX WARNING: Removed duplicated region for block: B:138:0x0277 A[Catch:{ IOException -> 0x0229, all -> 0x0d5f }] */
    /* JADX WARNING: Removed duplicated region for block: B:181:0x0398 A[Catch:{ IOException -> 0x0229, all -> 0x0d5f }] */
    /* JADX WARNING: Removed duplicated region for block: B:188:0x03a3 A[Catch:{ IOException -> 0x0229, all -> 0x0d5f }] */
    /* JADX WARNING: Removed duplicated region for block: B:189:0x03a4 A[Catch:{ IOException -> 0x0229, all -> 0x0d5f }] */
    /* JADX WARNING: Removed duplicated region for block: B:294:0x064c A[Catch:{ IOException -> 0x0229, all -> 0x0d5f }] */
    /* JADX WARNING: Removed duplicated region for block: B:324:0x06cd A[Catch:{ IOException -> 0x0229, all -> 0x0d5f }] */
    /* JADX WARNING: Removed duplicated region for block: B:367:0x0810 A[Catch:{ IOException -> 0x0229, all -> 0x0d5f }] */
    /* JADX WARNING: Removed duplicated region for block: B:373:0x0828 A[Catch:{ IOException -> 0x0229, all -> 0x0d5f }] */
    /* JADX WARNING: Removed duplicated region for block: B:376:0x0848 A[Catch:{ IOException -> 0x0229, all -> 0x0d5f }] */
    /* JADX WARNING: Removed duplicated region for block: B:421:0x098e A[Catch:{ IOException -> 0x0229, all -> 0x0d5f }] */
    /* JADX WARNING: Removed duplicated region for block: B:422:0x099d A[Catch:{ IOException -> 0x0229, all -> 0x0d5f }] */
    /* JADX WARNING: Removed duplicated region for block: B:424:0x09a0 A[Catch:{ IOException -> 0x0229, all -> 0x0d5f }] */
    /* JADX WARNING: Removed duplicated region for block: B:426:0x09bd A[SYNTHETIC, Splitter:B:426:0x09bd] */
    /* JADX WARNING: Removed duplicated region for block: B:481:0x0adf A[Catch:{ all -> 0x0d3e }] */
    /* JADX WARNING: Removed duplicated region for block: B:485:0x0b2b A[Catch:{ all -> 0x0d3e }] */
    /* JADX WARNING: Removed duplicated region for block: B:562:0x0d42  */
    /* JADX WARNING: Removed duplicated region for block: B:570:0x0d59 A[SYNTHETIC, Splitter:B:570:0x0d59] */
    /* JADX WARNING: Removed duplicated region for block: B:607:0x0825 A[SYNTHETIC] */
    private final boolean a(long r44) {
        /*
            r43 = this;
            r1 = r43
            java.lang.String r2 = "_lte"
            com.google.android.gms.measurement.internal.eo r3 = r43.d()
            r3.e()
            com.google.android.gms.measurement.internal.du$a r3 = new com.google.android.gms.measurement.internal.du$a     // Catch:{ all -> 0x0d5f }
            r4 = 0
            r3.<init>(r1, r4)     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.eo r5 = r43.d()     // Catch:{ all -> 0x0d5f }
            long r6 = r1.y     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.common.internal.l.a(r3)     // Catch:{ all -> 0x0d5f }
            r5.c()     // Catch:{ all -> 0x0d5f }
            r5.j()     // Catch:{ all -> 0x0d5f }
            r9 = -1
            r11 = 2
            r12 = 0
            r13 = 1
            android.database.sqlite.SQLiteDatabase r15 = r5.w()     // Catch:{ SQLiteException -> 0x024d, all -> 0x0247 }
            boolean r14 = android.text.TextUtils.isEmpty(r12)     // Catch:{ SQLiteException -> 0x024d, all -> 0x0247 }
            if (r14 == 0) goto L_0x00a3
            int r14 = (r6 > r9 ? 1 : (r6 == r9 ? 0 : -1))
            if (r14 == 0) goto L_0x004c
            java.lang.String[] r14 = new java.lang.String[r11]     // Catch:{ SQLiteException -> 0x0047, all -> 0x0042 }
            java.lang.String r16 = java.lang.String.valueOf(r6)     // Catch:{ SQLiteException -> 0x0047, all -> 0x0042 }
            r14[r4] = r16     // Catch:{ SQLiteException -> 0x0047, all -> 0x0042 }
            java.lang.String r16 = java.lang.String.valueOf(r44)     // Catch:{ SQLiteException -> 0x0047, all -> 0x0042 }
            r14[r13] = r16     // Catch:{ SQLiteException -> 0x0047, all -> 0x0042 }
            goto L_0x0054
        L_0x0042:
            r0 = move-exception
            r5 = r1
            r9 = r12
            goto L_0x024a
        L_0x0047:
            r0 = move-exception
            r6 = r0
            r9 = r12
            goto L_0x0251
        L_0x004c:
            java.lang.String[] r14 = new java.lang.String[r13]     // Catch:{ SQLiteException -> 0x024d, all -> 0x0247 }
            java.lang.String r16 = java.lang.String.valueOf(r44)     // Catch:{ SQLiteException -> 0x024d, all -> 0x0247 }
            r14[r4] = r16     // Catch:{ SQLiteException -> 0x024d, all -> 0x0247 }
        L_0x0054:
            int r16 = (r6 > r9 ? 1 : (r6 == r9 ? 0 : -1))
            if (r16 == 0) goto L_0x005b
            java.lang.String r16 = "rowid <= ? and "
            goto L_0x005d
        L_0x005b:
            java.lang.String r16 = ""
        L_0x005d:
            r44 = r16
            int r8 = r44.length()     // Catch:{ SQLiteException -> 0x024d, all -> 0x0247 }
            int r8 = r8 + 148
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x024d, all -> 0x0247 }
            r12.<init>(r8)     // Catch:{ SQLiteException -> 0x024d, all -> 0x0247 }
            java.lang.String r8 = "select app_id, metadata_fingerprint from raw_events where "
            r12.append(r8)     // Catch:{ SQLiteException -> 0x024d, all -> 0x0247 }
            r8 = r44
            r12.append(r8)     // Catch:{ SQLiteException -> 0x024d, all -> 0x0247 }
            java.lang.String r8 = "app_id in (select app_id from apps where config_fetched_time >= ?) order by rowid limit 1;"
            r12.append(r8)     // Catch:{ SQLiteException -> 0x024d, all -> 0x0247 }
            java.lang.String r8 = r12.toString()     // Catch:{ SQLiteException -> 0x024d, all -> 0x0247 }
            android.database.Cursor r12 = r15.rawQuery(r8, r14)     // Catch:{ SQLiteException -> 0x024d, all -> 0x0247 }
            boolean r8 = r12.moveToFirst()     // Catch:{ SQLiteException -> 0x0243, all -> 0x0042 }
            if (r8 != 0) goto L_0x008e
            if (r12 == 0) goto L_0x0265
            r12.close()     // Catch:{ all -> 0x0d5f }
            goto L_0x0265
        L_0x008e:
            java.lang.String r8 = r12.getString(r4)     // Catch:{ SQLiteException -> 0x0243, all -> 0x0042 }
            java.lang.String r14 = r12.getString(r13)     // Catch:{ SQLiteException -> 0x009d, all -> 0x0042 }
            r12.close()     // Catch:{ SQLiteException -> 0x009d, all -> 0x0042 }
            r9 = r12
            r12 = r8
            r8 = r14
            goto L_0x00f8
        L_0x009d:
            r0 = move-exception
            r6 = r0
            r9 = r12
            r12 = r8
            goto L_0x0251
        L_0x00a3:
            int r8 = (r6 > r9 ? 1 : (r6 == r9 ? 0 : -1))
            if (r8 == 0) goto L_0x00b3
            java.lang.String[] r8 = new java.lang.String[r11]     // Catch:{ SQLiteException -> 0x024d, all -> 0x0247 }
            r12 = 0
            r8[r4] = r12     // Catch:{ SQLiteException -> 0x024d, all -> 0x0247 }
            java.lang.String r12 = java.lang.String.valueOf(r6)     // Catch:{ SQLiteException -> 0x024d, all -> 0x0247 }
            r8[r13] = r12     // Catch:{ SQLiteException -> 0x024d, all -> 0x0247 }
            goto L_0x00b8
        L_0x00b3:
            java.lang.String[] r8 = new java.lang.String[r13]     // Catch:{ SQLiteException -> 0x024d, all -> 0x0247 }
            r12 = 0
            r8[r4] = r12     // Catch:{ SQLiteException -> 0x024d, all -> 0x0247 }
        L_0x00b8:
            int r12 = (r6 > r9 ? 1 : (r6 == r9 ? 0 : -1))
            if (r12 == 0) goto L_0x00bf
            java.lang.String r12 = " and rowid <= ?"
            goto L_0x00c1
        L_0x00bf:
            java.lang.String r12 = ""
        L_0x00c1:
            int r14 = r12.length()     // Catch:{ SQLiteException -> 0x024d, all -> 0x0247 }
            int r14 = r14 + 84
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x024d, all -> 0x0247 }
            r9.<init>(r14)     // Catch:{ SQLiteException -> 0x024d, all -> 0x0247 }
            java.lang.String r10 = "select metadata_fingerprint from raw_events where app_id = ?"
            r9.append(r10)     // Catch:{ SQLiteException -> 0x024d, all -> 0x0247 }
            r9.append(r12)     // Catch:{ SQLiteException -> 0x024d, all -> 0x0247 }
            java.lang.String r10 = " order by rowid limit 1;"
            r9.append(r10)     // Catch:{ SQLiteException -> 0x024d, all -> 0x0247 }
            java.lang.String r9 = r9.toString()     // Catch:{ SQLiteException -> 0x024d, all -> 0x0247 }
            android.database.Cursor r12 = r15.rawQuery(r9, r8)     // Catch:{ SQLiteException -> 0x024d, all -> 0x0247 }
            boolean r8 = r12.moveToFirst()     // Catch:{ SQLiteException -> 0x0243, all -> 0x0042 }
            if (r8 != 0) goto L_0x00ee
            if (r12 == 0) goto L_0x0265
            r12.close()     // Catch:{ all -> 0x0d5f }
            goto L_0x0265
        L_0x00ee:
            java.lang.String r14 = r12.getString(r4)     // Catch:{ SQLiteException -> 0x0243, all -> 0x0042 }
            r12.close()     // Catch:{ SQLiteException -> 0x0243, all -> 0x0042 }
            r9 = r12
            r8 = r14
            r12 = 0
        L_0x00f8:
            java.lang.String r10 = "raw_events_metadata"
            java.lang.String[] r14 = new java.lang.String[r13]     // Catch:{ SQLiteException -> 0x0240 }
            java.lang.String r16 = "metadata"
            r14[r4] = r16     // Catch:{ SQLiteException -> 0x0240 }
            java.lang.String r17 = "app_id = ? and metadata_fingerprint = ?"
            java.lang.String[] r13 = new java.lang.String[r11]     // Catch:{ SQLiteException -> 0x0240 }
            r13[r4] = r12     // Catch:{ SQLiteException -> 0x0240 }
            r16 = 1
            r13[r16] = r8     // Catch:{ SQLiteException -> 0x0240 }
            r19 = 0
            r20 = 0
            java.lang.String r21 = "rowid"
            java.lang.String r22 = "2"
            r16 = r14
            r14 = r15
            r24 = r15
            r15 = r10
            r18 = r13
            android.database.Cursor r9 = r14.query(r15, r16, r17, r18, r19, r20, r21, r22)     // Catch:{ SQLiteException -> 0x0240 }
            boolean r10 = r9.moveToFirst()     // Catch:{ SQLiteException -> 0x0240 }
            if (r10 != 0) goto L_0x013a
            com.google.android.gms.measurement.internal.o r6 = r5.q()     // Catch:{ SQLiteException -> 0x0240 }
            com.google.android.gms.measurement.internal.q r6 = r6.c     // Catch:{ SQLiteException -> 0x0240 }
            java.lang.String r7 = "Raw event metadata record is missing. appId"
            java.lang.Object r8 = com.google.android.gms.measurement.internal.o.a(r12)     // Catch:{ SQLiteException -> 0x0240 }
            r6.a(r7, r8)     // Catch:{ SQLiteException -> 0x0240 }
            if (r9 == 0) goto L_0x0265
            r9.close()     // Catch:{ all -> 0x0d5f }
            goto L_0x0265
        L_0x013a:
            byte[] r10 = r9.getBlob(r4)     // Catch:{ SQLiteException -> 0x0240 }
            int r13 = r10.length     // Catch:{ SQLiteException -> 0x0240 }
            com.google.android.gms.internal.measurement.iw r10 = com.google.android.gms.internal.measurement.iw.a(r10, r13)     // Catch:{ SQLiteException -> 0x0240 }
            com.google.android.gms.internal.measurement.cw r13 = new com.google.android.gms.internal.measurement.cw     // Catch:{ SQLiteException -> 0x0240 }
            r13.<init>()     // Catch:{ SQLiteException -> 0x0240 }
            r13.a(r10)     // Catch:{ IOException -> 0x0229 }
            boolean r10 = r9.moveToNext()     // Catch:{ SQLiteException -> 0x0240 }
            if (r10 == 0) goto L_0x0160
            com.google.android.gms.measurement.internal.o r10 = r5.q()     // Catch:{ SQLiteException -> 0x0240 }
            com.google.android.gms.measurement.internal.q r10 = r10.f     // Catch:{ SQLiteException -> 0x0240 }
            java.lang.String r14 = "Get multiple raw event metadata records, expected one. appId"
            java.lang.Object r15 = com.google.android.gms.measurement.internal.o.a(r12)     // Catch:{ SQLiteException -> 0x0240 }
            r10.a(r14, r15)     // Catch:{ SQLiteException -> 0x0240 }
        L_0x0160:
            r9.close()     // Catch:{ SQLiteException -> 0x0240 }
            r3.a(r13)     // Catch:{ SQLiteException -> 0x0240 }
            r13 = -1
            int r10 = (r6 > r13 ? 1 : (r6 == r13 ? 0 : -1))
            if (r10 == 0) goto L_0x0181
            java.lang.String r10 = "app_id = ? and metadata_fingerprint = ? and rowid <= ?"
            r13 = 3
            java.lang.String[] r14 = new java.lang.String[r13]     // Catch:{ SQLiteException -> 0x0240 }
            r14[r4] = r12     // Catch:{ SQLiteException -> 0x0240 }
            r13 = 1
            r14[r13] = r8     // Catch:{ SQLiteException -> 0x0240 }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ SQLiteException -> 0x0240 }
            r14[r11] = r6     // Catch:{ SQLiteException -> 0x0240 }
            r17 = r10
            r18 = r14
            goto L_0x018e
        L_0x0181:
            java.lang.String r6 = "app_id = ? and metadata_fingerprint = ?"
            java.lang.String[] r7 = new java.lang.String[r11]     // Catch:{ SQLiteException -> 0x0240 }
            r7[r4] = r12     // Catch:{ SQLiteException -> 0x0240 }
            r10 = 1
            r7[r10] = r8     // Catch:{ SQLiteException -> 0x0240 }
            r17 = r6
            r18 = r7
        L_0x018e:
            java.lang.String r15 = "raw_events"
            r6 = 4
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ SQLiteException -> 0x0240 }
            java.lang.String r7 = "rowid"
            r6[r4] = r7     // Catch:{ SQLiteException -> 0x0240 }
            java.lang.String r7 = "name"
            r8 = 1
            r6[r8] = r7     // Catch:{ SQLiteException -> 0x0240 }
            java.lang.String r7 = "timestamp"
            r6[r11] = r7     // Catch:{ SQLiteException -> 0x0240 }
            java.lang.String r7 = "data"
            r8 = 3
            r6[r8] = r7     // Catch:{ SQLiteException -> 0x0240 }
            r19 = 0
            r20 = 0
            java.lang.String r21 = "rowid"
            r22 = 0
            r14 = r24
            r16 = r6
            android.database.Cursor r6 = r14.query(r15, r16, r17, r18, r19, r20, r21, r22)     // Catch:{ SQLiteException -> 0x0240 }
            boolean r7 = r6.moveToFirst()     // Catch:{ SQLiteException -> 0x0226, all -> 0x0222 }
            if (r7 != 0) goto L_0x01d1
            com.google.android.gms.measurement.internal.o r7 = r5.q()     // Catch:{ SQLiteException -> 0x0226, all -> 0x0222 }
            com.google.android.gms.measurement.internal.q r7 = r7.f     // Catch:{ SQLiteException -> 0x0226, all -> 0x0222 }
            java.lang.String r8 = "Raw event data disappeared while in transaction. appId"
            java.lang.Object r9 = com.google.android.gms.measurement.internal.o.a(r12)     // Catch:{ SQLiteException -> 0x0226, all -> 0x0222 }
            r7.a(r8, r9)     // Catch:{ SQLiteException -> 0x0226, all -> 0x0222 }
            if (r6 == 0) goto L_0x0265
            r6.close()     // Catch:{ all -> 0x0d5f }
            goto L_0x0265
        L_0x01d1:
            long r7 = r6.getLong(r4)     // Catch:{ SQLiteException -> 0x0226, all -> 0x0222 }
            r9 = 3
            byte[] r10 = r6.getBlob(r9)     // Catch:{ SQLiteException -> 0x0226, all -> 0x0222 }
            int r9 = r10.length     // Catch:{ SQLiteException -> 0x0226, all -> 0x0222 }
            com.google.android.gms.internal.measurement.iw r9 = com.google.android.gms.internal.measurement.iw.a(r10, r9)     // Catch:{ SQLiteException -> 0x0226, all -> 0x0222 }
            com.google.android.gms.internal.measurement.ct r10 = new com.google.android.gms.internal.measurement.ct     // Catch:{ SQLiteException -> 0x0226, all -> 0x0222 }
            r10.<init>()     // Catch:{ SQLiteException -> 0x0226, all -> 0x0222 }
            r10.a(r9)     // Catch:{ IOException -> 0x0205 }
            r9 = 1
            java.lang.String r13 = r6.getString(r9)     // Catch:{ SQLiteException -> 0x0226, all -> 0x0222 }
            r10.f2137b = r13     // Catch:{ SQLiteException -> 0x0226, all -> 0x0222 }
            long r13 = r6.getLong(r11)     // Catch:{ SQLiteException -> 0x0226, all -> 0x0222 }
            java.lang.Long r9 = java.lang.Long.valueOf(r13)     // Catch:{ SQLiteException -> 0x0226, all -> 0x0222 }
            r10.c = r9     // Catch:{ SQLiteException -> 0x0226, all -> 0x0222 }
            boolean r7 = r3.a(r7, r10)     // Catch:{ SQLiteException -> 0x0226, all -> 0x0222 }
            if (r7 != 0) goto L_0x0216
            if (r6 == 0) goto L_0x0265
            r6.close()     // Catch:{ all -> 0x0d5f }
            goto L_0x0265
        L_0x0205:
            r0 = move-exception
            r7 = r0
            com.google.android.gms.measurement.internal.o r8 = r5.q()     // Catch:{ SQLiteException -> 0x0226, all -> 0x0222 }
            com.google.android.gms.measurement.internal.q r8 = r8.c     // Catch:{ SQLiteException -> 0x0226, all -> 0x0222 }
            java.lang.String r9 = "Data loss. Failed to merge raw event. appId"
            java.lang.Object r10 = com.google.android.gms.measurement.internal.o.a(r12)     // Catch:{ SQLiteException -> 0x0226, all -> 0x0222 }
            r8.a(r9, r10, r7)     // Catch:{ SQLiteException -> 0x0226, all -> 0x0222 }
        L_0x0216:
            boolean r7 = r6.moveToNext()     // Catch:{ SQLiteException -> 0x0226, all -> 0x0222 }
            if (r7 != 0) goto L_0x01d1
            if (r6 == 0) goto L_0x0265
            r6.close()     // Catch:{ all -> 0x0d5f }
            goto L_0x0265
        L_0x0222:
            r0 = move-exception
            r5 = r1
            r9 = r6
            goto L_0x024a
        L_0x0226:
            r0 = move-exception
            r9 = r6
            goto L_0x0241
        L_0x0229:
            r0 = move-exception
            r6 = r0
            com.google.android.gms.measurement.internal.o r7 = r5.q()     // Catch:{ SQLiteException -> 0x0240 }
            com.google.android.gms.measurement.internal.q r7 = r7.c     // Catch:{ SQLiteException -> 0x0240 }
            java.lang.String r8 = "Data loss. Failed to merge raw event metadata. appId"
            java.lang.Object r10 = com.google.android.gms.measurement.internal.o.a(r12)     // Catch:{ SQLiteException -> 0x0240 }
            r7.a(r8, r10, r6)     // Catch:{ SQLiteException -> 0x0240 }
            if (r9 == 0) goto L_0x0265
            r9.close()     // Catch:{ all -> 0x0d5f }
            goto L_0x0265
        L_0x0240:
            r0 = move-exception
        L_0x0241:
            r6 = r0
            goto L_0x0251
        L_0x0243:
            r0 = move-exception
            r6 = r0
            r9 = r12
            goto L_0x0250
        L_0x0247:
            r0 = move-exception
            r5 = r1
            r9 = 0
        L_0x024a:
            r1 = r0
            goto L_0x0d57
        L_0x024d:
            r0 = move-exception
            r6 = r0
            r9 = 0
        L_0x0250:
            r12 = 0
        L_0x0251:
            com.google.android.gms.measurement.internal.o r5 = r5.q()     // Catch:{ all -> 0x0d53 }
            com.google.android.gms.measurement.internal.q r5 = r5.c     // Catch:{ all -> 0x0d53 }
            java.lang.String r7 = "Data loss. Error selecting raw event. appId"
            java.lang.Object r8 = com.google.android.gms.measurement.internal.o.a(r12)     // Catch:{ all -> 0x0d53 }
            r5.a(r7, r8, r6)     // Catch:{ all -> 0x0d53 }
            if (r9 == 0) goto L_0x0265
            r9.close()     // Catch:{ all -> 0x0d5f }
        L_0x0265:
            java.util.List<com.google.android.gms.internal.measurement.ct> r5 = r3.c     // Catch:{ all -> 0x0d5f }
            if (r5 == 0) goto L_0x0274
            java.util.List<com.google.android.gms.internal.measurement.ct> r5 = r3.c     // Catch:{ all -> 0x0d5f }
            boolean r5 = r5.isEmpty()     // Catch:{ all -> 0x0d5f }
            if (r5 == 0) goto L_0x0272
            goto L_0x0274
        L_0x0272:
            r5 = 0
            goto L_0x0275
        L_0x0274:
            r5 = 1
        L_0x0275:
            if (r5 != 0) goto L_0x0d42
            com.google.android.gms.internal.measurement.cw r5 = r3.f2529a     // Catch:{ all -> 0x0d5f }
            java.util.List<com.google.android.gms.internal.measurement.ct> r6 = r3.c     // Catch:{ all -> 0x0d5f }
            int r6 = r6.size()     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.ct[] r6 = new com.google.android.gms.internal.measurement.ct[r6]     // Catch:{ all -> 0x0d5f }
            r5.f2142b = r6     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.ar r6 = r1.f2528b     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.el r6 = r6.e     // Catch:{ all -> 0x0d5f }
            java.lang.String r7 = r5.o     // Catch:{ all -> 0x0d5f }
            boolean r6 = r6.d(r7)     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.ar r7 = r1.f2528b     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.el r7 = r7.e     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cw r8 = r3.f2529a     // Catch:{ all -> 0x0d5f }
            java.lang.String r8 = r8.o     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.h$a<java.lang.Boolean> r9 = com.google.android.gms.measurement.internal.h.ao     // Catch:{ all -> 0x0d5f }
            boolean r7 = r7.c(r8, r9)     // Catch:{ all -> 0x0d5f }
            r8 = 0
            r9 = 0
            r10 = 0
            r12 = 0
            r13 = 0
            r14 = 0
        L_0x02a2:
            java.util.List<com.google.android.gms.internal.measurement.ct> r4 = r3.c     // Catch:{ all -> 0x0d5f }
            int r4 = r4.size()     // Catch:{ all -> 0x0d5f }
            java.lang.String r11 = "_et"
            r18 = r13
            java.lang.String r13 = "_e"
            r19 = 1
            if (r10 >= r4) goto L_0x0729
            java.util.List<com.google.android.gms.internal.measurement.ct> r4 = r3.c     // Catch:{ all -> 0x0d5f }
            java.lang.Object r4 = r4.get(r10)     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.ct r4 = (com.google.android.gms.internal.measurement.ct) r4     // Catch:{ all -> 0x0d5f }
            r21 = r2
            com.google.android.gms.measurement.internal.al r2 = r43.n()     // Catch:{ all -> 0x0d5f }
            r22 = r10
            com.google.android.gms.internal.measurement.cw r10 = r3.f2529a     // Catch:{ all -> 0x0d5f }
            java.lang.String r10 = r10.o     // Catch:{ all -> 0x0d5f }
            r24 = r12
            java.lang.String r12 = r4.f2137b     // Catch:{ all -> 0x0d5f }
            boolean r2 = r2.b(r10, r12)     // Catch:{ all -> 0x0d5f }
            java.lang.String r10 = "_err"
            if (r2 == 0) goto L_0x0345
            com.google.android.gms.measurement.internal.ar r2 = r1.f2528b     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.o r2 = r2.q()     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.q r2 = r2.f     // Catch:{ all -> 0x0d5f }
            java.lang.String r11 = "Dropping blacklisted raw event. appId"
            com.google.android.gms.internal.measurement.cw r12 = r3.f2529a     // Catch:{ all -> 0x0d5f }
            java.lang.String r12 = r12.o     // Catch:{ all -> 0x0d5f }
            java.lang.Object r12 = com.google.android.gms.measurement.internal.o.a(r12)     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.ar r13 = r1.f2528b     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.m r13 = r13.f()     // Catch:{ all -> 0x0d5f }
            r25 = r5
            java.lang.String r5 = r4.f2137b     // Catch:{ all -> 0x0d5f }
            java.lang.String r5 = r13.a(r5)     // Catch:{ all -> 0x0d5f }
            r2.a(r11, r12, r5)     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.al r2 = r43.n()     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cw r5 = r3.f2529a     // Catch:{ all -> 0x0d5f }
            java.lang.String r5 = r5.o     // Catch:{ all -> 0x0d5f }
            boolean r2 = r2.f(r5)     // Catch:{ all -> 0x0d5f }
            if (r2 != 0) goto L_0x0314
            com.google.android.gms.measurement.internal.al r2 = r43.n()     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cw r5 = r3.f2529a     // Catch:{ all -> 0x0d5f }
            java.lang.String r5 = r5.o     // Catch:{ all -> 0x0d5f }
            boolean r2 = r2.g(r5)     // Catch:{ all -> 0x0d5f }
            if (r2 == 0) goto L_0x0312
            goto L_0x0314
        L_0x0312:
            r2 = 0
            goto L_0x0315
        L_0x0314:
            r2 = 1
        L_0x0315:
            if (r2 != 0) goto L_0x0338
            java.lang.String r2 = r4.f2137b     // Catch:{ all -> 0x0d5f }
            boolean r2 = r10.equals(r2)     // Catch:{ all -> 0x0d5f }
            if (r2 != 0) goto L_0x0338
            com.google.android.gms.measurement.internal.ar r2 = r1.f2528b     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.ed r26 = r2.e()     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cw r2 = r3.f2529a     // Catch:{ all -> 0x0d5f }
            java.lang.String r2 = r2.o     // Catch:{ all -> 0x0d5f }
            r28 = 11
            java.lang.String r29 = "_ev"
            java.lang.String r4 = r4.f2137b     // Catch:{ all -> 0x0d5f }
            r31 = 0
            r27 = r2
            r30 = r4
            r26.a(r27, r28, r29, r30, r31)     // Catch:{ all -> 0x0d5f }
        L_0x0338:
            r29 = r6
            r31 = r7
            r13 = r18
            r12 = r24
            r5 = r25
            r11 = 3
            goto L_0x071e
        L_0x0345:
            r25 = r5
            com.google.android.gms.measurement.internal.al r2 = r43.n()     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cw r5 = r3.f2529a     // Catch:{ all -> 0x0d5f }
            java.lang.String r5 = r5.o     // Catch:{ all -> 0x0d5f }
            java.lang.String r12 = r4.f2137b     // Catch:{ all -> 0x0d5f }
            boolean r2 = r2.c(r5, r12)     // Catch:{ all -> 0x0d5f }
            java.lang.String r5 = "_c"
            if (r2 != 0) goto L_0x03ac
            r43.f()     // Catch:{ all -> 0x0d5f }
            java.lang.String r12 = r4.f2137b     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.common.internal.l.a(r12)     // Catch:{ all -> 0x0d5f }
            r27 = r14
            int r14 = r12.hashCode()     // Catch:{ all -> 0x0d5f }
            r15 = 94660(0x171c4, float:1.32647E-40)
            if (r14 == r15) goto L_0x038b
            r15 = 95025(0x17331, float:1.33158E-40)
            if (r14 == r15) goto L_0x0381
            r15 = 95027(0x17333, float:1.33161E-40)
            if (r14 == r15) goto L_0x0377
            goto L_0x0395
        L_0x0377:
            java.lang.String r14 = "_ui"
            boolean r12 = r12.equals(r14)     // Catch:{ all -> 0x0d5f }
            if (r12 == 0) goto L_0x0395
            r12 = 1
            goto L_0x0396
        L_0x0381:
            java.lang.String r14 = "_ug"
            boolean r12 = r12.equals(r14)     // Catch:{ all -> 0x0d5f }
            if (r12 == 0) goto L_0x0395
            r12 = 2
            goto L_0x0396
        L_0x038b:
            java.lang.String r14 = "_in"
            boolean r12 = r12.equals(r14)     // Catch:{ all -> 0x0d5f }
            if (r12 == 0) goto L_0x0395
            r12 = 0
            goto L_0x0396
        L_0x0395:
            r12 = -1
        L_0x0396:
            if (r12 == 0) goto L_0x03a0
            r14 = 1
            if (r12 == r14) goto L_0x03a0
            r14 = 2
            if (r12 == r14) goto L_0x03a0
            r12 = 0
            goto L_0x03a1
        L_0x03a0:
            r12 = 1
        L_0x03a1:
            if (r12 == 0) goto L_0x03a4
            goto L_0x03ae
        L_0x03a4:
            r29 = r6
            r31 = r7
            r30 = r11
            goto L_0x0583
        L_0x03ac:
            r27 = r14
        L_0x03ae:
            com.google.android.gms.internal.measurement.cu[] r12 = r4.f2136a     // Catch:{ all -> 0x0d5f }
            if (r12 != 0) goto L_0x03b7
            r12 = 0
            com.google.android.gms.internal.measurement.cu[] r14 = new com.google.android.gms.internal.measurement.cu[r12]     // Catch:{ all -> 0x0d5f }
            r4.f2136a = r14     // Catch:{ all -> 0x0d5f }
        L_0x03b7:
            com.google.android.gms.internal.measurement.cu[] r12 = r4.f2136a     // Catch:{ all -> 0x0d5f }
            int r14 = r12.length     // Catch:{ all -> 0x0d5f }
            r31 = r7
            r15 = 0
            r29 = 0
            r30 = 0
        L_0x03c1:
            java.lang.String r7 = "_r"
            if (r15 >= r14) goto L_0x03f3
            r32 = r14
            r14 = r12[r15]     // Catch:{ all -> 0x0d5f }
            r33 = r12
            java.lang.String r12 = r14.f2138a     // Catch:{ all -> 0x0d5f }
            boolean r12 = r5.equals(r12)     // Catch:{ all -> 0x0d5f }
            if (r12 == 0) goto L_0x03dc
            java.lang.Long r7 = java.lang.Long.valueOf(r19)     // Catch:{ all -> 0x0d5f }
            r14.c = r7     // Catch:{ all -> 0x0d5f }
            r29 = 1
            goto L_0x03ec
        L_0x03dc:
            java.lang.String r12 = r14.f2138a     // Catch:{ all -> 0x0d5f }
            boolean r7 = r7.equals(r12)     // Catch:{ all -> 0x0d5f }
            if (r7 == 0) goto L_0x03ec
            java.lang.Long r7 = java.lang.Long.valueOf(r19)     // Catch:{ all -> 0x0d5f }
            r14.c = r7     // Catch:{ all -> 0x0d5f }
            r30 = 1
        L_0x03ec:
            int r15 = r15 + 1
            r14 = r32
            r12 = r33
            goto L_0x03c1
        L_0x03f3:
            if (r29 != 0) goto L_0x0434
            if (r2 == 0) goto L_0x0434
            com.google.android.gms.measurement.internal.ar r12 = r1.f2528b     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.o r12 = r12.q()     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.q r12 = r12.k     // Catch:{ all -> 0x0d5f }
            java.lang.String r14 = "Marking event as conversion"
            com.google.android.gms.measurement.internal.ar r15 = r1.f2528b     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.m r15 = r15.f()     // Catch:{ all -> 0x0d5f }
            r29 = r6
            java.lang.String r6 = r4.f2137b     // Catch:{ all -> 0x0d5f }
            java.lang.String r6 = r15.a(r6)     // Catch:{ all -> 0x0d5f }
            r12.a(r14, r6)     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cu[] r6 = r4.f2136a     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cu[] r12 = r4.f2136a     // Catch:{ all -> 0x0d5f }
            int r12 = r12.length     // Catch:{ all -> 0x0d5f }
            r14 = 1
            int r12 = r12 + r14
            java.lang.Object[] r6 = java.util.Arrays.copyOf(r6, r12)     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cu[] r6 = (com.google.android.gms.internal.measurement.cu[]) r6     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cu r12 = new com.google.android.gms.internal.measurement.cu     // Catch:{ all -> 0x0d5f }
            r12.<init>()     // Catch:{ all -> 0x0d5f }
            r12.f2138a = r5     // Catch:{ all -> 0x0d5f }
            java.lang.Long r14 = java.lang.Long.valueOf(r19)     // Catch:{ all -> 0x0d5f }
            r12.c = r14     // Catch:{ all -> 0x0d5f }
            int r14 = r6.length     // Catch:{ all -> 0x0d5f }
            r15 = 1
            int r14 = r14 - r15
            r6[r14] = r12     // Catch:{ all -> 0x0d5f }
            r4.f2136a = r6     // Catch:{ all -> 0x0d5f }
            goto L_0x0436
        L_0x0434:
            r29 = r6
        L_0x0436:
            if (r30 != 0) goto L_0x0472
            com.google.android.gms.measurement.internal.ar r6 = r1.f2528b     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.o r6 = r6.q()     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.q r6 = r6.k     // Catch:{ all -> 0x0d5f }
            java.lang.String r12 = "Marking event as real-time"
            com.google.android.gms.measurement.internal.ar r14 = r1.f2528b     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.m r14 = r14.f()     // Catch:{ all -> 0x0d5f }
            java.lang.String r15 = r4.f2137b     // Catch:{ all -> 0x0d5f }
            java.lang.String r14 = r14.a(r15)     // Catch:{ all -> 0x0d5f }
            r6.a(r12, r14)     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cu[] r6 = r4.f2136a     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cu[] r12 = r4.f2136a     // Catch:{ all -> 0x0d5f }
            int r12 = r12.length     // Catch:{ all -> 0x0d5f }
            r14 = 1
            int r12 = r12 + r14
            java.lang.Object[] r6 = java.util.Arrays.copyOf(r6, r12)     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cu[] r6 = (com.google.android.gms.internal.measurement.cu[]) r6     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cu r12 = new com.google.android.gms.internal.measurement.cu     // Catch:{ all -> 0x0d5f }
            r12.<init>()     // Catch:{ all -> 0x0d5f }
            r12.f2138a = r7     // Catch:{ all -> 0x0d5f }
            java.lang.Long r14 = java.lang.Long.valueOf(r19)     // Catch:{ all -> 0x0d5f }
            r12.c = r14     // Catch:{ all -> 0x0d5f }
            int r14 = r6.length     // Catch:{ all -> 0x0d5f }
            r15 = 1
            int r14 = r14 - r15
            r6[r14] = r12     // Catch:{ all -> 0x0d5f }
            r4.f2136a = r6     // Catch:{ all -> 0x0d5f }
        L_0x0472:
            com.google.android.gms.measurement.internal.eo r32 = r43.d()     // Catch:{ all -> 0x0d5f }
            long r33 = r43.s()     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cw r6 = r3.f2529a     // Catch:{ all -> 0x0d5f }
            java.lang.String r6 = r6.o     // Catch:{ all -> 0x0d5f }
            r36 = 0
            r37 = 0
            r38 = 0
            r39 = 0
            r40 = 1
            r35 = r6
            com.google.android.gms.measurement.internal.ep r6 = r32.a(r33, r35, r36, r37, r38, r39, r40)     // Catch:{ all -> 0x0d5f }
            long r14 = r6.e     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.ar r6 = r1.f2528b     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.el r6 = r6.e     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cw r12 = r3.f2529a     // Catch:{ all -> 0x0d5f }
            java.lang.String r12 = r12.o     // Catch:{ all -> 0x0d5f }
            int r6 = r6.a(r12)     // Catch:{ all -> 0x0d5f }
            r30 = r11
            long r11 = (long) r6     // Catch:{ all -> 0x0d5f }
            int r6 = (r14 > r11 ? 1 : (r14 == r11 ? 0 : -1))
            if (r6 <= 0) goto L_0x04d6
            r6 = 0
        L_0x04a4:
            com.google.android.gms.internal.measurement.cu[] r11 = r4.f2136a     // Catch:{ all -> 0x0d5f }
            int r11 = r11.length     // Catch:{ all -> 0x0d5f }
            if (r6 >= r11) goto L_0x04d8
            com.google.android.gms.internal.measurement.cu[] r11 = r4.f2136a     // Catch:{ all -> 0x0d5f }
            r11 = r11[r6]     // Catch:{ all -> 0x0d5f }
            java.lang.String r11 = r11.f2138a     // Catch:{ all -> 0x0d5f }
            boolean r11 = r7.equals(r11)     // Catch:{ all -> 0x0d5f }
            if (r11 == 0) goto L_0x04d3
            com.google.android.gms.internal.measurement.cu[] r7 = r4.f2136a     // Catch:{ all -> 0x0d5f }
            int r7 = r7.length     // Catch:{ all -> 0x0d5f }
            r11 = 1
            int r7 = r7 - r11
            com.google.android.gms.internal.measurement.cu[] r7 = new com.google.android.gms.internal.measurement.cu[r7]     // Catch:{ all -> 0x0d5f }
            if (r6 <= 0) goto L_0x04c4
            com.google.android.gms.internal.measurement.cu[] r11 = r4.f2136a     // Catch:{ all -> 0x0d5f }
            r12 = 0
            java.lang.System.arraycopy(r11, r12, r7, r12, r6)     // Catch:{ all -> 0x0d5f }
        L_0x04c4:
            int r11 = r7.length     // Catch:{ all -> 0x0d5f }
            if (r6 >= r11) goto L_0x04d0
            com.google.android.gms.internal.measurement.cu[] r11 = r4.f2136a     // Catch:{ all -> 0x0d5f }
            int r12 = r6 + 1
            int r14 = r7.length     // Catch:{ all -> 0x0d5f }
            int r14 = r14 - r6
            java.lang.System.arraycopy(r11, r12, r7, r6, r14)     // Catch:{ all -> 0x0d5f }
        L_0x04d0:
            r4.f2136a = r7     // Catch:{ all -> 0x0d5f }
            goto L_0x04d8
        L_0x04d3:
            int r6 = r6 + 1
            goto L_0x04a4
        L_0x04d6:
            r18 = 1
        L_0x04d8:
            java.lang.String r6 = r4.f2137b     // Catch:{ all -> 0x0d5f }
            boolean r6 = com.google.android.gms.measurement.internal.ed.a(r6)     // Catch:{ all -> 0x0d5f }
            if (r6 == 0) goto L_0x0583
            if (r2 == 0) goto L_0x0583
            com.google.android.gms.measurement.internal.eo r32 = r43.d()     // Catch:{ all -> 0x0d5f }
            long r33 = r43.s()     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cw r6 = r3.f2529a     // Catch:{ all -> 0x0d5f }
            java.lang.String r6 = r6.o     // Catch:{ all -> 0x0d5f }
            r36 = 0
            r37 = 0
            r38 = 1
            r39 = 0
            r40 = 0
            r35 = r6
            com.google.android.gms.measurement.internal.ep r6 = r32.a(r33, r35, r36, r37, r38, r39, r40)     // Catch:{ all -> 0x0d5f }
            long r6 = r6.c     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.ar r11 = r1.f2528b     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.el r11 = r11.e     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cw r12 = r3.f2529a     // Catch:{ all -> 0x0d5f }
            java.lang.String r12 = r12.o     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.h$a<java.lang.Integer> r14 = com.google.android.gms.measurement.internal.h.v     // Catch:{ all -> 0x0d5f }
            int r11 = r11.b(r12, r14)     // Catch:{ all -> 0x0d5f }
            long r11 = (long) r11     // Catch:{ all -> 0x0d5f }
            int r14 = (r6 > r11 ? 1 : (r6 == r11 ? 0 : -1))
            if (r14 <= 0) goto L_0x0583
            com.google.android.gms.measurement.internal.ar r6 = r1.f2528b     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.o r6 = r6.q()     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.q r6 = r6.f     // Catch:{ all -> 0x0d5f }
            java.lang.String r7 = "Too many conversions. Not logging as conversion. appId"
            com.google.android.gms.internal.measurement.cw r11 = r3.f2529a     // Catch:{ all -> 0x0d5f }
            java.lang.String r11 = r11.o     // Catch:{ all -> 0x0d5f }
            java.lang.Object r11 = com.google.android.gms.measurement.internal.o.a(r11)     // Catch:{ all -> 0x0d5f }
            r6.a(r7, r11)     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cu[] r6 = r4.f2136a     // Catch:{ all -> 0x0d5f }
            int r7 = r6.length     // Catch:{ all -> 0x0d5f }
            r11 = 0
            r12 = 0
            r14 = 0
        L_0x052e:
            if (r11 >= r7) goto L_0x054c
            r15 = r6[r11]     // Catch:{ all -> 0x0d5f }
            r19 = r6
            java.lang.String r6 = r15.f2138a     // Catch:{ all -> 0x0d5f }
            boolean r6 = r5.equals(r6)     // Catch:{ all -> 0x0d5f }
            if (r6 == 0) goto L_0x053e
            r14 = r15
            goto L_0x0547
        L_0x053e:
            java.lang.String r6 = r15.f2138a     // Catch:{ all -> 0x0d5f }
            boolean r6 = r10.equals(r6)     // Catch:{ all -> 0x0d5f }
            if (r6 == 0) goto L_0x0547
            r12 = 1
        L_0x0547:
            int r11 = r11 + 1
            r6 = r19
            goto L_0x052e
        L_0x054c:
            if (r12 == 0) goto L_0x0561
            if (r14 == 0) goto L_0x0561
            com.google.android.gms.internal.measurement.cu[] r6 = r4.f2136a     // Catch:{ all -> 0x0d5f }
            r7 = 1
            com.google.android.gms.internal.measurement.cu[] r10 = new com.google.android.gms.internal.measurement.cu[r7]     // Catch:{ all -> 0x0d5f }
            r7 = 0
            r10[r7] = r14     // Catch:{ all -> 0x0d5f }
            java.lang.Object[] r6 = com.google.android.gms.common.util.b.a(r6, r10)     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cu[] r6 = (com.google.android.gms.internal.measurement.cu[]) r6     // Catch:{ all -> 0x0d5f }
            r4.f2136a = r6     // Catch:{ all -> 0x0d5f }
            goto L_0x0583
        L_0x0561:
            if (r14 == 0) goto L_0x056e
            r14.f2138a = r10     // Catch:{ all -> 0x0d5f }
            r6 = 10
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x0d5f }
            r14.c = r6     // Catch:{ all -> 0x0d5f }
            goto L_0x0583
        L_0x056e:
            com.google.android.gms.measurement.internal.ar r6 = r1.f2528b     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.o r6 = r6.q()     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.q r6 = r6.c     // Catch:{ all -> 0x0d5f }
            java.lang.String r7 = "Did not find conversion parameter. appId"
            com.google.android.gms.internal.measurement.cw r10 = r3.f2529a     // Catch:{ all -> 0x0d5f }
            java.lang.String r10 = r10.o     // Catch:{ all -> 0x0d5f }
            java.lang.Object r10 = com.google.android.gms.measurement.internal.o.a(r10)     // Catch:{ all -> 0x0d5f }
            r6.a(r7, r10)     // Catch:{ all -> 0x0d5f }
        L_0x0583:
            com.google.android.gms.measurement.internal.ar r6 = r1.f2528b     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.el r6 = r6.e     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cw r7 = r3.f2529a     // Catch:{ all -> 0x0d5f }
            java.lang.String r7 = r7.o     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.h$a<java.lang.Boolean> r10 = com.google.android.gms.measurement.internal.h.ae     // Catch:{ all -> 0x0d5f }
            boolean r6 = r6.c(r7, r10)     // Catch:{ all -> 0x0d5f }
            if (r6 == 0) goto L_0x063b
            if (r2 == 0) goto L_0x063b
            com.google.android.gms.internal.measurement.cu[] r2 = r4.f2136a     // Catch:{ all -> 0x0d5f }
            r6 = 0
            r7 = -1
            r10 = -1
        L_0x059a:
            int r11 = r2.length     // Catch:{ all -> 0x0d5f }
            if (r6 >= r11) goto L_0x05bb
            java.lang.String r11 = "value"
            r12 = r2[r6]     // Catch:{ all -> 0x0d5f }
            java.lang.String r12 = r12.f2138a     // Catch:{ all -> 0x0d5f }
            boolean r11 = r11.equals(r12)     // Catch:{ all -> 0x0d5f }
            if (r11 == 0) goto L_0x05ab
            r7 = r6
            goto L_0x05b8
        L_0x05ab:
            java.lang.String r11 = "currency"
            r12 = r2[r6]     // Catch:{ all -> 0x0d5f }
            java.lang.String r12 = r12.f2138a     // Catch:{ all -> 0x0d5f }
            boolean r11 = r11.equals(r12)     // Catch:{ all -> 0x0d5f }
            if (r11 == 0) goto L_0x05b8
            r10 = r6
        L_0x05b8:
            int r6 = r6 + 1
            goto L_0x059a
        L_0x05bb:
            r6 = -1
            if (r7 == r6) goto L_0x05e7
            r6 = r2[r7]     // Catch:{ all -> 0x0d5f }
            java.lang.Long r6 = r6.c     // Catch:{ all -> 0x0d5f }
            if (r6 != 0) goto L_0x05e9
            r6 = r2[r7]     // Catch:{ all -> 0x0d5f }
            java.lang.Double r6 = r6.d     // Catch:{ all -> 0x0d5f }
            if (r6 != 0) goto L_0x05e9
            com.google.android.gms.measurement.internal.ar r6 = r1.f2528b     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.o r6 = r6.q()     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.q r6 = r6.h     // Catch:{ all -> 0x0d5f }
            java.lang.String r10 = "Value must be specified with a numeric type."
            r6.a(r10)     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cu[] r2 = a(r2, r7)     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cu[] r2 = a(r2, r5)     // Catch:{ all -> 0x0d5f }
            r5 = 18
            java.lang.String r6 = "value"
            com.google.android.gms.internal.measurement.cu[] r2 = a(r2, r5, r6)     // Catch:{ all -> 0x0d5f }
        L_0x05e7:
            r11 = 3
            goto L_0x0638
        L_0x05e9:
            r6 = -1
            if (r10 != r6) goto L_0x05ef
            r6 = 1
            r11 = 3
            goto L_0x0619
        L_0x05ef:
            r6 = r2[r10]     // Catch:{ all -> 0x0d5f }
            java.lang.String r6 = r6.f2139b     // Catch:{ all -> 0x0d5f }
            if (r6 == 0) goto L_0x0617
            int r10 = r6.length()     // Catch:{ all -> 0x0d5f }
            r11 = 3
            if (r10 == r11) goto L_0x05fd
            goto L_0x0618
        L_0x05fd:
            r10 = 0
        L_0x05fe:
            int r12 = r6.length()     // Catch:{ all -> 0x0d5f }
            if (r10 >= r12) goto L_0x0615
            int r12 = r6.codePointAt(r10)     // Catch:{ all -> 0x0d5f }
            boolean r14 = java.lang.Character.isLetter(r12)     // Catch:{ all -> 0x0d5f }
            if (r14 != 0) goto L_0x060f
            goto L_0x0618
        L_0x060f:
            int r12 = java.lang.Character.charCount(r12)     // Catch:{ all -> 0x0d5f }
            int r10 = r10 + r12
            goto L_0x05fe
        L_0x0615:
            r6 = 0
            goto L_0x0619
        L_0x0617:
            r11 = 3
        L_0x0618:
            r6 = 1
        L_0x0619:
            if (r6 == 0) goto L_0x0638
            com.google.android.gms.measurement.internal.ar r6 = r1.f2528b     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.o r6 = r6.q()     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.q r6 = r6.h     // Catch:{ all -> 0x0d5f }
            java.lang.String r10 = "Value parameter discarded. You must also supply a 3-letter ISO_4217 currency code in the currency parameter."
            r6.a(r10)     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cu[] r2 = a(r2, r7)     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cu[] r2 = a(r2, r5)     // Catch:{ all -> 0x0d5f }
            r5 = 19
            java.lang.String r6 = "currency"
            com.google.android.gms.internal.measurement.cu[] r2 = a(r2, r5, r6)     // Catch:{ all -> 0x0d5f }
        L_0x0638:
            r4.f2136a = r2     // Catch:{ all -> 0x0d5f }
            goto L_0x063c
        L_0x063b:
            r11 = 3
        L_0x063c:
            com.google.android.gms.measurement.internal.ar r2 = r1.f2528b     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.el r2 = r2.e     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cw r5 = r3.f2529a     // Catch:{ all -> 0x0d5f }
            java.lang.String r5 = r5.o     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.h$a<java.lang.Boolean> r6 = com.google.android.gms.measurement.internal.h.an     // Catch:{ all -> 0x0d5f }
            boolean r2 = r2.c(r5, r6)     // Catch:{ all -> 0x0d5f }
            if (r2 == 0) goto L_0x0684
            java.lang.String r2 = r4.f2137b     // Catch:{ all -> 0x0d5f }
            boolean r2 = r13.equals(r2)     // Catch:{ all -> 0x0d5f }
            if (r2 == 0) goto L_0x0687
            r43.f()     // Catch:{ all -> 0x0d5f }
            java.lang.String r2 = "_fr"
            com.google.android.gms.internal.measurement.cu r2 = com.google.android.gms.measurement.internal.ea.a(r4, r2)     // Catch:{ all -> 0x0d5f }
            if (r2 != 0) goto L_0x0684
            if (r9 == 0) goto L_0x0683
            java.lang.Long r2 = r9.c     // Catch:{ all -> 0x0d5f }
            long r5 = r2.longValue()     // Catch:{ all -> 0x0d5f }
            java.lang.Long r2 = r4.c     // Catch:{ all -> 0x0d5f }
            long r7 = r2.longValue()     // Catch:{ all -> 0x0d5f }
            long r5 = r5 - r7
            long r5 = java.lang.Math.abs(r5)     // Catch:{ all -> 0x0d5f }
            r7 = 1000(0x3e8, double:4.94E-321)
            int r2 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r2 > 0) goto L_0x0683
            boolean r2 = r1.a(r4, r9)     // Catch:{ all -> 0x0d5f }
            if (r2 == 0) goto L_0x0683
            r2 = r30
        L_0x0680:
            r8 = 0
            r9 = 0
            goto L_0x06bd
        L_0x0683:
            r8 = r4
        L_0x0684:
            r2 = r30
            goto L_0x06bd
        L_0x0687:
            java.lang.String r2 = "_vs"
            java.lang.String r5 = r4.f2137b     // Catch:{ all -> 0x0d5f }
            boolean r2 = r2.equals(r5)     // Catch:{ all -> 0x0d5f }
            if (r2 == 0) goto L_0x0684
            r43.f()     // Catch:{ all -> 0x0d5f }
            r2 = r30
            com.google.android.gms.internal.measurement.cu r5 = com.google.android.gms.measurement.internal.ea.a(r4, r2)     // Catch:{ all -> 0x0d5f }
            if (r5 != 0) goto L_0x06bd
            if (r8 == 0) goto L_0x06bc
            java.lang.Long r5 = r8.c     // Catch:{ all -> 0x0d5f }
            long r5 = r5.longValue()     // Catch:{ all -> 0x0d5f }
            java.lang.Long r7 = r4.c     // Catch:{ all -> 0x0d5f }
            long r9 = r7.longValue()     // Catch:{ all -> 0x0d5f }
            long r5 = r5 - r9
            long r5 = java.lang.Math.abs(r5)     // Catch:{ all -> 0x0d5f }
            r9 = 1000(0x3e8, double:4.94E-321)
            int r7 = (r5 > r9 ? 1 : (r5 == r9 ? 0 : -1))
            if (r7 > 0) goto L_0x06bc
            boolean r5 = r1.a(r8, r4)     // Catch:{ all -> 0x0d5f }
            if (r5 == 0) goto L_0x06bc
            goto L_0x0680
        L_0x06bc:
            r9 = r4
        L_0x06bd:
            if (r29 == 0) goto L_0x0712
            if (r31 != 0) goto L_0x0712
            java.lang.String r5 = r4.f2137b     // Catch:{ all -> 0x0d5f }
            boolean r5 = r13.equals(r5)     // Catch:{ all -> 0x0d5f }
            if (r5 == 0) goto L_0x0712
            com.google.android.gms.internal.measurement.cu[] r5 = r4.f2136a     // Catch:{ all -> 0x0d5f }
            if (r5 == 0) goto L_0x06fd
            com.google.android.gms.internal.measurement.cu[] r5 = r4.f2136a     // Catch:{ all -> 0x0d5f }
            int r5 = r5.length     // Catch:{ all -> 0x0d5f }
            if (r5 != 0) goto L_0x06d3
            goto L_0x06fd
        L_0x06d3:
            r43.f()     // Catch:{ all -> 0x0d5f }
            java.lang.Object r2 = com.google.android.gms.measurement.internal.ea.b(r4, r2)     // Catch:{ all -> 0x0d5f }
            java.lang.Long r2 = (java.lang.Long) r2     // Catch:{ all -> 0x0d5f }
            if (r2 != 0) goto L_0x06f4
            com.google.android.gms.measurement.internal.ar r2 = r1.f2528b     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.o r2 = r2.q()     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.q r2 = r2.f     // Catch:{ all -> 0x0d5f }
            java.lang.String r5 = "Engagement event does not include duration. appId"
            com.google.android.gms.internal.measurement.cw r6 = r3.f2529a     // Catch:{ all -> 0x0d5f }
            java.lang.String r6 = r6.o     // Catch:{ all -> 0x0d5f }
            java.lang.Object r6 = com.google.android.gms.measurement.internal.o.a(r6)     // Catch:{ all -> 0x0d5f }
            r2.a(r5, r6)     // Catch:{ all -> 0x0d5f }
            goto L_0x0712
        L_0x06f4:
            long r5 = r2.longValue()     // Catch:{ all -> 0x0d5f }
            long r14 = r27 + r5
            r5 = r25
            goto L_0x0716
        L_0x06fd:
            com.google.android.gms.measurement.internal.ar r2 = r1.f2528b     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.o r2 = r2.q()     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.q r2 = r2.f     // Catch:{ all -> 0x0d5f }
            java.lang.String r5 = "Engagement event does not contain any parameters. appId"
            com.google.android.gms.internal.measurement.cw r6 = r3.f2529a     // Catch:{ all -> 0x0d5f }
            java.lang.String r6 = r6.o     // Catch:{ all -> 0x0d5f }
            java.lang.Object r6 = com.google.android.gms.measurement.internal.o.a(r6)     // Catch:{ all -> 0x0d5f }
            r2.a(r5, r6)     // Catch:{ all -> 0x0d5f }
        L_0x0712:
            r5 = r25
            r14 = r27
        L_0x0716:
            com.google.android.gms.internal.measurement.ct[] r2 = r5.f2142b     // Catch:{ all -> 0x0d5f }
            int r12 = r24 + 1
            r2[r24] = r4     // Catch:{ all -> 0x0d5f }
            r13 = r18
        L_0x071e:
            int r10 = r22 + 1
            r2 = r21
            r6 = r29
            r7 = r31
            r11 = 2
            goto L_0x02a2
        L_0x0729:
            r21 = r2
            r29 = r6
            r31 = r7
            r2 = r11
            r24 = r12
            r27 = r14
            if (r31 == 0) goto L_0x0783
            r4 = 0
        L_0x0737:
            if (r4 >= r12) goto L_0x0783
            com.google.android.gms.internal.measurement.ct[] r6 = r5.f2142b     // Catch:{ all -> 0x0d5f }
            r6 = r6[r4]     // Catch:{ all -> 0x0d5f }
            java.lang.String r7 = r6.f2137b     // Catch:{ all -> 0x0d5f }
            boolean r7 = r13.equals(r7)     // Catch:{ all -> 0x0d5f }
            if (r7 == 0) goto L_0x0762
            r43.f()     // Catch:{ all -> 0x0d5f }
            java.lang.String r7 = "_fr"
            com.google.android.gms.internal.measurement.cu r7 = com.google.android.gms.measurement.internal.ea.a(r6, r7)     // Catch:{ all -> 0x0d5f }
            if (r7 == 0) goto L_0x0762
            com.google.android.gms.internal.measurement.ct[] r6 = r5.f2142b     // Catch:{ all -> 0x0d5f }
            int r7 = r4 + 1
            com.google.android.gms.internal.measurement.ct[] r8 = r5.f2142b     // Catch:{ all -> 0x0d5f }
            int r9 = r12 - r4
            r10 = 1
            int r9 = r9 - r10
            java.lang.System.arraycopy(r6, r7, r8, r4, r9)     // Catch:{ all -> 0x0d5f }
            int r12 = r12 + -1
            int r4 = r4 + -1
            goto L_0x0780
        L_0x0762:
            if (r29 == 0) goto L_0x0780
            r43.f()     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cu r6 = com.google.android.gms.measurement.internal.ea.a(r6, r2)     // Catch:{ all -> 0x0d5f }
            if (r6 == 0) goto L_0x0780
            java.lang.Long r6 = r6.c     // Catch:{ all -> 0x0d5f }
            if (r6 == 0) goto L_0x0780
            long r7 = r6.longValue()     // Catch:{ all -> 0x0d5f }
            r9 = 0
            int r11 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r11 <= 0) goto L_0x0780
            long r6 = r6.longValue()     // Catch:{ all -> 0x0d5f }
            long r14 = r14 + r6
        L_0x0780:
            r6 = 1
            int r4 = r4 + r6
            goto L_0x0737
        L_0x0783:
            java.util.List<com.google.android.gms.internal.measurement.ct> r2 = r3.c     // Catch:{ all -> 0x0d5f }
            int r2 = r2.size()     // Catch:{ all -> 0x0d5f }
            if (r12 >= r2) goto L_0x0795
            com.google.android.gms.internal.measurement.ct[] r2 = r5.f2142b     // Catch:{ all -> 0x0d5f }
            java.lang.Object[] r2 = java.util.Arrays.copyOf(r2, r12)     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.ct[] r2 = (com.google.android.gms.internal.measurement.ct[]) r2     // Catch:{ all -> 0x0d5f }
            r5.f2142b = r2     // Catch:{ all -> 0x0d5f }
        L_0x0795:
            if (r29 == 0) goto L_0x085e
            com.google.android.gms.measurement.internal.eo r2 = r43.d()     // Catch:{ all -> 0x0d5f }
            java.lang.String r4 = r5.o     // Catch:{ all -> 0x0d5f }
            r6 = r21
            com.google.android.gms.measurement.internal.ec r2 = r2.c(r4, r6)     // Catch:{ all -> 0x0d5f }
            if (r2 == 0) goto L_0x07d0
            java.lang.Object r4 = r2.e     // Catch:{ all -> 0x0d5f }
            if (r4 != 0) goto L_0x07aa
            goto L_0x07d0
        L_0x07aa:
            com.google.android.gms.measurement.internal.ec r4 = new com.google.android.gms.measurement.internal.ec     // Catch:{ all -> 0x0d5f }
            java.lang.String r8 = r5.o     // Catch:{ all -> 0x0d5f }
            java.lang.String r9 = "auto"
            java.lang.String r10 = "_lte"
            com.google.android.gms.measurement.internal.ar r7 = r1.f2528b     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.common.util.e r7 = r7.l()     // Catch:{ all -> 0x0d5f }
            long r11 = r7.a()     // Catch:{ all -> 0x0d5f }
            java.lang.Object r2 = r2.e     // Catch:{ all -> 0x0d5f }
            java.lang.Long r2 = (java.lang.Long) r2     // Catch:{ all -> 0x0d5f }
            long r21 = r2.longValue()     // Catch:{ all -> 0x0d5f }
            long r21 = r21 + r14
            java.lang.Long r13 = java.lang.Long.valueOf(r21)     // Catch:{ all -> 0x0d5f }
            r7 = r4
            r7.<init>(r8, r9, r10, r11, r13)     // Catch:{ all -> 0x0d5f }
            r2 = r4
            goto L_0x07ed
        L_0x07d0:
            com.google.android.gms.measurement.internal.ec r2 = new com.google.android.gms.measurement.internal.ec     // Catch:{ all -> 0x0d5f }
            java.lang.String r4 = r5.o     // Catch:{ all -> 0x0d5f }
            java.lang.String r26 = "auto"
            java.lang.String r27 = "_lte"
            com.google.android.gms.measurement.internal.ar r7 = r1.f2528b     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.common.util.e r7 = r7.l()     // Catch:{ all -> 0x0d5f }
            long r28 = r7.a()     // Catch:{ all -> 0x0d5f }
            java.lang.Long r30 = java.lang.Long.valueOf(r14)     // Catch:{ all -> 0x0d5f }
            r24 = r2
            r25 = r4
            r24.<init>(r25, r26, r27, r28, r30)     // Catch:{ all -> 0x0d5f }
        L_0x07ed:
            com.google.android.gms.internal.measurement.cz r4 = new com.google.android.gms.internal.measurement.cz     // Catch:{ all -> 0x0d5f }
            r4.<init>()     // Catch:{ all -> 0x0d5f }
            r4.f2148b = r6     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.ar r7 = r1.f2528b     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.common.util.e r7 = r7.l()     // Catch:{ all -> 0x0d5f }
            long r7 = r7.a()     // Catch:{ all -> 0x0d5f }
            java.lang.Long r7 = java.lang.Long.valueOf(r7)     // Catch:{ all -> 0x0d5f }
            r4.f2147a = r7     // Catch:{ all -> 0x0d5f }
            java.lang.Object r7 = r2.e     // Catch:{ all -> 0x0d5f }
            java.lang.Long r7 = (java.lang.Long) r7     // Catch:{ all -> 0x0d5f }
            r4.d = r7     // Catch:{ all -> 0x0d5f }
            r7 = 0
        L_0x080b:
            com.google.android.gms.internal.measurement.cz[] r8 = r5.c     // Catch:{ all -> 0x0d5f }
            int r8 = r8.length     // Catch:{ all -> 0x0d5f }
            if (r7 >= r8) goto L_0x0825
            com.google.android.gms.internal.measurement.cz[] r8 = r5.c     // Catch:{ all -> 0x0d5f }
            r8 = r8[r7]     // Catch:{ all -> 0x0d5f }
            java.lang.String r8 = r8.f2148b     // Catch:{ all -> 0x0d5f }
            boolean r8 = r6.equals(r8)     // Catch:{ all -> 0x0d5f }
            if (r8 == 0) goto L_0x0822
            com.google.android.gms.internal.measurement.cz[] r6 = r5.c     // Catch:{ all -> 0x0d5f }
            r6[r7] = r4     // Catch:{ all -> 0x0d5f }
            r6 = 1
            goto L_0x0826
        L_0x0822:
            int r7 = r7 + 1
            goto L_0x080b
        L_0x0825:
            r6 = 0
        L_0x0826:
            if (r6 != 0) goto L_0x0842
            com.google.android.gms.internal.measurement.cz[] r6 = r5.c     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cz[] r7 = r5.c     // Catch:{ all -> 0x0d5f }
            int r7 = r7.length     // Catch:{ all -> 0x0d5f }
            r8 = 1
            int r7 = r7 + r8
            java.lang.Object[] r6 = java.util.Arrays.copyOf(r6, r7)     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cz[] r6 = (com.google.android.gms.internal.measurement.cz[]) r6     // Catch:{ all -> 0x0d5f }
            r5.c = r6     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cz[] r6 = r5.c     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cw r7 = r3.f2529a     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cz[] r7 = r7.c     // Catch:{ all -> 0x0d5f }
            int r7 = r7.length     // Catch:{ all -> 0x0d5f }
            r8 = 1
            int r7 = r7 - r8
            r6[r7] = r4     // Catch:{ all -> 0x0d5f }
        L_0x0842:
            r6 = 0
            int r4 = (r14 > r6 ? 1 : (r14 == r6 ? 0 : -1))
            if (r4 <= 0) goto L_0x085e
            com.google.android.gms.measurement.internal.eo r4 = r43.d()     // Catch:{ all -> 0x0d5f }
            r4.a(r2)     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.ar r4 = r1.f2528b     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.o r4 = r4.q()     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.q r4 = r4.j     // Catch:{ all -> 0x0d5f }
            java.lang.String r6 = "Updated lifetime engagement user property with value. Value"
            java.lang.Object r2 = r2.e     // Catch:{ all -> 0x0d5f }
            r4.a(r6, r2)     // Catch:{ all -> 0x0d5f }
        L_0x085e:
            java.lang.String r2 = r5.o     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cz[] r4 = r5.c     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.ct[] r6 = r5.f2142b     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.common.internal.l.a(r2)     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.ei r7 = r43.e()     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cr[] r2 = r7.a(r2, r6, r4)     // Catch:{ all -> 0x0d5f }
            r5.A = r2     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.ar r2 = r1.f2528b     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.el r2 = r2.e     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cw r4 = r3.f2529a     // Catch:{ all -> 0x0d5f }
            java.lang.String r4 = r4.o     // Catch:{ all -> 0x0d5f }
            java.lang.String r6 = "1"
            com.google.android.gms.measurement.internal.en r2 = r2.f2550a     // Catch:{ all -> 0x0d5f }
            java.lang.String r7 = "measurement.event_sampling_enabled"
            java.lang.String r2 = r2.a(r4, r7)     // Catch:{ all -> 0x0d5f }
            boolean r2 = r6.equals(r2)     // Catch:{ all -> 0x0d5f }
            if (r2 == 0) goto L_0x0b7b
            java.util.HashMap r2 = new java.util.HashMap     // Catch:{ all -> 0x0d3e }
            r2.<init>()     // Catch:{ all -> 0x0d3e }
            com.google.android.gms.internal.measurement.ct[] r4 = r5.f2142b     // Catch:{ all -> 0x0d3e }
            int r4 = r4.length     // Catch:{ all -> 0x0d3e }
            com.google.android.gms.internal.measurement.ct[] r4 = new com.google.android.gms.internal.measurement.ct[r4]     // Catch:{ all -> 0x0d3e }
            com.google.android.gms.measurement.internal.ar r6 = r1.f2528b     // Catch:{ all -> 0x0d3e }
            com.google.android.gms.measurement.internal.ed r6 = r6.e()     // Catch:{ all -> 0x0d3e }
            java.security.SecureRandom r6 = r6.g()     // Catch:{ all -> 0x0d3e }
            com.google.android.gms.internal.measurement.ct[] r7 = r5.f2142b     // Catch:{ all -> 0x0d3e }
            int r8 = r7.length     // Catch:{ all -> 0x0d3e }
            r9 = 0
            r10 = 0
        L_0x08a2:
            if (r9 >= r8) goto L_0x0b4a
            r11 = r7[r9]     // Catch:{ all -> 0x0d3e }
            java.lang.String r12 = r11.f2137b     // Catch:{ all -> 0x0d3e }
            java.lang.String r13 = "_ep"
            boolean r12 = r12.equals(r13)     // Catch:{ all -> 0x0d3e }
            if (r12 == 0) goto L_0x092b
            r43.f()     // Catch:{ all -> 0x0d5f }
            java.lang.String r12 = "_en"
            java.lang.Object r12 = com.google.android.gms.measurement.internal.ea.b(r11, r12)     // Catch:{ all -> 0x0d5f }
            java.lang.String r12 = (java.lang.String) r12     // Catch:{ all -> 0x0d5f }
            java.lang.Object r13 = r2.get(r12)     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.d r13 = (com.google.android.gms.measurement.internal.d) r13     // Catch:{ all -> 0x0d5f }
            if (r13 != 0) goto L_0x08d2
            com.google.android.gms.measurement.internal.eo r13 = r43.d()     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cw r14 = r3.f2529a     // Catch:{ all -> 0x0d5f }
            java.lang.String r14 = r14.o     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.d r13 = r13.a(r14, r12)     // Catch:{ all -> 0x0d5f }
            r2.put(r12, r13)     // Catch:{ all -> 0x0d5f }
        L_0x08d2:
            java.lang.Long r12 = r13.h     // Catch:{ all -> 0x0d5f }
            if (r12 != 0) goto L_0x091e
            java.lang.Long r12 = r13.i     // Catch:{ all -> 0x0d5f }
            long r14 = r12.longValue()     // Catch:{ all -> 0x0d5f }
            int r12 = (r14 > r19 ? 1 : (r14 == r19 ? 0 : -1))
            if (r12 <= 0) goto L_0x08ef
            r43.f()     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cu[] r12 = r11.f2136a     // Catch:{ all -> 0x0d5f }
            java.lang.String r14 = "_sr"
            java.lang.Long r15 = r13.i     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cu[] r12 = com.google.android.gms.measurement.internal.ea.a(r12, r14, r15)     // Catch:{ all -> 0x0d5f }
            r11.f2136a = r12     // Catch:{ all -> 0x0d5f }
        L_0x08ef:
            java.lang.Boolean r12 = r13.j     // Catch:{ all -> 0x0d5f }
            if (r12 == 0) goto L_0x090c
            java.lang.Boolean r12 = r13.j     // Catch:{ all -> 0x0d5f }
            boolean r12 = r12.booleanValue()     // Catch:{ all -> 0x0d5f }
            if (r12 == 0) goto L_0x090c
            r43.f()     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cu[] r12 = r11.f2136a     // Catch:{ all -> 0x0d5f }
            java.lang.String r13 = "_efs"
            java.lang.Long r14 = java.lang.Long.valueOf(r19)     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cu[] r12 = com.google.android.gms.measurement.internal.ea.a(r12, r13, r14)     // Catch:{ all -> 0x0d5f }
            r11.f2136a = r12     // Catch:{ all -> 0x0d5f }
        L_0x090c:
            int r12 = r10 + 1
            r4[r10] = r11     // Catch:{ all -> 0x0d5f }
            r25 = r5
            r24 = r6
            r21 = r7
            r22 = r8
            r23 = r9
            r10 = r12
            r12 = r2
            goto L_0x0b3b
        L_0x091e:
            r12 = r2
            r25 = r5
            r24 = r6
            r21 = r7
            r22 = r8
            r23 = r9
            goto L_0x0b3b
        L_0x092b:
            com.google.android.gms.measurement.internal.al r12 = r43.n()     // Catch:{ all -> 0x0d3e }
            com.google.android.gms.internal.measurement.cw r13 = r3.f2529a     // Catch:{ all -> 0x0d3e }
            java.lang.String r13 = r13.o     // Catch:{ all -> 0x0d3e }
            long r12 = r12.e(r13)     // Catch:{ all -> 0x0d3e }
            com.google.android.gms.measurement.internal.ar r14 = r1.f2528b     // Catch:{ all -> 0x0d3e }
            r14.e()     // Catch:{ all -> 0x0d3e }
            java.lang.Long r14 = r11.c     // Catch:{ all -> 0x0d3e }
            long r14 = r14.longValue()     // Catch:{ all -> 0x0d3e }
            long r14 = com.google.android.gms.measurement.internal.ed.a(r14, r12)     // Catch:{ all -> 0x0d3e }
            r21 = r7
            java.lang.String r7 = "_dbg"
            r22 = r8
            java.lang.Long r8 = java.lang.Long.valueOf(r19)     // Catch:{ all -> 0x0d3e }
            boolean r23 = android.text.TextUtils.isEmpty(r7)     // Catch:{ all -> 0x0d3e }
            if (r23 != 0) goto L_0x0985
            if (r8 != 0) goto L_0x0959
            goto L_0x0985
        L_0x0959:
            r25 = r5
            com.google.android.gms.internal.measurement.cu[] r5 = r11.f2136a     // Catch:{ all -> 0x0d5f }
            r23 = r9
            int r9 = r5.length     // Catch:{ all -> 0x0d5f }
            r26 = r12
            r12 = 0
        L_0x0963:
            if (r12 >= r9) goto L_0x098b
            r13 = r5[r12]     // Catch:{ all -> 0x0d5f }
            r24 = r5
            java.lang.String r5 = r13.f2138a     // Catch:{ all -> 0x0d5f }
            boolean r5 = r7.equals(r5)     // Catch:{ all -> 0x0d5f }
            if (r5 == 0) goto L_0x0980
            boolean r5 = r8 instanceof java.lang.Long     // Catch:{ all -> 0x0d5f }
            if (r5 == 0) goto L_0x098b
            java.lang.Long r5 = r13.c     // Catch:{ all -> 0x0d5f }
            boolean r5 = r8.equals(r5)     // Catch:{ all -> 0x0d5f }
            if (r5 != 0) goto L_0x097e
            goto L_0x098b
        L_0x097e:
            r5 = 1
            goto L_0x098c
        L_0x0980:
            int r12 = r12 + 1
            r5 = r24
            goto L_0x0963
        L_0x0985:
            r25 = r5
            r23 = r9
            r26 = r12
        L_0x098b:
            r5 = 0
        L_0x098c:
            if (r5 != 0) goto L_0x099d
            com.google.android.gms.measurement.internal.al r5 = r43.n()     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cw r7 = r3.f2529a     // Catch:{ all -> 0x0d5f }
            java.lang.String r7 = r7.o     // Catch:{ all -> 0x0d5f }
            java.lang.String r8 = r11.f2137b     // Catch:{ all -> 0x0d5f }
            int r13 = r5.d(r7, r8)     // Catch:{ all -> 0x0d5f }
            goto L_0x099e
        L_0x099d:
            r13 = 1
        L_0x099e:
            if (r13 > 0) goto L_0x09bd
            com.google.android.gms.measurement.internal.ar r5 = r1.f2528b     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.o r5 = r5.q()     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.q r5 = r5.f     // Catch:{ all -> 0x0d5f }
            java.lang.String r7 = "Sample rate must be positive. event, rate"
            java.lang.String r8 = r11.f2137b     // Catch:{ all -> 0x0d5f }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r13)     // Catch:{ all -> 0x0d5f }
            r5.a(r7, r8, r9)     // Catch:{ all -> 0x0d5f }
            int r5 = r10 + 1
            r4[r10] = r11     // Catch:{ all -> 0x0d5f }
            r12 = r2
            r10 = r5
            r24 = r6
            goto L_0x0b3b
        L_0x09bd:
            java.lang.String r5 = r11.f2137b     // Catch:{ all -> 0x0d3e }
            java.lang.Object r5 = r2.get(r5)     // Catch:{ all -> 0x0d3e }
            com.google.android.gms.measurement.internal.d r5 = (com.google.android.gms.measurement.internal.d) r5     // Catch:{ all -> 0x0d3e }
            if (r5 != 0) goto L_0x0a0f
            com.google.android.gms.measurement.internal.eo r5 = r43.d()     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cw r7 = r3.f2529a     // Catch:{ all -> 0x0d5f }
            java.lang.String r7 = r7.o     // Catch:{ all -> 0x0d5f }
            java.lang.String r8 = r11.f2137b     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.d r5 = r5.a(r7, r8)     // Catch:{ all -> 0x0d5f }
            if (r5 != 0) goto L_0x0a0f
            com.google.android.gms.measurement.internal.ar r5 = r1.f2528b     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.o r5 = r5.q()     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.q r5 = r5.f     // Catch:{ all -> 0x0d5f }
            java.lang.String r7 = "Event being bundled has no eventAggregate. appId, eventName"
            com.google.android.gms.internal.measurement.cw r8 = r3.f2529a     // Catch:{ all -> 0x0d5f }
            java.lang.String r8 = r8.o     // Catch:{ all -> 0x0d5f }
            java.lang.String r9 = r11.f2137b     // Catch:{ all -> 0x0d5f }
            r5.a(r7, r8, r9)     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.d r5 = new com.google.android.gms.measurement.internal.d     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cw r7 = r3.f2529a     // Catch:{ all -> 0x0d5f }
            java.lang.String r7 = r7.o     // Catch:{ all -> 0x0d5f }
            java.lang.String r8 = r11.f2137b     // Catch:{ all -> 0x0d5f }
            r31 = 1
            r33 = 1
            java.lang.Long r9 = r11.c     // Catch:{ all -> 0x0d5f }
            long r35 = r9.longValue()     // Catch:{ all -> 0x0d5f }
            r37 = 0
            r39 = 0
            r40 = 0
            r41 = 0
            r42 = 0
            r28 = r5
            r29 = r7
            r30 = r8
            r28.<init>(r29, r30, r31, r33, r35, r37, r39, r40, r41, r42)     // Catch:{ all -> 0x0d5f }
        L_0x0a0f:
            r43.f()     // Catch:{ all -> 0x0d3e }
            java.lang.String r7 = "_eid"
            java.lang.Object r7 = com.google.android.gms.measurement.internal.ea.b(r11, r7)     // Catch:{ all -> 0x0d3e }
            java.lang.Long r7 = (java.lang.Long) r7     // Catch:{ all -> 0x0d3e }
            if (r7 == 0) goto L_0x0a1e
            r8 = 1
            goto L_0x0a1f
        L_0x0a1e:
            r8 = 0
        L_0x0a1f:
            java.lang.Boolean r8 = java.lang.Boolean.valueOf(r8)     // Catch:{ all -> 0x0d3e }
            r9 = 1
            if (r13 != r9) goto L_0x0a4c
            int r7 = r10 + 1
            r4[r10] = r11     // Catch:{ all -> 0x0d5f }
            boolean r8 = r8.booleanValue()     // Catch:{ all -> 0x0d5f }
            if (r8 == 0) goto L_0x0a46
            java.lang.Long r8 = r5.h     // Catch:{ all -> 0x0d5f }
            if (r8 != 0) goto L_0x0a3c
            java.lang.Long r8 = r5.i     // Catch:{ all -> 0x0d5f }
            if (r8 != 0) goto L_0x0a3c
            java.lang.Boolean r8 = r5.j     // Catch:{ all -> 0x0d5f }
            if (r8 == 0) goto L_0x0a46
        L_0x0a3c:
            r8 = 0
            com.google.android.gms.measurement.internal.d r5 = r5.a(r8, r8, r8)     // Catch:{ all -> 0x0d5f }
            java.lang.String r8 = r11.f2137b     // Catch:{ all -> 0x0d5f }
            r2.put(r8, r5)     // Catch:{ all -> 0x0d5f }
        L_0x0a46:
            r12 = r2
            r24 = r6
            r10 = r7
            goto L_0x0b3b
        L_0x0a4c:
            int r9 = r6.nextInt(r13)     // Catch:{ all -> 0x0d3e }
            if (r9 != 0) goto L_0x0a8c
            r43.f()     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cu[] r7 = r11.f2136a     // Catch:{ all -> 0x0d5f }
            java.lang.String r9 = "_sr"
            long r12 = (long) r13     // Catch:{ all -> 0x0d5f }
            r24 = r6
            java.lang.Long r6 = java.lang.Long.valueOf(r12)     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.internal.measurement.cu[] r6 = com.google.android.gms.measurement.internal.ea.a(r7, r9, r6)     // Catch:{ all -> 0x0d5f }
            r11.f2136a = r6     // Catch:{ all -> 0x0d5f }
            int r6 = r10 + 1
            r4[r10] = r11     // Catch:{ all -> 0x0d5f }
            boolean r7 = r8.booleanValue()     // Catch:{ all -> 0x0d5f }
            if (r7 == 0) goto L_0x0a79
            java.lang.Long r7 = java.lang.Long.valueOf(r12)     // Catch:{ all -> 0x0d5f }
            r8 = 0
            com.google.android.gms.measurement.internal.d r5 = r5.a(r8, r7, r8)     // Catch:{ all -> 0x0d5f }
        L_0x0a79:
            java.lang.String r7 = r11.f2137b     // Catch:{ all -> 0x0d5f }
            java.lang.Long r8 = r11.c     // Catch:{ all -> 0x0d5f }
            long r8 = r8.longValue()     // Catch:{ all -> 0x0d5f }
            com.google.android.gms.measurement.internal.d r5 = r5.a(r8, r14)     // Catch:{ all -> 0x0d5f }
            r2.put(r7, r5)     // Catch:{ all -> 0x0d5f }
            r12 = r2
            r10 = r6
            goto L_0x0b3b
        L_0x0a8c:
            r24 = r6
            com.google.android.gms.measurement.internal.ar r6 = r1.f2528b     // Catch:{ all -> 0x0d3e }
            com.google.android.gms.measurement.internal.el r6 = r6.e     // Catch:{ all -> 0x0d3e }
            com.google.android.gms.internal.measurement.cw r9 = r3.f2529a     // Catch:{ all -> 0x0d3e }
            java.lang.String r9 = r9.o     // Catch:{ all -> 0x0d3e }
            com.google.android.gms.measurement.internal.h$a<java.lang.Boolean> r12 = com.google.android.gms.measurement.internal.h.af     // Catch:{ all -> 0x0d3e }
            boolean r6 = r6.c(r9, r12)     // Catch:{ all -> 0x0d3e }
            if (r6 == 0) goto L_0x0ac6
            java.lang.Long r6 = r5.g     // Catch:{ all -> 0x0d3e }
            if (r6 == 0) goto L_0x0aab
            java.lang.Long r6 = r5.g     // Catch:{ all -> 0x0d5f }
            long r26 = r6.longValue()     // Catch:{ all -> 0x0d5f }
            r12 = r2
            r9 = r7
            goto L_0x0abe
        L_0x0aab:
            com.google.android.gms.measurement.internal.ar r6 = r1.f2528b     // Catch:{ all -> 0x0d3e }
            r6.e()     // Catch:{ all -> 0x0d3e }
            java.lang.Long r6 = r11.d     // Catch:{ all -> 0x0d3e }
            r9 = r7
            long r6 = r6.longValue()     // Catch:{ all -> 0x0d3e }
            r12 = r2
            r1 = r26
            long r26 = com.google.android.gms.measurement.internal.ed.a(r6, r1)     // Catch:{ all -> 0x0d3e }
        L_0x0abe:
            int r1 = (r26 > r14 ? 1 : (r26 == r14 ? 0 : -1))
            if (r1 == 0) goto L_0x0ac4
        L_0x0ac2:
            r1 = 1
            goto L_0x0add
        L_0x0ac4:
            r1 = 0
            goto L_0x0add
        L_0x0ac6:
            r12 = r2
            r9 = r7
            long r1 = r5.f     // Catch:{ all -> 0x0d3e }
            java.lang.Long r6 = r11.c     // Catch:{ all -> 0x0d3e }
            long r6 = r6.longValue()     // Catch:{ all -> 0x0d3e }
            long r6 = r6 - r1
            long r1 = java.lang.Math.abs(r6)     // Catch:{ all -> 0x0d3e }
            r6 = 86400000(0x5265c00, double:4.2687272E-316)
            int r26 = (r1 > r6 ? 1 : (r1 == r6 ? 0 : -1))
            if (r26 < 0) goto L_0x0ac4
            goto L_0x0ac2
        L_0x0add:
            if (r1 == 0) goto L_0x0b2b
            r43.f()     // Catch:{ all -> 0x0d3e }
            com.google.android.gms.internal.measurement.cu[] r1 = r11.f2136a     // Catch:{ all -> 0x0d3e }
            java.lang.String r2 = "_efs"
            java.lang.Long r6 = java.lang.Long.valueOf(r19)     // Catch:{ all -> 0x0d3e }
            com.google.android.gms.internal.measurement.cu[] r1 = com.google.android.gms.measurement.internal.ea.a(r1, r2, r6)     // Catch:{ all -> 0x0d3e }
            r11.f2136a = r1     // Catch:{ all -> 0x0d3e }
            r43.f()     // Catch:{ all -> 0x0d3e }
            com.google.android.gms.internal.measurement.cu[] r1 = r11.f2136a     // Catch:{ all -> 0x0d3e }
            java.lang.String r2 = "_sr"
            long r6 = (long) r13     // Catch:{ all -> 0x0d3e }
            java.lang.Long r9 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x0d3e }
            com.google.android.gms.internal.measurement.cu[] r1 = com.google.android.gms.measurement.internal.ea.a(r1, r2, r9)     // Catch:{ all -> 0x0d3e }
            r11.f2136a = r1     // Catch:{ all -> 0x0d3e }
            int r1 = r10 + 1
            r4[r10] = r11     // Catch:{ all -> 0x0d3e }
            boolean r2 = r8.booleanValue()     // Catch:{ all -> 0x0d3e }
            if (r2 == 0) goto L_0x0b1a
            java.lang.Long r2 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x0d3e }
            r6 = 1
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r6)     // Catch:{ all -> 0x0d3e }
            r6 = 0
            com.google.android.gms.measurement.internal.d r5 = r5.a(r6, r2, r7)     // Catch:{ all -> 0x0d3e }
        L_0x0b1a:
            java.lang.String r2 = r11.f2137b     // Catch:{ all -> 0x0d3e }
            java.lang.Long r6 = r11.c     // Catch:{ all -> 0x0d3e }
            long r6 = r6.longValue()     // Catch:{ all -> 0x0d3e }
            com.google.android.gms.measurement.internal.d r5 = r5.a(r6, r14)     // Catch:{ all -> 0x0d3e }
            r12.put(r2, r5)     // Catch:{ all -> 0x0d3e }
            r10 = r1
            goto L_0x0b3b
        L_0x0b2b:
            boolean r1 = r8.booleanValue()     // Catch:{ all -> 0x0d3e }
            if (r1 == 0) goto L_0x0b3b
            java.lang.String r1 = r11.f2137b     // Catch:{ all -> 0x0d3e }
            r2 = 0
            com.google.android.gms.measurement.internal.d r5 = r5.a(r9, r2, r2)     // Catch:{ all -> 0x0d3e }
            r12.put(r1, r5)     // Catch:{ all -> 0x0d3e }
        L_0x0b3b:
            int r9 = r23 + 1
            r1 = r43
            r2 = r12
            r7 = r21
            r8 = r22
            r6 = r24
            r5 = r25
            goto L_0x08a2
        L_0x0b4a:
            r12 = r2
            r1 = r5
            com.google.android.gms.internal.measurement.ct[] r2 = r1.f2142b     // Catch:{ all -> 0x0d3e }
            int r2 = r2.length     // Catch:{ all -> 0x0d3e }
            if (r10 >= r2) goto L_0x0b59
            java.lang.Object[] r2 = java.util.Arrays.copyOf(r4, r10)     // Catch:{ all -> 0x0d3e }
            com.google.android.gms.internal.measurement.ct[] r2 = (com.google.android.gms.internal.measurement.ct[]) r2     // Catch:{ all -> 0x0d3e }
            r1.f2142b = r2     // Catch:{ all -> 0x0d3e }
        L_0x0b59:
            java.util.Set r2 = r12.entrySet()     // Catch:{ all -> 0x0d3e }
            java.util.Iterator r2 = r2.iterator()     // Catch:{ all -> 0x0d3e }
        L_0x0b61:
            boolean r4 = r2.hasNext()     // Catch:{ all -> 0x0d3e }
            if (r4 == 0) goto L_0x0b7c
            java.lang.Object r4 = r2.next()     // Catch:{ all -> 0x0d3e }
            java.util.Map$Entry r4 = (java.util.Map.Entry) r4     // Catch:{ all -> 0x0d3e }
            com.google.android.gms.measurement.internal.eo r5 = r43.d()     // Catch:{ all -> 0x0d3e }
            java.lang.Object r4 = r4.getValue()     // Catch:{ all -> 0x0d3e }
            com.google.android.gms.measurement.internal.d r4 = (com.google.android.gms.measurement.internal.d) r4     // Catch:{ all -> 0x0d3e }
            r5.a(r4)     // Catch:{ all -> 0x0d3e }
            goto L_0x0b61
        L_0x0b7b:
            r1 = r5
        L_0x0b7c:
            r4 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            java.lang.Long r2 = java.lang.Long.valueOf(r4)     // Catch:{ all -> 0x0d3e }
            r1.e = r2     // Catch:{ all -> 0x0d3e }
            r4 = -9223372036854775808
            java.lang.Long r2 = java.lang.Long.valueOf(r4)     // Catch:{ all -> 0x0d3e }
            r1.f = r2     // Catch:{ all -> 0x0d3e }
            r2 = 0
        L_0x0b90:
            com.google.android.gms.internal.measurement.ct[] r4 = r1.f2142b     // Catch:{ all -> 0x0d3e }
            int r4 = r4.length     // Catch:{ all -> 0x0d3e }
            if (r2 >= r4) goto L_0x0bc4
            com.google.android.gms.internal.measurement.ct[] r4 = r1.f2142b     // Catch:{ all -> 0x0d3e }
            r4 = r4[r2]     // Catch:{ all -> 0x0d3e }
            java.lang.Long r5 = r4.c     // Catch:{ all -> 0x0d3e }
            long r5 = r5.longValue()     // Catch:{ all -> 0x0d3e }
            java.lang.Long r7 = r1.e     // Catch:{ all -> 0x0d3e }
            long r7 = r7.longValue()     // Catch:{ all -> 0x0d3e }
            int r9 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r9 >= 0) goto L_0x0bad
            java.lang.Long r5 = r4.c     // Catch:{ all -> 0x0d3e }
            r1.e = r5     // Catch:{ all -> 0x0d3e }
        L_0x0bad:
            java.lang.Long r5 = r4.c     // Catch:{ all -> 0x0d3e }
            long r5 = r5.longValue()     // Catch:{ all -> 0x0d3e }
            java.lang.Long r7 = r1.f     // Catch:{ all -> 0x0d3e }
            long r7 = r7.longValue()     // Catch:{ all -> 0x0d3e }
            int r9 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r9 <= 0) goto L_0x0bc1
            java.lang.Long r4 = r4.c     // Catch:{ all -> 0x0d3e }
            r1.f = r4     // Catch:{ all -> 0x0d3e }
        L_0x0bc1:
            int r2 = r2 + 1
            goto L_0x0b90
        L_0x0bc4:
            com.google.android.gms.internal.measurement.cw r2 = r3.f2529a     // Catch:{ all -> 0x0d3e }
            java.lang.String r2 = r2.o     // Catch:{ all -> 0x0d3e }
            com.google.android.gms.measurement.internal.eo r4 = r43.d()     // Catch:{ all -> 0x0d3e }
            com.google.android.gms.measurement.internal.ef r4 = r4.b(r2)     // Catch:{ all -> 0x0d3e }
            if (r4 != 0) goto L_0x0bea
            r5 = r43
            com.google.android.gms.measurement.internal.ar r4 = r5.f2528b     // Catch:{ all -> 0x0d5d }
            com.google.android.gms.measurement.internal.o r4 = r4.q()     // Catch:{ all -> 0x0d5d }
            com.google.android.gms.measurement.internal.q r4 = r4.c     // Catch:{ all -> 0x0d5d }
            java.lang.String r6 = "Bundling raw events w/o app info. appId"
            com.google.android.gms.internal.measurement.cw r7 = r3.f2529a     // Catch:{ all -> 0x0d5d }
            java.lang.String r7 = r7.o     // Catch:{ all -> 0x0d5d }
            java.lang.Object r7 = com.google.android.gms.measurement.internal.o.a(r7)     // Catch:{ all -> 0x0d5d }
            r4.a(r6, r7)     // Catch:{ all -> 0x0d5d }
            goto L_0x0c53
        L_0x0bea:
            r5 = r43
            com.google.android.gms.internal.measurement.ct[] r6 = r1.f2142b     // Catch:{ all -> 0x0d5d }
            int r6 = r6.length     // Catch:{ all -> 0x0d5d }
            if (r6 <= 0) goto L_0x0c53
            long r6 = r4.h()     // Catch:{ all -> 0x0d5d }
            r8 = 0
            int r10 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r10 == 0) goto L_0x0c00
            java.lang.Long r12 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x0d5d }
            goto L_0x0c01
        L_0x0c00:
            r12 = 0
        L_0x0c01:
            r1.h = r12     // Catch:{ all -> 0x0d5d }
            long r8 = r4.g()     // Catch:{ all -> 0x0d5d }
            r10 = 0
            int r12 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r12 != 0) goto L_0x0c0e
            goto L_0x0c0f
        L_0x0c0e:
            r6 = r8
        L_0x0c0f:
            int r8 = (r6 > r10 ? 1 : (r6 == r10 ? 0 : -1))
            if (r8 == 0) goto L_0x0c18
            java.lang.Long r12 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x0d5d }
            goto L_0x0c19
        L_0x0c18:
            r12 = 0
        L_0x0c19:
            r1.g = r12     // Catch:{ all -> 0x0d5d }
            r4.r()     // Catch:{ all -> 0x0d5d }
            long r6 = r4.o()     // Catch:{ all -> 0x0d5d }
            int r7 = (int) r6     // Catch:{ all -> 0x0d5d }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r7)     // Catch:{ all -> 0x0d5d }
            r1.w = r6     // Catch:{ all -> 0x0d5d }
            java.lang.Long r6 = r1.e     // Catch:{ all -> 0x0d5d }
            long r6 = r6.longValue()     // Catch:{ all -> 0x0d5d }
            r4.a(r6)     // Catch:{ all -> 0x0d5d }
            java.lang.Long r6 = r1.f     // Catch:{ all -> 0x0d5d }
            long r6 = r6.longValue()     // Catch:{ all -> 0x0d5d }
            r4.b(r6)     // Catch:{ all -> 0x0d5d }
            com.google.android.gms.measurement.internal.ar r6 = r4.f2546a     // Catch:{ all -> 0x0d5d }
            com.google.android.gms.measurement.internal.am r6 = r6.p()     // Catch:{ all -> 0x0d5d }
            r6.c()     // Catch:{ all -> 0x0d5d }
            java.lang.String r6 = r4.h     // Catch:{ all -> 0x0d5d }
            r7 = 0
            r4.h(r7)     // Catch:{ all -> 0x0d5d }
            r1.x = r6     // Catch:{ all -> 0x0d5d }
            com.google.android.gms.measurement.internal.eo r6 = r43.d()     // Catch:{ all -> 0x0d5d }
            r6.a(r4)     // Catch:{ all -> 0x0d5d }
        L_0x0c53:
            com.google.android.gms.internal.measurement.ct[] r4 = r1.f2142b     // Catch:{ all -> 0x0d5d }
            int r4 = r4.length     // Catch:{ all -> 0x0d5d }
            if (r4 <= 0) goto L_0x0ca1
            com.google.android.gms.measurement.internal.al r4 = r43.n()     // Catch:{ all -> 0x0d5d }
            com.google.android.gms.internal.measurement.cw r6 = r3.f2529a     // Catch:{ all -> 0x0d5d }
            java.lang.String r6 = r6.o     // Catch:{ all -> 0x0d5d }
            com.google.android.gms.internal.measurement.cp r4 = r4.a(r6)     // Catch:{ all -> 0x0d5d }
            if (r4 == 0) goto L_0x0c70
            java.lang.Long r6 = r4.f2128a     // Catch:{ all -> 0x0d5d }
            if (r6 != 0) goto L_0x0c6b
            goto L_0x0c70
        L_0x0c6b:
            java.lang.Long r4 = r4.f2128a     // Catch:{ all -> 0x0d5d }
            r1.E = r4     // Catch:{ all -> 0x0d5d }
            goto L_0x0c98
        L_0x0c70:
            com.google.android.gms.internal.measurement.cw r4 = r3.f2529a     // Catch:{ all -> 0x0d5d }
            java.lang.String r4 = r4.y     // Catch:{ all -> 0x0d5d }
            boolean r4 = android.text.TextUtils.isEmpty(r4)     // Catch:{ all -> 0x0d5d }
            if (r4 == 0) goto L_0x0c83
            r6 = -1
            java.lang.Long r4 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x0d5d }
            r1.E = r4     // Catch:{ all -> 0x0d5d }
            goto L_0x0c98
        L_0x0c83:
            com.google.android.gms.measurement.internal.ar r4 = r5.f2528b     // Catch:{ all -> 0x0d5d }
            com.google.android.gms.measurement.internal.o r4 = r4.q()     // Catch:{ all -> 0x0d5d }
            com.google.android.gms.measurement.internal.q r4 = r4.f     // Catch:{ all -> 0x0d5d }
            java.lang.String r6 = "Did not find measurement config or missing version info. appId"
            com.google.android.gms.internal.measurement.cw r7 = r3.f2529a     // Catch:{ all -> 0x0d5d }
            java.lang.String r7 = r7.o     // Catch:{ all -> 0x0d5d }
            java.lang.Object r7 = com.google.android.gms.measurement.internal.o.a(r7)     // Catch:{ all -> 0x0d5d }
            r4.a(r6, r7)     // Catch:{ all -> 0x0d5d }
        L_0x0c98:
            com.google.android.gms.measurement.internal.eo r4 = r43.d()     // Catch:{ all -> 0x0d5d }
            r13 = r18
            r4.a(r1, r13)     // Catch:{ all -> 0x0d5d }
        L_0x0ca1:
            com.google.android.gms.measurement.internal.eo r1 = r43.d()     // Catch:{ all -> 0x0d5d }
            java.util.List<java.lang.Long> r3 = r3.f2530b     // Catch:{ all -> 0x0d5d }
            com.google.android.gms.common.internal.l.a(r3)     // Catch:{ all -> 0x0d5d }
            r1.c()     // Catch:{ all -> 0x0d5d }
            r1.j()     // Catch:{ all -> 0x0d5d }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0d5d }
            java.lang.String r6 = "rowid in ("
            r4.<init>(r6)     // Catch:{ all -> 0x0d5d }
            r6 = 0
        L_0x0cb8:
            int r7 = r3.size()     // Catch:{ all -> 0x0d5d }
            if (r6 >= r7) goto L_0x0cd5
            if (r6 == 0) goto L_0x0cc5
            java.lang.String r7 = ","
            r4.append(r7)     // Catch:{ all -> 0x0d5d }
        L_0x0cc5:
            java.lang.Object r7 = r3.get(r6)     // Catch:{ all -> 0x0d5d }
            java.lang.Long r7 = (java.lang.Long) r7     // Catch:{ all -> 0x0d5d }
            long r7 = r7.longValue()     // Catch:{ all -> 0x0d5d }
            r4.append(r7)     // Catch:{ all -> 0x0d5d }
            int r6 = r6 + 1
            goto L_0x0cb8
        L_0x0cd5:
            java.lang.String r6 = ")"
            r4.append(r6)     // Catch:{ all -> 0x0d5d }
            android.database.sqlite.SQLiteDatabase r6 = r1.w()     // Catch:{ all -> 0x0d5d }
            java.lang.String r7 = "raw_events"
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0d5d }
            r8 = 0
            int r4 = r6.delete(r7, r4, r8)     // Catch:{ all -> 0x0d5d }
            int r6 = r3.size()     // Catch:{ all -> 0x0d5d }
            if (r4 == r6) goto L_0x0d06
            com.google.android.gms.measurement.internal.o r1 = r1.q()     // Catch:{ all -> 0x0d5d }
            com.google.android.gms.measurement.internal.q r1 = r1.c     // Catch:{ all -> 0x0d5d }
            java.lang.String r6 = "Deleted fewer rows from raw events table than expected"
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x0d5d }
            int r3 = r3.size()     // Catch:{ all -> 0x0d5d }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x0d5d }
            r1.a(r6, r4, r3)     // Catch:{ all -> 0x0d5d }
        L_0x0d06:
            com.google.android.gms.measurement.internal.eo r1 = r43.d()     // Catch:{ all -> 0x0d5d }
            android.database.sqlite.SQLiteDatabase r3 = r1.w()     // Catch:{ all -> 0x0d5d }
            java.lang.String r4 = "delete from raw_events_metadata where app_id=? and metadata_fingerprint not in (select distinct metadata_fingerprint from raw_events where app_id=?)"
            r6 = 2
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ SQLiteException -> 0x0d1d }
            r7 = 0
            r6[r7] = r2     // Catch:{ SQLiteException -> 0x0d1d }
            r7 = 1
            r6[r7] = r2     // Catch:{ SQLiteException -> 0x0d1d }
            r3.execSQL(r4, r6)     // Catch:{ SQLiteException -> 0x0d1d }
            goto L_0x0d2e
        L_0x0d1d:
            r0 = move-exception
            r3 = r0
            com.google.android.gms.measurement.internal.o r1 = r1.q()     // Catch:{ all -> 0x0d5d }
            com.google.android.gms.measurement.internal.q r1 = r1.c     // Catch:{ all -> 0x0d5d }
            java.lang.String r4 = "Failed to remove unused event metadata. appId"
            java.lang.Object r2 = com.google.android.gms.measurement.internal.o.a(r2)     // Catch:{ all -> 0x0d5d }
            r1.a(r4, r2, r3)     // Catch:{ all -> 0x0d5d }
        L_0x0d2e:
            com.google.android.gms.measurement.internal.eo r1 = r43.d()     // Catch:{ all -> 0x0d5d }
            r1.u()     // Catch:{ all -> 0x0d5d }
            com.google.android.gms.measurement.internal.eo r1 = r43.d()
            r1.v()
            r1 = 1
            return r1
        L_0x0d3e:
            r0 = move-exception
            r5 = r43
            goto L_0x0d61
        L_0x0d42:
            r5 = r1
            com.google.android.gms.measurement.internal.eo r1 = r43.d()     // Catch:{ all -> 0x0d5d }
            r1.u()     // Catch:{ all -> 0x0d5d }
            com.google.android.gms.measurement.internal.eo r1 = r43.d()
            r1.v()
            r1 = 0
            return r1
        L_0x0d53:
            r0 = move-exception
            r5 = r1
            goto L_0x024a
        L_0x0d57:
            if (r9 == 0) goto L_0x0d5c
            r9.close()     // Catch:{ all -> 0x0d5d }
        L_0x0d5c:
            throw r1     // Catch:{ all -> 0x0d5d }
        L_0x0d5d:
            r0 = move-exception
            goto L_0x0d61
        L_0x0d5f:
            r0 = move-exception
            r5 = r1
        L_0x0d61:
            r1 = r0
            com.google.android.gms.measurement.internal.eo r2 = r43.d()
            r2.v()
            goto L_0x0d6b
        L_0x0d6a:
            throw r1
        L_0x0d6b:
            goto L_0x0d6a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.du.a(long):boolean");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.ea.a(com.google.android.gms.internal.measurement.cu[], java.lang.String, java.lang.Object):com.google.android.gms.internal.measurement.cu[]
     arg types: [com.google.android.gms.internal.measurement.cu[], java.lang.String, long]
     candidates:
      com.google.android.gms.measurement.internal.ea.a(java.lang.StringBuilder, int, com.google.android.gms.internal.measurement.ck):void
      com.google.android.gms.measurement.internal.ea.a(com.google.android.gms.internal.measurement.cu[], java.lang.String, java.lang.Object):com.google.android.gms.internal.measurement.cu[] */
    private final boolean a(ct ctVar, ct ctVar2) {
        String str;
        l.b("_e".equals(ctVar.f2137b));
        f();
        cu a2 = ea.a(ctVar, "_sc");
        String str2 = null;
        if (a2 == null) {
            str = null;
        } else {
            str = a2.f2139b;
        }
        f();
        cu a3 = ea.a(ctVar2, "_pc");
        if (a3 != null) {
            str2 = a3.f2139b;
        }
        if (str2 == null || !str2.equals(str)) {
            return false;
        }
        f();
        cu a4 = ea.a(ctVar, "_et");
        if (a4.c != null && a4.c.longValue() > 0) {
            long longValue = a4.c.longValue();
            f();
            cu a5 = ea.a(ctVar2, "_et");
            if (!(a5 == null || a5.c == null || a5.c.longValue() <= 0)) {
                longValue += a5.c.longValue();
            }
            f();
            ctVar2.f2136a = ea.a(ctVar2.f2136a, "_et", Long.valueOf(longValue));
            f();
            ctVar.f2136a = ea.a(ctVar.f2136a, "_fr", (Object) 1L);
        }
        return true;
    }

    private static cu[] a(cu[] cuVarArr, String str) {
        int i2 = 0;
        while (true) {
            if (i2 >= cuVarArr.length) {
                i2 = -1;
                break;
            } else if (str.equals(cuVarArr[i2].f2138a)) {
                break;
            } else {
                i2++;
            }
        }
        if (i2 < 0) {
            return cuVarArr;
        }
        return a(cuVarArr, i2);
    }

    private static cu[] a(cu[] cuVarArr, int i2) {
        cu[] cuVarArr2 = new cu[(cuVarArr.length - 1)];
        if (i2 > 0) {
            System.arraycopy(cuVarArr, 0, cuVarArr2, 0, i2);
        }
        if (i2 < cuVarArr2.length) {
            System.arraycopy(cuVarArr, i2 + 1, cuVarArr2, i2, cuVarArr2.length - i2);
        }
        return cuVarArr2;
    }

    private static cu[] a(cu[] cuVarArr, int i2, String str) {
        for (cu cuVar : cuVarArr) {
            if ("_err".equals(cuVar.f2138a)) {
                return cuVarArr;
            }
        }
        cu[] cuVarArr2 = new cu[(cuVarArr.length + 2)];
        System.arraycopy(cuVarArr, 0, cuVarArr2, 0, cuVarArr.length);
        cu cuVar2 = new cu();
        cuVar2.f2138a = "_err";
        cuVar2.c = Long.valueOf((long) i2);
        cu cuVar3 = new cu();
        cuVar3.f2138a = "_ev";
        cuVar3.f2139b = str;
        cuVarArr2[cuVarArr2.length - 2] = cuVar2;
        cuVarArr2[cuVarArr2.length - 1] = cuVar3;
        return cuVarArr2;
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: package-private */
    public final void a(int i2, Throwable th, byte[] bArr, String str) {
        eo d2;
        g();
        h();
        if (bArr == null) {
            try {
                bArr = new byte[0];
            } catch (Throwable th2) {
                this.s = false;
                u();
                throw th2;
            }
        }
        List<Long> list = this.w;
        this.w = null;
        boolean z = true;
        if ((i2 == 200 || i2 == 204) && th == null) {
            try {
                this.f2528b.b().d.a(this.f2528b.l().a());
                this.f2528b.b().e.a(0);
                j();
                this.f2528b.q().k.a("Successful upload. Got network response. code, size", Integer.valueOf(i2), Integer.valueOf(bArr.length));
                d().e();
                try {
                    for (Long next : list) {
                        try {
                            d2 = d();
                            long longValue = next.longValue();
                            d2.c();
                            d2.j();
                            if (d2.w().delete("queue", "rowid=?", new String[]{String.valueOf(longValue)}) != 1) {
                                throw new SQLiteException("Deleted fewer rows from queue than expected");
                            }
                        } catch (SQLiteException e2) {
                            d2.q().c.a("Failed to delete a bundle in a queue table", e2);
                            throw e2;
                        } catch (SQLiteException e3) {
                            if (this.x == null || !this.x.contains(next)) {
                                throw e3;
                            }
                        }
                    }
                    d().u();
                    d().v();
                    this.x = null;
                    if (!c().e() || !t()) {
                        this.y = -1;
                        j();
                    } else {
                        i();
                    }
                    this.q = 0;
                } catch (Throwable th3) {
                    d().v();
                    throw th3;
                }
            } catch (SQLiteException e4) {
                this.f2528b.q().c.a("Database error while trying to delete uploaded bundles", e4);
                this.q = this.f2528b.l().b();
                this.f2528b.q().k.a("Disable upload, time", Long.valueOf(this.q));
            }
        } else {
            this.f2528b.q().k.a("Network upload failed. Will retry later. code, error", Integer.valueOf(i2), th);
            this.f2528b.b().e.a(this.f2528b.l().a());
            if (i2 != 503) {
                if (i2 != 429) {
                    z = false;
                }
            }
            if (z) {
                this.f2528b.b().f.a(this.f2528b.l().a());
            }
            if (this.f2528b.e.c(str, h.Y)) {
                d().a(list);
            }
            j();
        }
        this.s = false;
        u();
    }

    private final boolean t() {
        g();
        h();
        return d().C() || !TextUtils.isEmpty(d().x());
    }

    private final void a(ef efVar) {
        ArrayMap arrayMap;
        g();
        if (!TextUtils.isEmpty(efVar.c()) || (el.u() && !TextUtils.isEmpty(efVar.d()))) {
            Uri.Builder builder = new Uri.Builder();
            String c2 = efVar.c();
            if (TextUtils.isEmpty(c2) && el.u()) {
                c2 = efVar.d();
            }
            Uri.Builder encodedAuthority = builder.scheme(h.m.a()).encodedAuthority(h.n.a());
            String valueOf = String.valueOf(c2);
            encodedAuthority.path(valueOf.length() != 0 ? "config/app/".concat(valueOf) : new String("config/app/")).appendQueryParameter("app_instance_id", efVar.b()).appendQueryParameter("platform", Constants.PLATFORM).appendQueryParameter("gmp_version", String.valueOf(14710L));
            String uri = builder.build().toString();
            try {
                URL url = new URL(uri);
                this.f2528b.q().k.a("Fetching remote configuration", efVar.a());
                cp a2 = n().a(efVar.a());
                String b2 = n().b(efVar.a());
                if (a2 == null || TextUtils.isEmpty(b2)) {
                    arrayMap = null;
                } else {
                    ArrayMap arrayMap2 = new ArrayMap();
                    arrayMap2.put("If-Modified-Since", b2);
                    arrayMap = arrayMap2;
                }
                this.r = true;
                s c3 = c();
                String a3 = efVar.a();
                dx dxVar = new dx(this);
                c3.c();
                c3.j();
                l.a(url);
                l.a(dxVar);
                c3.p().b(new w(c3, a3, url, null, arrayMap, dxVar));
            } catch (MalformedURLException unused) {
                this.f2528b.q().c.a("Failed to parse config URL. Not fetching. appId", o.a(efVar.a()), uri);
            }
        } else {
            a(efVar.a(), 204, null, null, null);
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0134 A[Catch:{ all -> 0x0183, all -> 0x018c }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0142 A[Catch:{ all -> 0x0183, all -> 0x018c }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.lang.String r7, int r8, java.lang.Throwable r9, byte[] r10, java.util.Map<java.lang.String, java.util.List<java.lang.String>> r11) {
        /*
            r6 = this;
            r6.g()
            r6.h()
            com.google.android.gms.common.internal.l.a(r7)
            r0 = 0
            if (r10 != 0) goto L_0x000e
            byte[] r10 = new byte[r0]     // Catch:{ all -> 0x018c }
        L_0x000e:
            com.google.android.gms.measurement.internal.ar r1 = r6.f2528b     // Catch:{ all -> 0x018c }
            com.google.android.gms.measurement.internal.o r1 = r1.q()     // Catch:{ all -> 0x018c }
            com.google.android.gms.measurement.internal.q r1 = r1.k     // Catch:{ all -> 0x018c }
            java.lang.String r2 = "onConfigFetched. Response size"
            int r3 = r10.length     // Catch:{ all -> 0x018c }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x018c }
            r1.a(r2, r3)     // Catch:{ all -> 0x018c }
            com.google.android.gms.measurement.internal.eo r1 = r6.d()     // Catch:{ all -> 0x018c }
            r1.e()     // Catch:{ all -> 0x018c }
            com.google.android.gms.measurement.internal.eo r1 = r6.d()     // Catch:{ all -> 0x0183 }
            com.google.android.gms.measurement.internal.ef r1 = r1.b(r7)     // Catch:{ all -> 0x0183 }
            r2 = 200(0xc8, float:2.8E-43)
            r3 = 304(0x130, float:4.26E-43)
            r4 = 1
            if (r8 == r2) goto L_0x003c
            r2 = 204(0xcc, float:2.86E-43)
            if (r8 == r2) goto L_0x003c
            if (r8 != r3) goto L_0x0040
        L_0x003c:
            if (r9 != 0) goto L_0x0040
            r2 = 1
            goto L_0x0041
        L_0x0040:
            r2 = 0
        L_0x0041:
            if (r1 != 0) goto L_0x0056
            com.google.android.gms.measurement.internal.ar r8 = r6.f2528b     // Catch:{ all -> 0x0183 }
            com.google.android.gms.measurement.internal.o r8 = r8.q()     // Catch:{ all -> 0x0183 }
            com.google.android.gms.measurement.internal.q r8 = r8.f     // Catch:{ all -> 0x0183 }
            java.lang.String r9 = "App does not exist in onConfigFetched. appId"
            java.lang.Object r7 = com.google.android.gms.measurement.internal.o.a(r7)     // Catch:{ all -> 0x0183 }
            r8.a(r9, r7)     // Catch:{ all -> 0x0183 }
            goto L_0x016f
        L_0x0056:
            r5 = 404(0x194, float:5.66E-43)
            if (r2 != 0) goto L_0x00c4
            if (r8 != r5) goto L_0x005d
            goto L_0x00c4
        L_0x005d:
            com.google.android.gms.measurement.internal.ar r10 = r6.f2528b     // Catch:{ all -> 0x0183 }
            com.google.android.gms.common.util.e r10 = r10.l()     // Catch:{ all -> 0x0183 }
            long r10 = r10.a()     // Catch:{ all -> 0x0183 }
            r1.h(r10)     // Catch:{ all -> 0x0183 }
            com.google.android.gms.measurement.internal.eo r10 = r6.d()     // Catch:{ all -> 0x0183 }
            r10.a(r1)     // Catch:{ all -> 0x0183 }
            com.google.android.gms.measurement.internal.ar r10 = r6.f2528b     // Catch:{ all -> 0x0183 }
            com.google.android.gms.measurement.internal.o r10 = r10.q()     // Catch:{ all -> 0x0183 }
            com.google.android.gms.measurement.internal.q r10 = r10.k     // Catch:{ all -> 0x0183 }
            java.lang.String r11 = "Fetching config failed. code, error"
            java.lang.Integer r1 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x0183 }
            r10.a(r11, r1, r9)     // Catch:{ all -> 0x0183 }
            com.google.android.gms.measurement.internal.al r9 = r6.n()     // Catch:{ all -> 0x0183 }
            r9.c(r7)     // Catch:{ all -> 0x0183 }
            com.google.android.gms.measurement.internal.ar r7 = r6.f2528b     // Catch:{ all -> 0x0183 }
            com.google.android.gms.measurement.internal.z r7 = r7.b()     // Catch:{ all -> 0x0183 }
            com.google.android.gms.measurement.internal.ab r7 = r7.e     // Catch:{ all -> 0x0183 }
            com.google.android.gms.measurement.internal.ar r9 = r6.f2528b     // Catch:{ all -> 0x0183 }
            com.google.android.gms.common.util.e r9 = r9.l()     // Catch:{ all -> 0x0183 }
            long r9 = r9.a()     // Catch:{ all -> 0x0183 }
            r7.a(r9)     // Catch:{ all -> 0x0183 }
            r7 = 503(0x1f7, float:7.05E-43)
            if (r8 == r7) goto L_0x00a8
            r7 = 429(0x1ad, float:6.01E-43)
            if (r8 != r7) goto L_0x00a7
            goto L_0x00a8
        L_0x00a7:
            r4 = 0
        L_0x00a8:
            if (r4 == 0) goto L_0x00bf
            com.google.android.gms.measurement.internal.ar r7 = r6.f2528b     // Catch:{ all -> 0x0183 }
            com.google.android.gms.measurement.internal.z r7 = r7.b()     // Catch:{ all -> 0x0183 }
            com.google.android.gms.measurement.internal.ab r7 = r7.f     // Catch:{ all -> 0x0183 }
            com.google.android.gms.measurement.internal.ar r8 = r6.f2528b     // Catch:{ all -> 0x0183 }
            com.google.android.gms.common.util.e r8 = r8.l()     // Catch:{ all -> 0x0183 }
            long r8 = r8.a()     // Catch:{ all -> 0x0183 }
            r7.a(r8)     // Catch:{ all -> 0x0183 }
        L_0x00bf:
            r6.j()     // Catch:{ all -> 0x0183 }
            goto L_0x016f
        L_0x00c4:
            r9 = 0
            if (r11 == 0) goto L_0x00d0
            java.lang.String r2 = "Last-Modified"
            java.lang.Object r11 = r11.get(r2)     // Catch:{ all -> 0x0183 }
            java.util.List r11 = (java.util.List) r11     // Catch:{ all -> 0x0183 }
            goto L_0x00d1
        L_0x00d0:
            r11 = r9
        L_0x00d1:
            if (r11 == 0) goto L_0x00e0
            int r2 = r11.size()     // Catch:{ all -> 0x0183 }
            if (r2 <= 0) goto L_0x00e0
            java.lang.Object r11 = r11.get(r0)     // Catch:{ all -> 0x0183 }
            java.lang.String r11 = (java.lang.String) r11     // Catch:{ all -> 0x0183 }
            goto L_0x00e1
        L_0x00e0:
            r11 = r9
        L_0x00e1:
            if (r8 == r5) goto L_0x00fd
            if (r8 != r3) goto L_0x00e6
            goto L_0x00fd
        L_0x00e6:
            com.google.android.gms.measurement.internal.al r9 = r6.n()     // Catch:{ all -> 0x0183 }
            boolean r9 = r9.a(r7, r10, r11)     // Catch:{ all -> 0x0183 }
            if (r9 != 0) goto L_0x011e
            com.google.android.gms.measurement.internal.eo r7 = r6.d()     // Catch:{ all -> 0x018c }
            r7.v()     // Catch:{ all -> 0x018c }
            r6.r = r0
            r6.u()
            return
        L_0x00fd:
            com.google.android.gms.measurement.internal.al r11 = r6.n()     // Catch:{ all -> 0x0183 }
            com.google.android.gms.internal.measurement.cp r11 = r11.a(r7)     // Catch:{ all -> 0x0183 }
            if (r11 != 0) goto L_0x011e
            com.google.android.gms.measurement.internal.al r11 = r6.n()     // Catch:{ all -> 0x0183 }
            boolean r9 = r11.a(r7, r9, r9)     // Catch:{ all -> 0x0183 }
            if (r9 != 0) goto L_0x011e
            com.google.android.gms.measurement.internal.eo r7 = r6.d()     // Catch:{ all -> 0x018c }
            r7.v()     // Catch:{ all -> 0x018c }
            r6.r = r0
            r6.u()
            return
        L_0x011e:
            com.google.android.gms.measurement.internal.ar r9 = r6.f2528b     // Catch:{ all -> 0x0183 }
            com.google.android.gms.common.util.e r9 = r9.l()     // Catch:{ all -> 0x0183 }
            long r2 = r9.a()     // Catch:{ all -> 0x0183 }
            r1.g(r2)     // Catch:{ all -> 0x0183 }
            com.google.android.gms.measurement.internal.eo r9 = r6.d()     // Catch:{ all -> 0x0183 }
            r9.a(r1)     // Catch:{ all -> 0x0183 }
            if (r8 != r5) goto L_0x0142
            com.google.android.gms.measurement.internal.ar r8 = r6.f2528b     // Catch:{ all -> 0x0183 }
            com.google.android.gms.measurement.internal.o r8 = r8.q()     // Catch:{ all -> 0x0183 }
            com.google.android.gms.measurement.internal.q r8 = r8.h     // Catch:{ all -> 0x0183 }
            java.lang.String r9 = "Config not found. Using empty config. appId"
            r8.a(r9, r7)     // Catch:{ all -> 0x0183 }
            goto L_0x0158
        L_0x0142:
            com.google.android.gms.measurement.internal.ar r7 = r6.f2528b     // Catch:{ all -> 0x0183 }
            com.google.android.gms.measurement.internal.o r7 = r7.q()     // Catch:{ all -> 0x0183 }
            com.google.android.gms.measurement.internal.q r7 = r7.k     // Catch:{ all -> 0x0183 }
            java.lang.String r9 = "Successfully fetched config. Got network response. code, size"
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x0183 }
            int r10 = r10.length     // Catch:{ all -> 0x0183 }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ all -> 0x0183 }
            r7.a(r9, r8, r10)     // Catch:{ all -> 0x0183 }
        L_0x0158:
            com.google.android.gms.measurement.internal.s r7 = r6.c()     // Catch:{ all -> 0x0183 }
            boolean r7 = r7.e()     // Catch:{ all -> 0x0183 }
            if (r7 == 0) goto L_0x016c
            boolean r7 = r6.t()     // Catch:{ all -> 0x0183 }
            if (r7 == 0) goto L_0x016c
            r6.i()     // Catch:{ all -> 0x0183 }
            goto L_0x016f
        L_0x016c:
            r6.j()     // Catch:{ all -> 0x0183 }
        L_0x016f:
            com.google.android.gms.measurement.internal.eo r7 = r6.d()     // Catch:{ all -> 0x0183 }
            r7.u()     // Catch:{ all -> 0x0183 }
            com.google.android.gms.measurement.internal.eo r7 = r6.d()     // Catch:{ all -> 0x018c }
            r7.v()     // Catch:{ all -> 0x018c }
            r6.r = r0
            r6.u()
            return
        L_0x0183:
            r7 = move-exception
            com.google.android.gms.measurement.internal.eo r8 = r6.d()     // Catch:{ all -> 0x018c }
            r8.v()     // Catch:{ all -> 0x018c }
            throw r7     // Catch:{ all -> 0x018c }
        L_0x018c:
            r7 = move-exception
            r6.r = r0
            r6.u()
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.du.a(java.lang.String, int, java.lang.Throwable, byte[], java.util.Map):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x019a  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01b6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void j() {
        /*
            r20 = this;
            r0 = r20
            r20.g()
            r20.h()
            boolean r1 = r20.w()
            if (r1 != 0) goto L_0x001b
            com.google.android.gms.measurement.internal.ar r1 = r0.f2528b
            com.google.android.gms.measurement.internal.el r1 = r1.e
            com.google.android.gms.measurement.internal.h$a<java.lang.Boolean> r2 = com.google.android.gms.measurement.internal.h.ar
            boolean r1 = r1.a(r2)
            if (r1 != 0) goto L_0x001b
            return
        L_0x001b:
            long r1 = r0.q
            r3 = 0
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 <= 0) goto L_0x005e
            com.google.android.gms.measurement.internal.ar r1 = r0.f2528b
            com.google.android.gms.common.util.e r1 = r1.l()
            long r1 = r1.b()
            r5 = 3600000(0x36ee80, double:1.7786363E-317)
            long r7 = r0.q
            long r1 = r1 - r7
            long r1 = java.lang.Math.abs(r1)
            long r5 = r5 - r1
            int r1 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r1 <= 0) goto L_0x005c
            com.google.android.gms.measurement.internal.ar r1 = r0.f2528b
            com.google.android.gms.measurement.internal.o r1 = r1.q()
            com.google.android.gms.measurement.internal.q r1 = r1.k
            java.lang.Long r2 = java.lang.Long.valueOf(r5)
            java.lang.String r3 = "Upload has been suspended. Will update scheduling later in approximately ms"
            r1.a(r3, r2)
            com.google.android.gms.measurement.internal.x r1 = r20.o()
            r1.b()
            com.google.android.gms.measurement.internal.dq r1 = r20.r()
            r1.e()
            return
        L_0x005c:
            r0.q = r3
        L_0x005e:
            com.google.android.gms.measurement.internal.ar r1 = r0.f2528b
            boolean r1 = r1.u()
            if (r1 == 0) goto L_0x025b
            boolean r1 = r20.t()
            if (r1 != 0) goto L_0x006e
            goto L_0x025b
        L_0x006e:
            com.google.android.gms.measurement.internal.ar r1 = r0.f2528b
            com.google.android.gms.common.util.e r1 = r1.l()
            long r1 = r1.a()
            com.google.android.gms.measurement.internal.h$a<java.lang.Long> r5 = com.google.android.gms.measurement.internal.h.I
            java.lang.Object r5 = r5.a()
            java.lang.Long r5 = (java.lang.Long) r5
            long r5 = r5.longValue()
            long r5 = java.lang.Math.max(r3, r5)
            com.google.android.gms.measurement.internal.eo r7 = r20.d()
            boolean r7 = r7.D()
            if (r7 != 0) goto L_0x009f
            com.google.android.gms.measurement.internal.eo r7 = r20.d()
            boolean r7 = r7.y()
            if (r7 == 0) goto L_0x009d
            goto L_0x009f
        L_0x009d:
            r7 = 0
            goto L_0x00a0
        L_0x009f:
            r7 = 1
        L_0x00a0:
            if (r7 == 0) goto L_0x00da
            com.google.android.gms.measurement.internal.ar r9 = r0.f2528b
            com.google.android.gms.measurement.internal.el r9 = r9.e
            java.lang.String r9 = r9.i()
            boolean r10 = android.text.TextUtils.isEmpty(r9)
            if (r10 != 0) goto L_0x00c9
            java.lang.String r10 = ".none."
            boolean r9 = r10.equals(r9)
            if (r9 != 0) goto L_0x00c9
            com.google.android.gms.measurement.internal.h$a<java.lang.Long> r9 = com.google.android.gms.measurement.internal.h.D
            java.lang.Object r9 = r9.a()
            java.lang.Long r9 = (java.lang.Long) r9
            long r9 = r9.longValue()
            long r9 = java.lang.Math.max(r3, r9)
            goto L_0x00ea
        L_0x00c9:
            com.google.android.gms.measurement.internal.h$a<java.lang.Long> r9 = com.google.android.gms.measurement.internal.h.C
            java.lang.Object r9 = r9.a()
            java.lang.Long r9 = (java.lang.Long) r9
            long r9 = r9.longValue()
            long r9 = java.lang.Math.max(r3, r9)
            goto L_0x00ea
        L_0x00da:
            com.google.android.gms.measurement.internal.h$a<java.lang.Long> r9 = com.google.android.gms.measurement.internal.h.B
            java.lang.Object r9 = r9.a()
            java.lang.Long r9 = (java.lang.Long) r9
            long r9 = r9.longValue()
            long r9 = java.lang.Math.max(r3, r9)
        L_0x00ea:
            com.google.android.gms.measurement.internal.ar r11 = r0.f2528b
            com.google.android.gms.measurement.internal.z r11 = r11.b()
            com.google.android.gms.measurement.internal.ab r11 = r11.d
            long r11 = r11.a()
            com.google.android.gms.measurement.internal.ar r13 = r0.f2528b
            com.google.android.gms.measurement.internal.z r13 = r13.b()
            com.google.android.gms.measurement.internal.ab r13 = r13.e
            long r13 = r13.a()
            com.google.android.gms.measurement.internal.eo r15 = r20.d()
            r16 = r9
            long r8 = r15.A()
            com.google.android.gms.measurement.internal.eo r10 = r20.d()
            r18 = r5
            long r5 = r10.B()
            long r5 = java.lang.Math.max(r8, r5)
            int r8 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r8 != 0) goto L_0x0121
        L_0x011e:
            r8 = r3
            goto L_0x0196
        L_0x0121:
            long r5 = r5 - r1
            long r5 = java.lang.Math.abs(r5)
            long r5 = r1 - r5
            long r11 = r11 - r1
            long r8 = java.lang.Math.abs(r11)
            long r8 = r1 - r8
            long r13 = r13 - r1
            long r10 = java.lang.Math.abs(r13)
            long r1 = r1 - r10
            long r8 = java.lang.Math.max(r8, r1)
            long r10 = r5 + r18
            if (r7 == 0) goto L_0x0147
            int r7 = (r8 > r3 ? 1 : (r8 == r3 ? 0 : -1))
            if (r7 <= 0) goto L_0x0147
            long r10 = java.lang.Math.min(r5, r8)
            long r10 = r10 + r16
        L_0x0147:
            com.google.android.gms.measurement.internal.ea r7 = r20.f()
            r12 = r16
            boolean r7 = r7.a(r8, r12)
            if (r7 != 0) goto L_0x0155
            long r8 = r8 + r12
            goto L_0x0156
        L_0x0155:
            r8 = r10
        L_0x0156:
            int r7 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r7 == 0) goto L_0x0196
            int r7 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r7 < 0) goto L_0x0196
            r5 = 0
        L_0x015f:
            r6 = 20
            com.google.android.gms.measurement.internal.h$a<java.lang.Integer> r7 = com.google.android.gms.measurement.internal.h.K
            java.lang.Object r7 = r7.a()
            java.lang.Integer r7 = (java.lang.Integer) r7
            int r7 = r7.intValue()
            r10 = 0
            int r7 = java.lang.Math.max(r10, r7)
            int r6 = java.lang.Math.min(r6, r7)
            if (r5 >= r6) goto L_0x011e
            r6 = 1
            long r6 = r6 << r5
            com.google.android.gms.measurement.internal.h$a<java.lang.Long> r11 = com.google.android.gms.measurement.internal.h.J
            java.lang.Object r11 = r11.a()
            java.lang.Long r11 = (java.lang.Long) r11
            long r11 = r11.longValue()
            long r11 = java.lang.Math.max(r3, r11)
            long r11 = r11 * r6
            long r8 = r8 + r11
            int r6 = (r8 > r1 ? 1 : (r8 == r1 ? 0 : -1))
            if (r6 <= 0) goto L_0x0193
            goto L_0x0196
        L_0x0193:
            int r5 = r5 + 1
            goto L_0x015f
        L_0x0196:
            int r1 = (r8 > r3 ? 1 : (r8 == r3 ? 0 : -1))
            if (r1 != 0) goto L_0x01b6
            com.google.android.gms.measurement.internal.ar r1 = r0.f2528b
            com.google.android.gms.measurement.internal.o r1 = r1.q()
            com.google.android.gms.measurement.internal.q r1 = r1.k
            java.lang.String r2 = "Next upload time is 0"
            r1.a(r2)
            com.google.android.gms.measurement.internal.x r1 = r20.o()
            r1.b()
            com.google.android.gms.measurement.internal.dq r1 = r20.r()
            r1.e()
            return
        L_0x01b6:
            com.google.android.gms.measurement.internal.s r1 = r20.c()
            boolean r1 = r1.e()
            if (r1 != 0) goto L_0x01dc
            com.google.android.gms.measurement.internal.ar r1 = r0.f2528b
            com.google.android.gms.measurement.internal.o r1 = r1.q()
            com.google.android.gms.measurement.internal.q r1 = r1.k
            java.lang.String r2 = "No network"
            r1.a(r2)
            com.google.android.gms.measurement.internal.x r1 = r20.o()
            r1.a()
            com.google.android.gms.measurement.internal.dq r1 = r20.r()
            r1.e()
            return
        L_0x01dc:
            com.google.android.gms.measurement.internal.ar r1 = r0.f2528b
            com.google.android.gms.measurement.internal.z r1 = r1.b()
            com.google.android.gms.measurement.internal.ab r1 = r1.f
            long r1 = r1.a()
            com.google.android.gms.measurement.internal.h$a<java.lang.Long> r5 = com.google.android.gms.measurement.internal.h.z
            java.lang.Object r5 = r5.a()
            java.lang.Long r5 = (java.lang.Long) r5
            long r5 = r5.longValue()
            long r5 = java.lang.Math.max(r3, r5)
            com.google.android.gms.measurement.internal.ea r7 = r20.f()
            boolean r7 = r7.a(r1, r5)
            if (r7 != 0) goto L_0x0207
            long r1 = r1 + r5
            long r8 = java.lang.Math.max(r8, r1)
        L_0x0207:
            com.google.android.gms.measurement.internal.x r1 = r20.o()
            r1.b()
            com.google.android.gms.measurement.internal.ar r1 = r0.f2528b
            com.google.android.gms.common.util.e r1 = r1.l()
            long r1 = r1.a()
            long r8 = r8 - r1
            int r1 = (r8 > r3 ? 1 : (r8 == r3 ? 0 : -1))
            if (r1 > 0) goto L_0x0242
            com.google.android.gms.measurement.internal.h$a<java.lang.Long> r1 = com.google.android.gms.measurement.internal.h.E
            java.lang.Object r1 = r1.a()
            java.lang.Long r1 = (java.lang.Long) r1
            long r1 = r1.longValue()
            long r8 = java.lang.Math.max(r3, r1)
            com.google.android.gms.measurement.internal.ar r1 = r0.f2528b
            com.google.android.gms.measurement.internal.z r1 = r1.b()
            com.google.android.gms.measurement.internal.ab r1 = r1.d
            com.google.android.gms.measurement.internal.ar r2 = r0.f2528b
            com.google.android.gms.common.util.e r2 = r2.l()
            long r2 = r2.a()
            r1.a(r2)
        L_0x0242:
            com.google.android.gms.measurement.internal.ar r1 = r0.f2528b
            com.google.android.gms.measurement.internal.o r1 = r1.q()
            com.google.android.gms.measurement.internal.q r1 = r1.k
            java.lang.Long r2 = java.lang.Long.valueOf(r8)
            java.lang.String r3 = "Upload scheduled in approximately ms"
            r1.a(r3, r2)
            com.google.android.gms.measurement.internal.dq r1 = r20.r()
            r1.a(r8)
            return
        L_0x025b:
            com.google.android.gms.measurement.internal.ar r1 = r0.f2528b
            com.google.android.gms.measurement.internal.o r1 = r1.q()
            com.google.android.gms.measurement.internal.q r1 = r1.k
            java.lang.String r2 = "Nothing to upload or uploading impossible"
            r1.a(r2)
            com.google.android.gms.measurement.internal.x r1 = r20.o()
            r1.b()
            com.google.android.gms.measurement.internal.dq r1 = r20.r()
            r1.e()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.du.j():void");
    }

    private final void u() {
        g();
        if (this.r || this.s || this.t) {
            this.f2528b.q().k.a("Not stopping services. fetch, network, upload", Boolean.valueOf(this.r), Boolean.valueOf(this.s), Boolean.valueOf(this.t));
            return;
        }
        this.f2528b.q().k.a("Stopping uploading service(s)");
        List<Runnable> list = this.c;
        if (list != null) {
            for (Runnable run : list) {
                run.run();
            }
            this.c.clear();
        }
    }

    private final Boolean b(ef efVar) {
        try {
            if (efVar.j() != -2147483648L) {
                if (efVar.j() == ((long) c.a(this.f2528b.m()).b(efVar.a(), 0).versionCode)) {
                    return true;
                }
            } else {
                String str = c.a(this.f2528b.m()).b(efVar.a(), 0).versionName;
                if (efVar.i() != null && efVar.i().equals(str)) {
                    return true;
                }
            }
            return false;
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }

    private final boolean v() {
        g();
        try {
            this.v = new RandomAccessFile(new File(this.f2528b.m().getFilesDir(), "google_app_measurement.db"), "rw").getChannel();
            this.u = this.v.tryLock();
            if (this.u != null) {
                this.f2528b.q().k.a("Storage concurrent access okay");
                return true;
            }
            this.f2528b.q().c.a("Storage concurrent data access panic");
            return false;
        } catch (FileNotFoundException e2) {
            this.f2528b.q().c.a("Failed to acquire storage lock", e2);
            return false;
        } catch (IOException e3) {
            this.f2528b.q().c.a("Failed to access storage lock file", e3);
            return false;
        }
    }

    private final int a(FileChannel fileChannel) {
        g();
        if (fileChannel == null || !fileChannel.isOpen()) {
            this.f2528b.q().c.a("Bad channel to read from");
            return 0;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        try {
            fileChannel.position(0L);
            int read = fileChannel.read(allocate);
            if (read != 4) {
                if (read != -1) {
                    this.f2528b.q().f.a("Unexpected data length. Bytes read", Integer.valueOf(read));
                }
                return 0;
            }
            allocate.flip();
            return allocate.getInt();
        } catch (IOException e2) {
            this.f2528b.q().c.a("Failed to read from channel", e2);
            return 0;
        }
    }

    private final boolean a(int i2, FileChannel fileChannel) {
        g();
        if (fileChannel == null || !fileChannel.isOpen()) {
            this.f2528b.q().c.a("Bad channel to read from");
            return false;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        allocate.putInt(i2);
        allocate.flip();
        try {
            fileChannel.truncate(0L);
            fileChannel.write(allocate);
            fileChannel.force(true);
            if (fileChannel.size() != 4) {
                this.f2528b.q().c.a("Error writing to channel. Bytes written", Long.valueOf(fileChannel.size()));
            }
            return true;
        } catch (IOException e2) {
            this.f2528b.q().c.a("Failed to write to channel", e2);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public final void k() {
        g();
        h();
        if (!this.p) {
            this.p = true;
            g();
            h();
            if ((this.f2528b.e.a(h.ar) || w()) && v()) {
                int a2 = a(this.v);
                int y2 = this.f2528b.k().y();
                g();
                if (a2 > y2) {
                    this.f2528b.q().c.a("Panic: can't downgrade version. Previous, current version", Integer.valueOf(a2), Integer.valueOf(y2));
                } else if (a2 < y2) {
                    if (a(y2, this.v)) {
                        this.f2528b.q().k.a("Storage version upgraded. Previous, current version", Integer.valueOf(a2), Integer.valueOf(y2));
                    } else {
                        this.f2528b.q().c.a("Storage version upgrade failed. Previous, current version", Integer.valueOf(a2), Integer.valueOf(y2));
                    }
                }
            }
        }
        if (!this.o && !this.f2528b.e.a(h.ar)) {
            this.f2528b.q().i.a("This instance being marked as an uploader");
            this.o = true;
            j();
        }
    }

    private final boolean w() {
        g();
        h();
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public final void a(zzk zzk) {
        if (this.w != null) {
            this.x = new ArrayList();
            this.x.addAll(this.w);
        }
        eo d2 = d();
        String str = zzk.f2601a;
        l.a(str);
        d2.c();
        d2.j();
        try {
            SQLiteDatabase w2 = d2.w();
            String[] strArr = {str};
            int delete = w2.delete("apps", "app_id=?", strArr) + 0 + w2.delete("events", "app_id=?", strArr) + w2.delete("user_attributes", "app_id=?", strArr) + w2.delete("conditional_properties", "app_id=?", strArr) + w2.delete("raw_events", "app_id=?", strArr) + w2.delete("raw_events_metadata", "app_id=?", strArr) + w2.delete("queue", "app_id=?", strArr) + w2.delete("audience_filter_values", "app_id=?", strArr) + w2.delete("main_event_params", "app_id=?", strArr);
            if (delete > 0) {
                d2.q().k.a("Reset analytics data. app, records", str, Integer.valueOf(delete));
            }
        } catch (SQLiteException e2) {
            d2.q().c.a("Error resetting analytics data. appId, error", o.a(str), e2);
        }
        zzk a2 = a(this.f2528b.m(), zzk.f2601a, zzk.f2602b, zzk.h, zzk.o, zzk.p, zzk.m, zzk.r);
        if (!this.f2528b.e.e(zzk.f2601a) || zzk.h) {
            b(a2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzk.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, int, long, ?[OBJECT, ARRAY], boolean, int, java.lang.String, int, long, int, boolean, boolean, int, java.lang.String]
     candidates:
      com.google.android.gms.measurement.internal.zzk.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, long, long, java.lang.String, boolean, boolean, long, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String):void
      com.google.android.gms.measurement.internal.zzk.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String):void */
    private final zzk a(Context context, String str, String str2, boolean z, boolean z2, boolean z3, long j2, String str3) {
        String str4;
        String str5;
        int i2;
        String str6 = str;
        PackageManager packageManager = context.getPackageManager();
        if (packageManager == null) {
            this.f2528b.q().c.a("PackageManager is null, can not log app install information");
            return null;
        }
        try {
            str4 = packageManager.getInstallerPackageName(str6);
        } catch (IllegalArgumentException unused) {
            this.f2528b.q().c.a("Error retrieving installer package name. appId", o.a(str));
            str4 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        }
        if (str4 == null) {
            str4 = "manual_install";
        } else if ("com.android.vending".equals(str4)) {
            str4 = "";
        }
        String str7 = str4;
        try {
            PackageInfo b2 = c.a(context).b(str6, 0);
            if (b2 != null) {
                CharSequence b3 = c.a(context).b(str6);
                if (!TextUtils.isEmpty(b3)) {
                    String charSequence = b3.toString();
                }
                String str8 = b2.versionName;
                i2 = b2.versionCode;
                str5 = str8;
            } else {
                i2 = Integer.MIN_VALUE;
                str5 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
            }
            return new zzk(str, str2, str5, (long) i2, str7, 14710L, this.f2528b.e().a(context, str6), (String) null, z, false, "", 0L, this.f2528b.e.c(str6, h.ad) ? j2 : 0, 0, z2, z3, false, str3);
        } catch (PackageManager.NameNotFoundException unused2) {
            this.f2528b.q().c.a("Error retrieving newly installed package info. appId, appName", o.a(str), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.ed.a(java.lang.String, int, boolean):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.google.android.gms.measurement.internal.ed.a(int, java.lang.Object, boolean):java.lang.Object
      com.google.android.gms.measurement.internal.ed.a(android.os.Bundle, java.lang.String, java.lang.Object):void
      com.google.android.gms.measurement.internal.ed.a(java.lang.String, int, java.lang.String):boolean
      com.google.android.gms.measurement.internal.ed.a(java.lang.String, java.lang.String[], java.lang.String):boolean
      com.google.android.gms.measurement.internal.ed.a(java.lang.String, int, boolean):java.lang.String */
    /* access modifiers changed from: package-private */
    public final void a(zzfv zzfv, zzk zzk) {
        g();
        h();
        if (TextUtils.isEmpty(zzk.f2602b) && TextUtils.isEmpty(zzk.r)) {
            return;
        }
        if (!zzk.h) {
            c(zzk);
            return;
        }
        int c2 = this.f2528b.e().c(zzfv.f2599a);
        if (c2 != 0) {
            this.f2528b.e();
            this.f2528b.e().a(zzk.f2601a, c2, "_ev", ed.a(zzfv.f2599a, 24, true), zzfv.f2599a != null ? zzfv.f2599a.length() : 0);
            return;
        }
        int b2 = this.f2528b.e().b(zzfv.f2599a, zzfv.a());
        if (b2 != 0) {
            this.f2528b.e();
            String a2 = ed.a(zzfv.f2599a, 24, true);
            Object a3 = zzfv.a();
            this.f2528b.e().a(zzk.f2601a, b2, "_ev", a2, (a3 == null || (!(a3 instanceof String) && !(a3 instanceof CharSequence))) ? 0 : String.valueOf(a3).length());
            return;
        }
        this.f2528b.e();
        Object c3 = ed.c(zzfv.f2599a, zzfv.a());
        if (c3 != null) {
            if (this.f2528b.e.i(zzk.f2601a) && "_sno".equals(zzfv.f2599a)) {
                long j2 = 0;
                ec c4 = d().c(zzk.f2601a, "_sno");
                if (c4 == null || !(c4.e instanceof Long)) {
                    d a4 = d().a(zzk.f2601a, "_s");
                    if (a4 != null) {
                        j2 = a4.c;
                        this.f2528b.q().k.a("Backfill the session number. Last used session number", Long.valueOf(j2));
                    }
                } else {
                    j2 = ((Long) c4.e).longValue();
                }
                c3 = Long.valueOf(j2 + 1);
            }
            ec ecVar = new ec(zzk.f2601a, zzfv.e, zzfv.f2599a, zzfv.f2600b, c3);
            this.f2528b.q().j.a("Setting user property", this.f2528b.f().c(ecVar.c), c3);
            d().e();
            try {
                c(zzk);
                boolean a5 = d().a(ecVar);
                d().u();
                if (a5) {
                    this.f2528b.q().j.a("User property set", this.f2528b.f().c(ecVar.c), ecVar.e);
                } else {
                    this.f2528b.q().c.a("Too many unique user properties are set. Ignoring user property", this.f2528b.f().c(ecVar.c), ecVar.e);
                    this.f2528b.e().a(zzk.f2601a, 9, (String) null, (String) null, 0);
                }
            } finally {
                d().v();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(zzfv zzfv, zzk zzk) {
        g();
        h();
        if (TextUtils.isEmpty(zzk.f2602b) && TextUtils.isEmpty(zzk.r)) {
            return;
        }
        if (!zzk.h) {
            c(zzk);
            return;
        }
        this.f2528b.q().j.a("Removing user property", this.f2528b.f().c(zzfv.f2599a));
        d().e();
        try {
            c(zzk);
            d().b(zzk.f2601a, zzfv.f2599a);
            d().u();
            this.f2528b.q().j.a("User property removed", this.f2528b.f().c(zzfv.f2599a));
        } finally {
            d().v();
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x031c A[Catch:{ SQLiteException -> 0x0141, all -> 0x042b }] */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x0349 A[Catch:{ SQLiteException -> 0x0141, all -> 0x042b }] */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x03cf A[Catch:{ SQLiteException -> 0x0141, all -> 0x042b }] */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x03fd A[Catch:{ SQLiteException -> 0x0141, all -> 0x042b }] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01ca A[Catch:{ SQLiteException -> 0x0141, all -> 0x042b }] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x01d7 A[Catch:{ SQLiteException -> 0x0141, all -> 0x042b }] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x01e9 A[Catch:{ SQLiteException -> 0x0141, all -> 0x042b }] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x02c0 A[Catch:{ SQLiteException -> 0x0141, all -> 0x042b }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b(com.google.android.gms.measurement.internal.zzk r22) {
        /*
            r21 = this;
            r1 = r21
            r2 = r22
            java.lang.String r3 = "_sysu"
            java.lang.String r4 = "_sys"
            java.lang.String r5 = "_pfo"
            java.lang.String r6 = "_uwa"
            java.lang.String r0 = "app_id=?"
            r21.g()
            r21.h()
            com.google.android.gms.common.internal.l.a(r22)
            java.lang.String r7 = r2.f2601a
            com.google.android.gms.common.internal.l.a(r7)
            java.lang.String r7 = r2.f2602b
            boolean r7 = android.text.TextUtils.isEmpty(r7)
            if (r7 == 0) goto L_0x002d
            java.lang.String r7 = r2.r
            boolean r7 = android.text.TextUtils.isEmpty(r7)
            if (r7 == 0) goto L_0x002d
            return
        L_0x002d:
            com.google.android.gms.measurement.internal.eo r7 = r21.d()
            java.lang.String r8 = r2.f2601a
            com.google.android.gms.measurement.internal.ef r7 = r7.b(r8)
            r8 = 0
            if (r7 == 0) goto L_0x0060
            java.lang.String r10 = r7.c()
            boolean r10 = android.text.TextUtils.isEmpty(r10)
            if (r10 == 0) goto L_0x0060
            java.lang.String r10 = r2.f2602b
            boolean r10 = android.text.TextUtils.isEmpty(r10)
            if (r10 != 0) goto L_0x0060
            r7.g(r8)
            com.google.android.gms.measurement.internal.eo r10 = r21.d()
            r10.a(r7)
            com.google.android.gms.measurement.internal.al r7 = r21.n()
            java.lang.String r10 = r2.f2601a
            r7.d(r10)
        L_0x0060:
            boolean r7 = r2.h
            if (r7 != 0) goto L_0x0068
            r21.c(r22)
            return
        L_0x0068:
            long r10 = r2.m
            int r7 = (r10 > r8 ? 1 : (r10 == r8 ? 0 : -1))
            if (r7 != 0) goto L_0x0078
            com.google.android.gms.measurement.internal.ar r7 = r1.f2528b
            com.google.android.gms.common.util.e r7 = r7.l()
            long r10 = r7.a()
        L_0x0078:
            int r7 = r2.n
            r14 = 1
            if (r7 == 0) goto L_0x0097
            if (r7 == r14) goto L_0x0097
            com.google.android.gms.measurement.internal.ar r12 = r1.f2528b
            com.google.android.gms.measurement.internal.o r12 = r12.q()
            com.google.android.gms.measurement.internal.q r12 = r12.f
            java.lang.String r13 = r2.f2601a
            java.lang.Object r13 = com.google.android.gms.measurement.internal.o.a(r13)
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)
            java.lang.String r8 = "Incorrect app type, assuming installed app. appId, appType"
            r12.a(r8, r13, r7)
            r7 = 0
        L_0x0097:
            com.google.android.gms.measurement.internal.eo r8 = r21.d()
            r8.e()
            com.google.android.gms.measurement.internal.eo r8 = r21.d()     // Catch:{ all -> 0x042b }
            java.lang.String r9 = r2.f2601a     // Catch:{ all -> 0x042b }
            com.google.android.gms.measurement.internal.ef r8 = r8.b(r9)     // Catch:{ all -> 0x042b }
            if (r8 == 0) goto L_0x0152
            com.google.android.gms.measurement.internal.ar r12 = r1.f2528b     // Catch:{ all -> 0x042b }
            r12.e()     // Catch:{ all -> 0x042b }
            java.lang.String r12 = r2.f2602b     // Catch:{ all -> 0x042b }
            java.lang.String r13 = r8.c()     // Catch:{ all -> 0x042b }
            java.lang.String r9 = r2.r     // Catch:{ all -> 0x042b }
            java.lang.String r15 = r8.d()     // Catch:{ all -> 0x042b }
            boolean r9 = com.google.android.gms.measurement.internal.ed.a(r12, r13, r9, r15)     // Catch:{ all -> 0x042b }
            if (r9 == 0) goto L_0x0152
            com.google.android.gms.measurement.internal.ar r9 = r1.f2528b     // Catch:{ all -> 0x042b }
            com.google.android.gms.measurement.internal.o r9 = r9.q()     // Catch:{ all -> 0x042b }
            com.google.android.gms.measurement.internal.q r9 = r9.f     // Catch:{ all -> 0x042b }
            java.lang.String r12 = "New GMP App Id passed in. Removing cached database data. appId"
            java.lang.String r13 = r8.a()     // Catch:{ all -> 0x042b }
            java.lang.Object r13 = com.google.android.gms.measurement.internal.o.a(r13)     // Catch:{ all -> 0x042b }
            r9.a(r12, r13)     // Catch:{ all -> 0x042b }
            com.google.android.gms.measurement.internal.eo r9 = r21.d()     // Catch:{ all -> 0x042b }
            java.lang.String r8 = r8.a()     // Catch:{ all -> 0x042b }
            r9.j()     // Catch:{ all -> 0x042b }
            r9.c()     // Catch:{ all -> 0x042b }
            com.google.android.gms.common.internal.l.a(r8)     // Catch:{ all -> 0x042b }
            android.database.sqlite.SQLiteDatabase r12 = r9.w()     // Catch:{ SQLiteException -> 0x0141 }
            java.lang.String[] r13 = new java.lang.String[r14]     // Catch:{ SQLiteException -> 0x0141 }
            r15 = 0
            r13[r15] = r8     // Catch:{ SQLiteException -> 0x0141 }
            java.lang.String r14 = "events"
            int r14 = r12.delete(r14, r0, r13)     // Catch:{ SQLiteException -> 0x0141 }
            int r14 = r14 + r15
            java.lang.String r15 = "user_attributes"
            int r15 = r12.delete(r15, r0, r13)     // Catch:{ SQLiteException -> 0x0141 }
            int r14 = r14 + r15
            java.lang.String r15 = "conditional_properties"
            int r15 = r12.delete(r15, r0, r13)     // Catch:{ SQLiteException -> 0x0141 }
            int r14 = r14 + r15
            java.lang.String r15 = "apps"
            int r15 = r12.delete(r15, r0, r13)     // Catch:{ SQLiteException -> 0x0141 }
            int r14 = r14 + r15
            java.lang.String r15 = "raw_events"
            int r15 = r12.delete(r15, r0, r13)     // Catch:{ SQLiteException -> 0x0141 }
            int r14 = r14 + r15
            java.lang.String r15 = "raw_events_metadata"
            int r15 = r12.delete(r15, r0, r13)     // Catch:{ SQLiteException -> 0x0141 }
            int r14 = r14 + r15
            java.lang.String r15 = "event_filters"
            int r15 = r12.delete(r15, r0, r13)     // Catch:{ SQLiteException -> 0x0141 }
            int r14 = r14 + r15
            java.lang.String r15 = "property_filters"
            int r15 = r12.delete(r15, r0, r13)     // Catch:{ SQLiteException -> 0x0141 }
            int r14 = r14 + r15
            java.lang.String r15 = "audience_filter_values"
            int r0 = r12.delete(r15, r0, r13)     // Catch:{ SQLiteException -> 0x0141 }
            int r14 = r14 + r0
            if (r14 <= 0) goto L_0x0151
            com.google.android.gms.measurement.internal.o r0 = r9.q()     // Catch:{ SQLiteException -> 0x0141 }
            com.google.android.gms.measurement.internal.q r0 = r0.k     // Catch:{ SQLiteException -> 0x0141 }
            java.lang.String r12 = "Deleted application data. app, records"
            java.lang.Integer r13 = java.lang.Integer.valueOf(r14)     // Catch:{ SQLiteException -> 0x0141 }
            r0.a(r12, r8, r13)     // Catch:{ SQLiteException -> 0x0141 }
            goto L_0x0151
        L_0x0141:
            r0 = move-exception
            com.google.android.gms.measurement.internal.o r9 = r9.q()     // Catch:{ all -> 0x042b }
            com.google.android.gms.measurement.internal.q r9 = r9.c     // Catch:{ all -> 0x042b }
            java.lang.String r12 = "Error deleting application data. appId, error"
            java.lang.Object r8 = com.google.android.gms.measurement.internal.o.a(r8)     // Catch:{ all -> 0x042b }
            r9.a(r12, r8, r0)     // Catch:{ all -> 0x042b }
        L_0x0151:
            r8 = 0
        L_0x0152:
            if (r8 == 0) goto L_0x01c4
            long r12 = r8.j()     // Catch:{ all -> 0x042b }
            r14 = -2147483648(0xffffffff80000000, double:NaN)
            java.lang.String r0 = "_pv"
            int r9 = (r12 > r14 ? 1 : (r12 == r14 ? 0 : -1))
            if (r9 == 0) goto L_0x018e
            long r12 = r8.j()     // Catch:{ all -> 0x042b }
            long r14 = r2.j     // Catch:{ all -> 0x042b }
            int r9 = (r12 > r14 ? 1 : (r12 == r14 ? 0 : -1))
            if (r9 == 0) goto L_0x01c4
            android.os.Bundle r9 = new android.os.Bundle     // Catch:{ all -> 0x042b }
            r9.<init>()     // Catch:{ all -> 0x042b }
            java.lang.String r8 = r8.i()     // Catch:{ all -> 0x042b }
            r9.putString(r0, r8)     // Catch:{ all -> 0x042b }
            com.google.android.gms.measurement.internal.zzag r0 = new com.google.android.gms.measurement.internal.zzag     // Catch:{ all -> 0x042b }
            java.lang.String r13 = "_au"
            com.google.android.gms.measurement.internal.zzad r14 = new com.google.android.gms.measurement.internal.zzad     // Catch:{ all -> 0x042b }
            r14.<init>(r9)     // Catch:{ all -> 0x042b }
            java.lang.String r15 = "auto"
            r12 = r0
            r9 = 1
            r8 = 0
            r16 = r10
            r12.<init>(r13, r14, r15, r16)     // Catch:{ all -> 0x042b }
            r1.a(r0, r2)     // Catch:{ all -> 0x042b }
            goto L_0x01c5
        L_0x018e:
            r9 = 1
            r15 = 0
            java.lang.String r12 = r8.i()     // Catch:{ all -> 0x042b }
            if (r12 == 0) goto L_0x01c5
            java.lang.String r12 = r8.i()     // Catch:{ all -> 0x042b }
            java.lang.String r13 = r2.c     // Catch:{ all -> 0x042b }
            boolean r12 = r12.equals(r13)     // Catch:{ all -> 0x042b }
            if (r12 != 0) goto L_0x01c5
            android.os.Bundle r12 = new android.os.Bundle     // Catch:{ all -> 0x042b }
            r12.<init>()     // Catch:{ all -> 0x042b }
            java.lang.String r8 = r8.i()     // Catch:{ all -> 0x042b }
            r12.putString(r0, r8)     // Catch:{ all -> 0x042b }
            com.google.android.gms.measurement.internal.zzag r0 = new com.google.android.gms.measurement.internal.zzag     // Catch:{ all -> 0x042b }
            java.lang.String r13 = "_au"
            com.google.android.gms.measurement.internal.zzad r14 = new com.google.android.gms.measurement.internal.zzad     // Catch:{ all -> 0x042b }
            r14.<init>(r12)     // Catch:{ all -> 0x042b }
            java.lang.String r8 = "auto"
            r12 = r0
            r15 = r8
            r16 = r10
            r12.<init>(r13, r14, r15, r16)     // Catch:{ all -> 0x042b }
            r1.a(r0, r2)     // Catch:{ all -> 0x042b }
            goto L_0x01c5
        L_0x01c4:
            r9 = 1
        L_0x01c5:
            r21.c(r22)     // Catch:{ all -> 0x042b }
            if (r7 != 0) goto L_0x01d7
            com.google.android.gms.measurement.internal.eo r0 = r21.d()     // Catch:{ all -> 0x042b }
            java.lang.String r8 = r2.f2601a     // Catch:{ all -> 0x042b }
            java.lang.String r12 = "_f"
            com.google.android.gms.measurement.internal.d r0 = r0.a(r8, r12)     // Catch:{ all -> 0x042b }
            goto L_0x01e7
        L_0x01d7:
            if (r7 != r9) goto L_0x01e6
            com.google.android.gms.measurement.internal.eo r0 = r21.d()     // Catch:{ all -> 0x042b }
            java.lang.String r8 = r2.f2601a     // Catch:{ all -> 0x042b }
            java.lang.String r12 = "_v"
            com.google.android.gms.measurement.internal.d r0 = r0.a(r8, r12)     // Catch:{ all -> 0x042b }
            goto L_0x01e7
        L_0x01e6:
            r0 = 0
        L_0x01e7:
            if (r0 != 0) goto L_0x03fd
            r12 = 3600000(0x36ee80, double:1.7786363E-317)
            long r14 = r10 / r12
            r18 = r10
            r9 = 1
            long r14 = r14 + r9
            long r14 = r14 * r12
            java.lang.String r0 = "_r"
            java.lang.String r11 = "_c"
            java.lang.String r13 = "_et"
            if (r7 != 0) goto L_0x0361
            com.google.android.gms.measurement.internal.zzfv r7 = new com.google.android.gms.measurement.internal.zzfv     // Catch:{ all -> 0x042b }
            java.lang.String r16 = "_fot"
            java.lang.Long r17 = java.lang.Long.valueOf(r14)     // Catch:{ all -> 0x042b }
            java.lang.String r20 = "auto"
            r12 = r7
            r14 = r13
            r13 = r16
            r8 = r14
            r14 = r18
            r16 = r17
            r17 = r20
            r12.<init>(r13, r14, r16, r17)     // Catch:{ all -> 0x042b }
            r1.a(r7, r2)     // Catch:{ all -> 0x042b }
            com.google.android.gms.measurement.internal.ar r7 = r1.f2528b     // Catch:{ all -> 0x042b }
            com.google.android.gms.measurement.internal.el r7 = r7.e     // Catch:{ all -> 0x042b }
            java.lang.String r12 = r2.f2602b     // Catch:{ all -> 0x042b }
            boolean r7 = r7.g(r12)     // Catch:{ all -> 0x042b }
            if (r7 == 0) goto L_0x0230
            r21.g()     // Catch:{ all -> 0x042b }
            com.google.android.gms.measurement.internal.ar r7 = r1.f2528b     // Catch:{ all -> 0x042b }
            com.google.android.gms.measurement.internal.ae r7 = r7.i     // Catch:{ all -> 0x042b }
            java.lang.String r12 = r2.f2601a     // Catch:{ all -> 0x042b }
            r7.a(r12)     // Catch:{ all -> 0x042b }
        L_0x0230:
            r21.g()     // Catch:{ all -> 0x042b }
            r21.h()     // Catch:{ all -> 0x042b }
            android.os.Bundle r7 = new android.os.Bundle     // Catch:{ all -> 0x042b }
            r7.<init>()     // Catch:{ all -> 0x042b }
            r7.putLong(r11, r9)     // Catch:{ all -> 0x042b }
            r7.putLong(r0, r9)     // Catch:{ all -> 0x042b }
            r11 = 0
            r7.putLong(r6, r11)     // Catch:{ all -> 0x042b }
            r7.putLong(r5, r11)     // Catch:{ all -> 0x042b }
            r7.putLong(r4, r11)     // Catch:{ all -> 0x042b }
            r7.putLong(r3, r11)     // Catch:{ all -> 0x042b }
            com.google.android.gms.measurement.internal.ar r0 = r1.f2528b     // Catch:{ all -> 0x042b }
            com.google.android.gms.measurement.internal.el r0 = r0.e     // Catch:{ all -> 0x042b }
            java.lang.String r11 = r2.f2601a     // Catch:{ all -> 0x042b }
            boolean r0 = r0.l(r11)     // Catch:{ all -> 0x042b }
            if (r0 == 0) goto L_0x025e
            r7.putLong(r8, r9)     // Catch:{ all -> 0x042b }
        L_0x025e:
            com.google.android.gms.measurement.internal.ar r0 = r1.f2528b     // Catch:{ all -> 0x042b }
            com.google.android.gms.measurement.internal.el r0 = r0.e     // Catch:{ all -> 0x042b }
            java.lang.String r11 = r2.f2601a     // Catch:{ all -> 0x042b }
            boolean r0 = r0.e(r11)     // Catch:{ all -> 0x042b }
            if (r0 == 0) goto L_0x0273
            boolean r0 = r2.q     // Catch:{ all -> 0x042b }
            if (r0 == 0) goto L_0x0273
            java.lang.String r0 = "_dac"
            r7.putLong(r0, r9)     // Catch:{ all -> 0x042b }
        L_0x0273:
            com.google.android.gms.measurement.internal.ar r0 = r1.f2528b     // Catch:{ all -> 0x042b }
            android.content.Context r0 = r0.m()     // Catch:{ all -> 0x042b }
            android.content.pm.PackageManager r0 = r0.getPackageManager()     // Catch:{ all -> 0x042b }
            if (r0 != 0) goto L_0x0294
            com.google.android.gms.measurement.internal.ar r0 = r1.f2528b     // Catch:{ all -> 0x042b }
            com.google.android.gms.measurement.internal.o r0 = r0.q()     // Catch:{ all -> 0x042b }
            com.google.android.gms.measurement.internal.q r0 = r0.c     // Catch:{ all -> 0x042b }
            java.lang.String r3 = "PackageManager is null, first open report might be inaccurate. appId"
            java.lang.String r4 = r2.f2601a     // Catch:{ all -> 0x042b }
            java.lang.Object r4 = com.google.android.gms.measurement.internal.o.a(r4)     // Catch:{ all -> 0x042b }
            r0.a(r3, r4)     // Catch:{ all -> 0x042b }
            goto L_0x032e
        L_0x0294:
            com.google.android.gms.measurement.internal.ar r0 = r1.f2528b     // Catch:{ NameNotFoundException -> 0x02a8 }
            android.content.Context r0 = r0.m()     // Catch:{ NameNotFoundException -> 0x02a8 }
            com.google.android.gms.common.c.b r0 = com.google.android.gms.common.c.c.a(r0)     // Catch:{ NameNotFoundException -> 0x02a8 }
            java.lang.String r11 = r2.f2601a     // Catch:{ NameNotFoundException -> 0x02a8 }
            r14 = 0
            android.content.pm.PackageInfo r0 = r0.b(r11, r14)     // Catch:{ NameNotFoundException -> 0x02a6 }
            goto L_0x02be
        L_0x02a6:
            r0 = move-exception
            goto L_0x02aa
        L_0x02a8:
            r0 = move-exception
            r14 = 0
        L_0x02aa:
            com.google.android.gms.measurement.internal.ar r11 = r1.f2528b     // Catch:{ all -> 0x042b }
            com.google.android.gms.measurement.internal.o r11 = r11.q()     // Catch:{ all -> 0x042b }
            com.google.android.gms.measurement.internal.q r11 = r11.c     // Catch:{ all -> 0x042b }
            java.lang.String r12 = "Package info is null, first open report might be inaccurate. appId"
            java.lang.String r13 = r2.f2601a     // Catch:{ all -> 0x042b }
            java.lang.Object r13 = com.google.android.gms.measurement.internal.o.a(r13)     // Catch:{ all -> 0x042b }
            r11.a(r12, r13, r0)     // Catch:{ all -> 0x042b }
            r0 = 0
        L_0x02be:
            if (r0 == 0) goto L_0x02f3
            long r11 = r0.firstInstallTime     // Catch:{ all -> 0x042b }
            r15 = 0
            int r13 = (r11 > r15 ? 1 : (r11 == r15 ? 0 : -1))
            if (r13 == 0) goto L_0x02f3
            long r11 = r0.firstInstallTime     // Catch:{ all -> 0x042b }
            long r14 = r0.lastUpdateTime     // Catch:{ all -> 0x042b }
            int r0 = (r11 > r14 ? 1 : (r11 == r14 ? 0 : -1))
            if (r0 == 0) goto L_0x02d5
            r7.putLong(r6, r9)     // Catch:{ all -> 0x042b }
            r0 = 0
            goto L_0x02d6
        L_0x02d5:
            r0 = 1
        L_0x02d6:
            com.google.android.gms.measurement.internal.zzfv r6 = new com.google.android.gms.measurement.internal.zzfv     // Catch:{ all -> 0x042b }
            java.lang.String r13 = "_fi"
            if (r0 == 0) goto L_0x02de
            r11 = r9
            goto L_0x02e0
        L_0x02de:
            r11 = 0
        L_0x02e0:
            java.lang.Long r0 = java.lang.Long.valueOf(r11)     // Catch:{ all -> 0x042b }
            java.lang.String r17 = "auto"
            r12 = r6
            r11 = 0
            r14 = r18
            r16 = r0
            r12.<init>(r13, r14, r16, r17)     // Catch:{ all -> 0x042b }
            r1.a(r6, r2)     // Catch:{ all -> 0x042b }
            goto L_0x02f4
        L_0x02f3:
            r11 = 0
        L_0x02f4:
            com.google.android.gms.measurement.internal.ar r0 = r1.f2528b     // Catch:{ NameNotFoundException -> 0x0305 }
            android.content.Context r0 = r0.m()     // Catch:{ NameNotFoundException -> 0x0305 }
            com.google.android.gms.common.c.b r0 = com.google.android.gms.common.c.c.a(r0)     // Catch:{ NameNotFoundException -> 0x0305 }
            java.lang.String r6 = r2.f2601a     // Catch:{ NameNotFoundException -> 0x0305 }
            android.content.pm.ApplicationInfo r0 = r0.a(r6, r11)     // Catch:{ NameNotFoundException -> 0x0305 }
            goto L_0x031a
        L_0x0305:
            r0 = move-exception
            com.google.android.gms.measurement.internal.ar r6 = r1.f2528b     // Catch:{ all -> 0x042b }
            com.google.android.gms.measurement.internal.o r6 = r6.q()     // Catch:{ all -> 0x042b }
            com.google.android.gms.measurement.internal.q r6 = r6.c     // Catch:{ all -> 0x042b }
            java.lang.String r11 = "Application info is null, first open report might be inaccurate. appId"
            java.lang.String r12 = r2.f2601a     // Catch:{ all -> 0x042b }
            java.lang.Object r12 = com.google.android.gms.measurement.internal.o.a(r12)     // Catch:{ all -> 0x042b }
            r6.a(r11, r12, r0)     // Catch:{ all -> 0x042b }
            r0 = 0
        L_0x031a:
            if (r0 == 0) goto L_0x032e
            int r6 = r0.flags     // Catch:{ all -> 0x042b }
            r11 = 1
            r6 = r6 & r11
            if (r6 == 0) goto L_0x0325
            r7.putLong(r4, r9)     // Catch:{ all -> 0x042b }
        L_0x0325:
            int r0 = r0.flags     // Catch:{ all -> 0x042b }
            r0 = r0 & 128(0x80, float:1.794E-43)
            if (r0 == 0) goto L_0x032e
            r7.putLong(r3, r9)     // Catch:{ all -> 0x042b }
        L_0x032e:
            com.google.android.gms.measurement.internal.eo r0 = r21.d()     // Catch:{ all -> 0x042b }
            java.lang.String r3 = r2.f2601a     // Catch:{ all -> 0x042b }
            com.google.android.gms.common.internal.l.a(r3)     // Catch:{ all -> 0x042b }
            r0.c()     // Catch:{ all -> 0x042b }
            r0.j()     // Catch:{ all -> 0x042b }
            java.lang.String r4 = "first_open_count"
            long r3 = r0.h(r3, r4)     // Catch:{ all -> 0x042b }
            r11 = 0
            int r0 = (r3 > r11 ? 1 : (r3 == r11 ? 0 : -1))
            if (r0 < 0) goto L_0x034c
            r7.putLong(r5, r3)     // Catch:{ all -> 0x042b }
        L_0x034c:
            com.google.android.gms.measurement.internal.zzag r0 = new com.google.android.gms.measurement.internal.zzag     // Catch:{ all -> 0x042b }
            java.lang.String r13 = "_f"
            com.google.android.gms.measurement.internal.zzad r14 = new com.google.android.gms.measurement.internal.zzad     // Catch:{ all -> 0x042b }
            r14.<init>(r7)     // Catch:{ all -> 0x042b }
            java.lang.String r15 = "auto"
            r12 = r0
            r16 = r18
            r12.<init>(r13, r14, r15, r16)     // Catch:{ all -> 0x042b }
            r1.a(r0, r2)     // Catch:{ all -> 0x042b }
            goto L_0x03c1
        L_0x0361:
            r8 = r13
            r3 = 1
            if (r7 != r3) goto L_0x03c1
            com.google.android.gms.measurement.internal.zzfv r3 = new com.google.android.gms.measurement.internal.zzfv     // Catch:{ all -> 0x042b }
            java.lang.String r13 = "_fvt"
            java.lang.Long r16 = java.lang.Long.valueOf(r14)     // Catch:{ all -> 0x042b }
            java.lang.String r17 = "auto"
            r12 = r3
            r14 = r18
            r12.<init>(r13, r14, r16, r17)     // Catch:{ all -> 0x042b }
            r1.a(r3, r2)     // Catch:{ all -> 0x042b }
            r21.g()     // Catch:{ all -> 0x042b }
            r21.h()     // Catch:{ all -> 0x042b }
            android.os.Bundle r3 = new android.os.Bundle     // Catch:{ all -> 0x042b }
            r3.<init>()     // Catch:{ all -> 0x042b }
            r3.putLong(r11, r9)     // Catch:{ all -> 0x042b }
            r3.putLong(r0, r9)     // Catch:{ all -> 0x042b }
            com.google.android.gms.measurement.internal.ar r0 = r1.f2528b     // Catch:{ all -> 0x042b }
            com.google.android.gms.measurement.internal.el r0 = r0.e     // Catch:{ all -> 0x042b }
            java.lang.String r4 = r2.f2601a     // Catch:{ all -> 0x042b }
            boolean r0 = r0.l(r4)     // Catch:{ all -> 0x042b }
            if (r0 == 0) goto L_0x0398
            r3.putLong(r8, r9)     // Catch:{ all -> 0x042b }
        L_0x0398:
            com.google.android.gms.measurement.internal.ar r0 = r1.f2528b     // Catch:{ all -> 0x042b }
            com.google.android.gms.measurement.internal.el r0 = r0.e     // Catch:{ all -> 0x042b }
            java.lang.String r4 = r2.f2601a     // Catch:{ all -> 0x042b }
            boolean r0 = r0.e(r4)     // Catch:{ all -> 0x042b }
            if (r0 == 0) goto L_0x03ad
            boolean r0 = r2.q     // Catch:{ all -> 0x042b }
            if (r0 == 0) goto L_0x03ad
            java.lang.String r0 = "_dac"
            r3.putLong(r0, r9)     // Catch:{ all -> 0x042b }
        L_0x03ad:
            com.google.android.gms.measurement.internal.zzag r0 = new com.google.android.gms.measurement.internal.zzag     // Catch:{ all -> 0x042b }
            java.lang.String r13 = "_v"
            com.google.android.gms.measurement.internal.zzad r14 = new com.google.android.gms.measurement.internal.zzad     // Catch:{ all -> 0x042b }
            r14.<init>(r3)     // Catch:{ all -> 0x042b }
            java.lang.String r15 = "auto"
            r12 = r0
            r16 = r18
            r12.<init>(r13, r14, r15, r16)     // Catch:{ all -> 0x042b }
            r1.a(r0, r2)     // Catch:{ all -> 0x042b }
        L_0x03c1:
            com.google.android.gms.measurement.internal.ar r0 = r1.f2528b     // Catch:{ all -> 0x042b }
            com.google.android.gms.measurement.internal.el r0 = r0.e     // Catch:{ all -> 0x042b }
            java.lang.String r3 = r2.f2601a     // Catch:{ all -> 0x042b }
            com.google.android.gms.measurement.internal.h$a<java.lang.Boolean> r4 = com.google.android.gms.measurement.internal.h.ao     // Catch:{ all -> 0x042b }
            boolean r0 = r0.c(r3, r4)     // Catch:{ all -> 0x042b }
            if (r0 != 0) goto L_0x041c
            android.os.Bundle r0 = new android.os.Bundle     // Catch:{ all -> 0x042b }
            r0.<init>()     // Catch:{ all -> 0x042b }
            r0.putLong(r8, r9)     // Catch:{ all -> 0x042b }
            com.google.android.gms.measurement.internal.ar r3 = r1.f2528b     // Catch:{ all -> 0x042b }
            com.google.android.gms.measurement.internal.el r3 = r3.e     // Catch:{ all -> 0x042b }
            java.lang.String r4 = r2.f2601a     // Catch:{ all -> 0x042b }
            boolean r3 = r3.l(r4)     // Catch:{ all -> 0x042b }
            if (r3 == 0) goto L_0x03e8
            java.lang.String r3 = "_fr"
            r0.putLong(r3, r9)     // Catch:{ all -> 0x042b }
        L_0x03e8:
            com.google.android.gms.measurement.internal.zzag r3 = new com.google.android.gms.measurement.internal.zzag     // Catch:{ all -> 0x042b }
            java.lang.String r13 = "_e"
            com.google.android.gms.measurement.internal.zzad r14 = new com.google.android.gms.measurement.internal.zzad     // Catch:{ all -> 0x042b }
            r14.<init>(r0)     // Catch:{ all -> 0x042b }
            java.lang.String r15 = "auto"
            r12 = r3
            r16 = r18
            r12.<init>(r13, r14, r15, r16)     // Catch:{ all -> 0x042b }
            r1.a(r3, r2)     // Catch:{ all -> 0x042b }
            goto L_0x041c
        L_0x03fd:
            r18 = r10
            boolean r0 = r2.i     // Catch:{ all -> 0x042b }
            if (r0 == 0) goto L_0x041c
            android.os.Bundle r0 = new android.os.Bundle     // Catch:{ all -> 0x042b }
            r0.<init>()     // Catch:{ all -> 0x042b }
            com.google.android.gms.measurement.internal.zzag r3 = new com.google.android.gms.measurement.internal.zzag     // Catch:{ all -> 0x042b }
            java.lang.String r13 = "_cd"
            com.google.android.gms.measurement.internal.zzad r14 = new com.google.android.gms.measurement.internal.zzad     // Catch:{ all -> 0x042b }
            r14.<init>(r0)     // Catch:{ all -> 0x042b }
            java.lang.String r15 = "auto"
            r12 = r3
            r16 = r18
            r12.<init>(r13, r14, r15, r16)     // Catch:{ all -> 0x042b }
            r1.a(r3, r2)     // Catch:{ all -> 0x042b }
        L_0x041c:
            com.google.android.gms.measurement.internal.eo r0 = r21.d()     // Catch:{ all -> 0x042b }
            r0.u()     // Catch:{ all -> 0x042b }
            com.google.android.gms.measurement.internal.eo r0 = r21.d()
            r0.v()
            return
        L_0x042b:
            r0 = move-exception
            com.google.android.gms.measurement.internal.eo r2 = r21.d()
            r2.v()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.du.b(com.google.android.gms.measurement.internal.zzk):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzk.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, ?[OBJECT, ARRAY], boolean, int, java.lang.String, long, int, int, boolean, boolean, int, java.lang.String]
     candidates:
      com.google.android.gms.measurement.internal.zzk.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, long, long, java.lang.String, boolean, boolean, long, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String):void
      com.google.android.gms.measurement.internal.zzk.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String):void */
    /* access modifiers changed from: package-private */
    public final zzk a(String str) {
        String str2 = str;
        ef b2 = d().b(str2);
        if (b2 == null || TextUtils.isEmpty(b2.i())) {
            this.f2528b.q().j.a("No app data available; dropping", str2);
            return null;
        }
        Boolean b3 = b(b2);
        if (b3 == null || b3.booleanValue()) {
            ef efVar = b2;
            return new zzk(str, b2.c(), b2.i(), b2.j(), b2.k(), b2.l(), b2.m(), (String) null, b2.n(), false, b2.f(), efVar.t(), 0L, 0, efVar.u(), efVar.v(), false, efVar.d());
        }
        this.f2528b.q().c.a("App version does not match; dropping. appId", o.a(str));
        return null;
    }

    /* access modifiers changed from: package-private */
    public final void a(zzo zzo, zzk zzk) {
        l.a(zzo);
        l.a(zzo.f2603a);
        l.a((Object) zzo.f2604b);
        l.a(zzo.c);
        l.a(zzo.c.f2599a);
        g();
        h();
        if (TextUtils.isEmpty(zzk.f2602b) && TextUtils.isEmpty(zzk.r)) {
            return;
        }
        if (!zzk.h) {
            c(zzk);
            return;
        }
        zzo zzo2 = new zzo(zzo);
        boolean z = false;
        zzo2.e = false;
        d().e();
        try {
            zzo d2 = d().d(zzo2.f2603a, zzo2.c.f2599a);
            if (d2 != null && !d2.f2604b.equals(zzo2.f2604b)) {
                this.f2528b.q().f.a("Updating a conditional user property with different origin. name, origin, origin (from DB)", this.f2528b.f().c(zzo2.c.f2599a), zzo2.f2604b, d2.f2604b);
            }
            if (d2 != null && d2.e) {
                zzo2.f2604b = d2.f2604b;
                zzo2.d = d2.d;
                zzo2.h = d2.h;
                zzo2.f = d2.f;
                zzo2.i = d2.i;
                zzo2.e = d2.e;
                zzo2.c = new zzfv(zzo2.c.f2599a, d2.c.f2600b, zzo2.c.a(), d2.c.e);
            } else if (TextUtils.isEmpty(zzo2.f)) {
                zzo2.c = new zzfv(zzo2.c.f2599a, zzo2.d, zzo2.c.a(), zzo2.c.e);
                zzo2.e = true;
                z = true;
            }
            if (zzo2.e) {
                zzfv zzfv = zzo2.c;
                ec ecVar = new ec(zzo2.f2603a, zzo2.f2604b, zzfv.f2599a, zzfv.f2600b, zzfv.a());
                if (d().a(ecVar)) {
                    this.f2528b.q().j.a("User property updated immediately", zzo2.f2603a, this.f2528b.f().c(ecVar.c), ecVar.e);
                } else {
                    this.f2528b.q().c.a("(2)Too many active user properties, ignoring", o.a(zzo2.f2603a), this.f2528b.f().c(ecVar.c), ecVar.e);
                }
                if (z && zzo2.i != null) {
                    b(new zzag(zzo2.i, zzo2.d), zzk);
                }
            }
            if (d().a(zzo2)) {
                this.f2528b.q().j.a("Conditional property added", zzo2.f2603a, this.f2528b.f().c(zzo2.c.f2599a), zzo2.c.a());
            } else {
                this.f2528b.q().c.a("Too many conditional properties, ignoring", o.a(zzo2.f2603a), this.f2528b.f().c(zzo2.c.f2599a), zzo2.c.a());
            }
            d().u();
        } finally {
            d().v();
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(zzo zzo, zzk zzk) {
        l.a(zzo);
        l.a(zzo.f2603a);
        l.a(zzo.c);
        l.a(zzo.c.f2599a);
        g();
        h();
        if (TextUtils.isEmpty(zzk.f2602b) && TextUtils.isEmpty(zzk.r)) {
            return;
        }
        if (!zzk.h) {
            c(zzk);
            return;
        }
        d().e();
        try {
            c(zzk);
            zzo d2 = d().d(zzo.f2603a, zzo.c.f2599a);
            if (d2 != null) {
                this.f2528b.q().j.a("Removing conditional user property", zzo.f2603a, this.f2528b.f().c(zzo.c.f2599a));
                d().e(zzo.f2603a, zzo.c.f2599a);
                if (d2.e) {
                    d().b(zzo.f2603a, zzo.c.f2599a);
                }
                if (zzo.k != null) {
                    Bundle bundle = null;
                    if (zzo.k.f2596b != null) {
                        bundle = zzo.k.f2596b.a();
                    }
                    Bundle bundle2 = bundle;
                    b(this.f2528b.e().a(zzo.f2603a, zzo.k.f2595a, bundle2, d2.f2604b, zzo.k.d), zzk);
                }
            } else {
                this.f2528b.q().f.a("Conditional user property doesn't exist", o.a(zzo.f2603a), this.f2528b.f().c(zzo.c.f2599a));
            }
            d().u();
        } finally {
            d().v();
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00d8  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00fe  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x010c  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0136  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0144  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0152  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x015a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.measurement.internal.ef c(com.google.android.gms.measurement.internal.zzk r9) {
        /*
            r8 = this;
            r8.g()
            r8.h()
            com.google.android.gms.common.internal.l.a(r9)
            java.lang.String r0 = r9.f2601a
            com.google.android.gms.common.internal.l.a(r0)
            com.google.android.gms.measurement.internal.eo r0 = r8.d()
            java.lang.String r1 = r9.f2601a
            com.google.android.gms.measurement.internal.ef r0 = r0.b(r1)
            com.google.android.gms.measurement.internal.ar r1 = r8.f2528b
            com.google.android.gms.measurement.internal.z r1 = r1.b()
            java.lang.String r2 = r9.f2601a
            java.lang.String r1 = r1.b(r2)
            r2 = 1
            if (r0 != 0) goto L_0x0042
            com.google.android.gms.measurement.internal.ef r0 = new com.google.android.gms.measurement.internal.ef
            com.google.android.gms.measurement.internal.ar r3 = r8.f2528b
            java.lang.String r4 = r9.f2601a
            r0.<init>(r3, r4)
            com.google.android.gms.measurement.internal.ar r3 = r8.f2528b
            com.google.android.gms.measurement.internal.ed r3 = r3.e()
            java.lang.String r3 = r3.i()
            r0.a(r3)
            r0.d(r1)
        L_0x0040:
            r1 = 1
            goto L_0x005e
        L_0x0042:
            java.lang.String r3 = r0.e()
            boolean r3 = r1.equals(r3)
            if (r3 != 0) goto L_0x005d
            r0.d(r1)
            com.google.android.gms.measurement.internal.ar r1 = r8.f2528b
            com.google.android.gms.measurement.internal.ed r1 = r1.e()
            java.lang.String r1 = r1.i()
            r0.a(r1)
            goto L_0x0040
        L_0x005d:
            r1 = 0
        L_0x005e:
            java.lang.String r3 = r9.f2602b
            java.lang.String r4 = r0.c()
            boolean r3 = android.text.TextUtils.equals(r3, r4)
            if (r3 != 0) goto L_0x0070
            java.lang.String r1 = r9.f2602b
            r0.b(r1)
            r1 = 1
        L_0x0070:
            java.lang.String r3 = r9.r
            java.lang.String r4 = r0.d()
            boolean r3 = android.text.TextUtils.equals(r3, r4)
            if (r3 != 0) goto L_0x0082
            java.lang.String r1 = r9.r
            r0.c(r1)
            r1 = 1
        L_0x0082:
            java.lang.String r3 = r9.k
            boolean r3 = android.text.TextUtils.isEmpty(r3)
            if (r3 != 0) goto L_0x009c
            java.lang.String r3 = r9.k
            java.lang.String r4 = r0.f()
            boolean r3 = r3.equals(r4)
            if (r3 != 0) goto L_0x009c
            java.lang.String r1 = r9.k
            r0.e(r1)
            r1 = 1
        L_0x009c:
            long r3 = r9.e
            r5 = 0
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 == 0) goto L_0x00b4
            long r3 = r9.e
            long r5 = r0.l()
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 == 0) goto L_0x00b4
            long r3 = r9.e
            r0.d(r3)
            r1 = 1
        L_0x00b4:
            java.lang.String r3 = r9.c
            boolean r3 = android.text.TextUtils.isEmpty(r3)
            if (r3 != 0) goto L_0x00ce
            java.lang.String r3 = r9.c
            java.lang.String r4 = r0.i()
            boolean r3 = r3.equals(r4)
            if (r3 != 0) goto L_0x00ce
            java.lang.String r1 = r9.c
            r0.f(r1)
            r1 = 1
        L_0x00ce:
            long r3 = r9.j
            long r5 = r0.j()
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 == 0) goto L_0x00de
            long r3 = r9.j
            r0.c(r3)
            r1 = 1
        L_0x00de:
            java.lang.String r3 = r9.d
            if (r3 == 0) goto L_0x00f4
            java.lang.String r3 = r9.d
            java.lang.String r4 = r0.k()
            boolean r3 = r3.equals(r4)
            if (r3 != 0) goto L_0x00f4
            java.lang.String r1 = r9.d
            r0.g(r1)
            r1 = 1
        L_0x00f4:
            long r3 = r9.f
            long r5 = r0.m()
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 == 0) goto L_0x0104
            long r3 = r9.f
            r0.e(r3)
            r1 = 1
        L_0x0104:
            boolean r3 = r9.h
            boolean r4 = r0.n()
            if (r3 == r4) goto L_0x0112
            boolean r1 = r9.h
            r0.a(r1)
            r1 = 1
        L_0x0112:
            java.lang.String r3 = r9.g
            boolean r3 = android.text.TextUtils.isEmpty(r3)
            if (r3 != 0) goto L_0x012c
            java.lang.String r3 = r9.g
            java.lang.String r4 = r0.s()
            boolean r3 = r3.equals(r4)
            if (r3 != 0) goto L_0x012c
            java.lang.String r1 = r9.g
            r0.h(r1)
            r1 = 1
        L_0x012c:
            long r3 = r9.l
            long r5 = r0.t()
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 == 0) goto L_0x013c
            long r3 = r9.l
            r0.i(r3)
            r1 = 1
        L_0x013c:
            boolean r3 = r9.o
            boolean r4 = r0.u()
            if (r3 == r4) goto L_0x014a
            boolean r1 = r9.o
            r0.b(r1)
            r1 = 1
        L_0x014a:
            boolean r3 = r9.p
            boolean r4 = r0.v()
            if (r3 == r4) goto L_0x0158
            boolean r9 = r9.p
            r0.c(r9)
            r1 = 1
        L_0x0158:
            if (r1 == 0) goto L_0x0161
            com.google.android.gms.measurement.internal.eo r9 = r8.d()
            r9.a(r0)
        L_0x0161:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.du.c(com.google.android.gms.measurement.internal.zzk):com.google.android.gms.measurement.internal.ef");
    }

    /* access modifiers changed from: package-private */
    public final String d(zzk zzk) {
        try {
            return (String) this.f2528b.p().a(new dy(this, zzk)).get(30000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e2) {
            this.f2528b.q().c.a("Failed to get app instance id. appId", o.a(zzk.f2601a), e2);
            return null;
        }
    }

    static /* synthetic */ void a(du duVar, dz dzVar) {
        duVar.f2528b.p().c();
        eo eoVar = new eo(duVar);
        eoVar.t();
        duVar.i = eoVar;
        duVar.f2528b.e.f2550a = duVar.g;
        ei eiVar = new ei(duVar);
        eiVar.t();
        duVar.l = eiVar;
        ce ceVar = new ce(duVar);
        ceVar.t();
        duVar.f2527a = ceVar;
        dq dqVar = new dq(duVar);
        dqVar.t();
        duVar.k = dqVar;
        duVar.j = new x(duVar);
        if (duVar.d != duVar.e) {
            duVar.f2528b.q().c.a("Not all upload components initialized", Integer.valueOf(duVar.d), Integer.valueOf(duVar.e));
        }
        duVar.n = true;
    }
}
