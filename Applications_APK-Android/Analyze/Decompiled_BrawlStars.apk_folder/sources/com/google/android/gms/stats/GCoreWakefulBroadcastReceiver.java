package com.google.android.gms.stats;

import android.support.v4.content.WakefulBroadcastReceiver;

public abstract class GCoreWakefulBroadcastReceiver extends WakefulBroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private static String f2615a = "GCoreWakefulBroadcastReceiver";
}
