package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.h;

public class zzhb<T> extends zzl {

    /* renamed from: a  reason: collision with root package name */
    h<T> f1976a;

    public final void a(Status status) throws RemoteException {
        this.f1976a.a(new ApiException(status));
    }
}
