package com.google.android.gms.analytics;

import android.content.Context;
import com.google.android.gms.internal.measurement.t;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class a extends e {
    private static List<Runnable> h = new ArrayList();

    /* renamed from: a  reason: collision with root package name */
    public boolean f1312a;

    /* renamed from: b  reason: collision with root package name */
    public boolean f1313b;
    volatile boolean c;
    private Set<Object> i = new HashSet();

    public final boolean a() {
        return this.f1312a;
    }

    public a(t tVar) {
        super(tVar);
    }

    public static a a(Context context) {
        return t.a(context).d();
    }

    public static void b() {
        synchronized (a.class) {
            if (h != null) {
                for (Runnable run : h) {
                    run.run();
                }
                h = null;
            }
        }
    }

    public final d a(String str) {
        d dVar;
        synchronized (this) {
            dVar = new d(this.d, str);
            dVar.n();
        }
        return dVar;
    }

    public final void c() {
        this.d.c().b();
    }
}
