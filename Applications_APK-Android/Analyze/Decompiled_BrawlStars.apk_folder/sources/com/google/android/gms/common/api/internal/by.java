package com.google.android.gms.common.api.internal;

import android.app.Dialog;

final class by extends ay {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Dialog f1445a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ bx f1446b;

    by(bx bxVar, Dialog dialog) {
        this.f1446b = bxVar;
        this.f1445a = dialog;
    }

    public final void a() {
        this.f1446b.f1443a.f();
        if (this.f1445a.isShowing()) {
            this.f1445a.dismiss();
        }
    }
}
