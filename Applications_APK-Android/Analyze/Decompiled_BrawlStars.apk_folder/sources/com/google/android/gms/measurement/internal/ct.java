package com.google.android.gms.measurement.internal;

import android.os.RemoteException;
import android.text.TextUtils;

final class ct implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ boolean f2483a = true;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ boolean f2484b;
    private final /* synthetic */ zzo c;
    private final /* synthetic */ zzk d;
    private final /* synthetic */ zzo e;
    private final /* synthetic */ cl f;

    ct(cl clVar, boolean z, boolean z2, zzo zzo, zzk zzk, zzo zzo2) {
        this.f = clVar;
        this.f2484b = z2;
        this.c = zzo;
        this.d = zzk;
        this.e = zzo2;
    }

    public final void run() {
        zzaj zzaj = this.f.f2470b;
        if (zzaj == null) {
            this.f.q().c.a("Discarding data. Failed to send conditional user property to service");
            return;
        }
        if (this.f2483a) {
            this.f.a(zzaj, this.f2484b ? null : this.c, this.d);
        } else {
            try {
                if (TextUtils.isEmpty(this.e.f2603a)) {
                    zzaj.a(this.c, this.d);
                } else {
                    zzaj.a(this.c);
                }
            } catch (RemoteException e2) {
                this.f.q().c.a("Failed to send conditional user property to the service", e2);
            }
        }
        this.f.y();
    }
}
