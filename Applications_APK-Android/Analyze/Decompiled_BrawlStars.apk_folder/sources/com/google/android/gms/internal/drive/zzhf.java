package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.drive.DriveId;

public final class zzhf extends zzhb<DriveId> {
    public final void a(zzfh zzfh) throws RemoteException {
        this.f1976a.a((Boolean) zzfh.f1942a);
    }

    public final void a(zzfs zzfs) throws RemoteException {
        this.f1976a.a((Boolean) ((DriveId) new a(zzfs.f1950a).a(au.f1893a)));
    }
}
