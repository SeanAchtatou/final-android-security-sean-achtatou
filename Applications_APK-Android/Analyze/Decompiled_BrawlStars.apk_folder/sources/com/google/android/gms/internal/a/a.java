package com.google.android.gms.internal.a;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;

public interface a {
    ExecutorService a(ThreadFactory threadFactory);

    ScheduledExecutorService b(ThreadFactory threadFactory);
}
