package com.google.android.gms.dynamite;

import android.content.Context;
import com.google.android.gms.dynamite.DynamiteModule;

final class f implements DynamiteModule.a {
    f() {
    }

    public final DynamiteModule.a.b a(Context context, String str, DynamiteModule.a.C0116a aVar) throws DynamiteModule.LoadingException {
        DynamiteModule.a.b bVar = new DynamiteModule.a.b();
        bVar.f1781a = aVar.a(context, str);
        bVar.f1782b = aVar.a(context, str, true);
        if (bVar.f1781a == 0 && bVar.f1782b == 0) {
            bVar.c = 0;
        } else if (bVar.f1782b >= bVar.f1781a) {
            bVar.c = 1;
        } else {
            bVar.c = -1;
        }
        return bVar;
    }
}
