package com.google.android.gms.common.internal;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.google.android.gms.common.api.d;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public final class f implements Handler.Callback {

    /* renamed from: a  reason: collision with root package name */
    public final a f1601a;

    /* renamed from: b  reason: collision with root package name */
    public final ArrayList<d.b> f1602b = new ArrayList<>();
    public final ArrayList<d.b> c = new ArrayList<>();
    public final ArrayList<d.c> d = new ArrayList<>();
    public volatile boolean e = false;
    public final AtomicInteger f = new AtomicInteger(0);
    public boolean g = false;
    public final Handler h;
    public final Object i = new Object();

    public interface a {
        Bundle a_();

        boolean b();
    }

    public f(Looper looper, a aVar) {
        this.f1601a = aVar;
        this.h = new com.google.android.gms.internal.base.d(looper, this);
    }

    public final void a() {
        this.e = false;
        this.f.incrementAndGet();
    }

    public final void a(d.c cVar) {
        l.a(cVar);
        synchronized (this.i) {
            if (this.d.contains(cVar)) {
                String valueOf = String.valueOf(cVar);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 67);
                sb.append("registerConnectionFailedListener(): listener ");
                sb.append(valueOf);
                sb.append(" is already registered");
                sb.toString();
            } else {
                this.d.add(cVar);
            }
        }
    }

    public final boolean handleMessage(Message message) {
        if (message.what == 1) {
            d.b bVar = (d.b) message.obj;
            synchronized (this.i) {
                if (this.e && this.f1601a.b() && this.f1602b.contains(bVar)) {
                    bVar.onConnected(this.f1601a.a_());
                }
            }
            return true;
        }
        int i2 = message.what;
        StringBuilder sb = new StringBuilder(45);
        sb.append("Don't know how to handle message: ");
        sb.append(i2);
        Log.wtf("GmsClientEvents", sb.toString(), new Exception());
        return false;
    }
}
