package com.google.android.gms.internal.measurement;

import java.lang.Comparable;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

class hm<K extends Comparable<K>, V> extends AbstractMap<K, V> {

    /* renamed from: a  reason: collision with root package name */
    boolean f2265a;

    /* renamed from: b  reason: collision with root package name */
    private final int f2266b;
    /* access modifiers changed from: private */
    public List<ht> c;
    /* access modifiers changed from: private */
    public Map<K, V> d;
    private volatile hv e;
    /* access modifiers changed from: private */
    public Map<K, V> f;
    private volatile hp g;

    static <FieldDescriptorType extends fj<FieldDescriptorType>> hm<FieldDescriptorType, Object> a(int i) {
        return new hn(i);
    }

    private hm(int i) {
        this.f2266b = i;
        this.c = Collections.emptyList();
        this.d = Collections.emptyMap();
        this.f = Collections.emptyMap();
    }

    public void a() {
        Map<K, V> map;
        Map<K, V> map2;
        if (!this.f2265a) {
            if (this.d.isEmpty()) {
                map = Collections.emptyMap();
            } else {
                map = Collections.unmodifiableMap(this.d);
            }
            this.d = map;
            if (this.f.isEmpty()) {
                map2 = Collections.emptyMap();
            } else {
                map2 = Collections.unmodifiableMap(this.f);
            }
            this.f = map2;
            this.f2265a = true;
        }
    }

    public final int b() {
        return this.c.size();
    }

    public final Map.Entry<K, V> b(int i) {
        return this.c.get(i);
    }

    public final Iterable<Map.Entry<K, V>> c() {
        if (this.d.isEmpty()) {
            return hq.a();
        }
        return this.d.entrySet();
    }

    public int size() {
        return this.c.size() + this.d.size();
    }

    public boolean containsKey(Object obj) {
        Comparable comparable = (Comparable) obj;
        return a(comparable) >= 0 || this.d.containsKey(comparable);
    }

    public V get(Object obj) {
        Comparable comparable = (Comparable) obj;
        int a2 = a(comparable);
        if (a2 >= 0) {
            return this.c.get(a2).getValue();
        }
        return this.d.get(comparable);
    }

    /* renamed from: a */
    public final V put(Comparable comparable, Object obj) {
        e();
        int a2 = a(comparable);
        if (a2 >= 0) {
            return this.c.get(a2).setValue(obj);
        }
        e();
        if (this.c.isEmpty() && !(this.c instanceof ArrayList)) {
            this.c = new ArrayList(this.f2266b);
        }
        int i = -(a2 + 1);
        if (i >= this.f2266b) {
            return f().put(comparable, obj);
        }
        int size = this.c.size();
        int i2 = this.f2266b;
        if (size == i2) {
            ht remove = this.c.remove(i2 - 1);
            f().put((Comparable) remove.getKey(), remove.getValue());
        }
        this.c.add(i, new ht(this, comparable, obj));
        return null;
    }

    public void clear() {
        e();
        if (!this.c.isEmpty()) {
            this.c.clear();
        }
        if (!this.d.isEmpty()) {
            this.d.clear();
        }
    }

    public V remove(Object obj) {
        e();
        Comparable comparable = (Comparable) obj;
        int a2 = a(comparable);
        if (a2 >= 0) {
            return c(a2);
        }
        if (this.d.isEmpty()) {
            return null;
        }
        return this.d.remove(comparable);
    }

    /* access modifiers changed from: private */
    public final V c(int i) {
        e();
        V value = this.c.remove(i).getValue();
        if (!this.d.isEmpty()) {
            Iterator it = f().entrySet().iterator();
            this.c.add(new ht(this, (Map.Entry) it.next()));
            it.remove();
        }
        return value;
    }

    private final int a(Comparable comparable) {
        int size = this.c.size() - 1;
        if (size >= 0) {
            int compareTo = comparable.compareTo((Comparable) this.c.get(size).getKey());
            if (compareTo > 0) {
                return -(size + 2);
            }
            if (compareTo == 0) {
                return size;
            }
        }
        int i = 0;
        while (i <= size) {
            int i2 = (i + size) / 2;
            int compareTo2 = comparable.compareTo((Comparable) this.c.get(i2).getKey());
            if (compareTo2 < 0) {
                size = i2 - 1;
            } else if (compareTo2 <= 0) {
                return i2;
            } else {
                i = i2 + 1;
            }
        }
        return -(i + 1);
    }

    public Set<Map.Entry<K, V>> entrySet() {
        if (this.e == null) {
            this.e = new hv(this, (byte) 0);
        }
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public final Set<Map.Entry<K, V>> d() {
        if (this.g == null) {
            this.g = new hp(this, (byte) 0);
        }
        return this.g;
    }

    /* access modifiers changed from: private */
    public final void e() {
        if (this.f2265a) {
            throw new UnsupportedOperationException();
        }
    }

    private final SortedMap<K, V> f() {
        e();
        if (this.d.isEmpty() && !(this.d instanceof TreeMap)) {
            this.d = new TreeMap();
            this.f = ((TreeMap) this.d).descendingMap();
        }
        return (SortedMap) this.d;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof hm)) {
            return super.equals(obj);
        }
        hm hmVar = (hm) obj;
        int size = size();
        if (size != hmVar.size()) {
            return false;
        }
        int b2 = b();
        if (b2 != hmVar.b()) {
            return entrySet().equals(hmVar.entrySet());
        }
        for (int i = 0; i < b2; i++) {
            if (!b(i).equals(hmVar.b(i))) {
                return false;
            }
        }
        if (b2 != size) {
            return this.d.equals(hmVar.d);
        }
        return true;
    }

    public int hashCode() {
        int b2 = b();
        int i = 0;
        for (int i2 = 0; i2 < b2; i2++) {
            i += this.c.get(i2).hashCode();
        }
        return this.d.size() > 0 ? i + this.d.hashCode() : i;
    }

    /* synthetic */ hm(int i, byte b2) {
        this(i);
    }
}
