package com.google.android.gms.internal.measurement;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import com.google.android.gms.analytics.n;

public final class l extends r {

    /* renamed from: a  reason: collision with root package name */
    final ag f2320a;

    public l(t tVar, v vVar) {
        super(tVar);
        com.google.android.gms.common.internal.l.a(vVar);
        this.f2320a = new ag(tVar, vVar);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2320a.n();
    }

    public final long a(w wVar) {
        m();
        com.google.android.gms.common.internal.l.a(wVar);
        n.b();
        long b2 = this.f2320a.b(wVar);
        if (b2 == 0) {
            this.f2320a.a(wVar);
        }
        return b2;
    }

    public final void a(bh bhVar) {
        com.google.android.gms.common.internal.l.a(bhVar);
        m();
        b("Hit delivery requested", bhVar);
        this.c.b().a(new o(this, bhVar));
    }

    public final void a(ba baVar) {
        m();
        this.c.b().a(new p(this, baVar));
    }

    public final void b() {
        m();
        Context context = this.c.f2332a;
        if (!bq.a(context) || !br.a(context)) {
            a((ba) null);
            return;
        }
        Intent intent = new Intent("com.google.android.gms.analytics.ANALYTICS_DISPATCH");
        intent.setComponent(new ComponentName(context, "com.google.android.gms.analytics.AnalyticsService"));
        context.startService(intent);
    }

    public final void c() {
        m();
        n.b();
        ag agVar = this.f2320a;
        n.b();
        agVar.m();
        agVar.b("Service disconnected");
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        n.b();
        this.f2320a.e();
    }

    /* access modifiers changed from: package-private */
    public final void e() {
        n.b();
        this.f2320a.d();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.l.a(java.lang.String, java.lang.Object):java.lang.String
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.l.a(java.lang.Object, java.lang.Object):T
      com.google.android.gms.common.internal.l.a(boolean, java.lang.Object):void
      com.google.android.gms.common.internal.l.a(java.lang.String, java.lang.Object):java.lang.String */
    public final void a(String str, Runnable runnable) {
        com.google.android.gms.common.internal.l.a(str, (Object) "campaign param can't be empty");
        this.c.b().a(new n(this, str, runnable));
    }
}
