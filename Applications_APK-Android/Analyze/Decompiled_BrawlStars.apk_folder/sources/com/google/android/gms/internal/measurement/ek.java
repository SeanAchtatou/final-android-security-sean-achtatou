package com.google.android.gms.internal.measurement;

import java.util.NoSuchElementException;

final class ek extends em {

    /* renamed from: a  reason: collision with root package name */
    private int f2186a = 0;

    /* renamed from: b  reason: collision with root package name */
    private final int f2187b = this.c.a();
    private final /* synthetic */ ej c;

    ek(ej ejVar) {
        this.c = ejVar;
    }

    public final boolean hasNext() {
        return this.f2186a < this.f2187b;
    }

    public final byte a() {
        int i = this.f2186a;
        if (i < this.f2187b) {
            this.f2186a = i + 1;
            return this.c.b(i);
        }
        throw new NoSuchElementException();
    }
}
