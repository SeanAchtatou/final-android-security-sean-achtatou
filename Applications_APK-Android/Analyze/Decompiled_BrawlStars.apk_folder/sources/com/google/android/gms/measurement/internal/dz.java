package com.google.android.gms.measurement.internal;

import android.content.Context;
import com.google.android.gms.common.internal.l;

public final class dz {

    /* renamed from: a  reason: collision with root package name */
    final Context f2538a;

    public dz(Context context) {
        l.a(context);
        Context applicationContext = context.getApplicationContext();
        l.a(applicationContext);
        this.f2538a = applicationContext;
    }
}
