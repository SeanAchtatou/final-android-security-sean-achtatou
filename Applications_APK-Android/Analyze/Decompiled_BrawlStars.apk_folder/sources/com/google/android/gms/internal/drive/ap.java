package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.drive.DriveId;

public final class ap implements Parcelable.Creator<zzgv> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zzgv[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        DriveId driveId = null;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            if ((65535 & readInt) != 2) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                driveId = (DriveId) SafeParcelReader.a(parcel, readInt, DriveId.CREATOR);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new zzgv(driveId);
    }
}
