package com.google.android.gms.internal.measurement;

final class ab implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zzcl f2033a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ aa f2034b;

    ab(aa aaVar, zzcl zzcl) {
        this.f2034b = aaVar;
        this.f2033a = zzcl;
    }

    public final void run() {
        if (!this.f2034b.f2031a.b()) {
            this.f2034b.f2031a.c("Connected to service after a timeout");
            y.a(this.f2034b.f2031a, this.f2033a);
        }
    }
}
