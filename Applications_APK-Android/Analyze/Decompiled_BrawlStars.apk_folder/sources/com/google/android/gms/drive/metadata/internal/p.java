package com.google.android.gms.drive.metadata.internal;

import android.os.Bundle;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.metadata.g;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import org.json.JSONArray;
import org.json.JSONException;

public final class p extends g<String> {
    public p(String str) {
        super(str, Collections.singleton(str), Collections.emptySet(), 4300000);
    }

    public final /* synthetic */ Object c(DataHolder dataHolder, int i, int i2) {
        return c(dataHolder, i, i2);
    }

    public final Collection<String> b_(DataHolder dataHolder, int i, int i2) {
        try {
            String c = dataHolder.c(this.f1731b, i, i2);
            if (c == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            JSONArray jSONArray = new JSONArray(c);
            for (int i3 = 0; i3 < jSONArray.length(); i3++) {
                arrayList.add(jSONArray.getString(i3));
            }
            return Collections.unmodifiableCollection(arrayList);
        } catch (JSONException e) {
            throw new IllegalStateException("DataHolder supplied invalid JSON", e);
        }
    }

    public final /* synthetic */ void a(Bundle bundle, Object obj) {
        bundle.putStringArrayList(this.f1731b, new ArrayList((Collection) obj));
    }

    public final /* synthetic */ Object b(Bundle bundle) {
        return bundle.getStringArrayList(this.f1731b);
    }
}
