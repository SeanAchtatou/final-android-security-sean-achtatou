package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.internal.b;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

final class aa extends ae {

    /* renamed from: a  reason: collision with root package name */
    private final ArrayList<a.f> f1383a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ u f1384b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public aa(u uVar, ArrayList<a.f> arrayList) {
        super(uVar, (byte) 0);
        this.f1384b = uVar;
        this.f1383a = arrayList;
    }

    public final void a() {
        Set<Scope> set;
        ag agVar = this.f1384b.f1496a.m;
        u uVar = this.f1384b;
        if (uVar.k == null) {
            set = Collections.emptySet();
        } else {
            HashSet hashSet = new HashSet(uVar.k.f1596b);
            Map<a<?>, b.C0115b> map = uVar.k.d;
            for (a next : map.keySet()) {
                if (!uVar.f1496a.g.containsKey(next.b())) {
                    hashSet.addAll(map.get(next).f1599a);
                }
            }
            set = hashSet;
        }
        agVar.c = set;
        ArrayList arrayList = this.f1383a;
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            Object obj = arrayList.get(i);
            i++;
            ((a.f) obj).a(this.f1384b.h, this.f1384b.f1496a.m.c);
        }
    }
}
