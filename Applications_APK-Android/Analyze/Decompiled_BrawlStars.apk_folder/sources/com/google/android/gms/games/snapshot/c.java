package com.google.android.gms.games.snapshot;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class c implements Parcelable.Creator<SnapshotEntity> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new SnapshotEntity[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        SnapshotMetadataEntity snapshotMetadataEntity = null;
        zza zza = null;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            int i = 65535 & readInt;
            if (i == 1) {
                snapshotMetadataEntity = (SnapshotMetadataEntity) SafeParcelReader.a(parcel, readInt, SnapshotMetadataEntity.CREATOR);
            } else if (i != 3) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                zza = (zza) SafeParcelReader.a(parcel, readInt, zza.CREATOR);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new SnapshotEntity(snapshotMetadataEntity, zza);
    }
}
