package com.google.android.gms.internal.measurement;

import com.google.android.gms.analytics.c;

@Deprecated
public final class bj {

    /* renamed from: a  reason: collision with root package name */
    private static volatile c f2077a = new av();

    public static void a(String str, Object obj) {
        bk b2 = bk.b();
        if (b2 != null) {
            b2.e(str, obj);
        } else if (a(3)) {
            if (obj != null) {
                String valueOf = String.valueOf(obj);
                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 1 + String.valueOf(valueOf).length());
                sb.append(str);
                sb.append(":");
                sb.append(valueOf);
                sb.toString();
            }
            bd<String> bdVar = bc.f2066b;
        }
    }

    public static void a(String str) {
        bk b2 = bk.b();
        if (b2 != null) {
            b2.e(str);
        } else if (a(2)) {
            bd<String> bdVar = bc.f2066b;
        }
    }

    private static boolean a(int i) {
        if (f2077a == null || f2077a.a() > i) {
            return false;
        }
        return true;
    }
}
