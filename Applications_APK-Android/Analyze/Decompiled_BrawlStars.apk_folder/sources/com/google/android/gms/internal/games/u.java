package com.google.android.gms.internal.games;

import android.content.Intent;
import com.google.android.gms.common.api.d;
import com.google.android.gms.games.achievement.a;

public final class u implements a {
    public final Intent a(d dVar) {
        return com.google.android.gms.games.a.a(dVar).t();
    }

    public final void a(d dVar, String str) {
        dVar.a(new v(str, dVar, str));
    }
}
