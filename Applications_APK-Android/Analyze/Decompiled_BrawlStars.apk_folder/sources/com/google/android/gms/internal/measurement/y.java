package com.google.android.gms.internal.measurement;

import android.content.ComponentName;
import android.os.RemoteException;
import com.google.android.gms.analytics.n;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.stats.a;
import java.util.Collections;

public final class y extends r {

    /* renamed from: a  reason: collision with root package name */
    final aa f2341a = new aa(this);

    /* renamed from: b  reason: collision with root package name */
    private zzcl f2342b;
    private final aw d;
    private final bw e;

    protected y(t tVar) {
        super(tVar);
        this.e = new bw(tVar.c);
        this.d = new z(this, tVar);
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    public final boolean b() {
        n.b();
        m();
        return this.f2342b != null;
    }

    public final boolean a(bh bhVar) {
        String str;
        l.a(bhVar);
        n.b();
        m();
        zzcl zzcl = this.f2342b;
        if (zzcl == null) {
            return false;
        }
        if (bhVar.f) {
            str = au.h();
        } else {
            str = au.i();
        }
        try {
            zzcl.a(bhVar.f2073a, bhVar.d, str, Collections.emptyList());
            e();
            return true;
        } catch (RemoteException unused) {
            b("Failed to send hits to AnalyticsService");
            return false;
        }
    }

    private final void e() {
        this.e.a();
        this.d.a(((Long) bc.A.f2067a).longValue());
    }

    public final boolean c() {
        n.b();
        m();
        if (this.f2342b != null) {
            return true;
        }
        zzcl a2 = this.f2341a.a();
        if (a2 == null) {
            return false;
        }
        this.f2342b = a2;
        e();
        return true;
    }

    public final void d() {
        n.b();
        m();
        try {
            a.a();
            a.a(g(), this.f2341a);
        } catch (IllegalArgumentException | IllegalStateException unused) {
        }
        if (this.f2342b != null) {
            this.f2342b = null;
            this.c.c().c();
        }
    }

    static /* synthetic */ void a(y yVar, zzcl zzcl) {
        n.b();
        yVar.f2342b = zzcl;
        yVar.e();
        yVar.c.c().d();
    }

    static /* synthetic */ void a(y yVar, ComponentName componentName) {
        n.b();
        if (yVar.f2342b != null) {
            yVar.f2342b = null;
            yVar.a("Disconnected from device AnalyticsService", componentName);
            yVar.c.c().c();
        }
    }

    static /* synthetic */ void a(y yVar) {
        n.b();
        if (yVar.b()) {
            yVar.b("Inactivity, disconnecting from device AnalyticsService");
            yVar.d();
        }
    }
}
