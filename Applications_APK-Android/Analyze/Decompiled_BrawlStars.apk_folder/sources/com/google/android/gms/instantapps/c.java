package com.google.android.gms.instantapps;

import com.google.android.gms.common.Feature;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    public static final Feature f1862a = new Feature("device_enabled_api", 1);

    /* renamed from: b  reason: collision with root package name */
    private static final Feature[] f1863b = {f1862a};
}
