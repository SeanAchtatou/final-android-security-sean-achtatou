package com.google.android.gms.tagmanager;

import com.facebook.internal.ServerProtocol;
import com.google.android.gms.internal.measurement.da;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class bk {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f2646a = null;

    /* renamed from: b  reason: collision with root package name */
    private static Long f2647b = new Long(0);
    private static Double c = new Double(0.0d);
    private static bj d = bj.a(0);
    private static String e = new String("");
    private static Boolean f = new Boolean(false);
    private static List<Object> g = new ArrayList(0);
    private static Map<Object, Object> h = new HashMap();
    private static da i = a(e);

    public static da a() {
        return i;
    }

    private static String b(Object obj) {
        return obj == null ? e : obj.toString();
    }

    public static String a(da daVar) {
        return b(c(daVar));
    }

    public static Boolean b(da daVar) {
        Object c2 = c(daVar);
        if (c2 instanceof Boolean) {
            return (Boolean) c2;
        }
        String b2 = b(c2);
        if (ServerProtocol.DIALOG_RETURN_SCOPES_TRUE.equalsIgnoreCase(b2)) {
            return Boolean.TRUE;
        }
        if ("false".equalsIgnoreCase(b2)) {
            return Boolean.FALSE;
        }
        return f;
    }

    public static da a(Object obj) {
        String str;
        long j;
        da daVar = new da();
        if (obj instanceof da) {
            return (da) obj;
        }
        boolean z = true;
        boolean z2 = false;
        if (obj instanceof String) {
            daVar.f2151a = 1;
            daVar.f2152b = (String) obj;
        } else if (obj instanceof List) {
            daVar.f2151a = 2;
            List<Object> list = (List) obj;
            ArrayList arrayList = new ArrayList(list.size());
            boolean z3 = false;
            for (Object a2 : list) {
                da a3 = a(a2);
                da daVar2 = i;
                if (a3 == daVar2) {
                    return daVar2;
                }
                z3 = z3 || a3.l;
                arrayList.add(a3);
            }
            daVar.c = (da[]) arrayList.toArray(new da[0]);
            z2 = z3;
        } else if (obj instanceof Map) {
            daVar.f2151a = 3;
            Set<Map.Entry> entrySet = ((Map) obj).entrySet();
            ArrayList arrayList2 = new ArrayList(entrySet.size());
            ArrayList arrayList3 = new ArrayList(entrySet.size());
            boolean z4 = false;
            for (Map.Entry entry : entrySet) {
                da a4 = a(entry.getKey());
                da a5 = a(entry.getValue());
                da daVar3 = i;
                if (a4 == daVar3 || a5 == daVar3) {
                    return i;
                }
                z4 = z4 || a4.l || a5.l;
                arrayList2.add(a4);
                arrayList3.add(a5);
            }
            daVar.d = (da[]) arrayList2.toArray(new da[0]);
            daVar.e = (da[]) arrayList3.toArray(new da[0]);
            z2 = z4;
        } else {
            if ((obj instanceof Double) || (obj instanceof Float) || ((obj instanceof bj) && (((bj) obj).f2644a ^ true))) {
                daVar.f2151a = 1;
                daVar.f2152b = obj.toString();
            } else {
                if (!(obj instanceof Byte) && !(obj instanceof Short) && !(obj instanceof Integer) && !(obj instanceof Long) && (!(obj instanceof bj) || !((bj) obj).f2644a)) {
                    z = false;
                }
                if (z) {
                    daVar.f2151a = 6;
                    if (obj instanceof Number) {
                        j = ((Number) obj).longValue();
                    } else {
                        ab.a("getInt64 received non-Number");
                        j = 0;
                    }
                    daVar.h = j;
                } else if (obj instanceof Boolean) {
                    daVar.f2151a = 8;
                    daVar.i = ((Boolean) obj).booleanValue();
                } else {
                    if (obj == null) {
                        str = "null";
                    } else {
                        str = obj.getClass().toString();
                    }
                    String valueOf = String.valueOf(str);
                    ab.a(valueOf.length() != 0 ? "Converting to Value from unknown object type: ".concat(valueOf) : new String("Converting to Value from unknown object type: "));
                    return i;
                }
            }
        }
        daVar.l = z2;
        return daVar;
    }

    public static Object c(da daVar) {
        if (daVar == null) {
            return null;
        }
        int i2 = 0;
        switch (daVar.f2151a) {
            case 1:
                return daVar.f2152b;
            case 2:
                ArrayList arrayList = new ArrayList(daVar.c.length);
                da[] daVarArr = daVar.c;
                int length = daVarArr.length;
                while (i2 < length) {
                    Object c2 = c(daVarArr[i2]);
                    if (c2 == null) {
                        return null;
                    }
                    arrayList.add(c2);
                    i2++;
                }
                return arrayList;
            case 3:
                if (daVar.d.length != daVar.e.length) {
                    String valueOf = String.valueOf(daVar.toString());
                    ab.a(valueOf.length() != 0 ? "Converting an invalid value to object: ".concat(valueOf) : new String("Converting an invalid value to object: "));
                    return null;
                }
                HashMap hashMap = new HashMap(daVar.e.length);
                while (i2 < daVar.d.length) {
                    Object c3 = c(daVar.d[i2]);
                    Object c4 = c(daVar.e[i2]);
                    if (c3 == null || c4 == null) {
                        return null;
                    }
                    hashMap.put(c3, c4);
                    i2++;
                }
                return hashMap;
            case 4:
                ab.a("Trying to convert a macro reference to object");
                return null;
            case 5:
                ab.a("Trying to convert a function id to object");
                return null;
            case 6:
                return Long.valueOf(daVar.h);
            case 7:
                StringBuilder sb = new StringBuilder();
                da[] daVarArr2 = daVar.j;
                int length2 = daVarArr2.length;
                while (i2 < length2) {
                    String b2 = b(c(daVarArr2[i2]));
                    if (b2 == e) {
                        return null;
                    }
                    sb.append(b2);
                    i2++;
                }
                return sb.toString();
            case 8:
                return Boolean.valueOf(daVar.i);
            default:
                int i3 = daVar.f2151a;
                StringBuilder sb2 = new StringBuilder(46);
                sb2.append("Failed to convert a value of type: ");
                sb2.append(i3);
                ab.a(sb2.toString());
                return null;
        }
    }
}
