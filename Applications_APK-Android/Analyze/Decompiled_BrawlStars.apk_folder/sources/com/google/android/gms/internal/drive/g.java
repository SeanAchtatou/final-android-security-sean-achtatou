package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable;

public class g {

    /* renamed from: a  reason: collision with root package name */
    private static final ClassLoader f1913a = g.class.getClassLoader();

    private g() {
    }

    public static <T extends Parcelable> T a(Parcel parcel, Parcelable.Creator<T> creator) {
        if (parcel.readInt() == 0) {
            return null;
        }
        return (Parcelable) creator.createFromParcel(parcel);
    }

    public static boolean a(Parcel parcel) {
        return parcel.readInt() != 0;
    }
}
