package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.drive.zzh;
import java.util.ArrayList;

public final class v implements Parcelable.Creator<zzff> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zzff[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        long j = 0;
        long j2 = 0;
        ArrayList arrayList = null;
        int i = 0;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            int i2 = 65535 & readInt;
            if (i2 == 2) {
                j = SafeParcelReader.f(parcel, readInt);
            } else if (i2 == 3) {
                j2 = SafeParcelReader.f(parcel, readInt);
            } else if (i2 == 4) {
                i = SafeParcelReader.d(parcel, readInt);
            } else if (i2 != 5) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                arrayList = SafeParcelReader.c(parcel, readInt, zzh.CREATOR);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new zzff(j, j2, i, arrayList);
    }
}
