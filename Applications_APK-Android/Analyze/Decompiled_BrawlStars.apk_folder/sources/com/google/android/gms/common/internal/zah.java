package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.base.a;
import com.google.android.gms.internal.base.zaa;

public final class zah extends zaa implements ISignInButtonCreator {
    zah(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.ISignInButtonCreator");
    }

    public final IObjectWrapper a(IObjectWrapper iObjectWrapper, SignInButtonConfig signInButtonConfig) throws RemoteException {
        Parcel a2 = a();
        a.a(a2, iObjectWrapper);
        a.a(a2, signInButtonConfig);
        Parcel a3 = a(2, a2);
        IObjectWrapper a4 = IObjectWrapper.Stub.a(a3.readStrongBinder());
        a3.recycle();
        return a4;
    }
}
