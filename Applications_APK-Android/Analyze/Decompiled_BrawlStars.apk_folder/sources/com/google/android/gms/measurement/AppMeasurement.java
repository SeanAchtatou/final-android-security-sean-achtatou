package com.google.android.gms.measurement;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.measurement.internal.ar;
import com.google.android.gms.measurement.internal.au;
import com.google.android.gms.measurement.internal.br;
import com.google.android.gms.measurement.internal.bv;
import com.google.android.gms.measurement.internal.cf;
import com.google.android.gms.measurement.internal.j;
import com.google.android.gms.measurement.internal.v;
import java.util.List;
import java.util.Map;

@Deprecated
public class AppMeasurement {

    /* renamed from: a  reason: collision with root package name */
    public final ar f2353a;

    public static class ConditionalUserProperty {
        public boolean mActive;
        public String mAppId;
        public long mCreationTimestamp;
        public String mExpiredEventName;
        public Bundle mExpiredEventParams;
        public String mName;
        public String mOrigin;
        public long mTimeToLive;
        public String mTimedOutEventName;
        public Bundle mTimedOutEventParams;
        public String mTriggerEventName;
        public long mTriggerTimeout;
        public String mTriggeredEventName;
        public Bundle mTriggeredEventParams;
        public long mTriggeredTimestamp;
        public Object mValue;

        public ConditionalUserProperty() {
        }

        public ConditionalUserProperty(ConditionalUserProperty conditionalUserProperty) {
            l.a(conditionalUserProperty);
            this.mAppId = conditionalUserProperty.mAppId;
            this.mOrigin = conditionalUserProperty.mOrigin;
            this.mCreationTimestamp = conditionalUserProperty.mCreationTimestamp;
            this.mName = conditionalUserProperty.mName;
            Object obj = conditionalUserProperty.mValue;
            if (obj != null) {
                this.mValue = cf.a(obj);
                if (this.mValue == null) {
                    this.mValue = conditionalUserProperty.mValue;
                }
            }
            this.mActive = conditionalUserProperty.mActive;
            this.mTriggerEventName = conditionalUserProperty.mTriggerEventName;
            this.mTriggerTimeout = conditionalUserProperty.mTriggerTimeout;
            this.mTimedOutEventName = conditionalUserProperty.mTimedOutEventName;
            Bundle bundle = conditionalUserProperty.mTimedOutEventParams;
            if (bundle != null) {
                this.mTimedOutEventParams = new Bundle(bundle);
            }
            this.mTriggeredEventName = conditionalUserProperty.mTriggeredEventName;
            Bundle bundle2 = conditionalUserProperty.mTriggeredEventParams;
            if (bundle2 != null) {
                this.mTriggeredEventParams = new Bundle(bundle2);
            }
            this.mTriggeredTimestamp = conditionalUserProperty.mTriggeredTimestamp;
            this.mTimeToLive = conditionalUserProperty.mTimeToLive;
            this.mExpiredEventName = conditionalUserProperty.mExpiredEventName;
            Bundle bundle3 = conditionalUserProperty.mExpiredEventParams;
            if (bundle3 != null) {
                this.mExpiredEventParams = new Bundle(bundle3);
            }
        }
    }

    public static final class a extends br {
    }

    @Deprecated
    public static AppMeasurement getInstance(Context context) {
        return ar.a(context, (j) null).h;
    }

    public AppMeasurement(ar arVar) {
        l.a(arVar);
        this.f2353a = arVar;
    }

    public void logEventInternal(String str, String str2, Bundle bundle) {
        this.f2353a.d().a(str, str2, bundle);
    }

    public String getCurrentScreenName() {
        return this.f2353a.d().w();
    }

    public String getCurrentScreenClass() {
        return this.f2353a.d().x();
    }

    public String getAppInstanceId() {
        return this.f2353a.d().f2443b.get();
    }

    public String getGmpAppId() {
        return this.f2353a.d().y();
    }

    public long generateEventId() {
        return this.f2353a.e().f();
    }

    public void beginAdUnitExposure(String str) {
        com.google.android.gms.measurement.internal.a n = this.f2353a.n();
        long b2 = this.f2353a.l().b();
        if (str == null || str.length() == 0) {
            n.q().c.a("Ad unit id must be a non-empty string");
        } else {
            n.p().a(new v(n, str, b2));
        }
    }

    public void endAdUnitExposure(String str) {
        com.google.android.gms.measurement.internal.a n = this.f2353a.n();
        long b2 = this.f2353a.l().b();
        if (str == null || str.length() == 0) {
            n.q().c.a("Ad unit id must be a non-empty string");
        } else {
            n.p().a(new au(n, str, b2));
        }
    }

    public void setConditionalUserProperty(ConditionalUserProperty conditionalUserProperty) {
        bv d = this.f2353a.d();
        l.a(conditionalUserProperty);
        ConditionalUserProperty conditionalUserProperty2 = new ConditionalUserProperty(conditionalUserProperty);
        if (!TextUtils.isEmpty(conditionalUserProperty2.mAppId)) {
            d.q().f.a("Package name should be null when calling setConditionalUserProperty");
        }
        conditionalUserProperty2.mAppId = null;
        d.a(conditionalUserProperty2);
    }

    /* access modifiers changed from: protected */
    public void setConditionalUserPropertyAs(ConditionalUserProperty conditionalUserProperty) {
        bv d = this.f2353a.d();
        l.a(conditionalUserProperty);
        l.a(conditionalUserProperty.mAppId);
        d.a();
        d.a(new ConditionalUserProperty(conditionalUserProperty));
    }

    public void clearConditionalUserProperty(String str, String str2, Bundle bundle) {
        this.f2353a.d().a((String) null, str, str2, bundle);
    }

    /* access modifiers changed from: protected */
    public void clearConditionalUserPropertyAs(String str, String str2, String str3, Bundle bundle) {
        bv d = this.f2353a.d();
        l.a(str);
        d.a();
        d.a(str, str2, str3, bundle);
    }

    /* access modifiers changed from: protected */
    public Map<String, Object> getUserProperties(String str, String str2, boolean z) {
        return this.f2353a.d().a((String) null, str, str2, z);
    }

    /* access modifiers changed from: protected */
    public Map<String, Object> getUserPropertiesAs(String str, String str2, String str3, boolean z) {
        bv d = this.f2353a.d();
        l.a(str);
        d.a();
        return d.a(str, str2, str3, z);
    }

    public List<ConditionalUserProperty> getConditionalUserProperties(String str, String str2) {
        return this.f2353a.d().a((String) null, str, str2);
    }

    /* access modifiers changed from: protected */
    public List<ConditionalUserProperty> getConditionalUserPropertiesAs(String str, String str2, String str3) {
        bv d = this.f2353a.d();
        l.a(str);
        d.a();
        return d.a(str, str2, str3);
    }

    public int getMaxUserProperties(String str) {
        this.f2353a.d();
        l.a(str);
        return 25;
    }
}
