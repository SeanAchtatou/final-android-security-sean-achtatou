package com.google.android.gms.tagmanager;

import java.util.Arrays;

final class o {

    /* renamed from: a  reason: collision with root package name */
    final String f2667a;

    /* renamed from: b  reason: collision with root package name */
    final byte[] f2668b;

    o(String str, byte[] bArr) {
        this.f2667a = str;
        this.f2668b = bArr;
    }

    public final String toString() {
        String str = this.f2667a;
        int hashCode = Arrays.hashCode(this.f2668b);
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 54);
        sb.append("KeyAndSerialized: key = ");
        sb.append(str);
        sb.append(" serialized hash = ");
        sb.append(hashCode);
        return sb.toString();
    }
}
