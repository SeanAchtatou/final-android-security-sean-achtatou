package com.google.android.gms.common;

import android.os.RemoteException;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.internal.zzi;
import com.google.android.gms.common.internal.zzj;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

abstract class m extends zzj {

    /* renamed from: a  reason: collision with root package name */
    private int f1630a;

    protected m(byte[] bArr) {
        l.b(bArr.length == 25);
        this.f1630a = Arrays.hashCode(bArr);
    }

    /* access modifiers changed from: package-private */
    public abstract byte[] c();

    public int hashCode() {
        return this.f1630a;
    }

    public boolean equals(Object obj) {
        IObjectWrapper a2;
        if (obj != null && (obj instanceof zzi)) {
            try {
                zzi zzi = (zzi) obj;
                if (zzi.b() == hashCode() && (a2 = zzi.a()) != null) {
                    return Arrays.equals(c(), (byte[]) ObjectWrapper.a(a2));
                }
                return false;
            } catch (RemoteException unused) {
            }
        }
        return false;
    }

    public final IObjectWrapper a() {
        return ObjectWrapper.a(c());
    }

    public final int b() {
        return hashCode();
    }

    protected static byte[] a(String str) {
        try {
            return str.getBytes("ISO-8859-1");
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }
}
