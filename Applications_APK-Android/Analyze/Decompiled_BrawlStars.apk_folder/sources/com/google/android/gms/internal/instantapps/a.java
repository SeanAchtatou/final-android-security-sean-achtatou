package com.google.android.gms.internal.instantapps;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.d;
import com.google.android.gms.common.internal.e;
import com.google.android.gms.instantapps.c;
import javax.annotation.Nullable;

public final class a extends e<zzt> {
    public final int g() {
        return 12200000;
    }

    public final String i() {
        return "com.google.android.gms.instantapps.START";
    }

    public final String j() {
        return "com.google.android.gms.instantapps.internal.IInstantAppsService";
    }

    public final Feature[] m() {
        return new Feature[]{c.f1862a};
    }

    @Nullable
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.instantapps.internal.IInstantAppsService");
        if (queryLocalInterface instanceof zzt) {
            return (zzt) queryLocalInterface;
        }
        return new zzu(iBinder);
    }

    public a(Context context, Looper looper, d.b bVar, d.c cVar) {
        super(context, looper, 121, new d.a(context).a(), bVar, cVar);
    }
}
