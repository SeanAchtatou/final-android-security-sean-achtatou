package com.google.android.gms.common.server.response;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.server.response.FastJsonResponse;
import com.google.android.gms.common.util.c;
import com.google.android.gms.common.util.m;
import com.google.android.gms.common.util.n;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SafeParcelResponse extends FastSafeParcelableJsonResponse {
    public static final Parcelable.Creator<SafeParcelResponse> CREATOR = new m();

    /* renamed from: a  reason: collision with root package name */
    private final int f1647a;

    /* renamed from: b  reason: collision with root package name */
    private final Parcel f1648b;
    private final int c = 2;
    private final zak d;
    private final String e;
    private int f;
    private int g;

    SafeParcelResponse(int i, Parcel parcel, zak zak) {
        this.f1647a = i;
        this.f1648b = (Parcel) l.a(parcel);
        this.d = zak;
        zak zak2 = this.d;
        if (zak2 == null) {
            this.e = null;
        } else {
            this.e = zak2.f1649a;
        }
        this.f = 2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0005, code lost:
        if (r0 != 1) goto L_0x001c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final android.os.Parcel d() {
        /*
            r2 = this;
            int r0 = r2.f
            if (r0 == 0) goto L_0x0008
            r1 = 1
            if (r0 == r1) goto L_0x0012
            goto L_0x001c
        L_0x0008:
            android.os.Parcel r0 = r2.f1648b
            r1 = 20293(0x4f45, float:2.8437E-41)
            int r0 = com.google.android.gms.common.internal.safeparcel.a.a(r0, r1)
            r2.g = r0
        L_0x0012:
            android.os.Parcel r0 = r2.f1648b
            int r1 = r2.g
            com.google.android.gms.common.internal.safeparcel.a.b(r0, r1)
            r0 = 2
            r2.f = r0
        L_0x001c:
            android.os.Parcel r0 = r2.f1648b
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.server.response.SafeParcelResponse.d():android.os.Parcel");
    }

    public final Map<String, FastJsonResponse.Field<?, ?>> a() {
        zak zak = this.d;
        if (zak == null) {
            return null;
        }
        return zak.a(this.e);
    }

    public final Object b() {
        throw new UnsupportedOperationException("Converting to JSON does not require this method.");
    }

    public final boolean c() {
        throw new UnsupportedOperationException("Converting to JSON does not require this method.");
    }

    public String toString() {
        l.a(this.d, "Cannot convert to JSON on client side.");
        Parcel d2 = d();
        d2.setDataPosition(0);
        StringBuilder sb = new StringBuilder(100);
        a(sb, this.d.a(this.e), d2);
        return sb.toString();
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v1, resolved type: float[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v2, resolved type: float[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v3, resolved type: long[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v4, resolved type: float[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v6, resolved type: float[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v7, resolved type: double[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v14, resolved type: float[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v15, resolved type: float[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v16, resolved type: float[]} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.util.b.a(java.lang.StringBuilder, double[]):void
     arg types: [java.lang.StringBuilder, float[]]
     candidates:
      com.google.android.gms.common.util.b.a(java.lang.StringBuilder, float[]):void
      com.google.android.gms.common.util.b.a(java.lang.StringBuilder, int[]):void
      com.google.android.gms.common.util.b.a(java.lang.StringBuilder, long[]):void
      com.google.android.gms.common.util.b.a(java.lang.StringBuilder, java.lang.Object[]):void
      com.google.android.gms.common.util.b.a(java.lang.StringBuilder, java.lang.String[]):void
      com.google.android.gms.common.util.b.a(java.lang.StringBuilder, boolean[]):void
      com.google.android.gms.common.util.b.a(java.lang.Object[], java.lang.Object):boolean
      com.google.android.gms.common.util.b.a(java.lang.Object[], java.lang.Object[]):T[]
      com.google.android.gms.common.util.b.a(java.lang.StringBuilder, double[]):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.util.b.a(java.lang.StringBuilder, long[]):void
     arg types: [java.lang.StringBuilder, float[]]
     candidates:
      com.google.android.gms.common.util.b.a(java.lang.StringBuilder, double[]):void
      com.google.android.gms.common.util.b.a(java.lang.StringBuilder, float[]):void
      com.google.android.gms.common.util.b.a(java.lang.StringBuilder, int[]):void
      com.google.android.gms.common.util.b.a(java.lang.StringBuilder, java.lang.Object[]):void
      com.google.android.gms.common.util.b.a(java.lang.StringBuilder, java.lang.String[]):void
      com.google.android.gms.common.util.b.a(java.lang.StringBuilder, boolean[]):void
      com.google.android.gms.common.util.b.a(java.lang.Object[], java.lang.Object):boolean
      com.google.android.gms.common.util.b.a(java.lang.Object[], java.lang.Object[]):T[]
      com.google.android.gms.common.util.b.a(java.lang.StringBuilder, long[]):void */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void a(java.lang.StringBuilder r11, java.util.Map<java.lang.String, com.google.android.gms.common.server.response.FastJsonResponse.Field<?, ?>> r12, android.os.Parcel r13) {
        /*
            r10 = this;
            android.util.SparseArray r0 = new android.util.SparseArray
            r0.<init>()
            java.util.Set r12 = r12.entrySet()
            java.util.Iterator r12 = r12.iterator()
        L_0x000d:
            boolean r1 = r12.hasNext()
            if (r1 == 0) goto L_0x0027
            java.lang.Object r1 = r12.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            java.lang.Object r2 = r1.getValue()
            com.google.android.gms.common.server.response.FastJsonResponse$Field r2 = (com.google.android.gms.common.server.response.FastJsonResponse.Field) r2
            int r2 = r2.a()
            r0.put(r2, r1)
            goto L_0x000d
        L_0x0027:
            r12 = 123(0x7b, float:1.72E-43)
            r11.append(r12)
            int r12 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.a(r13)
            r1 = 1
            r2 = 0
            r3 = 0
        L_0x0033:
            int r4 = r13.dataPosition()
            if (r4 >= r12) goto L_0x02dc
            int r4 = r13.readInt()
            r5 = 65535(0xffff, float:9.1834E-41)
            r5 = r5 & r4
            java.lang.Object r5 = r0.get(r5)
            java.util.Map$Entry r5 = (java.util.Map.Entry) r5
            if (r5 == 0) goto L_0x0033
            java.lang.String r6 = ","
            if (r3 == 0) goto L_0x0050
            r11.append(r6)
        L_0x0050:
            java.lang.Object r3 = r5.getKey()
            java.lang.String r3 = (java.lang.String) r3
            java.lang.Object r5 = r5.getValue()
            com.google.android.gms.common.server.response.FastJsonResponse$Field r5 = (com.google.android.gms.common.server.response.FastJsonResponse.Field) r5
            java.lang.String r7 = "\""
            r11.append(r7)
            r11.append(r3)
            java.lang.String r3 = "\":"
            r11.append(r3)
            com.google.android.gms.common.server.response.FastJsonResponse$a<I, O> r3 = r5.i
            if (r3 == 0) goto L_0x006f
            r3 = 1
            goto L_0x0070
        L_0x006f:
            r3 = 0
        L_0x0070:
            if (r3 == 0) goto L_0x0151
            int r3 = r5.c
            switch(r3) {
                case 0: goto L_0x0140;
                case 1: goto L_0x0133;
                case 2: goto L_0x0122;
                case 3: goto L_0x0111;
                case 4: goto L_0x0100;
                case 5: goto L_0x00f3;
                case 6: goto L_0x00e2;
                case 7: goto L_0x00d5;
                case 8: goto L_0x00c8;
                case 9: goto L_0x00c8;
                case 10: goto L_0x009a;
                case 11: goto L_0x0092;
                default: goto L_0x0077;
            }
        L_0x0077:
            java.lang.IllegalArgumentException r11 = new java.lang.IllegalArgumentException
            int r12 = r5.c
            r13 = 36
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>(r13)
            java.lang.String r13 = "Unknown field out type = "
            r0.append(r13)
            r0.append(r12)
            java.lang.String r12 = r0.toString()
            r11.<init>(r12)
            throw r11
        L_0x0092:
            java.lang.IllegalArgumentException r11 = new java.lang.IllegalArgumentException
            java.lang.String r12 = "Method does not accept concrete type."
            r11.<init>(r12)
            throw r11
        L_0x009a:
            android.os.Bundle r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.n(r13, r4)
            java.util.HashMap r4 = new java.util.HashMap
            r4.<init>()
            java.util.Set r6 = r3.keySet()
            java.util.Iterator r6 = r6.iterator()
        L_0x00ab:
            boolean r7 = r6.hasNext()
            if (r7 == 0) goto L_0x00bf
            java.lang.Object r7 = r6.next()
            java.lang.String r7 = (java.lang.String) r7
            java.lang.String r8 = r3.getString(r7)
            r4.put(r7, r8)
            goto L_0x00ab
        L_0x00bf:
            java.lang.Object r3 = a(r5, r4)
            a(r11, r5, r3)
            goto L_0x02d9
        L_0x00c8:
            byte[] r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.o(r13, r4)
            java.lang.Object r3 = a(r5, r3)
            a(r11, r5, r3)
            goto L_0x02d9
        L_0x00d5:
            java.lang.String r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.l(r13, r4)
            java.lang.Object r3 = a(r5, r3)
            a(r11, r5, r3)
            goto L_0x02d9
        L_0x00e2:
            boolean r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.c(r13, r4)
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r3)
            java.lang.Object r3 = a(r5, r3)
            a(r11, r5, r3)
            goto L_0x02d9
        L_0x00f3:
            java.math.BigDecimal r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.k(r13, r4)
            java.lang.Object r3 = a(r5, r3)
            a(r11, r5, r3)
            goto L_0x02d9
        L_0x0100:
            double r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.j(r13, r4)
            java.lang.Double r3 = java.lang.Double.valueOf(r3)
            java.lang.Object r3 = a(r5, r3)
            a(r11, r5, r3)
            goto L_0x02d9
        L_0x0111:
            float r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.i(r13, r4)
            java.lang.Float r3 = java.lang.Float.valueOf(r3)
            java.lang.Object r3 = a(r5, r3)
            a(r11, r5, r3)
            goto L_0x02d9
        L_0x0122:
            long r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.f(r13, r4)
            java.lang.Long r3 = java.lang.Long.valueOf(r3)
            java.lang.Object r3 = a(r5, r3)
            a(r11, r5, r3)
            goto L_0x02d9
        L_0x0133:
            java.math.BigInteger r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.h(r13, r4)
            java.lang.Object r3 = a(r5, r3)
            a(r11, r5, r3)
            goto L_0x02d9
        L_0x0140:
            int r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.d(r13, r4)
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            java.lang.Object r3 = a(r5, r3)
            a(r11, r5, r3)
            goto L_0x02d9
        L_0x0151:
            boolean r3 = r5.d
            if (r3 == 0) goto L_0x0201
            java.lang.String r3 = "["
            r11.append(r3)
            int r3 = r5.c
            r7 = 0
            switch(r3) {
                case 0: goto L_0x01f3;
                case 1: goto L_0x01eb;
                case 2: goto L_0x01d4;
                case 3: goto L_0x01bd;
                case 4: goto L_0x01a6;
                case 5: goto L_0x019e;
                case 6: goto L_0x0196;
                case 7: goto L_0x018e;
                case 8: goto L_0x0186;
                case 9: goto L_0x0186;
                case 10: goto L_0x0186;
                case 11: goto L_0x0168;
                default: goto L_0x0160;
            }
        L_0x0160:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "Unknown field type out."
            r11.<init>(r12)
            throw r11
        L_0x0168:
            android.os.Parcel[] r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.w(r13, r4)
            int r4 = r3.length
            r7 = 0
        L_0x016e:
            if (r7 >= r4) goto L_0x01fa
            if (r7 <= 0) goto L_0x0175
            r11.append(r6)
        L_0x0175:
            r8 = r3[r7]
            r8.setDataPosition(r2)
            java.util.Map r8 = r5.b()
            r9 = r3[r7]
            r10.a(r11, r8, r9)
            int r7 = r7 + 1
            goto L_0x016e
        L_0x0186:
            java.lang.UnsupportedOperationException r11 = new java.lang.UnsupportedOperationException
            java.lang.String r12 = "List of type BASE64, BASE64_URL_SAFE, or STRING_MAP is not supported"
            r11.<init>(r12)
            throw r11
        L_0x018e:
            java.lang.String[] r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.t(r13, r4)
            com.google.android.gms.common.util.b.a(r11, r3)
            goto L_0x01fa
        L_0x0196:
            boolean[] r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.p(r13, r4)
            com.google.android.gms.common.util.b.a(r11, r3)
            goto L_0x01fa
        L_0x019e:
            java.math.BigDecimal[] r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.s(r13, r4)
            com.google.android.gms.common.util.b.a(r11, r3)
            goto L_0x01fa
        L_0x01a6:
            int r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.a(r13, r4)
            int r4 = r13.dataPosition()
            if (r3 != 0) goto L_0x01b1
            goto L_0x01b9
        L_0x01b1:
            double[] r7 = r13.createDoubleArray()
            int r4 = r4 + r3
            r13.setDataPosition(r4)
        L_0x01b9:
            com.google.android.gms.common.util.b.a(r11, r7)
            goto L_0x01fa
        L_0x01bd:
            int r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.a(r13, r4)
            int r4 = r13.dataPosition()
            if (r3 != 0) goto L_0x01c8
            goto L_0x01d0
        L_0x01c8:
            float[] r7 = r13.createFloatArray()
            int r4 = r4 + r3
            r13.setDataPosition(r4)
        L_0x01d0:
            com.google.android.gms.common.util.b.a(r11, r7)
            goto L_0x01fa
        L_0x01d4:
            int r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.a(r13, r4)
            int r4 = r13.dataPosition()
            if (r3 != 0) goto L_0x01df
            goto L_0x01e7
        L_0x01df:
            long[] r7 = r13.createLongArray()
            int r4 = r4 + r3
            r13.setDataPosition(r4)
        L_0x01e7:
            com.google.android.gms.common.util.b.a(r11, r7)
            goto L_0x01fa
        L_0x01eb:
            java.math.BigInteger[] r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.r(r13, r4)
            com.google.android.gms.common.util.b.a(r11, r3)
            goto L_0x01fa
        L_0x01f3:
            int[] r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.q(r13, r4)
            com.google.android.gms.common.util.b.a(r11, r3)
        L_0x01fa:
            java.lang.String r3 = "]"
            r11.append(r3)
            goto L_0x02d9
        L_0x0201:
            int r3 = r5.c
            switch(r3) {
                case 0: goto L_0x02d2;
                case 1: goto L_0x02ca;
                case 2: goto L_0x02c2;
                case 3: goto L_0x02ba;
                case 4: goto L_0x02b2;
                case 5: goto L_0x02aa;
                case 6: goto L_0x02a2;
                case 7: goto L_0x0290;
                case 8: goto L_0x027e;
                case 9: goto L_0x026c;
                case 10: goto L_0x021e;
                case 11: goto L_0x020e;
                default: goto L_0x0206;
            }
        L_0x0206:
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "Unknown field type out"
            r11.<init>(r12)
            throw r11
        L_0x020e:
            android.os.Parcel r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.v(r13, r4)
            r3.setDataPosition(r2)
            java.util.Map r4 = r5.b()
            r10.a(r11, r4, r3)
            goto L_0x02d9
        L_0x021e:
            android.os.Bundle r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.n(r13, r4)
            java.util.Set r4 = r3.keySet()
            r4.size()
            java.lang.String r5 = "{"
            r11.append(r5)
            java.util.Iterator r4 = r4.iterator()
            r5 = 1
        L_0x0233:
            boolean r8 = r4.hasNext()
            if (r8 == 0) goto L_0x0265
            java.lang.Object r8 = r4.next()
            java.lang.String r8 = (java.lang.String) r8
            if (r5 != 0) goto L_0x0244
            r11.append(r6)
        L_0x0244:
            r11.append(r7)
            r11.append(r8)
            r11.append(r7)
            java.lang.String r5 = ":"
            r11.append(r5)
            r11.append(r7)
            java.lang.String r5 = r3.getString(r8)
            java.lang.String r5 = com.google.android.gms.common.util.m.a(r5)
            r11.append(r5)
            r11.append(r7)
            r5 = 0
            goto L_0x0233
        L_0x0265:
            java.lang.String r3 = "}"
            r11.append(r3)
            goto L_0x02d9
        L_0x026c:
            byte[] r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.o(r13, r4)
            r11.append(r7)
            java.lang.String r3 = com.google.android.gms.common.util.c.b(r3)
            r11.append(r3)
            r11.append(r7)
            goto L_0x02d9
        L_0x027e:
            byte[] r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.o(r13, r4)
            r11.append(r7)
            java.lang.String r3 = com.google.android.gms.common.util.c.a(r3)
            r11.append(r3)
            r11.append(r7)
            goto L_0x02d9
        L_0x0290:
            java.lang.String r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.l(r13, r4)
            r11.append(r7)
            java.lang.String r3 = com.google.android.gms.common.util.m.a(r3)
            r11.append(r3)
            r11.append(r7)
            goto L_0x02d9
        L_0x02a2:
            boolean r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.c(r13, r4)
            r11.append(r3)
            goto L_0x02d9
        L_0x02aa:
            java.math.BigDecimal r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.k(r13, r4)
            r11.append(r3)
            goto L_0x02d9
        L_0x02b2:
            double r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.j(r13, r4)
            r11.append(r3)
            goto L_0x02d9
        L_0x02ba:
            float r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.i(r13, r4)
            r11.append(r3)
            goto L_0x02d9
        L_0x02c2:
            long r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.f(r13, r4)
            r11.append(r3)
            goto L_0x02d9
        L_0x02ca:
            java.math.BigInteger r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.h(r13, r4)
            r11.append(r3)
            goto L_0x02d9
        L_0x02d2:
            int r3 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.d(r13, r4)
            r11.append(r3)
        L_0x02d9:
            r3 = 1
            goto L_0x0033
        L_0x02dc:
            int r0 = r13.dataPosition()
            if (r0 != r12) goto L_0x02e8
            r12 = 125(0x7d, float:1.75E-43)
            r11.append(r12)
            return
        L_0x02e8:
            com.google.android.gms.common.internal.safeparcel.SafeParcelReader$ParseException r11 = new com.google.android.gms.common.internal.safeparcel.SafeParcelReader$ParseException
            r0 = 37
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r0 = "Overread allowed size end="
            r1.append(r0)
            r1.append(r12)
            java.lang.String r12 = r1.toString()
            r11.<init>(r12, r13)
            goto L_0x0302
        L_0x0301:
            throw r11
        L_0x0302:
            goto L_0x0301
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.server.response.SafeParcelResponse.a(java.lang.StringBuilder, java.util.Map, android.os.Parcel):void");
    }

    private static void a(StringBuilder sb, FastJsonResponse.Field<?, ?> field, Object obj) {
        if (field.f1644b) {
            ArrayList arrayList = (ArrayList) obj;
            sb.append("[");
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                if (i != 0) {
                    sb.append(",");
                }
                a(sb, field.f1643a, arrayList.get(i));
            }
            sb.append("]");
            return;
        }
        a(sb, field.f1643a, obj);
    }

    private static void a(StringBuilder sb, int i, Object obj) {
        switch (i) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                sb.append(obj);
                return;
            case 7:
                sb.append("\"");
                sb.append(m.a(obj.toString()));
                sb.append("\"");
                return;
            case 8:
                sb.append("\"");
                sb.append(c.a((byte[]) obj));
                sb.append("\"");
                return;
            case 9:
                sb.append("\"");
                sb.append(c.b((byte[]) obj));
                sb.append("\"");
                return;
            case 10:
                n.a(sb, (HashMap) obj);
                return;
            case 11:
                throw new IllegalArgumentException("Method does not accept concrete type.");
            default:
                StringBuilder sb2 = new StringBuilder(26);
                sb2.append("Unknown type = ");
                sb2.append(i);
                throw new IllegalArgumentException(sb2.toString());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.common.server.response.zak, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public void writeToParcel(Parcel parcel, int i) {
        zak zak;
        int a2 = a.a(parcel, 20293);
        a.b(parcel, 1, this.f1647a);
        Parcel d2 = d();
        if (d2 != null) {
            int a3 = a.a(parcel, 2);
            parcel.appendFrom(d2, 0, d2.dataSize());
            a.b(parcel, a3);
        }
        int i2 = this.c;
        if (i2 == 0) {
            zak = null;
        } else if (i2 == 1) {
            zak = this.d;
        } else if (i2 == 2) {
            zak = this.d;
        } else {
            StringBuilder sb = new StringBuilder(34);
            sb.append("Invalid creation type: ");
            sb.append(i2);
            throw new IllegalStateException(sb.toString());
        }
        a.a(parcel, 3, (Parcelable) zak, i, false);
        a.b(parcel, a2);
    }
}
