package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.IInterface;
import com.google.android.gms.common.api.a;

public class n<T extends IInterface> extends e<T> {
    public final a.h<T> i;

    /* access modifiers changed from: protected */
    public final String i() {
        return this.i.a();
    }

    /* access modifiers changed from: protected */
    public final String j() {
        return this.i.b();
    }

    /* access modifiers changed from: protected */
    public final T a(IBinder iBinder) {
        return this.i.c();
    }

    public final int g() {
        return super.g();
    }
}
