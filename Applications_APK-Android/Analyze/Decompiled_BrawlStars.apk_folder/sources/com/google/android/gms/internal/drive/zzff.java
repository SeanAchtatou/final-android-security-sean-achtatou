package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.drive.zzh;
import java.util.Collections;
import java.util.List;

public final class zzff extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzff> CREATOR = new v();
    private static final List<zzh> c = Collections.emptyList();

    /* renamed from: a  reason: collision with root package name */
    final long f1940a;

    /* renamed from: b  reason: collision with root package name */
    final long f1941b;
    private final int d;
    private final List<zzh> e;

    public zzff(long j, long j2, int i, List<zzh> list) {
        this.f1940a = j;
        this.f1941b = j2;
        this.d = i;
        this.e = list;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.a(parcel, 2, this.f1940a);
        a.a(parcel, 3, this.f1941b);
        a.b(parcel, 4, this.d);
        a.b(parcel, 5, this.e, false);
        a.b(parcel, a2);
    }
}
