package com.google.android.gms.tasks;

final class t implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ g f2700a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ s f2701b;

    t(s sVar, g gVar) {
        this.f2701b = sVar;
        this.f2700a = gVar;
    }

    public final void run() {
        synchronized (this.f2701b.f2698a) {
            if (this.f2701b.f2699b != null) {
                this.f2701b.f2699b.a(this.f2700a.e());
            }
        }
    }
}
