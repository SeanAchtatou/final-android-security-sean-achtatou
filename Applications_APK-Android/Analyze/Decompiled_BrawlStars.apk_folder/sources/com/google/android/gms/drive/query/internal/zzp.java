package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.drive.metadata.g;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import java.util.Collection;

public final class zzp<T> extends zza {
    public static final j CREATOR = new j();

    /* renamed from: a  reason: collision with root package name */
    private final MetadataBundle f1760a;

    /* renamed from: b  reason: collision with root package name */
    private final g<T> f1761b;

    zzp(MetadataBundle metadataBundle) {
        this.f1760a = metadataBundle;
        this.f1761b = (g) f.a(metadataBundle);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.drive.query.internal.g.a(com.google.android.gms.drive.metadata.g, java.lang.Object):F
     arg types: [com.google.android.gms.drive.metadata.g<T>, java.lang.Object]
     candidates:
      com.google.android.gms.drive.query.internal.g.a(com.google.android.gms.drive.metadata.a, java.lang.Object):F
      com.google.android.gms.drive.query.internal.g.a(com.google.android.gms.drive.query.internal.zzx, java.util.List):F
      com.google.android.gms.drive.query.internal.g.a(com.google.android.gms.drive.metadata.g, java.lang.Object):F */
    public final <F> F a(g<F> gVar) {
        g<T> gVar2 = this.f1761b;
        return gVar.a((g) gVar2, ((Collection) this.f1760a.a(gVar2)).iterator().next());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.metadata.internal.MetadataBundle, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.a(parcel, 1, (Parcelable) this.f1760a, i, false);
        a.b(parcel, a2);
    }
}
