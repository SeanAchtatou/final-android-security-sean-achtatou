package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.l;

public final class au implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ String f2393a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ long f2394b;
    private final /* synthetic */ a c;

    public au(a aVar, String str, long j) {
        this.c = aVar;
        this.f2393a = str;
        this.f2394b = j;
    }

    public final void run() {
        a aVar = this.c;
        String str = this.f2393a;
        long j = this.f2394b;
        aVar.c();
        l.a(str);
        Integer num = aVar.f2359b.get(str);
        if (num != null) {
            cg v = aVar.h().v();
            int intValue = num.intValue() - 1;
            if (intValue == 0) {
                aVar.f2359b.remove(str);
                Long l = aVar.f2358a.get(str);
                if (l == null) {
                    aVar.q().c.a("First ad unit exposure time was never set");
                } else {
                    aVar.f2358a.remove(str);
                    aVar.a(str, j - l.longValue(), v);
                }
                if (!aVar.f2359b.isEmpty()) {
                    return;
                }
                if (aVar.c == 0) {
                    aVar.q().c.a("First ad exposure time was never set");
                    return;
                }
                aVar.a(j - aVar.c, v);
                aVar.c = 0;
                return;
            }
            aVar.f2359b.put(str, Integer.valueOf(intValue));
            return;
        }
        aVar.q().c.a("Call to endAdUnitExposure for unknown ad unit id", str);
    }
}
