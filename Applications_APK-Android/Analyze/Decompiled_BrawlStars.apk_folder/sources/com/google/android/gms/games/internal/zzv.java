package com.google.android.gms.games.internal;

import com.google.android.gms.internal.games.zzb;

public abstract class zzv extends zzb implements zzu {
    public zzv() {
        super("com.google.android.gms.games.internal.IGamesCallbacks");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.games.i.a(android.os.Parcel, android.os.Parcelable$Creator):T
     arg types: [android.os.Parcel, java.lang.Object]
     candidates:
      com.google.android.gms.internal.games.i.a(android.os.Parcel, android.os.IInterface):void
      com.google.android.gms.internal.games.i.a(android.os.Parcel, android.os.Parcelable):void
      com.google.android.gms.internal.games.i.a(android.os.Parcel, android.os.Parcelable$Creator):T */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0077 A[FALL_THROUGH] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(int r2, android.os.Parcel r3, android.os.Parcel r4) throws android.os.RemoteException {
        /*
            r1 = this;
            r0 = 6001(0x1771, float:8.409E-42)
            if (r2 == r0) goto L_0x0077
            r0 = 6002(0x1772, float:8.41E-42)
            if (r2 == r0) goto L_0x0077
            r0 = 13001(0x32c9, float:1.8218E-41)
            if (r2 == r0) goto L_0x0091
            r0 = 13002(0x32ca, float:1.822E-41)
            if (r2 == r0) goto L_0x0056
            r0 = 19001(0x4a39, float:2.6626E-41)
            if (r2 == r0) goto L_0x00a7
            r0 = 19002(0x4a3a, float:2.6627E-41)
            if (r2 == r0) goto L_0x00a3
            switch(r2) {
                case 5001: goto L_0x0097;
                case 5002: goto L_0x0091;
                case 5003: goto L_0x0085;
                case 5004: goto L_0x0091;
                case 5005: goto L_0x007f;
                case 5006: goto L_0x0091;
                case 5007: goto L_0x0091;
                case 5008: goto L_0x0091;
                case 5009: goto L_0x0091;
                case 5010: goto L_0x0091;
                case 5011: goto L_0x0091;
                default: goto L_0x001b;
            }
        L_0x001b:
            switch(r2) {
                case 5016: goto L_0x007b;
                case 5017: goto L_0x0091;
                case 5018: goto L_0x0091;
                case 5019: goto L_0x0091;
                case 5020: goto L_0x0074;
                case 5021: goto L_0x0091;
                case 5022: goto L_0x0091;
                case 5023: goto L_0x0091;
                case 5024: goto L_0x0091;
                case 5025: goto L_0x0091;
                case 5026: goto L_0x006b;
                case 5027: goto L_0x006b;
                case 5028: goto L_0x006b;
                case 5029: goto L_0x006b;
                case 5030: goto L_0x006b;
                case 5031: goto L_0x006b;
                case 5032: goto L_0x0068;
                case 5033: goto L_0x0064;
                case 5034: goto L_0x005a;
                case 5035: goto L_0x0091;
                case 5036: goto L_0x0056;
                case 5037: goto L_0x0091;
                case 5038: goto L_0x0091;
                case 5039: goto L_0x0091;
                case 5040: goto L_0x0056;
                case 9001: goto L_0x0091;
                case 11001: goto L_0x0047;
                case 12001: goto L_0x0091;
                case 14001: goto L_0x0050;
                case 15001: goto L_0x0091;
                case 17002: goto L_0x0056;
                case 20001: goto L_0x0091;
                case 20002: goto L_0x0091;
                case 20003: goto L_0x0091;
                case 20004: goto L_0x0091;
                case 20005: goto L_0x0091;
                case 20006: goto L_0x0091;
                case 20007: goto L_0x0091;
                case 20008: goto L_0x0091;
                case 20009: goto L_0x0091;
                case 20012: goto L_0x004d;
                case 20019: goto L_0x0056;
                case 20020: goto L_0x0047;
                default: goto L_0x001e;
            }
        L_0x001e:
            switch(r2) {
                case 8001: goto L_0x0091;
                case 8002: goto L_0x0047;
                case 8003: goto L_0x0091;
                case 8004: goto L_0x0091;
                case 8005: goto L_0x0091;
                case 8006: goto L_0x0091;
                case 8007: goto L_0x0074;
                case 8008: goto L_0x0091;
                case 8009: goto L_0x0077;
                case 8010: goto L_0x0077;
                default: goto L_0x0021;
            }
        L_0x0021:
            switch(r2) {
                case 10001: goto L_0x0091;
                case 10002: goto L_0x0077;
                case 10003: goto L_0x0091;
                case 10004: goto L_0x0091;
                case 10005: goto L_0x0047;
                case 10006: goto L_0x0091;
                default: goto L_0x0024;
            }
        L_0x0024:
            switch(r2) {
                case 12003: goto L_0x0047;
                case 12004: goto L_0x003f;
                case 12005: goto L_0x0091;
                case 12006: goto L_0x0091;
                case 12007: goto L_0x0091;
                case 12008: goto L_0x0091;
                default: goto L_0x0027;
            }
        L_0x0027:
            switch(r2) {
                case 12011: goto L_0x0091;
                case 12012: goto L_0x0074;
                case 12013: goto L_0x0091;
                case 12014: goto L_0x0091;
                case 12015: goto L_0x0047;
                case 12016: goto L_0x0091;
                case 12017: goto L_0x002f;
                default: goto L_0x002a;
            }
        L_0x002a:
            switch(r2) {
                case 19006: goto L_0x0091;
                case 19007: goto L_0x0047;
                case 19008: goto L_0x0056;
                case 19009: goto L_0x0056;
                case 19010: goto L_0x0056;
                default: goto L_0x002d;
            }
        L_0x002d:
            r2 = 0
            return r2
        L_0x002f:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r2 = com.google.android.gms.common.data.DataHolder.CREATOR
            com.google.android.gms.internal.games.i.a(r3, r2)
            r3.readString()
            android.os.Parcelable$Creator<com.google.android.gms.drive.Contents> r2 = com.google.android.gms.drive.Contents.CREATOR
            com.google.android.gms.internal.games.i.a(r3, r2)
            android.os.Parcelable$Creator<com.google.android.gms.drive.Contents> r2 = com.google.android.gms.drive.Contents.CREATOR
            goto L_0x0041
        L_0x003f:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r2 = com.google.android.gms.common.data.DataHolder.CREATOR
        L_0x0041:
            com.google.android.gms.internal.games.i.a(r3, r2)
            android.os.Parcelable$Creator<com.google.android.gms.drive.Contents> r2 = com.google.android.gms.drive.Contents.CREATOR
            goto L_0x0093
        L_0x0047:
            r3.readInt()
            android.os.Parcelable$Creator r2 = android.os.Bundle.CREATOR
            goto L_0x0093
        L_0x004d:
            android.os.Parcelable$Creator<com.google.android.gms.common.api.Status> r2 = com.google.android.gms.common.api.Status.CREATOR
            goto L_0x0093
        L_0x0050:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r2 = com.google.android.gms.common.data.DataHolder.CREATOR
            r3.createTypedArray(r2)
            goto L_0x00ad
        L_0x0056:
            r3.readInt()
            goto L_0x00ad
        L_0x005a:
            r3.readInt()
            r3.readString()
        L_0x0060:
            com.google.android.gms.internal.games.i.a(r3)
            goto L_0x00ad
        L_0x0064:
            r3.readInt()
            goto L_0x0074
        L_0x0068:
            android.os.Parcelable$Creator<com.google.android.gms.games.multiplayer.realtime.RealTimeMessage> r2 = com.google.android.gms.games.multiplayer.realtime.RealTimeMessage.CREATOR
            goto L_0x0093
        L_0x006b:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r2 = com.google.android.gms.common.data.DataHolder.CREATOR
            com.google.android.gms.internal.games.i.a(r3, r2)
            r3.createStringArray()
            goto L_0x00ad
        L_0x0074:
            r3.readInt()
        L_0x0077:
            r3.readString()
            goto L_0x00ad
        L_0x007b:
            r1.a()
            goto L_0x00ad
        L_0x007f:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r2 = com.google.android.gms.common.data.DataHolder.CREATOR
            com.google.android.gms.internal.games.i.a(r3, r2)
            goto L_0x0091
        L_0x0085:
            int r2 = r3.readInt()
            java.lang.String r3 = r3.readString()
            r1.b(r2, r3)
            goto L_0x00ad
        L_0x0091:
            android.os.Parcelable$Creator<com.google.android.gms.common.data.DataHolder> r2 = com.google.android.gms.common.data.DataHolder.CREATOR
        L_0x0093:
            com.google.android.gms.internal.games.i.a(r3, r2)
            goto L_0x00ad
        L_0x0097:
            int r2 = r3.readInt()
            java.lang.String r3 = r3.readString()
            r1.a(r2, r3)
            goto L_0x00ad
        L_0x00a3:
            r3.readInt()
            goto L_0x0060
        L_0x00a7:
            r3.readInt()
            android.os.Parcelable$Creator<com.google.android.gms.games.video.VideoCapabilities> r2 = com.google.android.gms.games.video.VideoCapabilities.CREATOR
            goto L_0x0093
        L_0x00ad:
            r4.writeNoException()
            r2 = 1
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.games.internal.zzv.a(int, android.os.Parcel, android.os.Parcel):boolean");
    }
}
