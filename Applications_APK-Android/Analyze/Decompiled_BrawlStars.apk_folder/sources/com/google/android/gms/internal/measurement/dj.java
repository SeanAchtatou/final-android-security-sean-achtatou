package com.google.android.gms.internal.measurement;

import android.database.ContentObserver;
import java.util.Iterator;

final class dj extends ContentObserver {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ dh f2164a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    dj(dh dhVar) {
        super(null);
        this.f2164a = dhVar;
    }

    public final void onChange(boolean z) {
        dh dhVar = this.f2164a;
        synchronized (dhVar.f2162b) {
            dhVar.c = null;
            ds.a();
        }
        synchronized (dhVar) {
            Iterator<Object> it = dhVar.d.iterator();
            while (it.hasNext()) {
                it.next();
            }
        }
    }
}
