package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.d;
import com.google.android.gms.common.internal.l;

public final class ca implements d.b, d.c {

    /* renamed from: a  reason: collision with root package name */
    public final a<?> f1449a;

    /* renamed from: b  reason: collision with root package name */
    cb f1450b;
    private final boolean c;

    public ca(a<?> aVar, boolean z) {
        this.f1449a = aVar;
        this.c = z;
    }

    public final void onConnected(Bundle bundle) {
        a();
        this.f1450b.onConnected(bundle);
    }

    public final void onConnectionSuspended(int i) {
        a();
        this.f1450b.onConnectionSuspended(i);
    }

    public final void onConnectionFailed(ConnectionResult connectionResult) {
        a();
        this.f1450b.a(connectionResult, this.f1449a, this.c);
    }

    private final void a() {
        l.a(this.f1450b, "Callbacks must be attached to a ClientConnectionHelper instance before connecting the client.");
    }
}
