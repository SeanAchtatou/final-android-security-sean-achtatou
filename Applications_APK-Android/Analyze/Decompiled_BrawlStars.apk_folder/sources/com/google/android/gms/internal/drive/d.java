package com.google.android.gms.internal.drive;

import com.google.android.gms.common.internal.h;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.drive.Contents;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private static final h f1911a = new h("DriveContentsImpl", "");

    /* renamed from: b  reason: collision with root package name */
    private final Contents f1912b;
    private boolean c = false;
    private boolean d = false;
    private boolean e = false;

    public d(Contents contents) {
        this.f1912b = (Contents) l.a(contents);
    }
}
