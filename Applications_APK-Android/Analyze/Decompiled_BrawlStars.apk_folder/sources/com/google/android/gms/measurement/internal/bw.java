package com.google.android.gms.measurement.internal;

import android.os.Bundle;

final class bw implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ String f2444a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f2445b;
    private final /* synthetic */ long c;
    private final /* synthetic */ Bundle d;
    private final /* synthetic */ boolean e;
    private final /* synthetic */ boolean f;
    private final /* synthetic */ boolean g;
    private final /* synthetic */ String h;
    private final /* synthetic */ bv i;

    bw(bv bvVar, String str, String str2, long j, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        this.i = bvVar;
        this.f2444a = str;
        this.f2445b = str2;
        this.c = j;
        this.d = bundle;
        this.e = z;
        this.f = z2;
        this.g = z3;
        this.h = str3;
    }

    public final void run() {
        this.i.a(this.f2444a, this.f2445b, this.c, this.d, this.e, this.f, this.g, this.h);
    }
}
