package com.google.android.gms.internal.measurement;

import android.support.v7.widget.ActivityChooserView;
import java.io.IOException;
import java.util.List;

final class ex implements hi {

    /* renamed from: a  reason: collision with root package name */
    private final ev f2192a;

    /* renamed from: b  reason: collision with root package name */
    private int f2193b;
    private int c;
    private int d = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.fs.a(java.lang.Object, java.lang.String):T
     arg types: [com.google.android.gms.internal.measurement.ev, java.lang.String]
     candidates:
      com.google.android.gms.internal.measurement.fs.a(java.lang.Object, java.lang.Object):java.lang.Object
      com.google.android.gms.internal.measurement.fs.a(java.lang.Object, java.lang.String):T */
    ex(ev evVar) {
        this.f2192a = (ev) fs.a((Object) evVar, "input");
        this.f2192a.c = this;
    }

    public final int a() throws IOException {
        int i = this.d;
        if (i != 0) {
            this.f2193b = i;
            this.d = 0;
        } else {
            this.f2193b = this.f2192a.a();
        }
        int i2 = this.f2193b;
        return (i2 == 0 || i2 == this.c) ? ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED : i2 >>> 3;
    }

    public final int b() {
        return this.f2193b;
    }

    public final boolean c() throws IOException {
        int i;
        if (this.f2192a.t() || (i = this.f2193b) == this.c) {
            return false;
        }
        return this.f2192a.b(i);
    }

    private final void a(int i) throws IOException {
        if ((this.f2193b & 7) != i) {
            throw zzuv.e();
        }
    }

    public final double d() throws IOException {
        a(1);
        return this.f2192a.b();
    }

    public final float e() throws IOException {
        a(5);
        return this.f2192a.c();
    }

    public final long f() throws IOException {
        a(0);
        return this.f2192a.d();
    }

    public final long g() throws IOException {
        a(0);
        return this.f2192a.e();
    }

    public final int h() throws IOException {
        a(0);
        return this.f2192a.f();
    }

    public final long i() throws IOException {
        a(1);
        return this.f2192a.g();
    }

    public final int j() throws IOException {
        a(5);
        return this.f2192a.h();
    }

    public final boolean k() throws IOException {
        a(0);
        return this.f2192a.i();
    }

    public final String l() throws IOException {
        a(2);
        return this.f2192a.j();
    }

    public final String m() throws IOException {
        a(2);
        return this.f2192a.k();
    }

    public final <T> T a(hj<T> hjVar, fd fdVar) throws IOException {
        a(2);
        return c(hjVar, fdVar);
    }

    public final <T> T b(hj<T> hjVar, fd fdVar) throws IOException {
        a(3);
        return d(hjVar, fdVar);
    }

    private final <T> T c(hj<T> hjVar, fd fdVar) throws IOException {
        int m = this.f2192a.m();
        if (this.f2192a.f2190a < this.f2192a.f2191b) {
            int c2 = this.f2192a.c(m);
            T a2 = hjVar.a();
            this.f2192a.f2190a++;
            hjVar.a(a2, this, fdVar);
            hjVar.c(a2);
            this.f2192a.a(0);
            ev evVar = this.f2192a;
            evVar.f2190a--;
            this.f2192a.d(c2);
            return a2;
        }
        throw zzuv.f();
    }

    private final <T> T d(hj<T> hjVar, fd fdVar) throws IOException {
        int i = this.c;
        this.c = ((this.f2193b >>> 3) << 3) | 4;
        try {
            T a2 = hjVar.a();
            hjVar.a(a2, this, fdVar);
            hjVar.c(a2);
            if (this.f2193b == this.c) {
                return a2;
            }
            throw zzuv.g();
        } finally {
            this.c = i;
        }
    }

    public final ej n() throws IOException {
        a(2);
        return this.f2192a.l();
    }

    public final int o() throws IOException {
        a(0);
        return this.f2192a.m();
    }

    public final int p() throws IOException {
        a(0);
        return this.f2192a.n();
    }

    public final int q() throws IOException {
        a(5);
        return this.f2192a.o();
    }

    public final long r() throws IOException {
        a(1);
        return this.f2192a.p();
    }

    public final int s() throws IOException {
        a(0);
        return this.f2192a.q();
    }

    public final long t() throws IOException {
        a(0);
        return this.f2192a.r();
    }

    public final void a(List<Double> list) throws IOException {
        int a2;
        int a3;
        if (list instanceof fa) {
            fa faVar = (fa) list;
            int i = this.f2193b & 7;
            if (i == 1) {
                do {
                    faVar.a(this.f2192a.b());
                    if (!this.f2192a.t()) {
                        a3 = this.f2192a.a();
                    } else {
                        return;
                    }
                } while (a3 == this.f2193b);
                this.d = a3;
            } else if (i == 2) {
                int m = this.f2192a.m();
                b(m);
                int u = this.f2192a.u() + m;
                do {
                    faVar.a(this.f2192a.b());
                } while (this.f2192a.u() < u);
            } else {
                throw zzuv.e();
            }
        } else {
            int i2 = this.f2193b & 7;
            if (i2 == 1) {
                do {
                    list.add(Double.valueOf(this.f2192a.b()));
                    if (!this.f2192a.t()) {
                        a2 = this.f2192a.a();
                    } else {
                        return;
                    }
                } while (a2 == this.f2193b);
                this.d = a2;
            } else if (i2 == 2) {
                int m2 = this.f2192a.m();
                b(m2);
                int u2 = this.f2192a.u() + m2;
                do {
                    list.add(Double.valueOf(this.f2192a.b()));
                } while (this.f2192a.u() < u2);
            } else {
                throw zzuv.e();
            }
        }
    }

    public final void b(List<Float> list) throws IOException {
        int a2;
        int a3;
        if (list instanceof fn) {
            fn fnVar = (fn) list;
            int i = this.f2193b & 7;
            if (i == 2) {
                int m = this.f2192a.m();
                c(m);
                int u = this.f2192a.u() + m;
                do {
                    fnVar.a(this.f2192a.c());
                } while (this.f2192a.u() < u);
            } else if (i == 5) {
                do {
                    fnVar.a(this.f2192a.c());
                    if (!this.f2192a.t()) {
                        a3 = this.f2192a.a();
                    } else {
                        return;
                    }
                } while (a3 == this.f2193b);
                this.d = a3;
            } else {
                throw zzuv.e();
            }
        } else {
            int i2 = this.f2193b & 7;
            if (i2 == 2) {
                int m2 = this.f2192a.m();
                c(m2);
                int u2 = this.f2192a.u() + m2;
                do {
                    list.add(Float.valueOf(this.f2192a.c()));
                } while (this.f2192a.u() < u2);
            } else if (i2 == 5) {
                do {
                    list.add(Float.valueOf(this.f2192a.c()));
                    if (!this.f2192a.t()) {
                        a2 = this.f2192a.a();
                    } else {
                        return;
                    }
                } while (a2 == this.f2193b);
                this.d = a2;
            } else {
                throw zzuv.e();
            }
        }
    }

    public final void c(List<Long> list) throws IOException {
        int a2;
        int a3;
        if (list instanceof gh) {
            gh ghVar = (gh) list;
            int i = this.f2193b & 7;
            if (i == 0) {
                do {
                    ghVar.a(this.f2192a.d());
                    if (!this.f2192a.t()) {
                        a3 = this.f2192a.a();
                    } else {
                        return;
                    }
                } while (a3 == this.f2193b);
                this.d = a3;
            } else if (i == 2) {
                int u = this.f2192a.u() + this.f2192a.m();
                do {
                    ghVar.a(this.f2192a.d());
                } while (this.f2192a.u() < u);
                d(u);
            } else {
                throw zzuv.e();
            }
        } else {
            int i2 = this.f2193b & 7;
            if (i2 == 0) {
                do {
                    list.add(Long.valueOf(this.f2192a.d()));
                    if (!this.f2192a.t()) {
                        a2 = this.f2192a.a();
                    } else {
                        return;
                    }
                } while (a2 == this.f2193b);
                this.d = a2;
            } else if (i2 == 2) {
                int u2 = this.f2192a.u() + this.f2192a.m();
                do {
                    list.add(Long.valueOf(this.f2192a.d()));
                } while (this.f2192a.u() < u2);
                d(u2);
            } else {
                throw zzuv.e();
            }
        }
    }

    public final void d(List<Long> list) throws IOException {
        int a2;
        int a3;
        if (list instanceof gh) {
            gh ghVar = (gh) list;
            int i = this.f2193b & 7;
            if (i == 0) {
                do {
                    ghVar.a(this.f2192a.e());
                    if (!this.f2192a.t()) {
                        a3 = this.f2192a.a();
                    } else {
                        return;
                    }
                } while (a3 == this.f2193b);
                this.d = a3;
            } else if (i == 2) {
                int u = this.f2192a.u() + this.f2192a.m();
                do {
                    ghVar.a(this.f2192a.e());
                } while (this.f2192a.u() < u);
                d(u);
            } else {
                throw zzuv.e();
            }
        } else {
            int i2 = this.f2193b & 7;
            if (i2 == 0) {
                do {
                    list.add(Long.valueOf(this.f2192a.e()));
                    if (!this.f2192a.t()) {
                        a2 = this.f2192a.a();
                    } else {
                        return;
                    }
                } while (a2 == this.f2193b);
                this.d = a2;
            } else if (i2 == 2) {
                int u2 = this.f2192a.u() + this.f2192a.m();
                do {
                    list.add(Long.valueOf(this.f2192a.e()));
                } while (this.f2192a.u() < u2);
                d(u2);
            } else {
                throw zzuv.e();
            }
        }
    }

    public final void e(List<Integer> list) throws IOException {
        int a2;
        int a3;
        if (list instanceof fr) {
            fr frVar = (fr) list;
            int i = this.f2193b & 7;
            if (i == 0) {
                do {
                    frVar.c(this.f2192a.f());
                    if (!this.f2192a.t()) {
                        a3 = this.f2192a.a();
                    } else {
                        return;
                    }
                } while (a3 == this.f2193b);
                this.d = a3;
            } else if (i == 2) {
                int u = this.f2192a.u() + this.f2192a.m();
                do {
                    frVar.c(this.f2192a.f());
                } while (this.f2192a.u() < u);
                d(u);
            } else {
                throw zzuv.e();
            }
        } else {
            int i2 = this.f2193b & 7;
            if (i2 == 0) {
                do {
                    list.add(Integer.valueOf(this.f2192a.f()));
                    if (!this.f2192a.t()) {
                        a2 = this.f2192a.a();
                    } else {
                        return;
                    }
                } while (a2 == this.f2193b);
                this.d = a2;
            } else if (i2 == 2) {
                int u2 = this.f2192a.u() + this.f2192a.m();
                do {
                    list.add(Integer.valueOf(this.f2192a.f()));
                } while (this.f2192a.u() < u2);
                d(u2);
            } else {
                throw zzuv.e();
            }
        }
    }

    public final void f(List<Long> list) throws IOException {
        int a2;
        int a3;
        if (list instanceof gh) {
            gh ghVar = (gh) list;
            int i = this.f2193b & 7;
            if (i == 1) {
                do {
                    ghVar.a(this.f2192a.g());
                    if (!this.f2192a.t()) {
                        a3 = this.f2192a.a();
                    } else {
                        return;
                    }
                } while (a3 == this.f2193b);
                this.d = a3;
            } else if (i == 2) {
                int m = this.f2192a.m();
                b(m);
                int u = this.f2192a.u() + m;
                do {
                    ghVar.a(this.f2192a.g());
                } while (this.f2192a.u() < u);
            } else {
                throw zzuv.e();
            }
        } else {
            int i2 = this.f2193b & 7;
            if (i2 == 1) {
                do {
                    list.add(Long.valueOf(this.f2192a.g()));
                    if (!this.f2192a.t()) {
                        a2 = this.f2192a.a();
                    } else {
                        return;
                    }
                } while (a2 == this.f2193b);
                this.d = a2;
            } else if (i2 == 2) {
                int m2 = this.f2192a.m();
                b(m2);
                int u2 = this.f2192a.u() + m2;
                do {
                    list.add(Long.valueOf(this.f2192a.g()));
                } while (this.f2192a.u() < u2);
            } else {
                throw zzuv.e();
            }
        }
    }

    public final void g(List<Integer> list) throws IOException {
        int a2;
        int a3;
        if (list instanceof fr) {
            fr frVar = (fr) list;
            int i = this.f2193b & 7;
            if (i == 2) {
                int m = this.f2192a.m();
                c(m);
                int u = this.f2192a.u() + m;
                do {
                    frVar.c(this.f2192a.h());
                } while (this.f2192a.u() < u);
            } else if (i == 5) {
                do {
                    frVar.c(this.f2192a.h());
                    if (!this.f2192a.t()) {
                        a3 = this.f2192a.a();
                    } else {
                        return;
                    }
                } while (a3 == this.f2193b);
                this.d = a3;
            } else {
                throw zzuv.e();
            }
        } else {
            int i2 = this.f2193b & 7;
            if (i2 == 2) {
                int m2 = this.f2192a.m();
                c(m2);
                int u2 = this.f2192a.u() + m2;
                do {
                    list.add(Integer.valueOf(this.f2192a.h()));
                } while (this.f2192a.u() < u2);
            } else if (i2 == 5) {
                do {
                    list.add(Integer.valueOf(this.f2192a.h()));
                    if (!this.f2192a.t()) {
                        a2 = this.f2192a.a();
                    } else {
                        return;
                    }
                } while (a2 == this.f2193b);
                this.d = a2;
            } else {
                throw zzuv.e();
            }
        }
    }

    public final void h(List<Boolean> list) throws IOException {
        int a2;
        int a3;
        if (list instanceof eh) {
            eh ehVar = (eh) list;
            int i = this.f2193b & 7;
            if (i == 0) {
                do {
                    ehVar.a(this.f2192a.i());
                    if (!this.f2192a.t()) {
                        a3 = this.f2192a.a();
                    } else {
                        return;
                    }
                } while (a3 == this.f2193b);
                this.d = a3;
            } else if (i == 2) {
                int u = this.f2192a.u() + this.f2192a.m();
                do {
                    ehVar.a(this.f2192a.i());
                } while (this.f2192a.u() < u);
                d(u);
            } else {
                throw zzuv.e();
            }
        } else {
            int i2 = this.f2193b & 7;
            if (i2 == 0) {
                do {
                    list.add(Boolean.valueOf(this.f2192a.i()));
                    if (!this.f2192a.t()) {
                        a2 = this.f2192a.a();
                    } else {
                        return;
                    }
                } while (a2 == this.f2193b);
                this.d = a2;
            } else if (i2 == 2) {
                int u2 = this.f2192a.u() + this.f2192a.m();
                do {
                    list.add(Boolean.valueOf(this.f2192a.i()));
                } while (this.f2192a.u() < u2);
                d(u2);
            } else {
                throw zzuv.e();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.ex.a(java.util.List<java.lang.String>, boolean):void
     arg types: [java.util.List<java.lang.String>, int]
     candidates:
      com.google.android.gms.internal.measurement.ex.a(com.google.android.gms.internal.measurement.hj, com.google.android.gms.internal.measurement.fd):T
      com.google.android.gms.internal.measurement.hi.a(com.google.android.gms.internal.measurement.hj, com.google.android.gms.internal.measurement.fd):T
      com.google.android.gms.internal.measurement.ex.a(java.util.List<java.lang.String>, boolean):void */
    public final void i(List<String> list) throws IOException {
        a(list, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.ex.a(java.util.List<java.lang.String>, boolean):void
     arg types: [java.util.List<java.lang.String>, int]
     candidates:
      com.google.android.gms.internal.measurement.ex.a(com.google.android.gms.internal.measurement.hj, com.google.android.gms.internal.measurement.fd):T
      com.google.android.gms.internal.measurement.hi.a(com.google.android.gms.internal.measurement.hj, com.google.android.gms.internal.measurement.fd):T
      com.google.android.gms.internal.measurement.ex.a(java.util.List<java.lang.String>, boolean):void */
    public final void j(List<String> list) throws IOException {
        a(list, true);
    }

    private final void a(List<String> list, boolean z) throws IOException {
        int a2;
        int a3;
        if ((this.f2193b & 7) != 2) {
            throw zzuv.e();
        } else if (!(list instanceof gd) || z) {
            do {
                list.add(z ? m() : l());
                if (!this.f2192a.t()) {
                    a2 = this.f2192a.a();
                } else {
                    return;
                }
            } while (a2 == this.f2193b);
            this.d = a2;
        } else {
            gd gdVar = (gd) list;
            do {
                gdVar.a(n());
                if (!this.f2192a.t()) {
                    a3 = this.f2192a.a();
                } else {
                    return;
                }
            } while (a3 == this.f2193b);
            this.d = a3;
        }
    }

    public final <T> void a(List<T> list, hj<T> hjVar, fd fdVar) throws IOException {
        int a2;
        int i = this.f2193b;
        if ((i & 7) == 2) {
            do {
                list.add(c(hjVar, fdVar));
                if (!this.f2192a.t() && this.d == 0) {
                    a2 = this.f2192a.a();
                } else {
                    return;
                }
            } while (a2 == i);
            this.d = a2;
            return;
        }
        throw zzuv.e();
    }

    public final <T> void b(List<T> list, hj<T> hjVar, fd fdVar) throws IOException {
        int a2;
        int i = this.f2193b;
        if ((i & 7) == 3) {
            do {
                list.add(d(hjVar, fdVar));
                if (!this.f2192a.t() && this.d == 0) {
                    a2 = this.f2192a.a();
                } else {
                    return;
                }
            } while (a2 == i);
            this.d = a2;
            return;
        }
        throw zzuv.e();
    }

    public final void k(List<ej> list) throws IOException {
        int a2;
        if ((this.f2193b & 7) == 2) {
            do {
                list.add(n());
                if (!this.f2192a.t()) {
                    a2 = this.f2192a.a();
                } else {
                    return;
                }
            } while (a2 == this.f2193b);
            this.d = a2;
            return;
        }
        throw zzuv.e();
    }

    public final void l(List<Integer> list) throws IOException {
        int a2;
        int a3;
        if (list instanceof fr) {
            fr frVar = (fr) list;
            int i = this.f2193b & 7;
            if (i == 0) {
                do {
                    frVar.c(this.f2192a.m());
                    if (!this.f2192a.t()) {
                        a3 = this.f2192a.a();
                    } else {
                        return;
                    }
                } while (a3 == this.f2193b);
                this.d = a3;
            } else if (i == 2) {
                int u = this.f2192a.u() + this.f2192a.m();
                do {
                    frVar.c(this.f2192a.m());
                } while (this.f2192a.u() < u);
                d(u);
            } else {
                throw zzuv.e();
            }
        } else {
            int i2 = this.f2193b & 7;
            if (i2 == 0) {
                do {
                    list.add(Integer.valueOf(this.f2192a.m()));
                    if (!this.f2192a.t()) {
                        a2 = this.f2192a.a();
                    } else {
                        return;
                    }
                } while (a2 == this.f2193b);
                this.d = a2;
            } else if (i2 == 2) {
                int u2 = this.f2192a.u() + this.f2192a.m();
                do {
                    list.add(Integer.valueOf(this.f2192a.m()));
                } while (this.f2192a.u() < u2);
                d(u2);
            } else {
                throw zzuv.e();
            }
        }
    }

    public final void m(List<Integer> list) throws IOException {
        int a2;
        int a3;
        if (list instanceof fr) {
            fr frVar = (fr) list;
            int i = this.f2193b & 7;
            if (i == 0) {
                do {
                    frVar.c(this.f2192a.n());
                    if (!this.f2192a.t()) {
                        a3 = this.f2192a.a();
                    } else {
                        return;
                    }
                } while (a3 == this.f2193b);
                this.d = a3;
            } else if (i == 2) {
                int u = this.f2192a.u() + this.f2192a.m();
                do {
                    frVar.c(this.f2192a.n());
                } while (this.f2192a.u() < u);
                d(u);
            } else {
                throw zzuv.e();
            }
        } else {
            int i2 = this.f2193b & 7;
            if (i2 == 0) {
                do {
                    list.add(Integer.valueOf(this.f2192a.n()));
                    if (!this.f2192a.t()) {
                        a2 = this.f2192a.a();
                    } else {
                        return;
                    }
                } while (a2 == this.f2193b);
                this.d = a2;
            } else if (i2 == 2) {
                int u2 = this.f2192a.u() + this.f2192a.m();
                do {
                    list.add(Integer.valueOf(this.f2192a.n()));
                } while (this.f2192a.u() < u2);
                d(u2);
            } else {
                throw zzuv.e();
            }
        }
    }

    public final void n(List<Integer> list) throws IOException {
        int a2;
        int a3;
        if (list instanceof fr) {
            fr frVar = (fr) list;
            int i = this.f2193b & 7;
            if (i == 2) {
                int m = this.f2192a.m();
                c(m);
                int u = this.f2192a.u() + m;
                do {
                    frVar.c(this.f2192a.o());
                } while (this.f2192a.u() < u);
            } else if (i == 5) {
                do {
                    frVar.c(this.f2192a.o());
                    if (!this.f2192a.t()) {
                        a3 = this.f2192a.a();
                    } else {
                        return;
                    }
                } while (a3 == this.f2193b);
                this.d = a3;
            } else {
                throw zzuv.e();
            }
        } else {
            int i2 = this.f2193b & 7;
            if (i2 == 2) {
                int m2 = this.f2192a.m();
                c(m2);
                int u2 = this.f2192a.u() + m2;
                do {
                    list.add(Integer.valueOf(this.f2192a.o()));
                } while (this.f2192a.u() < u2);
            } else if (i2 == 5) {
                do {
                    list.add(Integer.valueOf(this.f2192a.o()));
                    if (!this.f2192a.t()) {
                        a2 = this.f2192a.a();
                    } else {
                        return;
                    }
                } while (a2 == this.f2193b);
                this.d = a2;
            } else {
                throw zzuv.e();
            }
        }
    }

    public final void o(List<Long> list) throws IOException {
        int a2;
        int a3;
        if (list instanceof gh) {
            gh ghVar = (gh) list;
            int i = this.f2193b & 7;
            if (i == 1) {
                do {
                    ghVar.a(this.f2192a.p());
                    if (!this.f2192a.t()) {
                        a3 = this.f2192a.a();
                    } else {
                        return;
                    }
                } while (a3 == this.f2193b);
                this.d = a3;
            } else if (i == 2) {
                int m = this.f2192a.m();
                b(m);
                int u = this.f2192a.u() + m;
                do {
                    ghVar.a(this.f2192a.p());
                } while (this.f2192a.u() < u);
            } else {
                throw zzuv.e();
            }
        } else {
            int i2 = this.f2193b & 7;
            if (i2 == 1) {
                do {
                    list.add(Long.valueOf(this.f2192a.p()));
                    if (!this.f2192a.t()) {
                        a2 = this.f2192a.a();
                    } else {
                        return;
                    }
                } while (a2 == this.f2193b);
                this.d = a2;
            } else if (i2 == 2) {
                int m2 = this.f2192a.m();
                b(m2);
                int u2 = this.f2192a.u() + m2;
                do {
                    list.add(Long.valueOf(this.f2192a.p()));
                } while (this.f2192a.u() < u2);
            } else {
                throw zzuv.e();
            }
        }
    }

    public final void p(List<Integer> list) throws IOException {
        int a2;
        int a3;
        if (list instanceof fr) {
            fr frVar = (fr) list;
            int i = this.f2193b & 7;
            if (i == 0) {
                do {
                    frVar.c(this.f2192a.q());
                    if (!this.f2192a.t()) {
                        a3 = this.f2192a.a();
                    } else {
                        return;
                    }
                } while (a3 == this.f2193b);
                this.d = a3;
            } else if (i == 2) {
                int u = this.f2192a.u() + this.f2192a.m();
                do {
                    frVar.c(this.f2192a.q());
                } while (this.f2192a.u() < u);
                d(u);
            } else {
                throw zzuv.e();
            }
        } else {
            int i2 = this.f2193b & 7;
            if (i2 == 0) {
                do {
                    list.add(Integer.valueOf(this.f2192a.q()));
                    if (!this.f2192a.t()) {
                        a2 = this.f2192a.a();
                    } else {
                        return;
                    }
                } while (a2 == this.f2193b);
                this.d = a2;
            } else if (i2 == 2) {
                int u2 = this.f2192a.u() + this.f2192a.m();
                do {
                    list.add(Integer.valueOf(this.f2192a.q()));
                } while (this.f2192a.u() < u2);
                d(u2);
            } else {
                throw zzuv.e();
            }
        }
    }

    public final void q(List<Long> list) throws IOException {
        int a2;
        int a3;
        if (list instanceof gh) {
            gh ghVar = (gh) list;
            int i = this.f2193b & 7;
            if (i == 0) {
                do {
                    ghVar.a(this.f2192a.r());
                    if (!this.f2192a.t()) {
                        a3 = this.f2192a.a();
                    } else {
                        return;
                    }
                } while (a3 == this.f2193b);
                this.d = a3;
            } else if (i == 2) {
                int u = this.f2192a.u() + this.f2192a.m();
                do {
                    ghVar.a(this.f2192a.r());
                } while (this.f2192a.u() < u);
                d(u);
            } else {
                throw zzuv.e();
            }
        } else {
            int i2 = this.f2193b & 7;
            if (i2 == 0) {
                do {
                    list.add(Long.valueOf(this.f2192a.r()));
                    if (!this.f2192a.t()) {
                        a2 = this.f2192a.a();
                    } else {
                        return;
                    }
                } while (a2 == this.f2193b);
                this.d = a2;
            } else if (i2 == 2) {
                int u2 = this.f2192a.u() + this.f2192a.m();
                do {
                    list.add(Long.valueOf(this.f2192a.r()));
                } while (this.f2192a.u() < u2);
                d(u2);
            } else {
                throw zzuv.e();
            }
        }
    }

    private static void b(int i) throws IOException {
        if ((i & 7) != 0) {
            throw zzuv.g();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0052, code lost:
        if (c() != false) goto L_0x0054;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x005a, code lost:
        throw new com.google.android.gms.internal.measurement.zzuv("Unable to parse map entry.");
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x004e */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final <K, V> void a(java.util.Map<K, V> r8, com.google.android.gms.internal.measurement.gm<K, V> r9, com.google.android.gms.internal.measurement.fd r10) throws java.io.IOException {
        /*
            r7 = this;
            r0 = 2
            r7.a(r0)
            com.google.android.gms.internal.measurement.ev r1 = r7.f2192a
            int r1 = r1.m()
            com.google.android.gms.internal.measurement.ev r2 = r7.f2192a
            int r1 = r2.c(r1)
            K r2 = r9.f2245b
            V r3 = r9.d
        L_0x0014:
            int r4 = r7.a()     // Catch:{ all -> 0x0064 }
            r5 = 2147483647(0x7fffffff, float:NaN)
            if (r4 == r5) goto L_0x005b
            com.google.android.gms.internal.measurement.ev r5 = r7.f2192a     // Catch:{ all -> 0x0064 }
            boolean r5 = r5.t()     // Catch:{ all -> 0x0064 }
            if (r5 != 0) goto L_0x005b
            r5 = 1
            java.lang.String r6 = "Unable to parse map entry."
            if (r4 == r5) goto L_0x0046
            if (r4 == r0) goto L_0x0039
            boolean r4 = r7.c()     // Catch:{ zzuw -> 0x004e }
            if (r4 == 0) goto L_0x0033
            goto L_0x0014
        L_0x0033:
            com.google.android.gms.internal.measurement.zzuv r4 = new com.google.android.gms.internal.measurement.zzuv     // Catch:{ zzuw -> 0x004e }
            r4.<init>(r6)     // Catch:{ zzuw -> 0x004e }
            throw r4     // Catch:{ zzuw -> 0x004e }
        L_0x0039:
            com.google.android.gms.internal.measurement.ip r4 = r9.c     // Catch:{ zzuw -> 0x004e }
            V r5 = r9.d     // Catch:{ zzuw -> 0x004e }
            java.lang.Class r5 = r5.getClass()     // Catch:{ zzuw -> 0x004e }
            java.lang.Object r3 = r7.a(r4, r5, r10)     // Catch:{ zzuw -> 0x004e }
            goto L_0x0014
        L_0x0046:
            com.google.android.gms.internal.measurement.ip r4 = r9.f2244a     // Catch:{ zzuw -> 0x004e }
            r5 = 0
            java.lang.Object r2 = r7.a(r4, r5, r5)     // Catch:{ zzuw -> 0x004e }
            goto L_0x0014
        L_0x004e:
            boolean r4 = r7.c()     // Catch:{ all -> 0x0064 }
            if (r4 == 0) goto L_0x0055
            goto L_0x0014
        L_0x0055:
            com.google.android.gms.internal.measurement.zzuv r8 = new com.google.android.gms.internal.measurement.zzuv     // Catch:{ all -> 0x0064 }
            r8.<init>(r6)     // Catch:{ all -> 0x0064 }
            throw r8     // Catch:{ all -> 0x0064 }
        L_0x005b:
            r8.put(r2, r3)     // Catch:{ all -> 0x0064 }
            com.google.android.gms.internal.measurement.ev r8 = r7.f2192a
            r8.d(r1)
            return
        L_0x0064:
            r8 = move-exception
            com.google.android.gms.internal.measurement.ev r9 = r7.f2192a
            r9.d(r1)
            goto L_0x006c
        L_0x006b:
            throw r8
        L_0x006c:
            goto L_0x006b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.ex.a(java.util.Map, com.google.android.gms.internal.measurement.gm, com.google.android.gms.internal.measurement.fd):void");
    }

    private final Object a(ip ipVar, Class<?> cls, fd fdVar) throws IOException {
        switch (ey.f2194a[ipVar.ordinal()]) {
            case 1:
                return Boolean.valueOf(k());
            case 2:
                return n();
            case 3:
                return Double.valueOf(d());
            case 4:
                return Integer.valueOf(p());
            case 5:
                return Integer.valueOf(j());
            case 6:
                return Long.valueOf(i());
            case 7:
                return Float.valueOf(e());
            case 8:
                return Integer.valueOf(h());
            case 9:
                return Long.valueOf(g());
            case 10:
                a(2);
                return c(hf.a().a((Class) cls), fdVar);
            case 11:
                return Integer.valueOf(q());
            case 12:
                return Long.valueOf(r());
            case 13:
                return Integer.valueOf(s());
            case 14:
                return Long.valueOf(t());
            case 15:
                return m();
            case 16:
                return Integer.valueOf(o());
            case 17:
                return Long.valueOf(f());
            default:
                throw new RuntimeException("unsupported field type.");
        }
    }

    private static void c(int i) throws IOException {
        if ((i & 3) != 0) {
            throw zzuv.g();
        }
    }

    private final void d(int i) throws IOException {
        if (this.f2192a.u() != i) {
            throw zzuv.a();
        }
    }
}
