package com.google.android.gms.measurement.internal;

import java.util.List;
import java.util.concurrent.Callable;

final class bc implements Callable<List<zzo>> {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ String f2411a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f2412b;
    private final /* synthetic */ String c;
    private final /* synthetic */ zzby d;

    bc(zzby zzby, String str, String str2, String str3) {
        this.d = zzby;
        this.f2411a = str;
        this.f2412b = str2;
        this.c = str3;
    }

    public final /* synthetic */ Object call() throws Exception {
        this.d.f2597a.k();
        return this.d.f2597a.d().b(this.f2411a, this.f2412b, this.c);
    }
}
