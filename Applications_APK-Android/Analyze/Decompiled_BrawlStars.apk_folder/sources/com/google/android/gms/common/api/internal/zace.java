package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.d;
import com.google.android.gms.common.internal.IAccountAccessor;
import com.google.android.gms.common.internal.ResolveAccountResponse;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.signin.b;
import com.google.android.gms.signin.e;
import com.google.android.gms.signin.internal.zac;
import com.google.android.gms.signin.internal.zaj;
import java.util.Set;

public final class zace extends zac implements d.b, d.c {

    /* renamed from: b  reason: collision with root package name */
    private static a.C0111a<? extends e, com.google.android.gms.signin.a> f1508b = b.f2607a;

    /* renamed from: a  reason: collision with root package name */
    e f1509a;
    private final Context c;
    private final Handler d;
    private final a.C0111a<? extends e, com.google.android.gms.signin.a> e;
    private Set<Scope> f;
    private com.google.android.gms.common.internal.b g;
    /* access modifiers changed from: private */
    public bi h;

    public zace(Context context, Handler handler, com.google.android.gms.common.internal.b bVar) {
        this(context, handler, bVar, f1508b);
    }

    public zace(Context context, Handler handler, com.google.android.gms.common.internal.b bVar, a.C0111a<? extends e, com.google.android.gms.signin.a> aVar) {
        this.c = context;
        this.d = handler;
        this.g = (com.google.android.gms.common.internal.b) l.a(bVar, "ClientSettings must not be null");
        this.f = bVar.f1596b;
        this.e = aVar;
    }

    public final void a(bi biVar) {
        e eVar = this.f1509a;
        if (eVar != null) {
            eVar.a();
        }
        this.g.j = Integer.valueOf(System.identityHashCode(this));
        a.C0111a<? extends e, com.google.android.gms.signin.a> aVar = this.e;
        Context context = this.c;
        Looper looper = this.d.getLooper();
        com.google.android.gms.common.internal.b bVar = this.g;
        this.f1509a = (e) aVar.a(context, looper, bVar, bVar.i, this, this);
        this.h = biVar;
        Set<Scope> set = this.f;
        if (set == null || set.isEmpty()) {
            this.d.post(new bg(this));
        } else {
            this.f1509a.s();
        }
    }

    public final void onConnected(Bundle bundle) {
        this.f1509a.a(this);
    }

    public final void onConnectionSuspended(int i) {
        this.f1509a.a();
    }

    public final void onConnectionFailed(ConnectionResult connectionResult) {
        this.h.b(connectionResult);
    }

    public final void a(zaj zaj) {
        this.d.post(new bh(this, zaj));
    }

    static /* synthetic */ void a(zace zace, zaj zaj) {
        ConnectionResult connectionResult = zaj.f2613a;
        if (connectionResult.b()) {
            ResolveAccountResponse resolveAccountResponse = zaj.f2614b;
            ConnectionResult connectionResult2 = resolveAccountResponse.f1582b;
            if (!connectionResult2.b()) {
                String valueOf = String.valueOf(connectionResult2);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 48);
                sb.append("Sign-in succeeded with resolve account failure: ");
                sb.append(valueOf);
                Log.wtf("SignInCoordinator", sb.toString(), new Exception());
                zace.h.b(connectionResult2);
                zace.f1509a.a();
                return;
            }
            zace.h.a(IAccountAccessor.Stub.a(resolveAccountResponse.f1581a), zace.f);
        } else {
            zace.h.b(connectionResult);
        }
        zace.f1509a.a();
    }
}
