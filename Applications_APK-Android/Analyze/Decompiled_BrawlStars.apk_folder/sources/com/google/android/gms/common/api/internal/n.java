package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.e;

final class n implements e.a {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ BasePendingResult f1486a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ m f1487b;

    n(m mVar, BasePendingResult basePendingResult) {
        this.f1487b = mVar;
        this.f1486a = basePendingResult;
    }

    public final void a() {
        this.f1487b.f1484a.remove(this.f1486a);
    }
}
