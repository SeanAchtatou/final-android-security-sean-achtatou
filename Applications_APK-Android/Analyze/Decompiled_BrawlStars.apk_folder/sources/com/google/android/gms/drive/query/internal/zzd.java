package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.drive.metadata.a;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public final class zzd extends zza {
    public static final Parcelable.Creator<zzd> CREATOR = new c();

    /* renamed from: a  reason: collision with root package name */
    private final MetadataBundle f1753a;

    /* renamed from: b  reason: collision with root package name */
    private final a<?> f1754b;

    zzd(MetadataBundle metadataBundle) {
        this.f1753a = metadataBundle;
        this.f1754b = f.a(metadataBundle);
    }

    public final <T> T a(g<T> gVar) {
        return gVar.a(this.f1754b);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.metadata.internal.MetadataBundle, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = com.google.android.gms.common.internal.safeparcel.a.a(parcel, 20293);
        com.google.android.gms.common.internal.safeparcel.a.a(parcel, 1, (Parcelable) this.f1753a, i, false);
        com.google.android.gms.common.internal.safeparcel.a.b(parcel, a2);
    }
}
