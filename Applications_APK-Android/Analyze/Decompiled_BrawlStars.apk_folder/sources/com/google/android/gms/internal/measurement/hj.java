package com.google.android.gms.internal.measurement;

import java.io.IOException;

interface hj<T> {
    int a(T t);

    T a();

    void a(T t, hi hiVar, fd fdVar) throws IOException;

    void a(Object obj, iv ivVar) throws IOException;

    boolean a(Object obj, Object obj2);

    int b(T t);

    void b(T t, T t2);

    void c(T t);

    boolean d(T t);
}
