package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.d;

public final class GameRef extends d implements Game {
    public GameRef(DataHolder dataHolder, int i) {
        super(dataHolder, i);
    }

    public final /* synthetic */ Object a() {
        return new GameEntity(this);
    }

    public final String b() {
        return e("external_game_id");
    }

    public final String c() {
        return e("display_name");
    }

    public final String d() {
        return e("primary_category");
    }

    public final int describeContents() {
        return 0;
    }

    public final String e() {
        return e("secondary_category");
    }

    public final boolean equals(Object obj) {
        return GameEntity.a(this, obj);
    }

    public final String f() {
        return e("game_description");
    }

    public final String g() {
        return e("developer_name");
    }

    public final String getFeaturedImageUrl() {
        return e("featured_image_url");
    }

    public final String getHiResImageUrl() {
        return e("game_hi_res_image_url");
    }

    public final String getIconImageUrl() {
        return e("game_icon_image_url");
    }

    public final Uri h() {
        return h("game_icon_image_uri");
    }

    public final int hashCode() {
        return GameEntity.a(this);
    }

    public final Uri i() {
        return h("game_hi_res_image_uri");
    }

    public final Uri j() {
        return h("featured_image_uri");
    }

    public final boolean k() {
        return d("play_enabled_game");
    }

    public final boolean l() {
        return d("muted");
    }

    public final boolean m() {
        return d("identity_sharing_confirmed");
    }

    public final boolean n() {
        return c("installed") > 0;
    }

    public final String o() {
        return e("package_name");
    }

    public final int p() {
        return c("achievement_total_count");
    }

    public final int q() {
        return c("leaderboard_count");
    }

    public final boolean r() {
        return c("real_time_support") > 0;
    }

    public final boolean s() {
        return c("turn_based_support") > 0;
    }

    public final boolean t() {
        return c("snapshots_enabled") > 0;
    }

    public final String toString() {
        return GameEntity.b(this);
    }

    public final String u() {
        return e("theme_color");
    }

    public final boolean v() {
        return c("gamepad_support") > 0;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        ((GameEntity) ((Game) a())).writeToParcel(parcel, i);
    }
}
