package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.drive.Contents;

public final class cc implements Parcelable.Creator<zzo> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zzo[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        Contents contents = null;
        Boolean bool = null;
        int i = 0;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            int i2 = 65535 & readInt;
            if (i2 == 2) {
                contents = (Contents) SafeParcelReader.a(parcel, readInt, Contents.CREATOR);
            } else if (i2 == 3) {
                int a3 = SafeParcelReader.a(parcel, readInt);
                if (a3 == 0) {
                    bool = null;
                } else {
                    SafeParcelReader.a(parcel, a3, 4);
                    bool = Boolean.valueOf(parcel.readInt() != 0);
                }
            } else if (i2 != 4) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                i = SafeParcelReader.d(parcel, readInt);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new zzo(contents, bool, i);
    }
}
