package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class ci extends iz<ci> {
    private static volatile ci[] d;

    /* renamed from: a  reason: collision with root package name */
    public Integer f2114a = null;

    /* renamed from: b  reason: collision with root package name */
    public cm[] f2115b = cm.a();
    public cj[] c = cj.a();
    private Boolean e = null;
    private Boolean f = null;

    public static ci[] a() {
        if (d == null) {
            synchronized (jd.f2312b) {
                if (d == null) {
                    d = new ci[0];
                }
            }
        }
        return d;
    }

    public ci() {
        this.L = null;
        this.M = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ci)) {
            return false;
        }
        ci ciVar = (ci) obj;
        Integer num = this.f2114a;
        if (num == null) {
            if (ciVar.f2114a != null) {
                return false;
            }
        } else if (!num.equals(ciVar.f2114a)) {
            return false;
        }
        if (!jd.a(this.f2115b, ciVar.f2115b) || !jd.a(this.c, ciVar.c)) {
            return false;
        }
        Boolean bool = this.e;
        if (bool == null) {
            if (ciVar.e != null) {
                return false;
            }
        } else if (!bool.equals(ciVar.e)) {
            return false;
        }
        Boolean bool2 = this.f;
        if (bool2 == null) {
            if (ciVar.f != null) {
                return false;
            }
        } else if (!bool2.equals(ciVar.f)) {
            return false;
        }
        if (this.L == null || this.L.a()) {
            return ciVar.L == null || ciVar.L.a();
        }
        return this.L.equals(ciVar.L);
    }

    public final int hashCode() {
        int hashCode = (getClass().getName().hashCode() + 527) * 31;
        Integer num = this.f2114a;
        int i = 0;
        int hashCode2 = (((((hashCode + (num == null ? 0 : num.hashCode())) * 31) + jd.a(this.f2115b)) * 31) + jd.a(this.c)) * 31;
        Boolean bool = this.e;
        int hashCode3 = (hashCode2 + (bool == null ? 0 : bool.hashCode())) * 31;
        Boolean bool2 = this.f;
        int hashCode4 = (hashCode3 + (bool2 == null ? 0 : bool2.hashCode())) * 31;
        if (this.L != null && !this.L.a()) {
            i = this.L.hashCode();
        }
        return hashCode4 + i;
    }

    public final void a(iy iyVar) throws IOException {
        Integer num = this.f2114a;
        if (num != null) {
            iyVar.a(1, num.intValue());
        }
        cm[] cmVarArr = this.f2115b;
        int i = 0;
        if (cmVarArr != null && cmVarArr.length > 0) {
            int i2 = 0;
            while (true) {
                cm[] cmVarArr2 = this.f2115b;
                if (i2 >= cmVarArr2.length) {
                    break;
                }
                cm cmVar = cmVarArr2[i2];
                if (cmVar != null) {
                    iyVar.a(2, cmVar);
                }
                i2++;
            }
        }
        cj[] cjVarArr = this.c;
        if (cjVarArr != null && cjVarArr.length > 0) {
            while (true) {
                cj[] cjVarArr2 = this.c;
                if (i >= cjVarArr2.length) {
                    break;
                }
                cj cjVar = cjVarArr2[i];
                if (cjVar != null) {
                    iyVar.a(3, cjVar);
                }
                i++;
            }
        }
        Boolean bool = this.e;
        if (bool != null) {
            iyVar.a(4, bool.booleanValue());
        }
        Boolean bool2 = this.f;
        if (bool2 != null) {
            iyVar.a(5, bool2.booleanValue());
        }
        super.a(iyVar);
    }

    /* access modifiers changed from: protected */
    public final int b() {
        int b2 = super.b();
        Integer num = this.f2114a;
        if (num != null) {
            b2 += iy.b(1, num.intValue());
        }
        cm[] cmVarArr = this.f2115b;
        int i = 0;
        if (cmVarArr != null && cmVarArr.length > 0) {
            int i2 = b2;
            int i3 = 0;
            while (true) {
                cm[] cmVarArr2 = this.f2115b;
                if (i3 >= cmVarArr2.length) {
                    break;
                }
                cm cmVar = cmVarArr2[i3];
                if (cmVar != null) {
                    i2 += iy.b(2, cmVar);
                }
                i3++;
            }
            b2 = i2;
        }
        cj[] cjVarArr = this.c;
        if (cjVarArr != null && cjVarArr.length > 0) {
            while (true) {
                cj[] cjVarArr2 = this.c;
                if (i >= cjVarArr2.length) {
                    break;
                }
                cj cjVar = cjVarArr2[i];
                if (cjVar != null) {
                    b2 += iy.b(3, cjVar);
                }
                i++;
            }
        }
        Boolean bool = this.e;
        if (bool != null) {
            bool.booleanValue();
            b2 += iy.c(32) + 1;
        }
        Boolean bool2 = this.f;
        if (bool2 == null) {
            return b2;
        }
        bool2.booleanValue();
        return b2 + iy.c(40) + 1;
    }

    public final /* synthetic */ je a(iw iwVar) throws IOException {
        while (true) {
            int a2 = iwVar.a();
            if (a2 == 0) {
                return this;
            }
            if (a2 == 8) {
                this.f2114a = Integer.valueOf(iwVar.d());
            } else if (a2 == 18) {
                int a3 = jh.a(iwVar, 18);
                cm[] cmVarArr = this.f2115b;
                int length = cmVarArr == null ? 0 : cmVarArr.length;
                cm[] cmVarArr2 = new cm[(a3 + length)];
                if (length != 0) {
                    System.arraycopy(this.f2115b, 0, cmVarArr2, 0, length);
                }
                while (length < cmVarArr2.length - 1) {
                    cmVarArr2[length] = new cm();
                    iwVar.a(cmVarArr2[length]);
                    iwVar.a();
                    length++;
                }
                cmVarArr2[length] = new cm();
                iwVar.a(cmVarArr2[length]);
                this.f2115b = cmVarArr2;
            } else if (a2 == 26) {
                int a4 = jh.a(iwVar, 26);
                cj[] cjVarArr = this.c;
                int length2 = cjVarArr == null ? 0 : cjVarArr.length;
                cj[] cjVarArr2 = new cj[(a4 + length2)];
                if (length2 != 0) {
                    System.arraycopy(this.c, 0, cjVarArr2, 0, length2);
                }
                while (length2 < cjVarArr2.length - 1) {
                    cjVarArr2[length2] = new cj();
                    iwVar.a(cjVarArr2[length2]);
                    iwVar.a();
                    length2++;
                }
                cjVarArr2[length2] = new cj();
                iwVar.a(cjVarArr2[length2]);
                this.c = cjVarArr2;
            } else if (a2 == 32) {
                this.e = Boolean.valueOf(iwVar.b());
            } else if (a2 == 40) {
                this.f = Boolean.valueOf(iwVar.b());
            } else if (!super.a(iwVar, a2)) {
                return this;
            }
        }
    }
}
