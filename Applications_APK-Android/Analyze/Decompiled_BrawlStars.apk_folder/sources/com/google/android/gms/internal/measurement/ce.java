package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.fq;

public final class ce {

    public static final class a extends fq<a, C0120a> implements gv {
        /* access modifiers changed from: private */
        public static final a zzauw = new a();
        private static volatile hd<a> zznw;
        private String zzauu = "";
        private long zzauv;
        private int zznr;

        private a() {
        }

        /* renamed from: com.google.android.gms.internal.measurement.ce$a$a  reason: collision with other inner class name */
        public static final class C0120a extends fq.a<a, C0120a> implements gv {
            private C0120a() {
                super(a.zzauw);
            }

            public final C0120a a(String str) {
                b();
                a.a((a) this.f2220a, str);
                return this;
            }

            public final C0120a a(long j) {
                b();
                a.a((a) this.f2220a, j);
                return this;
            }

            /* synthetic */ C0120a(byte b2) {
                this();
            }
        }

        public static C0120a a() {
            return (C0120a) ((fq.a) zzauw.a(fq.e.e));
        }

        /* access modifiers changed from: protected */
        public final Object a(int i) {
            switch (cf.f2112a[i - 1]) {
                case 1:
                    return new a();
                case 2:
                    return new C0120a((byte) 0);
                case 3:
                    return a(zzauw, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\b\u0000\u0002\u0002\u0001", new Object[]{"zznr", "zzauu", "zzauv"});
                case 4:
                    return zzauw;
                case 5:
                    hd<a> hdVar = zznw;
                    if (hdVar == null) {
                        synchronized (a.class) {
                            hdVar = zznw;
                            if (hdVar == null) {
                                hdVar = new fq.b<>(zzauw);
                                zznw = hdVar;
                            }
                        }
                    }
                    return hdVar;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            fq.a(a.class, zzauw);
        }

        static /* synthetic */ void a(a aVar, String str) {
            if (str != null) {
                aVar.zznr |= 1;
                aVar.zzauu = str;
                return;
            }
            throw new NullPointerException();
        }

        static /* synthetic */ void a(a aVar, long j) {
            aVar.zznr |= 2;
            aVar.zzauv = j;
        }
    }

    public static final class b extends fq<b, a> implements gv {
        /* access modifiers changed from: private */
        public static final b zzauz = new b();
        private static volatile hd<b> zznw;
        private int zzaux = 1;
        private fw<a> zzauy = hg.d();
        private int zznr;

        /* renamed from: com.google.android.gms.internal.measurement.ce$b$b  reason: collision with other inner class name */
        public enum C0121b implements ft {
            RADS(1),
            PROVISIONING(2);
            
            private static final fu<C0121b> c = new cg();
            private final int d;

            public final int a() {
                return this.d;
            }

            public static C0121b a(int i) {
                if (i == 1) {
                    return RADS;
                }
                if (i != 2) {
                    return null;
                }
                return PROVISIONING;
            }

            public static fv b() {
                return ch.f2113a;
            }

            private C0121b(int i) {
                this.d = i;
            }
        }

        private b() {
        }

        public static final class a extends fq.a<b, a> implements gv {
            private a() {
                super(b.zzauz);
            }

            public final a a(a aVar) {
                b();
                b.a((b) this.f2220a, aVar);
                return this;
            }

            /* synthetic */ a(byte b2) {
                this();
            }
        }

        public static a a() {
            return (a) ((fq.a) zzauz.a(fq.e.e));
        }

        /* access modifiers changed from: protected */
        public final Object a(int i) {
            switch (cf.f2112a[i - 1]) {
                case 1:
                    return new b();
                case 2:
                    return new a((byte) 0);
                case 3:
                    return a(zzauz, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0001\u0000\u0001\f\u0000\u0002\u001b", new Object[]{"zznr", "zzaux", C0121b.b(), "zzauy", a.class});
                case 4:
                    return zzauz;
                case 5:
                    hd<b> hdVar = zznw;
                    if (hdVar == null) {
                        synchronized (b.class) {
                            hdVar = zznw;
                            if (hdVar == null) {
                                hdVar = new fq.b<>(zzauz);
                                zznw = hdVar;
                            }
                        }
                    }
                    return hdVar;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        public static hd<b> b() {
            return (hd) zzauz.a(fq.e.g);
        }

        static {
            fq.a(b.class, zzauz);
        }

        static /* synthetic */ void a(b bVar, a aVar) {
            if (aVar != null) {
                if (!bVar.zzauy.a()) {
                    fw<a> fwVar = bVar.zzauy;
                    int size = fwVar.size();
                    bVar.zzauy = fwVar.a(size == 0 ? 10 : size << 1);
                }
                bVar.zzauy.add(aVar);
                return;
            }
            throw new NullPointerException();
        }
    }
}
