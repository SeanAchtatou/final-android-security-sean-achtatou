package com.google.android.gms.internal.instantapps;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class h implements Parcelable.Creator<zzas> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zzas[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        String str = null;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        boolean z4 = false;
        boolean z5 = false;
        boolean z6 = false;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    z = SafeParcelReader.c(parcel, readInt);
                    break;
                case 2:
                    str = SafeParcelReader.l(parcel, readInt);
                    break;
                case 3:
                    z2 = SafeParcelReader.c(parcel, readInt);
                    break;
                case 4:
                    z3 = SafeParcelReader.c(parcel, readInt);
                    break;
                case 5:
                    z4 = SafeParcelReader.c(parcel, readInt);
                    break;
                case 6:
                    z5 = SafeParcelReader.c(parcel, readInt);
                    break;
                case 7:
                    z6 = SafeParcelReader.c(parcel, readInt);
                    break;
                default:
                    SafeParcelReader.b(parcel, readInt);
                    break;
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new zzas(z, str, z2, z3, z4, z5, z6);
    }
}
