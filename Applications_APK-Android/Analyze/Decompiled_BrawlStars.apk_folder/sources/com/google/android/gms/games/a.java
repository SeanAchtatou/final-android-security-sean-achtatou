package com.google.android.gms.games;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.d;
import com.google.android.gms.common.api.internal.c;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.internal.games.aa;
import com.google.android.gms.internal.games.g;
import com.google.android.gms.internal.games.h;
import com.google.android.gms.internal.games.j;
import com.google.android.gms.internal.games.k;
import com.google.android.gms.internal.games.l;
import com.google.android.gms.internal.games.m;
import com.google.android.gms.internal.games.o;
import com.google.android.gms.internal.games.p;
import com.google.android.gms.internal.games.q;
import com.google.android.gms.internal.games.t;
import com.google.android.gms.internal.games.u;
import com.google.android.gms.internal.games.y;
import java.util.ArrayList;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    static final a.g<zze> f1792a = new a.g<>();

    /* renamed from: b  reason: collision with root package name */
    public static final Scope f1793b = new Scope("https://www.googleapis.com/auth/games");
    public static final Scope c = new Scope("https://www.googleapis.com/auth/games_lite");
    @Deprecated
    public static final com.google.android.gms.common.api.a<C0117a> d = new com.google.android.gms.common.api.a<>("Games.API", t, f1792a);
    public static final Scope e = new Scope("https://www.googleapis.com/auth/games.firstparty");
    @Deprecated
    public static final c f = new com.google.android.gms.internal.games.a();
    @Deprecated
    public static final com.google.android.gms.games.achievement.a g = new u();
    @Deprecated
    public static final com.google.android.gms.games.event.a h = new aa();
    @Deprecated
    public static final com.google.android.gms.games.a.a i = new com.google.android.gms.internal.games.c();
    @Deprecated
    public static final com.google.android.gms.games.multiplayer.a j = new com.google.android.gms.internal.games.b();
    @Deprecated
    public static final com.google.android.gms.games.multiplayer.turnbased.a k = new o();
    @Deprecated
    public static final com.google.android.gms.games.multiplayer.realtime.a l = new h();
    @Deprecated
    public static final g m = new com.google.android.gms.internal.games.f();
    @Deprecated
    public static final e n = new com.google.android.gms.internal.games.e();
    @Deprecated
    public static final com.google.android.gms.games.quest.a o = new g();
    @Deprecated
    public static final com.google.android.gms.games.request.a p = new j();
    @Deprecated
    public static final com.google.android.gms.games.snapshot.a q = new k();
    @Deprecated
    public static final com.google.android.gms.games.stats.a r = new m();
    @Deprecated
    public static final com.google.android.gms.games.video.a s = new p();
    private static final a.C0111a<zze, C0117a> t = new ad();
    private static final a.C0111a<zze, C0117a> u = new ae();
    private static final com.google.android.gms.common.api.a<C0117a> v = new com.google.android.gms.common.api.a<>("Games.API_1P", u, f1792a);
    private static final q w = new y();
    private static final com.google.android.gms.games.multiplayer.b x = new com.google.android.gms.internal.games.d();
    private static final t y = new l();

    @Deprecated
    /* renamed from: com.google.android.gms.games.a$a  reason: collision with other inner class name */
    public static final class C0117a implements a.d.b, a.d.e {

        /* renamed from: a  reason: collision with root package name */
        public final boolean f1794a;

        /* renamed from: b  reason: collision with root package name */
        public final boolean f1795b;
        public final int c;
        public final boolean d;
        public final int e;
        public final String f;
        public final ArrayList<String> g;
        public final boolean h;
        public final boolean i;
        public final boolean j;
        public final GoogleSignInAccount k;

        @Deprecated
        /* renamed from: com.google.android.gms.games.a$a$a  reason: collision with other inner class name */
        public static final class C0118a {

            /* renamed from: a  reason: collision with root package name */
            boolean f1796a;

            /* renamed from: b  reason: collision with root package name */
            boolean f1797b;
            int c;
            boolean d;
            int e;
            String f;
            ArrayList<String> g;
            boolean h;
            boolean i;
            boolean j;
            GoogleSignInAccount k;

            private C0118a() {
                this.f1796a = false;
                this.f1797b = true;
                this.c = 17;
                this.d = false;
                this.e = 4368;
                this.f = null;
                this.g = new ArrayList<>();
                this.h = false;
                this.i = false;
                this.j = false;
                this.k = null;
            }

            /* synthetic */ C0118a(byte b2) {
                this();
            }
        }

        private C0117a(boolean z, boolean z2, int i2, boolean z3, int i3, String str, ArrayList<String> arrayList, boolean z4, boolean z5, boolean z6, GoogleSignInAccount googleSignInAccount) {
            this.f1794a = z;
            this.f1795b = z2;
            this.c = i2;
            this.d = z3;
            this.e = i3;
            this.f = str;
            this.g = arrayList;
            this.h = z4;
            this.i = z5;
            this.j = z6;
            this.k = googleSignInAccount;
        }

        /* synthetic */ C0117a(boolean z, boolean z2, int i2, boolean z3, int i3, String str, ArrayList arrayList, boolean z4, boolean z5, boolean z6, GoogleSignInAccount googleSignInAccount, byte b2) {
            this(z, z2, i2, z3, i3, str, arrayList, z4, z5, z6, googleSignInAccount);
        }

        public final GoogleSignInAccount a() {
            return this.k;
        }

        public final boolean equals(Object obj) {
            String str;
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof C0117a)) {
                return false;
            }
            C0117a aVar = (C0117a) obj;
            if (this.f1794a == aVar.f1794a && this.f1795b == aVar.f1795b && this.c == aVar.c && this.d == aVar.d && this.e == aVar.e && ((str = this.f) != null ? str.equals(aVar.f) : aVar.f == null) && this.g.equals(aVar.g) && this.h == aVar.h && this.i == aVar.i && this.j == aVar.j) {
                GoogleSignInAccount googleSignInAccount = this.k;
                GoogleSignInAccount googleSignInAccount2 = aVar.k;
                return googleSignInAccount != null ? googleSignInAccount.equals(googleSignInAccount2) : googleSignInAccount2 == null;
            }
        }

        public final int hashCode() {
            int i2 = ((((((((((this.f1794a ? 1 : 0) + true) * 31) + (this.f1795b ? 1 : 0)) * 31) + this.c) * 31) + (this.d ? 1 : 0)) * 31) + this.e) * 31;
            String str = this.f;
            int i3 = 0;
            int hashCode = (((((((((i2 + (str == null ? 0 : str.hashCode())) * 31) + this.g.hashCode()) * 31) + (this.h ? 1 : 0)) * 31) + (this.i ? 1 : 0)) * 31) + (this.j ? 1 : 0)) * 31;
            GoogleSignInAccount googleSignInAccount = this.k;
            if (googleSignInAccount != null) {
                i3 = googleSignInAccount.hashCode();
            }
            return hashCode + i3;
        }
    }

    @Deprecated
    public interface b extends com.google.android.gms.common.api.g {
        String a();
    }

    public static abstract class c<R extends com.google.android.gms.common.api.g> extends c.a<R, zze> {
        public c(com.google.android.gms.common.api.d dVar) {
            super(a.f1792a, dVar);
        }
    }

    static abstract class e extends c<b> {
        private e(com.google.android.gms.common.api.d dVar) {
            super(dVar);
        }

        /* synthetic */ e(com.google.android.gms.common.api.d dVar, byte b2) {
            this(dVar);
        }

        public final /* synthetic */ com.google.android.gms.common.api.g c(Status status) {
            return new ah(status);
        }
    }

    static abstract class f extends c<Status> {
        private f(com.google.android.gms.common.api.d dVar) {
            super(dVar);
        }

        /* synthetic */ f(com.google.android.gms.common.api.d dVar, byte b2) {
            this(dVar);
        }

        public final /* synthetic */ com.google.android.gms.common.api.g c(Status status) {
            return status;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.l.a(java.lang.String, java.lang.Object):java.lang.String
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.l.a(java.lang.Object, java.lang.Object):T
      com.google.android.gms.common.internal.l.a(boolean, java.lang.Object):void
      com.google.android.gms.common.internal.l.a(java.lang.String, java.lang.Object):java.lang.String */
    @Deprecated
    public static com.google.android.gms.common.api.e<b> a(com.google.android.gms.common.api.d dVar, String str) {
        com.google.android.gms.common.internal.l.a(str, (Object) "Please provide a valid serverClientId");
        return dVar.a(new af(dVar, str));
    }

    @Deprecated
    public static com.google.android.gms.common.api.e<Status> b(com.google.android.gms.common.api.d dVar) {
        return dVar.a(new ag(dVar));
    }

    static abstract class d extends a.C0111a<zze, C0117a> {
        private d() {
        }

        /* synthetic */ d(byte b2) {
            this();
        }

        public final int a() {
            return 1;
        }

        public final /* synthetic */ a.f a(Context context, Looper looper, com.google.android.gms.common.internal.b bVar, Object obj, d.b bVar2, d.c cVar) {
            C0117a aVar;
            C0117a aVar2 = (C0117a) obj;
            if (aVar2 == null) {
                C0117a.C0118a aVar3 = new C0117a.C0118a((byte) 0);
                aVar = new C0117a(aVar3.f1796a, aVar3.f1797b, aVar3.c, aVar3.d, aVar3.e, aVar3.f, aVar3.g, aVar3.h, aVar3.i, aVar3.j, aVar3.k, (byte) 0);
            } else {
                aVar = aVar2;
            }
            return new zze(context, looper, bVar, aVar, bVar2, cVar);
        }
    }

    public static zze a(com.google.android.gms.common.api.d dVar) {
        com.google.android.gms.common.internal.l.b(dVar != null, "GoogleApiClient parameter is required.");
        com.google.android.gms.common.internal.l.a(dVar.e(), "GoogleApiClient must be connected.");
        com.google.android.gms.common.internal.l.a(dVar.a(d), "GoogleApiClient is not configured to use the Games Api. Pass Games.API into GoogleApiClient.Builder#addApi() to use this feature.");
        boolean b2 = dVar.b(d);
        if (!b2) {
            throw new IllegalStateException("GoogleApiClient has an optional Games.API and is not connected to Games. Use GoogleApiClient.hasConnectedApi(Games.API) to guard this call.");
        } else if (b2) {
            return (zze) dVar.a(f1792a);
        } else {
            return null;
        }
    }
}
