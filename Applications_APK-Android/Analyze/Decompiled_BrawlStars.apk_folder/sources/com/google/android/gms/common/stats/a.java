package com.google.android.gms.common.stats;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import com.google.android.gms.common.util.d;
import java.util.Collections;
import java.util.List;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f1657a = new Object();

    /* renamed from: b  reason: collision with root package name */
    private static volatile a f1658b;
    private static boolean c = false;
    private final List<String> d = Collections.EMPTY_LIST;
    private final List<String> e = Collections.EMPTY_LIST;
    private final List<String> f = Collections.EMPTY_LIST;
    private final List<String> g = Collections.EMPTY_LIST;

    public static a a() {
        if (f1658b == null) {
            synchronized (f1657a) {
                if (f1658b == null) {
                    f1658b = new a();
                }
            }
        }
        return f1658b;
    }

    private a() {
    }

    public static boolean a(Context context, Intent intent, ServiceConnection serviceConnection, int i) {
        boolean z;
        ComponentName component = intent.getComponent();
        if (component == null) {
            z = false;
        } else {
            z = d.a(context, component.getPackageName());
        }
        if (z) {
            return false;
        }
        return context.bindService(intent, serviceConnection, i);
    }

    public final boolean b(Context context, Intent intent, ServiceConnection serviceConnection, int i) {
        context.getClass().getName();
        return a(context, intent, serviceConnection, i);
    }

    public static void a(Context context, ServiceConnection serviceConnection) {
        context.unbindService(serviceConnection);
    }
}
