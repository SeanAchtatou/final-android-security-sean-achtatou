package com.google.android.gms.internal.a;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

final class h {

    /* renamed from: a  reason: collision with root package name */
    private final ConcurrentHashMap<i, List<Throwable>> f1870a = new ConcurrentHashMap<>(16, 0.75f, 10);

    /* renamed from: b  reason: collision with root package name */
    private final ReferenceQueue<Throwable> f1871b = new ReferenceQueue<>();

    h() {
    }

    public final List<Throwable> a(Throwable th) {
        Reference<? extends Throwable> poll = this.f1871b.poll();
        while (poll != null) {
            this.f1870a.remove(poll);
            poll = this.f1871b.poll();
        }
        List<Throwable> list = this.f1870a.get(new i(th, null));
        if (list != null) {
            return list;
        }
        Vector vector = new Vector(2);
        List<Throwable> putIfAbsent = this.f1870a.putIfAbsent(new i(th, this.f1871b), vector);
        return putIfAbsent == null ? vector : putIfAbsent;
    }
}
