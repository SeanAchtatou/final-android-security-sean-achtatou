package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.iz;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public final class ja<M extends iz<M>, T> {

    /* renamed from: a  reason: collision with root package name */
    protected final Class<T> f2305a;

    /* renamed from: b  reason: collision with root package name */
    public final int f2306b;
    protected final boolean c;
    private final int d;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ja)) {
            return false;
        }
        ja jaVar = (ja) obj;
        return this.d == jaVar.d && this.f2305a == jaVar.f2305a && this.f2306b == jaVar.f2306b && this.c == jaVar.c;
    }

    public final int hashCode() {
        return ((((((this.d + 1147) * 31) + this.f2305a.hashCode()) * 31) + this.f2306b) * 31) + (this.c ? 1 : 0);
    }

    /* access modifiers changed from: package-private */
    public final T a(List<jg> list) {
        if (list == null) {
            return null;
        }
        if (this.c) {
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < list.size(); i++) {
                jg jgVar = list.get(i);
                if (jgVar.f2314b.length != 0) {
                    byte[] bArr = jgVar.f2314b;
                    arrayList.add(a(iw.a(bArr, bArr.length)));
                }
            }
            int size = arrayList.size();
            if (size == 0) {
                return null;
            }
            Class<T> cls = this.f2305a;
            T cast = cls.cast(Array.newInstance(cls.getComponentType(), size));
            for (int i2 = 0; i2 < size; i2++) {
                Array.set(cast, i2, arrayList.get(i2));
            }
            return cast;
        } else if (list.isEmpty()) {
            return null;
        } else {
            Class<T> cls2 = this.f2305a;
            byte[] bArr2 = list.get(list.size() - 1).f2314b;
            return cls2.cast(a(iw.a(bArr2, bArr2.length)));
        }
    }

    private final Object a(iw iwVar) {
        Class componentType = this.c ? this.f2305a.getComponentType() : this.f2305a;
        try {
            int i = this.d;
            if (i == 10) {
                je jeVar = (je) componentType.newInstance();
                int i2 = this.f2306b >>> 3;
                if (iwVar.c < iwVar.d) {
                    iwVar.c++;
                    jeVar.a(iwVar);
                    iwVar.a((i2 << 3) | 4);
                    iwVar.c--;
                    return jeVar;
                }
                throw zzyh.d();
            } else if (i == 11) {
                je jeVar2 = (je) componentType.newInstance();
                iwVar.a(jeVar2);
                return jeVar2;
            } else {
                int i3 = this.d;
                StringBuilder sb = new StringBuilder(24);
                sb.append("Unknown type ");
                sb.append(i3);
                throw new IllegalArgumentException(sb.toString());
            }
        } catch (InstantiationException e) {
            String valueOf = String.valueOf(componentType);
            StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf).length() + 33);
            sb2.append("Error creating instance of class ");
            sb2.append(valueOf);
            throw new IllegalArgumentException(sb2.toString(), e);
        } catch (IllegalAccessException e2) {
            String valueOf2 = String.valueOf(componentType);
            StringBuilder sb3 = new StringBuilder(String.valueOf(valueOf2).length() + 33);
            sb3.append("Error creating instance of class ");
            sb3.append(valueOf2);
            throw new IllegalArgumentException(sb3.toString(), e2);
        } catch (IOException e3) {
            throw new IllegalArgumentException("Error reading extension field", e3);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Object obj, iy iyVar) {
        try {
            iyVar.b(this.f2306b);
            int i = this.d;
            if (i == 10) {
                ((je) obj).a(iyVar);
                iyVar.c(this.f2306b >>> 3, 4);
            } else if (i == 11) {
                iyVar.a((je) obj);
            } else {
                int i2 = this.d;
                StringBuilder sb = new StringBuilder(24);
                sb.append("Unknown type ");
                sb.append(i2);
                throw new IllegalArgumentException(sb.toString());
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    /* access modifiers changed from: protected */
    public final int a(Object obj) {
        int i = this.f2306b >>> 3;
        int i2 = this.d;
        if (i2 == 10) {
            return (iy.c(i << 3) << 1) + ((je) obj).e();
        }
        if (i2 == 11) {
            return iy.b(i, (je) obj);
        }
        StringBuilder sb = new StringBuilder(24);
        sb.append("Unknown type ");
        sb.append(i2);
        throw new IllegalArgumentException(sb.toString());
    }
}
