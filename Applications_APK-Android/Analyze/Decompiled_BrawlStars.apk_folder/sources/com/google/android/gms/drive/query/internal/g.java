package com.google.android.gms.drive.query.internal;

import com.google.android.gms.drive.metadata.a;
import java.util.List;

public interface g<F> {
    F a();

    F a(a<?> aVar);

    <T> F a(a aVar, Object obj);

    <T> F a(com.google.android.gms.drive.metadata.g gVar, Object obj);

    <T> F a(zzx zzx, a<T> aVar, T t);

    F a(zzx zzx, List list);

    F a(F f);

    F a(String str);

    F b();
}
