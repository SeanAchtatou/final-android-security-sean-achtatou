package com.google.android.gms.games.multiplayer.realtime;

import android.os.Parcel;
import android.os.Parcelable;

final class b implements Parcelable.Creator<RealTimeMessage> {
    b() {
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new RealTimeMessage(parcel, (byte) 0);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new RealTimeMessage[i];
    }
}
