package com.google.android.gms.common;

import com.google.android.gms.common.util.a;
import com.google.android.gms.common.util.j;
import java.util.concurrent.Callable;
import javax.annotation.CheckReturnValue;
import javax.annotation.Nullable;

@CheckReturnValue
class u {
    private static final u c = new u(true, null, null);

    /* renamed from: a  reason: collision with root package name */
    final boolean f1663a;

    /* renamed from: b  reason: collision with root package name */
    final Throwable f1664b;
    private final String d;

    u(boolean z, @Nullable String str, @Nullable Throwable th) {
        this.f1663a = z;
        this.d = str;
        this.f1664b = th;
    }

    static u a() {
        return c;
    }

    static u a(Callable<String> callable) {
        return new v(callable, (byte) 0);
    }

    static u a(String str) {
        return new u(false, str, null);
    }

    static u a(String str, Throwable th) {
        return new u(false, str, th);
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public String b() {
        return this.d;
    }

    static String a(String str, m mVar, boolean z, boolean z2) {
        return String.format("%s: pkg=%s, sha1=%s, atk=%s, ver=%s", z2 ? "debug cert rejected" : "not whitelisted", str, j.a(a.a("SHA-1").digest(mVar.c())), Boolean.valueOf(z), "12451009.false");
    }
}
