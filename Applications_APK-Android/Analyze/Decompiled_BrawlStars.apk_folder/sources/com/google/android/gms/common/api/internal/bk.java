package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.d;
import com.google.android.gms.common.api.g;

final class bk implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ g f1424a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ bj f1425b;

    bk(bj bjVar, g gVar) {
        this.f1425b = bjVar;
        this.f1424a = gVar;
    }

    public final void run() {
        try {
            BasePendingResult.c.set(true);
            this.f1425b.h.sendMessage(this.f1425b.h.obtainMessage(0, this.f1425b.f1422a.a()));
            BasePendingResult.c.set(false);
            bj.b(this.f1424a);
            d dVar = this.f1425b.g.get();
            if (dVar != null) {
                dVar.b(this.f1425b);
            }
        } catch (RuntimeException e) {
            this.f1425b.h.sendMessage(this.f1425b.h.obtainMessage(1, e));
            BasePendingResult.c.set(false);
            bj.b(this.f1424a);
            d dVar2 = this.f1425b.g.get();
            if (dVar2 != null) {
                dVar2.b(this.f1425b);
            }
        } catch (Throwable th) {
            BasePendingResult.c.set(false);
            bj.b(this.f1424a);
            d dVar3 = this.f1425b.g.get();
            if (dVar3 != null) {
                dVar3.b(this.f1425b);
            }
            throw th;
        }
    }
}
