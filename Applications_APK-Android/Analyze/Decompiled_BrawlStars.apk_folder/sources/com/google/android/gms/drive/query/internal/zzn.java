package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.drive.metadata.a;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public final class zzn<T> extends zza {
    public static final i CREATOR = new i();

    /* renamed from: a  reason: collision with root package name */
    private final MetadataBundle f1758a;

    /* renamed from: b  reason: collision with root package name */
    private final a<T> f1759b;

    zzn(MetadataBundle metadataBundle) {
        this.f1758a = metadataBundle;
        this.f1759b = f.a(metadataBundle);
    }

    public final <F> F a(g<F> gVar) {
        a<T> aVar = this.f1759b;
        return gVar.a(aVar, this.f1758a.a(aVar));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.metadata.internal.MetadataBundle, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = com.google.android.gms.common.internal.safeparcel.a.a(parcel, 20293);
        com.google.android.gms.common.internal.safeparcel.a.a(parcel, 1, (Parcelable) this.f1758a, i, false);
        com.google.android.gms.common.internal.safeparcel.a.b(parcel, a2);
    }
}
