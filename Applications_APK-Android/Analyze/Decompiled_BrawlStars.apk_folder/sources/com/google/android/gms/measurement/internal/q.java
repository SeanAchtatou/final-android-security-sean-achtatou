package com.google.android.gms.measurement.internal;

public final class q {

    /* renamed from: a  reason: collision with root package name */
    private final int f2578a;

    /* renamed from: b  reason: collision with root package name */
    private final boolean f2579b;
    private final boolean c;
    private final /* synthetic */ o d;

    q(o oVar, int i, boolean z, boolean z2) {
        this.d = oVar;
        this.f2578a = i;
        this.f2579b = z;
        this.c = z2;
    }

    public final void a(String str) {
        this.d.a(this.f2578a, this.f2579b, this.c, str, null, null, null);
    }

    public final void a(String str, Object obj) {
        this.d.a(this.f2578a, this.f2579b, this.c, str, obj, null, null);
    }

    public final void a(String str, Object obj, Object obj2) {
        this.d.a(this.f2578a, this.f2579b, this.c, str, obj, obj2, null);
    }

    public final void a(String str, Object obj, Object obj2, Object obj3) {
        this.d.a(this.f2578a, this.f2579b, this.c, str, obj, obj2, obj3);
    }
}
