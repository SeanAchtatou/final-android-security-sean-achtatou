package com.google.android.gms.measurement.internal;

import android.os.RemoteException;
import android.text.TextUtils;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;

final class cv implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ AtomicReference f2487a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f2488b;
    private final /* synthetic */ String c;
    private final /* synthetic */ String d;
    private final /* synthetic */ boolean e;
    private final /* synthetic */ zzk f;
    private final /* synthetic */ cl g;

    cv(cl clVar, AtomicReference atomicReference, String str, String str2, String str3, boolean z, zzk zzk) {
        this.g = clVar;
        this.f2487a = atomicReference;
        this.f2488b = str;
        this.c = str2;
        this.d = str3;
        this.e = z;
        this.f = zzk;
    }

    public final void run() {
        synchronized (this.f2487a) {
            try {
                zzaj zzaj = this.g.f2470b;
                if (zzaj == null) {
                    this.g.q().c.a("Failed to get user properties", o.a(this.f2488b), this.c, this.d);
                    this.f2487a.set(Collections.emptyList());
                    this.f2487a.notify();
                    return;
                }
                if (TextUtils.isEmpty(this.f2488b)) {
                    this.f2487a.set(zzaj.a(this.c, this.d, this.e, this.f));
                } else {
                    this.f2487a.set(zzaj.a(this.f2488b, this.c, this.d, this.e));
                }
                this.g.y();
                this.f2487a.notify();
            } catch (RemoteException e2) {
                try {
                    this.g.q().c.a("Failed to get user properties", o.a(this.f2488b), this.c, e2);
                    this.f2487a.set(Collections.emptyList());
                    this.f2487a.notify();
                } catch (Throwable th) {
                    this.f2487a.notify();
                    throw th;
                }
            }
        }
    }
}
