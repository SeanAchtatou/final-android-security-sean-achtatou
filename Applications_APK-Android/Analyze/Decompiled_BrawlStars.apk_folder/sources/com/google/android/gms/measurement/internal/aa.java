package com.google.android.gms.measurement.internal;

import android.content.SharedPreferences;
import com.google.android.gms.common.internal.l;

public final class aa {

    /* renamed from: a  reason: collision with root package name */
    private final String f2360a;

    /* renamed from: b  reason: collision with root package name */
    private final boolean f2361b = true;
    private boolean c;
    private boolean d;
    private final /* synthetic */ z e;

    public aa(z zVar, String str) {
        this.e = zVar;
        l.a(str);
        this.f2360a = str;
    }

    public final boolean a() {
        if (!this.c) {
            this.c = true;
            this.d = this.e.f().getBoolean(this.f2360a, this.f2361b);
        }
        return this.d;
    }

    public final void a(boolean z) {
        SharedPreferences.Editor edit = this.e.f().edit();
        edit.putBoolean(this.f2360a, z);
        edit.apply();
        this.d = z;
    }
}
