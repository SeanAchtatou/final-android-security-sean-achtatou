package com.google.android.gms.common.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.internal.common.a;
import com.google.android.gms.internal.common.zza;

public final class zzl extends zza implements IGmsCallbacks {
    zzl(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.IGmsCallbacks");
    }

    public final void a(int i, IBinder iBinder, Bundle bundle) throws RemoteException {
        Parcel s = s();
        s.writeInt(i);
        s.writeStrongBinder(iBinder);
        a.a(s, bundle);
        b(1, s);
    }

    public final void a(int i, Bundle bundle) throws RemoteException {
        Parcel s = s();
        s.writeInt(i);
        a.a(s, bundle);
        b(2, s);
    }

    public final void a(int i, IBinder iBinder, zzb zzb) throws RemoteException {
        Parcel s = s();
        s.writeInt(i);
        s.writeStrongBinder(iBinder);
        a.a(s, zzb);
        b(3, s);
    }
}
