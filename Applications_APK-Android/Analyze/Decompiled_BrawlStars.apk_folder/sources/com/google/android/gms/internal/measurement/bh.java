package com.google.android.gms.internal.measurement;

import android.text.TextUtils;
import com.google.android.gms.common.internal.l;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class bh {

    /* renamed from: a  reason: collision with root package name */
    final Map<String, String> f2073a;

    /* renamed from: b  reason: collision with root package name */
    final List<zzbr> f2074b;
    final long c;
    final long d;
    final int e;
    final boolean f;
    private final String g;

    public bh(q qVar, Map<String, String> map, long j, boolean z) {
        this(qVar, map, j, z, 0, 0, null);
    }

    public bh(q qVar, Map<String, String> map, long j, boolean z, long j2, int i) {
        this(qVar, map, j, z, j2, i, null);
    }

    public bh(q qVar, Map<String, String> map, long j, boolean z, long j2, int i, List<zzbr> list) {
        List<zzbr> list2;
        String str;
        String a2;
        String a3;
        l.a(qVar);
        l.a(map);
        this.d = j;
        this.f = z;
        this.c = j2;
        this.e = i;
        if (list != null) {
            list2 = list;
        } else {
            list2 = Collections.emptyList();
        }
        this.f2074b = list2;
        String str2 = null;
        if (list != null) {
            Iterator<zzbr> it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                zzbr next = it.next();
                if ("appendVersion".equals(next.f2344a)) {
                    str = next.f2345b;
                    break;
                }
            }
        }
        str = null;
        this.g = !TextUtils.isEmpty(str) ? str : str2;
        HashMap hashMap = new HashMap();
        for (Map.Entry next2 : map.entrySet()) {
            if (a(next2.getKey()) && (a3 = a(qVar, next2.getKey())) != null) {
                hashMap.put(a3, b(qVar, next2.getValue()));
            }
        }
        for (Map.Entry next3 : map.entrySet()) {
            if (!a(next3.getKey()) && (a2 = a(qVar, next3.getKey())) != null) {
                hashMap.put(a2, b(qVar, next3.getValue()));
            }
        }
        if (!TextUtils.isEmpty(this.g)) {
            bx.a(hashMap, "_v", this.g);
            if (this.g.equals("ma4.0.0") || this.g.equals("ma4.0.1")) {
                hashMap.remove("adid");
            }
        }
        this.f2073a = Collections.unmodifiableMap(hashMap);
    }

    private static boolean a(Object obj) {
        if (obj == null) {
            return false;
        }
        return obj.toString().startsWith("&");
    }

    private static String a(q qVar, Object obj) {
        if (obj == null) {
            return null;
        }
        String obj2 = obj.toString();
        if (obj2.startsWith("&")) {
            obj2 = obj2.substring(1);
        }
        int length = obj2.length();
        if (length > 256) {
            obj2 = obj2.substring(0, 256);
            qVar.c("Hit param name is too long and will be trimmed", Integer.valueOf(length), obj2);
        }
        if (TextUtils.isEmpty(obj2)) {
            return null;
        }
        return obj2;
    }

    private static String b(q qVar, Object obj) {
        String obj2 = obj == null ? "" : obj.toString();
        int length = obj2.length();
        if (length <= 8192) {
            return obj2;
        }
        String substring = obj2.substring(0, 8192);
        qVar.c("Hit param value is too long and will be trimmed", Integer.valueOf(length), substring);
        return substring;
    }

    /* access modifiers changed from: package-private */
    public final String a(String str, String str2) {
        l.a(str);
        l.b(!str.startsWith("&"), "Short param name required");
        String str3 = this.f2073a.get(str);
        return str3 != null ? str3 : str2;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ht=");
        sb.append(this.d);
        if (this.c != 0) {
            sb.append(", dbId=");
            sb.append(this.c);
        }
        if (this.e != 0) {
            sb.append(", appUID=");
            sb.append(this.e);
        }
        ArrayList arrayList = new ArrayList(this.f2073a.keySet());
        Collections.sort(arrayList);
        ArrayList arrayList2 = arrayList;
        int size = arrayList2.size();
        int i = 0;
        while (i < size) {
            Object obj = arrayList2.get(i);
            i++;
            String str = (String) obj;
            sb.append(", ");
            sb.append(str);
            sb.append("=");
            sb.append(this.f2073a.get(str));
        }
        return sb.toString();
    }
}
