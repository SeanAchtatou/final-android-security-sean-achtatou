package com.google.android.gms.internal.measurement;

final class eo extends et {
    private final int d;
    private final int e;

    eo(byte[] bArr, int i, int i2) {
        super(bArr);
        a(i, i + i2, bArr.length);
        this.d = i;
        this.e = i2;
    }

    public final byte a(int i) {
        int a2 = a();
        if (((a2 - (i + 1)) | i) >= 0) {
            return this.c[this.d + i];
        }
        if (i < 0) {
            StringBuilder sb = new StringBuilder(22);
            sb.append("Index < 0: ");
            sb.append(i);
            throw new ArrayIndexOutOfBoundsException(sb.toString());
        }
        StringBuilder sb2 = new StringBuilder(40);
        sb2.append("Index > length: ");
        sb2.append(i);
        sb2.append(", ");
        sb2.append(a2);
        throw new ArrayIndexOutOfBoundsException(sb2.toString());
    }

    /* access modifiers changed from: package-private */
    public final byte b(int i) {
        return this.c[this.d + i];
    }

    public final int a() {
        return this.e;
    }

    /* access modifiers changed from: protected */
    public final int d() {
        return this.d;
    }
}
