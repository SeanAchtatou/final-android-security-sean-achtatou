package com.google.android.gms.measurement.internal;

import android.content.Context;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.util.e;

public class bm implements bo {
    protected final ar r;

    bm(ar arVar) {
        l.a(arVar);
        this.r = arVar;
    }

    public el s() {
        return this.r.e;
    }

    public z r() {
        return this.r.b();
    }

    public o q() {
        return this.r.q();
    }

    public am p() {
        return this.r.p();
    }

    public ed o() {
        return this.r.e();
    }

    public m n() {
        return this.r.f();
    }

    public Context m() {
        return this.r.m();
    }

    public e l() {
        return this.r.l();
    }

    public b k() {
        return this.r.j();
    }

    public void c() {
        this.r.p().c();
    }

    public void b() {
        this.r.p().b();
    }

    public void a() {
        this.r.s();
    }
}
