package com.google.android.gms.internal.measurement;

import android.content.pm.ApplicationInfo;
import android.text.TextUtils;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.util.p;
import java.util.HashSet;
import java.util.Set;

public final class au {

    /* renamed from: a  reason: collision with root package name */
    private final t f2056a;

    /* renamed from: b  reason: collision with root package name */
    private volatile Boolean f2057b;
    private String c;
    private Set<Integer> d;

    protected au(t tVar) {
        l.a(tVar);
        this.f2056a = tVar;
    }

    public final boolean a() {
        if (this.f2057b == null) {
            synchronized (this) {
                if (this.f2057b == null) {
                    ApplicationInfo applicationInfo = this.f2056a.f2332a.getApplicationInfo();
                    String a2 = p.a();
                    if (applicationInfo != null) {
                        String str = applicationInfo.processName;
                        this.f2057b = Boolean.valueOf(str != null && str.equals(a2));
                    }
                    if ((this.f2057b == null || !this.f2057b.booleanValue()) && "com.google.android.gms.analytics".equals(a2)) {
                        this.f2057b = Boolean.TRUE;
                    }
                    if (this.f2057b == null) {
                        this.f2057b = Boolean.TRUE;
                        this.f2056a.a().f("My process not in the list of running processes");
                    }
                }
            }
        }
        return this.f2057b.booleanValue();
    }

    public static boolean b() {
        return ((Boolean) bc.f2065a.f2067a).booleanValue();
    }

    public static int c() {
        return ((Integer) bc.r.f2067a).intValue();
    }

    public static long d() {
        return ((Long) bc.f.f2067a).longValue();
    }

    public static long e() {
        return ((Long) bc.g.f2067a).longValue();
    }

    public static int f() {
        return ((Integer) bc.i.f2067a).intValue();
    }

    public static int g() {
        return ((Integer) bc.j.f2067a).intValue();
    }

    public static String h() {
        return (String) bc.l.f2067a;
    }

    public static String i() {
        return (String) bc.k.f2067a;
    }

    public static String j() {
        return (String) bc.m.f2067a;
    }

    public final Set<Integer> k() {
        String str;
        String str2 = (String) bc.u.f2067a;
        if (this.d == null || (str = this.c) == null || !str.equals(str2)) {
            String[] split = TextUtils.split(str2, ",");
            HashSet hashSet = new HashSet();
            for (String parseInt : split) {
                try {
                    hashSet.add(Integer.valueOf(Integer.parseInt(parseInt)));
                } catch (NumberFormatException unused) {
                }
            }
            this.c = str2;
            this.d = hashSet;
        }
        return this.d;
    }

    public static long l() {
        return ((Long) bc.y.f2067a).longValue();
    }
}
