package com.google.android.gms.measurement.internal;

import java.util.concurrent.Callable;

final class bg implements Callable<byte[]> {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zzag f2419a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f2420b;
    private final /* synthetic */ zzby c;

    bg(zzby zzby, zzag zzag, String str) {
        this.c = zzby;
        this.f2419a = zzag;
        this.f2420b = str;
    }

    public final /* synthetic */ Object call() throws Exception {
        this.c.f2597a.k();
        du a2 = this.c.f2597a;
        du.a(a2.f2527a);
        return a2.f2527a.a(this.f2419a, this.f2420b);
    }
}
