package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.l;

public final class v implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ String f2584a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ long f2585b;
    private final /* synthetic */ a c;

    public v(a aVar, String str, long j) {
        this.c = aVar;
        this.f2584a = str;
        this.f2585b = j;
    }

    public final void run() {
        a aVar = this.c;
        String str = this.f2584a;
        long j = this.f2585b;
        aVar.c();
        l.a(str);
        if (aVar.f2359b.isEmpty()) {
            aVar.c = j;
        }
        Integer num = aVar.f2359b.get(str);
        if (num != null) {
            aVar.f2359b.put(str, Integer.valueOf(num.intValue() + 1));
        } else if (aVar.f2359b.size() >= 100) {
            aVar.q().f.a("Too many ads visible");
        } else {
            aVar.f2359b.put(str, 1);
            aVar.f2358a.put(str, Long.valueOf(j));
        }
    }
}
