package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class zztv extends ei {

    /* renamed from: a  reason: collision with root package name */
    static final boolean f2349a = ih.a();
    private static final Logger c = Logger.getLogger(zztv.class.getName());

    /* renamed from: b  reason: collision with root package name */
    ez f2350b;

    static final class b extends a {
        private final ByteBuffer e;
        private int f;

        b(ByteBuffer byteBuffer) {
            super(byteBuffer.array(), byteBuffer.arrayOffset() + byteBuffer.position(), byteBuffer.remaining());
            this.e = byteBuffer;
            this.f = byteBuffer.position();
        }

        public final void h() {
            this.e.position(this.f + (this.d - this.c));
        }
    }

    public static int a() {
        return 4;
    }

    public static zztv a(byte[] bArr) {
        return new a(bArr, 0, bArr.length);
    }

    public static int b() {
        return 4;
    }

    public static int c() {
        return 8;
    }

    public static int d() {
        return 8;
    }

    public static int e() {
        return 4;
    }

    public static int e(long j) {
        int i;
        if ((-128 & j) == 0) {
            return 1;
        }
        if (j < 0) {
            return 10;
        }
        if ((-34359738368L & j) != 0) {
            i = 6;
            j >>>= 28;
        } else {
            i = 2;
        }
        if ((-2097152 & j) != 0) {
            i += 2;
            j >>>= 14;
        }
        return (j & -16384) != 0 ? i + 1 : i;
    }

    public static int f() {
        return 8;
    }

    public static int g() {
        return 1;
    }

    private static long g(long j) {
        return (j >> 63) ^ (j << 1);
    }

    public static int n(int i) {
        if ((i & -128) == 0) {
            return 1;
        }
        if ((i & -16384) == 0) {
            return 2;
        }
        if ((-2097152 & i) == 0) {
            return 3;
        }
        return (i & -268435456) == 0 ? 4 : 5;
    }

    private static int r(int i) {
        return (i >> 31) ^ (i << 1);
    }

    public abstract void a(byte b2) throws IOException;

    public abstract void a(int i) throws IOException;

    public abstract void a(int i, int i2) throws IOException;

    public abstract void a(int i, long j) throws IOException;

    public abstract void a(int i, ej ejVar) throws IOException;

    public abstract void a(int i, gt gtVar) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract void a(int i, gt gtVar, hj hjVar) throws IOException;

    public abstract void a(int i, String str) throws IOException;

    public abstract void a(int i, boolean z) throws IOException;

    public abstract void a(long j) throws IOException;

    public abstract void a(ej ejVar) throws IOException;

    public abstract void a(gt gtVar) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract void a(gt gtVar, hj hjVar) throws IOException;

    public abstract void a(String str) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract void a(byte[] bArr, int i) throws IOException;

    public abstract void b(int i) throws IOException;

    public abstract void b(int i, int i2) throws IOException;

    public abstract void b(int i, ej ejVar) throws IOException;

    public abstract void b(int i, gt gtVar) throws IOException;

    public abstract void b(byte[] bArr, int i, int i2) throws IOException;

    public abstract void c(int i, int i2) throws IOException;

    public abstract void c(int i, long j) throws IOException;

    public abstract void c(long j) throws IOException;

    public abstract void d(int i) throws IOException;

    public abstract void e(int i, int i2) throws IOException;

    public abstract void h() throws IOException;

    public abstract int i();

    public static class zzc extends IOException {
        zzc() {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.");
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        zzc(java.lang.String r3) {
            /*
                r2 = this;
                java.lang.String r3 = java.lang.String.valueOf(r3)
                int r0 = r3.length()
                java.lang.String r1 = "CodedOutputStream was writing to a flat byte array and ran out of space.: "
                if (r0 == 0) goto L_0x0011
                java.lang.String r3 = r1.concat(r3)
                goto L_0x0016
            L_0x0011:
                java.lang.String r3 = new java.lang.String
                r3.<init>(r1)
            L_0x0016:
                r2.<init>(r3)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zztv.zzc.<init>(java.lang.String):void");
        }

        zzc(Throwable th) {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.", th);
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        zzc(java.lang.String r3, java.lang.Throwable r4) {
            /*
                r2 = this;
                java.lang.String r3 = java.lang.String.valueOf(r3)
                int r0 = r3.length()
                java.lang.String r1 = "CodedOutputStream was writing to a flat byte array and ran out of space.: "
                if (r0 == 0) goto L_0x0011
                java.lang.String r3 = r1.concat(r3)
                goto L_0x0016
            L_0x0011:
                java.lang.String r3 = new java.lang.String
                r3.<init>(r1)
            L_0x0016:
                r2.<init>(r3, r4)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.zztv.zzc.<init>(java.lang.String, java.lang.Throwable):void");
        }
    }

    public static zztv a(ByteBuffer byteBuffer) {
        if (byteBuffer.hasArray()) {
            return new b(byteBuffer);
        }
        if (!byteBuffer.isDirect() || byteBuffer.isReadOnly()) {
            throw new IllegalArgumentException("ByteBuffer is read-only");
        } else if (ih.b()) {
            return new d(byteBuffer);
        } else {
            return new c(byteBuffer);
        }
    }

    static final class c extends zztv {
        private final ByteBuffer c;
        private final ByteBuffer d;
        private final int e;

        c(ByteBuffer byteBuffer) {
            super((byte) 0);
            this.c = byteBuffer;
            this.d = byteBuffer.duplicate().order(ByteOrder.LITTLE_ENDIAN);
            this.e = byteBuffer.position();
        }

        public final void a(int i, int i2) throws IOException {
            b((i << 3) | i2);
        }

        public final void b(int i, int i2) throws IOException {
            a(i, 0);
            a(i2);
        }

        public final void c(int i, int i2) throws IOException {
            a(i, 0);
            b(i2);
        }

        public final void e(int i, int i2) throws IOException {
            a(i, 5);
            d(i2);
        }

        public final void a(int i, long j) throws IOException {
            a(i, 0);
            a(j);
        }

        public final void c(int i, long j) throws IOException {
            a(i, 1);
            c(j);
        }

        public final void a(int i, boolean z) throws IOException {
            a(i, 0);
            a(z ? (byte) 1 : 0);
        }

        public final void a(int i, String str) throws IOException {
            a(i, 2);
            a(str);
        }

        public final void a(int i, ej ejVar) throws IOException {
            a(i, 2);
            a(ejVar);
        }

        public final void a(int i, gt gtVar) throws IOException {
            a(i, 2);
            a(gtVar);
        }

        /* access modifiers changed from: package-private */
        public final void a(int i, gt gtVar, hj hjVar) throws IOException {
            a(i, 2);
            a(gtVar, hjVar);
        }

        public final void b(int i, gt gtVar) throws IOException {
            a(1, 3);
            c(2, i);
            a(3, gtVar);
            a(1, 4);
        }

        public final void b(int i, ej ejVar) throws IOException {
            a(1, 3);
            c(2, i);
            a(3, ejVar);
            a(1, 4);
        }

        public final void a(gt gtVar) throws IOException {
            b(gtVar.h());
            gtVar.a(this);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.internal.measurement.hj.a(java.lang.Object, com.google.android.gms.internal.measurement.iv):void
         arg types: [com.google.android.gms.internal.measurement.gt, com.google.android.gms.internal.measurement.ez]
         candidates:
          com.google.android.gms.internal.measurement.hj.a(java.lang.Object, java.lang.Object):boolean
          com.google.android.gms.internal.measurement.hj.a(java.lang.Object, com.google.android.gms.internal.measurement.iv):void */
        /* access modifiers changed from: package-private */
        public final void a(gt gtVar, hj hjVar) throws IOException {
            eb ebVar = (eb) gtVar;
            int e2 = ebVar.e();
            if (e2 == -1) {
                e2 = hjVar.b(ebVar);
                ebVar.b(e2);
            }
            b(e2);
            hjVar.a((Object) gtVar, (iv) this.f2350b);
        }

        public final void a(byte b2) throws IOException {
            try {
                this.d.put(b2);
            } catch (BufferOverflowException e2) {
                throw new zzc(e2);
            }
        }

        public final void a(ej ejVar) throws IOException {
            b(ejVar.a());
            ejVar.a(this);
        }

        public final void a(byte[] bArr, int i) throws IOException {
            b(i);
            b(bArr, 0, i);
        }

        public final void a(int i) throws IOException {
            if (i >= 0) {
                b(i);
            } else {
                a((long) i);
            }
        }

        public final void b(int i) throws IOException {
            while ((i & -128) != 0) {
                this.d.put((byte) ((i & 127) | 128));
                i >>>= 7;
            }
            try {
                this.d.put((byte) i);
            } catch (BufferOverflowException e2) {
                throw new zzc(e2);
            }
        }

        public final void d(int i) throws IOException {
            try {
                this.d.putInt(i);
            } catch (BufferOverflowException e2) {
                throw new zzc(e2);
            }
        }

        public final void a(long j) throws IOException {
            while ((-128 & j) != 0) {
                this.d.put((byte) ((((int) j) & 127) | 128));
                j >>>= 7;
            }
            try {
                this.d.put((byte) ((int) j));
            } catch (BufferOverflowException e2) {
                throw new zzc(e2);
            }
        }

        public final void c(long j) throws IOException {
            try {
                this.d.putLong(j);
            } catch (BufferOverflowException e2) {
                throw new zzc(e2);
            }
        }

        public final void b(byte[] bArr, int i, int i2) throws IOException {
            try {
                this.d.put(bArr, i, i2);
            } catch (IndexOutOfBoundsException e2) {
                throw new zzc(e2);
            } catch (BufferOverflowException e3) {
                throw new zzc(e3);
            }
        }

        public final void a(byte[] bArr, int i, int i2) throws IOException {
            b(bArr, i, i2);
        }

        public final void a(String str) throws IOException {
            int position = this.d.position();
            try {
                int n = n(str.length() * 3);
                int n2 = n(str.length());
                if (n2 == n) {
                    int position2 = this.d.position() + n2;
                    this.d.position(position2);
                    c(str);
                    int position3 = this.d.position();
                    this.d.position(position);
                    b(position3 - position2);
                    this.d.position(position3);
                    return;
                }
                b(ij.a(str));
                c(str);
            } catch (in e2) {
                this.d.position(position);
                a(str, e2);
            } catch (IllegalArgumentException e3) {
                throw new zzc(e3);
            }
        }

        public final void h() {
            this.c.position(this.d.position());
        }

        public final int i() {
            return this.d.remaining();
        }

        private final void c(String str) throws IOException {
            try {
                ij.a(str, this.d);
            } catch (IndexOutOfBoundsException e2) {
                throw new zzc(e2);
            }
        }
    }

    static final class d extends zztv {
        private final ByteBuffer c;
        private final ByteBuffer d;
        private final long e;
        private final long f;
        private final long g;
        private final long h = (this.g - 10);
        private long i = this.f;

        d(ByteBuffer byteBuffer) {
            super((byte) 0);
            this.c = byteBuffer;
            this.d = byteBuffer.duplicate().order(ByteOrder.LITTLE_ENDIAN);
            this.e = ih.a(byteBuffer);
            this.f = this.e + ((long) byteBuffer.position());
            this.g = this.e + ((long) byteBuffer.limit());
        }

        public final void a(int i2, int i3) throws IOException {
            b((i2 << 3) | i3);
        }

        public final void b(int i2, int i3) throws IOException {
            a(i2, 0);
            a(i3);
        }

        public final void c(int i2, int i3) throws IOException {
            a(i2, 0);
            b(i3);
        }

        public final void e(int i2, int i3) throws IOException {
            a(i2, 5);
            d(i3);
        }

        public final void a(int i2, long j) throws IOException {
            a(i2, 0);
            a(j);
        }

        public final void c(int i2, long j) throws IOException {
            a(i2, 1);
            c(j);
        }

        public final void a(int i2, boolean z) throws IOException {
            a(i2, 0);
            a(z ? (byte) 1 : 0);
        }

        public final void a(int i2, String str) throws IOException {
            a(i2, 2);
            a(str);
        }

        public final void a(int i2, ej ejVar) throws IOException {
            a(i2, 2);
            a(ejVar);
        }

        public final void a(int i2, gt gtVar) throws IOException {
            a(i2, 2);
            a(gtVar);
        }

        /* access modifiers changed from: package-private */
        public final void a(int i2, gt gtVar, hj hjVar) throws IOException {
            a(i2, 2);
            a(gtVar, hjVar);
        }

        public final void b(int i2, gt gtVar) throws IOException {
            a(1, 3);
            c(2, i2);
            a(3, gtVar);
            a(1, 4);
        }

        public final void b(int i2, ej ejVar) throws IOException {
            a(1, 3);
            c(2, i2);
            a(3, ejVar);
            a(1, 4);
        }

        public final void a(gt gtVar) throws IOException {
            b(gtVar.h());
            gtVar.a(this);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.internal.measurement.hj.a(java.lang.Object, com.google.android.gms.internal.measurement.iv):void
         arg types: [com.google.android.gms.internal.measurement.gt, com.google.android.gms.internal.measurement.ez]
         candidates:
          com.google.android.gms.internal.measurement.hj.a(java.lang.Object, java.lang.Object):boolean
          com.google.android.gms.internal.measurement.hj.a(java.lang.Object, com.google.android.gms.internal.measurement.iv):void */
        /* access modifiers changed from: package-private */
        public final void a(gt gtVar, hj hjVar) throws IOException {
            eb ebVar = (eb) gtVar;
            int e2 = ebVar.e();
            if (e2 == -1) {
                e2 = hjVar.b(ebVar);
                ebVar.b(e2);
            }
            b(e2);
            hjVar.a((Object) gtVar, (iv) this.f2350b);
        }

        public final void a(byte b2) throws IOException {
            long j = this.i;
            if (j < this.g) {
                this.i = 1 + j;
                ih.a(j, b2);
                return;
            }
            throw new zzc(String.format("Pos: %d, limit: %d, len: %d", Long.valueOf(j), Long.valueOf(this.g), 1));
        }

        public final void a(ej ejVar) throws IOException {
            b(ejVar.a());
            ejVar.a(this);
        }

        public final void a(byte[] bArr, int i2) throws IOException {
            b(i2);
            b(bArr, 0, i2);
        }

        public final void a(int i2) throws IOException {
            if (i2 >= 0) {
                b(i2);
            } else {
                a((long) i2);
            }
        }

        public final void b(int i2) throws IOException {
            if (this.i <= this.h) {
                while ((i2 & -128) != 0) {
                    long j = this.i;
                    this.i = j + 1;
                    ih.a(j, (byte) ((i2 & 127) | 128));
                    i2 >>>= 7;
                }
                long j2 = this.i;
                this.i = 1 + j2;
                ih.a(j2, (byte) i2);
                return;
            }
            while (true) {
                long j3 = this.i;
                if (j3 >= this.g) {
                    throw new zzc(String.format("Pos: %d, limit: %d, len: %d", Long.valueOf(j3), Long.valueOf(this.g), 1));
                } else if ((i2 & -128) == 0) {
                    this.i = 1 + j3;
                    ih.a(j3, (byte) i2);
                    return;
                } else {
                    this.i = j3 + 1;
                    ih.a(j3, (byte) ((i2 & 127) | 128));
                    i2 >>>= 7;
                }
            }
        }

        public final void d(int i2) throws IOException {
            this.d.putInt((int) (this.i - this.e), i2);
            this.i += 4;
        }

        public final void a(long j) throws IOException {
            if (this.i <= this.h) {
                while ((j & -128) != 0) {
                    long j2 = this.i;
                    this.i = j2 + 1;
                    ih.a(j2, (byte) ((((int) j) & 127) | 128));
                    j >>>= 7;
                }
                long j3 = this.i;
                this.i = 1 + j3;
                ih.a(j3, (byte) ((int) j));
                return;
            }
            while (true) {
                long j4 = this.i;
                if (j4 >= this.g) {
                    throw new zzc(String.format("Pos: %d, limit: %d, len: %d", Long.valueOf(j4), Long.valueOf(this.g), 1));
                } else if ((j & -128) == 0) {
                    this.i = 1 + j4;
                    ih.a(j4, (byte) ((int) j));
                    return;
                } else {
                    this.i = j4 + 1;
                    ih.a(j4, (byte) ((((int) j) & 127) | 128));
                    j >>>= 7;
                }
            }
        }

        public final void c(long j) throws IOException {
            this.d.putLong((int) (this.i - this.e), j);
            this.i += 8;
        }

        public final void b(byte[] bArr, int i2, int i3) throws IOException {
            if (bArr != null && i2 >= 0 && i3 >= 0 && bArr.length - i3 >= i2) {
                long j = (long) i3;
                long j2 = this.i;
                if (this.g - j >= j2) {
                    ih.a(bArr, (long) i2, j2, j);
                    this.i += j;
                    return;
                }
            }
            if (bArr == null) {
                throw new NullPointerException("value");
            }
            throw new zzc(String.format("Pos: %d, limit: %d, len: %d", Long.valueOf(this.i), Long.valueOf(this.g), Integer.valueOf(i3)));
        }

        public final void a(byte[] bArr, int i2, int i3) throws IOException {
            b(bArr, i2, i3);
        }

        public final void a(String str) throws IOException {
            long j = this.i;
            try {
                int n = n(str.length() * 3);
                int n2 = n(str.length());
                if (n2 == n) {
                    int i2 = ((int) (this.i - this.e)) + n2;
                    this.d.position(i2);
                    ij.a(str, this.d);
                    int position = this.d.position() - i2;
                    b(position);
                    this.i += (long) position;
                    return;
                }
                int a2 = ij.a(str);
                b(a2);
                g(this.i);
                ij.a(str, this.d);
                this.i += (long) a2;
            } catch (in e2) {
                this.i = j;
                g(this.i);
                a(str, e2);
            } catch (IllegalArgumentException e3) {
                throw new zzc(e3);
            } catch (IndexOutOfBoundsException e4) {
                throw new zzc(e4);
            }
        }

        public final void h() {
            this.c.position((int) (this.i - this.e));
        }

        public final int i() {
            return (int) (this.g - this.i);
        }

        private final void g(long j) {
            this.d.position((int) (j - this.e));
        }
    }

    static class a extends zztv {
        final int c;
        int d;
        private final byte[] e;
        private final int f;

        a(byte[] bArr, int i, int i2) {
            super((byte) 0);
            if (bArr != null) {
                int i3 = i + i2;
                if ((i | i2 | (bArr.length - i3)) >= 0) {
                    this.e = bArr;
                    this.c = i;
                    this.d = i;
                    this.f = i3;
                    return;
                }
                throw new IllegalArgumentException(String.format("Array range is invalid. Buffer.length=%d, offset=%d, length=%d", Integer.valueOf(bArr.length), Integer.valueOf(i), Integer.valueOf(i2)));
            }
            throw new NullPointerException("buffer");
        }

        public void h() {
        }

        public final void a(int i, int i2) throws IOException {
            b((i << 3) | i2);
        }

        public final void b(int i, int i2) throws IOException {
            a(i, 0);
            a(i2);
        }

        public final void c(int i, int i2) throws IOException {
            a(i, 0);
            b(i2);
        }

        public final void e(int i, int i2) throws IOException {
            a(i, 5);
            d(i2);
        }

        public final void a(int i, long j) throws IOException {
            a(i, 0);
            a(j);
        }

        public final void c(int i, long j) throws IOException {
            a(i, 1);
            c(j);
        }

        public final void a(int i, boolean z) throws IOException {
            a(i, 0);
            a(z ? (byte) 1 : 0);
        }

        public final void a(int i, String str) throws IOException {
            a(i, 2);
            a(str);
        }

        public final void a(int i, ej ejVar) throws IOException {
            a(i, 2);
            a(ejVar);
        }

        public final void a(ej ejVar) throws IOException {
            b(ejVar.a());
            ejVar.a(this);
        }

        public final void a(byte[] bArr, int i) throws IOException {
            b(i);
            b(bArr, 0, i);
        }

        public final void a(int i, gt gtVar) throws IOException {
            a(i, 2);
            a(gtVar);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.internal.measurement.hj.a(java.lang.Object, com.google.android.gms.internal.measurement.iv):void
         arg types: [com.google.android.gms.internal.measurement.gt, com.google.android.gms.internal.measurement.ez]
         candidates:
          com.google.android.gms.internal.measurement.hj.a(java.lang.Object, java.lang.Object):boolean
          com.google.android.gms.internal.measurement.hj.a(java.lang.Object, com.google.android.gms.internal.measurement.iv):void */
        /* access modifiers changed from: package-private */
        public final void a(int i, gt gtVar, hj hjVar) throws IOException {
            a(i, 2);
            eb ebVar = (eb) gtVar;
            int e2 = ebVar.e();
            if (e2 == -1) {
                e2 = hjVar.b(ebVar);
                ebVar.b(e2);
            }
            b(e2);
            hjVar.a((Object) gtVar, (iv) this.f2350b);
        }

        public final void b(int i, gt gtVar) throws IOException {
            a(1, 3);
            c(2, i);
            a(3, gtVar);
            a(1, 4);
        }

        public final void b(int i, ej ejVar) throws IOException {
            a(1, 3);
            c(2, i);
            a(3, ejVar);
            a(1, 4);
        }

        public final void a(gt gtVar) throws IOException {
            b(gtVar.h());
            gtVar.a(this);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.internal.measurement.hj.a(java.lang.Object, com.google.android.gms.internal.measurement.iv):void
         arg types: [com.google.android.gms.internal.measurement.gt, com.google.android.gms.internal.measurement.ez]
         candidates:
          com.google.android.gms.internal.measurement.hj.a(java.lang.Object, java.lang.Object):boolean
          com.google.android.gms.internal.measurement.hj.a(java.lang.Object, com.google.android.gms.internal.measurement.iv):void */
        /* access modifiers changed from: package-private */
        public final void a(gt gtVar, hj hjVar) throws IOException {
            eb ebVar = (eb) gtVar;
            int e2 = ebVar.e();
            if (e2 == -1) {
                e2 = hjVar.b(ebVar);
                ebVar.b(e2);
            }
            b(e2);
            hjVar.a((Object) gtVar, (iv) this.f2350b);
        }

        public final void a(byte b2) throws IOException {
            try {
                byte[] bArr = this.e;
                int i = this.d;
                this.d = i + 1;
                bArr[i] = b2;
            } catch (IndexOutOfBoundsException e2) {
                throw new zzc(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.d), Integer.valueOf(this.f), 1), e2);
            }
        }

        public final void a(int i) throws IOException {
            if (i >= 0) {
                b(i);
            } else {
                a((long) i);
            }
        }

        public final void d(int i) throws IOException {
            try {
                byte[] bArr = this.e;
                int i2 = this.d;
                this.d = i2 + 1;
                bArr[i2] = (byte) i;
                byte[] bArr2 = this.e;
                int i3 = this.d;
                this.d = i3 + 1;
                bArr2[i3] = (byte) (i >> 8);
                byte[] bArr3 = this.e;
                int i4 = this.d;
                this.d = i4 + 1;
                bArr3[i4] = (byte) (i >> 16);
                byte[] bArr4 = this.e;
                int i5 = this.d;
                this.d = i5 + 1;
                bArr4[i5] = (byte) (i >>> 24);
            } catch (IndexOutOfBoundsException e2) {
                throw new zzc(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.d), Integer.valueOf(this.f), 1), e2);
            }
        }

        public final void c(long j) throws IOException {
            try {
                byte[] bArr = this.e;
                int i = this.d;
                this.d = i + 1;
                bArr[i] = (byte) ((int) j);
                byte[] bArr2 = this.e;
                int i2 = this.d;
                this.d = i2 + 1;
                bArr2[i2] = (byte) ((int) (j >> 8));
                byte[] bArr3 = this.e;
                int i3 = this.d;
                this.d = i3 + 1;
                bArr3[i3] = (byte) ((int) (j >> 16));
                byte[] bArr4 = this.e;
                int i4 = this.d;
                this.d = i4 + 1;
                bArr4[i4] = (byte) ((int) (j >> 24));
                byte[] bArr5 = this.e;
                int i5 = this.d;
                this.d = i5 + 1;
                bArr5[i5] = (byte) ((int) (j >> 32));
                byte[] bArr6 = this.e;
                int i6 = this.d;
                this.d = i6 + 1;
                bArr6[i6] = (byte) ((int) (j >> 40));
                byte[] bArr7 = this.e;
                int i7 = this.d;
                this.d = i7 + 1;
                bArr7[i7] = (byte) ((int) (j >> 48));
                byte[] bArr8 = this.e;
                int i8 = this.d;
                this.d = i8 + 1;
                bArr8[i8] = (byte) ((int) (j >> 56));
            } catch (IndexOutOfBoundsException e2) {
                throw new zzc(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.d), Integer.valueOf(this.f), 1), e2);
            }
        }

        public final void b(byte[] bArr, int i, int i2) throws IOException {
            try {
                System.arraycopy(bArr, i, this.e, this.d, i2);
                this.d += i2;
            } catch (IndexOutOfBoundsException e2) {
                throw new zzc(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.d), Integer.valueOf(this.f), Integer.valueOf(i2)), e2);
            }
        }

        public final void a(byte[] bArr, int i, int i2) throws IOException {
            b(bArr, i, i2);
        }

        public final void a(String str) throws IOException {
            int i = this.d;
            try {
                int n = n(str.length() * 3);
                int n2 = n(str.length());
                if (n2 == n) {
                    this.d = i + n2;
                    int a2 = ij.a(str, this.e, this.d, i());
                    this.d = i;
                    b((a2 - i) - n2);
                    this.d = a2;
                    return;
                }
                b(ij.a(str));
                this.d = ij.a(str, this.e, this.d, i());
            } catch (in e2) {
                this.d = i;
                a(str, e2);
            } catch (IndexOutOfBoundsException e3) {
                throw new zzc(e3);
            }
        }

        public final int i() {
            return this.f - this.d;
        }

        public final void b(int i) throws IOException {
            if (!zztv.f2349a || i() < 10) {
                while ((i & -128) != 0) {
                    byte[] bArr = this.e;
                    int i2 = this.d;
                    this.d = i2 + 1;
                    bArr[i2] = (byte) ((i & 127) | 128);
                    i >>>= 7;
                }
                try {
                    byte[] bArr2 = this.e;
                    int i3 = this.d;
                    this.d = i3 + 1;
                    bArr2[i3] = (byte) i;
                } catch (IndexOutOfBoundsException e2) {
                    throw new zzc(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.d), Integer.valueOf(this.f), 1), e2);
                }
            } else {
                while ((i & -128) != 0) {
                    byte[] bArr3 = this.e;
                    int i4 = this.d;
                    this.d = i4 + 1;
                    ih.a(bArr3, (long) i4, (byte) ((i & 127) | 128));
                    i >>>= 7;
                }
                byte[] bArr4 = this.e;
                int i5 = this.d;
                this.d = i5 + 1;
                ih.a(bArr4, (long) i5, (byte) i);
            }
        }

        public final void a(long j) throws IOException {
            if (!zztv.f2349a || i() < 10) {
                while ((j & -128) != 0) {
                    byte[] bArr = this.e;
                    int i = this.d;
                    this.d = i + 1;
                    bArr[i] = (byte) ((((int) j) & 127) | 128);
                    j >>>= 7;
                }
                try {
                    byte[] bArr2 = this.e;
                    int i2 = this.d;
                    this.d = i2 + 1;
                    bArr2[i2] = (byte) ((int) j);
                } catch (IndexOutOfBoundsException e2) {
                    throw new zzc(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.d), Integer.valueOf(this.f), 1), e2);
                }
            } else {
                while ((j & -128) != 0) {
                    byte[] bArr3 = this.e;
                    int i3 = this.d;
                    this.d = i3 + 1;
                    ih.a(bArr3, (long) i3, (byte) ((((int) j) & 127) | 128));
                    j >>>= 7;
                }
                byte[] bArr4 = this.e;
                int i4 = this.d;
                this.d = i4 + 1;
                ih.a(bArr4, (long) i4, (byte) ((int) j));
            }
        }
    }

    private zztv() {
    }

    public final void d(int i, int i2) throws IOException {
        c(i, r(i2));
    }

    public final void b(int i, long j) throws IOException {
        a(i, g(j));
    }

    public final void a(int i, float f) throws IOException {
        e(i, Float.floatToRawIntBits(f));
    }

    public final void a(int i, double d2) throws IOException {
        c(i, Double.doubleToRawLongBits(d2));
    }

    public final void c(int i) throws IOException {
        b(r(i));
    }

    public final void b(long j) throws IOException {
        a(g(j));
    }

    public final void a(float f) throws IOException {
        d(Float.floatToRawIntBits(f));
    }

    public final void a(double d2) throws IOException {
        c(Double.doubleToRawLongBits(d2));
    }

    public final void a(boolean z) throws IOException {
        a(z ? (byte) 1 : 0);
    }

    public static int l(int i) {
        return n(i << 3);
    }

    public static int m(int i) {
        if (i >= 0) {
            return n(i);
        }
        return 10;
    }

    public static int o(int i) {
        return n(r(i));
    }

    public static int d(long j) {
        return e(j);
    }

    public static int f(long j) {
        return e(g(j));
    }

    public static int p(int i) {
        return m(i);
    }

    public static int b(String str) {
        int i;
        try {
            i = ij.a(str);
        } catch (in unused) {
            i = str.getBytes(fs.f2226a).length;
        }
        return n(i) + i;
    }

    public static int a(gb gbVar) {
        int b2 = gbVar.b();
        return n(b2) + b2;
    }

    public static int b(ej ejVar) {
        int a2 = ejVar.a();
        return n(a2) + a2;
    }

    public static int b(byte[] bArr) {
        int length = bArr.length;
        return n(length) + length;
    }

    public static int b(gt gtVar) {
        int h = gtVar.h();
        return n(h) + h;
    }

    static int b(gt gtVar, hj hjVar) {
        eb ebVar = (eb) gtVar;
        int e = ebVar.e();
        if (e == -1) {
            e = hjVar.b(ebVar);
            ebVar.b(e);
        }
        return n(e) + e;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.logp(java.util.logging.Level, java.lang.String, java.lang.String, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.String, java.lang.String, com.google.android.gms.internal.measurement.in]
     candidates:
      ClspMth{java.util.logging.Logger.logp(java.util.logging.Level, java.lang.String, java.lang.String, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.logp(java.util.logging.Level, java.lang.String, java.lang.String, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.logp(java.util.logging.Level, java.lang.String, java.lang.String, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.logp(java.util.logging.Level, java.lang.String, java.lang.String, java.lang.String, java.lang.Throwable):void} */
    /* access modifiers changed from: package-private */
    public final void a(String str, in inVar) throws IOException {
        c.logp(Level.WARNING, "com.google.protobuf.CodedOutputStream", "inefficientWriteStringNoTag", "Converting ill-formed UTF-16. Your Protocol Buffer will not round trip correctly!", (Throwable) inVar);
        byte[] bytes = str.getBytes(fs.f2226a);
        try {
            b(bytes.length);
            a(bytes, 0, bytes.length);
        } catch (IndexOutOfBoundsException e) {
            throw new zzc(e);
        } catch (zzc e2) {
            throw e2;
        }
    }

    @Deprecated
    public static int c(gt gtVar) {
        return gtVar.h();
    }

    @Deprecated
    public static int q(int i) {
        return n(i);
    }

    /* synthetic */ zztv(byte b2) {
        this();
    }

    public static int f(int i, int i2) {
        return n(i << 3) + m(i2);
    }

    public static int g(int i, int i2) {
        return n(i << 3) + n(i2);
    }

    public static int h(int i, int i2) {
        return n(i << 3) + n(r(i2));
    }

    public static int e(int i) {
        return n(i << 3) + 4;
    }

    public static int f(int i) {
        return n(i << 3) + 4;
    }

    public static int d(int i, long j) {
        return n(i << 3) + e(j);
    }

    public static int e(int i, long j) {
        return n(i << 3) + e(j);
    }

    public static int f(int i, long j) {
        return n(i << 3) + e(g(j));
    }

    public static int g(int i) {
        return n(i << 3) + 8;
    }

    public static int h(int i) {
        return n(i << 3) + 8;
    }

    public static int i(int i) {
        return n(i << 3) + 4;
    }

    public static int j(int i) {
        return n(i << 3) + 8;
    }

    public static int k(int i) {
        return n(i << 3) + 1;
    }

    public static int i(int i, int i2) {
        return n(i << 3) + m(i2);
    }

    public static int b(int i, String str) {
        return n(i << 3) + b(str);
    }

    public static int c(int i, ej ejVar) {
        int n = n(i << 3);
        int a2 = ejVar.a();
        return n + n(a2) + a2;
    }

    public static int a(int i, gb gbVar) {
        int n = n(i << 3);
        int b2 = gbVar.b();
        return n + n(b2) + b2;
    }

    public static int c(int i, gt gtVar) {
        return n(i << 3) + b(gtVar);
    }

    static int b(int i, gt gtVar, hj hjVar) {
        return n(i << 3) + b(gtVar, hjVar);
    }

    public static int d(int i, gt gtVar) {
        return (n(8) << 1) + g(2, i) + c(3, gtVar);
    }

    public static int d(int i, ej ejVar) {
        return (n(8) << 1) + g(2, i) + c(3, ejVar);
    }

    public static int b(int i, gb gbVar) {
        return (n(8) << 1) + g(2, i) + a(3, gbVar);
    }

    @Deprecated
    static int c(int i, gt gtVar, hj hjVar) {
        int n = n(i << 3) << 1;
        eb ebVar = (eb) gtVar;
        int e = ebVar.e();
        if (e == -1) {
            e = hjVar.b(ebVar);
            ebVar.b(e);
        }
        return n + e;
    }
}
