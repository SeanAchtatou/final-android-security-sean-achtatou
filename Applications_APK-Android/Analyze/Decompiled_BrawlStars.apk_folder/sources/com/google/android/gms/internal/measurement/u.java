package com.google.android.gms.internal.measurement;

import java.lang.Thread;

final class u implements Thread.UncaughtExceptionHandler {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ t f2334a;

    u(t tVar) {
        this.f2334a = tVar;
    }

    public final void uncaughtException(Thread thread, Throwable th) {
        bk bkVar = this.f2334a.e;
        if (bkVar != null) {
            bkVar.e("Job execution failed", th);
        }
    }
}
