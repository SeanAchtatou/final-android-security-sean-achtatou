package com.google.android.gms.common.api.internal;

import android.support.v4.util.ArrayMap;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.AvailabilityException;
import com.google.android.gms.common.api.c;
import com.google.android.gms.tasks.h;
import java.util.Map;

public final class bu {

    /* renamed from: a  reason: collision with root package name */
    final ArrayMap<bs<?>, ConnectionResult> f1438a = new ArrayMap<>();

    /* renamed from: b  reason: collision with root package name */
    final h<Map<bs<?>, String>> f1439b = new h<>();
    private final ArrayMap<bs<?>, String> c = new ArrayMap<>();
    private int d;
    private boolean e = false;

    public bu(Iterable<? extends c<?>> iterable) {
        for (c cVar : iterable) {
            this.f1438a.put(cVar.f1374b, null);
        }
        this.d = this.f1438a.keySet().size();
    }

    public final void a(bs<?> bsVar, ConnectionResult connectionResult, String str) {
        this.f1438a.put(bsVar, connectionResult);
        this.c.put(bsVar, str);
        this.d--;
        if (!connectionResult.b()) {
            this.e = true;
        }
        if (this.d != 0) {
            return;
        }
        if (this.e) {
            this.f1439b.a(new AvailabilityException(this.f1438a));
            return;
        }
        this.f1439b.a((Boolean) this.c);
    }
}
