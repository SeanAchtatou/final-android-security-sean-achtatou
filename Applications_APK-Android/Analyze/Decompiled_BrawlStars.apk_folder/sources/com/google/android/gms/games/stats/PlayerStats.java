package com.google.android.gms.games.stats;

import android.os.Bundle;
import android.os.Parcelable;
import com.google.android.gms.common.data.e;

public interface PlayerStats extends Parcelable, e<PlayerStats> {
    float b();

    @Deprecated
    float c();

    int d();

    int e();

    int f();

    float g();

    float h();

    @Deprecated
    float i();

    @Deprecated
    float j();

    @Deprecated
    float k();

    Bundle l();
}
