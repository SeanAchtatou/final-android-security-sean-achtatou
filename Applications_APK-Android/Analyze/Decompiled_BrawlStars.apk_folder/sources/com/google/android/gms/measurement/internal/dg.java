package com.google.android.gms.measurement.internal;

import android.app.job.JobParameters;

public final /* synthetic */ class dg implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final dd f2507a;

    /* renamed from: b  reason: collision with root package name */
    private final o f2508b;
    private final JobParameters c;

    public dg(dd ddVar, o oVar, JobParameters jobParameters) {
        this.f2507a = ddVar;
        this.f2508b = oVar;
        this.c = jobParameters;
    }

    public final void run() {
        dd ddVar = this.f2507a;
        o oVar = this.f2508b;
        JobParameters jobParameters = this.c;
        oVar.k.a("AppMeasurementJobService processed last upload request.");
        ((di) ddVar.f2503a).a(jobParameters);
    }
}
