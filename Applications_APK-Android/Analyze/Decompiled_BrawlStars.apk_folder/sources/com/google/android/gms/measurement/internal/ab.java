package com.google.android.gms.measurement.internal;

import android.content.SharedPreferences;
import com.google.android.gms.common.internal.l;

public final class ab {

    /* renamed from: a  reason: collision with root package name */
    private final String f2362a;

    /* renamed from: b  reason: collision with root package name */
    private final long f2363b;
    private boolean c;
    private long d;
    private final /* synthetic */ z e;

    public ab(z zVar, String str, long j) {
        this.e = zVar;
        l.a(str);
        this.f2362a = str;
        this.f2363b = j;
    }

    public final long a() {
        if (!this.c) {
            this.c = true;
            this.d = this.e.f().getLong(this.f2362a, this.f2363b);
        }
        return this.d;
    }

    public final void a(long j) {
        SharedPreferences.Editor edit = this.e.f().edit();
        edit.putLong(this.f2362a, j);
        edit.apply();
        this.d = j;
    }
}
