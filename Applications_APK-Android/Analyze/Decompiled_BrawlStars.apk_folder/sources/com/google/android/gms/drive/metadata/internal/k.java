package com.google.android.gms.drive.metadata.internal;

import android.os.Bundle;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.drive.metadata.f;
import java.util.Collection;

public abstract class k<T extends ReflectedParcelable> extends f<T> {
    public k(String str, Collection<String> collection, Collection<String> collection2, int i) {
        super(str, collection, collection2, i);
    }

    public final /* synthetic */ void a(Bundle bundle, Object obj) {
        bundle.putParcelable(this.f1731b, (ReflectedParcelable) obj);
    }

    public final /* synthetic */ Object b(Bundle bundle) {
        return (ReflectedParcelable) bundle.getParcelable(this.f1731b);
    }
}
