package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.internal.f;

final class ah implements f.a {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ag f1393a;

    ah(ag agVar) {
        this.f1393a = agVar;
    }

    public final Bundle a_() {
        return null;
    }

    public final boolean b() {
        return this.f1393a.e();
    }
}
