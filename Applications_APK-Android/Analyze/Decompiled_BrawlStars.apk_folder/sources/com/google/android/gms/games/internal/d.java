package com.google.android.gms.games.internal;

import android.app.Activity;
import android.content.Context;
import android.os.IBinder;
import android.view.Display;
import android.view.View;
import android.view.ViewTreeObserver;
import com.google.android.gms.common.util.o;
import java.lang.ref.WeakReference;

final class d extends b implements View.OnAttachStateChangeListener, ViewTreeObserver.OnGlobalLayoutListener {
    private WeakReference<View> c;
    private boolean d = false;

    protected d(zze zze, int i) {
        super(zze, i, (byte) 0);
    }

    private final void b(View view) {
        Display display;
        int i = -1;
        if (o.b() && (display = view.getDisplay()) != null) {
            i = display.getDisplayId();
        }
        IBinder windowToken = view.getWindowToken();
        int[] iArr = new int[2];
        view.getLocationInWindow(iArr);
        int width = view.getWidth();
        int height = view.getHeight();
        this.f1804b.c = i;
        this.f1804b.f1805a = windowToken;
        this.f1804b.d = iArr[0];
        this.f1804b.e = iArr[1];
        this.f1804b.f = iArr[0] + width;
        this.f1804b.g = iArr[1] + height;
        if (this.d) {
            a();
        }
    }

    public final void a() {
        boolean z;
        if (this.f1804b.f1805a != null) {
            super.a();
            z = false;
        } else {
            z = true;
        }
        this.d = z;
    }

    /* access modifiers changed from: protected */
    public final void a(int i) {
        this.f1804b = new c(i, null, (byte) 0);
    }

    public final void onGlobalLayout() {
        View view;
        WeakReference<View> weakReference = this.c;
        if (weakReference != null && (view = weakReference.get()) != null) {
            b(view);
        }
    }

    public final void onViewAttachedToWindow(View view) {
        b(view);
    }

    public final void onViewDetachedFromWindow(View view) {
        this.f1803a.w();
        view.removeOnAttachStateChangeListener(this);
    }

    public final void a(View view) {
        this.f1803a.w();
        WeakReference<View> weakReference = this.c;
        if (weakReference != null) {
            View view2 = weakReference.get();
            Context context = this.f1803a.c;
            if (view2 == null && (context instanceof Activity)) {
                view2 = ((Activity) context).getWindow().getDecorView();
            }
            if (view2 != null) {
                view2.removeOnAttachStateChangeListener(this);
                ViewTreeObserver viewTreeObserver = view2.getViewTreeObserver();
                if (o.a()) {
                    viewTreeObserver.removeOnGlobalLayoutListener(this);
                } else {
                    viewTreeObserver.removeGlobalOnLayoutListener(this);
                }
            }
        }
        this.c = null;
        Context context2 = this.f1803a.c;
        if (view == null && (context2 instanceof Activity)) {
            Activity activity = (Activity) context2;
            view = activity.findViewById(16908290);
            if (view == null) {
                view = activity.getWindow().getDecorView();
            }
            h.a("PopupManager", "You have not specified a View to use as content view for popups. Falling back to the Activity content view. Note that this may not work as expected in multi-screen environments");
        }
        if (view != null) {
            b(view);
            this.c = new WeakReference<>(view);
            view.addOnAttachStateChangeListener(this);
            view.getViewTreeObserver().addOnGlobalLayoutListener(this);
            return;
        }
        h.b("PopupManager", "No content view usable to display popups. Popups will not be displayed in response to this client's calls. Use setViewForPopups() to set your content view.");
    }
}
