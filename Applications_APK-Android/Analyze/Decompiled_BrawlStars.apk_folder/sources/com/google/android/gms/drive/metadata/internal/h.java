package com.google.android.gms.drive.metadata.internal;

import android.os.Bundle;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.metadata.f;

public class h extends f<Long> {
    public h(String str) {
        super(str, 4300000);
    }

    public final /* synthetic */ Object c(DataHolder dataHolder, int i, int i2) {
        return Long.valueOf(dataHolder.a(this.f1731b, i, i2));
    }

    public final /* synthetic */ void a(Bundle bundle, Object obj) {
        bundle.putLong(this.f1731b, ((Long) obj).longValue());
    }

    public final /* synthetic */ Object b(Bundle bundle) {
        return Long.valueOf(bundle.getLong(this.f1731b));
    }
}
