package com.google.android.gms.common.api.internal;

import com.google.android.gms.signin.internal.zac;
import com.google.android.gms.signin.internal.zaj;
import java.lang.ref.WeakReference;

final class ab extends zac {

    /* renamed from: a  reason: collision with root package name */
    private final WeakReference<u> f1385a;

    ab(u uVar) {
        this.f1385a = new WeakReference<>(uVar);
    }

    public final void a(zaj zaj) {
        u uVar = this.f1385a.get();
        if (uVar != null) {
            uVar.f1496a.a(new ac(uVar, uVar, zaj));
        }
    }
}
