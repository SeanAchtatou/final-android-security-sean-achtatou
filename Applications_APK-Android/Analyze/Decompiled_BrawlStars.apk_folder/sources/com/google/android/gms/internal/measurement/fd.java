package com.google.android.gms.internal.measurement;

import android.support.v4.internal.view.SupportMenu;
import com.google.android.gms.internal.measurement.fq;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class fd {

    /* renamed from: a  reason: collision with root package name */
    static final fd f2200a = new fd((byte) 0);
    private static volatile boolean c = false;
    private static final Class<?> d = d();
    private static volatile fd e;

    /* renamed from: b  reason: collision with root package name */
    final Map<a, fq.d<?, ?>> f2201b;

    private static Class<?> d() {
        try {
            return Class.forName("com.google.protobuf.Extension");
        } catch (ClassNotFoundException unused) {
            return null;
        }
    }

    static final class a {

        /* renamed from: a  reason: collision with root package name */
        private final Object f2202a;

        /* renamed from: b  reason: collision with root package name */
        private final int f2203b;

        a(Object obj, int i) {
            this.f2202a = obj;
            this.f2203b = i;
        }

        public final int hashCode() {
            return (System.identityHashCode(this.f2202a) * SupportMenu.USER_MASK) + this.f2203b;
        }

        public final boolean equals(Object obj) {
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            if (this.f2202a == aVar.f2202a && this.f2203b == aVar.f2203b) {
                return true;
            }
            return false;
        }
    }

    public static fd a() {
        return fc.a();
    }

    public static fd b() {
        fd fdVar = e;
        if (fdVar == null) {
            synchronized (fd.class) {
                fdVar = e;
                if (fdVar == null) {
                    fdVar = fc.b();
                    e = fdVar;
                }
            }
        }
        return fdVar;
    }

    static fd c() {
        return fo.a(fd.class);
    }

    fd() {
        this.f2201b = new HashMap();
    }

    private fd(byte b2) {
        this.f2201b = Collections.emptyMap();
    }
}
