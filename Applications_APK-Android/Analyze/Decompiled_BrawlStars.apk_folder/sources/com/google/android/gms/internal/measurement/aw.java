package com.google.android.gms.internal.measurement;

import android.os.Handler;
import com.google.android.gms.common.internal.l;

abstract class aw {

    /* renamed from: b  reason: collision with root package name */
    private static volatile Handler f2059b;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final t f2060a;
    private final Runnable c = new ax(this);
    /* access modifiers changed from: private */
    public volatile long d;

    aw(t tVar) {
        l.a(tVar);
        this.f2060a = tVar;
    }

    public abstract void a();

    public final void a(long j) {
        d();
        if (j >= 0) {
            this.d = this.f2060a.c.a();
            if (!e().postDelayed(this.c, j)) {
                this.f2060a.a().e("Failed to schedule delayed post. time", Long.valueOf(j));
            }
        }
    }

    public final void b(long j) {
        if (c()) {
            if (j < 0) {
                d();
                return;
            }
            long abs = j - Math.abs(this.f2060a.c.a() - this.d);
            if (abs < 0) {
                abs = 0;
            }
            e().removeCallbacks(this.c);
            if (!e().postDelayed(this.c, abs)) {
                this.f2060a.a().e("Failed to adjust delayed post. time", Long.valueOf(abs));
            }
        }
    }

    public final long b() {
        if (this.d == 0) {
            return 0;
        }
        return Math.abs(this.f2060a.c.a() - this.d);
    }

    public final boolean c() {
        return this.d != 0;
    }

    public final void d() {
        this.d = 0;
        e().removeCallbacks(this.c);
    }

    private final Handler e() {
        Handler handler;
        if (f2059b != null) {
            return f2059b;
        }
        synchronized (aw.class) {
            if (f2059b == null) {
                f2059b = new cc(this.f2060a.f2332a.getMainLooper());
            }
            handler = f2059b;
        }
        return handler;
    }
}
