package com.google.android.gms.common.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.google.android.gms.common.internal.g;
import com.google.android.gms.common.stats.a;
import java.util.HashSet;
import java.util.Set;

final class ad implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    final Set<ServiceConnection> f1587a = new HashSet();

    /* renamed from: b  reason: collision with root package name */
    int f1588b = 2;
    boolean c;
    IBinder d;
    final g.a e;
    ComponentName f;
    final /* synthetic */ ac g;

    public ad(ac acVar, g.a aVar) {
        this.g = acVar;
        this.e = aVar;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        synchronized (this.g.f1585a) {
            this.g.c.removeMessages(1, this.e);
            this.d = iBinder;
            this.f = componentName;
            for (ServiceConnection onServiceConnected : this.f1587a) {
                onServiceConnected.onServiceConnected(componentName, iBinder);
            }
            this.f1588b = 1;
        }
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        synchronized (this.g.f1585a) {
            this.g.c.removeMessages(1, this.e);
            this.d = null;
            this.f = componentName;
            for (ServiceConnection onServiceDisconnected : this.f1587a) {
                onServiceDisconnected.onServiceDisconnected(componentName);
            }
            this.f1588b = 2;
        }
    }

    public final void a(String str) {
        this.f1588b = 3;
        a unused = this.g.d;
        Context c2 = this.g.f1586b;
        g.a aVar = this.e;
        Context unused2 = this.g.f1586b;
        this.c = a.a(c2, aVar.a(), this, this.e.c);
        if (this.c) {
            this.g.c.sendMessageDelayed(this.g.c.obtainMessage(1, this.e), this.g.f);
            return;
        }
        this.f1588b = 2;
        try {
            a unused3 = this.g.d;
            a.a(this.g.f1586b, this);
        } catch (IllegalArgumentException unused4) {
        }
    }

    public final void a(ServiceConnection serviceConnection) {
        a unused = this.g.d;
        Context unused2 = this.g.f1586b;
        g.a aVar = this.e;
        Context unused3 = this.g.f1586b;
        aVar.a();
        this.f1587a.add(serviceConnection);
    }

    public final boolean b(ServiceConnection serviceConnection) {
        return this.f1587a.contains(serviceConnection);
    }

    public final boolean a() {
        return this.f1587a.isEmpty();
    }
}
