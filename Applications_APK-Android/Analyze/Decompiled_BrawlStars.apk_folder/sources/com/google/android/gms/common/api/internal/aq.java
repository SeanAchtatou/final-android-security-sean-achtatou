package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.internal.b;

final class aq implements b.a {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ d f1402a;

    aq(d dVar) {
        this.f1402a = dVar;
    }

    public final void a(boolean z) {
        this.f1402a.i.sendMessage(this.f1402a.i.obtainMessage(1, Boolean.valueOf(z)));
    }
}
