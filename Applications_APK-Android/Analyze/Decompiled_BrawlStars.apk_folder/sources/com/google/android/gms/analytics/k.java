package com.google.android.gms.analytics;

import android.os.Build;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.util.e;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class k {

    /* renamed from: a  reason: collision with root package name */
    final m f1328a;

    /* renamed from: b  reason: collision with root package name */
    boolean f1329b;
    long c;
    public long d;
    boolean e;
    final Map<Class<? extends l>, l> f;
    final List<q> g;
    private final e h;
    private long i;
    private long j;
    private long k;

    public final k a() {
        return new k(this);
    }

    public final void a(l lVar) {
        l.a(lVar);
        Class<?> cls = lVar.getClass();
        if (cls.getSuperclass() == l.class) {
            lVar.a(b(cls));
            return;
        }
        throw new IllegalArgumentException();
    }

    public final <T extends l> T a(Class cls) {
        return (l) this.f.get(cls);
    }

    public final <T extends l> T b(Class<T> cls) {
        T t = (l) this.f.get(cls);
        if (t != null) {
            return t;
        }
        T c2 = c(cls);
        this.f.put(cls, c2);
        return c2;
    }

    public final void b() {
        n nVar = this.f1328a.f;
        if (this.e) {
            throw new IllegalStateException("Measurement prototype can't be submitted");
        } else if (!this.f1329b) {
            k a2 = a();
            a2.i = a2.h.b();
            long j2 = a2.d;
            if (j2 != 0) {
                a2.c = j2;
            } else {
                a2.c = a2.h.a();
            }
            a2.f1329b = true;
            nVar.f1332b.execute(new o(nVar, a2));
        } else {
            throw new IllegalStateException("Measurement can only be submitted once");
        }
    }

    k(m mVar, e eVar) {
        l.a(mVar);
        l.a(eVar);
        this.f1328a = mVar;
        this.h = eVar;
        this.j = 1800000;
        this.k = 3024000000L;
        this.f = new HashMap();
        this.g = new ArrayList();
    }

    private k(k kVar) {
        this.f1328a = kVar.f1328a;
        this.h = kVar.h;
        this.c = kVar.c;
        this.d = kVar.d;
        this.i = kVar.i;
        this.j = kVar.j;
        this.k = kVar.k;
        this.g = new ArrayList(kVar.g);
        this.f = new HashMap(kVar.f.size());
        for (Map.Entry next : kVar.f.entrySet()) {
            l c2 = c((Class) next.getKey());
            ((l) next.getValue()).a(c2);
            this.f.put((Class) next.getKey(), c2);
        }
    }

    private static <T extends l> T c(Class<T> cls) {
        try {
            return (l) cls.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception e2) {
            if (e2 instanceof InstantiationException) {
                throw new IllegalArgumentException("dataType doesn't have default constructor", e2);
            } else if (e2 instanceof IllegalAccessException) {
                throw new IllegalArgumentException("dataType default constructor is not accessible", e2);
            } else if (Build.VERSION.SDK_INT < 19 || !(e2 instanceof ReflectiveOperationException)) {
                throw new RuntimeException(e2);
            } else {
                throw new IllegalArgumentException("Linkage exception", e2);
            }
        }
    }
}
