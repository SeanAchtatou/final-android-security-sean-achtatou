package com.google.android.gms.games.event;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.d;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerRef;

public final class EventRef extends d implements Event {
    public final /* synthetic */ Object a() {
        return new EventEntity(this);
    }

    public final String b() {
        return e("external_event_id");
    }

    public final String c() {
        return e("name");
    }

    public final String d() {
        return e("description");
    }

    public final int describeContents() {
        return 0;
    }

    public final Uri e() {
        return h("icon_image_uri");
    }

    public final boolean equals(Object obj) {
        return EventEntity.a(this, obj);
    }

    public final Player f() {
        return new PlayerRef(this.f1532a, this.f1533b);
    }

    public final long g() {
        return b("value");
    }

    public final String getIconImageUrl() {
        return e("icon_image_url");
    }

    public final String h() {
        return e("formatted_value");
    }

    public final int hashCode() {
        return EventEntity.a(this);
    }

    public final boolean i() {
        return d("visibility");
    }

    public final String toString() {
        return EventEntity.b(this);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        ((EventEntity) ((Event) a())).writeToParcel(parcel, i);
    }
}
