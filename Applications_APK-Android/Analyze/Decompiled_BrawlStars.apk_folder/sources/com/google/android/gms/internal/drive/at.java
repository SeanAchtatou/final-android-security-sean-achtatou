package com.google.android.gms.internal.drive;

import java.io.IOException;
import java.nio.BufferOverflowException;

public final class at extends bs<at> {

    /* renamed from: a  reason: collision with root package name */
    public int f1891a = 1;

    /* renamed from: b  reason: collision with root package name */
    public String f1892b = "";
    public long c = -1;
    public long d = -1;
    public int e = -1;

    public at() {
        this.f = null;
        this.g = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof at)) {
            return false;
        }
        at atVar = (at) obj;
        if (this.f1891a != atVar.f1891a) {
            return false;
        }
        String str = this.f1892b;
        if (str == null) {
            if (atVar.f1892b != null) {
                return false;
            }
        } else if (!str.equals(atVar.f1892b)) {
            return false;
        }
        if (this.c == atVar.c && this.d == atVar.d && this.e == atVar.e) {
            return (this.f == null || this.f.a()) ? atVar.f == null || atVar.f.a() : this.f.equals(atVar.f);
        }
        return false;
    }

    public final int hashCode() {
        int hashCode = (((getClass().getName().hashCode() + 527) * 31) + this.f1891a) * 31;
        String str = this.f1892b;
        int i = 0;
        int hashCode2 = str == null ? 0 : str.hashCode();
        long j = this.c;
        long j2 = this.d;
        int i2 = (((((((hashCode + hashCode2) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + this.e) * 31;
        if (this.f != null && !this.f.a()) {
            i = this.f.hashCode();
        }
        return i2 + i;
    }

    public final void a(br brVar) throws IOException {
        brVar.a(1, this.f1891a);
        String str = this.f1892b;
        brVar.c(2, 2);
        try {
            int b2 = br.b(str.length());
            if (b2 == br.b(str.length() * 3)) {
                int position = brVar.f1901a.position();
                if (brVar.f1901a.remaining() >= b2) {
                    brVar.f1901a.position(position + b2);
                    br.a(str, brVar.f1901a);
                    int position2 = brVar.f1901a.position();
                    brVar.f1901a.position(position);
                    brVar.a((position2 - position) - b2);
                    brVar.f1901a.position(position2);
                } else {
                    throw new zziq(position + b2, brVar.f1901a.limit());
                }
            } else {
                brVar.a(br.a(str));
                br.a(str, brVar.f1901a);
            }
            brVar.a(3, this.c);
            brVar.a(4, this.d);
            int i = this.e;
            if (i != -1) {
                brVar.a(5, i);
            }
            super.a(brVar);
        } catch (BufferOverflowException e2) {
            zziq zziq = new zziq(brVar.f1901a.position(), brVar.f1901a.limit());
            zziq.initCause(e2);
            throw zziq;
        }
    }

    /* access modifiers changed from: protected */
    public final int a() {
        int a2 = super.a() + br.b(1, this.f1891a);
        String str = this.f1892b;
        int b2 = br.b(16);
        int a3 = br.a(str);
        int b3 = a2 + b2 + br.b(a3) + a3 + br.b(3, this.c) + br.b(4, this.d);
        int i = this.e;
        return i != -1 ? b3 + br.b(5, i) : b3;
    }
}
