package com.google.android.gms.internal.measurement;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.google.android.gms.common.internal.l;

class bl extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private static final String f2079a = bl.class.getName();

    /* renamed from: b  reason: collision with root package name */
    private final t f2080b;
    private boolean c;
    private boolean d;

    bl(t tVar) {
        l.a(tVar);
        this.f2080b = tVar;
    }

    public void onReceive(Context context, Intent intent) {
        e();
        String action = intent.getAction();
        this.f2080b.a().a("NetworkBroadcastReceiver received action", action);
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
            boolean f = f();
            if (this.d != f) {
                this.d = f;
                l c2 = this.f2080b.c();
                c2.a("Network connectivity status changed", Boolean.valueOf(f));
                c2.c.b().a(new m(c2, f));
            }
        } else if (!"com.google.analytics.RADIO_POWERED".equals(action)) {
            this.f2080b.a().d("NetworkBroadcastReceiver received unknown action", action);
        } else if (!intent.hasExtra(f2079a)) {
            l c3 = this.f2080b.c();
            c3.b("Radio powered up");
            c3.b();
        }
    }

    public final void a() {
        e();
        if (!this.c) {
            Context context = this.f2080b.f2332a;
            context.registerReceiver(this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            IntentFilter intentFilter = new IntentFilter("com.google.analytics.RADIO_POWERED");
            intentFilter.addCategory(context.getPackageName());
            context.registerReceiver(this, intentFilter);
            this.d = f();
            this.f2080b.a().a("Registering connectivity change receiver. Network connected", Boolean.valueOf(this.d));
            this.c = true;
        }
    }

    private final void e() {
        this.f2080b.a();
        this.f2080b.c();
    }

    public final void b() {
        if (this.c) {
            this.f2080b.a().b("Unregistering connectivity change receiver");
            this.c = false;
            this.d = false;
            try {
                this.f2080b.f2332a.unregisterReceiver(this);
            } catch (IllegalArgumentException e) {
                this.f2080b.a().e("Failed to unregister the network broadcast receiver", e);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public final void c() {
        Context context = this.f2080b.f2332a;
        Intent intent = new Intent("com.google.analytics.RADIO_POWERED");
        intent.addCategory(context.getPackageName());
        intent.putExtra(f2079a, true);
        context.sendOrderedBroadcast(intent, null);
    }

    public final boolean d() {
        if (!this.c) {
            this.f2080b.a().e("Connectivity unknown. Receiver not registered");
        }
        return this.d;
    }

    private final boolean f() {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.f2080b.f2332a.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
                return false;
            }
            return true;
        } catch (SecurityException unused) {
        }
    }
}
