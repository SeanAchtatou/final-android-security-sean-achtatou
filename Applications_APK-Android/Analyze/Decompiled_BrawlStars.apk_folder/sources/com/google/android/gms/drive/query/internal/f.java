package com.google.android.gms.drive.query.internal;

import com.google.android.gms.drive.metadata.a;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import java.util.Set;

final class f {
    static a<?> a(MetadataBundle metadataBundle) {
        Set<a<?>> b2 = metadataBundle.b();
        if (b2.size() == 1) {
            return b2.iterator().next();
        }
        throw new IllegalArgumentException("bundle should have exactly 1 populated field");
    }
}
