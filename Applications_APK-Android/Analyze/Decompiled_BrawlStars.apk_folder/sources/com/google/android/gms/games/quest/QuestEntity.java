package com.google.android.gms.games.quest;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.internal.zzd;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Deprecated
public final class QuestEntity extends zzd implements Quest {
    public static final Parcelable.Creator<QuestEntity> CREATOR = new c();

    /* renamed from: a  reason: collision with root package name */
    private final GameEntity f1837a;

    /* renamed from: b  reason: collision with root package name */
    private final String f1838b;
    private final long c;
    private final Uri d;
    private final String e;
    private final String f;
    private final long g;
    private final long h;
    private final Uri i;
    private final String j;
    private final String k;
    private final long l;
    private final long m;
    private final int n;
    private final int o;
    private final ArrayList<MilestoneEntity> p;

    QuestEntity(GameEntity gameEntity, String str, long j2, Uri uri, String str2, String str3, long j3, long j4, Uri uri2, String str4, String str5, long j5, long j6, int i2, int i3, ArrayList<MilestoneEntity> arrayList) {
        this.f1837a = gameEntity;
        this.f1838b = str;
        this.c = j2;
        this.d = uri;
        this.e = str2;
        this.f = str3;
        this.g = j3;
        this.h = j4;
        this.i = uri2;
        this.j = str4;
        this.k = str5;
        this.l = j5;
        this.m = j6;
        this.n = i2;
        this.o = i3;
        this.p = arrayList;
    }

    public QuestEntity(Quest quest) {
        this.f1837a = new GameEntity(quest.h());
        this.f1838b = quest.b();
        this.c = quest.k();
        this.f = quest.d();
        this.d = quest.e();
        this.e = quest.getBannerImageUrl();
        this.g = quest.l();
        this.i = quest.f();
        this.j = quest.getIconImageUrl();
        this.h = quest.m();
        this.k = quest.c();
        this.l = quest.n();
        this.m = quest.o();
        this.n = quest.i();
        this.o = quest.j();
        List<Milestone> g2 = quest.g();
        int size = g2.size();
        this.p = new ArrayList<>(size);
        for (int i2 = 0; i2 < size; i2++) {
            this.p.add((MilestoneEntity) g2.get(i2).a());
        }
    }

    static boolean a(Quest quest, Object obj) {
        if (!(obj instanceof Quest)) {
            return false;
        }
        if (quest == obj) {
            return true;
        }
        Quest quest2 = (Quest) obj;
        return j.a(quest2.h(), quest.h()) && j.a(quest2.b(), quest.b()) && j.a(Long.valueOf(quest2.k()), Long.valueOf(quest.k())) && j.a(quest2.e(), quest.e()) && j.a(quest2.d(), quest.d()) && j.a(Long.valueOf(quest2.l()), Long.valueOf(quest.l())) && j.a(quest2.f(), quest.f()) && j.a(Long.valueOf(quest2.m()), Long.valueOf(quest.m())) && j.a(quest2.g(), quest.g()) && j.a(quest2.c(), quest.c()) && j.a(Long.valueOf(quest2.n()), Long.valueOf(quest.n())) && j.a(Long.valueOf(quest2.o()), Long.valueOf(quest.o())) && j.a(Integer.valueOf(quest2.i()), Integer.valueOf(quest.i()));
    }

    static String b(Quest quest) {
        return j.a(quest).a("Game", quest.h()).a("QuestId", quest.b()).a("AcceptedTimestamp", Long.valueOf(quest.k())).a("BannerImageUri", quest.e()).a("BannerImageUrl", quest.getBannerImageUrl()).a("Description", quest.d()).a("EndTimestamp", Long.valueOf(quest.l())).a("IconImageUri", quest.f()).a("IconImageUrl", quest.getIconImageUrl()).a("LastUpdatedTimestamp", Long.valueOf(quest.m())).a("Milestones", quest.g()).a("Name", quest.c()).a("NotifyTimestamp", Long.valueOf(quest.n())).a("StartTimestamp", Long.valueOf(quest.o())).a("State", Integer.valueOf(quest.i())).toString();
    }

    public final /* bridge */ /* synthetic */ Object a() {
        return this;
    }

    public final String b() {
        return this.f1838b;
    }

    public final String c() {
        return this.k;
    }

    public final String d() {
        return this.f;
    }

    public final Uri e() {
        return this.d;
    }

    public final boolean equals(Object obj) {
        return a(this, obj);
    }

    public final Uri f() {
        return this.i;
    }

    public final List<Milestone> g() {
        return new ArrayList(this.p);
    }

    public final String getBannerImageUrl() {
        return this.e;
    }

    public final String getIconImageUrl() {
        return this.j;
    }

    public final Game h() {
        return this.f1837a;
    }

    public final int hashCode() {
        return a(this);
    }

    public final int i() {
        return this.n;
    }

    public final int j() {
        return this.o;
    }

    public final long k() {
        return this.c;
    }

    public final long l() {
        return this.g;
    }

    public final long m() {
        return this.h;
    }

    public final long n() {
        return this.l;
    }

    public final long o() {
        return this.m;
    }

    public final String toString() {
        return b(this);
    }

    static int a(Quest quest) {
        return Arrays.hashCode(new Object[]{quest.h(), quest.b(), Long.valueOf(quest.k()), quest.e(), quest.d(), Long.valueOf(quest.l()), quest.f(), Long.valueOf(quest.m()), quest.g(), quest.c(), Long.valueOf(quest.n()), Long.valueOf(quest.o()), Integer.valueOf(quest.i())});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.GameEntity, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.net.Uri, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = a.a(parcel, 20293);
        a.a(parcel, 1, (Parcelable) this.f1837a, i2, false);
        a.a(parcel, 2, this.f1838b, false);
        a.a(parcel, 3, this.c);
        a.a(parcel, 4, (Parcelable) this.d, i2, false);
        a.a(parcel, 5, getBannerImageUrl(), false);
        a.a(parcel, 6, this.f, false);
        a.a(parcel, 7, this.g);
        a.a(parcel, 8, this.h);
        a.a(parcel, 9, (Parcelable) this.i, i2, false);
        a.a(parcel, 10, getIconImageUrl(), false);
        a.a(parcel, 12, this.k, false);
        a.a(parcel, 13, this.l);
        a.a(parcel, 14, this.m);
        a.b(parcel, 15, this.n);
        a.b(parcel, 16, this.o);
        a.b(parcel, 17, g(), false);
        a.b(parcel, a2);
    }
}
