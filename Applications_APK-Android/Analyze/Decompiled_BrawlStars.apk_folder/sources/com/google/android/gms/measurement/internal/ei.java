package com.google.android.gms.measurement.internal;

import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.internal.measurement.cj;
import com.google.android.gms.internal.measurement.ck;
import com.google.android.gms.internal.measurement.cl;
import com.google.android.gms.internal.measurement.cm;
import com.google.android.gms.internal.measurement.cn;
import com.google.android.gms.internal.measurement.cs;
import com.google.android.gms.internal.measurement.cu;
import com.google.android.gms.internal.measurement.cz;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

final class ei extends dt {
    ei(du duVar) {
        super(duVar);
    }

    /* access modifiers changed from: protected */
    public final boolean d() {
        return false;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v3, resolved type: android.support.v4.util.ArrayMap} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v27, resolved type: android.support.v4.util.ArrayMap} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v8, resolved type: android.support.v4.util.ArrayMap} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r19v6, resolved type: android.support.v4.util.ArrayMap} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v28, resolved type: android.support.v4.util.ArrayMap} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r19v9, resolved type: android.support.v4.util.ArrayMap} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v42, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v30, resolved type: java.lang.String} */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x02f9  */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x039d  */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x03f0  */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x0443  */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x0464  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x02c0  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x02dd  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.measurement.cr[] a(java.lang.String r68, com.google.android.gms.internal.measurement.ct[] r69, com.google.android.gms.internal.measurement.cz[] r70) {
        /*
            r67 = this;
            r7 = r67
            r15 = r68
            r13 = r69
            r14 = r70
            com.google.android.gms.common.internal.l.a(r68)
            java.util.HashSet r11 = new java.util.HashSet
            r11.<init>()
            android.support.v4.util.ArrayMap r12 = new android.support.v4.util.ArrayMap
            r12.<init>()
            android.support.v4.util.ArrayMap r10 = new android.support.v4.util.ArrayMap
            r10.<init>()
            android.support.v4.util.ArrayMap r9 = new android.support.v4.util.ArrayMap
            r9.<init>()
            android.support.v4.util.ArrayMap r8 = new android.support.v4.util.ArrayMap
            r8.<init>()
            android.support.v4.util.ArrayMap r6 = new android.support.v4.util.ArrayMap
            r6.<init>()
            com.google.android.gms.measurement.internal.el r0 = r67.s()
            boolean r23 = r0.f(r15)
            com.google.android.gms.measurement.internal.eo r0 = r67.h()
            java.util.Map r0 = r0.e(r15)
            r3 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r3)
            if (r0 == 0) goto L_0x0185
            java.util.Set r1 = r0.keySet()
            java.util.Iterator r1 = r1.iterator()
        L_0x0048:
            boolean r16 = r1.hasNext()
            if (r16 == 0) goto L_0x0185
            java.lang.Object r16 = r1.next()
            java.lang.Integer r16 = (java.lang.Integer) r16
            int r16 = r16.intValue()
            java.lang.Integer r4 = java.lang.Integer.valueOf(r16)
            java.lang.Object r4 = r0.get(r4)
            com.google.android.gms.internal.measurement.cx r4 = (com.google.android.gms.internal.measurement.cx) r4
            java.lang.Integer r3 = java.lang.Integer.valueOf(r16)
            java.lang.Object r3 = r10.get(r3)
            java.util.BitSet r3 = (java.util.BitSet) r3
            java.lang.Integer r5 = java.lang.Integer.valueOf(r16)
            java.lang.Object r5 = r9.get(r5)
            java.util.BitSet r5 = (java.util.BitSet) r5
            r20 = r0
            if (r23 == 0) goto L_0x00b6
            android.support.v4.util.ArrayMap r0 = new android.support.v4.util.ArrayMap
            r0.<init>()
            r21 = r1
            if (r4 == 0) goto L_0x00aa
            com.google.android.gms.internal.measurement.cs[] r1 = r4.c
            if (r1 != 0) goto L_0x0088
            goto L_0x00aa
        L_0x0088:
            com.google.android.gms.internal.measurement.cs[] r1 = r4.c
            r22 = r2
            int r2 = r1.length
            r24 = r5
            r5 = 0
        L_0x0090:
            if (r5 >= r2) goto L_0x00ae
            r25 = r2
            r2 = r1[r5]
            r26 = r1
            java.lang.Integer r1 = r2.f2134a
            if (r1 == 0) goto L_0x00a3
            java.lang.Integer r1 = r2.f2134a
            java.lang.Long r2 = r2.f2135b
            r0.put(r1, r2)
        L_0x00a3:
            int r5 = r5 + 1
            r2 = r25
            r1 = r26
            goto L_0x0090
        L_0x00aa:
            r22 = r2
            r24 = r5
        L_0x00ae:
            java.lang.Integer r1 = java.lang.Integer.valueOf(r16)
            r8.put(r1, r0)
            goto L_0x00bd
        L_0x00b6:
            r21 = r1
            r22 = r2
            r24 = r5
            r0 = 0
        L_0x00bd:
            if (r3 != 0) goto L_0x00d8
            java.util.BitSet r3 = new java.util.BitSet
            r3.<init>()
            java.lang.Integer r1 = java.lang.Integer.valueOf(r16)
            r10.put(r1, r3)
            java.util.BitSet r5 = new java.util.BitSet
            r5.<init>()
            java.lang.Integer r1 = java.lang.Integer.valueOf(r16)
            r9.put(r1, r5)
            goto L_0x00da
        L_0x00d8:
            r5 = r24
        L_0x00da:
            r1 = 0
        L_0x00db:
            long[] r2 = r4.f2143a
            int r2 = r2.length
            int r2 = r2 << 6
            if (r1 >= r2) goto L_0x012e
            long[] r2 = r4.f2143a
            boolean r2 = com.google.android.gms.measurement.internal.ea.a(r2, r1)
            if (r2 == 0) goto L_0x0113
            com.google.android.gms.measurement.internal.o r2 = r67.q()
            com.google.android.gms.measurement.internal.q r2 = r2.k
            r24 = r8
            java.lang.Integer r8 = java.lang.Integer.valueOf(r16)
            r25 = r9
            java.lang.Integer r9 = java.lang.Integer.valueOf(r1)
            r26 = r10
            java.lang.String r10 = "Filter already evaluated. audience ID, filter ID"
            r2.a(r10, r8, r9)
            r5.set(r1)
            long[] r2 = r4.f2144b
            boolean r2 = com.google.android.gms.measurement.internal.ea.a(r2, r1)
            if (r2 == 0) goto L_0x0119
            r3.set(r1)
            r2 = 1
            goto L_0x011a
        L_0x0113:
            r24 = r8
            r25 = r9
            r26 = r10
        L_0x0119:
            r2 = 0
        L_0x011a:
            if (r0 == 0) goto L_0x0125
            if (r2 != 0) goto L_0x0125
            java.lang.Integer r2 = java.lang.Integer.valueOf(r1)
            r0.remove(r2)
        L_0x0125:
            int r1 = r1 + 1
            r8 = r24
            r9 = r25
            r10 = r26
            goto L_0x00db
        L_0x012e:
            r24 = r8
            r25 = r9
            r26 = r10
            com.google.android.gms.internal.measurement.cr r1 = new com.google.android.gms.internal.measurement.cr
            r1.<init>()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r16)
            r12.put(r2, r1)
            r2 = 0
            java.lang.Boolean r8 = java.lang.Boolean.valueOf(r2)
            r1.d = r8
            r1.c = r4
            com.google.android.gms.internal.measurement.cx r2 = new com.google.android.gms.internal.measurement.cx
            r2.<init>()
            r1.f2133b = r2
            com.google.android.gms.internal.measurement.cx r2 = r1.f2133b
            long[] r3 = com.google.android.gms.measurement.internal.ea.a(r3)
            r2.f2144b = r3
            com.google.android.gms.internal.measurement.cx r2 = r1.f2133b
            long[] r3 = com.google.android.gms.measurement.internal.ea.a(r5)
            r2.f2143a = r3
            if (r23 == 0) goto L_0x0176
            com.google.android.gms.internal.measurement.cx r1 = r1.f2133b
            com.google.android.gms.internal.measurement.cs[] r0 = a(r0)
            r1.c = r0
            java.lang.Integer r0 = java.lang.Integer.valueOf(r16)
            android.support.v4.util.ArrayMap r1 = new android.support.v4.util.ArrayMap
            r1.<init>()
            r6.put(r0, r1)
        L_0x0176:
            r0 = r20
            r1 = r21
            r2 = r22
            r8 = r24
            r9 = r25
            r10 = r26
            r3 = 1
            goto L_0x0048
        L_0x0185:
            r22 = r2
            r24 = r8
            r25 = r9
            r26 = r10
            java.lang.String r9 = "Filter definition"
            java.lang.String r4 = "Skipping failed audience ID"
            java.lang.String r27 = "null"
            if (r13 == 0) goto L_0x07b4
            android.support.v4.util.ArrayMap r5 = new android.support.v4.util.ArrayMap
            r5.<init>()
            int r3 = r13.length
            r28 = 0
            r20 = r28
            r0 = 0
            r1 = 0
            r2 = 0
        L_0x01a2:
            if (r2 >= r3) goto L_0x07b4
            r14 = r13[r2]
            java.lang.String r8 = r14.f2137b
            com.google.android.gms.internal.measurement.cu[] r10 = r14.f2136a
            r30 = r2
            com.google.android.gms.measurement.internal.el r2 = r67.s()
            r31 = r3
            com.google.android.gms.measurement.internal.h$a<java.lang.Boolean> r3 = com.google.android.gms.measurement.internal.h.W
            boolean r2 = r2.c(r15, r3)
            r32 = 1
            if (r2 == 0) goto L_0x037e
            r67.f()
            java.lang.String r2 = "_eid"
            java.lang.Object r3 = com.google.android.gms.measurement.internal.ea.b(r14, r2)
            java.lang.Long r3 = (java.lang.Long) r3
            if (r3 == 0) goto L_0x01cc
            r34 = 1
            goto L_0x01ce
        L_0x01cc:
            r34 = 0
        L_0x01ce:
            r35 = r4
            if (r34 == 0) goto L_0x01dc
            java.lang.String r4 = "_ep"
            boolean r4 = r8.equals(r4)
            if (r4 == 0) goto L_0x01dc
            r4 = 1
            goto L_0x01dd
        L_0x01dc:
            r4 = 0
        L_0x01dd:
            if (r4 == 0) goto L_0x0331
            r67.f()
            java.lang.String r4 = "_en"
            java.lang.Object r4 = com.google.android.gms.measurement.internal.ea.b(r14, r4)
            r8 = r4
            java.lang.String r8 = (java.lang.String) r8
            boolean r4 = android.text.TextUtils.isEmpty(r8)
            if (r4 == 0) goto L_0x020a
            com.google.android.gms.measurement.internal.o r2 = r67.q()
            com.google.android.gms.measurement.internal.q r2 = r2.c
            java.lang.String r4 = "Extra parameter without an event name. eventId"
            r2.a(r4, r3)
            r36 = r0
            r41 = r5
            r43 = r6
            r40 = r22
            r42 = r35
            r37 = 1
            goto L_0x0320
        L_0x020a:
            if (r0 == 0) goto L_0x021f
            if (r1 == 0) goto L_0x021f
            long r36 = r3.longValue()
            long r38 = r1.longValue()
            int r4 = (r36 > r38 ? 1 : (r36 == r38 ? 0 : -1))
            if (r4 == 0) goto L_0x021b
            goto L_0x021f
        L_0x021b:
            r4 = r0
            r34 = r1
            goto L_0x0247
        L_0x021f:
            com.google.android.gms.measurement.internal.eo r4 = r67.h()
            android.util.Pair r4 = r4.a(r15, r3)
            r36 = r0
            if (r4 == 0) goto L_0x030b
            java.lang.Object r0 = r4.first
            if (r0 != 0) goto L_0x0231
            goto L_0x030b
        L_0x0231:
            java.lang.Object r0 = r4.first
            com.google.android.gms.internal.measurement.ct r0 = (com.google.android.gms.internal.measurement.ct) r0
            java.lang.Object r1 = r4.second
            java.lang.Long r1 = (java.lang.Long) r1
            long r20 = r1.longValue()
            r67.f()
            java.lang.Object r1 = com.google.android.gms.measurement.internal.ea.b(r0, r2)
            java.lang.Long r1 = (java.lang.Long) r1
            goto L_0x021b
        L_0x0247:
            long r20 = r20 - r32
            int r0 = (r20 > r28 ? 1 : (r20 == r28 ? 0 : -1))
            if (r0 > 0) goto L_0x0294
            com.google.android.gms.measurement.internal.eo r1 = r67.h()
            r1.c()
            com.google.android.gms.measurement.internal.o r0 = r1.q()
            com.google.android.gms.measurement.internal.q r0 = r0.k
            java.lang.String r2 = "Clearing complex main event info. appId"
            r0.a(r2, r15)
            android.database.sqlite.SQLiteDatabase r0 = r1.w()     // Catch:{ SQLiteException -> 0x0276 }
            java.lang.String r2 = "delete from main_event_params where app_id=?"
            r18 = r4
            r3 = 1
            java.lang.String[] r4 = new java.lang.String[r3]     // Catch:{ SQLiteException -> 0x0274 }
            r19 = 0
            r4[r19] = r15     // Catch:{ SQLiteException -> 0x0272 }
            r0.execSQL(r2, r4)     // Catch:{ SQLiteException -> 0x0272 }
            goto L_0x0287
        L_0x0272:
            r0 = move-exception
            goto L_0x027c
        L_0x0274:
            r0 = move-exception
            goto L_0x027a
        L_0x0276:
            r0 = move-exception
            r18 = r4
            r3 = 1
        L_0x027a:
            r19 = 0
        L_0x027c:
            com.google.android.gms.measurement.internal.o r1 = r1.q()
            com.google.android.gms.measurement.internal.q r1 = r1.c
            java.lang.String r2 = "Error clearing complex main event"
            r1.a(r2, r0)
        L_0x0287:
            r41 = r5
            r43 = r6
            r1 = r18
            r40 = r22
            r42 = r35
            r37 = 1
            goto L_0x02b2
        L_0x0294:
            r18 = r4
            r4 = 1
            r19 = 0
            com.google.android.gms.measurement.internal.eo r1 = r67.h()
            r40 = r22
            r2 = r68
            r37 = 1
            r41 = r5
            r42 = r35
            r4 = r20
            r43 = r6
            r6 = r18
            r1.a(r2, r3, r4, r6)
            r1 = r18
        L_0x02b2:
            com.google.android.gms.internal.measurement.cu[] r0 = r1.f2136a
            int r0 = r0.length
            int r2 = r10.length
            int r0 = r0 + r2
            com.google.android.gms.internal.measurement.cu[] r0 = new com.google.android.gms.internal.measurement.cu[r0]
            com.google.android.gms.internal.measurement.cu[] r2 = r1.f2136a
            int r3 = r2.length
            r4 = 0
            r5 = 0
        L_0x02be:
            if (r4 >= r3) goto L_0x02d9
            r6 = r2[r4]
            r67.f()
            r18 = r1
            java.lang.String r1 = r6.f2138a
            com.google.android.gms.internal.measurement.cu r1 = com.google.android.gms.measurement.internal.ea.a(r14, r1)
            if (r1 != 0) goto L_0x02d4
            int r1 = r5 + 1
            r0[r5] = r6
            r5 = r1
        L_0x02d4:
            int r4 = r4 + 1
            r1 = r18
            goto L_0x02be
        L_0x02d9:
            r18 = r1
            if (r5 <= 0) goto L_0x02f9
            int r1 = r10.length
            r2 = 0
        L_0x02df:
            if (r2 >= r1) goto L_0x02eb
            r3 = r10[r2]
            int r4 = r5 + 1
            r0[r5] = r3
            int r2 = r2 + 1
            r5 = r4
            goto L_0x02df
        L_0x02eb:
            int r1 = r0.length
            if (r5 != r1) goto L_0x02ef
            goto L_0x02f5
        L_0x02ef:
            java.lang.Object[] r0 = java.util.Arrays.copyOf(r0, r5)
            com.google.android.gms.internal.measurement.cu[] r0 = (com.google.android.gms.internal.measurement.cu[]) r0
        L_0x02f5:
            r35 = r0
            r0 = r8
            goto L_0x0307
        L_0x02f9:
            com.google.android.gms.measurement.internal.o r0 = r67.q()
            com.google.android.gms.measurement.internal.q r0 = r0.f
            java.lang.String r1 = "No unique parameters in main event. eventName"
            r0.a(r1, r8)
            r0 = r8
            r35 = r10
        L_0x0307:
            r36 = r18
            goto L_0x038f
        L_0x030b:
            r41 = r5
            r43 = r6
            r40 = r22
            r42 = r35
            r37 = 1
            com.google.android.gms.measurement.internal.o r0 = r67.q()
            com.google.android.gms.measurement.internal.q r0 = r0.c
            java.lang.String r2 = "Extra parameter without existing main event. eventName, eventId"
            r0.a(r2, r8, r3)
        L_0x0320:
            r46 = r9
            r14 = r11
            r66 = r12
            r44 = r24
            r45 = r25
            r47 = r26
            r0 = r36
            r65 = r40
            goto L_0x0793
        L_0x0331:
            r36 = r0
            r41 = r5
            r43 = r6
            r40 = r22
            r42 = r35
            r37 = 1
            if (r34 == 0) goto L_0x038a
            r67.f()
            java.lang.Long r0 = java.lang.Long.valueOf(r28)
            java.lang.String r1 = "_epc"
            java.lang.Object r1 = com.google.android.gms.measurement.internal.ea.b(r14, r1)
            if (r1 != 0) goto L_0x034f
            goto L_0x0350
        L_0x034f:
            r0 = r1
        L_0x0350:
            java.lang.Long r0 = (java.lang.Long) r0
            long r17 = r0.longValue()
            int r0 = (r17 > r28 ? 1 : (r17 == r28 ? 0 : -1))
            if (r0 > 0) goto L_0x0367
            com.google.android.gms.measurement.internal.o r0 = r67.q()
            com.google.android.gms.measurement.internal.q r0 = r0.f
            java.lang.String r1 = "Complex event with zero extra param count. eventName"
            r0.a(r1, r8)
            r0 = r3
            goto L_0x0374
        L_0x0367:
            com.google.android.gms.measurement.internal.eo r1 = r67.h()
            r2 = r68
            r0 = r3
            r4 = r17
            r6 = r14
            r1.a(r2, r3, r4, r6)
        L_0x0374:
            r34 = r0
            r0 = r8
            r35 = r10
            r36 = r14
            r38 = r17
            goto L_0x0391
        L_0x037e:
            r36 = r0
            r42 = r4
            r41 = r5
            r43 = r6
            r40 = r22
            r37 = 1
        L_0x038a:
            r34 = r1
            r0 = r8
            r35 = r10
        L_0x038f:
            r38 = r20
        L_0x0391:
            com.google.android.gms.measurement.internal.eo r1 = r67.h()
            java.lang.String r2 = r14.f2137b
            com.google.android.gms.measurement.internal.d r1 = r1.a(r15, r2)
            if (r1 != 0) goto L_0x03f0
            com.google.android.gms.measurement.internal.o r1 = r67.q()
            com.google.android.gms.measurement.internal.q r1 = r1.f
            java.lang.Object r2 = com.google.android.gms.measurement.internal.o.a(r68)
            com.google.android.gms.measurement.internal.m r3 = r67.n()
            java.lang.String r3 = r3.a(r0)
            java.lang.String r4 = "Event aggregate wasn't created during raw event logging. appId, event"
            r1.a(r4, r2, r3)
            com.google.android.gms.measurement.internal.d r1 = new com.google.android.gms.measurement.internal.d
            java.lang.String r10 = r14.f2137b
            r2 = 1
            r4 = 1
            java.lang.Long r6 = r14.c
            long r17 = r6.longValue()
            r19 = 0
            r6 = 0
            r21 = 0
            r22 = 0
            r32 = 0
            r44 = r24
            r8 = r1
            r46 = r9
            r45 = r25
            r9 = r68
            r47 = r26
            r48 = r11
            r49 = r12
            r11 = r2
            r3 = r70
            r2 = r14
            r13 = r4
            r5 = r15
            r15 = r17
            r17 = r19
            r19 = r6
            r20 = r21
            r21 = r22
            r22 = r32
            r8.<init>(r9, r10, r11, r13, r15, r17, r19, r20, r21, r22)
            goto L_0x0430
        L_0x03f0:
            r3 = r70
            r46 = r9
            r48 = r11
            r49 = r12
            r2 = r14
            r5 = r15
            r44 = r24
            r45 = r25
            r47 = r26
            com.google.android.gms.measurement.internal.d r4 = new com.google.android.gms.measurement.internal.d
            java.lang.String r6 = r1.f2497a
            java.lang.String r8 = r1.f2498b
            long r9 = r1.c
            long r53 = r9 + r32
            long r9 = r1.d
            long r55 = r9 + r32
            long r9 = r1.e
            long r11 = r1.f
            java.lang.Long r13 = r1.g
            java.lang.Long r14 = r1.h
            java.lang.Long r15 = r1.i
            java.lang.Boolean r1 = r1.j
            r50 = r4
            r51 = r6
            r52 = r8
            r57 = r9
            r59 = r11
            r61 = r13
            r62 = r14
            r63 = r15
            r64 = r1
            r50.<init>(r51, r52, r53, r55, r57, r59, r61, r62, r63, r64)
            r1 = r4
        L_0x0430:
            com.google.android.gms.measurement.internal.eo r4 = r67.h()
            r4.a(r1)
            long r8 = r1.c
            r10 = r41
            java.lang.Object r1 = r10.get(r0)
            java.util.Map r1 = (java.util.Map) r1
            if (r1 != 0) goto L_0x0455
            com.google.android.gms.measurement.internal.eo r1 = r67.h()
            java.util.Map r1 = r1.f(r5, r0)
            if (r1 != 0) goto L_0x0452
            android.support.v4.util.ArrayMap r1 = new android.support.v4.util.ArrayMap
            r1.<init>()
        L_0x0452:
            r10.put(r0, r1)
        L_0x0455:
            r11 = r1
            java.util.Set r1 = r11.keySet()
            java.util.Iterator r12 = r1.iterator()
        L_0x045e:
            boolean r1 = r12.hasNext()
            if (r1 == 0) goto L_0x0785
            java.lang.Object r1 = r12.next()
            java.lang.Integer r1 = (java.lang.Integer) r1
            int r13 = r1.intValue()
            java.lang.Integer r1 = java.lang.Integer.valueOf(r13)
            r14 = r48
            boolean r1 = r14.contains(r1)
            if (r1 == 0) goto L_0x048c
            com.google.android.gms.measurement.internal.o r1 = r67.q()
            com.google.android.gms.measurement.internal.q r1 = r1.k
            java.lang.Integer r4 = java.lang.Integer.valueOf(r13)
            r15 = r42
            r1.a(r15, r4)
            r48 = r14
            goto L_0x045e
        L_0x048c:
            r15 = r42
            java.lang.Integer r1 = java.lang.Integer.valueOf(r13)
            r6 = r49
            java.lang.Object r1 = r6.get(r1)
            com.google.android.gms.internal.measurement.cr r1 = (com.google.android.gms.internal.measurement.cr) r1
            java.lang.Integer r4 = java.lang.Integer.valueOf(r13)
            r41 = r10
            r10 = r47
            java.lang.Object r4 = r10.get(r4)
            java.util.BitSet r4 = (java.util.BitSet) r4
            r16 = r2
            java.lang.Integer r2 = java.lang.Integer.valueOf(r13)
            r17 = r12
            r12 = r45
            java.lang.Object r2 = r12.get(r2)
            java.util.BitSet r2 = (java.util.BitSet) r2
            r18 = r2
            if (r23 == 0) goto L_0x04d9
            java.lang.Integer r2 = java.lang.Integer.valueOf(r13)
            r7 = r44
            java.lang.Object r2 = r7.get(r2)
            java.util.Map r2 = (java.util.Map) r2
            r19 = r2
            java.lang.Integer r2 = java.lang.Integer.valueOf(r13)
            r42 = r15
            r15 = r43
            java.lang.Object r2 = r15.get(r2)
            java.util.Map r2 = (java.util.Map) r2
            goto L_0x04e2
        L_0x04d9:
            r42 = r15
            r15 = r43
            r7 = r44
            r2 = 0
            r19 = 0
        L_0x04e2:
            if (r1 != 0) goto L_0x0549
            com.google.android.gms.internal.measurement.cr r1 = new com.google.android.gms.internal.measurement.cr
            r1.<init>()
            java.lang.Integer r4 = java.lang.Integer.valueOf(r13)
            r6.put(r4, r1)
            r4 = r40
            r1.d = r4
            java.util.BitSet r1 = new java.util.BitSet
            r1.<init>()
            r20 = r2
            java.lang.Integer r2 = java.lang.Integer.valueOf(r13)
            r10.put(r2, r1)
            java.util.BitSet r2 = new java.util.BitSet
            r2.<init>()
            r18 = r1
            java.lang.Integer r1 = java.lang.Integer.valueOf(r13)
            r12.put(r1, r2)
            if (r23 == 0) goto L_0x0539
            android.support.v4.util.ArrayMap r1 = new android.support.v4.util.ArrayMap
            r1.<init>()
            r21 = r2
            java.lang.Integer r2 = java.lang.Integer.valueOf(r13)
            r7.put(r2, r1)
            android.support.v4.util.ArrayMap r2 = new android.support.v4.util.ArrayMap
            r2.<init>()
            r19 = r1
            java.lang.Integer r1 = java.lang.Integer.valueOf(r13)
            r15.put(r1, r2)
            r44 = r7
            r43 = r15
            r15 = r19
            r7 = r2
            r2 = r4
            r4 = r18
            goto L_0x0546
        L_0x0539:
            r21 = r2
            r2 = r4
            r44 = r7
            r43 = r15
            r4 = r18
            r15 = r19
            r7 = r20
        L_0x0546:
            r18 = r21
            goto L_0x0555
        L_0x0549:
            r20 = r2
            r2 = r40
            r44 = r7
            r43 = r15
            r15 = r19
            r7 = r20
        L_0x0555:
            java.lang.Integer r1 = java.lang.Integer.valueOf(r13)
            java.lang.Object r1 = r11.get(r1)
            java.util.List r1 = (java.util.List) r1
            java.util.Iterator r19 = r1.iterator()
        L_0x0563:
            boolean r1 = r19.hasNext()
            if (r1 == 0) goto L_0x0768
            java.lang.Object r1 = r19.next()
            com.google.android.gms.internal.measurement.cj r1 = (com.google.android.gms.internal.measurement.cj) r1
            r40 = r2
            com.google.android.gms.measurement.internal.o r2 = r67.q()
            r20 = r11
            r11 = 2
            boolean r2 = r2.a(r11)
            if (r2 == 0) goto L_0x05af
            com.google.android.gms.measurement.internal.o r2 = r67.q()
            com.google.android.gms.measurement.internal.q r2 = r2.k
            java.lang.Integer r11 = java.lang.Integer.valueOf(r13)
            java.lang.Integer r3 = r1.f2116a
            com.google.android.gms.measurement.internal.m r5 = r67.n()
            r49 = r6
            java.lang.String r6 = r1.f2117b
            java.lang.String r5 = r5.a(r6)
            java.lang.String r6 = "Evaluating filter. audience, filter, event"
            r2.a(r6, r11, r3, r5)
            com.google.android.gms.measurement.internal.o r2 = r67.q()
            com.google.android.gms.measurement.internal.q r2 = r2.k
            com.google.android.gms.measurement.internal.ea r3 = r67.f()
            java.lang.String r3 = r3.a(r1)
            r11 = r46
            r2.a(r11, r3)
            goto L_0x05b3
        L_0x05af:
            r49 = r6
            r11 = r46
        L_0x05b3:
            java.lang.Integer r2 = r1.f2116a
            if (r2 == 0) goto L_0x0726
            java.lang.Integer r2 = r1.f2116a
            int r2 = r2.intValue()
            r5 = 256(0x100, float:3.59E-43)
            if (r2 <= r5) goto L_0x05c3
            goto L_0x0726
        L_0x05c3:
            java.lang.String r6 = "Event filter result"
            if (r23 == 0) goto L_0x069d
            if (r1 == 0) goto L_0x05d8
            java.lang.Boolean r2 = r1.e
            if (r2 == 0) goto L_0x05d8
            java.lang.Boolean r2 = r1.e
            boolean r2 = r2.booleanValue()
            if (r2 == 0) goto L_0x05d8
            r21 = 1
            goto L_0x05da
        L_0x05d8:
            r21 = 0
        L_0x05da:
            if (r1 == 0) goto L_0x05eb
            java.lang.Boolean r2 = r1.f
            if (r2 == 0) goto L_0x05eb
            java.lang.Boolean r2 = r1.f
            boolean r2 = r2.booleanValue()
            if (r2 == 0) goto L_0x05eb
            r22 = 1
            goto L_0x05ed
        L_0x05eb:
            r22 = 0
        L_0x05ed:
            java.lang.Integer r2 = r1.f2116a
            int r2 = r2.intValue()
            boolean r2 = r4.get(r2)
            if (r2 == 0) goto L_0x061c
            if (r21 != 0) goto L_0x061c
            if (r22 != 0) goto L_0x061c
            com.google.android.gms.measurement.internal.o r2 = r67.q()
            com.google.android.gms.measurement.internal.q r2 = r2.k
            java.lang.Integer r3 = java.lang.Integer.valueOf(r13)
            java.lang.Integer r1 = r1.f2116a
            java.lang.String r6 = "Event filter already evaluated true and it is not associated with a dynamic audience. audience ID, filter ID"
            r2.a(r6, r3, r1)
            r5 = r68
            r3 = r70
            r46 = r11
            r11 = r20
            r2 = r40
            r6 = r49
            goto L_0x0563
        L_0x061c:
            r3 = r1
            r2 = r18
            r1 = r67
            r46 = r11
            r45 = r12
            r11 = r16
            r65 = r40
            r12 = r2
            r2 = r3
            r47 = r10
            r10 = r3
            r3 = r0
            r16 = r0
            r0 = r4
            r4 = r35
            r18 = r15
            r66 = r49
            r15 = r6
            r5 = r8
            java.lang.Boolean r1 = r1.a(r2, r3, r4, r5)
            com.google.android.gms.measurement.internal.o r2 = r67.q()
            com.google.android.gms.measurement.internal.q r2 = r2.k
            if (r1 != 0) goto L_0x0649
            r3 = r27
            goto L_0x064a
        L_0x0649:
            r3 = r1
        L_0x064a:
            r2.a(r15, r3)
            if (r1 != 0) goto L_0x0658
            java.lang.Integer r1 = java.lang.Integer.valueOf(r13)
            r14.add(r1)
            goto L_0x074f
        L_0x0658:
            java.lang.Integer r2 = r10.f2116a
            int r2 = r2.intValue()
            r12.set(r2)
            boolean r1 = r1.booleanValue()
            if (r1 == 0) goto L_0x074f
            java.lang.Integer r1 = r10.f2116a
            int r1 = r1.intValue()
            r0.set(r1)
            if (r21 != 0) goto L_0x0674
            if (r22 == 0) goto L_0x074f
        L_0x0674:
            java.lang.Long r1 = r11.c
            if (r1 == 0) goto L_0x074f
            if (r22 == 0) goto L_0x068b
            java.lang.Integer r1 = r10.f2116a
            int r1 = r1.intValue()
            java.lang.Long r2 = r11.c
            long r2 = r2.longValue()
            b(r7, r1, r2)
            goto L_0x074f
        L_0x068b:
            java.lang.Integer r1 = r10.f2116a
            int r1 = r1.intValue()
            java.lang.Long r2 = r11.c
            long r2 = r2.longValue()
            r5 = r18
            a(r5, r1, r2)
            goto L_0x06ce
        L_0x069d:
            r47 = r10
            r46 = r11
            r45 = r12
            r5 = r15
            r11 = r16
            r12 = r18
            r65 = r40
            r66 = r49
            r16 = r0
            r10 = r1
            r0 = r4
            r15 = r6
            java.lang.Integer r1 = r10.f2116a
            int r1 = r1.intValue()
            boolean r1 = r0.get(r1)
            if (r1 == 0) goto L_0x06e6
            com.google.android.gms.measurement.internal.o r1 = r67.q()
            com.google.android.gms.measurement.internal.q r1 = r1.k
            java.lang.Integer r2 = java.lang.Integer.valueOf(r13)
            java.lang.Integer r3 = r10.f2116a
            java.lang.String r4 = "Event filter already evaluated true. audience ID, filter ID"
            r1.a(r4, r2, r3)
        L_0x06ce:
            r3 = r70
            r4 = r0
            r15 = r5
            r18 = r12
            r0 = r16
            r12 = r45
            r10 = r47
            r2 = r65
            r6 = r66
            r5 = r68
            r16 = r11
            r11 = r20
            goto L_0x0563
        L_0x06e6:
            r1 = r67
            r2 = r10
            r3 = r16
            r4 = r35
            r18 = r5
            r5 = r8
            java.lang.Boolean r1 = r1.a(r2, r3, r4, r5)
            com.google.android.gms.measurement.internal.o r2 = r67.q()
            com.google.android.gms.measurement.internal.q r2 = r2.k
            if (r1 != 0) goto L_0x06ff
            r3 = r27
            goto L_0x0700
        L_0x06ff:
            r3 = r1
        L_0x0700:
            r2.a(r15, r3)
            if (r1 != 0) goto L_0x070d
            java.lang.Integer r1 = java.lang.Integer.valueOf(r13)
            r14.add(r1)
            goto L_0x074f
        L_0x070d:
            java.lang.Integer r2 = r10.f2116a
            int r2 = r2.intValue()
            r12.set(r2)
            boolean r1 = r1.booleanValue()
            if (r1 == 0) goto L_0x074f
            java.lang.Integer r1 = r10.f2116a
            int r1 = r1.intValue()
            r0.set(r1)
            goto L_0x074f
        L_0x0726:
            r47 = r10
            r46 = r11
            r45 = r12
            r11 = r16
            r12 = r18
            r65 = r40
            r66 = r49
            r16 = r0
            r10 = r1
            r0 = r4
            r18 = r15
            com.google.android.gms.measurement.internal.o r1 = r67.q()
            com.google.android.gms.measurement.internal.q r1 = r1.f
            java.lang.Object r2 = com.google.android.gms.measurement.internal.o.a(r68)
            java.lang.Integer r3 = r10.f2116a
            java.lang.String r3 = java.lang.String.valueOf(r3)
            java.lang.String r4 = "Invalid event filter ID. appId, id"
            r1.a(r4, r2, r3)
        L_0x074f:
            r5 = r68
            r3 = r70
            r4 = r0
            r0 = r16
            r15 = r18
            r10 = r47
            r2 = r65
            r6 = r66
            r16 = r11
            r18 = r12
            r11 = r20
            r12 = r45
            goto L_0x0563
        L_0x0768:
            r20 = r11
            r11 = r16
            r7 = r67
            r5 = r68
            r3 = r70
            r40 = r2
            r49 = r6
            r47 = r10
            r2 = r11
            r45 = r12
            r48 = r14
            r12 = r17
            r11 = r20
            r10 = r41
            goto L_0x045e
        L_0x0785:
            r41 = r10
            r65 = r40
            r14 = r48
            r66 = r49
            r1 = r34
            r0 = r36
            r20 = r38
        L_0x0793:
            int r2 = r30 + 1
            r7 = r67
            r15 = r68
            r13 = r69
            r11 = r14
            r3 = r31
            r5 = r41
            r4 = r42
            r6 = r43
            r24 = r44
            r25 = r45
            r9 = r46
            r26 = r47
            r22 = r65
            r12 = r66
            r14 = r70
            goto L_0x01a2
        L_0x07b4:
            r42 = r4
            r43 = r6
            r46 = r9
            r14 = r11
            r66 = r12
            r65 = r22
            r44 = r24
            r45 = r25
            r47 = r26
            r37 = 1
            r1 = r70
            if (r1 == 0) goto L_0x0af0
            android.support.v4.util.ArrayMap r0 = new android.support.v4.util.ArrayMap
            r0.<init>()
            int r2 = r1.length
            r3 = 0
        L_0x07d2:
            if (r3 >= r2) goto L_0x0af0
            r4 = r1[r3]
            java.lang.String r5 = r4.f2148b
            java.lang.Object r5 = r0.get(r5)
            java.util.Map r5 = (java.util.Map) r5
            if (r5 != 0) goto L_0x07f9
            com.google.android.gms.measurement.internal.eo r5 = r67.h()
            java.lang.String r6 = r4.f2148b
            r7 = r68
            java.util.Map r5 = r5.g(r7, r6)
            if (r5 != 0) goto L_0x07f3
            android.support.v4.util.ArrayMap r5 = new android.support.v4.util.ArrayMap
            r5.<init>()
        L_0x07f3:
            java.lang.String r6 = r4.f2148b
            r0.put(r6, r5)
            goto L_0x07fb
        L_0x07f9:
            r7 = r68
        L_0x07fb:
            java.util.Set r6 = r5.keySet()
            java.util.Iterator r6 = r6.iterator()
        L_0x0803:
            boolean r8 = r6.hasNext()
            if (r8 == 0) goto L_0x0ade
            java.lang.Object r8 = r6.next()
            java.lang.Integer r8 = (java.lang.Integer) r8
            int r8 = r8.intValue()
            java.lang.Integer r9 = java.lang.Integer.valueOf(r8)
            boolean r9 = r14.contains(r9)
            if (r9 == 0) goto L_0x082d
            com.google.android.gms.measurement.internal.o r9 = r67.q()
            com.google.android.gms.measurement.internal.q r9 = r9.k
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
            r10 = r42
            r9.a(r10, r8)
            goto L_0x0803
        L_0x082d:
            r10 = r42
            java.lang.Integer r9 = java.lang.Integer.valueOf(r8)
            r11 = r66
            java.lang.Object r9 = r11.get(r9)
            com.google.android.gms.internal.measurement.cr r9 = (com.google.android.gms.internal.measurement.cr) r9
            java.lang.Integer r12 = java.lang.Integer.valueOf(r8)
            r13 = r47
            java.lang.Object r12 = r13.get(r12)
            java.util.BitSet r12 = (java.util.BitSet) r12
            java.lang.Integer r15 = java.lang.Integer.valueOf(r8)
            r1 = r45
            java.lang.Object r15 = r1.get(r15)
            java.util.BitSet r15 = (java.util.BitSet) r15
            r69 = r0
            if (r23 == 0) goto L_0x0876
            java.lang.Integer r0 = java.lang.Integer.valueOf(r8)
            r16 = r2
            r2 = r44
            java.lang.Object r0 = r2.get(r0)
            java.util.Map r0 = (java.util.Map) r0
            r17 = r0
            java.lang.Integer r0 = java.lang.Integer.valueOf(r8)
            r18 = r6
            r6 = r43
            java.lang.Object r0 = r6.get(r0)
            java.util.Map r0 = (java.util.Map) r0
            goto L_0x0881
        L_0x0876:
            r16 = r2
            r18 = r6
            r6 = r43
            r2 = r44
            r0 = 0
            r17 = 0
        L_0x0881:
            if (r9 != 0) goto L_0x08e0
            com.google.android.gms.internal.measurement.cr r9 = new com.google.android.gms.internal.measurement.cr
            r9.<init>()
            java.lang.Integer r12 = java.lang.Integer.valueOf(r8)
            r11.put(r12, r9)
            r12 = r65
            r9.d = r12
            java.util.BitSet r9 = new java.util.BitSet
            r9.<init>()
            java.lang.Integer r15 = java.lang.Integer.valueOf(r8)
            r13.put(r15, r9)
            java.util.BitSet r15 = new java.util.BitSet
            r15.<init>()
            r19 = r0
            java.lang.Integer r0 = java.lang.Integer.valueOf(r8)
            r1.put(r0, r15)
            if (r23 == 0) goto L_0x08d3
            android.support.v4.util.ArrayMap r0 = new android.support.v4.util.ArrayMap
            r0.<init>()
            r20 = r9
            java.lang.Integer r9 = java.lang.Integer.valueOf(r8)
            r2.put(r9, r0)
            android.support.v4.util.ArrayMap r9 = new android.support.v4.util.ArrayMap
            r9.<init>()
            r17 = r0
            java.lang.Integer r0 = java.lang.Integer.valueOf(r8)
            r6.put(r0, r9)
            r44 = r2
            r2 = r9
            r40 = r12
            r0 = r17
            goto L_0x08dd
        L_0x08d3:
            r20 = r9
            r44 = r2
            r40 = r12
            r0 = r17
            r2 = r19
        L_0x08dd:
            r12 = r20
            goto L_0x08ea
        L_0x08e0:
            r19 = r0
            r44 = r2
            r0 = r17
            r2 = r19
            r40 = r65
        L_0x08ea:
            java.lang.Integer r9 = java.lang.Integer.valueOf(r8)
            java.lang.Object r9 = r5.get(r9)
            java.util.List r9 = (java.util.List) r9
            java.util.Iterator r9 = r9.iterator()
        L_0x08f8:
            boolean r17 = r9.hasNext()
            if (r17 == 0) goto L_0x0ac4
            java.lang.Object r17 = r9.next()
            r19 = r5
            r5 = r17
            com.google.android.gms.internal.measurement.cm r5 = (com.google.android.gms.internal.measurement.cm) r5
            r17 = r9
            com.google.android.gms.measurement.internal.o r9 = r67.q()
            r42 = r10
            r10 = 2
            boolean r9 = r9.a(r10)
            if (r9 == 0) goto L_0x094a
            com.google.android.gms.measurement.internal.o r9 = r67.q()
            com.google.android.gms.measurement.internal.q r9 = r9.k
            java.lang.Integer r10 = java.lang.Integer.valueOf(r8)
            java.lang.Integer r7 = r5.f2122a
            r43 = r6
            com.google.android.gms.measurement.internal.m r6 = r67.n()
            r45 = r1
            java.lang.String r1 = r5.f2123b
            java.lang.String r1 = r6.c(r1)
            java.lang.String r6 = "Evaluating filter. audience, filter, property"
            r9.a(r6, r10, r7, r1)
            com.google.android.gms.measurement.internal.o r1 = r67.q()
            com.google.android.gms.measurement.internal.q r1 = r1.k
            com.google.android.gms.measurement.internal.ea r6 = r67.f()
            java.lang.String r6 = r6.a(r5)
            r7 = r46
            r1.a(r7, r6)
            goto L_0x0950
        L_0x094a:
            r45 = r1
            r43 = r6
            r7 = r46
        L_0x0950:
            java.lang.Integer r1 = r5.f2122a
            if (r1 == 0) goto L_0x0a8e
            java.lang.Integer r1 = r5.f2122a
            int r1 = r1.intValue()
            r6 = 256(0x100, float:3.59E-43)
            if (r1 <= r6) goto L_0x0960
            goto L_0x0a8e
        L_0x0960:
            java.lang.String r1 = "Property filter result"
            if (r23 == 0) goto L_0x0a25
            if (r5 == 0) goto L_0x0974
            java.lang.Boolean r9 = r5.d
            if (r9 == 0) goto L_0x0974
            java.lang.Boolean r9 = r5.d
            boolean r9 = r9.booleanValue()
            if (r9 == 0) goto L_0x0974
            r9 = 1
            goto L_0x0975
        L_0x0974:
            r9 = 0
        L_0x0975:
            if (r5 == 0) goto L_0x0985
            java.lang.Boolean r10 = r5.e
            if (r10 == 0) goto L_0x0985
            java.lang.Boolean r10 = r5.e
            boolean r10 = r10.booleanValue()
            if (r10 == 0) goto L_0x0985
            r10 = 1
            goto L_0x0986
        L_0x0985:
            r10 = 0
        L_0x0986:
            java.lang.Integer r6 = r5.f2122a
            int r6 = r6.intValue()
            boolean r6 = r12.get(r6)
            if (r6 == 0) goto L_0x09b7
            if (r9 != 0) goto L_0x09b7
            if (r10 != 0) goto L_0x09b7
            com.google.android.gms.measurement.internal.o r1 = r67.q()
            com.google.android.gms.measurement.internal.q r1 = r1.k
            java.lang.Integer r6 = java.lang.Integer.valueOf(r8)
            java.lang.Integer r5 = r5.f2122a
            java.lang.String r9 = "Property filter already evaluated true and it is not associated with a dynamic audience. audience ID, filter ID"
            r1.a(r9, r6, r5)
            r46 = r7
            r9 = r17
            r5 = r19
            r10 = r42
            r6 = r43
            r1 = r45
            r7 = r68
            goto L_0x08f8
        L_0x09b7:
            r6 = r67
            r46 = r7
            r7 = r44
            java.lang.Boolean r20 = r6.a(r5, r4)
            r44 = r7
            com.google.android.gms.measurement.internal.o r7 = r67.q()
            com.google.android.gms.measurement.internal.q r7 = r7.k
            r49 = r11
            if (r20 != 0) goto L_0x09d0
            r11 = r27
            goto L_0x09d2
        L_0x09d0:
            r11 = r20
        L_0x09d2:
            r7.a(r1, r11)
            if (r20 != 0) goto L_0x09df
            java.lang.Integer r1 = java.lang.Integer.valueOf(r8)
            r14.add(r1)
            goto L_0x0a48
        L_0x09df:
            java.lang.Integer r1 = r5.f2122a
            int r1 = r1.intValue()
            r15.set(r1)
            java.lang.Integer r1 = r5.f2122a
            int r1 = r1.intValue()
            boolean r7 = r20.booleanValue()
            r12.set(r1, r7)
            boolean r1 = r20.booleanValue()
            if (r1 == 0) goto L_0x0a48
            if (r9 != 0) goto L_0x09ff
            if (r10 == 0) goto L_0x0a48
        L_0x09ff:
            java.lang.Long r1 = r4.f2147a
            if (r1 == 0) goto L_0x0a48
            if (r10 == 0) goto L_0x0a15
            java.lang.Integer r1 = r5.f2122a
            int r1 = r1.intValue()
            java.lang.Long r5 = r4.f2147a
            long r9 = r5.longValue()
            b(r2, r1, r9)
            goto L_0x0a48
        L_0x0a15:
            java.lang.Integer r1 = r5.f2122a
            int r1 = r1.intValue()
            java.lang.Long r5 = r4.f2147a
            long r9 = r5.longValue()
            a(r0, r1, r9)
            goto L_0x0a48
        L_0x0a25:
            r6 = r67
            r46 = r7
            r49 = r11
            java.lang.Integer r7 = r5.f2122a
            int r7 = r7.intValue()
            boolean r7 = r12.get(r7)
            if (r7 == 0) goto L_0x0a58
            com.google.android.gms.measurement.internal.o r1 = r67.q()
            com.google.android.gms.measurement.internal.q r1 = r1.k
            java.lang.Integer r7 = java.lang.Integer.valueOf(r8)
            java.lang.Integer r5 = r5.f2122a
            java.lang.String r9 = "Property filter already evaluated true. audience ID, filter ID"
            r1.a(r9, r7, r5)
        L_0x0a48:
            r7 = r68
            r9 = r17
            r5 = r19
            r10 = r42
            r6 = r43
            r1 = r45
            r11 = r49
            goto L_0x08f8
        L_0x0a58:
            java.lang.Boolean r7 = r6.a(r5, r4)
            com.google.android.gms.measurement.internal.o r9 = r67.q()
            com.google.android.gms.measurement.internal.q r9 = r9.k
            if (r7 != 0) goto L_0x0a67
            r10 = r27
            goto L_0x0a68
        L_0x0a67:
            r10 = r7
        L_0x0a68:
            r9.a(r1, r10)
            if (r7 != 0) goto L_0x0a75
            java.lang.Integer r1 = java.lang.Integer.valueOf(r8)
            r14.add(r1)
            goto L_0x0a48
        L_0x0a75:
            java.lang.Integer r1 = r5.f2122a
            int r1 = r1.intValue()
            r15.set(r1)
            boolean r1 = r7.booleanValue()
            if (r1 == 0) goto L_0x0a48
            java.lang.Integer r1 = r5.f2122a
            int r1 = r1.intValue()
            r12.set(r1)
            goto L_0x0a48
        L_0x0a8e:
            r6 = r67
            r46 = r7
            r49 = r11
            com.google.android.gms.measurement.internal.o r0 = r67.q()
            com.google.android.gms.measurement.internal.q r0 = r0.f
            java.lang.Object r1 = com.google.android.gms.measurement.internal.o.a(r68)
            java.lang.Integer r2 = r5.f2122a
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r5 = "Invalid property filter ID. appId, id"
            r0.a(r5, r1, r2)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r8)
            r14.add(r0)
            r7 = r68
            r0 = r69
            r1 = r70
            r47 = r13
            r2 = r16
            r6 = r18
            r5 = r19
            r65 = r40
            r66 = r49
            goto L_0x0803
        L_0x0ac4:
            r43 = r6
            r6 = r67
            r7 = r68
            r0 = r69
            r45 = r1
            r42 = r10
            r66 = r11
            r47 = r13
            r2 = r16
            r6 = r18
            r65 = r40
            r1 = r70
            goto L_0x0803
        L_0x0ade:
            r6 = r67
            r69 = r0
            r16 = r2
            r13 = r47
            r40 = r65
            r49 = r66
            int r3 = r3 + 1
            r1 = r70
            goto L_0x07d2
        L_0x0af0:
            r6 = r67
            r13 = r47
            r49 = r66
            int r0 = r13.size()
            com.google.android.gms.internal.measurement.cr[] r1 = new com.google.android.gms.internal.measurement.cr[r0]
            java.util.Set r0 = r13.keySet()
            java.util.Iterator r2 = r0.iterator()
            r5 = 0
        L_0x0b05:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0ca6
            java.lang.Object r0 = r2.next()
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r0)
            boolean r3 = r14.contains(r3)
            if (r3 != 0) goto L_0x0ca2
            java.lang.Integer r3 = java.lang.Integer.valueOf(r0)
            r4 = r49
            java.lang.Object r3 = r4.get(r3)
            com.google.android.gms.internal.measurement.cr r3 = (com.google.android.gms.internal.measurement.cr) r3
            if (r3 != 0) goto L_0x0b32
            com.google.android.gms.internal.measurement.cr r3 = new com.google.android.gms.internal.measurement.cr
            r3.<init>()
        L_0x0b32:
            int r7 = r5 + 1
            r1[r5] = r3
            java.lang.Integer r5 = java.lang.Integer.valueOf(r0)
            r3.f2132a = r5
            com.google.android.gms.internal.measurement.cx r5 = new com.google.android.gms.internal.measurement.cx
            r5.<init>()
            r3.f2133b = r5
            com.google.android.gms.internal.measurement.cx r5 = r3.f2133b
            java.lang.Integer r8 = java.lang.Integer.valueOf(r0)
            java.lang.Object r8 = r13.get(r8)
            java.util.BitSet r8 = (java.util.BitSet) r8
            long[] r8 = com.google.android.gms.measurement.internal.ea.a(r8)
            r5.f2144b = r8
            com.google.android.gms.internal.measurement.cx r5 = r3.f2133b
            java.lang.Integer r8 = java.lang.Integer.valueOf(r0)
            r9 = r45
            java.lang.Object r8 = r9.get(r8)
            java.util.BitSet r8 = (java.util.BitSet) r8
            long[] r8 = com.google.android.gms.measurement.internal.ea.a(r8)
            r5.f2143a = r8
            if (r23 == 0) goto L_0x0c09
            com.google.android.gms.internal.measurement.cx r5 = r3.f2133b
            java.lang.Integer r8 = java.lang.Integer.valueOf(r0)
            r10 = r44
            java.lang.Object r8 = r10.get(r8)
            java.util.Map r8 = (java.util.Map) r8
            com.google.android.gms.internal.measurement.cs[] r8 = a(r8)
            r5.c = r8
            com.google.android.gms.internal.measurement.cx r5 = r3.f2133b
            java.lang.Integer r8 = java.lang.Integer.valueOf(r0)
            r11 = r43
            java.lang.Object r8 = r11.get(r8)
            java.util.Map r8 = (java.util.Map) r8
            if (r8 != 0) goto L_0x0b99
            r12 = 0
            com.google.android.gms.internal.measurement.cy[] r8 = new com.google.android.gms.internal.measurement.cy[r12]
            r69 = r2
            r49 = r4
            r15 = r8
            goto L_0x0c06
        L_0x0b99:
            r12 = 0
            int r15 = r8.size()
            com.google.android.gms.internal.measurement.cy[] r15 = new com.google.android.gms.internal.measurement.cy[r15]
            java.util.Set r16 = r8.keySet()
            java.util.Iterator r16 = r16.iterator()
            r17 = 0
        L_0x0baa:
            boolean r18 = r16.hasNext()
            if (r18 == 0) goto L_0x0c02
            java.lang.Object r18 = r16.next()
            r12 = r18
            java.lang.Integer r12 = (java.lang.Integer) r12
            r69 = r2
            com.google.android.gms.internal.measurement.cy r2 = new com.google.android.gms.internal.measurement.cy
            r2.<init>()
            r2.f2145a = r12
            java.lang.Object r12 = r8.get(r12)
            java.util.List r12 = (java.util.List) r12
            if (r12 == 0) goto L_0x0bf4
            java.util.Collections.sort(r12)
            r49 = r4
            int r4 = r12.size()
            long[] r4 = new long[r4]
            java.util.Iterator r12 = r12.iterator()
            r18 = 0
        L_0x0bda:
            boolean r19 = r12.hasNext()
            if (r19 == 0) goto L_0x0bf1
            java.lang.Object r19 = r12.next()
            java.lang.Long r19 = (java.lang.Long) r19
            int r20 = r18 + 1
            long r21 = r19.longValue()
            r4[r18] = r21
            r18 = r20
            goto L_0x0bda
        L_0x0bf1:
            r2.f2146b = r4
            goto L_0x0bf6
        L_0x0bf4:
            r49 = r4
        L_0x0bf6:
            int r4 = r17 + 1
            r15[r17] = r2
            r2 = r69
            r17 = r4
            r4 = r49
            r12 = 0
            goto L_0x0baa
        L_0x0c02:
            r69 = r2
            r49 = r4
        L_0x0c06:
            r5.d = r15
            goto L_0x0c11
        L_0x0c09:
            r69 = r2
            r49 = r4
            r11 = r43
            r10 = r44
        L_0x0c11:
            com.google.android.gms.measurement.internal.eo r2 = r67.h()
            com.google.android.gms.internal.measurement.cx r3 = r3.f2133b
            r2.j()
            r2.c()
            com.google.android.gms.common.internal.l.a(r68)
            com.google.android.gms.common.internal.l.a(r3)
            int r4 = r3.e()     // Catch:{ IOException -> 0x0c84 }
            byte[] r4 = new byte[r4]     // Catch:{ IOException -> 0x0c84 }
            int r5 = r4.length     // Catch:{ IOException -> 0x0c84 }
            com.google.android.gms.internal.measurement.iy r5 = com.google.android.gms.internal.measurement.iy.a(r4, r5)     // Catch:{ IOException -> 0x0c84 }
            r3.a(r5)     // Catch:{ IOException -> 0x0c84 }
            r5.a()     // Catch:{ IOException -> 0x0c84 }
            android.content.ContentValues r3 = new android.content.ContentValues
            r3.<init>()
            java.lang.String r5 = "app_id"
            r8 = r68
            r3.put(r5, r8)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.String r5 = "audience_id"
            r3.put(r5, r0)
            java.lang.String r0 = "current_results"
            r3.put(r0, r4)
            android.database.sqlite.SQLiteDatabase r0 = r2.w()     // Catch:{ SQLiteException -> 0x0c72 }
            java.lang.String r4 = "audience_filter_values"
            r5 = 5
            r12 = 0
            long r3 = r0.insertWithOnConflict(r4, r12, r3, r5)     // Catch:{ SQLiteException -> 0x0c70 }
            r15 = -1
            int r0 = (r3 > r15 ? 1 : (r3 == r15 ? 0 : -1))
            if (r0 != 0) goto L_0x0c97
            com.google.android.gms.measurement.internal.o r0 = r2.q()     // Catch:{ SQLiteException -> 0x0c70 }
            com.google.android.gms.measurement.internal.q r0 = r0.c     // Catch:{ SQLiteException -> 0x0c70 }
            java.lang.String r3 = "Failed to insert filter results (got -1). appId"
            java.lang.Object r4 = com.google.android.gms.measurement.internal.o.a(r68)     // Catch:{ SQLiteException -> 0x0c70 }
            r0.a(r3, r4)     // Catch:{ SQLiteException -> 0x0c70 }
            goto L_0x0c97
        L_0x0c70:
            r0 = move-exception
            goto L_0x0c74
        L_0x0c72:
            r0 = move-exception
            r12 = 0
        L_0x0c74:
            com.google.android.gms.measurement.internal.o r2 = r2.q()
            com.google.android.gms.measurement.internal.q r2 = r2.c
            java.lang.Object r3 = com.google.android.gms.measurement.internal.o.a(r68)
            java.lang.String r4 = "Error storing filter results. appId"
            r2.a(r4, r3, r0)
            goto L_0x0c97
        L_0x0c84:
            r0 = move-exception
            r8 = r68
            r12 = 0
            com.google.android.gms.measurement.internal.o r2 = r2.q()
            com.google.android.gms.measurement.internal.q r2 = r2.c
            java.lang.Object r3 = com.google.android.gms.measurement.internal.o.a(r68)
            java.lang.String r4 = "Configuration loss. Failed to serialize filter results. appId"
            r2.a(r4, r3, r0)
        L_0x0c97:
            r2 = r69
            r5 = r7
            r45 = r9
            r44 = r10
            r43 = r11
            goto L_0x0b05
        L_0x0ca2:
            r8 = r68
            goto L_0x0b05
        L_0x0ca6:
            java.lang.Object[] r0 = java.util.Arrays.copyOf(r1, r5)
            com.google.android.gms.internal.measurement.cr[] r0 = (com.google.android.gms.internal.measurement.cr[]) r0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.ei.a(java.lang.String, com.google.android.gms.internal.measurement.ct[], com.google.android.gms.internal.measurement.cz[]):com.google.android.gms.internal.measurement.cr[]");
    }

    private final Boolean a(cj cjVar, String str, cu[] cuVarArr, long j) {
        Boolean bool;
        if (cjVar.d != null) {
            Boolean a2 = a(j, cjVar.d);
            if (a2 == null) {
                return null;
            }
            if (!a2.booleanValue()) {
                return false;
            }
        }
        HashSet hashSet = new HashSet();
        for (ck ckVar : cjVar.c) {
            if (TextUtils.isEmpty(ckVar.d)) {
                q().f.a("null or empty param name in filter. event", n().a(str));
                return null;
            }
            hashSet.add(ckVar.d);
        }
        ArrayMap arrayMap = new ArrayMap();
        for (cu cuVar : cuVarArr) {
            if (hashSet.contains(cuVar.f2138a)) {
                if (cuVar.c != null) {
                    arrayMap.put(cuVar.f2138a, cuVar.c);
                } else if (cuVar.d != null) {
                    arrayMap.put(cuVar.f2138a, cuVar.d);
                } else if (cuVar.f2139b != null) {
                    arrayMap.put(cuVar.f2138a, cuVar.f2139b);
                } else {
                    q().f.a("Unknown value for param. event, param", n().a(str), n().b(cuVar.f2138a));
                    return null;
                }
            }
        }
        for (ck ckVar2 : cjVar.c) {
            boolean equals = Boolean.TRUE.equals(ckVar2.c);
            String str2 = ckVar2.d;
            if (TextUtils.isEmpty(str2)) {
                q().f.a("Event has empty param name. event", n().a(str));
                return null;
            }
            Object obj = arrayMap.get(str2);
            if (obj instanceof Long) {
                if (ckVar2.f2119b == null) {
                    q().f.a("No number filter for long param. event, param", n().a(str), n().b(str2));
                    return null;
                }
                Boolean a3 = a(((Long) obj).longValue(), ckVar2.f2119b);
                if (a3 == null) {
                    return null;
                }
                if ((true ^ a3.booleanValue()) ^ equals) {
                    return false;
                }
            } else if (obj instanceof Double) {
                if (ckVar2.f2119b == null) {
                    q().f.a("No number filter for double param. event, param", n().a(str), n().b(str2));
                    return null;
                }
                Boolean a4 = a(((Double) obj).doubleValue(), ckVar2.f2119b);
                if (a4 == null) {
                    return null;
                }
                if ((true ^ a4.booleanValue()) ^ equals) {
                    return false;
                }
            } else if (obj instanceof String) {
                if (ckVar2.f2118a != null) {
                    bool = a((String) obj, ckVar2.f2118a);
                } else if (ckVar2.f2119b != null) {
                    String str3 = (String) obj;
                    if (ea.a(str3)) {
                        bool = a(str3, ckVar2.f2119b);
                    } else {
                        q().f.a("Invalid param value for number filter. event, param", n().a(str), n().b(str2));
                        return null;
                    }
                } else {
                    q().f.a("No filter for String param. event, param", n().a(str), n().b(str2));
                    return null;
                }
                if (bool == null) {
                    return null;
                }
                if ((true ^ bool.booleanValue()) ^ equals) {
                    return false;
                }
            } else if (obj == null) {
                q().k.a("Missing param for filter. event, param", n().a(str), n().b(str2));
                return false;
            } else {
                q().f.a("Unknown param type. event, param", n().a(str), n().b(str2));
                return null;
            }
        }
        return true;
    }

    private final Boolean a(cm cmVar, cz czVar) {
        ck ckVar = cmVar.c;
        if (ckVar == null) {
            q().f.a("Missing property filter. property", n().c(czVar.f2148b));
            return null;
        }
        boolean equals = Boolean.TRUE.equals(ckVar.c);
        if (czVar.d != null) {
            if (ckVar.f2119b != null) {
                return a(a(czVar.d.longValue(), ckVar.f2119b), equals);
            }
            q().f.a("No number filter for long property. property", n().c(czVar.f2148b));
            return null;
        } else if (czVar.e != null) {
            if (ckVar.f2119b != null) {
                return a(a(czVar.e.doubleValue(), ckVar.f2119b), equals);
            }
            q().f.a("No number filter for double property. property", n().c(czVar.f2148b));
            return null;
        } else if (czVar.c == null) {
            q().f.a("User property has no value, property", n().c(czVar.f2148b));
            return null;
        } else if (ckVar.f2118a != null) {
            return a(a(czVar.c, ckVar.f2118a), equals);
        } else {
            if (ckVar.f2119b == null) {
                q().f.a("No string or number filter defined. property", n().c(czVar.f2148b));
            } else if (ea.a(czVar.c)) {
                return a(a(czVar.c, ckVar.f2119b), equals);
            } else {
                q().f.a("Invalid user property value for Numeric number filter. property, value", n().c(czVar.f2148b), czVar.c);
            }
            return null;
        }
    }

    private static Boolean a(Boolean bool, boolean z) {
        if (bool == null) {
            return null;
        }
        return Boolean.valueOf(bool.booleanValue() ^ z);
    }

    private final Boolean a(String str, cn cnVar) {
        String str2;
        ArrayList arrayList;
        l.a(cnVar);
        if (str == null || cnVar.f2124a == null || cnVar.f2124a.intValue() == 0) {
            return null;
        }
        if (cnVar.f2124a.intValue() == 6) {
            if (cnVar.d == null || cnVar.d.length == 0) {
                return null;
            }
        } else if (cnVar.f2125b == null) {
            return null;
        }
        int intValue = cnVar.f2124a.intValue();
        boolean z = cnVar.c != null && cnVar.c.booleanValue();
        if (z || intValue == 1 || intValue == 6) {
            str2 = cnVar.f2125b;
        } else {
            str2 = cnVar.f2125b.toUpperCase(Locale.ENGLISH);
        }
        String str3 = str2;
        if (cnVar.d == null) {
            arrayList = null;
        } else {
            String[] strArr = cnVar.d;
            if (z) {
                arrayList = Arrays.asList(strArr);
            } else {
                ArrayList arrayList2 = new ArrayList();
                for (String upperCase : strArr) {
                    arrayList2.add(upperCase.toUpperCase(Locale.ENGLISH));
                }
                arrayList = arrayList2;
            }
        }
        return a(str, intValue, z, str3, arrayList, intValue == 1 ? str3 : null);
    }

    private final Boolean a(String str, int i, boolean z, String str2, List<String> list, String str3) {
        if (str == null) {
            return null;
        }
        if (i == 6) {
            if (list == null || list.size() == 0) {
                return null;
            }
        } else if (str2 == null) {
            return null;
        }
        if (!z && i != 1) {
            str = str.toUpperCase(Locale.ENGLISH);
        }
        switch (i) {
            case 1:
                try {
                    return Boolean.valueOf(Pattern.compile(str3, z ? 0 : 66).matcher(str).matches());
                } catch (PatternSyntaxException unused) {
                    q().f.a("Invalid regular expression in REGEXP audience filter. expression", str3);
                    return null;
                }
            case 2:
                return Boolean.valueOf(str.startsWith(str2));
            case 3:
                return Boolean.valueOf(str.endsWith(str2));
            case 4:
                return Boolean.valueOf(str.contains(str2));
            case 5:
                return Boolean.valueOf(str.equals(str2));
            case 6:
                return Boolean.valueOf(list.contains(str));
            default:
                return null;
        }
    }

    private static Boolean a(long j, cl clVar) {
        try {
            return a(new BigDecimal(j), clVar, 0.0d);
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    private static Boolean a(double d, cl clVar) {
        try {
            return a(new BigDecimal(d), clVar, Math.ulp(d));
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    private static Boolean a(String str, cl clVar) {
        if (!ea.a(str)) {
            return null;
        }
        try {
            return a(new BigDecimal(str), clVar, 0.0d);
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0071, code lost:
        if (r3 != null) goto L_0x0073;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.Boolean a(java.math.BigDecimal r10, com.google.android.gms.internal.measurement.cl r11, double r12) {
        /*
            com.google.android.gms.common.internal.l.a(r11)
            java.lang.Integer r0 = r11.f2120a
            r1 = 0
            if (r0 == 0) goto L_0x00f3
            java.lang.Integer r0 = r11.f2120a
            int r0 = r0.intValue()
            if (r0 != 0) goto L_0x0012
            goto L_0x00f3
        L_0x0012:
            java.lang.Integer r0 = r11.f2120a
            int r0 = r0.intValue()
            r2 = 4
            if (r0 != r2) goto L_0x0024
            java.lang.String r0 = r11.d
            if (r0 == 0) goto L_0x0023
            java.lang.String r0 = r11.e
            if (r0 != 0) goto L_0x0029
        L_0x0023:
            return r1
        L_0x0024:
            java.lang.String r0 = r11.c
            if (r0 != 0) goto L_0x0029
            return r1
        L_0x0029:
            java.lang.Integer r0 = r11.f2120a
            int r0 = r0.intValue()
            java.lang.Integer r3 = r11.f2120a
            int r3 = r3.intValue()
            if (r3 != r2) goto L_0x005a
            java.lang.String r3 = r11.d
            boolean r3 = com.google.android.gms.measurement.internal.ea.a(r3)
            if (r3 == 0) goto L_0x0059
            java.lang.String r3 = r11.e
            boolean r3 = com.google.android.gms.measurement.internal.ea.a(r3)
            if (r3 != 0) goto L_0x0048
            goto L_0x0059
        L_0x0048:
            java.math.BigDecimal r3 = new java.math.BigDecimal     // Catch:{ NumberFormatException -> 0x0059 }
            java.lang.String r4 = r11.d     // Catch:{ NumberFormatException -> 0x0059 }
            r3.<init>(r4)     // Catch:{ NumberFormatException -> 0x0059 }
            java.math.BigDecimal r4 = new java.math.BigDecimal     // Catch:{ NumberFormatException -> 0x0059 }
            java.lang.String r11 = r11.e     // Catch:{ NumberFormatException -> 0x0059 }
            r4.<init>(r11)     // Catch:{ NumberFormatException -> 0x0059 }
            r11 = r3
            r3 = r1
            goto L_0x006c
        L_0x0059:
            return r1
        L_0x005a:
            java.lang.String r3 = r11.c
            boolean r3 = com.google.android.gms.measurement.internal.ea.a(r3)
            if (r3 != 0) goto L_0x0063
            return r1
        L_0x0063:
            java.math.BigDecimal r3 = new java.math.BigDecimal     // Catch:{ NumberFormatException -> 0x00f3 }
            java.lang.String r11 = r11.c     // Catch:{ NumberFormatException -> 0x00f3 }
            r3.<init>(r11)     // Catch:{ NumberFormatException -> 0x00f3 }
            r11 = r1
            r4 = r11
        L_0x006c:
            if (r0 != r2) goto L_0x0071
            if (r11 != 0) goto L_0x0073
            return r1
        L_0x0071:
            if (r3 == 0) goto L_0x00f3
        L_0x0073:
            r5 = -1
            r6 = 0
            r7 = 1
            if (r0 == r7) goto L_0x00e7
            r8 = 2
            if (r0 == r8) goto L_0x00db
            r9 = 3
            if (r0 == r9) goto L_0x0093
            if (r0 == r2) goto L_0x0081
            goto L_0x00f3
        L_0x0081:
            int r11 = r10.compareTo(r11)
            if (r11 == r5) goto L_0x008e
            int r10 = r10.compareTo(r4)
            if (r10 == r7) goto L_0x008e
            r6 = 1
        L_0x008e:
            java.lang.Boolean r10 = java.lang.Boolean.valueOf(r6)
            return r10
        L_0x0093:
            r0 = 0
            int r11 = (r12 > r0 ? 1 : (r12 == r0 ? 0 : -1))
            if (r11 == 0) goto L_0x00cf
            java.math.BigDecimal r11 = new java.math.BigDecimal
            r11.<init>(r12)
            java.math.BigDecimal r0 = new java.math.BigDecimal
            r0.<init>(r8)
            java.math.BigDecimal r11 = r11.multiply(r0)
            java.math.BigDecimal r11 = r3.subtract(r11)
            int r11 = r10.compareTo(r11)
            if (r11 != r7) goto L_0x00ca
            java.math.BigDecimal r11 = new java.math.BigDecimal
            r11.<init>(r12)
            java.math.BigDecimal r12 = new java.math.BigDecimal
            r12.<init>(r8)
            java.math.BigDecimal r11 = r11.multiply(r12)
            java.math.BigDecimal r11 = r3.add(r11)
            int r10 = r10.compareTo(r11)
            if (r10 != r5) goto L_0x00ca
            r6 = 1
        L_0x00ca:
            java.lang.Boolean r10 = java.lang.Boolean.valueOf(r6)
            return r10
        L_0x00cf:
            int r10 = r10.compareTo(r3)
            if (r10 != 0) goto L_0x00d6
            r6 = 1
        L_0x00d6:
            java.lang.Boolean r10 = java.lang.Boolean.valueOf(r6)
            return r10
        L_0x00db:
            int r10 = r10.compareTo(r3)
            if (r10 != r7) goto L_0x00e2
            r6 = 1
        L_0x00e2:
            java.lang.Boolean r10 = java.lang.Boolean.valueOf(r6)
            return r10
        L_0x00e7:
            int r10 = r10.compareTo(r3)
            if (r10 != r5) goto L_0x00ee
            r6 = 1
        L_0x00ee:
            java.lang.Boolean r10 = java.lang.Boolean.valueOf(r6)
            return r10
        L_0x00f3:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.ei.a(java.math.BigDecimal, com.google.android.gms.internal.measurement.cl, double):java.lang.Boolean");
    }

    private static cs[] a(Map<Integer, Long> map) {
        if (map == null) {
            return null;
        }
        int i = 0;
        cs[] csVarArr = new cs[map.size()];
        for (Integer next : map.keySet()) {
            cs csVar = new cs();
            csVar.f2134a = next;
            csVar.f2135b = map.get(next);
            csVarArr[i] = csVar;
            i++;
        }
        return csVarArr;
    }

    private static void a(Map<Integer, Long> map, int i, long j) {
        Long l = map.get(Integer.valueOf(i));
        long j2 = j / 1000;
        if (l == null || j2 > l.longValue()) {
            map.put(Integer.valueOf(i), Long.valueOf(j2));
        }
    }

    private static void b(Map<Integer, List<Long>> map, int i, long j) {
        List list = map.get(Integer.valueOf(i));
        if (list == null) {
            list = new ArrayList();
            map.put(Integer.valueOf(i), list);
        }
        list.add(Long.valueOf(j / 1000));
    }
}
