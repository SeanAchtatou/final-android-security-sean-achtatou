package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.measurement.da;
import java.util.Set;

abstract class t {

    /* renamed from: a  reason: collision with root package name */
    final Set<String> f2671a;

    public abstract boolean a();

    public abstract da b();

    /* access modifiers changed from: package-private */
    public final boolean a(Set<String> set) {
        return set.containsAll(this.f2671a);
    }
}
