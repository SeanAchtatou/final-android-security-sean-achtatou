package com.google.android.gms.drive.metadata;

import android.os.Bundle;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import com.google.android.gms.drive.metadata.internal.e;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public abstract class f<T> implements a<T> {

    /* renamed from: a  reason: collision with root package name */
    private final Set<String> f1730a;

    /* renamed from: b  reason: collision with root package name */
    public final String f1731b;
    protected final Set<String> c;
    private final int d;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.l.a(java.lang.Object, java.lang.Object):T
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.l.a(java.lang.String, java.lang.Object):java.lang.String
      com.google.android.gms.common.internal.l.a(boolean, java.lang.Object):void
      com.google.android.gms.common.internal.l.a(java.lang.Object, java.lang.Object):T */
    protected f(String str, int i) {
        this.f1731b = (String) l.a((Object) str, (Object) "fieldName");
        this.c = Collections.singleton(str);
        this.f1730a = Collections.emptySet();
        this.d = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.l.a(java.lang.Object, java.lang.Object):T
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.l.a(java.lang.String, java.lang.Object):java.lang.String
      com.google.android.gms.common.internal.l.a(boolean, java.lang.Object):void
      com.google.android.gms.common.internal.l.a(java.lang.Object, java.lang.Object):T */
    protected f(String str, Collection<String> collection, Collection<String> collection2, int i) {
        this.f1731b = (String) l.a((Object) str, (Object) "fieldName");
        this.c = Collections.unmodifiableSet(new HashSet(collection));
        this.f1730a = Collections.unmodifiableSet(new HashSet(collection2));
        this.d = i;
    }

    public final T a(Bundle bundle) {
        l.a(bundle, "bundle");
        if (bundle.get(this.f1731b) != null) {
            return b(bundle);
        }
        return null;
    }

    public final T a(DataHolder dataHolder, int i, int i2) {
        if (b(dataHolder, i, i2)) {
            return c(dataHolder, i, i2);
        }
        return null;
    }

    public final String a() {
        return this.f1731b;
    }

    /* access modifiers changed from: protected */
    public abstract void a(Bundle bundle, T t);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.drive.metadata.f.a(android.os.Bundle, java.lang.Object):void
     arg types: [android.os.Bundle, T]
     candidates:
      com.google.android.gms.drive.metadata.f.a(java.lang.Object, android.os.Bundle):void
      com.google.android.gms.drive.metadata.a.a(java.lang.Object, android.os.Bundle):void
      com.google.android.gms.drive.metadata.f.a(android.os.Bundle, java.lang.Object):void */
    public final void a(T t, Bundle bundle) {
        l.a(bundle, "bundle");
        if (t == null) {
            bundle.putString(this.f1731b, null);
        } else {
            a(bundle, (Object) t);
        }
    }

    /* access modifiers changed from: protected */
    public abstract T b(Bundle bundle);

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x000c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean b(com.google.android.gms.common.data.DataHolder r4, int r5, int r6) {
        /*
            r3 = this;
            java.util.Set<java.lang.String> r0 = r3.c
            java.util.Iterator r0 = r0.iterator()
        L_0x0006:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0020
            java.lang.Object r1 = r0.next()
            java.lang.String r1 = (java.lang.String) r1
            boolean r2 = r4.a(r1)
            if (r2 == 0) goto L_0x001e
            boolean r1 = r4.e(r1, r5, r6)
            if (r1 == 0) goto L_0x0006
        L_0x001e:
            r4 = 0
            return r4
        L_0x0020:
            r4 = 1
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.drive.metadata.f.b(com.google.android.gms.common.data.DataHolder, int, int):boolean");
    }

    /* access modifiers changed from: protected */
    public abstract T c(DataHolder dataHolder, int i, int i2);

    public String toString() {
        return this.f1731b;
    }

    public final void a(DataHolder dataHolder, MetadataBundle metadataBundle, int i, int i2) {
        l.a(dataHolder, "dataHolder");
        l.a(metadataBundle, "bundle");
        if (b(dataHolder, i, i2)) {
            Object c2 = c(dataHolder, i, i2);
            if (e.a(a()) == null) {
                String valueOf = String.valueOf(a());
                throw new IllegalArgumentException(valueOf.length() != 0 ? "Unregistered field: ".concat(valueOf) : new String("Unregistered field: "));
            } else {
                a(c2, metadataBundle.f1736a);
            }
        }
    }
}
