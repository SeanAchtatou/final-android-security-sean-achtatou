package com.google.android.gms.internal.measurement;

import com.google.android.gms.common.internal.l;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class w {

    /* renamed from: a  reason: collision with root package name */
    final long f2337a = 0;

    /* renamed from: b  reason: collision with root package name */
    final String f2338b;
    final String c;
    final boolean d;
    long e;
    final Map<String, String> f;

    public w(String str, String str2, boolean z, long j, Map<String, String> map) {
        l.a(str);
        l.a(str2);
        this.f2338b = str;
        this.c = str2;
        this.d = z;
        this.e = j;
        if (map != null) {
            this.f = new HashMap(map);
        } else {
            this.f = Collections.emptyMap();
        }
    }
}
