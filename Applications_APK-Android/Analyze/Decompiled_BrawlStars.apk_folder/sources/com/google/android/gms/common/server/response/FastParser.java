package com.google.android.gms.common.server.response;

import com.google.android.gms.common.server.response.FastJsonResponse;
import java.math.BigDecimal;
import java.math.BigInteger;

public class FastParser<T extends FastJsonResponse> {

    /* renamed from: a  reason: collision with root package name */
    private static final char[] f1645a = {'u', 'l', 'l'};

    /* renamed from: b  reason: collision with root package name */
    private static final char[] f1646b = {'r', 'u', 'e'};
    private static final char[] c = {'r', 'u', 'e', '\"'};
    private static final char[] d = {'a', 'l', 's', 'e'};
    private static final char[] e = {'a', 'l', 's', 'e', '\"'};
    private static final char[] f = {10};
    private static final a<Integer> g = new a();
    private static final a<Long> h = new b();
    private static final a<Float> i = new c();
    private static final a<Double> j = new d();
    private static final a<Boolean> k = new e();
    private static final a<String> l = new f();
    private static final a<BigInteger> m = new g();
    private static final a<BigDecimal> n = new h();

    public static class ParseException extends Exception {
    }

    interface a<O> {
    }
}
