package com.google.android.gms.internal.common;

import android.os.Handler;
import android.os.Looper;

public class c extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private static volatile d f1884a;

    public c() {
    }

    public c(Looper looper) {
        super(looper);
    }

    public c(Looper looper, Handler.Callback callback) {
        super(looper, callback);
    }
}
