package com.google.android.gms.internal.measurement;

import java.util.List;

public final class dd {

    /* renamed from: a  reason: collision with root package name */
    public final List<dc> f2155a;

    /* renamed from: b  reason: collision with root package name */
    public final List<dc> f2156b;
    public final List<dc> c;
    public final List<dc> d;
    private final List<dc> e;
    private final List<dc> f;

    public final String toString() {
        String valueOf = String.valueOf(this.f2155a);
        String valueOf2 = String.valueOf(this.f2156b);
        String valueOf3 = String.valueOf(this.c);
        String valueOf4 = String.valueOf(this.d);
        String valueOf5 = String.valueOf(this.e);
        String valueOf6 = String.valueOf(this.f);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 102 + String.valueOf(valueOf2).length() + String.valueOf(valueOf3).length() + String.valueOf(valueOf4).length() + String.valueOf(valueOf5).length() + String.valueOf(valueOf6).length());
        sb.append("Positive predicates: ");
        sb.append(valueOf);
        sb.append("  Negative predicates: ");
        sb.append(valueOf2);
        sb.append("  Add tags: ");
        sb.append(valueOf3);
        sb.append("  Remove tags: ");
        sb.append(valueOf4);
        sb.append("  Add macros: ");
        sb.append(valueOf5);
        sb.append("  Remove macros: ");
        sb.append(valueOf6);
        return sb.toString();
    }
}
