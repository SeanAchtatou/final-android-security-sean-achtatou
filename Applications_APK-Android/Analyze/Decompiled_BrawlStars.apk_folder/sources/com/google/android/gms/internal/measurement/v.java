package com.google.android.gms.internal.measurement;

import android.content.Context;
import com.google.android.gms.common.internal.l;

public final class v {

    /* renamed from: a  reason: collision with root package name */
    final Context f2335a;

    /* renamed from: b  reason: collision with root package name */
    final Context f2336b;

    public v(Context context) {
        l.a(context);
        Context applicationContext = context.getApplicationContext();
        l.a(applicationContext, "Application context can't be null");
        this.f2335a = applicationContext;
        this.f2336b = applicationContext;
    }
}
