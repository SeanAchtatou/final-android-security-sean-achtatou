package com.google.android.gms.tagmanager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class c {

    /* renamed from: a  reason: collision with root package name */
    public static final Object f2650a = new Object();

    /* renamed from: b  reason: collision with root package name */
    private static final String[] f2651b = "gtm.lifetime".split("\\.");
    private static final Pattern c = Pattern.compile("(\\d+)\\s*([smhd]?)");
    private final ConcurrentHashMap<b, Integer> d;
    private final Map<String, Object> e;
    private final ReentrantLock f;
    private final LinkedList<Map<String, Object>> g;
    private final C0124c h;
    /* access modifiers changed from: private */
    public final CountDownLatch i;

    interface b {
        void a(Map<String, Object> map);
    }

    /* renamed from: com.google.android.gms.tagmanager.c$c  reason: collision with other inner class name */
    interface C0124c {
        void a(i iVar);

        void a(List<a> list, long j);
    }

    c() {
        this(new g());
    }

    c(C0124c cVar) {
        this.h = cVar;
        this.d = new ConcurrentHashMap<>();
        this.e = new HashMap();
        this.f = new ReentrantLock();
        this.g = new LinkedList<>();
        this.i = new CountDownLatch(1);
        this.h.a(new h(this));
    }

    static final class a {

        /* renamed from: a  reason: collision with root package name */
        public final String f2652a;

        /* renamed from: b  reason: collision with root package name */
        public final Object f2653b;

        a(String str, Object obj) {
            this.f2652a = str;
            this.f2653b = obj;
        }

        public final String toString() {
            String str = this.f2652a;
            String obj = this.f2653b.toString();
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 13 + String.valueOf(obj).length());
            sb.append("Key: ");
            sb.append(str);
            sb.append(" value: ");
            sb.append(obj);
            return sb.toString();
        }

        public final int hashCode() {
            return Arrays.hashCode(new Integer[]{Integer.valueOf(this.f2652a.hashCode()), Integer.valueOf(this.f2653b.hashCode())});
        }

        public final boolean equals(Object obj) {
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            if (!this.f2652a.equals(aVar.f2652a) || !this.f2653b.equals(aVar.f2653b)) {
                return false;
            }
            return true;
        }
    }

    public String toString() {
        String sb;
        synchronized (this.e) {
            StringBuilder sb2 = new StringBuilder();
            for (Map.Entry next : this.e.entrySet()) {
                sb2.append(String.format("{\n\tKey: %s\n\tValue: %s\n}\n", next.getKey(), next.getValue()));
            }
            sb = sb2.toString();
        }
        return sb;
    }

    public final void a(Map<String, Object> map) {
        try {
            this.i.await();
        } catch (InterruptedException unused) {
            ab.b("DataLayer.push: unexpected InterruptedException");
        }
        b(map);
    }

    /* access modifiers changed from: private */
    public final void b(Map<String, Object> map) {
        Long l;
        this.f.lock();
        try {
            this.g.offer(map);
            int i2 = 0;
            if (this.f.getHoldCount() == 1) {
                int i3 = 0;
                while (true) {
                    Map poll = this.g.poll();
                    if (poll == null) {
                        break;
                    }
                    synchronized (this.e) {
                        for (String str : poll.keySet()) {
                            a(a(str, poll.get(str)), this.e);
                        }
                    }
                    for (b a2 : this.d.keySet()) {
                        a2.a(poll);
                    }
                    i3++;
                    if (i3 > 500) {
                        this.g.clear();
                        throw new RuntimeException("Seems like an infinite loop of pushing to the data layer");
                    }
                }
            }
            String[] strArr = f2651b;
            int length = strArr.length;
            Object obj = map;
            while (true) {
                l = null;
                if (i2 >= length) {
                    break;
                }
                String str2 = strArr[i2];
                if (!(obj instanceof Map)) {
                    obj = null;
                    break;
                } else {
                    obj = ((Map) obj).get(str2);
                    i2++;
                }
            }
            if (obj != null) {
                l = a(obj.toString());
            }
            if (l != null) {
                ArrayList arrayList = new ArrayList();
                a(map, "", arrayList);
                this.h.a(arrayList, l.longValue());
            }
            this.f.unlock();
        } catch (Throwable th) {
            this.f.unlock();
            throw th;
        }
    }

    private final void a(Map<String, Object> map, String str, Collection<a> collection) {
        for (Map.Entry next : map.entrySet()) {
            String str2 = str.length() == 0 ? "" : ".";
            String str3 = (String) next.getKey();
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + str2.length() + String.valueOf(str3).length());
            sb.append(str);
            sb.append(str2);
            sb.append(str3);
            String sb2 = sb.toString();
            if (next.getValue() instanceof Map) {
                a((Map) next.getValue(), sb2, collection);
            } else if (!sb2.equals("gtm.lifetime")) {
                collection.add(new a(sb2, next.getValue()));
            }
        }
    }

    private static Long a(String str) {
        long j;
        Matcher matcher = c.matcher(str);
        if (!matcher.matches()) {
            String valueOf = String.valueOf(str);
            ab.c(valueOf.length() != 0 ? "unknown _lifetime: ".concat(valueOf) : new String("unknown _lifetime: "));
            return null;
        }
        try {
            j = Long.parseLong(matcher.group(1));
        } catch (NumberFormatException unused) {
            String valueOf2 = String.valueOf(str);
            ab.b(valueOf2.length() != 0 ? "illegal number in _lifetime value: ".concat(valueOf2) : new String("illegal number in _lifetime value: "));
            j = 0;
        }
        if (j <= 0) {
            String valueOf3 = String.valueOf(str);
            ab.c(valueOf3.length() != 0 ? "non-positive _lifetime: ".concat(valueOf3) : new String("non-positive _lifetime: "));
            return null;
        }
        String group = matcher.group(2);
        if (group.length() == 0) {
            return Long.valueOf(j);
        }
        char charAt = group.charAt(0);
        if (charAt == 'd') {
            return Long.valueOf(j * 1000 * 60 * 60 * 24);
        }
        if (charAt == 'h') {
            return Long.valueOf(j * 1000 * 60 * 60);
        }
        if (charAt == 'm') {
            return Long.valueOf(j * 1000 * 60);
        }
        if (charAt == 's') {
            return Long.valueOf(j * 1000);
        }
        String valueOf4 = String.valueOf(str);
        ab.b(valueOf4.length() != 0 ? "unknown units in _lifetime: ".concat(valueOf4) : new String("unknown units in _lifetime: "));
        return null;
    }

    /* access modifiers changed from: package-private */
    public final void a(b bVar) {
        this.d.put(bVar, 0);
    }

    static Map<String, Object> a(String str, Object obj) {
        HashMap hashMap = new HashMap();
        String[] split = str.toString().split("\\.");
        int i2 = 0;
        HashMap hashMap2 = hashMap;
        while (i2 < split.length - 1) {
            HashMap hashMap3 = new HashMap();
            hashMap2.put(split[i2], hashMap3);
            i2++;
            hashMap2 = hashMap3;
        }
        hashMap2.put(split[split.length - 1], obj);
        return hashMap;
    }

    private final void a(Map<String, Object> map, Map<String, Object> map2) {
        for (String next : map.keySet()) {
            Object obj = map.get(next);
            if (obj instanceof List) {
                if (!(map2.get(next) instanceof List)) {
                    map2.put(next, new ArrayList());
                }
                a((List) obj, (List) map2.get(next));
            } else if (obj instanceof Map) {
                if (!(map2.get(next) instanceof Map)) {
                    map2.put(next, new HashMap());
                }
                a((Map) obj, (Map) map2.get(next));
            } else {
                map2.put(next, obj);
            }
        }
    }

    private final void a(List<Object> list, List<Object> list2) {
        while (list2.size() < list.size()) {
            list2.add(null);
        }
        for (int i2 = 0; i2 < list.size(); i2++) {
            Object obj = list.get(i2);
            if (obj instanceof List) {
                if (!(list2.get(i2) instanceof List)) {
                    list2.set(i2, new ArrayList());
                }
                a((List) obj, (List) list2.get(i2));
            } else if (obj instanceof Map) {
                if (!(list2.get(i2) instanceof Map)) {
                    list2.set(i2, new HashMap());
                }
                a((Map) obj, (Map) list2.get(i2));
            } else if (obj != f2650a) {
                list2.set(i2, obj);
            }
        }
    }
}
