package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.l;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

final class ap<V> extends FutureTask<V> implements Comparable<ap> {

    /* renamed from: a  reason: collision with root package name */
    final boolean f2383a;

    /* renamed from: b  reason: collision with root package name */
    private final long f2384b = am.j.getAndIncrement();
    private final String c;
    private final /* synthetic */ am d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ap(am amVar, Callable<V> callable, boolean z, String str) {
        super(callable);
        this.d = amVar;
        l.a((Object) str);
        this.c = str;
        this.f2383a = z;
        if (this.f2384b == Long.MAX_VALUE) {
            amVar.q().c.a("Tasks index overflow");
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ap(am amVar, Runnable runnable, String str) {
        super(runnable, null);
        this.d = amVar;
        l.a((Object) str);
        this.c = str;
        this.f2383a = false;
        if (this.f2384b == Long.MAX_VALUE) {
            amVar.q().c.a("Tasks index overflow");
        }
    }

    /* access modifiers changed from: protected */
    public final void setException(Throwable th) {
        this.d.q().c.a(this.c, th);
        if (th instanceof an) {
            Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), th);
        }
        super.setException(th);
    }

    public final /* synthetic */ int compareTo(Object obj) {
        ap apVar = (ap) obj;
        boolean z = this.f2383a;
        if (z != apVar.f2383a) {
            return z ? -1 : 1;
        }
        long j = this.f2384b;
        long j2 = apVar.f2384b;
        if (j < j2) {
            return -1;
        }
        if (j > j2) {
            return 1;
        }
        this.d.q().d.a("Two tasks share the same index. index", Long.valueOf(this.f2384b));
        return 0;
    }
}
