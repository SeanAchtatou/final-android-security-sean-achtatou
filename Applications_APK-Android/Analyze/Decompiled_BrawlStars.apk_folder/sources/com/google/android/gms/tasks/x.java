package com.google.android.gms.tasks;

import java.util.concurrent.CancellationException;

final class x implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ g f2708a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ w f2709b;

    x(w wVar, g gVar) {
        this.f2709b = wVar;
        this.f2708a = gVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.e):com.google.android.gms.tasks.g<TResult>
     arg types: [java.util.concurrent.Executor, com.google.android.gms.tasks.w]
     candidates:
      com.google.android.gms.tasks.g.a(android.app.Activity, com.google.android.gms.tasks.e):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.a):com.google.android.gms.tasks.g<TContinuationResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.b):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.c):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.d):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.f):com.google.android.gms.tasks.g<TContinuationResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.e):com.google.android.gms.tasks.g<TResult> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.d):com.google.android.gms.tasks.g<TResult>
     arg types: [java.util.concurrent.Executor, com.google.android.gms.tasks.w]
     candidates:
      com.google.android.gms.tasks.g.a(android.app.Activity, com.google.android.gms.tasks.e):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.a):com.google.android.gms.tasks.g<TContinuationResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.b):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.c):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.e):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.f):com.google.android.gms.tasks.g<TContinuationResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.d):com.google.android.gms.tasks.g<TResult> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.b):com.google.android.gms.tasks.g<TResult>
     arg types: [java.util.concurrent.Executor, com.google.android.gms.tasks.w]
     candidates:
      com.google.android.gms.tasks.g.a(android.app.Activity, com.google.android.gms.tasks.e):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.a):com.google.android.gms.tasks.g<TContinuationResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.c):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.d):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.e):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.f):com.google.android.gms.tasks.g<TContinuationResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.b):com.google.android.gms.tasks.g<TResult> */
    public final void run() {
        try {
            g<TContinuationResult> a2 = this.f2709b.f2706a.a(this.f2708a.d());
            if (a2 == null) {
                this.f2709b.a((Exception) new NullPointerException("Continuation returned null"));
                return;
            }
            a2.a(i.f2680b, (e) this.f2709b);
            a2.a(i.f2680b, (d) this.f2709b);
            a2.a(i.f2680b, (b) this.f2709b);
        } catch (RuntimeExecutionException e) {
            if (e.getCause() instanceof Exception) {
                this.f2709b.a((Exception) e.getCause());
            } else {
                this.f2709b.a((Exception) e);
            }
        } catch (CancellationException unused) {
            this.f2709b.g_();
        } catch (Exception e2) {
            this.f2709b.a(e2);
        }
    }
}
