package com.google.android.gms.measurement;

import android.app.Service;
import android.app.job.JobParameters;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import com.google.android.gms.measurement.internal.ar;
import com.google.android.gms.measurement.internal.dd;
import com.google.android.gms.measurement.internal.de;
import com.google.android.gms.measurement.internal.di;
import com.google.android.gms.measurement.internal.du;
import com.google.android.gms.measurement.internal.j;
import com.google.android.gms.measurement.internal.o;
import com.google.android.gms.measurement.internal.zzby;

public final class AppMeasurementService extends Service implements di {

    /* renamed from: a  reason: collision with root package name */
    private dd<AppMeasurementService> f2357a;

    private final dd<AppMeasurementService> a() {
        if (this.f2357a == null) {
            this.f2357a = new dd<>(this);
        }
        return this.f2357a;
    }

    public final void onCreate() {
        super.onCreate();
        a().a();
    }

    public final void onDestroy() {
        a().b();
        super.onDestroy();
    }

    public final int onStartCommand(Intent intent, int i, int i2) {
        dd<AppMeasurementService> a2 = a();
        o q = ar.a(a2.f2503a, (j) null).q();
        if (intent == null) {
            q.f.a("AppMeasurementService started with null intent");
            return 2;
        }
        String action = intent.getAction();
        q.k.a("Local AppMeasurementService called. startId, action", Integer.valueOf(i2), action);
        if (!"com.google.android.gms.measurement.UPLOAD".equals(action)) {
            return 2;
        }
        a2.a(new de(a2, i2, q, intent));
        return 2;
    }

    public final IBinder onBind(Intent intent) {
        dd<AppMeasurementService> a2 = a();
        if (intent == null) {
            a2.c().c.a("onBind called with null intent");
            return null;
        }
        String action = intent.getAction();
        if ("com.google.android.gms.measurement.START".equals(action)) {
            return new zzby(du.a((Context) a2.f2503a));
        }
        a2.c().f.a("onBind received unknown action", action);
        return null;
    }

    public final boolean onUnbind(Intent intent) {
        return a().a(intent);
    }

    public final void onRebind(Intent intent) {
        a().b(intent);
    }

    public final boolean a(int i) {
        return stopSelfResult(i);
    }

    public final void a(JobParameters jobParameters) {
        throw new UnsupportedOperationException();
    }

    public final void a(Intent intent) {
        AppMeasurementReceiver.completeWakefulIntent(intent);
    }
}
