package com.google.android.gms.internal.drive;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Pair;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.h;
import com.google.android.gms.drive.c;
import com.google.android.gms.drive.events.ChangeEvent;
import com.google.android.gms.drive.events.CompletionEvent;
import com.google.android.gms.drive.events.DriveEvent;
import com.google.android.gms.drive.events.a;
import com.google.android.gms.drive.events.b;
import com.google.android.gms.drive.events.e;
import com.google.android.gms.drive.events.i;
import com.google.android.gms.drive.events.zzb;
import com.google.android.gms.drive.events.zzo;
import com.google.android.gms.drive.events.zzr;

final class l extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private final Context f1917a;

    public final void handleMessage(Message message) {
        if (message.what != 1) {
            h a2 = zzee.f1926a;
            Object[] objArr = {this.f1917a};
            if (a2.a(6)) {
                a2.b("Don't know how to handle this event in context %s", objArr);
                return;
            }
            return;
        }
        Pair pair = (Pair) message.obj;
        i iVar = (i) pair.first;
        DriveEvent driveEvent = (DriveEvent) pair.second;
        int a3 = driveEvent.a();
        if (a3 == 1) {
            ((a) iVar).a((ChangeEvent) driveEvent);
        } else if (a3 == 2) {
            ((b) iVar).a((CompletionEvent) driveEvent);
        } else if (a3 == 3) {
            DataHolder dataHolder = ((zzo) driveEvent).f1722a;
            if (dataHolder != null) {
                new m(new c(dataHolder));
            }
        } else if (a3 == 4) {
            ((e) iVar).a((zzb) driveEvent);
        } else if (a3 != 8) {
            zzee.f1926a.a("Unexpected event: %s", driveEvent);
        } else {
            new j(((zzr) driveEvent).f1724a);
        }
    }
}
