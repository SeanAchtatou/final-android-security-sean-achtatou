package com.google.android.gms.common.c;

import android.content.Context;
import com.google.android.gms.common.util.o;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static Context f1514a;

    /* renamed from: b  reason: collision with root package name */
    private static Boolean f1515b;

    public static synchronized boolean a(Context context) {
        synchronized (a.class) {
            Context applicationContext = context.getApplicationContext();
            if (f1514a == null || f1515b == null || f1514a != applicationContext) {
                f1515b = null;
                if (o.g()) {
                    f1515b = Boolean.valueOf(applicationContext.getPackageManager().isInstantApp());
                } else {
                    try {
                        context.getClassLoader().loadClass("com.google.android.instantapps.supervisor.InstantAppsRuntime");
                        f1515b = true;
                    } catch (ClassNotFoundException unused) {
                        f1515b = false;
                    }
                }
                f1514a = applicationContext;
                boolean booleanValue = f1515b.booleanValue();
                return booleanValue;
            }
            boolean booleanValue2 = f1515b.booleanValue();
            return booleanValue2;
        }
    }
}
