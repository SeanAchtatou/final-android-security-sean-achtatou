package com.google.android.gms.games.video;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.a;

public final class VideoConfiguration extends AbstractSafeParcelable {
    public static final Parcelable.Creator<VideoConfiguration> CREATOR = new c();

    /* renamed from: a  reason: collision with root package name */
    private final int f1854a;

    /* renamed from: b  reason: collision with root package name */
    private final int f1855b;
    private final boolean c;

    public VideoConfiguration(int i, int i2, boolean z) {
        boolean z2 = false;
        l.b(i != -1 && (i == 0 || i == 1 || i == 2 || i == 3));
        if (i2 != -1 && i2 == 0) {
            z2 = true;
        }
        l.b(z2);
        this.f1854a = i;
        this.f1855b = i2;
        this.c = z;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.b(parcel, 1, this.f1854a);
        a.b(parcel, 2, this.f1855b);
        a.a(parcel, 7, this.c);
        a.b(parcel, a2);
    }
}
