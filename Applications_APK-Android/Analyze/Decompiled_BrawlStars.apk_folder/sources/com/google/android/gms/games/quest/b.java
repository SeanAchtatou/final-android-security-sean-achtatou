package com.google.android.gms.games.quest;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class b implements Parcelable.Creator<MilestoneEntity> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new MilestoneEntity[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        long j = 0;
        long j2 = 0;
        String str = null;
        byte[] bArr = null;
        String str2 = null;
        int i = 0;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    str = SafeParcelReader.l(parcel, readInt);
                    break;
                case 2:
                    j = SafeParcelReader.f(parcel, readInt);
                    break;
                case 3:
                    j2 = SafeParcelReader.f(parcel, readInt);
                    break;
                case 4:
                    bArr = SafeParcelReader.o(parcel, readInt);
                    break;
                case 5:
                    i = SafeParcelReader.d(parcel, readInt);
                    break;
                case 6:
                    str2 = SafeParcelReader.l(parcel, readInt);
                    break;
                default:
                    SafeParcelReader.b(parcel, readInt);
                    break;
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new MilestoneEntity(str, j, j2, bArr, i, str2);
    }
}
