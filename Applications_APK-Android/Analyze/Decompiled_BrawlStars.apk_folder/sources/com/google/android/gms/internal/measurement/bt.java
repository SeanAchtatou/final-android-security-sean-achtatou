package com.google.android.gms.internal.measurement;

import android.app.job.JobParameters;

public final /* synthetic */ class bt implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final br f2095a;

    /* renamed from: b  reason: collision with root package name */
    private final bk f2096b;
    private final JobParameters c;

    public bt(br brVar, bk bkVar, JobParameters jobParameters) {
        this.f2095a = brVar;
        this.f2096b = bkVar;
        this.c = jobParameters;
    }

    public final void run() {
        br brVar = this.f2095a;
        bk bkVar = this.f2096b;
        JobParameters jobParameters = this.c;
        bkVar.b("AnalyticsJobService processed last dispatch request");
        ((bv) brVar.f2092b).a(jobParameters);
    }
}
