package com.google.android.gms.internal.measurement;

import java.util.Collections;
import java.util.Map;

public final class dc {

    /* renamed from: a  reason: collision with root package name */
    public final Map<String, da> f2153a;

    /* renamed from: b  reason: collision with root package name */
    public final da f2154b;

    public final void a(String str, da daVar) {
        this.f2153a.put(str, daVar);
    }

    public final String toString() {
        String valueOf = String.valueOf(Collections.unmodifiableMap(this.f2153a));
        String valueOf2 = String.valueOf(this.f2154b);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 32 + String.valueOf(valueOf2).length());
        sb.append("Properties: ");
        sb.append(valueOf);
        sb.append(" pushAfterEvaluate: ");
        sb.append(valueOf2);
        return sb.toString();
    }
}
