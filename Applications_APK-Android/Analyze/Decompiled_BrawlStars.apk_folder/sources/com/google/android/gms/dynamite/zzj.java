package com.google.android.gms.dynamite;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.common.a;
import com.google.android.gms.internal.common.zza;

public final class zzj extends zza implements zzi {
    zzj(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.dynamite.IDynamiteLoader");
    }

    public final IObjectWrapper a(IObjectWrapper iObjectWrapper, String str, int i) throws RemoteException {
        Parcel s = s();
        a.a(s, iObjectWrapper);
        s.writeString(str);
        s.writeInt(i);
        Parcel a2 = a(2, s);
        IObjectWrapper a3 = IObjectWrapper.Stub.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final int a(IObjectWrapper iObjectWrapper, String str, boolean z) throws RemoteException {
        Parcel s = s();
        a.a(s, iObjectWrapper);
        s.writeString(str);
        a.a(s, z);
        Parcel a2 = a(3, s);
        int readInt = a2.readInt();
        a2.recycle();
        return readInt;
    }

    public final IObjectWrapper b(IObjectWrapper iObjectWrapper, String str, int i) throws RemoteException {
        Parcel s = s();
        a.a(s, iObjectWrapper);
        s.writeString(str);
        s.writeInt(i);
        Parcel a2 = a(4, s);
        IObjectWrapper a3 = IObjectWrapper.Stub.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final int b(IObjectWrapper iObjectWrapper, String str, boolean z) throws RemoteException {
        Parcel s = s();
        a.a(s, iObjectWrapper);
        s.writeString(str);
        a.a(s, z);
        Parcel a2 = a(5, s);
        int readInt = a2.readInt();
        a2.recycle();
        return readInt;
    }

    public final int a() throws RemoteException {
        Parcel a2 = a(6, s());
        int readInt = a2.readInt();
        a2.recycle();
        return readInt;
    }
}
