package com.google.android.gms.measurement;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import com.facebook.internal.NativeProtocol;
import com.google.android.gms.measurement.internal.ar;
import com.google.android.gms.measurement.internal.dd;
import com.google.android.gms.measurement.internal.dg;
import com.google.android.gms.measurement.internal.di;
import com.google.android.gms.measurement.internal.j;
import com.google.android.gms.measurement.internal.o;

public final class AppMeasurementJobService extends JobService implements di {

    /* renamed from: a  reason: collision with root package name */
    private dd<AppMeasurementJobService> f2355a;

    private final dd<AppMeasurementJobService> a() {
        if (this.f2355a == null) {
            this.f2355a = new dd<>(this);
        }
        return this.f2355a;
    }

    public final void a(Intent intent) {
    }

    public final boolean onStopJob(JobParameters jobParameters) {
        return false;
    }

    public final void onCreate() {
        super.onCreate();
        a().a();
    }

    public final void onDestroy() {
        a().b();
        super.onDestroy();
    }

    public final boolean onStartJob(JobParameters jobParameters) {
        dd<AppMeasurementJobService> a2 = a();
        o q = ar.a(a2.f2503a, (j) null).q();
        String string = jobParameters.getExtras().getString(NativeProtocol.WEB_DIALOG_ACTION);
        q.k.a("Local AppMeasurementJobService called. action", string);
        if (!"com.google.android.gms.measurement.UPLOAD".equals(string)) {
            return true;
        }
        a2.a(new dg(a2, q, jobParameters));
        return true;
    }

    public final boolean onUnbind(Intent intent) {
        return a().a(intent);
    }

    public final void onRebind(Intent intent) {
        a().b(intent);
    }

    public final boolean a(int i) {
        throw new UnsupportedOperationException();
    }

    public final void a(JobParameters jobParameters) {
        jobFinished(jobParameters, false);
    }
}
