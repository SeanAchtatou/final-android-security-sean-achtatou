package com.google.android.gms.measurement.internal;

final class cw implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ boolean f2489a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ zzfv f2490b;
    private final /* synthetic */ zzk c;
    private final /* synthetic */ cl d;

    cw(cl clVar, boolean z, zzfv zzfv, zzk zzk) {
        this.d = clVar;
        this.f2489a = z;
        this.f2490b = zzfv;
        this.c = zzk;
    }

    public final void run() {
        zzaj zzaj = this.d.f2470b;
        if (zzaj == null) {
            this.d.q().c.a("Discarding data. Failed to set user attribute");
            return;
        }
        this.d.a(zzaj, this.f2489a ? null : this.f2490b, this.c);
        this.d.y();
    }
}
