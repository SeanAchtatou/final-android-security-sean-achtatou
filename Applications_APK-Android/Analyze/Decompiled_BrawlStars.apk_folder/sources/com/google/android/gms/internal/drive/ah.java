package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.drive.DriveId;

public final class ah implements Parcelable.Creator<zzgd> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zzgd[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        int i = 0;
        DriveId driveId = null;
        int i2 = 0;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            int i3 = 65535 & readInt;
            if (i3 == 2) {
                driveId = (DriveId) SafeParcelReader.a(parcel, readInt, DriveId.CREATOR);
            } else if (i3 == 3) {
                i = SafeParcelReader.d(parcel, readInt);
            } else if (i3 != 4) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                i2 = SafeParcelReader.d(parcel, readInt);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new zzgd(driveId, i, i2);
    }
}
