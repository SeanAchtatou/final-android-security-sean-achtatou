package com.google.android.gms.games.quest;

import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.d;

public final class zzb extends d implements Milestone {
    zzb(DataHolder dataHolder, int i) {
        super(dataHolder, i);
    }

    public final /* synthetic */ Object a() {
        return new MilestoneEntity(this);
    }

    public final String b() {
        return e("external_milestone_id");
    }

    public final String d() {
        return e("external_event_id");
    }

    public final int describeContents() {
        return 0;
    }

    public final int e() {
        return c("milestone_state");
    }

    public final boolean equals(Object obj) {
        return MilestoneEntity.a(this, obj);
    }

    public final long f() {
        return b("target_value");
    }

    public final byte[] g() {
        return g("completion_reward_data");
    }

    public final int hashCode() {
        return MilestoneEntity.a(this);
    }

    public final String toString() {
        return MilestoneEntity.b(this);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        ((MilestoneEntity) ((Milestone) a())).writeToParcel(parcel, i);
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:20:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final long c() {
        /*
            r10 = this;
            java.lang.String r0 = "milestone_state"
            int r0 = r10.c(r0)
            r1 = 1
            java.lang.String r2 = "target_value"
            r3 = 0
            if (r0 == r1) goto L_0x0016
            r1 = 2
            if (r0 == r1) goto L_0x001d
            r1 = 3
            if (r0 == r1) goto L_0x0018
            r1 = 4
            if (r0 == r1) goto L_0x0018
        L_0x0016:
            r0 = r3
            goto L_0x0036
        L_0x0018:
            long r0 = r10.b(r2)
            goto L_0x0036
        L_0x001d:
            java.lang.String r0 = "current_value"
            long r0 = r10.b(r0)
            java.lang.String r5 = "quest_state"
            long r5 = r10.b(r5)
            r7 = 6
            int r9 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r9 == 0) goto L_0x0036
            java.lang.String r5 = "initial_value"
            long r5 = r10.b(r5)
            long r0 = r0 - r5
        L_0x0036:
            java.lang.String r5 = "MilestoneRef"
            int r6 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r6 >= 0) goto L_0x0042
            java.lang.String r0 = "Current progress should never be negative"
            com.google.android.gms.games.internal.h.b(r5, r0)
            r0 = r3
        L_0x0042:
            long r3 = r10.b(r2)
            int r6 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r6 <= 0) goto L_0x0053
            java.lang.String r0 = "Current progress should never exceed target progress"
            com.google.android.gms.games.internal.h.b(r5, r0)
            long r0 = r10.b(r2)
        L_0x0053:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.games.quest.zzb.c():long");
    }
}
