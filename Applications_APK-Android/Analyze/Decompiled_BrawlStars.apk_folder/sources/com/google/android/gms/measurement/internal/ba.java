package com.google.android.gms.measurement.internal;

import java.util.List;
import java.util.concurrent.Callable;

final class ba implements Callable<List<ec>> {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ String f2407a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f2408b;
    private final /* synthetic */ String c;
    private final /* synthetic */ zzby d;

    ba(zzby zzby, String str, String str2, String str3) {
        this.d = zzby;
        this.f2407a = str;
        this.f2408b = str2;
        this.c = str3;
    }

    public final /* synthetic */ Object call() throws Exception {
        this.d.f2597a.k();
        return this.d.f2597a.d().a(this.f2407a, this.f2408b, this.c);
    }
}
