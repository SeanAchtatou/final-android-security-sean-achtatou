package com.google.android.gms.internal.measurement;

final class er {

    /* renamed from: a  reason: collision with root package name */
    final zztv f2188a;

    /* renamed from: b  reason: collision with root package name */
    private final byte[] f2189b;

    private er(int i) {
        this.f2189b = new byte[i];
        this.f2188a = zztv.a(this.f2189b);
    }

    public final ej a() {
        if (this.f2188a.i() == 0) {
            return new et(this.f2189b);
        }
        throw new IllegalStateException("Did not write as much data as expected.");
    }

    /* synthetic */ er(int i, byte b2) {
        this(i);
    }
}
