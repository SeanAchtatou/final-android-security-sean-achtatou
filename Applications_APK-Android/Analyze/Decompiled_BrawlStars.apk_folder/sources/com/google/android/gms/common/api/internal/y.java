package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;

final class y extends an {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ConnectionResult f1503a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ x f1504b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    y(x xVar, al alVar, ConnectionResult connectionResult) {
        super(alVar);
        this.f1504b = xVar;
        this.f1503a = connectionResult;
    }

    public final void a() {
        this.f1504b.f1501a.b(this.f1503a);
    }
}
