package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcelable;
import com.google.android.gms.common.data.e;

public interface Game extends Parcelable, e<Game> {
    String b();

    String c();

    String d();

    String e();

    String f();

    String g();

    @Deprecated
    String getFeaturedImageUrl();

    @Deprecated
    String getHiResImageUrl();

    @Deprecated
    String getIconImageUrl();

    Uri h();

    Uri i();

    Uri j();

    boolean k();

    boolean l();

    boolean m();

    boolean n();

    String o();

    int p();

    int q();

    boolean r();

    boolean s();

    boolean t();

    String u();

    boolean v();
}
