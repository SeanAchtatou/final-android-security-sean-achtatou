package com.google.android.gms.internal.measurement;

import java.lang.reflect.Field;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.AccessController;
import java.util.logging.Level;
import java.util.logging.Logger;
import libcore.io.Memory;
import sun.misc.Unsafe;

final class ih {

    /* renamed from: a  reason: collision with root package name */
    private static final Logger f2289a = Logger.getLogger(ih.class.getName());

    /* renamed from: b  reason: collision with root package name */
    private static final Unsafe f2290b = c();
    private static final Class<?> c = eg.b();
    private static final boolean d = d(Long.TYPE);
    private static final boolean e = d(Integer.TYPE);
    private static final d f;
    private static final boolean g = g();
    private static final boolean h = f();
    /* access modifiers changed from: private */
    public static final long i = ((long) b(byte[].class));
    private static final long j;
    private static final long k;
    private static final long l;
    private static final long m;
    private static final long n;
    private static final long o;
    private static final long p;
    private static final long q;
    private static final long r;
    private static final long s;
    private static final long t = ((long) b(Object[].class));
    private static final long u = ((long) c(Object[].class));
    private static final long v;
    /* access modifiers changed from: private */
    public static final boolean w = (ByteOrder.nativeOrder() == ByteOrder.BIG_ENDIAN);

    private ih() {
    }

    static final class b extends d {
        b(Unsafe unsafe) {
            super(unsafe);
        }

        public final void a(long j, byte b2) {
            Memory.pokeByte(j, b2);
        }

        public final byte a(Object obj, long j) {
            if (ih.w) {
                return ih.k(obj, j);
            }
            return ih.l(obj, j);
        }

        public final void a(Object obj, long j, byte b2) {
            if (ih.w) {
                ih.c(obj, j, b2);
            } else {
                ih.d(obj, j, b2);
            }
        }

        public final boolean b(Object obj, long j) {
            if (ih.w) {
                return ih.i(obj, j);
            }
            return ih.j(obj, j);
        }

        public final void a(Object obj, long j, boolean z) {
            if (ih.w) {
                ih.c(obj, j, r3 ? (byte) 1 : 0);
            } else {
                ih.d(obj, j, r3 ? (byte) 1 : 0);
            }
        }

        public final float c(Object obj, long j) {
            return Float.intBitsToFloat(e(obj, j));
        }

        public final void a(Object obj, long j, float f) {
            a(obj, j, Float.floatToIntBits(f));
        }

        public final double d(Object obj, long j) {
            return Double.longBitsToDouble(f(obj, j));
        }

        public final void a(Object obj, long j, double d) {
            a(obj, j, Double.doubleToLongBits(d));
        }

        public final void a(byte[] bArr, long j, long j2, long j3) {
            Memory.pokeByteArray(j2, bArr, (int) j, (int) j3);
        }
    }

    static final class c extends d {
        c(Unsafe unsafe) {
            super(unsafe);
        }

        public final void a(long j, byte b2) {
            this.f2291a.putByte(j, b2);
        }

        public final byte a(Object obj, long j) {
            return this.f2291a.getByte(obj, j);
        }

        public final void a(Object obj, long j, byte b2) {
            this.f2291a.putByte(obj, j, b2);
        }

        public final boolean b(Object obj, long j) {
            return this.f2291a.getBoolean(obj, j);
        }

        public final void a(Object obj, long j, boolean z) {
            this.f2291a.putBoolean(obj, j, z);
        }

        public final float c(Object obj, long j) {
            return this.f2291a.getFloat(obj, j);
        }

        public final void a(Object obj, long j, float f) {
            this.f2291a.putFloat(obj, j, f);
        }

        public final double d(Object obj, long j) {
            return this.f2291a.getDouble(obj, j);
        }

        public final void a(Object obj, long j, double d) {
            this.f2291a.putDouble(obj, j, d);
        }

        public final void a(byte[] bArr, long j, long j2, long j3) {
            this.f2291a.copyMemory(bArr, ih.i + j, (Object) null, j2, j3);
        }
    }

    static boolean a() {
        return h;
    }

    static abstract class d {

        /* renamed from: a  reason: collision with root package name */
        Unsafe f2291a;

        d(Unsafe unsafe) {
            this.f2291a = unsafe;
        }

        public abstract byte a(Object obj, long j);

        public abstract void a(long j, byte b2);

        public abstract void a(Object obj, long j, byte b2);

        public abstract void a(Object obj, long j, double d);

        public abstract void a(Object obj, long j, float f);

        public abstract void a(Object obj, long j, boolean z);

        public abstract void a(byte[] bArr, long j, long j2, long j3);

        public abstract boolean b(Object obj, long j);

        public abstract float c(Object obj, long j);

        public abstract double d(Object obj, long j);

        public final int e(Object obj, long j) {
            return this.f2291a.getInt(obj, j);
        }

        public final void a(Object obj, long j, int i) {
            this.f2291a.putInt(obj, j, i);
        }

        public final long f(Object obj, long j) {
            return this.f2291a.getLong(obj, j);
        }

        public final void a(Object obj, long j, long j2) {
            this.f2291a.putLong(obj, j, j2);
        }
    }

    static boolean b() {
        return g;
    }

    static final class a extends d {
        a(Unsafe unsafe) {
            super(unsafe);
        }

        public final void a(long j, byte b2) {
            Memory.pokeByte((int) (j & -1), b2);
        }

        public final byte a(Object obj, long j) {
            if (ih.w) {
                return ih.k(obj, j);
            }
            return ih.l(obj, j);
        }

        public final void a(Object obj, long j, byte b2) {
            if (ih.w) {
                ih.c(obj, j, b2);
            } else {
                ih.d(obj, j, b2);
            }
        }

        public final boolean b(Object obj, long j) {
            if (ih.w) {
                return ih.i(obj, j);
            }
            return ih.j(obj, j);
        }

        public final void a(Object obj, long j, boolean z) {
            if (ih.w) {
                ih.c(obj, j, r3 ? (byte) 1 : 0);
            } else {
                ih.d(obj, j, r3 ? (byte) 1 : 0);
            }
        }

        public final float c(Object obj, long j) {
            return Float.intBitsToFloat(e(obj, j));
        }

        public final void a(Object obj, long j, float f) {
            a(obj, j, Float.floatToIntBits(f));
        }

        public final double d(Object obj, long j) {
            return Double.longBitsToDouble(f(obj, j));
        }

        public final void a(Object obj, long j, double d) {
            a(obj, j, Double.doubleToLongBits(d));
        }

        public final void a(byte[] bArr, long j, long j2, long j3) {
            Memory.pokeByteArray((int) (j2 & -1), bArr, (int) j, (int) j3);
        }
    }

    static <T> T a(Class cls) {
        try {
            return f2290b.allocateInstance(cls);
        } catch (InstantiationException e2) {
            throw new IllegalStateException(e2);
        }
    }

    private static int b(Class<?> cls) {
        if (h) {
            return f.f2291a.arrayBaseOffset(cls);
        }
        return -1;
    }

    private static int c(Class<?> cls) {
        if (h) {
            return f.f2291a.arrayIndexScale(cls);
        }
        return -1;
    }

    static int a(Object obj, long j2) {
        return f.e(obj, j2);
    }

    static void a(Object obj, long j2, int i2) {
        f.a(obj, j2, i2);
    }

    static long b(Object obj, long j2) {
        return f.f(obj, j2);
    }

    static void a(Object obj, long j2, long j3) {
        f.a(obj, j2, j3);
    }

    static boolean c(Object obj, long j2) {
        return f.b(obj, j2);
    }

    static void a(Object obj, long j2, boolean z) {
        f.a(obj, j2, z);
    }

    static float d(Object obj, long j2) {
        return f.c(obj, j2);
    }

    static void a(Object obj, long j2, float f2) {
        f.a(obj, j2, f2);
    }

    static double e(Object obj, long j2) {
        return f.d(obj, j2);
    }

    static void a(Object obj, long j2, double d2) {
        f.a(obj, j2, d2);
    }

    static Object f(Object obj, long j2) {
        return f.f2291a.getObject(obj, j2);
    }

    static void a(Object obj, long j2, Object obj2) {
        f.f2291a.putObject(obj, j2, obj2);
    }

    static byte a(byte[] bArr, long j2) {
        return f.a(bArr, i + j2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.ih.d.a(java.lang.Object, long, byte):void
     arg types: [byte[], long, byte]
     candidates:
      com.google.android.gms.internal.measurement.ih.d.a(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.ih.d.a(java.lang.Object, long, float):void
      com.google.android.gms.internal.measurement.ih.d.a(java.lang.Object, long, int):void
      com.google.android.gms.internal.measurement.ih.d.a(java.lang.Object, long, long):void
      com.google.android.gms.internal.measurement.ih.d.a(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.measurement.ih.d.a(java.lang.Object, long, byte):void */
    static void a(byte[] bArr, long j2, byte b2) {
        f.a((Object) bArr, i + j2, b2);
    }

    static void a(byte[] bArr, long j2, long j3, long j4) {
        f.a(bArr, j2, j3, j4);
    }

    static void a(long j2, byte b2) {
        f.a(j2, b2);
    }

    static long a(ByteBuffer byteBuffer) {
        return f.f(byteBuffer, v);
    }

    static Unsafe c() {
        try {
            return (Unsafe) AccessController.doPrivileged(new ii());
        } catch (Throwable unused) {
            return null;
        }
    }

    private static boolean f() {
        Unsafe unsafe = f2290b;
        if (unsafe == null) {
            return false;
        }
        try {
            Class<?> cls = unsafe.getClass();
            cls.getMethod("objectFieldOffset", Field.class);
            cls.getMethod("arrayBaseOffset", Class.class);
            cls.getMethod("arrayIndexScale", Class.class);
            cls.getMethod("getInt", Object.class, Long.TYPE);
            cls.getMethod("putInt", Object.class, Long.TYPE, Integer.TYPE);
            cls.getMethod("getLong", Object.class, Long.TYPE);
            cls.getMethod("putLong", Object.class, Long.TYPE, Long.TYPE);
            cls.getMethod("getObject", Object.class, Long.TYPE);
            cls.getMethod("putObject", Object.class, Long.TYPE, Object.class);
            if (eg.a()) {
                return true;
            }
            cls.getMethod("getByte", Object.class, Long.TYPE);
            cls.getMethod("putByte", Object.class, Long.TYPE, Byte.TYPE);
            cls.getMethod("getBoolean", Object.class, Long.TYPE);
            cls.getMethod("putBoolean", Object.class, Long.TYPE, Boolean.TYPE);
            cls.getMethod("getFloat", Object.class, Long.TYPE);
            cls.getMethod("putFloat", Object.class, Long.TYPE, Float.TYPE);
            cls.getMethod("getDouble", Object.class, Long.TYPE);
            cls.getMethod("putDouble", Object.class, Long.TYPE, Double.TYPE);
            return true;
        } catch (Throwable th) {
            Logger logger = f2289a;
            Level level = Level.WARNING;
            String valueOf = String.valueOf(th);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 71);
            sb.append("platform method missing - proto runtime falling back to safer methods: ");
            sb.append(valueOf);
            logger.logp(level, "com.google.protobuf.UnsafeUtil", "supportsUnsafeArrayOperations", sb.toString());
            return false;
        }
    }

    private static boolean g() {
        Unsafe unsafe = f2290b;
        if (unsafe == null) {
            return false;
        }
        try {
            Class<?> cls = unsafe.getClass();
            cls.getMethod("objectFieldOffset", Field.class);
            cls.getMethod("getLong", Object.class, Long.TYPE);
            if (h() == null) {
                return false;
            }
            if (eg.a()) {
                return true;
            }
            cls.getMethod("getByte", Long.TYPE);
            cls.getMethod("putByte", Long.TYPE, Byte.TYPE);
            cls.getMethod("getInt", Long.TYPE);
            cls.getMethod("putInt", Long.TYPE, Integer.TYPE);
            cls.getMethod("getLong", Long.TYPE);
            cls.getMethod("putLong", Long.TYPE, Long.TYPE);
            cls.getMethod("copyMemory", Long.TYPE, Long.TYPE, Long.TYPE);
            cls.getMethod("copyMemory", Object.class, Long.TYPE, Object.class, Long.TYPE, Long.TYPE);
            return true;
        } catch (Throwable th) {
            Logger logger = f2289a;
            Level level = Level.WARNING;
            String valueOf = String.valueOf(th);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 71);
            sb.append("platform method missing - proto runtime falling back to safer methods: ");
            sb.append(valueOf);
            logger.logp(level, "com.google.protobuf.UnsafeUtil", "supportsUnsafeByteBufferOperations", sb.toString());
            return false;
        }
    }

    private static boolean d(Class<?> cls) {
        Class<byte[]> cls2 = byte[].class;
        if (!eg.a()) {
            return false;
        }
        try {
            Class<?> cls3 = c;
            cls3.getMethod("peekLong", cls, Boolean.TYPE);
            cls3.getMethod("pokeLong", cls, Long.TYPE, Boolean.TYPE);
            cls3.getMethod("pokeInt", cls, Integer.TYPE, Boolean.TYPE);
            cls3.getMethod("peekInt", cls, Boolean.TYPE);
            cls3.getMethod("pokeByte", cls, Byte.TYPE);
            cls3.getMethod("peekByte", cls);
            cls3.getMethod("pokeByteArray", cls, cls2, Integer.TYPE, Integer.TYPE);
            cls3.getMethod("peekByteArray", cls, cls2, Integer.TYPE, Integer.TYPE);
            return true;
        } catch (Throwable unused) {
            return false;
        }
    }

    private static Field h() {
        Field a2;
        if (eg.a() && (a2 = a(Buffer.class, "effectiveDirectAddress")) != null) {
            return a2;
        }
        Field a3 = a(Buffer.class, "address");
        if (a3 == null || a3.getType() != Long.TYPE) {
            return null;
        }
        return a3;
    }

    private static Field a(Class<?> cls, String str) {
        try {
            Field declaredField = cls.getDeclaredField(str);
            declaredField.setAccessible(true);
            return declaredField;
        } catch (Throwable unused) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public static byte k(Object obj, long j2) {
        return (byte) (a(obj, -4 & j2) >>> ((int) (((j2 ^ -1) & 3) << 3)));
    }

    /* access modifiers changed from: private */
    public static byte l(Object obj, long j2) {
        return (byte) (a(obj, -4 & j2) >>> ((int) ((j2 & 3) << 3)));
    }

    /* access modifiers changed from: private */
    public static void c(Object obj, long j2, byte b2) {
        long j3 = -4 & j2;
        int i2 = ((((int) j2) ^ -1) & 3) << 3;
        a(obj, j3, ((255 & b2) << i2) | (a(obj, j3) & ((255 << i2) ^ -1)));
    }

    /* access modifiers changed from: private */
    public static void d(Object obj, long j2, byte b2) {
        long j3 = -4 & j2;
        int i2 = (((int) j2) & 3) << 3;
        a(obj, j3, ((255 & b2) << i2) | (a(obj, j3) & ((255 << i2) ^ -1)));
    }

    static {
        d dVar;
        Class<double[]> cls = double[].class;
        Class<float[]> cls2 = float[].class;
        Class<long[]> cls3 = long[].class;
        Class<int[]> cls4 = int[].class;
        Class<boolean[]> cls5 = boolean[].class;
        d dVar2 = null;
        if (f2290b != null) {
            if (!eg.a()) {
                dVar2 = new c(f2290b);
            } else if (d) {
                dVar2 = new b(f2290b);
            } else if (e) {
                dVar2 = new a(f2290b);
            }
        }
        f = dVar2;
        j = (long) b(cls5);
        k = (long) c(cls5);
        l = (long) b(cls4);
        m = (long) c(cls4);
        n = (long) b(cls3);
        o = (long) c(cls3);
        p = (long) b(cls2);
        q = (long) c(cls2);
        r = (long) b(cls);
        s = (long) c(cls);
        Field h2 = h();
        v = (h2 == null || (dVar = f) == null) ? -1 : dVar.f2291a.objectFieldOffset(h2);
    }

    static /* synthetic */ boolean i(Object obj, long j2) {
        return k(obj, j2) != 0;
    }

    static /* synthetic */ boolean j(Object obj, long j2) {
        return l(obj, j2) != 0;
    }
}
