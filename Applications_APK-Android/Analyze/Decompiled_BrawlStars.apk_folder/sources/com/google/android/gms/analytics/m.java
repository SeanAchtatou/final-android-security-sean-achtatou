package com.google.android.gms.analytics;

import com.google.android.gms.analytics.m;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.util.e;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class m<T extends m> {

    /* renamed from: a  reason: collision with root package name */
    private final List<Object> f1330a = new ArrayList();
    final n f;
    protected final k g;

    protected m(n nVar, e eVar) {
        l.a(nVar);
        this.f = nVar;
        k kVar = new k(this, eVar);
        kVar.e = true;
        this.g = kVar;
    }

    /* access modifiers changed from: protected */
    public void a(k kVar) {
    }

    public k d() {
        k a2 = this.g.a();
        b(a2);
        return a2;
    }

    /* access modifiers changed from: protected */
    public final void b(k kVar) {
        Iterator<Object> it = this.f1330a.iterator();
        while (it.hasNext()) {
            it.next();
        }
    }
}
