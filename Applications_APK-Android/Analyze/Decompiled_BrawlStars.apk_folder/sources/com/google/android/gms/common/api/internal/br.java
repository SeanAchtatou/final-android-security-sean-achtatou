package com.google.android.gms.common.api.internal;

import android.os.RemoteException;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.d;
import com.google.android.gms.common.api.internal.h;

public final class br extends bp<Boolean> {

    /* renamed from: b  reason: collision with root package name */
    private final h.a<?> f1432b;

    public br(h.a<?> aVar, com.google.android.gms.tasks.h<Boolean> hVar) {
        super(4, hVar);
        this.f1432b = aVar;
    }

    public final /* bridge */ /* synthetic */ void a(m mVar, boolean z) {
    }

    public final /* bridge */ /* synthetic */ void a(RuntimeException runtimeException) {
        super.a(runtimeException);
    }

    public final /* bridge */ /* synthetic */ void a(Status status) {
        super.a(status);
    }

    public final void d(d.a<?> aVar) throws RemoteException {
        bc remove = aVar.c.remove(this.f1432b);
        if (remove != null) {
            remove.f1416a.f1481a.f1477a = null;
        } else {
            this.f1430a.f2678a.b((Object) false);
        }
    }

    public final Feature[] b(d.a<?> aVar) {
        bc bcVar = aVar.c.get(this.f1432b);
        if (bcVar == null) {
            return null;
        }
        return bcVar.f1416a.f1482b;
    }

    public final boolean c(d.a<?> aVar) {
        bc bcVar = aVar.c.get(this.f1432b);
        return bcVar != null && bcVar.f1416a.c;
    }
}
