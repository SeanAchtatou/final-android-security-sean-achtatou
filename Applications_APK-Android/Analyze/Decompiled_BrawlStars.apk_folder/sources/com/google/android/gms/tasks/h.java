package com.google.android.gms.tasks;

public class h<TResult> {

    /* renamed from: a  reason: collision with root package name */
    public final ab<TResult> f2678a = new ab<>();

    public final void a(Boolean bool) {
        this.f2678a.a(bool);
    }

    public final void a(Exception exc) {
        this.f2678a.a(exc);
    }

    public final boolean b(Exception exc) {
        return this.f2678a.b(exc);
    }
}
