package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.support.v4.util.ArraySet;
import android.view.View;
import com.google.android.gms.common.api.Scope;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    public final Account f1595a;

    /* renamed from: b  reason: collision with root package name */
    public final Set<Scope> f1596b;
    final Set<Scope> c;
    public final Map<com.google.android.gms.common.api.a<?>, C0115b> d;
    public final int e;
    public final View f;
    public final String g;
    final String h;
    public final com.google.android.gms.signin.a i;
    public Integer j;

    /* renamed from: com.google.android.gms.common.internal.b$b  reason: collision with other inner class name */
    public static final class C0115b {

        /* renamed from: a  reason: collision with root package name */
        public final Set<Scope> f1599a;
    }

    public b(Account account, Set<Scope> set, Map<com.google.android.gms.common.api.a<?>, C0115b> map, int i2, View view, String str, String str2, com.google.android.gms.signin.a aVar) {
        this.f1595a = account;
        this.f1596b = set == null ? Collections.EMPTY_SET : Collections.unmodifiableSet(set);
        this.d = map == null ? Collections.EMPTY_MAP : map;
        this.f = view;
        this.e = i2;
        this.g = str;
        this.h = str2;
        this.i = aVar;
        HashSet hashSet = new HashSet(this.f1596b);
        for (C0115b bVar : this.d.values()) {
            hashSet.addAll(bVar.f1599a);
        }
        this.c = Collections.unmodifiableSet(hashSet);
    }

    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        public Account f1597a;

        /* renamed from: b  reason: collision with root package name */
        public String f1598b;
        public String c;
        private ArraySet<Scope> d;
        private Map<com.google.android.gms.common.api.a<?>, C0115b> e;
        private int f = 0;
        private View g;
        private com.google.android.gms.signin.a h = com.google.android.gms.signin.a.f2605a;

        public final a a(Collection<Scope> collection) {
            if (this.d == null) {
                this.d = new ArraySet<>();
            }
            this.d.addAll(collection);
            return this;
        }

        public final b a() {
            return new b(this.f1597a, this.d, this.e, this.f, this.g, this.f1598b, this.c, this.h);
        }
    }
}
