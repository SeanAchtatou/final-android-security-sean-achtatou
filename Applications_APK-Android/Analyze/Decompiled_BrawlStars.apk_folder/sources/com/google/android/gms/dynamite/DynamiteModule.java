package com.google.android.gms.dynamite;

import android.content.Context;
import android.database.Cursor;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.util.f;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.lang.reflect.Field;

public final class DynamiteModule {

    /* renamed from: a  reason: collision with root package name */
    public static final a f1779a = new b();

    /* renamed from: b  reason: collision with root package name */
    public static final a f1780b = new d();
    public static final a c = new e();
    public static final a d = new f();
    private static Boolean e = null;
    private static zzi f = null;
    private static zzk g = null;
    private static String h = null;
    private static int i = -1;
    private static final ThreadLocal<b> j = new ThreadLocal<>();
    private static final a.C0116a k = new a();
    private static final a l = new c();
    private static final a m = new g();
    private final Context n;

    public static class DynamiteLoaderClassLoader {
        public static ClassLoader sClassLoader;
    }

    public interface a {

        /* renamed from: com.google.android.gms.dynamite.DynamiteModule$a$a  reason: collision with other inner class name */
        public interface C0116a {
            int a(Context context, String str);

            int a(Context context, String str, boolean z) throws LoadingException;
        }

        public static class b {

            /* renamed from: a  reason: collision with root package name */
            public int f1781a = 0;

            /* renamed from: b  reason: collision with root package name */
            public int f1782b = 0;
            public int c = 0;
        }

        b a(Context context, String str, C0116a aVar) throws LoadingException;
    }

    static class b {

        /* renamed from: a  reason: collision with root package name */
        public Cursor f1783a;

        private b() {
        }

        /* synthetic */ b(byte b2) {
            this();
        }
    }

    public static DynamiteModule a(Context context, a aVar, String str) throws LoadingException {
        a.b a2;
        b bVar = j.get();
        b bVar2 = new b((byte) 0);
        j.set(bVar2);
        try {
            a2 = aVar.a(context, str, k);
            int i2 = a2.f1781a;
            int i3 = a2.f1782b;
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 68 + String.valueOf(str).length());
            sb.append("Considering local module ");
            sb.append(str);
            sb.append(":");
            sb.append(i2);
            sb.append(" and remote module ");
            sb.append(str);
            sb.append(":");
            sb.append(i3);
            sb.toString();
            if (a2.c == 0 || ((a2.c == -1 && a2.f1781a == 0) || (a2.c == 1 && a2.f1782b == 0))) {
                int i4 = a2.f1781a;
                int i5 = a2.f1782b;
                StringBuilder sb2 = new StringBuilder(91);
                sb2.append("No acceptable module found. Local version is ");
                sb2.append(i4);
                sb2.append(" and remote version is ");
                sb2.append(i5);
                sb2.append(".");
                throw new LoadingException(sb2.toString(), (byte) 0);
            } else if (a2.c == -1) {
                DynamiteModule b2 = b(context, str);
                if (bVar2.f1783a != null) {
                    bVar2.f1783a.close();
                }
                j.set(bVar);
                return b2;
            } else if (a2.c == 1) {
                DynamiteModule a3 = a(context, str, a2.f1782b);
                if (bVar2.f1783a != null) {
                    bVar2.f1783a.close();
                }
                j.set(bVar);
                return a3;
            } else {
                int i6 = a2.c;
                StringBuilder sb3 = new StringBuilder(47);
                sb3.append("VersionPolicy returned invalid code:");
                sb3.append(i6);
                throw new LoadingException(sb3.toString(), (byte) 0);
            }
        } catch (LoadingException e2) {
            String valueOf = String.valueOf(e2.getMessage());
            if (valueOf.length() != 0) {
                "Failed to load remote module: ".concat(valueOf);
            } else {
                new String("Failed to load remote module: ");
            }
            if (a2.f1781a == 0 || aVar.a(context, str, new c(a2.f1781a)).c != -1) {
                throw new LoadingException("Remote load failed. No local fallback found.", e2, (byte) 0);
            }
            DynamiteModule b3 = b(context, str);
            if (bVar2.f1783a != null) {
                bVar2.f1783a.close();
            }
            j.set(bVar);
            return b3;
        } catch (Throwable th) {
            if (bVar2.f1783a != null) {
                bVar2.f1783a.close();
            }
            j.set(bVar);
            throw th;
        }
    }

    public static class LoadingException extends Exception {
        private LoadingException(String str) {
            super(str);
        }

        private LoadingException(String str, Throwable th) {
            super(str, th);
        }

        /* synthetic */ LoadingException(String str, byte b2) {
            this(str);
        }

        /* synthetic */ LoadingException(String str, Throwable th, byte b2) {
            this(str, th);
        }
    }

    static class c implements a.C0116a {

        /* renamed from: a  reason: collision with root package name */
        private final int f1784a;

        /* renamed from: b  reason: collision with root package name */
        private final int f1785b = 0;

        public c(int i) {
            this.f1784a = i;
        }

        public final int a(Context context, String str, boolean z) {
            return 0;
        }

        public final int a(Context context, String str) {
            return this.f1784a;
        }
    }

    public static int a(Context context, String str) {
        try {
            ClassLoader classLoader = context.getApplicationContext().getClassLoader();
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 61);
            sb.append("com.google.android.gms.dynamite.descriptors.");
            sb.append(str);
            sb.append(".ModuleDescriptor");
            Class<?> loadClass = classLoader.loadClass(sb.toString());
            Field declaredField = loadClass.getDeclaredField("MODULE_ID");
            Field declaredField2 = loadClass.getDeclaredField("MODULE_VERSION");
            if (declaredField.get(null).equals(str)) {
                return declaredField2.getInt(null);
            }
            String valueOf = String.valueOf(declaredField.get(null));
            StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf).length() + 51 + String.valueOf(str).length());
            sb2.append("Module descriptor id '");
            sb2.append(valueOf);
            sb2.append("' didn't match expected id '");
            sb2.append(str);
            sb2.append("'");
            sb2.toString();
            return 0;
        } catch (ClassNotFoundException unused) {
            StringBuilder sb3 = new StringBuilder(String.valueOf(str).length() + 45);
            sb3.append("Local module descriptor class for ");
            sb3.append(str);
            sb3.append(" not found.");
            sb3.toString();
            return 0;
        } catch (Exception e2) {
            String valueOf2 = String.valueOf(e2.getMessage());
            if (valueOf2.length() != 0) {
                "Failed to load module descriptor class: ".concat(valueOf2);
            } else {
                new String("Failed to load module descriptor class: ");
            }
            return 0;
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:16|17|18|19) */
    /* JADX WARNING: Can't wrap try/catch for region: R(9:40|41|42|43|51|52|53|54|(0)(0)) */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        r2.set(null, java.lang.ClassLoader.getSystemClassLoader());
        r2 = java.lang.Boolean.FALSE;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0088, code lost:
        r1 = r2;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:18:0x0035 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:40:0x007e */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00bb A[SYNTHETIC, Splitter:B:56:0x00bb] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x00dc A[Catch:{ LoadingException -> 0x00c0, all -> 0x00e4 }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:23:0x0052=Splitter:B:23:0x0052, B:18:0x0035=Splitter:B:18:0x0035, B:35:0x007b=Splitter:B:35:0x007b} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int a(android.content.Context r8, java.lang.String r9, boolean r10) {
        /*
            java.lang.Class<com.google.android.gms.dynamite.DynamiteModule> r0 = com.google.android.gms.dynamite.DynamiteModule.class
            monitor-enter(r0)     // Catch:{ all -> 0x00e4 }
            java.lang.Boolean r1 = com.google.android.gms.dynamite.DynamiteModule.e     // Catch:{ all -> 0x00e1 }
            if (r1 != 0) goto L_0x00b4
            android.content.Context r1 = r8.getApplicationContext()     // Catch:{ ClassNotFoundException -> 0x0091, IllegalAccessException -> 0x008f, NoSuchFieldException -> 0x008d }
            java.lang.ClassLoader r1 = r1.getClassLoader()     // Catch:{ ClassNotFoundException -> 0x0091, IllegalAccessException -> 0x008f, NoSuchFieldException -> 0x008d }
            java.lang.Class<com.google.android.gms.dynamite.DynamiteModule$DynamiteLoaderClassLoader> r2 = com.google.android.gms.dynamite.DynamiteModule.DynamiteLoaderClassLoader.class
            java.lang.String r2 = r2.getName()     // Catch:{ ClassNotFoundException -> 0x0091, IllegalAccessException -> 0x008f, NoSuchFieldException -> 0x008d }
            java.lang.Class r1 = r1.loadClass(r2)     // Catch:{ ClassNotFoundException -> 0x0091, IllegalAccessException -> 0x008f, NoSuchFieldException -> 0x008d }
            java.lang.String r2 = "sClassLoader"
            java.lang.reflect.Field r2 = r1.getDeclaredField(r2)     // Catch:{ ClassNotFoundException -> 0x0091, IllegalAccessException -> 0x008f, NoSuchFieldException -> 0x008d }
            monitor-enter(r1)     // Catch:{ ClassNotFoundException -> 0x0091, IllegalAccessException -> 0x008f, NoSuchFieldException -> 0x008d }
            r3 = 0
            java.lang.Object r4 = r2.get(r3)     // Catch:{ all -> 0x008a }
            java.lang.ClassLoader r4 = (java.lang.ClassLoader) r4     // Catch:{ all -> 0x008a }
            if (r4 == 0) goto L_0x0038
            java.lang.ClassLoader r2 = java.lang.ClassLoader.getSystemClassLoader()     // Catch:{ all -> 0x008a }
            if (r4 != r2) goto L_0x0032
            java.lang.Boolean r2 = java.lang.Boolean.FALSE     // Catch:{ all -> 0x008a }
            goto L_0x0087
        L_0x0032:
            a(r4)     // Catch:{ LoadingException -> 0x0035 }
        L_0x0035:
            java.lang.Boolean r2 = java.lang.Boolean.TRUE     // Catch:{ all -> 0x008a }
            goto L_0x0087
        L_0x0038:
            java.lang.String r4 = "com.google.android.gms"
            android.content.Context r5 = r8.getApplicationContext()     // Catch:{ all -> 0x008a }
            java.lang.String r5 = r5.getPackageName()     // Catch:{ all -> 0x008a }
            boolean r4 = r4.equals(r5)     // Catch:{ all -> 0x008a }
            if (r4 == 0) goto L_0x0052
            java.lang.ClassLoader r4 = java.lang.ClassLoader.getSystemClassLoader()     // Catch:{ all -> 0x008a }
            r2.set(r3, r4)     // Catch:{ all -> 0x008a }
            java.lang.Boolean r2 = java.lang.Boolean.FALSE     // Catch:{ all -> 0x008a }
            goto L_0x0087
        L_0x0052:
            int r4 = c(r8, r9, r10)     // Catch:{ LoadingException -> 0x007e }
            java.lang.String r5 = com.google.android.gms.dynamite.DynamiteModule.h     // Catch:{ LoadingException -> 0x007e }
            if (r5 == 0) goto L_0x007b
            java.lang.String r5 = com.google.android.gms.dynamite.DynamiteModule.h     // Catch:{ LoadingException -> 0x007e }
            boolean r5 = r5.isEmpty()     // Catch:{ LoadingException -> 0x007e }
            if (r5 == 0) goto L_0x0063
            goto L_0x007b
        L_0x0063:
            com.google.android.gms.dynamite.h r5 = new com.google.android.gms.dynamite.h     // Catch:{ LoadingException -> 0x007e }
            java.lang.String r6 = com.google.android.gms.dynamite.DynamiteModule.h     // Catch:{ LoadingException -> 0x007e }
            java.lang.ClassLoader r7 = java.lang.ClassLoader.getSystemClassLoader()     // Catch:{ LoadingException -> 0x007e }
            r5.<init>(r6, r7)     // Catch:{ LoadingException -> 0x007e }
            a(r5)     // Catch:{ LoadingException -> 0x007e }
            r2.set(r3, r5)     // Catch:{ LoadingException -> 0x007e }
            java.lang.Boolean r5 = java.lang.Boolean.TRUE     // Catch:{ LoadingException -> 0x007e }
            com.google.android.gms.dynamite.DynamiteModule.e = r5     // Catch:{ LoadingException -> 0x007e }
            monitor-exit(r1)     // Catch:{ all -> 0x008a }
            monitor-exit(r0)     // Catch:{ all -> 0x00e1 }
            return r4
        L_0x007b:
            monitor-exit(r1)     // Catch:{ all -> 0x008a }
            monitor-exit(r0)     // Catch:{ all -> 0x00e1 }
            return r4
        L_0x007e:
            java.lang.ClassLoader r4 = java.lang.ClassLoader.getSystemClassLoader()     // Catch:{ all -> 0x008a }
            r2.set(r3, r4)     // Catch:{ all -> 0x008a }
            java.lang.Boolean r2 = java.lang.Boolean.FALSE     // Catch:{ all -> 0x008a }
        L_0x0087:
            monitor-exit(r1)     // Catch:{ all -> 0x008a }
            r1 = r2
            goto L_0x00b2
        L_0x008a:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x008a }
            throw r2     // Catch:{ ClassNotFoundException -> 0x0091, IllegalAccessException -> 0x008f, NoSuchFieldException -> 0x008d }
        L_0x008d:
            r1 = move-exception
            goto L_0x0092
        L_0x008f:
            r1 = move-exception
            goto L_0x0092
        L_0x0091:
            r1 = move-exception
        L_0x0092:
            java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ all -> 0x00e1 }
            java.lang.String r2 = java.lang.String.valueOf(r1)     // Catch:{ all -> 0x00e1 }
            int r2 = r2.length()     // Catch:{ all -> 0x00e1 }
            int r2 = r2 + 30
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00e1 }
            r3.<init>(r2)     // Catch:{ all -> 0x00e1 }
            java.lang.String r2 = "Failed to load module via V2: "
            r3.append(r2)     // Catch:{ all -> 0x00e1 }
            r3.append(r1)     // Catch:{ all -> 0x00e1 }
            r3.toString()     // Catch:{ all -> 0x00e1 }
            java.lang.Boolean r1 = java.lang.Boolean.FALSE     // Catch:{ all -> 0x00e1 }
        L_0x00b2:
            com.google.android.gms.dynamite.DynamiteModule.e = r1     // Catch:{ all -> 0x00e1 }
        L_0x00b4:
            monitor-exit(r0)     // Catch:{ all -> 0x00e1 }
            boolean r0 = r1.booleanValue()     // Catch:{ all -> 0x00e4 }
            if (r0 == 0) goto L_0x00dc
            int r8 = c(r8, r9, r10)     // Catch:{ LoadingException -> 0x00c0 }
            return r8
        L_0x00c0:
            r9 = move-exception
            java.lang.String r10 = "Failed to retrieve remote module version: "
            java.lang.String r9 = r9.getMessage()     // Catch:{ all -> 0x00e4 }
            java.lang.String r9 = java.lang.String.valueOf(r9)     // Catch:{ all -> 0x00e4 }
            int r0 = r9.length()     // Catch:{ all -> 0x00e4 }
            if (r0 == 0) goto L_0x00d5
            r10.concat(r9)     // Catch:{ all -> 0x00e4 }
            goto L_0x00da
        L_0x00d5:
            java.lang.String r9 = new java.lang.String     // Catch:{ all -> 0x00e4 }
            r9.<init>(r10)     // Catch:{ all -> 0x00e4 }
        L_0x00da:
            r8 = 0
            return r8
        L_0x00dc:
            int r8 = b(r8, r9, r10)     // Catch:{ all -> 0x00e4 }
            return r8
        L_0x00e1:
            r9 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00e1 }
            throw r9     // Catch:{ all -> 0x00e4 }
        L_0x00e4:
            r9 = move-exception
            com.google.android.gms.common.util.f.a(r8, r9)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.dynamite.DynamiteModule.a(android.content.Context, java.lang.String, boolean):int");
    }

    private static int b(Context context, String str, boolean z) {
        zzi a2 = a(context);
        if (a2 == null) {
            return 0;
        }
        try {
            if (a2.a() >= 2) {
                return a2.b(ObjectWrapper.a(context), str, z);
            }
            return a2.a(ObjectWrapper.a(context), str, z);
        } catch (RemoteException e2) {
            String valueOf = String.valueOf(e2.getMessage());
            if (valueOf.length() != 0) {
                "Failed to retrieve remote module version: ".concat(valueOf);
            } else {
                new String("Failed to retrieve remote module version: ");
            }
            return 0;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:52:0x00a5  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int c(android.content.Context r8, java.lang.String r9, boolean r10) throws com.google.android.gms.dynamite.DynamiteModule.LoadingException {
        /*
            r0 = 0
            r1 = 0
            android.content.ContentResolver r2 = r8.getContentResolver()     // Catch:{ Exception -> 0x0095 }
            if (r10 == 0) goto L_0x000b
            java.lang.String r8 = "api_force_staging"
            goto L_0x000d
        L_0x000b:
            java.lang.String r8 = "api"
        L_0x000d:
            int r10 = r8.length()     // Catch:{ Exception -> 0x0095 }
            int r10 = r10 + 42
            java.lang.String r3 = java.lang.String.valueOf(r9)     // Catch:{ Exception -> 0x0095 }
            int r3 = r3.length()     // Catch:{ Exception -> 0x0095 }
            int r10 = r10 + r3
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0095 }
            r3.<init>(r10)     // Catch:{ Exception -> 0x0095 }
            java.lang.String r10 = "content://com.google.android.gms.chimera/"
            r3.append(r10)     // Catch:{ Exception -> 0x0095 }
            r3.append(r8)     // Catch:{ Exception -> 0x0095 }
            java.lang.String r8 = "/"
            r3.append(r8)     // Catch:{ Exception -> 0x0095 }
            r3.append(r9)     // Catch:{ Exception -> 0x0095 }
            java.lang.String r8 = r3.toString()     // Catch:{ Exception -> 0x0095 }
            android.net.Uri r3 = android.net.Uri.parse(r8)     // Catch:{ Exception -> 0x0095 }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r8 = r2.query(r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0095 }
            if (r8 == 0) goto L_0x0083
            boolean r9 = r8.moveToFirst()     // Catch:{ Exception -> 0x008f, all -> 0x008b }
            if (r9 == 0) goto L_0x0083
            int r9 = r8.getInt(r1)     // Catch:{ Exception -> 0x008f, all -> 0x008b }
            if (r9 <= 0) goto L_0x007d
            java.lang.Class<com.google.android.gms.dynamite.DynamiteModule> r10 = com.google.android.gms.dynamite.DynamiteModule.class
            monitor-enter(r10)     // Catch:{ Exception -> 0x008f, all -> 0x008b }
            r2 = 2
            java.lang.String r2 = r8.getString(r2)     // Catch:{ all -> 0x007a }
            com.google.android.gms.dynamite.DynamiteModule.h = r2     // Catch:{ all -> 0x007a }
            java.lang.String r2 = "loaderVersion"
            int r2 = r8.getColumnIndex(r2)     // Catch:{ all -> 0x007a }
            if (r2 < 0) goto L_0x0067
            int r2 = r8.getInt(r2)     // Catch:{ all -> 0x007a }
            com.google.android.gms.dynamite.DynamiteModule.i = r2     // Catch:{ all -> 0x007a }
        L_0x0067:
            monitor-exit(r10)     // Catch:{ all -> 0x007a }
            java.lang.ThreadLocal<com.google.android.gms.dynamite.DynamiteModule$b> r10 = com.google.android.gms.dynamite.DynamiteModule.j     // Catch:{ Exception -> 0x008f, all -> 0x008b }
            java.lang.Object r10 = r10.get()     // Catch:{ Exception -> 0x008f, all -> 0x008b }
            com.google.android.gms.dynamite.DynamiteModule$b r10 = (com.google.android.gms.dynamite.DynamiteModule.b) r10     // Catch:{ Exception -> 0x008f, all -> 0x008b }
            if (r10 == 0) goto L_0x007d
            android.database.Cursor r2 = r10.f1783a     // Catch:{ Exception -> 0x008f, all -> 0x008b }
            if (r2 != 0) goto L_0x007d
            r10.f1783a = r8     // Catch:{ Exception -> 0x008f, all -> 0x008b }
            r8 = r0
            goto L_0x007d
        L_0x007a:
            r9 = move-exception
            monitor-exit(r10)     // Catch:{ all -> 0x007a }
            throw r9     // Catch:{ Exception -> 0x008f, all -> 0x008b }
        L_0x007d:
            if (r8 == 0) goto L_0x0082
            r8.close()
        L_0x0082:
            return r9
        L_0x0083:
            com.google.android.gms.dynamite.DynamiteModule$LoadingException r9 = new com.google.android.gms.dynamite.DynamiteModule$LoadingException     // Catch:{ Exception -> 0x008f, all -> 0x008b }
            java.lang.String r10 = "Failed to connect to dynamite module ContentResolver."
            r9.<init>(r10, r1)     // Catch:{ Exception -> 0x008f, all -> 0x008b }
            throw r9     // Catch:{ Exception -> 0x008f, all -> 0x008b }
        L_0x008b:
            r9 = move-exception
            r0 = r8
            r8 = r9
            goto L_0x00a3
        L_0x008f:
            r9 = move-exception
            r0 = r8
            r8 = r9
            goto L_0x0096
        L_0x0093:
            r8 = move-exception
            goto L_0x00a3
        L_0x0095:
            r8 = move-exception
        L_0x0096:
            boolean r9 = r8 instanceof com.google.android.gms.dynamite.DynamiteModule.LoadingException     // Catch:{ all -> 0x0093 }
            if (r9 == 0) goto L_0x009b
            throw r8     // Catch:{ all -> 0x0093 }
        L_0x009b:
            com.google.android.gms.dynamite.DynamiteModule$LoadingException r9 = new com.google.android.gms.dynamite.DynamiteModule$LoadingException     // Catch:{ all -> 0x0093 }
            java.lang.String r10 = "V2 version check failed"
            r9.<init>(r10, r8, r1)     // Catch:{ all -> 0x0093 }
            throw r9     // Catch:{ all -> 0x0093 }
        L_0x00a3:
            if (r0 == 0) goto L_0x00a8
            r0.close()
        L_0x00a8:
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.dynamite.DynamiteModule.c(android.content.Context, java.lang.String, boolean):int");
    }

    private static DynamiteModule b(Context context, String str) {
        String valueOf = String.valueOf(str);
        if (valueOf.length() != 0) {
            "Selected local version of ".concat(valueOf);
        } else {
            new String("Selected local version of ");
        }
        return new DynamiteModule(context.getApplicationContext());
    }

    private static DynamiteModule a(Context context, String str, int i2) throws LoadingException {
        Boolean bool;
        try {
            synchronized (DynamiteModule.class) {
                bool = e;
            }
            if (bool == null) {
                throw new LoadingException("Failed to determine which loading route to use.", (byte) 0);
            } else if (bool.booleanValue()) {
                return c(context, str, i2);
            } else {
                return b(context, str, i2);
            }
        } catch (Throwable th) {
            f.a(context, th);
            throw th;
        }
    }

    private static DynamiteModule b(Context context, String str, int i2) throws LoadingException {
        IObjectWrapper iObjectWrapper;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 51);
        sb.append("Selected remote version of ");
        sb.append(str);
        sb.append(", version >= ");
        sb.append(i2);
        sb.toString();
        zzi a2 = a(context);
        if (a2 != null) {
            try {
                if (a2.a() >= 2) {
                    iObjectWrapper = a2.b(ObjectWrapper.a(context), str, i2);
                } else {
                    iObjectWrapper = a2.a(ObjectWrapper.a(context), str, i2);
                }
                if (ObjectWrapper.a(iObjectWrapper) != null) {
                    return new DynamiteModule((Context) ObjectWrapper.a(iObjectWrapper));
                }
                throw new LoadingException("Failed to load remote module.", (byte) 0);
            } catch (RemoteException e2) {
                throw new LoadingException("Failed to load remote module.", e2, (byte) 0);
            }
        } else {
            throw new LoadingException("Failed to create IDynamiteLoader.", (byte) 0);
        }
    }

    /* JADX WARN: Type inference failed for: r1v7, types: [android.os.IInterface] */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0068, code lost:
        return null;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.google.android.gms.dynamite.zzi a(android.content.Context r4) {
        /*
            java.lang.Class<com.google.android.gms.dynamite.DynamiteModule> r0 = com.google.android.gms.dynamite.DynamiteModule.class
            monitor-enter(r0)
            com.google.android.gms.dynamite.zzi r1 = com.google.android.gms.dynamite.DynamiteModule.f     // Catch:{ all -> 0x0069 }
            if (r1 == 0) goto L_0x000b
            com.google.android.gms.dynamite.zzi r4 = com.google.android.gms.dynamite.DynamiteModule.f     // Catch:{ all -> 0x0069 }
            monitor-exit(r0)     // Catch:{ all -> 0x0069 }
            return r4
        L_0x000b:
            com.google.android.gms.common.d r1 = com.google.android.gms.common.d.b()     // Catch:{ all -> 0x0069 }
            int r1 = r1.a(r4)     // Catch:{ all -> 0x0069 }
            r2 = 0
            if (r1 == 0) goto L_0x0018
            monitor-exit(r0)     // Catch:{ all -> 0x0069 }
            return r2
        L_0x0018:
            java.lang.String r1 = "com.google.android.gms"
            r3 = 3
            android.content.Context r4 = r4.createPackageContext(r1, r3)     // Catch:{ Exception -> 0x004d }
            java.lang.ClassLoader r4 = r4.getClassLoader()     // Catch:{ Exception -> 0x004d }
            java.lang.String r1 = "com.google.android.gms.chimera.container.DynamiteLoaderImpl"
            java.lang.Class r4 = r4.loadClass(r1)     // Catch:{ Exception -> 0x004d }
            java.lang.Object r4 = r4.newInstance()     // Catch:{ Exception -> 0x004d }
            android.os.IBinder r4 = (android.os.IBinder) r4     // Catch:{ Exception -> 0x004d }
            if (r4 != 0) goto L_0x0033
            r4 = r2
            goto L_0x0047
        L_0x0033:
            java.lang.String r1 = "com.google.android.gms.dynamite.IDynamiteLoader"
            android.os.IInterface r1 = r4.queryLocalInterface(r1)     // Catch:{ Exception -> 0x004d }
            boolean r3 = r1 instanceof com.google.android.gms.dynamite.zzi     // Catch:{ Exception -> 0x004d }
            if (r3 == 0) goto L_0x0041
            r4 = r1
            com.google.android.gms.dynamite.zzi r4 = (com.google.android.gms.dynamite.zzi) r4     // Catch:{ Exception -> 0x004d }
            goto L_0x0047
        L_0x0041:
            com.google.android.gms.dynamite.zzj r1 = new com.google.android.gms.dynamite.zzj     // Catch:{ Exception -> 0x004d }
            r1.<init>(r4)     // Catch:{ Exception -> 0x004d }
            r4 = r1
        L_0x0047:
            if (r4 == 0) goto L_0x0067
            com.google.android.gms.dynamite.DynamiteModule.f = r4     // Catch:{ Exception -> 0x004d }
            monitor-exit(r0)     // Catch:{ all -> 0x0069 }
            return r4
        L_0x004d:
            r4 = move-exception
            java.lang.String r1 = "Failed to load IDynamiteLoader from GmsCore: "
            java.lang.String r4 = r4.getMessage()     // Catch:{ all -> 0x0069 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ all -> 0x0069 }
            int r3 = r4.length()     // Catch:{ all -> 0x0069 }
            if (r3 == 0) goto L_0x0062
            r1.concat(r4)     // Catch:{ all -> 0x0069 }
            goto L_0x0067
        L_0x0062:
            java.lang.String r4 = new java.lang.String     // Catch:{ all -> 0x0069 }
            r4.<init>(r1)     // Catch:{ all -> 0x0069 }
        L_0x0067:
            monitor-exit(r0)     // Catch:{ all -> 0x0069 }
            return r2
        L_0x0069:
            r4 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0069 }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.dynamite.DynamiteModule.a(android.content.Context):com.google.android.gms.dynamite.zzi");
    }

    private static DynamiteModule c(Context context, String str, int i2) throws LoadingException {
        zzk zzk;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 51);
        sb.append("Selected remote version of ");
        sb.append(str);
        sb.append(", version >= ");
        sb.append(i2);
        sb.toString();
        synchronized (DynamiteModule.class) {
            zzk = g;
        }
        if (zzk != null) {
            b bVar = j.get();
            if (bVar == null || bVar.f1783a == null) {
                throw new LoadingException("No result cursor", (byte) 0);
            }
            Context a2 = a(context.getApplicationContext(), str, i2, bVar.f1783a, zzk);
            if (a2 != null) {
                return new DynamiteModule(a2);
            }
            throw new LoadingException("Failed to get module context", (byte) 0);
        }
        throw new LoadingException("DynamiteLoaderV2 was not cached.", (byte) 0);
    }

    private static Boolean a() {
        Boolean valueOf;
        synchronized (DynamiteModule.class) {
            valueOf = Boolean.valueOf(i >= 2);
        }
        return valueOf;
    }

    private static Context a(Context context, String str, int i2, Cursor cursor, zzk zzk) {
        IObjectWrapper iObjectWrapper;
        try {
            ObjectWrapper.a((Object) null);
            if (a().booleanValue()) {
                iObjectWrapper = zzk.b(ObjectWrapper.a(context), str, i2, ObjectWrapper.a(cursor));
            } else {
                iObjectWrapper = zzk.a(ObjectWrapper.a(context), str, i2, ObjectWrapper.a(cursor));
            }
            return (Context) ObjectWrapper.a(iObjectWrapper);
        } catch (Exception e2) {
            String valueOf = String.valueOf(e2.toString());
            if (valueOf.length() != 0) {
                "Failed to load DynamiteLoader: ".concat(valueOf);
            } else {
                new String("Failed to load DynamiteLoader: ");
            }
            return null;
        }
    }

    /* JADX WARN: Type inference failed for: r1v5, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(java.lang.ClassLoader r3) throws com.google.android.gms.dynamite.DynamiteModule.LoadingException {
        /*
            r0 = 0
            java.lang.String r1 = "com.google.android.gms.dynamiteloader.DynamiteLoaderV2"
            java.lang.Class r3 = r3.loadClass(r1)     // Catch:{ ClassNotFoundException -> 0x0038, IllegalAccessException -> 0x0036, InstantiationException -> 0x0034, InvocationTargetException -> 0x0032, NoSuchMethodException -> 0x0030 }
            java.lang.Class[] r1 = new java.lang.Class[r0]     // Catch:{ ClassNotFoundException -> 0x0038, IllegalAccessException -> 0x0036, InstantiationException -> 0x0034, InvocationTargetException -> 0x0032, NoSuchMethodException -> 0x0030 }
            java.lang.reflect.Constructor r3 = r3.getConstructor(r1)     // Catch:{ ClassNotFoundException -> 0x0038, IllegalAccessException -> 0x0036, InstantiationException -> 0x0034, InvocationTargetException -> 0x0032, NoSuchMethodException -> 0x0030 }
            java.lang.Object[] r1 = new java.lang.Object[r0]     // Catch:{ ClassNotFoundException -> 0x0038, IllegalAccessException -> 0x0036, InstantiationException -> 0x0034, InvocationTargetException -> 0x0032, NoSuchMethodException -> 0x0030 }
            java.lang.Object r3 = r3.newInstance(r1)     // Catch:{ ClassNotFoundException -> 0x0038, IllegalAccessException -> 0x0036, InstantiationException -> 0x0034, InvocationTargetException -> 0x0032, NoSuchMethodException -> 0x0030 }
            android.os.IBinder r3 = (android.os.IBinder) r3     // Catch:{ ClassNotFoundException -> 0x0038, IllegalAccessException -> 0x0036, InstantiationException -> 0x0034, InvocationTargetException -> 0x0032, NoSuchMethodException -> 0x0030 }
            if (r3 != 0) goto L_0x0019
            r3 = 0
            goto L_0x002d
        L_0x0019:
            java.lang.String r1 = "com.google.android.gms.dynamite.IDynamiteLoaderV2"
            android.os.IInterface r1 = r3.queryLocalInterface(r1)     // Catch:{ ClassNotFoundException -> 0x0038, IllegalAccessException -> 0x0036, InstantiationException -> 0x0034, InvocationTargetException -> 0x0032, NoSuchMethodException -> 0x0030 }
            boolean r2 = r1 instanceof com.google.android.gms.dynamite.zzk     // Catch:{ ClassNotFoundException -> 0x0038, IllegalAccessException -> 0x0036, InstantiationException -> 0x0034, InvocationTargetException -> 0x0032, NoSuchMethodException -> 0x0030 }
            if (r2 == 0) goto L_0x0027
            r3 = r1
            com.google.android.gms.dynamite.zzk r3 = (com.google.android.gms.dynamite.zzk) r3     // Catch:{ ClassNotFoundException -> 0x0038, IllegalAccessException -> 0x0036, InstantiationException -> 0x0034, InvocationTargetException -> 0x0032, NoSuchMethodException -> 0x0030 }
            goto L_0x002d
        L_0x0027:
            com.google.android.gms.dynamite.zzl r1 = new com.google.android.gms.dynamite.zzl     // Catch:{ ClassNotFoundException -> 0x0038, IllegalAccessException -> 0x0036, InstantiationException -> 0x0034, InvocationTargetException -> 0x0032, NoSuchMethodException -> 0x0030 }
            r1.<init>(r3)     // Catch:{ ClassNotFoundException -> 0x0038, IllegalAccessException -> 0x0036, InstantiationException -> 0x0034, InvocationTargetException -> 0x0032, NoSuchMethodException -> 0x0030 }
            r3 = r1
        L_0x002d:
            com.google.android.gms.dynamite.DynamiteModule.g = r3     // Catch:{ ClassNotFoundException -> 0x0038, IllegalAccessException -> 0x0036, InstantiationException -> 0x0034, InvocationTargetException -> 0x0032, NoSuchMethodException -> 0x0030 }
            return
        L_0x0030:
            r3 = move-exception
            goto L_0x0039
        L_0x0032:
            r3 = move-exception
            goto L_0x0039
        L_0x0034:
            r3 = move-exception
            goto L_0x0039
        L_0x0036:
            r3 = move-exception
            goto L_0x0039
        L_0x0038:
            r3 = move-exception
        L_0x0039:
            com.google.android.gms.dynamite.DynamiteModule$LoadingException r1 = new com.google.android.gms.dynamite.DynamiteModule$LoadingException
            java.lang.String r2 = "Failed to instantiate dynamite loader"
            r1.<init>(r2, r3, r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.dynamite.DynamiteModule.a(java.lang.ClassLoader):void");
    }

    public final IBinder a(String str) throws LoadingException {
        try {
            return (IBinder) this.n.getClassLoader().loadClass(str).newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e2) {
            String valueOf = String.valueOf(str);
            throw new LoadingException(valueOf.length() != 0 ? "Failed to instantiate module class: ".concat(valueOf) : new String("Failed to instantiate module class: "), e2, (byte) 0);
        }
    }

    private DynamiteModule(Context context) {
        this.n = (Context) l.a(context);
    }
}
