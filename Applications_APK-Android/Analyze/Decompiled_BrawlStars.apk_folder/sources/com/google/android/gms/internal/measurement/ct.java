package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class ct extends iz<ct> {
    private static volatile ct[] f;

    /* renamed from: a  reason: collision with root package name */
    public cu[] f2136a = cu.a();

    /* renamed from: b  reason: collision with root package name */
    public String f2137b = null;
    public Long c = null;
    public Long d = null;
    public Integer e = null;

    public static ct[] a() {
        if (f == null) {
            synchronized (jd.f2312b) {
                if (f == null) {
                    f = new ct[0];
                }
            }
        }
        return f;
    }

    public ct() {
        this.L = null;
        this.M = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ct)) {
            return false;
        }
        ct ctVar = (ct) obj;
        if (!jd.a(this.f2136a, ctVar.f2136a)) {
            return false;
        }
        String str = this.f2137b;
        if (str == null) {
            if (ctVar.f2137b != null) {
                return false;
            }
        } else if (!str.equals(ctVar.f2137b)) {
            return false;
        }
        Long l = this.c;
        if (l == null) {
            if (ctVar.c != null) {
                return false;
            }
        } else if (!l.equals(ctVar.c)) {
            return false;
        }
        Long l2 = this.d;
        if (l2 == null) {
            if (ctVar.d != null) {
                return false;
            }
        } else if (!l2.equals(ctVar.d)) {
            return false;
        }
        Integer num = this.e;
        if (num == null) {
            if (ctVar.e != null) {
                return false;
            }
        } else if (!num.equals(ctVar.e)) {
            return false;
        }
        if (this.L == null || this.L.a()) {
            return ctVar.L == null || ctVar.L.a();
        }
        return this.L.equals(ctVar.L);
    }

    public final int hashCode() {
        int hashCode = (((getClass().getName().hashCode() + 527) * 31) + jd.a(this.f2136a)) * 31;
        String str = this.f2137b;
        int i = 0;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        Long l = this.c;
        int hashCode3 = (hashCode2 + (l == null ? 0 : l.hashCode())) * 31;
        Long l2 = this.d;
        int hashCode4 = (hashCode3 + (l2 == null ? 0 : l2.hashCode())) * 31;
        Integer num = this.e;
        int hashCode5 = (hashCode4 + (num == null ? 0 : num.hashCode())) * 31;
        if (this.L != null && !this.L.a()) {
            i = this.L.hashCode();
        }
        return hashCode5 + i;
    }

    public final void a(iy iyVar) throws IOException {
        cu[] cuVarArr = this.f2136a;
        if (cuVarArr != null && cuVarArr.length > 0) {
            int i = 0;
            while (true) {
                cu[] cuVarArr2 = this.f2136a;
                if (i >= cuVarArr2.length) {
                    break;
                }
                cu cuVar = cuVarArr2[i];
                if (cuVar != null) {
                    iyVar.a(1, cuVar);
                }
                i++;
            }
        }
        String str = this.f2137b;
        if (str != null) {
            iyVar.a(2, str);
        }
        Long l = this.c;
        if (l != null) {
            iyVar.b(3, l.longValue());
        }
        Long l2 = this.d;
        if (l2 != null) {
            iyVar.b(4, l2.longValue());
        }
        Integer num = this.e;
        if (num != null) {
            iyVar.a(5, num.intValue());
        }
        super.a(iyVar);
    }

    /* access modifiers changed from: protected */
    public final int b() {
        int b2 = super.b();
        cu[] cuVarArr = this.f2136a;
        if (cuVarArr != null && cuVarArr.length > 0) {
            int i = 0;
            while (true) {
                cu[] cuVarArr2 = this.f2136a;
                if (i >= cuVarArr2.length) {
                    break;
                }
                cu cuVar = cuVarArr2[i];
                if (cuVar != null) {
                    b2 += iy.b(1, cuVar);
                }
                i++;
            }
        }
        String str = this.f2137b;
        if (str != null) {
            b2 += iy.b(2, str);
        }
        Long l = this.c;
        if (l != null) {
            b2 += iy.c(3, l.longValue());
        }
        Long l2 = this.d;
        if (l2 != null) {
            b2 += iy.c(4, l2.longValue());
        }
        Integer num = this.e;
        return num != null ? b2 + iy.b(5, num.intValue()) : b2;
    }

    public final /* synthetic */ je a(iw iwVar) throws IOException {
        while (true) {
            int a2 = iwVar.a();
            if (a2 == 0) {
                return this;
            }
            if (a2 == 10) {
                int a3 = jh.a(iwVar, 10);
                cu[] cuVarArr = this.f2136a;
                int length = cuVarArr == null ? 0 : cuVarArr.length;
                cu[] cuVarArr2 = new cu[(a3 + length)];
                if (length != 0) {
                    System.arraycopy(this.f2136a, 0, cuVarArr2, 0, length);
                }
                while (length < cuVarArr2.length - 1) {
                    cuVarArr2[length] = new cu();
                    iwVar.a(cuVarArr2[length]);
                    iwVar.a();
                    length++;
                }
                cuVarArr2[length] = new cu();
                iwVar.a(cuVarArr2[length]);
                this.f2136a = cuVarArr2;
            } else if (a2 == 18) {
                this.f2137b = iwVar.c();
            } else if (a2 == 24) {
                this.c = Long.valueOf(iwVar.e());
            } else if (a2 == 32) {
                this.d = Long.valueOf(iwVar.e());
            } else if (a2 == 40) {
                this.e = Integer.valueOf(iwVar.d());
            } else if (!super.a(iwVar, a2)) {
                return this;
            }
        }
    }
}
