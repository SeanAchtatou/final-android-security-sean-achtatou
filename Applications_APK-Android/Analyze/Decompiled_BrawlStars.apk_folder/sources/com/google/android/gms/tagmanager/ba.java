package com.google.android.gms.tagmanager;

final class ba extends az {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f2636a = new Object();
    private static ba k;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public x f2637b;
    private volatile v c;
    private int d = 1800000;
    private boolean e = true;
    private boolean f = false;
    private boolean g = true;
    private boolean h = true;
    private y i = new bb(this);
    private boolean j = false;

    public static ba b() {
        if (k == null) {
            k = new ba();
        }
        return k;
    }

    private ba() {
    }

    public final synchronized void a() {
        if (!this.f) {
            ab.d("Dispatch call queued. Dispatch will run once initialization is complete.");
            this.e = true;
            return;
        }
        v vVar = this.c;
        new bc(this);
    }
}
