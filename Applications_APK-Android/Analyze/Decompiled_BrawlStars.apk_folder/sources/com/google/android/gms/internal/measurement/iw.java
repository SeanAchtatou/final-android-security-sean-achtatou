package com.google.android.gms.internal.measurement;

import android.support.v7.widget.ActivityChooserView;
import java.io.IOException;

public final class iw {

    /* renamed from: a  reason: collision with root package name */
    final byte[] f2297a;

    /* renamed from: b  reason: collision with root package name */
    final int f2298b;
    int c;
    int d = 64;
    private final int e;
    private int f;
    private int g;
    private int h;
    private int i;
    private int j = ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
    private int k = 67108864;
    private ev l;

    public static iw a(byte[] bArr, int i2) {
        return new iw(bArr, 0, i2);
    }

    public final int a() throws IOException {
        if (this.h == this.f) {
            this.i = 0;
            return 0;
        }
        this.i = d();
        int i2 = this.i;
        if (i2 != 0) {
            return i2;
        }
        throw new zzyh("Protocol message contained an invalid tag (zero).");
    }

    public final void a(int i2) throws zzyh {
        if (this.i != i2) {
            throw new zzyh("Protocol message end-group tag did not match expected tag.");
        }
    }

    public final boolean b(int i2) throws IOException {
        int a2;
        int i3 = i2 & 7;
        if (i3 == 0) {
            d();
            return true;
        } else if (i3 == 1) {
            g();
            return true;
        } else if (i3 == 2) {
            f(d());
            return true;
        } else if (i3 == 3) {
            do {
                a2 = a();
                if (a2 == 0) {
                    break;
                }
            } while (b(a2));
            a(((i2 >>> 3) << 3) | 4);
            return true;
        } else if (i3 == 4) {
            return false;
        } else {
            if (i3 == 5) {
                f();
                return true;
            }
            throw new zzyh("Protocol message tag had invalid wire type.");
        }
    }

    public final boolean b() throws IOException {
        return d() != 0;
    }

    public final String c() throws IOException {
        int d2 = d();
        if (d2 >= 0) {
            int i2 = this.f;
            int i3 = this.h;
            if (d2 <= i2 - i3) {
                String str = new String(this.f2297a, i3, d2, jd.f2311a);
                this.h += d2;
                return str;
            }
            throw zzyh.a();
        }
        throw zzyh.b();
    }

    public final void a(je jeVar) throws IOException {
        int d2 = d();
        if (this.c < this.d) {
            int c2 = c(d2);
            this.c++;
            jeVar.a(this);
            a(0);
            this.c--;
            d(c2);
            return;
        }
        throw zzyh.d();
    }

    public final int d() throws IOException {
        int i2;
        byte k2 = k();
        if (k2 >= 0) {
            return k2;
        }
        byte b2 = k2 & Byte.MAX_VALUE;
        byte k3 = k();
        if (k3 >= 0) {
            i2 = k3 << 7;
        } else {
            b2 |= (k3 & Byte.MAX_VALUE) << 7;
            byte k4 = k();
            if (k4 >= 0) {
                i2 = k4 << 14;
            } else {
                b2 |= (k4 & Byte.MAX_VALUE) << 14;
                byte k5 = k();
                if (k5 >= 0) {
                    i2 = k5 << 21;
                } else {
                    byte b3 = b2 | ((k5 & Byte.MAX_VALUE) << 21);
                    byte k6 = k();
                    byte b4 = b3 | (k6 << 28);
                    if (k6 >= 0) {
                        return b4;
                    }
                    for (int i3 = 0; i3 < 5; i3++) {
                        if (k() >= 0) {
                            return b4;
                        }
                    }
                    throw zzyh.c();
                }
            }
        }
        return b2 | i2;
    }

    public final long e() throws IOException {
        long j2 = 0;
        for (int i2 = 0; i2 < 64; i2 += 7) {
            byte k2 = k();
            j2 |= ((long) (k2 & Byte.MAX_VALUE)) << i2;
            if ((k2 & 128) == 0) {
                return j2;
            }
        }
        throw zzyh.c();
    }

    public final int f() throws IOException {
        return (k() & 255) | ((k() & 255) << 8) | ((k() & 255) << 16) | ((k() & 255) << 24);
    }

    public final long g() throws IOException {
        byte k2 = k();
        byte k3 = k();
        return ((((long) k3) & 255) << 8) | (((long) k2) & 255) | ((((long) k()) & 255) << 16) | ((((long) k()) & 255) << 24) | ((((long) k()) & 255) << 32) | ((((long) k()) & 255) << 40) | ((((long) k()) & 255) << 48) | ((((long) k()) & 255) << 56);
    }

    private iw(byte[] bArr, int i2, int i3) {
        this.f2297a = bArr;
        this.f2298b = 0;
        int i4 = i3 + 0;
        this.f = i4;
        this.e = i4;
        this.h = 0;
    }

    public final int c(int i2) throws zzyh {
        if (i2 >= 0) {
            int i3 = i2 + this.h;
            int i4 = this.j;
            if (i3 <= i4) {
                this.j = i3;
                j();
                return i4;
            }
            throw zzyh.a();
        }
        throw zzyh.b();
    }

    private final void j() {
        this.f += this.g;
        int i2 = this.f;
        int i3 = this.j;
        if (i2 > i3) {
            this.g = i2 - i3;
            this.f = i2 - this.g;
            return;
        }
        this.g = 0;
    }

    public final void d(int i2) {
        this.j = i2;
        j();
    }

    public final int h() {
        int i2 = this.j;
        if (i2 == Integer.MAX_VALUE) {
            return -1;
        }
        return i2 - this.h;
    }

    public final int i() {
        return this.h - this.f2298b;
    }

    public final void e(int i2) {
        a(i2, this.i);
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2, int i3) {
        int i4 = this.h;
        int i5 = this.f2298b;
        if (i2 > i4 - i5) {
            StringBuilder sb = new StringBuilder(50);
            sb.append("Position ");
            sb.append(i2);
            sb.append(" is beyond current ");
            sb.append(i4 - i5);
            throw new IllegalArgumentException(sb.toString());
        } else if (i2 >= 0) {
            this.h = i5 + i2;
            this.i = i3;
        } else {
            StringBuilder sb2 = new StringBuilder(24);
            sb2.append("Bad position ");
            sb2.append(i2);
            throw new IllegalArgumentException(sb2.toString());
        }
    }

    private final byte k() throws IOException {
        int i2 = this.h;
        if (i2 != this.f) {
            byte[] bArr = this.f2297a;
            this.h = i2 + 1;
            return bArr[i2];
        }
        throw zzyh.a();
    }

    private final void f(int i2) throws IOException {
        if (i2 >= 0) {
            int i3 = this.h;
            int i4 = i3 + i2;
            int i5 = this.j;
            if (i4 > i5) {
                f(i5 - i3);
                throw zzyh.a();
            } else if (i2 <= this.f - i3) {
                this.h = i3 + i2;
            } else {
                throw zzyh.a();
            }
        } else {
            throw zzyh.b();
        }
    }

    public final <T extends fq<T, ?>> T a(hd hdVar) throws IOException {
        try {
            if (this.l == null) {
                this.l = ev.a(this.f2297a, this.f2298b, this.e);
            }
            int u = this.l.u();
            int i2 = this.h - this.f2298b;
            if (u <= i2) {
                this.l.e(i2 - u);
                ev evVar = this.l;
                int i3 = this.d - this.c;
                if (i3 >= 0) {
                    int i4 = evVar.f2191b;
                    evVar.f2191b = i3;
                    T t = (fq) this.l.a(hdVar, fd.b());
                    b(this.i);
                    return t;
                }
                StringBuilder sb = new StringBuilder(47);
                sb.append("Recursion limit cannot be negative: ");
                sb.append(i3);
                throw new IllegalArgumentException(sb.toString());
            }
            throw new IOException(String.format("CodedInputStream read ahead of CodedInputByteBufferNano: %s > %s", Integer.valueOf(u), Integer.valueOf(i2)));
        } catch (zzuv e2) {
            throw new zzyh("", e2);
        }
    }
}
