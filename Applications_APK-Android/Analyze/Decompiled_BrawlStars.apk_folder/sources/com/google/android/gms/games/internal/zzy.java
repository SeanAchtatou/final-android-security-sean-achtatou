package com.google.android.gms.games.internal;

import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.common.data.DataHolder;

public interface zzy extends IInterface {
    Bundle a() throws RemoteException;

    void a(long j) throws RemoteException;

    void a(IBinder iBinder, Bundle bundle) throws RemoteException;

    void a(zzu zzu) throws RemoteException;

    void a(zzu zzu, String str, IBinder iBinder, Bundle bundle) throws RemoteException;

    void a(zzw zzw, long j) throws RemoteException;

    void a(String str, zzu zzu) throws RemoteException;

    void b() throws RemoteException;

    String c() throws RemoteException;

    DataHolder d() throws RemoteException;

    Intent e() throws RemoteException;

    Intent f() throws RemoteException;

    boolean g() throws RemoteException;
}
