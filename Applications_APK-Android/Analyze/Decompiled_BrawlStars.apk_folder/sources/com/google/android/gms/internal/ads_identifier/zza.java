package com.google.android.gms.internal.ads_identifier;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public class zza implements IInterface {

    /* renamed from: a  reason: collision with root package name */
    private final IBinder f1875a;

    /* renamed from: b  reason: collision with root package name */
    private final String f1876b;

    protected zza(IBinder iBinder, String str) {
        this.f1875a = iBinder;
        this.f1876b = str;
    }

    /* access modifiers changed from: protected */
    public final Parcel a(int i, Parcel parcel) throws RemoteException {
        parcel = Parcel.obtain();
        try {
            this.f1875a.transact(i, parcel, parcel, 0);
            parcel.readException();
            return parcel;
        } catch (RuntimeException e) {
            throw e;
        } finally {
            parcel.recycle();
        }
    }

    public IBinder asBinder() {
        return this.f1875a;
    }

    /* access modifiers changed from: protected */
    public final Parcel d_() {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(this.f1876b);
        return obtain;
    }
}
