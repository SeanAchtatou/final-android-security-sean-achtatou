package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;

final class ce implements ba {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ cc f1454a;

    private ce(cc ccVar) {
        this.f1454a = ccVar;
    }

    public final void a(Bundle bundle) {
        this.f1454a.g.lock();
        try {
            this.f1454a.e = ConnectionResult.f1352a;
            cc.a(this.f1454a);
        } finally {
            this.f1454a.g.unlock();
        }
    }

    public final void a(ConnectionResult connectionResult) {
        this.f1454a.g.lock();
        try {
            this.f1454a.e = connectionResult;
            cc.a(this.f1454a);
        } finally {
            this.f1454a.g.unlock();
        }
    }

    public final void a(int i, boolean z) {
        this.f1454a.g.lock();
        try {
            if (this.f1454a.f) {
                this.f1454a.f = false;
                cc.a(this.f1454a, i, z);
                return;
            }
            this.f1454a.f = true;
            this.f1454a.f1451a.onConnectionSuspended(i);
            this.f1454a.g.unlock();
        } finally {
            this.f1454a.g.unlock();
        }
    }

    /* synthetic */ ce(cc ccVar, byte b2) {
        this(ccVar);
    }
}
