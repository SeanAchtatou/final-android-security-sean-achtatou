package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import com.google.android.gms.common.api.internal.e;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.measurement.AppMeasurement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicReference;

public final class bv extends df {

    /* renamed from: a  reason: collision with root package name */
    protected cd f2442a;

    /* renamed from: b  reason: collision with root package name */
    public final AtomicReference<String> f2443b = new AtomicReference<>();
    protected boolean c = true;
    private bs d;
    private final Set<Object> e = new CopyOnWriteArraySet();
    private boolean f;

    protected bv(ar arVar) {
        super(arVar);
    }

    /* access modifiers changed from: protected */
    public final boolean t() {
        return false;
    }

    public final void a(boolean z) {
        D();
        p().a(new cc(this, z));
    }

    public final void a(String str, String str2, Bundle bundle) {
        a(str, str2, bundle, true, true, l().a());
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, String str2, long j, Bundle bundle) {
        c();
        a(str, str2, j, bundle, true, this.d == null || ed.e(str2), false, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.ed.a(java.lang.String, int, boolean):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.google.android.gms.measurement.internal.ed.a(int, java.lang.Object, boolean):java.lang.Object
      com.google.android.gms.measurement.internal.ed.a(android.os.Bundle, java.lang.String, java.lang.Object):void
      com.google.android.gms.measurement.internal.ed.a(java.lang.String, int, java.lang.String):boolean
      com.google.android.gms.measurement.internal.ed.a(java.lang.String, java.lang.String[], java.lang.String):boolean
      com.google.android.gms.measurement.internal.ed.a(java.lang.String, int, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.dj.a(long, boolean):void
     arg types: [long, int]
     candidates:
      com.google.android.gms.measurement.internal.dj.a(boolean, boolean):boolean
      com.google.android.gms.measurement.internal.dj.a(long, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.ch.a(com.google.android.gms.measurement.internal.cg, android.os.Bundle, boolean):void
     arg types: [com.google.android.gms.measurement.internal.cg, android.os.Bundle, int]
     candidates:
      com.google.android.gms.measurement.internal.ch.a(com.google.android.gms.measurement.internal.ch, com.google.android.gms.measurement.internal.cg, boolean):void
      com.google.android.gms.measurement.internal.ch.a(android.app.Activity, com.google.android.gms.measurement.internal.cg, boolean):void
      com.google.android.gms.measurement.internal.ch.a(com.google.android.gms.measurement.internal.cg, android.os.Bundle, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.dj.a(boolean, boolean):boolean
     arg types: [int, int]
     candidates:
      com.google.android.gms.measurement.internal.dj.a(long, boolean):void
      com.google.android.gms.measurement.internal.dj.a(boolean, boolean):boolean */
    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00ab  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.lang.String r31, java.lang.String r32, long r33, android.os.Bundle r35, boolean r36, boolean r37, boolean r38, java.lang.String r39) {
        /*
            r30 = this;
            r1 = r30
            r8 = r31
            r6 = r32
            r2 = r35
            r7 = r39
            com.google.android.gms.common.internal.l.a(r31)
            com.google.android.gms.measurement.internal.el r0 = r30.s()
            com.google.android.gms.measurement.internal.h$a<java.lang.Boolean> r3 = com.google.android.gms.measurement.internal.h.au
            boolean r0 = r0.c(r7, r3)
            if (r0 != 0) goto L_0x001c
            com.google.android.gms.common.internal.l.a(r32)
        L_0x001c:
            com.google.android.gms.common.internal.l.a(r35)
            r30.c()
            r30.D()
            com.google.android.gms.measurement.internal.ar r0 = r1.r
            boolean r0 = r0.r()
            if (r0 != 0) goto L_0x0039
            com.google.android.gms.measurement.internal.o r0 = r30.q()
            com.google.android.gms.measurement.internal.q r0 = r0.j
            java.lang.String r2 = "Event not sent since app measurement is disabled"
            r0.a(r2)
            return
        L_0x0039:
            boolean r0 = r1.f
            r3 = 0
            r16 = 0
            r5 = 1
            if (r0 != 0) goto L_0x0079
            r1.f = r5
            java.lang.String r0 = "com.google.android.gms.tagmanager.TagManagerService"
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException -> 0x006e }
            java.lang.String r4 = "initialize"
            java.lang.Class[] r9 = new java.lang.Class[r5]     // Catch:{ Exception -> 0x0061 }
            java.lang.Class<android.content.Context> r10 = android.content.Context.class
            r9[r16] = r10     // Catch:{ Exception -> 0x0061 }
            java.lang.reflect.Method r0 = r0.getDeclaredMethod(r4, r9)     // Catch:{ Exception -> 0x0061 }
            java.lang.Object[] r4 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x0061 }
            android.content.Context r9 = r30.m()     // Catch:{ Exception -> 0x0061 }
            r4[r16] = r9     // Catch:{ Exception -> 0x0061 }
            r0.invoke(r3, r4)     // Catch:{ Exception -> 0x0061 }
            goto L_0x0079
        L_0x0061:
            r0 = move-exception
            com.google.android.gms.measurement.internal.o r4 = r30.q()     // Catch:{ ClassNotFoundException -> 0x006e }
            com.google.android.gms.measurement.internal.q r4 = r4.f     // Catch:{ ClassNotFoundException -> 0x006e }
            java.lang.String r9 = "Failed to invoke Tag Manager's initialize() method"
            r4.a(r9, r0)     // Catch:{ ClassNotFoundException -> 0x006e }
            goto L_0x0079
        L_0x006e:
            com.google.android.gms.measurement.internal.o r0 = r30.q()
            com.google.android.gms.measurement.internal.q r0 = r0.i
            java.lang.String r4 = "Tag Manager is not found and thus will not be used"
            r0.a(r4)
        L_0x0079:
            r0 = 40
            r4 = 2
            if (r38 == 0) goto L_0x00dd
            java.lang.String r9 = "_iap"
            boolean r9 = r9.equals(r6)
            if (r9 != 0) goto L_0x00dd
            com.google.android.gms.measurement.internal.ar r9 = r1.r
            com.google.android.gms.measurement.internal.ed r9 = r9.e()
            java.lang.String r10 = "event"
            boolean r11 = r9.a(r10, r6)
            if (r11 != 0) goto L_0x0096
        L_0x0094:
            r9 = 2
            goto L_0x00a9
        L_0x0096:
            java.lang.String[] r11 = com.google.android.gms.measurement.internal.bp.f2432a
            boolean r11 = r9.a(r10, r11, r6)
            if (r11 != 0) goto L_0x00a1
            r9 = 13
            goto L_0x00a9
        L_0x00a1:
            boolean r9 = r9.a(r10, r0, r6)
            if (r9 != 0) goto L_0x00a8
            goto L_0x0094
        L_0x00a8:
            r9 = 0
        L_0x00a9:
            if (r9 == 0) goto L_0x00dd
            com.google.android.gms.measurement.internal.o r2 = r30.q()
            com.google.android.gms.measurement.internal.q r2 = r2.e
            com.google.android.gms.measurement.internal.m r3 = r30.n()
            java.lang.String r3 = r3.a(r6)
            java.lang.String r4 = "Invalid public event name. Event will not be logged (FE)"
            r2.a(r4, r3)
            com.google.android.gms.measurement.internal.ar r2 = r1.r
            r2.e()
            java.lang.String r0 = com.google.android.gms.measurement.internal.ed.a(r6, r0, r5)
            if (r6 == 0) goto L_0x00d0
            int r16 = r32.length()
            r2 = r16
            goto L_0x00d1
        L_0x00d0:
            r2 = 0
        L_0x00d1:
            com.google.android.gms.measurement.internal.ar r3 = r1.r
            com.google.android.gms.measurement.internal.ed r3 = r3.e()
            java.lang.String r4 = "_ev"
            r3.a(r9, r4, r0, r2)
            return
        L_0x00dd:
            com.google.android.gms.measurement.internal.ch r9 = r30.h()
            com.google.android.gms.measurement.internal.cg r15 = r9.v()
            java.lang.String r14 = "_sc"
            if (r15 == 0) goto L_0x00f1
            boolean r9 = r2.containsKey(r14)
            if (r9 != 0) goto L_0x00f1
            r15.d = r5
        L_0x00f1:
            if (r36 == 0) goto L_0x00f7
            if (r38 == 0) goto L_0x00f7
            r9 = 1
            goto L_0x00f8
        L_0x00f7:
            r9 = 0
        L_0x00f8:
            com.google.android.gms.measurement.internal.ch.a(r15, r2, r9)
            java.lang.String r9 = "am"
            boolean r17 = r9.equals(r8)
            boolean r9 = com.google.android.gms.measurement.internal.ed.e(r32)
            if (r36 == 0) goto L_0x012b
            com.google.android.gms.measurement.internal.bs r10 = r1.d
            if (r10 == 0) goto L_0x012b
            if (r9 != 0) goto L_0x012b
            if (r17 != 0) goto L_0x012b
            com.google.android.gms.measurement.internal.o r0 = r30.q()
            com.google.android.gms.measurement.internal.q r0 = r0.j
            com.google.android.gms.measurement.internal.m r3 = r30.n()
            java.lang.String r3 = r3.a(r6)
            com.google.android.gms.measurement.internal.m r4 = r30.n()
            java.lang.String r2 = r4.a(r2)
            java.lang.String r4 = "Passing event to registered event handler (FE)"
            r0.a(r4, r3, r2)
            return
        L_0x012b:
            com.google.android.gms.measurement.internal.ar r9 = r1.r
            boolean r9 = r9.u()
            if (r9 != 0) goto L_0x0134
            return
        L_0x0134:
            com.google.android.gms.measurement.internal.ed r9 = r30.o()
            int r9 = r9.b(r6)
            if (r9 == 0) goto L_0x0178
            com.google.android.gms.measurement.internal.o r2 = r30.q()
            com.google.android.gms.measurement.internal.q r2 = r2.e
            com.google.android.gms.measurement.internal.m r3 = r30.n()
            java.lang.String r3 = r3.a(r6)
            java.lang.String r4 = "Invalid event name. Event will not be logged (FE)"
            r2.a(r4, r3)
            r30.o()
            java.lang.String r0 = com.google.android.gms.measurement.internal.ed.a(r6, r0, r5)
            if (r6 == 0) goto L_0x0160
            int r2 = r32.length()
            r16 = r2
        L_0x0160:
            com.google.android.gms.measurement.internal.ar r2 = r1.r
            com.google.android.gms.measurement.internal.ed r2 = r2.e()
            java.lang.String r3 = "_ev"
            r31 = r2
            r32 = r39
            r33 = r9
            r34 = r3
            r35 = r0
            r36 = r16
            r31.a(r32, r33, r34, r35, r36)
            return
        L_0x0178:
            r0 = 4
            java.lang.String[] r0 = new java.lang.String[r0]
            java.lang.String r13 = "_o"
            r0[r16] = r13
            java.lang.String r12 = "_sn"
            r0[r5] = r12
            r0[r4] = r14
            r4 = 3
            java.lang.String r11 = "_si"
            r0[r4] = r11
            java.util.List r0 = java.util.Arrays.asList(r0)
            java.util.List r0 = java.util.Collections.unmodifiableList(r0)
            com.google.android.gms.measurement.internal.ed r9 = r30.o()
            r4 = 1
            r10 = r39
            r3 = r11
            r11 = r32
            r5 = r12
            r12 = r35
            r20 = r13
            r13 = r0
            r2 = r14
            r14 = r38
            r21 = r15
            r15 = r4
            android.os.Bundle r4 = r9.a(r10, r11, r12, r13, r14, r15)
            if (r4 == 0) goto L_0x01d6
            boolean r9 = r4.containsKey(r2)
            if (r9 == 0) goto L_0x01d6
            boolean r9 = r4.containsKey(r3)
            if (r9 != 0) goto L_0x01bb
            goto L_0x01d6
        L_0x01bb:
            java.lang.String r5 = r4.getString(r5)
            java.lang.String r2 = r4.getString(r2)
            long r9 = r4.getLong(r3)
            java.lang.Long r3 = java.lang.Long.valueOf(r9)
            com.google.android.gms.measurement.internal.cg r9 = new com.google.android.gms.measurement.internal.cg
            long r10 = r3.longValue()
            r9.<init>(r5, r2, r10)
            r15 = r9
            goto L_0x01d7
        L_0x01d6:
            r15 = 0
        L_0x01d7:
            if (r15 != 0) goto L_0x01dc
            r2 = r21
            goto L_0x01dd
        L_0x01dc:
            r2 = r15
        L_0x01dd:
            com.google.android.gms.measurement.internal.el r3 = r30.s()
            boolean r3 = r3.l(r7)
            java.lang.String r5 = "_ae"
            r9 = 0
            if (r3 == 0) goto L_0x020e
            com.google.android.gms.measurement.internal.ch r3 = r30.h()
            com.google.android.gms.measurement.internal.cg r3 = r3.v()
            if (r3 == 0) goto L_0x020e
            boolean r3 = r5.equals(r6)
            if (r3 == 0) goto L_0x020e
            com.google.android.gms.measurement.internal.dj r3 = r30.j()
            long r11 = r3.w()
            int r3 = (r11 > r9 ? 1 : (r11 == r9 ? 0 : -1))
            if (r3 <= 0) goto L_0x020e
            com.google.android.gms.measurement.internal.ed r3 = r30.o()
            r3.a(r4, r11)
        L_0x020e:
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            r3.add(r4)
            com.google.android.gms.measurement.internal.ed r11 = r30.o()
            java.security.SecureRandom r11 = r11.g()
            long r14 = r11.nextLong()
            com.google.android.gms.measurement.internal.el r11 = r30.s()
            com.google.android.gms.measurement.internal.i r12 = r30.f()
            java.lang.String r12 = r12.v()
            boolean r11 = r11.k(r12)
            if (r11 == 0) goto L_0x0258
            java.lang.String r11 = "extend_session"
            long r9 = r4.getLong(r11, r9)
            r11 = 1
            int r13 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
            if (r13 != 0) goto L_0x0258
            com.google.android.gms.measurement.internal.o r9 = r30.q()
            com.google.android.gms.measurement.internal.q r9 = r9.k
            java.lang.String r10 = "EXTEND_SESSION param attached: initiate a new session or extend the current active session"
            r9.a(r10)
            com.google.android.gms.measurement.internal.ar r9 = r1.r
            com.google.android.gms.measurement.internal.dj r9 = r9.c()
            r12 = r33
            r10 = 1
            r9.a(r12, r10)
            goto L_0x025a
        L_0x0258:
            r12 = r33
        L_0x025a:
            java.util.Set r9 = r4.keySet()
            int r10 = r35.size()
            java.lang.String[] r10 = new java.lang.String[r10]
            java.lang.Object[] r9 = r9.toArray(r10)
            r11 = r9
            java.lang.String[] r11 = (java.lang.String[]) r11
            java.util.Arrays.sort(r11)
            int r10 = r11.length
            r23 = r14
            r9 = 0
            r22 = 0
        L_0x0274:
            java.lang.String r15 = "_eid"
            if (r9 >= r10) goto L_0x033e
            r14 = r11[r9]
            java.lang.Object r18 = r4.get(r14)
            r30.o()
            r35 = r15
            android.os.Bundle[] r15 = com.google.android.gms.measurement.internal.ed.a(r18)
            r36 = r5
            if (r15 == 0) goto L_0x0312
            int r5 = r15.length
            r4.putInt(r14, r5)
            r5 = 0
        L_0x0290:
            int r7 = r15.length
            if (r5 >= r7) goto L_0x02fb
            r7 = r15[r5]
            r18 = r15
            r15 = 1
            com.google.android.gms.measurement.internal.ch.a(r2, r7, r15)
            com.google.android.gms.measurement.internal.ed r19 = r30.o()
            r21 = 0
            java.lang.String r25 = "_ep"
            r26 = r9
            r9 = r19
            r19 = r10
            r10 = r39
            r27 = r11
            r11 = r25
            r12 = r7
            r13 = r0
            r7 = r0
            r0 = r23
            r23 = r2
            r2 = r14
            r14 = r38
            r8 = r35
            r24 = r7
            r7 = r18
            r18 = 1
            r15 = r21
            android.os.Bundle r9 = r9.a(r10, r11, r12, r13, r14, r15)
            java.lang.String r10 = "_en"
            r9.putString(r10, r6)
            r9.putLong(r8, r0)
            java.lang.String r10 = "_gn"
            r9.putString(r10, r2)
            int r10 = r7.length
            java.lang.String r11 = "_ll"
            r9.putInt(r11, r10)
            java.lang.String r10 = "_i"
            r9.putInt(r10, r5)
            r3.add(r9)
            int r5 = r5 + 1
            r12 = r33
            r14 = r2
            r15 = r7
            r10 = r19
            r2 = r23
            r9 = r26
            r11 = r27
            r8 = r31
            r28 = r0
            r1 = r30
            r0 = r24
            r23 = r28
            goto L_0x0290
        L_0x02fb:
            r26 = r9
            r19 = r10
            r27 = r11
            r7 = r15
            r18 = 1
            r28 = r23
            r24 = r0
            r23 = r2
            r0 = r28
            int r2 = r7.length
            r5 = r22
            int r22 = r5 + r2
            goto L_0x0324
        L_0x0312:
            r26 = r9
            r19 = r10
            r27 = r11
            r5 = r22
            r18 = 1
            r28 = r23
            r24 = r0
            r23 = r2
            r0 = r28
        L_0x0324:
            int r9 = r26 + 1
            r8 = r31
            r12 = r33
            r5 = r36
            r7 = r39
            r10 = r19
            r2 = r23
            r11 = r27
            r28 = r0
            r1 = r30
            r0 = r24
            r23 = r28
            goto L_0x0274
        L_0x033e:
            r36 = r5
            r8 = r15
            r5 = r22
            r0 = r23
            r18 = 1
            if (r5 == 0) goto L_0x0351
            r4.putLong(r8, r0)
            java.lang.String r0 = "_epc"
            r4.putInt(r0, r5)
        L_0x0351:
            r0 = 0
        L_0x0352:
            int r1 = r3.size()
            if (r0 >= r1) goto L_0x03dc
            java.lang.Object r1 = r3.get(r0)
            android.os.Bundle r1 = (android.os.Bundle) r1
            if (r0 == 0) goto L_0x0362
            r2 = 1
            goto L_0x0363
        L_0x0362:
            r2 = 0
        L_0x0363:
            if (r2 == 0) goto L_0x036b
            java.lang.String r2 = "_ep"
            r8 = r31
            r4 = r2
            goto L_0x036e
        L_0x036b:
            r8 = r31
            r4 = r6
        L_0x036e:
            r9 = r20
            r1.putString(r9, r8)
            if (r37 == 0) goto L_0x037d
            com.google.android.gms.measurement.internal.ed r2 = r30.o()
            android.os.Bundle r1 = r2.a(r1)
        L_0x037d:
            com.google.android.gms.measurement.internal.o r2 = r30.q()
            com.google.android.gms.measurement.internal.q r2 = r2.j
            com.google.android.gms.measurement.internal.m r5 = r30.n()
            java.lang.String r5 = r5.a(r6)
            com.google.android.gms.measurement.internal.m r7 = r30.n()
            java.lang.String r7 = r7.a(r1)
            java.lang.String r10 = "Logging event (FE)"
            r2.a(r10, r5, r7)
            com.google.android.gms.measurement.internal.zzag r10 = new com.google.android.gms.measurement.internal.zzag
            com.google.android.gms.measurement.internal.zzad r5 = new com.google.android.gms.measurement.internal.zzad
            r5.<init>(r1)
            r2 = r10
            r11 = r3
            r3 = r4
            r4 = r5
            r12 = r36
            r13 = 1
            r5 = r31
            r15 = r39
            r14 = r6
            r6 = r33
            r2.<init>(r3, r4, r5, r6)
            com.google.android.gms.measurement.internal.cl r2 = r30.g()
            r2.a(r10, r15)
            r2 = r30
            if (r17 != 0) goto L_0x03d0
            java.util.Set<java.lang.Object> r3 = r2.e
            java.util.Iterator r3 = r3.iterator()
        L_0x03c1:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x03d0
            r3.next()
            android.os.Bundle r4 = new android.os.Bundle
            r4.<init>(r1)
            goto L_0x03c1
        L_0x03d0:
            int r0 = r0 + 1
            r20 = r9
            r3 = r11
            r36 = r12
            r6 = r14
            r18 = 1
            goto L_0x0352
        L_0x03dc:
            r13 = 1
            r2 = r30
            r12 = r36
            r14 = r6
            com.google.android.gms.measurement.internal.ch r0 = r30.h()
            com.google.android.gms.measurement.internal.cg r0 = r0.v()
            if (r0 == 0) goto L_0x03f9
            boolean r0 = r12.equals(r14)
            if (r0 == 0) goto L_0x03f9
            com.google.android.gms.measurement.internal.dj r0 = r30.j()
            r0.a(r13, r13)
        L_0x03f9:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.bv.a(java.lang.String, java.lang.String, long, android.os.Bundle, boolean, boolean, boolean, java.lang.String):void");
    }

    private void a(String str, String str2, Bundle bundle, boolean z, boolean z2, long j) {
        bv bvVar;
        Bundle bundle2;
        String str3 = str == null ? "app" : str;
        if (bundle == null) {
            bvVar = this;
            bundle2 = new Bundle();
        } else {
            bvVar = this;
            bundle2 = bundle;
        }
        b(str3, str2, j, bundle2, true, bvVar.d == null || ed.e(str2), false, null);
    }

    private final void b(String str, String str2, long j, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        p().a(new bw(this, str, str2, j, ed.b(bundle), z, z2, z3, null));
    }

    public final void a(String str, String str2, Object obj, boolean z) {
        a(str, str2, obj, true, l().a());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.ed.a(java.lang.String, int, boolean):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.google.android.gms.measurement.internal.ed.a(int, java.lang.Object, boolean):java.lang.Object
      com.google.android.gms.measurement.internal.ed.a(android.os.Bundle, java.lang.String, java.lang.Object):void
      com.google.android.gms.measurement.internal.ed.a(java.lang.String, int, java.lang.String):boolean
      com.google.android.gms.measurement.internal.ed.a(java.lang.String, java.lang.String[], java.lang.String):boolean
      com.google.android.gms.measurement.internal.ed.a(java.lang.String, int, boolean):java.lang.String */
    private void a(String str, String str2, Object obj, boolean z, long j) {
        if (str == null) {
            str = "app";
        }
        String str3 = str;
        int i = 6;
        int i2 = 0;
        if (z) {
            i = o().c(str2);
        } else {
            ed o = o();
            if (o.a("user property", str2)) {
                if (!o.a("user property", br.f2436a, str2)) {
                    i = 15;
                } else if (o.a("user property", 24, str2)) {
                    i = 0;
                }
            }
        }
        if (i != 0) {
            o();
            String a2 = ed.a(str2, 24, true);
            if (str2 != null) {
                i2 = str2.length();
            }
            this.r.e().a(i, "_ev", a2, i2);
        } else if (obj != null) {
            int b2 = o().b(str2, obj);
            if (b2 != 0) {
                o();
                String a3 = ed.a(str2, 24, true);
                if ((obj instanceof String) || (obj instanceof CharSequence)) {
                    i2 = String.valueOf(obj).length();
                }
                this.r.e().a(b2, "_ev", a3, i2);
                return;
            }
            o();
            Object c2 = ed.c(str2, obj);
            if (c2 != null) {
                a(str3, str2, j, c2);
            }
        } else {
            a(str3, str2, j, (Object) null);
        }
    }

    private final void a(String str, String str2, long j, Object obj) {
        p().a(new bx(this, str, str2, obj, j));
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, String str2, Object obj, long j) {
        l.a(str);
        l.a(str2);
        c();
        D();
        if (!this.r.r()) {
            q().j.a("User property not set since app measurement is disabled");
        } else if (this.r.u()) {
            q().j.a("Setting user property (FE)", n().a(str2), obj);
            g().a(new zzfv(str2, j, obj, str));
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        this.f2443b.set(str);
    }

    public final void v() {
        c();
        D();
        if (this.r.u()) {
            g().x();
            this.c = false;
            String u = r().u();
            if (!TextUtils.isEmpty(u)) {
                k().w();
                if (!u.equals(Build.VERSION.RELEASE)) {
                    Bundle bundle = new Bundle();
                    bundle.putString("_po", u);
                    a("auto", "_ou", bundle);
                }
            }
        }
    }

    public final void a(AppMeasurement.ConditionalUserProperty conditionalUserProperty) {
        long a2 = l().a();
        l.a(conditionalUserProperty);
        l.a(conditionalUserProperty.mName);
        l.a(conditionalUserProperty.mOrigin);
        l.a(conditionalUserProperty.mValue);
        conditionalUserProperty.mCreationTimestamp = a2;
        String str = conditionalUserProperty.mName;
        Object obj = conditionalUserProperty.mValue;
        if (o().c(str) != 0) {
            q().c.a("Invalid conditional user property name", n().c(str));
        } else if (o().b(str, obj) != 0) {
            q().c.a("Invalid conditional user property value", n().c(str), obj);
        } else {
            o();
            Object c2 = ed.c(str, obj);
            if (c2 == null) {
                q().c.a("Unable to normalize conditional user property value", n().c(str), obj);
                return;
            }
            conditionalUserProperty.mValue = c2;
            long j = conditionalUserProperty.mTriggerTimeout;
            if (TextUtils.isEmpty(conditionalUserProperty.mTriggerEventName) || (j <= 15552000000L && j >= 1)) {
                long j2 = conditionalUserProperty.mTimeToLive;
                if (j2 > 15552000000L || j2 < 1) {
                    q().c.a("Invalid conditional user property time to live", n().c(str), Long.valueOf(j2));
                } else {
                    p().a(new by(this, conditionalUserProperty));
                }
            } else {
                q().c.a("Invalid conditional user property timeout", n().c(str), Long.valueOf(j));
            }
        }
    }

    public final void a(String str, String str2, String str3, Bundle bundle) {
        long a2 = l().a();
        l.a(str2);
        AppMeasurement.ConditionalUserProperty conditionalUserProperty = new AppMeasurement.ConditionalUserProperty();
        conditionalUserProperty.mAppId = str;
        conditionalUserProperty.mName = str2;
        conditionalUserProperty.mCreationTimestamp = a2;
        if (str3 != null) {
            conditionalUserProperty.mExpiredEventName = str3;
            conditionalUserProperty.mExpiredEventParams = bundle;
        }
        p().a(new bz(this, conditionalUserProperty));
    }

    public final List<AppMeasurement.ConditionalUserProperty> a(String str, String str2, String str3) {
        if (p().f()) {
            q().c.a("Cannot get conditional user properties from analytics worker thread");
            return Collections.emptyList();
        } else if (ej.a()) {
            q().c.a("Cannot get conditional user properties from main thread");
            return Collections.emptyList();
        } else {
            AtomicReference atomicReference = new AtomicReference();
            synchronized (atomicReference) {
                this.r.p().a(new ca(this, atomicReference, str, str2, str3));
                try {
                    atomicReference.wait(5000);
                } catch (InterruptedException e2) {
                    q().f.a("Interrupted waiting for get conditional user properties", str, e2);
                }
            }
            List<zzo> list = (List) atomicReference.get();
            if (list == null) {
                q().f.a("Timed out waiting for get conditional user properties", str);
                return Collections.emptyList();
            }
            ArrayList arrayList = new ArrayList(list.size());
            for (zzo zzo : list) {
                AppMeasurement.ConditionalUserProperty conditionalUserProperty = new AppMeasurement.ConditionalUserProperty();
                conditionalUserProperty.mAppId = zzo.f2603a;
                conditionalUserProperty.mOrigin = zzo.f2604b;
                conditionalUserProperty.mCreationTimestamp = zzo.d;
                conditionalUserProperty.mName = zzo.c.f2599a;
                conditionalUserProperty.mValue = zzo.c.a();
                conditionalUserProperty.mActive = zzo.e;
                conditionalUserProperty.mTriggerEventName = zzo.f;
                if (zzo.g != null) {
                    conditionalUserProperty.mTimedOutEventName = zzo.g.f2595a;
                    if (zzo.g.f2596b != null) {
                        conditionalUserProperty.mTimedOutEventParams = zzo.g.f2596b.a();
                    }
                }
                conditionalUserProperty.mTriggerTimeout = zzo.h;
                if (zzo.i != null) {
                    conditionalUserProperty.mTriggeredEventName = zzo.i.f2595a;
                    if (zzo.i.f2596b != null) {
                        conditionalUserProperty.mTriggeredEventParams = zzo.i.f2596b.a();
                    }
                }
                conditionalUserProperty.mTriggeredTimestamp = zzo.c.f2600b;
                conditionalUserProperty.mTimeToLive = zzo.j;
                if (zzo.k != null) {
                    conditionalUserProperty.mExpiredEventName = zzo.k.f2595a;
                    if (zzo.k.f2596b != null) {
                        conditionalUserProperty.mExpiredEventParams = zzo.k.f2596b.a();
                    }
                }
                arrayList.add(conditionalUserProperty);
            }
            return arrayList;
        }
    }

    public final Map<String, Object> a(String str, String str2, String str3, boolean z) {
        if (p().f()) {
            q().c.a("Cannot get user properties from analytics worker thread");
            return Collections.emptyMap();
        } else if (ej.a()) {
            q().c.a("Cannot get user properties from main thread");
            return Collections.emptyMap();
        } else {
            AtomicReference atomicReference = new AtomicReference();
            synchronized (atomicReference) {
                this.r.p().a(new cb(this, atomicReference, str, str2, str3, z));
                try {
                    atomicReference.wait(5000);
                } catch (InterruptedException e2) {
                    q().f.a("Interrupted waiting for get user properties", e2);
                }
            }
            List<zzfv> list = (List) atomicReference.get();
            if (list == null) {
                q().f.a("Timed out waiting for get user properties");
                return Collections.emptyMap();
            }
            ArrayMap arrayMap = new ArrayMap(list.size());
            for (zzfv zzfv : list) {
                arrayMap.put(zzfv.f2599a, zzfv.a());
            }
            return arrayMap;
        }
    }

    public final String w() {
        cg cgVar = this.r.h().f2464b;
        if (cgVar != null) {
            return cgVar.f2461a;
        }
        return null;
    }

    public final String x() {
        cg cgVar = this.r.h().f2464b;
        if (cgVar != null) {
            return cgVar.f2462b;
        }
        return null;
    }

    public final String y() {
        if (this.r.f2387a != null) {
            return this.r.f2387a;
        }
        try {
            return e.a();
        } catch (IllegalStateException e2) {
            this.r.q().c.a("getGoogleAppId failed with exception", e2);
            return null;
        }
    }

    public final /* bridge */ /* synthetic */ void a() {
        super.a();
    }

    public final /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    public final /* bridge */ /* synthetic */ void c() {
        super.c();
    }

    public final /* bridge */ /* synthetic */ a d() {
        return super.d();
    }

    public final /* bridge */ /* synthetic */ bv e() {
        return super.e();
    }

    public final /* bridge */ /* synthetic */ i f() {
        return super.f();
    }

    public final /* bridge */ /* synthetic */ cl g() {
        return super.g();
    }

    public final /* bridge */ /* synthetic */ ch h() {
        return super.h();
    }

    public final /* bridge */ /* synthetic */ k i() {
        return super.i();
    }

    public final /* bridge */ /* synthetic */ dj j() {
        return super.j();
    }

    public final /* bridge */ /* synthetic */ b k() {
        return super.k();
    }

    public final /* bridge */ /* synthetic */ com.google.android.gms.common.util.e l() {
        return super.l();
    }

    public final /* bridge */ /* synthetic */ Context m() {
        return super.m();
    }

    public final /* bridge */ /* synthetic */ m n() {
        return super.n();
    }

    public final /* bridge */ /* synthetic */ ed o() {
        return super.o();
    }

    public final /* bridge */ /* synthetic */ am p() {
        return super.p();
    }

    public final /* bridge */ /* synthetic */ o q() {
        return super.q();
    }

    public final /* bridge */ /* synthetic */ z r() {
        return super.r();
    }

    public final /* bridge */ /* synthetic */ el s() {
        return super.s();
    }

    static /* synthetic */ void a(bv bvVar) {
        if (!bvVar.s().e(bvVar.f().v()) || !bvVar.r.r() || !bvVar.c) {
            bvVar.q().j.a("Updating Scion state (FE)");
            bvVar.g().w();
            return;
        }
        bvVar.q().j.a("Recording app launch after enabling measurement for the first time (FE)");
        bvVar.v();
    }

    static /* synthetic */ void a(bv bvVar, AppMeasurement.ConditionalUserProperty conditionalUserProperty) {
        AppMeasurement.ConditionalUserProperty conditionalUserProperty2 = conditionalUserProperty;
        bvVar.c();
        bvVar.D();
        l.a(conditionalUserProperty);
        l.a(conditionalUserProperty2.mName);
        l.a(conditionalUserProperty2.mOrigin);
        l.a(conditionalUserProperty2.mValue);
        if (!bvVar.r.r()) {
            bvVar.q().j.a("Conditional property not sent since collection is disabled");
            return;
        }
        zzfv zzfv = new zzfv(conditionalUserProperty2.mName, conditionalUserProperty2.mTriggeredTimestamp, conditionalUserProperty2.mValue, conditionalUserProperty2.mOrigin);
        try {
            zzag a2 = bvVar.o().a(conditionalUserProperty2.mAppId, conditionalUserProperty2.mTriggeredEventName, conditionalUserProperty2.mTriggeredEventParams, conditionalUserProperty2.mOrigin, 0);
            zzag a3 = bvVar.o().a(conditionalUserProperty2.mAppId, conditionalUserProperty2.mTimedOutEventName, conditionalUserProperty2.mTimedOutEventParams, conditionalUserProperty2.mOrigin, 0);
            zzag a4 = bvVar.o().a(conditionalUserProperty2.mAppId, conditionalUserProperty2.mExpiredEventName, conditionalUserProperty2.mExpiredEventParams, conditionalUserProperty2.mOrigin, 0);
            zzo zzo = r2;
            zzo zzo2 = new zzo(conditionalUserProperty2.mAppId, conditionalUserProperty2.mOrigin, zzfv, conditionalUserProperty2.mCreationTimestamp, false, conditionalUserProperty2.mTriggerEventName, a3, conditionalUserProperty2.mTriggerTimeout, a2, conditionalUserProperty2.mTimeToLive, a4);
            bvVar.g().a(zzo);
        } catch (IllegalArgumentException unused) {
        }
    }

    static /* synthetic */ void b(bv bvVar, AppMeasurement.ConditionalUserProperty conditionalUserProperty) {
        AppMeasurement.ConditionalUserProperty conditionalUserProperty2 = conditionalUserProperty;
        bvVar.c();
        bvVar.D();
        l.a(conditionalUserProperty);
        l.a(conditionalUserProperty2.mName);
        if (!bvVar.r.r()) {
            bvVar.q().j.a("Conditional property not cleared since collection is disabled");
            return;
        }
        zzfv zzfv = new zzfv(conditionalUserProperty2.mName, 0, null, null);
        try {
            zzag a2 = bvVar.o().a(conditionalUserProperty2.mAppId, conditionalUserProperty2.mExpiredEventName, conditionalUserProperty2.mExpiredEventParams, conditionalUserProperty2.mOrigin, conditionalUserProperty2.mCreationTimestamp);
            String str = conditionalUserProperty2.mAppId;
            String str2 = conditionalUserProperty2.mOrigin;
            long j = conditionalUserProperty2.mCreationTimestamp;
            boolean z = conditionalUserProperty2.mActive;
            String str3 = conditionalUserProperty2.mTriggerEventName;
            long j2 = conditionalUserProperty2.mTimeToLive;
            zzo zzo = r2;
            zzo zzo2 = new zzo(str, str2, zzfv, j, z, str3, null, conditionalUserProperty2.mTriggerTimeout, null, j2, a2);
            bvVar.g().a(zzo);
        } catch (IllegalArgumentException unused) {
        }
    }
}
