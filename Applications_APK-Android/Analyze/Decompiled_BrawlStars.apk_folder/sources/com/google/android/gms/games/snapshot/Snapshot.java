package com.google.android.gms.games.snapshot;

import android.os.Parcelable;
import com.google.android.gms.common.data.e;

public interface Snapshot extends Parcelable, e<Snapshot> {
    SnapshotMetadata b();

    SnapshotContents c();
}
