package com.google.android.gms.games.multiplayer;

import android.net.Uri;
import android.os.Parcelable;
import com.google.android.gms.common.data.e;
import com.google.android.gms.games.Player;

public interface Participant extends Parcelable, e<Participant> {
    int b();

    String c();

    int d();

    boolean e();

    String f();

    Uri g();

    @Deprecated
    String getHiResImageUrl();

    @Deprecated
    String getIconImageUrl();

    Uri h();

    String i();

    Player j();

    ParticipantResult k();
}
