package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.d;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class q extends d {

    /* renamed from: a  reason: collision with root package name */
    private final String f1491a;

    public q(String str) {
        this.f1491a = str;
    }

    public final boolean b(a<?> aVar) {
        throw new UnsupportedOperationException(this.f1491a);
    }

    public final void b() {
        throw new UnsupportedOperationException(this.f1491a);
    }

    public final void c() {
        throw new UnsupportedOperationException(this.f1491a);
    }

    public final void d() {
        throw new UnsupportedOperationException(this.f1491a);
    }

    public final boolean e() {
        throw new UnsupportedOperationException(this.f1491a);
    }

    public final boolean f() {
        throw new UnsupportedOperationException(this.f1491a);
    }

    public final void a(d.c cVar) {
        throw new UnsupportedOperationException(this.f1491a);
    }

    public final void b(d.c cVar) {
        throw new UnsupportedOperationException(this.f1491a);
    }

    public final void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        throw new UnsupportedOperationException(this.f1491a);
    }
}
