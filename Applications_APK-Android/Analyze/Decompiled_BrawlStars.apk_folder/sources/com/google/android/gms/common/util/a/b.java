package com.google.android.gms.common.util.a;

import com.google.android.gms.common.internal.l;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

public class b implements ThreadFactory {

    /* renamed from: a  reason: collision with root package name */
    private final String f1667a;

    /* renamed from: b  reason: collision with root package name */
    private final int f1668b;
    private final ThreadFactory c;

    public b(String str) {
        this(str, (byte) 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.l.a(java.lang.Object, java.lang.Object):T
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.l.a(java.lang.String, java.lang.Object):java.lang.String
      com.google.android.gms.common.internal.l.a(boolean, java.lang.Object):void
      com.google.android.gms.common.internal.l.a(java.lang.Object, java.lang.Object):T */
    private b(String str, byte b2) {
        this.c = Executors.defaultThreadFactory();
        this.f1667a = (String) l.a((Object) str, (Object) "Name must not be null");
        this.f1668b = 0;
    }

    public Thread newThread(Runnable runnable) {
        Thread newThread = this.c.newThread(new d(runnable, 0));
        newThread.setName(this.f1667a);
        return newThread;
    }
}
