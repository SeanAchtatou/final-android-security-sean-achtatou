package com.google.android.gms.measurement.internal;

import java.util.List;
import java.util.concurrent.Callable;

final class az implements Callable<List<ec>> {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zzk f2403a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f2404b;
    private final /* synthetic */ String c;
    private final /* synthetic */ zzby d;

    az(zzby zzby, zzk zzk, String str, String str2) {
        this.d = zzby;
        this.f2403a = zzk;
        this.f2404b = str;
        this.c = str2;
    }

    public final /* synthetic */ Object call() throws Exception {
        this.d.f2597a.k();
        return this.d.f2597a.d().a(this.f2403a.f2601a, this.f2404b, this.c);
    }
}
