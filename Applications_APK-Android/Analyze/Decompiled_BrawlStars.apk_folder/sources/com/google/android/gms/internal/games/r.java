package com.google.android.gms.internal.games;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class r {

    /* renamed from: a  reason: collision with root package name */
    private final Object f1991a;

    /* renamed from: b  reason: collision with root package name */
    private HashMap<String, AtomicInteger> f1992b;

    public final void a() {
        synchronized (this.f1991a) {
            for (Map.Entry next : this.f1992b.entrySet()) {
                next.getKey();
                ((AtomicInteger) next.getValue()).get();
            }
            this.f1992b.clear();
        }
    }
}
