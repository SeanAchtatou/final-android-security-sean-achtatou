package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.support.v7.widget.helper.ItemTouchHelper;
import com.google.android.gms.common.d;
import com.google.android.gms.common.f;
import com.google.android.gms.internal.measurement.dh;
import com.google.android.gms.internal.measurement.dr;
import com.google.android.gms.internal.measurement.ds;
import com.google.android.gms.internal.measurement.dy;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class h {
    public static a<Long> A = a.a("measurement.upload.window_interval", 3600000L, 3600000L);
    public static a<Long> B = a.a("measurement.upload.interval", 3600000L, 3600000L);
    public static a<Long> C = a.a("measurement.upload.realtime_upload_interval", 10000L, 10000L);
    public static a<Long> D = a.a("measurement.upload.debug_upload_interval", 1000L, 1000L);
    public static a<Long> E = a.a("measurement.upload.minimum_delay", 500L, 500L);
    public static a<Long> F = a.a("measurement.alarm_manager.minimum_interval", 60000L, 60000L);
    public static a<Long> G = a.a("measurement.upload.stale_data_deletion_interval", 86400000L, 86400000L);
    public static a<Long> H = a.a("measurement.upload.refresh_blacklisted_config_interval", 604800000L, 604800000L);
    public static a<Long> I = a.a("measurement.upload.initial_upload_delay_time", 15000L, 15000L);
    public static a<Long> J = a.a("measurement.upload.retry_time", 1800000L, 1800000L);
    public static a<Integer> K = a.a("measurement.upload.retry_count", 6, 6);
    public static a<Long> L = a.a("measurement.upload.max_queue_time", 2419200000L, 2419200000L);
    public static a<Integer> M = a.a("measurement.lifetimevalue.max_currency_tracked", 4, 4);
    public static a<Integer> N = a.a("measurement.audience.filter_result_max_count", (int) ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION, (int) ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION);
    public static a<Long> O = a.a("measurement.service_client.idle_disconnect_millis", 5000L, 5000L);
    public static a<Boolean> P = a.a("measurement.test.boolean_flag", false, false);
    public static a<String> Q = a.a("measurement.test.string_flag", "---", "---");
    public static a<Long> R = a.a("measurement.test.long_flag", -1L, -1L);
    public static a<Integer> S = a.a("measurement.test.int_flag", -2, -2);
    public static a<Double> T = a.a("measurement.test.double_flag");
    public static a<Integer> U = a.a("measurement.experiment.max_ids", 50, 50);
    public static a<Boolean> V = a.a("measurement.lifetimevalue.user_engagement_tracking_enabled", true, true);
    public static a<Boolean> W = a.a("measurement.audience.complex_param_evaluation", true, true);
    public static a<Boolean> X = a.a("measurement.validation.internal_limits_internal_event_params", false, false);
    public static a<Boolean> Y = a.a("measurement.quality.unsuccessful_update_retry_counter", true, true);
    public static a<Boolean> Z = a.a("measurement.iid.disable_on_collection_disabled", true, true);

    /* renamed from: a  reason: collision with root package name */
    static ej f2561a;
    private static a<Boolean> aA = a.a("measurement.log_upgrades_enabled", false, false);
    public static a<Boolean> aa = a.a("measurement.app_launch.call_only_when_enabled", true, true);
    public static a<Boolean> ab = a.a("measurement.run_on_worker_inline", true, false);
    public static a<Boolean> ac = a.a("measurement.audience.dynamic_filters", true, true);
    public static a<Boolean> ad = a.a("measurement.reset_analytics.persist_time", false, false);
    public static a<Boolean> ae = a.a("measurement.validation.value_and_currency_params", false, false);
    public static a<Boolean> af = a.a("measurement.sampling.time_zone_offset_enabled", false, false);
    public static a<Boolean> ag = a.a("measurement.referrer.enable_logging_install_referrer_cmp_from_apk", false, false);
    public static a<Boolean> ah = a.a("measurement.fetch_config_with_admob_app_id", true, true);
    public static a<Boolean> ai = a.a("measurement.sessions.session_id_enabled", false, false);
    public static a<Boolean> aj = a.a("measurement.sessions.session_number_enabled", false, false);
    public static a<Boolean> ak = a.a("measurement.sessions.immediate_start_enabled", false, false);
    public static a<Boolean> al = a.a("measurement.sessions.background_sessions_enabled", false, false);
    public static a<Boolean> am = a.a("measurement.collection.firebase_global_collection_flag_enabled", true, true);
    public static a<Boolean> an = a.a("measurement.collection.efficient_engagement_reporting_enabled", false, false);
    public static a<Boolean> ao = a.a("measurement.collection.redundant_engagement_removal_enabled", false, false);
    public static a<Boolean> ap = a.a("measurement.remove_app_instance_id_cache_enabled", true, true);
    public static a<Boolean> aq = a.a("measurement.collection.init_params_control_enabled", true, true);
    public static a<Boolean> ar = a.a("measurement.upload.disable_is_uploader", false, false);
    public static a<Boolean> as = a.a("measurement.experiment.enable_experiment_reporting", false, false);
    public static a<Boolean> at = a.a("measurement.collection.log_event_and_bundle_v2", true, true);
    public static a<Boolean> au = a.a("measurement.collection.null_empty_event_name_fix", true, true);
    /* access modifiers changed from: private */
    public static final dy av = new dy(dr.a("com.google.android.gms.measurement"));
    private static volatile ar aw;
    private static Boolean ax;
    private static a<Boolean> ay = a.a("measurement.log_third_party_store_events_enabled", false, false);
    private static a<Boolean> az = a.a("measurement.log_installs_enabled", false, false);

    /* renamed from: b  reason: collision with root package name */
    static List<a<Integer>> f2562b = new ArrayList();
    static List<a<Long>> c = new ArrayList();
    static List<a<Boolean>> d = new ArrayList();
    static List<a<String>> e = new ArrayList();
    static List<a<Double>> f = new ArrayList();
    public static a<Boolean> g = a.a("measurement.log_androidId_enabled", false, false);
    public static a<Boolean> h = a.a("measurement.upload_dsid_enabled", false, false);
    public static a<String> i = a.a("measurement.log_tag", "FA", "FA-SVC");
    public static a<Long> j = a.a("measurement.ad_id_cache_time", 10000L, 10000L);
    public static a<Long> k = a.a("measurement.monitoring.sample_period_millis", 86400000L, 86400000L);
    public static a<Long> l = a.a("measurement.config.cache_time", 86400000L, 3600000L);
    public static a<String> m = a.a("measurement.config.url_scheme", "https", "https");
    public static a<String> n = a.a("measurement.config.url_authority", "app-measurement.com", "app-measurement.com");
    public static a<Integer> o = a.a("measurement.upload.max_bundles", 100, 100);
    public static a<Integer> p = a.a("measurement.upload.max_batch_size", 65536, 65536);
    public static a<Integer> q = a.a("measurement.upload.max_bundle_size", 65536, 65536);
    public static a<Integer> r = a.a("measurement.upload.max_events_per_bundle", 1000, 1000);
    public static a<Integer> s = a.a("measurement.upload.max_events_per_day", 100000, 100000);
    public static a<Integer> t = a.a("measurement.upload.max_error_events_per_day", 1000, 1000);
    public static a<Integer> u = a.a("measurement.upload.max_public_events_per_day", 50000, 50000);
    public static a<Integer> v = a.a("measurement.upload.max_conversions_per_day", 500, 500);
    public static a<Integer> w = a.a("measurement.upload.max_realtime_events_per_day", 10, 10);
    public static a<Integer> x = a.a("measurement.store.max_stored_events_per_app", 100000, 100000);
    public static a<String> y = a.a("measurement.upload.url", "https://app-measurement.com/a", "https://app-measurement.com/a");
    public static a<Long> z = a.a("measurement.upload.backoff_period", 43200000L, 43200000L);

    public static Map<String, String> a(Context context) {
        return dh.a(context.getContentResolver(), dr.a("com.google.android.gms.measurement")).a();
    }

    public static final class a<V> {

        /* renamed from: a  reason: collision with root package name */
        final String f2563a;

        /* renamed from: b  reason: collision with root package name */
        private ds<V> f2564b;
        private final V c;
        private final V d;
        private volatile V e;

        private a(String str, V v, V v2) {
            this.f2563a = str;
            this.d = v;
            this.c = v2;
        }

        static a<Boolean> a(String str, boolean z, boolean z2) {
            a<Boolean> aVar = new a<>(str, Boolean.valueOf(z), Boolean.valueOf(z2));
            h.d.add(aVar);
            return aVar;
        }

        static a<String> a(String str, String str2, String str3) {
            a<String> aVar = new a<>(str, str2, str3);
            h.e.add(aVar);
            return aVar;
        }

        static a<Long> a(String str, long j, long j2) {
            a<Long> aVar = new a<>(str, Long.valueOf(j), Long.valueOf(j2));
            h.c.add(aVar);
            return aVar;
        }

        static a<Integer> a(String str, int i, int i2) {
            a<Integer> aVar = new a<>(str, Integer.valueOf(i), Integer.valueOf(i2));
            h.f2562b.add(aVar);
            return aVar;
        }

        static a<Double> a(String str) {
            Double valueOf = Double.valueOf(-3.0d);
            a<Double> aVar = new a<>(str, valueOf, valueOf);
            h.f.add(aVar);
            return aVar;
        }

        private static void c() {
            synchronized (a.class) {
                if (!ej.a()) {
                    ej ejVar = h.f2561a;
                    try {
                        for (a next : h.d) {
                            next.e = next.f2564b.c();
                        }
                        for (a next2 : h.e) {
                            next2.e = next2.f2564b.c();
                        }
                        for (a next3 : h.c) {
                            next3.e = next3.f2564b.c();
                        }
                        for (a next4 : h.f2562b) {
                            next4.e = next4.f2564b.c();
                        }
                        for (a next5 : h.f) {
                            next5.e = next5.f2564b.c();
                        }
                    } catch (SecurityException e2) {
                        h.a(e2);
                    }
                } else {
                    throw new IllegalStateException("Tried to refresh flag cache on main thread or on package side.");
                }
            }
        }

        public final V a() {
            if (h.f2561a == null) {
                return this.d;
            }
            ej ejVar = h.f2561a;
            if (ej.a()) {
                return this.e == null ? this.d : this.e;
            }
            c();
            try {
                return this.f2564b.c();
            } catch (SecurityException e2) {
                h.a(e2);
                return this.f2564b.f2171a;
            }
        }

        public final V a(Long l) {
            if (l != null) {
                return l;
            }
            if (h.f2561a == null) {
                return this.d;
            }
            ej ejVar = h.f2561a;
            if (ej.a()) {
                return this.e == null ? this.d : this.e;
            }
            c();
            try {
                return this.f2564b.c();
            } catch (SecurityException e2) {
                h.a(e2);
                return this.f2564b.f2171a;
            }
        }

        static /* synthetic */ void b() {
            synchronized (a.class) {
                for (a next : h.d) {
                    dy a2 = h.av;
                    String str = next.f2563a;
                    ej ejVar = h.f2561a;
                    next.f2564b = ds.a(a2, str, ((Boolean) next.d).booleanValue());
                }
                for (a next2 : h.e) {
                    dy a3 = h.av;
                    String str2 = next2.f2563a;
                    ej ejVar2 = h.f2561a;
                    next2.f2564b = ds.a(a3, str2, (String) next2.d);
                }
                for (a next3 : h.c) {
                    dy a4 = h.av;
                    String str3 = next3.f2563a;
                    ej ejVar3 = h.f2561a;
                    next3.f2564b = ds.a(a4, str3, ((Long) next3.d).longValue());
                }
                for (a next4 : h.f2562b) {
                    dy a5 = h.av;
                    String str4 = next4.f2563a;
                    ej ejVar4 = h.f2561a;
                    next4.f2564b = ds.a(a5, str4, ((Integer) next4.d).intValue());
                }
                for (a next5 : h.f) {
                    dy a6 = h.av;
                    String str5 = next5.f2563a;
                    ej ejVar5 = h.f2561a;
                    next5.f2564b = ds.a(a6, str5, ((Double) next5.d).doubleValue());
                }
            }
        }
    }

    static void a(ar arVar) {
        aw = arVar;
    }

    static void a(Exception exc) {
        if (aw != null) {
            Context m2 = aw.m();
            if (ax == null) {
                ax = Boolean.valueOf(d.b().a(m2, f.f1536b) == 0);
            }
            if (ax.booleanValue()) {
                aw.q().c.a("Got Exception on PhenotypeFlag.get on Play device", exc);
            }
        }
    }

    static void a(ej ejVar) {
        f2561a = ejVar;
        a.b();
    }
}
