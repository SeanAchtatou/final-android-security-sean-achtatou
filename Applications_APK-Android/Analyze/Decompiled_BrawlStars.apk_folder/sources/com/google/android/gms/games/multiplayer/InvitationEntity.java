package com.google.android.gms.games.multiplayer;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;
import java.util.ArrayList;
import java.util.Arrays;

public final class InvitationEntity extends GamesDowngradeableSafeParcel implements Invitation {
    public static final Parcelable.Creator<InvitationEntity> CREATOR = new a();

    /* renamed from: b  reason: collision with root package name */
    private final GameEntity f1826b;
    private final String c;
    private final long d;
    private final int e;
    private final ParticipantEntity f;
    private final ArrayList<ParticipantEntity> g;
    private final int h;
    private final int i;

    static final class a extends d {
        a() {
        }

        public final InvitationEntity a(Parcel parcel) {
            if (InvitationEntity.b(InvitationEntity.b_()) || InvitationEntity.a(InvitationEntity.class.getCanonicalName())) {
                return super.createFromParcel(parcel);
            }
            GameEntity createFromParcel = GameEntity.CREATOR.createFromParcel(parcel);
            String readString = parcel.readString();
            long readLong = parcel.readLong();
            int readInt = parcel.readInt();
            ParticipantEntity createFromParcel2 = ParticipantEntity.CREATOR.createFromParcel(parcel);
            int readInt2 = parcel.readInt();
            ArrayList arrayList = new ArrayList(readInt2);
            for (int i = 0; i < readInt2; i++) {
                arrayList.add(ParticipantEntity.CREATOR.createFromParcel(parcel));
            }
            return new InvitationEntity(createFromParcel, readString, readLong, readInt, createFromParcel2, arrayList, -1, 0);
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return createFromParcel(parcel);
        }
    }

    InvitationEntity(GameEntity gameEntity, String str, long j, int i2, ParticipantEntity participantEntity, ArrayList<ParticipantEntity> arrayList, int i3, int i4) {
        this.f1826b = gameEntity;
        this.c = str;
        this.d = j;
        this.e = i2;
        this.f = participantEntity;
        this.g = arrayList;
        this.h = i3;
        this.i = i4;
    }

    InvitationEntity(Invitation invitation) {
        this.f1826b = new GameEntity(invitation.b());
        this.c = invitation.c();
        this.d = invitation.e();
        this.e = invitation.f();
        this.h = invitation.g();
        this.i = invitation.h();
        String i2 = invitation.d().i();
        ArrayList<Participant> i3 = invitation.i();
        int size = i3.size();
        this.g = new ArrayList<>(size);
        Participant participant = null;
        for (int i4 = 0; i4 < size; i4++) {
            Participant participant2 = i3.get(i4);
            if (participant2.i().equals(i2)) {
                participant = participant2;
            }
            this.g.add((ParticipantEntity) participant2.a());
        }
        l.a(participant, "Must have a valid inviter!");
        this.f = (ParticipantEntity) participant.a();
    }

    static boolean a(Invitation invitation, Object obj) {
        if (!(obj instanceof Invitation)) {
            return false;
        }
        if (invitation == obj) {
            return true;
        }
        Invitation invitation2 = (Invitation) obj;
        return j.a(invitation2.b(), invitation.b()) && j.a(invitation2.c(), invitation.c()) && j.a(Long.valueOf(invitation2.e()), Long.valueOf(invitation.e())) && j.a(Integer.valueOf(invitation2.f()), Integer.valueOf(invitation.f())) && j.a(invitation2.d(), invitation.d()) && j.a(invitation2.i(), invitation.i()) && j.a(Integer.valueOf(invitation2.g()), Integer.valueOf(invitation.g())) && j.a(Integer.valueOf(invitation2.h()), Integer.valueOf(invitation.h()));
    }

    static /* synthetic */ boolean a(String str) {
        return true;
    }

    static String b(Invitation invitation) {
        return j.a(invitation).a("Game", invitation.b()).a("InvitationId", invitation.c()).a("CreationTimestamp", Long.valueOf(invitation.e())).a("InvitationType", Integer.valueOf(invitation.f())).a("Inviter", invitation.d()).a("Participants", invitation.i()).a("Variant", Integer.valueOf(invitation.g())).a("AvailableAutoMatchSlots", Integer.valueOf(invitation.h())).toString();
    }

    public final /* bridge */ /* synthetic */ Object a() {
        return this;
    }

    public final Game b() {
        return this.f1826b;
    }

    public final String c() {
        return this.c;
    }

    public final Participant d() {
        return this.f;
    }

    public final long e() {
        return this.d;
    }

    public final boolean equals(Object obj) {
        return a(this, obj);
    }

    public final int f() {
        return this.e;
    }

    public final int g() {
        return this.h;
    }

    public final int h() {
        return this.i;
    }

    public final int hashCode() {
        return a(this);
    }

    public final ArrayList<Participant> i() {
        return new ArrayList<>(this.g);
    }

    public final String toString() {
        return b(this);
    }

    static int a(Invitation invitation) {
        return Arrays.hashCode(new Object[]{invitation.b(), invitation.c(), Long.valueOf(invitation.e()), Integer.valueOf(invitation.f()), invitation.d(), invitation.i(), Integer.valueOf(invitation.g()), Integer.valueOf(invitation.h())});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.GameEntity, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.multiplayer.ParticipantEntity, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i2) {
        if (!this.f1575a) {
            int a2 = com.google.android.gms.common.internal.safeparcel.a.a(parcel, 20293);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 1, (Parcelable) this.f1826b, i2, false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 2, this.c, false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 3, this.d);
            com.google.android.gms.common.internal.safeparcel.a.b(parcel, 4, this.e);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 5, (Parcelable) this.f, i2, false);
            com.google.android.gms.common.internal.safeparcel.a.b(parcel, 6, i(), false);
            com.google.android.gms.common.internal.safeparcel.a.b(parcel, 7, this.h);
            com.google.android.gms.common.internal.safeparcel.a.b(parcel, 8, this.i);
            com.google.android.gms.common.internal.safeparcel.a.b(parcel, a2);
            return;
        }
        this.f1826b.writeToParcel(parcel, i2);
        parcel.writeString(this.c);
        parcel.writeLong(this.d);
        parcel.writeInt(this.e);
        this.f.writeToParcel(parcel, i2);
        int size = this.g.size();
        parcel.writeInt(size);
        for (int i3 = 0; i3 < size; i3++) {
            this.g.get(i3).writeToParcel(parcel, i2);
        }
    }
}
