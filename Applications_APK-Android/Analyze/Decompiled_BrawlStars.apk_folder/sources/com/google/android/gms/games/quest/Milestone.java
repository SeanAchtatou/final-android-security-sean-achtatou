package com.google.android.gms.games.quest;

import android.os.Parcelable;
import com.google.android.gms.common.data.e;

@Deprecated
public interface Milestone extends Parcelable, e<Milestone> {
    String b();

    long c();

    String d();

    int e();

    long f();

    byte[] g();
}
