package com.google.android.gms.internal.measurement;

enum fm {
    SCALAR(false),
    VECTOR(true),
    PACKED_VECTOR(true),
    MAP(false);
    
    private final boolean e;

    private fm(boolean z) {
        this.e = z;
    }
}
