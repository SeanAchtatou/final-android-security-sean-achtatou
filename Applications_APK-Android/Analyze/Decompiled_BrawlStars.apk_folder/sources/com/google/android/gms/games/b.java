package com.google.android.gms.games;

import com.google.android.gms.common.api.Status;

public final class b extends com.google.android.gms.common.api.b {
    public static Status b(int i) {
        String str;
        switch (4) {
            case 26502:
                str = "CLIENT_RECONNECT_REQUIRED";
                break;
            case 26503:
                str = "NETWORK_ERROR_STALE_DATA";
                break;
            case 26504:
                str = "NETWORK_ERROR_NO_DATA";
                break;
            case 26505:
                str = "NETWORK_ERROR_OPERATION_DEFERRED";
                break;
            case 26506:
                str = "NETWORK_ERROR_OPERATION_FAILED";
                break;
            case 26507:
                str = "LICENSE_CHECK_FAILED";
                break;
            case 26508:
                str = "APP_MISCONFIGURED";
                break;
            case 26509:
                str = "GAME_NOT_FOUND";
                break;
            case 26520:
                str = "RESOLVE_STALE_OR_NO_DATA";
                break;
            case 26620:
                str = "VIDEO_NOT_ACTIVE";
                break;
            case 26621:
                str = "VIDEO_UNSUPPORTED";
                break;
            case 26622:
                str = "VIDEO_PERMISSION_ERROR";
                break;
            case 26623:
                str = "VIDEO_STORAGE_ERROR";
                break;
            case 26624:
                str = "VIDEO_UNEXPECTED_CAPTURE_ERROR";
                break;
            case 26625:
                str = "VIDEO_ALREADY_CAPTURING";
                break;
            case 26626:
                str = "VIDEO_OUT_OF_DISK_SPACE";
                break;
            case 26627:
                str = "VIDEO_NO_MIC";
                break;
            case 26628:
                str = "VIDEO_NO_CAMERA";
                break;
            case 26629:
                str = "VIDEO_SCREEN_OFF";
                break;
            case 26630:
                str = "VIDEO_RELEASE_TIMEOUT";
                break;
            case 26631:
                str = "VIDEO_CAPTURE_VIDEO_PERMISSION_REQUIRED";
                break;
            case 26632:
                str = "CAPTURE_ALREADY_PAUSED";
                break;
            case 26650:
                str = "VIDEO_MISSING_OVERLAY_PERMISSION";
                break;
            case 26652:
                str = "VIDEO_CAPTURE_OVERLAY_VISIBLE";
                break;
            case 26700:
                str = "CLIENT_LOADING";
                break;
            case 26701:
                str = "CLIENT_EMPTY";
                break;
            case 26702:
                str = "CLIENT_HIDDEN";
                break;
            default:
                switch (4) {
                    case 26530:
                        str = "AUTH_ERROR_HARD";
                        break;
                    case 26531:
                        str = "AUTH_ERROR_USER_RECOVERABLE";
                        break;
                    case 26532:
                        str = "AUTH_ERROR_UNREGISTERED_CLIENT_ID";
                        break;
                    case 26533:
                        str = "AUTH_ERROR_API_ACCESS_DENIED";
                        break;
                    case 26534:
                        str = "AUTH_ERROR_ACCOUNT_NOT_USABLE";
                        break;
                    default:
                        switch (4) {
                            case 26550:
                                str = "REQUEST_UPDATE_PARTIAL_SUCCESS";
                                break;
                            case 26551:
                                str = "REQUEST_UPDATE_TOTAL_FAILURE";
                                break;
                            case 26552:
                                str = "REQUEST_TOO_MANY_RECIPIENTS";
                                break;
                            default:
                                switch (4) {
                                    case 26560:
                                        str = "ACHIEVEMENT_UNLOCK_FAILURE";
                                        break;
                                    case 26561:
                                        str = "ACHIEVEMENT_UNKNOWN";
                                        break;
                                    case 26562:
                                        str = "ACHIEVEMENT_NOT_INCREMENTAL";
                                        break;
                                    case 26563:
                                        str = "ACHIEVEMENT_UNLOCKED";
                                        break;
                                    default:
                                        switch (4) {
                                            case 26570:
                                                str = "SNAPSHOT_NOT_FOUND";
                                                break;
                                            case 26571:
                                                str = "SNAPSHOT_CREATION_FAILED";
                                                break;
                                            case 26572:
                                                str = "SNAPSHOT_CONTENTS_UNAVAILABLE";
                                                break;
                                            case 26573:
                                                str = "SNAPSHOT_COMMIT_FAILED";
                                                break;
                                            case 26574:
                                                str = "SNAPSHOT_CONFLICT";
                                                break;
                                            case 26575:
                                                str = "SNAPSHOT_FOLDER_UNAVAILABLE";
                                                break;
                                            case 26576:
                                                str = "SNAPSHOT_CONFLICT_MISSING";
                                                break;
                                            default:
                                                switch (4) {
                                                    case 26580:
                                                        str = "MULTIPLAYER_ERROR_CREATION_NOT_ALLOWED";
                                                        break;
                                                    case 26581:
                                                        str = "MULTIPLAYER_ERROR_NOT_TRUSTED_TESTER";
                                                        break;
                                                    case 26582:
                                                        str = "MULTIPLAYER_ERROR_INVALID_MULTIPLAYER_TYPE";
                                                        break;
                                                    case 26583:
                                                        str = "MULTIPLAYER_DISABLED";
                                                        break;
                                                    case 26584:
                                                        str = "MULTIPLAYER_ERROR_INVALID_OPERATION";
                                                        break;
                                                    default:
                                                        switch (4) {
                                                            case 26590:
                                                                str = "MATCH_ERROR_INVALID_PARTICIPANT_STATE";
                                                                break;
                                                            case 26591:
                                                                str = "MATCH_ERROR_INACTIVE_MATCH";
                                                                break;
                                                            case 26592:
                                                                str = "MATCH_ERROR_INVALID_MATCH_STATE";
                                                                break;
                                                            case 26593:
                                                                str = "MATCH_ERROR_OUT_OF_DATE_VERSION";
                                                                break;
                                                            case 26594:
                                                                str = "MATCH_ERROR_INVALID_MATCH_RESULTS";
                                                                break;
                                                            case 26595:
                                                                str = "MATCH_ERROR_ALREADY_REMATCHED";
                                                                break;
                                                            case 26596:
                                                                str = "MATCH_NOT_FOUND";
                                                                break;
                                                            case 26597:
                                                                str = "MATCH_ERROR_LOCALLY_MODIFIED";
                                                                break;
                                                            default:
                                                                switch (4) {
                                                                    case 26600:
                                                                        str = "REAL_TIME_CONNECTION_FAILED";
                                                                        break;
                                                                    case 26601:
                                                                        str = "REAL_TIME_MESSAGE_SEND_FAILED";
                                                                        break;
                                                                    case 26602:
                                                                        str = "INVALID_REAL_TIME_ROOM_ID";
                                                                        break;
                                                                    case 26603:
                                                                        str = "PARTICIPANT_NOT_CONNECTED";
                                                                        break;
                                                                    case 26604:
                                                                        str = "REAL_TIME_ROOM_NOT_JOINED";
                                                                        break;
                                                                    case 26605:
                                                                        str = "REAL_TIME_INACTIVE_ROOM";
                                                                        break;
                                                                    case 26606:
                                                                        str = "REAL_TIME_SERVICE_NOT_CONNECTED";
                                                                        break;
                                                                    case 26607:
                                                                        str = "OPERATION_IN_FLIGHT";
                                                                        break;
                                                                    default:
                                                                        switch (4) {
                                                                            case 26610:
                                                                                str = "MILESTONE_CLAIMED_PREVIOUSLY";
                                                                                break;
                                                                            case 26611:
                                                                                str = "MILESTONE_CLAIM_FAILED";
                                                                                break;
                                                                            case 26612:
                                                                                str = "QUEST_NO_LONGER_AVAILABLE";
                                                                                break;
                                                                            case 26613:
                                                                                str = "QUEST_NOT_STARTED";
                                                                                break;
                                                                            default:
                                                                                str = com.google.android.gms.common.api.b.a(4);
                                                                                break;
                                                                        }
                                                                }
                                                        }
                                                }
                                        }
                                }
                        }
                }
        }
        return new Status(4, str);
    }
}
