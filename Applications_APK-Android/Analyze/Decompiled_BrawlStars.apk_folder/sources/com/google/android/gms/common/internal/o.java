package com.google.android.gms.common.internal;

import android.content.Context;
import android.content.res.Resources;
import com.google.android.gms.common.R;
import javax.annotation.Nullable;

public class o {

    /* renamed from: a  reason: collision with root package name */
    private final Resources f1615a;

    /* renamed from: b  reason: collision with root package name */
    private final String f1616b = this.f1615a.getResourcePackageName(R.string.common_google_play_services_unknown_issue);

    public o(Context context) {
        l.a(context);
        this.f1615a = context.getResources();
    }

    @Nullable
    public final String a(String str) {
        int identifier = this.f1615a.getIdentifier(str, "string", this.f1616b);
        if (identifier == 0) {
            return null;
        }
        return this.f1615a.getString(identifier);
    }
}
