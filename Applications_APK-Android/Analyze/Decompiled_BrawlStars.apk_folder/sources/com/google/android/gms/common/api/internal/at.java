package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.internal.d;

final class at implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ConnectionResult f1405a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ d.a f1406b;

    at(d.a aVar, ConnectionResult connectionResult) {
        this.f1406b = aVar;
        this.f1405a = connectionResult;
    }

    public final void run() {
        this.f1406b.onConnectionFailed(this.f1405a);
    }
}
