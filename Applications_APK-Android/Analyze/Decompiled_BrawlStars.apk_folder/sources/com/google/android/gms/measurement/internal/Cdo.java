package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.do  reason: invalid class name */
final class Cdo implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ long f2518a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ dj f2519b;

    Cdo(dj djVar, long j) {
        this.f2519b = djVar;
        this.f2518a = j;
    }

    public final void run() {
        dj djVar = this.f2519b;
        long j = this.f2518a;
        djVar.c();
        djVar.v();
        djVar.c.c();
        djVar.d.c();
        if (djVar.s().h(djVar.f().v()) || djVar.s().i(djVar.f().v())) {
            djVar.e.c();
            djVar.e.a(djVar.r().m.a());
        }
        djVar.q().k.a("Activity paused, time", Long.valueOf(j));
        if (djVar.f2511a != 0) {
            djVar.r().p.a(djVar.r().p.a() + (j - djVar.f2511a));
        }
    }
}
