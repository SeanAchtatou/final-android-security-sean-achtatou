package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.drive.metadata.a;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public final class zzb<T> extends zza {
    public static final b CREATOR = new b();

    /* renamed from: a  reason: collision with root package name */
    private final zzx f1751a;

    /* renamed from: b  reason: collision with root package name */
    private final MetadataBundle f1752b;
    private final a<T> c;

    zzb(zzx zzx, MetadataBundle metadataBundle) {
        this.f1751a = zzx;
        this.f1752b = metadataBundle;
        this.c = f.a(metadataBundle);
    }

    public final <F> F a(g<F> gVar) {
        zzx zzx = this.f1751a;
        a<T> aVar = this.c;
        return gVar.a(zzx, aVar, this.f1752b.a(aVar));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.query.internal.zzx, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.metadata.internal.MetadataBundle, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = com.google.android.gms.common.internal.safeparcel.a.a(parcel, 20293);
        com.google.android.gms.common.internal.safeparcel.a.a(parcel, 1, (Parcelable) this.f1751a, i, false);
        com.google.android.gms.common.internal.safeparcel.a.a(parcel, 2, (Parcelable) this.f1752b, i, false);
        com.google.android.gms.common.internal.safeparcel.a.b(parcel, a2);
    }
}
