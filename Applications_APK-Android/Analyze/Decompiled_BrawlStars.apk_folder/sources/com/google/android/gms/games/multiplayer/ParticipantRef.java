package com.google.android.gms.games.multiplayer;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.d;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerRef;

public final class ParticipantRef extends d implements Participant {
    private final PlayerRef c;

    public ParticipantRef(DataHolder dataHolder, int i) {
        super(dataHolder, i);
        this.c = new PlayerRef(dataHolder, i);
    }

    public final /* synthetic */ Object a() {
        return new ParticipantEntity(this);
    }

    public final int b() {
        return c("player_status");
    }

    public final String c() {
        return e("client_address");
    }

    public final int d() {
        return c("capabilities");
    }

    public final int describeContents() {
        return 0;
    }

    public final boolean e() {
        return c("connected") > 0;
    }

    public final boolean equals(Object obj) {
        return ParticipantEntity.a(this, obj);
    }

    public final String f() {
        return i("external_player_id") ? e("default_display_name") : this.c.c();
    }

    public final Uri g() {
        return i("external_player_id") ? h("default_display_image_uri") : this.c.g();
    }

    public final String getHiResImageUrl() {
        return i("external_player_id") ? e("default_display_hi_res_image_url") : this.c.getHiResImageUrl();
    }

    public final String getIconImageUrl() {
        return i("external_player_id") ? e("default_display_image_url") : this.c.getIconImageUrl();
    }

    public final Uri h() {
        return i("external_player_id") ? h("default_display_hi_res_image_uri") : this.c.h();
    }

    public final int hashCode() {
        return ParticipantEntity.a(this);
    }

    public final String i() {
        return e("external_participant_id");
    }

    public final Player j() {
        if (i("external_player_id")) {
            return null;
        }
        return this.c;
    }

    public final String toString() {
        return ParticipantEntity.b(this);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        ((ParticipantEntity) ((Participant) a())).writeToParcel(parcel, i);
    }

    public final ParticipantResult k() {
        if (i("result_type")) {
            return null;
        }
        return new ParticipantResult(e("external_participant_id"), c("result_type"), c("placing"));
    }
}
