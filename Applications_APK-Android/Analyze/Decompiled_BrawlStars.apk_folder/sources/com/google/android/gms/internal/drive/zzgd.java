package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.drive.DriveId;

public final class zzgd extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzgd> CREATOR = new ah();

    /* renamed from: a  reason: collision with root package name */
    private final DriveId f1956a;

    /* renamed from: b  reason: collision with root package name */
    private final int f1957b;
    private final int c;

    public zzgd(DriveId driveId, int i, int i2) {
        this.f1956a = driveId;
        this.f1957b = i;
        this.c = i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.drive.DriveId, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.a(parcel, 2, (Parcelable) this.f1956a, i, false);
        a.b(parcel, 3, this.f1957b);
        a.b(parcel, 4, this.c);
        a.b(parcel, a2);
    }
}
