package com.google.android.gms.analytics;

import com.google.android.gms.internal.measurement.bj;

public final class h {
    static String a(String str, int i) {
        if (i <= 0) {
            bj.a("index out of range for prefix", str);
            return "";
        }
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 11);
        sb.append(str);
        sb.append(i);
        return sb.toString();
    }
}
