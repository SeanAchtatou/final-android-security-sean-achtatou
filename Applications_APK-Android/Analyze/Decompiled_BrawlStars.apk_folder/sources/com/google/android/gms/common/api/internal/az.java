package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.g;
import com.google.android.gms.common.api.internal.c;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public interface az {
    <A extends a.b, T extends c.a<? extends g, A>> T a(c.a aVar);

    void a();

    void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

    void b();

    boolean d();

    boolean e();

    void f();
}
