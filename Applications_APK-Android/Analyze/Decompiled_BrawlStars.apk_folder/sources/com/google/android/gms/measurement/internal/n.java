package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.common.f;
import com.google.android.gms.common.internal.BaseGmsClient;

public final class n extends BaseGmsClient<zzaj> {
    public n(Context context, Looper looper, BaseGmsClient.a aVar, BaseGmsClient.b bVar) {
        super(context, looper, 93, aVar, bVar, null);
    }

    public final int g() {
        return f.f1536b;
    }

    public final String i() {
        return "com.google.android.gms.measurement.START";
    }

    public final String j() {
        return "com.google.android.gms.measurement.internal.IMeasurementService";
    }

    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.measurement.internal.IMeasurementService");
        if (queryLocalInterface instanceof zzaj) {
            return (zzaj) queryLocalInterface;
        }
        return new zzal(iBinder);
    }
}
