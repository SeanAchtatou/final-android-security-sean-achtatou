package com.google.android.gms.common.util.a;

import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.internal.common.c;
import java.util.concurrent.Executor;

public class a implements Executor {

    /* renamed from: a  reason: collision with root package name */
    private final Handler f1666a;

    public a(Looper looper) {
        this.f1666a = new c(looper);
    }

    public void execute(Runnable runnable) {
        this.f1666a.post(runnable);
    }
}
