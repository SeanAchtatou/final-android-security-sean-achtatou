package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class aj implements Parcelable.Creator<zzgi> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zzgi[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        int i = 0;
        int i2 = 0;
        boolean z = false;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            int i3 = 65535 & readInt;
            if (i3 == 2) {
                i = SafeParcelReader.d(parcel, readInt);
            } else if (i3 == 3) {
                i2 = SafeParcelReader.d(parcel, readInt);
            } else if (i3 != 4) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                z = SafeParcelReader.c(parcel, readInt);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new zzgi(i, i2, z);
    }
}
