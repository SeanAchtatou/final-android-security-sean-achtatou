package com.google.android.gms.measurement.internal;

import java.util.concurrent.Callable;

final class dy implements Callable<String> {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zzk f2536a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ du f2537b;

    dy(du duVar, zzk zzk) {
        this.f2537b = duVar;
        this.f2536a = zzk;
    }

    public final /* synthetic */ Object call() throws Exception {
        ef efVar;
        if (this.f2537b.b().e(this.f2536a.f2601a)) {
            efVar = this.f2537b.c(this.f2536a);
        } else {
            efVar = this.f2537b.d().b(this.f2536a.f2601a);
        }
        if (efVar != null) {
            return efVar.b();
        }
        this.f2537b.q().f.a("App info was null when attempting to get app instance id");
        return null;
    }
}
