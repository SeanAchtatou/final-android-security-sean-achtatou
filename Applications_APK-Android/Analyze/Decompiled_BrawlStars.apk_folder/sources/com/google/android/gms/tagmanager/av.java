package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.measurement.dc;
import com.google.android.gms.internal.measurement.dd;
import java.util.Set;

final class av implements aw {
    av() {
    }

    public final void a(dd ddVar, Set<dc> set, Set<dc> set2, ar arVar) {
        set.addAll(ddVar.c);
        set2.addAll(ddVar.d);
        arVar.e();
        arVar.f();
    }
}
