package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.a;

public final class zzr extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzr> CREATOR = new cd();

    /* renamed from: a  reason: collision with root package name */
    private final int f1983a;

    public zzr(int i) {
        l.b(i == 536870912 || i == 805306368, "Cannot create a new read-only contents!");
        this.f1983a = i;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.b(parcel, 2, this.f1983a);
        a.b(parcel, a2);
    }
}
