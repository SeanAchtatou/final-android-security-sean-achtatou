package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class cq extends iz<cq> {
    private static volatile cq[] c;

    /* renamed from: a  reason: collision with root package name */
    public String f2130a = null;

    /* renamed from: b  reason: collision with root package name */
    public String f2131b = null;

    public static cq[] a() {
        if (c == null) {
            synchronized (jd.f2312b) {
                if (c == null) {
                    c = new cq[0];
                }
            }
        }
        return c;
    }

    public cq() {
        this.L = null;
        this.M = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof cq)) {
            return false;
        }
        cq cqVar = (cq) obj;
        String str = this.f2130a;
        if (str == null) {
            if (cqVar.f2130a != null) {
                return false;
            }
        } else if (!str.equals(cqVar.f2130a)) {
            return false;
        }
        String str2 = this.f2131b;
        if (str2 == null) {
            if (cqVar.f2131b != null) {
                return false;
            }
        } else if (!str2.equals(cqVar.f2131b)) {
            return false;
        }
        if (this.L == null || this.L.a()) {
            return cqVar.L == null || cqVar.L.a();
        }
        return this.L.equals(cqVar.L);
    }

    public final int hashCode() {
        int hashCode = (getClass().getName().hashCode() + 527) * 31;
        String str = this.f2130a;
        int i = 0;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.f2131b;
        int hashCode3 = (hashCode2 + (str2 == null ? 0 : str2.hashCode())) * 31;
        if (this.L != null && !this.L.a()) {
            i = this.L.hashCode();
        }
        return hashCode3 + i;
    }

    public final void a(iy iyVar) throws IOException {
        String str = this.f2130a;
        if (str != null) {
            iyVar.a(1, str);
        }
        String str2 = this.f2131b;
        if (str2 != null) {
            iyVar.a(2, str2);
        }
        super.a(iyVar);
    }

    /* access modifiers changed from: protected */
    public final int b() {
        int b2 = super.b();
        String str = this.f2130a;
        if (str != null) {
            b2 += iy.b(1, str);
        }
        String str2 = this.f2131b;
        return str2 != null ? b2 + iy.b(2, str2) : b2;
    }

    public final /* synthetic */ je a(iw iwVar) throws IOException {
        while (true) {
            int a2 = iwVar.a();
            if (a2 == 0) {
                return this;
            }
            if (a2 == 10) {
                this.f2130a = iwVar.c();
            } else if (a2 == 18) {
                this.f2131b = iwVar.c();
            } else if (!super.a(iwVar, a2)) {
                return this;
            }
        }
    }
}
