package com.google.android.gms.games.stats;

import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.data.d;

public final class zzc extends d implements PlayerStats {
    private Bundle c;

    public final /* synthetic */ Object a() {
        return new zza(this);
    }

    public final float b() {
        return f("ave_session_length_minutes");
    }

    public final float c() {
        return f("churn_probability");
    }

    public final int d() {
        return c("days_since_last_played");
    }

    public final int describeContents() {
        return 0;
    }

    public final int e() {
        return c("num_purchases");
    }

    public final boolean equals(Object obj) {
        return zza.a(this, obj);
    }

    public final int f() {
        return c("num_sessions");
    }

    public final float g() {
        return f("num_sessions_percentile");
    }

    public final float h() {
        return f("spend_percentile");
    }

    public final int hashCode() {
        return zza.a(this);
    }

    public final float i() {
        if (!a_("spend_probability")) {
            return -1.0f;
        }
        return f("spend_probability");
    }

    public final float j() {
        if (!a_("high_spender_probability")) {
            return -1.0f;
        }
        return f("high_spender_probability");
    }

    public final float k() {
        if (!a_("total_spend_next_28_days")) {
            return -1.0f;
        }
        return f("total_spend_next_28_days");
    }

    public final String toString() {
        return zza.b(this);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        ((zza) ((PlayerStats) a())).writeToParcel(parcel, i);
    }

    public final Bundle l() {
        Bundle bundle = this.c;
        if (bundle != null) {
            return bundle;
        }
        this.c = new Bundle();
        String e = e("unknown_raw_keys");
        String e2 = e("unknown_raw_values");
        if (!(e == null || e2 == null)) {
            String[] split = e.split(",");
            String[] split2 = e2.split(",");
            if (split.length <= split2.length) {
                for (int i = 0; i < split.length; i++) {
                    this.c.putString(split[i], split2[i]);
                }
            } else {
                throw new IllegalStateException("Invalid raw arguments!");
            }
        }
        return this.c;
    }
}
