package com.google.android.gms.measurement.internal;

import android.content.ComponentName;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.stats.a;
import com.google.android.gms.common.util.e;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public final class cl extends df {

    /* renamed from: a  reason: collision with root package name */
    final cx f2469a;

    /* renamed from: b  reason: collision with root package name */
    zzaj f2470b;
    volatile Boolean c;
    private final et d;
    private final dp e;
    private final List<Runnable> f = new ArrayList();
    private final et g;

    protected cl(ar arVar) {
        super(arVar);
        this.e = new dp(arVar.l());
        this.f2469a = new cx(this);
        this.d = new cm(this, arVar);
        this.g = new cq(this, arVar);
    }

    /* access modifiers changed from: protected */
    public final boolean t() {
        return false;
    }

    public final boolean v() {
        c();
        D();
        return this.f2470b != null;
    }

    /* access modifiers changed from: protected */
    public final void w() {
        c();
        D();
        a(new cr(this, a(true)));
    }

    /* access modifiers changed from: package-private */
    public final void a(zzaj zzaj, AbstractSafeParcelable abstractSafeParcelable, zzk zzk) {
        int i;
        c();
        D();
        int i2 = 0;
        int i3 = 100;
        while (i2 < 1001 && i3 == 100) {
            ArrayList arrayList = new ArrayList();
            List<AbstractSafeParcelable> v = i().v();
            if (v != null) {
                arrayList.addAll(v);
                i = v.size();
            } else {
                i = 0;
            }
            if (abstractSafeParcelable != null && i < 100) {
                arrayList.add(abstractSafeParcelable);
            }
            ArrayList arrayList2 = arrayList;
            int size = arrayList2.size();
            int i4 = 0;
            while (i4 < size) {
                Object obj = arrayList2.get(i4);
                i4++;
                AbstractSafeParcelable abstractSafeParcelable2 = (AbstractSafeParcelable) obj;
                if (abstractSafeParcelable2 instanceof zzag) {
                    try {
                        zzaj.a((zzag) abstractSafeParcelable2, zzk);
                    } catch (RemoteException e2) {
                        q().c.a("Failed to send event to the service", e2);
                    }
                } else if (abstractSafeParcelable2 instanceof zzfv) {
                    try {
                        zzaj.a((zzfv) abstractSafeParcelable2, zzk);
                    } catch (RemoteException e3) {
                        q().c.a("Failed to send attribute to the service", e3);
                    }
                } else if (abstractSafeParcelable2 instanceof zzo) {
                    try {
                        zzaj.a((zzo) abstractSafeParcelable2, zzk);
                    } catch (RemoteException e4) {
                        q().c.a("Failed to send conditional property to the service", e4);
                    }
                } else {
                    q().c.a("Discarding data. Unrecognized parcel type.");
                }
            }
            i2++;
            i3 = i;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(zzag zzag, String str) {
        boolean z;
        l.a(zzag);
        c();
        D();
        k i = i();
        Parcel obtain = Parcel.obtain();
        zzag.writeToParcel(obtain, 0);
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        if (marshall.length > 131072) {
            i.q().f.a("Event is too long for local database. Sending event directly to service");
            z = false;
        } else {
            z = i.a(0, marshall);
        }
        a(new cs(this, true, z, zzag, a(true), str));
    }

    /* access modifiers changed from: protected */
    public final void a(zzo zzo) {
        boolean z;
        l.a(zzo);
        c();
        D();
        k i = i();
        i.o();
        byte[] a2 = ed.a((Parcelable) zzo);
        if (a2.length > 131072) {
            i.q().f.a("Conditional user property too long for local database. Sending directly to service");
            z = false;
        } else {
            z = i.a(2, a2);
        }
        a(new ct(this, true, z, new zzo(zzo), a(true), zzo));
    }

    /* access modifiers changed from: protected */
    public final void a(AtomicReference<List<zzo>> atomicReference, String str, String str2, String str3) {
        c();
        D();
        a(new cu(this, atomicReference, str, str2, str3, a(false)));
    }

    /* access modifiers changed from: protected */
    public final void a(AtomicReference<List<zzfv>> atomicReference, String str, String str2, String str3, boolean z) {
        c();
        D();
        a(new cv(this, atomicReference, str, str2, str3, z, a(false)));
    }

    /* access modifiers changed from: protected */
    public final void a(zzfv zzfv) {
        c();
        D();
        k i = i();
        Parcel obtain = Parcel.obtain();
        boolean z = false;
        zzfv.writeToParcel(obtain, 0);
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        if (marshall.length > 131072) {
            i.q().f.a("User property too long for local database. Sending directly to service");
        } else {
            z = i.a(1, marshall);
        }
        a(new cw(this, z, zzfv, a(true)));
    }

    public final void a(AtomicReference<String> atomicReference) {
        c();
        D();
        a(new cn(this, atomicReference, a(false)));
    }

    /* access modifiers changed from: protected */
    public final void x() {
        c();
        D();
        a(new co(this, a(true)));
    }

    /* access modifiers changed from: protected */
    public final void a(cg cgVar) {
        c();
        D();
        a(new cp(this, cgVar));
    }

    /* access modifiers changed from: package-private */
    public final void y() {
        c();
        this.e.a();
        this.d.a(h.O.a().longValue());
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0119  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void z() {
        /*
            r7 = this;
            r7.c()
            r7.D()
            boolean r0 = r7.v()
            if (r0 == 0) goto L_0x000d
            return
        L_0x000d:
            java.lang.Boolean r0 = r7.c
            r1 = 0
            r2 = 1
            if (r0 != 0) goto L_0x0126
            r7.c()
            r7.D()
            com.google.android.gms.measurement.internal.z r0 = r7.r()
            java.lang.Boolean r0 = r0.i()
            if (r0 == 0) goto L_0x002c
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x002c
            r0 = 1
            goto L_0x0120
        L_0x002c:
            com.google.android.gms.measurement.internal.i r0 = r7.f()
            int r0 = r0.z()
            if (r0 != r2) goto L_0x003a
        L_0x0036:
            r0 = 1
        L_0x0037:
            r3 = 1
            goto L_0x00ff
        L_0x003a:
            com.google.android.gms.measurement.internal.o r0 = r7.q()
            com.google.android.gms.measurement.internal.q r0 = r0.k
            java.lang.String r3 = "Checking service availability"
            r0.a(r3)
            com.google.android.gms.measurement.internal.ed r0 = r7.o()
            com.google.android.gms.common.d r3 = com.google.android.gms.common.d.b()
            android.content.Context r0 = r0.m()
            r4 = 12451000(0xbdfcb8, float:1.7447567E-38)
            int r0 = r3.a(r0, r4)
            if (r0 == 0) goto L_0x00f2
            if (r0 == r2) goto L_0x00e4
            r3 = 2
            if (r0 == r3) goto L_0x00a1
            r3 = 3
            if (r0 == r3) goto L_0x0095
            r3 = 9
            if (r0 == r3) goto L_0x0089
            r3 = 18
            if (r0 == r3) goto L_0x007d
            com.google.android.gms.measurement.internal.o r3 = r7.q()
            com.google.android.gms.measurement.internal.q r3 = r3.f
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.String r4 = "Unexpected service status"
            r3.a(r4, r0)
        L_0x0079:
            r0 = 0
        L_0x007a:
            r3 = 0
            goto L_0x00ff
        L_0x007d:
            com.google.android.gms.measurement.internal.o r0 = r7.q()
            com.google.android.gms.measurement.internal.q r0 = r0.f
            java.lang.String r3 = "Service updating"
            r0.a(r3)
            goto L_0x0036
        L_0x0089:
            com.google.android.gms.measurement.internal.o r0 = r7.q()
            com.google.android.gms.measurement.internal.q r0 = r0.f
            java.lang.String r3 = "Service invalid"
            r0.a(r3)
            goto L_0x0079
        L_0x0095:
            com.google.android.gms.measurement.internal.o r0 = r7.q()
            com.google.android.gms.measurement.internal.q r0 = r0.f
            java.lang.String r3 = "Service disabled"
            r0.a(r3)
            goto L_0x0079
        L_0x00a1:
            com.google.android.gms.measurement.internal.o r0 = r7.q()
            com.google.android.gms.measurement.internal.q r0 = r0.j
            java.lang.String r3 = "Service container out of date"
            r0.a(r3)
            com.google.android.gms.measurement.internal.ed r0 = r7.o()
            java.lang.Integer r3 = r0.f2544a
            if (r3 != 0) goto L_0x00c7
            com.google.android.gms.common.d.b()
            android.content.Context r3 = r0.m()
            int r3 = com.google.android.gms.common.d.c(r3)
            int r3 = r3 / 1000
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r0.f2544a = r3
        L_0x00c7:
            java.lang.Integer r0 = r0.f2544a
            int r0 = r0.intValue()
            r3 = 14500(0x38a4, float:2.0319E-41)
            if (r0 >= r3) goto L_0x00d2
            goto L_0x00ef
        L_0x00d2:
            com.google.android.gms.measurement.internal.z r0 = r7.r()
            java.lang.Boolean r0 = r0.i()
            if (r0 == 0) goto L_0x00e2
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0079
        L_0x00e2:
            r0 = 1
            goto L_0x007a
        L_0x00e4:
            com.google.android.gms.measurement.internal.o r0 = r7.q()
            com.google.android.gms.measurement.internal.q r0 = r0.k
            java.lang.String r3 = "Service missing"
            r0.a(r3)
        L_0x00ef:
            r0 = 0
            goto L_0x0037
        L_0x00f2:
            com.google.android.gms.measurement.internal.o r0 = r7.q()
            com.google.android.gms.measurement.internal.q r0 = r0.k
            java.lang.String r3 = "Service available"
            r0.a(r3)
            goto L_0x0036
        L_0x00ff:
            if (r0 != 0) goto L_0x0117
            com.google.android.gms.measurement.internal.el r4 = r7.s()
            boolean r4 = r4.t()
            if (r4 == 0) goto L_0x0117
            com.google.android.gms.measurement.internal.o r3 = r7.q()
            com.google.android.gms.measurement.internal.q r3 = r3.c
            java.lang.String r4 = "No way to upload. Consider using the full version of Analytics"
            r3.a(r4)
            r3 = 0
        L_0x0117:
            if (r3 == 0) goto L_0x0120
            com.google.android.gms.measurement.internal.z r3 = r7.r()
            r3.a(r0)
        L_0x0120:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r7.c = r0
        L_0x0126:
            java.lang.Boolean r0 = r7.c
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0196
            com.google.android.gms.measurement.internal.cx r0 = r7.f2469a
            com.google.android.gms.measurement.internal.cl r1 = r0.c
            r1.c()
            com.google.android.gms.measurement.internal.cl r1 = r0.c
            android.content.Context r1 = r1.m()
            monitor-enter(r0)
            boolean r3 = r0.f2491a     // Catch:{ all -> 0x0193 }
            if (r3 == 0) goto L_0x014f
            com.google.android.gms.measurement.internal.cl r1 = r0.c     // Catch:{ all -> 0x0193 }
            com.google.android.gms.measurement.internal.o r1 = r1.q()     // Catch:{ all -> 0x0193 }
            com.google.android.gms.measurement.internal.q r1 = r1.k     // Catch:{ all -> 0x0193 }
            java.lang.String r2 = "Connection attempt already in progress"
            r1.a(r2)     // Catch:{ all -> 0x0193 }
            monitor-exit(r0)     // Catch:{ all -> 0x0193 }
            goto L_0x0192
        L_0x014f:
            com.google.android.gms.measurement.internal.n r3 = r0.f2492b     // Catch:{ all -> 0x0193 }
            if (r3 == 0) goto L_0x0172
            com.google.android.gms.measurement.internal.n r3 = r0.f2492b     // Catch:{ all -> 0x0193 }
            boolean r3 = r3.c()     // Catch:{ all -> 0x0193 }
            if (r3 != 0) goto L_0x0163
            com.google.android.gms.measurement.internal.n r3 = r0.f2492b     // Catch:{ all -> 0x0193 }
            boolean r3 = r3.b()     // Catch:{ all -> 0x0193 }
            if (r3 == 0) goto L_0x0172
        L_0x0163:
            com.google.android.gms.measurement.internal.cl r1 = r0.c     // Catch:{ all -> 0x0193 }
            com.google.android.gms.measurement.internal.o r1 = r1.q()     // Catch:{ all -> 0x0193 }
            com.google.android.gms.measurement.internal.q r1 = r1.k     // Catch:{ all -> 0x0193 }
            java.lang.String r2 = "Already awaiting connection attempt"
            r1.a(r2)     // Catch:{ all -> 0x0193 }
            monitor-exit(r0)     // Catch:{ all -> 0x0193 }
            goto L_0x0192
        L_0x0172:
            com.google.android.gms.measurement.internal.n r3 = new com.google.android.gms.measurement.internal.n     // Catch:{ all -> 0x0193 }
            android.os.Looper r4 = android.os.Looper.getMainLooper()     // Catch:{ all -> 0x0193 }
            r3.<init>(r1, r4, r0, r0)     // Catch:{ all -> 0x0193 }
            r0.f2492b = r3     // Catch:{ all -> 0x0193 }
            com.google.android.gms.measurement.internal.cl r1 = r0.c     // Catch:{ all -> 0x0193 }
            com.google.android.gms.measurement.internal.o r1 = r1.q()     // Catch:{ all -> 0x0193 }
            com.google.android.gms.measurement.internal.q r1 = r1.k     // Catch:{ all -> 0x0193 }
            java.lang.String r3 = "Connecting to remote service"
            r1.a(r3)     // Catch:{ all -> 0x0193 }
            r0.f2491a = r2     // Catch:{ all -> 0x0193 }
            com.google.android.gms.measurement.internal.n r1 = r0.f2492b     // Catch:{ all -> 0x0193 }
            r1.k()     // Catch:{ all -> 0x0193 }
            monitor-exit(r0)     // Catch:{ all -> 0x0193 }
        L_0x0192:
            return
        L_0x0193:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0193 }
            throw r1
        L_0x0196:
            com.google.android.gms.measurement.internal.el r0 = r7.s()
            boolean r0 = r0.t()
            if (r0 != 0) goto L_0x022a
            android.content.Context r0 = r7.m()
            android.content.pm.PackageManager r0 = r0.getPackageManager()
            android.content.Intent r3 = new android.content.Intent
            r3.<init>()
            android.content.Context r4 = r7.m()
            java.lang.String r5 = "com.google.android.gms.measurement.AppMeasurementService"
            android.content.Intent r3 = r3.setClassName(r4, r5)
            r4 = 65536(0x10000, float:9.18355E-41)
            java.util.List r0 = r0.queryIntentServices(r3, r4)
            if (r0 == 0) goto L_0x01c6
            int r0 = r0.size()
            if (r0 <= 0) goto L_0x01c6
            r1 = 1
        L_0x01c6:
            if (r1 == 0) goto L_0x021f
            android.content.Intent r0 = new android.content.Intent
            java.lang.String r1 = "com.google.android.gms.measurement.START"
            r0.<init>(r1)
            android.content.ComponentName r1 = new android.content.ComponentName
            android.content.Context r3 = r7.m()
            java.lang.String r4 = "com.google.android.gms.measurement.AppMeasurementService"
            r1.<init>(r3, r4)
            r0.setComponent(r1)
            com.google.android.gms.measurement.internal.cx r1 = r7.f2469a
            com.google.android.gms.measurement.internal.cl r3 = r1.c
            r3.c()
            com.google.android.gms.measurement.internal.cl r3 = r1.c
            android.content.Context r3 = r3.m()
            com.google.android.gms.common.stats.a r4 = com.google.android.gms.common.stats.a.a()
            monitor-enter(r1)
            boolean r5 = r1.f2491a     // Catch:{ all -> 0x021c }
            if (r5 == 0) goto L_0x0202
            com.google.android.gms.measurement.internal.cl r0 = r1.c     // Catch:{ all -> 0x021c }
            com.google.android.gms.measurement.internal.o r0 = r0.q()     // Catch:{ all -> 0x021c }
            com.google.android.gms.measurement.internal.q r0 = r0.k     // Catch:{ all -> 0x021c }
            java.lang.String r2 = "Connection attempt already in progress"
            r0.a(r2)     // Catch:{ all -> 0x021c }
            monitor-exit(r1)     // Catch:{ all -> 0x021c }
            goto L_0x021b
        L_0x0202:
            com.google.android.gms.measurement.internal.cl r5 = r1.c     // Catch:{ all -> 0x021c }
            com.google.android.gms.measurement.internal.o r5 = r5.q()     // Catch:{ all -> 0x021c }
            com.google.android.gms.measurement.internal.q r5 = r5.k     // Catch:{ all -> 0x021c }
            java.lang.String r6 = "Using local app measurement service"
            r5.a(r6)     // Catch:{ all -> 0x021c }
            r1.f2491a = r2     // Catch:{ all -> 0x021c }
            com.google.android.gms.measurement.internal.cl r2 = r1.c     // Catch:{ all -> 0x021c }
            com.google.android.gms.measurement.internal.cx r2 = r2.f2469a     // Catch:{ all -> 0x021c }
            r5 = 129(0x81, float:1.81E-43)
            r4.b(r3, r0, r2, r5)     // Catch:{ all -> 0x021c }
            monitor-exit(r1)     // Catch:{ all -> 0x021c }
        L_0x021b:
            return
        L_0x021c:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x021c }
            throw r0
        L_0x021f:
            com.google.android.gms.measurement.internal.o r0 = r7.q()
            com.google.android.gms.measurement.internal.q r0 = r0.c
            java.lang.String r1 = "Unable to use remote or local measurement implementation. Please register the AppMeasurementService service in the app manifest"
            r0.a(r1)
        L_0x022a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.cl.z():void");
    }

    /* access modifiers changed from: protected */
    public final void a(zzaj zzaj) {
        c();
        l.a(zzaj);
        this.f2470b = zzaj;
        y();
        B();
    }

    public final void A() {
        c();
        D();
        cx cxVar = this.f2469a;
        if (cxVar.f2492b != null && (cxVar.f2492b.b() || cxVar.f2492b.c())) {
            cxVar.f2492b.a();
        }
        cxVar.f2492b = null;
        try {
            a.a();
            a.a(m(), this.f2469a);
        } catch (IllegalArgumentException | IllegalStateException unused) {
        }
        this.f2470b = null;
    }

    private final void a(Runnable runnable) throws IllegalStateException {
        c();
        if (v()) {
            runnable.run();
        } else if (((long) this.f.size()) >= 1000) {
            q().c.a("Discarding data. Max runnable queue size reached");
        } else {
            this.f.add(runnable);
            this.g.a(60000);
            z();
        }
    }

    /* access modifiers changed from: package-private */
    public final void B() {
        c();
        q().k.a("Processing queued up service tasks", Integer.valueOf(this.f.size()));
        for (Runnable run : this.f) {
            try {
                run.run();
            } catch (Exception e2) {
                q().c.a("Task exception while flushing queue", e2);
            }
        }
        this.f.clear();
        this.g.c();
    }

    private final zzk a(boolean z) {
        return f().a(z ? q().f_() : null);
    }

    public final /* bridge */ /* synthetic */ void a() {
        super.a();
    }

    public final /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    public final /* bridge */ /* synthetic */ void c() {
        super.c();
    }

    public final /* bridge */ /* synthetic */ a d() {
        return super.d();
    }

    public final /* bridge */ /* synthetic */ bv e() {
        return super.e();
    }

    public final /* bridge */ /* synthetic */ i f() {
        return super.f();
    }

    public final /* bridge */ /* synthetic */ cl g() {
        return super.g();
    }

    public final /* bridge */ /* synthetic */ ch h() {
        return super.h();
    }

    public final /* bridge */ /* synthetic */ k i() {
        return super.i();
    }

    public final /* bridge */ /* synthetic */ dj j() {
        return super.j();
    }

    public final /* bridge */ /* synthetic */ b k() {
        return super.k();
    }

    public final /* bridge */ /* synthetic */ e l() {
        return super.l();
    }

    public final /* bridge */ /* synthetic */ Context m() {
        return super.m();
    }

    public final /* bridge */ /* synthetic */ m n() {
        return super.n();
    }

    public final /* bridge */ /* synthetic */ ed o() {
        return super.o();
    }

    public final /* bridge */ /* synthetic */ am p() {
        return super.p();
    }

    public final /* bridge */ /* synthetic */ o q() {
        return super.q();
    }

    public final /* bridge */ /* synthetic */ z r() {
        return super.r();
    }

    public final /* bridge */ /* synthetic */ el s() {
        return super.s();
    }

    static /* synthetic */ void a(cl clVar, ComponentName componentName) {
        clVar.c();
        if (clVar.f2470b != null) {
            clVar.f2470b = null;
            clVar.q().k.a("Disconnected from device MeasurementService", componentName);
            clVar.c();
            clVar.z();
        }
    }

    static /* synthetic */ void a(cl clVar) {
        clVar.c();
        if (clVar.v()) {
            clVar.q().k.a("Inactivity, disconnecting from the service");
            clVar.A();
        }
    }
}
