package com.google.android.gms.common.util;

import android.content.Context;
import android.os.DropBoxManager;
import com.google.android.gms.common.internal.l;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    private static final String[] f1673a = {"android.", "com.android.", "dalvik.", "java.", "javax."};

    /* renamed from: b  reason: collision with root package name */
    private static DropBoxManager f1674b = null;
    private static boolean c = false;
    private static int d = -1;
    private static int e;
    private static int f;

    public static boolean a(Context context, Throwable th) {
        return b(context, th);
    }

    private static boolean b(Context context, Throwable th) {
        try {
            l.a(context);
            l.a(th);
        } catch (Exception unused) {
        }
        return false;
    }
}
