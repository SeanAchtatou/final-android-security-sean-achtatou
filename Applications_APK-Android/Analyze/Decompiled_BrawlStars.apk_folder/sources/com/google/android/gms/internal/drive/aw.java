package com.google.android.gms.internal.drive;

import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.metadata.internal.k;
import java.util.Collection;

final class aw extends k<BitmapTeleporter> {
    aw(String str, Collection collection, Collection collection2) {
        super(str, collection, collection2, 4400000);
    }

    public final /* synthetic */ Object c(DataHolder dataHolder, int i, int i2) {
        throw new IllegalStateException("Thumbnail field is write only");
    }
}
