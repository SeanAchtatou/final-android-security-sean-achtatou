package com.google.android.gms.measurement.internal;

import java.util.concurrent.atomic.AtomicReference;

final class ca implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ AtomicReference f2454a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f2455b;
    private final /* synthetic */ String c;
    private final /* synthetic */ String d;
    private final /* synthetic */ bv e;

    ca(bv bvVar, AtomicReference atomicReference, String str, String str2, String str3) {
        this.e = bvVar;
        this.f2454a = atomicReference;
        this.f2455b = str;
        this.c = str2;
        this.d = str3;
    }

    public final void run() {
        this.e.r.i().a(this.f2454a, this.f2455b, this.c, this.d);
    }
}
