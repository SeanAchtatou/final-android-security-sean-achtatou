package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.g;
import com.google.android.gms.common.api.internal.c;
import com.google.android.gms.common.d;
import com.google.android.gms.common.internal.IAccountAccessor;
import com.google.android.gms.common.internal.b;
import com.google.android.gms.signin.e;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;

public final class u implements al {

    /* renamed from: a  reason: collision with root package name */
    final am f1496a;

    /* renamed from: b  reason: collision with root package name */
    final Lock f1497b;
    final Context c;
    final d d;
    e e;
    boolean f;
    boolean g;
    IAccountAccessor h;
    boolean i;
    boolean j;
    final b k;
    private ConnectionResult l;
    private int m;
    private int n = 0;
    private int o;
    private final Bundle p = new Bundle();
    private final Set<a.c> q = new HashSet();
    private boolean r;
    private final Map<a<?>, Boolean> s;
    private final a.C0111a<? extends e, com.google.android.gms.signin.a> t;
    private ArrayList<Future<?>> u = new ArrayList<>();

    public u(am amVar, b bVar, Map<a<?>, Boolean> map, d dVar, a.C0111a<? extends e, com.google.android.gms.signin.a> aVar, Lock lock, Context context) {
        this.f1496a = amVar;
        this.k = bVar;
        this.s = map;
        this.d = dVar;
        this.t = aVar;
        this.f1497b = lock;
        this.c = context;
    }

    private static String c(int i2) {
        return i2 != 0 ? i2 != 1 ? "UNKNOWN" : "STEP_GETTING_REMOTE_SERVICE" : "STEP_SERVICE_BINDINGS_AND_SIGN_IN";
    }

    public final void c() {
    }

    public final void a() {
        this.f1496a.g.clear();
        this.f = false;
        this.l = null;
        this.n = 0;
        this.r = true;
        this.g = false;
        this.i = false;
        HashMap hashMap = new HashMap();
        boolean z = false;
        for (a next : this.s.keySet()) {
            a.f fVar = this.f1496a.f.get(next.b());
            z |= next.f1371a.a() == 1;
            boolean booleanValue = this.s.get(next).booleanValue();
            if (fVar.d()) {
                this.f = true;
                if (booleanValue) {
                    this.q.add(next.b());
                } else {
                    this.r = false;
                }
            }
            hashMap.put(fVar, new w(this, next, booleanValue));
        }
        if (z) {
            this.f = false;
        }
        if (this.f) {
            this.k.j = Integer.valueOf(System.identityHashCode(this.f1496a.m));
            ad adVar = new ad(this, (byte) 0);
            a.C0111a<? extends e, com.google.android.gms.signin.a> aVar = this.t;
            Context context = this.c;
            Looper a2 = this.f1496a.m.a();
            b bVar = this.k;
            this.e = (e) aVar.a(context, a2, bVar, bVar.i, adVar, adVar);
        }
        this.o = this.f1496a.f.size();
        this.u.add(ap.a().submit(new x(this, hashMap)));
    }

    /* access modifiers changed from: package-private */
    public final boolean d() {
        this.o--;
        int i2 = this.o;
        if (i2 > 0) {
            return false;
        }
        if (i2 < 0) {
            this.f1496a.m.j();
            Log.wtf("GoogleApiClientConnecting", "GoogleApiClient received too many callbacks for the given step. Clients may be in an unexpected state; GoogleApiClient will now disconnect.", new Exception());
            b(new ConnectionResult(8, null));
            return false;
        }
        ConnectionResult connectionResult = this.l;
        if (connectionResult == null) {
            return true;
        }
        this.f1496a.l = this.m;
        b(connectionResult);
        return false;
    }

    /* access modifiers changed from: package-private */
    public final void e() {
        if (this.o == 0) {
            if (!this.f || this.g) {
                ArrayList arrayList = new ArrayList();
                this.n = 1;
                this.o = this.f1496a.f.size();
                for (a.c next : this.f1496a.f.keySet()) {
                    if (!this.f1496a.g.containsKey(next)) {
                        arrayList.add(this.f1496a.f.get(next));
                    } else if (d()) {
                        g();
                    }
                }
                if (!arrayList.isEmpty()) {
                    this.u.add(ap.a().submit(new aa(this, arrayList)));
                }
            }
        }
    }

    public final void a(Bundle bundle) {
        if (b(1)) {
            if (bundle != null) {
                this.p.putAll(bundle);
            }
            if (d()) {
                g();
            }
        }
    }

    public final void a(ConnectionResult connectionResult, a<?> aVar, boolean z) {
        if (b(1)) {
            b(connectionResult, aVar, z);
            if (d()) {
                g();
            }
        }
    }

    private final void g() {
        this.f1496a.c();
        ap.a().execute(new v(this));
        e eVar = this.e;
        if (eVar != null) {
            if (this.i) {
                eVar.a(this.h, this.j);
            }
            a(false);
        }
        for (a.c<?> cVar : this.f1496a.g.keySet()) {
            this.f1496a.f.get(cVar).a();
        }
        this.f1496a.n.a(this.p.isEmpty() ? null : this.p);
    }

    public final <A extends a.b, T extends c.a<? extends g, A>> T a(T t2) {
        throw new IllegalStateException("GoogleApiClient is not connected yet.");
    }

    public final boolean b() {
        h();
        a(true);
        this.f1496a.a((ConnectionResult) null);
        return true;
    }

    public final void a(int i2) {
        b(new ConnectionResult(8, null));
    }

    /* access modifiers changed from: package-private */
    public final void f() {
        this.f = false;
        this.f1496a.m.c = Collections.emptySet();
        for (a.c next : this.q) {
            if (!this.f1496a.g.containsKey(next)) {
                this.f1496a.g.put(next, new ConnectionResult(17, null));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean a(ConnectionResult connectionResult) {
        return this.r && !connectionResult.a();
    }

    /* access modifiers changed from: package-private */
    public final void b(ConnectionResult connectionResult) {
        h();
        a(!connectionResult.a());
        this.f1496a.a(connectionResult);
        this.f1496a.n.a(connectionResult);
    }

    private final void a(boolean z) {
        e eVar = this.e;
        if (eVar != null) {
            if (eVar.b() && z) {
                this.e.r();
            }
            this.e.a();
            this.h = null;
        }
    }

    private final void h() {
        ArrayList arrayList = this.u;
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            Object obj = arrayList.get(i2);
            i2++;
            ((Future) obj).cancel(true);
        }
        this.u.clear();
    }

    /* access modifiers changed from: package-private */
    public final boolean b(int i2) {
        if (this.n == i2) {
            return true;
        }
        this.f1496a.m.j();
        String valueOf = String.valueOf(this);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 23);
        sb.append("Unexpected callback in ");
        sb.append(valueOf);
        sb.toString();
        int i3 = this.o;
        StringBuilder sb2 = new StringBuilder(33);
        sb2.append("mRemainingConnections=");
        sb2.append(i3);
        sb2.toString();
        String c2 = c(this.n);
        String c3 = c(i2);
        StringBuilder sb3 = new StringBuilder(String.valueOf(c2).length() + 70 + String.valueOf(c3).length());
        sb3.append("GoogleApiClient connecting is in step ");
        sb3.append(c2);
        sb3.append(" but received callback for step ");
        sb3.append(c3);
        Log.wtf("GoogleApiClientConnecting", sb3.toString(), new Exception());
        b(new ConnectionResult(8, null));
        return false;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001f, code lost:
        if (r8 != false) goto L_0x0021;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b(com.google.android.gms.common.ConnectionResult r6, com.google.android.gms.common.api.a<?> r7, boolean r8) {
        /*
            r5 = this;
            com.google.android.gms.common.api.a$a<?, O> r0 = r7.f1371a
            int r0 = r0.a()
            r1 = 0
            r2 = 1
            if (r8 == 0) goto L_0x0021
            boolean r8 = r6.a()
            if (r8 == 0) goto L_0x0012
        L_0x0010:
            r8 = 1
            goto L_0x001f
        L_0x0012:
            com.google.android.gms.common.d r8 = r5.d
            int r3 = r6.f1353b
            r4 = 0
            android.content.Intent r8 = r8.a(r4, r3, r4)
            if (r8 == 0) goto L_0x001e
            goto L_0x0010
        L_0x001e:
            r8 = 0
        L_0x001f:
            if (r8 == 0) goto L_0x002a
        L_0x0021:
            com.google.android.gms.common.ConnectionResult r8 = r5.l
            if (r8 == 0) goto L_0x0029
            int r8 = r5.m
            if (r0 >= r8) goto L_0x002a
        L_0x0029:
            r1 = 1
        L_0x002a:
            if (r1 == 0) goto L_0x0030
            r5.l = r6
            r5.m = r0
        L_0x0030:
            com.google.android.gms.common.api.internal.am r8 = r5.f1496a
            java.util.Map<com.google.android.gms.common.api.a$c<?>, com.google.android.gms.common.ConnectionResult> r8 = r8.g
            com.google.android.gms.common.api.a$c r7 = r7.b()
            r8.put(r7, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.u.b(com.google.android.gms.common.ConnectionResult, com.google.android.gms.common.api.a, boolean):void");
    }
}
