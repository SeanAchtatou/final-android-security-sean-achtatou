package com.google.android.gms.internal.a;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

final class i extends WeakReference<Throwable> {

    /* renamed from: a  reason: collision with root package name */
    private final int f1872a;

    public i(Throwable th, ReferenceQueue<Throwable> referenceQueue) {
        super(th, referenceQueue);
        if (th != null) {
            this.f1872a = System.identityHashCode(th);
            return;
        }
        throw new NullPointerException("The referent cannot be null");
    }

    public final int hashCode() {
        return this.f1872a;
    }

    public final boolean equals(Object obj) {
        if (obj != null && obj.getClass() == getClass()) {
            if (this == obj) {
                return true;
            }
            i iVar = (i) obj;
            return this.f1872a == iVar.f1872a && get() == iVar.get();
        }
    }
}
