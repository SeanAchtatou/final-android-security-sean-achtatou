package com.google.android.gms.internal.drive;

import com.google.android.gms.common.internal.j;
import com.google.android.gms.drive.DriveId;
import java.util.Arrays;

public final class r {

    /* renamed from: a  reason: collision with root package name */
    private final DriveId f1919a;

    /* renamed from: b  reason: collision with root package name */
    private final int f1920b;
    private final int c;

    public r(zzh zzh) {
        this.f1919a = zzh.f1975b;
        this.f1920b = zzh.f1974a;
        this.c = zzh.c;
    }

    public final boolean equals(Object obj) {
        if (obj != null && obj.getClass() == getClass()) {
            if (obj == this) {
                return true;
            }
            r rVar = (r) obj;
            return j.a(this.f1919a, rVar.f1919a) && this.f1920b == rVar.f1920b && this.c == rVar.c;
        }
    }

    public final String toString() {
        return String.format("FileTransferState[TransferType: %d, DriveId: %s, status: %d]", Integer.valueOf(this.f1920b), this.f1919a, Integer.valueOf(this.c));
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.f1919a, Integer.valueOf(this.f1920b), Integer.valueOf(this.c)});
    }
}
