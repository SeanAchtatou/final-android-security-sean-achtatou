package com.google.android.gms.games.achievement;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.a;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.internal.zzd;
import java.util.Arrays;

public final class AchievementEntity extends zzd implements Achievement {
    public static final Parcelable.Creator<AchievementEntity> CREATOR = new b();

    /* renamed from: a  reason: collision with root package name */
    private final String f1798a;

    /* renamed from: b  reason: collision with root package name */
    private final int f1799b;
    private final String c;
    private final String d;
    private final Uri e;
    private final String f;
    private final Uri g;
    private final String h;
    private final int i;
    private final String j;
    private final PlayerEntity k;
    private final int l;
    private final int m;
    private final String n;
    private final long o;
    private final long p;

    public AchievementEntity(Achievement achievement) {
        this.f1798a = achievement.b();
        this.f1799b = achievement.c();
        this.c = achievement.d();
        this.d = achievement.e();
        this.e = achievement.f();
        this.f = achievement.getUnlockedImageUrl();
        this.g = achievement.g();
        this.h = achievement.getRevealedImageUrl();
        this.k = (PlayerEntity) achievement.j().a();
        this.l = achievement.k();
        this.o = achievement.n();
        this.p = achievement.o();
        if (achievement.c() == 1) {
            this.i = achievement.h();
            this.j = achievement.i();
            this.m = achievement.l();
            this.n = achievement.m();
        } else {
            this.i = 0;
            this.j = null;
            this.m = 0;
            this.n = null;
        }
        a.a(this.f1798a);
        a.a(this.d);
    }

    AchievementEntity(String str, int i2, String str2, String str3, Uri uri, String str4, Uri uri2, String str5, int i3, String str6, PlayerEntity playerEntity, int i4, int i5, String str7, long j2, long j3) {
        this.f1798a = str;
        this.f1799b = i2;
        this.c = str2;
        this.d = str3;
        this.e = uri;
        this.f = str4;
        this.g = uri2;
        this.h = str5;
        this.i = i3;
        this.j = str6;
        this.k = playerEntity;
        this.l = i4;
        this.m = i5;
        this.n = str7;
        this.o = j2;
        this.p = j3;
    }

    static String a(Achievement achievement) {
        j.a a2 = j.a(achievement).a("Id", achievement.b()).a("Type", Integer.valueOf(achievement.c())).a("Name", achievement.d()).a("Description", achievement.e()).a("Player", achievement.j()).a("State", Integer.valueOf(achievement.k()));
        if (achievement.c() == 1) {
            a2.a("CurrentSteps", Integer.valueOf(achievement.l()));
            a2.a("TotalSteps", Integer.valueOf(achievement.h()));
        }
        return a2.toString();
    }

    public final /* bridge */ /* synthetic */ Object a() {
        return this;
    }

    public final String b() {
        return this.f1798a;
    }

    public final int c() {
        return this.f1799b;
    }

    public final String d() {
        return this.c;
    }

    public final String e() {
        return this.d;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof Achievement)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        Achievement achievement = (Achievement) obj;
        if (achievement.c() == c()) {
            return (c() != 1 || (achievement.l() == l() && achievement.h() == h())) && achievement.o() == o() && achievement.k() == k() && achievement.n() == n() && j.a(achievement.b(), b()) && j.a(achievement.d(), d()) && j.a(achievement.e(), e()) && j.a(achievement.j(), j());
        }
        return false;
    }

    public final Uri f() {
        return this.e;
    }

    public final Uri g() {
        return this.g;
    }

    public final String getRevealedImageUrl() {
        return this.h;
    }

    public final String getUnlockedImageUrl() {
        return this.f;
    }

    public final Player j() {
        return this.k;
    }

    public final int k() {
        return this.l;
    }

    public final long n() {
        return this.o;
    }

    public final long o() {
        return this.p;
    }

    public final String toString() {
        return a(this);
    }

    public final int h() {
        boolean z = true;
        if (this.f1799b != 1) {
            z = false;
        }
        a.a(z);
        return this.i;
    }

    public final String i() {
        boolean z = true;
        if (this.f1799b != 1) {
            z = false;
        }
        a.a(z);
        return this.j;
    }

    public final int l() {
        boolean z = true;
        if (this.f1799b != 1) {
            z = false;
        }
        a.a(z);
        return this.m;
    }

    public final String m() {
        boolean z = true;
        if (this.f1799b != 1) {
            z = false;
        }
        a.a(z);
        return this.n;
    }

    public final int hashCode() {
        int i2;
        int i3;
        if (c() == 1) {
            i3 = l();
            i2 = h();
        } else {
            i3 = 0;
            i2 = 0;
        }
        return Arrays.hashCode(new Object[]{b(), d(), Integer.valueOf(c()), e(), Long.valueOf(o()), Integer.valueOf(k()), Long.valueOf(n()), j(), Integer.valueOf(i3), Integer.valueOf(i2)});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.net.Uri, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.PlayerEntity, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = com.google.android.gms.common.internal.safeparcel.a.a(parcel, 20293);
        com.google.android.gms.common.internal.safeparcel.a.a(parcel, 1, this.f1798a, false);
        com.google.android.gms.common.internal.safeparcel.a.b(parcel, 2, this.f1799b);
        com.google.android.gms.common.internal.safeparcel.a.a(parcel, 3, this.c, false);
        com.google.android.gms.common.internal.safeparcel.a.a(parcel, 4, this.d, false);
        com.google.android.gms.common.internal.safeparcel.a.a(parcel, 5, (Parcelable) this.e, i2, false);
        com.google.android.gms.common.internal.safeparcel.a.a(parcel, 6, getUnlockedImageUrl(), false);
        com.google.android.gms.common.internal.safeparcel.a.a(parcel, 7, (Parcelable) this.g, i2, false);
        com.google.android.gms.common.internal.safeparcel.a.a(parcel, 8, getRevealedImageUrl(), false);
        com.google.android.gms.common.internal.safeparcel.a.b(parcel, 9, this.i);
        com.google.android.gms.common.internal.safeparcel.a.a(parcel, 10, this.j, false);
        com.google.android.gms.common.internal.safeparcel.a.a(parcel, 11, (Parcelable) this.k, i2, false);
        com.google.android.gms.common.internal.safeparcel.a.b(parcel, 12, this.l);
        com.google.android.gms.common.internal.safeparcel.a.b(parcel, 13, this.m);
        com.google.android.gms.common.internal.safeparcel.a.a(parcel, 14, this.n, false);
        com.google.android.gms.common.internal.safeparcel.a.a(parcel, 15, this.o);
        com.google.android.gms.common.internal.safeparcel.a.a(parcel, 16, this.p);
        com.google.android.gms.common.internal.safeparcel.a.b(parcel, a2);
    }
}
