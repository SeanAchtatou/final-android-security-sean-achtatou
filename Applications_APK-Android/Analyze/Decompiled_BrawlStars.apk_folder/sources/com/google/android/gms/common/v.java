package com.google.android.gms.common;

import java.util.concurrent.Callable;

final class v extends u {
    private final Callable<String> c;

    private v(Callable<String> callable) {
        super(false, null, null);
        this.c = callable;
    }

    /* access modifiers changed from: package-private */
    public final String b() {
        try {
            return this.c.call();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /* synthetic */ v(Callable callable, byte b2) {
        this(callable);
    }
}
