package com.google.android.gms.internal.measurement;

import android.text.TextUtils;
import com.facebook.internal.NativeProtocol;
import com.google.android.gms.analytics.l;
import java.util.HashMap;

public final class i extends l<i> {

    /* renamed from: a  reason: collision with root package name */
    public String f2280a;

    /* renamed from: b  reason: collision with root package name */
    public String f2281b;
    public String c;

    public final String toString() {
        HashMap hashMap = new HashMap();
        hashMap.put("network", this.f2280a);
        hashMap.put(NativeProtocol.WEB_DIALOG_ACTION, this.f2281b);
        hashMap.put("target", this.c);
        return a((Object) hashMap);
    }

    public final /* synthetic */ void a(l lVar) {
        i iVar = (i) lVar;
        if (!TextUtils.isEmpty(this.f2280a)) {
            iVar.f2280a = this.f2280a;
        }
        if (!TextUtils.isEmpty(this.f2281b)) {
            iVar.f2281b = this.f2281b;
        }
        if (!TextUtils.isEmpty(this.c)) {
            iVar.c = this.c;
        }
    }
}
