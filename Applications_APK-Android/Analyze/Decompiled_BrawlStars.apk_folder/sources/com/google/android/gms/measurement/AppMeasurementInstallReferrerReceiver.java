package com.google.android.gms.measurement;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.google.android.gms.measurement.internal.ah;
import com.google.android.gms.measurement.internal.ak;

public final class AppMeasurementInstallReferrerReceiver extends BroadcastReceiver implements ak {

    /* renamed from: a  reason: collision with root package name */
    private ah f2354a;

    public final void a(Context context, Intent intent) {
    }

    public final void onReceive(Context context, Intent intent) {
        if (this.f2354a == null) {
            this.f2354a = new ah(this);
        }
        this.f2354a.a(context, intent);
    }

    public final BroadcastReceiver.PendingResult a() {
        return goAsync();
    }
}
