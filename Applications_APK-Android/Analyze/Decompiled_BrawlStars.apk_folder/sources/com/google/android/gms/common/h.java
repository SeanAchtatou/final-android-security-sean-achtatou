package com.google.android.gms.common;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import com.google.android.gms.common.internal.l;

public class h extends DialogFragment {

    /* renamed from: a  reason: collision with root package name */
    private Dialog f1539a = null;

    /* renamed from: b  reason: collision with root package name */
    private DialogInterface.OnCancelListener f1540b = null;

    public Dialog onCreateDialog(Bundle bundle) {
        if (this.f1539a == null) {
            setShowsDialog(false);
        }
        return this.f1539a;
    }

    public void onCancel(DialogInterface dialogInterface) {
        DialogInterface.OnCancelListener onCancelListener = this.f1540b;
        if (onCancelListener != null) {
            onCancelListener.onCancel(dialogInterface);
        }
    }

    public static h a(Dialog dialog, DialogInterface.OnCancelListener onCancelListener) {
        h hVar = new h();
        Dialog dialog2 = (Dialog) l.a(dialog, "Cannot display null dialog");
        dialog2.setOnCancelListener(null);
        dialog2.setOnDismissListener(null);
        hVar.f1539a = dialog2;
        if (onCancelListener != null) {
            hVar.f1540b = onCancelListener;
        }
        return hVar;
    }

    public void show(FragmentManager fragmentManager, String str) {
        super.show(fragmentManager, str);
    }
}
