package com.google.android.gms.internal.drive;

import android.os.IBinder;
import android.os.IInterface;

public class zza implements IInterface {

    /* renamed from: a  reason: collision with root package name */
    private final IBinder f1921a;

    /* renamed from: b  reason: collision with root package name */
    private final String f1922b;

    protected zza(IBinder iBinder, String str) {
        this.f1921a = iBinder;
        this.f1922b = str;
    }

    public IBinder asBinder() {
        return this.f1921a;
    }
}
