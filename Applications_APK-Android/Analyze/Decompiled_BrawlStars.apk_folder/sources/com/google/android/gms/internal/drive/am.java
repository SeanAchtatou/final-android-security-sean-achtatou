package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class am implements Parcelable.Creator<zzgo> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zzgo[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        zzei zzei = null;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            if ((65535 & readInt) != 2) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                zzei = (zzei) SafeParcelReader.a(parcel, readInt, zzei.CREATOR);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new zzgo(zzei);
    }
}
