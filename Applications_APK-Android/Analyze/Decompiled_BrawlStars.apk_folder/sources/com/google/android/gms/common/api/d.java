package com.google.android.gms.common.api;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.util.ArrayMap;
import android.view.View;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.internal.ag;
import com.google.android.gms.common.api.internal.bj;
import com.google.android.gms.common.api.internal.bt;
import com.google.android.gms.common.api.internal.c;
import com.google.android.gms.common.api.internal.ca;
import com.google.android.gms.common.api.internal.f;
import com.google.android.gms.common.internal.b;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.signin.e;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.locks.ReentrantLock;

public abstract class d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final Set<d> f1375a = Collections.newSetFromMap(new WeakHashMap());

    public interface b {
        void onConnected(Bundle bundle);

        void onConnectionSuspended(int i);
    }

    public interface c {
        void onConnectionFailed(ConnectionResult connectionResult);
    }

    public abstract void a(c cVar);

    public abstract void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

    public abstract void b();

    public abstract void b(c cVar);

    public abstract boolean b(a<?> aVar);

    public abstract void c();

    public abstract void d();

    public abstract boolean e();

    public abstract boolean f();

    public <A extends a.b, T extends c.a<? extends g, A>> T a(T t) {
        throw new UnsupportedOperationException();
    }

    public <C extends a.f> C a(a.c<C> cVar) {
        throw new UnsupportedOperationException();
    }

    public boolean a(a<?> aVar) {
        throw new UnsupportedOperationException();
    }

    public Looper a() {
        throw new UnsupportedOperationException();
    }

    public void a(int i) {
        throw new UnsupportedOperationException();
    }

    public void a(bj bjVar) {
        throw new UnsupportedOperationException();
    }

    public void b(bj bjVar) {
        throw new UnsupportedOperationException();
    }

    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        public final Set<Scope> f1376a = new HashSet();

        /* renamed from: b  reason: collision with root package name */
        public final Set<Scope> f1377b = new HashSet();
        public View c;
        public final Map<a<?>, a.d> d = new ArrayMap();
        public final ArrayList<b> e = new ArrayList<>();
        public final ArrayList<c> f = new ArrayList<>();
        private Account g;
        private int h;
        private String i;
        private String j;
        private final Map<a<?>, b.C0115b> k = new ArrayMap();
        private final Context l;
        private f m;
        private int n = -1;
        private c o;
        private Looper p;
        private com.google.android.gms.common.c q = com.google.android.gms.common.c.a();
        private a.C0111a<? extends e, com.google.android.gms.signin.a> r = com.google.android.gms.signin.b.f2607a;
        private boolean s = false;

        public a(Context context) {
            this.l = context;
            this.p = context.getMainLooper();
            this.i = context.getPackageName();
            this.j = context.getClass().getName();
        }

        public final com.google.android.gms.common.internal.b a() {
            com.google.android.gms.signin.a aVar = com.google.android.gms.signin.a.f2605a;
            if (this.d.containsKey(com.google.android.gms.signin.b.f2608b)) {
                aVar = (com.google.android.gms.signin.a) this.d.get(com.google.android.gms.signin.b.f2608b);
            }
            return new com.google.android.gms.common.internal.b(this.g, this.f1376a, this.k, this.h, this.c, this.i, this.j, aVar);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.common.api.internal.ag.a(java.lang.Iterable<com.google.android.gms.common.api.a$f>, boolean):int
         arg types: [java.util.Collection, int]
         candidates:
          com.google.android.gms.common.api.internal.ag.a(int, boolean):void
          com.google.android.gms.common.api.internal.ba.a(int, boolean):void
          com.google.android.gms.common.api.internal.ag.a(java.lang.Iterable<com.google.android.gms.common.api.a$f>, boolean):int */
        public final d b() {
            l.b(!this.d.isEmpty(), "must call addApi() to add at least one API");
            com.google.android.gms.common.internal.b a2 = a();
            Map<a<?>, b.C0115b> map = a2.d;
            ArrayMap arrayMap = new ArrayMap();
            ArrayMap arrayMap2 = new ArrayMap();
            ArrayList arrayList = new ArrayList();
            for (a next : this.d.keySet()) {
                a.d dVar = this.d.get(next);
                boolean z = map.get(next) != null;
                arrayMap.put(next, Boolean.valueOf(z));
                ca caVar = new ca(next, z);
                arrayList.add(caVar);
                arrayMap2.put(next.b(), next.a().a(this.l, this.p, a2, dVar, caVar, caVar));
            }
            ag agVar = new ag(this.l, new ReentrantLock(), this.p, a2, this.q, this.r, arrayMap, this.e, this.f, arrayMap2, this.n, ag.a((Iterable<a.f>) arrayMap2.values(), true), arrayList);
            synchronized (d.f1375a) {
                d.f1375a.add(agVar);
            }
            if (this.n >= 0) {
                bt.b(this.m).a(this.n, agVar, this.o);
            }
            return agVar;
        }
    }
}
