package com.google.android.gms.measurement.internal;

public final class cg {

    /* renamed from: a  reason: collision with root package name */
    public final String f2461a;

    /* renamed from: b  reason: collision with root package name */
    public final String f2462b;
    public final long c;
    boolean d = false;

    public cg(String str, String str2, long j) {
        this.f2461a = str;
        this.f2462b = str2;
        this.c = j;
    }
}
