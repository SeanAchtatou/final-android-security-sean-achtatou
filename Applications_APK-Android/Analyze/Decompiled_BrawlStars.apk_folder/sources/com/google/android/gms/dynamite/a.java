package com.google.android.gms.dynamite;

import android.content.Context;
import com.google.android.gms.dynamite.DynamiteModule;

final class a implements DynamiteModule.a.C0116a {
    a() {
    }

    public final int a(Context context, String str, boolean z) throws DynamiteModule.LoadingException {
        return DynamiteModule.a(context, str, z);
    }

    public final int a(Context context, String str) {
        return DynamiteModule.a(context, str);
    }
}
