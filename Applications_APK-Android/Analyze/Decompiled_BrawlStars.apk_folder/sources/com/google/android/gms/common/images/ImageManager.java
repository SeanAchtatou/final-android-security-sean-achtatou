package com.google.android.gms.common.images;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelFileDescriptor;
import android.os.ResultReceiver;
import android.os.SystemClock;
import android.support.v4.util.LruCache;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;

public final class ImageManager {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final Object f1541a = new Object();
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static HashSet<Uri> f1542b = new HashSet<>();
    /* access modifiers changed from: private */
    public final Context c;
    /* access modifiers changed from: private */
    public final Handler d;
    /* access modifiers changed from: private */
    public final ExecutorService e;
    /* access modifiers changed from: private */
    public final b f;
    /* access modifiers changed from: private */
    public final com.google.android.gms.internal.base.c g;
    /* access modifiers changed from: private */
    public final Map<a, ImageReceiver> h;
    /* access modifiers changed from: private */
    public final Map<Uri, ImageReceiver> i;
    /* access modifiers changed from: private */
    public final Map<Uri, Long> j;

    public interface a {
    }

    static final class b extends LruCache<b, Bitmap> {
        public final /* synthetic */ int sizeOf(Object obj, Object obj2) {
            Bitmap bitmap = (Bitmap) obj2;
            return bitmap.getHeight() * bitmap.getRowBytes();
        }

        public final /* synthetic */ void entryRemoved(boolean z, Object obj, Object obj2, Object obj3) {
            super.entryRemoved(z, (b) obj, (Bitmap) obj2, (Bitmap) obj3);
        }
    }

    final class ImageReceiver extends ResultReceiver {

        /* renamed from: a  reason: collision with root package name */
        private final Uri f1543a;
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public final ArrayList<a> f1544b;
        private final /* synthetic */ ImageManager c;

        public final void onReceiveResult(int i, Bundle bundle) {
            this.c.e.execute(new c(this.f1543a, (ParcelFileDescriptor) bundle.getParcelable("com.google.android.gms.extra.fileDescriptor")));
        }
    }

    final class d implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private final Uri f1547a;

        /* renamed from: b  reason: collision with root package name */
        private final Bitmap f1548b;
        private final CountDownLatch c;
        private boolean d;

        public d(Uri uri, Bitmap bitmap, boolean z, CountDownLatch countDownLatch) {
            this.f1547a = uri;
            this.f1548b = bitmap;
            this.d = z;
            this.c = countDownLatch;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.common.images.a.a(android.content.Context, android.graphics.Bitmap, boolean):void
         arg types: [android.content.Context, android.graphics.Bitmap, int]
         candidates:
          com.google.android.gms.common.images.a.a(android.graphics.drawable.Drawable, boolean, boolean):void
          com.google.android.gms.common.images.a.a(android.content.Context, android.graphics.Bitmap, boolean):void */
        public final void run() {
            if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
                boolean z = this.f1548b != null;
                if (ImageManager.this.f != null) {
                    if (this.d) {
                        ImageManager.this.f.evictAll();
                        System.gc();
                        this.d = false;
                        ImageManager.this.d.post(this);
                        return;
                    } else if (z) {
                        ImageManager.this.f.put(new b(this.f1547a), this.f1548b);
                    }
                }
                ImageReceiver imageReceiver = (ImageReceiver) ImageManager.this.i.remove(this.f1547a);
                if (imageReceiver != null) {
                    ArrayList a2 = imageReceiver.f1544b;
                    int size = a2.size();
                    for (int i = 0; i < size; i++) {
                        a aVar = (a) a2.get(i);
                        if (z) {
                            aVar.a(ImageManager.this.c, this.f1548b, false);
                        } else {
                            ImageManager.this.j.put(this.f1547a, Long.valueOf(SystemClock.elapsedRealtime()));
                            Context b2 = ImageManager.this.c;
                            com.google.android.gms.internal.base.c unused = ImageManager.this.g;
                            aVar.a(b2, false);
                        }
                        if (!(aVar instanceof c)) {
                            ImageManager.this.h.remove(aVar);
                        }
                    }
                }
                this.c.countDown();
                synchronized (ImageManager.f1541a) {
                    ImageManager.f1542b.remove(this.f1547a);
                }
                return;
            }
            String valueOf = String.valueOf(Thread.currentThread());
            String valueOf2 = String.valueOf(Looper.getMainLooper().getThread());
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 57 + String.valueOf(valueOf2).length());
            sb.append("checkMainThread: current thread ");
            sb.append(valueOf);
            sb.append(" IS NOT the main thread ");
            sb.append(valueOf2);
            sb.append("!");
            sb.toString();
            throw new IllegalStateException("OnBitmapLoadedRunnable must be executed in the main thread");
        }
    }

    final class c implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private final Uri f1545a;

        /* renamed from: b  reason: collision with root package name */
        private final ParcelFileDescriptor f1546b;

        public c(Uri uri, ParcelFileDescriptor parcelFileDescriptor) {
            this.f1545a = uri;
            this.f1546b = parcelFileDescriptor;
        }

        public final void run() {
            boolean z;
            Bitmap bitmap;
            if (Looper.getMainLooper().getThread() != Thread.currentThread()) {
                boolean z2 = false;
                Bitmap bitmap2 = null;
                ParcelFileDescriptor parcelFileDescriptor = this.f1546b;
                if (parcelFileDescriptor != null) {
                    try {
                        bitmap2 = BitmapFactory.decodeFileDescriptor(parcelFileDescriptor.getFileDescriptor());
                    } catch (OutOfMemoryError unused) {
                        String valueOf = String.valueOf(this.f1545a);
                        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 34);
                        sb.append("OOM while loading bitmap for uri: ");
                        sb.append(valueOf);
                        sb.toString();
                        z2 = true;
                    }
                    try {
                        this.f1546b.close();
                    } catch (IOException unused2) {
                    }
                    z = z2;
                    bitmap = bitmap2;
                } else {
                    bitmap = null;
                    z = false;
                }
                CountDownLatch countDownLatch = new CountDownLatch(1);
                ImageManager.this.d.post(new d(this.f1545a, bitmap, z, countDownLatch));
                try {
                    countDownLatch.await();
                } catch (InterruptedException unused3) {
                    String valueOf2 = String.valueOf(this.f1545a);
                    StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 32);
                    sb2.append("Latch interrupted while posting ");
                    sb2.append(valueOf2);
                    sb2.toString();
                }
            } else {
                String valueOf3 = String.valueOf(Thread.currentThread());
                String valueOf4 = String.valueOf(Looper.getMainLooper().getThread());
                StringBuilder sb3 = new StringBuilder(String.valueOf(valueOf3).length() + 56 + String.valueOf(valueOf4).length());
                sb3.append("checkNotMainThread: current thread ");
                sb3.append(valueOf3);
                sb3.append(" IS the main thread ");
                sb3.append(valueOf4);
                sb3.append("!");
                sb3.toString();
                throw new IllegalStateException("LoadBitmapFromDiskRunnable can't be executed in the main thread");
            }
        }
    }
}
