package com.google.android.gms.measurement.internal;

import java.util.List;
import java.util.concurrent.Callable;

final class bj implements Callable<List<ec>> {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zzk f2425a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ zzby f2426b;

    bj(zzby zzby, zzk zzk) {
        this.f2426b = zzby;
        this.f2425a = zzk;
    }

    public final /* synthetic */ Object call() throws Exception {
        this.f2426b.f2597a.k();
        return this.f2426b.f2597a.d().a(this.f2425a.f2601a);
    }
}
