package com.google.android.gms.games.internal;

import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.internal.games.i;
import com.google.android.gms.internal.games.zza;

public final class zzz extends zza implements zzy {
    zzz(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.games.internal.IGamesService");
    }

    public final Bundle a() throws RemoteException {
        Parcel a2 = a(5004, h());
        Bundle bundle = (Bundle) i.a(a2, Bundle.CREATOR);
        a2.recycle();
        return bundle;
    }

    public final void a(long j) throws RemoteException {
        Parcel h = h();
        h.writeLong(j);
        b(5001, h);
    }

    public final void a(IBinder iBinder, Bundle bundle) throws RemoteException {
        Parcel h = h();
        h.writeStrongBinder(iBinder);
        i.a(h, bundle);
        b(5005, h);
    }

    public final void a(zzu zzu) throws RemoteException {
        Parcel h = h();
        i.a(h, zzu);
        b(5002, h);
    }

    public final void a(zzu zzu, String str, IBinder iBinder, Bundle bundle) throws RemoteException {
        Parcel h = h();
        i.a(h, zzu);
        h.writeString(str);
        h.writeStrongBinder(iBinder);
        i.a(h, bundle);
        b(5024, h);
    }

    public final void a(zzw zzw, long j) throws RemoteException {
        Parcel h = h();
        i.a(h, zzw);
        h.writeLong(j);
        b(15501, h);
    }

    public final void a(String str, zzu zzu) throws RemoteException {
        Parcel h = h();
        h.writeString(str);
        i.a(h, zzu);
        b(20001, h);
    }

    public final void b() throws RemoteException {
        b(5006, h());
    }

    public final String c() throws RemoteException {
        Parcel a2 = a(5012, h());
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }

    public final DataHolder d() throws RemoteException {
        Parcel a2 = a(5013, h());
        DataHolder dataHolder = (DataHolder) i.a(a2, DataHolder.CREATOR);
        a2.recycle();
        return dataHolder;
    }

    public final Intent e() throws RemoteException {
        Parcel a2 = a(9005, h());
        Intent intent = (Intent) i.a(a2, Intent.CREATOR);
        a2.recycle();
        return intent;
    }

    public final Intent f() throws RemoteException {
        Parcel a2 = a(19002, h());
        Intent intent = (Intent) i.a(a2, Intent.CREATOR);
        a2.recycle();
        return intent;
    }

    public final boolean g() throws RemoteException {
        Parcel a2 = a(22030, h());
        boolean a3 = i.a(a2);
        a2.recycle();
        return a3;
    }
}
