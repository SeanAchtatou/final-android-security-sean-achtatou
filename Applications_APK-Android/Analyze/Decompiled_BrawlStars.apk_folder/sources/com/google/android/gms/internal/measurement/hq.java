package com.google.android.gms.internal.measurement;

import java.util.Iterator;

final class hq {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final Iterator<Object> f2270a = new hr();

    /* renamed from: b  reason: collision with root package name */
    private static final Iterable<Object> f2271b = new hs();

    static <T> Iterable<T> a() {
        return f2271b;
    }
}
