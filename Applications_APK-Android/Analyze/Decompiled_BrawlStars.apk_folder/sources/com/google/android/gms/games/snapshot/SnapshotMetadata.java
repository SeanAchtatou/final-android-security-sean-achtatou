package com.google.android.gms.games.snapshot;

import android.net.Uri;
import android.os.Parcelable;
import com.google.android.gms.common.data.e;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.Player;

public interface SnapshotMetadata extends Parcelable, e<SnapshotMetadata> {
    Game b();

    Player c();

    String d();

    Uri e();

    float f();

    String g();

    @Deprecated
    String getCoverImageUrl();

    String h();

    String i();

    long j();

    long k();

    boolean l();

    long m();

    String n();
}
