package com.google.android.gms.internal.measurement;

import java.util.Map;

final class ht implements Comparable<ht>, Map.Entry<K, V> {

    /* renamed from: a  reason: collision with root package name */
    private final K f2272a;

    /* renamed from: b  reason: collision with root package name */
    private V f2273b;
    private final /* synthetic */ hm c;

    ht(hm hmVar, Map.Entry<K, V> entry) {
        this(hmVar, (Comparable) entry.getKey(), entry.getValue());
    }

    ht(hm hmVar, K k, V v) {
        this.c = hmVar;
        this.f2272a = k;
        this.f2273b = v;
    }

    public final V getValue() {
        return this.f2273b;
    }

    public final V setValue(V v) {
        this.c.e();
        V v2 = this.f2273b;
        this.f2273b = v;
        return v2;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        return a(this.f2272a, entry.getKey()) && a(this.f2273b, entry.getValue());
    }

    public final int hashCode() {
        K k = this.f2272a;
        int i = 0;
        int hashCode = k == null ? 0 : k.hashCode();
        V v = this.f2273b;
        if (v != null) {
            i = v.hashCode();
        }
        return hashCode ^ i;
    }

    public final String toString() {
        String valueOf = String.valueOf(this.f2272a);
        String valueOf2 = String.valueOf(this.f2273b);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 1 + String.valueOf(valueOf2).length());
        sb.append(valueOf);
        sb.append("=");
        sb.append(valueOf2);
        return sb.toString();
    }

    private static boolean a(Object obj, Object obj2) {
        if (obj == null) {
            return obj2 == null;
        }
        return obj.equals(obj2);
    }

    public final /* synthetic */ Object getKey() {
        return this.f2272a;
    }

    public final /* synthetic */ int compareTo(Object obj) {
        return ((Comparable) getKey()).compareTo((Comparable) ((ht) obj).getKey());
    }
}
