package com.google.android.gms.measurement.internal;

abstract class bn extends bm {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2431a;

    bn(ar arVar) {
        super(arVar);
        this.r.j++;
    }

    /* access modifiers changed from: protected */
    public abstract boolean d();

    /* access modifiers changed from: protected */
    public void e() {
    }

    /* access modifiers changed from: package-private */
    public final boolean v() {
        return this.f2431a;
    }

    /* access modifiers changed from: protected */
    public final void w() {
        if (!v()) {
            throw new IllegalStateException("Not initialized");
        }
    }

    public final void x() {
        if (this.f2431a) {
            throw new IllegalStateException("Can't initialize twice");
        } else if (!d()) {
            this.r.t();
            this.f2431a = true;
        }
    }

    public final void y() {
        if (!this.f2431a) {
            e();
            this.r.t();
            this.f2431a = true;
            return;
        }
        throw new IllegalStateException("Can't initialize twice");
    }
}
