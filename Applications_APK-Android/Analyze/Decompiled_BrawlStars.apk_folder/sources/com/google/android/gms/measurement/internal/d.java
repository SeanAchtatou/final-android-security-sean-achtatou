package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.l;

final class d {

    /* renamed from: a  reason: collision with root package name */
    final String f2497a;

    /* renamed from: b  reason: collision with root package name */
    final String f2498b;
    final long c;
    final long d;
    final long e;
    final long f;
    final Long g;
    final Long h;
    final Long i;
    final Boolean j;

    d(String str, String str2, long j2, long j3, long j4, long j5, Long l, Long l2, Long l3, Boolean bool) {
        long j6 = j2;
        long j7 = j3;
        long j8 = j5;
        l.a(str);
        l.a(str2);
        boolean z = true;
        l.b(j6 >= 0);
        l.b(j7 >= 0);
        l.b(j8 < 0 ? false : z);
        this.f2497a = str;
        this.f2498b = str2;
        this.c = j6;
        this.d = j7;
        this.e = j4;
        this.f = j8;
        this.g = l;
        this.h = l2;
        this.i = l3;
        this.j = bool;
    }

    /* access modifiers changed from: package-private */
    public final d a(long j2) {
        return new d(this.f2497a, this.f2498b, this.c, this.d, j2, this.f, this.g, this.h, this.i, this.j);
    }

    /* access modifiers changed from: package-private */
    public final d a(long j2, long j3) {
        return new d(this.f2497a, this.f2498b, this.c, this.d, this.e, j2, Long.valueOf(j3), this.h, this.i, this.j);
    }

    /* access modifiers changed from: package-private */
    public final d a(Long l, Long l2, Boolean bool) {
        return new d(this.f2497a, this.f2498b, this.c, this.d, this.e, this.f, this.g, l, l2, (bool == null || bool.booleanValue()) ? bool : null);
    }
}
