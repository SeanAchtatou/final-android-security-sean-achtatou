package com.google.android.gms.games.internal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.RemoteException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.d;
import com.google.android.gms.common.api.internal.c;
import com.google.android.gms.common.internal.BaseGmsClient;
import com.google.android.gms.common.internal.BinderWrapper;
import com.google.android.gms.common.internal.e;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.a;
import com.google.android.gms.games.achievement.a;
import com.google.android.gms.games.d;
import com.google.android.gms.games.f;
import com.google.android.gms.internal.games.s;
import java.util.HashSet;
import java.util.Set;

public class zze extends e<zzy> {
    private s i = new f(this);
    private final String j;
    private PlayerEntity k;
    private GameEntity l;
    private final b m;
    private boolean n = false;
    private final Binder o;
    private final long p;
    private final a.C0117a q;
    private boolean r = false;
    private Bundle s;

    static final class a extends zzb {

        /* renamed from: a  reason: collision with root package name */
        private final b f1821a;

        public a(b bVar) {
            this.f1821a = bVar;
        }

        public final zzaa a() {
            return new zzaa(this.f1821a.f1804b);
        }
    }

    static final class b extends zza {

        /* renamed from: a  reason: collision with root package name */
        private final c.b<Status> f1822a;

        public b(c.b<Status> bVar) {
            this.f1822a = (c.b) l.a(bVar, "Holder must not be null");
        }

        public final void a() {
            this.f1822a.a((Object) d.a(0));
        }
    }

    static final class c implements a.b {

        /* renamed from: a  reason: collision with root package name */
        private final Status f1823a;

        /* renamed from: b  reason: collision with root package name */
        private final String f1824b;

        c(Status status, String str) {
            this.f1823a = status;
            this.f1824b = str;
        }

        public final String a() {
            return this.f1824b;
        }

        public final Status b() {
            return this.f1823a;
        }
    }

    public static final class zzy extends zza {

        /* renamed from: a  reason: collision with root package name */
        private final c.b<a.b> f1825a;

        public zzy(c.b<a.b> bVar) {
            this.f1825a = (c.b) l.a(bVar, "Holder must not be null");
        }

        public final void a(int i, String str) {
            this.f1825a.a(new c(d.a(i), str));
        }
    }

    static void a(RemoteException remoteException) {
        h.a("GamesClientImpl", "service died", remoteException);
    }

    public static <R> void b(c.b bVar) {
        if (bVar != null) {
            bVar.a(com.google.android.gms.games.b.b(4));
        }
    }

    /* JADX INFO: finally extract failed */
    private Player x() throws RemoteException {
        o();
        synchronized (this) {
            if (this.k == null) {
                f fVar = new f(((zzy) p()).d());
                try {
                    if (fVar.b() > 0) {
                        this.k = (PlayerEntity) ((Player) fVar.a(0)).a();
                    }
                    fVar.a();
                } catch (Throwable th) {
                    fVar.a();
                    throw th;
                }
            }
        }
        return this.k;
    }

    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.games.internal.IGamesService");
        return queryLocalInterface instanceof zzy ? (zzy) queryLocalInterface : new zzz(iBinder);
    }

    public final Set<Scope> a(Set<Scope> set) {
        HashSet hashSet = new HashSet(set);
        boolean contains = set.contains(com.google.android.gms.games.a.f1793b);
        boolean contains2 = set.contains(com.google.android.gms.games.a.c);
        if (set.contains(com.google.android.gms.games.a.e)) {
            l.a(!contains, "Cannot have both %s and %s!", "https://www.googleapis.com/auth/games", "https://www.googleapis.com/auth/games.firstparty");
        } else {
            l.a(contains || contains2, "Games APIs requires %s function.", "https://www.googleapis.com/auth/games_lite");
            if (contains2 && contains) {
                hashSet.remove(com.google.android.gms.games.a.c);
            }
        }
        return hashSet;
    }

    public final void a() {
        this.n = false;
        if (b()) {
            try {
                zzy zzy2 = (zzy) p();
                zzy2.b();
                this.i.a();
                zzy2.a(this.p);
            } catch (RemoteException unused) {
                h.a("GamesClientImpl", "Failed to notify client disconnect.");
            }
        }
        super.a();
    }

    public final void a(int i2, IBinder iBinder, Bundle bundle, int i3) {
        if (i2 == 0 && bundle != null) {
            bundle.setClassLoader(zze.class.getClassLoader());
            this.n = bundle.getBoolean("show_welcome_popup");
            this.r = this.n;
            this.k = (PlayerEntity) bundle.getParcelable("com.google.android.gms.games.current_player");
            this.l = (GameEntity) bundle.getParcelable("com.google.android.gms.games.current_game");
        }
        super.a(i2, iBinder, bundle, i3);
    }

    public final /* synthetic */ void a(IInterface iInterface) {
        zzy zzy2 = (zzy) iInterface;
        super.a(zzy2);
        if (this.n) {
            this.m.a();
            this.n = false;
        }
        if (!this.q.f1794a && !this.q.i) {
            try {
                zzy2.a(new a(this.m), this.p);
            } catch (RemoteException e) {
                a(e);
            }
        }
    }

    public final void a(ConnectionResult connectionResult) {
        super.a(connectionResult);
        this.n = false;
    }

    public final void a(c.b<Status> bVar) throws RemoteException {
        this.i.a();
        try {
            ((zzy) p()).a(new b(bVar));
        } catch (SecurityException unused) {
            b(bVar);
        }
    }

    public final void a(c.b<a.C0119a> bVar, String str) throws RemoteException {
        try {
            ((zzy) p()).a(null, str, this.m.f1804b.f1805a, this.m.f1804b.a());
        } catch (SecurityException unused) {
            b((c.b) null);
        }
    }

    public final void a(BaseGmsClient.c cVar) {
        this.k = null;
        this.l = null;
        super.a(cVar);
    }

    public final void a(BaseGmsClient.e eVar) {
        try {
            a(new g(eVar));
        } catch (RemoteException unused) {
            eVar.a();
        }
    }

    public final Bundle a_() {
        try {
            Bundle a2 = ((zzy) p()).a();
            if (a2 != null) {
                a2.setClassLoader(zze.class.getClassLoader());
                this.s = a2;
            }
            return a2;
        } catch (RemoteException e) {
            a(e);
            return null;
        }
    }

    public final boolean d() {
        return true;
    }

    public final int g() {
        return com.google.android.gms.common.f.f1536b;
    }

    public final String i() {
        return "com.google.android.gms.games.service.START";
    }

    public final String j() {
        return "com.google.android.gms.games.internal.IGamesService";
    }

    public final Player s() {
        try {
            return x();
        } catch (RemoteException e) {
            a(e);
            return null;
        }
    }

    public final Intent t() {
        try {
            return ((zzy) p()).e();
        } catch (RemoteException e) {
            a(e);
            return null;
        }
    }

    public final void w() {
        if (b()) {
            try {
                ((zzy) p()).b();
            } catch (RemoteException e) {
                a(e);
            }
        }
    }

    public zze(Context context, Looper looper, com.google.android.gms.common.internal.b bVar, a.C0117a aVar, d.b bVar2, d.c cVar) {
        super(context, looper, 1, bVar, bVar2, cVar);
        this.j = bVar.g;
        this.o = new Binder();
        this.m = b.a(this, bVar.e);
        this.p = (long) hashCode();
        this.q = aVar;
        if (this.q.i) {
            return;
        }
        if (bVar.f != null || (context instanceof Activity)) {
            this.m.a(bVar.f);
        }
    }

    public final Bundle n() {
        String locale = this.c.getResources().getConfiguration().locale.toString();
        a.C0117a aVar = this.q;
        Bundle bundle = new Bundle();
        bundle.putBoolean("com.google.android.gms.games.key.isHeadless", aVar.f1794a);
        bundle.putBoolean("com.google.android.gms.games.key.showConnectingPopup", aVar.f1795b);
        bundle.putInt("com.google.android.gms.games.key.connectingPopupGravity", aVar.c);
        bundle.putBoolean("com.google.android.gms.games.key.retryingSignIn", aVar.d);
        bundle.putInt("com.google.android.gms.games.key.sdkVariant", aVar.e);
        bundle.putString("com.google.android.gms.games.key.forceResolveAccountKey", aVar.f);
        bundle.putStringArrayList("com.google.android.gms.games.key.proxyApis", aVar.g);
        bundle.putBoolean("com.google.android.gms.games.key.requireGooglePlus", aVar.h);
        bundle.putBoolean("com.google.android.gms.games.key.unauthenticated", aVar.i);
        bundle.putBoolean("com.google.android.gms.games.key.skipWelcomePopup", aVar.j);
        bundle.putString("com.google.android.gms.games.key.gamePackageName", this.j);
        bundle.putString("com.google.android.gms.games.key.desiredLocale", locale);
        bundle.putParcelable("com.google.android.gms.games.key.popupWindowToken", new BinderWrapper(this.m.f1804b.f1805a));
        bundle.putInt("com.google.android.gms.games.key.API_VERSION", 6);
        bundle.putBundle("com.google.android.gms.games.key.signInOptions", com.google.android.gms.signin.internal.a.a(this.h));
        return bundle;
    }

    public final String r() {
        try {
            return this.k != null ? this.k.b() : ((zzy) p()).c();
        } catch (RemoteException e) {
            a(e);
            return null;
        }
    }

    public final Intent u() {
        try {
            return ((zzy) p()).f();
        } catch (RemoteException e) {
            a(e);
            return null;
        }
    }

    public final boolean v() {
        try {
            return ((zzy) p()).g();
        } catch (RemoteException e) {
            a(e);
            return false;
        }
    }
}
