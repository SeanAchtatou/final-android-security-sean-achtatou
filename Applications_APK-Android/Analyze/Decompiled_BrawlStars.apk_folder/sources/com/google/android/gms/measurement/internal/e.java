package com.google.android.gms.measurement.internal;

import java.util.Iterator;

final class e implements Iterator<String> {

    /* renamed from: a  reason: collision with root package name */
    private Iterator<String> f2539a = this.f2540b.f2594a.keySet().iterator();

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ zzad f2540b;

    e(zzad zzad) {
        this.f2540b = zzad;
    }

    public final boolean hasNext() {
        return this.f2539a.hasNext();
    }

    public final void remove() {
        throw new UnsupportedOperationException("Remove not supported");
    }

    public final /* synthetic */ Object next() {
        return this.f2539a.next();
    }
}
