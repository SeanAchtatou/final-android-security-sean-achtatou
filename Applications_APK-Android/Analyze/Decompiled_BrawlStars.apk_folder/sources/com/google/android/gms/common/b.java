package com.google.android.gms.common;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;
import com.google.android.gms.common.internal.l;

public class b extends DialogFragment {

    /* renamed from: a  reason: collision with root package name */
    private Dialog f1510a = null;

    /* renamed from: b  reason: collision with root package name */
    private DialogInterface.OnCancelListener f1511b = null;

    public Dialog onCreateDialog(Bundle bundle) {
        if (this.f1510a == null) {
            setShowsDialog(false);
        }
        return this.f1510a;
    }

    public void onCancel(DialogInterface dialogInterface) {
        DialogInterface.OnCancelListener onCancelListener = this.f1511b;
        if (onCancelListener != null) {
            onCancelListener.onCancel(dialogInterface);
        }
    }

    public static b a(Dialog dialog, DialogInterface.OnCancelListener onCancelListener) {
        b bVar = new b();
        Dialog dialog2 = (Dialog) l.a(dialog, "Cannot display null dialog");
        dialog2.setOnCancelListener(null);
        dialog2.setOnDismissListener(null);
        bVar.f1510a = dialog2;
        if (onCancelListener != null) {
            bVar.f1511b = onCancelListener;
        }
        return bVar;
    }

    public void show(FragmentManager fragmentManager, String str) {
        super.show(fragmentManager, str);
    }
}
