package com.google.android.gms.games.achievement;

import android.net.Uri;
import android.os.Parcelable;
import com.google.android.gms.common.data.e;
import com.google.android.gms.games.Player;

public interface Achievement extends Parcelable, e<Achievement> {
    String b();

    int c();

    String d();

    String e();

    Uri f();

    Uri g();

    @Deprecated
    String getRevealedImageUrl();

    @Deprecated
    String getUnlockedImageUrl();

    int h();

    String i();

    Player j();

    int k();

    int l();

    String m();

    long n();

    long o();
}
