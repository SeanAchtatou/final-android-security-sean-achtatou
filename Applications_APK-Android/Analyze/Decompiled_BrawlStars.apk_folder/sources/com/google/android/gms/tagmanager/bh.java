package com.google.android.gms.tagmanager;

import android.content.ComponentCallbacks2;
import android.content.res.Configuration;

final class bh implements ComponentCallbacks2 {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ d f2642a;

    bh(d dVar) {
        this.f2642a = dVar;
    }

    public final void onConfigurationChanged(Configuration configuration) {
    }

    public final void onLowMemory() {
    }

    public final void onTrimMemory(int i) {
        if (i == 20) {
            this.f2642a.f2654a.a();
        }
    }
}
