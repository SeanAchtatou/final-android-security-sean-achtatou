package com.google.android.gms.games.stats;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class b implements Parcelable.Creator<zza> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zza[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int a2 = SafeParcelReader.a(parcel);
        Bundle bundle = null;
        float f = 0.0f;
        float f2 = 0.0f;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        float f3 = 0.0f;
        float f4 = 0.0f;
        float f5 = 0.0f;
        float f6 = 0.0f;
        float f7 = 0.0f;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    f = SafeParcelReader.i(parcel2, readInt);
                    break;
                case 2:
                    f2 = SafeParcelReader.i(parcel2, readInt);
                    break;
                case 3:
                    i = SafeParcelReader.d(parcel2, readInt);
                    break;
                case 4:
                    i2 = SafeParcelReader.d(parcel2, readInt);
                    break;
                case 5:
                    i3 = SafeParcelReader.d(parcel2, readInt);
                    break;
                case 6:
                    f3 = SafeParcelReader.i(parcel2, readInt);
                    break;
                case 7:
                    f4 = SafeParcelReader.i(parcel2, readInt);
                    break;
                case 8:
                    bundle = SafeParcelReader.n(parcel2, readInt);
                    break;
                case 9:
                    f5 = SafeParcelReader.i(parcel2, readInt);
                    break;
                case 10:
                    f6 = SafeParcelReader.i(parcel2, readInt);
                    break;
                case 11:
                    f7 = SafeParcelReader.i(parcel2, readInt);
                    break;
                default:
                    SafeParcelReader.b(parcel2, readInt);
                    break;
            }
        }
        SafeParcelReader.x(parcel2, a2);
        return new zza(f, f2, i, i2, i3, f3, f4, bundle, f5, f6, f7);
    }
}
