package com.google.android.gms.base;

public final class R {

    public static final class attr {
        public static final int buttonSize = 2130903128;
        public static final int circleCrop = 2130903165;
        public static final int colorScheme = 2130903188;
        public static final int imageAspectRatio = 2130903382;
        public static final int imageAspectRatioAdjust = 2130903383;
        public static final int scopeUris = 2130903527;

        private attr() {
        }
    }

    public static final class color {
        public static final int common_google_signin_btn_text_dark = 2131034189;
        public static final int common_google_signin_btn_text_dark_default = 2131034190;
        public static final int common_google_signin_btn_text_dark_disabled = 2131034191;
        public static final int common_google_signin_btn_text_dark_focused = 2131034192;
        public static final int common_google_signin_btn_text_dark_pressed = 2131034193;
        public static final int common_google_signin_btn_text_light = 2131034194;
        public static final int common_google_signin_btn_text_light_default = 2131034195;
        public static final int common_google_signin_btn_text_light_disabled = 2131034196;
        public static final int common_google_signin_btn_text_light_focused = 2131034197;
        public static final int common_google_signin_btn_text_light_pressed = 2131034198;
        public static final int common_google_signin_btn_tint = 2131034199;

        private color() {
        }
    }

    public static final class drawable {
        public static final int common_full_open_on_phone = 2131165333;
        public static final int common_google_signin_btn_icon_dark = 2131165334;
        public static final int common_google_signin_btn_icon_dark_focused = 2131165335;
        public static final int common_google_signin_btn_icon_dark_normal = 2131165336;
        public static final int common_google_signin_btn_icon_dark_normal_background = 2131165337;
        public static final int common_google_signin_btn_icon_disabled = 2131165338;
        public static final int common_google_signin_btn_icon_light = 2131165339;
        public static final int common_google_signin_btn_icon_light_focused = 2131165340;
        public static final int common_google_signin_btn_icon_light_normal = 2131165341;
        public static final int common_google_signin_btn_icon_light_normal_background = 2131165342;
        public static final int common_google_signin_btn_text_dark = 2131165343;
        public static final int common_google_signin_btn_text_dark_focused = 2131165344;
        public static final int common_google_signin_btn_text_dark_normal = 2131165345;
        public static final int common_google_signin_btn_text_dark_normal_background = 2131165346;
        public static final int common_google_signin_btn_text_disabled = 2131165347;
        public static final int common_google_signin_btn_text_light = 2131165348;
        public static final int common_google_signin_btn_text_light_focused = 2131165349;
        public static final int common_google_signin_btn_text_light_normal = 2131165350;
        public static final int common_google_signin_btn_text_light_normal_background = 2131165351;
        public static final int googleg_disabled_color_18 = 2131165370;
        public static final int googleg_standard_color_18 = 2131165371;

        private drawable() {
        }
    }

    public static final class id {
        public static final int adjust_height = 2131230770;
        public static final int adjust_width = 2131230771;
        public static final int auto = 2131230803;
        public static final int dark = 2131230906;
        public static final int icon_only = 2131231069;
        public static final int light = 2131231113;
        public static final int none = 2131231180;
        public static final int standard = 2131231371;
        public static final int wide = 2131231495;

        private id() {
        }
    }

    public static final class string {
        public static final int common_google_play_services_enable_button = 2131689544;
        public static final int common_google_play_services_enable_text = 2131689545;
        public static final int common_google_play_services_enable_title = 2131689546;
        public static final int common_google_play_services_install_button = 2131689547;
        public static final int common_google_play_services_install_text = 2131689548;
        public static final int common_google_play_services_install_title = 2131689549;
        public static final int common_google_play_services_notification_channel_name = 2131689550;
        public static final int common_google_play_services_notification_ticker = 2131689551;
        public static final int common_google_play_services_unsupported_text = 2131689553;
        public static final int common_google_play_services_update_button = 2131689554;
        public static final int common_google_play_services_update_text = 2131689555;
        public static final int common_google_play_services_update_title = 2131689556;
        public static final int common_google_play_services_updating_text = 2131689557;
        public static final int common_google_play_services_wear_update_text = 2131689558;
        public static final int common_open_on_phone = 2131689559;
        public static final int common_signin_button_text = 2131689560;
        public static final int common_signin_button_text_long = 2131689561;

        private string() {
        }
    }

    public static final class styleable {
        public static final int[] LoadingImageView = {com.supercell.brawlstars.R.attr.circleCrop, com.supercell.brawlstars.R.attr.imageAspectRatio, com.supercell.brawlstars.R.attr.imageAspectRatioAdjust};
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] SignInButton = {com.supercell.brawlstars.R.attr.buttonSize, com.supercell.brawlstars.R.attr.colorScheme, com.supercell.brawlstars.R.attr.scopeUris};
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;

        private styleable() {
        }
    }
}
