package com.google.android.gms.common.api.internal;

import android.app.Activity;
import com.google.android.gms.common.internal.l;

public class f {

    /* renamed from: a  reason: collision with root package name */
    final Object f1476a;

    public f(Activity activity) {
        l.a(activity, "Activity must not be null");
        this.f1476a = activity;
    }
}
