package com.google.android.gms.signin.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.ResolveAccountRequest;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class c implements Parcelable.Creator<zah> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zah[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        int i = 0;
        ResolveAccountRequest resolveAccountRequest = null;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            int i2 = 65535 & readInt;
            if (i2 == 1) {
                i = SafeParcelReader.d(parcel, readInt);
            } else if (i2 != 2) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                resolveAccountRequest = (ResolveAccountRequest) SafeParcelReader.a(parcel, readInt, ResolveAccountRequest.CREATOR);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new zah(i, resolveAccountRequest);
    }
}
