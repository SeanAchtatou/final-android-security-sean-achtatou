package com.google.android.gms.tagmanager;

import android.content.Context;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class d {
    private static d g;

    /* renamed from: a  reason: collision with root package name */
    final az f2654a;

    /* renamed from: b  reason: collision with root package name */
    final ConcurrentMap<String, bo> f2655b;
    private final a c;
    private final Context d;
    private final c e;
    private final f f;

    public interface a {
    }

    private d(Context context, a aVar, c cVar, az azVar) {
        if (context != null) {
            this.d = context.getApplicationContext();
            this.f2654a = azVar;
            this.c = aVar;
            this.f2655b = new ConcurrentHashMap();
            this.e = cVar;
            this.e.a(new bf(this));
            this.e.a(new be(this.d));
            this.f = new f();
            this.d.registerComponentCallbacks(new bh(this));
            e.a(this.d);
            return;
        }
        throw new NullPointerException("context cannot be null");
    }

    public static d a(Context context) {
        d dVar;
        synchronized (d.class) {
            if (g == null) {
                g = new d(context, new bg(), new c(new k(context)), ba.b());
            }
            dVar = g;
        }
        return dVar;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x007b, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized boolean a(android.net.Uri r7) {
        /*
            r6 = this;
            monitor-enter(r6)
            com.google.android.gms.tagmanager.an r0 = com.google.android.gms.tagmanager.an.a()     // Catch:{ all -> 0x007f }
            boolean r7 = r0.a(r7)     // Catch:{ all -> 0x007f }
            if (r7 == 0) goto L_0x007c
            java.lang.String r7 = r0.f2625b     // Catch:{ all -> 0x007f }
            int[] r1 = com.google.android.gms.tagmanager.bi.f2643a     // Catch:{ all -> 0x007f }
            com.google.android.gms.tagmanager.an$a r2 = r0.f2624a     // Catch:{ all -> 0x007f }
            int r2 = r2.ordinal()     // Catch:{ all -> 0x007f }
            r1 = r1[r2]     // Catch:{ all -> 0x007f }
            r2 = 0
            r3 = 1
            if (r1 == r3) goto L_0x006a
            r4 = 2
            if (r1 == r4) goto L_0x0022
            r4 = 3
            if (r1 == r4) goto L_0x0022
            goto L_0x007a
        L_0x0022:
            java.util.concurrent.ConcurrentMap<java.lang.String, com.google.android.gms.tagmanager.bo> r1 = r6.f2655b     // Catch:{ all -> 0x007f }
            java.util.Set r1 = r1.keySet()     // Catch:{ all -> 0x007f }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ all -> 0x007f }
        L_0x002c:
            boolean r4 = r1.hasNext()     // Catch:{ all -> 0x007f }
            if (r4 == 0) goto L_0x007a
            java.lang.Object r4 = r1.next()     // Catch:{ all -> 0x007f }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ all -> 0x007f }
            java.util.concurrent.ConcurrentMap<java.lang.String, com.google.android.gms.tagmanager.bo> r5 = r6.f2655b     // Catch:{ all -> 0x007f }
            java.lang.Object r5 = r5.get(r4)     // Catch:{ all -> 0x007f }
            com.google.android.gms.tagmanager.bo r5 = (com.google.android.gms.tagmanager.bo) r5     // Catch:{ all -> 0x007f }
            boolean r4 = r4.equals(r7)     // Catch:{ all -> 0x007f }
            if (r4 == 0) goto L_0x004f
            java.lang.String r4 = r0.c     // Catch:{ all -> 0x007f }
            r5.a(r4)     // Catch:{ all -> 0x007f }
            r5.c()     // Catch:{ all -> 0x007f }
            goto L_0x002c
        L_0x004f:
            boolean r4 = r5.c     // Catch:{ all -> 0x007f }
            if (r4 == 0) goto L_0x005b
            java.lang.String r4 = "setCtfeUrlPathAndQuery called on a released ContainerHolder."
            com.google.android.gms.tagmanager.ab.a(r4)     // Catch:{ all -> 0x007f }
            java.lang.String r4 = ""
            goto L_0x0061
        L_0x005b:
            com.google.android.gms.tagmanager.bp r4 = r5.f2649b     // Catch:{ all -> 0x007f }
            java.lang.String r4 = r4.a()     // Catch:{ all -> 0x007f }
        L_0x0061:
            if (r4 == 0) goto L_0x002c
            r5.a(r2)     // Catch:{ all -> 0x007f }
            r5.c()     // Catch:{ all -> 0x007f }
            goto L_0x002c
        L_0x006a:
            java.util.concurrent.ConcurrentMap<java.lang.String, com.google.android.gms.tagmanager.bo> r0 = r6.f2655b     // Catch:{ all -> 0x007f }
            java.lang.Object r7 = r0.get(r7)     // Catch:{ all -> 0x007f }
            com.google.android.gms.tagmanager.bo r7 = (com.google.android.gms.tagmanager.bo) r7     // Catch:{ all -> 0x007f }
            if (r7 == 0) goto L_0x007a
            r7.a(r2)     // Catch:{ all -> 0x007f }
            r7.c()     // Catch:{ all -> 0x007f }
        L_0x007a:
            monitor-exit(r6)
            return r3
        L_0x007c:
            r7 = 0
            monitor-exit(r6)
            return r7
        L_0x007f:
            r7 = move-exception
            monitor-exit(r6)
            goto L_0x0083
        L_0x0082:
            throw r7
        L_0x0083:
            goto L_0x0082
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.tagmanager.d.a(android.net.Uri):boolean");
    }

    static /* synthetic */ void a(d dVar, String str) {
        for (bo next : dVar.f2655b.values()) {
            if (!next.c) {
                next.f2648a.f2620b.a(str);
            }
        }
    }
}
