package com.google.android.gms.internal.instantapps;

import android.os.Parcel;
import android.os.Parcelable;

public class l {

    /* renamed from: a  reason: collision with root package name */
    private static final ClassLoader f2004a = l.class.getClassLoader();

    private l() {
    }

    public static boolean a(Parcel parcel) {
        return parcel.readInt() != 0;
    }

    public static <T extends Parcelable> T a(Parcel parcel, Parcelable.Creator<T> creator) {
        if (parcel.readInt() == 0) {
            return null;
        }
        return (Parcelable) creator.createFromParcel(parcel);
    }
}
