package com.google.android.gms.measurement.internal;

import android.content.SharedPreferences;
import com.google.android.gms.common.internal.l;

public final class ad {

    /* renamed from: a  reason: collision with root package name */
    final String f2366a;

    /* renamed from: b  reason: collision with root package name */
    boolean f2367b;
    String c;
    final /* synthetic */ z d;
    private final String e = null;

    public ad(z zVar, String str) {
        this.d = zVar;
        l.a(str);
        this.f2366a = str;
    }

    public final void a(String str) {
        if (!ed.c(str, this.c)) {
            SharedPreferences.Editor edit = this.d.f().edit();
            edit.putString(this.f2366a, str);
            edit.apply();
            this.c = str;
        }
    }
}
