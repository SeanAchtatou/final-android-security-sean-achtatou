package com.google.android.gms.tagmanager;

import com.google.android.gms.tagmanager.c;
import java.util.List;

final class h implements i {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ c f2659a;

    h(c cVar) {
        this.f2659a = cVar;
    }

    public final void a(List<c.a> list) {
        for (c.a next : list) {
            this.f2659a.b(c.a(next.f2652a, next.f2653b));
        }
        this.f2659a.i.countDown();
    }
}
