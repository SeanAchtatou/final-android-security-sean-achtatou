package com.google.android.gms.common.api;

import com.google.android.gms.common.api.g;

public abstract class e<R extends g> {

    public interface a {
        void a();
    }

    public abstract R a();

    public abstract void a(h hVar);

    public abstract void b();

    public abstract boolean c();

    public void a(a aVar) {
        throw new UnsupportedOperationException();
    }

    public Integer d() {
        throw new UnsupportedOperationException();
    }
}
