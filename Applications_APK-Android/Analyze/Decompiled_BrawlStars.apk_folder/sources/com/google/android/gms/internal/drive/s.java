package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.zza;
import java.util.ArrayList;

public final class s implements Parcelable.Creator<zzez> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zzez[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        DataHolder dataHolder = null;
        ArrayList arrayList = null;
        zza zza = null;
        boolean z = false;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            int i = 65535 & readInt;
            if (i == 2) {
                dataHolder = (DataHolder) SafeParcelReader.a(parcel, readInt, DataHolder.CREATOR);
            } else if (i == 3) {
                arrayList = SafeParcelReader.c(parcel, readInt, DriveId.CREATOR);
            } else if (i == 4) {
                zza = (zza) SafeParcelReader.a(parcel, readInt, zza.CREATOR);
            } else if (i != 5) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                z = SafeParcelReader.c(parcel, readInt);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new zzez(dataHolder, arrayList, zza, z);
    }
}
