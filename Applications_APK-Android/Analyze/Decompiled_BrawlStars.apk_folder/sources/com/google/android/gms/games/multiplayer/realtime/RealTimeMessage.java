package com.google.android.gms.games.multiplayer.realtime;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.l;

public final class RealTimeMessage implements Parcelable {
    public static final Parcelable.Creator<RealTimeMessage> CREATOR = new b();

    /* renamed from: a  reason: collision with root package name */
    private final String f1830a;

    /* renamed from: b  reason: collision with root package name */
    private final byte[] f1831b;
    private final int c;

    private RealTimeMessage(Parcel parcel) {
        this(parcel.readString(), parcel.createByteArray(), parcel.readInt());
    }

    /* synthetic */ RealTimeMessage(Parcel parcel, byte b2) {
        this(parcel);
    }

    private RealTimeMessage(String str, byte[] bArr, int i) {
        this.f1830a = (String) l.a((Object) str);
        this.f1831b = (byte[]) ((byte[]) l.a(bArr)).clone();
        this.c = i;
    }

    public final int describeContents() {
        return 0;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f1830a);
        parcel.writeByteArray(this.f1831b);
        parcel.writeInt(this.c);
    }
}
