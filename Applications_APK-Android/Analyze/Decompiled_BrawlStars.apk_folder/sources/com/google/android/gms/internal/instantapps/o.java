package com.google.android.gms.internal.instantapps;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class o implements Parcelable.Creator<zzi> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zzi[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        String str = null;
        String str2 = null;
        String[] strArr = null;
        int[] iArr = null;
        byte[] bArr = null;
        int i = 0;
        boolean z = false;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    str = SafeParcelReader.l(parcel, readInt);
                    break;
                case 3:
                default:
                    SafeParcelReader.b(parcel, readInt);
                    break;
                case 4:
                    str2 = SafeParcelReader.l(parcel, readInt);
                    break;
                case 5:
                    strArr = SafeParcelReader.t(parcel, readInt);
                    break;
                case 6:
                    i = SafeParcelReader.d(parcel, readInt);
                    break;
                case 7:
                    bArr = SafeParcelReader.o(parcel, readInt);
                    break;
                case 8:
                    iArr = SafeParcelReader.q(parcel, readInt);
                    break;
                case 9:
                    z = SafeParcelReader.c(parcel, readInt);
                    break;
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new zzi(str, str2, strArr, iArr, i, bArr, z);
    }
}
