package com.google.android.gms.common.api.internal;

final class cj implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ LifecycleCallback f1460a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f1461b;
    private final /* synthetic */ ci c;

    cj(ci ciVar, LifecycleCallback lifecycleCallback, String str) {
        this.c = ciVar;
        this.f1460a = lifecycleCallback;
        this.f1461b = str;
    }

    public final void run() {
        if (this.c.c > 0) {
            this.f1460a.a(this.c.d != null ? this.c.d.getBundle(this.f1461b) : null);
        }
        if (this.c.c >= 2) {
            this.f1460a.b();
        }
        if (this.c.c >= 3) {
            this.f1460a.c();
        }
        if (this.c.c >= 4) {
            this.f1460a.d();
        }
        int unused = this.c.c;
    }
}
