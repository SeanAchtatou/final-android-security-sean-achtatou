package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

final class gy<T> implements hj<T> {

    /* renamed from: a  reason: collision with root package name */
    private final gt f2252a;

    /* renamed from: b  reason: collision with root package name */
    private final ib<?, ?> f2253b;
    private final boolean c;
    private final fe<?> d;

    private gy(ib<?, ?> ibVar, fe<?> feVar, gt gtVar) {
        this.f2253b = ibVar;
        this.c = feVar.a(gtVar);
        this.d = feVar;
        this.f2252a = gtVar;
    }

    static <T> gy<T> a(ib<?, ?> ibVar, fe<?> feVar, gt gtVar) {
        return new gy<>(ibVar, feVar, gtVar);
    }

    public final T a() {
        return this.f2252a.j().c();
    }

    public final boolean a(T t, T t2) {
        if (!this.f2253b.b(t).equals(this.f2253b.b(t2))) {
            return false;
        }
        if (this.c) {
            return this.d.a((Object) t).equals(this.d.a((Object) t2));
        }
        return true;
    }

    public final int a(T t) {
        int hashCode = this.f2253b.b(t).hashCode();
        return this.c ? (hashCode * 53) + this.d.a((Object) t).hashCode() : hashCode;
    }

    public final void b(T t, T t2) {
        hl.a(this.f2253b, t, t2);
        if (this.c) {
            hl.a(this.d, t, t2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.iv.a(int, java.lang.Object):void
     arg types: [int, com.google.android.gms.internal.measurement.ej]
     candidates:
      com.google.android.gms.internal.measurement.iv.a(int, double):void
      com.google.android.gms.internal.measurement.iv.a(int, float):void
      com.google.android.gms.internal.measurement.iv.a(int, int):void
      com.google.android.gms.internal.measurement.iv.a(int, long):void
      com.google.android.gms.internal.measurement.iv.a(int, com.google.android.gms.internal.measurement.ej):void
      com.google.android.gms.internal.measurement.iv.a(int, java.lang.String):void
      com.google.android.gms.internal.measurement.iv.a(int, java.util.List<java.lang.String>):void
      com.google.android.gms.internal.measurement.iv.a(int, boolean):void
      com.google.android.gms.internal.measurement.iv.a(int, java.lang.Object):void */
    public final void a(T t, iv ivVar) throws IOException {
        Iterator<Map.Entry<?, Object>> c2 = this.d.a((Object) t).c();
        while (c2.hasNext()) {
            Map.Entry next = c2.next();
            fj fjVar = (fj) next.getKey();
            if (fjVar.c() != iu.MESSAGE || fjVar.d() || fjVar.e()) {
                throw new IllegalStateException("Found invalid MessageSet item.");
            } else if (next instanceof fz) {
                ivVar.a(fjVar.a(), (Object) ((fz) next).f2230a.getValue().c());
            } else {
                ivVar.a(fjVar.a(), next.getValue());
            }
        }
        ib<?, ?> ibVar = this.f2253b;
        ibVar.b(ibVar.b(t), ivVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.ib.a(java.lang.Object, com.google.android.gms.internal.measurement.hi):boolean
     arg types: [?, com.google.android.gms.internal.measurement.hi]
     candidates:
      com.google.android.gms.internal.measurement.ib.a(java.lang.Object, com.google.android.gms.internal.measurement.iv):void
      com.google.android.gms.internal.measurement.ib.a(java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.ib.a(java.lang.Object, com.google.android.gms.internal.measurement.hi):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.ib.a(java.lang.Object, int, com.google.android.gms.internal.measurement.ej):void
     arg types: [?, int, com.google.android.gms.internal.measurement.ej]
     candidates:
      com.google.android.gms.internal.measurement.ib.a(java.lang.Object, int, int):void
      com.google.android.gms.internal.measurement.ib.a(java.lang.Object, int, long):void
      com.google.android.gms.internal.measurement.ib.a(java.lang.Object, int, java.lang.Object):void
      com.google.android.gms.internal.measurement.ib.a(java.lang.Object, int, com.google.android.gms.internal.measurement.ej):void */
    public final void a(Object obj, hi hiVar, fd fdVar) throws IOException {
        boolean z;
        ib<?, ?> ibVar = this.f2253b;
        fe<?> feVar = this.d;
        Object c2 = ibVar.c(obj);
        feVar.b(obj);
        do {
            try {
                if (hiVar.a() == Integer.MAX_VALUE) {
                    ibVar.b(obj, c2);
                    return;
                }
                int b2 = hiVar.b();
                if (b2 == 11) {
                    int i = 0;
                    Object obj2 = null;
                    ej ejVar = null;
                    while (hiVar.a() != Integer.MAX_VALUE) {
                        int b3 = hiVar.b();
                        if (b3 == 16) {
                            i = hiVar.o();
                            obj2 = feVar.a(fdVar, this.f2252a, i);
                        } else if (b3 == 26) {
                            if (obj2 != null) {
                                feVar.b();
                            } else {
                                ejVar = hiVar.n();
                            }
                        } else if (!hiVar.c()) {
                            break;
                        }
                    }
                    if (hiVar.b() != 12) {
                        throw zzuv.d();
                    } else if (ejVar != null) {
                        if (obj2 != null) {
                            feVar.c();
                        } else {
                            ibVar.a((Object) c2, i, ejVar);
                        }
                    }
                } else if ((b2 & 7) != 2) {
                    z = hiVar.c();
                    continue;
                } else if (feVar.a(fdVar, this.f2252a, b2 >>> 3) != null) {
                    feVar.b();
                } else {
                    z = ibVar.a((Object) c2, hiVar);
                    continue;
                }
                z = true;
                continue;
            } finally {
                ibVar.b(obj, c2);
            }
        } while (z);
    }

    public final void c(T t) {
        this.f2253b.d(t);
        this.d.c(t);
    }

    public final boolean d(T t) {
        return this.d.a((Object) t).d();
    }

    public final int b(T t) {
        ib<?, ?> ibVar = this.f2253b;
        int e = ibVar.e(ibVar.b(t)) + 0;
        if (!this.c) {
            return e;
        }
        fh<?> a2 = this.d.a((Object) t);
        int i = 0;
        for (int i2 = 0; i2 < a2.f2206a.b(); i2++) {
            i += fh.b(a2.f2206a.b(i2));
        }
        for (Map.Entry<FieldDescriptorType, Object> b2 : a2.f2206a.c()) {
            i += fh.b(b2);
        }
        return e + i;
    }
}
