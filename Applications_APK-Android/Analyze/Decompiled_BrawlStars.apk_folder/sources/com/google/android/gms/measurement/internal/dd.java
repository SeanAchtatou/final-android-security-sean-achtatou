package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.content.Intent;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.measurement.internal.di;

public final class dd<T extends Context & di> {

    /* renamed from: a  reason: collision with root package name */
    public final T f2503a;

    public dd(T t) {
        l.a((Object) t);
        this.f2503a = t;
    }

    public final void a() {
        ar.a(this.f2503a, (j) null).q().k.a("Local AppMeasurementService is starting up");
    }

    public final void b() {
        ar.a(this.f2503a, (j) null).q().k.a("Local AppMeasurementService is shutting down");
    }

    public final void a(Runnable runnable) {
        du a2 = du.a((Context) this.f2503a);
        a2.p().a(new dh(a2, runnable));
    }

    public final boolean a(Intent intent) {
        if (intent == null) {
            c().c.a("onUnbind called with null intent");
            return true;
        }
        c().k.a("onUnbind called for intent. action", intent.getAction());
        return true;
    }

    public final void b(Intent intent) {
        if (intent == null) {
            c().c.a("onRebind called with null intent");
            return;
        }
        c().k.a("onRebind called. action", intent.getAction());
    }

    public final o c() {
        return ar.a(this.f2503a, (j) null).q();
    }
}
