package com.google.android.gms.games.multiplayer.realtime;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.games.multiplayer.ParticipantEntity;
import java.util.ArrayList;

public class c implements Parcelable.Creator<RoomEntity> {
    public /* synthetic */ Object[] newArray(int i) {
        return new RoomEntity[i];
    }

    /* renamed from: a */
    public RoomEntity createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int a2 = SafeParcelReader.a(parcel);
        String str = null;
        String str2 = null;
        String str3 = null;
        Bundle bundle = null;
        ArrayList arrayList = null;
        long j = 0;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    str = SafeParcelReader.l(parcel2, readInt);
                    break;
                case 2:
                    str2 = SafeParcelReader.l(parcel2, readInt);
                    break;
                case 3:
                    j = SafeParcelReader.f(parcel2, readInt);
                    break;
                case 4:
                    i = SafeParcelReader.d(parcel2, readInt);
                    break;
                case 5:
                    str3 = SafeParcelReader.l(parcel2, readInt);
                    break;
                case 6:
                    i2 = SafeParcelReader.d(parcel2, readInt);
                    break;
                case 7:
                    bundle = SafeParcelReader.n(parcel2, readInt);
                    break;
                case 8:
                    arrayList = SafeParcelReader.c(parcel2, readInt, ParticipantEntity.CREATOR);
                    break;
                case 9:
                    i3 = SafeParcelReader.d(parcel2, readInt);
                    break;
                default:
                    SafeParcelReader.b(parcel2, readInt);
                    break;
            }
        }
        SafeParcelReader.x(parcel2, a2);
        return new RoomEntity(str, str2, j, i, str3, i2, bundle, arrayList, i3);
    }
}
