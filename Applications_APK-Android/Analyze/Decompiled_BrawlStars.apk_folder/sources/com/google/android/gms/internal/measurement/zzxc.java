package com.google.android.gms.internal.measurement;

import java.util.List;

public final class zzxc extends RuntimeException {

    /* renamed from: a  reason: collision with root package name */
    private final List<String> f2352a = null;

    public zzxc() {
        super("Message was missing required fields.  (Lite runtime could not determine which fields were missing).");
    }
}
