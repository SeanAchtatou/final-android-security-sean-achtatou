package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.fj;

public interface fj<T extends fj<T>> extends Comparable<T> {
    int a();

    ip b();

    iu c();

    boolean d();

    boolean e();

    gu f();

    gz g();
}
