package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.tasks.h;

public final class zzhd extends zzhb<Object> {
    public final void a(zzfh zzfh) throws RemoteException {
        h<T> hVar = this.f1976a;
        DriveId driveId = zzfh.f1942a;
        if (driveId.f1696a != 1) {
            hVar.a((Boolean) new e(driveId));
            return;
        }
        throw new IllegalStateException("This DriveId corresponds to a folder. Call asDriveFolder instead.");
    }
}
