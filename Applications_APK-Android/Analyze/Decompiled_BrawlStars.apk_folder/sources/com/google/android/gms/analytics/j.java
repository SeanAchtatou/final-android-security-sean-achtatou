package com.google.android.gms.analytics;

import java.util.Comparator;

final class j implements Comparator<l> {
    j() {
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        return ((l) obj).getClass().getCanonicalName().compareTo(((l) obj2).getClass().getCanonicalName());
    }
}
