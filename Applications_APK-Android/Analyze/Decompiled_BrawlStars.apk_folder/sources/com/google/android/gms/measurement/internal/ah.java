package com.google.android.gms.measurement.internal;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import com.google.android.gms.common.internal.l;

public final class ah {

    /* renamed from: a  reason: collision with root package name */
    private final ak f2373a;

    public ah(ak akVar) {
        l.a(akVar);
        this.f2373a = akVar;
    }

    public static boolean a(Context context) {
        ActivityInfo receiverInfo;
        l.a(context);
        try {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager == null || (receiverInfo = packageManager.getReceiverInfo(new ComponentName(context, "com.google.android.gms.measurement.AppMeasurementReceiver"), 0)) == null || !receiverInfo.enabled) {
                return false;
            }
            return true;
        } catch (PackageManager.NameNotFoundException unused) {
        }
    }

    public final void a(Context context, Intent intent) {
        ar a2 = ar.a(context, (j) null);
        o q = a2.q();
        if (intent == null) {
            q.f.a("Receiver called with null intent");
            return;
        }
        String action = intent.getAction();
        q.k.a("Local receiver got", action);
        if ("com.google.android.gms.measurement.UPLOAD".equals(action)) {
            Intent className = new Intent().setClassName(context, "com.google.android.gms.measurement.AppMeasurementService");
            className.setAction("com.google.android.gms.measurement.UPLOAD");
            q.k.a("Starting wakeful intent.");
            this.f2373a.a(context, className);
        } else if ("com.android.vending.INSTALL_REFERRER".equals(action)) {
            try {
                a2.p().a(new ai(a2, q));
            } catch (Exception e) {
                q.f.a("Install Referrer Reporter encountered a problem", e);
            }
            BroadcastReceiver.PendingResult a3 = this.f2373a.a();
            String stringExtra = intent.getStringExtra("referrer");
            if (stringExtra == null) {
                q.k.a("Install referrer extras are null");
                if (a3 != null) {
                    a3.finish();
                    return;
                }
                return;
            }
            q.i.a("Install referrer extras are", stringExtra);
            if (!stringExtra.contains("?")) {
                String valueOf = String.valueOf(stringExtra);
                stringExtra = valueOf.length() != 0 ? "?".concat(valueOf) : new String("?");
            }
            Bundle a4 = a2.e().a(Uri.parse(stringExtra));
            if (a4 == null) {
                q.k.a("No campaign defined in install referrer broadcast");
                if (a3 != null) {
                    a3.finish();
                    return;
                }
                return;
            }
            long longExtra = intent.getLongExtra("referrer_timestamp_seconds", 0) * 1000;
            if (longExtra == 0) {
                q.f.a("Install referrer is missing timestamp");
            }
            a2.p().a(new aj(a2, longExtra, a4, context, q, a3));
        }
    }
}
