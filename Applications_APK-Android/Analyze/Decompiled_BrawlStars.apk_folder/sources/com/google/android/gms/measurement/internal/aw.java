package com.google.android.gms.measurement.internal;

final class aw implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zzo f2397a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ zzk f2398b;
    private final /* synthetic */ zzby c;

    aw(zzby zzby, zzo zzo, zzk zzk) {
        this.c = zzby;
        this.f2397a = zzo;
        this.f2398b = zzk;
    }

    public final void run() {
        this.c.f2597a.k();
        this.c.f2597a.a(this.f2397a, this.f2398b);
    }
}
