package com.google.android.gms.drive.query;

import android.os.Parcelable;

public final class a implements Parcelable.Creator<Query> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new Query[i];
    }

    /* JADX WARN: Type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARN: Type inference failed for: r1v4, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object createFromParcel(android.os.Parcel r12) {
        /*
            r11 = this;
            int r0 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.a(r12)
            r1 = 0
            r2 = 0
            r4 = r2
            r5 = r4
            r6 = r5
            r7 = r6
            r9 = r7
            r8 = 0
            r10 = 0
        L_0x000d:
            int r1 = r12.dataPosition()
            if (r1 >= r0) goto L_0x0051
            int r1 = r12.readInt()
            r2 = 65535(0xffff, float:9.1834E-41)
            r2 = r2 & r1
            switch(r2) {
                case 1: goto L_0x0047;
                case 2: goto L_0x001e;
                case 3: goto L_0x0042;
                case 4: goto L_0x0038;
                case 5: goto L_0x0033;
                case 6: goto L_0x002e;
                case 7: goto L_0x0027;
                case 8: goto L_0x0022;
                default: goto L_0x001e;
            }
        L_0x001e:
            com.google.android.gms.common.internal.safeparcel.SafeParcelReader.b(r12, r1)
            goto L_0x000d
        L_0x0022:
            boolean r10 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.c(r12, r1)
            goto L_0x000d
        L_0x0027:
            android.os.Parcelable$Creator<com.google.android.gms.drive.DriveSpace> r2 = com.google.android.gms.drive.DriveSpace.CREATOR
            java.util.ArrayList r9 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.c(r12, r1, r2)
            goto L_0x000d
        L_0x002e:
            boolean r8 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.c(r12, r1)
            goto L_0x000d
        L_0x0033:
            java.util.ArrayList r7 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.u(r12, r1)
            goto L_0x000d
        L_0x0038:
            android.os.Parcelable$Creator<com.google.android.gms.drive.query.SortOrder> r2 = com.google.android.gms.drive.query.SortOrder.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.a(r12, r1, r2)
            r6 = r1
            com.google.android.gms.drive.query.SortOrder r6 = (com.google.android.gms.drive.query.SortOrder) r6
            goto L_0x000d
        L_0x0042:
            java.lang.String r5 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.l(r12, r1)
            goto L_0x000d
        L_0x0047:
            android.os.Parcelable$Creator<com.google.android.gms.drive.query.internal.zzr> r2 = com.google.android.gms.drive.query.internal.zzr.CREATOR
            android.os.Parcelable r1 = com.google.android.gms.common.internal.safeparcel.SafeParcelReader.a(r12, r1, r2)
            r4 = r1
            com.google.android.gms.drive.query.internal.zzr r4 = (com.google.android.gms.drive.query.internal.zzr) r4
            goto L_0x000d
        L_0x0051:
            com.google.android.gms.common.internal.safeparcel.SafeParcelReader.x(r12, r0)
            com.google.android.gms.drive.query.Query r12 = new com.google.android.gms.drive.query.Query
            r3 = r12
            r3.<init>(r4, r5, r6, r7, r8, r9, r10)
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.drive.query.a.createFromParcel(android.os.Parcel):java.lang.Object");
    }
}
