package com.google.android.gms.internal.measurement;

public enum aq {
    NONE,
    GZIP;

    public static aq a(String str) {
        if ("GZIP".equalsIgnoreCase(str)) {
            return GZIP;
        }
        return NONE;
    }
}
