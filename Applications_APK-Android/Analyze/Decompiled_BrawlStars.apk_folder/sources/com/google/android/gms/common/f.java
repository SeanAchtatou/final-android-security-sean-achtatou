package com.google.android.gms.common;

import android.app.NotificationManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageInstaller;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.UserManager;
import android.util.Log;
import com.facebook.internal.ServerProtocol;
import com.google.android.gms.common.c.c;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.util.o;
import com.google.android.gms.common.util.r;
import com.google.android.gms.common.util.u;
import java.util.concurrent.atomic.AtomicBoolean;

public class f {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f1535a = false;
    @Deprecated

    /* renamed from: b  reason: collision with root package name */
    public static final int f1536b = 12451000;
    static final AtomicBoolean c = new AtomicBoolean();
    private static boolean d = false;
    private static boolean e = false;
    private static boolean f = false;
    private static final AtomicBoolean g = new AtomicBoolean();

    @Deprecated
    public static boolean b(int i) {
        return i == 1 || i == 2 || i == 3 || i == 9;
    }

    @Deprecated
    public static String a(int i) {
        return ConnectionResult.a(i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x008c, code lost:
        if (com.google.android.gms.common.util.h.f1676a.booleanValue() == false) goto L_0x0090;
     */
    @java.lang.Deprecated
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int a(android.content.Context r4, int r5) {
        /*
            android.content.res.Resources r0 = r4.getResources()     // Catch:{ all -> 0x000a }
            int r1 = com.google.android.gms.common.R.string.common_google_play_services_unknown_issue     // Catch:{ all -> 0x000a }
            r0.getString(r1)     // Catch:{ all -> 0x000a }
            goto L_0x000b
        L_0x000a:
        L_0x000b:
            java.lang.String r0 = r4.getPackageName()
            java.lang.String r1 = "com.google.android.gms"
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0058
            java.util.concurrent.atomic.AtomicBoolean r0 = com.google.android.gms.common.f.g
            boolean r0 = r0.get()
            if (r0 != 0) goto L_0x0058
            int r0 = com.google.android.gms.common.internal.ag.b(r4)
            if (r0 == 0) goto L_0x0050
            int r1 = com.google.android.gms.common.f.f1536b
            if (r0 != r1) goto L_0x002a
            goto L_0x0058
        L_0x002a:
            java.lang.IllegalStateException r4 = new java.lang.IllegalStateException
            r5 = 320(0x140, float:4.48E-43)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>(r5)
            java.lang.String r5 = "The meta-data tag in your app's AndroidManifest.xml does not have the right value.  Expected "
            r2.append(r5)
            r2.append(r1)
            java.lang.String r5 = " but found "
            r2.append(r5)
            r2.append(r0)
            java.lang.String r5 = ".  You must have the following declaration within the <application> element:     <meta-data android:name=\"com.google.android.gms.version\" android:value=\"@integer/google_play_services_version\" />"
            r2.append(r5)
            java.lang.String r5 = r2.toString()
            r4.<init>(r5)
            throw r4
        L_0x0050:
            java.lang.IllegalStateException r4 = new java.lang.IllegalStateException
            java.lang.String r5 = "A required meta-data tag in your app's AndroidManifest.xml does not exist.  You must have the following declaration within the <application> element:     <meta-data android:name=\"com.google.android.gms.version\" android:value=\"@integer/google_play_services_version\" />"
            r4.<init>(r5)
            throw r4
        L_0x0058:
            boolean r0 = com.google.android.gms.common.util.h.b(r4)
            r1 = 1
            r2 = 0
            if (r0 != 0) goto L_0x008f
            java.lang.Boolean r0 = com.google.android.gms.common.util.h.f1676a
            if (r0 != 0) goto L_0x0086
            android.content.pm.PackageManager r0 = r4.getPackageManager()
            java.lang.String r3 = "android.hardware.type.iot"
            boolean r0 = r0.hasSystemFeature(r3)
            if (r0 != 0) goto L_0x007f
            android.content.pm.PackageManager r0 = r4.getPackageManager()
            java.lang.String r3 = "android.hardware.type.embedded"
            boolean r0 = r0.hasSystemFeature(r3)
            if (r0 == 0) goto L_0x007d
            goto L_0x007f
        L_0x007d:
            r0 = 0
            goto L_0x0080
        L_0x007f:
            r0 = 1
        L_0x0080:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            com.google.android.gms.common.util.h.f1676a = r0
        L_0x0086:
            java.lang.Boolean r0 = com.google.android.gms.common.util.h.f1676a
            boolean r0 = r0.booleanValue()
            if (r0 != 0) goto L_0x008f
            goto L_0x0090
        L_0x008f:
            r1 = 0
        L_0x0090:
            int r4 = a(r4, r1, r5)
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.f.a(android.content.Context, int):int");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.g.a(android.content.pm.PackageInfo, boolean):boolean
     arg types: [android.content.pm.PackageInfo, int]
     candidates:
      com.google.android.gms.common.g.a(android.content.pm.PackageInfo, com.google.android.gms.common.m[]):com.google.android.gms.common.m
      com.google.android.gms.common.g.a(java.lang.String, int):com.google.android.gms.common.u
      com.google.android.gms.common.g.a(android.content.pm.PackageInfo, boolean):boolean */
    private static int a(Context context, boolean z, int i) {
        l.b(i >= 0);
        PackageManager packageManager = context.getPackageManager();
        PackageInfo packageInfo = null;
        if (z) {
            try {
                packageInfo = packageManager.getPackageInfo("com.android.vending", 8256);
            } catch (PackageManager.NameNotFoundException unused) {
                return 9;
            }
        }
        try {
            PackageInfo packageInfo2 = packageManager.getPackageInfo("com.google.android.gms", 64);
            g.a(context);
            if (!g.a(packageInfo2, true)) {
                return 9;
            }
            if (z && (!g.a(packageInfo, true) || !packageInfo.signatures[0].equals(packageInfo2.signatures[0]))) {
                return 9;
            }
            if (u.a(packageInfo2.versionCode) < u.a(i)) {
                int i2 = packageInfo2.versionCode;
                StringBuilder sb = new StringBuilder(77);
                sb.append("Google Play services out of date.  Requires ");
                sb.append(i);
                sb.append(" but found ");
                sb.append(i2);
                sb.toString();
                return 2;
            }
            ApplicationInfo applicationInfo = packageInfo2.applicationInfo;
            if (applicationInfo == null) {
                try {
                    applicationInfo = packageManager.getApplicationInfo("com.google.android.gms", 0);
                } catch (PackageManager.NameNotFoundException e2) {
                    Log.wtf("GooglePlayServicesUtil", "Google Play services missing when getting application info.", e2);
                    return 1;
                }
            }
            if (!applicationInfo.enabled) {
                return 3;
            }
            return 0;
        } catch (PackageManager.NameNotFoundException unused2) {
            return 1;
        }
    }

    @Deprecated
    public static boolean a(Context context, int i, String str) {
        return r.a(context, i, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.g.a(android.content.pm.PackageInfo, boolean):boolean
     arg types: [android.content.pm.PackageInfo, int]
     candidates:
      com.google.android.gms.common.g.a(android.content.pm.PackageInfo, com.google.android.gms.common.m[]):com.google.android.gms.common.m
      com.google.android.gms.common.g.a(java.lang.String, int):com.google.android.gms.common.u
      com.google.android.gms.common.g.a(android.content.pm.PackageInfo, boolean):boolean */
    public static boolean c(Context context) {
        if (!f) {
            try {
                PackageInfo b2 = c.a(context).b("com.google.android.gms", 64);
                g.a(context);
                if (b2 == null || g.a(b2, false) || !g.a(b2, true)) {
                    e = false;
                } else {
                    e = true;
                }
            } catch (PackageManager.NameNotFoundException unused) {
            } finally {
                f = true;
            }
        }
        return e || !"user".equals(Build.TYPE);
    }

    @Deprecated
    public static void d(Context context) {
        if (!c.getAndSet(true)) {
            try {
                NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
                if (notificationManager != null) {
                    notificationManager.cancel(10436);
                }
            } catch (SecurityException unused) {
            }
        }
    }

    public static Resources e(Context context) {
        try {
            return context.getPackageManager().getResourcesForApplication("com.google.android.gms");
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }

    public static Context f(Context context) {
        try {
            return context.createPackageContext("com.google.android.gms", 3);
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }

    @Deprecated
    public static int g(Context context) {
        try {
            return context.getPackageManager().getPackageInfo("com.google.android.gms", 0).versionCode;
        } catch (PackageManager.NameNotFoundException unused) {
            return 0;
        }
    }

    @Deprecated
    public static boolean b(Context context, int i) {
        if (i == 18) {
            return true;
        }
        if (i == 1) {
            return a(context, "com.google.android.gms");
        }
        return false;
    }

    static boolean a(Context context, String str) {
        Bundle applicationRestrictions;
        boolean equals = str.equals("com.google.android.gms");
        if (o.e()) {
            try {
                for (PackageInstaller.SessionInfo appPackageName : context.getPackageManager().getPackageInstaller().getAllSessions()) {
                    if (str.equals(appPackageName.getAppPackageName())) {
                        return true;
                    }
                }
            } catch (Exception unused) {
                return false;
            }
        }
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(str, 8192);
            if (equals) {
                return applicationInfo.enabled;
            }
            if (applicationInfo.enabled) {
                if (!(o.c() && (applicationRestrictions = ((UserManager) context.getSystemService("user")).getApplicationRestrictions(context.getPackageName())) != null && ServerProtocol.DIALOG_RETURN_SCOPES_TRUE.equals(applicationRestrictions.getString("restricted_profile")))) {
                    return true;
                }
            }
            return false;
        } catch (PackageManager.NameNotFoundException unused2) {
        }
    }
}
