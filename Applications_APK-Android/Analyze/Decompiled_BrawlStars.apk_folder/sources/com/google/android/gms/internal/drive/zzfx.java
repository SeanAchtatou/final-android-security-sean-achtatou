package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import java.util.List;

public final class zzfx extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzfx> CREATOR = new ae();

    /* renamed from: a  reason: collision with root package name */
    private final List<String> f1952a;

    zzfx(List<String> list) {
        this.f1952a = list;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
     arg types: [android.os.Parcel, int, java.util.List<java.lang.String>, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.a(parcel, 2, this.f1952a, false);
        a.b(parcel, a2);
    }
}
