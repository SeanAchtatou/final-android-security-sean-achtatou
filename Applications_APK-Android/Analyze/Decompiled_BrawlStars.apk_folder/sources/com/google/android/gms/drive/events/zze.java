package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.drive.DriveSpace;
import java.util.Arrays;
import java.util.List;

public final class zze extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zze> CREATOR = new f();

    /* renamed from: a  reason: collision with root package name */
    private final int f1720a;

    /* renamed from: b  reason: collision with root package name */
    private final boolean f1721b;
    private final List<DriveSpace> c;

    zze(int i, boolean z, List<DriveSpace> list) {
        this.f1720a = i;
        this.f1721b = z;
        this.c = list;
    }

    public final boolean equals(Object obj) {
        if (obj != null && obj.getClass() == getClass()) {
            if (obj == this) {
                return true;
            }
            zze zze = (zze) obj;
            return j.a(this.c, zze.c) && this.f1720a == zze.f1720a && this.f1721b == zze.f1721b;
        }
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.b(parcel, 2, this.f1720a);
        a.a(parcel, 3, this.f1721b);
        a.b(parcel, 4, this.c, false);
        a.b(parcel, a2);
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.c, Integer.valueOf(this.f1720a), Boolean.valueOf(this.f1721b)});
    }
}
