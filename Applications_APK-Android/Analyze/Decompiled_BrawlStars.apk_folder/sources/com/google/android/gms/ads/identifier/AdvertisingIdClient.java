package com.google.android.gms.ads.identifier;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.os.SystemClock;
import com.facebook.appevents.AppEventsConstants;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.d;
import com.google.android.gms.common.f;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.internal.ads_identifier.zze;
import com.google.android.gms.internal.ads_identifier.zzf;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
public class AdvertisingIdClient {

    /* renamed from: a  reason: collision with root package name */
    private com.google.android.gms.common.a f1300a;

    /* renamed from: b  reason: collision with root package name */
    private zze f1301b;
    private boolean c;
    private final Object d = new Object();
    private a e;
    private final Context f;
    private final boolean g;
    private final long h;

    public static final class Info {

        /* renamed from: a  reason: collision with root package name */
        private final String f1302a;

        /* renamed from: b  reason: collision with root package name */
        private final boolean f1303b;

        public Info(String str, boolean z) {
            this.f1302a = str;
            this.f1303b = z;
        }

        public final String getId() {
            return this.f1302a;
        }

        public final boolean isLimitAdTrackingEnabled() {
            return this.f1303b;
        }

        public final String toString() {
            String str = this.f1302a;
            boolean z = this.f1303b;
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 7);
            sb.append("{");
            sb.append(str);
            sb.append("}");
            sb.append(z);
            return sb.toString();
        }
    }

    static class a extends Thread {

        /* renamed from: a  reason: collision with root package name */
        CountDownLatch f1304a = new CountDownLatch(1);

        /* renamed from: b  reason: collision with root package name */
        boolean f1305b = false;
        private WeakReference<AdvertisingIdClient> c;
        private long d;

        public a(AdvertisingIdClient advertisingIdClient, long j) {
            this.c = new WeakReference<>(advertisingIdClient);
            this.d = j;
            start();
        }

        private final void a() {
            AdvertisingIdClient advertisingIdClient = this.c.get();
            if (advertisingIdClient != null) {
                advertisingIdClient.b();
                this.f1305b = true;
            }
        }

        public final void run() {
            try {
                if (!this.f1304a.await(this.d, TimeUnit.MILLISECONDS)) {
                    a();
                }
            } catch (InterruptedException unused) {
                a();
            }
        }
    }

    private AdvertisingIdClient(Context context, long j, boolean z, boolean z2) {
        Context applicationContext;
        l.a(context);
        if (z && (applicationContext = context.getApplicationContext()) != null) {
            context = applicationContext;
        }
        this.f = context;
        this.c = false;
        this.h = -1;
        this.g = z2;
    }

    private static com.google.android.gms.common.a a(Context context, boolean z) throws IOException, GooglePlayServicesNotAvailableException, GooglePlayServicesRepairableException {
        try {
            context.getPackageManager().getPackageInfo("com.android.vending", 0);
            int a2 = d.b().a(context, (int) f.f1536b);
            if (a2 == 0 || a2 == 2) {
                String str = z ? "com.google.android.gms.ads.identifier.service.PERSISTENT_START" : "com.google.android.gms.ads.identifier.service.START";
                com.google.android.gms.common.a aVar = new com.google.android.gms.common.a();
                Intent intent = new Intent(str);
                intent.setPackage("com.google.android.gms");
                try {
                    if (com.google.android.gms.common.stats.a.a().b(context, intent, aVar, 1)) {
                        return aVar;
                    }
                    throw new IOException("Connection failure");
                } catch (Throwable th) {
                    throw new IOException(th);
                }
            } else {
                throw new IOException("Google Play services not available");
            }
        } catch (PackageManager.NameNotFoundException unused) {
            throw new GooglePlayServicesNotAvailableException(9);
        }
    }

    public static void a() {
    }

    private final void a(boolean z) throws IOException, IllegalStateException, GooglePlayServicesNotAvailableException, GooglePlayServicesRepairableException {
        l.c("Calling this from your main thread can lead to deadlock");
        synchronized (this) {
            if (this.c) {
                b();
            }
            this.f1300a = a(this.f, this.g);
            this.f1301b = a(this.f1300a);
            this.c = true;
        }
    }

    private final boolean a(Info info, boolean z, float f2, long j, String str, Throwable th) {
        if (Math.random() > ((double) f2)) {
            return false;
        }
        HashMap hashMap = new HashMap();
        String str2 = AppEventsConstants.EVENT_PARAM_VALUE_YES;
        hashMap.put("app_context", z ? str2 : AppEventsConstants.EVENT_PARAM_VALUE_NO);
        if (info != null) {
            if (!info.isLimitAdTrackingEnabled()) {
                str2 = AppEventsConstants.EVENT_PARAM_VALUE_NO;
            }
            hashMap.put("limit_ad_tracking", str2);
        }
        if (!(info == null || info.getId() == null)) {
            hashMap.put("ad_id_size", Integer.toString(info.getId().length()));
        }
        if (th != null) {
            hashMap.put("error", th.getClass().getName());
        }
        if (str != null && !str.isEmpty()) {
            hashMap.put("experiment_id", str);
        }
        hashMap.put("tag", "AdvertisingIdClient");
        hashMap.put("time_spent", Long.toString(j));
        new a(hashMap).start();
        return true;
    }

    public static Info getAdvertisingIdInfo(Context context) throws IOException, IllegalStateException, GooglePlayServicesNotAvailableException, GooglePlayServicesRepairableException {
        b bVar = new b(context);
        boolean a2 = bVar.a("gads:ad_id_app_context:enabled");
        float b2 = bVar.b("gads:ad_id_app_context:ping_ratio");
        String a3 = bVar.a("gads:ad_id_use_shared_preference:experiment_id", "");
        AdvertisingIdClient advertisingIdClient = new AdvertisingIdClient(context, -1, a2, bVar.a("gads:ad_id_use_persistent_service:enabled"));
        try {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            advertisingIdClient.a(false);
            Info c2 = advertisingIdClient.c();
            advertisingIdClient.a(c2, a2, b2, SystemClock.elapsedRealtime() - elapsedRealtime, a3, null);
            advertisingIdClient.b();
            return c2;
        } catch (Throwable th) {
            advertisingIdClient.b();
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0028, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b() {
        /*
            r2 = this;
            java.lang.String r0 = "Calling this from your main thread can lead to deadlock"
            com.google.android.gms.common.internal.l.c(r0)
            monitor-enter(r2)
            android.content.Context r0 = r2.f     // Catch:{ all -> 0x0029 }
            if (r0 == 0) goto L_0x0027
            com.google.android.gms.common.a r0 = r2.f1300a     // Catch:{ all -> 0x0029 }
            if (r0 != 0) goto L_0x000f
            goto L_0x0027
        L_0x000f:
            boolean r0 = r2.c     // Catch:{ all -> 0x001d }
            if (r0 == 0) goto L_0x001d
            com.google.android.gms.common.stats.a.a()     // Catch:{ all -> 0x001d }
            android.content.Context r0 = r2.f     // Catch:{ all -> 0x001d }
            com.google.android.gms.common.a r1 = r2.f1300a     // Catch:{ all -> 0x001d }
            com.google.android.gms.common.stats.a.a(r0, r1)     // Catch:{ all -> 0x001d }
        L_0x001d:
            r0 = 0
            r2.c = r0     // Catch:{ all -> 0x0029 }
            r0 = 0
            r2.f1301b = r0     // Catch:{ all -> 0x0029 }
            r2.f1300a = r0     // Catch:{ all -> 0x0029 }
            monitor-exit(r2)     // Catch:{ all -> 0x0029 }
            return
        L_0x0027:
            monitor-exit(r2)     // Catch:{ all -> 0x0029 }
            return
        L_0x0029:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0029 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.identifier.AdvertisingIdClient.b():void");
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        b();
        super.finalize();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:54|55|56) */
    /* JADX WARNING: Can't wrap try/catch for region: R(7:39|40|(3:42|43|44)|45|46|(1:48)|49) */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0089, code lost:
        throw new java.io.IOException("Remote exception");
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:45:0x006c */
    /* JADX WARNING: Missing exception handler attribute for start block: B:54:0x0082 */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0074  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.google.android.gms.ads.identifier.AdvertisingIdClient.Info c() throws java.io.IOException {
        /*
            r7 = this;
            java.lang.String r0 = "Calling this from your main thread can lead to deadlock"
            com.google.android.gms.common.internal.l.c(r0)
            monitor-enter(r7)
            boolean r0 = r7.c     // Catch:{ all -> 0x008a }
            if (r0 != 0) goto L_0x003d
            java.lang.Object r0 = r7.d     // Catch:{ all -> 0x008a }
            monitor-enter(r0)     // Catch:{ all -> 0x008a }
            com.google.android.gms.ads.identifier.AdvertisingIdClient$a r1 = r7.e     // Catch:{ all -> 0x003a }
            if (r1 == 0) goto L_0x0032
            com.google.android.gms.ads.identifier.AdvertisingIdClient$a r1 = r7.e     // Catch:{ all -> 0x003a }
            boolean r1 = r1.f1305b     // Catch:{ all -> 0x003a }
            if (r1 == 0) goto L_0x0032
            monitor-exit(r0)     // Catch:{ all -> 0x003a }
            r0 = 0
            r7.a(r0)     // Catch:{ Exception -> 0x0029 }
            boolean r0 = r7.c     // Catch:{ all -> 0x008a }
            if (r0 == 0) goto L_0x0021
            goto L_0x003d
        L_0x0021:
            java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x008a }
            java.lang.String r1 = "AdvertisingIdClient cannot reconnect."
            r0.<init>(r1)     // Catch:{ all -> 0x008a }
            throw r0     // Catch:{ all -> 0x008a }
        L_0x0029:
            r0 = move-exception
            java.io.IOException r1 = new java.io.IOException     // Catch:{ all -> 0x008a }
            java.lang.String r2 = "AdvertisingIdClient cannot reconnect."
            r1.<init>(r2, r0)     // Catch:{ all -> 0x008a }
            throw r1     // Catch:{ all -> 0x008a }
        L_0x0032:
            java.io.IOException r1 = new java.io.IOException     // Catch:{ all -> 0x003a }
            java.lang.String r2 = "AdvertisingIdClient is not connected."
            r1.<init>(r2)     // Catch:{ all -> 0x003a }
            throw r1     // Catch:{ all -> 0x003a }
        L_0x003a:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x003a }
            throw r1     // Catch:{ all -> 0x008a }
        L_0x003d:
            com.google.android.gms.common.a r0 = r7.f1300a     // Catch:{ all -> 0x008a }
            com.google.android.gms.common.internal.l.a(r0)     // Catch:{ all -> 0x008a }
            com.google.android.gms.internal.ads_identifier.zze r0 = r7.f1301b     // Catch:{ all -> 0x008a }
            com.google.android.gms.common.internal.l.a(r0)     // Catch:{ all -> 0x008a }
            com.google.android.gms.ads.identifier.AdvertisingIdClient$Info r0 = new com.google.android.gms.ads.identifier.AdvertisingIdClient$Info     // Catch:{ RemoteException -> 0x0082 }
            com.google.android.gms.internal.ads_identifier.zze r1 = r7.f1301b     // Catch:{ RemoteException -> 0x0082 }
            java.lang.String r1 = r1.a()     // Catch:{ RemoteException -> 0x0082 }
            com.google.android.gms.internal.ads_identifier.zze r2 = r7.f1301b     // Catch:{ RemoteException -> 0x0082 }
            boolean r2 = r2.b()     // Catch:{ RemoteException -> 0x0082 }
            r0.<init>(r1, r2)     // Catch:{ RemoteException -> 0x0082 }
            monitor-exit(r7)     // Catch:{ all -> 0x008a }
            java.lang.Object r1 = r7.d
            monitor-enter(r1)
            com.google.android.gms.ads.identifier.AdvertisingIdClient$a r2 = r7.e     // Catch:{ all -> 0x007f }
            if (r2 == 0) goto L_0x006c
            com.google.android.gms.ads.identifier.AdvertisingIdClient$a r2 = r7.e     // Catch:{ all -> 0x007f }
            java.util.concurrent.CountDownLatch r2 = r2.f1304a     // Catch:{ all -> 0x007f }
            r2.countDown()     // Catch:{ all -> 0x007f }
            com.google.android.gms.ads.identifier.AdvertisingIdClient$a r2 = r7.e     // Catch:{ InterruptedException -> 0x006c }
            r2.join()     // Catch:{ InterruptedException -> 0x006c }
        L_0x006c:
            long r2 = r7.h     // Catch:{ all -> 0x007f }
            r4 = 0
            int r6 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r6 <= 0) goto L_0x007d
            com.google.android.gms.ads.identifier.AdvertisingIdClient$a r2 = new com.google.android.gms.ads.identifier.AdvertisingIdClient$a     // Catch:{ all -> 0x007f }
            long r3 = r7.h     // Catch:{ all -> 0x007f }
            r2.<init>(r7, r3)     // Catch:{ all -> 0x007f }
            r7.e = r2     // Catch:{ all -> 0x007f }
        L_0x007d:
            monitor-exit(r1)     // Catch:{ all -> 0x007f }
            return r0
        L_0x007f:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x007f }
            throw r0
        L_0x0082:
            java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x008a }
            java.lang.String r1 = "Remote exception"
            r0.<init>(r1)     // Catch:{ all -> 0x008a }
            throw r0     // Catch:{ all -> 0x008a }
        L_0x008a:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x008a }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.ads.identifier.AdvertisingIdClient.c():com.google.android.gms.ads.identifier.AdvertisingIdClient$Info");
    }

    private static zze a(com.google.android.gms.common.a aVar) throws IOException {
        try {
            TimeUnit timeUnit = TimeUnit.MILLISECONDS;
            l.c("BlockingServiceConnection.getServiceWithTimeout() called on main thread");
            if (!aVar.f1359a) {
                aVar.f1359a = true;
                IBinder poll = aVar.f1360b.poll(10000, timeUnit);
                if (poll != null) {
                    return zzf.a(poll);
                }
                throw new TimeoutException("Timed out waiting for the service connection");
            }
            throw new IllegalStateException("Cannot call get on this connection more than once");
        } catch (InterruptedException unused) {
            throw new IOException("Interrupted exception");
        } catch (Throwable th) {
            throw new IOException(th);
        }
    }
}
