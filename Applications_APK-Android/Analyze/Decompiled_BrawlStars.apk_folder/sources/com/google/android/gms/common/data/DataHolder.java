package com.google.android.gms.common.data;

import android.database.CursorIndexOutOfBoundsException;
import android.database.CursorWindow;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.HashMap;

public final class DataHolder extends AbstractSafeParcelable implements Closeable {
    public static final Parcelable.Creator<DataHolder> CREATOR = new h();
    private static final a k = new g(new String[0]);

    /* renamed from: a  reason: collision with root package name */
    final String[] f1525a;

    /* renamed from: b  reason: collision with root package name */
    Bundle f1526b;
    final CursorWindow[] c;
    public final Bundle d;
    int[] e;
    public int f;
    private final int g;
    private final int h;
    private boolean i = false;
    private boolean j = true;

    public static class zaa extends RuntimeException {
    }

    DataHolder(int i2, String[] strArr, CursorWindow[] cursorWindowArr, int i3, Bundle bundle) {
        this.g = i2;
        this.f1525a = strArr;
        this.c = cursorWindowArr;
        this.h = i3;
        this.d = bundle;
    }

    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private final String[] f1527a;

        /* renamed from: b  reason: collision with root package name */
        private final ArrayList<HashMap<String, Object>> f1528b;
        private final String c;
        private final HashMap<Object, Integer> d;
        private boolean e;
        private String f;

        private a(String[] strArr, String str) {
            this.f1527a = (String[]) l.a(strArr);
            this.f1528b = new ArrayList<>();
            this.c = null;
            this.d = new HashMap<>();
            this.e = false;
            this.f = null;
        }

        /* synthetic */ a(String[] strArr) {
            this(strArr, null);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, int i2) {
        Bundle bundle = this.f1526b;
        if (bundle == null || !bundle.containsKey(str)) {
            String valueOf = String.valueOf(str);
            throw new IllegalArgumentException(valueOf.length() != 0 ? "No such column: ".concat(valueOf) : new String("No such column: "));
        } else if (a()) {
            throw new IllegalArgumentException("Buffer is closed.");
        } else if (i2 < 0 || i2 >= this.f) {
            throw new CursorIndexOutOfBoundsException(i2, this.f);
        }
    }

    public final boolean a(String str) {
        return this.f1526b.containsKey(str);
    }

    public final long a(String str, int i2, int i3) {
        a(str, i2);
        return this.c[i3].getLong(i2, this.f1526b.getInt(str));
    }

    public final int b(String str, int i2, int i3) {
        a(str, i2);
        return this.c[i3].getInt(i2, this.f1526b.getInt(str));
    }

    public final String c(String str, int i2, int i3) {
        a(str, i2);
        return this.c[i3].getString(i2, this.f1526b.getInt(str));
    }

    public final boolean d(String str, int i2, int i3) {
        a(str, i2);
        return Long.valueOf(this.c[i3].getLong(i2, this.f1526b.getInt(str))).longValue() == 1;
    }

    public final boolean e(String str, int i2, int i3) {
        a(str, i2);
        return this.c[i3].isNull(i2, this.f1526b.getInt(str));
    }

    public final int a(int i2) {
        int i3 = 0;
        l.a(i2 >= 0 && i2 < this.f);
        while (true) {
            int[] iArr = this.e;
            if (i3 >= iArr.length) {
                break;
            } else if (i2 < iArr[i3]) {
                i3--;
                break;
            } else {
                i3++;
            }
        }
        return i3 == this.e.length ? i3 - 1 : i3;
    }

    private boolean a() {
        boolean z;
        synchronized (this) {
            z = this.i;
        }
        return z;
    }

    public final void close() {
        synchronized (this) {
            if (!this.i) {
                this.i = true;
                for (CursorWindow close : this.c) {
                    close.close();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void finalize() throws Throwable {
        try {
            if (this.j && this.c.length > 0 && !a()) {
                close();
                String obj = toString();
                StringBuilder sb = new StringBuilder(String.valueOf(obj).length() + 178);
                sb.append("Internal data leak within a DataBuffer object detected!  Be sure to explicitly call release() on all DataBuffer extending objects when you are done with them. (internal object: ");
                sb.append(obj);
                sb.append(")");
                sb.toString();
            }
        } finally {
            super.finalize();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
     arg types: [android.os.Parcel, int, java.lang.String[], int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
     arg types: [android.os.Parcel, int, android.database.CursorWindow[], int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
     arg types: [android.os.Parcel, int, android.os.Bundle, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void */
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = com.google.android.gms.common.internal.safeparcel.a.a(parcel, 20293);
        com.google.android.gms.common.internal.safeparcel.a.a(parcel, 1, this.f1525a, false);
        com.google.android.gms.common.internal.safeparcel.a.a(parcel, 2, (Parcelable[]) this.c, i2, false);
        com.google.android.gms.common.internal.safeparcel.a.b(parcel, 3, this.h);
        com.google.android.gms.common.internal.safeparcel.a.a(parcel, 4, this.d, false);
        com.google.android.gms.common.internal.safeparcel.a.b(parcel, 1000, this.g);
        com.google.android.gms.common.internal.safeparcel.a.b(parcel, a2);
        if ((i2 & 1) != 0) {
            close();
        }
    }
}
