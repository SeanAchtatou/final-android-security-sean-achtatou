package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import com.google.android.gms.common.R;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.ag;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.internal.o;

@Deprecated
public final class e {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f1474a = new Object();

    /* renamed from: b  reason: collision with root package name */
    private static e f1475b;
    private final String c;
    private final Status d;
    private final boolean e;
    private final boolean f;

    private e(Context context) {
        Resources resources = context.getResources();
        int identifier = resources.getIdentifier("google_app_measurement_enable", "integer", resources.getResourcePackageName(R.string.common_google_play_services_unknown_issue));
        boolean z = true;
        if (identifier != 0) {
            z = resources.getInteger(identifier) == 0 ? false : z;
            this.f = !z;
        } else {
            this.f = false;
        }
        this.e = z;
        String a2 = ag.a(context);
        a2 = a2 == null ? new o(context).a("google_app_id") : a2;
        if (TextUtils.isEmpty(a2)) {
            this.d = new Status(10, "Missing google app id value from from string resources with name google_app_id.");
            this.c = null;
            return;
        }
        this.c = a2;
        this.d = Status.f1368a;
    }

    public static Status a(Context context) {
        Status status;
        l.a(context, "Context must not be null.");
        synchronized (f1474a) {
            if (f1475b == null) {
                f1475b = new e(context);
            }
            status = f1475b.d;
        }
        return status;
    }

    public static String a() {
        return a("getGoogleAppId").c;
    }

    public static boolean b() {
        return a("isMeasurementExplicitlyDisabled").f;
    }

    private static e a(String str) {
        e eVar;
        synchronized (f1474a) {
            if (f1475b != null) {
                eVar = f1475b;
            } else {
                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 34);
                sb.append("Initialize must be called before ");
                sb.append(str);
                sb.append(".");
                throw new IllegalStateException(sb.toString());
            }
        }
        return eVar;
    }
}
