package com.google.android.gms.measurement.internal;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.util.ArrayMap;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.util.e;
import java.util.Map;

public final class ch extends df {

    /* renamed from: a  reason: collision with root package name */
    protected cg f2463a;

    /* renamed from: b  reason: collision with root package name */
    public volatile cg f2464b;
    cg c;
    public final Map<Activity, cg> d = new ArrayMap();
    private cg e;
    private String f;

    public ch(ar arVar) {
        super(arVar);
    }

    /* access modifiers changed from: protected */
    public final boolean t() {
        return false;
    }

    public final cg v() {
        D();
        c();
        return this.f2463a;
    }

    public final void a(Activity activity, cg cgVar, boolean z) {
        cg cgVar2 = this.f2464b == null ? this.c : this.f2464b;
        if (cgVar.f2462b == null) {
            cgVar = new cg(cgVar.f2461a, a(activity.getClass().getCanonicalName()), cgVar.c);
        }
        this.c = this.f2464b;
        this.f2464b = cgVar;
        p().a(new ci(this, z, cgVar2, cgVar));
    }

    public static void a(cg cgVar, Bundle bundle, boolean z) {
        if (bundle != null && cgVar != null && (!bundle.containsKey("_sc") || z)) {
            if (cgVar.f2461a != null) {
                bundle.putString("_sn", cgVar.f2461a);
            } else {
                bundle.remove("_sn");
            }
            bundle.putString("_sc", cgVar.f2462b);
            bundle.putLong("_si", cgVar.c);
        } else if (bundle != null && cgVar == null && z) {
            bundle.remove("_sn");
            bundle.remove("_sc");
            bundle.remove("_si");
        }
    }

    public final void a(String str, cg cgVar) {
        c();
        synchronized (this) {
            if (this.f == null || this.f.equals(str) || cgVar != null) {
                this.f = str;
                this.e = cgVar;
            }
        }
    }

    public static String a(String str) {
        String[] split = str.split("\\.");
        String str2 = split.length > 0 ? split[split.length - 1] : "";
        return str2.length() > 100 ? str2.substring(0, 100) : str2;
    }

    /* access modifiers changed from: package-private */
    public final cg a(Activity activity) {
        l.a(activity);
        cg cgVar = this.d.get(activity);
        if (cgVar != null) {
            return cgVar;
        }
        cg cgVar2 = new cg(null, a(activity.getClass().getCanonicalName()), o().f());
        this.d.put(activity, cgVar2);
        return cgVar2;
    }

    public final /* bridge */ /* synthetic */ void a() {
        super.a();
    }

    public final /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    public final /* bridge */ /* synthetic */ void c() {
        super.c();
    }

    public final /* bridge */ /* synthetic */ a d() {
        return super.d();
    }

    public final /* bridge */ /* synthetic */ bv e() {
        return super.e();
    }

    public final /* bridge */ /* synthetic */ i f() {
        return super.f();
    }

    public final /* bridge */ /* synthetic */ cl g() {
        return super.g();
    }

    public final /* bridge */ /* synthetic */ ch h() {
        return super.h();
    }

    public final /* bridge */ /* synthetic */ k i() {
        return super.i();
    }

    public final /* bridge */ /* synthetic */ dj j() {
        return super.j();
    }

    public final /* bridge */ /* synthetic */ b k() {
        return super.k();
    }

    public final /* bridge */ /* synthetic */ e l() {
        return super.l();
    }

    public final /* bridge */ /* synthetic */ Context m() {
        return super.m();
    }

    public final /* bridge */ /* synthetic */ m n() {
        return super.n();
    }

    public final /* bridge */ /* synthetic */ ed o() {
        return super.o();
    }

    public final /* bridge */ /* synthetic */ am p() {
        return super.p();
    }

    public final /* bridge */ /* synthetic */ o q() {
        return super.q();
    }

    public final /* bridge */ /* synthetic */ z r() {
        return super.r();
    }

    public final /* bridge */ /* synthetic */ el s() {
        return super.s();
    }

    static /* synthetic */ void a(ch chVar, cg cgVar, boolean z) {
        chVar.d().a(chVar.l().b());
        if (chVar.j().a(cgVar.d, z)) {
            cgVar.d = false;
        }
    }
}
