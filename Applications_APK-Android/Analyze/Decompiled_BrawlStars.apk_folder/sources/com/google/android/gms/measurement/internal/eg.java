package com.google.android.gms.measurement.internal;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Arrays;
import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;

final class eg extends SSLSocket {

    /* renamed from: a  reason: collision with root package name */
    private final SSLSocket f2548a;

    eg(SSLSocket sSLSocket) {
        this.f2548a = sSLSocket;
    }

    public final void setEnabledProtocols(String[] strArr) {
        if (strArr != null && Arrays.asList(strArr).contains("SSLv3")) {
            ArrayList arrayList = new ArrayList(Arrays.asList(this.f2548a.getEnabledProtocols()));
            if (arrayList.size() > 1) {
                arrayList.remove("SSLv3");
            }
            strArr = (String[]) arrayList.toArray(new String[arrayList.size()]);
        }
        this.f2548a.setEnabledProtocols(strArr);
    }

    public final String[] getSupportedCipherSuites() {
        return this.f2548a.getSupportedCipherSuites();
    }

    public final String[] getEnabledCipherSuites() {
        return this.f2548a.getEnabledCipherSuites();
    }

    public final void setEnabledCipherSuites(String[] strArr) {
        this.f2548a.setEnabledCipherSuites(strArr);
    }

    public final String[] getSupportedProtocols() {
        return this.f2548a.getSupportedProtocols();
    }

    public final String[] getEnabledProtocols() {
        return this.f2548a.getEnabledProtocols();
    }

    public final SSLSession getSession() {
        return this.f2548a.getSession();
    }

    public final void addHandshakeCompletedListener(HandshakeCompletedListener handshakeCompletedListener) {
        this.f2548a.addHandshakeCompletedListener(handshakeCompletedListener);
    }

    public final void removeHandshakeCompletedListener(HandshakeCompletedListener handshakeCompletedListener) {
        this.f2548a.removeHandshakeCompletedListener(handshakeCompletedListener);
    }

    public final void startHandshake() throws IOException {
        this.f2548a.startHandshake();
    }

    public final void setUseClientMode(boolean z) {
        this.f2548a.setUseClientMode(z);
    }

    public final boolean getUseClientMode() {
        return this.f2548a.getUseClientMode();
    }

    public final void setNeedClientAuth(boolean z) {
        this.f2548a.setNeedClientAuth(z);
    }

    public final void setWantClientAuth(boolean z) {
        this.f2548a.setWantClientAuth(z);
    }

    public final boolean getNeedClientAuth() {
        return this.f2548a.getNeedClientAuth();
    }

    public final boolean getWantClientAuth() {
        return this.f2548a.getWantClientAuth();
    }

    public final void setEnableSessionCreation(boolean z) {
        this.f2548a.setEnableSessionCreation(z);
    }

    public final boolean getEnableSessionCreation() {
        return this.f2548a.getEnableSessionCreation();
    }

    public final void bind(SocketAddress socketAddress) throws IOException {
        this.f2548a.bind(socketAddress);
    }

    public final synchronized void close() throws IOException {
        this.f2548a.close();
    }

    public final void connect(SocketAddress socketAddress) throws IOException {
        this.f2548a.connect(socketAddress);
    }

    public final void connect(SocketAddress socketAddress, int i) throws IOException {
        this.f2548a.connect(socketAddress, i);
    }

    public final SocketChannel getChannel() {
        return this.f2548a.getChannel();
    }

    public final InetAddress getInetAddress() {
        return this.f2548a.getInetAddress();
    }

    public final InputStream getInputStream() throws IOException {
        return this.f2548a.getInputStream();
    }

    public final boolean getKeepAlive() throws SocketException {
        return this.f2548a.getKeepAlive();
    }

    public final InetAddress getLocalAddress() {
        return this.f2548a.getLocalAddress();
    }

    public final int getLocalPort() {
        return this.f2548a.getLocalPort();
    }

    public final SocketAddress getLocalSocketAddress() {
        return this.f2548a.getLocalSocketAddress();
    }

    public final boolean getOOBInline() throws SocketException {
        return this.f2548a.getOOBInline();
    }

    public final OutputStream getOutputStream() throws IOException {
        return this.f2548a.getOutputStream();
    }

    public final int getPort() {
        return this.f2548a.getPort();
    }

    public final synchronized int getReceiveBufferSize() throws SocketException {
        return this.f2548a.getReceiveBufferSize();
    }

    public final SocketAddress getRemoteSocketAddress() {
        return this.f2548a.getRemoteSocketAddress();
    }

    public final boolean getReuseAddress() throws SocketException {
        return this.f2548a.getReuseAddress();
    }

    public final synchronized int getSendBufferSize() throws SocketException {
        return this.f2548a.getSendBufferSize();
    }

    public final int getSoLinger() throws SocketException {
        return this.f2548a.getSoLinger();
    }

    public final synchronized int getSoTimeout() throws SocketException {
        return this.f2548a.getSoTimeout();
    }

    public final boolean getTcpNoDelay() throws SocketException {
        return this.f2548a.getTcpNoDelay();
    }

    public final int getTrafficClass() throws SocketException {
        return this.f2548a.getTrafficClass();
    }

    public final boolean isBound() {
        return this.f2548a.isBound();
    }

    public final boolean isClosed() {
        return this.f2548a.isClosed();
    }

    public final boolean isConnected() {
        return this.f2548a.isConnected();
    }

    public final boolean isInputShutdown() {
        return this.f2548a.isInputShutdown();
    }

    public final boolean isOutputShutdown() {
        return this.f2548a.isOutputShutdown();
    }

    public final void sendUrgentData(int i) throws IOException {
        this.f2548a.sendUrgentData(i);
    }

    public final void setKeepAlive(boolean z) throws SocketException {
        this.f2548a.setKeepAlive(z);
    }

    public final void setOOBInline(boolean z) throws SocketException {
        this.f2548a.setOOBInline(z);
    }

    public final void setPerformancePreferences(int i, int i2, int i3) {
        this.f2548a.setPerformancePreferences(i, i2, i3);
    }

    public final synchronized void setReceiveBufferSize(int i) throws SocketException {
        this.f2548a.setReceiveBufferSize(i);
    }

    public final void setReuseAddress(boolean z) throws SocketException {
        this.f2548a.setReuseAddress(z);
    }

    public final synchronized void setSendBufferSize(int i) throws SocketException {
        this.f2548a.setSendBufferSize(i);
    }

    public final void setSoLinger(boolean z, int i) throws SocketException {
        this.f2548a.setSoLinger(z, i);
    }

    public final synchronized void setSoTimeout(int i) throws SocketException {
        this.f2548a.setSoTimeout(i);
    }

    public final void setTcpNoDelay(boolean z) throws SocketException {
        this.f2548a.setTcpNoDelay(z);
    }

    public final void setTrafficClass(int i) throws SocketException {
        this.f2548a.setTrafficClass(i);
    }

    public final void shutdownInput() throws IOException {
        this.f2548a.shutdownInput();
    }

    public final void shutdownOutput() throws IOException {
        this.f2548a.shutdownOutput();
    }

    public final String toString() {
        return this.f2548a.toString();
    }

    public final boolean equals(Object obj) {
        return this.f2548a.equals(obj);
    }
}
