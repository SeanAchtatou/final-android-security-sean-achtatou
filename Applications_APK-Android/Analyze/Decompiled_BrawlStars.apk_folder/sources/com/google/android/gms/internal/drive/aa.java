package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class aa implements Parcelable.Creator<zzfp> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zzfp[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        DataHolder dataHolder = null;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            if ((65535 & readInt) != 2) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                dataHolder = (DataHolder) SafeParcelReader.a(parcel, readInt, DataHolder.CREATOR);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new zzfp(dataHolder);
    }
}
