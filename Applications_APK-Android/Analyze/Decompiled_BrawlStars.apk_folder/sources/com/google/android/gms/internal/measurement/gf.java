package com.google.android.gms.internal.measurement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

final class gf extends ge {

    /* renamed from: a  reason: collision with root package name */
    private static final Class<?> f2239a = Collections.unmodifiableList(Collections.emptyList()).getClass();

    private gf() {
        super((byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final <L> List<L> a(Object obj, long j) {
        return a(obj, j, 10);
    }

    /* access modifiers changed from: package-private */
    public final void b(Object obj, long j) {
        Object obj2;
        List list = (List) ih.f(obj, j);
        if (list instanceof gd) {
            obj2 = ((gd) list).e();
        } else if (!f2239a.isAssignableFrom(list.getClass())) {
            if (!(list instanceof he) || !(list instanceof fw)) {
                obj2 = Collections.unmodifiableList(list);
            } else {
                fw fwVar = (fw) list;
                if (fwVar.a()) {
                    fwVar.b();
                    return;
                }
                return;
            }
        } else {
            return;
        }
        ih.a(obj, j, obj2);
    }

    private static <L> List<L> a(Object obj, long j, int i) {
        List<L> gcVar;
        List<L> list;
        List<L> c = c(obj, j);
        if (c.isEmpty()) {
            if (c instanceof gd) {
                list = new gc(i);
            } else if (!(c instanceof he) || !(c instanceof fw)) {
                list = new ArrayList<>(i);
            } else {
                list = ((fw) c).a(i);
            }
            ih.a(obj, j, list);
            return list;
        }
        if (f2239a.isAssignableFrom(c.getClass())) {
            gcVar = new ArrayList<>(c.size() + i);
            gcVar.addAll(c);
            ih.a(obj, j, gcVar);
        } else if (c instanceof ie) {
            gcVar = new gc(c.size() + i);
            gcVar.addAll((ie) c);
            ih.a(obj, j, gcVar);
        } else if (!(c instanceof he) || !(c instanceof fw)) {
            return c;
        } else {
            fw fwVar = (fw) c;
            if (fwVar.a()) {
                return c;
            }
            fw a2 = fwVar.a(c.size() + i);
            ih.a(obj, j, a2);
            return a2;
        }
        return gcVar;
    }

    /* access modifiers changed from: package-private */
    public final <E> void a(Object obj, Object obj2, long j) {
        List c = c(obj2, j);
        List a2 = a(obj, j, c.size());
        int size = a2.size();
        int size2 = c.size();
        if (size > 0 && size2 > 0) {
            a2.addAll(c);
        }
        if (size > 0) {
            c = a2;
        }
        ih.a(obj, j, c);
    }

    private static <E> List<E> c(Object obj, long j) {
        return (List) ih.f(obj, j);
    }

    /* synthetic */ gf(byte b2) {
        this();
    }
}
