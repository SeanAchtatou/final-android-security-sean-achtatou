package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class cl extends iz<cl> {

    /* renamed from: a  reason: collision with root package name */
    public Integer f2120a = null;

    /* renamed from: b  reason: collision with root package name */
    public Boolean f2121b = null;
    public String c = null;
    public String d = null;
    public String e = null;

    public cl() {
        this.L = null;
        this.M = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof cl)) {
            return false;
        }
        cl clVar = (cl) obj;
        Integer num = this.f2120a;
        if (num == null) {
            if (clVar.f2120a != null) {
                return false;
            }
        } else if (!num.equals(clVar.f2120a)) {
            return false;
        }
        Boolean bool = this.f2121b;
        if (bool == null) {
            if (clVar.f2121b != null) {
                return false;
            }
        } else if (!bool.equals(clVar.f2121b)) {
            return false;
        }
        String str = this.c;
        if (str == null) {
            if (clVar.c != null) {
                return false;
            }
        } else if (!str.equals(clVar.c)) {
            return false;
        }
        String str2 = this.d;
        if (str2 == null) {
            if (clVar.d != null) {
                return false;
            }
        } else if (!str2.equals(clVar.d)) {
            return false;
        }
        String str3 = this.e;
        if (str3 == null) {
            if (clVar.e != null) {
                return false;
            }
        } else if (!str3.equals(clVar.e)) {
            return false;
        }
        if (this.L == null || this.L.a()) {
            return clVar.L == null || clVar.L.a();
        }
        return this.L.equals(clVar.L);
    }

    public final int hashCode() {
        int hashCode = (getClass().getName().hashCode() + 527) * 31;
        Integer num = this.f2120a;
        int i = 0;
        int intValue = (hashCode + (num == null ? 0 : num.intValue())) * 31;
        Boolean bool = this.f2121b;
        int hashCode2 = (intValue + (bool == null ? 0 : bool.hashCode())) * 31;
        String str = this.c;
        int hashCode3 = (hashCode2 + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.d;
        int hashCode4 = (hashCode3 + (str2 == null ? 0 : str2.hashCode())) * 31;
        String str3 = this.e;
        int hashCode5 = (hashCode4 + (str3 == null ? 0 : str3.hashCode())) * 31;
        if (this.L != null && !this.L.a()) {
            i = this.L.hashCode();
        }
        return hashCode5 + i;
    }

    public final void a(iy iyVar) throws IOException {
        Integer num = this.f2120a;
        if (num != null) {
            iyVar.a(1, num.intValue());
        }
        Boolean bool = this.f2121b;
        if (bool != null) {
            iyVar.a(2, bool.booleanValue());
        }
        String str = this.c;
        if (str != null) {
            iyVar.a(3, str);
        }
        String str2 = this.d;
        if (str2 != null) {
            iyVar.a(4, str2);
        }
        String str3 = this.e;
        if (str3 != null) {
            iyVar.a(5, str3);
        }
        super.a(iyVar);
    }

    /* access modifiers changed from: protected */
    public final int b() {
        int b2 = super.b();
        Integer num = this.f2120a;
        if (num != null) {
            b2 += iy.b(1, num.intValue());
        }
        Boolean bool = this.f2121b;
        if (bool != null) {
            bool.booleanValue();
            b2 += iy.c(16) + 1;
        }
        String str = this.c;
        if (str != null) {
            b2 += iy.b(3, str);
        }
        String str2 = this.d;
        if (str2 != null) {
            b2 += iy.b(4, str2);
        }
        String str3 = this.e;
        return str3 != null ? b2 + iy.b(5, str3) : b2;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final cl a(iw iwVar) throws IOException {
        int d2;
        while (true) {
            int a2 = iwVar.a();
            if (a2 == 0) {
                return this;
            }
            if (a2 == 8) {
                try {
                    d2 = iwVar.d();
                    if (d2 < 0 || d2 > 4) {
                        StringBuilder sb = new StringBuilder(46);
                        sb.append(d2);
                        sb.append(" is not a valid enum ComparisonType");
                    } else {
                        this.f2120a = Integer.valueOf(d2);
                    }
                } catch (IllegalArgumentException unused) {
                    iwVar.e(iwVar.i());
                    a(iwVar, a2);
                }
            } else if (a2 == 16) {
                this.f2121b = Boolean.valueOf(iwVar.b());
            } else if (a2 == 26) {
                this.c = iwVar.c();
            } else if (a2 == 34) {
                this.d = iwVar.c();
            } else if (a2 == 42) {
                this.e = iwVar.c();
            } else if (!super.a(iwVar, a2)) {
                return this;
            }
        }
        StringBuilder sb2 = new StringBuilder(46);
        sb2.append(d2);
        sb2.append(" is not a valid enum ComparisonType");
        throw new IllegalArgumentException(sb2.toString());
    }
}
