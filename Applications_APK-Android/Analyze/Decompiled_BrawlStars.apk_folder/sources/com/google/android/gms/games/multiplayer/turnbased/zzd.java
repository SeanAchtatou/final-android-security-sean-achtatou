package com.google.android.gms.games.multiplayer.turnbased;

import android.os.Bundle;
import android.os.Parcel;
import com.facebook.internal.ServerProtocol;
import com.facebook.share.internal.ShareConstants;
import com.google.android.gms.common.data.d;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.ParticipantRef;
import java.util.ArrayList;

public final class zzd extends d implements TurnBasedMatch {
    private final Game c;
    private final int d;

    public final /* synthetic */ Object a() {
        return new TurnBasedMatchEntity(this);
    }

    public final Game b() {
        return this.c;
    }

    public final String c() {
        return e("external_match_id");
    }

    public final String d() {
        return e("creator_external");
    }

    public final int describeContents() {
        return 0;
    }

    public final long e() {
        return b("creation_timestamp");
    }

    public final boolean equals(Object obj) {
        return TurnBasedMatchEntity.a(this, obj);
    }

    public final int f() {
        return c("status");
    }

    public final int g() {
        return c("user_match_status");
    }

    public final String h() {
        return e("description");
    }

    public final int hashCode() {
        return TurnBasedMatchEntity.a(this);
    }

    public final ArrayList<Participant> i() {
        ArrayList<Participant> arrayList = new ArrayList<>(this.d);
        for (int i = 0; i < this.d; i++) {
            arrayList.add(new ParticipantRef(this.f1532a, this.f1533b + i));
        }
        return arrayList;
    }

    public final int j() {
        return c("variant");
    }

    public final String k() {
        return e("last_updater_external");
    }

    public final long l() {
        return b("last_updated_timestamp");
    }

    public final String m() {
        return e("pending_participant_external");
    }

    public final byte[] n() {
        return g(ShareConstants.WEB_DIALOG_PARAM_DATA);
    }

    public final int o() {
        return c(ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION);
    }

    public final String p() {
        return e("rematch_id");
    }

    public final byte[] q() {
        return g("previous_match_data");
    }

    public final int r() {
        return c("match_number");
    }

    public final int t() {
        if (!d("has_automatch_criteria")) {
            return 0;
        }
        return c("automatch_max_players");
    }

    public final String toString() {
        return TurnBasedMatchEntity.b(this);
    }

    public final boolean u() {
        return d("upsync_required");
    }

    public final String v() {
        return e("description_participant_id");
    }

    public final void writeToParcel(Parcel parcel, int i) {
        ((TurnBasedMatchEntity) ((TurnBasedMatch) a())).writeToParcel(parcel, i);
    }

    public final Bundle s() {
        if (!d("has_automatch_criteria")) {
            return null;
        }
        int c2 = c("automatch_min_players");
        int c3 = c("automatch_max_players");
        long b2 = b("automatch_bit_mask");
        Bundle bundle = new Bundle();
        bundle.putInt("min_automatch_players", c2);
        bundle.putInt("max_automatch_players", c3);
        bundle.putLong("exclusive_bit_mask", b2);
        return bundle;
    }
}
