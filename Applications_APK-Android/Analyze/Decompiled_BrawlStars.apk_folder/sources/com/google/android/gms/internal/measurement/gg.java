package com.google.android.gms.internal.measurement;

import java.util.List;

final class gg extends ge {
    private gg() {
        super((byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final <L> List<L> a(Object obj, long j) {
        fw c = c(obj, j);
        if (c.a()) {
            return c;
        }
        int size = c.size();
        fw a2 = c.a(size == 0 ? 10 : size << 1);
        ih.a(obj, j, a2);
        return a2;
    }

    /* access modifiers changed from: package-private */
    public final void b(Object obj, long j) {
        c(obj, j).b();
    }

    /* access modifiers changed from: package-private */
    public final <E> void a(Object obj, Object obj2, long j) {
        fw c = c(obj, j);
        fw c2 = c(obj2, j);
        int size = c.size();
        int size2 = c2.size();
        if (size > 0 && size2 > 0) {
            if (!c.a()) {
                c = c.a(size2 + size);
            }
            c.addAll(c2);
        }
        if (size > 0) {
            c2 = c;
        }
        ih.a(obj, j, c2);
    }

    private static <E> fw<E> c(Object obj, long j) {
        return (fw) ih.f(obj, j);
    }

    /* synthetic */ gg(byte b2) {
        this();
    }
}
