package com.google.android.gms.internal.instantapps;

import android.os.IInterface;
import android.os.RemoteException;

public interface zzr extends IInterface {
    void a() throws RemoteException;

    void b() throws RemoteException;

    void c() throws RemoteException;

    void d() throws RemoteException;

    void e() throws RemoteException;

    void f() throws RemoteException;

    void g() throws RemoteException;

    void h() throws RemoteException;

    void i() throws RemoteException;

    void j() throws RemoteException;

    void k() throws RemoteException;

    void l() throws RemoteException;

    void m() throws RemoteException;

    void n() throws RemoteException;

    void o() throws RemoteException;
}
