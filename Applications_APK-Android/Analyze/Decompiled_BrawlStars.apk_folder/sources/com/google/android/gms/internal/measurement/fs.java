package com.google.android.gms.internal.measurement;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public final class fs {

    /* renamed from: a  reason: collision with root package name */
    static final Charset f2226a = Charset.forName("UTF-8");

    /* renamed from: b  reason: collision with root package name */
    public static final byte[] f2227b;
    private static final Charset c = Charset.forName("ISO-8859-1");
    private static final ByteBuffer d;
    private static final ev e;

    public static int a(long j) {
        return (int) (j ^ (j >>> 32));
    }

    public static int a(boolean z) {
        return z ? 1231 : 1237;
    }

    static boolean a() {
        return false;
    }

    static <T> T a(Object obj) {
        if (obj != null) {
            return obj;
        }
        throw new NullPointerException();
    }

    static <T> T a(T t, String str) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(str);
    }

    public static boolean a(byte[] bArr) {
        return ij.a(bArr);
    }

    public static String b(byte[] bArr) {
        return new String(bArr, f2226a);
    }

    public static int c(byte[] bArr) {
        int length = bArr.length;
        int a2 = a(length, bArr, 0, length);
        if (a2 == 0) {
            return 1;
        }
        return a2;
    }

    static int a(int i, byte[] bArr, int i2, int i3) {
        int i4 = i;
        for (int i5 = i2; i5 < i2 + i3; i5++) {
            i4 = (i4 * 31) + bArr[i5];
        }
        return i4;
    }

    static Object a(Object obj, Object obj2) {
        return ((gt) obj).i().a((gt) obj2).c();
    }

    static {
        byte[] bArr = new byte[0];
        f2227b = bArr;
        d = ByteBuffer.wrap(bArr);
        byte[] bArr2 = f2227b;
        e = ev.a(bArr2, 0, bArr2.length);
    }
}
