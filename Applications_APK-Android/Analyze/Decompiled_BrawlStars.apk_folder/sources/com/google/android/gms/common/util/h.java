package com.google.android.gms.common.util;

import android.content.Context;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    public static Boolean f1676a;

    /* renamed from: b  reason: collision with root package name */
    private static Boolean f1677b;
    private static Boolean c;

    public static boolean a(Context context) {
        if (f1677b == null) {
            f1677b = Boolean.valueOf(o.d() && context.getPackageManager().hasSystemFeature("android.hardware.type.watch"));
        }
        return f1677b.booleanValue();
    }

    public static boolean b(Context context) {
        if (!a(context)) {
            return false;
        }
        if (o.f()) {
            return c(context) && !o.g();
        }
        return true;
    }

    private static boolean c(Context context) {
        if (c == null) {
            c = Boolean.valueOf(o.e() && context.getPackageManager().hasSystemFeature("cn.google"));
        }
        return c.booleanValue();
    }
}
