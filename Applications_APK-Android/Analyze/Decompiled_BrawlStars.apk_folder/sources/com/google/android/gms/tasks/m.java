package com.google.android.gms.tasks;

import java.util.concurrent.Executor;

final class m<TResult, TContinuationResult> implements b, d, e<TContinuationResult>, y<TResult> {

    /* renamed from: a  reason: collision with root package name */
    final a<TResult, g<TContinuationResult>> f2687a;

    /* renamed from: b  reason: collision with root package name */
    final ab<TContinuationResult> f2688b;
    private final Executor c;

    public m(Executor executor, a<TResult, g<TContinuationResult>> aVar, ab<TContinuationResult> abVar) {
        this.c = executor;
        this.f2687a = aVar;
        this.f2688b = abVar;
    }

    public final void a(g<TResult> gVar) {
        this.c.execute(new n(this, gVar));
    }

    public final void a(TContinuationResult tcontinuationresult) {
        this.f2688b.a((Object) tcontinuationresult);
    }

    public final void a(Exception exc) {
        this.f2688b.a(exc);
    }

    public final void g_() {
        this.f2688b.f();
    }

    public final void a() {
        throw new UnsupportedOperationException();
    }
}
