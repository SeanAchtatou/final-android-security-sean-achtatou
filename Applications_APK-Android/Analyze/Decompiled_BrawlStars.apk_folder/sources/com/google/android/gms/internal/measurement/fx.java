package com.google.android.gms.internal.measurement;

public enum fx {
    VOID(Void.class, Void.class, null),
    INT(Integer.TYPE, Integer.class, 0),
    LONG(Long.TYPE, Long.class, 0L),
    FLOAT(Float.TYPE, Float.class, Float.valueOf(0.0f)),
    DOUBLE(Double.TYPE, Double.class, Double.valueOf(0.0d)),
    BOOLEAN(Boolean.TYPE, Boolean.class, false),
    STRING(String.class, String.class, ""),
    BYTE_STRING(ej.class, ej.class, ej.f2184a),
    ENUM(Integer.TYPE, Integer.class, null),
    MESSAGE(Object.class, Object.class, null);
    
    final Class<?> k;
    private final Class<?> l;
    private final Object m;

    private fx(Class<?> cls, Class<?> cls2, Object obj) {
        this.l = cls;
        this.k = cls2;
        this.m = obj;
    }
}
