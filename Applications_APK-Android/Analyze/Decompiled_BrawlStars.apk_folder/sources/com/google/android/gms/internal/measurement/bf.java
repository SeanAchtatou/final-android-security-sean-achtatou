package com.google.android.gms.internal.measurement;

final class bf implements at<bg> {

    /* renamed from: a  reason: collision with root package name */
    private final t f2069a;

    /* renamed from: b  reason: collision with root package name */
    private final bg f2070b = new bg();

    public bf(t tVar) {
        this.f2069a = tVar;
    }

    public final void a(String str, String str2) {
        if ("ga_appName".equals(str)) {
            this.f2070b.f2071a = str2;
        } else if ("ga_appVersion".equals(str)) {
            this.f2070b.f2072b = str2;
        } else if ("ga_logLevel".equals(str)) {
            this.f2070b.c = str2;
        } else {
            this.f2069a.a().d("String xml configuration name not recognized", str);
        }
    }

    public final void a(String str, boolean z) {
        if ("ga_dryRun".equals(str)) {
            this.f2070b.e = z ? 1 : 0;
            return;
        }
        this.f2069a.a().d("Bool xml configuration name not recognized", str);
    }

    public final void a(String str, int i) {
        if ("ga_dispatchPeriod".equals(str)) {
            this.f2070b.d = i;
        } else {
            this.f2069a.a().d("Int xml configuration name not recognized", str);
        }
    }

    public final /* synthetic */ ar a() {
        return this.f2070b;
    }
}
