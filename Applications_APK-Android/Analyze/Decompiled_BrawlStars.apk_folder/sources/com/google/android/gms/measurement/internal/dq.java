package com.google.android.gms.measurement.internal;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.PersistableBundle;
import android.support.v4.app.NotificationCompat;
import com.facebook.internal.NativeProtocol;
import com.google.android.gms.common.util.e;
import com.google.android.gms.internal.measurement.bz;

public final class dq extends dt {

    /* renamed from: a  reason: collision with root package name */
    private final AlarmManager f2522a = ((AlarmManager) m().getSystemService(NotificationCompat.CATEGORY_ALARM));
    private final et c;
    private Integer d;

    protected dq(du duVar) {
        super(duVar);
        this.c = new dr(this, duVar.f2528b, duVar);
    }

    /* access modifiers changed from: protected */
    public final boolean d() {
        this.f2522a.cancel(w());
        if (Build.VERSION.SDK_INT < 24) {
            return false;
        }
        u();
        return false;
    }

    private final void u() {
        int v = v();
        q().k.a("Cancelling job. JobID", Integer.valueOf(v));
        ((JobScheduler) m().getSystemService("jobscheduler")).cancel(v);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public final void a(long j) {
        j();
        Context m = m();
        if (!ah.a(m)) {
            q().j.a("Receiver not registered/enabled");
        }
        if (!ed.a(m)) {
            q().j.a("Service not registered/enabled");
        }
        e();
        long b2 = l().b() + j;
        if (j < Math.max(0L, h.F.a().longValue()) && !this.c.b()) {
            q().k.a("Scheduling upload with DelayedRunnable");
            this.c.a(j);
        }
        if (Build.VERSION.SDK_INT >= 24) {
            q().k.a("Scheduling upload with JobScheduler");
            Context m2 = m();
            ComponentName componentName = new ComponentName(m2, "com.google.android.gms.measurement.AppMeasurementJobService");
            int v = v();
            PersistableBundle persistableBundle = new PersistableBundle();
            persistableBundle.putString(NativeProtocol.WEB_DIALOG_ACTION, "com.google.android.gms.measurement.UPLOAD");
            JobInfo build = new JobInfo.Builder(v, componentName).setMinimumLatency(j).setOverrideDeadline(j << 1).setExtras(persistableBundle).build();
            q().k.a("Scheduling job. JobID", Integer.valueOf(v));
            bz.a(m2, build, "com.google.android.gms", "UploadAlarm");
            return;
        }
        q().k.a("Scheduling upload with AlarmManager");
        this.f2522a.setInexactRepeating(2, b2, Math.max(h.A.a().longValue(), j), w());
    }

    private final int v() {
        if (this.d == null) {
            String valueOf = String.valueOf(m().getPackageName());
            this.d = Integer.valueOf((valueOf.length() != 0 ? "measurement".concat(valueOf) : new String("measurement")).hashCode());
        }
        return this.d.intValue();
    }

    public final void e() {
        j();
        this.f2522a.cancel(w());
        this.c.c();
        if (Build.VERSION.SDK_INT >= 24) {
            u();
        }
    }

    private final PendingIntent w() {
        Context m = m();
        return PendingIntent.getBroadcast(m, 0, new Intent().setClassName(m, "com.google.android.gms.measurement.AppMeasurementReceiver").setAction("com.google.android.gms.measurement.UPLOAD"), 0);
    }

    public final /* bridge */ /* synthetic */ ea f() {
        return super.f();
    }

    public final /* bridge */ /* synthetic */ ei g() {
        return super.g();
    }

    public final /* bridge */ /* synthetic */ eo h() {
        return super.h();
    }

    public final /* bridge */ /* synthetic */ void a() {
        super.a();
    }

    public final /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    public final /* bridge */ /* synthetic */ void c() {
        super.c();
    }

    public final /* bridge */ /* synthetic */ b k() {
        return super.k();
    }

    public final /* bridge */ /* synthetic */ e l() {
        return super.l();
    }

    public final /* bridge */ /* synthetic */ Context m() {
        return super.m();
    }

    public final /* bridge */ /* synthetic */ m n() {
        return super.n();
    }

    public final /* bridge */ /* synthetic */ ed o() {
        return super.o();
    }

    public final /* bridge */ /* synthetic */ am p() {
        return super.p();
    }

    public final /* bridge */ /* synthetic */ o q() {
        return super.q();
    }

    public final /* bridge */ /* synthetic */ z r() {
        return super.r();
    }

    public final /* bridge */ /* synthetic */ el s() {
        return super.s();
    }
}
