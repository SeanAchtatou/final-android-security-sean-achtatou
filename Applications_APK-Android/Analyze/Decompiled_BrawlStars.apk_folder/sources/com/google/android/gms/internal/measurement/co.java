package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class co extends iz<co> {
    private static volatile co[] e;

    /* renamed from: a  reason: collision with root package name */
    public String f2126a = null;

    /* renamed from: b  reason: collision with root package name */
    public Boolean f2127b = null;
    public Boolean c = null;
    public Integer d = null;

    public static co[] a() {
        if (e == null) {
            synchronized (jd.f2312b) {
                if (e == null) {
                    e = new co[0];
                }
            }
        }
        return e;
    }

    public co() {
        this.L = null;
        this.M = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof co)) {
            return false;
        }
        co coVar = (co) obj;
        String str = this.f2126a;
        if (str == null) {
            if (coVar.f2126a != null) {
                return false;
            }
        } else if (!str.equals(coVar.f2126a)) {
            return false;
        }
        Boolean bool = this.f2127b;
        if (bool == null) {
            if (coVar.f2127b != null) {
                return false;
            }
        } else if (!bool.equals(coVar.f2127b)) {
            return false;
        }
        Boolean bool2 = this.c;
        if (bool2 == null) {
            if (coVar.c != null) {
                return false;
            }
        } else if (!bool2.equals(coVar.c)) {
            return false;
        }
        Integer num = this.d;
        if (num == null) {
            if (coVar.d != null) {
                return false;
            }
        } else if (!num.equals(coVar.d)) {
            return false;
        }
        if (this.L == null || this.L.a()) {
            return coVar.L == null || coVar.L.a();
        }
        return this.L.equals(coVar.L);
    }

    public final int hashCode() {
        int hashCode = (getClass().getName().hashCode() + 527) * 31;
        String str = this.f2126a;
        int i = 0;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        Boolean bool = this.f2127b;
        int hashCode3 = (hashCode2 + (bool == null ? 0 : bool.hashCode())) * 31;
        Boolean bool2 = this.c;
        int hashCode4 = (hashCode3 + (bool2 == null ? 0 : bool2.hashCode())) * 31;
        Integer num = this.d;
        int hashCode5 = (hashCode4 + (num == null ? 0 : num.hashCode())) * 31;
        if (this.L != null && !this.L.a()) {
            i = this.L.hashCode();
        }
        return hashCode5 + i;
    }

    public final void a(iy iyVar) throws IOException {
        String str = this.f2126a;
        if (str != null) {
            iyVar.a(1, str);
        }
        Boolean bool = this.f2127b;
        if (bool != null) {
            iyVar.a(2, bool.booleanValue());
        }
        Boolean bool2 = this.c;
        if (bool2 != null) {
            iyVar.a(3, bool2.booleanValue());
        }
        Integer num = this.d;
        if (num != null) {
            iyVar.a(4, num.intValue());
        }
        super.a(iyVar);
    }

    /* access modifiers changed from: protected */
    public final int b() {
        int b2 = super.b();
        String str = this.f2126a;
        if (str != null) {
            b2 += iy.b(1, str);
        }
        Boolean bool = this.f2127b;
        if (bool != null) {
            bool.booleanValue();
            b2 += iy.c(16) + 1;
        }
        Boolean bool2 = this.c;
        if (bool2 != null) {
            bool2.booleanValue();
            b2 += iy.c(24) + 1;
        }
        Integer num = this.d;
        return num != null ? b2 + iy.b(4, num.intValue()) : b2;
    }

    public final /* synthetic */ je a(iw iwVar) throws IOException {
        while (true) {
            int a2 = iwVar.a();
            if (a2 == 0) {
                return this;
            }
            if (a2 == 10) {
                this.f2126a = iwVar.c();
            } else if (a2 == 16) {
                this.f2127b = Boolean.valueOf(iwVar.b());
            } else if (a2 == 24) {
                this.c = Boolean.valueOf(iwVar.b());
            } else if (a2 == 32) {
                this.d = Integer.valueOf(iwVar.d());
            } else if (!super.a(iwVar, a2)) {
                return this;
            }
        }
    }
}
