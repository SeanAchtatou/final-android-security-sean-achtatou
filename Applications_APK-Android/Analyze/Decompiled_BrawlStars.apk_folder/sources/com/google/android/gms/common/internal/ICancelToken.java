package com.google.android.gms.common.internal;

import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.internal.common.zzb;

public interface ICancelToken extends IInterface {

    public static abstract class Stub extends zzb implements ICancelToken {
        public Stub() {
            super("com.google.android.gms.common.internal.ICancelToken");
        }

        public static class zza extends com.google.android.gms.internal.common.zza implements ICancelToken {
            public final void a() throws RemoteException {
                Parcel s = s();
                try {
                    this.f1885a.transact(2, s, null, 1);
                } finally {
                    s.recycle();
                }
            }
        }
    }

    void a() throws RemoteException;
}
