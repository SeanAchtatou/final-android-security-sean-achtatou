package com.google.android.gms.drive.events;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import com.google.android.gms.common.internal.h;
import com.google.android.gms.common.util.r;
import com.google.android.gms.internal.drive.zzet;
import com.google.android.gms.internal.drive.zzfj;
import java.lang.ref.WeakReference;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class DriveEventService extends Service implements a, b, e, i {
    /* access modifiers changed from: private */
    public static final h c = new h("DriveEventService", "");

    /* renamed from: a  reason: collision with root package name */
    a f1713a;

    /* renamed from: b  reason: collision with root package name */
    boolean f1714b;
    private final String d;
    /* access modifiers changed from: private */
    public CountDownLatch e;
    private int f;

    static final class a extends Handler {

        /* renamed from: a  reason: collision with root package name */
        private final WeakReference<DriveEventService> f1715a;

        private a(DriveEventService driveEventService) {
            this.f1715a = new WeakReference<>(driveEventService);
        }

        /* synthetic */ a(DriveEventService driveEventService, byte b2) {
            this(driveEventService);
        }

        public final void handleMessage(Message message) {
            int i = message.what;
            if (i == 1) {
                DriveEventService driveEventService = this.f1715a.get();
                if (driveEventService != null) {
                    DriveEventService.a(driveEventService, (zzfj) message.obj);
                } else {
                    getLooper().quit();
                }
            } else if (i != 2) {
                DriveEventService.c.a("Unexpected message type: %s", Integer.valueOf(message.what));
            } else {
                getLooper().quit();
            }
        }
    }

    final class b extends zzet {
        private b() {
        }

        /* synthetic */ b(DriveEventService driveEventService, byte b2) {
            this();
        }

        public final void a(zzfj zzfj) throws RemoteException {
            synchronized (DriveEventService.this) {
                DriveEventService.a(DriveEventService.this);
                if (DriveEventService.this.f1713a != null) {
                    DriveEventService.this.f1713a.sendMessage(DriveEventService.this.f1713a.obtainMessage(1, zzfj));
                } else {
                    DriveEventService.c.b("DriveEventService", "Receiving event before initialize is completed.");
                }
            }
        }
    }

    protected DriveEventService() {
        this("DriveEventService");
    }

    private DriveEventService(String str) {
        this.f1714b = false;
        this.f = -1;
        this.d = str;
    }

    public final void a(ChangeEvent changeEvent) {
        c.a("Unhandled change event in %s: %s", this.d, changeEvent);
    }

    public final void a(CompletionEvent completionEvent) {
        c.a("Unhandled completion event in %s: %s", this.d, completionEvent);
    }

    public final void a(zzb zzb) {
        c.a("Unhandled changes available event in %s: %s", this.d, zzb);
    }

    public final synchronized IBinder onBind(Intent intent) {
        if (!"com.google.android.gms.drive.events.HANDLE_EVENT".equals(intent.getAction())) {
            return null;
        }
        if (this.f1713a == null && !this.f1714b) {
            this.f1714b = true;
            CountDownLatch countDownLatch = new CountDownLatch(1);
            this.e = new CountDownLatch(1);
            new h(this, countDownLatch).start();
            try {
                if (!countDownLatch.await(5000, TimeUnit.MILLISECONDS)) {
                    c.b("DriveEventService", "Failed to synchronously initialize event handler.");
                }
            } catch (InterruptedException e2) {
                throw new RuntimeException("Unable to start event handler", e2);
            }
        }
        return new b(this, (byte) 0).asBinder();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:4|5|6|(1:8)|9|10) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0028 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void onDestroy() {
        /*
            r5 = this;
            monitor-enter(r5)
            com.google.android.gms.drive.events.DriveEventService$a r0 = r5.f1713a     // Catch:{ all -> 0x002f }
            if (r0 == 0) goto L_0x002a
            com.google.android.gms.drive.events.DriveEventService$a r0 = r5.f1713a     // Catch:{ all -> 0x002f }
            android.os.Message r0 = r0.obtainMessage(2)     // Catch:{ all -> 0x002f }
            com.google.android.gms.drive.events.DriveEventService$a r1 = r5.f1713a     // Catch:{ all -> 0x002f }
            r1.sendMessage(r0)     // Catch:{ all -> 0x002f }
            r0 = 0
            r5.f1713a = r0     // Catch:{ all -> 0x002f }
            java.util.concurrent.CountDownLatch r1 = r5.e     // Catch:{ InterruptedException -> 0x0028 }
            r2 = 5000(0x1388, double:2.4703E-320)
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ InterruptedException -> 0x0028 }
            boolean r1 = r1.await(r2, r4)     // Catch:{ InterruptedException -> 0x0028 }
            if (r1 != 0) goto L_0x0028
            com.google.android.gms.common.internal.h r1 = com.google.android.gms.drive.events.DriveEventService.c     // Catch:{ InterruptedException -> 0x0028 }
            java.lang.String r2 = "DriveEventService"
            java.lang.String r3 = "Failed to synchronously quit event handler. Will quit itself"
            r1.a(r2, r3)     // Catch:{ InterruptedException -> 0x0028 }
        L_0x0028:
            r5.e = r0     // Catch:{ all -> 0x002f }
        L_0x002a:
            super.onDestroy()     // Catch:{ all -> 0x002f }
            monitor-exit(r5)
            return
        L_0x002f:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.drive.events.DriveEventService.onDestroy():void");
    }

    public boolean onUnbind(Intent intent) {
        return true;
    }

    static /* synthetic */ void a(DriveEventService driveEventService, zzfj zzfj) {
        DriveEvent a2 = zzfj.a();
        try {
            int a3 = a2.a();
            if (a3 == 1) {
                driveEventService.a((ChangeEvent) a2);
            } else if (a3 == 2) {
                driveEventService.a((CompletionEvent) a2);
            } else if (a3 == 4) {
                driveEventService.a((zzb) a2);
            } else if (a3 != 7) {
                c.a("Unhandled event: %s", a2);
            } else {
                c.a("Unhandled transfer state event in %s: %s", driveEventService.d, (zzv) a2);
            }
        } catch (Exception unused) {
            h hVar = c;
            String format = String.format("Error handling event in %s", driveEventService.d);
            if (hVar.a(6)) {
                hVar.a(format);
            }
        }
    }

    static /* synthetic */ void a(DriveEventService driveEventService) throws SecurityException {
        int callingUid = Binder.getCallingUid();
        if (callingUid != driveEventService.f) {
            if (r.a(driveEventService, callingUid)) {
                driveEventService.f = callingUid;
                return;
            }
            throw new SecurityException("Caller is not GooglePlayServices");
        }
    }
}
