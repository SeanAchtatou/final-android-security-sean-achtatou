package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.internal.d;

public abstract class bd extends ai {
    public bd(int i) {
        super(i);
    }

    public abstract Feature[] b(d.a<?> aVar);

    public abstract boolean c(d.a<?> aVar);
}
