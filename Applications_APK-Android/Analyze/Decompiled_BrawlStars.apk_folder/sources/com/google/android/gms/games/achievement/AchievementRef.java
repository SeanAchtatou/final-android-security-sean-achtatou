package com.google.android.gms.games.achievement;

import android.net.Uri;
import android.os.Parcel;
import com.facebook.internal.ServerProtocol;
import com.google.android.gms.common.data.d;
import com.google.android.gms.common.internal.a;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerRef;

public final class AchievementRef extends d implements Achievement {
    public final /* synthetic */ Object a() {
        return new AchievementEntity(this);
    }

    public final String b() {
        return e("external_achievement_id");
    }

    public final int c() {
        return c("type");
    }

    public final String d() {
        return e("name");
    }

    public final int describeContents() {
        return 0;
    }

    public final String e() {
        return e("description");
    }

    public final Uri f() {
        return h("unlocked_icon_image_uri");
    }

    public final Uri g() {
        return h("revealed_icon_image_uri");
    }

    public final String getRevealedImageUrl() {
        return e("revealed_icon_image_url");
    }

    public final String getUnlockedImageUrl() {
        return e("unlocked_icon_image_url");
    }

    public final Player j() {
        return new PlayerRef(this.f1532a, this.f1533b);
    }

    public final int k() {
        return c(ServerProtocol.DIALOG_PARAM_STATE);
    }

    public final long n() {
        return b("last_updated_timestamp");
    }

    public final long o() {
        return (!a_("instance_xp_value") || i("instance_xp_value")) ? b("definition_xp_value") : b("instance_xp_value");
    }

    public final String toString() {
        return AchievementEntity.a(this);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        ((AchievementEntity) ((Achievement) a())).writeToParcel(parcel, i);
    }

    public final int h() {
        boolean z = true;
        if (c("type") != 1) {
            z = false;
        }
        a.a(z);
        return c("total_steps");
    }

    public final String i() {
        boolean z = true;
        if (c("type") != 1) {
            z = false;
        }
        a.a(z);
        return e("formatted_total_steps");
    }

    public final int l() {
        boolean z = true;
        if (c("type") != 1) {
            z = false;
        }
        a.a(z);
        return c("current_steps");
    }

    public final String m() {
        boolean z = true;
        if (c("type") != 1) {
            z = false;
        }
        a.a(z);
        return e("formatted_current_steps");
    }
}
