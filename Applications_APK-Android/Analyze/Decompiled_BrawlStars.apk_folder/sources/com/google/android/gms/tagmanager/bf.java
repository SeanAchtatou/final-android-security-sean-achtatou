package com.google.android.gms.tagmanager;

import android.support.v4.app.NotificationCompat;
import com.google.android.gms.tagmanager.c;
import java.util.Map;

final class bf implements c.b {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ d f2641a;

    bf(d dVar) {
        this.f2641a = dVar;
    }

    public final void a(Map<String, Object> map) {
        Object obj = map.get(NotificationCompat.CATEGORY_EVENT);
        if (obj != null) {
            d.a(this.f2641a, obj.toString());
        }
    }
}
