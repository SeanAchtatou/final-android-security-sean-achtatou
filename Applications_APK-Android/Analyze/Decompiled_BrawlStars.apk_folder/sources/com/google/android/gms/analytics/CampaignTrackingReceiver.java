package com.google.android.gms.analytics;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.internal.measurement.au;
import com.google.android.gms.internal.measurement.bk;
import com.google.android.gms.internal.measurement.bx;
import com.google.android.gms.internal.measurement.t;

public class CampaignTrackingReceiver extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private static Boolean f1311a;

    /* access modifiers changed from: protected */
    public void a(Context context, String str) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.bx.a(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.measurement.bx.a(java.util.Map<java.lang.String, java.lang.String>, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.measurement.bx.a(java.util.Map<java.lang.String, java.lang.String>, java.lang.String, java.util.Map<java.lang.String, java.lang.String>):void
      com.google.android.gms.internal.measurement.bx.a(java.util.Map<java.lang.String, java.lang.String>, java.lang.String, boolean):void
      com.google.android.gms.internal.measurement.bx.a(android.content.Context, java.lang.String, boolean):boolean */
    public static boolean a(Context context) {
        l.a(context);
        Boolean bool = f1311a;
        if (bool != null) {
            return bool.booleanValue();
        }
        boolean a2 = bx.a(context, "com.google.android.gms.analytics.CampaignTrackingReceiver", true);
        f1311a = Boolean.valueOf(a2);
        return a2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.l.a(java.lang.String, java.lang.Runnable):void
     arg types: [java.lang.String, com.google.android.gms.analytics.g]
     candidates:
      com.google.android.gms.internal.measurement.q.a(java.lang.String, java.lang.Object):void
      com.google.android.gms.internal.measurement.l.a(java.lang.String, java.lang.Runnable):void */
    public void onReceive(Context context, Intent intent) {
        t a2 = t.a(context);
        bk a3 = a2.a();
        if (intent == null) {
            a3.e("CampaignTrackingReceiver received null intent");
            return;
        }
        String stringExtra = intent.getStringExtra("referrer");
        String action = intent.getAction();
        a3.a("CampaignTrackingReceiver received", action);
        if (!"com.android.vending.INSTALL_REFERRER".equals(action) || TextUtils.isEmpty(stringExtra)) {
            a3.e("CampaignTrackingReceiver received unexpected intent without referrer extra");
            return;
        }
        a(context, stringExtra);
        int c = au.c();
        if (stringExtra.length() > c) {
            a3.c("Campaign data exceed the maximum supported size and will be clipped. size, limit", Integer.valueOf(stringExtra.length()), Integer.valueOf(c));
            stringExtra = stringExtra.substring(0, c);
        }
        a2.c().a(stringExtra, (Runnable) new g(goAsync()));
    }
}
