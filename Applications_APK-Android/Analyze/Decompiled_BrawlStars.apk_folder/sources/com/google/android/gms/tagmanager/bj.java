package com.google.android.gms.tagmanager;

final class bj extends Number implements Comparable<bj> {

    /* renamed from: a  reason: collision with root package name */
    boolean f2644a = true;

    /* renamed from: b  reason: collision with root package name */
    private double f2645b;
    private long c;

    private bj(long j) {
        this.c = j;
    }

    public static bj a(long j) {
        return new bj(0);
    }

    public final String toString() {
        return this.f2644a ? Long.toString(this.c) : Double.toString(this.f2645b);
    }

    public final boolean equals(Object obj) {
        return (obj instanceof bj) && compareTo((bj) obj) == 0;
    }

    public final int hashCode() {
        return new Long(longValue()).hashCode();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public int compareTo(bj bjVar) {
        if (!this.f2644a || !bjVar.f2644a) {
            return Double.compare(doubleValue(), bjVar.doubleValue());
        }
        return new Long(this.c).compareTo(Long.valueOf(bjVar.c));
    }

    public final double doubleValue() {
        return this.f2644a ? (double) this.c : this.f2645b;
    }

    public final float floatValue() {
        return (float) doubleValue();
    }

    public final long longValue() {
        return this.f2644a ? this.c : (long) this.f2645b;
    }

    public final int intValue() {
        return (int) longValue();
    }

    public final short shortValue() {
        return (short) ((int) longValue());
    }

    public final byte byteValue() {
        return (byte) ((int) longValue());
    }
}
