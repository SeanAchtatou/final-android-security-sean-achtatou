package com.google.android.gms.internal.base;

import android.os.Handler;
import android.os.Looper;

public class d extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private static volatile e f1879a;

    public d() {
    }

    public d(Looper looper) {
        super(looper);
    }

    public d(Looper looper, Handler.Callback callback) {
        super(looper, callback);
    }
}
