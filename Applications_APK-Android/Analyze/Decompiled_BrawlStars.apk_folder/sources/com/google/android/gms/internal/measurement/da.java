package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class da extends iz<da> {
    private static volatile da[] m;

    /* renamed from: a  reason: collision with root package name */
    public int f2151a = 1;

    /* renamed from: b  reason: collision with root package name */
    public String f2152b = "";
    public da[] c = a();
    public da[] d = a();
    public da[] e = a();
    public String f = "";
    public String g = "";
    public long h = 0;
    public boolean i = false;
    public da[] j = a();
    public int[] k = jh.f2315a;
    public boolean l = false;

    private static int a(int i2) {
        if (i2 > 0 && i2 <= 17) {
            return i2;
        }
        StringBuilder sb = new StringBuilder(40);
        sb.append(i2);
        sb.append(" is not a valid enum Escaping");
        throw new IllegalArgumentException(sb.toString());
    }

    private static da[] a() {
        if (m == null) {
            synchronized (jd.f2312b) {
                if (m == null) {
                    m = new da[0];
                }
            }
        }
        return m;
    }

    public da() {
        this.L = null;
        this.M = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof da)) {
            return false;
        }
        da daVar = (da) obj;
        if (this.f2151a != daVar.f2151a) {
            return false;
        }
        String str = this.f2152b;
        if (str == null) {
            if (daVar.f2152b != null) {
                return false;
            }
        } else if (!str.equals(daVar.f2152b)) {
            return false;
        }
        if (!jd.a(this.c, daVar.c) || !jd.a(this.d, daVar.d) || !jd.a(this.e, daVar.e)) {
            return false;
        }
        String str2 = this.f;
        if (str2 == null) {
            if (daVar.f != null) {
                return false;
            }
        } else if (!str2.equals(daVar.f)) {
            return false;
        }
        String str3 = this.g;
        if (str3 == null) {
            if (daVar.g != null) {
                return false;
            }
        } else if (!str3.equals(daVar.g)) {
            return false;
        }
        if (this.h != daVar.h || this.i != daVar.i || !jd.a(this.j, daVar.j) || !jd.a(this.k, daVar.k) || this.l != daVar.l) {
            return false;
        }
        if (this.L == null || this.L.a()) {
            return daVar.L == null || daVar.L.a();
        }
        return this.L.equals(daVar.L);
    }

    public final int hashCode() {
        int hashCode = (((getClass().getName().hashCode() + 527) * 31) + this.f2151a) * 31;
        String str = this.f2152b;
        int i2 = 0;
        int hashCode2 = (((((((hashCode + (str == null ? 0 : str.hashCode())) * 31) + jd.a(this.c)) * 31) + jd.a(this.d)) * 31) + jd.a(this.e)) * 31;
        String str2 = this.f;
        int hashCode3 = (hashCode2 + (str2 == null ? 0 : str2.hashCode())) * 31;
        String str3 = this.g;
        int hashCode4 = str3 == null ? 0 : str3.hashCode();
        long j2 = this.h;
        int i3 = (((hashCode3 + hashCode4) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31;
        int i4 = 1231;
        int a2 = (((((i3 + (this.i ? 1231 : 1237)) * 31) + jd.a(this.j)) * 31) + jd.a(this.k)) * 31;
        if (!this.l) {
            i4 = 1237;
        }
        int i5 = (a2 + i4) * 31;
        if (this.L != null && !this.L.a()) {
            i2 = this.L.hashCode();
        }
        return i5 + i2;
    }

    public final void a(iy iyVar) throws IOException {
        iyVar.a(1, this.f2151a);
        String str = this.f2152b;
        if (str != null && !str.equals("")) {
            iyVar.a(2, this.f2152b);
        }
        da[] daVarArr = this.c;
        int i2 = 0;
        if (daVarArr != null && daVarArr.length > 0) {
            int i3 = 0;
            while (true) {
                da[] daVarArr2 = this.c;
                if (i3 >= daVarArr2.length) {
                    break;
                }
                da daVar = daVarArr2[i3];
                if (daVar != null) {
                    iyVar.a(3, daVar);
                }
                i3++;
            }
        }
        da[] daVarArr3 = this.d;
        if (daVarArr3 != null && daVarArr3.length > 0) {
            int i4 = 0;
            while (true) {
                da[] daVarArr4 = this.d;
                if (i4 >= daVarArr4.length) {
                    break;
                }
                da daVar2 = daVarArr4[i4];
                if (daVar2 != null) {
                    iyVar.a(4, daVar2);
                }
                i4++;
            }
        }
        da[] daVarArr5 = this.e;
        if (daVarArr5 != null && daVarArr5.length > 0) {
            int i5 = 0;
            while (true) {
                da[] daVarArr6 = this.e;
                if (i5 >= daVarArr6.length) {
                    break;
                }
                da daVar3 = daVarArr6[i5];
                if (daVar3 != null) {
                    iyVar.a(5, daVar3);
                }
                i5++;
            }
        }
        String str2 = this.f;
        if (str2 != null && !str2.equals("")) {
            iyVar.a(6, this.f);
        }
        String str3 = this.g;
        if (str3 != null && !str3.equals("")) {
            iyVar.a(7, this.g);
        }
        long j2 = this.h;
        if (j2 != 0) {
            iyVar.b(8, j2);
        }
        boolean z = this.l;
        if (z) {
            iyVar.a(9, z);
        }
        int[] iArr = this.k;
        if (iArr != null && iArr.length > 0) {
            int i6 = 0;
            while (true) {
                int[] iArr2 = this.k;
                if (i6 >= iArr2.length) {
                    break;
                }
                iyVar.a(10, iArr2[i6]);
                i6++;
            }
        }
        da[] daVarArr7 = this.j;
        if (daVarArr7 != null && daVarArr7.length > 0) {
            while (true) {
                da[] daVarArr8 = this.j;
                if (i2 >= daVarArr8.length) {
                    break;
                }
                da daVar4 = daVarArr8[i2];
                if (daVar4 != null) {
                    iyVar.a(11, daVar4);
                }
                i2++;
            }
        }
        boolean z2 = this.i;
        if (z2) {
            iyVar.a(12, z2);
        }
        super.a(iyVar);
    }

    /* access modifiers changed from: protected */
    public final int b() {
        int[] iArr;
        int b2 = super.b() + iy.b(1, this.f2151a);
        String str = this.f2152b;
        if (str != null && !str.equals("")) {
            b2 += iy.b(2, this.f2152b);
        }
        da[] daVarArr = this.c;
        int i2 = 0;
        if (daVarArr != null && daVarArr.length > 0) {
            int i3 = b2;
            int i4 = 0;
            while (true) {
                da[] daVarArr2 = this.c;
                if (i4 >= daVarArr2.length) {
                    break;
                }
                da daVar = daVarArr2[i4];
                if (daVar != null) {
                    i3 += iy.b(3, daVar);
                }
                i4++;
            }
            b2 = i3;
        }
        da[] daVarArr3 = this.d;
        if (daVarArr3 != null && daVarArr3.length > 0) {
            int i5 = b2;
            int i6 = 0;
            while (true) {
                da[] daVarArr4 = this.d;
                if (i6 >= daVarArr4.length) {
                    break;
                }
                da daVar2 = daVarArr4[i6];
                if (daVar2 != null) {
                    i5 += iy.b(4, daVar2);
                }
                i6++;
            }
            b2 = i5;
        }
        da[] daVarArr5 = this.e;
        if (daVarArr5 != null && daVarArr5.length > 0) {
            int i7 = b2;
            int i8 = 0;
            while (true) {
                da[] daVarArr6 = this.e;
                if (i8 >= daVarArr6.length) {
                    break;
                }
                da daVar3 = daVarArr6[i8];
                if (daVar3 != null) {
                    i7 += iy.b(5, daVar3);
                }
                i8++;
            }
            b2 = i7;
        }
        String str2 = this.f;
        if (str2 != null && !str2.equals("")) {
            b2 += iy.b(6, this.f);
        }
        String str3 = this.g;
        if (str3 != null && !str3.equals("")) {
            b2 += iy.b(7, this.g);
        }
        long j2 = this.h;
        if (j2 != 0) {
            b2 += iy.c(8, j2);
        }
        if (this.l) {
            b2 += iy.c(72) + 1;
        }
        int[] iArr2 = this.k;
        if (iArr2 != null && iArr2.length > 0) {
            int i9 = 0;
            int i10 = 0;
            while (true) {
                iArr = this.k;
                if (i9 >= iArr.length) {
                    break;
                }
                i10 += iy.a(iArr[i9]);
                i9++;
            }
            b2 = b2 + i10 + (iArr.length * 1);
        }
        da[] daVarArr7 = this.j;
        if (daVarArr7 != null && daVarArr7.length > 0) {
            while (true) {
                da[] daVarArr8 = this.j;
                if (i2 >= daVarArr8.length) {
                    break;
                }
                da daVar4 = daVarArr8[i2];
                if (daVar4 != null) {
                    b2 += iy.b(11, daVar4);
                }
                i2++;
            }
        }
        return this.i ? b2 + iy.c(96) + 1 : b2;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final da a(iw iwVar) throws IOException {
        int d2;
        while (true) {
            int a2 = iwVar.a();
            switch (a2) {
                case 0:
                    return this;
                case 8:
                    try {
                        d2 = iwVar.d();
                        if (d2 > 0 && d2 <= 8) {
                            this.f2151a = d2;
                            break;
                        } else {
                            StringBuilder sb = new StringBuilder(36);
                            sb.append(d2);
                            sb.append(" is not a valid enum Type");
                            break;
                        }
                    } catch (IllegalArgumentException unused) {
                        iwVar.e(iwVar.i());
                        a(iwVar, a2);
                        break;
                    }
                case 18:
                    this.f2152b = iwVar.c();
                    break;
                case 26:
                    int a3 = jh.a(iwVar, 26);
                    da[] daVarArr = this.c;
                    int length = daVarArr == null ? 0 : daVarArr.length;
                    da[] daVarArr2 = new da[(a3 + length)];
                    if (length != 0) {
                        System.arraycopy(this.c, 0, daVarArr2, 0, length);
                    }
                    while (length < daVarArr2.length - 1) {
                        daVarArr2[length] = new da();
                        iwVar.a(daVarArr2[length]);
                        iwVar.a();
                        length++;
                    }
                    daVarArr2[length] = new da();
                    iwVar.a(daVarArr2[length]);
                    this.c = daVarArr2;
                    break;
                case 34:
                    int a4 = jh.a(iwVar, 34);
                    da[] daVarArr3 = this.d;
                    int length2 = daVarArr3 == null ? 0 : daVarArr3.length;
                    da[] daVarArr4 = new da[(a4 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.d, 0, daVarArr4, 0, length2);
                    }
                    while (length2 < daVarArr4.length - 1) {
                        daVarArr4[length2] = new da();
                        iwVar.a(daVarArr4[length2]);
                        iwVar.a();
                        length2++;
                    }
                    daVarArr4[length2] = new da();
                    iwVar.a(daVarArr4[length2]);
                    this.d = daVarArr4;
                    break;
                case 42:
                    int a5 = jh.a(iwVar, 42);
                    da[] daVarArr5 = this.e;
                    int length3 = daVarArr5 == null ? 0 : daVarArr5.length;
                    da[] daVarArr6 = new da[(a5 + length3)];
                    if (length3 != 0) {
                        System.arraycopy(this.e, 0, daVarArr6, 0, length3);
                    }
                    while (length3 < daVarArr6.length - 1) {
                        daVarArr6[length3] = new da();
                        iwVar.a(daVarArr6[length3]);
                        iwVar.a();
                        length3++;
                    }
                    daVarArr6[length3] = new da();
                    iwVar.a(daVarArr6[length3]);
                    this.e = daVarArr6;
                    break;
                case 50:
                    this.f = iwVar.c();
                    break;
                case 58:
                    this.g = iwVar.c();
                    break;
                case 64:
                    this.h = iwVar.e();
                    break;
                case 72:
                    this.l = iwVar.b();
                    break;
                case 80:
                    int a6 = jh.a(iwVar, 80);
                    int[] iArr = new int[a6];
                    int i2 = 0;
                    for (int i3 = 0; i3 < a6; i3++) {
                        if (i3 != 0) {
                            iwVar.a();
                        }
                        int i4 = iwVar.i();
                        try {
                            iArr[i2] = a(iwVar.d());
                            i2++;
                        } catch (IllegalArgumentException unused2) {
                            iwVar.e(i4);
                            a(iwVar, a2);
                        }
                    }
                    if (i2 != 0) {
                        int[] iArr2 = this.k;
                        int length4 = iArr2 == null ? 0 : iArr2.length;
                        if (length4 != 0 || i2 != iArr.length) {
                            int[] iArr3 = new int[(length4 + i2)];
                            if (length4 != 0) {
                                System.arraycopy(this.k, 0, iArr3, 0, length4);
                            }
                            System.arraycopy(iArr, 0, iArr3, length4, i2);
                            this.k = iArr3;
                            break;
                        } else {
                            this.k = iArr;
                            break;
                        }
                    } else {
                        break;
                    }
                case 82:
                    int c2 = iwVar.c(iwVar.d());
                    int i5 = iwVar.i();
                    int i6 = 0;
                    while (iwVar.h() > 0) {
                        try {
                            a(iwVar.d());
                            i6++;
                        } catch (IllegalArgumentException unused3) {
                        }
                    }
                    if (i6 != 0) {
                        iwVar.e(i5);
                        int[] iArr4 = this.k;
                        int length5 = iArr4 == null ? 0 : iArr4.length;
                        int[] iArr5 = new int[(i6 + length5)];
                        if (length5 != 0) {
                            System.arraycopy(this.k, 0, iArr5, 0, length5);
                        }
                        while (iwVar.h() > 0) {
                            int i7 = iwVar.i();
                            try {
                                iArr5[length5] = a(iwVar.d());
                                length5++;
                            } catch (IllegalArgumentException unused4) {
                                iwVar.e(i7);
                                a(iwVar, 80);
                            }
                        }
                        this.k = iArr5;
                    }
                    iwVar.d(c2);
                    break;
                case 90:
                    int a7 = jh.a(iwVar, 90);
                    da[] daVarArr7 = this.j;
                    int length6 = daVarArr7 == null ? 0 : daVarArr7.length;
                    da[] daVarArr8 = new da[(a7 + length6)];
                    if (length6 != 0) {
                        System.arraycopy(this.j, 0, daVarArr8, 0, length6);
                    }
                    while (length6 < daVarArr8.length - 1) {
                        daVarArr8[length6] = new da();
                        iwVar.a(daVarArr8[length6]);
                        iwVar.a();
                        length6++;
                    }
                    daVarArr8[length6] = new da();
                    iwVar.a(daVarArr8[length6]);
                    this.j = daVarArr8;
                    break;
                case 96:
                    this.i = iwVar.b();
                    break;
                default:
                    if (super.a(iwVar, a2)) {
                        break;
                    } else {
                        return this;
                    }
            }
        }
        StringBuilder sb2 = new StringBuilder(36);
        sb2.append(d2);
        sb2.append(" is not a valid enum Type");
        throw new IllegalArgumentException(sb2.toString());
    }
}
