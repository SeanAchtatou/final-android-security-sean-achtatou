package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import com.google.android.gms.common.util.e;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import javax.net.ssl.SSLSocketFactory;

public final class s extends dt {

    /* renamed from: a  reason: collision with root package name */
    final SSLSocketFactory f2581a;

    public s(du duVar) {
        super(duVar);
        this.f2581a = Build.VERSION.SDK_INT < 19 ? new ee() : null;
    }

    /* access modifiers changed from: protected */
    public final boolean d() {
        return false;
    }

    public final boolean e() {
        NetworkInfo networkInfo;
        j();
        try {
            networkInfo = ((ConnectivityManager) m().getSystemService("connectivity")).getActiveNetworkInfo();
        } catch (SecurityException unused) {
            networkInfo = null;
        }
        return networkInfo != null && networkInfo.isConnected();
    }

    static byte[] a(HttpURLConnection httpURLConnection) throws IOException {
        InputStream inputStream = null;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            inputStream = httpURLConnection.getInputStream();
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr);
                if (read <= 0) {
                    break;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
            return byteArrayOutputStream.toByteArray();
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    public final /* bridge */ /* synthetic */ ea f() {
        return super.f();
    }

    public final /* bridge */ /* synthetic */ ei g() {
        return super.g();
    }

    public final /* bridge */ /* synthetic */ eo h() {
        return super.h();
    }

    public final /* bridge */ /* synthetic */ void a() {
        super.a();
    }

    public final /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    public final /* bridge */ /* synthetic */ void c() {
        super.c();
    }

    public final /* bridge */ /* synthetic */ b k() {
        return super.k();
    }

    public final /* bridge */ /* synthetic */ e l() {
        return super.l();
    }

    public final /* bridge */ /* synthetic */ Context m() {
        return super.m();
    }

    public final /* bridge */ /* synthetic */ m n() {
        return super.n();
    }

    public final /* bridge */ /* synthetic */ ed o() {
        return super.o();
    }

    public final /* bridge */ /* synthetic */ am p() {
        return super.p();
    }

    public final /* bridge */ /* synthetic */ o q() {
        return super.q();
    }

    public final /* bridge */ /* synthetic */ z r() {
        return super.r();
    }

    public final /* bridge */ /* synthetic */ el s() {
        return super.s();
    }
}
