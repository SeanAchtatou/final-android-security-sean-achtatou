package com.google.android.gms.internal.measurement;

import android.content.SharedPreferences;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class dz implements dl {

    /* renamed from: a  reason: collision with root package name */
    static final Map<String, dz> f2174a = new HashMap();

    /* renamed from: b  reason: collision with root package name */
    final Object f2175b = new Object();
    volatile Map<String, ?> c;
    final List<Object> d = new ArrayList();
    private final SharedPreferences e;
    private final SharedPreferences.OnSharedPreferenceChangeListener f = new ea(this);

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, com.google.android.gms.internal.measurement.dz, java.lang.String] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    static com.google.android.gms.internal.measurement.dz a(android.content.Context r4, java.lang.String r5) {
        /*
            boolean r5 = com.google.android.gms.internal.measurement.dg.a()
            r0 = 0
            if (r5 == 0) goto L_0x0014
            java.lang.String r5 = "direct_boot:"
            boolean r5 = r0.startsWith(r5)
            if (r5 != 0) goto L_0x0014
            boolean r5 = com.google.android.gms.internal.measurement.dg.a(r4)
            goto L_0x0015
        L_0x0014:
            r5 = 1
        L_0x0015:
            if (r5 != 0) goto L_0x0018
            return r0
        L_0x0018:
            java.lang.Class<com.google.android.gms.internal.measurement.dz> r5 = com.google.android.gms.internal.measurement.dz.class
            monitor-enter(r5)
            java.util.Map<java.lang.String, com.google.android.gms.internal.measurement.dz> r1 = com.google.android.gms.internal.measurement.dz.f2174a     // Catch:{ all -> 0x0053 }
            java.lang.Object r1 = r1.get(r0)     // Catch:{ all -> 0x0053 }
            com.google.android.gms.internal.measurement.dz r1 = (com.google.android.gms.internal.measurement.dz) r1     // Catch:{ all -> 0x0053 }
            if (r1 != 0) goto L_0x0051
            com.google.android.gms.internal.measurement.dz r1 = new com.google.android.gms.internal.measurement.dz     // Catch:{ all -> 0x0053 }
            java.lang.String r2 = "direct_boot:"
            boolean r2 = r0.startsWith(r2)     // Catch:{ all -> 0x0053 }
            r3 = 0
            if (r2 == 0) goto L_0x0045
            boolean r2 = com.google.android.gms.internal.measurement.dg.a()     // Catch:{ all -> 0x0053 }
            if (r2 == 0) goto L_0x003a
            android.content.Context r4 = r4.createDeviceProtectedStorageContext()     // Catch:{ all -> 0x0053 }
        L_0x003a:
            r2 = 12
            java.lang.String r2 = r0.substring(r2)     // Catch:{ all -> 0x0053 }
            android.content.SharedPreferences r4 = r4.getSharedPreferences(r2, r3)     // Catch:{ all -> 0x0053 }
            goto L_0x0049
        L_0x0045:
            android.content.SharedPreferences r4 = r4.getSharedPreferences(r0, r3)     // Catch:{ all -> 0x0053 }
        L_0x0049:
            r1.<init>(r4)     // Catch:{ all -> 0x0053 }
            java.util.Map<java.lang.String, com.google.android.gms.internal.measurement.dz> r4 = com.google.android.gms.internal.measurement.dz.f2174a     // Catch:{ all -> 0x0053 }
            r4.put(r0, r1)     // Catch:{ all -> 0x0053 }
        L_0x0051:
            monitor-exit(r5)     // Catch:{ all -> 0x0053 }
            return r1
        L_0x0053:
            r4 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0053 }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.dz.a(android.content.Context, java.lang.String):com.google.android.gms.internal.measurement.dz");
    }

    private dz(SharedPreferences sharedPreferences) {
        this.e = sharedPreferences;
        this.e.registerOnSharedPreferenceChangeListener(this.f);
    }

    public final Object a(String str) {
        Map<String, ?> map = this.c;
        if (map == null) {
            synchronized (this.f2175b) {
                map = this.c;
                if (map == null) {
                    map = this.e.getAll();
                    this.c = map;
                }
            }
        }
        if (map != null) {
            return map.get(str);
        }
        return null;
    }
}
