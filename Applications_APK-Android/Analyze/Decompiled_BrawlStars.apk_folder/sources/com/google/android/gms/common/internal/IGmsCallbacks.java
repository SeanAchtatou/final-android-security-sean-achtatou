package com.google.android.gms.common.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.internal.common.a;
import com.google.android.gms.internal.common.zzb;

public interface IGmsCallbacks extends IInterface {
    void a(int i, Bundle bundle) throws RemoteException;

    void a(int i, IBinder iBinder, Bundle bundle) throws RemoteException;

    void a(int i, IBinder iBinder, zzb zzb) throws RemoteException;

    public static abstract class zza extends zzb implements IGmsCallbacks {
        public zza() {
            super("com.google.android.gms.common.internal.IGmsCallbacks");
        }

        public final boolean a(int i, Parcel parcel, Parcel parcel2) throws RemoteException {
            if (i == 1) {
                a(parcel.readInt(), parcel.readStrongBinder(), (Bundle) a.a(parcel, Bundle.CREATOR));
            } else if (i == 2) {
                a(parcel.readInt(), (Bundle) a.a(parcel, Bundle.CREATOR));
            } else if (i != 3) {
                return false;
            } else {
                a(parcel.readInt(), parcel.readStrongBinder(), (zzb) a.a(parcel, zzb.CREATOR));
            }
            parcel2.writeNoException();
            return true;
        }
    }
}
