package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.fj;
import java.io.IOException;
import java.util.Map;

abstract class fe<T extends fj<T>> {
    fe() {
    }

    /* access modifiers changed from: package-private */
    public abstract int a(Map.Entry<?, ?> entry);

    /* access modifiers changed from: package-private */
    public abstract fh<T> a(Object obj);

    /* access modifiers changed from: package-private */
    public abstract <UT, UB> UB a() throws IOException;

    /* access modifiers changed from: package-private */
    public abstract Object a(fd fdVar, gt gtVar, int i);

    /* access modifiers changed from: package-private */
    public abstract boolean a(gt gtVar);

    /* access modifiers changed from: package-private */
    public abstract fh<T> b(Object obj);

    /* access modifiers changed from: package-private */
    public abstract void b() throws IOException;

    /* access modifiers changed from: package-private */
    public abstract void b(Map.Entry<?, ?> entry) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract void c() throws IOException;

    /* access modifiers changed from: package-private */
    public abstract void c(Object obj);
}
