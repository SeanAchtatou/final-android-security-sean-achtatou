package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.g;
import com.google.android.gms.common.api.internal.c;
import com.google.android.gms.common.d;
import com.google.android.gms.common.internal.b;
import com.google.android.gms.signin.e;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public final class am implements az, cb {

    /* renamed from: a  reason: collision with root package name */
    final Lock f1397a;

    /* renamed from: b  reason: collision with root package name */
    final Condition f1398b;
    final Context c;
    final d d;
    final ao e;
    final Map<a.c<?>, a.f> f;
    final Map<a.c<?>, ConnectionResult> g = new HashMap();
    final b h;
    final Map<a<?>, Boolean> i;
    final a.C0111a<? extends e, com.google.android.gms.signin.a> j;
    volatile al k;
    int l;
    final ag m;
    final ba n;
    private ConnectionResult o = null;

    public am(Context context, ag agVar, Lock lock, Looper looper, d dVar, Map<a.c<?>, a.f> map, b bVar, Map<a<?>, Boolean> map2, a.C0111a<? extends e, com.google.android.gms.signin.a> aVar, ArrayList<ca> arrayList, ba baVar) {
        this.c = context;
        this.f1397a = lock;
        this.d = dVar;
        this.f = map;
        this.h = bVar;
        this.i = map2;
        this.j = aVar;
        this.m = agVar;
        this.n = baVar;
        ArrayList arrayList2 = arrayList;
        int size = arrayList2.size();
        int i2 = 0;
        while (i2 < size) {
            Object obj = arrayList2.get(i2);
            i2++;
            ((ca) obj).f1450b = this;
        }
        this.e = new ao(this, looper);
        this.f1398b = lock.newCondition();
        this.k = new af(this);
    }

    public final <A extends a.b, T extends c.a<? extends g, A>> T a(T t) {
        t.g();
        return this.k.a((c.a) t);
    }

    public final void a() {
        this.k.c();
    }

    public final void b() {
        if (this.k.b()) {
            this.g.clear();
        }
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        this.f1397a.lock();
        try {
            this.m.h();
            this.k = new r(this);
            this.k.a();
            this.f1398b.signalAll();
        } finally {
            this.f1397a.unlock();
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(ConnectionResult connectionResult) {
        this.f1397a.lock();
        try {
            this.o = connectionResult;
            this.k = new af(this);
            this.k.a();
            this.f1398b.signalAll();
        } finally {
            this.f1397a.unlock();
        }
    }

    public final boolean d() {
        return this.k instanceof r;
    }

    public final boolean e() {
        return this.k instanceof u;
    }

    public final void f() {
        if (d()) {
            r rVar = (r) this.k;
            if (rVar.f1493b) {
                rVar.f1493b = false;
                rVar.f1492a.m.e.a();
                rVar.b();
            }
        }
    }

    public final void a(ConnectionResult connectionResult, a<?> aVar, boolean z) {
        this.f1397a.lock();
        try {
            this.k.a(connectionResult, aVar, z);
        } finally {
            this.f1397a.unlock();
        }
    }

    public final void onConnected(Bundle bundle) {
        this.f1397a.lock();
        try {
            this.k.a(bundle);
        } finally {
            this.f1397a.unlock();
        }
    }

    public final void onConnectionSuspended(int i2) {
        this.f1397a.lock();
        try {
            this.k.a(i2);
        } finally {
            this.f1397a.unlock();
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(an anVar) {
        this.e.sendMessage(this.e.obtainMessage(1, anVar));
    }

    public final void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        String concat = String.valueOf(str).concat("  ");
        printWriter.append((CharSequence) str).append((CharSequence) "mState=").println(this.k);
        for (a next : this.i.keySet()) {
            printWriter.append((CharSequence) str).append((CharSequence) next.f1372b).println(":");
            this.f.get(next.b()).a(concat, printWriter);
        }
    }
}
