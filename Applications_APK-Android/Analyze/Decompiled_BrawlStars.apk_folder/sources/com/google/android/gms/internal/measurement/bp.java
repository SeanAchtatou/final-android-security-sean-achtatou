package com.google.android.gms.internal.measurement;

import android.content.SharedPreferences;
import android.util.Pair;
import com.google.android.gms.common.internal.l;

public final class bp {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bo f2087a;

    /* renamed from: b  reason: collision with root package name */
    private final String f2088b;
    private final long c;

    private bp(bo boVar, String str, long j) {
        this.f2087a = boVar;
        l.a(str);
        l.b(j > 0);
        this.f2088b = str;
        this.c = j;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        long a2 = this.f2087a.f().a();
        SharedPreferences.Editor edit = this.f2087a.f2085a.edit();
        edit.remove(d());
        edit.remove(e());
        edit.putLong(f(), a2);
        edit.commit();
    }

    public final Pair<String, Long> b() {
        long j;
        long c2 = c();
        if (c2 == 0) {
            j = 0;
        } else {
            j = Math.abs(c2 - this.f2087a.f().a());
        }
        long j2 = this.c;
        if (j < j2) {
            return null;
        }
        if (j > (j2 << 1)) {
            a();
            return null;
        }
        String string = this.f2087a.f2085a.getString(e(), null);
        long j3 = this.f2087a.f2085a.getLong(d(), 0);
        a();
        if (string == null || j3 <= 0) {
            return null;
        }
        return new Pair<>(string, Long.valueOf(j3));
    }

    /* access modifiers changed from: package-private */
    public final long c() {
        return this.f2087a.f2085a.getLong(f(), 0);
    }

    private final String f() {
        return String.valueOf(this.f2088b).concat(":start");
    }

    /* access modifiers changed from: package-private */
    public final String d() {
        return String.valueOf(this.f2088b).concat(":count");
    }

    /* access modifiers changed from: package-private */
    public final String e() {
        return String.valueOf(this.f2088b).concat(":value");
    }

    /* synthetic */ bp(bo boVar, String str, long j, byte b2) {
        this(boVar, str, j);
    }
}
