package com.google.android.gms.internal.measurement;

import java.util.Comparator;

final class el implements Comparator<ej> {
    el() {
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        ej ejVar = (ej) obj;
        ej ejVar2 = (ej) obj2;
        eq eqVar = (eq) ejVar.iterator();
        eq eqVar2 = (eq) ejVar2.iterator();
        while (eqVar.hasNext() && eqVar2.hasNext()) {
            int compare = Integer.compare(ej.a(eqVar.a()), ej.a(eqVar2.a()));
            if (compare != 0) {
                return compare;
            }
        }
        return Integer.compare(ejVar.a(), ejVar2.a());
    }
}
