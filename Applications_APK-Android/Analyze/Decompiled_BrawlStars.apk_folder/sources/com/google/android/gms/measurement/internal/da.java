package com.google.android.gms.measurement.internal;

final class da implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zzaj f2499a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ cx f2500b;

    da(cx cxVar, zzaj zzaj) {
        this.f2500b = cxVar;
        this.f2499a = zzaj;
    }

    public final void run() {
        synchronized (this.f2500b) {
            boolean unused = this.f2500b.f2491a = false;
            if (!this.f2500b.c.v()) {
                this.f2500b.c.q().j.a("Connected to remote service");
                this.f2500b.c.a(this.f2499a);
            }
        }
    }
}
