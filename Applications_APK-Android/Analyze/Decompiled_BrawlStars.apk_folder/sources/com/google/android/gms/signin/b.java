package com.google.android.gms.signin;

import android.support.v4.app.NotificationCompat;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.a;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    public static final a.C0111a<com.google.android.gms.signin.internal.a, a> f2607a = new c();

    /* renamed from: b  reason: collision with root package name */
    public static final a<a> f2608b = new a<>("SignIn.API", f2607a, c);
    private static final a.g<com.google.android.gms.signin.internal.a> c = new a.g<>();
    private static final a.g<com.google.android.gms.signin.internal.a> d = new a.g<>();
    private static final a.C0111a<com.google.android.gms.signin.internal.a, Object> e = new d();
    private static final Scope f = new Scope("profile");
    private static final Scope g = new Scope(NotificationCompat.CATEGORY_EMAIL);
    private static final a<Object> h = new a<>("SignIn.INTERNAL_API", e, d);
}
