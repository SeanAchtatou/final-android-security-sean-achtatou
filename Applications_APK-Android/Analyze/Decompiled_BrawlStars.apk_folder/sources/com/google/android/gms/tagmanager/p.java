package com.google.android.gms.tagmanager;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import java.io.IOException;

final class p implements aa {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ e f2669a;

    p(e eVar) {
        this.f2669a = eVar;
    }

    public final AdvertisingIdClient.Info a() {
        try {
            return AdvertisingIdClient.getAdvertisingIdInfo(this.f2669a.h);
        } catch (IllegalStateException e) {
            ab.b("IllegalStateException getting Advertising Id Info", e);
            return null;
        } catch (GooglePlayServicesRepairableException e2) {
            ab.b("GooglePlayServicesRepairableException getting Advertising Id Info", e2);
            return null;
        } catch (IOException e3) {
            ab.b("IOException getting Ad Id Info", e3);
            return null;
        } catch (GooglePlayServicesNotAvailableException e4) {
            e eVar = this.f2669a;
            eVar.f2656a = true;
            eVar.f2657b.interrupt();
            ab.b("GooglePlayServicesNotAvailableException getting Advertising Id Info", e4);
            return null;
        } catch (Exception e5) {
            ab.b("Unknown exception. Could not get the Advertising Id Info.", e5);
            return null;
        }
    }
}
