package com.google.android.gms.games.snapshot;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.d;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.Player;

public final class SnapshotMetadataRef extends d implements SnapshotMetadata {
    private final Game c;
    private final Player d;

    public final /* synthetic */ Object a() {
        return new SnapshotMetadataEntity(this);
    }

    public final Game b() {
        return this.c;
    }

    public final Player c() {
        return this.d;
    }

    public final String d() {
        return e("external_snapshot_id");
    }

    public final int describeContents() {
        return 0;
    }

    public final Uri e() {
        return h("cover_icon_image_uri");
    }

    public final boolean equals(Object obj) {
        return SnapshotMetadataEntity.a(this, obj);
    }

    public final float f() {
        float f = f("cover_icon_image_height");
        float f2 = f("cover_icon_image_width");
        if (f == 0.0f) {
            return 0.0f;
        }
        return f2 / f;
    }

    public final String g() {
        return e("unique_name");
    }

    public final String getCoverImageUrl() {
        return e("cover_icon_image_url");
    }

    public final String h() {
        return e("title");
    }

    public final int hashCode() {
        return SnapshotMetadataEntity.a(this);
    }

    public final String i() {
        return e("description");
    }

    public final long j() {
        return b("last_modified_timestamp");
    }

    public final long k() {
        return b("duration");
    }

    public final boolean l() {
        return c("pending_change_count") > 0;
    }

    public final long m() {
        return b("progress_value");
    }

    public final String n() {
        return e("device_name");
    }

    public final String toString() {
        return SnapshotMetadataEntity.b(this);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        ((SnapshotMetadataEntity) ((SnapshotMetadata) a())).writeToParcel(parcel, i);
    }
}
