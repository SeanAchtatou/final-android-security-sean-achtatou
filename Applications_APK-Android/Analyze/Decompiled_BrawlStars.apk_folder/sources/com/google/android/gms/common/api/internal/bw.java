package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.l;

final class bw {

    /* renamed from: a  reason: collision with root package name */
    final int f1441a;

    /* renamed from: b  reason: collision with root package name */
    final ConnectionResult f1442b;

    bw(ConnectionResult connectionResult, int i) {
        l.a(connectionResult);
        this.f1442b = connectionResult;
        this.f1441a = i;
    }
}
