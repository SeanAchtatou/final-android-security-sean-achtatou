package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.util.ArrayMap;
import com.google.android.gms.common.util.e;
import java.util.Map;

public final class a extends cj {

    /* renamed from: a  reason: collision with root package name */
    final Map<String, Long> f2358a = new ArrayMap();

    /* renamed from: b  reason: collision with root package name */
    final Map<String, Integer> f2359b = new ArrayMap();
    long c;

    public a(ar arVar) {
        super(arVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.ch.a(com.google.android.gms.measurement.internal.cg, android.os.Bundle, boolean):void
     arg types: [com.google.android.gms.measurement.internal.cg, android.os.Bundle, int]
     candidates:
      com.google.android.gms.measurement.internal.ch.a(com.google.android.gms.measurement.internal.ch, com.google.android.gms.measurement.internal.cg, boolean):void
      com.google.android.gms.measurement.internal.ch.a(android.app.Activity, com.google.android.gms.measurement.internal.cg, boolean):void
      com.google.android.gms.measurement.internal.ch.a(com.google.android.gms.measurement.internal.cg, android.os.Bundle, boolean):void */
    /* access modifiers changed from: package-private */
    public final void a(long j, cg cgVar) {
        if (cgVar == null) {
            q().k.a("Not logging ad exposure. No active activity");
        } else if (j < 1000) {
            q().k.a("Not logging ad exposure. Less than 1000 ms. exposure", Long.valueOf(j));
        } else {
            Bundle bundle = new Bundle();
            bundle.putLong("_xt", j);
            ch.a(cgVar, bundle, true);
            e().a("am", "_xa", bundle);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.ch.a(com.google.android.gms.measurement.internal.cg, android.os.Bundle, boolean):void
     arg types: [com.google.android.gms.measurement.internal.cg, android.os.Bundle, int]
     candidates:
      com.google.android.gms.measurement.internal.ch.a(com.google.android.gms.measurement.internal.ch, com.google.android.gms.measurement.internal.cg, boolean):void
      com.google.android.gms.measurement.internal.ch.a(android.app.Activity, com.google.android.gms.measurement.internal.cg, boolean):void
      com.google.android.gms.measurement.internal.ch.a(com.google.android.gms.measurement.internal.cg, android.os.Bundle, boolean):void */
    /* access modifiers changed from: package-private */
    public final void a(String str, long j, cg cgVar) {
        if (cgVar == null) {
            q().k.a("Not logging ad unit exposure. No active activity");
        } else if (j < 1000) {
            q().k.a("Not logging ad unit exposure. Less than 1000 ms. exposure", Long.valueOf(j));
        } else {
            Bundle bundle = new Bundle();
            bundle.putString("_ai", str);
            bundle.putLong("_xt", j);
            ch.a(cgVar, bundle, true);
            e().a("am", "_xu", bundle);
        }
    }

    public final void a(long j) {
        cg v = h().v();
        for (String next : this.f2358a.keySet()) {
            a(next, j - this.f2358a.get(next).longValue(), v);
        }
        if (!this.f2358a.isEmpty()) {
            a(j - this.c, v);
        }
        b(j);
    }

    /* access modifiers changed from: package-private */
    public final void b(long j) {
        for (String put : this.f2358a.keySet()) {
            this.f2358a.put(put, Long.valueOf(j));
        }
        if (!this.f2358a.isEmpty()) {
            this.c = j;
        }
    }

    public final /* bridge */ /* synthetic */ void a() {
        super.a();
    }

    public final /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    public final /* bridge */ /* synthetic */ void c() {
        super.c();
    }

    public final /* bridge */ /* synthetic */ a d() {
        return super.d();
    }

    public final /* bridge */ /* synthetic */ bv e() {
        return super.e();
    }

    public final /* bridge */ /* synthetic */ i f() {
        return super.f();
    }

    public final /* bridge */ /* synthetic */ cl g() {
        return super.g();
    }

    public final /* bridge */ /* synthetic */ ch h() {
        return super.h();
    }

    public final /* bridge */ /* synthetic */ k i() {
        return super.i();
    }

    public final /* bridge */ /* synthetic */ dj j() {
        return super.j();
    }

    public final /* bridge */ /* synthetic */ b k() {
        return super.k();
    }

    public final /* bridge */ /* synthetic */ e l() {
        return super.l();
    }

    public final /* bridge */ /* synthetic */ Context m() {
        return super.m();
    }

    public final /* bridge */ /* synthetic */ m n() {
        return super.n();
    }

    public final /* bridge */ /* synthetic */ ed o() {
        return super.o();
    }

    public final /* bridge */ /* synthetic */ am p() {
        return super.p();
    }

    public final /* bridge */ /* synthetic */ o q() {
        return super.q();
    }

    public final /* bridge */ /* synthetic */ z r() {
        return super.r();
    }

    public final /* bridge */ /* synthetic */ el s() {
        return super.s();
    }
}
