package com.google.android.gms.dynamic;

import android.content.Context;
import android.os.IBinder;
import com.google.android.gms.common.f;
import com.google.android.gms.common.internal.l;

public abstract class RemoteCreator<T> {

    /* renamed from: a  reason: collision with root package name */
    private final String f1776a;

    /* renamed from: b  reason: collision with root package name */
    private T f1777b;

    protected RemoteCreator(String str) {
        this.f1776a = str;
    }

    /* access modifiers changed from: protected */
    public abstract T a(IBinder iBinder);

    public static class RemoteCreatorException extends Exception {
        public RemoteCreatorException(String str) {
            super(str);
        }

        public RemoteCreatorException(String str, Throwable th) {
            super(str, th);
        }
    }

    /* access modifiers changed from: protected */
    public final T a(Context context) throws RemoteCreatorException {
        if (this.f1777b == null) {
            l.a(context);
            Context f = f.f(context);
            if (f != null) {
                try {
                    this.f1777b = a((IBinder) f.getClassLoader().loadClass(this.f1776a).newInstance());
                } catch (ClassNotFoundException e) {
                    throw new RemoteCreatorException("Could not load creator class.", e);
                } catch (InstantiationException e2) {
                    throw new RemoteCreatorException("Could not instantiate creator.", e2);
                } catch (IllegalAccessException e3) {
                    throw new RemoteCreatorException("Could not access creator.", e3);
                }
            } else {
                throw new RemoteCreatorException("Could not get remote context.");
            }
        }
        return this.f1777b;
    }
}
