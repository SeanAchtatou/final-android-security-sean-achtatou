package com.google.android.gms.common.images;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import java.util.Arrays;
import java.util.Locale;

public final class WebImage extends AbstractSafeParcelable {
    public static final Parcelable.Creator<WebImage> CREATOR = new d();

    /* renamed from: a  reason: collision with root package name */
    private final int f1549a;

    /* renamed from: b  reason: collision with root package name */
    private final Uri f1550b;
    private final int c;
    private final int d;

    WebImage(int i, Uri uri, int i2, int i3) {
        this.f1549a = i;
        this.f1550b = uri;
        this.c = i2;
        this.d = i3;
    }

    public final String toString() {
        return String.format(Locale.US, "Image %dx%d %s", Integer.valueOf(this.c), Integer.valueOf(this.d), this.f1550b.toString());
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && (obj instanceof WebImage)) {
            WebImage webImage = (WebImage) obj;
            return j.a(this.f1550b, webImage.f1550b) && this.c == webImage.c && this.d == webImage.d;
        }
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.f1550b, Integer.valueOf(this.c), Integer.valueOf(this.d)});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.net.Uri, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.b(parcel, 1, this.f1549a);
        a.a(parcel, 2, (Parcelable) this.f1550b, i, false);
        a.b(parcel, 3, this.c);
        a.b(parcel, 4, this.d);
        a.b(parcel, a2);
    }
}
