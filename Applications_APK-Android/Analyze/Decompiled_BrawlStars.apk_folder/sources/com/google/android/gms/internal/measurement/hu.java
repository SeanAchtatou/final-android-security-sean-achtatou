package com.google.android.gms.internal.measurement;

import java.util.Iterator;
import java.util.Map;

final class hu implements Iterator<Map.Entry<K, V>> {

    /* renamed from: a  reason: collision with root package name */
    private int f2274a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f2275b;
    private Iterator<Map.Entry<K, V>> c;
    private final /* synthetic */ hm d;

    private hu(hm hmVar) {
        this.d = hmVar;
        this.f2274a = -1;
    }

    public final boolean hasNext() {
        if (this.f2274a + 1 < this.d.c.size() || (!this.d.d.isEmpty() && a().hasNext())) {
            return true;
        }
        return false;
    }

    public final void remove() {
        if (this.f2275b) {
            this.f2275b = false;
            this.d.e();
            if (this.f2274a < this.d.c.size()) {
                hm hmVar = this.d;
                int i = this.f2274a;
                this.f2274a = i - 1;
                Object unused = hmVar.c(i);
                return;
            }
            a().remove();
            return;
        }
        throw new IllegalStateException("remove() was called before next()");
    }

    private final Iterator<Map.Entry<K, V>> a() {
        if (this.c == null) {
            this.c = this.d.d.entrySet().iterator();
        }
        return this.c;
    }

    public final /* synthetic */ Object next() {
        this.f2275b = true;
        int i = this.f2274a + 1;
        this.f2274a = i;
        if (i < this.d.c.size()) {
            return (Map.Entry) this.d.c.get(this.f2274a);
        }
        return (Map.Entry) a().next();
    }

    /* synthetic */ hu(hm hmVar, byte b2) {
        this(hmVar);
    }
}
