package com.google.android.gms.games.internal;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.View;

public class b {

    /* renamed from: a  reason: collision with root package name */
    protected zze f1803a;

    /* renamed from: b  reason: collision with root package name */
    protected c f1804b;

    private b(zze zze, int i) {
        this.f1803a = zze;
        a(i);
    }

    /* synthetic */ b(zze zze, int i, byte b2) {
        this(zze, i);
    }

    public static b a(zze zze, int i) {
        return new d(zze, i);
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        this.f1804b = new c(i, new Binder(), (byte) 0);
    }

    public void a(View view) {
    }

    public void a() {
        zze zze = this.f1803a;
        IBinder iBinder = this.f1804b.f1805a;
        Bundle a2 = this.f1804b.a();
        if (zze.b()) {
            try {
                ((zzy) zze.p()).a(iBinder, a2);
            } catch (RemoteException e) {
                zze.a(e);
            }
        }
    }
}
