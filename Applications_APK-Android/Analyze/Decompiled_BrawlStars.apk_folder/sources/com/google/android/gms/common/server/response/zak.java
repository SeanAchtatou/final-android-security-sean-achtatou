package com.google.android.gms.common.server.response;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.server.response.FastJsonResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public final class zak extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zak> CREATOR = new k();

    /* renamed from: a  reason: collision with root package name */
    final String f1649a;

    /* renamed from: b  reason: collision with root package name */
    private final int f1650b;
    private final HashMap<String, Map<String, FastJsonResponse.Field<?, ?>>> c;
    private final ArrayList<zal> d = null;

    zak(int i, ArrayList<zal> arrayList, String str) {
        this.f1650b = i;
        HashMap<String, Map<String, FastJsonResponse.Field<?, ?>>> hashMap = new HashMap<>();
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            zal zal = arrayList.get(i2);
            String str2 = zal.f1651a;
            HashMap hashMap2 = new HashMap();
            int size2 = zal.f1652b.size();
            for (int i3 = 0; i3 < size2; i3++) {
                zam zam = zal.f1652b.get(i3);
                hashMap2.put(zam.f1653a, zam.f1654b);
            }
            hashMap.put(str2, hashMap2);
        }
        this.c = hashMap;
        this.f1649a = (String) l.a((Object) str);
        a();
    }

    private void a() {
        for (String str : this.c.keySet()) {
            Map map = this.c.get(str);
            for (String str2 : map.keySet()) {
                ((FastJsonResponse.Field) map.get(str2)).h = this;
            }
        }
    }

    public final Map<String, FastJsonResponse.Field<?, ?>> a(String str) {
        return this.c.get(str);
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        for (String next : this.c.keySet()) {
            sb.append(next);
            sb.append(":\n");
            Map map = this.c.get(next);
            for (String str : map.keySet()) {
                sb.append("  ");
                sb.append(str);
                sb.append(": ");
                sb.append(map.get(str));
            }
        }
        return sb.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.b(parcel, 1, this.f1650b);
        ArrayList arrayList = new ArrayList();
        for (String next : this.c.keySet()) {
            arrayList.add(new zal(next, this.c.get(next)));
        }
        a.b(parcel, 2, arrayList, false);
        a.a(parcel, 3, this.f1649a, false);
        a.b(parcel, a2);
    }
}
