package com.google.android.gms.measurement.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.internal.measurement.dk;
import com.google.android.gms.internal.measurement.zzq;
import java.util.ArrayList;
import java.util.List;

public final class zzal extends zzq implements zzaj {
    zzal(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.internal.IMeasurementService");
    }

    public final void a(zzag zzag, zzk zzk) throws RemoteException {
        Parcel a2 = a();
        dk.a(a2, zzag);
        dk.a(a2, zzk);
        b(1, a2);
    }

    public final void a(zzfv zzfv, zzk zzk) throws RemoteException {
        Parcel a2 = a();
        dk.a(a2, zzfv);
        dk.a(a2, zzk);
        b(2, a2);
    }

    public final void a(zzk zzk) throws RemoteException {
        Parcel a2 = a();
        dk.a(a2, zzk);
        b(4, a2);
    }

    public final void a(zzag zzag, String str, String str2) throws RemoteException {
        Parcel a2 = a();
        dk.a(a2, zzag);
        a2.writeString(str);
        a2.writeString(str2);
        b(5, a2);
    }

    public final void b(zzk zzk) throws RemoteException {
        Parcel a2 = a();
        dk.a(a2, zzk);
        b(6, a2);
    }

    public final List<zzfv> a(zzk zzk, boolean z) throws RemoteException {
        Parcel a2 = a();
        dk.a(a2, zzk);
        dk.a(a2, z);
        Parcel a3 = a(7, a2);
        ArrayList createTypedArrayList = a3.createTypedArrayList(zzfv.CREATOR);
        a3.recycle();
        return createTypedArrayList;
    }

    public final byte[] a(zzag zzag, String str) throws RemoteException {
        Parcel a2 = a();
        dk.a(a2, zzag);
        a2.writeString(str);
        Parcel a3 = a(9, a2);
        byte[] createByteArray = a3.createByteArray();
        a3.recycle();
        return createByteArray;
    }

    public final void a(long j, String str, String str2, String str3) throws RemoteException {
        Parcel a2 = a();
        a2.writeLong(j);
        a2.writeString(str);
        a2.writeString(str2);
        a2.writeString(str3);
        b(10, a2);
    }

    public final String c(zzk zzk) throws RemoteException {
        Parcel a2 = a();
        dk.a(a2, zzk);
        Parcel a3 = a(11, a2);
        String readString = a3.readString();
        a3.recycle();
        return readString;
    }

    public final void a(zzo zzo, zzk zzk) throws RemoteException {
        Parcel a2 = a();
        dk.a(a2, zzo);
        dk.a(a2, zzk);
        b(12, a2);
    }

    public final void a(zzo zzo) throws RemoteException {
        Parcel a2 = a();
        dk.a(a2, zzo);
        b(13, a2);
    }

    public final List<zzfv> a(String str, String str2, boolean z, zzk zzk) throws RemoteException {
        Parcel a2 = a();
        a2.writeString(str);
        a2.writeString(str2);
        dk.a(a2, z);
        dk.a(a2, zzk);
        Parcel a3 = a(14, a2);
        ArrayList createTypedArrayList = a3.createTypedArrayList(zzfv.CREATOR);
        a3.recycle();
        return createTypedArrayList;
    }

    public final List<zzfv> a(String str, String str2, String str3, boolean z) throws RemoteException {
        Parcel a2 = a();
        a2.writeString(str);
        a2.writeString(str2);
        a2.writeString(str3);
        dk.a(a2, z);
        Parcel a3 = a(15, a2);
        ArrayList createTypedArrayList = a3.createTypedArrayList(zzfv.CREATOR);
        a3.recycle();
        return createTypedArrayList;
    }

    public final List<zzo> a(String str, String str2, zzk zzk) throws RemoteException {
        Parcel a2 = a();
        a2.writeString(str);
        a2.writeString(str2);
        dk.a(a2, zzk);
        Parcel a3 = a(16, a2);
        ArrayList createTypedArrayList = a3.createTypedArrayList(zzo.CREATOR);
        a3.recycle();
        return createTypedArrayList;
    }

    public final List<zzo> a(String str, String str2, String str3) throws RemoteException {
        Parcel a2 = a();
        a2.writeString(str);
        a2.writeString(str2);
        a2.writeString(str3);
        Parcel a3 = a(17, a2);
        ArrayList createTypedArrayList = a3.createTypedArrayList(zzo.CREATOR);
        a3.recycle();
        return createTypedArrayList;
    }

    public final void d(zzk zzk) throws RemoteException {
        Parcel a2 = a();
        dk.a(a2, zzk);
        b(18, a2);
    }
}
