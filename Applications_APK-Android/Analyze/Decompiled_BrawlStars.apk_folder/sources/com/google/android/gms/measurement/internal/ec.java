package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.l;

final class ec {

    /* renamed from: a  reason: collision with root package name */
    final String f2541a;

    /* renamed from: b  reason: collision with root package name */
    final String f2542b;
    final String c;
    final long d;
    final Object e;

    ec(String str, String str2, String str3, long j, Object obj) {
        l.a(str);
        l.a(str3);
        l.a(obj);
        this.f2541a = str;
        this.f2542b = str2;
        this.c = str3;
        this.d = j;
        this.e = obj;
    }
}
