package com.google.android.gms.measurement.internal;

import com.google.android.gms.measurement.AppMeasurement;

final class by implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ AppMeasurement.ConditionalUserProperty f2448a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ bv f2449b;

    by(bv bvVar, AppMeasurement.ConditionalUserProperty conditionalUserProperty) {
        this.f2449b = bvVar;
        this.f2448a = conditionalUserProperty;
    }

    public final void run() {
        bv.a(this.f2449b, this.f2448a);
    }
}
