package com.google.android.gms.common.api.internal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.ArrayMap;
import com.google.android.gms.internal.common.c;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;

public final class ck extends Fragment implements g {

    /* renamed from: a  reason: collision with root package name */
    private static WeakHashMap<FragmentActivity, WeakReference<ck>> f1462a = new WeakHashMap<>();

    /* renamed from: b  reason: collision with root package name */
    private Map<String, LifecycleCallback> f1463b = new ArrayMap();
    /* access modifiers changed from: private */
    public int c = 0;
    /* access modifiers changed from: private */
    public Bundle d;

    public static ck a(FragmentActivity fragmentActivity) {
        ck ckVar;
        WeakReference weakReference = f1462a.get(fragmentActivity);
        if (weakReference != null && (ckVar = (ck) weakReference.get()) != null) {
            return ckVar;
        }
        try {
            ck ckVar2 = (ck) fragmentActivity.getSupportFragmentManager().findFragmentByTag("SupportLifecycleFragmentImpl");
            if (ckVar2 == null || ckVar2.isRemoving()) {
                ckVar2 = new ck();
                fragmentActivity.getSupportFragmentManager().beginTransaction().add(ckVar2, "SupportLifecycleFragmentImpl").commitAllowingStateLoss();
            }
            f1462a.put(fragmentActivity, new WeakReference(ckVar2));
            return ckVar2;
        } catch (ClassCastException e) {
            throw new IllegalStateException("Fragment with tag SupportLifecycleFragmentImpl is not a SupportLifecycleFragmentImpl", e);
        }
    }

    public final <T extends LifecycleCallback> T a(String str, Class<T> cls) {
        return (LifecycleCallback) cls.cast(this.f1463b.get(str));
    }

    public final void a(String str, LifecycleCallback lifecycleCallback) {
        if (!this.f1463b.containsKey(str)) {
            this.f1463b.put(str, lifecycleCallback);
            if (this.c > 0) {
                new c(Looper.getMainLooper()).post(new cl(this, lifecycleCallback, str));
                return;
            }
            return;
        }
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 59);
        sb.append("LifecycleCallback with tag ");
        sb.append(str);
        sb.append(" already added to this fragment.");
        throw new IllegalArgumentException(sb.toString());
    }

    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.c = 1;
        this.d = bundle;
        for (Map.Entry next : this.f1463b.entrySet()) {
            ((LifecycleCallback) next.getValue()).a(bundle != null ? bundle.getBundle((String) next.getKey()) : null);
        }
    }

    public final void onStart() {
        super.onStart();
        this.c = 2;
        for (LifecycleCallback b2 : this.f1463b.values()) {
            b2.b();
        }
    }

    public final void onResume() {
        super.onResume();
        this.c = 3;
        for (LifecycleCallback c2 : this.f1463b.values()) {
            c2.c();
        }
    }

    public final void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        for (LifecycleCallback a2 : this.f1463b.values()) {
            a2.a(i, i2, intent);
        }
    }

    public final void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (bundle != null) {
            for (Map.Entry next : this.f1463b.entrySet()) {
                Bundle bundle2 = new Bundle();
                ((LifecycleCallback) next.getValue()).b(bundle2);
                bundle.putBundle((String) next.getKey(), bundle2);
            }
        }
    }

    public final void onStop() {
        super.onStop();
        this.c = 4;
        for (LifecycleCallback d2 : this.f1463b.values()) {
            d2.d();
        }
    }

    public final void onDestroy() {
        super.onDestroy();
        this.c = 5;
        Iterator<LifecycleCallback> it = this.f1463b.values().iterator();
        while (it.hasNext()) {
            it.next();
        }
    }

    public final void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        for (LifecycleCallback a2 : this.f1463b.values()) {
            a2.a(str, fileDescriptor, printWriter, strArr);
        }
    }

    public final /* synthetic */ Activity a() {
        return getActivity();
    }
}
