package com.google.android.gms.analytics.a;

import com.google.android.gms.analytics.l;
import java.util.HashMap;
import java.util.Map;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private Map<String, String> f1316a = new HashMap();

    public final Map<String, String> a(String str) {
        HashMap hashMap = new HashMap();
        for (Map.Entry next : this.f1316a.entrySet()) {
            String valueOf = String.valueOf(str);
            String valueOf2 = String.valueOf((String) next.getKey());
            hashMap.put(valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf), (String) next.getValue());
        }
        return hashMap;
    }

    public String toString() {
        return l.a((Map) this.f1316a);
    }
}
