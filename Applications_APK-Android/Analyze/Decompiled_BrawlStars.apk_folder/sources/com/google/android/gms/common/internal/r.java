package com.google.android.gms.common.internal;

import android.app.Activity;
import android.content.Intent;

final class r extends d {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Intent f1617a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ Activity f1618b;
    private final /* synthetic */ int c;

    r(Intent intent, Activity activity, int i) {
        this.f1617a = intent;
        this.f1618b = activity;
        this.c = i;
    }

    public final void a() {
        Intent intent = this.f1617a;
        if (intent != null) {
            this.f1618b.startActivityForResult(intent, this.c);
        }
    }
}
