package com.google.android.gms.common.api.internal;

import android.util.Log;
import android.util.SparseArray;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.d;
import com.google.android.gms.common.internal.l;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class bt extends bv {

    /* renamed from: b  reason: collision with root package name */
    private final SparseArray<a> f1435b = new SparseArray<>();

    public static bt b(f fVar) {
        g a2 = a(fVar);
        bt btVar = (bt) a2.a("AutoManageHelper", bt.class);
        if (btVar != null) {
            return btVar;
        }
        return new bt(a2);
    }

    class a implements d.c {

        /* renamed from: a  reason: collision with root package name */
        public final int f1436a;

        /* renamed from: b  reason: collision with root package name */
        public final d f1437b;
        public final d.c c;

        public a(int i, d dVar, d.c cVar) {
            this.f1436a = i;
            this.f1437b = dVar;
            this.c = cVar;
            dVar.a(this);
        }

        public final void onConnectionFailed(ConnectionResult connectionResult) {
            String valueOf = String.valueOf(connectionResult);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("beginFailureResolution for ");
            sb.append(valueOf);
            sb.toString();
            bt.this.b(connectionResult, this.f1436a);
        }
    }

    private bt(g gVar) {
        super(gVar);
        this.f1381a.a("AutoManageHelper", this);
    }

    public final void a(int i, d dVar, d.c cVar) {
        l.a(dVar, "GoogleApiClient instance cannot be null");
        boolean z = this.f1435b.indexOfKey(i) < 0;
        StringBuilder sb = new StringBuilder(54);
        sb.append("Already managing a GoogleApiClient with id ");
        sb.append(i);
        l.a(z, sb.toString());
        bw bwVar = (bw) this.d.get();
        boolean z2 = this.c;
        String valueOf = String.valueOf(bwVar);
        StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf).length() + 49);
        sb2.append("starting AutoManage for client ");
        sb2.append(i);
        sb2.append(" ");
        sb2.append(z2);
        sb2.append(" ");
        sb2.append(valueOf);
        sb2.toString();
        this.f1435b.put(i, new a(i, dVar, cVar));
        if (this.c && bwVar == null) {
            String valueOf2 = String.valueOf(dVar);
            StringBuilder sb3 = new StringBuilder(String.valueOf(valueOf2).length() + 11);
            sb3.append("connecting ");
            sb3.append(valueOf2);
            sb3.toString();
            dVar.b();
        }
    }

    public final void b() {
        super.b();
        boolean z = this.c;
        String valueOf = String.valueOf(this.f1435b);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 14);
        sb.append("onStart ");
        sb.append(z);
        sb.append(" ");
        sb.append(valueOf);
        sb.toString();
        if (this.d.get() == null) {
            for (int i = 0; i < this.f1435b.size(); i++) {
                a a2 = a(i);
                if (a2 != null) {
                    a2.f1437b.b();
                }
            }
        }
    }

    public final void d() {
        super.d();
        for (int i = 0; i < this.f1435b.size(); i++) {
            a a2 = a(i);
            if (a2 != null) {
                a2.f1437b.c();
            }
        }
    }

    public final void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        for (int i = 0; i < this.f1435b.size(); i++) {
            a a2 = a(i);
            if (a2 != null) {
                printWriter.append((CharSequence) str).append((CharSequence) "GoogleApiClient #").print(a2.f1436a);
                printWriter.println(":");
                a2.f1437b.a(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(ConnectionResult connectionResult, int i) {
        if (i < 0) {
            Log.wtf("AutoManageHelper", "AutoManageLifecycleHelper received onErrorResolutionFailed callback but no failing client ID is set", new Exception());
            return;
        }
        a aVar = this.f1435b.get(i);
        if (aVar != null) {
            a aVar2 = this.f1435b.get(i);
            this.f1435b.remove(i);
            if (aVar2 != null) {
                aVar2.f1437b.b(aVar2);
                aVar2.f1437b.c();
            }
            d.c cVar = aVar.c;
            if (cVar != null) {
                cVar.onConnectionFailed(connectionResult);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void e() {
        for (int i = 0; i < this.f1435b.size(); i++) {
            a a2 = a(i);
            if (a2 != null) {
                a2.f1437b.b();
            }
        }
    }

    private final a a(int i) {
        if (this.f1435b.size() <= i) {
            return null;
        }
        SparseArray<a> sparseArray = this.f1435b;
        return sparseArray.get(sparseArray.keyAt(i));
    }
}
