package com.google.android.gms.common.api;

import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.internal.bs;
import com.google.android.gms.common.internal.l;
import java.util.ArrayList;

public class AvailabilityException extends Exception {

    /* renamed from: a  reason: collision with root package name */
    public final ArrayMap<bs<?>, ConnectionResult> f1364a;

    public AvailabilityException(ArrayMap<bs<?>, ConnectionResult> arrayMap) {
        this.f1364a = arrayMap;
    }

    public String getMessage() {
        ArrayList arrayList = new ArrayList();
        boolean z = true;
        for (bs next : this.f1364a.keySet()) {
            ConnectionResult connectionResult = this.f1364a.get(next);
            if (connectionResult.b()) {
                z = false;
            }
            String str = next.f1433a.f1372b;
            String valueOf = String.valueOf(connectionResult);
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 2 + String.valueOf(valueOf).length());
            sb.append(str);
            sb.append(": ");
            sb.append(valueOf);
            arrayList.add(sb.toString());
        }
        StringBuilder sb2 = new StringBuilder();
        if (z) {
            sb2.append("None of the queried APIs are available. ");
        } else {
            sb2.append("Some of the queried APIs are unavailable. ");
        }
        sb2.append(TextUtils.join("; ", arrayList));
        return sb2.toString();
    }

    public final ConnectionResult a(c<? extends a.d> cVar) {
        bs<O> bsVar = cVar.f1374b;
        l.b(this.f1364a.get(bsVar) != null, "The given API was not part of the availability request.");
        return this.f1364a.get(bsVar);
    }
}
