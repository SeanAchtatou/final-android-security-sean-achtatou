package com.google.android.gms.games.multiplayer.realtime;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;
import com.google.android.gms.games.internal.e;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.ParticipantEntity;
import java.util.ArrayList;
import java.util.Arrays;

public final class RoomEntity extends GamesDowngradeableSafeParcel implements Room {
    public static final Parcelable.Creator<RoomEntity> CREATOR = new a();

    /* renamed from: b  reason: collision with root package name */
    private final String f1832b;
    private final String c;
    private final long d;
    private final int e;
    private final String f;
    private final int g;
    private final Bundle h;
    private final ArrayList<ParticipantEntity> i;
    private final int j;

    static final class a extends c {
        a() {
        }

        public final RoomEntity a(Parcel parcel) {
            if (RoomEntity.b(RoomEntity.b_()) || RoomEntity.a(RoomEntity.class.getCanonicalName())) {
                return super.createFromParcel(parcel);
            }
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            long readLong = parcel.readLong();
            int readInt = parcel.readInt();
            String readString3 = parcel.readString();
            int readInt2 = parcel.readInt();
            Bundle readBundle = parcel.readBundle();
            int readInt3 = parcel.readInt();
            ArrayList arrayList = new ArrayList(readInt3);
            for (int i = 0; i < readInt3; i++) {
                arrayList.add(ParticipantEntity.CREATOR.createFromParcel(parcel));
            }
            return new RoomEntity(readString, readString2, readLong, readInt, readString3, readInt2, readBundle, arrayList, -1);
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return createFromParcel(parcel);
        }
    }

    public RoomEntity(Room room) {
        this.f1832b = room.b();
        this.c = room.c();
        this.d = room.d();
        this.e = room.e();
        this.f = room.f();
        this.g = room.g();
        this.h = room.h();
        ArrayList<Participant> i2 = room.i();
        int size = i2.size();
        this.i = new ArrayList<>(size);
        for (int i3 = 0; i3 < size; i3++) {
            this.i.add((ParticipantEntity) i2.get(i3).a());
        }
        this.j = room.j();
    }

    RoomEntity(String str, String str2, long j2, int i2, String str3, int i3, Bundle bundle, ArrayList<ParticipantEntity> arrayList, int i4) {
        this.f1832b = str;
        this.c = str2;
        this.d = j2;
        this.e = i2;
        this.f = str3;
        this.g = i3;
        this.h = bundle;
        this.i = arrayList;
        this.j = i4;
    }

    static boolean a(Room room, Object obj) {
        if (!(obj instanceof Room)) {
            return false;
        }
        if (room == obj) {
            return true;
        }
        Room room2 = (Room) obj;
        return j.a(room2.b(), room.b()) && j.a(room2.c(), room.c()) && j.a(Long.valueOf(room2.d()), Long.valueOf(room.d())) && j.a(Integer.valueOf(room2.e()), Integer.valueOf(room.e())) && j.a(room2.f(), room.f()) && j.a(Integer.valueOf(room2.g()), Integer.valueOf(room.g())) && e.a(room2.h(), room.h()) && j.a(room2.i(), room.i()) && j.a(Integer.valueOf(room2.j()), Integer.valueOf(room.j()));
    }

    static /* synthetic */ boolean a(String str) {
        return true;
    }

    static String b(Room room) {
        return j.a(room).a("RoomId", room.b()).a("CreatorId", room.c()).a("CreationTimestamp", Long.valueOf(room.d())).a("RoomStatus", Integer.valueOf(room.e())).a("Description", room.f()).a("Variant", Integer.valueOf(room.g())).a("AutoMatchCriteria", room.h()).a("Participants", room.i()).a("AutoMatchWaitEstimateSeconds", Integer.valueOf(room.j())).toString();
    }

    public final /* bridge */ /* synthetic */ Object a() {
        return this;
    }

    public final String b() {
        return this.f1832b;
    }

    public final String c() {
        return this.c;
    }

    public final long d() {
        return this.d;
    }

    public final int e() {
        return this.e;
    }

    public final boolean equals(Object obj) {
        return a(this, obj);
    }

    public final String f() {
        return this.f;
    }

    public final int g() {
        return this.g;
    }

    public final Bundle h() {
        return this.h;
    }

    public final int hashCode() {
        return a(this);
    }

    public final ArrayList<Participant> i() {
        return new ArrayList<>(this.i);
    }

    public final int j() {
        return this.j;
    }

    public final String toString() {
        return b(this);
    }

    static int a(Room room) {
        return Arrays.hashCode(new Object[]{room.b(), room.c(), Long.valueOf(room.d()), Integer.valueOf(room.e()), room.f(), Integer.valueOf(room.g()), Integer.valueOf(e.a(room.h())), room.i(), Integer.valueOf(room.j())});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
     arg types: [android.os.Parcel, int, android.os.Bundle, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void */
    public final void writeToParcel(Parcel parcel, int i2) {
        if (!this.f1575a) {
            int a2 = com.google.android.gms.common.internal.safeparcel.a.a(parcel, 20293);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 1, this.f1832b, false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 2, this.c, false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 3, this.d);
            com.google.android.gms.common.internal.safeparcel.a.b(parcel, 4, this.e);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 5, this.f, false);
            com.google.android.gms.common.internal.safeparcel.a.b(parcel, 6, this.g);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 7, this.h, false);
            com.google.android.gms.common.internal.safeparcel.a.b(parcel, 8, i(), false);
            com.google.android.gms.common.internal.safeparcel.a.b(parcel, 9, this.j);
            com.google.android.gms.common.internal.safeparcel.a.b(parcel, a2);
            return;
        }
        parcel.writeString(this.f1832b);
        parcel.writeString(this.c);
        parcel.writeLong(this.d);
        parcel.writeInt(this.e);
        parcel.writeString(this.f);
        parcel.writeInt(this.g);
        parcel.writeBundle(this.h);
        int size = this.i.size();
        parcel.writeInt(size);
        for (int i3 = 0; i3 < size; i3++) {
            this.i.get(i3).writeToParcel(parcel, i2);
        }
    }
}
