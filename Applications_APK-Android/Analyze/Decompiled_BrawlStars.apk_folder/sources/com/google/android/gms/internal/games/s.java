package com.google.android.gms.internal.games;

import java.util.concurrent.atomic.AtomicReference;

public abstract class s {

    /* renamed from: a  reason: collision with root package name */
    private final AtomicReference<r> f1993a = new AtomicReference<>();

    public final void a() {
        r rVar = this.f1993a.get();
        if (rVar != null) {
            rVar.a();
        }
    }
}
