package com.google.android.gms.measurement.internal;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.google.android.gms.internal.measurement.zzu;
import com.google.android.gms.internal.measurement.zzv;

public final class af implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ae f2369a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final String f2370b;

    af(ae aeVar, String str) {
        this.f2369a = aeVar;
        this.f2370b = str;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (iBinder == null) {
            this.f2369a.f2368a.q().f.a("Install Referrer connection returned with null binder");
            return;
        }
        try {
            zzu a2 = zzv.a(iBinder);
            if (a2 == null) {
                this.f2369a.f2368a.q().f.a("Install Referrer Service implementation was not found");
                return;
            }
            this.f2369a.f2368a.q().i.a("Install Referrer Service connected");
            this.f2369a.f2368a.p().a(new ag(this, a2, this));
        } catch (Exception e) {
            this.f2369a.f2368a.q().f.a("Exception occurred while calling Install Referrer API", e);
        }
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        this.f2369a.f2368a.q().i.a("Install Referrer Service disconnected");
    }
}
