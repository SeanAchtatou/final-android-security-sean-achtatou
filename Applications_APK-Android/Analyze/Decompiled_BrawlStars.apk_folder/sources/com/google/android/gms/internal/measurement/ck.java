package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class ck extends iz<ck> {
    private static volatile ck[] e;

    /* renamed from: a  reason: collision with root package name */
    public cn f2118a = null;

    /* renamed from: b  reason: collision with root package name */
    public cl f2119b = null;
    public Boolean c = null;
    public String d = null;

    public static ck[] a() {
        if (e == null) {
            synchronized (jd.f2312b) {
                if (e == null) {
                    e = new ck[0];
                }
            }
        }
        return e;
    }

    public ck() {
        this.L = null;
        this.M = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ck)) {
            return false;
        }
        ck ckVar = (ck) obj;
        cn cnVar = this.f2118a;
        if (cnVar == null) {
            if (ckVar.f2118a != null) {
                return false;
            }
        } else if (!cnVar.equals(ckVar.f2118a)) {
            return false;
        }
        cl clVar = this.f2119b;
        if (clVar == null) {
            if (ckVar.f2119b != null) {
                return false;
            }
        } else if (!clVar.equals(ckVar.f2119b)) {
            return false;
        }
        Boolean bool = this.c;
        if (bool == null) {
            if (ckVar.c != null) {
                return false;
            }
        } else if (!bool.equals(ckVar.c)) {
            return false;
        }
        String str = this.d;
        if (str == null) {
            if (ckVar.d != null) {
                return false;
            }
        } else if (!str.equals(ckVar.d)) {
            return false;
        }
        if (this.L == null || this.L.a()) {
            return ckVar.L == null || ckVar.L.a();
        }
        return this.L.equals(ckVar.L);
    }

    public final int hashCode() {
        int i;
        int i2;
        cn cnVar = this.f2118a;
        int hashCode = (getClass().getName().hashCode() + 527) * 31;
        int i3 = 0;
        if (cnVar == null) {
            i = 0;
        } else {
            i = cnVar.hashCode();
        }
        int i4 = hashCode + i;
        cl clVar = this.f2119b;
        int i5 = i4 * 31;
        if (clVar == null) {
            i2 = 0;
        } else {
            i2 = clVar.hashCode();
        }
        int i6 = (i5 + i2) * 31;
        Boolean bool = this.c;
        int hashCode2 = (i6 + (bool == null ? 0 : bool.hashCode())) * 31;
        String str = this.d;
        int hashCode3 = (hashCode2 + (str == null ? 0 : str.hashCode())) * 31;
        if (this.L != null && !this.L.a()) {
            i3 = this.L.hashCode();
        }
        return hashCode3 + i3;
    }

    public final void a(iy iyVar) throws IOException {
        cn cnVar = this.f2118a;
        if (cnVar != null) {
            iyVar.a(1, cnVar);
        }
        cl clVar = this.f2119b;
        if (clVar != null) {
            iyVar.a(2, clVar);
        }
        Boolean bool = this.c;
        if (bool != null) {
            iyVar.a(3, bool.booleanValue());
        }
        String str = this.d;
        if (str != null) {
            iyVar.a(4, str);
        }
        super.a(iyVar);
    }

    /* access modifiers changed from: protected */
    public final int b() {
        int b2 = super.b();
        cn cnVar = this.f2118a;
        if (cnVar != null) {
            b2 += iy.b(1, cnVar);
        }
        cl clVar = this.f2119b;
        if (clVar != null) {
            b2 += iy.b(2, clVar);
        }
        Boolean bool = this.c;
        if (bool != null) {
            bool.booleanValue();
            b2 += iy.c(24) + 1;
        }
        String str = this.d;
        return str != null ? b2 + iy.b(4, str) : b2;
    }

    public final /* synthetic */ je a(iw iwVar) throws IOException {
        while (true) {
            int a2 = iwVar.a();
            if (a2 == 0) {
                return this;
            }
            if (a2 == 10) {
                if (this.f2118a == null) {
                    this.f2118a = new cn();
                }
                iwVar.a(this.f2118a);
            } else if (a2 == 18) {
                if (this.f2119b == null) {
                    this.f2119b = new cl();
                }
                iwVar.a(this.f2119b);
            } else if (a2 == 24) {
                this.c = Boolean.valueOf(iwVar.b());
            } else if (a2 == 34) {
                this.d = iwVar.c();
            } else if (!super.a(iwVar, a2)) {
                return this;
            }
        }
    }
}
