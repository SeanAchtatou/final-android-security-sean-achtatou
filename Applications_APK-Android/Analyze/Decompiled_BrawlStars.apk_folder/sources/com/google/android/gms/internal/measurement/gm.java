package com.google.android.gms.internal.measurement;

final class gm<K, V> {

    /* renamed from: a  reason: collision with root package name */
    public final ip f2244a;

    /* renamed from: b  reason: collision with root package name */
    public final K f2245b;
    public final ip c;
    public final V d;
}
