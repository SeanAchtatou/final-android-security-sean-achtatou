package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcelable;
import com.google.android.gms.common.data.e;
import com.google.android.gms.games.internal.player.zza;

public interface Player extends Parcelable, e<Player> {
    String b();

    String c();

    String d();

    String e();

    boolean f();

    Uri g();

    @Deprecated
    String getBannerImageLandscapeUrl();

    @Deprecated
    String getBannerImagePortraitUrl();

    @Deprecated
    String getHiResImageUrl();

    @Deprecated
    String getIconImageUrl();

    Uri h();

    long i();

    long j();

    @Deprecated
    int k();

    boolean l();

    String m();

    PlayerLevelInfo n();

    zza o();

    Uri p();

    Uri q();

    int r();

    long s();

    boolean t();
}
