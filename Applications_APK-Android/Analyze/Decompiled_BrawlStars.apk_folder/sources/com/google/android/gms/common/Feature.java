package com.google.android.gms.common;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.internal.ServerProtocol;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import java.util.Arrays;

public class Feature extends AbstractSafeParcelable {
    public static final Parcelable.Creator<Feature> CREATOR = new j();

    /* renamed from: a  reason: collision with root package name */
    public final String f1354a;
    @Deprecated

    /* renamed from: b  reason: collision with root package name */
    private final int f1355b;
    private final long c;

    public Feature(String str, long j) {
        this.f1354a = str;
        this.c = 1;
        this.f1355b = -1;
    }

    public Feature(String str, int i, long j) {
        this.f1354a = str;
        this.f1355b = i;
        this.c = j;
    }

    public final long a() {
        long j = this.c;
        return j == -1 ? (long) this.f1355b : j;
    }

    public boolean equals(Object obj) {
        if (obj instanceof Feature) {
            Feature feature = (Feature) obj;
            String str = this.f1354a;
            if (((str == null || !str.equals(feature.f1354a)) && (this.f1354a != null || feature.f1354a != null)) || a() != feature.a()) {
                return false;
            }
            return true;
        }
        return false;
    }

    public String toString() {
        return j.a(this).a("name", this.f1354a).a(ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION, Long.valueOf(a())).toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void */
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.a(parcel, 1, this.f1354a, false);
        a.b(parcel, 2, this.f1355b);
        a.a(parcel, 3, a());
        a.b(parcel, a2);
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.f1354a, Long.valueOf(a())});
    }
}
