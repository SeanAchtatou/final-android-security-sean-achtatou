package com.google.android.gms.auth.api.signin.internal;

public class a {

    /* renamed from: b  reason: collision with root package name */
    private static int f1348b = 31;

    /* renamed from: a  reason: collision with root package name */
    public int f1349a = 1;

    public final a a(Object obj) {
        this.f1349a = (f1348b * this.f1349a) + (obj == null ? 0 : obj.hashCode());
        return this;
    }

    public final a a(boolean z) {
        this.f1349a = (f1348b * this.f1349a) + (z ? 1 : 0);
        return this;
    }
}
