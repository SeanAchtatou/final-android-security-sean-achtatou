package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.c;

public final class bb {

    /* renamed from: a  reason: collision with root package name */
    public final ai f1414a;

    /* renamed from: b  reason: collision with root package name */
    public final int f1415b;
    public final c<?> c;

    public bb(ai aiVar, int i, c<?> cVar) {
        this.f1414a = aiVar;
        this.f1415b = i;
        this.c = cVar;
    }
}
