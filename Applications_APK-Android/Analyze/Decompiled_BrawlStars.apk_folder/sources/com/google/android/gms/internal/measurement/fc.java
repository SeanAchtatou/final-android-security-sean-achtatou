package com.google.android.gms.internal.measurement;

final class fc {

    /* renamed from: a  reason: collision with root package name */
    private static final Class<?> f2199a = c();

    private static Class<?> c() {
        try {
            return Class.forName("com.google.protobuf.ExtensionRegistry");
        } catch (ClassNotFoundException unused) {
            return null;
        }
    }

    public static fd a() {
        if (f2199a != null) {
            try {
                return a("getEmptyRegistry");
            } catch (Exception unused) {
            }
        }
        return fd.f2200a;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0014  */
    /* JADX WARNING: Removed duplicated region for block: B:12:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x000e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static com.google.android.gms.internal.measurement.fd b() {
        /*
            java.lang.Class<?> r0 = com.google.android.gms.internal.measurement.fc.f2199a
            if (r0 == 0) goto L_0x000b
            java.lang.String r0 = "loadGeneratedRegistry"
            com.google.android.gms.internal.measurement.fd r0 = a(r0)     // Catch:{ Exception -> 0x000b }
            goto L_0x000c
        L_0x000b:
            r0 = 0
        L_0x000c:
            if (r0 != 0) goto L_0x0012
            com.google.android.gms.internal.measurement.fd r0 = com.google.android.gms.internal.measurement.fd.c()
        L_0x0012:
            if (r0 != 0) goto L_0x0018
            com.google.android.gms.internal.measurement.fd r0 = a()
        L_0x0018:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.fc.b():com.google.android.gms.internal.measurement.fd");
    }

    private static final fd a(String str) throws Exception {
        return (fd) f2199a.getDeclaredMethod(str, new Class[0]).invoke(null, new Object[0]);
    }
}
