package com.google.android.gms.measurement.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Bundle;

final class aj implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ar f2376a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ long f2377b;
    private final /* synthetic */ Bundle c;
    private final /* synthetic */ Context d;
    private final /* synthetic */ o e;
    private final /* synthetic */ BroadcastReceiver.PendingResult f;

    aj(ar arVar, long j, Bundle bundle, Context context, o oVar, BroadcastReceiver.PendingResult pendingResult) {
        this.f2376a = arVar;
        this.f2377b = j;
        this.c = bundle;
        this.d = context;
        this.e = oVar;
        this.f = pendingResult;
    }

    public final void run() {
        long a2 = this.f2376a.b().i.a();
        long j = this.f2377b;
        if (a2 > 0 && (j >= a2 || j <= 0)) {
            j = a2 - 1;
        }
        if (j > 0) {
            this.c.putLong("click_timestamp", j);
        }
        this.c.putString("_cis", "referrer broadcast");
        ar.a(this.d, (j) null).d().a("auto", "_cmp", this.c);
        this.e.k.a("Install campaign recorded");
        BroadcastReceiver.PendingResult pendingResult = this.f;
        if (pendingResult != null) {
            pendingResult.finish();
        }
    }
}
