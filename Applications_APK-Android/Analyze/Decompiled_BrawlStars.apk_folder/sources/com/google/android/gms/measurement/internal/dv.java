package com.google.android.gms.measurement.internal;

final class dv implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ dz f2531a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ du f2532b;

    dv(du duVar, dz dzVar) {
        this.f2532b = duVar;
        this.f2531a = dzVar;
    }

    public final void run() {
        du.a(this.f2532b, this.f2531a);
        this.f2532b.a();
    }
}
