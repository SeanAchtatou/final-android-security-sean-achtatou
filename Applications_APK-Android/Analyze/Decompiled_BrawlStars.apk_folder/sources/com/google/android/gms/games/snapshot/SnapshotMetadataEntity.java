package com.google.android.gms.games.snapshot;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.internal.zzd;
import java.util.Arrays;

public final class SnapshotMetadataEntity extends zzd implements SnapshotMetadata {
    public static final Parcelable.Creator<SnapshotMetadataEntity> CREATOR = new e();

    /* renamed from: a  reason: collision with root package name */
    private final GameEntity f1844a;

    /* renamed from: b  reason: collision with root package name */
    private final PlayerEntity f1845b;
    private final String c;
    private final Uri d;
    private final String e;
    private final String f;
    private final String g;
    private final long h;
    private final long i;
    private final float j;
    private final String k;
    private final boolean l;
    private final long m;
    private final String n;

    SnapshotMetadataEntity(GameEntity gameEntity, PlayerEntity playerEntity, String str, Uri uri, String str2, String str3, String str4, long j2, long j3, float f2, String str5, boolean z, long j4, String str6) {
        this.f1844a = gameEntity;
        this.f1845b = playerEntity;
        this.c = str;
        this.d = uri;
        this.e = str2;
        this.j = f2;
        this.f = str3;
        this.g = str4;
        this.h = j2;
        this.i = j3;
        this.k = str5;
        this.l = z;
        this.m = j4;
        this.n = str6;
    }

    public SnapshotMetadataEntity(SnapshotMetadata snapshotMetadata) {
        this.f1844a = new GameEntity(snapshotMetadata.b());
        this.f1845b = new PlayerEntity(snapshotMetadata.c());
        this.c = snapshotMetadata.d();
        this.d = snapshotMetadata.e();
        this.e = snapshotMetadata.getCoverImageUrl();
        this.j = snapshotMetadata.f();
        this.f = snapshotMetadata.h();
        this.g = snapshotMetadata.i();
        this.h = snapshotMetadata.j();
        this.i = snapshotMetadata.k();
        this.k = snapshotMetadata.g();
        this.l = snapshotMetadata.l();
        this.m = snapshotMetadata.m();
        this.n = snapshotMetadata.n();
    }

    static boolean a(SnapshotMetadata snapshotMetadata, Object obj) {
        if (!(obj instanceof SnapshotMetadata)) {
            return false;
        }
        if (snapshotMetadata == obj) {
            return true;
        }
        SnapshotMetadata snapshotMetadata2 = (SnapshotMetadata) obj;
        return j.a(snapshotMetadata2.b(), snapshotMetadata.b()) && j.a(snapshotMetadata2.c(), snapshotMetadata.c()) && j.a(snapshotMetadata2.d(), snapshotMetadata.d()) && j.a(snapshotMetadata2.e(), snapshotMetadata.e()) && j.a(Float.valueOf(snapshotMetadata2.f()), Float.valueOf(snapshotMetadata.f())) && j.a(snapshotMetadata2.h(), snapshotMetadata.h()) && j.a(snapshotMetadata2.i(), snapshotMetadata.i()) && j.a(Long.valueOf(snapshotMetadata2.j()), Long.valueOf(snapshotMetadata.j())) && j.a(Long.valueOf(snapshotMetadata2.k()), Long.valueOf(snapshotMetadata.k())) && j.a(snapshotMetadata2.g(), snapshotMetadata.g()) && j.a(Boolean.valueOf(snapshotMetadata2.l()), Boolean.valueOf(snapshotMetadata.l())) && j.a(Long.valueOf(snapshotMetadata2.m()), Long.valueOf(snapshotMetadata.m())) && j.a(snapshotMetadata2.n(), snapshotMetadata.n());
    }

    static String b(SnapshotMetadata snapshotMetadata) {
        return j.a(snapshotMetadata).a("Game", snapshotMetadata.b()).a("Owner", snapshotMetadata.c()).a("SnapshotId", snapshotMetadata.d()).a("CoverImageUri", snapshotMetadata.e()).a("CoverImageUrl", snapshotMetadata.getCoverImageUrl()).a("CoverImageAspectRatio", Float.valueOf(snapshotMetadata.f())).a("Description", snapshotMetadata.i()).a("LastModifiedTimestamp", Long.valueOf(snapshotMetadata.j())).a("PlayedTime", Long.valueOf(snapshotMetadata.k())).a("UniqueName", snapshotMetadata.g()).a("ChangePending", Boolean.valueOf(snapshotMetadata.l())).a("ProgressValue", Long.valueOf(snapshotMetadata.m())).a("DeviceName", snapshotMetadata.n()).toString();
    }

    public final /* bridge */ /* synthetic */ Object a() {
        return this;
    }

    public final Game b() {
        return this.f1844a;
    }

    public final Player c() {
        return this.f1845b;
    }

    public final String d() {
        return this.c;
    }

    public final Uri e() {
        return this.d;
    }

    public final boolean equals(Object obj) {
        return a(this, obj);
    }

    public final float f() {
        return this.j;
    }

    public final String g() {
        return this.k;
    }

    public final String getCoverImageUrl() {
        return this.e;
    }

    public final String h() {
        return this.f;
    }

    public final int hashCode() {
        return a(this);
    }

    public final String i() {
        return this.g;
    }

    public final long j() {
        return this.h;
    }

    public final long k() {
        return this.i;
    }

    public final boolean l() {
        return this.l;
    }

    public final long m() {
        return this.m;
    }

    public final String n() {
        return this.n;
    }

    public final String toString() {
        return b(this);
    }

    static int a(SnapshotMetadata snapshotMetadata) {
        return Arrays.hashCode(new Object[]{snapshotMetadata.b(), snapshotMetadata.c(), snapshotMetadata.d(), snapshotMetadata.e(), Float.valueOf(snapshotMetadata.f()), snapshotMetadata.h(), snapshotMetadata.i(), Long.valueOf(snapshotMetadata.j()), Long.valueOf(snapshotMetadata.k()), snapshotMetadata.g(), Boolean.valueOf(snapshotMetadata.l()), Long.valueOf(snapshotMetadata.m()), snapshotMetadata.n()});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.GameEntity, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.PlayerEntity, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.net.Uri, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = a.a(parcel, 20293);
        a.a(parcel, 1, (Parcelable) this.f1844a, i2, false);
        a.a(parcel, 2, (Parcelable) this.f1845b, i2, false);
        a.a(parcel, 3, this.c, false);
        a.a(parcel, 5, (Parcelable) this.d, i2, false);
        a.a(parcel, 6, getCoverImageUrl(), false);
        a.a(parcel, 7, this.f, false);
        a.a(parcel, 8, this.g, false);
        a.a(parcel, 9, this.h);
        a.a(parcel, 10, this.i);
        a.a(parcel, 11, this.j);
        a.a(parcel, 12, this.k, false);
        a.a(parcel, 13, this.l);
        a.a(parcel, 14, this.m);
        a.a(parcel, 15, this.n, false);
        a.b(parcel, a2);
    }
}
