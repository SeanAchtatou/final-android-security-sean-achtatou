package com.google.android.gms.games;

import android.support.graphics.drawable.PathInterpolatorCompat;
import android.support.v4.view.PointerIconCompat;
import com.google.android.gms.common.api.Status;
import java.util.Locale;

@Deprecated
public final class d {
    public static Status a(int i) {
        String str;
        if (i == 0) {
            str = "STATUS_OK";
        } else if (i == 1) {
            str = "STATUS_INTERNAL_ERROR";
        } else if (i == 2) {
            str = "STATUS_CLIENT_RECONNECT_REQUIRED";
        } else if (i == 3) {
            str = "STATUS_NETWORK_ERROR_STALE_DATA";
        } else if (i == 4) {
            str = "STATUS_NETWORK_ERROR_NO_DATA";
        } else if (i == 5) {
            str = "STATUS_NETWORK_ERROR_OPERATION_DEFERRED";
        } else if (i != 6) {
            if (i != 7) {
                if (i == 14) {
                    str = "STATUS_INTERRUPTED";
                } else if (i == 15) {
                    str = "STATUS_TIMEOUT";
                } else if (i == 6500) {
                    str = "STATUS_MATCH_ERROR_INVALID_PARTICIPANT_STATE";
                } else if (i != 6501) {
                    switch (i) {
                        case 7:
                            break;
                        case 8:
                            str = "STATUS_APP_MISCONFIGURED";
                            break;
                        case 9:
                            str = "STATUS_GAME_NOT_FOUND";
                            break;
                        case 500:
                            str = "STATUS_RESOLVE_STALE_OR_NO_DATA";
                            break;
                        case 1500:
                            str = "STATUS_PLAYER_OOB_REQUIRED";
                            break;
                        case 4006:
                            str = "STATUS_SNAPSHOT_CONFLICT_MISSING";
                            break;
                        case 8000:
                            str = "STATUS_MILESTONE_CLAIMED_PREVIOUSLY";
                            break;
                        case 8001:
                            str = "STATUS_MILESTONE_CLAIM_FAILED";
                            break;
                        case 8002:
                            str = "STATUS_QUEST_NO_LONGER_AVAILABLE";
                            break;
                        case 8003:
                            str = "STATUS_QUEST_NOT_STARTED";
                            break;
                        case 9000:
                            str = "STATUS_VIDEO_NOT_ACTIVE";
                            break;
                        case 9001:
                            str = "STATUS_VIDEO_UNSUPPORTED";
                            break;
                        case 9002:
                            str = "STATUS_VIDEO_PERMISSION_ERROR";
                            break;
                        case 9003:
                            str = "STATUS_VIDEO_STORAGE_ERROR";
                            break;
                        case 9004:
                            str = "STATUS_VIDEO_UNEXPECTED_CAPTURE_ERROR";
                            break;
                        case 9006:
                            str = "STATUS_VIDEO_ALREADY_CAPTURING";
                            break;
                        case 9009:
                            str = "STATUS_VIDEO_OUT_OF_DISK_SPACE";
                            break;
                        case 9010:
                            str = "STATUS_VIDEO_NO_MIC";
                            break;
                        case 9011:
                            str = "STATUS_VIDEO_NO_CAMERA";
                            break;
                        case 9012:
                            str = "STATUS_VIDEO_SCREEN_OFF";
                            break;
                        case 9016:
                            str = "STATUS_VIDEO_RELEASE_TIMEOUT";
                            break;
                        case 9017:
                            str = "STATUS_VIDEO_CAPTURE_VIDEO_PERMISSION_REQUIRED";
                            break;
                        case 9200:
                            str = "STATUS_VIDEO_MISSING_OVERLAY_PERMISSION";
                            break;
                        case 10000:
                            str = "STATUS_CLIENT_LOADING";
                            break;
                        case 10001:
                            str = "STATUS_CLIENT_EMPTY";
                            break;
                        case 10002:
                            str = "STATUS_CLIENT_HIDDEN";
                            break;
                        default:
                            switch (i) {
                                case 1000:
                                    str = "STATUS_AUTH_ERROR_HARD";
                                    break;
                                case PointerIconCompat.TYPE_CONTEXT_MENU:
                                    str = "STATUS_AUTH_ERROR_USER_RECOVERABLE";
                                    break;
                                case PointerIconCompat.TYPE_HAND:
                                    str = "STATUS_AUTH_ERROR_UNREGISTERED_CLIENT_ID";
                                    break;
                                case PointerIconCompat.TYPE_HELP:
                                    str = "STATUS_AUTH_ERROR_API_ACCESS_DENIED";
                                    break;
                                case PointerIconCompat.TYPE_WAIT:
                                    str = "STATUS_AUTH_ERROR_ACCOUNT_NOT_USABLE";
                                    break;
                                default:
                                    switch (i) {
                                        case 2000:
                                            str = "STATUS_REQUEST_UPDATE_PARTIAL_SUCCESS";
                                            break;
                                        case 2001:
                                            str = "STATUS_REQUEST_UPDATE_TOTAL_FAILURE";
                                            break;
                                        case 2002:
                                            str = "STATUS_REQUEST_TOO_MANY_RECIPIENTS";
                                            break;
                                        default:
                                            switch (i) {
                                                case PathInterpolatorCompat.MAX_NUM_POINTS:
                                                    str = "STATUS_ACHIEVEMENT_UNLOCK_FAILURE";
                                                    break;
                                                case 3001:
                                                    str = "STATUS_ACHIEVEMENT_UNKNOWN";
                                                    break;
                                                case 3002:
                                                    str = "STATUS_ACHIEVEMENT_NOT_INCREMENTAL";
                                                    break;
                                                case 3003:
                                                    str = "STATUS_ACHIEVEMENT_UNLOCKED";
                                                    break;
                                                default:
                                                    switch (i) {
                                                        case 4000:
                                                            str = "STATUS_SNAPSHOT_NOT_FOUND";
                                                            break;
                                                        case 4001:
                                                            str = "STATUS_SNAPSHOT_CREATION_FAILED";
                                                            break;
                                                        case 4002:
                                                            str = "STATUS_SNAPSHOT_CONTENTS_UNAVAILABLE";
                                                            break;
                                                        case 4003:
                                                            str = "STATUS_SNAPSHOT_COMMIT_FAILED";
                                                            break;
                                                        case 4004:
                                                            str = "STATUS_SNAPSHOT_CONFLICT";
                                                            break;
                                                        default:
                                                            switch (i) {
                                                                case 6000:
                                                                    str = "STATUS_MULTIPLAYER_ERROR_CREATION_NOT_ALLOWED";
                                                                    break;
                                                                case 6001:
                                                                    str = "STATUS_MULTIPLAYER_ERROR_NOT_TRUSTED_TESTER";
                                                                    break;
                                                                case 6002:
                                                                    str = "STATUS_MULTIPLAYER_ERROR_INVALID_MULTIPLAYER_TYPE";
                                                                    break;
                                                                case 6003:
                                                                    str = "STATUS_MULTIPLAYER_DISABLED";
                                                                    break;
                                                                default:
                                                                    switch (i) {
                                                                        case 6503:
                                                                            str = "STATUS_MATCH_ERROR_OUT_OF_DATE_VERSION";
                                                                            break;
                                                                        case 6504:
                                                                            str = "STATUS_MATCH_ERROR_INVALID_MATCH_RESULTS";
                                                                            break;
                                                                        case 6505:
                                                                            str = "STATUS_MATCH_ERROR_ALREADY_REMATCHED";
                                                                            break;
                                                                        case 6506:
                                                                            str = "STATUS_MATCH_NOT_FOUND";
                                                                            break;
                                                                        case 6507:
                                                                            str = "STATUS_MATCH_ERROR_LOCALLY_MODIFIED";
                                                                            break;
                                                                        default:
                                                                            switch (i) {
                                                                                case 7000:
                                                                                    str = "STATUS_REAL_TIME_CONNECTION_FAILED";
                                                                                    break;
                                                                                case 7001:
                                                                                    str = "STATUS_REAL_TIME_MESSAGE_SEND_FAILED";
                                                                                    break;
                                                                                case 7002:
                                                                                    str = "STATUS_INVALID_REAL_TIME_ROOM_ID";
                                                                                    break;
                                                                                case 7003:
                                                                                    str = "STATUS_PARTICIPANT_NOT_CONNECTED";
                                                                                    break;
                                                                                case 7004:
                                                                                    str = "STATUS_REAL_TIME_ROOM_NOT_JOINED";
                                                                                    break;
                                                                                case 7005:
                                                                                    str = "STATUS_REAL_TIME_INACTIVE_ROOM";
                                                                                    break;
                                                                                case 7006:
                                                                                    str = "STATUS_REAL_TIME_SERVICE_NOT_CONNECTED";
                                                                                    break;
                                                                                case 7007:
                                                                                    str = "STATUS_OPERATION_IN_FLIGHT";
                                                                                    break;
                                                                                default:
                                                                                    str = String.format(Locale.US, "Status code (%d) not found!", Integer.valueOf(i));
                                                                                    break;
                                                                            }
                                                                    }
                                                            }
                                                    }
                                            }
                                    }
                            }
                    }
                } else {
                    str = "STATUS_MATCH_ERROR_INACTIVE_MATCH";
                }
            }
            str = "STATUS_LICENSE_CHECK_FAILED";
        } else {
            str = "STATUS_NETWORK_ERROR_OPERATION_FAILED";
        }
        return new Status(i, str);
    }
}
