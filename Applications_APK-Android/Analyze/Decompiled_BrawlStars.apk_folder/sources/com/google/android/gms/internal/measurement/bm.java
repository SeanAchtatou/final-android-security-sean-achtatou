package com.google.android.gms.internal.measurement;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import com.facebook.appevents.AppEventsConstants;
import com.google.android.gms.analytics.n;
import com.google.android.gms.common.internal.l;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Locale;
import java.util.Map;

final class bm extends r {
    /* access modifiers changed from: private */
    public static final byte[] d = "\n".getBytes();

    /* renamed from: a  reason: collision with root package name */
    private final String f2081a = String.format("%s/%s (Linux; U; Android %s; %s; %s Build/%s)", "GoogleAnalytics", s.f2330a, Build.VERSION.RELEASE, bx.a(Locale.getDefault()), Build.MODEL, Build.ID);

    /* renamed from: b  reason: collision with root package name */
    private final bw f2082b;

    bm(t tVar) {
        super(tVar);
        this.f2082b = new bw(tVar.c);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        a("Network initialized. User agent", this.f2081a);
    }

    public final boolean b() {
        NetworkInfo networkInfo;
        n.b();
        m();
        try {
            networkInfo = ((ConnectivityManager) g().getSystemService("connectivity")).getActiveNetworkInfo();
        } catch (SecurityException unused) {
            networkInfo = null;
        }
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        b("No network connectivity");
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.bm.a(com.google.android.gms.internal.measurement.bh, boolean):java.lang.String
     arg types: [com.google.android.gms.internal.measurement.bh, int]
     candidates:
      com.google.android.gms.internal.measurement.bm.a(java.net.URL, byte[]):int
      com.google.android.gms.internal.measurement.bm.a(com.google.android.gms.internal.measurement.bh, java.lang.String):java.net.URL
      com.google.android.gms.internal.measurement.q.a(java.lang.String, java.lang.Object):void
      com.google.android.gms.internal.measurement.bm.a(com.google.android.gms.internal.measurement.bh, boolean):java.lang.String */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x014f, code lost:
        if (a(r5) == 200) goto L_0x012d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0193, code lost:
        if (a(r6, r5) == 200) goto L_0x012d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00fd  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0198  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x01ab A[EDGE_INSN: B:71:0x01ab->B:67:0x01ab ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<java.lang.Long> a(java.util.List<com.google.android.gms.internal.measurement.bh> r9) {
        /*
            r8 = this;
            com.google.android.gms.analytics.n.b()
            r8.m()
            com.google.android.gms.common.internal.l.a(r9)
            com.google.android.gms.internal.measurement.au r0 = r8.h()
            java.util.Set r0 = r0.k()
            boolean r0 = r0.isEmpty()
            r1 = 0
            r2 = 1
            if (r0 != 0) goto L_0x0053
            com.google.android.gms.internal.measurement.bw r0 = r8.f2082b
            com.google.android.gms.internal.measurement.bd<java.lang.Integer> r3 = com.google.android.gms.internal.measurement.bc.v
            V r3 = r3.f2067a
            java.lang.Integer r3 = (java.lang.Integer) r3
            int r3 = r3.intValue()
            long r3 = (long) r3
            r5 = 1000(0x3e8, double:4.94E-321)
            long r3 = r3 * r5
            boolean r0 = r0.a(r3)
            if (r0 != 0) goto L_0x0031
            goto L_0x0053
        L_0x0031:
            com.google.android.gms.internal.measurement.bd<java.lang.String> r0 = com.google.android.gms.internal.measurement.bc.p
            V r0 = r0.f2067a
            java.lang.String r0 = (java.lang.String) r0
            com.google.android.gms.internal.measurement.al r0 = com.google.android.gms.internal.measurement.al.a(r0)
            com.google.android.gms.internal.measurement.al r3 = com.google.android.gms.internal.measurement.al.NONE
            if (r0 == r3) goto L_0x0041
            r0 = 1
            goto L_0x0042
        L_0x0041:
            r0 = 0
        L_0x0042:
            com.google.android.gms.internal.measurement.bd<java.lang.String> r3 = com.google.android.gms.internal.measurement.bc.q
            V r3 = r3.f2067a
            java.lang.String r3 = (java.lang.String) r3
            com.google.android.gms.internal.measurement.aq r3 = com.google.android.gms.internal.measurement.aq.a(r3)
            com.google.android.gms.internal.measurement.aq r4 = com.google.android.gms.internal.measurement.aq.GZIP
            if (r3 != r4) goto L_0x0054
            r3 = r0
            r0 = 1
            goto L_0x0056
        L_0x0053:
            r0 = 0
        L_0x0054:
            r3 = r0
            r0 = 0
        L_0x0056:
            r4 = 200(0xc8, float:2.8E-43)
            if (r3 == 0) goto L_0x00fd
            boolean r1 = r9.isEmpty()
            r1 = r1 ^ r2
            com.google.android.gms.common.internal.l.b(r1)
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r0)
            int r2 = r9.size()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            java.lang.String r3 = "Uploading batched hits. compression, count"
            r8.a(r3, r1, r2)
            com.google.android.gms.internal.measurement.bn r1 = new com.google.android.gms.internal.measurement.bn
            r1.<init>(r8)
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            java.util.Iterator r9 = r9.iterator()
        L_0x0081:
            boolean r3 = r9.hasNext()
            if (r3 == 0) goto L_0x009d
            java.lang.Object r3 = r9.next()
            com.google.android.gms.internal.measurement.bh r3 = (com.google.android.gms.internal.measurement.bh) r3
            boolean r5 = r1.a(r3)
            if (r5 == 0) goto L_0x009d
            long r5 = r3.c
            java.lang.Long r3 = java.lang.Long.valueOf(r5)
            r2.add(r3)
            goto L_0x0081
        L_0x009d:
            int r9 = r1.f2083a
            if (r9 != 0) goto L_0x00a2
            return r2
        L_0x00a2:
            java.net.URL r9 = r8.d()
            if (r9 != 0) goto L_0x00ae
            java.lang.String r9 = "Failed to build batching endpoint url"
            r8.f(r9)
            goto L_0x00f8
        L_0x00ae:
            if (r0 == 0) goto L_0x00bb
            java.io.ByteArrayOutputStream r0 = r1.f2084b
            byte[] r0 = r0.toByteArray()
            int r9 = r8.b(r9, r0)
            goto L_0x00c5
        L_0x00bb:
            java.io.ByteArrayOutputStream r0 = r1.f2084b
            byte[] r0 = r0.toByteArray()
            int r9 = r8.a(r9, r0)
        L_0x00c5:
            if (r4 != r9) goto L_0x00d3
            int r9 = r1.f2083a
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)
            java.lang.String r0 = "Batched upload completed. Hits batched"
            r8.a(r0, r9)
            return r2
        L_0x00d3:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r9)
            java.lang.String r1 = "Network error uploading hits. status code"
            r8.a(r1, r0)
            com.google.android.gms.internal.measurement.au r0 = r8.h()
            java.util.Set r0 = r0.k()
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)
            boolean r9 = r0.contains(r9)
            if (r9 == 0) goto L_0x00f8
            java.lang.String r9 = "Server instructed the client to stop batching"
            r8.e(r9)
            com.google.android.gms.internal.measurement.bw r9 = r8.f2082b
            r9.a()
        L_0x00f8:
            java.util.List r9 = java.util.Collections.emptyList()
            return r9
        L_0x00fd:
            java.util.ArrayList r0 = new java.util.ArrayList
            int r3 = r9.size()
            r0.<init>(r3)
            java.util.Iterator r9 = r9.iterator()
        L_0x010a:
            boolean r3 = r9.hasNext()
            if (r3 == 0) goto L_0x01ab
            java.lang.Object r3 = r9.next()
            com.google.android.gms.internal.measurement.bh r3 = (com.google.android.gms.internal.measurement.bh) r3
            com.google.android.gms.common.internal.l.a(r3)
            boolean r5 = r3.f
            r5 = r5 ^ r2
            java.lang.String r5 = r8.a(r3, r5)
            if (r5 != 0) goto L_0x012f
            com.google.android.gms.internal.measurement.t r5 = r8.c
            com.google.android.gms.internal.measurement.bk r5 = r5.a()
            java.lang.String r6 = "Error formatting hit for upload"
            r5.a(r3, r6)
        L_0x012d:
            r5 = 1
            goto L_0x0196
        L_0x012f:
            int r6 = r5.length()
            com.google.android.gms.internal.measurement.bd<java.lang.Integer> r7 = com.google.android.gms.internal.measurement.bc.o
            V r7 = r7.f2067a
            java.lang.Integer r7 = (java.lang.Integer) r7
            int r7 = r7.intValue()
            if (r6 > r7) goto L_0x0154
            java.net.URL r5 = r8.a(r3, r5)
            if (r5 != 0) goto L_0x014b
            java.lang.String r5 = "Failed to build collect GET endpoint url"
            r8.f(r5)
            goto L_0x0152
        L_0x014b:
            int r5 = r8.a(r5)
            if (r5 != r4) goto L_0x0152
        L_0x0151:
            goto L_0x012d
        L_0x0152:
            r5 = 0
            goto L_0x0196
        L_0x0154:
            java.lang.String r5 = r8.a(r3, r1)
            if (r5 != 0) goto L_0x0166
            com.google.android.gms.internal.measurement.t r5 = r8.c
            com.google.android.gms.internal.measurement.bk r5 = r5.a()
            java.lang.String r6 = "Error formatting hit for POST upload"
            r5.a(r3, r6)
            goto L_0x012d
        L_0x0166:
            byte[] r5 = r5.getBytes()
            int r6 = r5.length
            com.google.android.gms.internal.measurement.bd<java.lang.Integer> r7 = com.google.android.gms.internal.measurement.bc.s
            V r7 = r7.f2067a
            java.lang.Integer r7 = (java.lang.Integer) r7
            int r7 = r7.intValue()
            if (r6 <= r7) goto L_0x0183
            com.google.android.gms.internal.measurement.t r5 = r8.c
            com.google.android.gms.internal.measurement.bk r5 = r5.a()
            java.lang.String r6 = "Hit payload exceeds size limit"
            r5.a(r3, r6)
            goto L_0x012d
        L_0x0183:
            java.net.URL r6 = r8.a(r3)
            if (r6 != 0) goto L_0x018f
            java.lang.String r5 = "Failed to build collect POST endpoint url"
            r8.f(r5)
            goto L_0x0152
        L_0x018f:
            int r5 = r8.a(r6, r5)
            if (r5 != r4) goto L_0x0152
            goto L_0x0151
        L_0x0196:
            if (r5 == 0) goto L_0x01ab
            long r5 = r3.c
            java.lang.Long r3 = java.lang.Long.valueOf(r5)
            r0.add(r3)
            int r3 = r0.size()
            int r5 = com.google.android.gms.internal.measurement.au.f()
            if (r3 < r5) goto L_0x010a
        L_0x01ab:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.bm.a(java.util.List):java.util.List");
    }

    private final int a(URL url) {
        l.a(url);
        b("GET request", url);
        HttpURLConnection httpURLConnection = null;
        try {
            HttpURLConnection b2 = b(url);
            b2.connect();
            a(b2);
            int responseCode = b2.getResponseCode();
            if (responseCode == 200) {
                this.c.c().e();
            }
            b("GET status", Integer.valueOf(responseCode));
            if (b2 != null) {
                b2.disconnect();
            }
            return responseCode;
        } catch (IOException e) {
            d("Network GET connection error", e);
            if (httpURLConnection == null) {
                return 0;
            }
            httpURLConnection.disconnect();
            return 0;
        } catch (Throwable th) {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            throw th;
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r5v1, types: [java.net.HttpURLConnection] */
    /* JADX WARN: Type inference failed for: r5v2, types: [java.net.HttpURLConnection] */
    /* JADX WARN: Type inference failed for: r5v5 */
    /* JADX WARN: Type inference failed for: r5v6 */
    /* JADX WARN: Type inference failed for: r5v8 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x007b A[SYNTHETIC, Splitter:B:29:0x007b] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x008d A[SYNTHETIC, Splitter:B:37:0x008d] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:45:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final int a(java.net.URL r5, byte[] r6) {
        /*
            r4 = this;
            java.lang.String r0 = "Error closing http post connection output stream"
            com.google.android.gms.common.internal.l.a(r5)
            com.google.android.gms.common.internal.l.a(r6)
            int r1 = r6.length
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            java.lang.String r2 = "POST bytes, url"
            r4.b(r2, r1, r5)
            boolean r1 = k()
            if (r1 == 0) goto L_0x0022
            java.lang.String r1 = new java.lang.String
            r1.<init>(r6)
            java.lang.String r2 = "Post payload\n"
            r4.a(r2, r1)
        L_0x0022:
            r1 = 0
            android.content.Context r2 = r4.g()     // Catch:{ IOException -> 0x0072, all -> 0x006f }
            r2.getPackageName()     // Catch:{ IOException -> 0x0072, all -> 0x006f }
            java.net.HttpURLConnection r5 = r4.b(r5)     // Catch:{ IOException -> 0x0072, all -> 0x006f }
            r2 = 1
            r5.setDoOutput(r2)     // Catch:{ IOException -> 0x006d }
            int r2 = r6.length     // Catch:{ IOException -> 0x006d }
            r5.setFixedLengthStreamingMode(r2)     // Catch:{ IOException -> 0x006d }
            r5.connect()     // Catch:{ IOException -> 0x006d }
            java.io.OutputStream r1 = r5.getOutputStream()     // Catch:{ IOException -> 0x006d }
            r1.write(r6)     // Catch:{ IOException -> 0x006d }
            r4.a(r5)     // Catch:{ IOException -> 0x006d }
            int r6 = r5.getResponseCode()     // Catch:{ IOException -> 0x006d }
            r2 = 200(0xc8, float:2.8E-43)
            if (r6 != r2) goto L_0x0054
            com.google.android.gms.internal.measurement.t r2 = r4.c     // Catch:{ IOException -> 0x006d }
            com.google.android.gms.internal.measurement.l r2 = r2.c()     // Catch:{ IOException -> 0x006d }
            r2.e()     // Catch:{ IOException -> 0x006d }
        L_0x0054:
            java.lang.String r2 = "POST status"
            java.lang.Integer r3 = java.lang.Integer.valueOf(r6)     // Catch:{ IOException -> 0x006d }
            r4.b(r2, r3)     // Catch:{ IOException -> 0x006d }
            if (r1 == 0) goto L_0x0067
            r1.close()     // Catch:{ IOException -> 0x0063 }
            goto L_0x0067
        L_0x0063:
            r1 = move-exception
            r4.e(r0, r1)
        L_0x0067:
            if (r5 == 0) goto L_0x006c
            r5.disconnect()
        L_0x006c:
            return r6
        L_0x006d:
            r6 = move-exception
            goto L_0x0074
        L_0x006f:
            r6 = move-exception
            r5 = r1
            goto L_0x008b
        L_0x0072:
            r6 = move-exception
            r5 = r1
        L_0x0074:
            java.lang.String r2 = "Network POST connection error"
            r4.d(r2, r6)     // Catch:{ all -> 0x008a }
            if (r1 == 0) goto L_0x0083
            r1.close()     // Catch:{ IOException -> 0x007f }
            goto L_0x0083
        L_0x007f:
            r6 = move-exception
            r4.e(r0, r6)
        L_0x0083:
            if (r5 == 0) goto L_0x0088
            r5.disconnect()
        L_0x0088:
            r5 = 0
            return r5
        L_0x008a:
            r6 = move-exception
        L_0x008b:
            if (r1 == 0) goto L_0x0095
            r1.close()     // Catch:{ IOException -> 0x0091 }
            goto L_0x0095
        L_0x0091:
            r1 = move-exception
            r4.e(r0, r1)
        L_0x0095:
            if (r5 == 0) goto L_0x009a
            r5.disconnect()
        L_0x009a:
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.bm.a(java.net.URL, byte[]):int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:42:0x00d8 A[SYNTHETIC, Splitter:B:42:0x00d8] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00e2  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00ea A[SYNTHETIC, Splitter:B:50:0x00ea] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00f4  */
    /* JADX WARNING: Removed duplicated region for block: B:58:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final int b(java.net.URL r11, byte[] r12) {
        /*
            r10 = this;
            java.lang.String r0 = "Error closing http compressed post connection output stream"
            com.google.android.gms.common.internal.l.a(r11)
            com.google.android.gms.common.internal.l.a(r12)
            r1 = 0
            android.content.Context r2 = r10.g()     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            r2.getPackageName()     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            r2.<init>()     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            java.util.zip.GZIPOutputStream r3 = new java.util.zip.GZIPOutputStream     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            r3.<init>(r2)     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            r3.write(r12)     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            r3.close()     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            r2.close()     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            byte[] r2 = r2.toByteArray()     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            java.lang.String r3 = "POST compressed size, ratio %, url"
            int r4 = r2.length     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            r5 = 100
            int r7 = r2.length     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            long r7 = (long) r7     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            long r7 = r7 * r5
            int r5 = r12.length     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            long r5 = (long) r5     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            long r7 = r7 / r5
            java.lang.Long r5 = java.lang.Long.valueOf(r7)     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            r10.a(r3, r4, r5, r11)     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            int r3 = r2.length     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            int r4 = r12.length     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            if (r3 <= r4) goto L_0x0051
            java.lang.String r3 = "Compressed payload is larger then uncompressed. compressed, uncompressed"
            int r4 = r2.length     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            int r5 = r12.length     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            r10.c(r3, r4, r5)     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
        L_0x0051:
            boolean r3 = k()     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            if (r3 == 0) goto L_0x0073
            java.lang.String r3 = "Post payload"
            java.lang.String r4 = "\n"
            java.lang.String r5 = new java.lang.String     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            r5.<init>(r12)     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            int r12 = r5.length()     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            if (r12 == 0) goto L_0x006b
            java.lang.String r12 = r4.concat(r5)     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            goto L_0x0070
        L_0x006b:
            java.lang.String r12 = new java.lang.String     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            r12.<init>(r4)     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
        L_0x0070:
            r10.a(r3, r12)     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
        L_0x0073:
            java.net.HttpURLConnection r11 = r10.b(r11)     // Catch:{ IOException -> 0x00cf, all -> 0x00cc }
            r12 = 1
            r11.setDoOutput(r12)     // Catch:{ IOException -> 0x00c7, all -> 0x00c2 }
            java.lang.String r12 = "Content-Encoding"
            java.lang.String r3 = "gzip"
            r11.addRequestProperty(r12, r3)     // Catch:{ IOException -> 0x00c7, all -> 0x00c2 }
            int r12 = r2.length     // Catch:{ IOException -> 0x00c7, all -> 0x00c2 }
            r11.setFixedLengthStreamingMode(r12)     // Catch:{ IOException -> 0x00c7, all -> 0x00c2 }
            r11.connect()     // Catch:{ IOException -> 0x00c7, all -> 0x00c2 }
            java.io.OutputStream r12 = r11.getOutputStream()     // Catch:{ IOException -> 0x00c7, all -> 0x00c2 }
            r12.write(r2)     // Catch:{ IOException -> 0x00bc, all -> 0x00b6 }
            r12.close()     // Catch:{ IOException -> 0x00bc, all -> 0x00b6 }
            r10.a(r11)     // Catch:{ IOException -> 0x00c7, all -> 0x00c2 }
            int r12 = r11.getResponseCode()     // Catch:{ IOException -> 0x00c7, all -> 0x00c2 }
            r2 = 200(0xc8, float:2.8E-43)
            if (r12 != r2) goto L_0x00a7
            com.google.android.gms.internal.measurement.t r2 = r10.c     // Catch:{ IOException -> 0x00c7, all -> 0x00c2 }
            com.google.android.gms.internal.measurement.l r2 = r2.c()     // Catch:{ IOException -> 0x00c7, all -> 0x00c2 }
            r2.e()     // Catch:{ IOException -> 0x00c7, all -> 0x00c2 }
        L_0x00a7:
            java.lang.String r2 = "POST status"
            java.lang.Integer r3 = java.lang.Integer.valueOf(r12)     // Catch:{ IOException -> 0x00c7, all -> 0x00c2 }
            r10.b(r2, r3)     // Catch:{ IOException -> 0x00c7, all -> 0x00c2 }
            if (r11 == 0) goto L_0x00b5
            r11.disconnect()
        L_0x00b5:
            return r12
        L_0x00b6:
            r1 = move-exception
            r9 = r12
            r12 = r11
            r11 = r1
            r1 = r9
            goto L_0x00e8
        L_0x00bc:
            r1 = move-exception
            r9 = r12
            r12 = r11
            r11 = r1
            r1 = r9
            goto L_0x00d1
        L_0x00c2:
            r12 = move-exception
            r9 = r12
            r12 = r11
            r11 = r9
            goto L_0x00e8
        L_0x00c7:
            r12 = move-exception
            r9 = r12
            r12 = r11
            r11 = r9
            goto L_0x00d1
        L_0x00cc:
            r11 = move-exception
            r12 = r1
            goto L_0x00e8
        L_0x00cf:
            r11 = move-exception
            r12 = r1
        L_0x00d1:
            java.lang.String r2 = "Network compressed POST connection error"
            r10.d(r2, r11)     // Catch:{ all -> 0x00e7 }
            if (r1 == 0) goto L_0x00e0
            r1.close()     // Catch:{ IOException -> 0x00dc }
            goto L_0x00e0
        L_0x00dc:
            r11 = move-exception
            r10.e(r0, r11)
        L_0x00e0:
            if (r12 == 0) goto L_0x00e5
            r12.disconnect()
        L_0x00e5:
            r11 = 0
            return r11
        L_0x00e7:
            r11 = move-exception
        L_0x00e8:
            if (r1 == 0) goto L_0x00f2
            r1.close()     // Catch:{ IOException -> 0x00ee }
            goto L_0x00f2
        L_0x00ee:
            r1 = move-exception
            r10.e(r0, r1)
        L_0x00f2:
            if (r12 == 0) goto L_0x00f7
            r12.disconnect()
        L_0x00f7:
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.bm.b(java.net.URL, byte[]):int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0021 A[SYNTHETIC, Splitter:B:18:0x0021] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void a(java.net.HttpURLConnection r4) throws java.io.IOException {
        /*
            r3 = this;
            java.lang.String r0 = "Error closing http connection input stream"
            java.io.InputStream r4 = r4.getInputStream()     // Catch:{ all -> 0x001d }
            r1 = 1024(0x400, float:1.435E-42)
            byte[] r1 = new byte[r1]     // Catch:{ all -> 0x001b }
        L_0x000a:
            int r2 = r4.read(r1)     // Catch:{ all -> 0x001b }
            if (r2 > 0) goto L_0x000a
            if (r4 == 0) goto L_0x001a
            r4.close()     // Catch:{ IOException -> 0x0016 }
            return
        L_0x0016:
            r4 = move-exception
            r3.e(r0, r4)
        L_0x001a:
            return
        L_0x001b:
            r1 = move-exception
            goto L_0x001f
        L_0x001d:
            r1 = move-exception
            r4 = 0
        L_0x001f:
            if (r4 == 0) goto L_0x0029
            r4.close()     // Catch:{ IOException -> 0x0025 }
            goto L_0x0029
        L_0x0025:
            r4 = move-exception
            r3.e(r0, r4)
        L_0x0029:
            goto L_0x002b
        L_0x002a:
            throw r1
        L_0x002b:
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.bm.a(java.net.HttpURLConnection):void");
    }

    private final HttpURLConnection b(URL url) throws IOException {
        URLConnection openConnection = url.openConnection();
        if (openConnection instanceof HttpURLConnection) {
            HttpURLConnection httpURLConnection = (HttpURLConnection) openConnection;
            httpURLConnection.setDefaultUseCaches(false);
            httpURLConnection.setConnectTimeout(((Integer) bc.w.f2067a).intValue());
            httpURLConnection.setReadTimeout(((Integer) bc.x.f2067a).intValue());
            httpURLConnection.setInstanceFollowRedirects(false);
            httpURLConnection.setRequestProperty("User-Agent", this.f2081a);
            httpURLConnection.setDoInput(true);
            return httpURLConnection;
        }
        throw new IOException("Failed to obtain http connection");
    }

    private final URL d() {
        String valueOf = String.valueOf(au.h());
        String valueOf2 = String.valueOf((String) bc.n.f2067a);
        try {
            return new URL(valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf));
        } catch (MalformedURLException e) {
            e("Error trying to parse the hardcoded host url", e);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public final String a(bh bhVar, boolean z) {
        String str;
        l.a(bhVar);
        StringBuilder sb = new StringBuilder();
        try {
            for (Map.Entry next : bhVar.f2073a.entrySet()) {
                String str2 = (String) next.getKey();
                if (!"ht".equals(str2) && !"qt".equals(str2) && !"AppUID".equals(str2) && !"z".equals(str2) && !"_gmsv".equals(str2)) {
                    a(sb, str2, (String) next.getValue());
                }
            }
            a(sb, "ht", String.valueOf(bhVar.d));
            a(sb, "qt", String.valueOf(f().a() - bhVar.d));
            if (z) {
                long b2 = bx.b(bhVar.a("_s", AppEventsConstants.EVENT_PARAM_VALUE_NO));
                if (b2 != 0) {
                    str = String.valueOf(b2);
                } else {
                    str = String.valueOf(bhVar.c);
                }
                a(sb, "z", str);
            }
            return sb.toString();
        } catch (UnsupportedEncodingException e) {
            e("Failed to encode name or value", e);
            return null;
        }
    }

    private static void a(StringBuilder sb, String str, String str2) throws UnsupportedEncodingException {
        if (sb.length() != 0) {
            sb.append('&');
        }
        sb.append(URLEncoder.encode(str, "UTF-8"));
        sb.append('=');
        sb.append(URLEncoder.encode(str2, "UTF-8"));
    }

    private final URL a(bh bhVar) {
        String str;
        String str2;
        if (bhVar.f) {
            String valueOf = String.valueOf(au.h());
            String valueOf2 = String.valueOf(au.j());
            if (valueOf2.length() != 0) {
                str = valueOf.concat(valueOf2);
                return new URL(str);
            }
            str2 = new String(valueOf);
        } else {
            String valueOf3 = String.valueOf(au.i());
            String valueOf4 = String.valueOf(au.j());
            if (valueOf4.length() != 0) {
                str = valueOf3.concat(valueOf4);
                return new URL(str);
            }
            str2 = new String(valueOf3);
        }
        str = str2;
        try {
            return new URL(str);
        } catch (MalformedURLException e) {
            e("Error trying to parse the hardcoded host url", e);
            return null;
        }
    }

    private final URL a(bh bhVar, String str) {
        String str2;
        if (bhVar.f) {
            String h = au.h();
            String j = au.j();
            StringBuilder sb = new StringBuilder(String.valueOf(h).length() + 1 + String.valueOf(j).length() + String.valueOf(str).length());
            sb.append(h);
            sb.append(j);
            sb.append("?");
            sb.append(str);
            str2 = sb.toString();
        } else {
            String i = au.i();
            String j2 = au.j();
            StringBuilder sb2 = new StringBuilder(String.valueOf(i).length() + 1 + String.valueOf(j2).length() + String.valueOf(str).length());
            sb2.append(i);
            sb2.append(j2);
            sb2.append("?");
            sb2.append(str);
            str2 = sb2.toString();
        }
        try {
            return new URL(str2);
        } catch (MalformedURLException e) {
            e("Error trying to parse the hardcoded host url", e);
            return null;
        }
    }
}
