package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.fq;
import java.io.IOException;
import java.util.Arrays;

public final class ic {
    private static final ic f = new ic(0, new int[0], new Object[0], false);

    /* renamed from: a  reason: collision with root package name */
    int f2282a;

    /* renamed from: b  reason: collision with root package name */
    int[] f2283b;
    Object[] c;
    int d;
    boolean e;

    public static ic a() {
        return f;
    }

    static ic b() {
        return new ic();
    }

    static ic a(ic icVar, ic icVar2) {
        int i = icVar.f2282a + icVar2.f2282a;
        int[] copyOf = Arrays.copyOf(icVar.f2283b, i);
        System.arraycopy(icVar2.f2283b, 0, copyOf, icVar.f2282a, icVar2.f2282a);
        Object[] copyOf2 = Arrays.copyOf(icVar.c, i);
        System.arraycopy(icVar2.c, 0, copyOf2, icVar.f2282a, icVar2.f2282a);
        return new ic(i, copyOf, copyOf2, true);
    }

    private ic() {
        this(0, new int[8], new Object[8], true);
    }

    private ic(int i, int[] iArr, Object[] objArr, boolean z) {
        this.d = -1;
        this.f2282a = i;
        this.f2283b = iArr;
        this.c = objArr;
        this.e = z;
    }

    public final void a(iv ivVar) throws IOException {
        if (this.f2282a != 0) {
            if (ivVar.a() == fq.e.j) {
                for (int i = 0; i < this.f2282a; i++) {
                    a(this.f2283b[i], this.c[i], ivVar);
                }
                return;
            }
            for (int i2 = this.f2282a - 1; i2 >= 0; i2--) {
                a(this.f2283b[i2], this.c[i2], ivVar);
            }
        }
    }

    private static void a(int i, Object obj, iv ivVar) throws IOException {
        int i2 = i >>> 3;
        int i3 = i & 7;
        if (i3 == 0) {
            ivVar.a(i2, ((Long) obj).longValue());
        } else if (i3 == 1) {
            ivVar.d(i2, ((Long) obj).longValue());
        } else if (i3 == 2) {
            ivVar.a(i2, (ej) obj);
        } else if (i3 != 3) {
            if (i3 == 5) {
                ivVar.d(i2, ((Integer) obj).intValue());
                return;
            }
            throw new RuntimeException(zzuv.e());
        } else if (ivVar.a() == fq.e.j) {
            ivVar.a(i2);
            ((ic) obj).a(ivVar);
            ivVar.b(i2);
        } else {
            ivVar.b(i2);
            ((ic) obj).a(ivVar);
            ivVar.a(i2);
        }
    }

    public final int c() {
        int i;
        int i2 = this.d;
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < this.f2282a; i4++) {
            int i5 = this.f2283b[i4];
            int i6 = i5 >>> 3;
            int i7 = i5 & 7;
            if (i7 == 0) {
                i = zztv.e(i6, ((Long) this.c[i4]).longValue());
            } else if (i7 == 1) {
                ((Long) this.c[i4]).longValue();
                i = zztv.g(i6);
            } else if (i7 == 2) {
                i = zztv.c(i6, (ej) this.c[i4]);
            } else if (i7 == 3) {
                i = (zztv.l(i6) << 1) + ((ic) this.c[i4]).c();
            } else if (i7 == 5) {
                ((Integer) this.c[i4]).intValue();
                i = zztv.e(i6);
            } else {
                throw new IllegalStateException(zzuv.e());
            }
            i3 += i;
        }
        this.d = i3;
        return i3;
    }

    public final boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof ic)) {
            return false;
        }
        ic icVar = (ic) obj;
        int i = this.f2282a;
        if (i == icVar.f2282a) {
            int[] iArr = this.f2283b;
            int[] iArr2 = icVar.f2283b;
            int i2 = 0;
            while (true) {
                if (i2 >= i) {
                    z = true;
                    break;
                } else if (iArr[i2] != iArr2[i2]) {
                    z = false;
                    break;
                } else {
                    i2++;
                }
            }
            if (z) {
                Object[] objArr = this.c;
                Object[] objArr2 = icVar.c;
                int i3 = this.f2282a;
                int i4 = 0;
                while (true) {
                    if (i4 >= i3) {
                        z2 = true;
                        break;
                    } else if (!objArr[i4].equals(objArr2[i4])) {
                        z2 = false;
                        break;
                    } else {
                        i4++;
                    }
                }
                return z2;
            }
        }
    }

    public final int hashCode() {
        int i = this.f2282a;
        int i2 = (i + 527) * 31;
        int[] iArr = this.f2283b;
        int i3 = 17;
        int i4 = 17;
        for (int i5 = 0; i5 < i; i5++) {
            i4 = (i4 * 31) + iArr[i5];
        }
        int i6 = (i2 + i4) * 31;
        Object[] objArr = this.c;
        int i7 = this.f2282a;
        for (int i8 = 0; i8 < i7; i8++) {
            i3 = (i3 * 31) + objArr[i8].hashCode();
        }
        return i6 + i3;
    }

    /* access modifiers changed from: package-private */
    public final void a(StringBuilder sb, int i) {
        for (int i2 = 0; i2 < this.f2282a; i2++) {
            gw.a(sb, i, String.valueOf(this.f2283b[i2] >>> 3), this.c[i2]);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(int i, Object obj) {
        if (this.e) {
            int i2 = this.f2282a;
            if (i2 == this.f2283b.length) {
                int i3 = this.f2282a + (i2 < 4 ? 8 : i2 >> 1);
                this.f2283b = Arrays.copyOf(this.f2283b, i3);
                this.c = Arrays.copyOf(this.c, i3);
            }
            int[] iArr = this.f2283b;
            int i4 = this.f2282a;
            iArr[i4] = i;
            this.c[i4] = obj;
            this.f2282a = i4 + 1;
            return;
        }
        throw new UnsupportedOperationException();
    }
}
