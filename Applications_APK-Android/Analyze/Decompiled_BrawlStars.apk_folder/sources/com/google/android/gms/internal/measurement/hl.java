package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.RandomAccess;

final class hl {

    /* renamed from: a  reason: collision with root package name */
    private static final Class<?> f2263a = d();

    /* renamed from: b  reason: collision with root package name */
    private static final ib<?, ?> f2264b = a(false);
    private static final ib<?, ?> c = a(true);
    private static final ib<?, ?> d = new id();

    public static void a(Class<?> cls) {
        Class<?> cls2;
        if (!fq.class.isAssignableFrom(cls) && (cls2 = f2263a) != null && !cls2.isAssignableFrom(cls)) {
            throw new IllegalArgumentException("Message classes must extend GeneratedMessage or GeneratedMessageLite");
        }
    }

    public static void a(int i, List<Double> list, iv ivVar, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            ivVar.g(i, list, z);
        }
    }

    public static void b(int i, List<Float> list, iv ivVar, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            ivVar.f(i, list, z);
        }
    }

    public static void c(int i, List<Long> list, iv ivVar, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            ivVar.c(i, list, z);
        }
    }

    public static void d(int i, List<Long> list, iv ivVar, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            ivVar.d(i, list, z);
        }
    }

    public static void e(int i, List<Long> list, iv ivVar, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            ivVar.n(i, list, z);
        }
    }

    public static void f(int i, List<Long> list, iv ivVar, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            ivVar.e(i, list, z);
        }
    }

    public static void g(int i, List<Long> list, iv ivVar, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            ivVar.l(i, list, z);
        }
    }

    public static void h(int i, List<Integer> list, iv ivVar, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            ivVar.a(i, list, z);
        }
    }

    public static void i(int i, List<Integer> list, iv ivVar, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            ivVar.j(i, list, z);
        }
    }

    public static void j(int i, List<Integer> list, iv ivVar, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            ivVar.m(i, list, z);
        }
    }

    public static void k(int i, List<Integer> list, iv ivVar, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            ivVar.b(i, list, z);
        }
    }

    public static void l(int i, List<Integer> list, iv ivVar, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            ivVar.k(i, list, z);
        }
    }

    public static void m(int i, List<Integer> list, iv ivVar, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            ivVar.h(i, list, z);
        }
    }

    public static void n(int i, List<Boolean> list, iv ivVar, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            ivVar.i(i, list, z);
        }
    }

    public static void a(int i, List<String> list, iv ivVar) throws IOException {
        if (list != null && !list.isEmpty()) {
            ivVar.a(i, list);
        }
    }

    public static void b(int i, List<ej> list, iv ivVar) throws IOException {
        if (list != null && !list.isEmpty()) {
            ivVar.b(i, list);
        }
    }

    public static void a(int i, List<?> list, iv ivVar, hj hjVar) throws IOException {
        if (list != null && !list.isEmpty()) {
            ivVar.a(i, list, hjVar);
        }
    }

    public static void b(int i, List<?> list, iv ivVar, hj hjVar) throws IOException {
        if (list != null && !list.isEmpty()) {
            ivVar.b(i, list, hjVar);
        }
    }

    static int a(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof gh) {
            gh ghVar = (gh) list;
            i = 0;
            while (i2 < size) {
                i += zztv.d(ghVar.b(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zztv.d(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    static int a(int i, List<Long> list) {
        if (list.size() == 0) {
            return 0;
        }
        return a(list) + (list.size() * zztv.l(i));
    }

    static int b(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof gh) {
            gh ghVar = (gh) list;
            i = 0;
            while (i2 < size) {
                i += zztv.e(ghVar.b(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zztv.e(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    static int b(int i, List<Long> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return b(list) + (size * zztv.l(i));
    }

    static int c(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof gh) {
            gh ghVar = (gh) list;
            i = 0;
            while (i2 < size) {
                i += zztv.f(ghVar.b(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zztv.f(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    static int c(int i, List<Long> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return c(list) + (size * zztv.l(i));
    }

    static int d(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof fr) {
            fr frVar = (fr) list;
            i = 0;
            while (i2 < size) {
                i += zztv.p(frVar.b(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zztv.p(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    static int d(int i, List<Integer> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return d(list) + (size * zztv.l(i));
    }

    static int e(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof fr) {
            fr frVar = (fr) list;
            i = 0;
            while (i2 < size) {
                i += zztv.m(frVar.b(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zztv.m(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    static int e(int i, List<Integer> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return e(list) + (size * zztv.l(i));
    }

    static int f(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof fr) {
            fr frVar = (fr) list;
            i = 0;
            while (i2 < size) {
                i += zztv.n(frVar.b(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zztv.n(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    static int f(int i, List<Integer> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return f(list) + (size * zztv.l(i));
    }

    static int g(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof fr) {
            fr frVar = (fr) list;
            i = 0;
            while (i2 < size) {
                i += zztv.o(frVar.b(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zztv.o(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    static int g(int i, List<Integer> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return g(list) + (size * zztv.l(i));
    }

    static int h(List<?> list) {
        return list.size() << 2;
    }

    static int h(int i, List<?> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * zztv.e(i);
    }

    static int i(List<?> list) {
        return list.size() << 3;
    }

    static int i(int i, List<?> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * zztv.g(i);
    }

    static int j(List<?> list) {
        return list.size();
    }

    static int j(int i, List<?> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * zztv.k(i);
    }

    static int k(int i, List<?> list) {
        int i2;
        int i3;
        int size = list.size();
        int i4 = 0;
        if (size == 0) {
            return 0;
        }
        int l = zztv.l(i) * size;
        if (list instanceof gd) {
            gd gdVar = (gd) list;
            while (i4 < size) {
                Object b2 = gdVar.b(i4);
                if (b2 instanceof ej) {
                    i3 = zztv.b((ej) b2);
                } else {
                    i3 = zztv.b((String) b2);
                }
                l += i3;
                i4++;
            }
        } else {
            while (i4 < size) {
                Object obj = list.get(i4);
                if (obj instanceof ej) {
                    i2 = zztv.b((ej) obj);
                } else {
                    i2 = zztv.b((String) obj);
                }
                l += i2;
                i4++;
            }
        }
        return l;
    }

    static int a(int i, Object obj, hj hjVar) {
        if (obj instanceof gb) {
            return zztv.a(i, (gb) obj);
        }
        return zztv.b(i, (gt) obj, hjVar);
    }

    static int a(int i, List<?> list, hj hjVar) {
        int i2;
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int l = zztv.l(i) * size;
        for (int i3 = 0; i3 < size; i3++) {
            Object obj = list.get(i3);
            if (obj instanceof gb) {
                i2 = zztv.a((gb) obj);
            } else {
                i2 = zztv.b((gt) obj, hjVar);
            }
            l += i2;
        }
        return l;
    }

    static int l(int i, List<ej> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int l = size * zztv.l(i);
        for (int i2 = 0; i2 < list.size(); i2++) {
            l += zztv.b(list.get(i2));
        }
        return l;
    }

    static int b(int i, List<gt> list, hj hjVar) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < size; i3++) {
            i2 += zztv.c(i, list.get(i3), hjVar);
        }
        return i2;
    }

    public static ib<?, ?> a() {
        return f2264b;
    }

    public static ib<?, ?> b() {
        return c;
    }

    public static ib<?, ?> c() {
        return d;
    }

    private static ib<?, ?> a(boolean z) {
        try {
            Class<?> e = e();
            if (e == null) {
                return null;
            }
            return (ib) e.getConstructor(Boolean.TYPE).newInstance(Boolean.valueOf(z));
        } catch (Throwable unused) {
            return null;
        }
    }

    private static Class<?> d() {
        try {
            return Class.forName("com.google.protobuf.GeneratedMessage");
        } catch (Throwable unused) {
            return null;
        }
    }

    private static Class<?> e() {
        try {
            return Class.forName("com.google.protobuf.UnknownFieldSetSchema");
        } catch (Throwable unused) {
            return null;
        }
    }

    static boolean a(Object obj, Object obj2) {
        if (obj != obj2) {
            return obj != null && obj.equals(obj2);
        }
        return true;
    }

    static <T> void a(go goVar, Object obj, Object obj2, long j) {
        ih.a(obj, j, goVar.a(ih.f(obj, j), ih.f(obj2, j)));
    }

    static <T, FT extends fj<FT>> void a(fe feVar, Object obj, Object obj2) {
        fh a2 = feVar.a(obj2);
        if (!a2.f2206a.isEmpty()) {
            fh b2 = feVar.b(obj);
            for (int i = 0; i < a2.f2206a.b(); i++) {
                b2.a((Map.Entry) a2.f2206a.b(i));
            }
            for (Map.Entry<FieldDescriptorType, Object> entry : a2.f2206a.c()) {
                b2.a((Map.Entry) entry);
            }
        }
    }

    static <T, UT, UB> void a(ib ibVar, Object obj, Object obj2) {
        ibVar.a(obj, ibVar.c(ibVar.b(obj), ibVar.b(obj2)));
    }

    static <UT, UB> UB a(int i, List<Integer> list, fv fvVar, UB ub, ib<UT, UB> ibVar) {
        UB ub2;
        if (fvVar == null) {
            return ub;
        }
        if (!(list instanceof RandomAccess)) {
            Iterator<Integer> it = list.iterator();
            loop1:
            while (true) {
                ub2 = ub;
                while (it.hasNext()) {
                    int intValue = it.next().intValue();
                    if (!fvVar.a(intValue)) {
                        ub = a(i, intValue, ub2, ibVar);
                        it.remove();
                    }
                }
                break loop1;
            }
        } else {
            int size = list.size();
            ub2 = ub;
            int i2 = 0;
            for (int i3 = 0; i3 < size; i3++) {
                int intValue2 = list.get(i3).intValue();
                if (fvVar.a(intValue2)) {
                    if (i3 != i2) {
                        list.set(i2, Integer.valueOf(intValue2));
                    }
                    i2++;
                } else {
                    ub2 = a(i, intValue2, ub2, ibVar);
                }
            }
            if (i2 != size) {
                list.subList(i2, size).clear();
            }
        }
        return ub2;
    }

    static <UT, UB> UB a(int i, int i2, Object obj, ib ibVar) {
        if (obj == null) {
            obj = ibVar.a();
        }
        ibVar.a(obj, i, (long) i2);
        return obj;
    }
}
