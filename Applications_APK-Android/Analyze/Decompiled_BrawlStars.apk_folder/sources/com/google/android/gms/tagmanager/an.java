package com.google.android.gms.tagmanager;

import android.net.Uri;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

class an {
    private static an d;

    /* renamed from: a  reason: collision with root package name */
    volatile a f2624a = a.NONE;

    /* renamed from: b  reason: collision with root package name */
    volatile String f2625b = null;
    volatile String c = null;
    private volatile String e = null;

    enum a {
        NONE,
        CONTAINER,
        CONTAINER_DEBUG
    }

    an() {
    }

    static an a() {
        an anVar;
        synchronized (an.class) {
            if (d == null) {
                d = new an();
            }
            anVar = d;
        }
        return anVar;
    }

    /* access modifiers changed from: package-private */
    public final synchronized boolean a(Uri uri) {
        try {
            String decode = URLDecoder.decode(uri.toString(), "UTF-8");
            if (decode.matches("^tagmanager.c.\\S+:\\/\\/preview\\/p\\?id=\\S+&gtm_auth=\\S+&gtm_preview=\\d+(&gtm_debug=x)?$")) {
                String valueOf = String.valueOf(decode);
                ab.d(valueOf.length() != 0 ? "Container preview url: ".concat(valueOf) : new String("Container preview url: "));
                if (decode.matches(".*?&gtm_debug=x$")) {
                    this.f2624a = a.CONTAINER_DEBUG;
                } else {
                    this.f2624a = a.CONTAINER;
                }
                this.e = uri.getQuery().replace("&gtm_debug=x", "");
                if (this.f2624a == a.CONTAINER || this.f2624a == a.CONTAINER_DEBUG) {
                    String valueOf2 = String.valueOf(this.e);
                    this.c = valueOf2.length() != 0 ? "/r?".concat(valueOf2) : new String("/r?");
                }
                this.f2625b = a(this.e);
                return true;
            } else if (!decode.matches("^tagmanager.c.\\S+:\\/\\/preview\\/p\\?id=\\S+&gtm_preview=$")) {
                String valueOf3 = String.valueOf(decode);
                ab.b(valueOf3.length() != 0 ? "Invalid preview uri: ".concat(valueOf3) : new String("Invalid preview uri: "));
                return false;
            } else if (!a(uri.getQuery()).equals(this.f2625b)) {
                return false;
            } else {
                String valueOf4 = String.valueOf(this.f2625b);
                ab.d(valueOf4.length() != 0 ? "Exit preview mode for container: ".concat(valueOf4) : new String("Exit preview mode for container: "));
                this.f2624a = a.NONE;
                this.c = null;
                return true;
            }
        } catch (UnsupportedEncodingException unused) {
            return false;
        }
    }

    private static String a(String str) {
        return str.split("&")[0].split("=")[1];
    }
}
