package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.support.v4.util.ArrayMap;
import android.support.v4.util.ArraySet;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.UnsupportedApiCallException;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.d;
import com.google.android.gms.common.api.internal.h;
import com.google.android.gms.common.internal.BaseGmsClient;
import com.google.android.gms.common.internal.IAccountAccessor;
import com.google.android.gms.common.internal.i;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.internal.n;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class d implements Handler.Callback {

    /* renamed from: a  reason: collision with root package name */
    public static final Status f1466a = new Status(4, "Sign-out occurred while this API call was in progress.");
    /* access modifiers changed from: package-private */

    /* renamed from: b  reason: collision with root package name */
    public static final Object f1467b = new Object();
    /* access modifiers changed from: private */
    public static final Status j = new Status(4, "The user must be signed in to make this API call.");
    private static d n;
    /* access modifiers changed from: package-private */
    public final Context c;
    public final AtomicInteger d = new AtomicInteger(1);
    public final AtomicInteger e = new AtomicInteger(0);
    /* access modifiers changed from: package-private */
    public final Map<bs<?>, a<?>> f = new ConcurrentHashMap(5, 0.75f, 1);
    /* access modifiers changed from: package-private */
    public o g = null;
    /* access modifiers changed from: package-private */
    public final Set<bs<?>> h = new ArraySet();
    public final Handler i;
    /* access modifiers changed from: private */
    public long k = 5000;
    /* access modifiers changed from: private */
    public long l = 120000;
    /* access modifiers changed from: private */
    public long m = 10000;
    private final com.google.android.gms.common.c o;
    /* access modifiers changed from: private */
    public final i p;
    private final Set<bs<?>> q = new ArraySet();

    public static d a(Context context) {
        d dVar;
        synchronized (f1467b) {
            if (n == null) {
                HandlerThread handlerThread = new HandlerThread("GoogleApiHandler", 9);
                handlerThread.start();
                n = new d(context.getApplicationContext(), handlerThread.getLooper(), com.google.android.gms.common.c.a());
            }
            dVar = n;
        }
        return dVar;
    }

    static class b {

        /* renamed from: a  reason: collision with root package name */
        final bs<?> f1470a;

        /* renamed from: b  reason: collision with root package name */
        final Feature f1471b;

        private b(bs<?> bsVar, Feature feature) {
            this.f1470a = bsVar;
            this.f1471b = feature;
        }

        public final boolean equals(Object obj) {
            if (obj != null && (obj instanceof b)) {
                b bVar = (b) obj;
                if (!j.a(this.f1470a, bVar.f1470a) || !j.a(this.f1471b, bVar.f1471b)) {
                    return false;
                }
                return true;
            }
            return false;
        }

        public final int hashCode() {
            return Arrays.hashCode(new Object[]{this.f1470a, this.f1471b});
        }

        public final String toString() {
            return j.a(this).a("key", this.f1470a).a("feature", this.f1471b).toString();
        }

        /* synthetic */ b(bs bsVar, Feature feature, byte b2) {
            this(bsVar, feature);
        }
    }

    class c implements bi, BaseGmsClient.c {

        /* renamed from: a  reason: collision with root package name */
        final a.f f1472a;

        /* renamed from: b  reason: collision with root package name */
        final bs<?> f1473b;
        boolean c = false;
        private IAccountAccessor e = null;
        private Set<Scope> f = null;

        public c(a.f fVar, bs<?> bsVar) {
            this.f1472a = fVar;
            this.f1473b = bsVar;
        }

        public final void a(ConnectionResult connectionResult) {
            d.this.i.post(new aw(this, connectionResult));
        }

        public final void b(ConnectionResult connectionResult) {
            a aVar = (a) d.this.f.get(this.f1473b);
            l.a(d.this.i);
            aVar.f1468a.a();
            aVar.onConnectionFailed(connectionResult);
        }

        public final void a(IAccountAccessor iAccountAccessor, Set<Scope> set) {
            if (iAccountAccessor == null || set == null) {
                Log.wtf("GoogleApiManager", "Received null response from onSignInSuccess", new Exception());
                b(new ConnectionResult(4));
                return;
            }
            this.e = iAccountAccessor;
            this.f = set;
            a();
        }

        /* access modifiers changed from: package-private */
        public final void a() {
            IAccountAccessor iAccountAccessor;
            if (this.c && (iAccountAccessor = this.e) != null) {
                this.f1472a.a(iAccountAccessor, this.f);
            }
        }
    }

    public static d a() {
        d dVar;
        synchronized (f1467b) {
            l.a(n, "Must guarantee manager is non-null before using getInstance");
            dVar = n;
        }
        return dVar;
    }

    public class a<O extends a.d> implements d.b, d.c, cb {

        /* renamed from: a  reason: collision with root package name */
        final a.f f1468a;

        /* renamed from: b  reason: collision with root package name */
        final Set<bu> f1469b = new HashSet();
        final Map<h.a<?>, bc> c = new HashMap();
        final int d;
        final zace e;
        boolean f;
        final List<b> g = new ArrayList();
        private final Queue<ai> i = new LinkedList();
        private final a.b j;
        private final bs<O> k;
        private final m l;
        private ConnectionResult m = null;

        public a(com.google.android.gms.common.api.c<O> cVar) {
            this.f1468a = cVar.a(d.this.i.getLooper(), this);
            a.f fVar = this.f1468a;
            if (fVar instanceof n) {
                this.j = ((n) fVar).i;
            } else {
                this.j = fVar;
            }
            this.k = cVar.f1374b;
            this.l = new m();
            this.d = cVar.d;
            if (this.f1468a.d()) {
                this.e = cVar.a(d.this.c, d.this.i);
            } else {
                this.e = null;
            }
        }

        public final void onConnected(Bundle bundle) {
            if (Looper.myLooper() == d.this.i.getLooper()) {
                a();
            } else {
                d.this.i.post(new ar(this));
            }
        }

        /* access modifiers changed from: package-private */
        public final void a() {
            e();
            b(ConnectionResult.f1352a);
            g();
            Iterator<bc> it = this.c.values().iterator();
            while (it.hasNext()) {
                if (a(it.next().f1416a.f1482b) != null) {
                    it.remove();
                } else {
                    try {
                        new com.google.android.gms.tasks.h();
                    } catch (DeadObjectException unused) {
                        onConnectionSuspended(1);
                        this.f1468a.a();
                    } catch (RemoteException unused2) {
                        it.remove();
                    }
                }
            }
            c();
            j();
        }

        public final void onConnectionSuspended(int i2) {
            if (Looper.myLooper() == d.this.i.getLooper()) {
                b();
            } else {
                d.this.i.post(new as(this));
            }
        }

        /* access modifiers changed from: package-private */
        public final void b() {
            e();
            this.f = true;
            this.l.c();
            d.this.i.sendMessageDelayed(Message.obtain(d.this.i, 9, this.k), d.this.k);
            d.this.i.sendMessageDelayed(Message.obtain(d.this.i, 11, this.k), d.this.l);
            d.this.p.f1609a.clear();
        }

        private final boolean a(ConnectionResult connectionResult) {
            synchronized (d.f1467b) {
                if (d.this.g == null || !d.this.h.contains(this.k)) {
                    return false;
                }
                d.this.g.b(connectionResult, this.d);
                return true;
            }
        }

        public final void a(ConnectionResult connectionResult, com.google.android.gms.common.api.a<?> aVar, boolean z) {
            if (Looper.myLooper() == d.this.i.getLooper()) {
                onConnectionFailed(connectionResult);
            } else {
                d.this.i.post(new at(this, connectionResult));
            }
        }

        public final void onConnectionFailed(ConnectionResult connectionResult) {
            l.a(d.this.i);
            zace zace = this.e;
            if (!(zace == null || zace.f1509a == null)) {
                zace.f1509a.a();
            }
            e();
            d.this.p.f1609a.clear();
            b(connectionResult);
            if (connectionResult.f1353b == 4) {
                a(d.j);
            } else if (this.i.isEmpty()) {
                this.m = connectionResult;
            } else if (!a(connectionResult) && !d.this.a(connectionResult, this.d)) {
                if (connectionResult.f1353b == 18) {
                    this.f = true;
                }
                if (this.f) {
                    d.this.i.sendMessageDelayed(Message.obtain(d.this.i, 9, this.k), d.this.k);
                    return;
                }
                String str = this.k.f1433a.f1372b;
                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 38);
                sb.append("API: ");
                sb.append(str);
                sb.append(" is not available on this device.");
                a(new Status(17, sb.toString()));
            }
        }

        /* access modifiers changed from: package-private */
        public final void c() {
            ArrayList arrayList = new ArrayList(this.i);
            int size = arrayList.size();
            int i2 = 0;
            while (i2 < size) {
                Object obj = arrayList.get(i2);
                i2++;
                ai aiVar = (ai) obj;
                if (!this.f1468a.b()) {
                    return;
                }
                if (b(aiVar)) {
                    this.i.remove(aiVar);
                }
            }
        }

        public final void a(ai aiVar) {
            l.a(d.this.i);
            if (!this.f1468a.b()) {
                this.i.add(aiVar);
                ConnectionResult connectionResult = this.m;
                if (connectionResult == null || !connectionResult.a()) {
                    h();
                } else {
                    onConnectionFailed(this.m);
                }
            } else if (b(aiVar)) {
                j();
            } else {
                this.i.add(aiVar);
            }
        }

        public final void d() {
            l.a(d.this.i);
            a(d.f1466a);
            this.l.b();
            for (h.a brVar : (h.a[]) this.c.keySet().toArray(new h.a[this.c.size()])) {
                a(new br(brVar, new com.google.android.gms.tasks.h()));
            }
            b(new ConnectionResult(4));
            if (this.f1468a.b()) {
                this.f1468a.a(new au(this));
            }
        }

        public final void e() {
            l.a(d.this.i);
            this.m = null;
        }

        public final ConnectionResult f() {
            l.a(d.this.i);
            return this.m;
        }

        private final boolean b(ai aiVar) {
            if (!(aiVar instanceof bd)) {
                c(aiVar);
                return true;
            }
            bd bdVar = (bd) aiVar;
            Feature a2 = a(bdVar.b(this));
            if (a2 == null) {
                c(aiVar);
                return true;
            }
            if (bdVar.c(this)) {
                b bVar = new b(this.k, a2, (byte) 0);
                int indexOf = this.g.indexOf(bVar);
                if (indexOf >= 0) {
                    b bVar2 = this.g.get(indexOf);
                    d.this.i.removeMessages(15, bVar2);
                    d.this.i.sendMessageDelayed(Message.obtain(d.this.i, 15, bVar2), d.this.k);
                } else {
                    this.g.add(bVar);
                    d.this.i.sendMessageDelayed(Message.obtain(d.this.i, 15, bVar), d.this.k);
                    d.this.i.sendMessageDelayed(Message.obtain(d.this.i, 16, bVar), d.this.l);
                    ConnectionResult connectionResult = new ConnectionResult(2, null);
                    if (!a(connectionResult)) {
                        d.this.a(connectionResult, this.d);
                    }
                }
            } else {
                bdVar.a(new UnsupportedApiCallException(a2));
            }
            return false;
        }

        private final void c(ai aiVar) {
            aiVar.a(this.l, i());
            try {
                aiVar.a(this);
            } catch (DeadObjectException unused) {
                onConnectionSuspended(1);
                this.f1468a.a();
            }
        }

        public final void a(Status status) {
            l.a(d.this.i);
            for (ai a2 : this.i) {
                a2.a(status);
            }
            this.i.clear();
        }

        /* access modifiers changed from: package-private */
        public final void g() {
            if (this.f) {
                d.this.i.removeMessages(11, this.k);
                d.this.i.removeMessages(9, this.k);
                this.f = false;
            }
        }

        private final void j() {
            d.this.i.removeMessages(12, this.k);
            d.this.i.sendMessageDelayed(d.this.i.obtainMessage(12, this.k), d.this.m);
        }

        /* access modifiers changed from: package-private */
        public final boolean a(boolean z) {
            l.a(d.this.i);
            if (!this.f1468a.b() || this.c.size() != 0) {
                return false;
            }
            if (this.l.a()) {
                if (z) {
                    j();
                }
                return false;
            }
            this.f1468a.a();
            return true;
        }

        public final void h() {
            l.a(d.this.i);
            if (!this.f1468a.b() && !this.f1468a.c()) {
                int a2 = d.this.p.a(d.this.c, this.f1468a);
                if (a2 != 0) {
                    onConnectionFailed(new ConnectionResult(a2, null));
                    return;
                }
                c cVar = new c(this.f1468a, this.k);
                if (this.f1468a.d()) {
                    this.e.a(cVar);
                }
                this.f1468a.a(cVar);
            }
        }

        private final void b(ConnectionResult connectionResult) {
            for (bu next : this.f1469b) {
                String str = null;
                if (j.a(connectionResult, ConnectionResult.f1352a)) {
                    str = this.f1468a.f();
                }
                next.a(this.k, connectionResult, str);
            }
            this.f1469b.clear();
        }

        public final boolean i() {
            return this.f1468a.d();
        }

        private final Feature a(Feature[] featureArr) {
            if (!(featureArr == null || featureArr.length == 0)) {
                Feature[] h2 = this.f1468a.h();
                if (h2 == null) {
                    h2 = new Feature[0];
                }
                ArrayMap arrayMap = new ArrayMap(h2.length);
                for (Feature feature : h2) {
                    arrayMap.put(feature.f1354a, Long.valueOf(feature.a()));
                }
                for (Feature feature2 : featureArr) {
                    if (!arrayMap.containsKey(feature2.f1354a) || ((Long) arrayMap.get(feature2.f1354a)).longValue() < feature2.a()) {
                        return feature2;
                    }
                }
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        public final void a(b bVar) {
            Feature[] b2;
            if (this.g.remove(bVar)) {
                d.this.i.removeMessages(15, bVar);
                d.this.i.removeMessages(16, bVar);
                Feature feature = bVar.f1471b;
                ArrayList arrayList = new ArrayList(this.i.size());
                for (ai next : this.i) {
                    if ((next instanceof bd) && (b2 = ((bd) next).b(this)) != null && com.google.android.gms.common.util.b.a(b2, feature)) {
                        arrayList.add(next);
                    }
                }
                ArrayList arrayList2 = arrayList;
                int size = arrayList2.size();
                int i2 = 0;
                while (i2 < size) {
                    Object obj = arrayList2.get(i2);
                    i2++;
                    ai aiVar = (ai) obj;
                    this.i.remove(aiVar);
                    aiVar.a(new UnsupportedApiCallException(feature));
                }
            }
        }
    }

    private d(Context context, Looper looper, com.google.android.gms.common.c cVar) {
        this.c = context;
        this.i = new com.google.android.gms.internal.base.d(looper, this);
        this.o = cVar;
        this.p = new i(cVar);
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(6));
    }

    public final void a(o oVar) {
        synchronized (f1467b) {
            if (this.g != oVar) {
                this.g = oVar;
                this.h.clear();
            }
            this.h.addAll(oVar.f1488b);
        }
    }

    public final void b() {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(3));
    }

    /* JADX WARNING: Removed duplicated region for block: B:58:0x01b6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean handleMessage(android.os.Message r8) {
        /*
            r7 = this;
            int r0 = r8.what
            r1 = 300000(0x493e0, double:1.482197E-318)
            r3 = 0
            r4 = 0
            r5 = 1
            switch(r0) {
                case 1: goto L_0x0308;
                case 2: goto L_0x02a7;
                case 3: goto L_0x028a;
                case 4: goto L_0x0244;
                case 5: goto L_0x01ba;
                case 6: goto L_0x015d;
                case 7: goto L_0x0154;
                case 8: goto L_0x0244;
                case 9: goto L_0x0130;
                case 10: goto L_0x010b;
                case 11: goto L_0x00be;
                case 12: goto L_0x00a5;
                case 13: goto L_0x0244;
                case 14: goto L_0x0075;
                case 15: goto L_0x003d;
                case 16: goto L_0x0020;
                default: goto L_0x000b;
            }
        L_0x000b:
            int r8 = r8.what
            r0 = 31
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r0 = "Unknown message id: "
            r1.append(r0)
            r1.append(r8)
            r1.toString()
            return r4
        L_0x0020:
            java.lang.Object r8 = r8.obj
            com.google.android.gms.common.api.internal.d$b r8 = (com.google.android.gms.common.api.internal.d.b) r8
            java.util.Map<com.google.android.gms.common.api.internal.bs<?>, com.google.android.gms.common.api.internal.d$a<?>> r0 = r7.f
            com.google.android.gms.common.api.internal.bs<?> r1 = r8.f1470a
            boolean r0 = r0.containsKey(r1)
            if (r0 == 0) goto L_0x033f
            java.util.Map<com.google.android.gms.common.api.internal.bs<?>, com.google.android.gms.common.api.internal.d$a<?>> r0 = r7.f
            com.google.android.gms.common.api.internal.bs<?> r1 = r8.f1470a
            java.lang.Object r0 = r0.get(r1)
            com.google.android.gms.common.api.internal.d$a r0 = (com.google.android.gms.common.api.internal.d.a) r0
            r0.a(r8)
            goto L_0x033f
        L_0x003d:
            java.lang.Object r8 = r8.obj
            com.google.android.gms.common.api.internal.d$b r8 = (com.google.android.gms.common.api.internal.d.b) r8
            java.util.Map<com.google.android.gms.common.api.internal.bs<?>, com.google.android.gms.common.api.internal.d$a<?>> r0 = r7.f
            com.google.android.gms.common.api.internal.bs<?> r1 = r8.f1470a
            boolean r0 = r0.containsKey(r1)
            if (r0 == 0) goto L_0x033f
            java.util.Map<com.google.android.gms.common.api.internal.bs<?>, com.google.android.gms.common.api.internal.d$a<?>> r0 = r7.f
            com.google.android.gms.common.api.internal.bs<?> r1 = r8.f1470a
            java.lang.Object r0 = r0.get(r1)
            com.google.android.gms.common.api.internal.d$a r0 = (com.google.android.gms.common.api.internal.d.a) r0
            java.util.List<com.google.android.gms.common.api.internal.d$b> r1 = r0.g
            boolean r8 = r1.contains(r8)
            if (r8 != 0) goto L_0x005f
            goto L_0x033f
        L_0x005f:
            boolean r8 = r0.f
            if (r8 != 0) goto L_0x033f
            com.google.android.gms.common.api.a$f r8 = r0.f1468a
            boolean r8 = r8.b()
            if (r8 != 0) goto L_0x0070
            r0.h()
            goto L_0x033f
        L_0x0070:
            r0.c()
            goto L_0x033f
        L_0x0075:
            java.lang.Object r8 = r8.obj
            com.google.android.gms.common.api.internal.p r8 = (com.google.android.gms.common.api.internal.p) r8
            com.google.android.gms.common.api.internal.bs<?> r0 = r8.f1489a
            java.util.Map<com.google.android.gms.common.api.internal.bs<?>, com.google.android.gms.common.api.internal.d$a<?>> r1 = r7.f
            boolean r1 = r1.containsKey(r0)
            if (r1 != 0) goto L_0x008e
            com.google.android.gms.tasks.h<java.lang.Boolean> r8 = r8.f1490b
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r4)
            r8.a(r0)
            goto L_0x033f
        L_0x008e:
            java.util.Map<com.google.android.gms.common.api.internal.bs<?>, com.google.android.gms.common.api.internal.d$a<?>> r1 = r7.f
            java.lang.Object r0 = r1.get(r0)
            com.google.android.gms.common.api.internal.d$a r0 = (com.google.android.gms.common.api.internal.d.a) r0
            boolean r0 = r0.a(r4)
            com.google.android.gms.tasks.h<java.lang.Boolean> r8 = r8.f1490b
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r8.a(r0)
            goto L_0x033f
        L_0x00a5:
            java.util.Map<com.google.android.gms.common.api.internal.bs<?>, com.google.android.gms.common.api.internal.d$a<?>> r0 = r7.f
            java.lang.Object r1 = r8.obj
            boolean r0 = r0.containsKey(r1)
            if (r0 == 0) goto L_0x033f
            java.util.Map<com.google.android.gms.common.api.internal.bs<?>, com.google.android.gms.common.api.internal.d$a<?>> r0 = r7.f
            java.lang.Object r8 = r8.obj
            java.lang.Object r8 = r0.get(r8)
            com.google.android.gms.common.api.internal.d$a r8 = (com.google.android.gms.common.api.internal.d.a) r8
            r8.a(r5)
            goto L_0x033f
        L_0x00be:
            java.util.Map<com.google.android.gms.common.api.internal.bs<?>, com.google.android.gms.common.api.internal.d$a<?>> r0 = r7.f
            java.lang.Object r1 = r8.obj
            boolean r0 = r0.containsKey(r1)
            if (r0 == 0) goto L_0x033f
            java.util.Map<com.google.android.gms.common.api.internal.bs<?>, com.google.android.gms.common.api.internal.d$a<?>> r0 = r7.f
            java.lang.Object r8 = r8.obj
            java.lang.Object r8 = r0.get(r8)
            com.google.android.gms.common.api.internal.d$a r8 = (com.google.android.gms.common.api.internal.d.a) r8
            com.google.android.gms.common.api.internal.d r0 = com.google.android.gms.common.api.internal.d.this
            android.os.Handler r0 = r0.i
            com.google.android.gms.common.internal.l.a(r0)
            boolean r0 = r8.f
            if (r0 == 0) goto L_0x033f
            r8.g()
            com.google.android.gms.common.api.internal.d r0 = com.google.android.gms.common.api.internal.d.this
            com.google.android.gms.common.c r0 = r0.o
            com.google.android.gms.common.api.internal.d r1 = com.google.android.gms.common.api.internal.d.this
            android.content.Context r1 = r1.c
            int r0 = r0.a(r1)
            r1 = 18
            r2 = 8
            if (r0 != r1) goto L_0x00fa
            com.google.android.gms.common.api.Status r0 = new com.google.android.gms.common.api.Status
            java.lang.String r1 = "Connection timed out while waiting for Google Play services update to complete."
            r0.<init>(r2, r1)
            goto L_0x0101
        L_0x00fa:
            com.google.android.gms.common.api.Status r0 = new com.google.android.gms.common.api.Status
            java.lang.String r1 = "API failed to connect while resuming due to an unknown error."
            r0.<init>(r2, r1)
        L_0x0101:
            r8.a(r0)
            com.google.android.gms.common.api.a$f r8 = r8.f1468a
            r8.a()
            goto L_0x033f
        L_0x010b:
            java.util.Set<com.google.android.gms.common.api.internal.bs<?>> r8 = r7.q
            java.util.Iterator r8 = r8.iterator()
        L_0x0111:
            boolean r0 = r8.hasNext()
            if (r0 == 0) goto L_0x0129
            java.lang.Object r0 = r8.next()
            com.google.android.gms.common.api.internal.bs r0 = (com.google.android.gms.common.api.internal.bs) r0
            java.util.Map<com.google.android.gms.common.api.internal.bs<?>, com.google.android.gms.common.api.internal.d$a<?>> r1 = r7.f
            java.lang.Object r0 = r1.remove(r0)
            com.google.android.gms.common.api.internal.d$a r0 = (com.google.android.gms.common.api.internal.d.a) r0
            r0.d()
            goto L_0x0111
        L_0x0129:
            java.util.Set<com.google.android.gms.common.api.internal.bs<?>> r8 = r7.q
            r8.clear()
            goto L_0x033f
        L_0x0130:
            java.util.Map<com.google.android.gms.common.api.internal.bs<?>, com.google.android.gms.common.api.internal.d$a<?>> r0 = r7.f
            java.lang.Object r1 = r8.obj
            boolean r0 = r0.containsKey(r1)
            if (r0 == 0) goto L_0x033f
            java.util.Map<com.google.android.gms.common.api.internal.bs<?>, com.google.android.gms.common.api.internal.d$a<?>> r0 = r7.f
            java.lang.Object r8 = r8.obj
            java.lang.Object r8 = r0.get(r8)
            com.google.android.gms.common.api.internal.d$a r8 = (com.google.android.gms.common.api.internal.d.a) r8
            com.google.android.gms.common.api.internal.d r0 = com.google.android.gms.common.api.internal.d.this
            android.os.Handler r0 = r0.i
            com.google.android.gms.common.internal.l.a(r0)
            boolean r0 = r8.f
            if (r0 == 0) goto L_0x033f
            r8.h()
            goto L_0x033f
        L_0x0154:
            java.lang.Object r8 = r8.obj
            com.google.android.gms.common.api.c r8 = (com.google.android.gms.common.api.c) r8
            r7.a(r8)
            goto L_0x033f
        L_0x015d:
            android.content.Context r8 = r7.c
            android.content.Context r8 = r8.getApplicationContext()
            boolean r8 = r8 instanceof android.app.Application
            if (r8 == 0) goto L_0x033f
            android.content.Context r8 = r7.c
            android.content.Context r8 = r8.getApplicationContext()
            android.app.Application r8 = (android.app.Application) r8
            com.google.android.gms.common.api.internal.b.a(r8)
            com.google.android.gms.common.api.internal.b r8 = com.google.android.gms.common.api.internal.b.a()
            com.google.android.gms.common.api.internal.aq r0 = new com.google.android.gms.common.api.internal.aq
            r0.<init>(r7)
            r8.a(r0)
            com.google.android.gms.common.api.internal.b r8 = com.google.android.gms.common.api.internal.b.a()
            java.util.concurrent.atomic.AtomicBoolean r0 = r8.f1413b
            boolean r0 = r0.get()
            if (r0 != 0) goto L_0x01ae
            boolean r0 = com.google.android.gms.common.util.o.a()
            if (r0 == 0) goto L_0x01ac
            android.app.ActivityManager$RunningAppProcessInfo r0 = new android.app.ActivityManager$RunningAppProcessInfo
            r0.<init>()
            android.app.ActivityManager.getMyMemoryState(r0)
            java.util.concurrent.atomic.AtomicBoolean r3 = r8.f1413b
            boolean r3 = r3.getAndSet(r5)
            if (r3 != 0) goto L_0x01ae
            int r0 = r0.importance
            r3 = 100
            if (r0 <= r3) goto L_0x01ae
            java.util.concurrent.atomic.AtomicBoolean r0 = r8.f1412a
            r0.set(r5)
            goto L_0x01ae
        L_0x01ac:
            r8 = 1
            goto L_0x01b4
        L_0x01ae:
            java.util.concurrent.atomic.AtomicBoolean r8 = r8.f1412a
            boolean r8 = r8.get()
        L_0x01b4:
            if (r8 != 0) goto L_0x033f
            r7.m = r1
            goto L_0x033f
        L_0x01ba:
            int r0 = r8.arg1
            java.lang.Object r8 = r8.obj
            com.google.android.gms.common.ConnectionResult r8 = (com.google.android.gms.common.ConnectionResult) r8
            java.util.Map<com.google.android.gms.common.api.internal.bs<?>, com.google.android.gms.common.api.internal.d$a<?>> r1 = r7.f
            java.util.Collection r1 = r1.values()
            java.util.Iterator r1 = r1.iterator()
        L_0x01ca:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x01db
            java.lang.Object r2 = r1.next()
            com.google.android.gms.common.api.internal.d$a r2 = (com.google.android.gms.common.api.internal.d.a) r2
            int r4 = r2.d
            if (r4 != r0) goto L_0x01ca
            goto L_0x01dc
        L_0x01db:
            r2 = r3
        L_0x01dc:
            if (r2 == 0) goto L_0x0220
            com.google.android.gms.common.api.Status r0 = new com.google.android.gms.common.api.Status
            r1 = 17
            com.google.android.gms.common.c r3 = r7.o
            int r4 = r8.f1353b
            java.lang.String r3 = r3.b(r4)
            java.lang.String r8 = r8.d
            java.lang.String r4 = java.lang.String.valueOf(r3)
            int r4 = r4.length()
            int r4 = r4 + 69
            java.lang.String r6 = java.lang.String.valueOf(r8)
            int r6 = r6.length()
            int r4 = r4 + r6
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>(r4)
            java.lang.String r4 = "Error resolution was canceled by the user, original error message: "
            r6.append(r4)
            r6.append(r3)
            java.lang.String r3 = ": "
            r6.append(r3)
            r6.append(r8)
            java.lang.String r8 = r6.toString()
            r0.<init>(r1, r8)
            r2.a(r0)
            goto L_0x033f
        L_0x0220:
            r8 = 76
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r8)
            java.lang.String r8 = "Could not find API instance "
            r1.append(r8)
            r1.append(r0)
            java.lang.String r8 = " while trying to fail enqueued calls."
            r1.append(r8)
            java.lang.String r8 = r1.toString()
            java.lang.Exception r0 = new java.lang.Exception
            r0.<init>()
            java.lang.String r1 = "GoogleApiManager"
            android.util.Log.wtf(r1, r8, r0)
            goto L_0x033f
        L_0x0244:
            java.lang.Object r8 = r8.obj
            com.google.android.gms.common.api.internal.bb r8 = (com.google.android.gms.common.api.internal.bb) r8
            java.util.Map<com.google.android.gms.common.api.internal.bs<?>, com.google.android.gms.common.api.internal.d$a<?>> r0 = r7.f
            com.google.android.gms.common.api.c<?> r1 = r8.c
            com.google.android.gms.common.api.internal.bs<O> r1 = r1.f1374b
            java.lang.Object r0 = r0.get(r1)
            com.google.android.gms.common.api.internal.d$a r0 = (com.google.android.gms.common.api.internal.d.a) r0
            if (r0 != 0) goto L_0x0267
            com.google.android.gms.common.api.c<?> r0 = r8.c
            r7.a(r0)
            java.util.Map<com.google.android.gms.common.api.internal.bs<?>, com.google.android.gms.common.api.internal.d$a<?>> r0 = r7.f
            com.google.android.gms.common.api.c<?> r1 = r8.c
            com.google.android.gms.common.api.internal.bs<O> r1 = r1.f1374b
            java.lang.Object r0 = r0.get(r1)
            com.google.android.gms.common.api.internal.d$a r0 = (com.google.android.gms.common.api.internal.d.a) r0
        L_0x0267:
            boolean r1 = r0.i()
            if (r1 == 0) goto L_0x0283
            java.util.concurrent.atomic.AtomicInteger r1 = r7.e
            int r1 = r1.get()
            int r2 = r8.f1415b
            if (r1 == r2) goto L_0x0283
            com.google.android.gms.common.api.internal.ai r8 = r8.f1414a
            com.google.android.gms.common.api.Status r1 = com.google.android.gms.common.api.internal.d.f1466a
            r8.a(r1)
            r0.d()
            goto L_0x033f
        L_0x0283:
            com.google.android.gms.common.api.internal.ai r8 = r8.f1414a
            r0.a(r8)
            goto L_0x033f
        L_0x028a:
            java.util.Map<com.google.android.gms.common.api.internal.bs<?>, com.google.android.gms.common.api.internal.d$a<?>> r8 = r7.f
            java.util.Collection r8 = r8.values()
            java.util.Iterator r8 = r8.iterator()
        L_0x0294:
            boolean r0 = r8.hasNext()
            if (r0 == 0) goto L_0x033f
            java.lang.Object r0 = r8.next()
            com.google.android.gms.common.api.internal.d$a r0 = (com.google.android.gms.common.api.internal.d.a) r0
            r0.e()
            r0.h()
            goto L_0x0294
        L_0x02a7:
            java.lang.Object r8 = r8.obj
            com.google.android.gms.common.api.internal.bu r8 = (com.google.android.gms.common.api.internal.bu) r8
            android.support.v4.util.ArrayMap<com.google.android.gms.common.api.internal.bs<?>, com.google.android.gms.common.ConnectionResult> r0 = r8.f1438a
            java.util.Set r0 = r0.keySet()
            java.util.Iterator r0 = r0.iterator()
        L_0x02b5:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x033f
            java.lang.Object r1 = r0.next()
            com.google.android.gms.common.api.internal.bs r1 = (com.google.android.gms.common.api.internal.bs) r1
            java.util.Map<com.google.android.gms.common.api.internal.bs<?>, com.google.android.gms.common.api.internal.d$a<?>> r2 = r7.f
            java.lang.Object r2 = r2.get(r1)
            com.google.android.gms.common.api.internal.d$a r2 = (com.google.android.gms.common.api.internal.d.a) r2
            if (r2 != 0) goto L_0x02d6
            com.google.android.gms.common.ConnectionResult r0 = new com.google.android.gms.common.ConnectionResult
            r2 = 13
            r0.<init>(r2)
            r8.a(r1, r0, r3)
            goto L_0x033f
        L_0x02d6:
            com.google.android.gms.common.api.a$f r4 = r2.f1468a
            boolean r4 = r4.b()
            if (r4 == 0) goto L_0x02ea
            com.google.android.gms.common.ConnectionResult r4 = com.google.android.gms.common.ConnectionResult.f1352a
            com.google.android.gms.common.api.a$f r2 = r2.f1468a
            java.lang.String r2 = r2.f()
            r8.a(r1, r4, r2)
            goto L_0x02b5
        L_0x02ea:
            com.google.android.gms.common.ConnectionResult r4 = r2.f()
            if (r4 == 0) goto L_0x02f8
            com.google.android.gms.common.ConnectionResult r2 = r2.f()
            r8.a(r1, r2, r3)
            goto L_0x02b5
        L_0x02f8:
            com.google.android.gms.common.api.internal.d r1 = com.google.android.gms.common.api.internal.d.this
            android.os.Handler r1 = r1.i
            com.google.android.gms.common.internal.l.a(r1)
            java.util.Set<com.google.android.gms.common.api.internal.bu> r1 = r2.f1469b
            r1.add(r8)
            r2.h()
            goto L_0x02b5
        L_0x0308:
            java.lang.Object r8 = r8.obj
            java.lang.Boolean r8 = (java.lang.Boolean) r8
            boolean r8 = r8.booleanValue()
            if (r8 == 0) goto L_0x0314
            r1 = 10000(0x2710, double:4.9407E-320)
        L_0x0314:
            r7.m = r1
            android.os.Handler r8 = r7.i
            r0 = 12
            r8.removeMessages(r0)
            java.util.Map<com.google.android.gms.common.api.internal.bs<?>, com.google.android.gms.common.api.internal.d$a<?>> r8 = r7.f
            java.util.Set r8 = r8.keySet()
            java.util.Iterator r8 = r8.iterator()
        L_0x0327:
            boolean r1 = r8.hasNext()
            if (r1 == 0) goto L_0x033f
            java.lang.Object r1 = r8.next()
            com.google.android.gms.common.api.internal.bs r1 = (com.google.android.gms.common.api.internal.bs) r1
            android.os.Handler r2 = r7.i
            android.os.Message r1 = r2.obtainMessage(r0, r1)
            long r3 = r7.m
            r2.sendMessageDelayed(r1, r3)
            goto L_0x0327
        L_0x033f:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.d.handleMessage(android.os.Message):boolean");
    }

    /* access modifiers changed from: package-private */
    public final boolean a(ConnectionResult connectionResult, int i2) {
        return this.o.a(this.c, connectionResult, i2);
    }

    public final void b(ConnectionResult connectionResult, int i2) {
        if (!a(connectionResult, i2)) {
            Handler handler = this.i;
            handler.sendMessage(handler.obtainMessage(5, i2, 0, connectionResult));
        }
    }

    private final void a(com.google.android.gms.common.api.c<?> cVar) {
        bs<O> bsVar = cVar.f1374b;
        a aVar = this.f.get(bsVar);
        if (aVar == null) {
            aVar = new a(cVar);
            this.f.put(bsVar, aVar);
        }
        if (aVar.i()) {
            this.q.add(bsVar);
        }
        aVar.h();
    }
}
