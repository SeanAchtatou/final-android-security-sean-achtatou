package com.google.android.gms.internal.measurement;

import android.support.v7.widget.ActivityChooserView;
import java.io.IOException;

public abstract class ev {

    /* renamed from: a  reason: collision with root package name */
    int f2190a;

    /* renamed from: b  reason: collision with root package name */
    int f2191b;
    ex c;
    private int d;
    private boolean e;

    public abstract int a() throws IOException;

    public abstract <T extends gt> T a(hd<T> hdVar, fd fdVar) throws IOException;

    public abstract void a(int i) throws zzuv;

    public abstract double b() throws IOException;

    public abstract boolean b(int i) throws IOException;

    public abstract float c() throws IOException;

    public abstract int c(int i) throws zzuv;

    public abstract long d() throws IOException;

    public abstract void d(int i);

    public abstract long e() throws IOException;

    public abstract void e(int i) throws IOException;

    public abstract int f() throws IOException;

    public abstract long g() throws IOException;

    public abstract int h() throws IOException;

    public abstract boolean i() throws IOException;

    public abstract String j() throws IOException;

    public abstract String k() throws IOException;

    public abstract ej l() throws IOException;

    public abstract int m() throws IOException;

    public abstract int n() throws IOException;

    public abstract int o() throws IOException;

    public abstract long p() throws IOException;

    public abstract int q() throws IOException;

    public abstract long r() throws IOException;

    /* access modifiers changed from: package-private */
    public abstract long s() throws IOException;

    public abstract boolean t() throws IOException;

    public abstract int u();

    static ev a(byte[] bArr, int i, int i2) {
        ew ewVar = new ew(bArr, i, i2, false, (byte) 0);
        try {
            ewVar.c(i2);
            return ewVar;
        } catch (zzuv e2) {
            throw new IllegalArgumentException(e2);
        }
    }

    private ev() {
        this.f2191b = 100;
        this.d = ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
        this.e = false;
    }

    /* synthetic */ ev(byte b2) {
        this();
    }
}
