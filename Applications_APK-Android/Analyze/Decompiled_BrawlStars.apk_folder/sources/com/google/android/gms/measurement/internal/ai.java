package com.google.android.gms.measurement.internal;

final class ai implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ar f2374a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ o f2375b;

    ai(ar arVar, o oVar) {
        this.f2374a = arVar;
        this.f2375b = oVar;
    }

    public final void run() {
        if (this.f2374a.i == null) {
            this.f2375b.c.a("Install Referrer Reporter is null");
            return;
        }
        ae aeVar = this.f2374a.i;
        aeVar.a(aeVar.f2368a.m().getPackageName());
    }
}
