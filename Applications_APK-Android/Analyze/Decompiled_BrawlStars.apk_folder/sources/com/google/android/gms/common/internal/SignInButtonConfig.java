package com.google.android.gms.common.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.a;

public class SignInButtonConfig extends AbstractSafeParcelable {
    public static final Parcelable.Creator<SignInButtonConfig> CREATOR = new y();

    /* renamed from: a  reason: collision with root package name */
    private final int f1583a;

    /* renamed from: b  reason: collision with root package name */
    private final int f1584b;
    private final int c;
    @Deprecated
    private final Scope[] d;

    SignInButtonConfig(int i, int i2, int i3, Scope[] scopeArr) {
        this.f1583a = i;
        this.f1584b = i2;
        this.c = i3;
        this.d = scopeArr;
    }

    public SignInButtonConfig(int i, int i2) {
        this(1, i, i2, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.common.api.Scope[], int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void */
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.b(parcel, 1, this.f1583a);
        a.b(parcel, 2, this.f1584b);
        a.b(parcel, 3, this.c);
        a.a(parcel, 4, (Parcelable[]) this.d, i, false);
        a.b(parcel, a2);
    }
}
