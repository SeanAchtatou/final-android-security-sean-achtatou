package com.google.android.gms.measurement.internal;

import android.text.TextUtils;

final class be implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zzag f2415a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ zzk f2416b;
    private final /* synthetic */ zzby c;

    be(zzby zzby, zzag zzag, zzk zzk) {
        this.c = zzby;
        this.f2415a = zzag;
        this.f2416b = zzk;
    }

    public final void run() {
        zzag zzag;
        zzby zzby = this.c;
        zzag zzag2 = this.f2415a;
        zzk zzk = this.f2416b;
        boolean z = false;
        if (!(!"_cmp".equals(zzag2.f2595a) || zzag2.f2596b == null || zzag2.f2596b.f2594a.size() == 0)) {
            String d = zzag2.f2596b.d("_cis");
            if (!TextUtils.isEmpty(d) && (("referrer broadcast".equals(d) || "referrer API".equals(d)) && zzby.f2597a.f2528b.e.g(zzk.f2601a))) {
                z = true;
            }
        }
        if (z) {
            zzby.f2597a.q().i.a("Event has been filtered ", zzag2.toString());
            zzag = new zzag("_cmpx", zzag2.f2596b, zzag2.c, zzag2.d);
        } else {
            zzag = zzag2;
        }
        this.c.f2597a.k();
        this.c.f2597a.a(zzag, this.f2416b);
    }
}
