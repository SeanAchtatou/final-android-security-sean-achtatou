package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiActivity;
import com.google.android.gms.common.c;

final class bx implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bv f1443a;

    /* renamed from: b  reason: collision with root package name */
    private final bw f1444b;

    bx(bv bvVar, bw bwVar) {
        this.f1443a = bvVar;
        this.f1444b = bwVar;
    }

    public final void run() {
        if (this.f1443a.c) {
            ConnectionResult connectionResult = this.f1444b.f1442b;
            if (connectionResult.a()) {
                this.f1443a.f1381a.startActivityForResult(GoogleApiActivity.a(this.f1443a.a(), connectionResult.c, this.f1444b.f1441a, false), 1);
            } else if (this.f1443a.e.a(connectionResult.f1353b)) {
                this.f1443a.e.a(this.f1443a.a(), this.f1443a.f1381a, connectionResult.f1353b, this.f1443a);
            } else if (connectionResult.f1353b == 18) {
                this.f1443a.e.a(this.f1443a.a().getApplicationContext(), new by(this, c.a(this.f1443a.a(), this.f1443a)));
            } else {
                this.f1443a.a(connectionResult, this.f1444b.f1441a);
            }
        }
    }
}
