package com.google.android.gms.common.api.internal;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.d;
import com.google.android.gms.common.api.g;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.internal.n;

public class c {

    public interface b<R> {
        void a(Status status);

        void a(R r);
    }

    public static abstract class a<R extends g, A extends a.b> extends BasePendingResult<R> implements b<R> {

        /* renamed from: a  reason: collision with root package name */
        final a.c<A> f1447a;

        /* renamed from: b  reason: collision with root package name */
        final com.google.android.gms.common.api.a<?> f1448b = null;

        @Deprecated
        protected a(a.c<A> cVar, d dVar) {
            super((d) l.a(dVar, "GoogleApiClient must not be null"));
            this.f1447a = (a.c) l.a(cVar);
        }

        /* access modifiers changed from: protected */
        public abstract void b(A a2) throws RemoteException;

        public final void a(A a2) throws DeadObjectException {
            if (a2 instanceof n) {
                a2 = ((n) a2).i;
            }
            try {
                b((a.b) a2);
            } catch (DeadObjectException e) {
                a((RemoteException) e);
                throw e;
            } catch (RemoteException e2) {
                a(e2);
            }
        }

        public final void a(Status status) {
            l.b(!status.a(), "Failed result must not be success");
            a(c(status));
        }

        private void a(RemoteException remoteException) {
            a(new Status(8, remoteException.getLocalizedMessage(), null));
        }

        public final /* bridge */ /* synthetic */ void a(Object obj) {
            super.a((g) obj);
        }
    }
}
