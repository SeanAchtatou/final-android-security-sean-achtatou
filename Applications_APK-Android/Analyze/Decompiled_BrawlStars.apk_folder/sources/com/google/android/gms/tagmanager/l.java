package com.google.android.gms.tagmanager;

import java.util.List;

final class l implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ List f2662a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ long f2663b;
    private final /* synthetic */ k c;

    l(k kVar, List list, long j) {
        this.c = kVar;
        this.f2662a = list;
        this.f2663b = j;
    }

    public final void run() {
        this.c.b(this.f2662a, this.f2663b);
    }
}
