package com.google.android.gms.common.api.internal;

import android.os.Build;
import android.os.DeadObjectException;
import android.os.RemoteException;
import android.os.TransactionTooLargeException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.d;

public abstract class ai {

    /* renamed from: a  reason: collision with root package name */
    private final int f1394a;

    public ai(int i) {
        this.f1394a = i;
    }

    public abstract void a(Status status);

    public abstract void a(d.a<?> aVar) throws DeadObjectException;

    public abstract void a(m mVar, boolean z);

    public abstract void a(RuntimeException runtimeException);

    static /* synthetic */ Status a(RemoteException remoteException) {
        StringBuilder sb = new StringBuilder();
        if ((Build.VERSION.SDK_INT >= 15) && (remoteException instanceof TransactionTooLargeException)) {
            sb.append("TransactionTooLargeException: ");
        }
        sb.append(remoteException.getLocalizedMessage());
        return new Status(8, sb.toString());
    }
}
