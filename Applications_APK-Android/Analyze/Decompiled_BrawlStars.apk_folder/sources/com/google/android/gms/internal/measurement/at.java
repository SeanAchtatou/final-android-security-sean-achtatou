package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.ar;

public interface at<U extends ar> {
    U a();

    void a(String str, int i);

    void a(String str, String str2);

    void a(String str, boolean z);
}
