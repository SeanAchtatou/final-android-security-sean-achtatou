package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.query.internal.FilterHolder;

public final class ai implements Parcelable.Creator<zzgg> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zzgg[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        String str = null;
        String[] strArr = null;
        DriveId driveId = null;
        FilterHolder filterHolder = null;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            int i = 65535 & readInt;
            if (i == 2) {
                str = SafeParcelReader.l(parcel, readInt);
            } else if (i == 3) {
                strArr = SafeParcelReader.t(parcel, readInt);
            } else if (i == 4) {
                driveId = (DriveId) SafeParcelReader.a(parcel, readInt, DriveId.CREATOR);
            } else if (i != 5) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                filterHolder = (FilterHolder) SafeParcelReader.a(parcel, readInt, FilterHolder.CREATOR);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new zzgg(str, strArr, driveId, filterHolder);
    }
}
