package com.google.android.gms.dynamic;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import com.google.android.gms.dynamic.IFragmentWrapper;

public final class SupportFragmentWrapper extends IFragmentWrapper.Stub {

    /* renamed from: a  reason: collision with root package name */
    private Fragment f1778a;

    private static SupportFragmentWrapper a(Fragment fragment) {
        if (fragment != null) {
            return new SupportFragmentWrapper(fragment);
        }
        return null;
    }

    private SupportFragmentWrapper(Fragment fragment) {
        this.f1778a = fragment;
    }

    public final IObjectWrapper a() {
        return ObjectWrapper.a(this.f1778a.getActivity());
    }

    public final Bundle b() {
        return this.f1778a.getArguments();
    }

    public final int c() {
        return this.f1778a.getId();
    }

    public final IFragmentWrapper d() {
        return a(this.f1778a.getParentFragment());
    }

    public final IObjectWrapper e() {
        return ObjectWrapper.a(this.f1778a.getResources());
    }

    public final boolean f() {
        return this.f1778a.getRetainInstance();
    }

    public final String g() {
        return this.f1778a.getTag();
    }

    public final IFragmentWrapper h() {
        return a(this.f1778a.getTargetFragment());
    }

    public final int i() {
        return this.f1778a.getTargetRequestCode();
    }

    public final boolean j() {
        return this.f1778a.getUserVisibleHint();
    }

    public final IObjectWrapper k() {
        return ObjectWrapper.a(this.f1778a.getView());
    }

    public final boolean l() {
        return this.f1778a.isAdded();
    }

    public final boolean m() {
        return this.f1778a.isDetached();
    }

    public final boolean n() {
        return this.f1778a.isHidden();
    }

    public final boolean o() {
        return this.f1778a.isInLayout();
    }

    public final boolean p() {
        return this.f1778a.isRemoving();
    }

    public final boolean q() {
        return this.f1778a.isResumed();
    }

    public final boolean r() {
        return this.f1778a.isVisible();
    }

    public final void a(IObjectWrapper iObjectWrapper) {
        this.f1778a.registerForContextMenu((View) ObjectWrapper.a(iObjectWrapper));
    }

    public final void a(boolean z) {
        this.f1778a.setHasOptionsMenu(z);
    }

    public final void b(boolean z) {
        this.f1778a.setMenuVisibility(z);
    }

    public final void c(boolean z) {
        this.f1778a.setRetainInstance(z);
    }

    public final void d(boolean z) {
        this.f1778a.setUserVisibleHint(z);
    }

    public final void a(Intent intent) {
        this.f1778a.startActivity(intent);
    }

    public final void a(Intent intent, int i) {
        this.f1778a.startActivityForResult(intent, i);
    }

    public final void b(IObjectWrapper iObjectWrapper) {
        this.f1778a.unregisterForContextMenu((View) ObjectWrapper.a(iObjectWrapper));
    }
}
