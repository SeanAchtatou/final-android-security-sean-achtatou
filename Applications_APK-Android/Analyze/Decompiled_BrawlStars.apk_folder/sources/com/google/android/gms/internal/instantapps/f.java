package com.google.android.gms.internal.instantapps;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class f implements Parcelable.Creator<zzao> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zzao[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        String[] strArr = null;
        String[] strArr2 = null;
        String[] strArr3 = null;
        String[] strArr4 = null;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            int i = 65535 & readInt;
            if (i == 1) {
                strArr = SafeParcelReader.t(parcel, readInt);
            } else if (i == 2) {
                strArr2 = SafeParcelReader.t(parcel, readInt);
            } else if (i == 3) {
                strArr4 = SafeParcelReader.t(parcel, readInt);
            } else if (i != 4) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                strArr3 = SafeParcelReader.t(parcel, readInt);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new zzao(strArr, strArr2, strArr3, strArr4);
    }
}
