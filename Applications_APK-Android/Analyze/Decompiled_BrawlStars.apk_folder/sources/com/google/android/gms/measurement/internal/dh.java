package com.google.android.gms.measurement.internal;

import java.util.ArrayList;

final class dh implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ du f2509a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ Runnable f2510b;

    dh(du duVar, Runnable runnable) {
        this.f2509a = duVar;
        this.f2510b = runnable;
    }

    public final void run() {
        this.f2509a.k();
        du duVar = this.f2509a;
        Runnable runnable = this.f2510b;
        duVar.g();
        if (duVar.c == null) {
            duVar.c = new ArrayList();
        }
        duVar.c.add(runnable);
        this.f2509a.i();
    }
}
