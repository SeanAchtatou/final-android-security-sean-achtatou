package com.google.android.gms.games.event;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.internal.zzd;
import java.util.Arrays;

public final class EventEntity extends zzd implements Event {
    public static final Parcelable.Creator<EventEntity> CREATOR = new b();

    /* renamed from: a  reason: collision with root package name */
    private final String f1801a;

    /* renamed from: b  reason: collision with root package name */
    private final String f1802b;
    private final String c;
    private final Uri d;
    private final String e;
    private final PlayerEntity f;
    private final long g;
    private final String h;
    private final boolean i;

    public EventEntity(Event event) {
        this.f1801a = event.b();
        this.f1802b = event.c();
        this.c = event.d();
        this.d = event.e();
        this.e = event.getIconImageUrl();
        this.f = (PlayerEntity) event.f().a();
        this.g = event.g();
        this.h = event.h();
        this.i = event.i();
    }

    EventEntity(String str, String str2, String str3, Uri uri, String str4, Player player, long j, String str5, boolean z) {
        this.f1801a = str;
        this.f1802b = str2;
        this.c = str3;
        this.d = uri;
        this.e = str4;
        this.f = new PlayerEntity(player);
        this.g = j;
        this.h = str5;
        this.i = z;
    }

    static boolean a(Event event, Object obj) {
        if (!(obj instanceof Event)) {
            return false;
        }
        if (event == obj) {
            return true;
        }
        Event event2 = (Event) obj;
        return j.a(event2.b(), event.b()) && j.a(event2.c(), event.c()) && j.a(event2.d(), event.d()) && j.a(event2.e(), event.e()) && j.a(event2.getIconImageUrl(), event.getIconImageUrl()) && j.a(event2.f(), event.f()) && j.a(Long.valueOf(event2.g()), Long.valueOf(event.g())) && j.a(event2.h(), event.h()) && j.a(Boolean.valueOf(event2.i()), Boolean.valueOf(event.i()));
    }

    static String b(Event event) {
        return j.a(event).a("Id", event.b()).a("Name", event.c()).a("Description", event.d()).a("IconImageUri", event.e()).a("IconImageUrl", event.getIconImageUrl()).a("Player", event.f()).a("Value", Long.valueOf(event.g())).a("FormattedValue", event.h()).a("isVisible", Boolean.valueOf(event.i())).toString();
    }

    public final /* bridge */ /* synthetic */ Object a() {
        return this;
    }

    public final String b() {
        return this.f1801a;
    }

    public final String c() {
        return this.f1802b;
    }

    public final String d() {
        return this.c;
    }

    public final Uri e() {
        return this.d;
    }

    public final boolean equals(Object obj) {
        return a(this, obj);
    }

    public final Player f() {
        return this.f;
    }

    public final long g() {
        return this.g;
    }

    public final String getIconImageUrl() {
        return this.e;
    }

    public final String h() {
        return this.h;
    }

    public final int hashCode() {
        return a(this);
    }

    public final boolean i() {
        return this.i;
    }

    public final String toString() {
        return b(this);
    }

    static int a(Event event) {
        return Arrays.hashCode(new Object[]{event.b(), event.c(), event.d(), event.e(), event.getIconImageUrl(), event.f(), Long.valueOf(event.g()), event.h(), Boolean.valueOf(event.i())});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.net.Uri, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.PlayerEntity, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = a.a(parcel, 20293);
        a.a(parcel, 1, this.f1801a, false);
        a.a(parcel, 2, this.f1802b, false);
        a.a(parcel, 3, this.c, false);
        a.a(parcel, 4, (Parcelable) this.d, i2, false);
        a.a(parcel, 5, getIconImageUrl(), false);
        a.a(parcel, 6, (Parcelable) this.f, i2, false);
        a.a(parcel, 7, this.g);
        a.a(parcel, 8, this.h, false);
        a.a(parcel, 9, this.i);
        a.b(parcel, a2);
    }
}
