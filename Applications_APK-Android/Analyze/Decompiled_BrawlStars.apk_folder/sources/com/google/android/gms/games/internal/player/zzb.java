package com.google.android.gms.games.internal.player;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.games.internal.zzd;
import java.util.Arrays;

public final class zzb extends zzd implements zza {
    public static final Parcelable.Creator<zzb> CREATOR = new a();

    /* renamed from: a  reason: collision with root package name */
    private final String f1817a;

    /* renamed from: b  reason: collision with root package name */
    private final String f1818b;
    private final long c;
    private final Uri d;
    private final Uri e;
    private final Uri f;

    public zzb(zza zza) {
        this.f1817a = zza.b();
        this.f1818b = zza.c();
        this.c = zza.d();
        this.d = zza.e();
        this.e = zza.f();
        this.f = zza.g();
    }

    zzb(String str, String str2, long j, Uri uri, Uri uri2, Uri uri3) {
        this.f1817a = str;
        this.f1818b = str2;
        this.c = j;
        this.d = uri;
        this.e = uri2;
        this.f = uri3;
    }

    static boolean a(zza zza, Object obj) {
        if (!(obj instanceof zza)) {
            return false;
        }
        if (zza == obj) {
            return true;
        }
        zza zza2 = (zza) obj;
        return j.a(zza2.b(), zza.b()) && j.a(zza2.c(), zza.c()) && j.a(Long.valueOf(zza2.d()), Long.valueOf(zza.d())) && j.a(zza2.e(), zza.e()) && j.a(zza2.f(), zza.f()) && j.a(zza2.g(), zza.g());
    }

    static String b(zza zza) {
        return j.a(zza).a("GameId", zza.b()).a("GameName", zza.c()).a("ActivityTimestampMillis", Long.valueOf(zza.d())).a("GameIconUri", zza.e()).a("GameHiResUri", zza.f()).a("GameFeaturedUri", zza.g()).toString();
    }

    public final /* bridge */ /* synthetic */ Object a() {
        return this;
    }

    public final String b() {
        return this.f1817a;
    }

    public final String c() {
        return this.f1818b;
    }

    public final long d() {
        return this.c;
    }

    public final Uri e() {
        return this.d;
    }

    public final boolean equals(Object obj) {
        return a(this, obj);
    }

    public final Uri f() {
        return this.e;
    }

    public final Uri g() {
        return this.f;
    }

    public final int hashCode() {
        return a(this);
    }

    public final String toString() {
        return b(this);
    }

    static int a(zza zza) {
        return Arrays.hashCode(new Object[]{zza.b(), zza.c(), Long.valueOf(zza.d()), zza.e(), zza.f(), zza.g()});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.net.Uri, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.a(parcel, 1, this.f1817a, false);
        a.a(parcel, 2, this.f1818b, false);
        a.a(parcel, 3, this.c);
        a.a(parcel, 4, (Parcelable) this.d, i, false);
        a.a(parcel, 5, (Parcelable) this.e, i, false);
        a.a(parcel, 6, (Parcelable) this.f, i, false);
        a.b(parcel, a2);
    }
}
