package com.google.android.gms.common.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.a;

@Deprecated
public final class zzr extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzr> CREATOR = new ah();

    /* renamed from: a  reason: collision with root package name */
    private final int f1625a;

    zzr(int i) {
        this.f1625a = i;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.b(parcel, 1, this.f1625a);
        a.b(parcel, a2);
    }
}
