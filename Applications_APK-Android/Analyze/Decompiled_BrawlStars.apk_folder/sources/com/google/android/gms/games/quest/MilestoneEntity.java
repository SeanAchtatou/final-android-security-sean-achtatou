package com.google.android.gms.games.quest;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.games.internal.zzd;
import java.util.Arrays;

@Deprecated
public final class MilestoneEntity extends zzd implements Milestone {
    public static final Parcelable.Creator<MilestoneEntity> CREATOR = new b();

    /* renamed from: a  reason: collision with root package name */
    private final String f1835a;

    /* renamed from: b  reason: collision with root package name */
    private final long f1836b;
    private final long c;
    private final byte[] d;
    private final int e;
    private final String f;

    public MilestoneEntity(Milestone milestone) {
        this.f1835a = milestone.b();
        this.f1836b = milestone.c();
        this.c = milestone.f();
        this.e = milestone.e();
        this.f = milestone.d();
        byte[] g = milestone.g();
        if (g == null) {
            this.d = null;
            return;
        }
        this.d = new byte[g.length];
        System.arraycopy(g, 0, this.d, 0, g.length);
    }

    MilestoneEntity(String str, long j, long j2, byte[] bArr, int i, String str2) {
        this.f1835a = str;
        this.f1836b = j;
        this.c = j2;
        this.d = bArr;
        this.e = i;
        this.f = str2;
    }

    static boolean a(Milestone milestone, Object obj) {
        if (!(obj instanceof Milestone)) {
            return false;
        }
        if (milestone == obj) {
            return true;
        }
        Milestone milestone2 = (Milestone) obj;
        return j.a(milestone2.b(), milestone.b()) && j.a(Long.valueOf(milestone2.c()), Long.valueOf(milestone.c())) && j.a(Long.valueOf(milestone2.f()), Long.valueOf(milestone.f())) && j.a(Integer.valueOf(milestone2.e()), Integer.valueOf(milestone.e())) && j.a(milestone2.d(), milestone.d());
    }

    static String b(Milestone milestone) {
        return j.a(milestone).a("MilestoneId", milestone.b()).a("CurrentProgress", Long.valueOf(milestone.c())).a("TargetProgress", Long.valueOf(milestone.f())).a("State", Integer.valueOf(milestone.e())).a("CompletionRewardData", milestone.g()).a("EventId", milestone.d()).toString();
    }

    public final /* bridge */ /* synthetic */ Object a() {
        return this;
    }

    public final String b() {
        return this.f1835a;
    }

    public final long c() {
        return this.f1836b;
    }

    public final String d() {
        return this.f;
    }

    public final int e() {
        return this.e;
    }

    public final boolean equals(Object obj) {
        return a(this, obj);
    }

    public final long f() {
        return this.c;
    }

    public final byte[] g() {
        return this.d;
    }

    public final int hashCode() {
        return a(this);
    }

    public final String toString() {
        return b(this);
    }

    static int a(Milestone milestone) {
        return Arrays.hashCode(new Object[]{milestone.b(), Long.valueOf(milestone.c()), Long.valueOf(milestone.f()), Integer.valueOf(milestone.e()), milestone.d()});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
     arg types: [android.os.Parcel, int, byte[], int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.a(parcel, 1, this.f1835a, false);
        a.a(parcel, 2, this.f1836b);
        a.a(parcel, 3, this.c);
        a.a(parcel, 4, this.d, false);
        a.b(parcel, 5, this.e);
        a.a(parcel, 6, this.f, false);
        a.b(parcel, a2);
    }
}
