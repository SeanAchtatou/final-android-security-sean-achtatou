package com.google.android.gms.common.internal;

import android.content.Context;
import android.util.SparseIntArray;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.c;
import com.google.android.gms.common.d;

public class i {

    /* renamed from: a  reason: collision with root package name */
    public final SparseIntArray f1609a;

    /* renamed from: b  reason: collision with root package name */
    private d f1610b;

    public i() {
        this(c.a());
    }

    public i(d dVar) {
        this.f1609a = new SparseIntArray();
        l.a(dVar);
        this.f1610b = dVar;
    }

    public final int a(Context context, a.f fVar) {
        l.a(context);
        l.a(fVar);
        int g = fVar.g();
        int i = this.f1609a.get(g, -1);
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        while (true) {
            if (i2 < this.f1609a.size()) {
                int keyAt = this.f1609a.keyAt(i2);
                if (keyAt > g && this.f1609a.get(keyAt) == 0) {
                    i = 0;
                    break;
                }
                i2++;
            } else {
                break;
            }
        }
        if (i == -1) {
            i = this.f1610b.a(context, g);
        }
        this.f1609a.put(g, i);
        return i;
    }
}
