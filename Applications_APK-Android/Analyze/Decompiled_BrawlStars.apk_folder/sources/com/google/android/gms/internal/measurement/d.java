package com.google.android.gms.internal.measurement;

import com.google.android.gms.analytics.a.a;
import com.google.android.gms.analytics.a.b;
import com.google.android.gms.analytics.a.c;
import com.google.android.gms.analytics.l;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class d extends l<d> {

    /* renamed from: a  reason: collision with root package name */
    public final List<a> f2149a = new ArrayList();

    /* renamed from: b  reason: collision with root package name */
    public final List<c> f2150b = new ArrayList();
    public final Map<String, List<a>> c = new HashMap();
    public b d;

    public final String toString() {
        HashMap hashMap = new HashMap();
        if (!this.f2149a.isEmpty()) {
            hashMap.put("products", this.f2149a);
        }
        if (!this.f2150b.isEmpty()) {
            hashMap.put("promotions", this.f2150b);
        }
        if (!this.c.isEmpty()) {
            hashMap.put("impressions", this.c);
        }
        hashMap.put("productAction", this.d);
        return a((Object) hashMap);
    }

    public final /* synthetic */ void a(l lVar) {
        d dVar = (d) lVar;
        dVar.f2149a.addAll(this.f2149a);
        dVar.f2150b.addAll(this.f2150b);
        for (Map.Entry next : this.c.entrySet()) {
            String str = (String) next.getKey();
            for (a aVar : (List) next.getValue()) {
                if (aVar != null) {
                    String str2 = str == null ? "" : str;
                    if (!dVar.c.containsKey(str2)) {
                        dVar.c.put(str2, new ArrayList());
                    }
                    dVar.c.get(str2).add(aVar);
                }
            }
        }
        b bVar = this.d;
        if (bVar != null) {
            dVar.d = bVar;
        }
    }
}
