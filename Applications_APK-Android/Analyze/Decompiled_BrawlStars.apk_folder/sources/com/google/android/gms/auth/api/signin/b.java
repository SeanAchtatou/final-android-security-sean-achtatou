package com.google.android.gms.auth.api.signin;

import com.google.android.gms.common.api.Scope;
import java.util.Comparator;

final class b implements Comparator<Scope> {
    b() {
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        return ((Scope) obj).f1366a.compareTo(((Scope) obj2).f1366a);
    }
}
