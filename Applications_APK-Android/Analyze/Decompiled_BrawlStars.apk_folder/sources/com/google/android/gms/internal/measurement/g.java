package com.google.android.gms.internal.measurement;

import android.text.TextUtils;
import com.google.android.gms.analytics.l;
import java.util.HashMap;

public final class g extends l<g> {

    /* renamed from: a  reason: collision with root package name */
    public String f2231a;

    /* renamed from: b  reason: collision with root package name */
    public String f2232b;
    public String c;
    public String d;
    public boolean e;
    public String f;
    public boolean g;
    public double h;

    public final String toString() {
        HashMap hashMap = new HashMap();
        hashMap.put("hitType", this.f2231a);
        hashMap.put("clientId", this.f2232b);
        hashMap.put("userId", this.c);
        hashMap.put("androidAdId", this.d);
        hashMap.put("AdTargetingEnabled", Boolean.valueOf(this.e));
        hashMap.put("sessionControl", this.f);
        hashMap.put("nonInteraction", Boolean.valueOf(this.g));
        hashMap.put("sampleRate", Double.valueOf(this.h));
        return a((Object) hashMap);
    }

    public final /* synthetic */ void a(l lVar) {
        g gVar = (g) lVar;
        if (!TextUtils.isEmpty(this.f2231a)) {
            gVar.f2231a = this.f2231a;
        }
        if (!TextUtils.isEmpty(this.f2232b)) {
            gVar.f2232b = this.f2232b;
        }
        if (!TextUtils.isEmpty(this.c)) {
            gVar.c = this.c;
        }
        if (!TextUtils.isEmpty(this.d)) {
            gVar.d = this.d;
        }
        boolean z = true;
        if (this.e) {
            gVar.e = true;
        }
        if (!TextUtils.isEmpty(this.f)) {
            gVar.f = this.f;
        }
        boolean z2 = this.g;
        if (z2) {
            gVar.g = z2;
        }
        double d2 = this.h;
        if (d2 != 0.0d) {
            if (d2 < 0.0d || d2 > 100.0d) {
                z = false;
            }
            com.google.android.gms.common.internal.l.b(z, "Sample rate must be between 0% and 100%");
            gVar.h = d2;
        }
    }
}
