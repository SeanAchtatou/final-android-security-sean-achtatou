package com.google.android.gms.analytics;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.google.android.gms.internal.measurement.bq;

public final class AnalyticsReceiver extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private bq f1309a;

    public final void onReceive(Context context, Intent intent) {
        if (this.f1309a == null) {
            this.f1309a = new bq();
        }
        bq.a(context, intent);
    }
}
