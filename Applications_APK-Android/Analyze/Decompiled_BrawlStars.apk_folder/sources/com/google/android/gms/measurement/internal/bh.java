package com.google.android.gms.measurement.internal;

final class bh implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zzfv f2421a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ zzk f2422b;
    private final /* synthetic */ zzby c;

    bh(zzby zzby, zzfv zzfv, zzk zzk) {
        this.c = zzby;
        this.f2421a = zzfv;
        this.f2422b = zzk;
    }

    public final void run() {
        this.c.f2597a.k();
        this.c.f2597a.b(this.f2421a, this.f2422b);
    }
}
