package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.h;

public final class zzhl extends zzhb<Void> {
    public final void a() throws RemoteException {
        Status status = Status.f1368a;
        h<T> hVar = this.f1976a;
        if (status.a()) {
            hVar.a((Boolean) null);
        } else {
            hVar.a(new ApiException(status));
        }
    }
}
