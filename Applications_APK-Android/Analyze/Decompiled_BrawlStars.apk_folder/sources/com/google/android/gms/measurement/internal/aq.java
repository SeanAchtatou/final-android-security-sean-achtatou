package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.l;
import java.util.concurrent.BlockingQueue;

final class aq extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private final Object f2385a = new Object();

    /* renamed from: b  reason: collision with root package name */
    private final BlockingQueue<ap<?>> f2386b;
    private final /* synthetic */ am c;

    public aq(am amVar, String str, BlockingQueue<ap<?>> blockingQueue) {
        this.c = amVar;
        l.a((Object) str);
        l.a(blockingQueue);
        this.f2386b = blockingQueue;
        setName(str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0064, code lost:
        r0 = r5.c.g;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x006a, code lost:
        monitor-enter(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
        r5.c.h.release();
        r5.c.g.notifyAll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0083, code lost:
        if (r5 != r5.c.f2379a) goto L_0x008b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0085, code lost:
        com.google.android.gms.measurement.internal.aq unused = r5.c.f2379a = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0091, code lost:
        if (r5 != r5.c.f2380b) goto L_0x0099;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0093, code lost:
        com.google.android.gms.measurement.internal.aq unused = r5.c.f2380b = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0099, code lost:
        r5.c.q().c.a("Current scheduler thread is neither worker nor network");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00a6, code lost:
        monitor-exit(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00a7, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r5 = this;
            r0 = 0
        L_0x0001:
            if (r0 != 0) goto L_0x0013
            com.google.android.gms.measurement.internal.am r1 = r5.c     // Catch:{ InterruptedException -> 0x000e }
            java.util.concurrent.Semaphore r1 = r1.h     // Catch:{ InterruptedException -> 0x000e }
            r1.acquire()     // Catch:{ InterruptedException -> 0x000e }
            r0 = 1
            goto L_0x0001
        L_0x000e:
            r1 = move-exception
            r5.a(r1)
            goto L_0x0001
        L_0x0013:
            int r0 = android.os.Process.myTid()     // Catch:{ all -> 0x00b4 }
            int r0 = android.os.Process.getThreadPriority(r0)     // Catch:{ all -> 0x00b4 }
        L_0x001b:
            java.util.concurrent.BlockingQueue<com.google.android.gms.measurement.internal.ap<?>> r1 = r5.f2386b     // Catch:{ all -> 0x00b4 }
            java.lang.Object r1 = r1.poll()     // Catch:{ all -> 0x00b4 }
            com.google.android.gms.measurement.internal.ap r1 = (com.google.android.gms.measurement.internal.ap) r1     // Catch:{ all -> 0x00b4 }
            if (r1 == 0) goto L_0x0034
            boolean r2 = r1.f2383a     // Catch:{ all -> 0x00b4 }
            if (r2 == 0) goto L_0x002b
            r2 = r0
            goto L_0x002d
        L_0x002b:
            r2 = 10
        L_0x002d:
            android.os.Process.setThreadPriority(r2)     // Catch:{ all -> 0x00b4 }
            r1.run()     // Catch:{ all -> 0x00b4 }
            goto L_0x001b
        L_0x0034:
            java.lang.Object r1 = r5.f2385a     // Catch:{ all -> 0x00b4 }
            monitor-enter(r1)     // Catch:{ all -> 0x00b4 }
            java.util.concurrent.BlockingQueue<com.google.android.gms.measurement.internal.ap<?>> r2 = r5.f2386b     // Catch:{ all -> 0x00b1 }
            java.lang.Object r2 = r2.peek()     // Catch:{ all -> 0x00b1 }
            if (r2 != 0) goto L_0x0053
            com.google.android.gms.measurement.internal.am r2 = r5.c     // Catch:{ all -> 0x00b1 }
            boolean r2 = r2.i     // Catch:{ all -> 0x00b1 }
            if (r2 != 0) goto L_0x0053
            java.lang.Object r2 = r5.f2385a     // Catch:{ InterruptedException -> 0x004f }
            r3 = 30000(0x7530, double:1.4822E-319)
            r2.wait(r3)     // Catch:{ InterruptedException -> 0x004f }
            goto L_0x0053
        L_0x004f:
            r2 = move-exception
            r5.a(r2)     // Catch:{ all -> 0x00b1 }
        L_0x0053:
            monitor-exit(r1)     // Catch:{ all -> 0x00b1 }
            com.google.android.gms.measurement.internal.am r1 = r5.c     // Catch:{ all -> 0x00b4 }
            java.lang.Object r1 = r1.g     // Catch:{ all -> 0x00b4 }
            monitor-enter(r1)     // Catch:{ all -> 0x00b4 }
            java.util.concurrent.BlockingQueue<com.google.android.gms.measurement.internal.ap<?>> r2 = r5.f2386b     // Catch:{ all -> 0x00ae }
            java.lang.Object r2 = r2.peek()     // Catch:{ all -> 0x00ae }
            if (r2 != 0) goto L_0x00ab
            monitor-exit(r1)     // Catch:{ all -> 0x00ae }
            com.google.android.gms.measurement.internal.am r0 = r5.c
            java.lang.Object r0 = r0.g
            monitor-enter(r0)
            com.google.android.gms.measurement.internal.am r1 = r5.c     // Catch:{ all -> 0x00a8 }
            java.util.concurrent.Semaphore r1 = r1.h     // Catch:{ all -> 0x00a8 }
            r1.release()     // Catch:{ all -> 0x00a8 }
            com.google.android.gms.measurement.internal.am r1 = r5.c     // Catch:{ all -> 0x00a8 }
            java.lang.Object r1 = r1.g     // Catch:{ all -> 0x00a8 }
            r1.notifyAll()     // Catch:{ all -> 0x00a8 }
            com.google.android.gms.measurement.internal.am r1 = r5.c     // Catch:{ all -> 0x00a8 }
            com.google.android.gms.measurement.internal.aq r1 = r1.f2379a     // Catch:{ all -> 0x00a8 }
            if (r5 != r1) goto L_0x008b
            com.google.android.gms.measurement.internal.am r1 = r5.c     // Catch:{ all -> 0x00a8 }
            com.google.android.gms.measurement.internal.aq unused = r1.f2379a = null     // Catch:{ all -> 0x00a8 }
            goto L_0x00a6
        L_0x008b:
            com.google.android.gms.measurement.internal.am r1 = r5.c     // Catch:{ all -> 0x00a8 }
            com.google.android.gms.measurement.internal.aq r1 = r1.f2380b     // Catch:{ all -> 0x00a8 }
            if (r5 != r1) goto L_0x0099
            com.google.android.gms.measurement.internal.am r1 = r5.c     // Catch:{ all -> 0x00a8 }
            com.google.android.gms.measurement.internal.aq unused = r1.f2380b = null     // Catch:{ all -> 0x00a8 }
            goto L_0x00a6
        L_0x0099:
            com.google.android.gms.measurement.internal.am r1 = r5.c     // Catch:{ all -> 0x00a8 }
            com.google.android.gms.measurement.internal.o r1 = r1.q()     // Catch:{ all -> 0x00a8 }
            com.google.android.gms.measurement.internal.q r1 = r1.c     // Catch:{ all -> 0x00a8 }
            java.lang.String r2 = "Current scheduler thread is neither worker nor network"
            r1.a(r2)     // Catch:{ all -> 0x00a8 }
        L_0x00a6:
            monitor-exit(r0)     // Catch:{ all -> 0x00a8 }
            return
        L_0x00a8:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00a8 }
            throw r1
        L_0x00ab:
            monitor-exit(r1)     // Catch:{ all -> 0x00ae }
            goto L_0x001b
        L_0x00ae:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00ae }
            throw r0     // Catch:{ all -> 0x00b4 }
        L_0x00b1:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00b1 }
            throw r0     // Catch:{ all -> 0x00b4 }
        L_0x00b4:
            r0 = move-exception
            com.google.android.gms.measurement.internal.am r1 = r5.c
            java.lang.Object r1 = r1.g
            monitor-enter(r1)
            com.google.android.gms.measurement.internal.am r2 = r5.c     // Catch:{ all -> 0x00f9 }
            java.util.concurrent.Semaphore r2 = r2.h     // Catch:{ all -> 0x00f9 }
            r2.release()     // Catch:{ all -> 0x00f9 }
            com.google.android.gms.measurement.internal.am r2 = r5.c     // Catch:{ all -> 0x00f9 }
            java.lang.Object r2 = r2.g     // Catch:{ all -> 0x00f9 }
            r2.notifyAll()     // Catch:{ all -> 0x00f9 }
            com.google.android.gms.measurement.internal.am r2 = r5.c     // Catch:{ all -> 0x00f9 }
            com.google.android.gms.measurement.internal.aq r2 = r2.f2379a     // Catch:{ all -> 0x00f9 }
            if (r5 == r2) goto L_0x00f2
            com.google.android.gms.measurement.internal.am r2 = r5.c     // Catch:{ all -> 0x00f9 }
            com.google.android.gms.measurement.internal.aq r2 = r2.f2380b     // Catch:{ all -> 0x00f9 }
            if (r5 != r2) goto L_0x00e4
            com.google.android.gms.measurement.internal.am r2 = r5.c     // Catch:{ all -> 0x00f9 }
            com.google.android.gms.measurement.internal.aq unused = r2.f2380b = null     // Catch:{ all -> 0x00f9 }
            goto L_0x00f7
        L_0x00e4:
            com.google.android.gms.measurement.internal.am r2 = r5.c     // Catch:{ all -> 0x00f9 }
            com.google.android.gms.measurement.internal.o r2 = r2.q()     // Catch:{ all -> 0x00f9 }
            com.google.android.gms.measurement.internal.q r2 = r2.c     // Catch:{ all -> 0x00f9 }
            java.lang.String r3 = "Current scheduler thread is neither worker nor network"
            r2.a(r3)     // Catch:{ all -> 0x00f9 }
            goto L_0x00f7
        L_0x00f2:
            com.google.android.gms.measurement.internal.am r2 = r5.c     // Catch:{ all -> 0x00f9 }
            com.google.android.gms.measurement.internal.aq unused = r2.f2379a = null     // Catch:{ all -> 0x00f9 }
        L_0x00f7:
            monitor-exit(r1)     // Catch:{ all -> 0x00f9 }
            throw r0
        L_0x00f9:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00f9 }
            goto L_0x00fd
        L_0x00fc:
            throw r0
        L_0x00fd:
            goto L_0x00fc
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.aq.run():void");
    }

    public final void a() {
        synchronized (this.f2385a) {
            this.f2385a.notifyAll();
        }
    }

    private final void a(InterruptedException interruptedException) {
        this.c.q().f.a(String.valueOf(getName()).concat(" was interrupted"), interruptedException);
    }
}
