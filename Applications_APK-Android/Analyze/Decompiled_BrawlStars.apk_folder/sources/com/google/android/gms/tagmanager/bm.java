package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.measurement.da;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

final class bm {
    static al<da> a(al<da> alVar, int... iArr) {
        for (int i : iArr) {
            if (!(bk.c((da) alVar.f2622a) instanceof String)) {
                ab.a("Escaping can only be applied to strings.");
            } else if (i != 12) {
                StringBuilder sb = new StringBuilder(39);
                sb.append("Unsupported Value Escaping: ");
                sb.append(i);
                ab.a(sb.toString());
            } else {
                alVar = a(alVar);
            }
        }
        return alVar;
    }

    private static al<da> a(al<da> alVar) {
        try {
            return new al<>(bk.a(URLEncoder.encode(bk.a((da) alVar.f2622a), "UTF-8").replaceAll("\\+", "%20")), alVar.f2623b);
        } catch (UnsupportedEncodingException e) {
            ab.a("Escape URI: unsupported encoding", e);
            return alVar;
        }
    }
}
