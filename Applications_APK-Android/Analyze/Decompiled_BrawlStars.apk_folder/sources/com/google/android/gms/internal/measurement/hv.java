package com.google.android.gms.internal.measurement;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;

class hv extends AbstractSet<Map.Entry<K, V>> {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ hm f2276a;

    private hv(hm hmVar) {
        this.f2276a = hmVar;
    }

    public Iterator<Map.Entry<K, V>> iterator() {
        return new hu(this.f2276a, (byte) 0);
    }

    public int size() {
        return this.f2276a.size();
    }

    public boolean contains(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        Object obj2 = this.f2276a.get(entry.getKey());
        Object value = entry.getValue();
        if (obj2 != value) {
            return obj2 != null && obj2.equals(value);
        }
        return true;
    }

    public boolean remove(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        if (!contains(entry)) {
            return false;
        }
        this.f2276a.remove(entry.getKey());
        return true;
    }

    public void clear() {
        this.f2276a.clear();
    }

    public /* synthetic */ boolean add(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        if (contains(entry)) {
            return false;
        }
        this.f2276a.put((Comparable) entry.getKey(), entry.getValue());
        return true;
    }

    /* synthetic */ hv(hm hmVar, byte b2) {
        this(hmVar);
    }
}
