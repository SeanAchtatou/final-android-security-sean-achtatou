package com.google.android.gms.auth.api.signin.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.internal.l;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import javax.annotation.Nullable;
import org.json.JSONException;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private static final Lock f1350a = new ReentrantLock();

    /* renamed from: b  reason: collision with root package name */
    private static b f1351b;
    private final Lock c = new ReentrantLock();
    private final SharedPreferences d;

    public static b a(Context context) {
        l.a(context);
        f1350a.lock();
        try {
            if (f1351b == null) {
                f1351b = new b(context.getApplicationContext());
            }
            return f1351b;
        } finally {
            f1350a.unlock();
        }
    }

    private b(Context context) {
        this.d = context.getSharedPreferences("com.google.android.gms.signin", 0);
    }

    @Nullable
    public final GoogleSignInAccount a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        StringBuilder sb = new StringBuilder("googleSignInAccount".length() + 1 + String.valueOf(str).length());
        sb.append("googleSignInAccount");
        sb.append(":");
        sb.append(str);
        String b2 = b(sb.toString());
        if (b2 != null) {
            try {
                return GoogleSignInAccount.a(b2);
            } catch (JSONException unused) {
            }
        }
        return null;
    }

    @Nullable
    public final String b(String str) {
        this.c.lock();
        try {
            return this.d.getString(str, null);
        } finally {
            this.c.unlock();
        }
    }
}
