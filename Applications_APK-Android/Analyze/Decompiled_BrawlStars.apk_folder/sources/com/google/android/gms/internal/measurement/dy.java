package com.google.android.gms.internal.measurement;

import android.net.Uri;

public final class dy {

    /* renamed from: a  reason: collision with root package name */
    final Uri f2172a;

    /* renamed from: b  reason: collision with root package name */
    final String f2173b;
    final String c;
    private final String d;
    private final boolean e;
    private final boolean f;
    private final boolean g;

    public dy(Uri uri) {
        this(uri, "", "");
    }

    private dy(Uri uri, String str, String str2) {
        this.d = null;
        this.f2172a = uri;
        this.f2173b = str;
        this.c = str2;
        this.e = false;
        this.f = false;
        this.g = false;
    }
}
