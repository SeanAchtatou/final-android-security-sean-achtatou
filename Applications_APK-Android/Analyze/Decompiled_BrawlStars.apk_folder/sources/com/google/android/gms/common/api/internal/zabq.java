package com.google.android.gms.common.api.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public final class zabq extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    public Context f1506a;

    /* renamed from: b  reason: collision with root package name */
    private final ay f1507b;

    public zabq(ay ayVar) {
        this.f1507b = ayVar;
    }

    public final synchronized void a() {
        if (this.f1506a != null) {
            this.f1506a.unregisterReceiver(this);
        }
        this.f1506a = null;
    }

    public final void onReceive(Context context, Intent intent) {
        Uri data = intent.getData();
        if ("com.google.android.gms".equals(data != null ? data.getSchemeSpecificPart() : null)) {
            this.f1507b.a();
            a();
        }
    }
}
