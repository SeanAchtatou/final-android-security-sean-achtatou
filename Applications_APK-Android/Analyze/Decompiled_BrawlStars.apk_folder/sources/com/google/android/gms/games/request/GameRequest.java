package com.google.android.gms.games.request;

import android.os.Parcelable;
import com.google.android.gms.common.data.e;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.Player;
import java.util.List;

@Deprecated
public interface GameRequest extends Parcelable, e<GameRequest> {
    int a(String str);

    String b();

    Game c();

    Player d();

    List<Player> e();

    byte[] f();

    int g();

    long h();

    long i();

    int j();
}
