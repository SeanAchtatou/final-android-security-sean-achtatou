package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public final class ab implements Parcelable.Creator<zzfs> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zzfs[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        MetadataBundle metadataBundle = null;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            if ((65535 & readInt) != 2) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                metadataBundle = (MetadataBundle) SafeParcelReader.a(parcel, readInt, MetadataBundle.CREATOR);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new zzfs(metadataBundle);
    }
}
