package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.BaseGmsClient;

final class z extends an {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ BaseGmsClient.c f1505a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    z(al alVar, BaseGmsClient.c cVar) {
        super(alVar);
        this.f1505a = cVar;
    }

    public final void a() {
        this.f1505a.a(new ConnectionResult(16, null));
    }
}
