package com.google.android.gms.analytics;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Process;
import android.text.TextUtils;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.internal.measurement.hy;
import java.lang.Thread;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public final class n {
    private static volatile n d;

    /* renamed from: a  reason: collision with root package name */
    public final Context f1331a;

    /* renamed from: b  reason: collision with root package name */
    final a f1332b = new a();
    public Thread.UncaughtExceptionHandler c;
    /* access modifiers changed from: private */
    public final List<Object> e = new CopyOnWriteArrayList();
    private final i f = new i();
    private volatile hy g;

    static class b implements ThreadFactory {

        /* renamed from: a  reason: collision with root package name */
        private static final AtomicInteger f1334a = new AtomicInteger();

        private b() {
        }

        public final Thread newThread(Runnable runnable) {
            int incrementAndGet = f1334a.incrementAndGet();
            StringBuilder sb = new StringBuilder(23);
            sb.append("measurement-");
            sb.append(incrementAndGet);
            return new c(runnable, sb.toString());
        }

        /* synthetic */ b(byte b2) {
            this();
        }
    }

    private n(Context context) {
        Context applicationContext = context.getApplicationContext();
        l.a(applicationContext);
        this.f1331a = applicationContext;
    }

    static class c extends Thread {
        c(Runnable runnable, String str) {
            super(runnable, str);
        }

        public final void run() {
            Process.setThreadPriority(10);
            super.run();
        }
    }

    class a extends ThreadPoolExecutor {
        public a() {
            super(1, 1, 1, TimeUnit.MINUTES, new LinkedBlockingQueue());
            setThreadFactory(new b((byte) 0));
            allowCoreThreadTimeOut(true);
        }

        /* access modifiers changed from: protected */
        public final <T> RunnableFuture<T> newTaskFor(Runnable runnable, T t) {
            return new p(this, runnable, t);
        }
    }

    public static n a(Context context) {
        l.a(context);
        if (d == null) {
            synchronized (n.class) {
                if (d == null) {
                    d = new n(context);
                }
            }
        }
        return d;
    }

    public final hy a() {
        if (this.g == null) {
            synchronized (this) {
                if (this.g == null) {
                    hy hyVar = new hy();
                    PackageManager packageManager = this.f1331a.getPackageManager();
                    String packageName = this.f1331a.getPackageName();
                    hyVar.c = packageName;
                    hyVar.d = packageManager.getInstallerPackageName(packageName);
                    String str = null;
                    try {
                        PackageInfo packageInfo = packageManager.getPackageInfo(this.f1331a.getPackageName(), 0);
                        if (packageInfo != null) {
                            CharSequence applicationLabel = packageManager.getApplicationLabel(packageInfo.applicationInfo);
                            if (!TextUtils.isEmpty(applicationLabel)) {
                                packageName = applicationLabel.toString();
                            }
                            str = packageInfo.versionName;
                        }
                    } catch (PackageManager.NameNotFoundException unused) {
                        String valueOf = String.valueOf(packageName);
                        if (valueOf.length() != 0) {
                            "Error retrieving package info: appName set to ".concat(valueOf);
                        } else {
                            new String("Error retrieving package info: appName set to ");
                        }
                    }
                    hyVar.f2277a = packageName;
                    hyVar.f2278b = str;
                    this.g = hyVar;
                }
            }
        }
        return this.g;
    }

    public static void b() {
        if (!(Thread.currentThread() instanceof c)) {
            throw new IllegalStateException("Call expected from worker thread");
        }
    }

    public final <V> Future<V> a(Callable callable) {
        l.a(callable);
        if (!(Thread.currentThread() instanceof c)) {
            return this.f1332b.submit(callable);
        }
        FutureTask futureTask = new FutureTask(callable);
        futureTask.run();
        return futureTask;
    }

    public final void a(Runnable runnable) {
        l.a(runnable);
        this.f1332b.submit(runnable);
    }

    static /* synthetic */ void a(k kVar) {
        l.c("deliver should be called from worker thread");
        l.b(kVar.f1329b, "Measurement must be submitted");
        List<q> list = kVar.g;
        if (!list.isEmpty()) {
            HashSet hashSet = new HashSet();
            for (q next : list) {
                Uri a2 = next.a();
                if (!hashSet.contains(a2)) {
                    hashSet.add(a2);
                    next.a(kVar);
                }
            }
        }
    }
}
