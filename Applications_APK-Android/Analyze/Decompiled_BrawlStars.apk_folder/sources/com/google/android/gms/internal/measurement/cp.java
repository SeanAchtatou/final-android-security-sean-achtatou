package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class cp extends iz<cp> {

    /* renamed from: a  reason: collision with root package name */
    public Long f2128a = null;

    /* renamed from: b  reason: collision with root package name */
    public String f2129b = null;
    public cq[] c = cq.a();
    public co[] d = co.a();
    public ci[] e = ci.a();
    private Integer f = null;
    private String g = null;
    private Boolean h = null;

    public cp() {
        this.L = null;
        this.M = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof cp)) {
            return false;
        }
        cp cpVar = (cp) obj;
        Long l = this.f2128a;
        if (l == null) {
            if (cpVar.f2128a != null) {
                return false;
            }
        } else if (!l.equals(cpVar.f2128a)) {
            return false;
        }
        String str = this.f2129b;
        if (str == null) {
            if (cpVar.f2129b != null) {
                return false;
            }
        } else if (!str.equals(cpVar.f2129b)) {
            return false;
        }
        Integer num = this.f;
        if (num == null) {
            if (cpVar.f != null) {
                return false;
            }
        } else if (!num.equals(cpVar.f)) {
            return false;
        }
        if (!jd.a(this.c, cpVar.c) || !jd.a(this.d, cpVar.d) || !jd.a(this.e, cpVar.e)) {
            return false;
        }
        String str2 = this.g;
        if (str2 == null) {
            if (cpVar.g != null) {
                return false;
            }
        } else if (!str2.equals(cpVar.g)) {
            return false;
        }
        Boolean bool = this.h;
        if (bool == null) {
            if (cpVar.h != null) {
                return false;
            }
        } else if (!bool.equals(cpVar.h)) {
            return false;
        }
        if (this.L == null || this.L.a()) {
            return cpVar.L == null || cpVar.L.a();
        }
        return this.L.equals(cpVar.L);
    }

    public final int hashCode() {
        int hashCode = (getClass().getName().hashCode() + 527) * 31;
        Long l = this.f2128a;
        int i = 0;
        int hashCode2 = (hashCode + (l == null ? 0 : l.hashCode())) * 31;
        String str = this.f2129b;
        int hashCode3 = (hashCode2 + (str == null ? 0 : str.hashCode())) * 31;
        Integer num = this.f;
        int hashCode4 = (((((((hashCode3 + (num == null ? 0 : num.hashCode())) * 31) + jd.a(this.c)) * 31) + jd.a(this.d)) * 31) + jd.a(this.e)) * 31;
        String str2 = this.g;
        int hashCode5 = (hashCode4 + (str2 == null ? 0 : str2.hashCode())) * 31;
        Boolean bool = this.h;
        int hashCode6 = (hashCode5 + (bool == null ? 0 : bool.hashCode())) * 31;
        if (this.L != null && !this.L.a()) {
            i = this.L.hashCode();
        }
        return hashCode6 + i;
    }

    public final void a(iy iyVar) throws IOException {
        Long l = this.f2128a;
        if (l != null) {
            iyVar.b(1, l.longValue());
        }
        String str = this.f2129b;
        if (str != null) {
            iyVar.a(2, str);
        }
        Integer num = this.f;
        if (num != null) {
            iyVar.a(3, num.intValue());
        }
        cq[] cqVarArr = this.c;
        int i = 0;
        if (cqVarArr != null && cqVarArr.length > 0) {
            int i2 = 0;
            while (true) {
                cq[] cqVarArr2 = this.c;
                if (i2 >= cqVarArr2.length) {
                    break;
                }
                cq cqVar = cqVarArr2[i2];
                if (cqVar != null) {
                    iyVar.a(4, cqVar);
                }
                i2++;
            }
        }
        co[] coVarArr = this.d;
        if (coVarArr != null && coVarArr.length > 0) {
            int i3 = 0;
            while (true) {
                co[] coVarArr2 = this.d;
                if (i3 >= coVarArr2.length) {
                    break;
                }
                co coVar = coVarArr2[i3];
                if (coVar != null) {
                    iyVar.a(5, coVar);
                }
                i3++;
            }
        }
        ci[] ciVarArr = this.e;
        if (ciVarArr != null && ciVarArr.length > 0) {
            while (true) {
                ci[] ciVarArr2 = this.e;
                if (i >= ciVarArr2.length) {
                    break;
                }
                ci ciVar = ciVarArr2[i];
                if (ciVar != null) {
                    iyVar.a(6, ciVar);
                }
                i++;
            }
        }
        String str2 = this.g;
        if (str2 != null) {
            iyVar.a(7, str2);
        }
        Boolean bool = this.h;
        if (bool != null) {
            iyVar.a(8, bool.booleanValue());
        }
        super.a(iyVar);
    }

    /* access modifiers changed from: protected */
    public final int b() {
        int b2 = super.b();
        Long l = this.f2128a;
        if (l != null) {
            b2 += iy.c(1, l.longValue());
        }
        String str = this.f2129b;
        if (str != null) {
            b2 += iy.b(2, str);
        }
        Integer num = this.f;
        if (num != null) {
            b2 += iy.b(3, num.intValue());
        }
        cq[] cqVarArr = this.c;
        int i = 0;
        if (cqVarArr != null && cqVarArr.length > 0) {
            int i2 = b2;
            int i3 = 0;
            while (true) {
                cq[] cqVarArr2 = this.c;
                if (i3 >= cqVarArr2.length) {
                    break;
                }
                cq cqVar = cqVarArr2[i3];
                if (cqVar != null) {
                    i2 += iy.b(4, cqVar);
                }
                i3++;
            }
            b2 = i2;
        }
        co[] coVarArr = this.d;
        if (coVarArr != null && coVarArr.length > 0) {
            int i4 = b2;
            int i5 = 0;
            while (true) {
                co[] coVarArr2 = this.d;
                if (i5 >= coVarArr2.length) {
                    break;
                }
                co coVar = coVarArr2[i5];
                if (coVar != null) {
                    i4 += iy.b(5, coVar);
                }
                i5++;
            }
            b2 = i4;
        }
        ci[] ciVarArr = this.e;
        if (ciVarArr != null && ciVarArr.length > 0) {
            while (true) {
                ci[] ciVarArr2 = this.e;
                if (i >= ciVarArr2.length) {
                    break;
                }
                ci ciVar = ciVarArr2[i];
                if (ciVar != null) {
                    b2 += iy.b(6, ciVar);
                }
                i++;
            }
        }
        String str2 = this.g;
        if (str2 != null) {
            b2 += iy.b(7, str2);
        }
        Boolean bool = this.h;
        if (bool == null) {
            return b2;
        }
        bool.booleanValue();
        return b2 + iy.c(64) + 1;
    }

    public final /* synthetic */ je a(iw iwVar) throws IOException {
        while (true) {
            int a2 = iwVar.a();
            if (a2 == 0) {
                return this;
            }
            if (a2 == 8) {
                this.f2128a = Long.valueOf(iwVar.e());
            } else if (a2 == 18) {
                this.f2129b = iwVar.c();
            } else if (a2 == 24) {
                this.f = Integer.valueOf(iwVar.d());
            } else if (a2 == 34) {
                int a3 = jh.a(iwVar, 34);
                cq[] cqVarArr = this.c;
                int length = cqVarArr == null ? 0 : cqVarArr.length;
                cq[] cqVarArr2 = new cq[(a3 + length)];
                if (length != 0) {
                    System.arraycopy(this.c, 0, cqVarArr2, 0, length);
                }
                while (length < cqVarArr2.length - 1) {
                    cqVarArr2[length] = new cq();
                    iwVar.a(cqVarArr2[length]);
                    iwVar.a();
                    length++;
                }
                cqVarArr2[length] = new cq();
                iwVar.a(cqVarArr2[length]);
                this.c = cqVarArr2;
            } else if (a2 == 42) {
                int a4 = jh.a(iwVar, 42);
                co[] coVarArr = this.d;
                int length2 = coVarArr == null ? 0 : coVarArr.length;
                co[] coVarArr2 = new co[(a4 + length2)];
                if (length2 != 0) {
                    System.arraycopy(this.d, 0, coVarArr2, 0, length2);
                }
                while (length2 < coVarArr2.length - 1) {
                    coVarArr2[length2] = new co();
                    iwVar.a(coVarArr2[length2]);
                    iwVar.a();
                    length2++;
                }
                coVarArr2[length2] = new co();
                iwVar.a(coVarArr2[length2]);
                this.d = coVarArr2;
            } else if (a2 == 50) {
                int a5 = jh.a(iwVar, 50);
                ci[] ciVarArr = this.e;
                int length3 = ciVarArr == null ? 0 : ciVarArr.length;
                ci[] ciVarArr2 = new ci[(a5 + length3)];
                if (length3 != 0) {
                    System.arraycopy(this.e, 0, ciVarArr2, 0, length3);
                }
                while (length3 < ciVarArr2.length - 1) {
                    ciVarArr2[length3] = new ci();
                    iwVar.a(ciVarArr2[length3]);
                    iwVar.a();
                    length3++;
                }
                ciVarArr2[length3] = new ci();
                iwVar.a(ciVarArr2[length3]);
                this.e = ciVarArr2;
            } else if (a2 == 58) {
                this.g = iwVar.c();
            } else if (a2 == 64) {
                this.h = Boolean.valueOf(iwVar.b());
            } else if (!super.a(iwVar, a2)) {
                return this;
            }
        }
    }
}
