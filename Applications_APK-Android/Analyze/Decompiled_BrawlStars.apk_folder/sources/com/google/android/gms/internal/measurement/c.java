package com.google.android.gms.internal.measurement;

import android.text.TextUtils;
import com.google.android.gms.analytics.l;
import java.util.HashMap;

public final class c extends l<c> {

    /* renamed from: a  reason: collision with root package name */
    public String f2106a;

    /* renamed from: b  reason: collision with root package name */
    public int f2107b;
    public int c;
    public int d;
    public int e;
    public int f;

    public final String toString() {
        HashMap hashMap = new HashMap();
        hashMap.put("language", this.f2106a);
        hashMap.put("screenColors", Integer.valueOf(this.f2107b));
        hashMap.put("screenWidth", Integer.valueOf(this.c));
        hashMap.put("screenHeight", Integer.valueOf(this.d));
        hashMap.put("viewportWidth", Integer.valueOf(this.e));
        hashMap.put("viewportHeight", Integer.valueOf(this.f));
        return a((Object) hashMap);
    }

    public final /* synthetic */ void a(l lVar) {
        c cVar = (c) lVar;
        int i = this.f2107b;
        if (i != 0) {
            cVar.f2107b = i;
        }
        int i2 = this.c;
        if (i2 != 0) {
            cVar.c = i2;
        }
        int i3 = this.d;
        if (i3 != 0) {
            cVar.d = i3;
        }
        int i4 = this.e;
        if (i4 != 0) {
            cVar.e = i4;
        }
        int i5 = this.f;
        if (i5 != 0) {
            cVar.f = i5;
        }
        if (!TextUtils.isEmpty(this.f2106a)) {
            cVar.f2106a = this.f2106a;
        }
    }
}
