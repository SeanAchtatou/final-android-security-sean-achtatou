package com.google.android.gms.common;

import java.util.concurrent.Callable;

final /* synthetic */ class l implements Callable {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f1628a;

    /* renamed from: b  reason: collision with root package name */
    private final String f1629b;
    private final m c;

    l(boolean z, String str, m mVar) {
        this.f1628a = z;
        this.f1629b = str;
        this.c = mVar;
    }

    public final Object call() {
        return k.a(this.f1628a, this.f1629b, this.c);
    }
}
