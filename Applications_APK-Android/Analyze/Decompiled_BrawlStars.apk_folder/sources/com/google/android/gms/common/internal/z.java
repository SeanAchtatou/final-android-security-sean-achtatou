package com.google.android.gms.common.internal;

import android.os.Parcel;
import android.os.Parcelable;

final class z implements Parcelable.Creator<BinderWrapper> {
    z() {
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new BinderWrapper[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new BinderWrapper(parcel, (byte) 0);
    }
}
