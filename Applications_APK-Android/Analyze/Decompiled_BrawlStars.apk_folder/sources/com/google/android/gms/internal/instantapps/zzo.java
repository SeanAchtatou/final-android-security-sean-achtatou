package com.google.android.gms.internal.instantapps;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.a;

public final class zzo extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzo> CREATOR = new r();

    /* renamed from: a  reason: collision with root package name */
    private final int f2026a;

    /* renamed from: b  reason: collision with root package name */
    private final long f2027b;
    private final int c;
    private final Account d;
    private final Account[] e;

    public zzo(int i, long j, int i2, Account account, Account[] accountArr) {
        this.f2026a = i;
        this.f2027b = j;
        this.c = i2;
        this.d = account;
        this.e = accountArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.accounts.Account, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
     arg types: [android.os.Parcel, int, android.accounts.Account[], int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.b(parcel, 2, this.f2026a);
        a.a(parcel, 3, this.f2027b);
        a.b(parcel, 4, this.c);
        a.a(parcel, 5, (Parcelable) this.d, i, false);
        a.a(parcel, 6, (Parcelable[]) this.e, i, false);
        a.b(parcel, a2);
    }
}
