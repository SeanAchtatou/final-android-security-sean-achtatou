package com.google.android.gms.measurement.internal;

final class as implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ bt f2389a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ ar f2390b;

    as(ar arVar, bt btVar) {
        this.f2390b = arVar;
        this.f2389a = btVar;
    }

    public final void run() {
        ar.a(this.f2390b, this.f2389a);
        this.f2390b.a();
    }
}
