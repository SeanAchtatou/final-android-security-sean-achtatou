package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.h;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

public final class m {

    /* renamed from: a  reason: collision with root package name */
    final Map<BasePendingResult<?>, Boolean> f1484a = Collections.synchronizedMap(new WeakHashMap());

    /* renamed from: b  reason: collision with root package name */
    private final Map<h<?>, Boolean> f1485b = Collections.synchronizedMap(new WeakHashMap());

    /* access modifiers changed from: package-private */
    public final boolean a() {
        return !this.f1484a.isEmpty() || !this.f1485b.isEmpty();
    }

    public final void b() {
        a(false, d.f1466a);
    }

    public final void c() {
        a(true, bm.f1427a);
    }

    private final void a(boolean z, Status status) {
        HashMap hashMap;
        HashMap hashMap2;
        synchronized (this.f1484a) {
            hashMap = new HashMap(this.f1484a);
        }
        synchronized (this.f1485b) {
            hashMap2 = new HashMap(this.f1485b);
        }
        for (Map.Entry entry : hashMap.entrySet()) {
            if (z || ((Boolean) entry.getValue()).booleanValue()) {
                ((BasePendingResult) entry.getKey()).b(status);
            }
        }
        for (Map.Entry entry2 : hashMap2.entrySet()) {
            if (z || ((Boolean) entry2.getValue()).booleanValue()) {
                ((h) entry2.getKey()).b(new ApiException(status));
            }
        }
    }
}
