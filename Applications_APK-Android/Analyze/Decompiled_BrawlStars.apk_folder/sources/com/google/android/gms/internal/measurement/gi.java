package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.fq;

final class gi implements hk {

    /* renamed from: b  reason: collision with root package name */
    private static final gs f2241b = new gj();

    /* renamed from: a  reason: collision with root package name */
    private final gs f2242a;

    public gi() {
        this(new gk(fp.a(), a()));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.fs.a(java.lang.Object, java.lang.String):T
     arg types: [com.google.android.gms.internal.measurement.gs, java.lang.String]
     candidates:
      com.google.android.gms.internal.measurement.fs.a(java.lang.Object, java.lang.Object):java.lang.Object
      com.google.android.gms.internal.measurement.fs.a(java.lang.Object, java.lang.String):T */
    private gi(gs gsVar) {
        this.f2242a = (gs) fs.a((Object) gsVar, "messageInfoFactory");
    }

    public final <T> hj<T> a(Class<T> cls) {
        hl.a((Class<?>) cls);
        gr b2 = this.f2242a.b(cls);
        if (b2.b()) {
            if (fq.class.isAssignableFrom(cls)) {
                return gy.a(hl.c(), fg.a(), b2.c());
            }
            return gy.a(hl.a(), fg.b(), b2.c());
        } else if (fq.class.isAssignableFrom(cls)) {
            if (a(b2)) {
                return gx.a(b2, hc.b(), ge.b(), hl.c(), fg.a(), gq.b());
            }
            return gx.a(b2, hc.b(), ge.b(), hl.c(), (fe<?>) null, gq.b());
        } else if (a(b2)) {
            return gx.a(b2, hc.a(), ge.a(), hl.a(), fg.b(), gq.a());
        } else {
            return gx.a(b2, hc.a(), ge.a(), hl.b(), (fe<?>) null, gq.a());
        }
    }

    private static boolean a(gr grVar) {
        return grVar.a() == fq.e.h;
    }

    private static gs a() {
        try {
            return (gs) Class.forName("com.google.protobuf.DescriptorMessageInfoFactory").getDeclaredMethod("getInstance", new Class[0]).invoke(null, new Object[0]);
        } catch (Exception unused) {
            return f2241b;
        }
    }
}
