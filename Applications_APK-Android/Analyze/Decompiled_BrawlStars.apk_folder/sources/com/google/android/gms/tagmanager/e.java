package com.google.android.gms.tagmanager;

import android.content.Context;
import android.os.Process;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.util.g;

public final class e {
    private static Object l = new Object();
    private static e m;

    /* renamed from: a  reason: collision with root package name */
    volatile boolean f2656a;

    /* renamed from: b  reason: collision with root package name */
    final Thread f2657b;
    private volatile long c;
    private volatile long d;
    private volatile AdvertisingIdClient.Info e;
    private volatile long f;
    private volatile long g;
    /* access modifiers changed from: private */
    public final Context h;
    private final com.google.android.gms.common.util.e i;
    private final Object j;
    private aa k;

    public static e a(Context context) {
        if (m == null) {
            synchronized (l) {
                if (m == null) {
                    e eVar = new e(context);
                    m = eVar;
                    eVar.f2657b.start();
                }
            }
        }
        return m;
    }

    private e(Context context) {
        this(context, g.d());
    }

    private e(Context context, com.google.android.gms.common.util.e eVar) {
        this.c = 900000;
        this.d = 30000;
        this.f2656a = false;
        this.j = new Object();
        this.k = new p(this);
        this.i = eVar;
        if (context != null) {
            this.h = context.getApplicationContext();
        } else {
            this.h = context;
        }
        this.f = this.i.a();
        this.f2657b = new Thread(new w(this));
    }

    static /* synthetic */ void b(e eVar) {
        Process.setThreadPriority(10);
        while (!eVar.f2656a) {
            AdvertisingIdClient.Info a2 = eVar.k.a();
            if (a2 != null) {
                eVar.e = a2;
                eVar.g = eVar.i.a();
                ab.c("Obtained fresh AdvertisingId info from GmsCore.");
            }
            synchronized (eVar) {
                eVar.notifyAll();
            }
            try {
                synchronized (eVar.j) {
                    eVar.j.wait(eVar.c);
                }
            } catch (InterruptedException unused) {
                ab.c("sleep interrupted in AdvertiserDataPoller thread; continuing");
            }
        }
    }
}
