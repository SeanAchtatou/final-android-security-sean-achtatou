package com.google.android.gms.games.request;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.internal.zzd;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Deprecated
public final class GameRequestEntity extends zzd implements GameRequest {
    public static final Parcelable.Creator<GameRequestEntity> CREATOR = new b();

    /* renamed from: a  reason: collision with root package name */
    private final GameEntity f1840a;

    /* renamed from: b  reason: collision with root package name */
    private final PlayerEntity f1841b;
    private final byte[] c;
    private final String d;
    private final ArrayList<PlayerEntity> e;
    private final int f;
    private final long g;
    private final long h;
    private final Bundle i;
    private final int j;

    GameRequestEntity(GameEntity gameEntity, PlayerEntity playerEntity, byte[] bArr, String str, ArrayList<PlayerEntity> arrayList, int i2, long j2, long j3, Bundle bundle, int i3) {
        this.f1840a = gameEntity;
        this.f1841b = playerEntity;
        this.c = bArr;
        this.d = str;
        this.e = arrayList;
        this.f = i2;
        this.g = j2;
        this.h = j3;
        this.i = bundle;
        this.j = i3;
    }

    public GameRequestEntity(GameRequest gameRequest) {
        this.f1840a = new GameEntity(gameRequest.c());
        this.f1841b = new PlayerEntity(gameRequest.d());
        this.d = gameRequest.b();
        this.f = gameRequest.g();
        this.g = gameRequest.h();
        this.h = gameRequest.i();
        this.j = gameRequest.j();
        byte[] f2 = gameRequest.f();
        if (f2 == null) {
            this.c = null;
        } else {
            this.c = new byte[f2.length];
            System.arraycopy(f2, 0, this.c, 0, f2.length);
        }
        List<Player> e2 = gameRequest.e();
        int size = e2.size();
        this.e = new ArrayList<>(size);
        this.i = new Bundle();
        for (int i2 = 0; i2 < size; i2++) {
            Player player = (Player) e2.get(i2).a();
            String b2 = player.b();
            this.e.add((PlayerEntity) player);
            this.i.putInt(b2, gameRequest.a(b2));
        }
    }

    static boolean a(GameRequest gameRequest, Object obj) {
        if (!(obj instanceof GameRequest)) {
            return false;
        }
        if (gameRequest == obj) {
            return true;
        }
        GameRequest gameRequest2 = (GameRequest) obj;
        return j.a(gameRequest2.c(), gameRequest.c()) && j.a(gameRequest2.e(), gameRequest.e()) && j.a(gameRequest2.b(), gameRequest.b()) && j.a(gameRequest2.d(), gameRequest.d()) && Arrays.equals(c(gameRequest2), c(gameRequest)) && j.a(Integer.valueOf(gameRequest2.g()), Integer.valueOf(gameRequest.g())) && j.a(Long.valueOf(gameRequest2.h()), Long.valueOf(gameRequest.h())) && j.a(Long.valueOf(gameRequest2.i()), Long.valueOf(gameRequest.i()));
    }

    static String b(GameRequest gameRequest) {
        return j.a(gameRequest).a("Game", gameRequest.c()).a("Sender", gameRequest.d()).a("Recipients", gameRequest.e()).a("Data", gameRequest.f()).a("RequestId", gameRequest.b()).a("Type", Integer.valueOf(gameRequest.g())).a("CreationTimestamp", Long.valueOf(gameRequest.h())).a("ExpirationTimestamp", Long.valueOf(gameRequest.i())).toString();
    }

    private static int[] c(GameRequest gameRequest) {
        List<Player> e2 = gameRequest.e();
        int size = e2.size();
        int[] iArr = new int[size];
        for (int i2 = 0; i2 < size; i2++) {
            iArr[i2] = gameRequest.a(e2.get(i2).b());
        }
        return iArr;
    }

    public final int a(String str) {
        return this.i.getInt(str, 0);
    }

    public final /* bridge */ /* synthetic */ Object a() {
        return this;
    }

    public final String b() {
        return this.d;
    }

    public final Game c() {
        return this.f1840a;
    }

    public final Player d() {
        return this.f1841b;
    }

    public final List<Player> e() {
        return new ArrayList(this.e);
    }

    public final boolean equals(Object obj) {
        return a(this, obj);
    }

    public final byte[] f() {
        return this.c;
    }

    public final int g() {
        return this.f;
    }

    public final long h() {
        return this.g;
    }

    public final int hashCode() {
        return a(this);
    }

    public final long i() {
        return this.h;
    }

    public final int j() {
        return this.j;
    }

    public final String toString() {
        return b(this);
    }

    static int a(GameRequest gameRequest) {
        return (Arrays.hashCode(c(gameRequest)) * 31) + Arrays.hashCode(new Object[]{gameRequest.c(), gameRequest.e(), gameRequest.b(), gameRequest.d(), Integer.valueOf(gameRequest.g()), Long.valueOf(gameRequest.h()), Long.valueOf(gameRequest.i())});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.GameEntity, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.PlayerEntity, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
     arg types: [android.os.Parcel, int, byte[], int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
     arg types: [android.os.Parcel, int, android.os.Bundle, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void */
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = a.a(parcel, 20293);
        a.a(parcel, 1, (Parcelable) this.f1840a, i2, false);
        a.a(parcel, 2, (Parcelable) this.f1841b, i2, false);
        a.a(parcel, 3, this.c, false);
        a.a(parcel, 4, this.d, false);
        a.b(parcel, 5, e(), false);
        a.b(parcel, 7, this.f);
        a.a(parcel, 9, this.g);
        a.a(parcel, 10, this.h);
        a.a(parcel, 11, this.i, false);
        a.b(parcel, 12, this.j);
        a.b(parcel, a2);
    }
}
