package com.google.android.gms.stats;

import android.content.Context;
import android.os.PowerManager;
import android.os.WorkSource;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.util.q;
import com.google.android.gms.common.util.s;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicInteger;

public class a {
    public static ScheduledExecutorService k;
    private static volatile C0123a o = new b();

    /* renamed from: a  reason: collision with root package name */
    public final Object f2616a;

    /* renamed from: b  reason: collision with root package name */
    public final PowerManager.WakeLock f2617b;
    public WorkSource c;
    public final int d;
    public final String e;
    public final Context f;
    public boolean g;
    public final Map<String, Integer[]> h;
    public int i;
    public AtomicInteger j;
    private final String l;
    private final String m;
    private final Set<Future<?>> n;

    /* renamed from: com.google.android.gms.stats.a$a  reason: collision with other inner class name */
    public interface C0123a {
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public a(Context context, int i2, String str) {
        this(context, 1, str, context == null ? null : context.getPackageName());
    }

    private a(Context context, int i2, String str, String str2) {
        this(context, i2, str, str2, (byte) 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.l.a(java.lang.String, java.lang.Object):java.lang.String
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.l.a(java.lang.Object, java.lang.Object):T
      com.google.android.gms.common.internal.l.a(boolean, java.lang.Object):void
      com.google.android.gms.common.internal.l.a(java.lang.String, java.lang.Object):java.lang.String */
    private a(Context context, int i2, String str, String str2, byte b2) {
        this.f2616a = this;
        this.g = true;
        this.h = new HashMap();
        this.n = Collections.synchronizedSet(new HashSet());
        this.j = new AtomicInteger(0);
        l.a(context, "WakeLock: context must not be null");
        l.a(str, (Object) "WakeLock: wakeLockName must not be empty");
        this.d = i2;
        this.l = null;
        this.m = null;
        this.f = context.getApplicationContext();
        if (!"com.google.android.gms".equals(context.getPackageName())) {
            String valueOf = String.valueOf(str);
            this.e = valueOf.length() != 0 ? "*gcore*:".concat(valueOf) : new String("*gcore*:");
        } else {
            this.e = str;
        }
        this.f2617b = ((PowerManager) context.getSystemService("power")).newWakeLock(i2, str);
        if (s.a(context)) {
            this.c = s.a(context, q.a(str2) ? context.getPackageName() : str2);
            WorkSource workSource = this.c;
            if (workSource != null && s.a(this.f)) {
                WorkSource workSource2 = this.c;
                if (workSource2 != null) {
                    workSource2.add(workSource);
                } else {
                    this.c = workSource;
                }
                try {
                    this.f2617b.setWorkSource(this.c);
                } catch (ArrayIndexOutOfBoundsException | IllegalArgumentException e2) {
                    Log.wtf("WakeLock", e2.toString());
                }
            }
        }
        if (k == null) {
            k = com.google.android.gms.common.b.a.a().a();
        }
    }

    public final void a() {
        if (this.f2617b.isHeld()) {
            try {
                this.f2617b.release();
            } catch (RuntimeException e2) {
                if (e2.getClass().equals(RuntimeException.class)) {
                    String.valueOf(this.e).concat(" was already released!");
                } else {
                    throw e2;
                }
            }
            this.f2617b.isHeld();
        }
    }

    public final String a(String str) {
        if (!this.g) {
            return this.l;
        }
        if (!TextUtils.isEmpty(null)) {
            return null;
        }
        return this.l;
    }
}
