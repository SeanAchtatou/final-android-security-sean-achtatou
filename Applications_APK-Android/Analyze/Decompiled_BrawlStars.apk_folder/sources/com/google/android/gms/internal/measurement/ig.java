package com.google.android.gms.internal.measurement;

import java.util.Iterator;

final class ig implements Iterator<String> {

    /* renamed from: a  reason: collision with root package name */
    private Iterator<String> f2287a = this.f2288b.f2284a.iterator();

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ ie f2288b;

    ig(ie ieVar) {
        this.f2288b = ieVar;
    }

    public final boolean hasNext() {
        return this.f2287a.hasNext();
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }

    public final /* synthetic */ Object next() {
        return this.f2287a.next();
    }
}
