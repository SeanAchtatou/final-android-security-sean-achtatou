package com.google.android.gms.internal.drive;

public final class bi {

    /* renamed from: a  reason: collision with root package name */
    public static final bj f1896a = new bj("created");

    /* renamed from: b  reason: collision with root package name */
    public static final bk f1897b = new bk("lastOpenedTime");
    public static final bm c = new bm("modified");
    public static final bl d = new bl("modifiedByMe");
    public static final bo e = new bo("sharedWithMe");
    public static final bn f = new bn("recency");
}
