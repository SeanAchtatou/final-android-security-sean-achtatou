package com.google.android.gms.common.c;

import android.app.AppOpsManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.Build;
import android.os.Process;
import com.google.android.gms.common.util.o;

public class b {

    /* renamed from: a  reason: collision with root package name */
    public final Context f1518a;

    public b(Context context) {
        this.f1518a = context;
    }

    public final ApplicationInfo a(String str, int i) throws PackageManager.NameNotFoundException {
        return this.f1518a.getPackageManager().getApplicationInfo(str, i);
    }

    public final PackageInfo b(String str, int i) throws PackageManager.NameNotFoundException {
        return this.f1518a.getPackageManager().getPackageInfo(str, i);
    }

    public final int a(String str) {
        return this.f1518a.checkCallingOrSelfPermission(str);
    }

    public final CharSequence b(String str) throws PackageManager.NameNotFoundException {
        return this.f1518a.getPackageManager().getApplicationLabel(this.f1518a.getPackageManager().getApplicationInfo(str, 0));
    }

    public final boolean a() {
        String nameForUid;
        if (Binder.getCallingUid() == Process.myUid()) {
            return a.a(this.f1518a);
        }
        if (!o.g() || (nameForUid = this.f1518a.getPackageManager().getNameForUid(Binder.getCallingUid())) == null) {
            return false;
        }
        return this.f1518a.getPackageManager().isInstantApp(nameForUid);
    }

    public final boolean a(int i, String str) {
        if (Build.VERSION.SDK_INT >= 19) {
            try {
                ((AppOpsManager) this.f1518a.getSystemService("appops")).checkPackage(i, str);
                return true;
            } catch (SecurityException unused) {
                return false;
            }
        } else {
            String[] packagesForUid = this.f1518a.getPackageManager().getPackagesForUid(i);
            if (!(str == null || packagesForUid == null)) {
                for (String equals : packagesForUid) {
                    if (str.equals(equals)) {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
