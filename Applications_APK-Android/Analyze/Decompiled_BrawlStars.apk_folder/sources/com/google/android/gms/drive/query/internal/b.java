package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public final class b implements Parcelable.Creator<zzb> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zzb[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        zzx zzx = null;
        MetadataBundle metadataBundle = null;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            int i = 65535 & readInt;
            if (i == 1) {
                zzx = (zzx) SafeParcelReader.a(parcel, readInt, zzx.CREATOR);
            } else if (i != 2) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                metadataBundle = (MetadataBundle) SafeParcelReader.a(parcel, readInt, MetadataBundle.CREATOR);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new zzb(zzx, metadataBundle);
    }
}
