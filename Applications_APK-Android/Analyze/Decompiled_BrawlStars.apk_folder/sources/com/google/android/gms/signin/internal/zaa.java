package com.google.android.gms.signin.internal;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.g;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.a;

public final class zaa extends AbstractSafeParcelable implements g {
    public static final Parcelable.Creator<zaa> CREATOR = new b();

    /* renamed from: a  reason: collision with root package name */
    private final int f2609a;

    /* renamed from: b  reason: collision with root package name */
    private int f2610b;
    private Intent c;

    zaa(int i, int i2, Intent intent) {
        this.f2609a = i;
        this.f2610b = i2;
        this.c = intent;
    }

    public zaa() {
        this((byte) 0);
    }

    private zaa(byte b2) {
        this(2, 0, null);
    }

    public final Status b() {
        if (this.f2610b == 0) {
            return Status.f1368a;
        }
        return Status.e;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.content.Intent, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.b(parcel, 1, this.f2609a);
        a.b(parcel, 2, this.f2610b);
        a.a(parcel, 3, (Parcelable) this.c, i, false);
        a.b(parcel, a2);
    }
}
