package com.google.android.gms.internal.measurement;

import android.text.TextUtils;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Locale;

public final class k extends r {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f2318a;

    /* renamed from: b  reason: collision with root package name */
    private AdvertisingIdClient.Info f2319b;
    private final bw d;
    private String e;
    private boolean f = false;
    private final Object g = new Object();

    k(t tVar) {
        super(tVar);
        this.d = new bw(tVar.c);
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    public final boolean b() {
        m();
        AdvertisingIdClient.Info d2 = d();
        if (d2 == null || d2.isLimitAdTrackingEnabled()) {
            return false;
        }
        return true;
    }

    public final String c() {
        m();
        AdvertisingIdClient.Info d2 = d();
        String id = d2 != null ? d2.getId() : null;
        if (TextUtils.isEmpty(id)) {
            return null;
        }
        return id;
    }

    private final synchronized AdvertisingIdClient.Info d() {
        if (this.d.a(1000)) {
            this.d.a();
            AdvertisingIdClient.Info e2 = e();
            if (a(this.f2319b, e2)) {
                this.f2319b = e2;
            } else {
                f("Failed to reset client id on adid change. Not using adid");
                this.f2319b = new AdvertisingIdClient.Info("", false);
            }
        }
        return this.f2319b;
    }

    private final boolean a(AdvertisingIdClient.Info info, AdvertisingIdClient.Info info2) {
        String str = null;
        String id = info2 == null ? null : info2.getId();
        if (TextUtils.isEmpty(id)) {
            return true;
        }
        String b2 = this.c.g().b();
        synchronized (this.g) {
            if (!this.f) {
                this.e = o();
                this.f = true;
            } else if (TextUtils.isEmpty(this.e)) {
                if (info != null) {
                    str = info.getId();
                }
                if (str == null) {
                    String valueOf = String.valueOf(id);
                    String valueOf2 = String.valueOf(b2);
                    boolean g2 = g(valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf));
                    return g2;
                }
                String valueOf3 = String.valueOf(str);
                String valueOf4 = String.valueOf(b2);
                this.e = a(valueOf4.length() != 0 ? valueOf3.concat(valueOf4) : new String(valueOf3));
            }
            String valueOf5 = String.valueOf(id);
            String valueOf6 = String.valueOf(b2);
            String a2 = a(valueOf6.length() != 0 ? valueOf5.concat(valueOf6) : new String(valueOf5));
            if (TextUtils.isEmpty(a2)) {
                return false;
            }
            if (a2.equals(this.e)) {
                return true;
            }
            if (!TextUtils.isEmpty(this.e)) {
                b("Resetting the client id because Advertising Id changed.");
                b2 = this.c.g().c();
                a("New client Id", b2);
            }
            String valueOf7 = String.valueOf(id);
            String valueOf8 = String.valueOf(b2);
            boolean g3 = g(valueOf8.length() != 0 ? valueOf7.concat(valueOf8) : new String(valueOf7));
            return g3;
        }
    }

    private static String a(String str) {
        MessageDigest d2 = bx.d("MD5");
        if (d2 == null) {
            return null;
        }
        return String.format(Locale.US, "%032X", new BigInteger(1, d2.digest(str.getBytes())));
    }

    private final boolean g(String str) {
        try {
            String a2 = a(str);
            b("Storing hashed adid.");
            FileOutputStream openFileOutput = this.c.f2332a.openFileOutput("gaClientIdData", 0);
            openFileOutput.write(a2.getBytes());
            openFileOutput.close();
            this.e = a2;
            return true;
        } catch (IOException e2) {
            e("Error creating hash file", e2);
            return false;
        }
    }

    private final AdvertisingIdClient.Info e() {
        try {
            return AdvertisingIdClient.getAdvertisingIdInfo(this.c.f2332a);
        } catch (IllegalStateException unused) {
            e("IllegalStateException getting Ad Id Info. If you would like to see Audience reports, please ensure that you have added '<meta-data android:name=\"com.google.android.gms.version\" android:value=\"@integer/google_play_services_version\" />' to your application manifest file. See http://goo.gl/naFqQk for details.");
            return null;
        } catch (Exception e2) {
            if (!f2318a) {
                f2318a = true;
                d("Error getting advertiser id", e2);
            }
            return null;
        }
    }

    private final String o() {
        String str = null;
        try {
            FileInputStream openFileInput = this.c.f2332a.openFileInput("gaClientIdData");
            byte[] bArr = new byte[128];
            int read = openFileInput.read(bArr, 0, 128);
            if (openFileInput.available() > 0) {
                e("Hash file seems corrupted, deleting it.");
                openFileInput.close();
                this.c.f2332a.deleteFile("gaClientIdData");
                return null;
            } else if (read <= 0) {
                b("Hash file is empty.");
                openFileInput.close();
                return null;
            } else {
                String str2 = new String(bArr, 0, read);
                try {
                    openFileInput.close();
                } catch (FileNotFoundException unused) {
                } catch (IOException e2) {
                    e = e2;
                    str = str2;
                    d("Error reading Hash file, deleting it", e);
                    this.c.f2332a.deleteFile("gaClientIdData");
                    return str;
                }
                return str2;
            }
        } catch (FileNotFoundException unused2) {
            return null;
        } catch (IOException e3) {
            e = e3;
            d("Error reading Hash file, deleting it", e);
            this.c.f2332a.deleteFile("gaClientIdData");
            return str;
        }
    }
}
