package com.google.android.gms.analytics;

import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.internal.measurement.bh;
import com.google.android.gms.internal.measurement.bx;
import com.google.android.gms.internal.measurement.hy;
import com.google.android.gms.internal.measurement.k;
import com.google.android.gms.internal.measurement.s;
import com.google.android.gms.internal.measurement.w;
import java.util.HashMap;
import java.util.Map;

final class r implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Map f1338a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ boolean f1339b;
    private final /* synthetic */ String c;
    private final /* synthetic */ long d;
    private final /* synthetic */ boolean e;
    private final /* synthetic */ boolean f;
    private final /* synthetic */ String g;
    private final /* synthetic */ d h;

    r(d dVar, Map map, boolean z, String str, long j, boolean z2, boolean z3, String str2) {
        this.h = dVar;
        this.f1338a = map;
        this.f1339b = z;
        this.c = str;
        this.d = j;
        this.e = z2;
        this.f = z3;
        this.g = str2;
    }

    public final void run() {
        if (this.h.f1320b.b()) {
            this.f1338a.put("sc", "start");
        }
        Map map = this.f1338a;
        a d2 = this.h.c.d();
        l.c("getClientId can not be called from the main thread");
        bx.b(map, "cid", d2.d.g().b());
        String str = (String) this.f1338a.get("sf");
        if (str != null) {
            double a2 = bx.a(str);
            if (bx.a(a2, (String) this.f1338a.get("cid"))) {
                this.h.b("Sampling enabled. Hit sampled out. sample rate", Double.valueOf(a2));
                return;
            }
        }
        k a3 = this.h.c.f();
        if (this.f1339b) {
            bx.a(this.f1338a, "ate", a3.b());
            bx.a(this.f1338a, "adid", a3.c());
        } else {
            this.f1338a.remove("ate");
            this.f1338a.remove("adid");
        }
        hy b2 = this.h.c.h().b();
        bx.a(this.f1338a, "an", b2.f2277a);
        bx.a(this.f1338a, "av", b2.f2278b);
        bx.a(this.f1338a, "aid", b2.c);
        bx.a(this.f1338a, "aiid", b2.d);
        this.f1338a.put("v", AppEventsConstants.EVENT_PARAM_VALUE_YES);
        this.f1338a.put("_v", s.f2331b);
        bx.a(this.f1338a, "ul", this.h.c.h.b().f2106a);
        bx.a(this.f1338a, "sr", this.h.c.h.c());
        if ((this.c.equals("transaction") || this.c.equals("item")) || this.h.f1319a.a()) {
            long b3 = bx.b((String) this.f1338a.get("ht"));
            if (b3 == 0) {
                b3 = this.d;
            }
            long j = b3;
            if (this.e) {
                this.h.c.a().c("Dry run enabled. Would have sent hit", new bh(this.h, this.f1338a, j, this.f));
                return;
            }
            HashMap hashMap = new HashMap();
            bx.a(hashMap, "uid", this.f1338a);
            bx.a(hashMap, "an", this.f1338a);
            bx.a(hashMap, "aid", this.f1338a);
            bx.a(hashMap, "av", this.f1338a);
            bx.a(hashMap, "aiid", this.f1338a);
            this.f1338a.put("_s", String.valueOf(this.h.c.c().a(new w((String) this.f1338a.get("cid"), this.g, !TextUtils.isEmpty((CharSequence) this.f1338a.get("adid")), 0, hashMap))));
            this.h.c.c().a(new bh(this.h, this.f1338a, j, this.f));
            return;
        }
        this.h.c.a().a(this.f1338a, "Too many hits sent too quickly, rate limiting invoked");
    }
}
