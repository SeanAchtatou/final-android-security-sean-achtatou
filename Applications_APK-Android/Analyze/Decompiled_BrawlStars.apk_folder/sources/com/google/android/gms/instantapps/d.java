package com.google.android.gms.instantapps;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class d implements Parcelable.Creator<InstantAppIntentData> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new InstantAppIntentData[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        Intent intent = null;
        int i = 0;
        String str = null;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            int i2 = 65535 & readInt;
            if (i2 == 1) {
                intent = (Intent) SafeParcelReader.a(parcel, readInt, Intent.CREATOR);
            } else if (i2 == 2) {
                i = SafeParcelReader.d(parcel, readInt);
            } else if (i2 != 3) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                str = SafeParcelReader.l(parcel, readInt);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new InstantAppIntentData(intent, i, str);
    }
}
