package com.google.android.gms.tasks;

final class l implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ g f2685a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ k f2686b;

    l(k kVar, g gVar) {
        this.f2686b = kVar;
        this.f2685a = gVar;
    }

    public final void run() {
        if (this.f2685a.c()) {
            this.f2686b.f2684b.f();
            return;
        }
        try {
            this.f2686b.f2684b.a((Object) this.f2686b.f2683a.a(this.f2685a));
        } catch (RuntimeExecutionException e) {
            if (e.getCause() instanceof Exception) {
                this.f2686b.f2684b.a((Exception) e.getCause());
            } else {
                this.f2686b.f2684b.a((Exception) e);
            }
        } catch (Exception e2) {
            this.f2686b.f2684b.a(e2);
        }
    }
}
