package com.google.android.gms.analytics.a;

import com.google.android.gms.analytics.l;
import java.util.HashMap;
import java.util.Map;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private Map<String, String> f1315a;

    public final Map<String, String> a() {
        return new HashMap(this.f1315a);
    }

    public String toString() {
        HashMap hashMap = new HashMap();
        for (Map.Entry next : this.f1315a.entrySet()) {
            if (((String) next.getKey()).startsWith("&")) {
                hashMap.put(((String) next.getKey()).substring(1), (String) next.getValue());
            } else {
                hashMap.put((String) next.getKey(), (String) next.getValue());
            }
        }
        return l.a((Map) hashMap);
    }
}
