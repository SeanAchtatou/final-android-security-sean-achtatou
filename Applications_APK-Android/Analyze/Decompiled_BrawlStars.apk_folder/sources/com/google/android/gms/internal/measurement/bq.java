package com.google.android.gms.internal.measurement;

import android.content.Context;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.stats.a;

public final class bq {

    /* renamed from: a  reason: collision with root package name */
    static Object f2089a = new Object();

    /* renamed from: b  reason: collision with root package name */
    static a f2090b;
    private static Boolean c;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.bx.a(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.measurement.bx.a(java.util.Map<java.lang.String, java.lang.String>, java.lang.String, java.lang.String):void
      com.google.android.gms.internal.measurement.bx.a(java.util.Map<java.lang.String, java.lang.String>, java.lang.String, java.util.Map<java.lang.String, java.lang.String>):void
      com.google.android.gms.internal.measurement.bx.a(java.util.Map<java.lang.String, java.lang.String>, java.lang.String, boolean):void
      com.google.android.gms.internal.measurement.bx.a(android.content.Context, java.lang.String, boolean):boolean */
    public static boolean a(Context context) {
        l.a(context);
        Boolean bool = c;
        if (bool != null) {
            return bool.booleanValue();
        }
        boolean a2 = bx.a(context, "com.google.android.gms.analytics.AnalyticsReceiver", false);
        c = Boolean.valueOf(a2);
        return a2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00af, code lost:
        if (r3 == false) goto L_0x00b1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00b7, code lost:
        if (r14.i == 0) goto L_0x00b9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00b9, code lost:
        com.google.android.gms.common.stats.d.a();
        com.google.android.gms.common.stats.d.a(r14.f, com.google.android.gms.common.stats.c.a(r14.f2617b, r8), 7, r14.e, r8, null, r14.d, com.google.android.gms.common.util.s.a(r14.c), 1000);
        r14.i++;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(android.content.Context r14, android.content.Intent r15) {
        /*
            com.google.android.gms.internal.measurement.t r0 = com.google.android.gms.internal.measurement.t.a(r14)
            com.google.android.gms.internal.measurement.bk r0 = r0.a()
            if (r15 != 0) goto L_0x0010
            java.lang.String r14 = "AnalyticsReceiver called with null intent"
            r0.e(r14)
            return
        L_0x0010:
            java.lang.String r15 = r15.getAction()
            java.lang.String r1 = "Local AnalyticsReceiver got"
            r0.a(r1, r15)
            java.lang.String r1 = "com.google.android.gms.analytics.ANALYTICS_DISPATCH"
            boolean r15 = r1.equals(r15)
            if (r15 == 0) goto L_0x00fc
            boolean r15 = com.google.android.gms.internal.measurement.br.a(r14)
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r2 = "com.google.android.gms.analytics.ANALYTICS_DISPATCH"
            r1.<init>(r2)
            android.content.ComponentName r2 = new android.content.ComponentName
            java.lang.String r3 = "com.google.android.gms.analytics.AnalyticsService"
            r2.<init>(r14, r3)
            r1.setComponent(r2)
            java.lang.String r2 = "com.google.android.gms.analytics.ANALYTICS_DISPATCH"
            r1.setAction(r2)
            java.lang.Object r2 = com.google.android.gms.internal.measurement.bq.f2089a
            monitor-enter(r2)
            r14.startService(r1)     // Catch:{ all -> 0x00f9 }
            if (r15 != 0) goto L_0x0045
            monitor-exit(r2)     // Catch:{ all -> 0x00f9 }
            return
        L_0x0045:
            com.google.android.gms.stats.a r15 = com.google.android.gms.internal.measurement.bq.f2090b     // Catch:{ SecurityException -> 0x00f2 }
            r1 = 1
            r3 = 0
            if (r15 != 0) goto L_0x005b
            com.google.android.gms.stats.a r15 = new com.google.android.gms.stats.a     // Catch:{ SecurityException -> 0x00f2 }
            java.lang.String r4 = "Analytics WakeLock"
            r15.<init>(r14, r1, r4)     // Catch:{ SecurityException -> 0x00f2 }
            com.google.android.gms.internal.measurement.bq.f2090b = r15     // Catch:{ SecurityException -> 0x00f2 }
            android.os.PowerManager$WakeLock r14 = r15.f2617b     // Catch:{ SecurityException -> 0x00f2 }
            r14.setReferenceCounted(r3)     // Catch:{ SecurityException -> 0x00f2 }
            r15.g = r3     // Catch:{ SecurityException -> 0x00f2 }
        L_0x005b:
            com.google.android.gms.stats.a r14 = com.google.android.gms.internal.measurement.bq.f2090b     // Catch:{ SecurityException -> 0x00f2 }
            java.util.concurrent.atomic.AtomicInteger r15 = r14.j     // Catch:{ SecurityException -> 0x00f2 }
            r15.incrementAndGet()     // Catch:{ SecurityException -> 0x00f2 }
            r15 = 0
            java.lang.String r8 = r14.a(r15)     // Catch:{ SecurityException -> 0x00f2 }
            java.lang.Object r15 = r14.f2616a     // Catch:{ SecurityException -> 0x00f2 }
            monitor-enter(r15)     // Catch:{ SecurityException -> 0x00f2 }
            java.util.Map<java.lang.String, java.lang.Integer[]> r4 = r14.h     // Catch:{ all -> 0x00ef }
            boolean r4 = r4.isEmpty()     // Catch:{ all -> 0x00ef }
            if (r4 == 0) goto L_0x0076
            int r4 = r14.i     // Catch:{ all -> 0x00ef }
            if (r4 <= 0) goto L_0x0085
        L_0x0076:
            android.os.PowerManager$WakeLock r4 = r14.f2617b     // Catch:{ all -> 0x00ef }
            boolean r4 = r4.isHeld()     // Catch:{ all -> 0x00ef }
            if (r4 != 0) goto L_0x0085
            java.util.Map<java.lang.String, java.lang.Integer[]> r4 = r14.h     // Catch:{ all -> 0x00ef }
            r4.clear()     // Catch:{ all -> 0x00ef }
            r14.i = r3     // Catch:{ all -> 0x00ef }
        L_0x0085:
            boolean r4 = r14.g     // Catch:{ all -> 0x00ef }
            if (r4 == 0) goto L_0x00b1
            java.util.Map<java.lang.String, java.lang.Integer[]> r4 = r14.h     // Catch:{ all -> 0x00ef }
            java.lang.Object r4 = r4.get(r8)     // Catch:{ all -> 0x00ef }
            java.lang.Integer[] r4 = (java.lang.Integer[]) r4     // Catch:{ all -> 0x00ef }
            if (r4 != 0) goto L_0x00a2
            java.util.Map<java.lang.String, java.lang.Integer[]> r4 = r14.h     // Catch:{ all -> 0x00ef }
            java.lang.Integer[] r5 = new java.lang.Integer[r1]     // Catch:{ all -> 0x00ef }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r1)     // Catch:{ all -> 0x00ef }
            r5[r3] = r6     // Catch:{ all -> 0x00ef }
            r4.put(r8, r5)     // Catch:{ all -> 0x00ef }
            r3 = 1
            goto L_0x00af
        L_0x00a2:
            r5 = r4[r3]     // Catch:{ all -> 0x00ef }
            int r5 = r5.intValue()     // Catch:{ all -> 0x00ef }
            int r5 = r5 + r1
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x00ef }
            r4[r3] = r5     // Catch:{ all -> 0x00ef }
        L_0x00af:
            if (r3 != 0) goto L_0x00b9
        L_0x00b1:
            boolean r3 = r14.g     // Catch:{ all -> 0x00ef }
            if (r3 != 0) goto L_0x00da
            int r3 = r14.i     // Catch:{ all -> 0x00ef }
            if (r3 != 0) goto L_0x00da
        L_0x00b9:
            com.google.android.gms.common.stats.d.a()     // Catch:{ all -> 0x00ef }
            android.content.Context r4 = r14.f     // Catch:{ all -> 0x00ef }
            android.os.PowerManager$WakeLock r3 = r14.f2617b     // Catch:{ all -> 0x00ef }
            java.lang.String r5 = com.google.android.gms.common.stats.c.a(r3, r8)     // Catch:{ all -> 0x00ef }
            r6 = 7
            java.lang.String r7 = r14.e     // Catch:{ all -> 0x00ef }
            r9 = 0
            int r10 = r14.d     // Catch:{ all -> 0x00ef }
            android.os.WorkSource r3 = r14.c     // Catch:{ all -> 0x00ef }
            java.util.List r11 = com.google.android.gms.common.util.s.a(r3)     // Catch:{ all -> 0x00ef }
            r12 = 1000(0x3e8, double:4.94E-321)
            com.google.android.gms.common.stats.d.a(r4, r5, r6, r7, r8, r9, r10, r11, r12)     // Catch:{ all -> 0x00ef }
            int r3 = r14.i     // Catch:{ all -> 0x00ef }
            int r3 = r3 + r1
            r14.i = r3     // Catch:{ all -> 0x00ef }
        L_0x00da:
            monitor-exit(r15)     // Catch:{ all -> 0x00ef }
            android.os.PowerManager$WakeLock r15 = r14.f2617b     // Catch:{ SecurityException -> 0x00f2 }
            r15.acquire()     // Catch:{ SecurityException -> 0x00f2 }
            java.util.concurrent.ScheduledExecutorService r15 = com.google.android.gms.stats.a.k     // Catch:{ SecurityException -> 0x00f2 }
            com.google.android.gms.stats.c r1 = new com.google.android.gms.stats.c     // Catch:{ SecurityException -> 0x00f2 }
            r1.<init>(r14)     // Catch:{ SecurityException -> 0x00f2 }
            r3 = 1000(0x3e8, double:4.94E-321)
            java.util.concurrent.TimeUnit r14 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ SecurityException -> 0x00f2 }
            r15.schedule(r1, r3, r14)     // Catch:{ SecurityException -> 0x00f2 }
            goto L_0x00f7
        L_0x00ef:
            r14 = move-exception
            monitor-exit(r15)     // Catch:{ all -> 0x00ef }
            throw r14     // Catch:{ SecurityException -> 0x00f2 }
        L_0x00f2:
            java.lang.String r14 = "Analytics service at risk of not starting. For more reliable analytics, add the WAKE_LOCK permission to your manifest. See http://goo.gl/8Rd3yj for instructions."
            r0.e(r14)     // Catch:{ all -> 0x00f9 }
        L_0x00f7:
            monitor-exit(r2)     // Catch:{ all -> 0x00f9 }
            return
        L_0x00f9:
            r14 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x00f9 }
            throw r14
        L_0x00fc:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.bq.a(android.content.Context, android.content.Intent):void");
    }
}
