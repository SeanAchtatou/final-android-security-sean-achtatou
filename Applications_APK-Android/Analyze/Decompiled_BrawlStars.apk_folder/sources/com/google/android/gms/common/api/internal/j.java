package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.a.b;

public abstract class j<A extends a.b, L> {

    /* renamed from: a  reason: collision with root package name */
    final h<L> f1481a;

    /* renamed from: b  reason: collision with root package name */
    final Feature[] f1482b;
    final boolean c;
}
