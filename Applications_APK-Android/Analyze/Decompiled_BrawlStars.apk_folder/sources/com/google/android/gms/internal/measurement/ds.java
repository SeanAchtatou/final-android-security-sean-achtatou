package com.google.android.gms.internal.measurement;

import android.content.Context;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class ds<T> {

    /* renamed from: b  reason: collision with root package name */
    private static final Object f2170b = new Object();
    private static Context c = null;
    private static boolean d = false;
    private static final AtomicInteger g = new AtomicInteger();

    /* renamed from: a  reason: collision with root package name */
    public final T f2171a;
    private final dy e;
    private final String f;
    private volatile int h;
    private volatile T i;

    public static void a(Context context) {
        synchronized (f2170b) {
            Context applicationContext = context.getApplicationContext();
            if (applicationContext != null) {
                context = applicationContext;
            }
            if (c != context) {
                synchronized (dh.class) {
                    dh.f2161a.clear();
                }
                synchronized (dz.class) {
                    dz.f2174a.clear();
                }
                synchronized (Cdo.class) {
                    Cdo.f2166a = null;
                }
                g.incrementAndGet();
                c = context;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public abstract T a(Object obj);

    static void a() {
        g.incrementAndGet();
    }

    private ds(dy dyVar, String str, T t) {
        this.h = -1;
        if (dyVar.f2172a != null) {
            this.e = dyVar;
            this.f = str;
            this.f2171a = t;
            return;
        }
        throw new IllegalArgumentException("Must pass a valid SharedPreferences file name or ContentProvider URI");
    }

    private final String a(String str) {
        if (str != null && str.isEmpty()) {
            return this.f;
        }
        String valueOf = String.valueOf(str);
        String valueOf2 = String.valueOf(this.f);
        return valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
    }

    public final String b() {
        return a(this.e.c);
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x007e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final T c() {
        /*
            r5 = this;
            java.util.concurrent.atomic.AtomicInteger r0 = com.google.android.gms.internal.measurement.ds.g
            int r0 = r0.get()
            int r1 = r5.h
            if (r1 >= r0) goto L_0x00ae
            monitor-enter(r5)
            int r1 = r5.h     // Catch:{ all -> 0x00ab }
            if (r1 >= r0) goto L_0x00a9
            android.content.Context r1 = com.google.android.gms.internal.measurement.ds.c     // Catch:{ all -> 0x00ab }
            if (r1 == 0) goto L_0x00a1
            android.content.Context r1 = com.google.android.gms.internal.measurement.ds.c     // Catch:{ all -> 0x00ab }
            com.google.android.gms.internal.measurement.do r1 = com.google.android.gms.internal.measurement.Cdo.a(r1)     // Catch:{ all -> 0x00ab }
            java.lang.String r2 = "gms:phenotype:phenotype_flag:debug_bypass_phenotype"
            java.lang.Object r1 = r1.a(r2)     // Catch:{ all -> 0x00ab }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x00ab }
            if (r1 == 0) goto L_0x0031
            java.util.regex.Pattern r2 = com.google.android.gms.internal.measurement.de.f2158b     // Catch:{ all -> 0x00ab }
            java.util.regex.Matcher r1 = r2.matcher(r1)     // Catch:{ all -> 0x00ab }
            boolean r1 = r1.matches()     // Catch:{ all -> 0x00ab }
            if (r1 == 0) goto L_0x0031
            r1 = 1
            goto L_0x0032
        L_0x0031:
            r1 = 0
        L_0x0032:
            r2 = 0
            if (r1 != 0) goto L_0x0061
            com.google.android.gms.internal.measurement.dy r1 = r5.e     // Catch:{ all -> 0x00ab }
            android.net.Uri r1 = r1.f2172a     // Catch:{ all -> 0x00ab }
            if (r1 == 0) goto L_0x004a
            android.content.Context r1 = com.google.android.gms.internal.measurement.ds.c     // Catch:{ all -> 0x00ab }
            android.content.ContentResolver r1 = r1.getContentResolver()     // Catch:{ all -> 0x00ab }
            com.google.android.gms.internal.measurement.dy r3 = r5.e     // Catch:{ all -> 0x00ab }
            android.net.Uri r3 = r3.f2172a     // Catch:{ all -> 0x00ab }
            com.google.android.gms.internal.measurement.dh r1 = com.google.android.gms.internal.measurement.dh.a(r1, r3)     // Catch:{ all -> 0x00ab }
            goto L_0x0050
        L_0x004a:
            android.content.Context r1 = com.google.android.gms.internal.measurement.ds.c     // Catch:{ all -> 0x00ab }
            com.google.android.gms.internal.measurement.dz r1 = com.google.android.gms.internal.measurement.dz.a(r1, r2)     // Catch:{ all -> 0x00ab }
        L_0x0050:
            if (r1 == 0) goto L_0x007a
            java.lang.String r3 = r5.b()     // Catch:{ all -> 0x00ab }
            java.lang.Object r1 = r1.a(r3)     // Catch:{ all -> 0x00ab }
            if (r1 == 0) goto L_0x007a
            java.lang.Object r1 = r5.a(r1)     // Catch:{ all -> 0x00ab }
            goto L_0x007b
        L_0x0061:
            java.lang.String r1 = "Bypass reading Phenotype values for flag: "
            java.lang.String r3 = r5.b()     // Catch:{ all -> 0x00ab }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ all -> 0x00ab }
            int r4 = r3.length()     // Catch:{ all -> 0x00ab }
            if (r4 == 0) goto L_0x0075
            r1.concat(r3)     // Catch:{ all -> 0x00ab }
            goto L_0x007a
        L_0x0075:
            java.lang.String r3 = new java.lang.String     // Catch:{ all -> 0x00ab }
            r3.<init>(r1)     // Catch:{ all -> 0x00ab }
        L_0x007a:
            r1 = r2
        L_0x007b:
            if (r1 == 0) goto L_0x007e
            goto L_0x009c
        L_0x007e:
            android.content.Context r1 = com.google.android.gms.internal.measurement.ds.c     // Catch:{ all -> 0x00ab }
            com.google.android.gms.internal.measurement.do r1 = com.google.android.gms.internal.measurement.Cdo.a(r1)     // Catch:{ all -> 0x00ab }
            com.google.android.gms.internal.measurement.dy r3 = r5.e     // Catch:{ all -> 0x00ab }
            java.lang.String r3 = r3.f2173b     // Catch:{ all -> 0x00ab }
            java.lang.String r3 = r5.a(r3)     // Catch:{ all -> 0x00ab }
            java.lang.Object r1 = r1.a(r3)     // Catch:{ all -> 0x00ab }
            if (r1 == 0) goto L_0x0096
            java.lang.Object r2 = r5.a(r1)     // Catch:{ all -> 0x00ab }
        L_0x0096:
            r1 = r2
            if (r1 == 0) goto L_0x009a
            goto L_0x009c
        L_0x009a:
            T r1 = r5.f2171a     // Catch:{ all -> 0x00ab }
        L_0x009c:
            r5.i = r1     // Catch:{ all -> 0x00ab }
            r5.h = r0     // Catch:{ all -> 0x00ab }
            goto L_0x00a9
        L_0x00a1:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x00ab }
            java.lang.String r1 = "Must call PhenotypeFlag.init() first"
            r0.<init>(r1)     // Catch:{ all -> 0x00ab }
            throw r0     // Catch:{ all -> 0x00ab }
        L_0x00a9:
            monitor-exit(r5)     // Catch:{ all -> 0x00ab }
            goto L_0x00ae
        L_0x00ab:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x00ab }
            throw r0
        L_0x00ae:
            T r0 = r5.i
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.ds.c():java.lang.Object");
    }

    /* synthetic */ ds(dy dyVar, String str, Object obj, byte b2) {
        this(dyVar, str, obj);
    }

    public static /* synthetic */ ds a(dy dyVar, String str, long j) {
        return new dt(dyVar, str, Long.valueOf(j));
    }

    public static /* synthetic */ ds a(dy dyVar, String str, boolean z) {
        return new dv(dyVar, str, Boolean.valueOf(z));
    }

    public static /* synthetic */ ds a(dy dyVar, String str, int i2) {
        return new du(dyVar, str, Integer.valueOf(i2));
    }

    public static /* synthetic */ ds a(dy dyVar, String str, double d2) {
        return new dw(dyVar, str, Double.valueOf(d2));
    }

    public static /* synthetic */ ds a(dy dyVar, String str, String str2) {
        return new dx(dyVar, str, str2);
    }
}
