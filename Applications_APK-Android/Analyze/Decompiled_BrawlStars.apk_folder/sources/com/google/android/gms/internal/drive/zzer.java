package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.zza;

public abstract class zzer extends zzb implements zzeq {
    public zzer() {
        super("com.google.android.gms.drive.internal.IDriveServiceCallbacks");
    }

    /* access modifiers changed from: protected */
    public final boolean a(int i, Parcel parcel, Parcel parcel2) throws RemoteException {
        Object obj;
        switch (i) {
            case 1:
                obj = zzff.CREATOR;
                g.a(parcel, obj);
                break;
            case 2:
                a((zzfn) g.a(parcel, zzfn.CREATOR));
                break;
            case 3:
                a((zzfh) g.a(parcel, zzfh.CREATOR));
                break;
            case 4:
                a((zzfs) g.a(parcel, zzfs.CREATOR));
                break;
            case 5:
                a((zzfb) g.a(parcel, zzfb.CREATOR));
                break;
            case 6:
                a((Status) g.a(parcel, Status.CREATOR));
                break;
            case 7:
                a();
                break;
            case 8:
                a((zzfp) g.a(parcel, zzfp.CREATOR));
                break;
            case 9:
                obj = zzgb.CREATOR;
                g.a(parcel, obj);
                break;
            case 10:
            case 19:
            default:
                return false;
            case 11:
                g.a(parcel, zzfr.CREATOR);
                zzim.a(parcel.readStrongBinder());
                break;
            case 12:
                obj = zzfx.CREATOR;
                g.a(parcel, obj);
                break;
            case 13:
                a((zzfu) g.a(parcel, zzfu.CREATOR));
                break;
            case 14:
                a((zzfd) g.a(parcel, zzfd.CREATOR));
                break;
            case 15:
                g.a(parcel);
                break;
            case 16:
                obj = zzfl.CREATOR;
                g.a(parcel, obj);
                break;
            case 17:
                obj = zza.CREATOR;
                g.a(parcel, obj);
                break;
            case 18:
                obj = zzez.CREATOR;
                g.a(parcel, obj);
                break;
            case 20:
                obj = zzem.CREATOR;
                g.a(parcel, obj);
                break;
            case 21:
                obj = zzgt.CREATOR;
                g.a(parcel, obj);
                break;
            case 22:
                obj = zzfz.CREATOR;
                g.a(parcel, obj);
                break;
        }
        parcel2.writeNoException();
        return true;
    }
}
