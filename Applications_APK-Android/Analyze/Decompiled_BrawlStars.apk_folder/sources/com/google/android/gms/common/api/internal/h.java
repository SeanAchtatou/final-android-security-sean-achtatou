package com.google.android.gms.common.api.internal;

public final class h<L> {

    /* renamed from: a  reason: collision with root package name */
    volatile L f1477a;

    public static final class a<L> {

        /* renamed from: a  reason: collision with root package name */
        private final L f1478a;

        /* renamed from: b  reason: collision with root package name */
        private final String f1479b;

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return this.f1478a == aVar.f1478a && this.f1479b.equals(aVar.f1479b);
        }

        public final int hashCode() {
            return (System.identityHashCode(this.f1478a) * 31) + this.f1479b.hashCode();
        }
    }
}
