package com.google.android.gms.common;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NotificationCompat;
import android.util.TypedValue;
import android.widget.ProgressBar;
import com.google.android.gms.base.R;
import com.google.android.gms.common.api.GoogleApiActivity;
import com.google.android.gms.common.api.internal.ay;
import com.google.android.gms.common.api.internal.g;
import com.google.android.gms.common.api.internal.zabq;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.util.h;
import com.google.android.gms.common.util.o;
import com.google.android.gms.internal.base.d;

public class c extends d {

    /* renamed from: a  reason: collision with root package name */
    public static final int f1513a = d.f1522b;
    private static final Object c = new Object();
    private static final c d = new c();
    private String e;

    public static c a() {
        return d;
    }

    c() {
    }

    class a extends d {

        /* renamed from: a  reason: collision with root package name */
        private final Context f1516a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Context context) {
            super(Looper.myLooper() == null ? Looper.getMainLooper() : Looper.myLooper());
            this.f1516a = context.getApplicationContext();
        }

        public final void handleMessage(Message message) {
            if (message.what != 1) {
                int i = message.what;
                StringBuilder sb = new StringBuilder(50);
                sb.append("Don't know how to handle this message: ");
                sb.append(i);
                sb.toString();
                return;
            }
            int a2 = c.this.a(this.f1516a);
            if (c.this.a(a2)) {
                c cVar = c.this;
                Context context = this.f1516a;
                cVar.a(context, a2, cVar.a(context, a2, 0, "n"));
            }
        }
    }

    public final boolean b(Activity activity, int i, int i2, DialogInterface.OnCancelListener onCancelListener) {
        Dialog a2 = a(activity, i, 2, onCancelListener);
        if (a2 == null) {
            return false;
        }
        a(activity, a2, "GooglePlayServicesErrorDialog", onCancelListener);
        return true;
    }

    public static Dialog a(Activity activity, DialogInterface.OnCancelListener onCancelListener) {
        ProgressBar progressBar = new ProgressBar(activity, null, 16842874);
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(0);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setView(progressBar);
        builder.setMessage(com.google.android.gms.common.internal.c.c(activity, 18));
        builder.setPositiveButton("", (DialogInterface.OnClickListener) null);
        AlertDialog create = builder.create();
        a(activity, create, "GooglePlayServicesUpdatingDialog", onCancelListener);
        return create;
    }

    public final zabq a(Context context, ay ayVar) {
        IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
        intentFilter.addDataScheme("package");
        zabq zabq = new zabq(ayVar);
        context.registerReceiver(zabq, intentFilter);
        zabq.f1506a = context;
        if (f.a(context, "com.google.android.gms")) {
            return zabq;
        }
        ayVar.a();
        zabq.a();
        return null;
    }

    private final String c() {
        String str;
        synchronized (c) {
            str = this.e;
        }
        return str;
    }

    public final int a(Context context) {
        return super.a(context);
    }

    public final int a(Context context, int i) {
        return super.a(context, i);
    }

    public final boolean a(int i) {
        return super.a(i);
    }

    public final Intent a(Context context, int i, String str) {
        return super.a(context, i, str);
    }

    public final PendingIntent a(Context context, int i, int i2) {
        return super.a(context, i, i2);
    }

    public final String b(int i) {
        return super.b(i);
    }

    private static Dialog a(Context context, int i, com.google.android.gms.common.internal.d dVar, DialogInterface.OnCancelListener onCancelListener) {
        AlertDialog.Builder builder = null;
        if (i == 0) {
            return null;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(16843529, typedValue, true);
        if ("Theme.Dialog.Alert".equals(context.getResources().getResourceEntryName(typedValue.resourceId))) {
            builder = new AlertDialog.Builder(context, 5);
        }
        if (builder == null) {
            builder = new AlertDialog.Builder(context);
        }
        builder.setMessage(com.google.android.gms.common.internal.c.c(context, i));
        if (onCancelListener != null) {
            builder.setOnCancelListener(onCancelListener);
        }
        String e2 = com.google.android.gms.common.internal.c.e(context, i);
        if (e2 != null) {
            builder.setPositiveButton(e2, dVar);
        }
        String a2 = com.google.android.gms.common.internal.c.a(context, i);
        if (a2 != null) {
            builder.setTitle(a2);
        }
        return builder.create();
    }

    private static void a(Activity activity, Dialog dialog, String str, DialogInterface.OnCancelListener onCancelListener) {
        if (activity instanceof FragmentActivity) {
            h.a(dialog, onCancelListener).show(((FragmentActivity) activity).getSupportFragmentManager(), str);
            return;
        }
        b.a(dialog, onCancelListener).show(activity.getFragmentManager(), str);
    }

    /* access modifiers changed from: package-private */
    public final void a(Context context, int i, PendingIntent pendingIntent) {
        int i2;
        if (i == 18) {
            d(context);
        } else if (pendingIntent != null) {
            String b2 = com.google.android.gms.common.internal.c.b(context, i);
            String d2 = com.google.android.gms.common.internal.c.d(context, i);
            Resources resources = context.getResources();
            NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
            NotificationCompat.Builder style = new NotificationCompat.Builder(context).setLocalOnly(true).setAutoCancel(true).setContentTitle(b2).setStyle(new NotificationCompat.BigTextStyle().bigText(d2));
            if (h.a(context)) {
                l.a(o.d());
                style.setSmallIcon(context.getApplicationInfo().icon).setPriority(2);
                if (h.b(context)) {
                    style.addAction(R.drawable.common_full_open_on_phone, resources.getString(R.string.common_open_on_phone), pendingIntent);
                } else {
                    style.setContentIntent(pendingIntent);
                }
            } else {
                style.setSmallIcon(17301642).setTicker(resources.getString(R.string.common_google_play_services_notification_ticker)).setWhen(System.currentTimeMillis()).setContentIntent(pendingIntent).setContentText(d2);
            }
            if (o.g()) {
                l.a(o.g());
                String c2 = c();
                if (c2 == null) {
                    c2 = "com.google.android.gms.availability";
                    NotificationChannel notificationChannel = notificationManager.getNotificationChannel(c2);
                    String a2 = com.google.android.gms.common.internal.c.a(context);
                    if (notificationChannel == null) {
                        notificationManager.createNotificationChannel(new NotificationChannel(c2, a2, 4));
                    } else if (!a2.equals(notificationChannel.getName())) {
                        notificationChannel.setName(a2);
                        notificationManager.createNotificationChannel(notificationChannel);
                    }
                }
                style.setChannelId(c2);
            }
            Notification build = style.build();
            if (i == 1 || i == 2 || i == 3) {
                i2 = 10436;
                f.c.set(false);
            } else {
                i2 = 39789;
            }
            notificationManager.notify(i2, build);
        }
    }

    private void d(Context context) {
        new a(context).sendEmptyMessageDelayed(1, 120000);
    }

    public final Dialog a(Activity activity, int i, int i2, DialogInterface.OnCancelListener onCancelListener) {
        return a(activity, i, com.google.android.gms.common.internal.d.a(activity, super.a(activity, i, "d"), i2), onCancelListener);
    }

    public final boolean a(Activity activity, g gVar, int i, DialogInterface.OnCancelListener onCancelListener) {
        Dialog a2 = a(activity, i, com.google.android.gms.common.internal.d.a(gVar, super.a(activity, i, "d"), 2), onCancelListener);
        if (a2 == null) {
            return false;
        }
        a(activity, a2, "GooglePlayServicesErrorDialog", onCancelListener);
        return true;
    }

    public final boolean a(Context context, ConnectionResult connectionResult, int i) {
        PendingIntent pendingIntent;
        if (connectionResult.a()) {
            pendingIntent = connectionResult.c;
        } else {
            pendingIntent = super.a(context, connectionResult.f1353b, 0);
        }
        if (pendingIntent == null) {
            return false;
        }
        a(context, connectionResult.f1353b, GoogleApiActivity.a(context, pendingIntent, i));
        return true;
    }
}
