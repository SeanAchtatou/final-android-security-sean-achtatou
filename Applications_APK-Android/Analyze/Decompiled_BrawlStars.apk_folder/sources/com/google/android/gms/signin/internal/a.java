package com.google.android.gms.signin.internal;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.d;
import com.google.android.gms.common.f;
import com.google.android.gms.common.internal.BaseGmsClient;
import com.google.android.gms.common.internal.IAccountAccessor;
import com.google.android.gms.common.internal.ResolveAccountRequest;
import com.google.android.gms.common.internal.b;
import com.google.android.gms.common.internal.e;
import com.google.android.gms.common.internal.l;

public class a extends e<zaf> implements com.google.android.gms.signin.e {
    private final boolean i;
    private final b j;
    private final Bundle k;
    private Integer l;

    private a(Context context, Looper looper, b bVar, Bundle bundle, d.b bVar2, d.c cVar) {
        super(context, looper, 44, bVar, bVar2, cVar);
        this.i = true;
        this.j = bVar;
        this.k = bundle;
        this.l = bVar.j;
    }

    public final int g() {
        return f.f1536b;
    }

    public final String i() {
        return "com.google.android.gms.signin.service.START";
    }

    public final String j() {
        return "com.google.android.gms.signin.internal.ISignInService";
    }

    public a(Context context, Looper looper, b bVar, d.b bVar2, d.c cVar) {
        this(context, looper, bVar, a(bVar), bVar2, cVar);
    }

    public final boolean d() {
        return this.i;
    }

    public final void a(IAccountAccessor iAccountAccessor, boolean z) {
        try {
            ((zaf) p()).a(iAccountAccessor, this.l.intValue(), z);
        } catch (RemoteException unused) {
        }
    }

    public final void r() {
        try {
            ((zaf) p()).a(this.l.intValue());
        } catch (RemoteException unused) {
        }
    }

    public final void a(zad zad) {
        Account account;
        l.a(zad, "Expecting a valid ISignInCallbacks");
        try {
            b bVar = this.j;
            if (bVar.f1595a != null) {
                account = bVar.f1595a;
            } else {
                account = new Account("<<default account>>", "com.google");
            }
            GoogleSignInAccount googleSignInAccount = null;
            if ("<<default account>>".equals(account.name)) {
                com.google.android.gms.auth.api.signin.internal.b a2 = com.google.android.gms.auth.api.signin.internal.b.a(this.c);
                googleSignInAccount = a2.a(a2.b("defaultGoogleSignInAccount"));
            }
            ((zaf) p()).a(new zah(new ResolveAccountRequest(account, this.l.intValue(), googleSignInAccount)), zad);
        } catch (RemoteException e) {
            try {
                zad.a(new zaj());
            } catch (RemoteException unused) {
                Log.wtf("SignInClientImpl", "ISignInCallbacks#onSignInComplete should be executed from the same process, unexpected RemoteException.", e);
            }
        }
    }

    public final Bundle n() {
        if (!this.c.getPackageName().equals(this.j.g)) {
            this.k.putString("com.google.android.gms.signin.internal.realClientPackageName", this.j.g);
        }
        return this.k;
    }

    public final void s() {
        a(new BaseGmsClient.d());
    }

    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.signin.internal.ISignInService");
        if (queryLocalInterface instanceof zaf) {
            return (zaf) queryLocalInterface;
        }
        return new zag(iBinder);
    }

    public static Bundle a(b bVar) {
        com.google.android.gms.signin.a aVar = bVar.i;
        Integer num = bVar.j;
        Bundle bundle = new Bundle();
        bundle.putParcelable("com.google.android.gms.signin.internal.clientRequestedAccount", bVar.f1595a);
        if (num != null) {
            bundle.putInt("com.google.android.gms.common.internal.ClientSettings.sessionId", num.intValue());
        }
        if (aVar != null) {
            bundle.putBoolean("com.google.android.gms.signin.internal.offlineAccessRequested", aVar.f2606b);
            bundle.putBoolean("com.google.android.gms.signin.internal.idTokenRequested", aVar.c);
            bundle.putString("com.google.android.gms.signin.internal.serverClientId", aVar.d);
            bundle.putBoolean("com.google.android.gms.signin.internal.usePromptModeForAuthCode", true);
            bundle.putBoolean("com.google.android.gms.signin.internal.forceCodeForRefreshToken", aVar.e);
            bundle.putString("com.google.android.gms.signin.internal.hostedDomain", aVar.f);
            bundle.putBoolean("com.google.android.gms.signin.internal.waitForAccessTokenRefresh", aVar.g);
            if (aVar.h != null) {
                bundle.putLong("com.google.android.gms.signin.internal.authApiSignInModuleVersion", aVar.h.longValue());
            }
            if (aVar.i != null) {
                bundle.putLong("com.google.android.gms.signin.internal.realClientLibraryVersion", aVar.i.longValue());
            }
        }
        return bundle;
    }
}
