package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.games.internal.GamesDowngradeableSafeParcel;
import java.util.Arrays;

public final class GameEntity extends GamesDowngradeableSafeParcel implements Game {
    public static final Parcelable.Creator<GameEntity> CREATOR = new a();

    /* renamed from: b  reason: collision with root package name */
    private final String f1786b;
    private final String c;
    private final String d;
    private final String e;
    private final String f;
    private final String g;
    private final Uri h;
    private final Uri i;
    private final Uri j;
    private final boolean k;
    private final boolean l;
    private final String m;
    private final int n;
    private final int o;
    private final int p;
    private final boolean q;
    private final boolean r;
    private final String s;
    private final String t;
    private final String u;
    private final boolean v;
    private final boolean w;
    private final boolean x;
    private final String y;
    private final boolean z;

    static final class a extends ac {
        a() {
        }

        public final GameEntity a(Parcel parcel) {
            if (GameEntity.b(GameEntity.b_()) || GameEntity.a(GameEntity.class.getCanonicalName())) {
                return super.createFromParcel(parcel);
            }
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            String readString3 = parcel.readString();
            String readString4 = parcel.readString();
            String readString5 = parcel.readString();
            String readString6 = parcel.readString();
            String readString7 = parcel.readString();
            Uri parse = readString7 == null ? null : Uri.parse(readString7);
            String readString8 = parcel.readString();
            Uri parse2 = readString8 == null ? null : Uri.parse(readString8);
            String readString9 = parcel.readString();
            return new GameEntity(readString, readString2, readString3, readString4, readString5, readString6, parse, parse2, readString9 == null ? null : Uri.parse(readString9), parcel.readInt() > 0, parcel.readInt() > 0, parcel.readString(), parcel.readInt(), parcel.readInt(), parcel.readInt(), false, false, null, null, null, false, false, false, null, false);
        }

        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return createFromParcel(parcel);
        }
    }

    public GameEntity(Game game) {
        this.f1786b = game.b();
        this.d = game.d();
        this.e = game.e();
        this.f = game.f();
        this.g = game.g();
        this.c = game.c();
        this.h = game.h();
        this.s = game.getIconImageUrl();
        this.i = game.i();
        this.t = game.getHiResImageUrl();
        this.j = game.j();
        this.u = game.getFeaturedImageUrl();
        this.k = game.k();
        this.l = game.n();
        this.m = game.o();
        this.n = 1;
        this.o = game.p();
        this.p = game.q();
        this.q = game.r();
        this.r = game.s();
        this.v = game.l();
        this.w = game.m();
        this.x = game.t();
        this.y = game.u();
        this.z = game.v();
    }

    GameEntity(String str, String str2, String str3, String str4, String str5, String str6, Uri uri, Uri uri2, Uri uri3, boolean z2, boolean z3, String str7, int i2, int i3, int i4, boolean z4, boolean z5, String str8, String str9, String str10, boolean z6, boolean z7, boolean z8, String str11, boolean z9) {
        this.f1786b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = str5;
        this.g = str6;
        this.h = uri;
        this.s = str8;
        this.i = uri2;
        this.t = str9;
        this.j = uri3;
        this.u = str10;
        this.k = z2;
        this.l = z3;
        this.m = str7;
        this.n = i2;
        this.o = i3;
        this.p = i4;
        this.q = z4;
        this.r = z5;
        this.v = z6;
        this.w = z7;
        this.x = z8;
        this.y = str11;
        this.z = z9;
    }

    static boolean a(Game game, Object obj) {
        if (!(obj instanceof Game)) {
            return false;
        }
        if (game == obj) {
            return true;
        }
        Game game2 = (Game) obj;
        return j.a(game2.b(), game.b()) && j.a(game2.c(), game.c()) && j.a(game2.d(), game.d()) && j.a(game2.e(), game.e()) && j.a(game2.f(), game.f()) && j.a(game2.g(), game.g()) && j.a(game2.h(), game.h()) && j.a(game2.i(), game.i()) && j.a(game2.j(), game.j()) && j.a(Boolean.valueOf(game2.k()), Boolean.valueOf(game.k())) && j.a(Boolean.valueOf(game2.n()), Boolean.valueOf(game.n())) && j.a(game2.o(), game.o()) && j.a(Integer.valueOf(game2.p()), Integer.valueOf(game.p())) && j.a(Integer.valueOf(game2.q()), Integer.valueOf(game.q())) && j.a(Boolean.valueOf(game2.r()), Boolean.valueOf(game.r())) && j.a(Boolean.valueOf(game2.s()), Boolean.valueOf(game.s())) && j.a(Boolean.valueOf(game2.l()), Boolean.valueOf(game.l())) && j.a(Boolean.valueOf(game2.m()), Boolean.valueOf(game.m())) && j.a(Boolean.valueOf(game2.t()), Boolean.valueOf(game.t())) && j.a(game2.u(), game.u()) && j.a(Boolean.valueOf(game2.v()), Boolean.valueOf(game.v()));
    }

    static /* synthetic */ boolean a(String str) {
        return true;
    }

    static String b(Game game) {
        return j.a(game).a("ApplicationId", game.b()).a("DisplayName", game.c()).a("PrimaryCategory", game.d()).a("SecondaryCategory", game.e()).a("Description", game.f()).a("DeveloperName", game.g()).a("IconImageUri", game.h()).a("IconImageUrl", game.getIconImageUrl()).a("HiResImageUri", game.i()).a("HiResImageUrl", game.getHiResImageUrl()).a("FeaturedImageUri", game.j()).a("FeaturedImageUrl", game.getFeaturedImageUrl()).a("PlayEnabledGame", Boolean.valueOf(game.k())).a("InstanceInstalled", Boolean.valueOf(game.n())).a("InstancePackageName", game.o()).a("AchievementTotalCount", Integer.valueOf(game.p())).a("LeaderboardCount", Integer.valueOf(game.q())).a("RealTimeMultiplayerEnabled", Boolean.valueOf(game.r())).a("TurnBasedMultiplayerEnabled", Boolean.valueOf(game.s())).a("AreSnapshotsEnabled", Boolean.valueOf(game.t())).a("ThemeColor", game.u()).a("HasGamepadSupport", Boolean.valueOf(game.v())).toString();
    }

    public final /* bridge */ /* synthetic */ Object a() {
        return this;
    }

    public final String b() {
        return this.f1786b;
    }

    public final String c() {
        return this.c;
    }

    public final String d() {
        return this.d;
    }

    public final String e() {
        return this.e;
    }

    public final boolean equals(Object obj) {
        return a(this, obj);
    }

    public final String f() {
        return this.f;
    }

    public final String g() {
        return this.g;
    }

    public final String getFeaturedImageUrl() {
        return this.u;
    }

    public final String getHiResImageUrl() {
        return this.t;
    }

    public final String getIconImageUrl() {
        return this.s;
    }

    public final Uri h() {
        return this.h;
    }

    public final int hashCode() {
        return a(this);
    }

    public final Uri i() {
        return this.i;
    }

    public final Uri j() {
        return this.j;
    }

    public final boolean k() {
        return this.k;
    }

    public final boolean l() {
        return this.v;
    }

    public final boolean m() {
        return this.w;
    }

    public final boolean n() {
        return this.l;
    }

    public final String o() {
        return this.m;
    }

    public final int p() {
        return this.o;
    }

    public final int q() {
        return this.p;
    }

    public final boolean r() {
        return this.q;
    }

    public final boolean s() {
        return this.r;
    }

    public final boolean t() {
        return this.x;
    }

    public final String toString() {
        return b(this);
    }

    public final String u() {
        return this.y;
    }

    public final boolean v() {
        return this.z;
    }

    static int a(Game game) {
        return Arrays.hashCode(new Object[]{game.b(), game.c(), game.d(), game.e(), game.f(), game.g(), game.h(), game.i(), game.j(), Boolean.valueOf(game.k()), Boolean.valueOf(game.n()), game.o(), Integer.valueOf(game.p()), Integer.valueOf(game.q()), Boolean.valueOf(game.r()), Boolean.valueOf(game.s()), Boolean.valueOf(game.l()), Boolean.valueOf(game.m()), Boolean.valueOf(game.t()), game.u(), Boolean.valueOf(game.v())});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.net.Uri, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i2) {
        if (!this.f1575a) {
            int a2 = com.google.android.gms.common.internal.safeparcel.a.a(parcel, 20293);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 1, this.f1786b, false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 2, this.c, false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 3, this.d, false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 4, this.e, false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 5, this.f, false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 6, this.g, false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 7, (Parcelable) this.h, i2, false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 8, (Parcelable) this.i, i2, false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 9, (Parcelable) this.j, i2, false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 10, this.k);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 11, this.l);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 12, this.m, false);
            com.google.android.gms.common.internal.safeparcel.a.b(parcel, 13, this.n);
            com.google.android.gms.common.internal.safeparcel.a.b(parcel, 14, this.o);
            com.google.android.gms.common.internal.safeparcel.a.b(parcel, 15, this.p);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 16, this.q);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 17, this.r);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 18, getIconImageUrl(), false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 19, getHiResImageUrl(), false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 20, getFeaturedImageUrl(), false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 21, this.v);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 22, this.w);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 23, this.x);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 24, this.y, false);
            com.google.android.gms.common.internal.safeparcel.a.a(parcel, 25, this.z);
            com.google.android.gms.common.internal.safeparcel.a.b(parcel, a2);
            return;
        }
        parcel.writeString(this.f1786b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeString(this.f);
        parcel.writeString(this.g);
        Uri uri = this.h;
        String str = null;
        parcel.writeString(uri == null ? null : uri.toString());
        Uri uri2 = this.i;
        parcel.writeString(uri2 == null ? null : uri2.toString());
        Uri uri3 = this.j;
        if (uri3 != null) {
            str = uri3.toString();
        }
        parcel.writeString(str);
        parcel.writeInt(this.k ? 1 : 0);
        parcel.writeInt(this.l ? 1 : 0);
        parcel.writeString(this.m);
        parcel.writeInt(this.n);
        parcel.writeInt(this.o);
        parcel.writeInt(this.p);
    }
}
