package com.google.android.gms.measurement.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.google.android.gms.common.internal.l;

class x extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private static final String f2588a = x.class.getName();
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final du f2589b;
    private boolean c;
    private boolean d;

    x(du duVar) {
        l.a(duVar);
        this.f2589b = duVar;
    }

    public void onReceive(Context context, Intent intent) {
        this.f2589b.h();
        String action = intent.getAction();
        this.f2589b.q().k.a("NetworkBroadcastReceiver received action", action);
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
            boolean e = this.f2589b.c().e();
            if (this.d != e) {
                this.d = e;
                this.f2589b.p().a(new y(this, e));
                return;
            }
            return;
        }
        this.f2589b.q().f.a("NetworkBroadcastReceiver received unknown action", action);
    }

    public final void a() {
        this.f2589b.h();
        this.f2589b.p().c();
        if (!this.c) {
            this.f2589b.m().registerReceiver(this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            this.d = this.f2589b.c().e();
            this.f2589b.q().k.a("Registering connectivity change receiver. Network connected", Boolean.valueOf(this.d));
            this.c = true;
        }
    }

    public final void b() {
        this.f2589b.h();
        this.f2589b.p().c();
        this.f2589b.p().c();
        if (this.c) {
            this.f2589b.q().k.a("Unregistering connectivity change receiver");
            this.c = false;
            this.d = false;
            try {
                this.f2589b.m().unregisterReceiver(this);
            } catch (IllegalArgumentException e) {
                this.f2589b.q().c.a("Failed to unregister the network broadcast receiver", e);
            }
        }
    }
}
