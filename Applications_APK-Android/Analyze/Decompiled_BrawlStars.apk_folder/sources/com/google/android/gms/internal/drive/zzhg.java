package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.drive.d;
import com.google.android.gms.drive.e;
import com.google.android.gms.tasks.h;

public final class zzhg extends zzhb<d> {
    public final void a(zzfd zzfd) throws RemoteException {
        h<T> hVar = this.f1976a;
        e eVar = new e(zzfd.f1939a);
        hVar.a((Boolean) new e.a(eVar.f1706b, eVar.c, eVar.d));
    }

    public final void a(zzfu zzfu) throws RemoteException {
        this.f1976a.a((Boolean) zzfu.f1951a);
    }
}
