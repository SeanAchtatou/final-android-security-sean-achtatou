package com.google.android.gms.common;

import android.content.Context;
import android.os.RemoteException;
import android.os.StrictMode;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.internal.zzm;
import com.google.android.gms.common.internal.zzn;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.dynamite.DynamiteModule;
import javax.annotation.CheckReturnValue;

@CheckReturnValue
final class k {

    /* renamed from: a  reason: collision with root package name */
    private static volatile zzm f1626a;

    /* renamed from: b  reason: collision with root package name */
    private static final Object f1627b = new Object();
    private static Context c;

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0012, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static synchronized void a(android.content.Context r2) {
        /*
            java.lang.Class<com.google.android.gms.common.k> r0 = com.google.android.gms.common.k.class
            monitor-enter(r0)
            android.content.Context r1 = com.google.android.gms.common.k.c     // Catch:{ all -> 0x0013 }
            if (r1 != 0) goto L_0x0011
            if (r2 == 0) goto L_0x0011
            android.content.Context r2 = r2.getApplicationContext()     // Catch:{ all -> 0x0013 }
            com.google.android.gms.common.k.c = r2     // Catch:{ all -> 0x0013 }
            monitor-exit(r0)
            return
        L_0x0011:
            monitor-exit(r0)
            return
        L_0x0013:
            r2 = move-exception
            monitor-exit(r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.k.a(android.content.Context):void");
    }

    static u a(String str, m mVar, boolean z) {
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            return b(str, mVar, z);
        } finally {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        }
    }

    private static u b(String str, m mVar, boolean z) {
        try {
            if (f1626a == null) {
                l.a(c);
                synchronized (f1627b) {
                    if (f1626a == null) {
                        f1626a = zzn.a(DynamiteModule.a(c, DynamiteModule.c, "com.google.android.gms.googlecertificates").a("com.google.android.gms.common.GoogleCertificatesImpl"));
                    }
                }
            }
            l.a(c);
            try {
                if (f1626a.a(new zzk(str, mVar, z), ObjectWrapper.a(c.getPackageManager()))) {
                    return u.a();
                }
                return u.a(new l(z, str, mVar));
            } catch (RemoteException e) {
                return u.a("module call", e);
            }
        } catch (DynamiteModule.LoadingException e2) {
            String valueOf = String.valueOf(e2.getMessage());
            return u.a(valueOf.length() != 0 ? "module init: ".concat(valueOf) : new String("module init: "), e2);
        }
    }

    static final /* synthetic */ String a(boolean z, String str, m mVar) throws Exception {
        boolean z2 = true;
        if (z || !b(str, mVar, true).f1663a) {
            z2 = false;
        }
        return u.a(str, mVar, z, z2);
    }
}
