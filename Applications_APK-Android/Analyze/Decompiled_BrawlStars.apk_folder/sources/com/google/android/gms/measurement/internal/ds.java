package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.l;

class ds extends bm implements bo {

    /* renamed from: b  reason: collision with root package name */
    protected final du f2525b;

    public eo h() {
        return this.f2525b.d();
    }

    public ei g() {
        return this.f2525b.e();
    }

    public ea f() {
        return this.f2525b.f();
    }

    ds(du duVar) {
        super(duVar.f2528b);
        l.a(duVar);
        this.f2525b = duVar;
    }
}
