package com.google.android.gms.internal.drive;

import android.os.Bundle;
import android.support.v4.util.LongSparseArray;
import android.util.SparseArray;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.drive.metadata.CustomPropertyKey;
import com.google.android.gms.drive.metadata.internal.AppVisibleCustomProperties;
import com.google.android.gms.drive.metadata.internal.f;
import com.google.android.gms.drive.metadata.internal.k;
import com.google.android.gms.drive.metadata.internal.zzc;
import java.util.Arrays;

public class bg extends k<AppVisibleCustomProperties> {

    /* renamed from: a  reason: collision with root package name */
    public static final f f1895a = new bh();

    public bg() {
        super("customProperties", Arrays.asList("hasCustomProperties", "sqlId"), Arrays.asList("customPropertiesExtra", "customPropertiesExtraHolder"), 5000000);
    }

    public final /* synthetic */ Object c(DataHolder dataHolder, int i, int i2) {
        return a(dataHolder, i);
    }

    private static AppVisibleCustomProperties a(DataHolder dataHolder, int i) {
        String str;
        DataHolder dataHolder2 = dataHolder;
        Bundle bundle = dataHolder2.d;
        SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("customPropertiesExtra");
        if (sparseParcelableArray == null) {
            if (bundle.getParcelable("customPropertiesExtraHolder") != null) {
                synchronized (dataHolder) {
                    DataHolder dataHolder3 = (DataHolder) dataHolder2.d.getParcelable("customPropertiesExtraHolder");
                    if (dataHolder3 != null) {
                        try {
                            Bundle bundle2 = dataHolder3.d;
                            String string = bundle2.getString("entryIdColumn");
                            String string2 = bundle2.getString("keyColumn");
                            String string3 = bundle2.getString("visibilityColumn");
                            String string4 = bundle2.getString("valueColumn");
                            LongSparseArray longSparseArray = new LongSparseArray();
                            for (int i2 = 0; i2 < dataHolder3.f; i2++) {
                                int a2 = dataHolder3.a(i2);
                                long a3 = dataHolder3.a(string, i2, a2);
                                String c = dataHolder3.c(string2, i2, a2);
                                int b2 = dataHolder3.b(string3, i2, a2);
                                zzc zzc = new zzc(new CustomPropertyKey(c, b2), dataHolder3.c(string4, i2, a2));
                                AppVisibleCustomProperties.a aVar = (AppVisibleCustomProperties.a) longSparseArray.get(a3);
                                if (aVar == null) {
                                    aVar = new AppVisibleCustomProperties.a();
                                    longSparseArray.put(a3, aVar);
                                }
                                l.a(zzc, "property");
                                aVar.f1734a.put(zzc.f1741a, zzc);
                            }
                            SparseArray sparseArray = new SparseArray();
                            for (int i3 = 0; i3 < dataHolder2.f; i3++) {
                                AppVisibleCustomProperties.a aVar2 = (AppVisibleCustomProperties.a) longSparseArray.get(dataHolder2.a("sqlId", i3, dataHolder2.a(i3)));
                                if (aVar2 != null) {
                                    sparseArray.append(i3, aVar2.a());
                                }
                            }
                            dataHolder2.d.putSparseParcelableArray("customPropertiesExtra", sparseArray);
                        } finally {
                            dataHolder3.close();
                            str = "customPropertiesExtraHolder";
                            dataHolder2.d.remove(str);
                        }
                    }
                }
                sparseParcelableArray = bundle.getSparseParcelableArray("customPropertiesExtra");
            }
            if (sparseParcelableArray == null) {
                return AppVisibleCustomProperties.f1732a;
            }
        }
        return (AppVisibleCustomProperties) sparseParcelableArray.get(i, AppVisibleCustomProperties.f1732a);
    }

    static /* synthetic */ void a(DataHolder dataHolder) {
        Bundle bundle = dataHolder.d;
        if (bundle != null) {
            synchronized (dataHolder) {
                DataHolder dataHolder2 = (DataHolder) bundle.getParcelable("customPropertiesExtraHolder");
                if (dataHolder2 != null) {
                    dataHolder2.close();
                    bundle.remove("customPropertiesExtraHolder");
                }
            }
        }
    }
}
