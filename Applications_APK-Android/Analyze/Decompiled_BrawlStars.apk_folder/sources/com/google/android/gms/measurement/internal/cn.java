package com.google.android.gms.measurement.internal;

import android.os.RemoteException;
import java.util.concurrent.atomic.AtomicReference;

final class cn implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ AtomicReference f2472a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ zzk f2473b;
    private final /* synthetic */ cl c;

    cn(cl clVar, AtomicReference atomicReference, zzk zzk) {
        this.c = clVar;
        this.f2472a = atomicReference;
        this.f2473b = zzk;
    }

    public final void run() {
        synchronized (this.f2472a) {
            try {
                zzaj zzaj = this.c.f2470b;
                if (zzaj == null) {
                    this.c.q().c.a("Failed to get app instance id");
                    this.f2472a.notify();
                    return;
                }
                this.f2472a.set(zzaj.c(this.f2473b));
                String str = (String) this.f2472a.get();
                if (str != null) {
                    this.c.e().a(str);
                    this.c.r().k.a(str);
                }
                this.c.y();
                this.f2472a.notify();
            } catch (RemoteException e) {
                try {
                    this.c.q().c.a("Failed to get app instance id", e);
                    this.f2472a.notify();
                } catch (Throwable th) {
                    this.f2472a.notify();
                    throw th;
                }
            }
        }
    }
}
