package com.google.android.gms.internal.measurement;

public final class db {
    public static da a(da daVar) {
        da daVar2 = new da();
        daVar2.f2151a = daVar.f2151a;
        daVar2.k = (int[]) daVar.k.clone();
        if (daVar.l) {
            daVar2.l = daVar.l;
        }
        return daVar2;
    }
}
