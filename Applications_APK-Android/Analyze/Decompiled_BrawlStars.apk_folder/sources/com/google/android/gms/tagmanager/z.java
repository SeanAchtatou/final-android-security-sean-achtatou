package com.google.android.gms.tagmanager;

import android.content.Context;
import android.net.Uri;
import java.util.HashMap;
import java.util.Map;

public class z {

    /* renamed from: a  reason: collision with root package name */
    static Map<String, String> f2673a = new HashMap();

    /* renamed from: b  reason: collision with root package name */
    private static String f2674b;

    public static void a(String str) {
        synchronized (z.class) {
            f2674b = str;
        }
    }

    static void a(Context context, String str) {
        bd.a(context, "gtm_install_referrer", "referrer", str);
        b(context, str);
    }

    public static void b(Context context, String str) {
        String a2 = a(str, "conv");
        if (a2 != null && a2.length() > 0) {
            f2673a.put(a2, str);
            bd.a(context, "gtm_click_referrers", a2, str);
        }
    }

    private static String a(String str, String str2) {
        String valueOf = String.valueOf(str);
        return Uri.parse(valueOf.length() != 0 ? "http://hostname/?".concat(valueOf) : new String("http://hostname/?")).getQueryParameter(str2);
    }
}
