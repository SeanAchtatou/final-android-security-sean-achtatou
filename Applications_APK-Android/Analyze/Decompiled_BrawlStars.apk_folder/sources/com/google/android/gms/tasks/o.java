package com.google.android.gms.tasks;

import java.util.concurrent.Executor;

final class o<TResult> implements y<TResult> {

    /* renamed from: a  reason: collision with root package name */
    final Object f2691a = new Object();

    /* renamed from: b  reason: collision with root package name */
    b f2692b;
    private final Executor c;

    public o(Executor executor, b bVar) {
        this.c = executor;
        this.f2692b = bVar;
    }

    public final void a(g gVar) {
        if (gVar.c()) {
            synchronized (this.f2691a) {
                if (this.f2692b != null) {
                    this.c.execute(new p(this));
                }
            }
        }
    }

    public final void a() {
        synchronized (this.f2691a) {
            this.f2692b = null;
        }
    }
}
