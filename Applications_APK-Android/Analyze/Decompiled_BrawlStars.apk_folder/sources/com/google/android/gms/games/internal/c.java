package com.google.android.gms.games.internal;

import android.os.Bundle;
import android.os.IBinder;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    public IBinder f1805a;

    /* renamed from: b  reason: collision with root package name */
    public int f1806b;
    public int c;
    public int d;
    public int e;
    public int f;
    public int g;

    private c(int i, IBinder iBinder) {
        this.c = -1;
        this.d = 0;
        this.e = 0;
        this.f = 0;
        this.g = 0;
        this.f1806b = i;
        this.f1805a = iBinder;
    }

    /* synthetic */ c(int i, IBinder iBinder, byte b2) {
        this(i, iBinder);
    }

    public final Bundle a() {
        Bundle bundle = new Bundle();
        bundle.putInt("popupLocationInfo.gravity", this.f1806b);
        bundle.putInt("popupLocationInfo.displayId", this.c);
        bundle.putInt("popupLocationInfo.left", this.d);
        bundle.putInt("popupLocationInfo.top", this.e);
        bundle.putInt("popupLocationInfo.right", this.f);
        bundle.putInt("popupLocationInfo.bottom", this.g);
        return bundle;
    }
}
