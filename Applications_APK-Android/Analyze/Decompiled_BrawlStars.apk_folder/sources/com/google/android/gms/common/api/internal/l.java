package com.google.android.gms.common.api.internal;

import android.support.v4.util.ArrayMap;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.AvailabilityException;
import com.google.android.gms.tasks.c;
import com.google.android.gms.tasks.g;
import java.util.Collections;
import java.util.Map;

final class l implements c<Map<bs<?>, String>> {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ cg f1483a;

    public final void a(g<Map<bs<?>, String>> gVar) {
        this.f1483a.d.lock();
        try {
            if (this.f1483a.g) {
                if (gVar.b()) {
                    this.f1483a.i = new ArrayMap(this.f1483a.f1456b.size());
                    for (cf<?> cfVar : this.f1483a.f1456b.values()) {
                        this.f1483a.i.put(cfVar.f1374b, ConnectionResult.f1352a);
                    }
                } else if (gVar.e() instanceof AvailabilityException) {
                    AvailabilityException availabilityException = (AvailabilityException) gVar.e();
                    if (this.f1483a.f) {
                        this.f1483a.i = new ArrayMap(this.f1483a.f1456b.size());
                        for (cf next : this.f1483a.f1456b.values()) {
                            bs<O> bsVar = next.f1374b;
                            ConnectionResult a2 = availabilityException.a(next);
                            if (this.f1483a.a(next, a2)) {
                                this.f1483a.i.put(bsVar, new ConnectionResult(16));
                            } else {
                                this.f1483a.i.put(bsVar, a2);
                            }
                        }
                    } else {
                        this.f1483a.i = availabilityException.f1364a;
                    }
                } else {
                    this.f1483a.i = Collections.emptyMap();
                }
                if (this.f1483a.d()) {
                    this.f1483a.h.putAll(this.f1483a.i);
                    if (this.f1483a.h() == null) {
                        this.f1483a.c();
                        this.f1483a.g();
                        this.f1483a.e.signalAll();
                    }
                }
                this.f1483a.d.unlock();
            }
        } finally {
            this.f1483a.d.unlock();
        }
    }
}
