package com.google.android.gms.measurement.internal;

abstract class dt extends ds {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2526a;

    dt(du duVar) {
        super(duVar);
        this.f2525b.d++;
    }

    /* access modifiers changed from: protected */
    public abstract boolean d();

    /* access modifiers changed from: package-private */
    public final boolean i() {
        return this.f2526a;
    }

    /* access modifiers changed from: protected */
    public final void j() {
        if (!i()) {
            throw new IllegalStateException("Not initialized");
        }
    }

    public final void t() {
        if (!this.f2526a) {
            d();
            this.f2525b.e++;
            this.f2526a = true;
            return;
        }
        throw new IllegalStateException("Can't initialize twice");
    }
}
