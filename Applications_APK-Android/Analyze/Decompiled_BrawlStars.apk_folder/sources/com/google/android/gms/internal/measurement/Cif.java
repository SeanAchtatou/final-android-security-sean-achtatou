package com.google.android.gms.internal.measurement;

import java.util.ListIterator;

/* renamed from: com.google.android.gms.internal.measurement.if  reason: invalid class name */
final class Cif implements ListIterator<String> {

    /* renamed from: a  reason: collision with root package name */
    private ListIterator<String> f2285a = this.c.f2284a.listIterator(this.f2286b);

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ int f2286b;
    private final /* synthetic */ ie c;

    Cif(ie ieVar, int i) {
        this.c = ieVar;
        this.f2286b = i;
    }

    public final boolean hasNext() {
        return this.f2285a.hasNext();
    }

    public final boolean hasPrevious() {
        return this.f2285a.hasPrevious();
    }

    public final int nextIndex() {
        return this.f2285a.nextIndex();
    }

    public final int previousIndex() {
        return this.f2285a.previousIndex();
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }

    public final /* synthetic */ void add(Object obj) {
        throw new UnsupportedOperationException();
    }

    public final /* synthetic */ void set(Object obj) {
        throw new UnsupportedOperationException();
    }

    public final /* synthetic */ Object previous() {
        return this.f2285a.previous();
    }

    public final /* synthetic */ Object next() {
        return this.f2285a.next();
    }
}
