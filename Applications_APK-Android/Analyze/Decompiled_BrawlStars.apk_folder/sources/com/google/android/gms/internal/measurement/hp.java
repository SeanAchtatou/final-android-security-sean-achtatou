package com.google.android.gms.internal.measurement;

import java.util.Iterator;
import java.util.Map;

final class hp extends hv {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ hm f2269a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private hp(hm hmVar) {
        super(hmVar, (byte) 0);
        this.f2269a = hmVar;
    }

    public final Iterator<Map.Entry<K, V>> iterator() {
        return new ho(this.f2269a, (byte) 0);
    }

    /* synthetic */ hp(hm hmVar, byte b2) {
        this(hmVar);
    }
}
