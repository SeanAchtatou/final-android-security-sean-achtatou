package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class cm extends iz<cm> {
    private static volatile cm[] f;

    /* renamed from: a  reason: collision with root package name */
    public Integer f2122a = null;

    /* renamed from: b  reason: collision with root package name */
    public String f2123b = null;
    public ck c = null;
    public Boolean d = null;
    public Boolean e = null;

    public static cm[] a() {
        if (f == null) {
            synchronized (jd.f2312b) {
                if (f == null) {
                    f = new cm[0];
                }
            }
        }
        return f;
    }

    public cm() {
        this.L = null;
        this.M = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof cm)) {
            return false;
        }
        cm cmVar = (cm) obj;
        Integer num = this.f2122a;
        if (num == null) {
            if (cmVar.f2122a != null) {
                return false;
            }
        } else if (!num.equals(cmVar.f2122a)) {
            return false;
        }
        String str = this.f2123b;
        if (str == null) {
            if (cmVar.f2123b != null) {
                return false;
            }
        } else if (!str.equals(cmVar.f2123b)) {
            return false;
        }
        ck ckVar = this.c;
        if (ckVar == null) {
            if (cmVar.c != null) {
                return false;
            }
        } else if (!ckVar.equals(cmVar.c)) {
            return false;
        }
        Boolean bool = this.d;
        if (bool == null) {
            if (cmVar.d != null) {
                return false;
            }
        } else if (!bool.equals(cmVar.d)) {
            return false;
        }
        Boolean bool2 = this.e;
        if (bool2 == null) {
            if (cmVar.e != null) {
                return false;
            }
        } else if (!bool2.equals(cmVar.e)) {
            return false;
        }
        if (this.L == null || this.L.a()) {
            return cmVar.L == null || cmVar.L.a();
        }
        return this.L.equals(cmVar.L);
    }

    public final int hashCode() {
        int i;
        int hashCode = (getClass().getName().hashCode() + 527) * 31;
        Integer num = this.f2122a;
        int i2 = 0;
        int hashCode2 = (hashCode + (num == null ? 0 : num.hashCode())) * 31;
        String str = this.f2123b;
        int hashCode3 = hashCode2 + (str == null ? 0 : str.hashCode());
        ck ckVar = this.c;
        int i3 = hashCode3 * 31;
        if (ckVar == null) {
            i = 0;
        } else {
            i = ckVar.hashCode();
        }
        int i4 = (i3 + i) * 31;
        Boolean bool = this.d;
        int hashCode4 = (i4 + (bool == null ? 0 : bool.hashCode())) * 31;
        Boolean bool2 = this.e;
        int hashCode5 = (hashCode4 + (bool2 == null ? 0 : bool2.hashCode())) * 31;
        if (this.L != null && !this.L.a()) {
            i2 = this.L.hashCode();
        }
        return hashCode5 + i2;
    }

    public final void a(iy iyVar) throws IOException {
        Integer num = this.f2122a;
        if (num != null) {
            iyVar.a(1, num.intValue());
        }
        String str = this.f2123b;
        if (str != null) {
            iyVar.a(2, str);
        }
        ck ckVar = this.c;
        if (ckVar != null) {
            iyVar.a(3, ckVar);
        }
        Boolean bool = this.d;
        if (bool != null) {
            iyVar.a(4, bool.booleanValue());
        }
        Boolean bool2 = this.e;
        if (bool2 != null) {
            iyVar.a(5, bool2.booleanValue());
        }
        super.a(iyVar);
    }

    /* access modifiers changed from: protected */
    public final int b() {
        int b2 = super.b();
        Integer num = this.f2122a;
        if (num != null) {
            b2 += iy.b(1, num.intValue());
        }
        String str = this.f2123b;
        if (str != null) {
            b2 += iy.b(2, str);
        }
        ck ckVar = this.c;
        if (ckVar != null) {
            b2 += iy.b(3, ckVar);
        }
        Boolean bool = this.d;
        if (bool != null) {
            bool.booleanValue();
            b2 += iy.c(32) + 1;
        }
        Boolean bool2 = this.e;
        if (bool2 == null) {
            return b2;
        }
        bool2.booleanValue();
        return b2 + iy.c(40) + 1;
    }

    public final /* synthetic */ je a(iw iwVar) throws IOException {
        while (true) {
            int a2 = iwVar.a();
            if (a2 == 0) {
                return this;
            }
            if (a2 == 8) {
                this.f2122a = Integer.valueOf(iwVar.d());
            } else if (a2 == 18) {
                this.f2123b = iwVar.c();
            } else if (a2 == 26) {
                if (this.c == null) {
                    this.c = new ck();
                }
                iwVar.a(this.c);
            } else if (a2 == 32) {
                this.d = Boolean.valueOf(iwVar.b());
            } else if (a2 == 40) {
                this.e = Boolean.valueOf(iwVar.b());
            } else if (!super.a(iwVar, a2)) {
                return this;
            }
        }
    }
}
