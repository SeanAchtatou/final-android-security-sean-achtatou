package com.google.android.gms.internal.instantapps;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class g implements Parcelable.Creator<zzaq> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zzaq[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        int i = 0;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    str = SafeParcelReader.l(parcel, readInt);
                    break;
                case 3:
                    i = SafeParcelReader.d(parcel, readInt);
                    break;
                case 4:
                    str2 = SafeParcelReader.l(parcel, readInt);
                    break;
                case 5:
                    str3 = SafeParcelReader.l(parcel, readInt);
                    break;
                case 6:
                    str4 = SafeParcelReader.l(parcel, readInt);
                    break;
                case 7:
                    str6 = SafeParcelReader.l(parcel, readInt);
                    break;
                case 8:
                    str7 = SafeParcelReader.l(parcel, readInt);
                    break;
                case 9:
                    str5 = SafeParcelReader.l(parcel, readInt);
                    break;
                default:
                    SafeParcelReader.b(parcel, readInt);
                    break;
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new zzaq(str, i, str2, str3, str4, str5, str6, str7);
    }
}
