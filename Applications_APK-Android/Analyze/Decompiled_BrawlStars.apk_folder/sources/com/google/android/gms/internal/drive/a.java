package com.google.android.gms.internal.drive;

import android.os.Bundle;
import com.google.android.gms.drive.b;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public final class a extends b {

    /* renamed from: a  reason: collision with root package name */
    private final MetadataBundle f1888a;

    public a(MetadataBundle metadataBundle) {
        this.f1888a = metadataBundle;
    }

    public final <T> T a(com.google.android.gms.drive.metadata.a<T> aVar) {
        return this.f1888a.a(aVar);
    }

    public final String toString() {
        String valueOf = String.valueOf(this.f1888a);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 17);
        sb.append("Metadata [mImpl=");
        sb.append(valueOf);
        sb.append("]");
        return sb.toString();
    }

    public final /* synthetic */ Object a() {
        return new a(new MetadataBundle(new Bundle(this.f1888a.f1736a)));
    }
}
