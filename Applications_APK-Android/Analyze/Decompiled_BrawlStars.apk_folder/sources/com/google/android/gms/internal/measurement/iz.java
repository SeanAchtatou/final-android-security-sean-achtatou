package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.iz;
import java.io.IOException;

public abstract class iz<M extends iz<M>> extends je {
    protected jb L;

    /* access modifiers changed from: protected */
    public int b() {
        if (this.L == null) {
            return 0;
        }
        int i = 0;
        for (int i2 = 0; i2 < this.L.d; i2++) {
            i += this.L.c[i2].a();
        }
        return i;
    }

    public void a(iy iyVar) throws IOException {
        if (this.L != null) {
            for (int i = 0; i < this.L.d; i++) {
                this.L.c[i].a(iyVar);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00ce  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(com.google.android.gms.internal.measurement.iw r11, int r12) throws java.io.IOException {
        /*
            r10 = this;
            int r0 = r11.i()
            boolean r1 = r11.b(r12)
            r2 = 0
            if (r1 != 0) goto L_0x000c
            return r2
        L_0x000c:
            int r1 = r12 >>> 3
            int r3 = r11.i()
            int r3 = r3 - r0
            if (r3 != 0) goto L_0x0018
            byte[] r11 = com.google.android.gms.internal.measurement.jh.d
            goto L_0x0023
        L_0x0018:
            byte[] r4 = new byte[r3]
            int r5 = r11.f2298b
            int r5 = r5 + r0
            byte[] r11 = r11.f2297a
            java.lang.System.arraycopy(r11, r5, r4, r2, r3)
            r11 = r4
        L_0x0023:
            com.google.android.gms.internal.measurement.jg r0 = new com.google.android.gms.internal.measurement.jg
            r0.<init>(r12, r11)
            com.google.android.gms.internal.measurement.jb r11 = r10.L
            r12 = 0
            if (r11 != 0) goto L_0x0036
            com.google.android.gms.internal.measurement.jb r11 = new com.google.android.gms.internal.measurement.jb
            r11.<init>()
            r10.L = r11
        L_0x0034:
            r11 = r12
            goto L_0x0049
        L_0x0036:
            int r3 = r11.b(r1)
            if (r3 < 0) goto L_0x0034
            com.google.android.gms.internal.measurement.jc[] r4 = r11.c
            r4 = r4[r3]
            com.google.android.gms.internal.measurement.jc r5 = com.google.android.gms.internal.measurement.jb.f2307a
            if (r4 != r5) goto L_0x0045
            goto L_0x0034
        L_0x0045:
            com.google.android.gms.internal.measurement.jc[] r11 = r11.c
            r11 = r11[r3]
        L_0x0049:
            r3 = 1
            if (r11 != 0) goto L_0x00c3
            com.google.android.gms.internal.measurement.jc r11 = new com.google.android.gms.internal.measurement.jc
            r11.<init>()
            com.google.android.gms.internal.measurement.jb r4 = r10.L
            int r5 = r4.b(r1)
            if (r5 < 0) goto L_0x005e
            com.google.android.gms.internal.measurement.jc[] r1 = r4.c
            r1[r5] = r11
            goto L_0x00c3
        L_0x005e:
            r5 = r5 ^ -1
            int r6 = r4.d
            if (r5 >= r6) goto L_0x0075
            com.google.android.gms.internal.measurement.jc[] r6 = r4.c
            r6 = r6[r5]
            com.google.android.gms.internal.measurement.jc r7 = com.google.android.gms.internal.measurement.jb.f2307a
            if (r6 != r7) goto L_0x0075
            int[] r6 = r4.f2308b
            r6[r5] = r1
            com.google.android.gms.internal.measurement.jc[] r1 = r4.c
            r1[r5] = r11
            goto L_0x00c3
        L_0x0075:
            int r6 = r4.d
            int[] r7 = r4.f2308b
            int r7 = r7.length
            if (r6 < r7) goto L_0x009b
            int r6 = r4.d
            int r6 = r6 + r3
            int r6 = com.google.android.gms.internal.measurement.jb.a(r6)
            int[] r7 = new int[r6]
            com.google.android.gms.internal.measurement.jc[] r6 = new com.google.android.gms.internal.measurement.jc[r6]
            int[] r8 = r4.f2308b
            int[] r9 = r4.f2308b
            int r9 = r9.length
            java.lang.System.arraycopy(r8, r2, r7, r2, r9)
            com.google.android.gms.internal.measurement.jc[] r8 = r4.c
            com.google.android.gms.internal.measurement.jc[] r9 = r4.c
            int r9 = r9.length
            java.lang.System.arraycopy(r8, r2, r6, r2, r9)
            r4.f2308b = r7
            r4.c = r6
        L_0x009b:
            int r6 = r4.d
            int r6 = r6 - r5
            if (r6 == 0) goto L_0x00b6
            int[] r6 = r4.f2308b
            int[] r7 = r4.f2308b
            int r8 = r5 + 1
            int r9 = r4.d
            int r9 = r9 - r5
            java.lang.System.arraycopy(r6, r5, r7, r8, r9)
            com.google.android.gms.internal.measurement.jc[] r6 = r4.c
            com.google.android.gms.internal.measurement.jc[] r7 = r4.c
            int r9 = r4.d
            int r9 = r9 - r5
            java.lang.System.arraycopy(r6, r5, r7, r8, r9)
        L_0x00b6:
            int[] r6 = r4.f2308b
            r6[r5] = r1
            com.google.android.gms.internal.measurement.jc[] r1 = r4.c
            r1[r5] = r11
            int r1 = r4.d
            int r1 = r1 + r3
            r4.d = r1
        L_0x00c3:
            java.util.List<com.google.android.gms.internal.measurement.jg> r1 = r11.c
            if (r1 == 0) goto L_0x00ce
            java.util.List<com.google.android.gms.internal.measurement.jg> r11 = r11.c
            r11.add(r0)
            goto L_0x0176
        L_0x00ce:
            java.lang.Object r1 = r11.f2310b
            boolean r1 = r1 instanceof com.google.android.gms.internal.measurement.je
            if (r1 == 0) goto L_0x00f6
            byte[] r0 = r0.f2314b
            int r1 = r0.length
            com.google.android.gms.internal.measurement.iw r1 = com.google.android.gms.internal.measurement.iw.a(r0, r1)
            int r2 = r1.d()
            int r0 = r0.length
            int r4 = com.google.android.gms.internal.measurement.iy.a(r2)
            int r0 = r0 - r4
            if (r2 != r0) goto L_0x00f1
            java.lang.Object r0 = r11.f2310b
            com.google.android.gms.internal.measurement.je r0 = (com.google.android.gms.internal.measurement.je) r0
            com.google.android.gms.internal.measurement.je r0 = r0.a(r1)
            goto L_0x016e
        L_0x00f1:
            com.google.android.gms.internal.measurement.zzyh r11 = com.google.android.gms.internal.measurement.zzyh.a()
            throw r11
        L_0x00f6:
            java.lang.Object r1 = r11.f2310b
            boolean r1 = r1 instanceof com.google.android.gms.internal.measurement.je[]
            if (r1 == 0) goto L_0x011c
            com.google.android.gms.internal.measurement.ja<?, ?> r1 = r11.f2309a
            java.util.List r0 = java.util.Collections.singletonList(r0)
            java.lang.Object r0 = r1.a(r0)
            com.google.android.gms.internal.measurement.je[] r0 = (com.google.android.gms.internal.measurement.je[]) r0
            java.lang.Object r1 = r11.f2310b
            com.google.android.gms.internal.measurement.je[] r1 = (com.google.android.gms.internal.measurement.je[]) r1
            int r4 = r1.length
            int r5 = r0.length
            int r4 = r4 + r5
            java.lang.Object[] r4 = java.util.Arrays.copyOf(r1, r4)
            com.google.android.gms.internal.measurement.je[] r4 = (com.google.android.gms.internal.measurement.je[]) r4
            int r1 = r1.length
            int r5 = r0.length
            java.lang.System.arraycopy(r0, r2, r4, r1, r5)
        L_0x011a:
            r0 = r4
            goto L_0x016e
        L_0x011c:
            java.lang.Object r1 = r11.f2310b
            boolean r1 = r1 instanceof com.google.android.gms.internal.measurement.gt
            if (r1 == 0) goto L_0x013f
            com.google.android.gms.internal.measurement.ja<?, ?> r1 = r11.f2309a
            java.util.List r0 = java.util.Collections.singletonList(r0)
            java.lang.Object r0 = r1.a(r0)
            com.google.android.gms.internal.measurement.gt r0 = (com.google.android.gms.internal.measurement.gt) r0
            java.lang.Object r1 = r11.f2310b
            com.google.android.gms.internal.measurement.gt r1 = (com.google.android.gms.internal.measurement.gt) r1
            com.google.android.gms.internal.measurement.gu r1 = r1.i()
            com.google.android.gms.internal.measurement.gu r0 = r1.a(r0)
            com.google.android.gms.internal.measurement.gt r0 = r0.d()
            goto L_0x016e
        L_0x013f:
            java.lang.Object r1 = r11.f2310b
            boolean r1 = r1 instanceof com.google.android.gms.internal.measurement.gt[]
            if (r1 == 0) goto L_0x0164
            com.google.android.gms.internal.measurement.ja<?, ?> r1 = r11.f2309a
            java.util.List r0 = java.util.Collections.singletonList(r0)
            java.lang.Object r0 = r1.a(r0)
            com.google.android.gms.internal.measurement.gt[] r0 = (com.google.android.gms.internal.measurement.gt[]) r0
            java.lang.Object r1 = r11.f2310b
            com.google.android.gms.internal.measurement.gt[] r1 = (com.google.android.gms.internal.measurement.gt[]) r1
            int r4 = r1.length
            int r5 = r0.length
            int r4 = r4 + r5
            java.lang.Object[] r4 = java.util.Arrays.copyOf(r1, r4)
            com.google.android.gms.internal.measurement.gt[] r4 = (com.google.android.gms.internal.measurement.gt[]) r4
            int r1 = r1.length
            int r5 = r0.length
            java.lang.System.arraycopy(r0, r2, r4, r1, r5)
            goto L_0x011a
        L_0x0164:
            com.google.android.gms.internal.measurement.ja<?, ?> r1 = r11.f2309a
            java.util.List r0 = java.util.Collections.singletonList(r0)
            java.lang.Object r0 = r1.a(r0)
        L_0x016e:
            com.google.android.gms.internal.measurement.ja<?, ?> r1 = r11.f2309a
            r11.f2309a = r1
            r11.f2310b = r0
            r11.c = r12
        L_0x0176:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.iz.a(com.google.android.gms.internal.measurement.iw, int):boolean");
    }

    public final /* synthetic */ je c() throws CloneNotSupportedException {
        return (iz) clone();
    }

    public /* synthetic */ Object clone() throws CloneNotSupportedException {
        iz izVar = (iz) super.clone();
        jd.a(this, izVar);
        return izVar;
    }
}
