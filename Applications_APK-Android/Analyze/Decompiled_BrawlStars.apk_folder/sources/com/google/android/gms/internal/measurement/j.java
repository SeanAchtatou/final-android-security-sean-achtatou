package com.google.android.gms.internal.measurement;

import android.text.TextUtils;
import com.google.android.gms.analytics.l;
import java.util.HashMap;

public final class j extends l<j> {

    /* renamed from: a  reason: collision with root package name */
    public String f2303a;

    /* renamed from: b  reason: collision with root package name */
    public long f2304b;
    public String c;
    public String d;

    public final String toString() {
        HashMap hashMap = new HashMap();
        hashMap.put("variableName", this.f2303a);
        hashMap.put("timeInMillis", Long.valueOf(this.f2304b));
        hashMap.put("category", this.c);
        hashMap.put("label", this.d);
        return a((Object) hashMap);
    }

    public final /* synthetic */ void a(l lVar) {
        j jVar = (j) lVar;
        if (!TextUtils.isEmpty(this.f2303a)) {
            jVar.f2303a = this.f2303a;
        }
        long j = this.f2304b;
        if (j != 0) {
            jVar.f2304b = j;
        }
        if (!TextUtils.isEmpty(this.c)) {
            jVar.c = this.c;
        }
        if (!TextUtils.isEmpty(this.d)) {
            jVar.d = this.d;
        }
    }
}
