package com.google.android.gms.internal.drive;

public final class bu implements Cloneable {
    private static final bv c = new bv();

    /* renamed from: a  reason: collision with root package name */
    bv[] f1903a;

    /* renamed from: b  reason: collision with root package name */
    int f1904b;
    private boolean d;
    private int[] e;

    bu() {
        this(10);
    }

    private bu(int i) {
        this.d = false;
        int a2 = a(i);
        this.e = new int[a2];
        this.f1903a = new bv[a2];
        this.f1904b = 0;
    }

    private static int a(int i) {
        int i2 = i << 2;
        int i3 = 4;
        while (true) {
            if (i3 >= 32) {
                break;
            }
            int i4 = (1 << i3) - 12;
            if (i2 <= i4) {
                i2 = i4;
                break;
            }
            i3++;
        }
        return i2 / 4;
    }

    public final boolean a() {
        return this.f1904b == 0;
    }

    public final /* synthetic */ Object clone() throws CloneNotSupportedException {
        int i = this.f1904b;
        bu buVar = new bu(i);
        System.arraycopy(this.e, 0, buVar.e, 0, i);
        for (int i2 = 0; i2 < i; i2++) {
            bv[] bvVarArr = this.f1903a;
            if (bvVarArr[i2] != null) {
                buVar.f1903a[i2] = (bv) bvVarArr[i2].clone();
            }
        }
        buVar.f1904b = i;
        return buVar;
    }

    public final boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof bu)) {
            return false;
        }
        bu buVar = (bu) obj;
        int i = this.f1904b;
        if (i != buVar.f1904b) {
            return false;
        }
        int[] iArr = this.e;
        int[] iArr2 = buVar.e;
        int i2 = 0;
        while (true) {
            if (i2 >= i) {
                z = true;
                break;
            } else if (iArr[i2] != iArr2[i2]) {
                z = false;
                break;
            } else {
                i2++;
            }
        }
        if (z) {
            bv[] bvVarArr = this.f1903a;
            bv[] bvVarArr2 = buVar.f1903a;
            int i3 = this.f1904b;
            int i4 = 0;
            while (true) {
                if (i4 >= i3) {
                    z2 = true;
                    break;
                } else if (!bvVarArr[i4].equals(bvVarArr2[i4])) {
                    z2 = false;
                    break;
                } else {
                    i4++;
                }
            }
            if (z2) {
                return true;
            }
        }
        return false;
    }

    public final int hashCode() {
        int i = 17;
        for (int i2 = 0; i2 < this.f1904b; i2++) {
            i = (((i * 31) + this.e[i2]) * 31) + this.f1903a[i2].hashCode();
        }
        return i;
    }
}
