package com.google.android.gms.common.api.internal;

import android.app.Activity;
import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.res.Configuration;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public final class b implements Application.ActivityLifecycleCallbacks, ComponentCallbacks2 {
    private static final b c = new b();

    /* renamed from: a  reason: collision with root package name */
    final AtomicBoolean f1412a = new AtomicBoolean();

    /* renamed from: b  reason: collision with root package name */
    final AtomicBoolean f1413b = new AtomicBoolean();
    private final ArrayList<a> d = new ArrayList<>();
    private boolean e = false;

    public interface a {
        void a(boolean z);
    }

    private b() {
    }

    public final void onActivityDestroyed(Activity activity) {
    }

    public final void onActivityPaused(Activity activity) {
    }

    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public final void onActivityStarted(Activity activity) {
    }

    public final void onActivityStopped(Activity activity) {
    }

    public final void onConfigurationChanged(Configuration configuration) {
    }

    public final void onLowMemory() {
    }

    public static b a() {
        return c;
    }

    public static void a(Application application) {
        synchronized (c) {
            if (!c.e) {
                application.registerActivityLifecycleCallbacks(c);
                application.registerComponentCallbacks(c);
                c.e = true;
            }
        }
    }

    public final void a(a aVar) {
        synchronized (c) {
            this.d.add(aVar);
        }
    }

    public final void onActivityCreated(Activity activity, Bundle bundle) {
        boolean compareAndSet = this.f1412a.compareAndSet(true, false);
        this.f1413b.set(true);
        if (compareAndSet) {
            a(false);
        }
    }

    public final void onActivityResumed(Activity activity) {
        boolean compareAndSet = this.f1412a.compareAndSet(true, false);
        this.f1413b.set(true);
        if (compareAndSet) {
            a(false);
        }
    }

    public final void onTrimMemory(int i) {
        if (i == 20 && this.f1412a.compareAndSet(false, true)) {
            this.f1413b.set(true);
            a(true);
        }
    }

    private final void a(boolean z) {
        synchronized (c) {
            ArrayList arrayList = this.d;
            int size = arrayList.size();
            int i = 0;
            while (i < size) {
                Object obj = arrayList.get(i);
                i++;
                ((a) obj).a(z);
            }
        }
    }
}
