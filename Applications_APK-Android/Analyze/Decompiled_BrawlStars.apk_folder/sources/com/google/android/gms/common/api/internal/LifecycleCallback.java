package com.google.android.gms.common.api.internal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class LifecycleCallback {

    /* renamed from: a  reason: collision with root package name */
    protected final g f1381a;

    private static g getChimeraLifecycleFragmentImpl(f fVar) {
        throw new IllegalStateException("Method not available in SDK.");
    }

    public void a(int i, int i2, Intent intent) {
    }

    public void a(Bundle bundle) {
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
    }

    public void b() {
    }

    public void b(Bundle bundle) {
    }

    public void c() {
    }

    public void d() {
    }

    public static g a(Activity activity) {
        return a(new f(activity));
    }

    protected LifecycleCallback(g gVar) {
        this.f1381a = gVar;
    }

    public final Activity a() {
        return this.f1381a.a();
    }

    protected static g a(f fVar) {
        if (fVar.f1476a instanceof FragmentActivity) {
            return ck.a((FragmentActivity) fVar.f1476a);
        }
        if (fVar.f1476a instanceof Activity) {
            return ci.a((Activity) fVar.f1476a);
        }
        throw new IllegalArgumentException("Can't get fragment for unexpected activity.");
    }
}
