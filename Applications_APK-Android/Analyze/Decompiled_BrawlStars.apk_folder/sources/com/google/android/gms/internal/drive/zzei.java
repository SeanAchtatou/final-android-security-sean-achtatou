package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.drive.a;

@Deprecated
public final class zzei extends AbstractSafeParcelable implements a {
    public static final Parcelable.Creator<zzei> CREATOR = new n();

    /* renamed from: a  reason: collision with root package name */
    private int f1928a;

    /* renamed from: b  reason: collision with root package name */
    private int f1929b;
    private boolean c;

    public zzei(int i, int i2, boolean z) {
        this.f1928a = i;
        this.f1929b = i2;
        this.c = z;
    }

    public final boolean b() {
        return this.c;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = com.google.android.gms.common.internal.safeparcel.a.a(parcel, 20293);
        com.google.android.gms.common.internal.safeparcel.a.b(parcel, 2, this.f1928a);
        com.google.android.gms.common.internal.safeparcel.a.b(parcel, 3, this.f1929b);
        com.google.android.gms.common.internal.safeparcel.a.a(parcel, 4, this.c);
        com.google.android.gms.common.internal.safeparcel.a.b(parcel, a2);
    }

    public final int a() {
        int i = this.f1928a;
        boolean z = true;
        if (!(i == 1 || i == 2)) {
            z = false;
        }
        if (!z) {
            return 0;
        }
        return this.f1928a;
    }

    public final int c() {
        int i = this.f1929b;
        if (!(i == 256 || i == 257)) {
            return 0;
        }
        return this.f1929b;
    }
}
