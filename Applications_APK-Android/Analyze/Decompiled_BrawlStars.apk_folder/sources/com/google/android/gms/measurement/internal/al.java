package com.google.android.gms.measurement.internal;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.util.e;
import com.google.android.gms.internal.measurement.ci;
import com.google.android.gms.internal.measurement.cj;
import com.google.android.gms.internal.measurement.ck;
import com.google.android.gms.internal.measurement.cm;
import com.google.android.gms.internal.measurement.co;
import com.google.android.gms.internal.measurement.cp;
import com.google.android.gms.internal.measurement.cq;
import com.google.android.gms.internal.measurement.iw;
import com.google.android.gms.internal.measurement.iy;
import java.io.IOException;
import java.util.Map;

public final class al extends dt implements en {

    /* renamed from: a  reason: collision with root package name */
    private static int f2378a = 65535;
    private static int c = 2;
    private final Map<String, Map<String, String>> d = new ArrayMap();
    private final Map<String, Map<String, Boolean>> e = new ArrayMap();
    private final Map<String, Map<String, Boolean>> f = new ArrayMap();
    private final Map<String, cp> g = new ArrayMap();
    private final Map<String, Map<String, Integer>> h = new ArrayMap();
    private final Map<String, String> i = new ArrayMap();

    al(du duVar) {
        super(duVar);
    }

    /* access modifiers changed from: protected */
    public final boolean d() {
        return false;
    }

    private final void h(String str) {
        j();
        c();
        l.a(str);
        if (this.g.get(str) == null) {
            byte[] d2 = h().d(str);
            if (d2 == null) {
                this.d.put(str, null);
                this.e.put(str, null);
                this.f.put(str, null);
                this.g.put(str, null);
                this.i.put(str, null);
                this.h.put(str, null);
                return;
            }
            cp a2 = a(str, d2);
            this.d.put(str, a(a2));
            a(str, a2);
            this.g.put(str, a2);
            this.i.put(str, null);
        }
    }

    /* access modifiers changed from: protected */
    public final cp a(String str) {
        j();
        c();
        l.a(str);
        h(str);
        return this.g.get(str);
    }

    /* access modifiers changed from: protected */
    public final String b(String str) {
        c();
        return this.i.get(str);
    }

    /* access modifiers changed from: protected */
    public final void c(String str) {
        c();
        this.i.put(str, null);
    }

    /* access modifiers changed from: package-private */
    public final void d(String str) {
        c();
        this.g.remove(str);
    }

    public final String a(String str, String str2) {
        c();
        h(str);
        Map map = this.d.get(str);
        if (map != null) {
            return (String) map.get(str2);
        }
        return null;
    }

    private static Map<String, String> a(cp cpVar) {
        ArrayMap arrayMap = new ArrayMap();
        if (!(cpVar == null || cpVar.c == null)) {
            for (cq cqVar : cpVar.c) {
                if (cqVar != null) {
                    arrayMap.put(cqVar.f2130a, cqVar.f2131b);
                }
            }
        }
        return arrayMap;
    }

    private final void a(String str, cp cpVar) {
        ArrayMap arrayMap = new ArrayMap();
        ArrayMap arrayMap2 = new ArrayMap();
        ArrayMap arrayMap3 = new ArrayMap();
        if (!(cpVar == null || cpVar.d == null)) {
            for (co coVar : cpVar.d) {
                if (TextUtils.isEmpty(coVar.f2126a)) {
                    q().f.a("EventConfig contained null event name");
                } else {
                    String a2 = bp.a(coVar.f2126a);
                    if (!TextUtils.isEmpty(a2)) {
                        coVar.f2126a = a2;
                    }
                    arrayMap.put(coVar.f2126a, coVar.f2127b);
                    arrayMap2.put(coVar.f2126a, coVar.c);
                    if (coVar.d != null) {
                        if (coVar.d.intValue() < c || coVar.d.intValue() > f2378a) {
                            q().f.a("Invalid sampling rate. Event name, sample rate", coVar.f2126a, coVar.d);
                        } else {
                            arrayMap3.put(coVar.f2126a, coVar.d);
                        }
                    }
                }
            }
        }
        this.e.put(str, arrayMap);
        this.f.put(str, arrayMap2);
        this.h.put(str, arrayMap3);
    }

    /* access modifiers changed from: protected */
    public final boolean a(String str, byte[] bArr, String str2) {
        byte[] bArr2;
        String str3 = str;
        j();
        c();
        l.a(str);
        cp a2 = a(str, bArr);
        a(str3, a2);
        this.g.put(str3, a2);
        this.i.put(str3, str2);
        this.d.put(str3, a(a2));
        ei g2 = g();
        ci[] ciVarArr = a2.e;
        l.a(ciVarArr);
        for (ci ciVar : ciVarArr) {
            for (cj cjVar : ciVar.c) {
                String a3 = bp.a(cjVar.f2117b);
                if (a3 != null) {
                    cjVar.f2117b = a3;
                }
                for (ck ckVar : cjVar.c) {
                    String a4 = bq.a(ckVar.d);
                    if (a4 != null) {
                        ckVar.d = a4;
                    }
                }
            }
            for (cm cmVar : ciVar.f2115b) {
                String a5 = br.a(cmVar.f2123b);
                if (a5 != null) {
                    cmVar.f2123b = a5;
                }
            }
        }
        g2.h().a(str3, ciVarArr);
        try {
            a2.e = null;
            bArr2 = new byte[a2.e()];
            a2.a(iy.a(bArr2, bArr2.length));
        } catch (IOException e2) {
            q().f.a("Unable to serialize reduced-size config. Storing full config instead. appId", o.a(str), e2);
            bArr2 = bArr;
        }
        eo h2 = h();
        l.a(str);
        h2.c();
        h2.j();
        ContentValues contentValues = new ContentValues();
        contentValues.put("remote_config", bArr2);
        try {
            if (((long) h2.w().update("apps", contentValues, "app_id = ?", new String[]{str3})) == 0) {
                h2.q().c.a("Failed to update remote config (got 0). appId", o.a(str));
            }
        } catch (SQLiteException e3) {
            h2.q().c.a("Error storing remote config. appId", o.a(str), e3);
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public final boolean b(String str, String str2) {
        Boolean bool;
        c();
        h(str);
        if (f(str) && ed.e(str2)) {
            return true;
        }
        if (g(str) && ed.a(str2)) {
            return true;
        }
        Map map = this.e.get(str);
        if (map == null || (bool = (Boolean) map.get(str2)) == null) {
            return false;
        }
        return bool.booleanValue();
    }

    /* access modifiers changed from: package-private */
    public final boolean c(String str, String str2) {
        Boolean bool;
        c();
        h(str);
        if ("ecommerce_purchase".equals(str2)) {
            return true;
        }
        Map map = this.f.get(str);
        if (map == null || (bool = (Boolean) map.get(str2)) == null) {
            return false;
        }
        return bool.booleanValue();
    }

    /* access modifiers changed from: package-private */
    public final int d(String str, String str2) {
        Integer num;
        c();
        h(str);
        Map map = this.h.get(str);
        if (map == null || (num = (Integer) map.get(str2)) == null) {
            return 1;
        }
        return num.intValue();
    }

    /* access modifiers changed from: package-private */
    public final long e(String str) {
        String a2 = a(str, "measurement.account.time_zone_offset_minutes");
        if (TextUtils.isEmpty(a2)) {
            return 0;
        }
        try {
            return Long.parseLong(a2);
        } catch (NumberFormatException e2) {
            q().f.a("Unable to parse timezone offset. appId", o.a(str), e2);
            return 0;
        }
    }

    private final cp a(String str, byte[] bArr) {
        if (bArr == null) {
            return new cp();
        }
        iw a2 = iw.a(bArr, bArr.length);
        cp cpVar = new cp();
        try {
            cpVar.a(a2);
            q().k.a("Parsed config. version, gmp_app_id", cpVar.f2128a, cpVar.f2129b);
            return cpVar;
        } catch (IOException e2) {
            q().f.a("Unable to merge remote config. appId", o.a(str), e2);
            return new cp();
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean f(String str) {
        return AppEventsConstants.EVENT_PARAM_VALUE_YES.equals(a(str, "measurement.upload.blacklist_internal"));
    }

    /* access modifiers changed from: package-private */
    public final boolean g(String str) {
        return AppEventsConstants.EVENT_PARAM_VALUE_YES.equals(a(str, "measurement.upload.blacklist_public"));
    }

    public final /* bridge */ /* synthetic */ ea f() {
        return super.f();
    }

    public final /* bridge */ /* synthetic */ ei g() {
        return super.g();
    }

    public final /* bridge */ /* synthetic */ eo h() {
        return super.h();
    }

    public final /* bridge */ /* synthetic */ void a() {
        super.a();
    }

    public final /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    public final /* bridge */ /* synthetic */ void c() {
        super.c();
    }

    public final /* bridge */ /* synthetic */ b k() {
        return super.k();
    }

    public final /* bridge */ /* synthetic */ e l() {
        return super.l();
    }

    public final /* bridge */ /* synthetic */ Context m() {
        return super.m();
    }

    public final /* bridge */ /* synthetic */ m n() {
        return super.n();
    }

    public final /* bridge */ /* synthetic */ ed o() {
        return super.o();
    }

    public final /* bridge */ /* synthetic */ am p() {
        return super.p();
    }

    public final /* bridge */ /* synthetic */ o q() {
        return super.q();
    }

    public final /* bridge */ /* synthetic */ z r() {
        return super.r();
    }

    public final /* bridge */ /* synthetic */ el s() {
        return super.s();
    }
}
