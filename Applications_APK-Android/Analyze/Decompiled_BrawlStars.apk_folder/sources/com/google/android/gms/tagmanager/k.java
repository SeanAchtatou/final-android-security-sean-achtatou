package com.google.android.gms.tagmanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import com.google.android.gms.common.util.e;
import com.google.android.gms.common.util.g;
import com.google.android.gms.tagmanager.c;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

final class k implements c.C0124c {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f2660a = String.format("CREATE TABLE IF NOT EXISTS %s ( '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '%s' STRING NOT NULL, '%s' BLOB NOT NULL, '%s' INTEGER NOT NULL);", "datalayer", "ID", "key", "value", "expires");

    /* renamed from: b  reason: collision with root package name */
    private final Executor f2661b;
    /* access modifiers changed from: private */
    public final Context c;
    private n d;
    private e e;
    private int f;

    public k(Context context) {
        this(context, g.d(), "google_tagmanager.db", Executors.newSingleThreadExecutor());
    }

    private k(Context context, e eVar, String str, Executor executor) {
        this.c = context;
        this.e = eVar;
        this.f = 2000;
        this.f2661b = executor;
        this.d = new n(this, this.c, str);
    }

    public final void a(List<c.a> list, long j) {
        ArrayList arrayList = new ArrayList();
        for (c.a next : list) {
            arrayList.add(new o(next.f2652a, a(next.f2653b)));
        }
        this.f2661b.execute(new l(this, arrayList, j));
    }

    public final void a(i iVar) {
        this.f2661b.execute(new m(this, iVar));
    }

    /* access modifiers changed from: private */
    public final List<c.a> b() {
        try {
            a(this.e.a());
            List<o> c2 = c();
            ArrayList arrayList = new ArrayList();
            for (o next : c2) {
                arrayList.add(new c.a(next.f2667a, a(next.f2668b)));
            }
            return arrayList;
        } finally {
            e();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x001e A[SYNTHETIC, Splitter:B:13:0x001e] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0028 A[SYNTHETIC, Splitter:B:22:0x0028] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0032 A[SYNTHETIC, Splitter:B:31:0x0032] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.Object a(byte[] r3) {
        /*
            java.io.ByteArrayInputStream r0 = new java.io.ByteArrayInputStream
            r0.<init>(r3)
            r3 = 0
            java.io.ObjectInputStream r1 = new java.io.ObjectInputStream     // Catch:{ IOException -> 0x002f, ClassNotFoundException -> 0x0025, all -> 0x001b }
            r1.<init>(r0)     // Catch:{ IOException -> 0x002f, ClassNotFoundException -> 0x0025, all -> 0x001b }
            java.lang.Object r3 = r1.readObject()     // Catch:{ IOException -> 0x0030, ClassNotFoundException -> 0x0026, all -> 0x0016 }
            r1.close()     // Catch:{ IOException -> 0x0015 }
            r0.close()     // Catch:{ IOException -> 0x0015 }
        L_0x0015:
            return r3
        L_0x0016:
            r3 = move-exception
            r2 = r1
            r1 = r3
            r3 = r2
            goto L_0x001c
        L_0x001b:
            r1 = move-exception
        L_0x001c:
            if (r3 == 0) goto L_0x0021
            r3.close()     // Catch:{ IOException -> 0x0024 }
        L_0x0021:
            r0.close()     // Catch:{ IOException -> 0x0024 }
        L_0x0024:
            throw r1
        L_0x0025:
            r1 = r3
        L_0x0026:
            if (r1 == 0) goto L_0x002b
            r1.close()     // Catch:{ IOException -> 0x002e }
        L_0x002b:
            r0.close()     // Catch:{ IOException -> 0x002e }
        L_0x002e:
            return r3
        L_0x002f:
            r1 = r3
        L_0x0030:
            if (r1 == 0) goto L_0x0035
            r1.close()     // Catch:{ IOException -> 0x0038 }
        L_0x0035:
            r0.close()     // Catch:{ IOException -> 0x0038 }
        L_0x0038:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.tagmanager.k.a(byte[]):java.lang.Object");
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x001f A[SYNTHETIC, Splitter:B:13:0x001f] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0029 A[SYNTHETIC, Splitter:B:22:0x0029] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] a(java.lang.Object r3) {
        /*
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream
            r0.<init>()
            r1 = 0
            java.io.ObjectOutputStream r2 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x0026, all -> 0x001c }
            r2.<init>(r0)     // Catch:{ IOException -> 0x0026, all -> 0x001c }
            r2.writeObject(r3)     // Catch:{ IOException -> 0x0027, all -> 0x0019 }
            byte[] r3 = r0.toByteArray()     // Catch:{ IOException -> 0x0027, all -> 0x0019 }
            r2.close()     // Catch:{ IOException -> 0x0018 }
            r0.close()     // Catch:{ IOException -> 0x0018 }
        L_0x0018:
            return r3
        L_0x0019:
            r3 = move-exception
            r1 = r2
            goto L_0x001d
        L_0x001c:
            r3 = move-exception
        L_0x001d:
            if (r1 == 0) goto L_0x0022
            r1.close()     // Catch:{ IOException -> 0x0025 }
        L_0x0022:
            r0.close()     // Catch:{ IOException -> 0x0025 }
        L_0x0025:
            throw r3
        L_0x0026:
            r2 = r1
        L_0x0027:
            if (r2 == 0) goto L_0x002c
            r2.close()     // Catch:{ IOException -> 0x002f }
        L_0x002c:
            r0.close()     // Catch:{ IOException -> 0x002f }
        L_0x002f:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.tagmanager.k.a(java.lang.Object):byte[]");
    }

    /* access modifiers changed from: private */
    public final synchronized void b(List<o> list, long j) {
        long a2;
        String[] strArr;
        try {
            a2 = this.e.a();
            a(a2);
            int d2 = (d() - this.f) + list.size();
            if (d2 > 0) {
                List<String> a3 = a(d2);
                int size = a3.size();
                StringBuilder sb = new StringBuilder(64);
                sb.append("DataLayer store full, deleting ");
                sb.append(size);
                sb.append(" entries to make room.");
                ab.c(sb.toString());
                strArr = (String[]) a3.toArray(new String[0]);
                if (strArr != null) {
                    if (strArr.length != 0) {
                        SQLiteDatabase a4 = a("Error opening database for deleteEntries.");
                        if (a4 != null) {
                            a4.delete("datalayer", String.format("%s in (%s)", "ID", TextUtils.join(",", Collections.nCopies(strArr.length, "?"))), strArr);
                        }
                    }
                }
            }
        } catch (SQLiteException unused) {
            String valueOf = String.valueOf(Arrays.toString(strArr));
            ab.b(valueOf.length() != 0 ? "Error deleting entries ".concat(valueOf) : new String("Error deleting entries "));
        } catch (Throwable th) {
            e();
            throw th;
        }
        long j2 = a2 + j;
        SQLiteDatabase a5 = a("Error opening database for writeEntryToDatabase.");
        if (a5 != null) {
            for (o next : list) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("expires", Long.valueOf(j2));
                contentValues.put("key", next.f2667a);
                contentValues.put("value", next.f2668b);
                a5.insert("datalayer", null, contentValues);
            }
        }
        e();
    }

    private final List<o> c() {
        SQLiteDatabase a2 = a("Error opening database for loadSerialized.");
        ArrayList arrayList = new ArrayList();
        if (a2 == null) {
            return arrayList;
        }
        Cursor query = a2.query("datalayer", new String[]{"key", "value"}, null, null, null, null, "ID", null);
        while (query.moveToNext()) {
            try {
                arrayList.add(new o(query.getString(0), query.getBlob(1)));
            } finally {
                query.close();
            }
        }
        return arrayList;
    }

    private final void a(long j) {
        SQLiteDatabase a2 = a("Error opening database for deleteOlderThan.");
        if (a2 != null) {
            try {
                int delete = a2.delete("datalayer", "expires <= ?", new String[]{Long.toString(j)});
                StringBuilder sb = new StringBuilder(33);
                sb.append("Deleted ");
                sb.append(delete);
                sb.append(" expired items");
                ab.d(sb.toString());
            } catch (SQLiteException unused) {
                ab.b("Error deleting old entries.");
            }
        }
    }

    private final List<String> a(int i) {
        ArrayList arrayList = new ArrayList();
        if (i <= 0) {
            ab.b("Invalid maxEntries specified. Skipping.");
            return arrayList;
        }
        SQLiteDatabase a2 = a("Error opening database for peekEntryIds.");
        if (a2 == null) {
            return arrayList;
        }
        Cursor cursor = null;
        try {
            Cursor query = a2.query("datalayer", new String[]{"ID"}, null, null, null, null, String.format("%s ASC", "ID"), Integer.toString(i));
            if (query.moveToFirst()) {
                do {
                    arrayList.add(String.valueOf(query.getLong(0)));
                } while (query.moveToNext());
            }
            if (query != null) {
                query.close();
            }
        } catch (SQLiteException e2) {
            String valueOf = String.valueOf(e2.getMessage());
            ab.b(valueOf.length() != 0 ? "Error in peekEntries fetching entryIds: ".concat(valueOf) : new String("Error in peekEntries fetching entryIds: "));
            if (cursor != null) {
                cursor.close();
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        return arrayList;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [java.lang.String[], android.database.Cursor] */
    private final int d() {
        SQLiteDatabase a2 = a("Error opening database for getNumStoredEntries.");
        int i = 0;
        if (a2 == null) {
            return 0;
        }
        ? r2 = 0;
        try {
            Cursor rawQuery = a2.rawQuery("SELECT COUNT(*) from datalayer", r2);
            if (rawQuery.moveToFirst()) {
                i = (int) rawQuery.getLong(0);
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
        } catch (SQLiteException unused) {
            ab.b("Error getting numStoredEntries");
            if (r2 != 0) {
                r2.close();
            }
        } catch (Throwable th) {
            if (r2 != 0) {
                r2.close();
            }
            throw th;
        }
        return i;
    }

    private final SQLiteDatabase a(String str) {
        try {
            return this.d.getWritableDatabase();
        } catch (SQLiteException unused) {
            ab.b(str);
            return null;
        }
    }

    private final void e() {
        try {
            this.d.close();
        } catch (SQLiteException unused) {
        }
    }
}
