package com.google.android.gms.internal.drive;

import java.util.Arrays;
import java.util.Locale;

public final class j {

    /* renamed from: a  reason: collision with root package name */
    private final com.google.android.gms.drive.events.j f1915a;

    /* renamed from: b  reason: collision with root package name */
    private final long f1916b;
    private final long c;

    /* JADX WARN: Type inference failed for: r0v0, types: [com.google.android.gms.internal.drive.r, com.google.android.gms.drive.events.j] */
    public j(zzh zzh) {
        this.f1915a = new r(zzh);
        this.f1916b = zzh.d;
        this.c = zzh.e;
    }

    public final boolean equals(Object obj) {
        if (obj != null && obj.getClass() == getClass()) {
            if (obj == this) {
                return true;
            }
            j jVar = (j) obj;
            return com.google.android.gms.common.internal.j.a(this.f1915a, jVar.f1915a) && this.f1916b == jVar.f1916b && this.c == jVar.c;
        }
    }

    public final String toString() {
        return String.format(Locale.US, "FileTransferProgress[FileTransferState: %s, BytesTransferred: %d, TotalBytes: %d]", this.f1915a.toString(), Long.valueOf(this.f1916b), Long.valueOf(this.c));
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{Long.valueOf(this.c), Long.valueOf(this.f1916b), Long.valueOf(this.c)});
    }
}
