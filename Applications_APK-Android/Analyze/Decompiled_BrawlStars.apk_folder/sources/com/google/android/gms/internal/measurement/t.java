package com.google.android.gms.internal.measurement;

import android.content.Context;
import com.google.android.gms.analytics.a;
import com.google.android.gms.analytics.n;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.util.e;
import com.google.android.gms.common.util.g;

public class t {
    private static volatile t i;

    /* renamed from: a  reason: collision with root package name */
    final Context f2332a;

    /* renamed from: b  reason: collision with root package name */
    final Context f2333b;
    public final e c = g.d();
    final au d = new au(this);
    final bk e;
    final az f;
    final bo g;
    public final ay h;
    private final n j;
    private final l k;
    private final by l;
    private final a m;
    private final am n;
    private final k o;
    private final af p;

    private t(v vVar) {
        Context context = vVar.f2335a;
        l.a(context, "Application context can't be null");
        Context context2 = vVar.f2336b;
        l.a(context2);
        this.f2332a = context;
        this.f2333b = context2;
        bk bkVar = new bk(this);
        bkVar.n();
        this.e = bkVar;
        bk a2 = a();
        String str = s.f2330a;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 134);
        sb.append("Google Analytics ");
        sb.append(str);
        sb.append(" is starting up. To enable debug logging on a device run:\n  adb shell setprop log.tag.GAv4 DEBUG\n  adb logcat -s GAv4");
        a2.d(sb.toString());
        bo boVar = new bo(this);
        boVar.n();
        this.g = boVar;
        by byVar = new by(this);
        byVar.n();
        this.l = byVar;
        l lVar = new l(this, vVar);
        am amVar = new am(this);
        k kVar = new k(this);
        af afVar = new af(this);
        ay ayVar = new ay(this);
        n a3 = n.a(context);
        a3.c = new u(this);
        this.j = a3;
        a aVar = new a(this);
        amVar.n();
        this.n = amVar;
        kVar.n();
        this.o = kVar;
        afVar.n();
        this.p = afVar;
        ayVar.n();
        this.h = ayVar;
        az azVar = new az(this);
        azVar.n();
        this.f = azVar;
        lVar.n();
        this.k = lVar;
        by e2 = aVar.d.e();
        e2.d();
        if (e2.e()) {
            aVar.f1313b = e2.o();
        }
        e2.d();
        aVar.f1312a = true;
        this.m = aVar;
        lVar.f2320a.b();
    }

    public static t a(Context context) {
        l.a(context);
        if (i == null) {
            synchronized (t.class) {
                if (i == null) {
                    e d2 = g.d();
                    long b2 = d2.b();
                    t tVar = new t(new v(context));
                    i = tVar;
                    a.b();
                    long b3 = d2.b() - b2;
                    long longValue = ((Long) bc.E.f2067a).longValue();
                    if (b3 > longValue) {
                        tVar.a().c("Slow initialization (ms)", Long.valueOf(b3), Long.valueOf(longValue));
                    }
                }
            }
        }
        return i;
    }

    public final bk a() {
        a(this.e);
        return this.e;
    }

    public final n b() {
        l.a(this.j);
        return this.j;
    }

    public final l c() {
        a(this.k);
        return this.k;
    }

    public final a d() {
        l.a(this.m);
        l.b(this.m.a(), "Analytics instance not initialized");
        return this.m;
    }

    public final by e() {
        a(this.l);
        return this.l;
    }

    public final k f() {
        a(this.o);
        return this.o;
    }

    public final am g() {
        a(this.n);
        return this.n;
    }

    public final af h() {
        a(this.p);
        return this.p;
    }

    static void a(r rVar) {
        l.a(rVar, "Analytics service not created/initialized");
        l.b(rVar.l(), "Analytics service not initialized");
    }
}
