package com.google.android.gms.internal.measurement;

import android.os.Build;

public final class bb {
    public static int a() {
        try {
            return Integer.parseInt(Build.VERSION.SDK);
        } catch (NumberFormatException unused) {
            bj.a("Invalid version number", Build.VERSION.SDK);
            return 0;
        }
    }
}
