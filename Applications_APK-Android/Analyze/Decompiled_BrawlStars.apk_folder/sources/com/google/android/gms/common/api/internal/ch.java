package com.google.android.gms.common.api.internal;

import android.support.v4.util.ArrayMap;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.AvailabilityException;
import com.google.android.gms.tasks.c;
import com.google.android.gms.tasks.g;
import java.util.Collections;
import java.util.Map;

final class ch implements c<Map<bs<?>, String>> {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ cg f1457a;

    private ch(cg cgVar) {
        this.f1457a = cgVar;
    }

    public final void a(g<Map<bs<?>, String>> gVar) {
        this.f1457a.d.lock();
        try {
            if (this.f1457a.g) {
                if (gVar.b()) {
                    this.f1457a.h = new ArrayMap(this.f1457a.f1455a.size());
                    for (cf<?> cfVar : this.f1457a.f1455a.values()) {
                        this.f1457a.h.put(cfVar.f1374b, ConnectionResult.f1352a);
                    }
                } else if (gVar.e() instanceof AvailabilityException) {
                    AvailabilityException availabilityException = (AvailabilityException) gVar.e();
                    if (this.f1457a.f) {
                        this.f1457a.h = new ArrayMap(this.f1457a.f1455a.size());
                        for (cf next : this.f1457a.f1455a.values()) {
                            bs<O> bsVar = next.f1374b;
                            ConnectionResult a2 = availabilityException.a(next);
                            if (this.f1457a.a(next, a2)) {
                                this.f1457a.h.put(bsVar, new ConnectionResult(16));
                            } else {
                                this.f1457a.h.put(bsVar, a2);
                            }
                        }
                    } else {
                        this.f1457a.h = availabilityException.f1364a;
                    }
                    this.f1457a.j = this.f1457a.h();
                } else {
                    this.f1457a.h = Collections.emptyMap();
                    this.f1457a.j = new ConnectionResult(8);
                }
                if (this.f1457a.i != null) {
                    this.f1457a.h.putAll(this.f1457a.i);
                    this.f1457a.j = this.f1457a.h();
                }
                if (this.f1457a.j == null) {
                    this.f1457a.c();
                    this.f1457a.g();
                } else {
                    this.f1457a.g = false;
                    this.f1457a.c.a(this.f1457a.j);
                }
                this.f1457a.e.signalAll();
                this.f1457a.d.unlock();
            }
        } finally {
            this.f1457a.d.unlock();
        }
    }

    /* synthetic */ ch(cg cgVar, byte b2) {
        this(cgVar);
    }
}
