package com.google.android.gms.tasks;

final class r implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ g f2696a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ q f2697b;

    r(q qVar, g gVar) {
        this.f2697b = qVar;
        this.f2696a = gVar;
    }

    public final void run() {
        synchronized (this.f2697b.f2694a) {
            if (this.f2697b.f2695b != null) {
                this.f2697b.f2695b.a(this.f2696a);
            }
        }
    }
}
