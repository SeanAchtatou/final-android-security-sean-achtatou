package com.google.android.gms.drive;

import android.os.Parcel;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public abstract class zzu extends AbstractSafeParcelable {

    /* renamed from: a  reason: collision with root package name */
    private volatile transient boolean f1773a = false;

    /* access modifiers changed from: protected */
    public abstract void a(Parcel parcel, int i);

    public void writeToParcel(Parcel parcel, int i) {
        l.a(!this.f1773a);
        this.f1773a = true;
        a(parcel, i);
    }
}
