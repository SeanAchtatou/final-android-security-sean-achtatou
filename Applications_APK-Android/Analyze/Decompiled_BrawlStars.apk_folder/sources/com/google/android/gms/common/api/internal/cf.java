package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.a.d;
import com.google.android.gms.common.api.c;
import com.google.android.gms.common.api.internal.d;
import com.google.android.gms.common.internal.b;
import com.google.android.gms.signin.e;

public final class cf<O extends a.d> extends c<O> {
    private final a.f f;
    private final ca g;
    private final b h;
    private final a.C0111a<? extends e, com.google.android.gms.signin.a> i;

    public cf(Context context, a<O> aVar, Looper looper, a.f fVar, ca caVar, b bVar, a.C0111a<? extends e, com.google.android.gms.signin.a> aVar2) {
        super(context, aVar, looper);
        this.f = fVar;
        this.g = caVar;
        this.h = bVar;
        this.i = aVar2;
        d dVar = this.e;
        dVar.i.sendMessage(dVar.i.obtainMessage(7, this));
    }

    public final a.f a(Looper looper, d.a<O> aVar) {
        this.g.f1450b = aVar;
        return this.f;
    }

    public final zace a(Context context, Handler handler) {
        return new zace(context, handler, this.h, this.i);
    }
}
