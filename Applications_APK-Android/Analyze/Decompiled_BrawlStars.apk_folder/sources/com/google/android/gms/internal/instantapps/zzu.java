package com.google.android.gms.internal.instantapps;

import android.os.IBinder;

public final class zzu extends zza implements zzt {
    zzu(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.instantapps.internal.IInstantAppsService");
    }
}
