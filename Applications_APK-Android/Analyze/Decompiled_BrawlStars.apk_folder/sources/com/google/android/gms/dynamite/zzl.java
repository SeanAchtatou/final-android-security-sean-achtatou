package com.google.android.gms.dynamite;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.common.a;
import com.google.android.gms.internal.common.zza;

public final class zzl extends zza implements zzk {
    zzl(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.dynamite.IDynamiteLoaderV2");
    }

    public final IObjectWrapper a(IObjectWrapper iObjectWrapper, String str, int i, IObjectWrapper iObjectWrapper2) throws RemoteException {
        Parcel s = s();
        a.a(s, iObjectWrapper);
        s.writeString(str);
        s.writeInt(i);
        a.a(s, iObjectWrapper2);
        Parcel a2 = a(2, s);
        IObjectWrapper a3 = IObjectWrapper.Stub.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    public final IObjectWrapper b(IObjectWrapper iObjectWrapper, String str, int i, IObjectWrapper iObjectWrapper2) throws RemoteException {
        Parcel s = s();
        a.a(s, iObjectWrapper);
        s.writeString(str);
        s.writeInt(i);
        a.a(s, iObjectWrapper2);
        Parcel a2 = a(3, s);
        IObjectWrapper a3 = IObjectWrapper.Stub.a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }
}
