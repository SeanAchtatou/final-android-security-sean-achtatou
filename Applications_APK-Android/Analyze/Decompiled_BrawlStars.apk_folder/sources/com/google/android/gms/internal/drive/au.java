package com.google.android.gms.internal.drive;

import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.metadata.a;
import com.google.android.gms.drive.metadata.g;
import com.google.android.gms.drive.metadata.internal.b;
import com.google.android.gms.drive.metadata.internal.h;
import com.google.android.gms.drive.metadata.internal.m;
import com.google.android.gms.drive.metadata.internal.p;
import com.google.android.gms.drive.metadata.internal.q;
import com.google.android.gms.drive.metadata.internal.r;
import java.util.Collections;

public final class au {
    public static final r A = new r("lastModifyingUser");
    public static final r B = new r("sharingUser");
    public static final m C = new m();
    public static final ba D = new ba("quotaBytesUsed");
    public static final bc E = new bc("starred");
    public static final a<BitmapTeleporter> F = new aw("thumbnail", Collections.emptySet(), Collections.emptySet());
    public static final bd G = new bd("title");
    public static final be H = new be("trashed");
    public static final a<String> I = new q("webContentLink", 4300000);
    public static final a<String> J = new q("webViewLink", 4300000);
    public static final a<String> K = new q("uniqueIdentifier", 5000000);
    public static final b L = new b("writersCanShare", 6000000);
    public static final a<String> M = new q("role", 6000000);
    public static final a<String> N = new q("md5Checksum", 7000000);
    public static final bb O = new bb();
    public static final a<String> P = new q("recencyReason", 8000000);
    public static final a<Boolean> Q = new b("subscribed", 8000000);

    /* renamed from: a  reason: collision with root package name */
    public static final a<DriveId> f1893a = bp.f1898a;

    /* renamed from: b  reason: collision with root package name */
    public static final a<String> f1894b = new q("alternateLink", 4300000);
    public static final ax c = new ax();
    public static final a<String> d = new q("description", 4300000);
    public static final a<String> e = new q("embedLink", 4300000);
    public static final a<String> f = new q("fileExtension", 4300000);
    public static final a<Long> g = new h("fileSize");
    public static final a<String> h = new q("folderColorRgb", 7500000);
    public static final a<Boolean> i = new b("hasThumbnail", 4300000);
    public static final a<String> j = new q("indexableText", 4300000);
    public static final a<Boolean> k = new b("isAppData", 4300000);
    public static final a<Boolean> l = new b("isCopyable", 4300000);
    public static final a<Boolean> m = new b("isEditable", 4100000);
    public static final a<Boolean> n = new av("isExplicitlyTrashed", Collections.singleton("trashed"), Collections.emptySet());
    public static final a<Boolean> o = new b("isLocalContentUpToDate", 7800000);
    public static final ay p = new ay("isPinned");
    public static final a<Boolean> q = new b("isOpenable", 7200000);
    public static final a<Boolean> r = new b("isRestricted", 4300000);
    public static final a<Boolean> s = new b("isShared", 4300000);
    public static final a<Boolean> t = new b("isGooglePhotosFolder", 7000000);
    public static final a<Boolean> u = new b("isGooglePhotosRootFolder", 7000000);
    public static final a<Boolean> v = new b("isTrashable", 4400000);
    public static final a<Boolean> w = new b("isViewed", 4300000);
    public static final az x = new az();
    public static final a<String> y = new q("originalFilename", 4300000);
    public static final g<String> z = new p("ownerNames");
}
