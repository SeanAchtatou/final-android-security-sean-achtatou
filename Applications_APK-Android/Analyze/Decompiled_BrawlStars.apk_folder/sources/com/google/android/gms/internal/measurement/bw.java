package com.google.android.gms.internal.measurement;

import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.util.e;

final class bw {

    /* renamed from: a  reason: collision with root package name */
    long f2099a;

    /* renamed from: b  reason: collision with root package name */
    private final e f2100b;

    public bw(e eVar) {
        l.a(eVar);
        this.f2100b = eVar;
    }

    public bw(e eVar, long j) {
        l.a(eVar);
        this.f2100b = eVar;
        this.f2099a = j;
    }

    public final void a() {
        this.f2099a = this.f2100b.b();
    }

    public final boolean a(long j) {
        if (this.f2099a != 0 && this.f2100b.b() - this.f2099a <= j) {
            return false;
        }
        return true;
    }
}
