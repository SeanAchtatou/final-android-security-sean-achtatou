package com.google.android.gms.common.api.internal;

abstract class an {

    /* renamed from: a  reason: collision with root package name */
    private final al f1399a;

    protected an(al alVar) {
        this.f1399a = alVar;
    }

    /* access modifiers changed from: protected */
    public abstract void a();

    public final void a(am amVar) {
        amVar.f1397a.lock();
        try {
            if (amVar.k == this.f1399a) {
                a();
                amVar.f1397a.unlock();
            }
        } finally {
            amVar.f1397a.unlock();
        }
    }
}
