package com.google.android.gms.measurement.internal;

final class av implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zzo f2395a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ zzk f2396b;
    private final /* synthetic */ zzby c;

    av(zzby zzby, zzo zzo, zzk zzk) {
        this.c = zzby;
        this.f2395a = zzo;
        this.f2396b = zzk;
    }

    public final void run() {
        this.c.f2597a.k();
        this.c.f2597a.b(this.f2395a, this.f2396b);
    }
}
