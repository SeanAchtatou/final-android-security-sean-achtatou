package com.google.android.gms.measurement.internal;

final class bi implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zzfv f2423a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ zzk f2424b;
    private final /* synthetic */ zzby c;

    bi(zzby zzby, zzfv zzfv, zzk zzk) {
        this.c = zzby;
        this.f2423a = zzfv;
        this.f2424b = zzk;
    }

    public final void run() {
        this.c.f2597a.k();
        this.c.f2597a.a(this.f2423a, this.f2424b);
    }
}
