package com.google.android.gms.internal.measurement;

import com.google.android.gms.common.a.a;
import com.google.android.gms.common.internal.l;

public final class bd<V> {

    /* renamed from: a  reason: collision with root package name */
    final V f2067a;

    /* renamed from: b  reason: collision with root package name */
    private final a<V> f2068b;

    bd(a<V> aVar, V v) {
        l.a(aVar);
        this.f2068b = aVar;
        this.f2067a = v;
    }

    static bd<Boolean> a(String str, boolean z, boolean z2) {
        return new bd<>(a.a(str, z2), Boolean.valueOf(z));
    }

    static bd<String> a(String str, String str2, String str3) {
        return new bd<>(a.a(str, str3), str2);
    }

    static bd<Long> a(String str, long j, long j2) {
        return new bd<>(a.a(str, Long.valueOf(j2)), Long.valueOf(j));
    }

    static bd<Integer> a(String str, int i, int i2) {
        return new bd<>(a.a(str, Integer.valueOf(i2)), Integer.valueOf(i));
    }
}
