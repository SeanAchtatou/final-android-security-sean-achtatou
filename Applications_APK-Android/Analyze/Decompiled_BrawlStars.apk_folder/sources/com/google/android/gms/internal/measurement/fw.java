package com.google.android.gms.internal.measurement;

import java.util.List;
import java.util.RandomAccess;

public interface fw<E> extends List<E>, RandomAccess {
    fw<E> a(int i);

    boolean a();

    void b();
}
