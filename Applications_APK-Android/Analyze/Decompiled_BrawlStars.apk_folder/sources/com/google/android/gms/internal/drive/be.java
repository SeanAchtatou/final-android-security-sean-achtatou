package com.google.android.gms.internal.drive;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.metadata.c;
import com.google.android.gms.drive.metadata.internal.b;

public final class be extends b implements c<Boolean> {
    public be(String str) {
        super(str, 4100000);
    }

    public final /* synthetic */ Object c(DataHolder dataHolder, int i, int i2) {
        return c(dataHolder, i, i2);
    }

    public final Boolean a_(DataHolder dataHolder, int i, int i2) {
        return Boolean.valueOf(dataHolder.b(this.f1731b, i, i2) != 0);
    }
}
