package com.google.android.gms.measurement.internal;

import com.facebook.AccessToken;

public class br {

    /* renamed from: a  reason: collision with root package name */
    public static final String[] f2436a = {"firebase_last_notification", "first_open_time", "first_visit_time", "last_deep_link_referrer", AccessToken.USER_ID_KEY, "first_open_after_install", "lifetime_user_engagement", "google_allow_ad_personalization_signals", "session_number", "session_id"};

    /* renamed from: b  reason: collision with root package name */
    public static final String[] f2437b = {"_ln", "_fot", "_fvt", "_ldl", "_id", "_fi", "_lte", "_ap", "_sno", "_sid"};

    public static String a(String str) {
        return cf.a(str, f2436a, f2437b);
    }
}
