package com.google.android.gms.common.api.internal;

final class cl implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ LifecycleCallback f1464a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f1465b;
    private final /* synthetic */ ck c;

    cl(ck ckVar, LifecycleCallback lifecycleCallback, String str) {
        this.c = ckVar;
        this.f1464a = lifecycleCallback;
        this.f1465b = str;
    }

    public final void run() {
        if (this.c.c > 0) {
            this.f1464a.a(this.c.d != null ? this.c.d.getBundle(this.f1465b) : null);
        }
        if (this.c.c >= 2) {
            this.f1464a.b();
        }
        if (this.c.c >= 3) {
            this.f1464a.c();
        }
        if (this.c.c >= 4) {
            this.f1464a.d();
        }
        int unused = this.c.c;
    }
}
