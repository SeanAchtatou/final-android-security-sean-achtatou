package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class cx extends iz<cx> {

    /* renamed from: a  reason: collision with root package name */
    public long[] f2143a = jh.f2316b;

    /* renamed from: b  reason: collision with root package name */
    public long[] f2144b = jh.f2316b;
    public cs[] c = cs.a();
    public cy[] d = cy.a();

    public cx() {
        this.L = null;
        this.M = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof cx)) {
            return false;
        }
        cx cxVar = (cx) obj;
        if (!jd.a(this.f2143a, cxVar.f2143a) || !jd.a(this.f2144b, cxVar.f2144b) || !jd.a(this.c, cxVar.c) || !jd.a(this.d, cxVar.d)) {
            return false;
        }
        if (this.L == null || this.L.a()) {
            return cxVar.L == null || cxVar.L.a();
        }
        return this.L.equals(cxVar.L);
    }

    public final int hashCode() {
        return ((((((((((getClass().getName().hashCode() + 527) * 31) + jd.a(this.f2143a)) * 31) + jd.a(this.f2144b)) * 31) + jd.a(this.c)) * 31) + jd.a(this.d)) * 31) + ((this.L == null || this.L.a()) ? 0 : this.L.hashCode());
    }

    public final void a(iy iyVar) throws IOException {
        long[] jArr = this.f2143a;
        int i = 0;
        if (jArr != null && jArr.length > 0) {
            int i2 = 0;
            while (true) {
                long[] jArr2 = this.f2143a;
                if (i2 >= jArr2.length) {
                    break;
                }
                iyVar.a(1, jArr2[i2]);
                i2++;
            }
        }
        long[] jArr3 = this.f2144b;
        if (jArr3 != null && jArr3.length > 0) {
            int i3 = 0;
            while (true) {
                long[] jArr4 = this.f2144b;
                if (i3 >= jArr4.length) {
                    break;
                }
                iyVar.a(2, jArr4[i3]);
                i3++;
            }
        }
        cs[] csVarArr = this.c;
        if (csVarArr != null && csVarArr.length > 0) {
            int i4 = 0;
            while (true) {
                cs[] csVarArr2 = this.c;
                if (i4 >= csVarArr2.length) {
                    break;
                }
                cs csVar = csVarArr2[i4];
                if (csVar != null) {
                    iyVar.a(3, csVar);
                }
                i4++;
            }
        }
        cy[] cyVarArr = this.d;
        if (cyVarArr != null && cyVarArr.length > 0) {
            while (true) {
                cy[] cyVarArr2 = this.d;
                if (i >= cyVarArr2.length) {
                    break;
                }
                cy cyVar = cyVarArr2[i];
                if (cyVar != null) {
                    iyVar.a(4, cyVar);
                }
                i++;
            }
        }
        super.a(iyVar);
    }

    /* access modifiers changed from: protected */
    public final int b() {
        long[] jArr;
        long[] jArr2;
        int b2 = super.b();
        long[] jArr3 = this.f2143a;
        int i = 0;
        if (jArr3 != null && jArr3.length > 0) {
            int i2 = 0;
            int i3 = 0;
            while (true) {
                jArr2 = this.f2143a;
                if (i2 >= jArr2.length) {
                    break;
                }
                i3 += iy.a(jArr2[i2]);
                i2++;
            }
            b2 = b2 + i3 + (jArr2.length * 1);
        }
        long[] jArr4 = this.f2144b;
        if (jArr4 != null && jArr4.length > 0) {
            int i4 = 0;
            int i5 = 0;
            while (true) {
                jArr = this.f2144b;
                if (i4 >= jArr.length) {
                    break;
                }
                i5 += iy.a(jArr[i4]);
                i4++;
            }
            b2 = b2 + i5 + (jArr.length * 1);
        }
        cs[] csVarArr = this.c;
        if (csVarArr != null && csVarArr.length > 0) {
            int i6 = b2;
            int i7 = 0;
            while (true) {
                cs[] csVarArr2 = this.c;
                if (i7 >= csVarArr2.length) {
                    break;
                }
                cs csVar = csVarArr2[i7];
                if (csVar != null) {
                    i6 += iy.b(3, csVar);
                }
                i7++;
            }
            b2 = i6;
        }
        cy[] cyVarArr = this.d;
        if (cyVarArr != null && cyVarArr.length > 0) {
            while (true) {
                cy[] cyVarArr2 = this.d;
                if (i >= cyVarArr2.length) {
                    break;
                }
                cy cyVar = cyVarArr2[i];
                if (cyVar != null) {
                    b2 += iy.b(4, cyVar);
                }
                i++;
            }
        }
        return b2;
    }

    public final /* synthetic */ je a(iw iwVar) throws IOException {
        while (true) {
            int a2 = iwVar.a();
            if (a2 == 0) {
                return this;
            }
            if (a2 == 8) {
                int a3 = jh.a(iwVar, 8);
                long[] jArr = this.f2143a;
                int length = jArr == null ? 0 : jArr.length;
                long[] jArr2 = new long[(a3 + length)];
                if (length != 0) {
                    System.arraycopy(this.f2143a, 0, jArr2, 0, length);
                }
                while (length < jArr2.length - 1) {
                    jArr2[length] = iwVar.e();
                    iwVar.a();
                    length++;
                }
                jArr2[length] = iwVar.e();
                this.f2143a = jArr2;
            } else if (a2 == 10) {
                int c2 = iwVar.c(iwVar.d());
                int i = iwVar.i();
                int i2 = 0;
                while (iwVar.h() > 0) {
                    iwVar.e();
                    i2++;
                }
                iwVar.e(i);
                long[] jArr3 = this.f2143a;
                int length2 = jArr3 == null ? 0 : jArr3.length;
                long[] jArr4 = new long[(i2 + length2)];
                if (length2 != 0) {
                    System.arraycopy(this.f2143a, 0, jArr4, 0, length2);
                }
                while (length2 < jArr4.length) {
                    jArr4[length2] = iwVar.e();
                    length2++;
                }
                this.f2143a = jArr4;
                iwVar.d(c2);
            } else if (a2 == 16) {
                int a4 = jh.a(iwVar, 16);
                long[] jArr5 = this.f2144b;
                int length3 = jArr5 == null ? 0 : jArr5.length;
                long[] jArr6 = new long[(a4 + length3)];
                if (length3 != 0) {
                    System.arraycopy(this.f2144b, 0, jArr6, 0, length3);
                }
                while (length3 < jArr6.length - 1) {
                    jArr6[length3] = iwVar.e();
                    iwVar.a();
                    length3++;
                }
                jArr6[length3] = iwVar.e();
                this.f2144b = jArr6;
            } else if (a2 == 18) {
                int c3 = iwVar.c(iwVar.d());
                int i3 = iwVar.i();
                int i4 = 0;
                while (iwVar.h() > 0) {
                    iwVar.e();
                    i4++;
                }
                iwVar.e(i3);
                long[] jArr7 = this.f2144b;
                int length4 = jArr7 == null ? 0 : jArr7.length;
                long[] jArr8 = new long[(i4 + length4)];
                if (length4 != 0) {
                    System.arraycopy(this.f2144b, 0, jArr8, 0, length4);
                }
                while (length4 < jArr8.length) {
                    jArr8[length4] = iwVar.e();
                    length4++;
                }
                this.f2144b = jArr8;
                iwVar.d(c3);
            } else if (a2 == 26) {
                int a5 = jh.a(iwVar, 26);
                cs[] csVarArr = this.c;
                int length5 = csVarArr == null ? 0 : csVarArr.length;
                cs[] csVarArr2 = new cs[(a5 + length5)];
                if (length5 != 0) {
                    System.arraycopy(this.c, 0, csVarArr2, 0, length5);
                }
                while (length5 < csVarArr2.length - 1) {
                    csVarArr2[length5] = new cs();
                    iwVar.a(csVarArr2[length5]);
                    iwVar.a();
                    length5++;
                }
                csVarArr2[length5] = new cs();
                iwVar.a(csVarArr2[length5]);
                this.c = csVarArr2;
            } else if (a2 == 34) {
                int a6 = jh.a(iwVar, 34);
                cy[] cyVarArr = this.d;
                int length6 = cyVarArr == null ? 0 : cyVarArr.length;
                cy[] cyVarArr2 = new cy[(a6 + length6)];
                if (length6 != 0) {
                    System.arraycopy(this.d, 0, cyVarArr2, 0, length6);
                }
                while (length6 < cyVarArr2.length - 1) {
                    cyVarArr2[length6] = new cy();
                    iwVar.a(cyVarArr2[length6]);
                    iwVar.a();
                    length6++;
                }
                cyVarArr2[length6] = new cy();
                iwVar.a(cyVarArr2[length6]);
                this.d = cyVarArr2;
            } else if (!super.a(iwVar, a2)) {
                return this;
            }
        }
    }
}
