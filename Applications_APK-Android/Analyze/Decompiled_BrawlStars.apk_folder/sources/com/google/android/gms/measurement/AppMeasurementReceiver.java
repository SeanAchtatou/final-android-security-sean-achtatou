package com.google.android.gms.measurement;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import com.google.android.gms.measurement.internal.ah;
import com.google.android.gms.measurement.internal.ak;

public final class AppMeasurementReceiver extends WakefulBroadcastReceiver implements ak {

    /* renamed from: a  reason: collision with root package name */
    private ah f2356a;

    public final void onReceive(Context context, Intent intent) {
        if (this.f2356a == null) {
            this.f2356a = new ah(this);
        }
        this.f2356a.a(context, intent);
    }

    public final void a(Context context, Intent intent) {
        startWakefulService(context, intent);
    }

    public final BroadcastReceiver.PendingResult a() {
        return goAsync();
    }
}
