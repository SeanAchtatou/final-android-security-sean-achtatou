package com.google.android.gms.internal.drive;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

final class bv implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private bt<?, ?> f1905a;

    /* renamed from: b  reason: collision with root package name */
    private Object f1906b;
    private List<bz> c = new ArrayList();

    bv() {
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public final bv clone() {
        Object clone;
        bv bvVar = new bv();
        try {
            bvVar.f1905a = this.f1905a;
            if (this.c == null) {
                bvVar.c = null;
            } else {
                bvVar.c.addAll(this.c);
            }
            if (this.f1906b != null) {
                if (this.f1906b instanceof bx) {
                    clone = (bx) ((bx) this.f1906b).clone();
                } else if (this.f1906b instanceof byte[]) {
                    clone = ((byte[]) this.f1906b).clone();
                } else {
                    int i = 0;
                    if (this.f1906b instanceof byte[][]) {
                        byte[][] bArr = (byte[][]) this.f1906b;
                        byte[][] bArr2 = new byte[bArr.length][];
                        bvVar.f1906b = bArr2;
                        while (i < bArr.length) {
                            bArr2[i] = (byte[]) bArr[i].clone();
                            i++;
                        }
                    } else if (this.f1906b instanceof boolean[]) {
                        clone = ((boolean[]) this.f1906b).clone();
                    } else if (this.f1906b instanceof int[]) {
                        clone = ((int[]) this.f1906b).clone();
                    } else if (this.f1906b instanceof long[]) {
                        clone = ((long[]) this.f1906b).clone();
                    } else if (this.f1906b instanceof float[]) {
                        clone = ((float[]) this.f1906b).clone();
                    } else if (this.f1906b instanceof double[]) {
                        clone = ((double[]) this.f1906b).clone();
                    } else if (this.f1906b instanceof bx[]) {
                        bx[] bxVarArr = (bx[]) this.f1906b;
                        bx[] bxVarArr2 = new bx[bxVarArr.length];
                        bvVar.f1906b = bxVarArr2;
                        while (i < bxVarArr.length) {
                            bxVarArr2[i] = (bx) bxVarArr[i].clone();
                            i++;
                        }
                    }
                }
                bvVar.f1906b = clone;
            }
            return bvVar;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    /* access modifiers changed from: package-private */
    public final int a() {
        if (this.f1906b == null) {
            int i = 0;
            for (bz next : this.c) {
                i += br.b(next.f1909a) + 0 + next.f1910b.length;
            }
            return i;
        }
        throw new NoSuchMethodError();
    }

    /* access modifiers changed from: package-private */
    public final void a(br brVar) throws IOException {
        if (this.f1906b == null) {
            for (bz next : this.c) {
                brVar.a(next.f1909a);
                brVar.a(next.f1910b);
            }
            return;
        }
        throw new NoSuchMethodError();
    }

    public final boolean equals(Object obj) {
        List<bz> list;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof bv)) {
            return false;
        }
        bv bvVar = (bv) obj;
        if (this.f1906b == null || bvVar.f1906b == null) {
            List<bz> list2 = this.c;
            if (list2 != null && (list = bvVar.c) != null) {
                return list2.equals(list);
            }
            try {
                return Arrays.equals(b(), bvVar.b());
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        } else {
            bt<?, ?> btVar = this.f1905a;
            if (btVar != bvVar.f1905a) {
                return false;
            }
            if (!btVar.f1902a.isArray()) {
                return this.f1906b.equals(bvVar.f1906b);
            }
            Object obj2 = this.f1906b;
            return obj2 instanceof byte[] ? Arrays.equals((byte[]) obj2, (byte[]) bvVar.f1906b) : obj2 instanceof int[] ? Arrays.equals((int[]) obj2, (int[]) bvVar.f1906b) : obj2 instanceof long[] ? Arrays.equals((long[]) obj2, (long[]) bvVar.f1906b) : obj2 instanceof float[] ? Arrays.equals((float[]) obj2, (float[]) bvVar.f1906b) : obj2 instanceof double[] ? Arrays.equals((double[]) obj2, (double[]) bvVar.f1906b) : obj2 instanceof boolean[] ? Arrays.equals((boolean[]) obj2, (boolean[]) bvVar.f1906b) : Arrays.deepEquals((Object[]) obj2, (Object[]) bvVar.f1906b);
        }
    }

    public final int hashCode() {
        try {
            return Arrays.hashCode(b()) + 527;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private final byte[] b() throws IOException {
        byte[] bArr = new byte[a()];
        a(br.a(bArr, bArr.length));
        return bArr;
    }
}
