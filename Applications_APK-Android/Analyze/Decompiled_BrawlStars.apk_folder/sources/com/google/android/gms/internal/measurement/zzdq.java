package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;

public interface zzdq extends IInterface {
    void a(Bundle bundle) throws RemoteException;
}
