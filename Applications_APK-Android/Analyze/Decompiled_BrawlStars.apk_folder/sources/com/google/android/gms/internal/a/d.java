package com.google.android.gms.internal.a;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

public class d extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private static volatile e f1866a;

    public d() {
    }

    public d(Looper looper) {
        super(looper);
    }

    public d(Looper looper, Handler.Callback callback) {
        super(looper, callback);
    }

    public final void dispatchMessage(Message message) {
        super.dispatchMessage(message);
    }
}
