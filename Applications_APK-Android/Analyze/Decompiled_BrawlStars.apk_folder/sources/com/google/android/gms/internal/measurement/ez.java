package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.fq;
import java.io.IOException;
import java.util.List;
import java.util.Map;

final class ez implements iv {

    /* renamed from: a  reason: collision with root package name */
    private final zztv f2195a;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.fs.a(java.lang.Object, java.lang.String):T
     arg types: [com.google.android.gms.internal.measurement.zztv, java.lang.String]
     candidates:
      com.google.android.gms.internal.measurement.fs.a(java.lang.Object, java.lang.Object):java.lang.Object
      com.google.android.gms.internal.measurement.fs.a(java.lang.Object, java.lang.String):T */
    ez(zztv zztv) {
        this.f2195a = (zztv) fs.a((Object) zztv, "output");
        this.f2195a.f2350b = this;
    }

    public final int a() {
        return fq.e.j;
    }

    public final void a(int i, int i2) throws IOException {
        this.f2195a.e(i, i2);
    }

    public final void a(int i, long j) throws IOException {
        this.f2195a.a(i, j);
    }

    public final void b(int i, long j) throws IOException {
        this.f2195a.c(i, j);
    }

    public final void a(int i, float f) throws IOException {
        this.f2195a.a(i, f);
    }

    public final void a(int i, double d) throws IOException {
        this.f2195a.a(i, d);
    }

    public final void b(int i, int i2) throws IOException {
        this.f2195a.b(i, i2);
    }

    public final void c(int i, long j) throws IOException {
        this.f2195a.a(i, j);
    }

    public final void c(int i, int i2) throws IOException {
        this.f2195a.b(i, i2);
    }

    public final void d(int i, long j) throws IOException {
        this.f2195a.c(i, j);
    }

    public final void d(int i, int i2) throws IOException {
        this.f2195a.e(i, i2);
    }

    public final void a(int i, boolean z) throws IOException {
        this.f2195a.a(i, z);
    }

    public final void a(int i, String str) throws IOException {
        this.f2195a.a(i, str);
    }

    public final void a(int i, ej ejVar) throws IOException {
        this.f2195a.a(i, ejVar);
    }

    public final void e(int i, int i2) throws IOException {
        this.f2195a.c(i, i2);
    }

    public final void f(int i, int i2) throws IOException {
        this.f2195a.d(i, i2);
    }

    public final void e(int i, long j) throws IOException {
        this.f2195a.b(i, j);
    }

    public final void a(int i, Object obj, hj hjVar) throws IOException {
        this.f2195a.a(i, (gt) obj, hjVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.hj.a(java.lang.Object, com.google.android.gms.internal.measurement.iv):void
     arg types: [com.google.android.gms.internal.measurement.gt, com.google.android.gms.internal.measurement.ez]
     candidates:
      com.google.android.gms.internal.measurement.hj.a(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.hj.a(java.lang.Object, com.google.android.gms.internal.measurement.iv):void */
    public final void b(int i, Object obj, hj hjVar) throws IOException {
        zztv zztv = this.f2195a;
        zztv.a(i, 3);
        hjVar.a((Object) ((gt) obj), (iv) zztv.f2350b);
        zztv.a(i, 4);
    }

    public final void a(int i) throws IOException {
        this.f2195a.a(i, 3);
    }

    public final void b(int i) throws IOException {
        this.f2195a.a(i, 4);
    }

    public final void a(int i, Object obj) throws IOException {
        if (obj instanceof ej) {
            this.f2195a.b(i, (ej) obj);
        } else {
            this.f2195a.b(i, (gt) obj);
        }
    }

    public final void a(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2195a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zztv.m(list.get(i4).intValue());
            }
            this.f2195a.b(i3);
            while (i2 < list.size()) {
                this.f2195a.a(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2195a.b(i, list.get(i2).intValue());
            i2++;
        }
    }

    public final void b(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2195a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                list.get(i4).intValue();
                i3 += zztv.a();
            }
            this.f2195a.b(i3);
            while (i2 < list.size()) {
                this.f2195a.d(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2195a.e(i, list.get(i2).intValue());
            i2++;
        }
    }

    public final void c(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2195a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zztv.d(list.get(i4).longValue());
            }
            this.f2195a.b(i3);
            while (i2 < list.size()) {
                this.f2195a.a(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2195a.a(i, list.get(i2).longValue());
            i2++;
        }
    }

    public final void d(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2195a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zztv.e(list.get(i4).longValue());
            }
            this.f2195a.b(i3);
            while (i2 < list.size()) {
                this.f2195a.a(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2195a.a(i, list.get(i2).longValue());
            i2++;
        }
    }

    public final void e(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2195a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                list.get(i4).longValue();
                i3 += zztv.c();
            }
            this.f2195a.b(i3);
            while (i2 < list.size()) {
                this.f2195a.c(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2195a.c(i, list.get(i2).longValue());
            i2++;
        }
    }

    public final void f(int i, List<Float> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2195a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                list.get(i4).floatValue();
                i3 += zztv.e();
            }
            this.f2195a.b(i3);
            while (i2 < list.size()) {
                this.f2195a.a(list.get(i2).floatValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2195a.a(i, list.get(i2).floatValue());
            i2++;
        }
    }

    public final void g(int i, List<Double> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2195a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                list.get(i4).doubleValue();
                i3 += zztv.f();
            }
            this.f2195a.b(i3);
            while (i2 < list.size()) {
                this.f2195a.a(list.get(i2).doubleValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2195a.a(i, list.get(i2).doubleValue());
            i2++;
        }
    }

    public final void h(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2195a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zztv.p(list.get(i4).intValue());
            }
            this.f2195a.b(i3);
            while (i2 < list.size()) {
                this.f2195a.a(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2195a.b(i, list.get(i2).intValue());
            i2++;
        }
    }

    public final void i(int i, List<Boolean> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2195a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                list.get(i4).booleanValue();
                i3 += zztv.g();
            }
            this.f2195a.b(i3);
            while (i2 < list.size()) {
                this.f2195a.a(list.get(i2).booleanValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2195a.a(i, list.get(i2).booleanValue());
            i2++;
        }
    }

    public final void a(int i, List<String> list) throws IOException {
        int i2 = 0;
        if (list instanceof gd) {
            gd gdVar = (gd) list;
            while (i2 < list.size()) {
                Object b2 = gdVar.b(i2);
                if (b2 instanceof String) {
                    this.f2195a.a(i, (String) b2);
                } else {
                    this.f2195a.a(i, (ej) b2);
                }
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2195a.a(i, list.get(i2));
            i2++;
        }
    }

    public final void b(int i, List<ej> list) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            this.f2195a.a(i, list.get(i2));
        }
    }

    public final void j(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2195a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zztv.n(list.get(i4).intValue());
            }
            this.f2195a.b(i3);
            while (i2 < list.size()) {
                this.f2195a.b(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2195a.c(i, list.get(i2).intValue());
            i2++;
        }
    }

    public final void k(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2195a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                list.get(i4).intValue();
                i3 += zztv.b();
            }
            this.f2195a.b(i3);
            while (i2 < list.size()) {
                this.f2195a.d(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2195a.e(i, list.get(i2).intValue());
            i2++;
        }
    }

    public final void l(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2195a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                list.get(i4).longValue();
                i3 += zztv.d();
            }
            this.f2195a.b(i3);
            while (i2 < list.size()) {
                this.f2195a.c(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2195a.c(i, list.get(i2).longValue());
            i2++;
        }
    }

    public final void m(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2195a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zztv.o(list.get(i4).intValue());
            }
            this.f2195a.b(i3);
            while (i2 < list.size()) {
                this.f2195a.c(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2195a.d(i, list.get(i2).intValue());
            i2++;
        }
    }

    public final void n(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.f2195a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zztv.f(list.get(i4).longValue());
            }
            this.f2195a.b(i3);
            while (i2 < list.size()) {
                this.f2195a.b(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f2195a.b(i, list.get(i2).longValue());
            i2++;
        }
    }

    public final void a(int i, List<?> list, hj hjVar) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            a(i, list.get(i2), hjVar);
        }
    }

    public final void b(int i, List<?> list, hj hjVar) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            b(i, list.get(i2), hjVar);
        }
    }

    public final <K, V> void a(int i, gm<K, V> gmVar, Map<K, V> map) throws IOException {
        for (Map.Entry next : map.entrySet()) {
            this.f2195a.a(i, 2);
            this.f2195a.b(gl.a(gmVar, next.getKey(), next.getValue()));
            gl.a(this.f2195a, gmVar, next.getKey(), next.getValue());
        }
    }
}
