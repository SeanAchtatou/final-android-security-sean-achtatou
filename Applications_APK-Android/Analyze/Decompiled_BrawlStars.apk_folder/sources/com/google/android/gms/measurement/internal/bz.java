package com.google.android.gms.measurement.internal;

import com.google.android.gms.measurement.AppMeasurement;

final class bz implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ AppMeasurement.ConditionalUserProperty f2450a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ bv f2451b;

    bz(bv bvVar, AppMeasurement.ConditionalUserProperty conditionalUserProperty) {
        this.f2451b = bvVar;
        this.f2450a = conditionalUserProperty;
    }

    public final void run() {
        bv.b(this.f2451b, this.f2450a);
    }
}
