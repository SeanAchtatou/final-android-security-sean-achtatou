package com.google.android.gms.measurement.internal;

abstract class df extends cj {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2506a;

    df(ar arVar) {
        super(arVar);
        this.r.j++;
    }

    /* access modifiers changed from: protected */
    public abstract boolean t();

    /* access modifiers changed from: protected */
    public void u() {
    }

    /* access modifiers changed from: package-private */
    public final boolean C() {
        return this.f2506a;
    }

    /* access modifiers changed from: protected */
    public final void D() {
        if (!C()) {
            throw new IllegalStateException("Not initialized");
        }
    }

    public final void E() {
        if (this.f2506a) {
            throw new IllegalStateException("Can't initialize twice");
        } else if (!t()) {
            this.r.t();
            this.f2506a = true;
        }
    }

    public final void F() {
        if (!this.f2506a) {
            u();
            this.r.t();
            this.f2506a = true;
            return;
        }
        throw new IllegalStateException("Can't initialize twice");
    }
}
