package com.google.android.gms.common.api.internal;

import android.os.Looper;
import android.os.Message;
import com.google.android.gms.internal.base.d;

final class ao extends d {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ am f1400a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ao(am amVar, Looper looper) {
        super(looper);
        this.f1400a = amVar;
    }

    public final void handleMessage(Message message) {
        int i = message.what;
        if (i == 1) {
            ((an) message.obj).a(this.f1400a);
        } else if (i != 2) {
            int i2 = message.what;
            StringBuilder sb = new StringBuilder(31);
            sb.append("Unknown message id: ");
            sb.append(i2);
            sb.toString();
        } else {
            throw ((RuntimeException) message.obj);
        }
    }
}
