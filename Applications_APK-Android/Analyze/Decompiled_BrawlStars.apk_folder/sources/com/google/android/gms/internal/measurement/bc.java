package com.google.android.gms.internal.measurement;

import com.google.android.gms.common.a.a;

public final class bc {
    public static bd<Long> A = bd.a("analytics.service_client.idle_disconnect_millis", 10000L, 10000L);
    public static bd<Long> B = bd.a("analytics.service_client.connect_timeout_millis", 5000L, 5000L);
    public static bd<Long> C = bd.a("analytics.service_client.reconnect_throttle_millis", 1800000L, 1800000L);
    public static bd<Long> D = bd.a("analytics.monitoring.sample_period_millis", 86400000L, 86400000L);
    public static bd<Long> E = bd.a("analytics.initialization_warning_threshold", 5000L, 5000L);
    public static bd<Boolean> F = bd.a("analytics.gcm_task_service", false, false);
    private static bd<Boolean> G = bd.a("analytics.service_enabled", false, false);
    private static bd<Long> H = bd.a("analytics.max_tokens", 60L, 60L);
    private static bd<Float> I;
    private static bd<Integer> J = bd.a("analytics.max_stored_hits_per_app", 2000, 2000);
    private static bd<Long> K = bd.a("analytics.min_local_dispatch_millis", 120000L, 120000L);
    private static bd<Long> L = bd.a("analytics.max_local_dispatch_millis", 7200000L, 7200000L);
    private static bd<Integer> M = bd.a("analytics.max_hits_per_request.k", 20, 20);
    private static bd<Long> N = bd.a("analytics.service_monitor_interval", 86400000L, 86400000L);
    private static bd<String> O = bd.a("analytics.first_party_experiment_id", "", "");
    private static bd<Integer> P = bd.a("analytics.first_party_experiment_variant", 0, 0);
    private static bd<Long> Q = bd.a("analytics.service_client.second_connect_delay_millis", 5000L, 5000L);
    private static bd<Long> R = bd.a("analytics.service_client.unexpected_reconnect_millis", 60000L, 60000L);

    /* renamed from: a  reason: collision with root package name */
    public static bd<Boolean> f2065a = bd.a("analytics.service_client_enabled", true, true);

    /* renamed from: b  reason: collision with root package name */
    public static bd<String> f2066b = bd.a("analytics.log_tag", "GAv4", "GAv4-SVC");
    public static bd<Integer> c = bd.a("analytics.max_stored_hits", 2000, 20000);
    public static bd<Integer> d = bd.a("analytics.max_stored_properties_per_app", 100, 100);
    public static bd<Long> e = bd.a("analytics.local_dispatch_millis", 1800000L, 120000L);
    public static bd<Long> f = bd.a("analytics.initial_local_dispatch_millis", 5000L, 5000L);
    public static bd<Long> g = bd.a("analytics.dispatch_alarm_millis", 7200000L, 7200000L);
    public static bd<Long> h = bd.a("analytics.max_dispatch_alarm_millis", 32400000L, 32400000L);
    public static bd<Integer> i = bd.a("analytics.max_hits_per_dispatch", 20, 20);
    public static bd<Integer> j = bd.a("analytics.max_hits_per_batch", 20, 20);
    public static bd<String> k = bd.a("analytics.insecure_host", "http://www.google-analytics.com", "http://www.google-analytics.com");
    public static bd<String> l = bd.a("analytics.secure_host", "https://ssl.google-analytics.com", "https://ssl.google-analytics.com");
    public static bd<String> m = bd.a("analytics.simple_endpoint", "/collect", "/collect");
    public static bd<String> n = bd.a("analytics.batching_endpoint", "/batch", "/batch");
    public static bd<Integer> o = bd.a("analytics.max_get_length", 2036, 2036);
    public static bd<String> p = bd.a("analytics.batching_strategy.k", al.BATCH_BY_COUNT.name(), al.BATCH_BY_COUNT.name());
    public static bd<String> q;
    public static bd<Integer> r = bd.a("analytics.max_hit_length.k", 8192, 8192);
    public static bd<Integer> s = bd.a("analytics.max_post_length.k", 8192, 8192);
    public static bd<Integer> t = bd.a("analytics.max_batch_post_length", 8192, 8192);
    public static bd<String> u = bd.a("analytics.fallback_responses.k", "404,502", "404,502");
    public static bd<Integer> v = bd.a("analytics.batch_retry_interval.seconds.k", 3600, 3600);
    public static bd<Integer> w = bd.a("analytics.http_connection.connect_timeout_millis", 60000, 60000);
    public static bd<Integer> x = bd.a("analytics.http_connection.read_timeout_millis", 61000, 61000);
    public static bd<Long> y = bd.a("analytics.campaigns.time_limit", 86400000L, 86400000L);
    public static bd<Boolean> z = bd.a("analytics.test.disable_receiver", false, false);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.bd.a(java.lang.String, boolean, boolean):com.google.android.gms.internal.measurement.bd<java.lang.Boolean>
     arg types: [java.lang.String, int, int]
     candidates:
      com.google.android.gms.internal.measurement.bd.a(java.lang.String, int, int):com.google.android.gms.internal.measurement.bd<java.lang.Integer>
      com.google.android.gms.internal.measurement.bd.a(java.lang.String, long, long):com.google.android.gms.internal.measurement.bd<java.lang.Long>
      com.google.android.gms.internal.measurement.bd.a(java.lang.String, java.lang.String, java.lang.String):com.google.android.gms.internal.measurement.bd<java.lang.String>
      com.google.android.gms.internal.measurement.bd.a(java.lang.String, boolean, boolean):com.google.android.gms.internal.measurement.bd<java.lang.Boolean> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.bd.a(java.lang.String, long, long):com.google.android.gms.internal.measurement.bd<java.lang.Long>
     arg types: [java.lang.String, int, int]
     candidates:
      com.google.android.gms.internal.measurement.bd.a(java.lang.String, int, int):com.google.android.gms.internal.measurement.bd<java.lang.Integer>
      com.google.android.gms.internal.measurement.bd.a(java.lang.String, java.lang.String, java.lang.String):com.google.android.gms.internal.measurement.bd<java.lang.String>
      com.google.android.gms.internal.measurement.bd.a(java.lang.String, boolean, boolean):com.google.android.gms.internal.measurement.bd<java.lang.Boolean>
      com.google.android.gms.internal.measurement.bd.a(java.lang.String, long, long):com.google.android.gms.internal.measurement.bd<java.lang.Long> */
    static {
        Float valueOf = Float.valueOf(0.5f);
        I = new bd<>(a.a("analytics.tokens_per_sec", valueOf), valueOf);
        String name = aq.GZIP.name();
        q = bd.a("analytics.compression_strategy.k", name, name);
    }
}
