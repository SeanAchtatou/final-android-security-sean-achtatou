package com.google.android.gms.games.internal.player;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class c implements Parcelable.Creator<StockProfileImageEntity> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new StockProfileImageEntity[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        String str = null;
        Uri uri = null;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            int i = 65535 & readInt;
            if (i == 1) {
                str = SafeParcelReader.l(parcel, readInt);
            } else if (i != 2) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                uri = (Uri) SafeParcelReader.a(parcel, readInt, Uri.CREATOR);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new StockProfileImageEntity(str, uri);
    }
}
