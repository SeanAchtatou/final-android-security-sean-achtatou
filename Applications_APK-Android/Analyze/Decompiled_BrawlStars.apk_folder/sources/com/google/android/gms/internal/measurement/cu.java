package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class cu extends iz<cu> {
    private static volatile cu[] e;

    /* renamed from: a  reason: collision with root package name */
    public String f2138a = null;

    /* renamed from: b  reason: collision with root package name */
    public String f2139b = null;
    public Long c = null;
    public Double d = null;
    private Float f = null;

    public static cu[] a() {
        if (e == null) {
            synchronized (jd.f2312b) {
                if (e == null) {
                    e = new cu[0];
                }
            }
        }
        return e;
    }

    public cu() {
        this.L = null;
        this.M = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof cu)) {
            return false;
        }
        cu cuVar = (cu) obj;
        String str = this.f2138a;
        if (str == null) {
            if (cuVar.f2138a != null) {
                return false;
            }
        } else if (!str.equals(cuVar.f2138a)) {
            return false;
        }
        String str2 = this.f2139b;
        if (str2 == null) {
            if (cuVar.f2139b != null) {
                return false;
            }
        } else if (!str2.equals(cuVar.f2139b)) {
            return false;
        }
        Long l = this.c;
        if (l == null) {
            if (cuVar.c != null) {
                return false;
            }
        } else if (!l.equals(cuVar.c)) {
            return false;
        }
        Float f2 = this.f;
        if (f2 == null) {
            if (cuVar.f != null) {
                return false;
            }
        } else if (!f2.equals(cuVar.f)) {
            return false;
        }
        Double d2 = this.d;
        if (d2 == null) {
            if (cuVar.d != null) {
                return false;
            }
        } else if (!d2.equals(cuVar.d)) {
            return false;
        }
        if (this.L == null || this.L.a()) {
            return cuVar.L == null || cuVar.L.a();
        }
        return this.L.equals(cuVar.L);
    }

    public final int hashCode() {
        int hashCode = (getClass().getName().hashCode() + 527) * 31;
        String str = this.f2138a;
        int i = 0;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.f2139b;
        int hashCode3 = (hashCode2 + (str2 == null ? 0 : str2.hashCode())) * 31;
        Long l = this.c;
        int hashCode4 = (hashCode3 + (l == null ? 0 : l.hashCode())) * 31;
        Float f2 = this.f;
        int hashCode5 = (hashCode4 + (f2 == null ? 0 : f2.hashCode())) * 31;
        Double d2 = this.d;
        int hashCode6 = (hashCode5 + (d2 == null ? 0 : d2.hashCode())) * 31;
        if (this.L != null && !this.L.a()) {
            i = this.L.hashCode();
        }
        return hashCode6 + i;
    }

    public final void a(iy iyVar) throws IOException {
        String str = this.f2138a;
        if (str != null) {
            iyVar.a(1, str);
        }
        String str2 = this.f2139b;
        if (str2 != null) {
            iyVar.a(2, str2);
        }
        Long l = this.c;
        if (l != null) {
            iyVar.b(3, l.longValue());
        }
        Float f2 = this.f;
        if (f2 != null) {
            iyVar.a(4, f2.floatValue());
        }
        Double d2 = this.d;
        if (d2 != null) {
            iyVar.a(5, d2.doubleValue());
        }
        super.a(iyVar);
    }

    /* access modifiers changed from: protected */
    public final int b() {
        int b2 = super.b();
        String str = this.f2138a;
        if (str != null) {
            b2 += iy.b(1, str);
        }
        String str2 = this.f2139b;
        if (str2 != null) {
            b2 += iy.b(2, str2);
        }
        Long l = this.c;
        if (l != null) {
            b2 += iy.c(3, l.longValue());
        }
        Float f2 = this.f;
        if (f2 != null) {
            f2.floatValue();
            b2 += iy.c(32) + 4;
        }
        Double d2 = this.d;
        if (d2 == null) {
            return b2;
        }
        d2.doubleValue();
        return b2 + iy.c(40) + 8;
    }

    public final /* synthetic */ je a(iw iwVar) throws IOException {
        while (true) {
            int a2 = iwVar.a();
            if (a2 == 0) {
                return this;
            }
            if (a2 == 10) {
                this.f2138a = iwVar.c();
            } else if (a2 == 18) {
                this.f2139b = iwVar.c();
            } else if (a2 == 24) {
                this.c = Long.valueOf(iwVar.e());
            } else if (a2 == 37) {
                this.f = Float.valueOf(Float.intBitsToFloat(iwVar.f()));
            } else if (a2 == 41) {
                this.d = Double.valueOf(Double.longBitsToDouble(iwVar.g()));
            } else if (!super.a(iwVar, a2)) {
                return this;
            }
        }
    }
}
