package com.google.android.gms.common.internal;

import android.os.Bundle;
import com.google.android.gms.common.api.d;
import com.google.android.gms.common.internal.BaseGmsClient;

final class t implements BaseGmsClient.a {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ d.b f1621a;

    t(d.b bVar) {
        this.f1621a = bVar;
    }

    public final void a(Bundle bundle) {
        this.f1621a.onConnected(bundle);
    }

    public final void a(int i) {
        this.f1621a.onConnectionSuspended(i);
    }
}
