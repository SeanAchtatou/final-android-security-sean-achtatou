package com.google.android.gms.drive.metadata.internal;

import android.os.Bundle;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.metadata.i;
import java.util.Date;

public class d extends i<Date> {
    public d(String str, int i) {
        super(str, i);
    }

    public final /* synthetic */ Object c(DataHolder dataHolder, int i, int i2) {
        return new Date(dataHolder.a(this.f1731b, i, i2));
    }

    public final /* synthetic */ void a(Bundle bundle, Object obj) {
        bundle.putLong(this.f1731b, ((Date) obj).getTime());
    }

    public final /* synthetic */ Object b(Bundle bundle) {
        return new Date(bundle.getLong(this.f1731b));
    }
}
