package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class cr extends iz<cr> {
    private static volatile cr[] e;

    /* renamed from: a  reason: collision with root package name */
    public Integer f2132a = null;

    /* renamed from: b  reason: collision with root package name */
    public cx f2133b = null;
    public cx c = null;
    public Boolean d = null;

    public static cr[] a() {
        if (e == null) {
            synchronized (jd.f2312b) {
                if (e == null) {
                    e = new cr[0];
                }
            }
        }
        return e;
    }

    public cr() {
        this.L = null;
        this.M = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof cr)) {
            return false;
        }
        cr crVar = (cr) obj;
        Integer num = this.f2132a;
        if (num == null) {
            if (crVar.f2132a != null) {
                return false;
            }
        } else if (!num.equals(crVar.f2132a)) {
            return false;
        }
        cx cxVar = this.f2133b;
        if (cxVar == null) {
            if (crVar.f2133b != null) {
                return false;
            }
        } else if (!cxVar.equals(crVar.f2133b)) {
            return false;
        }
        cx cxVar2 = this.c;
        if (cxVar2 == null) {
            if (crVar.c != null) {
                return false;
            }
        } else if (!cxVar2.equals(crVar.c)) {
            return false;
        }
        Boolean bool = this.d;
        if (bool == null) {
            if (crVar.d != null) {
                return false;
            }
        } else if (!bool.equals(crVar.d)) {
            return false;
        }
        if (this.L == null || this.L.a()) {
            return crVar.L == null || crVar.L.a();
        }
        return this.L.equals(crVar.L);
    }

    public final int hashCode() {
        int i;
        int i2;
        int hashCode = (getClass().getName().hashCode() + 527) * 31;
        Integer num = this.f2132a;
        int i3 = 0;
        int hashCode2 = hashCode + (num == null ? 0 : num.hashCode());
        cx cxVar = this.f2133b;
        int i4 = hashCode2 * 31;
        if (cxVar == null) {
            i = 0;
        } else {
            i = cxVar.hashCode();
        }
        int i5 = i4 + i;
        cx cxVar2 = this.c;
        int i6 = i5 * 31;
        if (cxVar2 == null) {
            i2 = 0;
        } else {
            i2 = cxVar2.hashCode();
        }
        int i7 = (i6 + i2) * 31;
        Boolean bool = this.d;
        int hashCode3 = (i7 + (bool == null ? 0 : bool.hashCode())) * 31;
        if (this.L != null && !this.L.a()) {
            i3 = this.L.hashCode();
        }
        return hashCode3 + i3;
    }

    public final void a(iy iyVar) throws IOException {
        Integer num = this.f2132a;
        if (num != null) {
            iyVar.a(1, num.intValue());
        }
        cx cxVar = this.f2133b;
        if (cxVar != null) {
            iyVar.a(2, cxVar);
        }
        cx cxVar2 = this.c;
        if (cxVar2 != null) {
            iyVar.a(3, cxVar2);
        }
        Boolean bool = this.d;
        if (bool != null) {
            iyVar.a(4, bool.booleanValue());
        }
        super.a(iyVar);
    }

    /* access modifiers changed from: protected */
    public final int b() {
        int b2 = super.b();
        Integer num = this.f2132a;
        if (num != null) {
            b2 += iy.b(1, num.intValue());
        }
        cx cxVar = this.f2133b;
        if (cxVar != null) {
            b2 += iy.b(2, cxVar);
        }
        cx cxVar2 = this.c;
        if (cxVar2 != null) {
            b2 += iy.b(3, cxVar2);
        }
        Boolean bool = this.d;
        if (bool == null) {
            return b2;
        }
        bool.booleanValue();
        return b2 + iy.c(32) + 1;
    }

    public final /* synthetic */ je a(iw iwVar) throws IOException {
        while (true) {
            int a2 = iwVar.a();
            if (a2 == 0) {
                return this;
            }
            if (a2 == 8) {
                this.f2132a = Integer.valueOf(iwVar.d());
            } else if (a2 == 18) {
                if (this.f2133b == null) {
                    this.f2133b = new cx();
                }
                iwVar.a(this.f2133b);
            } else if (a2 == 26) {
                if (this.c == null) {
                    this.c = new cx();
                }
                iwVar.a(this.c);
            } else if (a2 == 32) {
                this.d = Boolean.valueOf(iwVar.b());
            } else if (!super.a(iwVar, a2)) {
                return this;
            }
        }
    }
}
