package com.google.android.gms.internal.measurement;

final class gk implements gs {

    /* renamed from: a  reason: collision with root package name */
    private gs[] f2243a;

    gk(gs... gsVarArr) {
        this.f2243a = gsVarArr;
    }

    public final boolean a(Class<?> cls) {
        for (gs a2 : this.f2243a) {
            if (a2.a(cls)) {
                return true;
            }
        }
        return false;
    }

    public final gr b(Class<?> cls) {
        for (gs gsVar : this.f2243a) {
            if (gsVar.a(cls)) {
                return gsVar.b(cls);
            }
        }
        String valueOf = String.valueOf(cls.getName());
        throw new UnsupportedOperationException(valueOf.length() != 0 ? "No factory is available for message type: ".concat(valueOf) : new String("No factory is available for message type: "));
    }
}
