package com.google.android.gms.tasks;

import java.util.concurrent.Executor;

final class q<TResult> implements y<TResult> {

    /* renamed from: a  reason: collision with root package name */
    final Object f2694a = new Object();

    /* renamed from: b  reason: collision with root package name */
    c<TResult> f2695b;
    private final Executor c;

    public q(Executor executor, c<TResult> cVar) {
        this.c = executor;
        this.f2695b = cVar;
    }

    public final void a(g<TResult> gVar) {
        synchronized (this.f2694a) {
            if (this.f2695b != null) {
                this.c.execute(new r(this, gVar));
            }
        }
    }

    public final void a() {
        synchronized (this.f2694a) {
            this.f2695b = null;
        }
    }
}
