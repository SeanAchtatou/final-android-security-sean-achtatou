package com.google.android.gms.internal.measurement;

final class fg {

    /* renamed from: a  reason: collision with root package name */
    private static final fe<?> f2204a = new ff();

    /* renamed from: b  reason: collision with root package name */
    private static final fe<?> f2205b = c();

    private static fe<?> c() {
        try {
            return (fe) Class.forName("com.google.protobuf.ExtensionSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }

    static fe<?> a() {
        return f2204a;
    }

    static fe<?> b() {
        fe<?> feVar = f2205b;
        if (feVar != null) {
            return feVar;
        }
        throw new IllegalStateException("Protobuf runtime is not correctly loaded.");
    }
}
