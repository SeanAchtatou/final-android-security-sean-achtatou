package com.google.android.gms.internal.measurement;

public enum iu {
    INT(0),
    LONG(0L),
    FLOAT(Float.valueOf(0.0f)),
    DOUBLE(Double.valueOf(0.0d)),
    BOOLEAN(false),
    STRING(""),
    BYTE_STRING(ej.f2184a),
    ENUM(null),
    MESSAGE(null);
    
    private final Object j;

    private iu(Object obj) {
        this.j = obj;
    }
}
