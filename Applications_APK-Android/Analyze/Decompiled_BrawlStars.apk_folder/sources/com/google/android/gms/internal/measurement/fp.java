package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.fq;

final class fp implements gs {

    /* renamed from: a  reason: collision with root package name */
    private static final fp f2219a = new fp();

    private fp() {
    }

    public static fp a() {
        return f2219a;
    }

    public final boolean a(Class<?> cls) {
        return fq.class.isAssignableFrom(cls);
    }

    public final gr b(Class<?> cls) {
        if (!fq.class.isAssignableFrom(cls)) {
            String valueOf = String.valueOf(cls.getName());
            throw new IllegalArgumentException(valueOf.length() != 0 ? "Unsupported message type: ".concat(valueOf) : new String("Unsupported message type: "));
        }
        try {
            return (gr) fq.a(cls.asSubclass(fq.class)).a(fq.e.c);
        } catch (Exception e) {
            String valueOf2 = String.valueOf(cls.getName());
            throw new RuntimeException(valueOf2.length() != 0 ? "Unable to get message info for ".concat(valueOf2) : new String("Unable to get message info for "), e);
        }
    }
}
