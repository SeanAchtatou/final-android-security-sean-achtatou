package com.google.android.gms.common.util;

import android.os.SystemClock;

public class g implements e {

    /* renamed from: a  reason: collision with root package name */
    private static final g f1675a = new g();

    public static e d() {
        return f1675a;
    }

    public final long a() {
        return System.currentTimeMillis();
    }

    public final long b() {
        return SystemClock.elapsedRealtime();
    }

    public final long c() {
        return System.nanoTime();
    }

    private g() {
    }
}
