package com.google.android.gms.tasks;

final class n implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ g f2689a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ m f2690b;

    n(m mVar, g gVar) {
        this.f2690b = mVar;
        this.f2689a = gVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.e):com.google.android.gms.tasks.g<TResult>
     arg types: [java.util.concurrent.Executor, com.google.android.gms.tasks.m]
     candidates:
      com.google.android.gms.tasks.g.a(android.app.Activity, com.google.android.gms.tasks.e):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.a):com.google.android.gms.tasks.g<TContinuationResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.b):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.c):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.d):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.f):com.google.android.gms.tasks.g<TContinuationResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.e):com.google.android.gms.tasks.g<TResult> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.d):com.google.android.gms.tasks.g<TResult>
     arg types: [java.util.concurrent.Executor, com.google.android.gms.tasks.m]
     candidates:
      com.google.android.gms.tasks.g.a(android.app.Activity, com.google.android.gms.tasks.e):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.a):com.google.android.gms.tasks.g<TContinuationResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.b):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.c):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.e):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.f):com.google.android.gms.tasks.g<TContinuationResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.d):com.google.android.gms.tasks.g<TResult> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.b):com.google.android.gms.tasks.g<TResult>
     arg types: [java.util.concurrent.Executor, com.google.android.gms.tasks.m]
     candidates:
      com.google.android.gms.tasks.g.a(android.app.Activity, com.google.android.gms.tasks.e):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.a):com.google.android.gms.tasks.g<TContinuationResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.c):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.d):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.e):com.google.android.gms.tasks.g<TResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.f):com.google.android.gms.tasks.g<TContinuationResult>
      com.google.android.gms.tasks.g.a(java.util.concurrent.Executor, com.google.android.gms.tasks.b):com.google.android.gms.tasks.g<TResult> */
    public final void run() {
        try {
            g a2 = this.f2690b.f2687a.a(this.f2689a);
            if (a2 == null) {
                this.f2690b.a((Exception) new NullPointerException("Continuation returned null"));
                return;
            }
            a2.a(i.f2680b, (e) this.f2690b);
            a2.a(i.f2680b, (d) this.f2690b);
            a2.a(i.f2680b, (b) this.f2690b);
        } catch (RuntimeExecutionException e) {
            if (e.getCause() instanceof Exception) {
                this.f2690b.f2688b.a((Exception) e.getCause());
            } else {
                this.f2690b.f2688b.a((Exception) e);
            }
        } catch (Exception e2) {
            this.f2690b.f2688b.a(e2);
        }
    }
}
