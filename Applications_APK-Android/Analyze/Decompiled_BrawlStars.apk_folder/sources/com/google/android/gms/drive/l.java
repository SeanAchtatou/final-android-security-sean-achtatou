package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class l implements Parcelable.Creator<UserMetadata> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new UserMetadata[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        boolean z = false;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            int i = 65535 & readInt;
            if (i == 2) {
                str = SafeParcelReader.l(parcel, readInt);
            } else if (i == 3) {
                str2 = SafeParcelReader.l(parcel, readInt);
            } else if (i == 4) {
                str3 = SafeParcelReader.l(parcel, readInt);
            } else if (i == 5) {
                z = SafeParcelReader.c(parcel, readInt);
            } else if (i != 6) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                str4 = SafeParcelReader.l(parcel, readInt);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new UserMetadata(str, str2, str3, z, str4);
    }
}
