package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.internal.measurement.dk;
import com.google.android.gms.internal.measurement.zzr;
import java.util.List;

public abstract class zzak extends zzr implements zzaj {
    public zzak() {
        super("com.google.android.gms.measurement.internal.IMeasurementService");
    }

    public final boolean a(int i, Parcel parcel, Parcel parcel2) throws RemoteException {
        switch (i) {
            case 1:
                a((zzag) dk.a(parcel, zzag.CREATOR), (zzk) dk.a(parcel, zzk.CREATOR));
                parcel2.writeNoException();
                return true;
            case 2:
                a((zzfv) dk.a(parcel, zzfv.CREATOR), (zzk) dk.a(parcel, zzk.CREATOR));
                parcel2.writeNoException();
                return true;
            case 3:
            case 8:
            default:
                return false;
            case 4:
                a((zzk) dk.a(parcel, zzk.CREATOR));
                parcel2.writeNoException();
                return true;
            case 5:
                a((zzag) dk.a(parcel, zzag.CREATOR), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                return true;
            case 6:
                b((zzk) dk.a(parcel, zzk.CREATOR));
                parcel2.writeNoException();
                return true;
            case 7:
                List<zzfv> a2 = a((zzk) dk.a(parcel, zzk.CREATOR), dk.a(parcel));
                parcel2.writeNoException();
                parcel2.writeTypedList(a2);
                return true;
            case 9:
                byte[] a3 = a((zzag) dk.a(parcel, zzag.CREATOR), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeByteArray(a3);
                return true;
            case 10:
                a(parcel.readLong(), parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                return true;
            case 11:
                String c = c((zzk) dk.a(parcel, zzk.CREATOR));
                parcel2.writeNoException();
                parcel2.writeString(c);
                return true;
            case 12:
                a((zzo) dk.a(parcel, zzo.CREATOR), (zzk) dk.a(parcel, zzk.CREATOR));
                parcel2.writeNoException();
                return true;
            case 13:
                a((zzo) dk.a(parcel, zzo.CREATOR));
                parcel2.writeNoException();
                return true;
            case 14:
                List<zzfv> a4 = a(parcel.readString(), parcel.readString(), dk.a(parcel), (zzk) dk.a(parcel, zzk.CREATOR));
                parcel2.writeNoException();
                parcel2.writeTypedList(a4);
                return true;
            case 15:
                List<zzfv> a5 = a(parcel.readString(), parcel.readString(), parcel.readString(), dk.a(parcel));
                parcel2.writeNoException();
                parcel2.writeTypedList(a5);
                return true;
            case 16:
                List<zzo> a6 = a(parcel.readString(), parcel.readString(), (zzk) dk.a(parcel, zzk.CREATOR));
                parcel2.writeNoException();
                parcel2.writeTypedList(a6);
                return true;
            case 17:
                List<zzo> a7 = a(parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeTypedList(a7);
                return true;
            case 18:
                d((zzk) dk.a(parcel, zzk.CREATOR));
                parcel2.writeNoException();
                return true;
        }
    }
}
