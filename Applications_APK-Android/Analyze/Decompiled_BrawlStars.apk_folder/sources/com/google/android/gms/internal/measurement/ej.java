package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.Comparator;
import java.util.Iterator;

public abstract class ej implements Serializable, Iterable<Byte> {

    /* renamed from: a  reason: collision with root package name */
    public static final ej f2184a = new et(fs.f2227b);
    private static final ep c = (eg.a() ? new eu((byte) 0) : new en((byte) 0));
    private static final Comparator<ej> d = new el();

    /* renamed from: b  reason: collision with root package name */
    int f2185b = 0;

    ej() {
    }

    static /* synthetic */ int a(byte b2) {
        return b2 & 255;
    }

    public abstract byte a(int i);

    public abstract int a();

    /* access modifiers changed from: protected */
    public abstract int a(int i, int i2);

    /* access modifiers changed from: protected */
    public abstract String a(Charset charset);

    /* access modifiers changed from: package-private */
    public abstract void a(ei eiVar) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract byte b(int i);

    public abstract ej c(int i);

    public abstract boolean c();

    public abstract boolean equals(Object obj);

    public static ej a(byte[] bArr, int i, int i2) {
        a(i, i + i2, bArr.length);
        return new et(c.a(bArr, i, i2));
    }

    static ej a(byte[] bArr) {
        return new et(bArr);
    }

    public static ej a(String str) {
        return new et(str.getBytes(fs.f2226a));
    }

    public final String b() {
        return a() == 0 ? "" : a(fs.f2226a);
    }

    public final int hashCode() {
        int i = this.f2185b;
        if (i == 0) {
            int a2 = a();
            i = a(a2, a2);
            if (i == 0) {
                i = 1;
            }
            this.f2185b = i;
        }
        return i;
    }

    static er d(int i) {
        return new er(i, (byte) 0);
    }

    static int a(int i, int i2, int i3) {
        int i4 = i2 - i;
        if ((i | i2 | i4 | (i3 - i2)) >= 0) {
            return i4;
        }
        if (i < 0) {
            StringBuilder sb = new StringBuilder(32);
            sb.append("Beginning index: ");
            sb.append(i);
            sb.append(" < 0");
            throw new IndexOutOfBoundsException(sb.toString());
        } else if (i2 < i) {
            StringBuilder sb2 = new StringBuilder(66);
            sb2.append("Beginning index larger than ending index: ");
            sb2.append(i);
            sb2.append(", ");
            sb2.append(i2);
            throw new IndexOutOfBoundsException(sb2.toString());
        } else {
            StringBuilder sb3 = new StringBuilder(37);
            sb3.append("End index: ");
            sb3.append(i2);
            sb3.append(" >= ");
            sb3.append(i3);
            throw new IndexOutOfBoundsException(sb3.toString());
        }
    }

    public final String toString() {
        return String.format("<ByteString@%s size=%d>", Integer.toHexString(System.identityHashCode(this)), Integer.valueOf(a()));
    }

    public /* synthetic */ Iterator iterator() {
        return new ek(this);
    }
}
