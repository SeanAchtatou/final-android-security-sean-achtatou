package com.google.android.gms.measurement.internal;

final class dn implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ long f2516a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ dj f2517b;

    dn(dj djVar, long j) {
        this.f2517b = djVar;
        this.f2516a = j;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public final void run() {
        dj djVar = this.f2517b;
        long j = this.f2516a;
        djVar.c();
        djVar.v();
        djVar.q().k.a("Activity resumed, time", Long.valueOf(j));
        djVar.f2511a = j;
        djVar.f2512b = djVar.f2511a;
        if (djVar.s().j(djVar.f().v())) {
            djVar.a(djVar.l().a());
            return;
        }
        djVar.c.c();
        djVar.d.c();
        if (djVar.s().h(djVar.f().v()) || djVar.s().i(djVar.f().v())) {
            djVar.e.c();
        }
        if (djVar.r().a(djVar.l().a())) {
            djVar.r().n.a(true);
            djVar.r().p.a(0);
        }
        if (djVar.r().n.a()) {
            djVar.c.a(Math.max(0L, djVar.r().l.a() - djVar.r().p.a()));
        } else {
            djVar.d.a(Math.max(0L, 3600000 - djVar.r().p.a()));
        }
    }
}
