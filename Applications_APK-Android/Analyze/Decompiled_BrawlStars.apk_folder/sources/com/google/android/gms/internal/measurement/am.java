package com.google.android.gms.internal.measurement;

import android.content.Context;
import com.facebook.appevents.AppEventsConstants;
import com.google.android.gms.common.internal.l;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public final class am extends r {

    /* renamed from: a  reason: collision with root package name */
    private volatile String f2049a;

    /* renamed from: b  reason: collision with root package name */
    private Future<String> f2050b;

    protected am(t tVar) {
        super(tVar);
    }

    /* access modifiers changed from: protected */
    public final void a() {
    }

    public final String b() {
        String str;
        m();
        synchronized (this) {
            if (this.f2049a == null) {
                this.f2050b = this.c.b().a(new an(this));
            }
            if (this.f2050b != null) {
                try {
                    this.f2049a = this.f2050b.get();
                } catch (InterruptedException e) {
                    d("ClientId loading or generation was interrupted", e);
                    this.f2049a = AppEventsConstants.EVENT_PARAM_VALUE_NO;
                } catch (ExecutionException e2) {
                    e("Failed to load or generate client id", e2);
                    this.f2049a = AppEventsConstants.EVENT_PARAM_VALUE_NO;
                }
                if (this.f2049a == null) {
                    this.f2049a = AppEventsConstants.EVENT_PARAM_VALUE_NO;
                }
                a("Loaded clientId", this.f2049a);
                this.f2050b = null;
            }
            str = this.f2049a;
        }
        return str;
    }

    /* access modifiers changed from: package-private */
    public final String c() {
        synchronized (this) {
            this.f2049a = null;
            this.f2050b = this.c.b().a(new ao(this));
        }
        return b();
    }

    /* access modifiers changed from: package-private */
    public final String e() {
        String lowerCase = UUID.randomUUID().toString().toLowerCase(Locale.US);
        try {
            if (!a(this.c.b().f1331a, lowerCase)) {
                return AppEventsConstants.EVENT_PARAM_VALUE_NO;
            }
            return lowerCase;
        } catch (Exception e) {
            e("Error saving clientId file", e);
            return AppEventsConstants.EVENT_PARAM_VALUE_NO;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:43:0x0078 A[SYNTHETIC, Splitter:B:43:0x0078] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0084 A[SYNTHETIC, Splitter:B:50:0x0084] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0090 A[SYNTHETIC, Splitter:B:58:0x0090] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final java.lang.String a(android.content.Context r9) {
        /*
            r8 = this;
            java.lang.String r0 = "gaClientId"
            java.lang.String r1 = "Failed to close client id reading stream"
            java.lang.String r2 = "ClientId should be loaded from worker thread"
            com.google.android.gms.common.internal.l.c(r2)
            r2 = 0
            java.io.FileInputStream r3 = r9.openFileInput(r0)     // Catch:{ FileNotFoundException -> 0x008d, IOException -> 0x006c, all -> 0x0069 }
            r4 = 36
            byte[] r5 = new byte[r4]     // Catch:{ FileNotFoundException -> 0x0067, IOException -> 0x0065 }
            r6 = 0
            int r4 = r3.read(r5, r6, r4)     // Catch:{ FileNotFoundException -> 0x0067, IOException -> 0x0065 }
            int r7 = r3.available()     // Catch:{ FileNotFoundException -> 0x0067, IOException -> 0x0065 }
            if (r7 <= 0) goto L_0x0033
            java.lang.String r4 = "clientId file seems corrupted, deleting it."
            r8.e(r4)     // Catch:{ FileNotFoundException -> 0x0067, IOException -> 0x0065 }
            r3.close()     // Catch:{ FileNotFoundException -> 0x0067, IOException -> 0x0065 }
            r9.deleteFile(r0)     // Catch:{ FileNotFoundException -> 0x0067, IOException -> 0x0065 }
            if (r3 == 0) goto L_0x0032
            r3.close()     // Catch:{ IOException -> 0x002e }
            goto L_0x0032
        L_0x002e:
            r9 = move-exception
            r8.e(r1, r9)
        L_0x0032:
            return r2
        L_0x0033:
            r7 = 14
            if (r4 >= r7) goto L_0x004d
            java.lang.String r4 = "clientId file is empty, deleting it."
            r8.e(r4)     // Catch:{ FileNotFoundException -> 0x0067, IOException -> 0x0065 }
            r3.close()     // Catch:{ FileNotFoundException -> 0x0067, IOException -> 0x0065 }
            r9.deleteFile(r0)     // Catch:{ FileNotFoundException -> 0x0067, IOException -> 0x0065 }
            if (r3 == 0) goto L_0x004c
            r3.close()     // Catch:{ IOException -> 0x0048 }
            goto L_0x004c
        L_0x0048:
            r9 = move-exception
            r8.e(r1, r9)
        L_0x004c:
            return r2
        L_0x004d:
            r3.close()     // Catch:{ FileNotFoundException -> 0x0067, IOException -> 0x0065 }
            java.lang.String r7 = new java.lang.String     // Catch:{ FileNotFoundException -> 0x0067, IOException -> 0x0065 }
            r7.<init>(r5, r6, r4)     // Catch:{ FileNotFoundException -> 0x0067, IOException -> 0x0065 }
            java.lang.String r4 = "Read client id from disk"
            r8.a(r4, r7)     // Catch:{ FileNotFoundException -> 0x0067, IOException -> 0x0065 }
            if (r3 == 0) goto L_0x0064
            r3.close()     // Catch:{ IOException -> 0x0060 }
            goto L_0x0064
        L_0x0060:
            r9 = move-exception
            r8.e(r1, r9)
        L_0x0064:
            return r7
        L_0x0065:
            r4 = move-exception
            goto L_0x006e
        L_0x0067:
            goto L_0x008e
        L_0x0069:
            r9 = move-exception
            r3 = r2
            goto L_0x0082
        L_0x006c:
            r4 = move-exception
            r3 = r2
        L_0x006e:
            java.lang.String r5 = "Error reading client id file, deleting it"
            r8.e(r5, r4)     // Catch:{ all -> 0x0081 }
            r9.deleteFile(r0)     // Catch:{ all -> 0x0081 }
            if (r3 == 0) goto L_0x0080
            r3.close()     // Catch:{ IOException -> 0x007c }
            goto L_0x0080
        L_0x007c:
            r9 = move-exception
            r8.e(r1, r9)
        L_0x0080:
            return r2
        L_0x0081:
            r9 = move-exception
        L_0x0082:
            if (r3 == 0) goto L_0x008c
            r3.close()     // Catch:{ IOException -> 0x0088 }
            goto L_0x008c
        L_0x0088:
            r0 = move-exception
            r8.e(r1, r0)
        L_0x008c:
            throw r9
        L_0x008d:
            r3 = r2
        L_0x008e:
            if (r3 == 0) goto L_0x0098
            r3.close()     // Catch:{ IOException -> 0x0094 }
            goto L_0x0098
        L_0x0094:
            r9 = move-exception
            r8.e(r1, r9)
        L_0x0098:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.am.a(android.content.Context):java.lang.String");
    }

    private final boolean a(Context context, String str) {
        l.a(str);
        l.c("ClientId should be saved from worker thread");
        FileOutputStream fileOutputStream = null;
        try {
            a("Storing clientId", str);
            fileOutputStream = context.openFileOutput("gaClientId", 0);
            fileOutputStream.write(str.getBytes());
            if (fileOutputStream == null) {
                return true;
            }
            try {
                fileOutputStream.close();
                return true;
            } catch (IOException e) {
                e("Failed to close clientId writing stream", e);
                return true;
            }
        } catch (FileNotFoundException e2) {
            e("Error creating clientId file", e2);
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e3) {
                    e("Failed to close clientId writing stream", e3);
                }
            }
            return false;
        } catch (IOException e4) {
            e("Error writing to clientId file", e4);
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e5) {
                    e("Failed to close clientId writing stream", e5);
                }
            }
            return false;
        } catch (Throwable th) {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e6) {
                    e("Failed to close clientId writing stream", e6);
                }
            }
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    public final String d() {
        String a2 = a(this.c.b().f1331a);
        return a2 == null ? e() : a2;
    }
}
