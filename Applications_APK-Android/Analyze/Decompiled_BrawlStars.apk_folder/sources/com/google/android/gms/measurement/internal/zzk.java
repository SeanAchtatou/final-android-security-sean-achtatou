package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.a;

public final class zzk extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzk> CREATOR = new eh();

    /* renamed from: a  reason: collision with root package name */
    public final String f2601a;

    /* renamed from: b  reason: collision with root package name */
    public final String f2602b;
    public final String c;
    public final String d;
    public final long e;
    public final long f;
    public final String g;
    public final boolean h;
    public final boolean i;
    public final long j;
    public final String k;
    public final long l;
    public final long m;
    public final int n;
    public final boolean o;
    public final boolean p;
    public final boolean q;
    public final String r;

    zzk(String str, String str2, String str3, long j2, String str4, long j3, long j4, String str5, boolean z, boolean z2, String str6, long j5, long j6, int i2, boolean z3, boolean z4, boolean z5, String str7) {
        l.a(str);
        this.f2601a = str;
        this.f2602b = TextUtils.isEmpty(str2) ? null : str2;
        this.c = str3;
        this.j = j2;
        this.d = str4;
        this.e = j3;
        this.f = j4;
        this.g = str5;
        this.h = z;
        this.i = z2;
        this.k = str6;
        this.l = j5;
        this.m = j6;
        this.n = i2;
        this.o = z3;
        this.p = z4;
        this.q = z5;
        this.r = str7;
    }

    zzk(String str, String str2, String str3, String str4, long j2, long j3, String str5, boolean z, boolean z2, long j4, String str6, long j5, long j6, int i2, boolean z3, boolean z4, boolean z5, String str7) {
        this.f2601a = str;
        this.f2602b = str2;
        this.c = str3;
        this.j = j4;
        this.d = str4;
        this.e = j2;
        this.f = j3;
        this.g = str5;
        this.h = z;
        this.i = z2;
        this.k = str6;
        this.l = j5;
        this.m = j6;
        this.n = i2;
        this.o = z3;
        this.p = z4;
        this.q = z5;
        this.r = str7;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void */
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = a.a(parcel, 20293);
        a.a(parcel, 2, this.f2601a, false);
        a.a(parcel, 3, this.f2602b, false);
        a.a(parcel, 4, this.c, false);
        a.a(parcel, 5, this.d, false);
        a.a(parcel, 6, this.e);
        a.a(parcel, 7, this.f);
        a.a(parcel, 8, this.g, false);
        a.a(parcel, 9, this.h);
        a.a(parcel, 10, this.i);
        a.a(parcel, 11, this.j);
        a.a(parcel, 12, this.k, false);
        a.a(parcel, 13, this.l);
        a.a(parcel, 14, this.m);
        a.b(parcel, 15, this.n);
        a.a(parcel, 16, this.o);
        a.a(parcel, 17, this.p);
        a.a(parcel, 18, this.q);
        a.a(parcel, 19, this.r, false);
        a.b(parcel, a2);
    }
}
