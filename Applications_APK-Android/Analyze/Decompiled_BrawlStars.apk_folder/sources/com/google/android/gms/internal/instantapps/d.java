package com.google.android.gms.internal.instantapps;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class d implements Parcelable.Creator<zzal> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zzal[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        String str = null;
        int i = 0;
        Account[] accountArr = null;
        boolean z = false;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            int i2 = 65535 & readInt;
            if (i2 == 2) {
                i = SafeParcelReader.d(parcel, readInt);
            } else if (i2 == 3) {
                str = SafeParcelReader.l(parcel, readInt);
            } else if (i2 == 4) {
                accountArr = (Account[]) SafeParcelReader.b(parcel, readInt, Account.CREATOR);
            } else if (i2 != 5) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                z = SafeParcelReader.c(parcel, readInt);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new zzal(i, str, accountArr, z);
    }
}
