package com.google.android.gms.common.images;

import android.net.Uri;
import com.google.android.gms.common.internal.j;
import java.util.Arrays;

final class b {

    /* renamed from: a  reason: collision with root package name */
    public final Uri f1553a;

    public b(Uri uri) {
        this.f1553a = uri;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.f1553a});
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof b)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        return j.a(((b) obj).f1553a, this.f1553a);
    }
}
