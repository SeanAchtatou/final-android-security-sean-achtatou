package com.google.android.gms.measurement.internal;

import android.os.RemoteException;

final class cp implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ cg f2476a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ cl f2477b;

    cp(cl clVar, cg cgVar) {
        this.f2477b = clVar;
        this.f2476a = cgVar;
    }

    public final void run() {
        zzaj zzaj = this.f2477b.f2470b;
        if (zzaj == null) {
            this.f2477b.q().c.a("Failed to send current screen to service");
            return;
        }
        try {
            if (this.f2476a == null) {
                zzaj.a(0, (String) null, (String) null, this.f2477b.m().getPackageName());
            } else {
                zzaj.a(this.f2476a.c, this.f2476a.f2461a, this.f2476a.f2462b, this.f2477b.m().getPackageName());
            }
            this.f2477b.y();
        } catch (RemoteException e) {
            this.f2477b.q().c.a("Failed to send current screen to the service", e);
        }
    }
}
