package com.google.android.gms.auth.api.signin;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.NotificationCompat;
import com.google.android.gms.auth.api.signin.internal.GoogleSignInOptionsExtensionParcelable;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GoogleSignInOptions extends AbstractSafeParcelable implements a.d.e, ReflectedParcelable {
    public static final Parcelable.Creator<GoogleSignInOptions> CREATOR = new c();

    /* renamed from: a  reason: collision with root package name */
    public static final Scope f1342a = new Scope("profile");

    /* renamed from: b  reason: collision with root package name */
    public static final Scope f1343b = new Scope(NotificationCompat.CATEGORY_EMAIL);
    public static final Scope c = new Scope("openid");
    public static final Scope d = new Scope("https://www.googleapis.com/auth/games_lite");
    public static final Scope e = new Scope("https://www.googleapis.com/auth/games");
    public static final GoogleSignInOptions f;
    public static final GoogleSignInOptions g;
    private static Comparator<Scope> r = new b();
    private final int h;
    private final ArrayList<Scope> i;
    private Account j;
    private boolean k;
    private final boolean l;
    private final boolean m;
    private String n;
    private String o;
    private ArrayList<GoogleSignInOptionsExtensionParcelable> p;
    private Map<Integer, GoogleSignInOptionsExtensionParcelable> q;

    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        Set<Scope> f1344a = new HashSet();

        /* renamed from: b  reason: collision with root package name */
        private boolean f1345b;
        private boolean c;
        private boolean d;
        private String e;
        private Account f;
        private String g;
        private Map<Integer, GoogleSignInOptionsExtensionParcelable> h = new HashMap();

        public final a a() {
            this.f1344a.add(GoogleSignInOptions.c);
            return this;
        }

        public final GoogleSignInOptions b() {
            if (this.f1344a.contains(GoogleSignInOptions.e) && this.f1344a.contains(GoogleSignInOptions.d)) {
                this.f1344a.remove(GoogleSignInOptions.d);
            }
            if (this.d && (this.f == null || !this.f1344a.isEmpty())) {
                a();
            }
            return new GoogleSignInOptions(new ArrayList(this.f1344a), this.f, this.d, this.f1345b, this.c, this.e, this.g, this.h);
        }
    }

    GoogleSignInOptions(int i2, ArrayList<Scope> arrayList, Account account, boolean z, boolean z2, boolean z3, String str, String str2, ArrayList<GoogleSignInOptionsExtensionParcelable> arrayList2) {
        this(i2, arrayList, account, z, z2, z3, str, str2, a(arrayList2));
    }

    private GoogleSignInOptions(int i2, ArrayList<Scope> arrayList, Account account, boolean z, boolean z2, boolean z3, String str, String str2, Map<Integer, GoogleSignInOptionsExtensionParcelable> map) {
        this.h = i2;
        this.i = arrayList;
        this.j = account;
        this.k = z;
        this.l = z2;
        this.m = z3;
        this.n = str;
        this.o = str2;
        this.p = new ArrayList<>(map.values());
        this.q = map;
    }

    private ArrayList<Scope> a() {
        return new ArrayList<>(this.i);
    }

    private static Map<Integer, GoogleSignInOptionsExtensionParcelable> a(List<GoogleSignInOptionsExtensionParcelable> list) {
        HashMap hashMap = new HashMap();
        if (list == null) {
            return hashMap;
        }
        for (GoogleSignInOptionsExtensionParcelable next : list) {
            hashMap.put(Integer.valueOf(next.f1346a), next);
        }
        return hashMap;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0045, code lost:
        if (r3.j.equals(r4.j) != false) goto L_0x0047;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0060, code lost:
        if (r3.n.equals(r4.n) != false) goto L_0x0062;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r4) {
        /*
            r3 = this;
            r0 = 0
            if (r4 != 0) goto L_0x0004
            return r0
        L_0x0004:
            com.google.android.gms.auth.api.signin.GoogleSignInOptions r4 = (com.google.android.gms.auth.api.signin.GoogleSignInOptions) r4     // Catch:{ ClassCastException -> 0x0076 }
            java.util.ArrayList<com.google.android.gms.auth.api.signin.internal.GoogleSignInOptionsExtensionParcelable> r1 = r3.p     // Catch:{ ClassCastException -> 0x0076 }
            int r1 = r1.size()     // Catch:{ ClassCastException -> 0x0076 }
            if (r1 > 0) goto L_0x0076
            java.util.ArrayList<com.google.android.gms.auth.api.signin.internal.GoogleSignInOptionsExtensionParcelable> r1 = r4.p     // Catch:{ ClassCastException -> 0x0076 }
            int r1 = r1.size()     // Catch:{ ClassCastException -> 0x0076 }
            if (r1 <= 0) goto L_0x0017
            goto L_0x0076
        L_0x0017:
            java.util.ArrayList<com.google.android.gms.common.api.Scope> r1 = r3.i     // Catch:{ ClassCastException -> 0x0076 }
            int r1 = r1.size()     // Catch:{ ClassCastException -> 0x0076 }
            java.util.ArrayList r2 = r4.a()     // Catch:{ ClassCastException -> 0x0076 }
            int r2 = r2.size()     // Catch:{ ClassCastException -> 0x0076 }
            if (r1 != r2) goto L_0x0076
            java.util.ArrayList<com.google.android.gms.common.api.Scope> r1 = r3.i     // Catch:{ ClassCastException -> 0x0076 }
            java.util.ArrayList r2 = r4.a()     // Catch:{ ClassCastException -> 0x0076 }
            boolean r1 = r1.containsAll(r2)     // Catch:{ ClassCastException -> 0x0076 }
            if (r1 != 0) goto L_0x0034
            goto L_0x0076
        L_0x0034:
            android.accounts.Account r1 = r3.j     // Catch:{ ClassCastException -> 0x0076 }
            if (r1 != 0) goto L_0x003d
            android.accounts.Account r1 = r4.j     // Catch:{ ClassCastException -> 0x0076 }
            if (r1 != 0) goto L_0x0076
            goto L_0x0047
        L_0x003d:
            android.accounts.Account r1 = r3.j     // Catch:{ ClassCastException -> 0x0076 }
            android.accounts.Account r2 = r4.j     // Catch:{ ClassCastException -> 0x0076 }
            boolean r1 = r1.equals(r2)     // Catch:{ ClassCastException -> 0x0076 }
            if (r1 == 0) goto L_0x0076
        L_0x0047:
            java.lang.String r1 = r3.n     // Catch:{ ClassCastException -> 0x0076 }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ ClassCastException -> 0x0076 }
            if (r1 == 0) goto L_0x0058
            java.lang.String r1 = r4.n     // Catch:{ ClassCastException -> 0x0076 }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ ClassCastException -> 0x0076 }
            if (r1 == 0) goto L_0x0076
            goto L_0x0062
        L_0x0058:
            java.lang.String r1 = r3.n     // Catch:{ ClassCastException -> 0x0076 }
            java.lang.String r2 = r4.n     // Catch:{ ClassCastException -> 0x0076 }
            boolean r1 = r1.equals(r2)     // Catch:{ ClassCastException -> 0x0076 }
            if (r1 == 0) goto L_0x0076
        L_0x0062:
            boolean r1 = r3.m     // Catch:{ ClassCastException -> 0x0076 }
            boolean r2 = r4.m     // Catch:{ ClassCastException -> 0x0076 }
            if (r1 != r2) goto L_0x0076
            boolean r1 = r3.k     // Catch:{ ClassCastException -> 0x0076 }
            boolean r2 = r4.k     // Catch:{ ClassCastException -> 0x0076 }
            if (r1 != r2) goto L_0x0076
            boolean r1 = r3.l     // Catch:{ ClassCastException -> 0x0076 }
            boolean r4 = r4.l     // Catch:{ ClassCastException -> 0x0076 }
            if (r1 != r4) goto L_0x0076
            r4 = 1
            return r4
        L_0x0076:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.auth.api.signin.GoogleSignInOptions.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = this.i;
        int size = arrayList2.size();
        int i2 = 0;
        while (i2 < size) {
            Object obj = arrayList2.get(i2);
            i2++;
            arrayList.add(((Scope) obj).f1366a);
        }
        Collections.sort(arrayList);
        return new com.google.android.gms.auth.api.signin.internal.a().a(arrayList).a(this.j).a(this.n).a(this.m).a(this.k).a(this.l).f1349a;
    }

    /* synthetic */ GoogleSignInOptions(ArrayList arrayList, Account account, boolean z, boolean z2, boolean z3, String str, String str2, Map map) {
        this(3, arrayList, account, z, z2, z3, str, str2, map);
    }

    static {
        a a2 = new a().a();
        a2.f1344a.add(f1342a);
        f = a2.b();
        a aVar = new a();
        aVar.f1344a.add(d);
        aVar.f1344a.addAll(Arrays.asList(new Scope[0]));
        g = aVar.b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.accounts.Account, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void */
    public void writeToParcel(Parcel parcel, int i2) {
        int a2 = com.google.android.gms.common.internal.safeparcel.a.a(parcel, 20293);
        com.google.android.gms.common.internal.safeparcel.a.b(parcel, 1, this.h);
        com.google.android.gms.common.internal.safeparcel.a.b(parcel, 2, a(), false);
        com.google.android.gms.common.internal.safeparcel.a.a(parcel, 3, (Parcelable) this.j, i2, false);
        com.google.android.gms.common.internal.safeparcel.a.a(parcel, 4, this.k);
        com.google.android.gms.common.internal.safeparcel.a.a(parcel, 5, this.l);
        com.google.android.gms.common.internal.safeparcel.a.a(parcel, 6, this.m);
        com.google.android.gms.common.internal.safeparcel.a.a(parcel, 7, this.n, false);
        com.google.android.gms.common.internal.safeparcel.a.a(parcel, 8, this.o, false);
        com.google.android.gms.common.internal.safeparcel.a.b(parcel, 9, this.p, false);
        com.google.android.gms.common.internal.safeparcel.a.b(parcel, a2);
    }
}
