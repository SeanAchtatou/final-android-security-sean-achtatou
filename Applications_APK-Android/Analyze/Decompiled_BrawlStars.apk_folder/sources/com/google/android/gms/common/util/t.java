package com.google.android.gms.common.util;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.PowerManager;
import android.os.SystemClock;

public final class t {

    /* renamed from: a  reason: collision with root package name */
    private static final IntentFilter f1689a = new IntentFilter("android.intent.action.BATTERY_CHANGED");

    /* renamed from: b  reason: collision with root package name */
    private static long f1690b;
    private static float c = Float.NaN;

    public static int a(Context context) {
        int i;
        boolean z;
        if (context == null || context.getApplicationContext() == null) {
            return -1;
        }
        Intent registerReceiver = context.getApplicationContext().registerReceiver(null, f1689a);
        int i2 = 0;
        if (registerReceiver == null) {
            i = 0;
        } else {
            i = registerReceiver.getIntExtra("plugged", 0);
        }
        if ((i & 7) != 0) {
            i2 = 1;
        }
        PowerManager powerManager = (PowerManager) context.getSystemService("power");
        if (powerManager == null) {
            return -1;
        }
        if (o.d()) {
            z = powerManager.isInteractive();
        } else {
            z = powerManager.isScreenOn();
        }
        return ((z ? 1 : 0) << true) | i2;
    }

    public static synchronized float b(Context context) {
        synchronized (t.class) {
            if (SystemClock.elapsedRealtime() - f1690b >= 60000 || Float.isNaN(c)) {
                Intent registerReceiver = context.getApplicationContext().registerReceiver(null, f1689a);
                if (registerReceiver != null) {
                    c = ((float) registerReceiver.getIntExtra("level", -1)) / ((float) registerReceiver.getIntExtra("scale", -1));
                }
                f1690b = SystemClock.elapsedRealtime();
                float f = c;
                return f;
            }
            float f2 = c;
            return f2;
        }
    }
}
