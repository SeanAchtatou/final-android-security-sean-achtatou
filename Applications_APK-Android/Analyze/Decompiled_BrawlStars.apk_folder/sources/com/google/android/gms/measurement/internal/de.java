package com.google.android.gms.measurement.internal;

import android.content.Intent;

public final /* synthetic */ class de implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final dd f2504a;

    /* renamed from: b  reason: collision with root package name */
    private final int f2505b;
    private final o c;
    private final Intent d;

    public de(dd ddVar, int i, o oVar, Intent intent) {
        this.f2504a = ddVar;
        this.f2505b = i;
        this.c = oVar;
        this.d = intent;
    }

    public final void run() {
        dd ddVar = this.f2504a;
        int i = this.f2505b;
        o oVar = this.c;
        Intent intent = this.d;
        if (((di) ddVar.f2503a).a(i)) {
            oVar.k.a("Local AppMeasurementService processed last upload request. StartId", Integer.valueOf(i));
            ddVar.c().k.a("Completed wakeful intent.");
            ((di) ddVar.f2503a).a(intent);
        }
    }
}
