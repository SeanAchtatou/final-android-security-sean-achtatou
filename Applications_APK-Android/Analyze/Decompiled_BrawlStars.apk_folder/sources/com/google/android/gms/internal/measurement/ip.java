package com.google.android.gms.internal.measurement;

public enum ip {
    DOUBLE(iu.DOUBLE, 1),
    FLOAT(iu.FLOAT, 5),
    INT64(iu.LONG, 0),
    UINT64(iu.LONG, 0),
    INT32(iu.INT, 0),
    FIXED64(iu.LONG, 1),
    FIXED32(iu.INT, 5),
    BOOL(iu.BOOLEAN, 0),
    STRING,
    GROUP,
    MESSAGE,
    BYTES,
    UINT32(iu.INT, 0),
    ENUM(iu.ENUM, 0),
    SFIXED32(iu.INT, 5),
    SFIXED64(iu.LONG, 1),
    SINT32(iu.INT, 0),
    SINT64(iu.LONG, 0);
    
    final iu s;
    final int t;

    private ip(iu iuVar, int i) {
        this.s = iuVar;
        this.t = i;
    }
}
