package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.fq;
import java.io.IOException;

final class id extends ib<ic, ic> {
    id() {
    }

    private static void a(Object obj, ic icVar) {
        ((fq) obj).zzbyd = icVar;
    }

    /* access modifiers changed from: package-private */
    public final void d(Object obj) {
        ((fq) obj).zzbyd.e = false;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ int f(Object obj) {
        return ((ic) obj).c();
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ int e(Object obj) {
        ic icVar = (ic) obj;
        int i = icVar.d;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < icVar.f2282a; i3++) {
            i2 += zztv.d(icVar.f2283b[i3] >>> 3, (ej) icVar.c[i3]);
        }
        icVar.d = i2;
        return i2;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Object c(Object obj, Object obj2) {
        ic icVar = (ic) obj;
        ic icVar2 = (ic) obj2;
        if (icVar2.equals(ic.a())) {
            return icVar;
        }
        return ic.a(icVar, icVar2);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void b(Object obj, iv ivVar) throws IOException {
        ic icVar = (ic) obj;
        if (ivVar.a() == fq.e.k) {
            for (int i = icVar.f2282a - 1; i >= 0; i--) {
                ivVar.a(icVar.f2283b[i] >>> 3, icVar.c[i]);
            }
            return;
        }
        for (int i2 = 0; i2 < icVar.f2282a; i2++) {
            ivVar.a(icVar.f2283b[i2] >>> 3, icVar.c[i2]);
        }
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void a(Object obj, iv ivVar) throws IOException {
        ((ic) obj).a(ivVar);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void b(Object obj, Object obj2) {
        a(obj, (ic) obj2);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Object c(Object obj) {
        ic icVar = ((fq) obj).zzbyd;
        if (icVar != ic.a()) {
            return icVar;
        }
        ic b2 = ic.b();
        a(obj, b2);
        return b2;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Object b(Object obj) {
        return ((fq) obj).zzbyd;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void a(Object obj, Object obj2) {
        a(obj, (ic) obj2);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Object a(Object obj) {
        ic icVar = (ic) obj;
        icVar.e = false;
        return icVar;
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Object a() {
        return ic.b();
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void a(Object obj, int i, Object obj2) {
        ((ic) obj).a((i << 3) | 3, (ic) obj2);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void a(Object obj, int i, ej ejVar) {
        ((ic) obj).a((i << 3) | 2, ejVar);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void b(Object obj, int i, long j) {
        ((ic) obj).a((i << 3) | 1, Long.valueOf(j));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void a(Object obj, int i, int i2) {
        ((ic) obj).a((i << 3) | 5, Integer.valueOf(i2));
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ void a(Object obj, int i, long j) {
        ((ic) obj).a(i << 3, Long.valueOf(j));
    }
}
