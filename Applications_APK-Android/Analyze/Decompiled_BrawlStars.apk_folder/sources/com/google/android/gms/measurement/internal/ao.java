package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.l;
import java.lang.Thread;

final class ao implements Thread.UncaughtExceptionHandler {

    /* renamed from: a  reason: collision with root package name */
    private final String f2381a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ am f2382b;

    public ao(am amVar, String str) {
        this.f2382b = amVar;
        l.a((Object) str);
        this.f2381a = str;
    }

    public final synchronized void uncaughtException(Thread thread, Throwable th) {
        this.f2382b.q().c.a(this.f2381a, th);
    }
}
