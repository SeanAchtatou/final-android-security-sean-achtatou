package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class ac implements Parcelable.Creator<zzfu> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zzfu[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        zzgi zzgi = null;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            if ((65535 & readInt) != 2) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                zzgi = (zzgi) SafeParcelReader.a(parcel, readInt, zzgi.CREATOR);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new zzfu(zzgi);
    }
}
