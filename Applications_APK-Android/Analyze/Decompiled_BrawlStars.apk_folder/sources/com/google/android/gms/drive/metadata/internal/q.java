package com.google.android.gms.drive.metadata.internal;

import android.os.Bundle;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.metadata.f;

public class q extends f<String> {
    public q(String str, int i) {
        super(str, i);
    }

    public final /* synthetic */ Object c(DataHolder dataHolder, int i, int i2) {
        return dataHolder.c(this.f1731b, i, i2);
    }

    public final /* synthetic */ void a(Bundle bundle, Object obj) {
        bundle.putString(this.f1731b, (String) obj);
    }

    public final /* synthetic */ Object b(Bundle bundle) {
        return bundle.getString(this.f1731b);
    }
}
