package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.d;
import com.google.android.gms.common.api.g;
import com.google.android.gms.games.a;
import com.google.android.gms.games.achievement.a;

abstract class w extends a.c<a.C0119a> {
    private final String d;

    public w(String str, d dVar) {
        super(dVar);
        this.d = str;
    }

    public final /* synthetic */ g c(Status status) {
        return new x(this, status);
    }
}
