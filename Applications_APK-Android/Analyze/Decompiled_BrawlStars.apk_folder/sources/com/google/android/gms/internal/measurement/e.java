package com.google.android.gms.internal.measurement;

import android.text.TextUtils;
import com.facebook.internal.NativeProtocol;
import com.google.android.gms.analytics.l;
import java.util.HashMap;

public final class e extends l<e> {

    /* renamed from: a  reason: collision with root package name */
    public String f2176a;

    /* renamed from: b  reason: collision with root package name */
    public String f2177b;
    public String c;
    public long d;

    public final String toString() {
        HashMap hashMap = new HashMap();
        hashMap.put("category", this.f2176a);
        hashMap.put(NativeProtocol.WEB_DIALOG_ACTION, this.f2177b);
        hashMap.put("label", this.c);
        hashMap.put("value", Long.valueOf(this.d));
        return a((Object) hashMap);
    }

    public final /* synthetic */ void a(l lVar) {
        e eVar = (e) lVar;
        if (!TextUtils.isEmpty(this.f2176a)) {
            eVar.f2176a = this.f2176a;
        }
        if (!TextUtils.isEmpty(this.f2177b)) {
            eVar.f2177b = this.f2177b;
        }
        if (!TextUtils.isEmpty(this.c)) {
            eVar.c = this.c;
        }
        long j = this.d;
        if (j != 0) {
            eVar.d = j;
        }
    }
}
