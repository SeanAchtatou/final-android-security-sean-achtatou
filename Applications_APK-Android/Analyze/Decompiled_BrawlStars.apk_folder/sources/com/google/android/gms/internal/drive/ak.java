package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.drive.query.Query;

public final class ak implements Parcelable.Creator<zzgk> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zzgk[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        Query query = null;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            if ((65535 & readInt) != 2) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                query = (Query) SafeParcelReader.a(parcel, readInt, Query.CREATOR);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new zzgk(query);
    }
}
