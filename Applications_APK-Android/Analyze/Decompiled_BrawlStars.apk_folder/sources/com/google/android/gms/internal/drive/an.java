package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.drive.DriveId;
import java.util.ArrayList;

public final class an implements Parcelable.Creator<zzgq> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zzgq[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        DriveId driveId = null;
        ArrayList arrayList = null;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            int i = 65535 & readInt;
            if (i == 2) {
                driveId = (DriveId) SafeParcelReader.a(parcel, readInt, DriveId.CREATOR);
            } else if (i != 3) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                arrayList = SafeParcelReader.c(parcel, readInt, DriveId.CREATOR);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new zzgq(driveId, arrayList);
    }
}
