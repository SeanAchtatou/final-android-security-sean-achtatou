package com.google.android.gms.internal.drive;

import java.nio.charset.Charset;

public final class bw {

    /* renamed from: a  reason: collision with root package name */
    protected static final Charset f1907a = Charset.forName("UTF-8");

    /* renamed from: b  reason: collision with root package name */
    private static final Charset f1908b = Charset.forName("ISO-8859-1");
    private static final Object c = new Object();

    public static void a(bs bsVar, bs bsVar2) {
        if (bsVar.f != null) {
            bsVar2.f = (bu) bsVar.f.clone();
        }
    }
}
