package com.google.android.gms.internal.measurement;

import java.io.IOException;

public abstract class je {
    protected volatile int M = -1;

    public abstract je a(iw iwVar) throws IOException;

    public void a(iy iyVar) throws IOException {
    }

    /* access modifiers changed from: protected */
    public int b() {
        return 0;
    }

    public final int d() {
        if (this.M < 0) {
            e();
        }
        return this.M;
    }

    public final int e() {
        int b2 = b();
        this.M = b2;
        return b2;
    }

    public String toString() {
        return jf.a(this);
    }

    /* renamed from: c */
    public je clone() throws CloneNotSupportedException {
        return (je) super.clone();
    }
}
