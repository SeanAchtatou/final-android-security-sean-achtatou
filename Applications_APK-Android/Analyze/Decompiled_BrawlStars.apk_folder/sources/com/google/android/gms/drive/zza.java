package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.internal.drive.as;
import com.google.android.gms.internal.drive.bx;

public class zza extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zza> CREATOR = new f();

    /* renamed from: a  reason: collision with root package name */
    private final long f1767a;

    /* renamed from: b  reason: collision with root package name */
    private final long f1768b;
    private final long c;
    private volatile String d = null;

    public zza(long j, long j2, long j3) {
        boolean z = true;
        l.b(j != -1);
        l.b(j2 != -1);
        l.b(j3 == -1 ? false : z);
        this.f1767a = j;
        this.f1768b = j2;
        this.c = j3;
    }

    public boolean equals(Object obj) {
        if (obj != null && obj.getClass() == zza.class) {
            zza zza = (zza) obj;
            return zza.f1768b == this.f1768b && zza.c == this.c && zza.f1767a == this.f1767a;
        }
    }

    public int hashCode() {
        String valueOf = String.valueOf(this.f1767a);
        String valueOf2 = String.valueOf(this.f1768b);
        String valueOf3 = String.valueOf(this.c);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + String.valueOf(valueOf2).length() + String.valueOf(valueOf3).length());
        sb.append(valueOf);
        sb.append(valueOf2);
        sb.append(valueOf3);
        return sb.toString().hashCode();
    }

    public String toString() {
        if (this.d == null) {
            as asVar = new as();
            asVar.f1889a = 1;
            asVar.f1890b = this.f1767a;
            asVar.c = this.f1768b;
            asVar.d = this.c;
            String valueOf = String.valueOf(Base64.encodeToString(bx.a(asVar), 10));
            this.d = valueOf.length() != 0 ? "ChangeSequenceNumber:".concat(valueOf) : new String("ChangeSequenceNumber:");
        }
        return this.d;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.a(parcel, 2, this.f1767a);
        a.a(parcel, 3, this.f1768b);
        a.a(parcel, 4, this.c);
        a.b(parcel, a2);
    }
}
