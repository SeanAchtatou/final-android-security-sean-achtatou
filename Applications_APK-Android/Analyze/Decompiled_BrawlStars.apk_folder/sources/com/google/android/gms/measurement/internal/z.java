package com.google.android.gms.measurement.internal;

import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;
import android.util.Pair;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Locale;

final class z extends bn {

    /* renamed from: a  reason: collision with root package name */
    static final Pair<String, Long> f2592a = new Pair<>("", 0L);

    /* renamed from: b  reason: collision with root package name */
    SharedPreferences f2593b;
    public ac c;
    public final ab d = new ab(this, "last_upload", 0);
    public final ab e = new ab(this, "last_upload_attempt", 0);
    public final ab f = new ab(this, "backoff", 0);
    public final ab g = new ab(this, "last_delete_stale", 0);
    public final ab h = new ab(this, "midnight_offset", 0);
    public final ab i = new ab(this, "first_open_time", 0);
    public final ab j = new ab(this, "app_install_time", 0);
    public final ad k = new ad(this, "app_instance_id");
    public final ab l = new ab(this, "time_before_start", 10000);
    public final ab m = new ab(this, "session_timeout", 1800000);
    public final aa n = new aa(this, "start_new_session");
    public final ab o = new ab(this, "last_pause_time", 0);
    public final ab p = new ab(this, "time_active", 0);
    public boolean q;
    private String s;
    private boolean t;
    private long u;

    /* access modifiers changed from: package-private */
    public final Pair<String, Boolean> a(String str) {
        c();
        long b2 = l().b();
        String str2 = this.s;
        if (str2 != null && b2 < this.u) {
            return new Pair<>(str2, Boolean.valueOf(this.t));
        }
        this.u = b2 + s().a(str, h.j);
        AdvertisingIdClient.a();
        try {
            AdvertisingIdClient.Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(m());
            if (advertisingIdInfo != null) {
                this.s = advertisingIdInfo.getId();
                this.t = advertisingIdInfo.isLimitAdTrackingEnabled();
            }
            if (this.s == null) {
                this.s = "";
            }
        } catch (Exception e2) {
            q().j.a("Unable to get advertising id", e2);
            this.s = "";
        }
        AdvertisingIdClient.a();
        return new Pair<>(this.s, Boolean.valueOf(this.t));
    }

    /* access modifiers changed from: protected */
    public final boolean d() {
        return true;
    }

    /* access modifiers changed from: package-private */
    public final String b(String str) {
        c();
        String str2 = (String) a(str).first;
        MessageDigest h2 = ed.h();
        if (h2 == null) {
            return null;
        }
        return String.format(Locale.US, "%032X", new BigInteger(1, h2.digest(str2.getBytes())));
    }

    z(ar arVar) {
        super(arVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* access modifiers changed from: protected */
    public final void e() {
        this.f2593b = m().getSharedPreferences("com.google.android.gms.measurement.prefs", 0);
        this.q = this.f2593b.getBoolean("has_been_opened", false);
        if (!this.q) {
            SharedPreferences.Editor edit = this.f2593b.edit();
            edit.putBoolean("has_been_opened", true);
            edit.apply();
        }
        this.c = new ac(this, "health_monitor", Math.max(0L, h.k.a().longValue()), (byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final SharedPreferences f() {
        c();
        w();
        return this.f2593b;
    }

    /* access modifiers changed from: package-private */
    public final void c(String str) {
        c();
        SharedPreferences.Editor edit = f().edit();
        edit.putString("gmp_app_id", str);
        edit.apply();
    }

    /* access modifiers changed from: package-private */
    public final String g() {
        c();
        return f().getString("gmp_app_id", null);
    }

    /* access modifiers changed from: package-private */
    public final void d(String str) {
        c();
        SharedPreferences.Editor edit = f().edit();
        edit.putString("admob_app_id", str);
        edit.apply();
    }

    /* access modifiers changed from: package-private */
    public final String h() {
        c();
        return f().getString("admob_app_id", null);
    }

    /* access modifiers changed from: package-private */
    public final Boolean i() {
        c();
        if (!f().contains("use_service")) {
            return null;
        }
        return Boolean.valueOf(f().getBoolean("use_service", false));
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z) {
        c();
        q().k.a("Setting useService", Boolean.valueOf(z));
        SharedPreferences.Editor edit = f().edit();
        edit.putBoolean("use_service", z);
        edit.apply();
    }

    /* access modifiers changed from: package-private */
    public final void j() {
        c();
        q().k.a("Clearing collection preferences.");
        if (s().a(h.aq)) {
            Boolean t2 = t();
            SharedPreferences.Editor edit = f().edit();
            edit.clear();
            edit.apply();
            if (t2 != null) {
                d(t2.booleanValue());
                return;
            }
            return;
        }
        boolean contains = f().contains("measurement_enabled");
        boolean z = true;
        if (contains) {
            z = b(true);
        }
        SharedPreferences.Editor edit2 = f().edit();
        edit2.clear();
        edit2.apply();
        if (contains) {
            d(z);
        }
    }

    private void d(boolean z) {
        c();
        q().k.a("Setting measurementEnabled", Boolean.valueOf(z));
        SharedPreferences.Editor edit = f().edit();
        edit.putBoolean("measurement_enabled", z);
        edit.apply();
    }

    /* access modifiers changed from: package-private */
    public final boolean b(boolean z) {
        c();
        return f().getBoolean("measurement_enabled", z);
    }

    /* access modifiers changed from: package-private */
    public final Boolean t() {
        c();
        if (f().contains("measurement_enabled")) {
            return Boolean.valueOf(f().getBoolean("measurement_enabled", true));
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final String u() {
        c();
        String string = f().getString("previous_os_version", null);
        k().w();
        String str = Build.VERSION.RELEASE;
        if (!TextUtils.isEmpty(str) && !str.equals(string)) {
            SharedPreferences.Editor edit = f().edit();
            edit.putString("previous_os_version", str);
            edit.apply();
        }
        return string;
    }

    /* access modifiers changed from: package-private */
    public final void c(boolean z) {
        c();
        q().k.a("Updating deferred analytics collection", Boolean.valueOf(z));
        SharedPreferences.Editor edit = f().edit();
        edit.putBoolean("deferred_analytics_collection", z);
        edit.apply();
    }

    /* access modifiers changed from: package-private */
    public final boolean a(long j2) {
        return j2 - this.m.a() > this.o.a();
    }
}
