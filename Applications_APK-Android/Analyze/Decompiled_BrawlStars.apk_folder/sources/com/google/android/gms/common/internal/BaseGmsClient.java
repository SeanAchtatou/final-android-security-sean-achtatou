package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.IGmsCallbacks;
import com.google.android.gms.common.internal.IGmsServiceBroker;
import com.google.android.gms.common.internal.g;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class BaseGmsClient<T extends IInterface> {
    public static final String[] g = {"service_esmobile", "service_googleme"};
    private static final Feature[] h = new Feature[0];
    /* access modifiers changed from: private */
    public ConnectionResult A;
    /* access modifiers changed from: private */
    public boolean B;
    /* access modifiers changed from: private */
    public volatile zzb C;

    /* renamed from: a  reason: collision with root package name */
    int f1556a;

    /* renamed from: b  reason: collision with root package name */
    long f1557b;
    public final Context c;
    final Handler d;
    protected c e;
    protected AtomicInteger f;
    private long i;
    private int j;
    private long k;
    private af l;
    private final Looper m;
    private final g n;
    private final com.google.android.gms.common.d o;
    private final Object p;
    /* access modifiers changed from: private */
    public final Object q;
    /* access modifiers changed from: private */
    public IGmsServiceBroker r;
    private T s;
    /* access modifiers changed from: private */
    public final ArrayList<h<?>> t;
    private i u;
    private int v;
    /* access modifiers changed from: private */
    public final a w;
    /* access modifiers changed from: private */
    public final b x;
    private final int y;
    private final String z;

    public interface a {
        void a(int i);

        void a(Bundle bundle);
    }

    public interface b {
        void a(ConnectionResult connectionResult);
    }

    public interface c {
        void a(ConnectionResult connectionResult);
    }

    public class d implements c {
        public d() {
        }

        public final void a(ConnectionResult connectionResult) {
            if (connectionResult.b()) {
                BaseGmsClient baseGmsClient = BaseGmsClient.this;
                baseGmsClient.a((IAccountAccessor) null, baseGmsClient.q());
            } else if (BaseGmsClient.this.x != null) {
                BaseGmsClient.this.x.a(connectionResult);
            }
        }
    }

    public interface e {
        void a();
    }

    /* access modifiers changed from: protected */
    public abstract T a(IBinder iBinder);

    public Bundle a_() {
        return null;
    }

    public boolean d() {
        return false;
    }

    /* access modifiers changed from: protected */
    public abstract String i();

    /* access modifiers changed from: protected */
    public abstract String j();

    public Account l() {
        return null;
    }

    protected BaseGmsClient(Context context, Looper looper, int i2, a aVar, b bVar, String str) {
        this(context, looper, g.a(context), com.google.android.gms.common.d.b(), 93, (a) l.a(aVar), (b) l.a(bVar), null);
    }

    final class g extends com.google.android.gms.internal.common.c {
        public g(Looper looper) {
            super(looper);
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v24, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v7, resolved type: android.app.PendingIntent} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void handleMessage(android.os.Message r7) {
            /*
                r6 = this;
                com.google.android.gms.common.internal.BaseGmsClient r0 = com.google.android.gms.common.internal.BaseGmsClient.this
                java.util.concurrent.atomic.AtomicInteger r0 = r0.f
                int r0 = r0.get()
                int r1 = r7.arg1
                if (r0 == r1) goto L_0x0016
                boolean r0 = b(r7)
                if (r0 == 0) goto L_0x0015
                a(r7)
            L_0x0015:
                return
            L_0x0016:
                int r0 = r7.what
                r1 = 4
                r2 = 1
                r3 = 5
                if (r0 == r2) goto L_0x002a
                int r0 = r7.what
                r4 = 7
                if (r0 == r4) goto L_0x002a
                int r0 = r7.what
                if (r0 == r1) goto L_0x002a
                int r0 = r7.what
                if (r0 != r3) goto L_0x0036
            L_0x002a:
                com.google.android.gms.common.internal.BaseGmsClient r0 = com.google.android.gms.common.internal.BaseGmsClient.this
                boolean r0 = r0.c()
                if (r0 != 0) goto L_0x0036
                a(r7)
                return
            L_0x0036:
                int r0 = r7.what
                r4 = 8
                r5 = 3
                if (r0 != r1) goto L_0x0080
                com.google.android.gms.common.internal.BaseGmsClient r0 = com.google.android.gms.common.internal.BaseGmsClient.this
                com.google.android.gms.common.ConnectionResult r1 = new com.google.android.gms.common.ConnectionResult
                int r7 = r7.arg2
                r1.<init>(r7)
                com.google.android.gms.common.ConnectionResult unused = r0.A = r1
                com.google.android.gms.common.internal.BaseGmsClient r7 = com.google.android.gms.common.internal.BaseGmsClient.this
                boolean r7 = r7.t()
                if (r7 == 0) goto L_0x005f
                com.google.android.gms.common.internal.BaseGmsClient r7 = com.google.android.gms.common.internal.BaseGmsClient.this
                boolean r7 = r7.B
                if (r7 != 0) goto L_0x005f
                com.google.android.gms.common.internal.BaseGmsClient r7 = com.google.android.gms.common.internal.BaseGmsClient.this
                r7.a(r5, (android.os.IInterface) null)
                return
            L_0x005f:
                com.google.android.gms.common.internal.BaseGmsClient r7 = com.google.android.gms.common.internal.BaseGmsClient.this
                com.google.android.gms.common.ConnectionResult r7 = r7.A
                if (r7 == 0) goto L_0x006e
                com.google.android.gms.common.internal.BaseGmsClient r7 = com.google.android.gms.common.internal.BaseGmsClient.this
                com.google.android.gms.common.ConnectionResult r7 = r7.A
                goto L_0x0073
            L_0x006e:
                com.google.android.gms.common.ConnectionResult r7 = new com.google.android.gms.common.ConnectionResult
                r7.<init>(r4)
            L_0x0073:
                com.google.android.gms.common.internal.BaseGmsClient r0 = com.google.android.gms.common.internal.BaseGmsClient.this
                com.google.android.gms.common.internal.BaseGmsClient$c r0 = r0.e
                r0.a(r7)
                com.google.android.gms.common.internal.BaseGmsClient r0 = com.google.android.gms.common.internal.BaseGmsClient.this
                r0.a(r7)
                return
            L_0x0080:
                int r0 = r7.what
                if (r0 != r3) goto L_0x00a5
                com.google.android.gms.common.internal.BaseGmsClient r7 = com.google.android.gms.common.internal.BaseGmsClient.this
                com.google.android.gms.common.ConnectionResult r7 = r7.A
                if (r7 == 0) goto L_0x0093
                com.google.android.gms.common.internal.BaseGmsClient r7 = com.google.android.gms.common.internal.BaseGmsClient.this
                com.google.android.gms.common.ConnectionResult r7 = r7.A
                goto L_0x0098
            L_0x0093:
                com.google.android.gms.common.ConnectionResult r7 = new com.google.android.gms.common.ConnectionResult
                r7.<init>(r4)
            L_0x0098:
                com.google.android.gms.common.internal.BaseGmsClient r0 = com.google.android.gms.common.internal.BaseGmsClient.this
                com.google.android.gms.common.internal.BaseGmsClient$c r0 = r0.e
                r0.a(r7)
                com.google.android.gms.common.internal.BaseGmsClient r0 = com.google.android.gms.common.internal.BaseGmsClient.this
                r0.a(r7)
                return
            L_0x00a5:
                int r0 = r7.what
                r1 = 0
                if (r0 != r5) goto L_0x00c9
                java.lang.Object r0 = r7.obj
                boolean r0 = r0 instanceof android.app.PendingIntent
                if (r0 == 0) goto L_0x00b5
                java.lang.Object r0 = r7.obj
                r1 = r0
                android.app.PendingIntent r1 = (android.app.PendingIntent) r1
            L_0x00b5:
                com.google.android.gms.common.ConnectionResult r0 = new com.google.android.gms.common.ConnectionResult
                int r7 = r7.arg2
                r0.<init>(r7, r1)
                com.google.android.gms.common.internal.BaseGmsClient r7 = com.google.android.gms.common.internal.BaseGmsClient.this
                com.google.android.gms.common.internal.BaseGmsClient$c r7 = r7.e
                r7.a(r0)
                com.google.android.gms.common.internal.BaseGmsClient r7 = com.google.android.gms.common.internal.BaseGmsClient.this
                r7.a(r0)
                return
            L_0x00c9:
                int r0 = r7.what
                r4 = 6
                if (r0 != r4) goto L_0x00f8
                com.google.android.gms.common.internal.BaseGmsClient r0 = com.google.android.gms.common.internal.BaseGmsClient.this
                r0.a(r3, (android.os.IInterface) null)
                com.google.android.gms.common.internal.BaseGmsClient r0 = com.google.android.gms.common.internal.BaseGmsClient.this
                com.google.android.gms.common.internal.BaseGmsClient$a r0 = r0.w
                if (r0 == 0) goto L_0x00e6
                com.google.android.gms.common.internal.BaseGmsClient r0 = com.google.android.gms.common.internal.BaseGmsClient.this
                com.google.android.gms.common.internal.BaseGmsClient$a r0 = r0.w
                int r4 = r7.arg2
                r0.a(r4)
            L_0x00e6:
                com.google.android.gms.common.internal.BaseGmsClient r0 = com.google.android.gms.common.internal.BaseGmsClient.this
                int r7 = r7.arg2
                r0.f1556a = r7
                long r4 = java.lang.System.currentTimeMillis()
                r0.f1557b = r4
                com.google.android.gms.common.internal.BaseGmsClient r7 = com.google.android.gms.common.internal.BaseGmsClient.this
                boolean unused = r7.a(r3, r2, r1)
                return
            L_0x00f8:
                int r0 = r7.what
                r1 = 2
                if (r0 != r1) goto L_0x0109
                com.google.android.gms.common.internal.BaseGmsClient r0 = com.google.android.gms.common.internal.BaseGmsClient.this
                boolean r0 = r0.b()
                if (r0 != 0) goto L_0x0109
                a(r7)
                return
            L_0x0109:
                boolean r0 = b(r7)
                if (r0 == 0) goto L_0x0117
                java.lang.Object r7 = r7.obj
                com.google.android.gms.common.internal.BaseGmsClient$h r7 = (com.google.android.gms.common.internal.BaseGmsClient.h) r7
                r7.b()
                return
            L_0x0117:
                int r7 = r7.what
                r0 = 45
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>(r0)
                java.lang.String r0 = "Don't know how to handle message: "
                r1.append(r0)
                r1.append(r7)
                java.lang.String r7 = r1.toString()
                java.lang.Exception r0 = new java.lang.Exception
                r0.<init>()
                java.lang.String r1 = "GmsClient"
                android.util.Log.wtf(r1, r7, r0)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.internal.BaseGmsClient.g.handleMessage(android.os.Message):void");
        }

        private static void a(Message message) {
            ((h) message.obj).c();
        }

        private static boolean b(Message message) {
            return message.what == 2 || message.what == 1 || message.what == 7;
        }
    }

    protected final class k extends f {
        public k(int i) {
            super(i, null);
        }

        /* access modifiers changed from: protected */
        public final void a(ConnectionResult connectionResult) {
            BaseGmsClient.this.e.a(connectionResult);
            BaseGmsClient.this.a(connectionResult);
        }

        /* access modifiers changed from: protected */
        public final boolean a() {
            BaseGmsClient.this.e.a(ConnectionResult.f1352a);
            return true;
        }
    }

    protected abstract class h<TListener> {

        /* renamed from: a  reason: collision with root package name */
        private TListener f1562a;

        /* renamed from: b  reason: collision with root package name */
        private boolean f1563b = false;

        public h(TListener tlistener) {
            this.f1562a = tlistener;
        }

        /* access modifiers changed from: protected */
        public abstract void a(TListener tlistener);

        public final void b() {
            TListener tlistener;
            synchronized (this) {
                tlistener = this.f1562a;
                if (this.f1563b) {
                    String valueOf = String.valueOf(this);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 47);
                    sb.append("Callback proxy ");
                    sb.append(valueOf);
                    sb.append(" being reused. This is not safe.");
                    sb.toString();
                }
            }
            if (tlistener != null) {
                try {
                    a(tlistener);
                } catch (RuntimeException e) {
                    throw e;
                }
            }
            synchronized (this) {
                this.f1563b = true;
            }
            c();
        }

        public final void c() {
            d();
            synchronized (BaseGmsClient.this.t) {
                BaseGmsClient.this.t.remove(this);
            }
        }

        public final void d() {
            synchronized (this) {
                this.f1562a = null;
            }
        }
    }

    public final class i implements ServiceConnection {

        /* renamed from: a  reason: collision with root package name */
        private final int f1564a;

        public i(int i) {
            this.f1564a = i;
        }

        public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            IGmsServiceBroker iGmsServiceBroker;
            if (iBinder == null) {
                BaseGmsClient.a(BaseGmsClient.this);
                return;
            }
            synchronized (BaseGmsClient.this.q) {
                BaseGmsClient baseGmsClient = BaseGmsClient.this;
                if (iBinder == null) {
                    iGmsServiceBroker = null;
                } else {
                    IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    if (queryLocalInterface == null || !(queryLocalInterface instanceof IGmsServiceBroker)) {
                        iGmsServiceBroker = new IGmsServiceBroker.Stub.a(iBinder);
                    } else {
                        iGmsServiceBroker = (IGmsServiceBroker) queryLocalInterface;
                    }
                }
                IGmsServiceBroker unused = baseGmsClient.r = iGmsServiceBroker;
            }
            BaseGmsClient.this.a(0, this.f1564a);
        }

        public final void onServiceDisconnected(ComponentName componentName) {
            synchronized (BaseGmsClient.this.q) {
                IGmsServiceBroker unused = BaseGmsClient.this.r = (IGmsServiceBroker) null;
            }
            BaseGmsClient.this.d.sendMessage(BaseGmsClient.this.d.obtainMessage(6, this.f1564a, 1));
        }
    }

    protected final class j extends f {

        /* renamed from: a  reason: collision with root package name */
        private final IBinder f1566a;

        public j(int i, IBinder iBinder, Bundle bundle) {
            super(i, bundle);
            this.f1566a = iBinder;
        }

        /* access modifiers changed from: protected */
        public final void a(ConnectionResult connectionResult) {
            if (BaseGmsClient.this.x != null) {
                BaseGmsClient.this.x.a(connectionResult);
            }
            BaseGmsClient.this.a(connectionResult);
        }

        /* access modifiers changed from: protected */
        public final boolean a() {
            try {
                String interfaceDescriptor = this.f1566a.getInterfaceDescriptor();
                if (!BaseGmsClient.this.j().equals(interfaceDescriptor)) {
                    String j = BaseGmsClient.this.j();
                    StringBuilder sb = new StringBuilder(String.valueOf(j).length() + 34 + String.valueOf(interfaceDescriptor).length());
                    sb.append("service descriptor mismatch: ");
                    sb.append(j);
                    sb.append(" vs. ");
                    sb.append(interfaceDescriptor);
                    sb.toString();
                    return false;
                }
                IInterface a2 = BaseGmsClient.this.a(this.f1566a);
                if (a2 == null) {
                    return false;
                }
                if (!BaseGmsClient.this.a(2, 4, a2) && !BaseGmsClient.this.a(3, 4, a2)) {
                    return false;
                }
                ConnectionResult unused = BaseGmsClient.this.A = (ConnectionResult) null;
                Bundle a_ = BaseGmsClient.this.a_();
                if (BaseGmsClient.this.w != null) {
                    BaseGmsClient.this.w.a(a_);
                }
                return true;
            } catch (RemoteException unused2) {
                return false;
            }
        }
    }

    public static final class zzd extends IGmsCallbacks.zza {

        /* renamed from: a  reason: collision with root package name */
        private BaseGmsClient f1569a;

        /* renamed from: b  reason: collision with root package name */
        private final int f1570b;

        public zzd(BaseGmsClient baseGmsClient, int i) {
            this.f1569a = baseGmsClient;
            this.f1570b = i;
        }

        public final void a(int i, Bundle bundle) {
            Log.wtf("GmsClient", "received deprecated onAccountValidationComplete callback, ignoring", new Exception());
        }

        public final void a(int i, IBinder iBinder, Bundle bundle) {
            l.a(this.f1569a, "onPostInitComplete can be called only once per call to getRemoteService");
            this.f1569a.a(i, iBinder, bundle, this.f1570b);
            this.f1569a = null;
        }

        public final void a(int i, IBinder iBinder, zzb zzb) {
            l.a(this.f1569a, "onPostInitCompleteWithConnectionInfo can be called only once per call togetRemoteService");
            l.a(zzb);
            this.f1569a.C = zzb;
            a(i, iBinder, zzb.f1623a);
        }
    }

    abstract class f extends h<Boolean> {

        /* renamed from: a  reason: collision with root package name */
        private final int f1559a;

        /* renamed from: b  reason: collision with root package name */
        private final Bundle f1560b;

        protected f(int i, Bundle bundle) {
            super(true);
            this.f1559a = i;
            this.f1560b = bundle;
        }

        /* access modifiers changed from: protected */
        public abstract void a(ConnectionResult connectionResult);

        /* access modifiers changed from: protected */
        public abstract boolean a();

        /* JADX WARN: Type inference failed for: r5v11, types: [android.os.Parcelable] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final /* synthetic */ void a(java.lang.Object r5) {
            /*
                r4 = this;
                java.lang.Boolean r5 = (java.lang.Boolean) r5
                r0 = 1
                if (r5 != 0) goto L_0x000b
                com.google.android.gms.common.internal.BaseGmsClient r5 = com.google.android.gms.common.internal.BaseGmsClient.this
                r5.a(r0, (android.os.IInterface) null)
                return
            L_0x000b:
                int r5 = r4.f1559a
                r1 = 0
                if (r5 == 0) goto L_0x0061
                r2 = 10
                if (r5 == r2) goto L_0x0031
                com.google.android.gms.common.internal.BaseGmsClient r5 = com.google.android.gms.common.internal.BaseGmsClient.this
                r5.a(r0, (android.os.IInterface) null)
                android.os.Bundle r5 = r4.f1560b
                if (r5 == 0) goto L_0x0026
                java.lang.String r0 = "pendingIntent"
                android.os.Parcelable r5 = r5.getParcelable(r0)
                r1 = r5
                android.app.PendingIntent r1 = (android.app.PendingIntent) r1
            L_0x0026:
                com.google.android.gms.common.ConnectionResult r5 = new com.google.android.gms.common.ConnectionResult
                int r0 = r4.f1559a
                r5.<init>(r0, r1)
                r4.a(r5)
                goto L_0x0076
            L_0x0031:
                com.google.android.gms.common.internal.BaseGmsClient r5 = com.google.android.gms.common.internal.BaseGmsClient.this
                r5.a(r0, (android.os.IInterface) null)
                java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
                r1 = 3
                java.lang.Object[] r1 = new java.lang.Object[r1]
                r2 = 0
                java.lang.Class r3 = r4.getClass()
                java.lang.String r3 = r3.getSimpleName()
                r1[r2] = r3
                com.google.android.gms.common.internal.BaseGmsClient r2 = com.google.android.gms.common.internal.BaseGmsClient.this
                java.lang.String r2 = r2.i()
                r1[r0] = r2
                r0 = 2
                com.google.android.gms.common.internal.BaseGmsClient r2 = com.google.android.gms.common.internal.BaseGmsClient.this
                java.lang.String r2 = r2.j()
                r1[r0] = r2
                java.lang.String r0 = "A fatal developer error has occurred. Class name: %s. Start service action: %s. Service Descriptor: %s. "
                java.lang.String r0 = java.lang.String.format(r0, r1)
                r5.<init>(r0)
                throw r5
            L_0x0061:
                boolean r5 = r4.a()
                if (r5 != 0) goto L_0x0076
                com.google.android.gms.common.internal.BaseGmsClient r5 = com.google.android.gms.common.internal.BaseGmsClient.this
                r5.a(r0, (android.os.IInterface) null)
                com.google.android.gms.common.ConnectionResult r5 = new com.google.android.gms.common.ConnectionResult
                r0 = 8
                r5.<init>(r0, r1)
                r4.a(r5)
            L_0x0076:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.internal.BaseGmsClient.f.a(java.lang.Object):void");
        }
    }

    protected BaseGmsClient(Context context, Looper looper, g gVar, com.google.android.gms.common.d dVar, int i2, a aVar, b bVar, String str) {
        this.p = new Object();
        this.q = new Object();
        this.t = new ArrayList<>();
        this.v = 1;
        this.A = null;
        this.B = false;
        this.C = null;
        this.f = new AtomicInteger(0);
        this.c = (Context) l.a(context, "Context must not be null");
        this.m = (Looper) l.a(looper, "Looper must not be null");
        this.n = (g) l.a(gVar, "Supervisor must not be null");
        this.o = (com.google.android.gms.common.d) l.a(dVar, "API availability must not be null");
        this.d = new g(looper);
        this.y = i2;
        this.w = aVar;
        this.x = bVar;
        this.z = str;
    }

    private final String r() {
        String str = this.z;
        return str == null ? this.c.getClass().getName() : str;
    }

    public final Feature[] h() {
        zzb zzb = this.C;
        if (zzb == null) {
            return null;
        }
        return zzb.f1624b;
    }

    /* access modifiers changed from: protected */
    public void a(IInterface iInterface) {
        this.i = System.currentTimeMillis();
    }

    /* access modifiers changed from: private */
    public final void a(int i2, T t2) {
        l.b((i2 == 4) == (t2 != null));
        synchronized (this.p) {
            this.v = i2;
            this.s = t2;
            if (i2 != 1) {
                if (i2 == 2 || i2 == 3) {
                    if (!(this.u == null || this.l == null)) {
                        String str = this.l.f1591a;
                        String str2 = this.l.f1592b;
                        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 70 + String.valueOf(str2).length());
                        sb.append("Calling connect() while still connected, missing disconnect() for ");
                        sb.append(str);
                        sb.append(" on ");
                        sb.append(str2);
                        sb.toString();
                        this.n.a(this.l.f1591a, this.l.f1592b, this.l.c, this.u, r());
                        this.f.incrementAndGet();
                    }
                    this.u = new i(this.f.get());
                    this.l = new af("com.google.android.gms", i(), false);
                    if (!this.n.a(new g.a(this.l.f1591a, this.l.f1592b, this.l.c), this.u, r())) {
                        String str3 = this.l.f1591a;
                        String str4 = this.l.f1592b;
                        StringBuilder sb2 = new StringBuilder(String.valueOf(str3).length() + 34 + String.valueOf(str4).length());
                        sb2.append("unable to connect to service: ");
                        sb2.append(str3);
                        sb2.append(" on ");
                        sb2.append(str4);
                        sb2.toString();
                        a(16, this.f.get());
                    }
                } else if (i2 == 4) {
                    a((IInterface) t2);
                }
            } else if (this.u != null) {
                this.n.a(i(), "com.google.android.gms", 129, this.u, r());
                this.u = null;
            }
        }
    }

    /* access modifiers changed from: private */
    public final boolean a(int i2, int i3, T t2) {
        synchronized (this.p) {
            if (this.v != i2) {
                return false;
            }
            a(i3, t2);
            return true;
        }
    }

    public final void k() {
        int a2 = this.o.a(this.c, g());
        if (a2 != 0) {
            a(1, (IInterface) null);
            this.e = (c) l.a(new d(), "Connection progress callbacks cannot be null.");
            Handler handler = this.d;
            handler.sendMessage(handler.obtainMessage(3, this.f.get(), a2, null));
            return;
        }
        a(new d());
    }

    public void a(c cVar) {
        this.e = (c) l.a(cVar, "Connection progress callbacks cannot be null.");
        a(2, (IInterface) null);
    }

    public final boolean b() {
        boolean z2;
        synchronized (this.p) {
            z2 = this.v == 4;
        }
        return z2;
    }

    public final boolean c() {
        boolean z2;
        synchronized (this.p) {
            if (this.v != 2) {
                if (this.v != 3) {
                    z2 = false;
                }
            }
            z2 = true;
        }
        return z2;
    }

    private final boolean s() {
        boolean z2;
        synchronized (this.p) {
            z2 = this.v == 3;
        }
        return z2;
    }

    public void a() {
        this.f.incrementAndGet();
        synchronized (this.t) {
            int size = this.t.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.t.get(i2).d();
            }
            this.t.clear();
        }
        synchronized (this.q) {
            this.r = null;
        }
        a(1, (IInterface) null);
    }

    public Feature[] m() {
        return h;
    }

    /* access modifiers changed from: protected */
    public Bundle n() {
        return new Bundle();
    }

    /* access modifiers changed from: protected */
    public void a(int i2, IBinder iBinder, Bundle bundle, int i3) {
        Handler handler = this.d;
        handler.sendMessage(handler.obtainMessage(1, i3, -1, new j(i2, iBinder, bundle)));
    }

    /* access modifiers changed from: protected */
    public final void a(int i2, int i3) {
        Handler handler = this.d;
        handler.sendMessage(handler.obtainMessage(7, i3, -1, new k(i2)));
    }

    /* access modifiers changed from: protected */
    public final void o() {
        if (!b()) {
            throw new IllegalStateException("Not connected. Call connect() and wait for onConnected() to be called.");
        }
    }

    public final T p() throws DeadObjectException {
        T t2;
        synchronized (this.p) {
            if (this.v != 5) {
                o();
                l.a(this.s != null, "Client is connected but service is null");
                t2 = this.s;
            } else {
                throw new DeadObjectException();
            }
        }
        return t2;
    }

    public final void a(IAccountAccessor iAccountAccessor, Set<Scope> set) {
        Bundle n2 = n();
        GetServiceRequest getServiceRequest = new GetServiceRequest(this.y);
        getServiceRequest.f1576a = this.c.getPackageName();
        getServiceRequest.d = n2;
        if (set != null) {
            getServiceRequest.c = (Scope[]) set.toArray(new Scope[set.size()]);
        }
        if (d()) {
            getServiceRequest.e = l() != null ? l() : new Account("<<default account>>", "com.google");
            if (iAccountAccessor != null) {
                getServiceRequest.f1577b = iAccountAccessor.asBinder();
            }
        }
        getServiceRequest.f = h;
        getServiceRequest.g = m();
        try {
            synchronized (this.q) {
                if (this.r != null) {
                    this.r.a(new zzd(this, this.f.get()), getServiceRequest);
                }
            }
        } catch (DeadObjectException unused) {
            Handler handler = this.d;
            handler.sendMessage(handler.obtainMessage(6, this.f.get(), 1));
        } catch (SecurityException e2) {
            throw e2;
        } catch (RemoteException | RuntimeException unused2) {
            a(8, (IBinder) null, (Bundle) null, this.f.get());
        }
    }

    public void a(e eVar) {
        eVar.a();
    }

    public final Intent e() {
        throw new UnsupportedOperationException("Not a sign in API");
    }

    /* access modifiers changed from: protected */
    public Set<Scope> q() {
        return Collections.EMPTY_SET;
    }

    public final void a(String str, PrintWriter printWriter) {
        int i2;
        T t2;
        IGmsServiceBroker iGmsServiceBroker;
        synchronized (this.p) {
            i2 = this.v;
            t2 = this.s;
        }
        synchronized (this.q) {
            iGmsServiceBroker = this.r;
        }
        printWriter.append((CharSequence) str).append((CharSequence) "mConnectState=");
        if (i2 == 1) {
            printWriter.print("DISCONNECTED");
        } else if (i2 == 2) {
            printWriter.print("REMOTE_CONNECTING");
        } else if (i2 == 3) {
            printWriter.print("LOCAL_CONNECTING");
        } else if (i2 == 4) {
            printWriter.print("CONNECTED");
        } else if (i2 != 5) {
            printWriter.print("UNKNOWN");
        } else {
            printWriter.print("DISCONNECTING");
        }
        printWriter.append((CharSequence) " mService=");
        if (t2 == null) {
            printWriter.append((CharSequence) "null");
        } else {
            printWriter.append((CharSequence) j()).append((CharSequence) "@").append((CharSequence) Integer.toHexString(System.identityHashCode(t2.asBinder())));
        }
        printWriter.append((CharSequence) " mServiceBroker=");
        if (iGmsServiceBroker == null) {
            printWriter.println("null");
        } else {
            printWriter.append((CharSequence) "IGmsServiceBroker@").println(Integer.toHexString(System.identityHashCode(iGmsServiceBroker.asBinder())));
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
        if (this.i > 0) {
            PrintWriter append = printWriter.append((CharSequence) str).append((CharSequence) "lastConnectedTime=");
            long j2 = this.i;
            String format = simpleDateFormat.format(new Date(j2));
            StringBuilder sb = new StringBuilder(String.valueOf(format).length() + 21);
            sb.append(j2);
            sb.append(" ");
            sb.append(format);
            append.println(sb.toString());
        }
        if (this.f1557b > 0) {
            printWriter.append((CharSequence) str).append((CharSequence) "lastSuspendedCause=");
            int i3 = this.f1556a;
            if (i3 == 1) {
                printWriter.append((CharSequence) "CAUSE_SERVICE_DISCONNECTED");
            } else if (i3 != 2) {
                printWriter.append((CharSequence) String.valueOf(i3));
            } else {
                printWriter.append((CharSequence) "CAUSE_NETWORK_LOST");
            }
            PrintWriter append2 = printWriter.append((CharSequence) " lastSuspendedTime=");
            long j3 = this.f1557b;
            String format2 = simpleDateFormat.format(new Date(j3));
            StringBuilder sb2 = new StringBuilder(String.valueOf(format2).length() + 21);
            sb2.append(j3);
            sb2.append(" ");
            sb2.append(format2);
            append2.println(sb2.toString());
        }
        if (this.k > 0) {
            printWriter.append((CharSequence) str).append((CharSequence) "lastFailedStatus=").append((CharSequence) com.google.android.gms.common.api.b.a(this.j));
            PrintWriter append3 = printWriter.append((CharSequence) " lastFailedTime=");
            long j4 = this.k;
            String format3 = simpleDateFormat.format(new Date(j4));
            StringBuilder sb3 = new StringBuilder(String.valueOf(format3).length() + 21);
            sb3.append(j4);
            sb3.append(" ");
            sb3.append(format3);
            append3.println(sb3.toString());
        }
    }

    /* access modifiers changed from: private */
    public final boolean t() {
        if (this.B || TextUtils.isEmpty(j()) || TextUtils.isEmpty(null)) {
            return false;
        }
        try {
            Class.forName(j());
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        }
    }

    public final String f() {
        af afVar;
        if (b() && (afVar = this.l) != null) {
            return afVar.f1592b;
        }
        throw new RuntimeException("Failed to connect when checking package");
    }

    public int g() {
        return com.google.android.gms.common.d.f1522b;
    }

    /* access modifiers changed from: protected */
    public void a(ConnectionResult connectionResult) {
        this.j = connectionResult.f1353b;
        this.k = System.currentTimeMillis();
    }

    static /* synthetic */ void a(BaseGmsClient baseGmsClient) {
        int i2;
        if (baseGmsClient.s()) {
            i2 = 5;
            baseGmsClient.B = true;
        } else {
            i2 = 4;
        }
        Handler handler = baseGmsClient.d;
        handler.sendMessage(handler.obtainMessage(i2, baseGmsClient.f.get(), 16));
    }
}
