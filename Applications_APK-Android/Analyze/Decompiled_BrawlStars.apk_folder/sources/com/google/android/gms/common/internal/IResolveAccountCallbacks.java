package com.google.android.gms.common.internal;

import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.internal.base.a;
import com.google.android.gms.internal.base.zaa;
import com.google.android.gms.internal.base.zab;

public interface IResolveAccountCallbacks extends IInterface {

    public static abstract class Stub extends zab implements IResolveAccountCallbacks {
        public Stub() {
            super("com.google.android.gms.common.internal.IResolveAccountCallbacks");
        }

        public static class Proxy extends zaa implements IResolveAccountCallbacks {
            public final void a(ResolveAccountResponse resolveAccountResponse) throws RemoteException {
                Parcel a2 = a();
                a.a(a2, resolveAccountResponse);
                b(2, a2);
            }
        }

        public final boolean a(int i, Parcel parcel, Parcel parcel2) throws RemoteException {
            if (i != 2) {
                return false;
            }
            a((ResolveAccountResponse) a.a(parcel, ResolveAccountResponse.CREATOR));
            parcel2.writeNoException();
            return true;
        }
    }

    void a(ResolveAccountResponse resolveAccountResponse) throws RemoteException;
}
