package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.fq;
import com.google.android.gms.internal.measurement.fq.a;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public abstract class fq<MessageType extends fq<MessageType, BuilderType>, BuilderType extends a<MessageType, BuilderType>> extends eb<MessageType, BuilderType> {
    private static Map<Object, fq<?, ?>> zzbyf = new ConcurrentHashMap();
    protected ic zzbyd = ic.a();
    private int zzbye = -1;

    public static abstract class c<MessageType extends c<MessageType, BuilderType>, BuilderType> extends fq<MessageType, BuilderType> implements gv {
        protected fh<Object> zzbyj = fh.a();
    }

    public static class d<ContainingType extends gt, Type> extends fb<ContainingType, Type> {
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class e {

        /* renamed from: a  reason: collision with root package name */
        public static final int f2223a = 1;

        /* renamed from: b  reason: collision with root package name */
        public static final int f2224b = 2;
        public static final int c = 3;
        public static final int d = 4;
        public static final int e = 5;
        public static final int f = 6;
        public static final int g = 7;
        public static final int h = 1;
        public static final int i = 2;
        public static final int j = 1;
        public static final int k = 2;
        private static final /* synthetic */ int[] l = {f2223a, f2224b, c, d, e, f, g};
        private static final /* synthetic */ int[] m = {h, i};
        private static final /* synthetic */ int[] n = {j, k};

        public static int[] a() {
            return (int[]) l.clone();
        }
    }

    /* access modifiers changed from: protected */
    public abstract Object a(int i);

    public static class b<T extends fq<T, ?>> extends ed<T> {

        /* renamed from: a  reason: collision with root package name */
        private final T f2222a;

        public b(T t) {
            this.f2222a = t;
        }

        public final /* synthetic */ Object a(ev evVar, fd fdVar) throws zzuv {
            return fq.a(this.f2222a, evVar, fdVar);
        }
    }

    public String toString() {
        String obj = super.toString();
        StringBuilder sb = new StringBuilder();
        sb.append("# ");
        sb.append(obj);
        gw.a(this, sb, 0);
        return sb.toString();
    }

    public int hashCode() {
        if (this.zzbti != 0) {
            return this.zzbti;
        }
        this.zzbti = hf.a().a(this).a(this);
        return this.zzbti;
    }

    public static abstract class a<MessageType extends fq<MessageType, BuilderType>, BuilderType extends a<MessageType, BuilderType>> extends ec<MessageType, BuilderType> {

        /* renamed from: a  reason: collision with root package name */
        protected MessageType f2220a;

        /* renamed from: b  reason: collision with root package name */
        private final MessageType f2221b;
        private boolean c = false;

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: MessageType
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        protected a(MessageType r2) {
            /*
                r1 = this;
                r1.<init>()
                r1.f2221b = r2
                int r0 = com.google.android.gms.internal.measurement.fq.e.d
                java.lang.Object r2 = r2.a(r0)
                com.google.android.gms.internal.measurement.fq r2 = (com.google.android.gms.internal.measurement.fq) r2
                r1.f2220a = r2
                r2 = 0
                r1.c = r2
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.fq.a.<init>(com.google.android.gms.internal.measurement.fq):void");
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: MessageType
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        protected final void b() {
            /*
                r2 = this;
                boolean r0 = r2.c
                if (r0 == 0) goto L_0x0018
                MessageType r0 = r2.f2220a
                int r1 = com.google.android.gms.internal.measurement.fq.e.d
                java.lang.Object r0 = r0.a(r1)
                com.google.android.gms.internal.measurement.fq r0 = (com.google.android.gms.internal.measurement.fq) r0
                MessageType r1 = r2.f2220a
                a(r0, r1)
                r2.f2220a = r0
                r0 = 0
                r2.c = r0
            L_0x0018:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.fq.a.b():void");
        }

        public final boolean f() {
            return fq.a((fq) this.f2220a);
        }

        /* access modifiers changed from: private */
        /* renamed from: e */
        public MessageType c() {
            if (this.c) {
                return this.f2220a;
            }
            MessageType messagetype = this.f2220a;
            hf.a().a((Object) messagetype).c(messagetype);
            this.c = true;
            return this.f2220a;
        }

        public final BuilderType a(MessageType messagetype) {
            b();
            a(this.f2220a, messagetype);
            return this;
        }

        private static void a(MessageType messagetype, MessageType messagetype2) {
            hf.a().a((Object) messagetype).b(messagetype, messagetype2);
        }

        public final /* synthetic */ ec a() {
            return (a) clone();
        }

        public final /* synthetic */ gt k() {
            return this.f2221b;
        }

        public /* synthetic */ Object clone() throws CloneNotSupportedException {
            a aVar = (a) ((fq) this.f2221b).a(e.e);
            aVar.a(c());
            return aVar;
        }

        /* access modifiers changed from: private */
        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: MessageType
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        /* renamed from: g */
        public MessageType d() {
            /*
                r4 = this;
                com.google.android.gms.internal.measurement.fq r0 = r4.c()
                com.google.android.gms.internal.measurement.fq r0 = (com.google.android.gms.internal.measurement.fq) r0
                java.lang.Boolean r1 = java.lang.Boolean.TRUE
                boolean r1 = r1.booleanValue()
                int r2 = com.google.android.gms.internal.measurement.fq.e.f2223a
                java.lang.Object r2 = r0.a(r2)
                java.lang.Byte r2 = (java.lang.Byte) r2
                byte r2 = r2.byteValue()
                r3 = 1
                if (r2 != r3) goto L_0x001c
                goto L_0x0033
            L_0x001c:
                if (r2 != 0) goto L_0x0020
                r3 = 0
                goto L_0x0033
            L_0x0020:
                com.google.android.gms.internal.measurement.hf r2 = com.google.android.gms.internal.measurement.hf.a()
                com.google.android.gms.internal.measurement.hj r2 = r2.a(r0)
                boolean r3 = r2.d(r0)
                if (r1 == 0) goto L_0x0033
                int r1 = com.google.android.gms.internal.measurement.fq.e.f2224b
                r0.a(r1)
            L_0x0033:
                if (r3 == 0) goto L_0x0036
                return r0
            L_0x0036:
                com.google.android.gms.internal.measurement.zzxc r0 = new com.google.android.gms.internal.measurement.zzxc
                r0.<init>()
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.fq.a.d():com.google.android.gms.internal.measurement.fq");
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!((fq) a(e.f)).getClass().isInstance(obj)) {
            return false;
        }
        return hf.a().a(this).a(this, (fq) obj);
    }

    public final boolean f() {
        boolean booleanValue = Boolean.TRUE.booleanValue();
        byte byteValue = ((Byte) a(e.f2223a)).byteValue();
        if (byteValue == 1) {
            return true;
        }
        if (byteValue == 0) {
            return false;
        }
        boolean d2 = hf.a().a(this).d(this);
        if (booleanValue) {
            a(e.f2224b);
        }
        return d2;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: BuilderType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final BuilderType g() {
        /*
            r1 = this;
            int r0 = com.google.android.gms.internal.measurement.fq.e.e
            java.lang.Object r0 = r1.a(r0)
            com.google.android.gms.internal.measurement.fq$a r0 = (com.google.android.gms.internal.measurement.fq.a) r0
            r0.a(r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.fq.g():com.google.android.gms.internal.measurement.fq$a");
    }

    /* access modifiers changed from: package-private */
    public final int e() {
        return this.zzbye;
    }

    /* access modifiers changed from: package-private */
    public final void b(int i) {
        this.zzbye = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.hj.a(java.lang.Object, com.google.android.gms.internal.measurement.iv):void
     arg types: [com.google.android.gms.internal.measurement.fq, com.google.android.gms.internal.measurement.ez]
     candidates:
      com.google.android.gms.internal.measurement.hj.a(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.hj.a(java.lang.Object, com.google.android.gms.internal.measurement.iv):void */
    public final void a(zztv zztv) throws IOException {
        ez ezVar;
        hj a2 = hf.a().a((Class) getClass());
        if (zztv.f2350b != null) {
            ezVar = zztv.f2350b;
        } else {
            ezVar = new ez(zztv);
        }
        a2.a((Object) this, (iv) ezVar);
    }

    public final int h() {
        if (this.zzbye == -1) {
            this.zzbye = hf.a().a(this).b(this);
        }
        return this.zzbye;
    }

    static <T extends fq<?, ?>> T a(Class<T> cls) {
        T t = (fq) zzbyf.get(cls);
        if (t == null) {
            try {
                Class.forName(cls.getName(), true, cls.getClassLoader());
                t = (fq) zzbyf.get(cls);
            } catch (ClassNotFoundException e2) {
                throw new IllegalStateException("Class initialization cannot fail.", e2);
            }
        }
        if (t == null) {
            t = (fq) ((fq) ih.a(cls)).a(e.f);
            if (t != null) {
                zzbyf.put(cls, t);
            } else {
                throw new IllegalStateException();
            }
        }
        return t;
    }

    protected static <T extends fq<?, ?>> void a(Class<T> cls, T t) {
        zzbyf.put(cls, t);
    }

    protected static Object a(gt gtVar, String str, Object[] objArr) {
        return new hh(gtVar, str, objArr);
    }

    static Object a(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e2) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e2);
        } catch (InvocationTargetException e3) {
            Throwable cause = e3.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected static final <T extends com.google.android.gms.internal.measurement.fq<T, ?>> boolean a(T r2) {
        /*
            int r0 = com.google.android.gms.internal.measurement.fq.e.f2223a
            java.lang.Object r0 = r2.a(r0)
            java.lang.Byte r0 = (java.lang.Byte) r0
            byte r0 = r0.byteValue()
            r1 = 1
            if (r0 != r1) goto L_0x0010
            return r1
        L_0x0010:
            if (r0 != 0) goto L_0x0014
            r2 = 0
            return r2
        L_0x0014:
            com.google.android.gms.internal.measurement.hf r0 = com.google.android.gms.internal.measurement.hf.a()
            com.google.android.gms.internal.measurement.hj r0 = r0.a(r2)
            boolean r2 = r0.d(r2)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.fq.a(com.google.android.gms.internal.measurement.fq):boolean");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    static <T extends com.google.android.gms.internal.measurement.fq<T, ?>> T a(T r2, com.google.android.gms.internal.measurement.ev r3, com.google.android.gms.internal.measurement.fd r4) throws com.google.android.gms.internal.measurement.zzuv {
        /*
            int r0 = com.google.android.gms.internal.measurement.fq.e.d
            java.lang.Object r2 = r2.a(r0)
            com.google.android.gms.internal.measurement.fq r2 = (com.google.android.gms.internal.measurement.fq) r2
            com.google.android.gms.internal.measurement.hf r0 = com.google.android.gms.internal.measurement.hf.a()     // Catch:{ IOException -> 0x003d, RuntimeException -> 0x002c }
            com.google.android.gms.internal.measurement.hj r0 = r0.a(r2)     // Catch:{ IOException -> 0x003d, RuntimeException -> 0x002c }
            com.google.android.gms.internal.measurement.ex r1 = r3.c     // Catch:{ IOException -> 0x003d, RuntimeException -> 0x002c }
            if (r1 == 0) goto L_0x0017
            com.google.android.gms.internal.measurement.ex r3 = r3.c     // Catch:{ IOException -> 0x003d, RuntimeException -> 0x002c }
            goto L_0x001d
        L_0x0017:
            com.google.android.gms.internal.measurement.ex r1 = new com.google.android.gms.internal.measurement.ex     // Catch:{ IOException -> 0x003d, RuntimeException -> 0x002c }
            r1.<init>(r3)     // Catch:{ IOException -> 0x003d, RuntimeException -> 0x002c }
            r3 = r1
        L_0x001d:
            r0.a(r2, r3, r4)     // Catch:{ IOException -> 0x003d, RuntimeException -> 0x002c }
            com.google.android.gms.internal.measurement.hf r3 = com.google.android.gms.internal.measurement.hf.a()     // Catch:{ IOException -> 0x003d, RuntimeException -> 0x002c }
            com.google.android.gms.internal.measurement.hj r3 = r3.a(r2)     // Catch:{ IOException -> 0x003d, RuntimeException -> 0x002c }
            r3.c(r2)     // Catch:{ IOException -> 0x003d, RuntimeException -> 0x002c }
            return r2
        L_0x002c:
            r2 = move-exception
            java.lang.Throwable r3 = r2.getCause()
            boolean r3 = r3 instanceof com.google.android.gms.internal.measurement.zzuv
            if (r3 == 0) goto L_0x003c
            java.lang.Throwable r2 = r2.getCause()
            com.google.android.gms.internal.measurement.zzuv r2 = (com.google.android.gms.internal.measurement.zzuv) r2
            throw r2
        L_0x003c:
            throw r2
        L_0x003d:
            r3 = move-exception
            java.lang.Throwable r4 = r3.getCause()
            boolean r4 = r4 instanceof com.google.android.gms.internal.measurement.zzuv
            if (r4 == 0) goto L_0x004d
            java.lang.Throwable r2 = r3.getCause()
            com.google.android.gms.internal.measurement.zzuv r2 = (com.google.android.gms.internal.measurement.zzuv) r2
            throw r2
        L_0x004d:
            com.google.android.gms.internal.measurement.zzuv r4 = new com.google.android.gms.internal.measurement.zzuv
            java.lang.String r3 = r3.getMessage()
            r4.<init>(r3)
            r4.f2351a = r2
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.fq.a(com.google.android.gms.internal.measurement.fq, com.google.android.gms.internal.measurement.ev, com.google.android.gms.internal.measurement.fd):com.google.android.gms.internal.measurement.fq");
    }

    public final /* synthetic */ gu i() {
        a aVar = (a) a(e.e);
        aVar.a(this);
        return aVar;
    }

    public final /* synthetic */ gu j() {
        return (a) a(e.e);
    }

    public final /* synthetic */ gt k() {
        return (fq) a(e.f);
    }
}
