package com.google.android.gms.measurement.internal;

import android.content.Context;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.util.e;
import java.lang.Thread;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicLong;

public final class am extends bn {
    /* access modifiers changed from: private */
    public static final AtomicLong j = new AtomicLong(Long.MIN_VALUE);
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public aq f2379a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public aq f2380b;
    private final PriorityBlockingQueue<ap<?>> c = new PriorityBlockingQueue<>();
    private final BlockingQueue<ap<?>> d = new LinkedBlockingQueue();
    private final Thread.UncaughtExceptionHandler e = new ao(this, "Thread death: Uncaught exception on worker thread");
    private final Thread.UncaughtExceptionHandler f = new ao(this, "Thread death: Uncaught exception on network thread");
    /* access modifiers changed from: private */
    public final Object g = new Object();
    /* access modifiers changed from: private */
    public final Semaphore h = new Semaphore(2);
    /* access modifiers changed from: private */
    public volatile boolean i;

    am(ar arVar) {
        super(arVar);
    }

    /* access modifiers changed from: protected */
    public final boolean d() {
        return false;
    }

    public final void c() {
        if (Thread.currentThread() != this.f2379a) {
            throw new IllegalStateException("Call expected from worker thread");
        }
    }

    public final void b() {
        if (Thread.currentThread() != this.f2380b) {
            throw new IllegalStateException("Call expected from network thread");
        }
    }

    public final boolean f() {
        return Thread.currentThread() == this.f2379a;
    }

    public final <V> Future<V> a(Callable callable) throws IllegalStateException {
        w();
        l.a(callable);
        ap apVar = new ap(this, callable, false, "Task exception on worker thread");
        if (Thread.currentThread() == this.f2379a) {
            if (!this.c.isEmpty()) {
                q().f.a("Callable skipped the worker queue.");
            }
            apVar.run();
        } else {
            a((ap<?>) apVar);
        }
        return apVar;
    }

    public final <V> Future<V> b(Callable<V> callable) throws IllegalStateException {
        w();
        l.a(callable);
        ap apVar = new ap(this, callable, true, "Task exception on worker thread");
        if (Thread.currentThread() == this.f2379a) {
            apVar.run();
        } else {
            a((ap<?>) apVar);
        }
        return apVar;
    }

    public final void a(Runnable runnable) throws IllegalStateException {
        w();
        l.a(runnable);
        a((ap<?>) new ap(this, runnable, "Task exception on worker thread"));
    }

    private final void a(ap<?> apVar) {
        synchronized (this.g) {
            this.c.add(apVar);
            if (this.f2379a == null) {
                this.f2379a = new aq(this, "Measurement Worker", this.c);
                this.f2379a.setUncaughtExceptionHandler(this.e);
                this.f2379a.start();
            } else {
                this.f2379a.a();
            }
        }
    }

    public final void b(Runnable runnable) throws IllegalStateException {
        w();
        l.a(runnable);
        ap apVar = new ap(this, runnable, "Task exception on network thread");
        synchronized (this.g) {
            this.d.add(apVar);
            if (this.f2380b == null) {
                this.f2380b = new aq(this, "Measurement Network", this.d);
                this.f2380b.setUncaughtExceptionHandler(this.f);
                this.f2380b.start();
            } else {
                this.f2380b.a();
            }
        }
    }

    public final /* bridge */ /* synthetic */ void a() {
        super.a();
    }

    public final /* bridge */ /* synthetic */ b k() {
        return super.k();
    }

    public final /* bridge */ /* synthetic */ e l() {
        return super.l();
    }

    public final /* bridge */ /* synthetic */ Context m() {
        return super.m();
    }

    public final /* bridge */ /* synthetic */ m n() {
        return super.n();
    }

    public final /* bridge */ /* synthetic */ ed o() {
        return super.o();
    }

    public final /* bridge */ /* synthetic */ am p() {
        return super.p();
    }

    public final /* bridge */ /* synthetic */ o q() {
        return super.q();
    }

    public final /* bridge */ /* synthetic */ z r() {
        return super.r();
    }

    public final /* bridge */ /* synthetic */ el s() {
        return super.s();
    }
}
