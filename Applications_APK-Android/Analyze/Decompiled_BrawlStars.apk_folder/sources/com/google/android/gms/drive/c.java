package com.google.android.gms.drive;

import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;
import com.google.android.gms.drive.metadata.internal.e;
import com.google.android.gms.internal.drive.au;

public final class c extends com.google.android.gms.common.data.a<b> {

    /* renamed from: b  reason: collision with root package name */
    private a f1702b;

    static class a extends b {

        /* renamed from: a  reason: collision with root package name */
        final int f1703a;

        /* renamed from: b  reason: collision with root package name */
        private final DataHolder f1704b;
        private final int c;

        public a(DataHolder dataHolder, int i) {
            this.f1704b = dataHolder;
            this.f1703a = i;
            this.c = dataHolder.a(i);
        }

        public final /* synthetic */ Object a() {
            MetadataBundle a2 = MetadataBundle.a();
            for (com.google.android.gms.drive.metadata.a<BitmapTeleporter> next : e.a()) {
                if (next != au.F) {
                    next.a(this.f1704b, a2, this.f1703a, this.c);
                }
            }
            return new com.google.android.gms.internal.drive.a(a2);
        }

        public final <T> T a(com.google.android.gms.drive.metadata.a<T> aVar) {
            return aVar.a(this.f1704b, this.f1703a, this.c);
        }
    }

    public final void a() {
        if (this.f1529a != null) {
            e.a(this.f1529a);
        }
        super.a();
    }

    public c(DataHolder dataHolder) {
        super(dataHolder);
        dataHolder.d.setClassLoader(c.class.getClassLoader());
    }

    public final /* synthetic */ Object a(int i) {
        a aVar = this.f1702b;
        if (aVar != null && aVar.f1703a == i) {
            return aVar;
        }
        a aVar2 = new a(this.f1529a, i);
        this.f1702b = aVar2;
        return aVar2;
    }
}
