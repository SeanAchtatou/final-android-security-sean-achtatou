package com.google.android.gms.measurement.internal;

import android.os.Bundle;

public final class j {

    /* renamed from: a  reason: collision with root package name */
    public final long f2567a;

    /* renamed from: b  reason: collision with root package name */
    public final long f2568b;
    public final boolean c;
    public final String d;
    public final String e = null;
    public final String f = null;
    public final Bundle g;

    public j(long j, long j2, boolean z, String str, String str2, String str3, Bundle bundle) {
        this.f2567a = j;
        this.f2568b = j2;
        this.c = z;
        this.d = str;
        this.g = bundle;
    }
}
