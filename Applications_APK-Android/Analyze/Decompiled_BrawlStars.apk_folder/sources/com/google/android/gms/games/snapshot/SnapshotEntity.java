package com.google.android.gms.games.snapshot;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.games.internal.zzd;
import java.util.Arrays;

public final class SnapshotEntity extends zzd implements Snapshot {
    public static final Parcelable.Creator<SnapshotEntity> CREATOR = new c();

    /* renamed from: a  reason: collision with root package name */
    private final SnapshotMetadataEntity f1842a;

    /* renamed from: b  reason: collision with root package name */
    private final zza f1843b;

    public SnapshotEntity(SnapshotMetadata snapshotMetadata, zza zza) {
        this.f1842a = new SnapshotMetadataEntity(snapshotMetadata);
        this.f1843b = zza;
    }

    public final /* bridge */ /* synthetic */ Object a() {
        return this;
    }

    public final SnapshotMetadata b() {
        return this.f1842a;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof Snapshot)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        Snapshot snapshot = (Snapshot) obj;
        return j.a(snapshot.b(), b()) && j.a(snapshot.c(), c());
    }

    public final String toString() {
        return j.a(this).a("Metadata", b()).a("HasContents", Boolean.valueOf(c() != null)).toString();
    }

    public final SnapshotContents c() {
        if (this.f1843b.f1847a == null) {
            return null;
        }
        return this.f1843b;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{b(), c()});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.snapshot.SnapshotMetadataEntity, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.snapshot.SnapshotContents, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.a(parcel, 1, (Parcelable) this.f1842a, i, false);
        a.a(parcel, 3, (Parcelable) c(), i, false);
        a.b(parcel, a2);
    }
}
