package com.google.android.gms.analytics;

import android.net.Uri;
import android.text.TextUtils;
import android.util.LogPrinter;
import com.facebook.share.internal.ShareConstants;
import java.util.ArrayList;
import java.util.Collections;

public final class i implements q {

    /* renamed from: a  reason: collision with root package name */
    private static final Uri f1326a;

    /* renamed from: b  reason: collision with root package name */
    private final LogPrinter f1327b = new LogPrinter(4, "GA/LogCatTransport");

    public final Uri a() {
        return f1326a;
    }

    public final void a(k kVar) {
        ArrayList arrayList = new ArrayList(kVar.f.values());
        Collections.sort(arrayList, new j());
        StringBuilder sb = new StringBuilder();
        ArrayList arrayList2 = arrayList;
        int size = arrayList2.size();
        int i = 0;
        while (i < size) {
            Object obj = arrayList2.get(i);
            i++;
            String obj2 = ((l) obj).toString();
            if (!TextUtils.isEmpty(obj2)) {
                if (sb.length() != 0) {
                    sb.append(", ");
                }
                sb.append(obj2);
            }
        }
        this.f1327b.println(sb.toString());
    }

    static {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme(ShareConstants.MEDIA_URI);
        builder.authority("local");
        f1326a = builder.build();
    }
}
