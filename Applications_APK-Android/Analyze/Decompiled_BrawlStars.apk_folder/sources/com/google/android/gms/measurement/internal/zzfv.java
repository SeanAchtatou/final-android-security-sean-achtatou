package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.a;

public final class zzfv extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzfv> CREATOR = new eb();

    /* renamed from: a  reason: collision with root package name */
    public final String f2599a;

    /* renamed from: b  reason: collision with root package name */
    public final long f2600b;
    public final Long c;
    public final String d;
    public final String e;
    public final Double f;
    private final int g;
    private final Float h;

    zzfv(ec ecVar) {
        this(ecVar.c, ecVar.d, ecVar.e, ecVar.f2542b);
    }

    zzfv(String str, long j, Object obj, String str2) {
        l.a(str);
        this.g = 2;
        this.f2599a = str;
        this.f2600b = j;
        this.e = str2;
        if (obj == null) {
            this.c = null;
            this.h = null;
            this.f = null;
            this.d = null;
        } else if (obj instanceof Long) {
            this.c = (Long) obj;
            this.h = null;
            this.f = null;
            this.d = null;
        } else if (obj instanceof String) {
            this.c = null;
            this.h = null;
            this.f = null;
            this.d = (String) obj;
        } else if (obj instanceof Double) {
            this.c = null;
            this.h = null;
            this.f = (Double) obj;
            this.d = null;
        } else {
            throw new IllegalArgumentException("User attribute given of un-supported type");
        }
    }

    zzfv(int i, String str, long j, Long l, Float f2, String str2, String str3, Double d2) {
        this.g = i;
        this.f2599a = str;
        this.f2600b = j;
        this.c = l;
        Double d3 = null;
        this.h = null;
        if (i == 1) {
            this.f = f2 != null ? Double.valueOf(f2.doubleValue()) : d3;
        } else {
            this.f = d2;
        }
        this.d = str2;
        this.e = str3;
    }

    public final Object a() {
        Long l = this.c;
        if (l != null) {
            return l;
        }
        Double d2 = this.f;
        if (d2 != null) {
            return d2;
        }
        String str = this.d;
        if (str != null) {
            return str;
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
     arg types: [android.os.Parcel, int, java.lang.Long, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.b(parcel, 1, this.g);
        a.a(parcel, 2, this.f2599a, false);
        a.a(parcel, 3, this.f2600b);
        a.a(parcel, 4, this.c, false);
        a.a(parcel, 6, this.d, false);
        a.a(parcel, 7, this.e, false);
        Double d2 = this.f;
        if (d2 != null) {
            a.a(parcel, 8, 8);
            parcel.writeDouble(d2.doubleValue());
        }
        a.b(parcel, a2);
    }
}
