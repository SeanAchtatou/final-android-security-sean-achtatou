package com.google.android.gms.drive.events;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.drive.DriveSpace;
import java.util.ArrayList;

public final class f implements Parcelable.Creator<zze> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zze[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        int i = 0;
        ArrayList arrayList = null;
        boolean z = false;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            int i2 = 65535 & readInt;
            if (i2 == 2) {
                i = SafeParcelReader.d(parcel, readInt);
            } else if (i2 == 3) {
                z = SafeParcelReader.c(parcel, readInt);
            } else if (i2 != 4) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                arrayList = SafeParcelReader.c(parcel, readInt, DriveSpace.CREATOR);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new zze(i, z, arrayList);
    }
}
