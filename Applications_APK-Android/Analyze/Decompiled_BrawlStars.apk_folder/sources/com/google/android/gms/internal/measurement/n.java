package com.google.android.gms.internal.measurement;

final class n implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ String f2323a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ Runnable f2324b;
    private final /* synthetic */ l c;

    n(l lVar, String str, Runnable runnable) {
        this.c = lVar;
        this.f2323a = str;
        this.f2324b = runnable;
    }

    public final void run() {
        this.c.f2320a.a(this.f2323a);
        Runnable runnable = this.f2324b;
        if (runnable != null) {
            runnable.run();
        }
    }
}
