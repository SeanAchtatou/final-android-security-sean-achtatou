package com.google.android.gms.measurement.internal;

import android.os.Bundle;

final class ci implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ boolean f2465a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ cg f2466b;
    private final /* synthetic */ cg c;
    private final /* synthetic */ ch d;

    ci(ch chVar, boolean z, cg cgVar, cg cgVar2) {
        this.d = chVar;
        this.f2465a = z;
        this.f2466b = cgVar;
        this.c = cgVar2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.ch.a(com.google.android.gms.measurement.internal.ch, com.google.android.gms.measurement.internal.cg, boolean):void
     arg types: [com.google.android.gms.measurement.internal.ch, com.google.android.gms.measurement.internal.cg, int]
     candidates:
      com.google.android.gms.measurement.internal.ch.a(com.google.android.gms.measurement.internal.cg, android.os.Bundle, boolean):void
      com.google.android.gms.measurement.internal.ch.a(android.app.Activity, com.google.android.gms.measurement.internal.cg, boolean):void
      com.google.android.gms.measurement.internal.ch.a(com.google.android.gms.measurement.internal.ch, com.google.android.gms.measurement.internal.cg, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.ch.a(com.google.android.gms.measurement.internal.cg, android.os.Bundle, boolean):void
     arg types: [com.google.android.gms.measurement.internal.cg, android.os.Bundle, int]
     candidates:
      com.google.android.gms.measurement.internal.ch.a(com.google.android.gms.measurement.internal.ch, com.google.android.gms.measurement.internal.cg, boolean):void
      com.google.android.gms.measurement.internal.ch.a(android.app.Activity, com.google.android.gms.measurement.internal.cg, boolean):void
      com.google.android.gms.measurement.internal.ch.a(com.google.android.gms.measurement.internal.cg, android.os.Bundle, boolean):void */
    public final void run() {
        boolean z;
        boolean z2 = false;
        if (this.d.s().l(this.d.f().v())) {
            z = this.f2465a && this.d.f2463a != null;
            if (z) {
                ch chVar = this.d;
                ch.a(chVar, chVar.f2463a, true);
            }
        } else {
            if (this.f2465a && this.d.f2463a != null) {
                ch chVar2 = this.d;
                ch.a(chVar2, chVar2.f2463a, true);
            }
            z = false;
        }
        cg cgVar = this.f2466b;
        if (cgVar == null || cgVar.c != this.c.c || !ed.c(this.f2466b.f2462b, this.c.f2462b) || !ed.c(this.f2466b.f2461a, this.c.f2461a)) {
            z2 = true;
        }
        if (z2) {
            Bundle bundle = new Bundle();
            ch.a(this.c, bundle, true);
            cg cgVar2 = this.f2466b;
            if (cgVar2 != null) {
                if (cgVar2.f2461a != null) {
                    bundle.putString("_pn", this.f2466b.f2461a);
                }
                bundle.putString("_pc", this.f2466b.f2462b);
                bundle.putLong("_pi", this.f2466b.c);
            }
            if (this.d.s().l(this.d.f().v()) && z) {
                long w = this.d.j().w();
                if (w > 0) {
                    this.d.o().a(bundle, w);
                }
            }
            bv e = this.d.e();
            e.c();
            e.a("auto", "_vs", e.l().a(), bundle);
        }
        ch chVar3 = this.d;
        chVar3.f2463a = this.c;
        chVar3.g().a(this.c);
    }
}
