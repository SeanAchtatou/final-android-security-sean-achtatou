package com.google.android.gms.internal.drive;

import java.util.Arrays;

final class bz {

    /* renamed from: a  reason: collision with root package name */
    final int f1909a;

    /* renamed from: b  reason: collision with root package name */
    final byte[] f1910b;

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof bz)) {
            return false;
        }
        bz bzVar = (bz) obj;
        return this.f1909a == bzVar.f1909a && Arrays.equals(this.f1910b, bzVar.f1910b);
    }

    public final int hashCode() {
        return ((this.f1909a + 527) * 31) + Arrays.hashCode(this.f1910b);
    }
}
