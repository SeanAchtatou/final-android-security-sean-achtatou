package com.google.android.gms.common.images;

import android.graphics.drawable.Drawable;
import com.google.android.gms.common.images.ImageManager;
import com.google.android.gms.common.internal.j;
import java.lang.ref.WeakReference;
import java.util.Arrays;

public final class c extends a {
    private WeakReference<ImageManager.a> c;

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.f1551a});
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof c)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        c cVar = (c) obj;
        ImageManager.a aVar = this.c.get();
        ImageManager.a aVar2 = cVar.c.get();
        return aVar2 != null && aVar != null && j.a(aVar2, aVar) && j.a(cVar.f1551a, this.f1551a);
    }

    /* access modifiers changed from: protected */
    public final void a(Drawable drawable, boolean z, boolean z2) {
        this.c.get();
    }
}
