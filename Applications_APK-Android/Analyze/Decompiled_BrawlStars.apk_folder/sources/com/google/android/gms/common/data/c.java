package com.google.android.gms.common.data;

import com.google.android.gms.common.internal.l;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class c<T> implements Iterator<T> {

    /* renamed from: a  reason: collision with root package name */
    protected final b<T> f1530a;

    /* renamed from: b  reason: collision with root package name */
    protected int f1531b = -1;

    public c(b<T> bVar) {
        this.f1530a = (b) l.a(bVar);
    }

    public boolean hasNext() {
        return this.f1531b < this.f1530a.b() - 1;
    }

    public T next() {
        if (hasNext()) {
            b<T> bVar = this.f1530a;
            int i = this.f1531b + 1;
            this.f1531b = i;
            return bVar.a(i);
        }
        int i2 = this.f1531b;
        StringBuilder sb = new StringBuilder(46);
        sb.append("Cannot advance the iterator beyond ");
        sb.append(i2);
        throw new NoSuchElementException(sb.toString());
    }

    public void remove() {
        throw new UnsupportedOperationException("Cannot remove elements from a DataBufferIterator");
    }
}
