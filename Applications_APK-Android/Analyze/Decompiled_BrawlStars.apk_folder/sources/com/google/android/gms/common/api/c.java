package com.google.android.gms.common.api;

import android.accounts.Account;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.a.d;
import com.google.android.gms.common.api.internal.ax;
import com.google.android.gms.common.api.internal.bb;
import com.google.android.gms.common.api.internal.bq;
import com.google.android.gms.common.api.internal.bs;
import com.google.android.gms.common.api.internal.c;
import com.google.android.gms.common.api.internal.d;
import com.google.android.gms.common.api.internal.k;
import com.google.android.gms.common.api.internal.zace;
import com.google.android.gms.common.internal.b;
import com.google.android.gms.common.internal.l;
import java.util.Collections;
import java.util.Set;

public class c<O extends a.d> {

    /* renamed from: a  reason: collision with root package name */
    public final a<O> f1373a;

    /* renamed from: b  reason: collision with root package name */
    public final bs<O> f1374b;
    public final Looper c;
    public final int d;
    protected final com.google.android.gms.common.api.internal.d e;
    private final Context f;
    private final O g = null;
    private final d h;
    private final k i;

    protected c(Context context, a<O> aVar, Looper looper) {
        l.a(context, "Null context is not permitted.");
        l.a(aVar, "Api must not be null.");
        l.a(looper, "Looper must not be null.");
        this.f = context.getApplicationContext();
        this.f1373a = aVar;
        this.c = looper;
        this.f1374b = new bs<>(aVar);
        this.h = new ax(this);
        this.e = com.google.android.gms.common.api.internal.d.a(this.f);
        this.d = this.e.d.getAndIncrement();
        this.i = new com.google.android.gms.common.api.internal.a();
    }

    public a.f a(Looper looper, d.a<O> aVar) {
        return this.f1373a.a().a(this.f, looper, a().a(), this.g, aVar, aVar);
    }

    private b.a a() {
        Account account;
        Set<Scope> set;
        GoogleSignInAccount a2;
        GoogleSignInAccount a3;
        b.a aVar = new b.a();
        O o = this.g;
        if (!(o instanceof a.d.b) || (a3 = ((a.d.b) o).a()) == null) {
            O o2 = this.g;
            account = o2 instanceof a.d.C0112a ? ((a.d.C0112a) o2).a() : null;
        } else {
            account = a3.a();
        }
        aVar.f1597a = account;
        O o3 = this.g;
        if (!(o3 instanceof a.d.b) || (a2 = ((a.d.b) o3).a()) == null) {
            set = Collections.emptySet();
        } else {
            set = a2.b();
        }
        b.a a4 = aVar.a(set);
        a4.c = this.f.getClass().getName();
        a4.f1598b = this.f.getPackageName();
        return a4;
    }

    public zace a(Context context, Handler handler) {
        return new zace(context, handler, a().a());
    }

    public final <A extends a.b, T extends c.a<? extends g, A>> T a(T t) {
        t.g();
        com.google.android.gms.common.api.internal.d dVar = this.e;
        dVar.i.sendMessage(dVar.i.obtainMessage(4, new bb(new bq(1, t), dVar.e.get(), this)));
        return t;
    }
}
