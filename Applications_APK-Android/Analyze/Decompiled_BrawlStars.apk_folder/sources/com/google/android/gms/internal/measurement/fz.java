package com.google.android.gms.internal.measurement;

import java.util.Map;

final class fz<K> implements Map.Entry<K, Object> {

    /* renamed from: a  reason: collision with root package name */
    Map.Entry<K, fy> f2230a;

    private fz(Map.Entry<K, fy> entry) {
        this.f2230a = entry;
    }

    public final K getKey() {
        return this.f2230a.getKey();
    }

    public final Object getValue() {
        if (this.f2230a.getValue() == null) {
            return null;
        }
        return fy.a();
    }

    public final Object setValue(Object obj) {
        if (obj instanceof gt) {
            fy value = this.f2230a.getValue();
            gt gtVar = value.f2235b;
            value.f2234a = null;
            value.c = null;
            value.f2235b = (gt) obj;
            return gtVar;
        }
        throw new IllegalArgumentException("LazyField now only used for MessageSet, and the value of MessageSet must be an instance of MessageLite");
    }

    /* synthetic */ fz(Map.Entry entry, byte b2) {
        this(entry);
    }
}
