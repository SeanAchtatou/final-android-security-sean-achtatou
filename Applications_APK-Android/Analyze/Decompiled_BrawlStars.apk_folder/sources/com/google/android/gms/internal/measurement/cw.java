package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.ce;
import com.supercell.id.view.AvatarView;
import java.io.IOException;

public final class cw extends iz<cw> {
    private static volatile cw[] N;
    public cr[] A = cr.a();
    public String B = null;
    public Integer C = null;
    public String D = null;
    public Long E = null;
    public Long F = null;
    public String G = null;
    public Integer H = null;
    public String I = null;
    public ce.b J = null;
    public int[] K = jh.f2315a;
    private Integer O = null;
    private Integer P = null;
    private String Q = null;
    private Long R = null;

    /* renamed from: a  reason: collision with root package name */
    public Integer f2141a = null;

    /* renamed from: b  reason: collision with root package name */
    public ct[] f2142b = ct.a();
    public cz[] c = cz.a();
    public Long d = null;
    public Long e = null;
    public Long f = null;
    public Long g = null;
    public Long h = null;
    public String i = null;
    public String j = null;
    public String k = null;
    public String l = null;
    public Integer m = null;
    public String n = null;
    public String o = null;
    public String p = null;
    public Long q = null;
    public Long r = null;
    public String s = null;
    public Boolean t = null;
    public String u = null;
    public Long v = null;
    public Integer w = null;
    public String x = null;
    public String y = null;
    public Boolean z = null;

    public static cw[] a() {
        if (N == null) {
            synchronized (jd.f2312b) {
                if (N == null) {
                    N = new cw[0];
                }
            }
        }
        return N;
    }

    public cw() {
        this.L = null;
        this.M = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof cw)) {
            return false;
        }
        cw cwVar = (cw) obj;
        Integer num = this.f2141a;
        if (num == null) {
            if (cwVar.f2141a != null) {
                return false;
            }
        } else if (!num.equals(cwVar.f2141a)) {
            return false;
        }
        if (!jd.a(this.f2142b, cwVar.f2142b) || !jd.a(this.c, cwVar.c)) {
            return false;
        }
        Long l2 = this.d;
        if (l2 == null) {
            if (cwVar.d != null) {
                return false;
            }
        } else if (!l2.equals(cwVar.d)) {
            return false;
        }
        Long l3 = this.e;
        if (l3 == null) {
            if (cwVar.e != null) {
                return false;
            }
        } else if (!l3.equals(cwVar.e)) {
            return false;
        }
        Long l4 = this.f;
        if (l4 == null) {
            if (cwVar.f != null) {
                return false;
            }
        } else if (!l4.equals(cwVar.f)) {
            return false;
        }
        Long l5 = this.g;
        if (l5 == null) {
            if (cwVar.g != null) {
                return false;
            }
        } else if (!l5.equals(cwVar.g)) {
            return false;
        }
        Long l6 = this.h;
        if (l6 == null) {
            if (cwVar.h != null) {
                return false;
            }
        } else if (!l6.equals(cwVar.h)) {
            return false;
        }
        String str = this.i;
        if (str == null) {
            if (cwVar.i != null) {
                return false;
            }
        } else if (!str.equals(cwVar.i)) {
            return false;
        }
        String str2 = this.j;
        if (str2 == null) {
            if (cwVar.j != null) {
                return false;
            }
        } else if (!str2.equals(cwVar.j)) {
            return false;
        }
        String str3 = this.k;
        if (str3 == null) {
            if (cwVar.k != null) {
                return false;
            }
        } else if (!str3.equals(cwVar.k)) {
            return false;
        }
        String str4 = this.l;
        if (str4 == null) {
            if (cwVar.l != null) {
                return false;
            }
        } else if (!str4.equals(cwVar.l)) {
            return false;
        }
        Integer num2 = this.m;
        if (num2 == null) {
            if (cwVar.m != null) {
                return false;
            }
        } else if (!num2.equals(cwVar.m)) {
            return false;
        }
        String str5 = this.n;
        if (str5 == null) {
            if (cwVar.n != null) {
                return false;
            }
        } else if (!str5.equals(cwVar.n)) {
            return false;
        }
        String str6 = this.o;
        if (str6 == null) {
            if (cwVar.o != null) {
                return false;
            }
        } else if (!str6.equals(cwVar.o)) {
            return false;
        }
        String str7 = this.p;
        if (str7 == null) {
            if (cwVar.p != null) {
                return false;
            }
        } else if (!str7.equals(cwVar.p)) {
            return false;
        }
        Long l7 = this.q;
        if (l7 == null) {
            if (cwVar.q != null) {
                return false;
            }
        } else if (!l7.equals(cwVar.q)) {
            return false;
        }
        Long l8 = this.r;
        if (l8 == null) {
            if (cwVar.r != null) {
                return false;
            }
        } else if (!l8.equals(cwVar.r)) {
            return false;
        }
        String str8 = this.s;
        if (str8 == null) {
            if (cwVar.s != null) {
                return false;
            }
        } else if (!str8.equals(cwVar.s)) {
            return false;
        }
        Boolean bool = this.t;
        if (bool == null) {
            if (cwVar.t != null) {
                return false;
            }
        } else if (!bool.equals(cwVar.t)) {
            return false;
        }
        String str9 = this.u;
        if (str9 == null) {
            if (cwVar.u != null) {
                return false;
            }
        } else if (!str9.equals(cwVar.u)) {
            return false;
        }
        Long l9 = this.v;
        if (l9 == null) {
            if (cwVar.v != null) {
                return false;
            }
        } else if (!l9.equals(cwVar.v)) {
            return false;
        }
        Integer num3 = this.w;
        if (num3 == null) {
            if (cwVar.w != null) {
                return false;
            }
        } else if (!num3.equals(cwVar.w)) {
            return false;
        }
        String str10 = this.x;
        if (str10 == null) {
            if (cwVar.x != null) {
                return false;
            }
        } else if (!str10.equals(cwVar.x)) {
            return false;
        }
        String str11 = this.y;
        if (str11 == null) {
            if (cwVar.y != null) {
                return false;
            }
        } else if (!str11.equals(cwVar.y)) {
            return false;
        }
        Boolean bool2 = this.z;
        if (bool2 == null) {
            if (cwVar.z != null) {
                return false;
            }
        } else if (!bool2.equals(cwVar.z)) {
            return false;
        }
        if (!jd.a(this.A, cwVar.A)) {
            return false;
        }
        String str12 = this.B;
        if (str12 == null) {
            if (cwVar.B != null) {
                return false;
            }
        } else if (!str12.equals(cwVar.B)) {
            return false;
        }
        Integer num4 = this.C;
        if (num4 == null) {
            if (cwVar.C != null) {
                return false;
            }
        } else if (!num4.equals(cwVar.C)) {
            return false;
        }
        Integer num5 = this.O;
        if (num5 == null) {
            if (cwVar.O != null) {
                return false;
            }
        } else if (!num5.equals(cwVar.O)) {
            return false;
        }
        Integer num6 = this.P;
        if (num6 == null) {
            if (cwVar.P != null) {
                return false;
            }
        } else if (!num6.equals(cwVar.P)) {
            return false;
        }
        String str13 = this.D;
        if (str13 == null) {
            if (cwVar.D != null) {
                return false;
            }
        } else if (!str13.equals(cwVar.D)) {
            return false;
        }
        Long l10 = this.E;
        if (l10 == null) {
            if (cwVar.E != null) {
                return false;
            }
        } else if (!l10.equals(cwVar.E)) {
            return false;
        }
        Long l11 = this.F;
        if (l11 == null) {
            if (cwVar.F != null) {
                return false;
            }
        } else if (!l11.equals(cwVar.F)) {
            return false;
        }
        String str14 = this.G;
        if (str14 == null) {
            if (cwVar.G != null) {
                return false;
            }
        } else if (!str14.equals(cwVar.G)) {
            return false;
        }
        String str15 = this.Q;
        if (str15 == null) {
            if (cwVar.Q != null) {
                return false;
            }
        } else if (!str15.equals(cwVar.Q)) {
            return false;
        }
        Integer num7 = this.H;
        if (num7 == null) {
            if (cwVar.H != null) {
                return false;
            }
        } else if (!num7.equals(cwVar.H)) {
            return false;
        }
        String str16 = this.I;
        if (str16 == null) {
            if (cwVar.I != null) {
                return false;
            }
        } else if (!str16.equals(cwVar.I)) {
            return false;
        }
        ce.b bVar = this.J;
        if (bVar == null) {
            if (cwVar.J != null) {
                return false;
            }
        } else if (!bVar.equals(cwVar.J)) {
            return false;
        }
        if (!jd.a(this.K, cwVar.K)) {
            return false;
        }
        Long l12 = this.R;
        if (l12 == null) {
            if (cwVar.R != null) {
                return false;
            }
        } else if (!l12.equals(cwVar.R)) {
            return false;
        }
        if (this.L == null || this.L.a()) {
            return cwVar.L == null || cwVar.L.a();
        }
        return this.L.equals(cwVar.L);
    }

    public final int hashCode() {
        int i2;
        int hashCode = (getClass().getName().hashCode() + 527) * 31;
        Integer num = this.f2141a;
        int i3 = 0;
        int hashCode2 = (((((hashCode + (num == null ? 0 : num.hashCode())) * 31) + jd.a(this.f2142b)) * 31) + jd.a(this.c)) * 31;
        Long l2 = this.d;
        int hashCode3 = (hashCode2 + (l2 == null ? 0 : l2.hashCode())) * 31;
        Long l3 = this.e;
        int hashCode4 = (hashCode3 + (l3 == null ? 0 : l3.hashCode())) * 31;
        Long l4 = this.f;
        int hashCode5 = (hashCode4 + (l4 == null ? 0 : l4.hashCode())) * 31;
        Long l5 = this.g;
        int hashCode6 = (hashCode5 + (l5 == null ? 0 : l5.hashCode())) * 31;
        Long l6 = this.h;
        int hashCode7 = (hashCode6 + (l6 == null ? 0 : l6.hashCode())) * 31;
        String str = this.i;
        int hashCode8 = (hashCode7 + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.j;
        int hashCode9 = (hashCode8 + (str2 == null ? 0 : str2.hashCode())) * 31;
        String str3 = this.k;
        int hashCode10 = (hashCode9 + (str3 == null ? 0 : str3.hashCode())) * 31;
        String str4 = this.l;
        int hashCode11 = (hashCode10 + (str4 == null ? 0 : str4.hashCode())) * 31;
        Integer num2 = this.m;
        int hashCode12 = (hashCode11 + (num2 == null ? 0 : num2.hashCode())) * 31;
        String str5 = this.n;
        int hashCode13 = (hashCode12 + (str5 == null ? 0 : str5.hashCode())) * 31;
        String str6 = this.o;
        int hashCode14 = (hashCode13 + (str6 == null ? 0 : str6.hashCode())) * 31;
        String str7 = this.p;
        int hashCode15 = (hashCode14 + (str7 == null ? 0 : str7.hashCode())) * 31;
        Long l7 = this.q;
        int hashCode16 = (hashCode15 + (l7 == null ? 0 : l7.hashCode())) * 31;
        Long l8 = this.r;
        int hashCode17 = (hashCode16 + (l8 == null ? 0 : l8.hashCode())) * 31;
        String str8 = this.s;
        int hashCode18 = (hashCode17 + (str8 == null ? 0 : str8.hashCode())) * 31;
        Boolean bool = this.t;
        int hashCode19 = (hashCode18 + (bool == null ? 0 : bool.hashCode())) * 31;
        String str9 = this.u;
        int hashCode20 = (hashCode19 + (str9 == null ? 0 : str9.hashCode())) * 31;
        Long l9 = this.v;
        int hashCode21 = (hashCode20 + (l9 == null ? 0 : l9.hashCode())) * 31;
        Integer num3 = this.w;
        int hashCode22 = (hashCode21 + (num3 == null ? 0 : num3.hashCode())) * 31;
        String str10 = this.x;
        int hashCode23 = (hashCode22 + (str10 == null ? 0 : str10.hashCode())) * 31;
        String str11 = this.y;
        int hashCode24 = (hashCode23 + (str11 == null ? 0 : str11.hashCode())) * 31;
        Boolean bool2 = this.z;
        int hashCode25 = (((hashCode24 + (bool2 == null ? 0 : bool2.hashCode())) * 31) + jd.a(this.A)) * 31;
        String str12 = this.B;
        int hashCode26 = (hashCode25 + (str12 == null ? 0 : str12.hashCode())) * 31;
        Integer num4 = this.C;
        int hashCode27 = (hashCode26 + (num4 == null ? 0 : num4.hashCode())) * 31;
        Integer num5 = this.O;
        int hashCode28 = (hashCode27 + (num5 == null ? 0 : num5.hashCode())) * 31;
        Integer num6 = this.P;
        int hashCode29 = (hashCode28 + (num6 == null ? 0 : num6.hashCode())) * 31;
        String str13 = this.D;
        int hashCode30 = (hashCode29 + (str13 == null ? 0 : str13.hashCode())) * 31;
        Long l10 = this.E;
        int hashCode31 = (hashCode30 + (l10 == null ? 0 : l10.hashCode())) * 31;
        Long l11 = this.F;
        int hashCode32 = (hashCode31 + (l11 == null ? 0 : l11.hashCode())) * 31;
        String str14 = this.G;
        int hashCode33 = (hashCode32 + (str14 == null ? 0 : str14.hashCode())) * 31;
        String str15 = this.Q;
        int hashCode34 = (hashCode33 + (str15 == null ? 0 : str15.hashCode())) * 31;
        Integer num7 = this.H;
        int hashCode35 = (hashCode34 + (num7 == null ? 0 : num7.hashCode())) * 31;
        String str16 = this.I;
        int hashCode36 = hashCode35 + (str16 == null ? 0 : str16.hashCode());
        ce.b bVar = this.J;
        int i4 = hashCode36 * 31;
        if (bVar == null) {
            i2 = 0;
        } else {
            i2 = bVar.hashCode();
        }
        int a2 = (((i4 + i2) * 31) + jd.a(this.K)) * 31;
        Long l12 = this.R;
        int hashCode37 = (a2 + (l12 == null ? 0 : l12.hashCode())) * 31;
        if (this.L != null && !this.L.a()) {
            i3 = this.L.hashCode();
        }
        return hashCode37 + i3;
    }

    public final void a(iy iyVar) throws IOException {
        Integer num = this.f2141a;
        if (num != null) {
            iyVar.a(1, num.intValue());
        }
        ct[] ctVarArr = this.f2142b;
        if (ctVarArr != null && ctVarArr.length > 0) {
            int i2 = 0;
            while (true) {
                ct[] ctVarArr2 = this.f2142b;
                if (i2 >= ctVarArr2.length) {
                    break;
                }
                ct ctVar = ctVarArr2[i2];
                if (ctVar != null) {
                    iyVar.a(2, ctVar);
                }
                i2++;
            }
        }
        cz[] czVarArr = this.c;
        if (czVarArr != null && czVarArr.length > 0) {
            int i3 = 0;
            while (true) {
                cz[] czVarArr2 = this.c;
                if (i3 >= czVarArr2.length) {
                    break;
                }
                cz czVar = czVarArr2[i3];
                if (czVar != null) {
                    iyVar.a(3, czVar);
                }
                i3++;
            }
        }
        Long l2 = this.d;
        if (l2 != null) {
            iyVar.b(4, l2.longValue());
        }
        Long l3 = this.e;
        if (l3 != null) {
            iyVar.b(5, l3.longValue());
        }
        Long l4 = this.f;
        if (l4 != null) {
            iyVar.b(6, l4.longValue());
        }
        Long l5 = this.h;
        if (l5 != null) {
            iyVar.b(7, l5.longValue());
        }
        String str = this.i;
        if (str != null) {
            iyVar.a(8, str);
        }
        String str2 = this.j;
        if (str2 != null) {
            iyVar.a(9, str2);
        }
        String str3 = this.k;
        if (str3 != null) {
            iyVar.a(10, str3);
        }
        String str4 = this.l;
        if (str4 != null) {
            iyVar.a(11, str4);
        }
        Integer num2 = this.m;
        if (num2 != null) {
            iyVar.a(12, num2.intValue());
        }
        String str5 = this.n;
        if (str5 != null) {
            iyVar.a(13, str5);
        }
        String str6 = this.o;
        if (str6 != null) {
            iyVar.a(14, str6);
        }
        String str7 = this.p;
        if (str7 != null) {
            iyVar.a(16, str7);
        }
        Long l6 = this.q;
        if (l6 != null) {
            iyVar.b(17, l6.longValue());
        }
        Long l7 = this.r;
        if (l7 != null) {
            iyVar.b(18, l7.longValue());
        }
        String str8 = this.s;
        if (str8 != null) {
            iyVar.a(19, str8);
        }
        Boolean bool = this.t;
        if (bool != null) {
            iyVar.a(20, bool.booleanValue());
        }
        String str9 = this.u;
        if (str9 != null) {
            iyVar.a(21, str9);
        }
        Long l8 = this.v;
        if (l8 != null) {
            iyVar.b(22, l8.longValue());
        }
        Integer num3 = this.w;
        if (num3 != null) {
            iyVar.a(23, num3.intValue());
        }
        String str10 = this.x;
        if (str10 != null) {
            iyVar.a(24, str10);
        }
        String str11 = this.y;
        if (str11 != null) {
            iyVar.a(25, str11);
        }
        Long l9 = this.g;
        if (l9 != null) {
            iyVar.b(26, l9.longValue());
        }
        Boolean bool2 = this.z;
        if (bool2 != null) {
            iyVar.a(28, bool2.booleanValue());
        }
        cr[] crVarArr = this.A;
        if (crVarArr != null && crVarArr.length > 0) {
            int i4 = 0;
            while (true) {
                cr[] crVarArr2 = this.A;
                if (i4 >= crVarArr2.length) {
                    break;
                }
                cr crVar = crVarArr2[i4];
                if (crVar != null) {
                    iyVar.a(29, crVar);
                }
                i4++;
            }
        }
        String str12 = this.B;
        if (str12 != null) {
            iyVar.a(30, str12);
        }
        Integer num4 = this.C;
        if (num4 != null) {
            iyVar.a(31, num4.intValue());
        }
        Integer num5 = this.O;
        if (num5 != null) {
            iyVar.a(32, num5.intValue());
        }
        Integer num6 = this.P;
        if (num6 != null) {
            iyVar.a(33, num6.intValue());
        }
        String str13 = this.D;
        if (str13 != null) {
            iyVar.a(34, str13);
        }
        Long l10 = this.E;
        if (l10 != null) {
            iyVar.b(35, l10.longValue());
        }
        Long l11 = this.F;
        if (l11 != null) {
            iyVar.b(36, l11.longValue());
        }
        String str14 = this.G;
        if (str14 != null) {
            iyVar.a(37, str14);
        }
        String str15 = this.Q;
        if (str15 != null) {
            iyVar.a(38, str15);
        }
        Integer num7 = this.H;
        if (num7 != null) {
            iyVar.a(39, num7.intValue());
        }
        String str16 = this.I;
        if (str16 != null) {
            iyVar.a(41, str16);
        }
        ce.b bVar = this.J;
        if (bVar != null) {
            if (iyVar.f2302b == null) {
                iyVar.f2302b = zztv.a(iyVar.f2301a);
                iyVar.c = iyVar.f2301a.position();
            } else if (iyVar.c != iyVar.f2301a.position()) {
                iyVar.f2302b.b(iyVar.f2301a.array(), iyVar.c, iyVar.f2301a.position() - iyVar.c);
                iyVar.c = iyVar.f2301a.position();
            }
            zztv zztv = iyVar.f2302b;
            zztv.a(44, bVar);
            zztv.h();
            iyVar.c = iyVar.f2301a.position();
        }
        int[] iArr = this.K;
        if (iArr != null && iArr.length > 0) {
            int i5 = 0;
            while (true) {
                int[] iArr2 = this.K;
                if (i5 >= iArr2.length) {
                    break;
                }
                int i6 = iArr2[i5];
                iyVar.c(45, 0);
                iyVar.b(i6);
                i5++;
            }
        }
        Long l12 = this.R;
        if (l12 != null) {
            iyVar.b(46, l12.longValue());
        }
        super.a(iyVar);
    }

    /* access modifiers changed from: protected */
    public final int b() {
        int[] iArr;
        int b2 = super.b();
        Integer num = this.f2141a;
        if (num != null) {
            b2 += iy.b(1, num.intValue());
        }
        ct[] ctVarArr = this.f2142b;
        int i2 = 0;
        if (ctVarArr != null && ctVarArr.length > 0) {
            int i3 = b2;
            int i4 = 0;
            while (true) {
                ct[] ctVarArr2 = this.f2142b;
                if (i4 >= ctVarArr2.length) {
                    break;
                }
                ct ctVar = ctVarArr2[i4];
                if (ctVar != null) {
                    i3 += iy.b(2, ctVar);
                }
                i4++;
            }
            b2 = i3;
        }
        cz[] czVarArr = this.c;
        if (czVarArr != null && czVarArr.length > 0) {
            int i5 = b2;
            int i6 = 0;
            while (true) {
                cz[] czVarArr2 = this.c;
                if (i6 >= czVarArr2.length) {
                    break;
                }
                cz czVar = czVarArr2[i6];
                if (czVar != null) {
                    i5 += iy.b(3, czVar);
                }
                i6++;
            }
            b2 = i5;
        }
        Long l2 = this.d;
        if (l2 != null) {
            b2 += iy.c(4, l2.longValue());
        }
        Long l3 = this.e;
        if (l3 != null) {
            b2 += iy.c(5, l3.longValue());
        }
        Long l4 = this.f;
        if (l4 != null) {
            b2 += iy.c(6, l4.longValue());
        }
        Long l5 = this.h;
        if (l5 != null) {
            b2 += iy.c(7, l5.longValue());
        }
        String str = this.i;
        if (str != null) {
            b2 += iy.b(8, str);
        }
        String str2 = this.j;
        if (str2 != null) {
            b2 += iy.b(9, str2);
        }
        String str3 = this.k;
        if (str3 != null) {
            b2 += iy.b(10, str3);
        }
        String str4 = this.l;
        if (str4 != null) {
            b2 += iy.b(11, str4);
        }
        Integer num2 = this.m;
        if (num2 != null) {
            b2 += iy.b(12, num2.intValue());
        }
        String str5 = this.n;
        if (str5 != null) {
            b2 += iy.b(13, str5);
        }
        String str6 = this.o;
        if (str6 != null) {
            b2 += iy.b(14, str6);
        }
        String str7 = this.p;
        if (str7 != null) {
            b2 += iy.b(16, str7);
        }
        Long l6 = this.q;
        if (l6 != null) {
            b2 += iy.c(17, l6.longValue());
        }
        Long l7 = this.r;
        if (l7 != null) {
            b2 += iy.c(18, l7.longValue());
        }
        String str8 = this.s;
        if (str8 != null) {
            b2 += iy.b(19, str8);
        }
        Boolean bool = this.t;
        if (bool != null) {
            bool.booleanValue();
            b2 += iy.c(AvatarView.INTRINSIC_POINT_SIZE) + 1;
        }
        String str9 = this.u;
        if (str9 != null) {
            b2 += iy.b(21, str9);
        }
        Long l8 = this.v;
        if (l8 != null) {
            b2 += iy.c(22, l8.longValue());
        }
        Integer num3 = this.w;
        if (num3 != null) {
            b2 += iy.b(23, num3.intValue());
        }
        String str10 = this.x;
        if (str10 != null) {
            b2 += iy.b(24, str10);
        }
        String str11 = this.y;
        if (str11 != null) {
            b2 += iy.b(25, str11);
        }
        Long l9 = this.g;
        if (l9 != null) {
            b2 += iy.c(26, l9.longValue());
        }
        Boolean bool2 = this.z;
        if (bool2 != null) {
            bool2.booleanValue();
            b2 += iy.c(224) + 1;
        }
        cr[] crVarArr = this.A;
        if (crVarArr != null && crVarArr.length > 0) {
            int i7 = b2;
            int i8 = 0;
            while (true) {
                cr[] crVarArr2 = this.A;
                if (i8 >= crVarArr2.length) {
                    break;
                }
                cr crVar = crVarArr2[i8];
                if (crVar != null) {
                    i7 += iy.b(29, crVar);
                }
                i8++;
            }
            b2 = i7;
        }
        String str12 = this.B;
        if (str12 != null) {
            b2 += iy.b(30, str12);
        }
        Integer num4 = this.C;
        if (num4 != null) {
            b2 += iy.b(31, num4.intValue());
        }
        Integer num5 = this.O;
        if (num5 != null) {
            b2 += iy.b(32, num5.intValue());
        }
        Integer num6 = this.P;
        if (num6 != null) {
            b2 += iy.b(33, num6.intValue());
        }
        String str13 = this.D;
        if (str13 != null) {
            b2 += iy.b(34, str13);
        }
        Long l10 = this.E;
        if (l10 != null) {
            b2 += iy.c(35, l10.longValue());
        }
        Long l11 = this.F;
        if (l11 != null) {
            b2 += iy.c(36, l11.longValue());
        }
        String str14 = this.G;
        if (str14 != null) {
            b2 += iy.b(37, str14);
        }
        String str15 = this.Q;
        if (str15 != null) {
            b2 += iy.b(38, str15);
        }
        Integer num7 = this.H;
        if (num7 != null) {
            b2 += iy.b(39, num7.intValue());
        }
        String str16 = this.I;
        if (str16 != null) {
            b2 += iy.b(41, str16);
        }
        ce.b bVar = this.J;
        if (bVar != null) {
            b2 += zztv.c(44, bVar);
        }
        int[] iArr2 = this.K;
        if (iArr2 != null && iArr2.length > 0) {
            int i9 = 0;
            while (true) {
                iArr = this.K;
                if (i2 >= iArr.length) {
                    break;
                }
                i9 += iy.c(iArr[i2]);
                i2++;
            }
            b2 = b2 + i9 + (iArr.length * 2);
        }
        Long l12 = this.R;
        return l12 != null ? b2 + iy.c(46, l12.longValue()) : b2;
    }

    public final /* synthetic */ je a(iw iwVar) throws IOException {
        while (true) {
            int a2 = iwVar.a();
            switch (a2) {
                case 0:
                    return this;
                case 8:
                    this.f2141a = Integer.valueOf(iwVar.d());
                    break;
                case 18:
                    int a3 = jh.a(iwVar, 18);
                    ct[] ctVarArr = this.f2142b;
                    int length = ctVarArr == null ? 0 : ctVarArr.length;
                    ct[] ctVarArr2 = new ct[(a3 + length)];
                    if (length != 0) {
                        System.arraycopy(this.f2142b, 0, ctVarArr2, 0, length);
                    }
                    while (length < ctVarArr2.length - 1) {
                        ctVarArr2[length] = new ct();
                        iwVar.a(ctVarArr2[length]);
                        iwVar.a();
                        length++;
                    }
                    ctVarArr2[length] = new ct();
                    iwVar.a(ctVarArr2[length]);
                    this.f2142b = ctVarArr2;
                    break;
                case 26:
                    int a4 = jh.a(iwVar, 26);
                    cz[] czVarArr = this.c;
                    int length2 = czVarArr == null ? 0 : czVarArr.length;
                    cz[] czVarArr2 = new cz[(a4 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.c, 0, czVarArr2, 0, length2);
                    }
                    while (length2 < czVarArr2.length - 1) {
                        czVarArr2[length2] = new cz();
                        iwVar.a(czVarArr2[length2]);
                        iwVar.a();
                        length2++;
                    }
                    czVarArr2[length2] = new cz();
                    iwVar.a(czVarArr2[length2]);
                    this.c = czVarArr2;
                    break;
                case 32:
                    this.d = Long.valueOf(iwVar.e());
                    break;
                case 40:
                    this.e = Long.valueOf(iwVar.e());
                    break;
                case 48:
                    this.f = Long.valueOf(iwVar.e());
                    break;
                case 56:
                    this.h = Long.valueOf(iwVar.e());
                    break;
                case 66:
                    this.i = iwVar.c();
                    break;
                case 74:
                    this.j = iwVar.c();
                    break;
                case 82:
                    this.k = iwVar.c();
                    break;
                case 90:
                    this.l = iwVar.c();
                    break;
                case 96:
                    this.m = Integer.valueOf(iwVar.d());
                    break;
                case 106:
                    this.n = iwVar.c();
                    break;
                case 114:
                    this.o = iwVar.c();
                    break;
                case 130:
                    this.p = iwVar.c();
                    break;
                case 136:
                    this.q = Long.valueOf(iwVar.e());
                    break;
                case 144:
                    this.r = Long.valueOf(iwVar.e());
                    break;
                case 154:
                    this.s = iwVar.c();
                    break;
                case AvatarView.INTRINSIC_POINT_SIZE /*160*/:
                    this.t = Boolean.valueOf(iwVar.b());
                    break;
                case 170:
                    this.u = iwVar.c();
                    break;
                case 176:
                    this.v = Long.valueOf(iwVar.e());
                    break;
                case 184:
                    this.w = Integer.valueOf(iwVar.d());
                    break;
                case 194:
                    this.x = iwVar.c();
                    break;
                case 202:
                    this.y = iwVar.c();
                    break;
                case 208:
                    this.g = Long.valueOf(iwVar.e());
                    break;
                case 224:
                    this.z = Boolean.valueOf(iwVar.b());
                    break;
                case 234:
                    int a5 = jh.a(iwVar, 234);
                    cr[] crVarArr = this.A;
                    int length3 = crVarArr == null ? 0 : crVarArr.length;
                    cr[] crVarArr2 = new cr[(a5 + length3)];
                    if (length3 != 0) {
                        System.arraycopy(this.A, 0, crVarArr2, 0, length3);
                    }
                    while (length3 < crVarArr2.length - 1) {
                        crVarArr2[length3] = new cr();
                        iwVar.a(crVarArr2[length3]);
                        iwVar.a();
                        length3++;
                    }
                    crVarArr2[length3] = new cr();
                    iwVar.a(crVarArr2[length3]);
                    this.A = crVarArr2;
                    break;
                case 242:
                    this.B = iwVar.c();
                    break;
                case 248:
                    this.C = Integer.valueOf(iwVar.d());
                    break;
                case 256:
                    this.O = Integer.valueOf(iwVar.d());
                    break;
                case 264:
                    this.P = Integer.valueOf(iwVar.d());
                    break;
                case 274:
                    this.D = iwVar.c();
                    break;
                case 280:
                    this.E = Long.valueOf(iwVar.e());
                    break;
                case 288:
                    this.F = Long.valueOf(iwVar.e());
                    break;
                case 298:
                    this.G = iwVar.c();
                    break;
                case 306:
                    this.Q = iwVar.c();
                    break;
                case 312:
                    this.H = Integer.valueOf(iwVar.d());
                    break;
                case 330:
                    this.I = iwVar.c();
                    break;
                case 354:
                    ce.b bVar = (ce.b) iwVar.a(ce.b.b());
                    ce.b bVar2 = this.J;
                    if (bVar2 != null) {
                        bVar = (ce.b) ((fq) ((ce.b.a) ((ce.b.a) bVar2.g()).a((fq) bVar)).d());
                    }
                    this.J = bVar;
                    break;
                case 360:
                    int a6 = jh.a(iwVar, 360);
                    int[] iArr = this.K;
                    int length4 = iArr == null ? 0 : iArr.length;
                    int[] iArr2 = new int[(a6 + length4)];
                    if (length4 != 0) {
                        System.arraycopy(this.K, 0, iArr2, 0, length4);
                    }
                    while (length4 < iArr2.length - 1) {
                        iArr2[length4] = iwVar.d();
                        iwVar.a();
                        length4++;
                    }
                    iArr2[length4] = iwVar.d();
                    this.K = iArr2;
                    break;
                case 362:
                    int c2 = iwVar.c(iwVar.d());
                    int i2 = iwVar.i();
                    int i3 = 0;
                    while (iwVar.h() > 0) {
                        iwVar.d();
                        i3++;
                    }
                    iwVar.e(i2);
                    int[] iArr3 = this.K;
                    int length5 = iArr3 == null ? 0 : iArr3.length;
                    int[] iArr4 = new int[(i3 + length5)];
                    if (length5 != 0) {
                        System.arraycopy(this.K, 0, iArr4, 0, length5);
                    }
                    while (length5 < iArr4.length) {
                        iArr4[length5] = iwVar.d();
                        length5++;
                    }
                    this.K = iArr4;
                    iwVar.d(c2);
                    break;
                case 368:
                    this.R = Long.valueOf(iwVar.e());
                    break;
                default:
                    if (super.a(iwVar, a2)) {
                        break;
                    } else {
                        return this;
                    }
            }
        }
    }
}
