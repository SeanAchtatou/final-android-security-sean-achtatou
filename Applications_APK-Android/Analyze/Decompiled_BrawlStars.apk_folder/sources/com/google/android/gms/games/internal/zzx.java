package com.google.android.gms.games.internal;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.internal.games.i;
import com.google.android.gms.internal.games.zzb;

public abstract class zzx extends zzb implements zzw {
    public zzx() {
        super("com.google.android.gms.games.internal.IGamesClient");
    }

    public final boolean a(int i, Parcel parcel, Parcel parcel2) throws RemoteException {
        if (i != 1001) {
            return false;
        }
        zzaa a2 = a();
        parcel2.writeNoException();
        i.b(parcel2, a2);
        return true;
    }
}
