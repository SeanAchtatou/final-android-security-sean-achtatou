package com.google.android.gms.common.c;

import android.content.Context;

public class c {

    /* renamed from: b  reason: collision with root package name */
    private static c f1519b = new c();

    /* renamed from: a  reason: collision with root package name */
    private b f1520a = null;

    private final synchronized b b(Context context) {
        if (this.f1520a == null) {
            if (context.getApplicationContext() != null) {
                context = context.getApplicationContext();
            }
            this.f1520a = new b(context);
        }
        return this.f1520a;
    }

    public static b a(Context context) {
        return f1519b.b(context);
    }
}
