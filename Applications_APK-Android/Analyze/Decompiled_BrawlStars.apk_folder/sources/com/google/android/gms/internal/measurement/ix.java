package com.google.android.gms.internal.measurement;

import android.text.TextUtils;
import com.facebook.share.internal.ShareConstants;
import com.google.android.gms.analytics.l;
import java.util.HashMap;

public final class ix extends l<ix> {

    /* renamed from: a  reason: collision with root package name */
    public String f2299a;

    /* renamed from: b  reason: collision with root package name */
    public String f2300b;
    public String c;
    public String d;
    public String e;
    public String f;
    public String g;
    public String h;
    public String i;
    public String j;

    public final String toString() {
        HashMap hashMap = new HashMap();
        hashMap.put("name", this.f2299a);
        hashMap.put(ShareConstants.FEED_SOURCE_PARAM, this.f2300b);
        hashMap.put("medium", this.c);
        hashMap.put("keyword", this.d);
        hashMap.put("content", this.e);
        hashMap.put("id", this.f);
        hashMap.put("adNetworkId", this.g);
        hashMap.put("gclid", this.h);
        hashMap.put("dclid", this.i);
        hashMap.put("aclid", this.j);
        return a((Object) hashMap);
    }

    public final /* synthetic */ void a(l lVar) {
        ix ixVar = (ix) lVar;
        if (!TextUtils.isEmpty(this.f2299a)) {
            ixVar.f2299a = this.f2299a;
        }
        if (!TextUtils.isEmpty(this.f2300b)) {
            ixVar.f2300b = this.f2300b;
        }
        if (!TextUtils.isEmpty(this.c)) {
            ixVar.c = this.c;
        }
        if (!TextUtils.isEmpty(this.d)) {
            ixVar.d = this.d;
        }
        if (!TextUtils.isEmpty(this.e)) {
            ixVar.e = this.e;
        }
        if (!TextUtils.isEmpty(this.f)) {
            ixVar.f = this.f;
        }
        if (!TextUtils.isEmpty(this.g)) {
            ixVar.g = this.g;
        }
        if (!TextUtils.isEmpty(this.h)) {
            ixVar.h = this.h;
        }
        if (!TextUtils.isEmpty(this.i)) {
            ixVar.i = this.i;
        }
        if (!TextUtils.isEmpty(this.j)) {
            ixVar.j = this.j;
        }
    }
}
