package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class n implements Parcelable.Creator<zzx> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zzx[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        String str = null;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            if ((65535 & readInt) != 1) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                str = SafeParcelReader.l(parcel, readInt);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new zzx(str);
    }
}
