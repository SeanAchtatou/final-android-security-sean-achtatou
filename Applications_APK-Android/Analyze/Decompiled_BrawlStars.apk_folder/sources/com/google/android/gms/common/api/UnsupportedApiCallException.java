package com.google.android.gms.common.api;

import com.google.android.gms.common.Feature;

public final class UnsupportedApiCallException extends UnsupportedOperationException {

    /* renamed from: a  reason: collision with root package name */
    private final Feature f1370a;

    public UnsupportedApiCallException(Feature feature) {
        this.f1370a = feature;
    }

    public final String getMessage() {
        String valueOf = String.valueOf(this.f1370a);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 8);
        sb.append("Missing ");
        sb.append(valueOf);
        return sb.toString();
    }
}
