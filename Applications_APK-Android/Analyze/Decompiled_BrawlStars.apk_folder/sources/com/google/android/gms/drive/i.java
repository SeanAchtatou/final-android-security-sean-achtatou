package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class i implements Parcelable.Creator<DriveId> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new DriveId[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        long j = 0;
        long j2 = 0;
        String str = null;
        int i = -1;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            int i2 = 65535 & readInt;
            if (i2 == 2) {
                str = SafeParcelReader.l(parcel, readInt);
            } else if (i2 == 3) {
                j = SafeParcelReader.f(parcel, readInt);
            } else if (i2 == 4) {
                j2 = SafeParcelReader.f(parcel, readInt);
            } else if (i2 != 5) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                i = SafeParcelReader.d(parcel, readInt);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new DriveId(str, j, j2, i);
    }
}
