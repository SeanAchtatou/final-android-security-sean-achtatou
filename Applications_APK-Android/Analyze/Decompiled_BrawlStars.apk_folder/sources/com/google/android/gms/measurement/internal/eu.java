package com.google.android.gms.measurement.internal;

final class eu implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ bo f2559a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ et f2560b;

    eu(et etVar, bo boVar) {
        this.f2560b = etVar;
        this.f2559a = boVar;
    }

    public final void run() {
        if (ej.a()) {
            this.f2559a.p().a(this);
            return;
        }
        boolean b2 = this.f2560b.b();
        long unused = this.f2560b.d = 0;
        if (b2) {
            this.f2560b.a();
        }
    }
}
