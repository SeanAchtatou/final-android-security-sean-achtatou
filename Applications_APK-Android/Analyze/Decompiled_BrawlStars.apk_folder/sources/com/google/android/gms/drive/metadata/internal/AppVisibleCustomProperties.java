package com.google.android.gms.drive.metadata.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.drive.metadata.CustomPropertyKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class AppVisibleCustomProperties extends AbstractSafeParcelable implements ReflectedParcelable, Iterable<zzc> {
    public static final Parcelable.Creator<AppVisibleCustomProperties> CREATOR = new a();

    /* renamed from: a  reason: collision with root package name */
    public static final AppVisibleCustomProperties f1732a = new a().a();

    /* renamed from: b  reason: collision with root package name */
    private final List<zzc> f1733b;

    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public final Map<CustomPropertyKey, zzc> f1734a = new HashMap();

        public final AppVisibleCustomProperties a() {
            return new AppVisibleCustomProperties(this.f1734a.values());
        }
    }

    AppVisibleCustomProperties(Collection<zzc> collection) {
        l.a(collection);
        this.f1733b = new ArrayList(collection);
    }

    private Map<CustomPropertyKey, String> a() {
        HashMap hashMap = new HashMap(this.f1733b.size());
        for (zzc next : this.f1733b) {
            hashMap.put(next.f1741a, next.f1742b);
        }
        return Collections.unmodifiableMap(hashMap);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        return a().equals(((AppVisibleCustomProperties) obj).a());
    }

    public final Iterator<zzc> iterator() {
        return this.f1733b.iterator();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = com.google.android.gms.common.internal.safeparcel.a.a(parcel, 20293);
        com.google.android.gms.common.internal.safeparcel.a.b(parcel, 2, this.f1733b, false);
        com.google.android.gms.common.internal.safeparcel.a.b(parcel, a2);
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.f1733b});
    }
}
