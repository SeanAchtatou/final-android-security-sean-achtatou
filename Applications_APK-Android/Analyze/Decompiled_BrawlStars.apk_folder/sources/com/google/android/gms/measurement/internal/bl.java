package com.google.android.gms.measurement.internal;

final class bl implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ String f2429a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f2430b;
    private final /* synthetic */ String c;
    private final /* synthetic */ long d;
    private final /* synthetic */ zzby e;

    bl(zzby zzby, String str, String str2, String str3, long j) {
        this.e = zzby;
        this.f2429a = str;
        this.f2430b = str2;
        this.c = str3;
        this.d = j;
    }

    public final void run() {
        String str = this.f2429a;
        if (str == null) {
            this.e.f2597a.f2528b.h().a(this.f2430b, null);
            return;
        }
        this.e.f2597a.f2528b.h().a(this.f2430b, new cg(this.c, str, this.d));
    }
}
