package com.google.android.gms.internal.measurement;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.PersistableBundle;
import android.support.v4.app.NotificationCompat;
import com.facebook.internal.NativeProtocol;
import com.google.android.gms.common.internal.l;

public final class az extends r {

    /* renamed from: a  reason: collision with root package name */
    boolean f2062a;

    /* renamed from: b  reason: collision with root package name */
    boolean f2063b;
    private final AlarmManager d = ((AlarmManager) g().getSystemService(NotificationCompat.CATEGORY_ALARM));
    private Integer e;

    protected az(t tVar) {
        super(tVar);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        try {
            c();
            if (au.e() > 0) {
                Context g = g();
                ActivityInfo receiverInfo = g.getPackageManager().getReceiverInfo(new ComponentName(g, "com.google.android.gms.analytics.AnalyticsReceiver"), 0);
                if (receiverInfo != null && receiverInfo.enabled) {
                    b("Receiver registered for local dispatch.");
                    this.f2062a = true;
                }
            }
        } catch (PackageManager.NameNotFoundException unused) {
        }
    }

    public final void b() {
        m();
        l.a(this.f2062a, "Receiver not registered");
        long e2 = au.e();
        if (e2 > 0) {
            c();
            long b2 = f().b() + e2;
            this.f2063b = true;
            ((Boolean) bc.F.f2067a).booleanValue();
            if (Build.VERSION.SDK_INT >= 24) {
                b("Scheduling upload with JobScheduler");
                Context g = g();
                ComponentName componentName = new ComponentName(g, "com.google.android.gms.analytics.AnalyticsJobService");
                int e3 = e();
                PersistableBundle persistableBundle = new PersistableBundle();
                persistableBundle.putString(NativeProtocol.WEB_DIALOG_ACTION, "com.google.android.gms.analytics.ANALYTICS_DISPATCH");
                JobInfo build = new JobInfo.Builder(e3, componentName).setMinimumLatency(e2).setOverrideDeadline(e2 << 1).setExtras(persistableBundle).build();
                a("Scheduling job. JobID", Integer.valueOf(e3));
                bz.a(g, build, "com.google.android.gms", "DispatchAlarm");
                return;
            }
            b("Scheduling upload with AlarmManager");
            this.d.setInexactRepeating(2, b2, e2, d());
        }
    }

    private final PendingIntent d() {
        Context g = g();
        return PendingIntent.getBroadcast(g, 0, new Intent("com.google.android.gms.analytics.ANALYTICS_DISPATCH").setComponent(new ComponentName(g, "com.google.android.gms.analytics.AnalyticsReceiver")), 0);
    }

    public final void c() {
        this.f2063b = false;
        this.d.cancel(d());
        if (Build.VERSION.SDK_INT >= 24) {
            int e2 = e();
            a("Cancelling job. JobID", Integer.valueOf(e2));
            ((JobScheduler) g().getSystemService("jobscheduler")).cancel(e2);
        }
    }

    private final int e() {
        if (this.e == null) {
            String valueOf = String.valueOf(g().getPackageName());
            this.e = Integer.valueOf((valueOf.length() != 0 ? "analytics".concat(valueOf) : new String("analytics")).hashCode());
        }
        return this.e.intValue();
    }
}
