package com.google.android.gms.internal.measurement;

import com.google.android.gms.common.d;

public final class s {

    /* renamed from: a  reason: collision with root package name */
    public static final String f2330a = String.valueOf(d.f1522b / 1000).replaceAll("(\\d+)(\\d)(\\d\\d)", "$1.$2.$3");

    /* renamed from: b  reason: collision with root package name */
    public static final String f2331b;

    static {
        String valueOf = String.valueOf(f2330a);
        f2331b = valueOf.length() != 0 ? "ma".concat(valueOf) : new String("ma");
    }
}
