package com.google.android.gms.common;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import com.google.android.gms.common.c.c;
import com.google.android.gms.common.internal.l;
import javax.annotation.CheckReturnValue;

@CheckReturnValue
public class g {

    /* renamed from: a  reason: collision with root package name */
    private static g f1537a;

    /* renamed from: b  reason: collision with root package name */
    private final Context f1538b;

    private g(Context context) {
        this.f1538b = context.getApplicationContext();
    }

    public static g a(Context context) {
        l.a(context);
        synchronized (g.class) {
            if (f1537a == null) {
                k.a(context);
                f1537a = new g(context);
            }
        }
        return f1537a;
    }

    public final boolean a(int i) {
        u uVar;
        String[] packagesForUid = c.a(this.f1538b).f1518a.getPackageManager().getPackagesForUid(i);
        if (packagesForUid != null && packagesForUid.length != 0) {
            uVar = null;
            for (String a2 : packagesForUid) {
                uVar = a(a2, i);
                if (uVar.f1663a) {
                    break;
                }
            }
        } else {
            uVar = u.a("no pkgs");
        }
        if (!uVar.f1663a && Log.isLoggable("GoogleCertificatesRslt", 3)) {
            if (uVar.f1664b != null) {
                uVar.b();
            } else {
                uVar.b();
            }
        }
        return uVar.f1663a;
    }

    public static boolean a(PackageInfo packageInfo, boolean z) {
        m mVar;
        if (!(packageInfo == null || packageInfo.signatures == null)) {
            if (z) {
                mVar = a(packageInfo, p.f1634a);
            } else {
                mVar = a(packageInfo, p.f1634a[0]);
            }
            if (mVar != null) {
                return true;
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.g.a(android.content.pm.PackageInfo, boolean):boolean
     arg types: [android.content.pm.PackageInfo, int]
     candidates:
      com.google.android.gms.common.g.a(android.content.pm.PackageInfo, com.google.android.gms.common.m[]):com.google.android.gms.common.m
      com.google.android.gms.common.g.a(java.lang.String, int):com.google.android.gms.common.u
      com.google.android.gms.common.g.a(android.content.pm.PackageInfo, boolean):boolean */
    public final boolean a(PackageInfo packageInfo) {
        if (packageInfo == null) {
            return false;
        }
        if (a(packageInfo, false)) {
            return true;
        }
        return a(packageInfo, true) && f.c(this.f1538b);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.k.a(java.lang.String, com.google.android.gms.common.m, boolean):com.google.android.gms.common.u
     arg types: [java.lang.String, com.google.android.gms.common.n, int]
     candidates:
      com.google.android.gms.common.k.a(boolean, java.lang.String, com.google.android.gms.common.m):java.lang.String
      com.google.android.gms.common.k.a(java.lang.String, com.google.android.gms.common.m, boolean):com.google.android.gms.common.u */
    private final u a(String str, int i) {
        try {
            PackageInfo packageInfo = c.a(this.f1538b).f1518a.getPackageManager().getPackageInfo(str, 64);
            boolean c = f.c(this.f1538b);
            if (packageInfo == null) {
                return u.a("null pkg");
            }
            if (packageInfo.signatures.length != 1) {
                return u.a("single cert required");
            }
            n nVar = new n(packageInfo.signatures[0].toByteArray());
            String str2 = packageInfo.packageName;
            u a2 = k.a(str2, nVar, c);
            if (!a2.f1663a || packageInfo.applicationInfo == null || (packageInfo.applicationInfo.flags & 2) == 0 || (c && !k.a(str2, (m) nVar, false).f1663a)) {
                return a2;
            }
            return u.a("debuggable release cert app rejected");
        } catch (PackageManager.NameNotFoundException unused) {
            String valueOf = String.valueOf(str);
            return u.a(valueOf.length() != 0 ? "no pkg ".concat(valueOf) : new String("no pkg "));
        }
    }

    private static m a(PackageInfo packageInfo, m... mVarArr) {
        if (packageInfo.signatures == null || packageInfo.signatures.length != 1) {
            return null;
        }
        n nVar = new n(packageInfo.signatures[0].toByteArray());
        for (int i = 0; i < mVarArr.length; i++) {
            if (mVarArr[i].equals(nVar)) {
                return mVarArr[i];
            }
        }
        return null;
    }
}
