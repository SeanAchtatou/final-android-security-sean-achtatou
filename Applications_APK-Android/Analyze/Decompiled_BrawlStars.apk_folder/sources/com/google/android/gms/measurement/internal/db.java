package com.google.android.gms.measurement.internal;

import android.content.ComponentName;

final class db implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ cx f2501a;

    db(cx cxVar) {
        this.f2501a = cxVar;
    }

    public final void run() {
        cl.a(this.f2501a.c, new ComponentName(this.f2501a.c.m(), "com.google.android.gms.measurement.AppMeasurementService"));
    }
}
