package com.google.android.gms.games.multiplayer.realtime;

import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.data.d;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.ParticipantRef;
import java.util.ArrayList;

public final class zzf extends d implements Room {
    private final int c;

    public final /* synthetic */ Object a() {
        return new RoomEntity(this);
    }

    public final String b() {
        return e("external_match_id");
    }

    public final String c() {
        return e("creator_external");
    }

    public final long d() {
        return b("creation_timestamp");
    }

    public final int describeContents() {
        return 0;
    }

    public final int e() {
        return c("status");
    }

    public final boolean equals(Object obj) {
        return RoomEntity.a(this, obj);
    }

    public final String f() {
        return e("description");
    }

    public final int g() {
        return c("variant");
    }

    public final int hashCode() {
        return RoomEntity.a(this);
    }

    public final ArrayList<Participant> i() {
        ArrayList<Participant> arrayList = new ArrayList<>(this.c);
        for (int i = 0; i < this.c; i++) {
            arrayList.add(new ParticipantRef(this.f1532a, this.f1533b + i));
        }
        return arrayList;
    }

    public final int j() {
        return c("automatch_wait_estimate_sec");
    }

    public final String toString() {
        return RoomEntity.b(this);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        ((RoomEntity) ((Room) a())).writeToParcel(parcel, i);
    }

    public final Bundle h() {
        if (!d("has_automatch_criteria")) {
            return null;
        }
        int c2 = c("automatch_min_players");
        int c3 = c("automatch_max_players");
        long b2 = b("automatch_bit_mask");
        Bundle bundle = new Bundle();
        bundle.putInt("min_automatch_players", c2);
        bundle.putInt("max_automatch_players", c3);
        bundle.putLong("exclusive_bit_mask", b2);
        return bundle;
    }
}
