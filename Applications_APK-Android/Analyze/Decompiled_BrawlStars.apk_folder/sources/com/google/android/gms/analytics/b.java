package com.google.android.gms.analytics;

import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class b {

    public static class c extends C0109b<c> {
        public c() {
            a("&t", "screenview");
        }
    }

    public static class a extends C0109b<a> {
        public a() {
            a("&t", NotificationCompat.CATEGORY_EVENT);
        }

        public a(String str, String str2) {
            this();
            a("&ec", str);
            a("&ea", str2);
        }

        public final a a(String str) {
            a("&el", str);
            return this;
        }

        public final a a(long j) {
            a("&ev", Long.toString(j));
            return this;
        }
    }

    /* renamed from: com.google.android.gms.analytics.b$b  reason: collision with other inner class name */
    public static class C0109b<T extends C0109b> {

        /* renamed from: a  reason: collision with root package name */
        private Map<String, String> f1317a = new HashMap();

        /* renamed from: b  reason: collision with root package name */
        private com.google.android.gms.analytics.a.b f1318b;
        private Map<String, List<com.google.android.gms.analytics.a.a>> c = new HashMap();
        private List<com.google.android.gms.analytics.a.c> d = new ArrayList();
        private List<com.google.android.gms.analytics.a.a> e = new ArrayList();

        protected C0109b() {
        }

        public final T a(String str, String str2) {
            this.f1317a.put(str, str2);
            return this;
        }

        public final Map<String, String> a() {
            HashMap hashMap = new HashMap(this.f1317a);
            com.google.android.gms.analytics.a.b bVar = this.f1318b;
            if (bVar != null) {
                hashMap.putAll(bVar.a());
            }
            int i = 1;
            for (com.google.android.gms.analytics.a.c a2 : this.d) {
                hashMap.putAll(a2.a(h.a("&promo", i)));
                i++;
            }
            int i2 = 1;
            for (com.google.android.gms.analytics.a.a a3 : this.e) {
                hashMap.putAll(a3.a(h.a("&pr", i2)));
                i2++;
            }
            int i3 = 1;
            for (Map.Entry next : this.c.entrySet()) {
                String a4 = h.a("&il", i3);
                int i4 = 1;
                for (com.google.android.gms.analytics.a.a aVar : (List) next.getValue()) {
                    String valueOf = String.valueOf(a4);
                    String valueOf2 = String.valueOf(h.a("pi", i4));
                    hashMap.putAll(aVar.a(valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf)));
                    i4++;
                }
                if (!TextUtils.isEmpty((CharSequence) next.getKey())) {
                    String valueOf3 = String.valueOf(a4);
                    hashMap.put("nm".length() != 0 ? valueOf3.concat("nm") : new String(valueOf3), (String) next.getKey());
                }
                i3++;
            }
            return hashMap;
        }
    }
}
