package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.common.internal.l;
import java.util.Iterator;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    final String f2452a;

    /* renamed from: b  reason: collision with root package name */
    final String f2453b;
    final long c;
    final long d;
    final zzad e;
    private final String f;

    private c(ar arVar, String str, String str2, String str3, long j, long j2, zzad zzad) {
        l.a(str2);
        l.a(str3);
        l.a(zzad);
        this.f2452a = str2;
        this.f2453b = str3;
        this.f = TextUtils.isEmpty(str) ? null : str;
        this.c = j;
        this.d = j2;
        long j3 = this.d;
        if (j3 != 0 && j3 > this.c) {
            arVar.q().f.a("Event created with reverse previous/current timestamps. appId, name", o.a(str2), o.a(str3));
        }
        this.e = zzad;
    }

    c(ar arVar, String str, String str2, String str3, long j, long j2, Bundle bundle) {
        zzad zzad;
        l.a(str2);
        l.a(str3);
        this.f2452a = str2;
        this.f2453b = str3;
        this.f = TextUtils.isEmpty(str) ? null : str;
        this.c = j;
        this.d = j2;
        long j3 = this.d;
        if (j3 != 0 && j3 > this.c) {
            arVar.q().f.a("Event created with reverse previous/current timestamps. appId", o.a(str2));
        }
        if (bundle == null || bundle.isEmpty()) {
            zzad = new zzad(new Bundle());
        } else {
            Bundle bundle2 = new Bundle(bundle);
            Iterator<String> it = bundle2.keySet().iterator();
            while (it.hasNext()) {
                String next = it.next();
                if (next == null) {
                    arVar.q().c.a("Param name can't be null");
                    it.remove();
                } else {
                    arVar.e();
                    Object a2 = ed.a(next, bundle2.get(next));
                    if (a2 == null) {
                        arVar.q().f.a("Param value can't be null", arVar.f().b(next));
                        it.remove();
                    } else {
                        arVar.e().a(bundle2, next, a2);
                    }
                }
            }
            zzad = new zzad(bundle2);
        }
        this.e = zzad;
    }

    /* access modifiers changed from: package-private */
    public final c a(ar arVar, long j) {
        return new c(arVar, this.f, this.f2452a, this.f2453b, this.c, j, this.e);
    }

    public final String toString() {
        String str = this.f2452a;
        String str2 = this.f2453b;
        String valueOf = String.valueOf(this.e);
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 33 + String.valueOf(str2).length() + String.valueOf(valueOf).length());
        sb.append("Event{appId='");
        sb.append(str);
        sb.append("', name='");
        sb.append(str2);
        sb.append("', params=");
        sb.append(valueOf);
        sb.append('}');
        return sb.toString();
    }
}
