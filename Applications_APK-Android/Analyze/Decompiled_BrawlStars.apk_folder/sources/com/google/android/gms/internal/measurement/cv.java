package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class cv extends iz<cv> {

    /* renamed from: a  reason: collision with root package name */
    public cw[] f2140a = cw.a();

    public cv() {
        this.L = null;
        this.M = -1;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof cv)) {
            return false;
        }
        cv cvVar = (cv) obj;
        if (!jd.a(this.f2140a, cvVar.f2140a)) {
            return false;
        }
        if (this.L == null || this.L.a()) {
            return cvVar.L == null || cvVar.L.a();
        }
        return this.L.equals(cvVar.L);
    }

    public final int hashCode() {
        return ((((getClass().getName().hashCode() + 527) * 31) + jd.a(this.f2140a)) * 31) + ((this.L == null || this.L.a()) ? 0 : this.L.hashCode());
    }

    public final void a(iy iyVar) throws IOException {
        cw[] cwVarArr = this.f2140a;
        if (cwVarArr != null && cwVarArr.length > 0) {
            int i = 0;
            while (true) {
                cw[] cwVarArr2 = this.f2140a;
                if (i >= cwVarArr2.length) {
                    break;
                }
                cw cwVar = cwVarArr2[i];
                if (cwVar != null) {
                    iyVar.a(1, cwVar);
                }
                i++;
            }
        }
        super.a(iyVar);
    }

    /* access modifiers changed from: protected */
    public final int b() {
        int b2 = super.b();
        cw[] cwVarArr = this.f2140a;
        if (cwVarArr != null && cwVarArr.length > 0) {
            int i = 0;
            while (true) {
                cw[] cwVarArr2 = this.f2140a;
                if (i >= cwVarArr2.length) {
                    break;
                }
                cw cwVar = cwVarArr2[i];
                if (cwVar != null) {
                    b2 += iy.b(1, cwVar);
                }
                i++;
            }
        }
        return b2;
    }

    public final /* synthetic */ je a(iw iwVar) throws IOException {
        while (true) {
            int a2 = iwVar.a();
            if (a2 == 0) {
                return this;
            }
            if (a2 == 10) {
                int a3 = jh.a(iwVar, 10);
                cw[] cwVarArr = this.f2140a;
                int length = cwVarArr == null ? 0 : cwVarArr.length;
                cw[] cwVarArr2 = new cw[(a3 + length)];
                if (length != 0) {
                    System.arraycopy(this.f2140a, 0, cwVarArr2, 0, length);
                }
                while (length < cwVarArr2.length - 1) {
                    cwVarArr2[length] = new cw();
                    iwVar.a(cwVarArr2[length]);
                    iwVar.a();
                    length++;
                }
                cwVarArr2[length] = new cw();
                iwVar.a(cwVarArr2[length]);
                this.f2140a = cwVarArr2;
            } else if (!super.a(iwVar, a2)) {
                return this;
            }
        }
    }
}
