package com.google.android.gms.internal.measurement;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Pair;
import com.facebook.internal.NativeProtocol;
import com.facebook.share.internal.ShareConstants;
import com.google.android.gms.analytics.CampaignTrackingReceiver;
import com.google.android.gms.analytics.e;
import com.google.android.gms.analytics.k;
import com.google.android.gms.analytics.n;
import com.google.android.gms.common.c.c;
import com.google.android.gms.common.internal.l;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class ag extends r {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2041a;

    /* renamed from: b  reason: collision with root package name */
    private final ad f2042b;
    private final bm d;
    private final bl e;
    private final y f;
    private long g = Long.MIN_VALUE;
    private final aw h;
    private final aw i;
    private final bw j;
    private long k;
    private boolean l;

    protected ag(t tVar, v vVar) {
        super(tVar);
        l.a(vVar);
        this.e = new bl(tVar);
        this.f2042b = new ad(tVar);
        this.d = new bm(tVar);
        this.f = new y(tVar);
        this.j = new bw(f());
        this.h = new ah(this, tVar);
        this.i = new ai(this, tVar);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f2042b.n();
        this.d.n();
        this.f.n();
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        m();
        l.a(!this.f2041a, "Analytics backend already started");
        this.f2041a = true;
        this.c.b().a(new aj(this));
    }

    private final boolean g(String str) {
        return c.a(g()).a(str) == 0;
    }

    /* access modifiers changed from: protected */
    public final void c() {
        m();
        n.b();
        Context context = this.c.f2332a;
        if (!bq.a(context)) {
            e("AnalyticsReceiver is not registered or is disabled. Register the receiver for reliable dispatching on non-Google Play devices. See http://goo.gl/8Rd3yj for instructions.");
        } else if (!br.a(context)) {
            f("AnalyticsService is not registered or is disabled. Analytics service at risk of not starting. See http://goo.gl/8Rd3yj for instructions.");
        }
        if (!CampaignTrackingReceiver.a(context)) {
            e("CampaignTrackingReceiver is not registered, not exported or is disabled. Installation campaign tracking is not possible. See http://goo.gl/8Rd3yj for instructions.");
        }
        j().b();
        if (!g("android.permission.ACCESS_NETWORK_STATE")) {
            f("Missing required android.permission.ACCESS_NETWORK_STATE. Google Analytics disabled. See http://goo.gl/8Rd3yj for instructions");
            v();
        }
        if (!g("android.permission.INTERNET")) {
            f("Missing required android.permission.INTERNET. Google Analytics disabled. See http://goo.gl/8Rd3yj for instructions");
            v();
        }
        if (br.a(g())) {
            b("AnalyticsService registered in the app manifest and enabled");
        } else {
            e("AnalyticsService not registered in the app manifest. Hits might not be delivered reliably. See http://goo.gl/8Rd3yj for instructions.");
        }
        if (!this.l && !this.f2042b.e()) {
            p();
        }
        o();
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        n.b();
        this.k = f().a();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0044 A[LOOP:1: B:15:0x0044->B:23:?, LOOP_START] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0040 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void e() {
        /*
            r5 = this;
            com.google.android.gms.analytics.n.b()
            com.google.android.gms.analytics.n.b()
            r5.m()
            boolean r0 = com.google.android.gms.internal.measurement.au.b()
            if (r0 != 0) goto L_0x0014
            java.lang.String r0 = "Service client disabled. Can't dispatch local hits to device AnalyticsService"
            r5.e(r0)
        L_0x0014:
            com.google.android.gms.internal.measurement.y r0 = r5.f
            boolean r0 = r0.b()
            if (r0 != 0) goto L_0x0022
            java.lang.String r0 = "Service not connected"
            r5.b(r0)
            return
        L_0x0022:
            com.google.android.gms.internal.measurement.ad r0 = r5.f2042b
            boolean r0 = r0.e()
            if (r0 != 0) goto L_0x007b
            java.lang.String r0 = "Dispatching local hits to device AnalyticsService"
            r5.b(r0)
        L_0x002f:
            com.google.android.gms.internal.measurement.ad r0 = r5.f2042b     // Catch:{ SQLiteException -> 0x0072 }
            int r1 = com.google.android.gms.internal.measurement.au.f()     // Catch:{ SQLiteException -> 0x0072 }
            long r1 = (long) r1     // Catch:{ SQLiteException -> 0x0072 }
            java.util.List r0 = r0.a(r1)     // Catch:{ SQLiteException -> 0x0072 }
            boolean r1 = r0.isEmpty()     // Catch:{ SQLiteException -> 0x0072 }
            if (r1 == 0) goto L_0x0044
            r5.o()     // Catch:{ SQLiteException -> 0x0072 }
            return
        L_0x0044:
            boolean r1 = r0.isEmpty()
            if (r1 != 0) goto L_0x002f
            r1 = 0
            java.lang.Object r1 = r0.get(r1)
            com.google.android.gms.internal.measurement.bh r1 = (com.google.android.gms.internal.measurement.bh) r1
            com.google.android.gms.internal.measurement.y r2 = r5.f
            boolean r2 = r2.a(r1)
            if (r2 != 0) goto L_0x005d
            r5.o()
            return
        L_0x005d:
            r0.remove(r1)
            com.google.android.gms.internal.measurement.ad r2 = r5.f2042b     // Catch:{ SQLiteException -> 0x0068 }
            long r3 = r1.c     // Catch:{ SQLiteException -> 0x0068 }
            r2.b(r3)     // Catch:{ SQLiteException -> 0x0068 }
            goto L_0x0044
        L_0x0068:
            r0 = move-exception
            java.lang.String r1 = "Failed to remove hit that was send for delivery"
            r5.e(r1, r0)
            r5.t()
            return
        L_0x0072:
            r0 = move-exception
            java.lang.String r1 = "Failed to read hits from store"
            r5.e(r1, r0)
            r5.t()
        L_0x007b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.ag.e():void");
    }

    /* access modifiers changed from: protected */
    public final void a(w wVar) {
        n.b();
        b("Sending first hit to property", wVar.c);
        if (!j().c().a(au.l())) {
            String o = j().o();
            if (!TextUtils.isEmpty(o)) {
                ix a2 = bx.a(this.c.a(), o);
                b("Found relevant installation campaign", a2);
                a(wVar, a2);
            }
        }
    }

    private final void p() {
        if (!this.l && au.b() && !this.f.b()) {
            if (this.j.a(((Long) bc.C.f2067a).longValue())) {
                this.j.a();
                b("Connecting to service");
                if (this.f.c()) {
                    b("Connected to service");
                    this.j.f2099a = 0;
                    e();
                }
            }
        }
    }

    public final long b(w wVar) {
        w wVar2 = wVar;
        l.a(wVar);
        m();
        n.b();
        try {
            this.f2042b.b();
            ad adVar = this.f2042b;
            long j2 = wVar2.f2337a;
            String str = wVar2.f2338b;
            l.a(str);
            adVar.m();
            n.b();
            int delete = adVar.r().delete("properties", "app_uid=? AND cid<>?", new String[]{String.valueOf(j2), str});
            if (delete > 0) {
                adVar.a("Deleted property records", Integer.valueOf(delete));
            }
            long a2 = this.f2042b.a(wVar2.f2337a, wVar2.f2338b, wVar2.c);
            wVar2.e = 1 + a2;
            ad adVar2 = this.f2042b;
            l.a(wVar);
            adVar2.m();
            n.b();
            SQLiteDatabase r = adVar2.r();
            Map<String, String> map = wVar2.f;
            l.a(map);
            Uri.Builder builder = new Uri.Builder();
            for (Map.Entry next : map.entrySet()) {
                builder.appendQueryParameter((String) next.getKey(), (String) next.getValue());
            }
            String encodedQuery = builder.build().getEncodedQuery();
            if (encodedQuery == null) {
                encodedQuery = "";
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_uid", Long.valueOf(wVar2.f2337a));
            contentValues.put("cid", wVar2.f2338b);
            contentValues.put("tid", wVar2.c);
            contentValues.put("adid", Integer.valueOf(wVar2.d ? 1 : 0));
            contentValues.put("hits_count", Long.valueOf(wVar2.e));
            contentValues.put(NativeProtocol.WEB_DIALOG_PARAMS, encodedQuery);
            try {
                if (r.insertWithOnConflict("properties", null, contentValues, 5) == -1) {
                    adVar2.f("Failed to insert/update a property (got -1)");
                }
            } catch (SQLiteException e2) {
                adVar2.e("Error storing a property", e2);
            }
            this.f2042b.c();
            try {
                this.f2042b.d();
            } catch (SQLiteException e3) {
                e("Failed to end transaction", e3);
            }
            return a2;
        } catch (SQLiteException e4) {
            e("Failed to update Analytics property", e4);
            try {
                this.f2042b.d();
            } catch (SQLiteException e5) {
                e("Failed to end transaction", e5);
            }
            return -1;
        } catch (Throwable th) {
            Throwable th2 = th;
            try {
                this.f2042b.d();
            } catch (SQLiteException e6) {
                e("Failed to end transaction", e6);
            }
            throw th2;
        }
    }

    public final void a(bh bhVar) {
        Pair<String, Long> b2;
        l.a(bhVar);
        n.b();
        m();
        if (this.l) {
            c("Hit delivery not possible. Missing network permissions. See http://goo.gl/8Rd3yj for instructions");
        } else {
            a("Delivering hit", bhVar);
        }
        if (TextUtils.isEmpty(bhVar.a("_m", "")) && (b2 = j().f2086b.b()) != null) {
            String str = (String) b2.first;
            String valueOf = String.valueOf((Long) b2.second);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 1 + String.valueOf(str).length());
            sb.append(valueOf);
            sb.append(":");
            sb.append(str);
            String sb2 = sb.toString();
            HashMap hashMap = new HashMap(bhVar.f2073a);
            hashMap.put("_m", sb2);
            bhVar = new bh(this, hashMap, bhVar.d, bhVar.f, bhVar.c, bhVar.e, bhVar.f2074b);
        }
        p();
        if (this.f.a(bhVar)) {
            c("Hit sent to the device AnalyticsService for delivery");
            return;
        }
        try {
            this.f2042b.a(bhVar);
            o();
        } catch (SQLiteException e2) {
            e("Delivery failed to save hit to a database", e2);
            this.c.a().a(bhVar, "deliver: failed to insert hit to database");
        }
    }

    private final boolean q() {
        n.b();
        m();
        b("Dispatching a batch of local hits");
        boolean z = !this.f.b();
        boolean z2 = !this.d.b();
        if (!z || !z2) {
            long max = (long) Math.max(au.f(), au.g());
            ArrayList arrayList = new ArrayList();
            long j2 = 0;
            while (true) {
                this.f2042b.b();
                arrayList.clear();
                try {
                    List<bh> a2 = this.f2042b.a(max);
                    if (a2.isEmpty()) {
                        b("Store is empty, nothing to dispatch");
                        t();
                        try {
                            this.f2042b.c();
                            this.f2042b.d();
                            return false;
                        } catch (SQLiteException e2) {
                            e("Failed to commit local dispatch transaction", e2);
                            t();
                            return false;
                        }
                    } else {
                        a("Hits loaded from store. count", Integer.valueOf(a2.size()));
                        for (bh bhVar : a2) {
                            if (bhVar.c == j2) {
                                d("Database contains successfully uploaded hit", Long.valueOf(j2), Integer.valueOf(a2.size()));
                                t();
                                try {
                                    this.f2042b.c();
                                    this.f2042b.d();
                                    return false;
                                } catch (SQLiteException e3) {
                                    e("Failed to commit local dispatch transaction", e3);
                                    t();
                                    return false;
                                }
                            }
                        }
                        if (this.f.b()) {
                            b("Service connected, sending hits to the service");
                            while (!a2.isEmpty()) {
                                bh bhVar2 = a2.get(0);
                                if (this.f.a(bhVar2)) {
                                    j2 = Math.max(j2, bhVar2.c);
                                    a2.remove(bhVar2);
                                    b("Hit sent do device AnalyticsService for delivery", bhVar2);
                                    try {
                                        this.f2042b.b(bhVar2.c);
                                        arrayList.add(Long.valueOf(bhVar2.c));
                                    } catch (SQLiteException e4) {
                                        e("Failed to remove hit that was send for delivery", e4);
                                        t();
                                        try {
                                            this.f2042b.c();
                                            this.f2042b.d();
                                            return false;
                                        } catch (SQLiteException e5) {
                                            e("Failed to commit local dispatch transaction", e5);
                                            t();
                                            return false;
                                        }
                                    } catch (Throwable th) {
                                        try {
                                            this.f2042b.c();
                                            this.f2042b.d();
                                            throw th;
                                        } catch (SQLiteException e6) {
                                            e("Failed to commit local dispatch transaction", e6);
                                            t();
                                            return false;
                                        }
                                    }
                                }
                            }
                        }
                        if (this.d.b()) {
                            List<Long> a3 = this.d.a(a2);
                            for (Long longValue : a3) {
                                j2 = Math.max(j2, longValue.longValue());
                            }
                            try {
                                this.f2042b.a(a3);
                                arrayList.addAll(a3);
                            } catch (SQLiteException e7) {
                                e("Failed to remove successfully uploaded hits", e7);
                                t();
                                try {
                                    this.f2042b.c();
                                    this.f2042b.d();
                                    return false;
                                } catch (SQLiteException e8) {
                                    e("Failed to commit local dispatch transaction", e8);
                                    t();
                                    return false;
                                }
                            }
                        }
                        if (arrayList.isEmpty()) {
                            try {
                                this.f2042b.c();
                                this.f2042b.d();
                                return false;
                            } catch (SQLiteException e9) {
                                e("Failed to commit local dispatch transaction", e9);
                                t();
                                return false;
                            }
                        } else {
                            try {
                                this.f2042b.c();
                                this.f2042b.d();
                            } catch (SQLiteException e10) {
                                e("Failed to commit local dispatch transaction", e10);
                                t();
                                return false;
                            }
                        }
                    }
                } catch (SQLiteException e11) {
                    d("Failed to read hits from persisted store", e11);
                    t();
                    try {
                        this.f2042b.c();
                        this.f2042b.d();
                        return false;
                    } catch (SQLiteException e12) {
                        e("Failed to commit local dispatch transaction", e12);
                        t();
                        return false;
                    }
                }
            }
        } else {
            b("No network or service available. Will retry later");
            return false;
        }
    }

    public final void a(ba baVar) {
        long j2 = this.k;
        n.b();
        m();
        long d2 = j().d();
        b("Dispatching local hits. Elapsed time since last dispatch (ms)", Long.valueOf(d2 != 0 ? Math.abs(f().a() - d2) : -1));
        p();
        try {
            q();
            j().e();
            o();
            if (baVar != null) {
                baVar.a();
            }
            if (this.k != j2) {
                this.e.c();
            }
        } catch (Exception e2) {
            e("Local dispatch failed", e2);
            j().e();
            o();
            if (baVar != null) {
                baVar.a();
            }
        }
    }

    private final long r() {
        n.b();
        m();
        try {
            return this.f2042b.p();
        } catch (SQLiteException e2) {
            e("Failed to get min/max hit times from local store", e2);
            return 0;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public final void o() {
        long j2;
        n.b();
        m();
        boolean z = true;
        if (!(!this.l && u() > 0)) {
            this.e.b();
            t();
        } else if (this.f2042b.e()) {
            this.e.b();
            t();
        } else {
            if (!((Boolean) bc.z.f2067a).booleanValue()) {
                this.e.a();
                z = this.e.d();
            }
            if (z) {
                s();
                long u = u();
                long d2 = j().d();
                if (d2 != 0) {
                    j2 = u - Math.abs(f().a() - d2);
                    if (j2 <= 0) {
                        j2 = Math.min(au.d(), u);
                    }
                } else {
                    j2 = Math.min(au.d(), u);
                }
                a("Dispatch scheduled (ms)", Long.valueOf(j2));
                if (this.h.c()) {
                    this.h.b(Math.max(1L, j2 + this.h.b()));
                } else {
                    this.h.a(j2);
                }
            } else {
                t();
                s();
            }
        }
    }

    private final void s() {
        az i2 = i();
        if (i2.f2062a && !i2.f2063b) {
            long r = r();
            if (r != 0 && Math.abs(f().a() - r) <= ((Long) bc.h.f2067a).longValue()) {
                a("Dispatch alarm scheduled (ms)", Long.valueOf(au.e()));
                i2.b();
            }
        }
    }

    private final void t() {
        if (this.h.c()) {
            b("All hits dispatched or no network/service. Going to power save mode");
        }
        this.h.d();
        az i2 = i();
        if (i2.f2063b) {
            i2.c();
        }
    }

    private final long u() {
        long j2 = this.g;
        if (j2 != Long.MIN_VALUE) {
            return j2;
        }
        long longValue = ((Long) bc.e.f2067a).longValue();
        by e2 = this.c.e();
        e2.m();
        if (!e2.f2102a) {
            return longValue;
        }
        by e3 = this.c.e();
        e3.m();
        return ((long) e3.f2103b) * 1000;
    }

    public final void a(String str) {
        l.a(str);
        n.b();
        ix a2 = bx.a(this.c.a(), str);
        if (a2 == null) {
            d("Parsing failed. Ignoring invalid campaign data", str);
            return;
        }
        String o = j().o();
        if (str.equals(o)) {
            e("Ignoring duplicate install campaign");
        } else if (!TextUtils.isEmpty(o)) {
            d("Ignoring multiple install campaigns. original, new", o, str);
        } else {
            j().a(str);
            if (j().c().a(au.l())) {
                d("Campaign received too late, ignoring", a2);
                return;
            }
            b("Received installation campaign", a2);
            for (w a3 : this.f2042b.q()) {
                a(a3, a2);
            }
        }
    }

    private final void a(w wVar, ix ixVar) {
        l.a(wVar);
        l.a(ixVar);
        e eVar = new e(this.c);
        eVar.b(wVar.c);
        eVar.e = wVar.d;
        k d2 = eVar.d();
        g gVar = (g) d2.b(g.class);
        gVar.f2231a = ShareConstants.WEB_DIALOG_PARAM_DATA;
        gVar.g = true;
        d2.a(ixVar);
        b bVar = (b) d2.b(b.class);
        hy hyVar = (hy) d2.b(hy.class);
        for (Map.Entry next : wVar.f.entrySet()) {
            String str = (String) next.getKey();
            String str2 = (String) next.getValue();
            if ("an".equals(str)) {
                hyVar.f2277a = str2;
            } else if ("av".equals(str)) {
                hyVar.f2278b = str2;
            } else if ("aid".equals(str)) {
                hyVar.c = str2;
            } else if ("aiid".equals(str)) {
                hyVar.d = str2;
            } else if ("uid".equals(str)) {
                gVar.c = str2;
            } else {
                bVar.a(str, str2);
            }
        }
        b("Sending installation campaign to", wVar.c, ixVar);
        d2.d = j().b();
        d2.b();
    }

    private final void v() {
        m();
        n.b();
        this.l = true;
        this.f.d();
        o();
    }

    static /* synthetic */ void b(ag agVar) {
        try {
            agVar.f2042b.o();
            agVar.o();
        } catch (SQLiteException e2) {
            agVar.d("Failed to delete stale hits", e2);
        }
        agVar.i.a(86400000);
    }
}
