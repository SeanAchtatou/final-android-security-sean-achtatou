package com.google.android.gms.common.api.internal;

import java.lang.ref.WeakReference;

final class ak extends ay {

    /* renamed from: a  reason: collision with root package name */
    private WeakReference<ag> f1396a;

    ak(ag agVar) {
        this.f1396a = new WeakReference<>(agVar);
    }

    public final void a() {
        ag agVar = this.f1396a.get();
        if (agVar != null) {
            ag.a(agVar);
        }
    }
}
