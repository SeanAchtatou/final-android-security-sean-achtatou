package com.google.android.gms.games.internal.experience;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.games.internal.zzd;
import java.util.Arrays;

public final class ExperienceEventEntity extends zzd implements ExperienceEvent {
    public static final Parcelable.Creator<ExperienceEventEntity> CREATOR = new a();

    /* renamed from: a  reason: collision with root package name */
    private final String f1807a;

    /* renamed from: b  reason: collision with root package name */
    private final GameEntity f1808b;
    private final String c;
    private final String d;
    private final String e;
    private final Uri f;
    private final long g;
    private final long h;
    private final long i;
    private final int j;
    private final int k;

    ExperienceEventEntity(String str, GameEntity gameEntity, String str2, String str3, String str4, Uri uri, long j2, long j3, long j4, int i2, int i3) {
        this.f1807a = str;
        this.f1808b = gameEntity;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = uri;
        this.g = j2;
        this.h = j3;
        this.i = j4;
        this.j = i2;
        this.k = i3;
    }

    public final /* bridge */ /* synthetic */ Object a() {
        return this;
    }

    public final String b() {
        return this.f1807a;
    }

    public final Game c() {
        return this.f1808b;
    }

    public final String d() {
        return this.c;
    }

    public final String e() {
        return this.d;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof ExperienceEvent)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        ExperienceEvent experienceEvent = (ExperienceEvent) obj;
        return j.a(experienceEvent.b(), b()) && j.a(experienceEvent.c(), c()) && j.a(experienceEvent.d(), d()) && j.a(experienceEvent.e(), e()) && j.a(experienceEvent.getIconImageUrl(), getIconImageUrl()) && j.a(experienceEvent.f(), f()) && j.a(Long.valueOf(experienceEvent.g()), Long.valueOf(g())) && j.a(Long.valueOf(experienceEvent.h()), Long.valueOf(h())) && j.a(Long.valueOf(experienceEvent.i()), Long.valueOf(i())) && j.a(Integer.valueOf(experienceEvent.j()), Integer.valueOf(j())) && j.a(Integer.valueOf(experienceEvent.k()), Integer.valueOf(k()));
    }

    public final Uri f() {
        return this.f;
    }

    public final long g() {
        return this.g;
    }

    public final String getIconImageUrl() {
        return this.e;
    }

    public final long h() {
        return this.h;
    }

    public final long i() {
        return this.i;
    }

    public final int j() {
        return this.j;
    }

    public final int k() {
        return this.k;
    }

    public final String toString() {
        return j.a(this).a("ExperienceId", b()).a("Game", c()).a("DisplayTitle", d()).a("DisplayDescription", e()).a("IconImageUrl", getIconImageUrl()).a("IconImageUri", f()).a("CreatedTimestamp", Long.valueOf(g())).a("XpEarned", Long.valueOf(h())).a("CurrentXp", Long.valueOf(i())).a("Type", Integer.valueOf(j())).a("NewLevel", Integer.valueOf(k())).toString();
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{b(), c(), d(), e(), getIconImageUrl(), f(), Long.valueOf(g()), Long.valueOf(h()), Long.valueOf(i()), Integer.valueOf(j()), Integer.valueOf(k())});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.games.GameEntity, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.net.Uri, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = a.a(parcel, 20293);
        a.a(parcel, 1, this.f1807a, false);
        a.a(parcel, 2, (Parcelable) this.f1808b, i2, false);
        a.a(parcel, 3, this.c, false);
        a.a(parcel, 4, this.d, false);
        a.a(parcel, 5, getIconImageUrl(), false);
        a.a(parcel, 6, (Parcelable) this.f, i2, false);
        a.a(parcel, 7, this.g);
        a.a(parcel, 8, this.h);
        a.a(parcel, 9, this.i);
        a.b(parcel, 10, this.j);
        a.b(parcel, 11, this.k);
        a.b(parcel, a2);
    }
}
