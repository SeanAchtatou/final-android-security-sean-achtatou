package com.google.android.gms.tagmanager;

final class al<T> {

    /* renamed from: a  reason: collision with root package name */
    final T f2622a;

    /* renamed from: b  reason: collision with root package name */
    final boolean f2623b;

    al(T t, boolean z) {
        this.f2622a = t;
        this.f2623b = z;
    }
}
