package com.google.android.gms.common.internal;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;

public interface zzi extends IInterface {
    IObjectWrapper a() throws RemoteException;

    int b() throws RemoteException;
}
