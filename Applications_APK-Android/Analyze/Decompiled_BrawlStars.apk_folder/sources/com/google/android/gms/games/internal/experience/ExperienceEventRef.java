package com.google.android.gms.games.internal.experience;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.d;
import com.google.android.gms.games.Game;

public final class ExperienceEventRef extends d implements ExperienceEvent {
    public final /* synthetic */ Object a() {
        throw new NoSuchMethodError();
    }

    public final String b() {
        throw new NoSuchMethodError();
    }

    public final Game c() {
        throw new NoSuchMethodError();
    }

    public final String d() {
        throw new NoSuchMethodError();
    }

    public final int describeContents() {
        throw new NoSuchMethodError();
    }

    public final String e() {
        throw new NoSuchMethodError();
    }

    public final boolean equals(Object obj) {
        throw new NoSuchMethodError();
    }

    public final Uri f() {
        throw new NoSuchMethodError();
    }

    public final long g() {
        throw new NoSuchMethodError();
    }

    public final String getIconImageUrl() {
        return e("icon_url");
    }

    public final long h() {
        throw new NoSuchMethodError();
    }

    public final int hashCode() {
        throw new NoSuchMethodError();
    }

    public final long i() {
        throw new NoSuchMethodError();
    }

    public final int j() {
        throw new NoSuchMethodError();
    }

    public final int k() {
        throw new NoSuchMethodError();
    }

    public final String toString() {
        throw new NoSuchMethodError();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        throw new NoSuchMethodError();
    }
}
