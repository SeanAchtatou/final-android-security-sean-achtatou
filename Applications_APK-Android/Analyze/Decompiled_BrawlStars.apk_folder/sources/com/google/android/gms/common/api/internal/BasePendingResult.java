package com.google.android.gms.common.api.internal;

import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.util.Pair;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.d;
import com.google.android.gms.common.api.e;
import com.google.android.gms.common.api.f;
import com.google.android.gms.common.api.g;
import com.google.android.gms.common.api.h;
import com.google.android.gms.common.internal.ICancelToken;
import com.google.android.gms.common.internal.l;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

public abstract class BasePendingResult<R extends g> extends e<R> {
    static final ThreadLocal<Boolean> c = new bz();

    /* renamed from: a  reason: collision with root package name */
    private final Object f1378a;

    /* renamed from: b  reason: collision with root package name */
    private final a<R> f1379b;
    private final WeakReference<d> d;
    private final CountDownLatch e;
    private final ArrayList<e.a> f;
    private h<? super R> g;
    private final AtomicReference<bo> h;
    /* access modifiers changed from: private */
    public R i;
    private Status j;
    private volatile boolean k;
    private boolean l;
    private boolean m;
    private b mResultGuardian;
    private ICancelToken n;
    private volatile bj<R> o;
    private boolean p;

    final class b {
        private b() {
        }

        /* access modifiers changed from: protected */
        public final void finalize() throws Throwable {
            BasePendingResult.b(BasePendingResult.this.i);
            super.finalize();
        }

        /* synthetic */ b(BasePendingResult basePendingResult, byte b2) {
            this();
        }
    }

    @Deprecated
    BasePendingResult() {
        this.f1378a = new Object();
        this.e = new CountDownLatch(1);
        this.f = new ArrayList<>();
        this.h = new AtomicReference<>();
        this.p = false;
        this.f1379b = new a<>(Looper.getMainLooper());
        this.d = new WeakReference<>(null);
    }

    /* access modifiers changed from: protected */
    public abstract R c(Status status);

    public final Integer d() {
        return null;
    }

    public static class a<R extends g> extends com.google.android.gms.internal.base.d {
        public a() {
            this(Looper.getMainLooper());
        }

        public a(Looper looper) {
            super(looper);
        }

        public final void a(h<? super R> hVar, R r) {
            sendMessage(obtainMessage(1, new Pair(hVar, r)));
        }

        public void handleMessage(Message message) {
            int i = message.what;
            if (i == 1) {
                Pair pair = (Pair) message.obj;
                h hVar = (h) pair.first;
                g gVar = (g) pair.second;
                try {
                    hVar.a(gVar);
                } catch (RuntimeException e) {
                    BasePendingResult.b(gVar);
                    throw e;
                }
            } else if (i != 2) {
                int i2 = message.what;
                StringBuilder sb = new StringBuilder(45);
                sb.append("Don't know how to handle message: ");
                sb.append(i2);
                Log.wtf("BasePendingResult", sb.toString(), new Exception());
            } else {
                ((BasePendingResult) message.obj).b(Status.d);
            }
        }
    }

    protected BasePendingResult(d dVar) {
        this.f1378a = new Object();
        this.e = new CountDownLatch(1);
        this.f = new ArrayList<>();
        this.h = new AtomicReference<>();
        this.p = false;
        this.f1379b = new a<>(dVar != null ? dVar.a() : Looper.getMainLooper());
        this.d = new WeakReference<>(dVar);
    }

    public final boolean e() {
        return this.e.getCount() == 0;
    }

    public final R a() {
        l.c("await must not be called on the UI thread");
        boolean z = true;
        l.a(!this.k, "Result has already been consumed");
        if (this.o != null) {
            z = false;
        }
        l.a(z, "Cannot await if then() has been called.");
        try {
            this.e.await();
        } catch (InterruptedException unused) {
            b(Status.f1369b);
        }
        l.a(e(), "Result is not ready.");
        return h();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x003e, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.google.android.gms.common.api.h<? super R> r6) {
        /*
            r5 = this;
            java.lang.Object r0 = r5.f1378a
            monitor-enter(r0)
            if (r6 != 0) goto L_0x000a
            r6 = 0
            r5.g = r6     // Catch:{ all -> 0x003f }
            monitor-exit(r0)     // Catch:{ all -> 0x003f }
            return
        L_0x000a:
            boolean r1 = r5.k     // Catch:{ all -> 0x003f }
            r2 = 1
            r3 = 0
            if (r1 != 0) goto L_0x0012
            r1 = 1
            goto L_0x0013
        L_0x0012:
            r1 = 0
        L_0x0013:
            java.lang.String r4 = "Result has already been consumed."
            com.google.android.gms.common.internal.l.a(r1, r4)     // Catch:{ all -> 0x003f }
            com.google.android.gms.common.api.internal.bj<R> r1 = r5.o     // Catch:{ all -> 0x003f }
            if (r1 != 0) goto L_0x001d
            goto L_0x001e
        L_0x001d:
            r2 = 0
        L_0x001e:
            java.lang.String r1 = "Cannot set callbacks if then() has been called."
            com.google.android.gms.common.internal.l.a(r2, r1)     // Catch:{ all -> 0x003f }
            boolean r1 = r5.c()     // Catch:{ all -> 0x003f }
            if (r1 == 0) goto L_0x002b
            monitor-exit(r0)     // Catch:{ all -> 0x003f }
            return
        L_0x002b:
            boolean r1 = r5.e()     // Catch:{ all -> 0x003f }
            if (r1 == 0) goto L_0x003b
            com.google.android.gms.common.api.internal.BasePendingResult$a<R> r1 = r5.f1379b     // Catch:{ all -> 0x003f }
            com.google.android.gms.common.api.g r2 = r5.h()     // Catch:{ all -> 0x003f }
            r1.a(r6, r2)     // Catch:{ all -> 0x003f }
            goto L_0x003d
        L_0x003b:
            r5.g = r6     // Catch:{ all -> 0x003f }
        L_0x003d:
            monitor-exit(r0)     // Catch:{ all -> 0x003f }
            return
        L_0x003f:
            r6 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x003f }
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.BasePendingResult.a(com.google.android.gms.common.api.h):void");
    }

    public final void a(e.a aVar) {
        l.b(true, "Callback cannot be null.");
        synchronized (this.f1378a) {
            if (e()) {
                aVar.a();
            } else {
                this.f.add(aVar);
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:8|(2:10|11)|12|13|14|15) */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0029, code lost:
        return;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:12:0x0015 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b() {
        /*
            r2 = this;
            java.lang.Object r0 = r2.f1378a
            monitor-enter(r0)
            boolean r1 = r2.l     // Catch:{ all -> 0x002a }
            if (r1 != 0) goto L_0x0028
            boolean r1 = r2.k     // Catch:{ all -> 0x002a }
            if (r1 == 0) goto L_0x000c
            goto L_0x0028
        L_0x000c:
            com.google.android.gms.common.internal.ICancelToken r1 = r2.n     // Catch:{ all -> 0x002a }
            if (r1 == 0) goto L_0x0015
            com.google.android.gms.common.internal.ICancelToken r1 = r2.n     // Catch:{ RemoteException -> 0x0015 }
            r1.a()     // Catch:{ RemoteException -> 0x0015 }
        L_0x0015:
            R r1 = r2.i     // Catch:{ all -> 0x002a }
            b(r1)     // Catch:{ all -> 0x002a }
            r1 = 1
            r2.l = r1     // Catch:{ all -> 0x002a }
            com.google.android.gms.common.api.Status r1 = com.google.android.gms.common.api.Status.e     // Catch:{ all -> 0x002a }
            com.google.android.gms.common.api.g r1 = r2.c(r1)     // Catch:{ all -> 0x002a }
            r2.c(r1)     // Catch:{ all -> 0x002a }
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            return
        L_0x0028:
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            return
        L_0x002a:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.BasePendingResult.b():void");
    }

    public final boolean f() {
        boolean c2;
        synchronized (this.f1378a) {
            if (this.d.get() == null || !this.p) {
                b();
            }
            c2 = c();
        }
        return c2;
    }

    public final boolean c() {
        boolean z;
        synchronized (this.f1378a) {
            z = this.l;
        }
        return z;
    }

    public final void a(R r) {
        synchronized (this.f1378a) {
            if (this.m || this.l) {
                b((g) r);
                return;
            }
            e();
            boolean z = true;
            l.a(!e(), "Results have already been set");
            if (this.k) {
                z = false;
            }
            l.a(z, "Result has already been consumed");
            c((g) r);
        }
    }

    public final void b(Status status) {
        synchronized (this.f1378a) {
            if (!e()) {
                a(c(status));
                this.m = true;
            }
        }
    }

    public final void a(bo boVar) {
        this.h.set(boVar);
    }

    public final void g() {
        this.p = this.p || c.get().booleanValue();
    }

    private final R h() {
        R r;
        synchronized (this.f1378a) {
            l.a(!this.k, "Result has already been consumed.");
            l.a(e(), "Result is not ready.");
            r = this.i;
            this.i = null;
            this.g = null;
            this.k = true;
        }
        bo andSet = this.h.getAndSet(null);
        if (andSet != null) {
            andSet.a(this);
        }
        return r;
    }

    private final void c(R r) {
        this.i = r;
        this.n = null;
        this.e.countDown();
        this.j = this.i.b();
        int i2 = 0;
        if (this.l) {
            this.g = null;
        } else if (this.g != null) {
            this.f1379b.removeMessages(2);
            this.f1379b.a(this.g, h());
        } else if (this.i instanceof f) {
            this.mResultGuardian = new b(this, (byte) 0);
        }
        ArrayList arrayList = this.f;
        int size = arrayList.size();
        while (i2 < size) {
            Object obj = arrayList.get(i2);
            i2++;
            ((e.a) obj).a();
        }
        this.f.clear();
    }

    public static void b(g gVar) {
        if (gVar instanceof f) {
            try {
                ((f) gVar).a();
            } catch (RuntimeException unused) {
                String valueOf = String.valueOf(gVar);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 18);
                sb.append("Unable to release ");
                sb.append(valueOf);
                sb.toString();
            }
        }
    }
}
