package com.google.android.gms.common.a;

public abstract class a<T> {
    private static final Object c = new Object();
    private static C0110a d = null;
    private static int e = 0;

    /* renamed from: a  reason: collision with root package name */
    protected final String f1361a;

    /* renamed from: b  reason: collision with root package name */
    protected final T f1362b;
    private T f = null;

    /* renamed from: com.google.android.gms.common.a.a$a  reason: collision with other inner class name */
    interface C0110a {
    }

    protected a(String str, T t) {
        this.f1361a = str;
        this.f1362b = t;
    }

    public static a<Boolean> a(String str, boolean z) {
        return new b(str, Boolean.valueOf(z));
    }

    public static a<Long> a(String str, Long l) {
        return new c(str, l);
    }

    public static a<Integer> a(String str, Integer num) {
        return new d(str, num);
    }

    public static a<Float> a(String str, Float f2) {
        return new e(str, f2);
    }

    public static a<String> a(String str, String str2) {
        return new f(str, str2);
    }
}
