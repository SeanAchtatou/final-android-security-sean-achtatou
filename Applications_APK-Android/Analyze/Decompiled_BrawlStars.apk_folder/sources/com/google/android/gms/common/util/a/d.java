package com.google.android.gms.common.util.a;

import android.os.Process;

final class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final Runnable f1671a;

    /* renamed from: b  reason: collision with root package name */
    private final int f1672b = 0;

    public d(Runnable runnable, int i) {
        this.f1671a = runnable;
    }

    public final void run() {
        Process.setThreadPriority(this.f1672b);
        this.f1671a.run();
    }
}
