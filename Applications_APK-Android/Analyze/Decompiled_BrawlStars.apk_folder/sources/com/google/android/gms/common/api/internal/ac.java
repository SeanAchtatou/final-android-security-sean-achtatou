package com.google.android.gms.common.api.internal;

import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.IAccountAccessor;
import com.google.android.gms.common.internal.ResolveAccountResponse;
import com.google.android.gms.signin.internal.zaj;

final class ac extends an {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ u f1386a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ zaj f1387b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ac(al alVar, u uVar, zaj zaj) {
        super(alVar);
        this.f1386a = uVar;
        this.f1387b = zaj;
    }

    public final void a() {
        u uVar = this.f1386a;
        zaj zaj = this.f1387b;
        if (uVar.b(0)) {
            ConnectionResult connectionResult = zaj.f2613a;
            if (connectionResult.b()) {
                ResolveAccountResponse resolveAccountResponse = zaj.f2614b;
                ConnectionResult connectionResult2 = resolveAccountResponse.f1582b;
                if (!connectionResult2.b()) {
                    String valueOf = String.valueOf(connectionResult2);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 48);
                    sb.append("Sign-in succeeded with resolve account failure: ");
                    sb.append(valueOf);
                    Log.wtf("GoogleApiClientConnecting", sb.toString(), new Exception());
                    uVar.b(connectionResult2);
                    return;
                }
                uVar.g = true;
                uVar.h = IAccountAccessor.Stub.a(resolveAccountResponse.f1581a);
                uVar.i = resolveAccountResponse.c;
                uVar.j = resolveAccountResponse.d;
                uVar.e();
            } else if (uVar.a(connectionResult)) {
                uVar.f();
                uVar.e();
            } else {
                uVar.b(connectionResult);
            }
        }
    }
}
