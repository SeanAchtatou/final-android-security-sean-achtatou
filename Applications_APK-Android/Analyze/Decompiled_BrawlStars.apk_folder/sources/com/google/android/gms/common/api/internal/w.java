package com.google.android.gms.common.api.internal;

import android.os.Looper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.internal.BaseGmsClient;
import com.google.android.gms.common.internal.l;
import java.lang.ref.WeakReference;

final class w implements BaseGmsClient.c {

    /* renamed from: a  reason: collision with root package name */
    final boolean f1499a;

    /* renamed from: b  reason: collision with root package name */
    private final WeakReference<u> f1500b;
    private final a<?> c;

    public w(u uVar, a<?> aVar, boolean z) {
        this.f1500b = new WeakReference<>(uVar);
        this.c = aVar;
        this.f1499a = z;
    }

    public final void a(ConnectionResult connectionResult) {
        u uVar = this.f1500b.get();
        if (uVar != null) {
            l.a(Looper.myLooper() == uVar.f1496a.m.a(), "onReportServiceBinding must be called on the GoogleApiClient handler thread");
            uVar.f1497b.lock();
            try {
                if (uVar.b(0)) {
                    if (!connectionResult.b()) {
                        uVar.b(connectionResult, this.c, this.f1499a);
                    }
                    if (uVar.d()) {
                        uVar.e();
                    }
                    uVar.f1497b.unlock();
                }
            } finally {
                uVar.f1497b.unlock();
            }
        }
    }
}
