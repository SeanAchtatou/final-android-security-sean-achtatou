package com.google.android.gms.tasks;

final class v implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ g f2704a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ u f2705b;

    v(u uVar, g gVar) {
        this.f2705b = uVar;
        this.f2704a = gVar;
    }

    public final void run() {
        synchronized (this.f2705b.f2702a) {
            if (this.f2705b.f2703b != null) {
                this.f2705b.f2703b.a(this.f2704a.d());
            }
        }
    }
}
