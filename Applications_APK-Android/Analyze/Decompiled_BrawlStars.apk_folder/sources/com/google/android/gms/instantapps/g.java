package com.google.android.gms.instantapps;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class g implements Parcelable.Creator<LaunchData> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new LaunchData[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        Intent intent = null;
        String str = null;
        String str2 = null;
        BitmapTeleporter bitmapTeleporter = null;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            int i = 65535 & readInt;
            if (i == 2) {
                intent = (Intent) SafeParcelReader.a(parcel, readInt, Intent.CREATOR);
            } else if (i == 3) {
                str = SafeParcelReader.l(parcel, readInt);
            } else if (i == 4) {
                str2 = SafeParcelReader.l(parcel, readInt);
            } else if (i != 5) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                bitmapTeleporter = (BitmapTeleporter) SafeParcelReader.a(parcel, readInt, BitmapTeleporter.CREATOR);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new LaunchData(intent, str, str2, bitmapTeleporter);
    }
}
