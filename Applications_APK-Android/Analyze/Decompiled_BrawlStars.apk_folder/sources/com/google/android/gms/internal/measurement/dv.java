package com.google.android.gms.internal.measurement;

final class dv extends ds<Boolean> {
    dv(dy dyVar, String str, Boolean bool) {
        super(dyVar, str, bool, (byte) 0);
    }

    /* access modifiers changed from: package-private */
    public final /* synthetic */ Object a(Object obj) {
        if (obj instanceof Boolean) {
            return (Boolean) obj;
        }
        if (obj instanceof String) {
            String str = (String) obj;
            if (de.f2158b.matcher(str).matches()) {
                return true;
            }
            if (de.c.matcher(str).matches()) {
                return false;
            }
        }
        String b2 = super.b();
        String valueOf = String.valueOf(obj);
        StringBuilder sb = new StringBuilder(String.valueOf(b2).length() + 28 + String.valueOf(valueOf).length());
        sb.append("Invalid boolean value for ");
        sb.append(b2);
        sb.append(": ");
        sb.append(valueOf);
        sb.toString();
        return null;
    }
}
