package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.util.e;
import java.util.concurrent.atomic.AtomicReference;

public final class m extends bn {

    /* renamed from: a  reason: collision with root package name */
    private static final AtomicReference<String[]> f2572a = new AtomicReference<>();

    /* renamed from: b  reason: collision with root package name */
    private static final AtomicReference<String[]> f2573b = new AtomicReference<>();
    private static final AtomicReference<String[]> c = new AtomicReference<>();

    m(ar arVar) {
        super(arVar);
    }

    /* access modifiers changed from: protected */
    public final boolean d() {
        return false;
    }

    private final boolean f() {
        return TextUtils.isEmpty(this.r.f2387a) && this.r.q().a(3);
    }

    /* access modifiers changed from: protected */
    public final String a(String str) {
        if (str == null) {
            return null;
        }
        if (!f()) {
            return str;
        }
        return a(str, bp.f2433b, bp.f2432a, f2572a);
    }

    /* access modifiers changed from: protected */
    public final String b(String str) {
        if (str == null) {
            return null;
        }
        if (!f()) {
            return str;
        }
        return a(str, bq.f2435b, bq.f2434a, f2573b);
    }

    /* access modifiers changed from: protected */
    public final String c(String str) {
        if (str == null) {
            return null;
        }
        if (!f()) {
            return str;
        }
        if (!str.startsWith("_exp_")) {
            return a(str, br.f2437b, br.f2436a, c);
        }
        return "experiment_id" + "(" + str + ")";
    }

    private static String a(String str, String[] strArr, String[] strArr2, AtomicReference<String[]> atomicReference) {
        String str2;
        l.a(strArr);
        l.a(strArr2);
        l.a(atomicReference);
        l.b(strArr.length == strArr2.length);
        for (int i = 0; i < strArr.length; i++) {
            if (ed.c(str, strArr[i])) {
                synchronized (atomicReference) {
                    String[] strArr3 = atomicReference.get();
                    if (strArr3 == null) {
                        strArr3 = new String[strArr2.length];
                        atomicReference.set(strArr3);
                    }
                    if (strArr3[i] == null) {
                        strArr3[i] = strArr2[i] + "(" + strArr[i] + ")";
                    }
                    str2 = strArr3[i];
                }
                return str2;
            }
        }
        return str;
    }

    /* access modifiers changed from: protected */
    public final String a(zzag zzag) {
        if (zzag == null) {
            return null;
        }
        if (!f()) {
            return zzag.toString();
        }
        return "origin=" + zzag.c + ",name=" + a(zzag.f2595a) + ",params=" + a(zzag.f2596b);
    }

    /* access modifiers changed from: protected */
    public final String a(c cVar) {
        if (cVar == null) {
            return null;
        }
        if (!f()) {
            return cVar.toString();
        }
        return "Event{appId='" + cVar.f2452a + "', name='" + a(cVar.f2453b) + "', params=" + a(cVar.e) + "}";
    }

    private final String a(zzad zzad) {
        if (zzad == null) {
            return null;
        }
        if (!f()) {
            return zzad.toString();
        }
        return a(zzad.a());
    }

    /* access modifiers changed from: protected */
    public final String a(Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        if (!f()) {
            return bundle.toString();
        }
        StringBuilder sb = new StringBuilder();
        for (String next : bundle.keySet()) {
            if (sb.length() != 0) {
                sb.append(", ");
            } else {
                sb.append("Bundle[{");
            }
            sb.append(b(next));
            sb.append("=");
            sb.append(bundle.get(next));
        }
        sb.append("}]");
        return sb.toString();
    }

    public final /* bridge */ /* synthetic */ void a() {
        super.a();
    }

    public final /* bridge */ /* synthetic */ void b() {
        super.b();
    }

    public final /* bridge */ /* synthetic */ void c() {
        super.c();
    }

    public final /* bridge */ /* synthetic */ b k() {
        return super.k();
    }

    public final /* bridge */ /* synthetic */ e l() {
        return super.l();
    }

    public final /* bridge */ /* synthetic */ Context m() {
        return super.m();
    }

    public final /* bridge */ /* synthetic */ m n() {
        return super.n();
    }

    public final /* bridge */ /* synthetic */ ed o() {
        return super.o();
    }

    public final /* bridge */ /* synthetic */ am p() {
        return super.p();
    }

    public final /* bridge */ /* synthetic */ o q() {
        return super.q();
    }

    public final /* bridge */ /* synthetic */ z r() {
        return super.r();
    }

    public final /* bridge */ /* synthetic */ el s() {
        return super.s();
    }
}
