package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.drive.zzu;

public final class zzfn extends zzu {
    public static final Parcelable.Creator<zzfn> CREATOR = new z();

    /* renamed from: a  reason: collision with root package name */
    final DataHolder f1946a;

    /* renamed from: b  reason: collision with root package name */
    final boolean f1947b;

    public zzfn(DataHolder dataHolder, boolean z) {
        this.f1946a = dataHolder;
        this.f1947b = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.common.data.DataHolder, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void a(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.a(parcel, 2, (Parcelable) this.f1946a, i, false);
        a.a(parcel, 3, this.f1947b);
        a.b(parcel, a2);
    }
}
