package com.google.android.gms.games.internal;

import com.google.android.gms.common.a.a;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    private static final com.google.android.gms.common.internal.h f1811a = new com.google.android.gms.common.internal.h("Games");

    /* renamed from: b  reason: collision with root package name */
    private static final a<Boolean> f1812b = a.a("games.play_games_dogfood", false);

    public static void a(String str, String str2) {
        f1811a.a(str, str2);
    }

    public static void b(String str, String str2) {
        f1811a.b(str, str2);
    }

    public static void a(String str, String str2, Throwable th) {
        com.google.android.gms.common.internal.h hVar = f1811a;
        if (hVar.a(5)) {
            hVar.a(str2);
        }
    }
}
