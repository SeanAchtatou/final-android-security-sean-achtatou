package com.google.android.gms.games;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class i implements Parcelable.Creator<PlayerLevel> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new PlayerLevel[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        long j = 0;
        long j2 = 0;
        int i = 0;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            int i2 = 65535 & readInt;
            if (i2 == 1) {
                i = SafeParcelReader.d(parcel, readInt);
            } else if (i2 == 2) {
                j = SafeParcelReader.f(parcel, readInt);
            } else if (i2 != 3) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                j2 = SafeParcelReader.f(parcel, readInt);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new PlayerLevel(i, j, j2);
    }
}
