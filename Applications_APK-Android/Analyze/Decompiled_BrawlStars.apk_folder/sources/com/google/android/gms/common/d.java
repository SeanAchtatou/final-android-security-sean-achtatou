package com.google.android.gms.common;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import com.google.android.gms.common.c.c;
import com.google.android.gms.common.internal.ae;
import com.google.android.gms.common.util.h;

public class d {

    /* renamed from: a  reason: collision with root package name */
    private static final d f1521a = new d();

    /* renamed from: b  reason: collision with root package name */
    public static final int f1522b = f.f1536b;

    public static d b() {
        return f1521a;
    }

    d() {
    }

    public int a(Context context) {
        return a(context, f1522b);
    }

    public int a(Context context, int i) {
        int a2 = f.a(context, i);
        if (f.b(context, a2)) {
            return 18;
        }
        return a2;
    }

    public boolean a(int i) {
        return f.b(i);
    }

    public Intent a(Context context, int i, String str) {
        if (i == 1 || i == 2) {
            if (context == null || !h.b(context)) {
                return ae.a("com.google.android.gms", a(context, str));
            }
            return ae.a();
        } else if (i != 3) {
            return null;
        } else {
            return ae.a("com.google.android.gms");
        }
    }

    public PendingIntent a(Context context, int i, int i2) {
        return a(context, i, i2, null);
    }

    public final PendingIntent a(Context context, int i, int i2, String str) {
        Intent a2 = a(context, i, str);
        if (a2 == null) {
            return null;
        }
        return PendingIntent.getActivity(context, i2, a2, 134217728);
    }

    public static void b(Context context) {
        f.d(context);
    }

    public static int c(Context context) {
        return f.g(context);
    }

    public static boolean b(Context context, int i) {
        return f.b(context, i);
    }

    public String b(int i) {
        return f.a(i);
    }

    private static String a(Context context, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("gcore_");
        sb.append(f1522b);
        sb.append("-");
        if (!TextUtils.isEmpty(str)) {
            sb.append(str);
        }
        sb.append("-");
        if (context != null) {
            sb.append(context.getPackageName());
        }
        sb.append("-");
        if (context != null) {
            try {
                sb.append(c.a(context).b(context.getPackageName(), 0).versionCode);
            } catch (PackageManager.NameNotFoundException unused) {
            }
        }
        return sb.toString();
    }
}
