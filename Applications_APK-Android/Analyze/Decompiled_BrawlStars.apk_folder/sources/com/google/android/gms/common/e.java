package com.google.android.gms.common;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;

public final class e extends f {
    @Deprecated

    /* renamed from: a  reason: collision with root package name */
    public static final int f1534a = f.f1536b;

    public static Resources b(Context context) {
        return f.e(context);
    }

    @Deprecated
    public static Dialog a(int i, Activity activity, int i2) {
        if (f.b(activity, i)) {
            i = 18;
        }
        return c.a().a(activity, i, 0, (DialogInterface.OnCancelListener) null);
    }

    @Deprecated
    public static int a(Context context) {
        return f.a(context, f.f1536b);
    }
}
