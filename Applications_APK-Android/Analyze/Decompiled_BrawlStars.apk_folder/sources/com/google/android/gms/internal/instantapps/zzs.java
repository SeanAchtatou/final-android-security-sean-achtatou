package com.google.android.gms.internal.instantapps;

import android.content.pm.PackageInfo;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.BitmapTeleporter;
import com.google.android.gms.instantapps.LaunchData;

public abstract class zzs extends zzb implements zzr {
    public zzs() {
        super("com.google.android.gms.instantapps.internal.IInstantAppsCallbacks");
    }

    /* access modifiers changed from: protected */
    public final boolean a(int i, Parcel parcel) throws RemoteException {
        if (i == 2) {
            l.a(parcel, Status.CREATOR);
            l.a(parcel, zzv.CREATOR);
            a();
            return true;
        } else if (i == 9) {
            l.a(parcel, Status.CREATOR);
            l.a(parcel, zzao.CREATOR);
            b();
            return true;
        } else if (i == 10) {
            parcel.readInt();
            c();
            return true;
        } else if (i == 12) {
            l.a(parcel, Status.CREATOR);
            parcel.readInt();
            d();
            return true;
        } else if (i != 13) {
            switch (i) {
                case 18:
                    l.a(parcel, Status.CREATOR);
                    l.a(parcel, PackageInfo.CREATOR);
                    f();
                    return true;
                case 19:
                    l.a(parcel, Status.CREATOR);
                    l.a(parcel, LaunchData.CREATOR);
                    g();
                    return true;
                case 20:
                    l.a(parcel, Status.CREATOR);
                    parcel.createTypedArrayList(zzax.CREATOR);
                    h();
                    return true;
                case 21:
                    l.a(parcel, Status.CREATOR);
                    l.a(parcel, ParcelFileDescriptor.CREATOR);
                    i();
                    return true;
                case 22:
                    l.a(parcel, Status.CREATOR);
                    l.a(parcel, BitmapTeleporter.CREATOR);
                    j();
                    return true;
                case 23:
                    l.a(parcel, Status.CREATOR);
                    l.a(parcel, zzo.CREATOR);
                    k();
                    return true;
                case 24:
                    l.a(parcel, Status.CREATOR);
                    parcel.createByteArray();
                    l();
                    return true;
                case 25:
                    l.a(parcel, Status.CREATOR);
                    l.a(parcel);
                    m();
                    return true;
                case 26:
                    l.a(parcel, Status.CREATOR);
                    l.a(parcel);
                    n();
                    return true;
                case 27:
                    l.a(parcel, Status.CREATOR);
                    l.a(parcel);
                    o();
                    return true;
                default:
                    return false;
            }
        } else {
            l.a(parcel, Status.CREATOR);
            l.a(parcel, zzal.CREATOR);
            e();
            return true;
        }
    }
}
