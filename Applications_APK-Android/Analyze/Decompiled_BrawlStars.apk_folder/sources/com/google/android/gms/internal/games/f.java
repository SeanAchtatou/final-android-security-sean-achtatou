package com.google.android.gms.internal.games;

import com.google.android.gms.common.api.d;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.a;
import com.google.android.gms.games.g;

public final class f implements g {
    public final String a(d dVar) {
        return a.a(dVar).r();
    }

    public final Player b(d dVar) {
        return a.a(dVar).s();
    }
}
