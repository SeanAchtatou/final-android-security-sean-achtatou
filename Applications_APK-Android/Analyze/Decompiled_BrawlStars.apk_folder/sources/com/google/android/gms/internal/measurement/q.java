package com.google.android.gms.internal.measurement;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.facebook.internal.ServerProtocol;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.util.e;

public class q {
    public final t c;

    protected q(t tVar) {
        l.a(tVar);
        this.c = tVar;
    }

    /* access modifiers changed from: protected */
    public final e f() {
        return this.c.c;
    }

    /* access modifiers changed from: protected */
    public final Context g() {
        return this.c.f2332a;
    }

    /* access modifiers changed from: protected */
    public final au h() {
        return this.c.d;
    }

    /* access modifiers changed from: protected */
    public final az i() {
        t tVar = this.c;
        t.a(tVar.f);
        return tVar.f;
    }

    /* access modifiers changed from: protected */
    public final bo j() {
        t tVar = this.c;
        t.a(tVar.g);
        return tVar.g;
    }

    public final void b(String str) {
        a(2, str, null, null, null);
    }

    public final void a(String str, Object obj) {
        a(2, str, obj, null, null);
    }

    public final void a(String str, Object obj, Object obj2) {
        a(2, str, obj, obj2, null);
    }

    public final void c(String str) {
        a(3, str, null, null, null);
    }

    public final void b(String str, Object obj) {
        a(3, str, obj, null, null);
    }

    public final void b(String str, Object obj, Object obj2) {
        a(3, str, obj, obj2, null);
    }

    public final void a(String str, Object obj, Object obj2, Object obj3) {
        a(3, str, obj, obj2, obj3);
    }

    public final void d(String str) {
        a(4, str, null, null, null);
    }

    public final void c(String str, Object obj) {
        a(4, str, obj, null, null);
    }

    public final void e(String str) {
        a(5, str, null, null, null);
    }

    public final void d(String str, Object obj) {
        a(5, str, obj, null, null);
    }

    public final void c(String str, Object obj, Object obj2) {
        a(5, str, obj, obj2, null);
    }

    public final void b(String str, Object obj, Object obj2, Object obj3) {
        a(5, str, obj, obj2, obj3);
    }

    public final void f(String str) {
        a(6, str, null, null, null);
    }

    public final void e(String str, Object obj) {
        a(6, str, obj, null, null);
    }

    public final void d(String str, Object obj, Object obj2) {
        a(6, str, obj, obj2, null);
    }

    public static boolean k() {
        return Log.isLoggable((String) bc.f2066b.f2067a, 2);
    }

    private final void a(int i, String str, Object obj, Object obj2, Object obj3) {
        bk bkVar;
        t tVar = this.c;
        if (tVar != null) {
            bkVar = tVar.e;
        } else {
            bkVar = null;
        }
        bk bkVar2 = bkVar;
        if (bkVar2 != null) {
            String str2 = (String) bc.f2066b.f2067a;
            if (Log.isLoggable(str2, i)) {
                Log.println(i, str2, bk.c(str, obj, obj2, obj3));
            }
            if (i >= 5) {
                bkVar2.a(i, str, obj, obj2, obj3);
                return;
            }
            return;
        }
        String str3 = (String) bc.f2066b.f2067a;
        if (Log.isLoggable(str3, i)) {
            Log.println(i, str3, c(str, obj, obj2, obj3));
        }
    }

    protected static String c(String str, Object obj, Object obj2, Object obj3) {
        String str2 = "";
        if (str == null) {
            str = str2;
        }
        String a2 = a(obj);
        String a3 = a(obj2);
        String a4 = a(obj3);
        StringBuilder sb = new StringBuilder();
        if (!TextUtils.isEmpty(str)) {
            sb.append(str);
            str2 = ": ";
        }
        if (!TextUtils.isEmpty(a2)) {
            sb.append(str2);
            sb.append(a2);
            str2 = ", ";
        }
        if (!TextUtils.isEmpty(a3)) {
            sb.append(str2);
            sb.append(a3);
            str2 = ", ";
        }
        if (!TextUtils.isEmpty(a4)) {
            sb.append(str2);
            sb.append(a4);
        }
        return sb.toString();
    }

    private static String a(Object obj) {
        if (obj == null) {
            return "";
        }
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof Boolean) {
            return obj == Boolean.TRUE ? ServerProtocol.DIALOG_RETURN_SCOPES_TRUE : "false";
        }
        if (obj instanceof Throwable) {
            return ((Throwable) obj).toString();
        }
        return obj.toString();
    }
}
