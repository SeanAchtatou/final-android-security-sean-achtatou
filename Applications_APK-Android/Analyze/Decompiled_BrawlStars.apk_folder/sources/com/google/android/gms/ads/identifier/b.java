package com.google.android.gms.ads.identifier;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.android.gms.common.f;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private SharedPreferences f1307a;

    public b(Context context) {
        try {
            Context f = f.f(context);
            this.f1307a = f == null ? null : f.getSharedPreferences("google_ads_flags", 0);
        } catch (Throwable unused) {
            this.f1307a = null;
        }
    }

    /* access modifiers changed from: package-private */
    public final String a(String str, String str2) {
        try {
            return this.f1307a == null ? str2 : this.f1307a.getString(str, str2);
        } catch (Throwable unused) {
            return str2;
        }
    }

    public final boolean a(String str) {
        try {
            if (this.f1307a == null) {
                return false;
            }
            return this.f1307a.getBoolean(str, false);
        } catch (Throwable unused) {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public final float b(String str) {
        try {
            if (this.f1307a == null) {
                return 0.0f;
            }
            return this.f1307a.getFloat(str, 0.0f);
        } catch (Throwable unused) {
            return 0.0f;
        }
    }
}
