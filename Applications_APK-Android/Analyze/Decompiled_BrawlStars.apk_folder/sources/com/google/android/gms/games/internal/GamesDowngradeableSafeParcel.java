package com.google.android.gms.games.internal;

import com.google.android.gms.common.internal.DowngradeableSafeParcel;
import com.google.android.gms.common.util.i;

public abstract class GamesDowngradeableSafeParcel extends DowngradeableSafeParcel {
    public static boolean b(Integer num) {
        if (num == null) {
            return false;
        }
        return i.a(num.intValue());
    }
}
