package com.google.android.gms.internal.measurement;

import java.util.ArrayList;
import java.util.List;

final class hg<E> extends ef<E> {

    /* renamed from: b  reason: collision with root package name */
    private static final hg<Object> f2260b;
    private final List<E> c;

    public static <E> hg<E> d() {
        return f2260b;
    }

    hg() {
        this(new ArrayList(10));
    }

    private hg(List<E> list) {
        this.c = list;
    }

    public final void add(int i, E e) {
        c();
        this.c.add(i, e);
        this.modCount++;
    }

    public final E get(int i) {
        return this.c.get(i);
    }

    public final E remove(int i) {
        c();
        E remove = this.c.remove(i);
        this.modCount++;
        return remove;
    }

    public final E set(int i, E e) {
        c();
        E e2 = this.c.set(i, e);
        this.modCount++;
        return e2;
    }

    public final int size() {
        return this.c.size();
    }

    public final /* synthetic */ fw a(int i) {
        if (i >= size()) {
            ArrayList arrayList = new ArrayList(i);
            arrayList.addAll(this.c);
            return new hg(arrayList);
        }
        throw new IllegalArgumentException();
    }

    static {
        hg<Object> hgVar = new hg<>();
        f2260b = hgVar;
        hgVar.f2180a = false;
    }
}
