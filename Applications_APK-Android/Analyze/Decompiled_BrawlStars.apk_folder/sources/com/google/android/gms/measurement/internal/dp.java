package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.util.e;

final class dp {

    /* renamed from: a  reason: collision with root package name */
    final e f2520a;

    /* renamed from: b  reason: collision with root package name */
    long f2521b;

    public dp(e eVar) {
        l.a(eVar);
        this.f2520a = eVar;
    }

    public final void a() {
        this.f2521b = this.f2520a.b();
    }
}
