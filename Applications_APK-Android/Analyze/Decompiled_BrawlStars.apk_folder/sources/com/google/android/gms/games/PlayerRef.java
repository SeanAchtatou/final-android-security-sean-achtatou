package com.google.android.gms.games;

import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.d;
import com.google.android.gms.games.internal.player.b;
import com.google.android.gms.games.internal.player.zza;
import com.google.android.gms.games.internal.player.zzd;

public final class PlayerRef extends d implements Player {
    private final b c;
    private final PlayerLevelInfo d;
    private final zzd e;

    public PlayerRef(DataHolder dataHolder, int i) {
        this(dataHolder, i, null);
    }

    public PlayerRef(DataHolder dataHolder, int i, String str) {
        super(dataHolder, i);
        PlayerLevelInfo playerLevelInfo;
        this.c = new b(str);
        this.e = new zzd(dataHolder, i, this.c);
        if (!i(this.c.j) && b(this.c.j) != -1) {
            int c2 = c(this.c.k);
            int c3 = c(this.c.n);
            PlayerLevel playerLevel = new PlayerLevel(c2, b(this.c.l), b(this.c.m));
            playerLevelInfo = new PlayerLevelInfo(b(this.c.j), b(this.c.p), playerLevel, c2 != c3 ? new PlayerLevel(c3, b(this.c.m), b(this.c.o)) : playerLevel);
        } else {
            playerLevelInfo = null;
        }
        this.d = playerLevelInfo;
    }

    public final /* synthetic */ Object a() {
        return new PlayerEntity(this);
    }

    public final String b() {
        return e(this.c.f1815a);
    }

    public final String c() {
        return e(this.c.f1816b);
    }

    public final String d() {
        return e(this.c.z);
    }

    public final int describeContents() {
        return 0;
    }

    public final String e() {
        return e(this.c.A);
    }

    public final boolean equals(Object obj) {
        return PlayerEntity.a(this, obj);
    }

    public final boolean f() {
        return d(this.c.y);
    }

    public final Uri g() {
        return h(this.c.c);
    }

    public final String getBannerImageLandscapeUrl() {
        return e(this.c.C);
    }

    public final String getBannerImagePortraitUrl() {
        return e(this.c.E);
    }

    public final String getHiResImageUrl() {
        return e(this.c.f);
    }

    public final String getIconImageUrl() {
        return e(this.c.d);
    }

    public final Uri h() {
        return h(this.c.e);
    }

    public final int hashCode() {
        return PlayerEntity.a(this);
    }

    public final long i() {
        return b(this.c.g);
    }

    public final long j() {
        if (!a_(this.c.i) || i(this.c.i)) {
            return -1;
        }
        return b(this.c.i);
    }

    public final int k() {
        return c(this.c.h);
    }

    public final boolean l() {
        return d(this.c.r);
    }

    public final String m() {
        return e(this.c.q);
    }

    public final PlayerLevelInfo n() {
        return this.d;
    }

    public final zza o() {
        if (i(this.c.s)) {
            return null;
        }
        return this.e;
    }

    public final Uri p() {
        return h(this.c.B);
    }

    public final Uri q() {
        return h(this.c.D);
    }

    public final int r() {
        return c(this.c.F);
    }

    public final long s() {
        return b(this.c.G);
    }

    public final boolean t() {
        return d(this.c.H);
    }

    public final String toString() {
        return PlayerEntity.b(this);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        ((PlayerEntity) ((Player) a())).writeToParcel(parcel, i);
    }
}
