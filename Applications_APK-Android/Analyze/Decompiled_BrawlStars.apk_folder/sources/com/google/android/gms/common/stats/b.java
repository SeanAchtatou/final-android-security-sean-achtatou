package com.google.android.gms.common.stats;

import android.content.ComponentName;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    public static final ComponentName f1659a = new ComponentName("com.google.android.gms", "com.google.android.gms.common.stats.GmsCoreStatsService");

    /* renamed from: b  reason: collision with root package name */
    private static int f1660b = 0;
    private static int c = 1;
    private static int d = 2;
    private static int e = 4;
    private static int f = 8;
    private static int g = 16;
    private static int h = 32;
    private static int i = 1;
}
