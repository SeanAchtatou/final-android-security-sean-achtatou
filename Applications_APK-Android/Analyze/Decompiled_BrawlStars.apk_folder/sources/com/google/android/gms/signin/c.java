package com.google.android.gms.signin;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.d;
import com.google.android.gms.common.internal.b;

final class c extends a.C0111a<com.google.android.gms.signin.internal.a, a> {
    c() {
    }

    public final /* synthetic */ a.f a(Context context, Looper looper, b bVar, Object obj, d.b bVar2, d.c cVar) {
        if (((a) obj) == null) {
            a aVar = a.f2605a;
        }
        return new com.google.android.gms.signin.internal.a(context, looper, bVar, bVar2, cVar);
    }
}
