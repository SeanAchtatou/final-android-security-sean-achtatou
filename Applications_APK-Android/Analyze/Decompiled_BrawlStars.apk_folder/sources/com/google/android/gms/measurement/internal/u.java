package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.l;
import java.util.List;
import java.util.Map;

final class u implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final t f2582a;

    /* renamed from: b  reason: collision with root package name */
    private final int f2583b;
    private final Throwable c;
    private final byte[] d;
    private final String e;
    private final Map<String, List<String>> f;

    private u(String str, t tVar, int i, Throwable th, byte[] bArr, Map<String, List<String>> map) {
        l.a(tVar);
        this.f2582a = tVar;
        this.f2583b = i;
        this.c = th;
        this.d = bArr;
        this.e = str;
        this.f = map;
    }

    public final void run() {
        this.f2582a.a(this.e, this.f2583b, this.c, this.d, this.f);
    }

    /* synthetic */ u(String str, t tVar, int i, Throwable th, byte[] bArr, Map map, byte b2) {
        this(str, tVar, i, th, bArr, map);
    }
}
