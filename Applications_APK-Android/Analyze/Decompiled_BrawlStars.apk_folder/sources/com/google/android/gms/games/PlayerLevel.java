package com.google.android.gms.games;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.games.internal.zzd;
import java.util.Arrays;

public final class PlayerLevel extends zzd {
    public static final Parcelable.Creator<PlayerLevel> CREATOR = new i();

    /* renamed from: a  reason: collision with root package name */
    private final int f1788a;

    /* renamed from: b  reason: collision with root package name */
    private final long f1789b;
    private final long c;

    public PlayerLevel(int i, long j, long j2) {
        boolean z = true;
        l.a(j >= 0, "Min XP must be positive!");
        l.a(j2 <= j ? false : z, "Max XP must be more than min XP!");
        this.f1788a = i;
        this.f1789b = j;
        this.c = j2;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.f1788a), Long.valueOf(this.f1789b), Long.valueOf(this.c)});
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof PlayerLevel)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        PlayerLevel playerLevel = (PlayerLevel) obj;
        return j.a(Integer.valueOf(playerLevel.f1788a), Integer.valueOf(this.f1788a)) && j.a(Long.valueOf(playerLevel.f1789b), Long.valueOf(this.f1789b)) && j.a(Long.valueOf(playerLevel.c), Long.valueOf(this.c));
    }

    public final String toString() {
        return j.a(this).a("LevelNumber", Integer.valueOf(this.f1788a)).a("MinXp", Long.valueOf(this.f1789b)).a("MaxXp", Long.valueOf(this.c)).toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.b(parcel, 1, this.f1788a);
        a.a(parcel, 2, this.f1789b);
        a.a(parcel, 3, this.c);
        a.b(parcel, a2);
    }
}
