package com.google.android.gms.common.internal;

import android.util.Log;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    private static final int f1607a = 15;

    /* renamed from: b  reason: collision with root package name */
    private static final String f1608b = null;
    private final String c;
    private final String d;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.l.a(java.lang.Object, java.lang.Object):T
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.l.a(java.lang.String, java.lang.Object):java.lang.String
      com.google.android.gms.common.internal.l.a(boolean, java.lang.Object):void
      com.google.android.gms.common.internal.l.a(java.lang.Object, java.lang.Object):T */
    public h(String str, String str2) {
        l.a((Object) str, (Object) "log tag cannot be null");
        Object[] objArr = {str, 23};
        if (str.length() <= 23) {
            this.c = str;
            if (str2 == null || str2.length() <= 0) {
                this.d = null;
            } else {
                this.d = str2;
            }
        } else {
            throw new IllegalArgumentException(String.format("tag \"%s\" is longer than the %d character maximum", objArr));
        }
    }

    public h(String str) {
        this(str, null);
    }

    public final boolean a(int i) {
        return Log.isLoggable(this.c, i);
    }

    public final void a(String str, String str2) {
        if (a(5)) {
            a(str2);
        }
    }

    public final void a(String str, Object... objArr) {
        if (a(5)) {
            b(str, objArr);
        }
    }

    public final void b(String str, String str2) {
        if (a(6)) {
            a(str2);
        }
    }

    public final String a(String str) {
        String str2 = this.d;
        if (str2 == null) {
            return str;
        }
        return str2.concat(str);
    }

    public final String b(String str, Object... objArr) {
        String format = String.format(str, objArr);
        String str2 = this.d;
        if (str2 == null) {
            return format;
        }
        return str2.concat(format);
    }
}
