package com.google.android.gms.internal.drive;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.drive.DriveSpace;
import com.google.android.gms.drive.metadata.internal.j;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public final class bb extends j<DriveSpace> {
    public bb() {
        super("spaces", Arrays.asList("inDriveSpace", "isAppData", "inGooglePhotosSpace"), Collections.emptySet(), 7000000);
    }

    public final Collection<DriveSpace> b_(DataHolder dataHolder, int i, int i2) {
        ArrayList arrayList = new ArrayList();
        if (dataHolder.d("inDriveSpace", i, i2)) {
            arrayList.add(DriveSpace.f1698a);
        }
        if (dataHolder.d("isAppData", i, i2)) {
            arrayList.add(DriveSpace.f1699b);
        }
        if (dataHolder.d("inGooglePhotosSpace", i, i2)) {
            arrayList.add(DriveSpace.c);
        }
        return arrayList;
    }

    public final /* synthetic */ Object c(DataHolder dataHolder, int i, int i2) {
        return c(dataHolder, i, i2);
    }
}
