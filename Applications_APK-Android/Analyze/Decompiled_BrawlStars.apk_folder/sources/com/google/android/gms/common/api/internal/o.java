package com.google.android.gms.common.api.internal;

import android.support.v4.util.ArraySet;
import com.google.android.gms.common.ConnectionResult;

public class o extends bv {

    /* renamed from: b  reason: collision with root package name */
    final ArraySet<bs<?>> f1488b;
    private d f;

    public final void b() {
        super.b();
        g();
    }

    public final void c() {
        super.c();
        g();
    }

    public final void d() {
        super.d();
        d dVar = this.f;
        synchronized (d.f1467b) {
            if (dVar.g == this) {
                dVar.g = null;
                dVar.h.clear();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(ConnectionResult connectionResult, int i) {
        this.f.b(connectionResult, i);
    }

    /* access modifiers changed from: protected */
    public final void e() {
        this.f.b();
    }

    private final void g() {
        if (!this.f1488b.isEmpty()) {
            this.f.a(this);
        }
    }
}
