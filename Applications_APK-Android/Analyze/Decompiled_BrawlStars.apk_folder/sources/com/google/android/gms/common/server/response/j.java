package com.google.android.gms.common.server.response;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.common.server.response.FastJsonResponse;

public final class j implements Parcelable.Creator<zam> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zam[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        String str = null;
        int i = 0;
        FastJsonResponse.Field field = null;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            int i2 = 65535 & readInt;
            if (i2 == 1) {
                i = SafeParcelReader.d(parcel, readInt);
            } else if (i2 == 2) {
                str = SafeParcelReader.l(parcel, readInt);
            } else if (i2 != 3) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                field = (FastJsonResponse.Field) SafeParcelReader.a(parcel, readInt, FastJsonResponse.Field.CREATOR);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new zam(i, str, field);
    }
}
