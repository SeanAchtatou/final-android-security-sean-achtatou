package com.google.android.gms.internal.measurement;

import java.util.Collections;
import java.util.List;
import java.util.Map;

final class hn extends hm<FieldDescriptorType, Object> {
    hn(int i) {
        super(i, (byte) 0);
    }

    public final void a() {
        if (!this.f2265a) {
            for (int i = 0; i < b(); i++) {
                Map.Entry b2 = b(i);
                if (((fj) b2.getKey()).d()) {
                    b2.setValue(Collections.unmodifiableList((List) b2.getValue()));
                }
            }
            for (Map.Entry entry : c()) {
                if (((fj) entry.getKey()).d()) {
                    entry.setValue(Collections.unmodifiableList((List) entry.getValue()));
                }
            }
        }
        super.a();
    }
}
