package com.google.android.gms.games;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.internal.k;
import com.google.android.gms.games.internal.i;
import com.google.android.gms.games.snapshot.Snapshot;
import com.google.android.gms.games.snapshot.SnapshotMetadata;
import com.google.android.gms.internal.games.z;

public class SnapshotsClient extends z {
    private static final i<Object> f = new m();
    private static final k.a<Object, String> g = new n();
    private static final k.a<Object, SnapshotMetadata> h = new o();
    private static final k.a<Object, Object> i = new p();
    private static final com.google.android.gms.games.internal.k j = new q();
    private static final k.a<Object, Object<Snapshot>> k = new k();
    private static final k.a<Object, Object> l = new l();

    public static class SnapshotContentUnavailableApiException extends ApiException {
    }
}
