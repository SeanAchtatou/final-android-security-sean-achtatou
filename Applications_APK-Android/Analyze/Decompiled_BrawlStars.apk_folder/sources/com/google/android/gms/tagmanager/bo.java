package com.google.android.gms.tagmanager;

import com.google.android.gms.common.api.Status;
import java.util.concurrent.ConcurrentMap;

final class bo implements b {

    /* renamed from: a  reason: collision with root package name */
    a f2648a;

    /* renamed from: b  reason: collision with root package name */
    bp f2649b;
    boolean c;
    private a d;
    private Status e;
    private bq f;
    private d g;

    public final Status b() {
        return this.e;
    }

    public final void c() {
        if (this.c) {
            ab.a("Refreshing a released ContainerHolder.");
        }
    }

    public final void a() {
        String str;
        if (this.c) {
            ab.a("Releasing a released ContainerHolder.");
            return;
        }
        this.c = true;
        ConcurrentMap<String, bo> concurrentMap = this.g.f2655b;
        if (this.c) {
            ab.a("getContainerId called on a released ContainerHolder.");
            str = "";
        } else {
            str = this.f2648a.f2619a;
        }
        bo remove = concurrentMap.remove(str);
        this.f2648a.f2620b = null;
        this.f2648a = null;
        this.d = null;
        this.f2649b = null;
        this.f = null;
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        if (this.c) {
            ab.a("setCtfeUrlPathAndQuery called on a released ContainerHolder.");
        }
    }
}
