package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.common.zzk;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.common.a;
import com.google.android.gms.internal.common.zza;

public final class zzo extends zza implements zzm {
    zzo(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.IGoogleCertificatesApi");
    }

    public final boolean a(zzk zzk, IObjectWrapper iObjectWrapper) throws RemoteException {
        Parcel s = s();
        a.a(s, zzk);
        a.a(s, iObjectWrapper);
        Parcel a2 = a(5, s);
        boolean a3 = a.a(a2);
        a2.recycle();
        return a3;
    }
}
