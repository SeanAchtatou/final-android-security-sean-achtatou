package com.google.android.gms.signin.internal;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.common.internal.IAccountAccessor;
import com.google.android.gms.internal.base.a;
import com.google.android.gms.internal.base.zaa;

public final class zag extends zaa implements zaf {
    zag(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.signin.internal.ISignInService");
    }

    public final void a(int i) throws RemoteException {
        Parcel a2 = a();
        a2.writeInt(i);
        b(7, a2);
    }

    public final void a(IAccountAccessor iAccountAccessor, int i, boolean z) throws RemoteException {
        Parcel a2 = a();
        a.a(a2, iAccountAccessor);
        a2.writeInt(i);
        a.a(a2, z);
        b(9, a2);
    }

    public final void a(zah zah, zad zad) throws RemoteException {
        Parcel a2 = a();
        a.a(a2, zah);
        a.a(a2, zad);
        b(12, a2);
    }
}
