package com.google.android.gms.dynamite;

import android.content.Context;
import com.google.android.gms.dynamite.DynamiteModule;

final class c implements DynamiteModule.a {
    c() {
    }

    public final DynamiteModule.a.b a(Context context, String str, DynamiteModule.a.C0116a aVar) throws DynamiteModule.LoadingException {
        DynamiteModule.a.b bVar = new DynamiteModule.a.b();
        bVar.f1781a = aVar.a(context, str);
        if (bVar.f1781a != 0) {
            bVar.c = -1;
        } else {
            bVar.f1782b = aVar.a(context, str, true);
            if (bVar.f1782b != 0) {
                bVar.c = 1;
            }
        }
        return bVar;
    }
}
