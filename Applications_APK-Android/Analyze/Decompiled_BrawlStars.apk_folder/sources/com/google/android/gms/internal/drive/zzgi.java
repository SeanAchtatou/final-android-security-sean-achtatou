package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.drive.d;

public final class zzgi extends AbstractSafeParcelable implements d {
    public static final Parcelable.Creator<zzgi> CREATOR = new aj();

    /* renamed from: a  reason: collision with root package name */
    private final int f1960a;

    /* renamed from: b  reason: collision with root package name */
    private final int f1961b;
    private final boolean c;

    zzgi(int i, int i2, boolean z) {
        this.f1960a = i;
        this.f1961b = i2;
        this.c = z;
    }

    public final int a() {
        return this.f1960a;
    }

    public final boolean b() {
        return this.c;
    }

    public final int c() {
        return this.f1961b;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.b(parcel, 2, this.f1960a);
        a.b(parcel, 3, this.f1961b);
        a.a(parcel, 4, this.c);
        a.b(parcel, a2);
    }
}
