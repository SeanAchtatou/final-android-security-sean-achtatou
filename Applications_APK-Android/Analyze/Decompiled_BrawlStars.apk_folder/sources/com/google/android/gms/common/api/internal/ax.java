package com.google.android.gms.common.api.internal;

import android.os.Looper;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.a.d;
import com.google.android.gms.common.api.c;
import com.google.android.gms.common.api.g;
import com.google.android.gms.common.api.internal.c;

public final class ax<O extends a.d> extends q {

    /* renamed from: a  reason: collision with root package name */
    private final c<O> f1411a;

    public ax(c<O> cVar) {
        super("Method is not supported by connectionless client. APIs supporting connectionless client must not call this method.");
        this.f1411a = cVar;
    }

    public final void a(bj bjVar) {
    }

    public final void b(bj bjVar) {
    }

    public final <A extends a.b, T extends c.a<? extends g, A>> T a(T t) {
        return this.f1411a.a(t);
    }

    public final Looper a() {
        return this.f1411a.c;
    }
}
