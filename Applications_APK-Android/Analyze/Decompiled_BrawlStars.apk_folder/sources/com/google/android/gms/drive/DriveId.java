package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.internal.drive.at;
import com.google.android.gms.internal.drive.bx;

public class DriveId extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Parcelable.Creator<DriveId> CREATOR = new i();

    /* renamed from: a  reason: collision with root package name */
    public final int f1696a;

    /* renamed from: b  reason: collision with root package name */
    private final String f1697b;
    private final long c;
    private final long d;
    private volatile String e = null;
    private volatile String f = null;

    public DriveId(String str, long j, long j2, int i) {
        this.f1697b = str;
        boolean z = true;
        l.b(!"".equals(str));
        if (str == null && j == -1) {
            z = false;
        }
        l.b(z);
        this.c = j;
        this.d = j2;
        this.f1696a = i;
    }

    public boolean equals(Object obj) {
        String str;
        if (obj != null && obj.getClass() == DriveId.class) {
            DriveId driveId = (DriveId) obj;
            if (driveId.d != this.d) {
                return false;
            }
            if (driveId.c == -1 && this.c == -1) {
                return driveId.f1697b.equals(this.f1697b);
            }
            String str2 = this.f1697b;
            if (str2 != null && (str = driveId.f1697b) != null) {
                return driveId.c == this.c && str.equals(str2);
            }
            if (driveId.c == this.c) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        if (this.c == -1) {
            return this.f1697b.hashCode();
        }
        String valueOf = String.valueOf(String.valueOf(this.d));
        String valueOf2 = String.valueOf(String.valueOf(this.c));
        return (valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf)).hashCode();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.util.List<java.lang.String>, boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.a.a(android.os.Parcel, int, java.lang.String, boolean):void */
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = a.a(parcel, 20293);
        a.a(parcel, 2, this.f1697b, false);
        a.a(parcel, 3, this.c);
        a.a(parcel, 4, this.d);
        a.b(parcel, 5, this.f1696a);
        a.b(parcel, a2);
    }

    public String toString() {
        if (this.e == null) {
            at atVar = new at();
            atVar.f1891a = 1;
            String str = this.f1697b;
            if (str == null) {
                str = "";
            }
            atVar.f1892b = str;
            atVar.c = this.c;
            atVar.d = this.d;
            atVar.e = this.f1696a;
            String valueOf = String.valueOf(Base64.encodeToString(bx.a(atVar), 10));
            this.e = valueOf.length() != 0 ? "DriveId:".concat(valueOf) : new String("DriveId:");
        }
        return this.e;
    }
}
