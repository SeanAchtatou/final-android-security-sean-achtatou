package com.google.android.gms.internal.drive;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.c;

public final class zzgs extends zzl {

    /* renamed from: a  reason: collision with root package name */
    private final c.b<Status> f1968a;

    public final void a() throws RemoteException {
        this.f1968a.a((Object) Status.f1368a);
    }

    public final void a(Status status) throws RemoteException {
        this.f1968a.a((Object) status);
    }
}
