package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public final class zzw extends zzq implements zzu {
    zzw(IBinder iBinder) {
        super(iBinder, "com.google.android.finsky.externalreferrer.IGetInstallReferrerService");
    }

    public final Bundle a(Bundle bundle) throws RemoteException {
        Parcel a2 = a();
        dk.a(a2, bundle);
        Parcel a3 = a(1, a2);
        Bundle bundle2 = (Bundle) dk.a(a3, Bundle.CREATOR);
        a3.recycle();
        return bundle2;
    }
}
