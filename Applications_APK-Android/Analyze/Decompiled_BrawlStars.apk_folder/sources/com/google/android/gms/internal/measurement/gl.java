package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class gl<K, V> {
    static <K, V> void a(zztv zztv, gm<K, V> gmVar, K k, V v) throws IOException {
        fh.a(zztv, gmVar.f2244a, 1, k);
        fh.a(zztv, gmVar.c, 2, v);
    }

    static <K, V> int a(gm<K, V> gmVar, K k, V v) {
        return fh.a(gmVar.f2244a, 1, k) + fh.a(gmVar.c, 2, v);
    }
}
