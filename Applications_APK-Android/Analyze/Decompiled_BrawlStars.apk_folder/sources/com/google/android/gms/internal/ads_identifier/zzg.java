package com.google.android.gms.internal.ads_identifier;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public final class zzg extends zza implements zze {
    zzg(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
    }

    public final String a() throws RemoteException {
        Parcel a2 = a(1, d_());
        String readString = a2.readString();
        a2.recycle();
        return readString;
    }

    public final boolean b() throws RemoteException {
        Parcel d_ = d_();
        a.b(d_);
        Parcel a2 = a(2, d_);
        boolean a3 = a.a(a2);
        a2.recycle();
        return a3;
    }
}
