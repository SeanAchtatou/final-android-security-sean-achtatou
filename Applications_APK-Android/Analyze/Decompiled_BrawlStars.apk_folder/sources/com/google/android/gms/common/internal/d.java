package com.google.android.gms.common.internal;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import com.google.android.gms.common.api.internal.g;

public abstract class d implements DialogInterface.OnClickListener {
    /* access modifiers changed from: protected */
    public abstract void a();

    public static d a(Activity activity, Intent intent, int i) {
        return new r(intent, activity, i);
    }

    public static d a(g gVar, Intent intent, int i) {
        return new s(intent, gVar, 2);
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        try {
            a();
        } catch (ActivityNotFoundException unused) {
        } finally {
            dialogInterface.dismiss();
        }
    }
}
