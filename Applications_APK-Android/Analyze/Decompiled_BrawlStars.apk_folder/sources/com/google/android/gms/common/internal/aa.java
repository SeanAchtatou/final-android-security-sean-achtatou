package com.google.android.gms.common.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class aa implements Parcelable.Creator<zzb> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zzb[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        Bundle bundle = null;
        Feature[] featureArr = null;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            int i = 65535 & readInt;
            if (i == 1) {
                bundle = SafeParcelReader.n(parcel, readInt);
            } else if (i != 2) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                featureArr = (Feature[]) SafeParcelReader.b(parcel, readInt, Feature.CREATOR);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new zzb(bundle, featureArr);
    }
}
