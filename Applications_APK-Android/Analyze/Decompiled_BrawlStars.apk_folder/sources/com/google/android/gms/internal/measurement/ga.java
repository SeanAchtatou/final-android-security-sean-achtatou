package com.google.android.gms.internal.measurement;

import java.util.Iterator;
import java.util.Map;

final class ga<K> implements Iterator<Map.Entry<K, Object>> {

    /* renamed from: a  reason: collision with root package name */
    private Iterator<Map.Entry<K, Object>> f2233a;

    public ga(Iterator<Map.Entry<K, Object>> it) {
        this.f2233a = it;
    }

    public final boolean hasNext() {
        return this.f2233a.hasNext();
    }

    public final void remove() {
        this.f2233a.remove();
    }

    public final /* synthetic */ Object next() {
        Map.Entry next = this.f2233a.next();
        return next.getValue() instanceof fy ? new fz(next, (byte) 0) : next;
    }
}
