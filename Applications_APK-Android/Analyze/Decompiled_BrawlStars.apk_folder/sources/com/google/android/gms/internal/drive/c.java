package com.google.android.gms.internal.drive;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class c implements Parcelable.Creator<zzad> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zzad[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        while (parcel.dataPosition() < a2) {
            SafeParcelReader.b(parcel, parcel.readInt());
        }
        SafeParcelReader.x(parcel, a2);
        return new zzad();
    }
}
