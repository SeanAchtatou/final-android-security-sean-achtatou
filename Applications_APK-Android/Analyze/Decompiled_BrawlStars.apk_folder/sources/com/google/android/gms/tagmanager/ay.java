package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.measurement.dc;
import com.google.android.gms.internal.measurement.dd;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

final class ay {

    /* renamed from: a  reason: collision with root package name */
    final Set<dd> f2634a = new HashSet();

    /* renamed from: b  reason: collision with root package name */
    final Map<dd, List<dc>> f2635b = new HashMap();
    final Map<dd, List<dc>> c = new HashMap();
    final Map<dd, List<String>> d = new HashMap();
    final Map<dd, List<String>> e = new HashMap();
    dc f;
}
