package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.internal.BaseGmsClient;
import com.google.android.gms.common.internal.i;
import java.util.ArrayList;
import java.util.Map;

final class x extends ae {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ u f1501a;

    /* renamed from: b  reason: collision with root package name */
    private final Map<a.f, w> f1502b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public x(u uVar, Map<a.f, w> map) {
        super(uVar, (byte) 0);
        this.f1501a = uVar;
        this.f1502b = map;
    }

    public final void a() {
        i iVar = new i(this.f1501a.d);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (a.f next : this.f1502b.keySet()) {
            if (!this.f1502b.get(next).f1499a) {
                arrayList.add(next);
            } else {
                arrayList2.add(next);
            }
        }
        int i = -1;
        int i2 = 0;
        if (!arrayList.isEmpty()) {
            ArrayList arrayList3 = arrayList;
            int size = arrayList3.size();
            while (i2 < size) {
                Object obj = arrayList3.get(i2);
                i2++;
                i = iVar.a(this.f1501a.c, (a.f) obj);
                if (i != 0) {
                    break;
                }
            }
        } else {
            ArrayList arrayList4 = arrayList2;
            int size2 = arrayList4.size();
            while (i2 < size2) {
                Object obj2 = arrayList4.get(i2);
                i2++;
                i = iVar.a(this.f1501a.c, (a.f) obj2);
                if (i == 0) {
                    break;
                }
            }
        }
        if (i != 0) {
            this.f1501a.f1496a.a(new y(this, this.f1501a, new ConnectionResult(i, null)));
            return;
        }
        if (this.f1501a.f) {
            this.f1501a.e.s();
        }
        for (a.f next2 : this.f1502b.keySet()) {
            BaseGmsClient.c cVar = this.f1502b.get(next2);
            if (iVar.a(this.f1501a.c, next2) != 0) {
                this.f1501a.f1496a.a(new z(this.f1501a, cVar));
            } else {
                next2.a(cVar);
            }
        }
    }
}
