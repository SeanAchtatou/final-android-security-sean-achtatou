package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.eb;
import com.google.android.gms.internal.measurement.ec;
import java.io.IOException;

public abstract class eb<MessageType extends eb<MessageType, BuilderType>, BuilderType extends ec<MessageType, BuilderType>> implements gt {
    private static boolean zzbtj = false;
    protected int zzbti = 0;

    public final ej d() {
        try {
            er d = ej.d(h());
            a(d.f2188a);
            return d.a();
        } catch (IOException e) {
            String name = getClass().getName();
            StringBuilder sb = new StringBuilder(String.valueOf(name).length() + 62 + "ByteString".length());
            sb.append("Serializing ");
            sb.append(name);
            sb.append(" to a ");
            sb.append("ByteString");
            sb.append(" threw an IOException (should never happen).");
            throw new RuntimeException(sb.toString(), e);
        }
    }

    /* access modifiers changed from: package-private */
    public int e() {
        throw new UnsupportedOperationException();
    }

    /* access modifiers changed from: package-private */
    public void b(int i) {
        throw new UnsupportedOperationException();
    }
}
