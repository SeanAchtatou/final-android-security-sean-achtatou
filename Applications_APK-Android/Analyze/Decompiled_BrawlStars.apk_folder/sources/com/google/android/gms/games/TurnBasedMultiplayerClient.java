package com.google.android.gms.games;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.internal.k;
import com.google.android.gms.games.internal.i;
import com.google.android.gms.games.internal.j;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch;
import com.google.android.gms.internal.games.z;

public class TurnBasedMultiplayerClient extends z {
    private static final i<TurnBasedMatch> f = new ab();
    private static final k.a<Object, Object> g = new r();
    private static final j<Object> h = new s();
    private static final k.a<Object, TurnBasedMatch> i = new t();
    private static final k.a<Object, String> j = new u();
    private static final com.google.android.gms.games.internal.k k = new v();
    private static final k.a<Object, Void> l = new w();
    private static final k.a<Object, TurnBasedMatch> m = new x();
    private static final com.google.android.gms.games.internal.k n = new y();
    private static final k.a<Object, TurnBasedMatch> o = new z();
    private static final k.a<Object, TurnBasedMatch> p = new aa();

    public static class MatchOutOfDateApiException extends ApiException {
    }
}
