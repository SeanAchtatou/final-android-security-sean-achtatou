package com.google.android.gms.common.api.internal;

abstract class ae implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ u f1389a;

    private ae(u uVar) {
        this.f1389a = uVar;
    }

    /* access modifiers changed from: protected */
    public abstract void a();

    public void run() {
        this.f1389a.f1497b.lock();
        try {
            if (!Thread.interrupted()) {
                a();
                this.f1389a.f1497b.unlock();
            }
        } catch (RuntimeException e) {
            am amVar = this.f1389a.f1496a;
            amVar.e.sendMessage(amVar.e.obtainMessage(2, e));
        } finally {
            this.f1389a.f1497b.unlock();
        }
    }

    /* synthetic */ ae(u uVar, byte b2) {
        this(uVar);
    }
}
