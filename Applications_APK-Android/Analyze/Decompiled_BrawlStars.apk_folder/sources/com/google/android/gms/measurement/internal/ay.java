package com.google.android.gms.measurement.internal;

final class ay implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zzo f2401a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ zzby f2402b;

    ay(zzby zzby, zzo zzo) {
        this.f2402b = zzby;
        this.f2401a = zzo;
    }

    public final void run() {
        this.f2402b.f2597a.k();
        du a2 = this.f2402b.f2597a;
        zzo zzo = this.f2401a;
        zzk a3 = a2.a(zzo.f2603a);
        if (a3 != null) {
            a2.a(zzo, a3);
        }
    }
}
