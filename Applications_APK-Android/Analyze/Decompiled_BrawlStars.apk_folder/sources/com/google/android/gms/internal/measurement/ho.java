package com.google.android.gms.internal.measurement;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

final class ho implements Iterator<Map.Entry<K, V>> {

    /* renamed from: a  reason: collision with root package name */
    private int f2267a;

    /* renamed from: b  reason: collision with root package name */
    private Iterator<Map.Entry<K, V>> f2268b;
    private final /* synthetic */ hm c;

    private ho(hm hmVar) {
        this.c = hmVar;
        this.f2267a = this.c.c.size();
    }

    public final boolean hasNext() {
        int i = this.f2267a;
        return (i > 0 && i <= this.c.c.size()) || a().hasNext();
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }

    private final Iterator<Map.Entry<K, V>> a() {
        if (this.f2268b == null) {
            this.f2268b = this.c.f.entrySet().iterator();
        }
        return this.f2268b;
    }

    public final /* synthetic */ Object next() {
        if (a().hasNext()) {
            return (Map.Entry) a().next();
        }
        List b2 = this.c.c;
        int i = this.f2267a - 1;
        this.f2267a = i;
        return (Map.Entry) b2.get(i);
    }

    /* synthetic */ ho(hm hmVar, byte b2) {
        this(hmVar);
    }
}
