package com.google.android.gms.games;

import android.os.RemoteException;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.d;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.games.a;
import com.google.android.gms.games.internal.zze;
import com.google.android.gms.games.internal.zzy;

final class af extends a.e {
    private final /* synthetic */ String d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    af(d dVar, String str) {
        super(dVar, (byte) 0);
        this.d = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.l.a(java.lang.String, java.lang.Object):java.lang.String
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.l.a(java.lang.Object, java.lang.Object):T
      com.google.android.gms.common.internal.l.a(boolean, java.lang.Object):void
      com.google.android.gms.common.internal.l.a(java.lang.String, java.lang.Object):java.lang.String */
    public final /* synthetic */ void b(a.b bVar) throws RemoteException {
        zze zze = (zze) bVar;
        String str = this.d;
        l.a(str, (Object) "Please provide a valid serverClientId");
        try {
            ((zzy) zze.p()).a(str, new zze.zzy(this));
        } catch (SecurityException unused) {
            zze.b(this);
        }
    }
}
