package com.google.android.gms.common.api.internal;

import com.google.android.gms.signin.internal.zaj;

final class bh implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zaj f1420a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ zace f1421b;

    bh(zace zace, zaj zaj) {
        this.f1421b = zace;
        this.f1420a = zaj;
    }

    public final void run() {
        zace.a(this.f1421b, this.f1420a);
    }
}
