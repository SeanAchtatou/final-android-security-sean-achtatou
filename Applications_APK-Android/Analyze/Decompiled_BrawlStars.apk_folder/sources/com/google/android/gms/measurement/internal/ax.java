package com.google.android.gms.measurement.internal;

final class ax implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zzo f2399a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ zzby f2400b;

    ax(zzby zzby, zzo zzo) {
        this.f2400b = zzby;
        this.f2399a = zzo;
    }

    public final void run() {
        this.f2400b.f2597a.k();
        du a2 = this.f2400b.f2597a;
        zzo zzo = this.f2399a;
        zzk a3 = a2.a(zzo.f2603a);
        if (a3 != null) {
            a2.b(zzo, a3);
        }
    }
}
