package com.google.android.gms.drive;

import java.util.Arrays;

public class e {

    /* renamed from: a  reason: collision with root package name */
    public static final d f1705a = new a(1, true, 256);

    /* renamed from: b  reason: collision with root package name */
    public int f1706b;
    public boolean c;
    public int d;

    public e() {
        this(f1705a);
    }

    public e(a aVar) {
        this.f1706b = aVar.a();
        this.c = aVar.b();
        this.d = aVar.c();
    }

    private e(d dVar) {
        this.f1706b = dVar.a();
        this.c = dVar.b();
        this.d = dVar.c();
    }

    public static class a implements d {

        /* renamed from: a  reason: collision with root package name */
        private final int f1707a;

        /* renamed from: b  reason: collision with root package name */
        private final boolean f1708b;
        private final int c;

        public a(int i, boolean z, int i2) {
            this.f1707a = i;
            this.f1708b = z;
            this.c = i2;
        }

        public final int a() {
            return this.f1707a;
        }

        public final boolean b() {
            return this.f1708b;
        }

        public final int c() {
            return this.c;
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj != null && getClass() == obj.getClass()) {
                a aVar = (a) obj;
                return aVar.f1707a == this.f1707a && aVar.f1708b == this.f1708b && aVar.c == this.c;
            }
        }

        public final String toString() {
            return String.format("NetworkPreference: %s, IsRoamingAllowed %s, BatteryUsagePreference %s", Integer.valueOf(this.f1707a), Boolean.valueOf(this.f1708b), Integer.valueOf(this.c));
        }

        public final int hashCode() {
            return Arrays.hashCode(new Object[]{Integer.valueOf(this.f1707a), Boolean.valueOf(this.f1708b), Integer.valueOf(this.c)});
        }
    }
}
