package com.google.android.gms.measurement.internal;

import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import com.google.android.gms.common.stats.a;
import com.google.android.gms.internal.measurement.zzu;

final class ag implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ zzu f2371a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ ServiceConnection f2372b;
    private final /* synthetic */ af c;

    ag(af afVar, zzu zzu, ServiceConnection serviceConnection) {
        this.c = afVar;
        this.f2371a = zzu;
        this.f2372b = serviceConnection;
    }

    public final void run() {
        ae aeVar = this.c.f2369a;
        String a2 = this.c.f2370b;
        zzu zzu = this.f2371a;
        ServiceConnection serviceConnection = this.f2372b;
        Bundle a3 = aeVar.a(a2, zzu);
        aeVar.f2368a.p().c();
        if (a3 != null) {
            long j = a3.getLong("install_begin_timestamp_seconds", 0) * 1000;
            if (j == 0) {
                aeVar.f2368a.q().c.a("Service response is missing Install Referrer install timestamp");
            } else {
                String string = a3.getString("install_referrer");
                if (string == null || string.isEmpty()) {
                    aeVar.f2368a.q().c.a("No referrer defined in install referrer response");
                } else {
                    aeVar.f2368a.q().k.a("InstallReferrer API result", string);
                    ed e = aeVar.f2368a.e();
                    String valueOf = String.valueOf(string);
                    Bundle a4 = e.a(Uri.parse(valueOf.length() != 0 ? "?".concat(valueOf) : new String("?")));
                    if (a4 == null) {
                        aeVar.f2368a.q().c.a("No campaign params defined in install referrer result");
                    } else {
                        String string2 = a4.getString("medium");
                        if (string2 != null && !"(not set)".equalsIgnoreCase(string2) && !"organic".equalsIgnoreCase(string2)) {
                            long j2 = a3.getLong("referrer_click_timestamp_seconds", 0) * 1000;
                            if (j2 == 0) {
                                aeVar.f2368a.q().c.a("Install Referrer is missing click timestamp for ad campaign");
                            } else {
                                a4.putLong("click_timestamp", j2);
                            }
                        }
                        if (j == aeVar.f2368a.b().j.a()) {
                            aeVar.f2368a.q().k.a("Campaign has already been logged");
                        } else {
                            aeVar.f2368a.b().j.a(j);
                            aeVar.f2368a.q().k.a("Logging Install Referrer campaign from sdk with ", "referrer API");
                            a4.putString("_cis", "referrer API");
                            aeVar.f2368a.d().a("auto", "_cmp", a4);
                        }
                    }
                }
            }
        }
        if (serviceConnection != null) {
            a.a();
            a.a(aeVar.f2368a.m(), serviceConnection);
        }
    }
}
