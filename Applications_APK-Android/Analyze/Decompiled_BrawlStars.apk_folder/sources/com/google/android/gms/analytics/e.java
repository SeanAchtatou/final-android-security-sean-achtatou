package com.google.android.gms.analytics;

import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.internal.measurement.g;
import com.google.android.gms.internal.measurement.k;
import com.google.android.gms.internal.measurement.t;
import java.util.ListIterator;

public class e extends m<e> {
    public final t d;
    public boolean e;

    public e(t tVar) {
        super(tVar.b(), tVar.c);
        this.d = tVar;
    }

    public final k d() {
        k a2 = this.g.a();
        a2.a(this.d.h().b());
        a2.a(this.d.h.b());
        b(a2);
        return a2;
    }

    /* access modifiers changed from: protected */
    public final void a(k kVar) {
        g gVar = (g) kVar.b(g.class);
        if (TextUtils.isEmpty(gVar.f2232b)) {
            gVar.f2232b = this.d.g().b();
        }
        if (this.e && TextUtils.isEmpty(gVar.d)) {
            k f = this.d.f();
            gVar.d = f.c();
            gVar.e = f.b();
        }
    }

    public final void b(String str) {
        l.a(str);
        Uri a2 = f.a(str);
        ListIterator<q> listIterator = this.g.g.listIterator();
        while (listIterator.hasNext()) {
            if (a2.equals(listIterator.next().a())) {
                listIterator.remove();
            }
        }
        this.g.g.add(new f(this.d, str));
    }
}
