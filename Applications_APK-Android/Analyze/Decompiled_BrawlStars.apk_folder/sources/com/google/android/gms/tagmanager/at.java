package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.measurement.da;
import com.google.android.gms.internal.measurement.db;
import com.google.android.gms.internal.measurement.dc;
import com.google.android.gms.internal.measurement.dd;
import com.google.android.gms.internal.measurement.x;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

final class at {

    /* renamed from: a  reason: collision with root package name */
    private static final al<da> f2628a = new al<>(bk.a(), true);

    /* renamed from: b  reason: collision with root package name */
    private final s f2629b;
    private final Map<String, t> c;
    private final Map<String, t> d;
    private final Map<String, t> e;
    private final bn<dc, al<da>> f;
    private final bn<String, ax> g;
    private final Set<dd> h;
    private final c i;
    private final Map<String, ay> j;
    private volatile String k;
    private int l;

    public final synchronized void a(String str) {
        b(str);
        j a2 = this.f2629b.a().a();
        for (dc a3 : (Set) a(this.h, new HashSet(), new av(), a2.b()).f2622a) {
            a(this.c, a3, new HashSet(), a2.a());
        }
        b(null);
    }

    private final synchronized void b(String str) {
        this.k = str;
    }

    private final al<Set<dc>> a(Set<dd> set, Set<String> set2, aw awVar, as asVar) {
        boolean z;
        al alVar;
        Set<String> set3 = set2;
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet();
        Iterator<dd> it = set.iterator();
        while (true) {
            boolean z2 = true;
            while (true) {
                if (it.hasNext()) {
                    dd next = it.next();
                    ar a2 = asVar.a();
                    Iterator<dc> it2 = next.f2156b.iterator();
                    while (true) {
                        boolean z3 = true;
                        while (true) {
                            if (!it2.hasNext()) {
                                Iterator<dc> it3 = next.f2155a.iterator();
                                while (true) {
                                    if (!it3.hasNext()) {
                                        bk.a((Object) true);
                                        alVar = new al(true, z);
                                        break;
                                    }
                                    al<Boolean> a3 = a(it3.next(), set3, a2.b());
                                    if (!((Boolean) a3.f2622a).booleanValue()) {
                                        bk.a((Object) false);
                                        alVar = new al(false, a3.f2623b);
                                        break;
                                    }
                                    z = z && a3.f2623b;
                                }
                            } else {
                                al<Boolean> a4 = a(it2.next(), set3, a2.a());
                                if (((Boolean) a4.f2622a).booleanValue()) {
                                    bk.a((Object) false);
                                    alVar = new al(false, a4.f2623b);
                                    break;
                                } else if (!z || !a4.f2623b) {
                                    z3 = false;
                                }
                            }
                        }
                    }
                    if (((Boolean) alVar.f2622a).booleanValue()) {
                        awVar.a(next, hashSet, hashSet2, a2);
                    }
                    if (!z2 || !alVar.f2623b) {
                        z2 = false;
                    }
                } else {
                    hashSet.removeAll(hashSet2);
                    return new al<>(hashSet, z2);
                }
            }
        }
    }

    private final al<Boolean> a(dc dcVar, Set<String> set, ao aoVar) {
        al<da> a2 = a(this.d, dcVar, set, aoVar);
        Boolean b2 = bk.b((da) a2.f2622a);
        bk.a(b2);
        return new al<>(b2, a2.f2623b);
    }

    private final String a() {
        if (this.l <= 1) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(Integer.toString(this.l));
        for (int i2 = 2; i2 < this.l; i2++) {
            sb.append(' ');
        }
        sb.append(": ");
        return sb.toString();
    }

    private final al<da> a(String str, Set<String> set, ad adVar) {
        dc dcVar;
        this.l++;
        ax a2 = this.g.a();
        if (a2 != null) {
            a(a2.f2633b, set);
            this.l--;
            return a2.f2632a;
        }
        ay ayVar = this.j.get(str);
        if (ayVar == null) {
            String a3 = a();
            StringBuilder sb = new StringBuilder(String.valueOf(a3).length() + 15 + String.valueOf(str).length());
            sb.append(a3);
            sb.append("Invalid macro: ");
            sb.append(str);
            ab.a(sb.toString());
            this.l--;
            return f2628a;
        }
        al<Set<dc>> a4 = a(ayVar.f2634a, set, new au(ayVar.f2635b, ayVar.d, ayVar.c, ayVar.e), adVar.b());
        if (((Set) a4.f2622a).isEmpty()) {
            dcVar = ayVar.f;
        } else {
            if (((Set) a4.f2622a).size() > 1) {
                String a5 = a();
                StringBuilder sb2 = new StringBuilder(String.valueOf(a5).length() + 37 + String.valueOf(str).length());
                sb2.append(a5);
                sb2.append("Multiple macros active for macroName ");
                sb2.append(str);
                ab.b(sb2.toString());
            }
            dcVar = (dc) ((Set) a4.f2622a).iterator().next();
        }
        if (dcVar == null) {
            this.l--;
            return f2628a;
        }
        al<da> a6 = a(this.e, dcVar, set, adVar.a());
        boolean z = a4.f2623b && a6.f2623b;
        al<da> alVar = f2628a;
        if (a6 != alVar) {
            alVar = new al<>((da) a6.f2622a, z);
        }
        da daVar = dcVar.f2154b;
        if (alVar.f2623b) {
            new ax(alVar, daVar);
        }
        a(daVar, set);
        this.l--;
        return alVar;
    }

    private final void a(da daVar, Set<String> set) {
        al<da> a2;
        if (daVar != null && (a2 = a(daVar, set, new ak())) != f2628a) {
            Object c2 = bk.c((da) a2.f2622a);
            if (c2 instanceof Map) {
                this.i.a((Map) c2);
            } else if (c2 instanceof List) {
                for (Object next : (List) c2) {
                    if (next instanceof Map) {
                        this.i.a((Map) next);
                    } else {
                        ab.b("pushAfterEvaluate: value not a Map");
                    }
                }
            } else {
                ab.b("pushAfterEvaluate: value not a Map or List");
            }
        }
    }

    private final al<da> a(da daVar, Set<String> set, bl blVar) {
        if (!daVar.l) {
            return new al<>(daVar, true);
        }
        int i2 = daVar.f2151a;
        if (i2 == 2) {
            da a2 = db.a(daVar);
            a2.c = new da[daVar.c.length];
            for (int i3 = 0; i3 < daVar.c.length; i3++) {
                al<da> a3 = a(daVar.c[i3], set, blVar.a());
                al<da> alVar = f2628a;
                if (a3 == alVar) {
                    return alVar;
                }
                a2.c[i3] = (da) a3.f2622a;
            }
            return new al<>(a2, false);
        } else if (i2 == 3) {
            da a4 = db.a(daVar);
            if (daVar.d.length != daVar.e.length) {
                String valueOf = String.valueOf(daVar.toString());
                ab.a(valueOf.length() != 0 ? "Invalid serving value: ".concat(valueOf) : new String("Invalid serving value: "));
                return f2628a;
            }
            a4.d = new da[daVar.d.length];
            a4.e = new da[daVar.d.length];
            for (int i4 = 0; i4 < daVar.d.length; i4++) {
                al<da> a5 = a(daVar.d[i4], set, blVar.b());
                al<da> a6 = a(daVar.e[i4], set, blVar.c());
                al<da> alVar2 = f2628a;
                if (a5 == alVar2 || a6 == alVar2) {
                    return f2628a;
                }
                a4.d[i4] = (da) a5.f2622a;
                a4.e[i4] = (da) a6.f2622a;
            }
            return new al<>(a4, false);
        } else if (i2 != 4) {
            if (i2 != 7) {
                int i5 = daVar.f2151a;
                StringBuilder sb = new StringBuilder(25);
                sb.append("Unknown type: ");
                sb.append(i5);
                ab.a(sb.toString());
                return f2628a;
            }
            da a7 = db.a(daVar);
            a7.j = new da[daVar.j.length];
            for (int i6 = 0; i6 < daVar.j.length; i6++) {
                al<da> a8 = a(daVar.j[i6], set, blVar.d());
                al<da> alVar3 = f2628a;
                if (a8 == alVar3) {
                    return alVar3;
                }
                a7.j[i6] = (da) a8.f2622a;
            }
            return new al<>(a7, false);
        } else if (set.contains(daVar.f)) {
            String str = daVar.f;
            String obj = set.toString();
            StringBuilder sb2 = new StringBuilder(String.valueOf(str).length() + 79 + String.valueOf(obj).length());
            sb2.append("Macro cycle detected.  Current macro reference: ");
            sb2.append(str);
            sb2.append(".  Previous macro references: ");
            sb2.append(obj);
            sb2.append(".");
            ab.a(sb2.toString());
            return f2628a;
        } else {
            set.add(daVar.f);
            al<da> a9 = bm.a(a(daVar.f, set, blVar.e()), daVar.k);
            set.remove(daVar.f);
            return a9;
        }
    }

    private final al<da> a(Map<String, t> map, dc dcVar, Set<String> set, ao aoVar) {
        da daVar = (da) Collections.unmodifiableMap(dcVar.f2153a).get(x.FUNCTION.toString());
        if (daVar == null) {
            ab.a("No function id in properties");
            return f2628a;
        }
        String str = daVar.g;
        t tVar = map.get(str);
        if (tVar == null) {
            ab.a(String.valueOf(str).concat(" has no backing implementation."));
            return f2628a;
        }
        al<da> a2 = this.f.a();
        if (a2 != null) {
            return a2;
        }
        HashMap hashMap = new HashMap();
        boolean z = true;
        boolean z2 = true;
        for (Map.Entry entry : Collections.unmodifiableMap(dcVar.f2153a).entrySet()) {
            entry.getKey();
            aq a3 = aoVar.a();
            entry.getValue();
            al<da> a4 = a((da) entry.getValue(), set, a3.a());
            al<da> alVar = f2628a;
            if (a4 == alVar) {
                return alVar;
            }
            if (a4.f2623b) {
                dcVar.a((String) entry.getKey(), (da) a4.f2622a);
            } else {
                z2 = false;
            }
            hashMap.put((String) entry.getKey(), (da) a4.f2622a);
        }
        if (!tVar.a(hashMap.keySet())) {
            String valueOf = String.valueOf(tVar.f2671a);
            String valueOf2 = String.valueOf(hashMap.keySet());
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 43 + String.valueOf(valueOf).length() + String.valueOf(valueOf2).length());
            sb.append("Incorrect keys for function ");
            sb.append(str);
            sb.append(" required ");
            sb.append(valueOf);
            sb.append(" had ");
            sb.append(valueOf2);
            ab.a(sb.toString());
            return f2628a;
        }
        if (!z2 || !tVar.a()) {
            z = false;
        }
        return new al<>(tVar.b(), z);
    }
}
