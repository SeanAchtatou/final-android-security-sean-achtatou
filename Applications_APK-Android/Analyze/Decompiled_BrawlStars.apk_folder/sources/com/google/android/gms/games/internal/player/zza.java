package com.google.android.gms.games.internal.player;

import android.net.Uri;
import android.os.Parcelable;
import com.google.android.gms.common.data.e;

public interface zza extends Parcelable, e<zza> {
    String b();

    String c();

    long d();

    Uri e();

    Uri f();

    Uri g();
}
