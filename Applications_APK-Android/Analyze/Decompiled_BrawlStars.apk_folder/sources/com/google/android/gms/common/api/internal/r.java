package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.a;

public final class r implements al {

    /* renamed from: a  reason: collision with root package name */
    final am f1492a;

    /* renamed from: b  reason: collision with root package name */
    boolean f1493b = false;

    public r(am amVar) {
        this.f1492a = amVar;
    }

    public final void a() {
    }

    public final void a(Bundle bundle) {
    }

    public final void a(ConnectionResult connectionResult, a<?> aVar, boolean z) {
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final <A extends com.google.android.gms.common.api.a.b, T extends com.google.android.gms.common.api.internal.c.a<? extends com.google.android.gms.common.api.g, A>> T a(T r4) {
        /*
            r3 = this;
            com.google.android.gms.common.api.internal.am r0 = r3.f1492a     // Catch:{ DeadObjectException -> 0x0045 }
            com.google.android.gms.common.api.internal.ag r0 = r0.m     // Catch:{ DeadObjectException -> 0x0045 }
            com.google.android.gms.common.api.internal.bm r0 = r0.e     // Catch:{ DeadObjectException -> 0x0045 }
            r0.a(r4)     // Catch:{ DeadObjectException -> 0x0045 }
            com.google.android.gms.common.api.internal.am r0 = r3.f1492a     // Catch:{ DeadObjectException -> 0x0045 }
            com.google.android.gms.common.api.internal.ag r0 = r0.m     // Catch:{ DeadObjectException -> 0x0045 }
            com.google.android.gms.common.api.a$c<A> r1 = r4.f1447a     // Catch:{ DeadObjectException -> 0x0045 }
            java.util.Map<com.google.android.gms.common.api.a$c<?>, com.google.android.gms.common.api.a$f> r0 = r0.f1392b     // Catch:{ DeadObjectException -> 0x0045 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ DeadObjectException -> 0x0045 }
            com.google.android.gms.common.api.a$f r0 = (com.google.android.gms.common.api.a.f) r0     // Catch:{ DeadObjectException -> 0x0045 }
            java.lang.String r1 = "Appropriate Api was not requested."
            com.google.android.gms.common.internal.l.a(r0, r1)     // Catch:{ DeadObjectException -> 0x0045 }
            boolean r1 = r0.b()     // Catch:{ DeadObjectException -> 0x0045 }
            if (r1 != 0) goto L_0x0039
            com.google.android.gms.common.api.internal.am r1 = r3.f1492a     // Catch:{ DeadObjectException -> 0x0045 }
            java.util.Map<com.google.android.gms.common.api.a$c<?>, com.google.android.gms.common.ConnectionResult> r1 = r1.g     // Catch:{ DeadObjectException -> 0x0045 }
            com.google.android.gms.common.api.a$c<A> r2 = r4.f1447a     // Catch:{ DeadObjectException -> 0x0045 }
            boolean r1 = r1.containsKey(r2)     // Catch:{ DeadObjectException -> 0x0045 }
            if (r1 == 0) goto L_0x0039
            com.google.android.gms.common.api.Status r0 = new com.google.android.gms.common.api.Status     // Catch:{ DeadObjectException -> 0x0045 }
            r1 = 17
            r0.<init>(r1)     // Catch:{ DeadObjectException -> 0x0045 }
            r4.a(r0)     // Catch:{ DeadObjectException -> 0x0045 }
            goto L_0x004f
        L_0x0039:
            boolean r1 = r0 instanceof com.google.android.gms.common.internal.n     // Catch:{ DeadObjectException -> 0x0045 }
            if (r1 == 0) goto L_0x0041
            com.google.android.gms.common.internal.n r0 = (com.google.android.gms.common.internal.n) r0     // Catch:{ DeadObjectException -> 0x0045 }
            com.google.android.gms.common.api.a$h<T> r0 = r0.i     // Catch:{ DeadObjectException -> 0x0045 }
        L_0x0041:
            r4.a(r0)     // Catch:{ DeadObjectException -> 0x0045 }
            goto L_0x004f
        L_0x0045:
            com.google.android.gms.common.api.internal.am r0 = r3.f1492a
            com.google.android.gms.common.api.internal.s r1 = new com.google.android.gms.common.api.internal.s
            r1.<init>(r3, r3)
            r0.a(r1)
        L_0x004f:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.r.a(com.google.android.gms.common.api.internal.c$a):com.google.android.gms.common.api.internal.c$a");
    }

    public final boolean b() {
        if (this.f1493b) {
            return false;
        }
        if (this.f1492a.m.i()) {
            this.f1493b = true;
            for (bj bjVar : this.f1492a.m.d) {
                bjVar.c = null;
            }
            return false;
        }
        this.f1492a.a((ConnectionResult) null);
        return true;
    }

    public final void c() {
        if (this.f1493b) {
            this.f1493b = false;
            this.f1492a.a(new t(this, this));
        }
    }

    public final void a(int i) {
        this.f1492a.a((ConnectionResult) null);
        this.f1492a.n.a(i, this.f1493b);
    }
}
