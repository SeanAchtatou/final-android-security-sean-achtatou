package com.google.android.gms.common;

import java.lang.ref.WeakReference;

abstract class o extends m {

    /* renamed from: b  reason: collision with root package name */
    private static final WeakReference<byte[]> f1632b = new WeakReference<>(null);

    /* renamed from: a  reason: collision with root package name */
    private WeakReference<byte[]> f1633a = f1632b;

    o(byte[] bArr) {
        super(bArr);
    }

    /* access modifiers changed from: protected */
    public abstract byte[] d();

    /* access modifiers changed from: package-private */
    public final byte[] c() {
        byte[] bArr;
        synchronized (this) {
            bArr = this.f1633a.get();
            if (bArr == null) {
                bArr = d();
                this.f1633a = new WeakReference<>(bArr);
            }
        }
        return bArr;
    }
}
