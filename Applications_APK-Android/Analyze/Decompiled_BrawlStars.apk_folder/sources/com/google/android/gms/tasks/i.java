package com.google.android.gms.tasks;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;

public final class i {

    /* renamed from: a  reason: collision with root package name */
    public static final Executor f2679a = new a();

    /* renamed from: b  reason: collision with root package name */
    static final Executor f2680b = new aa();

    static final class a implements Executor {

        /* renamed from: a  reason: collision with root package name */
        private final Handler f2681a = new Handler(Looper.getMainLooper());

        public final void execute(Runnable runnable) {
            this.f2681a.post(runnable);
        }
    }
}
