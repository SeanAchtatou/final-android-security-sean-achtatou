package com.google.android.gms.internal.measurement;

import java.io.IOException;

public final class jh {

    /* renamed from: a  reason: collision with root package name */
    public static final int[] f2315a = new int[0];

    /* renamed from: b  reason: collision with root package name */
    public static final long[] f2316b = new long[0];
    public static final String[] c = new String[0];
    public static final byte[] d = new byte[0];
    private static final int e = 11;
    private static final int f = 12;
    private static final int g = 16;
    private static final int h = 26;
    private static final float[] i = new float[0];
    private static final double[] j = new double[0];
    private static final boolean[] k = new boolean[0];
    private static final byte[][] l = new byte[0][];

    public static final int a(iw iwVar, int i2) throws IOException {
        int i3 = iwVar.i();
        iwVar.b(i2);
        int i4 = 1;
        while (iwVar.a() == i2) {
            iwVar.b(i2);
            i4++;
        }
        iwVar.a(i3, i2);
        return i4;
    }
}
