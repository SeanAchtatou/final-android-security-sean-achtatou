package com.google.android.gms.internal.measurement;

final /* synthetic */ class dp implements dn {

    /* renamed from: a  reason: collision with root package name */
    private final Cdo f2168a;

    /* renamed from: b  reason: collision with root package name */
    private final String f2169b;

    dp(Cdo doVar, String str) {
        this.f2168a = doVar;
        this.f2169b = str;
    }

    public final Object a() {
        Cdo doVar = this.f2168a;
        return de.a(doVar.f2167b.getContentResolver(), this.f2169b);
    }
}
