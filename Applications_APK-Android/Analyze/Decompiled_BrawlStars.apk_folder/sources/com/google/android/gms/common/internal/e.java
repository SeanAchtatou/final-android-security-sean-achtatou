package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.content.Context;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.d;
import com.google.android.gms.common.c;
import com.google.android.gms.common.internal.f;
import java.util.Set;

public abstract class e<T extends IInterface> extends BaseGmsClient<T> implements a.f, f.a {
    protected final b h;
    private final Set<Scope> i;
    private final Account j;

    /* access modifiers changed from: protected */
    public Set<Scope> a(Set<Scope> set) {
        return set;
    }

    protected e(Context context, Looper looper, int i2, b bVar, d.b bVar2, d.c cVar) {
        this(context, looper, g.a(context), c.a(), i2, bVar, (d.b) l.a(bVar2), (d.c) l.a(cVar));
    }

    public final Account l() {
        return this.j;
    }

    /* access modifiers changed from: protected */
    public final Set<Scope> q() {
        return this.i;
    }

    public int g() {
        return super.g();
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private e(android.content.Context r12, android.os.Looper r13, com.google.android.gms.common.internal.g r14, com.google.android.gms.common.c r15, int r16, com.google.android.gms.common.internal.b r17, com.google.android.gms.common.api.d.b r18, com.google.android.gms.common.api.d.c r19) {
        /*
            r11 = this;
            r9 = r11
            r10 = r17
            r0 = r18
            r1 = r19
            r2 = 0
            if (r0 != 0) goto L_0x000c
            r6 = r2
            goto L_0x0012
        L_0x000c:
            com.google.android.gms.common.internal.t r3 = new com.google.android.gms.common.internal.t
            r3.<init>(r0)
            r6 = r3
        L_0x0012:
            if (r1 != 0) goto L_0x0016
            r7 = r2
            goto L_0x001c
        L_0x0016:
            com.google.android.gms.common.internal.u r0 = new com.google.android.gms.common.internal.u
            r0.<init>(r1)
            r7 = r0
        L_0x001c:
            java.lang.String r8 = r10.h
            r0 = r11
            r1 = r12
            r2 = r13
            r3 = r14
            r4 = r15
            r5 = r16
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
            r9.h = r10
            android.accounts.Account r0 = r10.f1595a
            r9.j = r0
            java.util.Set<com.google.android.gms.common.api.Scope> r0 = r10.c
            java.util.Set r1 = r11.a(r0)
            java.util.Iterator r2 = r1.iterator()
        L_0x0038:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x0053
            java.lang.Object r3 = r2.next()
            com.google.android.gms.common.api.Scope r3 = (com.google.android.gms.common.api.Scope) r3
            boolean r3 = r0.contains(r3)
            if (r3 == 0) goto L_0x004b
            goto L_0x0038
        L_0x004b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Expanding scopes is not permitted, use implied scopes instead"
            r0.<init>(r1)
            throw r0
        L_0x0053:
            r9.i = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.internal.e.<init>(android.content.Context, android.os.Looper, com.google.android.gms.common.internal.g, com.google.android.gms.common.c, int, com.google.android.gms.common.internal.b, com.google.android.gms.common.api.d$b, com.google.android.gms.common.api.d$c):void");
    }
}
