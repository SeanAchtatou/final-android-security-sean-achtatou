package com.google.android.gms.common.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.d;
import com.google.android.gms.common.internal.BaseGmsClient;

final class u implements BaseGmsClient.b {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ d.c f1622a;

    u(d.c cVar) {
        this.f1622a = cVar;
    }

    public final void a(ConnectionResult connectionResult) {
        this.f1622a.onConnectionFailed(connectionResult);
    }
}
