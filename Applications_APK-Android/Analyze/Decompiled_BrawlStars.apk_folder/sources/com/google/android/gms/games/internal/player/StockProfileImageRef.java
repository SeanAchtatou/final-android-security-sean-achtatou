package com.google.android.gms.games.internal.player;

import android.net.Uri;
import android.os.Parcel;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.google.android.gms.common.data.d;

public class StockProfileImageRef extends d implements StockProfileImage {
    public final /* synthetic */ Object a() {
        throw new NoSuchMethodError();
    }

    public final String b() {
        return e(MessengerShareContentUtility.IMAGE_URL);
    }

    public final Uri c() {
        throw new NoSuchMethodError();
    }

    public int describeContents() {
        throw new NoSuchMethodError();
    }

    public void writeToParcel(Parcel parcel, int i) {
        throw new NoSuchMethodError();
    }
}
