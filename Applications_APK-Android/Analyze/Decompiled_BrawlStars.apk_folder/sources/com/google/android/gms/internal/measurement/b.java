package com.google.android.gms.internal.measurement;

import com.google.android.gms.analytics.l;
import java.util.HashMap;
import java.util.Map;

public final class b extends l<b> {

    /* renamed from: a  reason: collision with root package name */
    public final Map<String, Object> f2064a = new HashMap();

    public final String toString() {
        return l.a(this.f2064a, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.l.a(java.lang.String, java.lang.Object):java.lang.String
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.l.a(java.lang.Object, java.lang.Object):T
      com.google.android.gms.common.internal.l.a(boolean, java.lang.Object):void
      com.google.android.gms.common.internal.l.a(java.lang.String, java.lang.Object):java.lang.String */
    public final void a(String str, String str2) {
        com.google.android.gms.common.internal.l.a(str);
        if (str != null && str.startsWith("&")) {
            str = str.substring(1);
        }
        com.google.android.gms.common.internal.l.a(str, (Object) "Name can not be empty or \"&\"");
        this.f2064a.put(str, str2);
    }

    public final /* synthetic */ void a(l lVar) {
        b bVar = (b) lVar;
        com.google.android.gms.common.internal.l.a(bVar);
        bVar.f2064a.putAll(this.f2064a);
    }
}
