package com.google.android.gms.internal.measurement;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

final class hf {

    /* renamed from: a  reason: collision with root package name */
    private static final hf f2258a = new hf();

    /* renamed from: b  reason: collision with root package name */
    private final hk f2259b = new gi();
    private final ConcurrentMap<Class<?>, hj<?>> c = new ConcurrentHashMap();

    public static hf a() {
        return f2258a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.fs.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.Class, java.lang.String]
     candidates:
      com.google.android.gms.internal.measurement.fs.a(java.lang.Object, java.lang.Object):java.lang.Object
      com.google.android.gms.internal.measurement.fs.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.fs.a(java.lang.Object, java.lang.String):T
     arg types: [com.google.android.gms.internal.measurement.hj<T>, java.lang.String]
     candidates:
      com.google.android.gms.internal.measurement.fs.a(java.lang.Object, java.lang.Object):java.lang.Object
      com.google.android.gms.internal.measurement.fs.a(java.lang.Object, java.lang.String):T */
    public final <T> hj<T> a(Class cls) {
        fs.a((Object) cls, "messageType");
        hj<T> hjVar = this.c.get(cls);
        if (hjVar != null) {
            return hjVar;
        }
        hj<T> a2 = this.f2259b.a(cls);
        fs.a((Object) cls, "messageType");
        fs.a((Object) a2, "schema");
        hj<T> putIfAbsent = this.c.putIfAbsent(cls, a2);
        return putIfAbsent != null ? putIfAbsent : a2;
    }

    public final <T> hj<T> a(Object obj) {
        return a((Class) obj.getClass());
    }

    private hf() {
    }
}
