package com.google.android.gms.measurement.internal;

import android.os.IInterface;
import android.os.RemoteException;
import java.util.List;

public interface zzaj extends IInterface {
    List<zzfv> a(zzk zzk, boolean z) throws RemoteException;

    List<zzo> a(String str, String str2, zzk zzk) throws RemoteException;

    List<zzo> a(String str, String str2, String str3) throws RemoteException;

    List<zzfv> a(String str, String str2, String str3, boolean z) throws RemoteException;

    List<zzfv> a(String str, String str2, boolean z, zzk zzk) throws RemoteException;

    void a(long j, String str, String str2, String str3) throws RemoteException;

    void a(zzag zzag, zzk zzk) throws RemoteException;

    void a(zzag zzag, String str, String str2) throws RemoteException;

    void a(zzfv zzfv, zzk zzk) throws RemoteException;

    void a(zzk zzk) throws RemoteException;

    void a(zzo zzo) throws RemoteException;

    void a(zzo zzo, zzk zzk) throws RemoteException;

    byte[] a(zzag zzag, String str) throws RemoteException;

    void b(zzk zzk) throws RemoteException;

    String c(zzk zzk) throws RemoteException;

    void d(zzk zzk) throws RemoteException;
}
