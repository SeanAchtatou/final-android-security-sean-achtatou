package com.google.android.gms.internal.drive;

import java.io.IOException;

public abstract class bx {
    protected volatile int g = -1;

    /* access modifiers changed from: protected */
    public int a() {
        return 0;
    }

    public void a(br brVar) throws IOException {
    }

    /* renamed from: b */
    public bx clone() throws CloneNotSupportedException {
        return (bx) super.clone();
    }

    public String toString() {
        return by.a(this);
    }

    public static final byte[] a(bx bxVar) {
        int a2 = bxVar.a();
        bxVar.g = a2;
        byte[] bArr = new byte[a2];
        try {
            br a3 = br.a(bArr, bArr.length);
            bxVar.a(a3);
            if (a3.f1901a.remaining() == 0) {
                return bArr;
            }
            throw new IllegalStateException(String.format("Did not write as much data as expected, %s bytes remaining.", Integer.valueOf(a3.f1901a.remaining())));
        } catch (IOException e) {
            throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", e);
        }
    }
}
