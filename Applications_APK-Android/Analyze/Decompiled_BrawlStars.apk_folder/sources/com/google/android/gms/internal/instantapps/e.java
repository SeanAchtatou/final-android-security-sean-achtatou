package com.google.android.gms.internal.instantapps;

import android.content.Context;
import android.os.Bundle;
import android.os.Process;
import android.os.RemoteException;
import com.google.android.gms.instantapps.b;

public final class e implements b {

    /* renamed from: a  reason: collision with root package name */
    private static e f2002a;

    /* renamed from: b  reason: collision with root package name */
    private final Context f2003b;
    private final boolean c = true;

    public static synchronized e a(Context context) {
        e eVar;
        synchronized (e.class) {
            Context a2 = q.a(context);
            if (!(f2002a != null && f2002a.f2003b == a2 && f2002a.c)) {
                f2002a = new e(a2);
            }
            eVar = f2002a;
        }
        return eVar;
    }

    private e(Context context) {
        this.f2003b = context;
    }

    public final boolean a(byte[] bArr) {
        b a2 = b.a(this.f2003b);
        if (a2 == null) {
            return false;
        }
        try {
            int myUid = Process.myUid();
            Bundle bundle = new Bundle();
            bundle.putInt("uid", myUid);
            bundle.putByteArray("cookie", bArr);
            return a2.a("setInstantAppCookie", bundle).getBoolean("result");
        } catch (RemoteException unused) {
            return false;
        }
    }

    public final byte[] a() {
        b a2 = b.a(this.f2003b);
        if (a2 == null) {
            return null;
        }
        try {
            int myUid = Process.myUid();
            Bundle bundle = new Bundle();
            bundle.putInt("uid", myUid);
            return a2.a("getInstantAppCookie", bundle).getByteArray("result");
        } catch (RemoteException unused) {
            return null;
        }
    }
}
