package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class eh implements Parcelable.Creator<zzk> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zzk[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int a2 = SafeParcelReader.a(parcel);
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        long j4 = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        long j5 = -2147483648L;
        boolean z = true;
        boolean z2 = false;
        int i = 0;
        boolean z3 = true;
        boolean z4 = true;
        boolean z5 = false;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 2:
                    str = SafeParcelReader.l(parcel2, readInt);
                    break;
                case 3:
                    str2 = SafeParcelReader.l(parcel2, readInt);
                    break;
                case 4:
                    str3 = SafeParcelReader.l(parcel2, readInt);
                    break;
                case 5:
                    str4 = SafeParcelReader.l(parcel2, readInt);
                    break;
                case 6:
                    j = SafeParcelReader.f(parcel2, readInt);
                    break;
                case 7:
                    j2 = SafeParcelReader.f(parcel2, readInt);
                    break;
                case 8:
                    str5 = SafeParcelReader.l(parcel2, readInt);
                    break;
                case 9:
                    z = SafeParcelReader.c(parcel2, readInt);
                    break;
                case 10:
                    z2 = SafeParcelReader.c(parcel2, readInt);
                    break;
                case 11:
                    j5 = SafeParcelReader.f(parcel2, readInt);
                    break;
                case 12:
                    str6 = SafeParcelReader.l(parcel2, readInt);
                    break;
                case 13:
                    j3 = SafeParcelReader.f(parcel2, readInt);
                    break;
                case 14:
                    j4 = SafeParcelReader.f(parcel2, readInt);
                    break;
                case 15:
                    i = SafeParcelReader.d(parcel2, readInt);
                    break;
                case 16:
                    z3 = SafeParcelReader.c(parcel2, readInt);
                    break;
                case 17:
                    z4 = SafeParcelReader.c(parcel2, readInt);
                    break;
                case 18:
                    z5 = SafeParcelReader.c(parcel2, readInt);
                    break;
                case 19:
                    str7 = SafeParcelReader.l(parcel2, readInt);
                    break;
                default:
                    SafeParcelReader.b(parcel2, readInt);
                    break;
            }
        }
        SafeParcelReader.x(parcel2, a2);
        return new zzk(str, str2, str3, str4, j, j2, str5, z, z2, j5, str6, j3, j4, i, z3, z4, z5, str7);
    }
}
