package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.measurement.dc;
import com.google.android.gms.internal.measurement.dd;
import java.util.List;
import java.util.Map;
import java.util.Set;

final class au implements aw {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Map f2630a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ Map f2631b;
    private final /* synthetic */ Map c;
    private final /* synthetic */ Map d;

    au(Map map, Map map2, Map map3, Map map4) {
        this.f2630a = map;
        this.f2631b = map2;
        this.c = map3;
        this.d = map4;
    }

    public final void a(dd ddVar, Set<dc> set, Set<dc> set2, ar arVar) {
        List list = (List) this.f2630a.get(ddVar);
        this.f2631b.get(ddVar);
        if (list != null) {
            set.addAll(list);
            arVar.c();
        }
        List list2 = (List) this.c.get(ddVar);
        this.d.get(ddVar);
        if (list2 != null) {
            set2.addAll(list2);
            arVar.d();
        }
    }
}
