package com.google.android.gms.internal.measurement;

final class hc {

    /* renamed from: a  reason: collision with root package name */
    private static final ha f2256a = c();

    /* renamed from: b  reason: collision with root package name */
    private static final ha f2257b = new hb();

    static ha a() {
        return f2256a;
    }

    static ha b() {
        return f2257b;
    }

    private static ha c() {
        try {
            return (ha) Class.forName("com.google.protobuf.NewInstanceSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
