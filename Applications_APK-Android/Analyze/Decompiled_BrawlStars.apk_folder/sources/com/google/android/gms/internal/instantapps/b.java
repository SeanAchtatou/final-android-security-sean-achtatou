package com.google.android.gms.internal.instantapps;

import android.content.Context;
import android.content.pm.ProviderInfo;
import android.os.Build;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private static b f1999a;

    /* renamed from: b  reason: collision with root package name */
    private final Context f2000b;

    private static synchronized void a() {
        synchronized (b.class) {
            f1999a = null;
        }
    }

    public static synchronized b a(Context context) {
        b bVar;
        synchronized (b.class) {
            Context a2 = q.a(context);
            if (f1999a == null || f1999a.f2000b != a2) {
                b bVar2 = null;
                if (Build.VERSION.SDK_INT >= 21) {
                    if (j.a(a2)) {
                        ProviderInfo resolveContentProvider = a2.getPackageManager().resolveContentProvider(c.f2001a.getAuthority(), 0);
                        if (resolveContentProvider != null) {
                            if (!resolveContentProvider.packageName.equals("com.google.android.gms")) {
                                String str = resolveContentProvider.packageName;
                                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 85);
                                sb.append("Package ");
                                sb.append(str);
                                sb.append(" is invalid for instant apps content provider; instant apps will be disabled.");
                                sb.toString();
                            } else {
                                bVar2 = new b(a2);
                            }
                        }
                    }
                }
                f1999a = bVar2;
            }
            bVar = f1999a;
        }
        return bVar;
    }

    private b(Context context) {
        this.f2000b = context;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x001f */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.os.Bundle a(java.lang.String r6, android.os.Bundle r7) throws android.os.RemoteException {
        /*
            r5 = this;
            long r0 = android.os.Binder.clearCallingIdentity()
            android.content.Context r2 = r5.f2000b     // Catch:{ IllegalArgumentException -> 0x002a, SecurityException -> 0x001f }
            android.content.ContentResolver r2 = r2.getContentResolver()     // Catch:{ IllegalArgumentException -> 0x002a, SecurityException -> 0x001f }
            android.net.Uri r3 = com.google.android.gms.internal.instantapps.c.f2001a     // Catch:{ IllegalArgumentException -> 0x002a, SecurityException -> 0x001f }
            r4 = 0
            android.os.Bundle r6 = r2.call(r3, r6, r4, r7)     // Catch:{ IllegalArgumentException -> 0x002a, SecurityException -> 0x001f }
            android.os.Binder.restoreCallingIdentity(r0)
            if (r6 == 0) goto L_0x0017
            return r6
        L_0x0017:
            android.os.RemoteException r6 = new android.os.RemoteException
            r6.<init>()
            throw r6
        L_0x001d:
            r6 = move-exception
            goto L_0x0035
        L_0x001f:
            a()     // Catch:{ all -> 0x001d }
            android.os.RemoteException r6 = new android.os.RemoteException     // Catch:{ all -> 0x001d }
            java.lang.String r7 = "SecurityException: Content provider unavailable. Likely framework issue."
            r6.<init>(r7)     // Catch:{ all -> 0x001d }
            throw r6     // Catch:{ all -> 0x001d }
        L_0x002a:
            a()     // Catch:{ all -> 0x001d }
            android.os.RemoteException r6 = new android.os.RemoteException     // Catch:{ all -> 0x001d }
            java.lang.String r7 = "IAE: Content provider unavailable. Likely GmsCore down."
            r6.<init>(r7)     // Catch:{ all -> 0x001d }
            throw r6     // Catch:{ all -> 0x001d }
        L_0x0035:
            android.os.Binder.restoreCallingIdentity(r0)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.instantapps.b.a(java.lang.String, android.os.Bundle):android.os.Bundle");
    }
}
