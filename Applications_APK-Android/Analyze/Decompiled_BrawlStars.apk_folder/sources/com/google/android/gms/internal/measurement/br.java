package com.google.android.gms.internal.measurement;

import android.content.Context;
import android.os.Handler;
import com.google.android.gms.common.internal.l;
import com.google.android.gms.internal.measurement.bv;

public final class br<T extends Context & bv> {
    private static Boolean c;

    /* renamed from: a  reason: collision with root package name */
    final Handler f2091a = new cc();

    /* renamed from: b  reason: collision with root package name */
    public final T f2092b;

    public br(T t) {
        l.a((Object) t);
        this.f2092b = t;
    }

    public static boolean a(Context context) {
        l.a(context);
        Boolean bool = c;
        if (bool != null) {
            return bool.booleanValue();
        }
        boolean a2 = bx.a(context, "com.google.android.gms.analytics.AnalyticsService");
        c = Boolean.valueOf(a2);
        return a2;
    }

    public final void a() {
        t.a((Context) this.f2092b).a().b("Local AnalyticsService is starting up");
    }

    public final void b() {
        t.a((Context) this.f2092b).a().b("Local AnalyticsService is shutting down");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0057, code lost:
        if (r4 == false) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005f, code lost:
        if (r1.i == 1) goto L_0x0061;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0061, code lost:
        com.google.android.gms.common.stats.d.a().a(r1.f, com.google.android.gms.common.stats.c.a(r1.f2617b, r8), 8, r1.e, r8, null, r1.d, com.google.android.gms.common.util.s.a(r1.c));
        r1.i--;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int a(android.content.Intent r14, int r15) {
        /*
            r13 = this;
            java.lang.Object r0 = com.google.android.gms.internal.measurement.bq.f2089a     // Catch:{ SecurityException -> 0x008f }
            monitor-enter(r0)     // Catch:{ SecurityException -> 0x008f }
            com.google.android.gms.stats.a r1 = com.google.android.gms.internal.measurement.bq.f2090b     // Catch:{ all -> 0x008c }
            if (r1 == 0) goto L_0x008a
            android.os.PowerManager$WakeLock r2 = r1.f2617b     // Catch:{ all -> 0x008c }
            boolean r2 = r2.isHeld()     // Catch:{ all -> 0x008c }
            if (r2 == 0) goto L_0x008a
            java.util.concurrent.atomic.AtomicInteger r2 = r1.j     // Catch:{ all -> 0x008c }
            int r2 = r2.decrementAndGet()     // Catch:{ all -> 0x008c }
            if (r2 >= 0) goto L_0x0022
            java.lang.String r2 = r1.e     // Catch:{ all -> 0x008c }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ all -> 0x008c }
            java.lang.String r3 = " release without a matched acquire!"
            r2.concat(r3)     // Catch:{ all -> 0x008c }
        L_0x0022:
            r2 = 0
            java.lang.String r8 = r1.a(r2)     // Catch:{ all -> 0x008c }
            java.lang.Object r2 = r1.f2616a     // Catch:{ all -> 0x008c }
            monitor-enter(r2)     // Catch:{ all -> 0x008c }
            boolean r3 = r1.g     // Catch:{ all -> 0x0087 }
            r12 = 1
            if (r3 == 0) goto L_0x0059
            java.util.Map<java.lang.String, java.lang.Integer[]> r3 = r1.h     // Catch:{ all -> 0x0087 }
            java.lang.Object r3 = r3.get(r8)     // Catch:{ all -> 0x0087 }
            java.lang.Integer[] r3 = (java.lang.Integer[]) r3     // Catch:{ all -> 0x0087 }
            r4 = 0
            if (r3 != 0) goto L_0x003b
            goto L_0x0057
        L_0x003b:
            r5 = r3[r4]     // Catch:{ all -> 0x0087 }
            int r5 = r5.intValue()     // Catch:{ all -> 0x0087 }
            if (r5 != r12) goto L_0x004a
            java.util.Map<java.lang.String, java.lang.Integer[]> r3 = r1.h     // Catch:{ all -> 0x0087 }
            r3.remove(r8)     // Catch:{ all -> 0x0087 }
            r4 = 1
            goto L_0x0057
        L_0x004a:
            r5 = r3[r4]     // Catch:{ all -> 0x0087 }
            int r5 = r5.intValue()     // Catch:{ all -> 0x0087 }
            int r5 = r5 - r12
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x0087 }
            r3[r4] = r5     // Catch:{ all -> 0x0087 }
        L_0x0057:
            if (r4 != 0) goto L_0x0061
        L_0x0059:
            boolean r3 = r1.g     // Catch:{ all -> 0x0087 }
            if (r3 != 0) goto L_0x0082
            int r3 = r1.i     // Catch:{ all -> 0x0087 }
            if (r3 != r12) goto L_0x0082
        L_0x0061:
            com.google.android.gms.common.stats.d r3 = com.google.android.gms.common.stats.d.a()     // Catch:{ all -> 0x0087 }
            android.content.Context r4 = r1.f     // Catch:{ all -> 0x0087 }
            android.os.PowerManager$WakeLock r5 = r1.f2617b     // Catch:{ all -> 0x0087 }
            java.lang.String r5 = com.google.android.gms.common.stats.c.a(r5, r8)     // Catch:{ all -> 0x0087 }
            r6 = 8
            java.lang.String r7 = r1.e     // Catch:{ all -> 0x0087 }
            r9 = 0
            int r10 = r1.d     // Catch:{ all -> 0x0087 }
            android.os.WorkSource r11 = r1.c     // Catch:{ all -> 0x0087 }
            java.util.List r11 = com.google.android.gms.common.util.s.a(r11)     // Catch:{ all -> 0x0087 }
            r3.a(r4, r5, r6, r7, r8, r9, r10, r11)     // Catch:{ all -> 0x0087 }
            int r3 = r1.i     // Catch:{ all -> 0x0087 }
            int r3 = r3 - r12
            r1.i = r3     // Catch:{ all -> 0x0087 }
        L_0x0082:
            monitor-exit(r2)     // Catch:{ all -> 0x0087 }
            r1.a()     // Catch:{ all -> 0x008c }
            goto L_0x008a
        L_0x0087:
            r1 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0087 }
            throw r1     // Catch:{ all -> 0x008c }
        L_0x008a:
            monitor-exit(r0)     // Catch:{ all -> 0x008c }
            goto L_0x0090
        L_0x008c:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x008c }
            throw r1     // Catch:{ SecurityException -> 0x008f }
        L_0x008f:
        L_0x0090:
            T r0 = r13.f2092b
            com.google.android.gms.internal.measurement.t r0 = com.google.android.gms.internal.measurement.t.a(r0)
            com.google.android.gms.internal.measurement.bk r0 = r0.a()
            r1 = 2
            if (r14 != 0) goto L_0x00a3
            java.lang.String r14 = "AnalyticsService started with null intent"
            r0.e(r14)
            return r1
        L_0x00a3:
            java.lang.String r14 = r14.getAction()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r15)
            java.lang.String r3 = "Local AnalyticsService called. startId, action"
            r0.a(r3, r2, r14)
            java.lang.String r2 = "com.google.android.gms.analytics.ANALYTICS_DISPATCH"
            boolean r14 = r2.equals(r14)
            if (r14 == 0) goto L_0x00c0
            com.google.android.gms.internal.measurement.bs r14 = new com.google.android.gms.internal.measurement.bs
            r14.<init>(r13, r15, r0)
            r13.a(r14)
        L_0x00c0:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.br.a(android.content.Intent, int):int");
    }

    public final void a(Runnable runnable) {
        t.a((Context) this.f2092b).c().a((ba) new bu(this, runnable));
    }
}
