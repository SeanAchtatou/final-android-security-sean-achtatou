package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;

public final class m implements Parcelable.Creator<zzv> {
    public final /* synthetic */ Object[] newArray(int i) {
        return new zzv[i];
    }

    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int a2 = SafeParcelReader.a(parcel);
        FilterHolder filterHolder = null;
        while (parcel.dataPosition() < a2) {
            int readInt = parcel.readInt();
            if ((65535 & readInt) != 1) {
                SafeParcelReader.b(parcel, readInt);
            } else {
                filterHolder = (FilterHolder) SafeParcelReader.a(parcel, readInt, FilterHolder.CREATOR);
            }
        }
        SafeParcelReader.x(parcel, a2);
        return new zzv(filterHolder);
    }
}
